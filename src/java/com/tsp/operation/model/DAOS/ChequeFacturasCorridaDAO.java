/***************************************
 * Nombre Clase ............. ChequeFacturasCorridaDAO.java
 * Descripci�n  .. . . . . .  Generamos Los Cheques de las facturas aprobada para pago dentro de una corrida.
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  07/03/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/


package com.tsp.operation.model.DAOS;




import java.io.*;
import java.util.*;
import java.sql.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.services.TasaService;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;






public class ChequeFacturasCorridaDAO extends MainDAO{
    
    
    
    
     public  static String TABLA_TAMANO_BANCO = "TPAGEBANCO";
    
     public  static String OFICINA_PPAL       = "OP";
    
    
    
    
    
    
    
    
    /** ____________________________________________________________________________________________
     * METODOS
     * ____________________________________________________________________________________________ */
    
    
    
    
      public ChequeFacturasCorridaDAO() {
        super("ChequeFacturasCorridaDAO.xml") ;    
      }
      public ChequeFacturasCorridaDAO(String dataBaseName) {
        super("ChequeFacturasCorridaDAO.xml",dataBaseName) ;    
      }
    
      
      
     /**
     * M�todos que setea valores nulos
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
       private String reset(String val){
            if(val==null)
               val = "";
            return val;
       }
      
    
      
      /**
       * M�todo que devuelve la agencia real para la consulta
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      private String getAgencia(String agencia){
           String newAgencia  =   ( agencia.equals( this.OFICINA_PPAL ) )?" like '%' " : " ='" + agencia + "'";
           return newAgencia;
      }
      
      
      
      
      
      
      
      
    /**
       * M�todo que busca los bancos dependiendo la agencia del usuario         
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  List loadBancos(String distrito, String agencia) throws Exception{
            Connection         con     = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            List               lista   = new LinkedList();
            String             query   = "SQL_LOAD_BANCOS";
            try{
                
                con                =   this.conectar(query);
                String newAgencia  =   getAgencia(agencia);
                String sql         =   this.obtenerSQL( query ).replaceAll("#AGENCIA#", newAgencia );
                st                 =   con.prepareStatement( sql );
                
                st= con.prepareStatement(sql);
                st.setString(1, distrito   );
                
                rs=st.executeQuery();                
                while(rs.next()){
                    String banco =  rs.getString("banco");
                    lista.add(banco);
                }
                
            }catch(Exception e){
                  throw new Exception( "loadBancos " + e.getMessage());
            }
            finally{
                if(st!=null) st.close();
                if(rs!=null) rs.close();
                this.desconectar(query);
            }
            return lista; 
      }
      
      
   
   
      /**
       * M�todo que busca los bancos dependiendo la agencia del usuario         
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  List loadSucursales(String distrito, String agencia) throws Exception{
            Connection         con     = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            List               lista   = new LinkedList();
            String             query   = "SQL_LOAD_SUCURSALES";
            try{
                
                con                =   this.conectar(query);
                String newAgencia  =   getAgencia(agencia);
                String sql         =   this.obtenerSQL( query ).replaceAll("#AGENCIA#", newAgencia );
                st                 =   con.prepareStatement( sql );
                
                st= con.prepareStatement(sql);
                st.setString(1, distrito   );
                
                rs=st.executeQuery();                
                while(rs.next()){
                    Hashtable  hc =  new Hashtable();
                        hc.put("banco",    rs.getString("banco") );
                        hc.put("sucursal", rs.getString("sucursal") );
                    lista.add(hc);
                }
                
            }catch(Exception e){
                  throw new Exception( "loadSucursales " + e.getMessage());
            }
            finally{
                if(st!=null) st.close();
                if(rs!=null) rs.close();
                this.desconectar(query);
            }
            return lista; 
      }
      
   
      
    
      /**
       * M�todo que busca las distintas corridas que tengan facturas aprobadas para pago, 
         teniendo en cuenta distrito y agencia del usuario.
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  List getCorridasConFacturasPago(String distrito, String agencia) throws Exception{
            Connection         con     = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            List               lista   = new LinkedList();
            String             query   = "SQL_CORRIDAS_CON_FACTURAS_APROBADAS";
            try{
                
                con = this.conectar(query);
                String newAgencia  =   getAgencia(agencia);
                String sql         =   this.obtenerSQL( query ).replaceAll("#AGENCIA#", newAgencia );
                st                 =   con.prepareStatement( sql );
                
                st= con.prepareStatement(sql);
                st.setString(1, distrito   );
                
                ////System.out.println("CORRIDA "  +  st.toString() );
                rs=st.executeQuery();                
                while(rs.next()){
                    String corrida =  rs.getString("corrida");
                    lista.add(corrida);
                }
                
            }catch(Exception e){
                  throw new Exception( "getCorridasConFacturasPago " + e.getMessage());
            }
            finally{
                if(st!=null) st.close();
                if(rs!=null) rs.close();
                this.desconectar(query);
            }
            return lista; 
      }
      
      
      
      
      
       /**
       * M�todo que busca los bancos de la  corridas que tengan facturas aprobadas para pago, 
         teniendo en cuenta distrito y agencia del usuario.
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  List getBancosConFacturasPago(String distrito, String corrida,  String agencia) throws Exception{
            Connection         con     = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            List               lista   = new LinkedList();
            String             query   = "SQL_BANCOS_CON_FACTURAS_APROBADAS";
            try{
                
                con = this.conectar(query);
                String newAgencia  =   getAgencia(agencia);
                String sql         =   this.obtenerSQL( query ).replaceAll("#AGENCIA#", newAgencia );
                st                 =   con.prepareStatement( sql );
                
                st= con.prepareStatement(sql);
                st.setString(1, distrito   );
                st.setString(2, corrida    );
                
                rs=st.executeQuery();                
                while(rs.next()){
                    String banc =  rs.getString("banco");
                    lista.add(banc);
                }
                
            }catch(Exception e){
                  throw new Exception( "getBancosConFacturasPago " + e.getMessage());
            }
            finally{
                if(st!=null) st.close();
                if(rs!=null) rs.close();
                this.desconectar(query);
            }
            return lista; 
      }
      
      
      
      
      
       /**
       * M�todo que busca las sucursales de los bancos de la  corridas que tengan facturas aprobadas para pago, 
         teniendo en cuenta distrito y agencia del usuario.
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  List getSucursalesBancosConFacturasPago(String distrito, String corrida, String banco, String agencia) throws Exception{
            Connection         con     = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            List               lista   = new LinkedList();
            String             query   = "SQL_SUCURSALES_BANCOS_CON_FACTURAS_APROBADAS";
            try{
                
                con = this.conectar(query);
                String newAgencia  =   getAgencia(agencia);
                String sql         =   this.obtenerSQL( query ).replaceAll("#AGENCIA#", newAgencia );
                st                 =   con.prepareStatement( sql );
                
                st= con.prepareStatement(sql);
                st.setString(1, distrito   );
                st.setString(2, corrida    );
                st.setString(3, banco      );
                 
                rs=st.executeQuery();                
                while(rs.next()){
                    String suc =  rs.getString("sucursal");
                    lista.add( suc );
                }
                
            }catch(Exception e){
                  throw new Exception( " getSucursalesBancosConFacturasPago " + e.getMessage());
            }
            finally{
                if(st!=null) st.close();
                if(rs!=null) rs.close();
                this.desconectar(query);
            }
            return lista; 
      }
      
      
      
      
      
      
      
      
      /**
       *  M�todo que busca los distintos beneficiarios a los cuales se le han aprobado para pago
       *  las facturas dentro de una corrida. Tener en cuenta distrito y agencia del usuario.
       *  Reutilaza la funcion getBancoProveedor de la clase  ChequeXFacturaDAO, para extraer datos del banco 
       *  del proveedor, ya que el cheque hay que generarselo  en la moneda de su banco.
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  List getBeneficiariosFacturasCorrida(String distrito, String corrida, String banco, String sucursal, String agencia) throws Exception{
            Connection         con     = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            List               lista   = new LinkedList();
            String             query   = "SQL_BENEFICIARIOS_FACTURA_CORRIDA";
            ChequeXFacturaDAO  dao     = new ChequeXFacturaDAO();
            try{
                
                con = this.conectar(query);
                String newAgencia  =   getAgencia(agencia);
                String sql         =   this.obtenerSQL( query ).replaceAll("#AGENCIA#", newAgencia );
                st                 =   con.prepareStatement( sql );
                st.setString(1, distrito   );
                st.setString(2, corrida    );
                st.setString(3, banco      );
                st.setString(4, sucursal   );
                 
                rs=st.executeQuery(); 
                while(rs.next()){
                    String     nit           =  rs.getString("nit");
                    Hashtable  datoProveedor =  dao.getBancoProveedor(distrito, nit );
                    if( datoProveedor!=null  )
                        lista.add  (datoProveedor);
                }
                
            }catch(Exception e){
                  throw new Exception( " getBeneficiariosFacturasCorrida " +e.getMessage());
            }
            finally{
                if(st!=null) st.close();
                if(rs!=null) rs.close();
                this.desconectar(query);
            }
            return lista; 
      }
      
      
      
      
      /**
       *  M�todo que busca las facturas aprobadas para el beneficiario dentro de la corrida
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  List getFacturasAprobadasBeneficiarios(String distrito, String corrida, String banco, String sucursal, String proveedor, String agencia) throws Exception{
            Connection         con     = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            List               lista   = new LinkedList();
            String             query   = "SQL_FACTURAS_APROBADAS_PROVEEDOR_CORRIDA";
            try{
                
                con = this.conectar(query);
                String newAgencia  =   getAgencia(agencia);
                String sql         =   this.obtenerSQL( query ).replaceAll("#AGENCIA#", newAgencia );
                st                 =   con.prepareStatement( sql );
                st.setString(1, distrito   );
                st.setString(2, corrida    );
                st.setString(3, banco      );
                st.setString(4, sucursal   );
                st.setString(5, proveedor  );
                
                rs=st.executeQuery();
                while(rs.next()){
                    FacturasCheques  factura = this.load(rs);
                    lista.add  (factura);
                }
                
            }catch(Exception e){
                  throw new Exception( " getFacturasAprobadasBeneficiarios " + e.getMessage());
            }
            finally{
                if(st!=null) st.close();
                if(rs!=null) rs.close();
                this.desconectar(query);
            }
            return lista; 
      }
      
      
      
      
      
      
       
      /**
       * M�todo que busca los cheques de la corrida establecida en mims: 281
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public String  pageBanco(String banco) throws Exception{
            Connection         con     = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            String             page    = "";
            String             query   = "SQL_TAMA�O_PAGINAS_BANCOS";
            try{
                con = this.conectar(query);
                String sql   =   this.obtenerSQL( query );
                st= con.prepareStatement(sql);
                st.setString(1, this.TABLA_TAMANO_BANCO );
                st.setString(2, banco   );
                rs=st.executeQuery(); 
                while(rs.next()){
                    page = this.reset( rs.getString("descripcion") );
                    break;                    
                }
                
            }catch(Exception e){
                  throw new Exception( "pageBanco " + e.getMessage());
            }
            finally{
                if(st!=null) st.close();
                if(rs!=null) rs.close();
                this.desconectar(query);
            }
            return page; 
      }

      
       /**
       * M�todo que actualiza la factura, campos abono, saldo, y/o fecha y usuario de cancelacion
       * @autor.......fvillacob
       * @parameter   FacturasCheques    factura, double abono
       * @throws......Exception
       * @version.....1.0.
       **/
      public String updateCXP_DOC  (String cheque, FacturasCheques    factura, double abono, String user)throws Exception{
       
          PreparedStatement  st      = null;
          String             sql     = "";
          String            query    = "SQL_UPDATE_CXP_DOC";
          try{
            
              TasaService  svc   =  new  TasaService();
              String  hoy        = Util.getFechaActual_String(4);
              
              
              String monedaLocal = factura.getMonedaLocal();
              String monedaME    = factura.getMoneda();
              String monedaBanco = factura.getMonedaBanco();              
              
              double vlrLocal    = abono;
              double vlrME       = abono;              
              
              if( !monedaBanco.equals( monedaME )  ){
                  Tasa  obj  =  svc.buscarValorTasa(monedaLocal,  monedaBanco , monedaME , hoy);
                  if(obj!=null){
                        vlrME =  abono *  obj.getValor_tasa();
                  } 
              }
              
             
              if( !monedaBanco.equals( monedaLocal )  ){
                  Tasa obj  =  svc.buscarValorTasa(monedaLocal,  monedaBanco , monedaLocal , hoy);
                  if(obj!=null){
                        vlrLocal = abono * obj.getValor_tasa();
                  }
              }
              
              vlrME     =  ( monedaME.equals("PES")     ||  monedaME.equals("BOL")     ) ? (int)Math.round(vlrME)    : Util.roundByDecimal(vlrME, 2);
              vlrLocal  =  ( monedaLocal.equals("PES")  ||  monedaLocal.equals("BOL")  ) ? (int)Math.round(vlrLocal) : Util.roundByDecimal(vlrLocal, 2);                        
             
              
              st= this.crearPreparedStatement(query);
                st.setDouble(1,   vlrLocal  );
                st.setDouble(2,   vlrLocal  );
                st.setDouble(3,   vlrME     );
                st.setDouble(4,   vlrME     );
                st.setString(5,   cheque    );
                st.setString(6,   user      );                 
                st.setString(7,   factura.getDstrct()         );
                st.setString(8,   factura.getProveedor()      );
                st.setString(9,   factura.getTipo_documento() );
                st.setString(10,  factura.getDocumento()      );
              
              sql = st.toString();
              
          }catch(Exception e){
              throw new Exception( " updateCXP_DOC: " + e.getMessage());
          }
          finally{
              if(st!=null) st.close();
              this.desconectar(query);
          }
          return sql;
      }
      
      
      
      
      
      
      /**
       * M�todo que actualiza la tabla corrida
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public String  updateCorrida(String cheque, FacturasCheques    factura, String user) throws Exception{
            Connection         con     = null;
            PreparedStatement  st      = null;
            String             query   = "SQL_UPDATE_CHEQUE_CORRIDA";
            String             sql     = "";
            try{
                con = this.conectar(query);
                st= con.prepareStatement(  this.obtenerSQL( query ) );
                
                st.setString(1,  cheque );
                st.setString(2,  user   );
                st.setString(3,  factura.getDstrct()         );
                st.setString(4,  factura.getCorrida()        );
                st.setString(5,  factura.getProveedor()      );
                st.setString(6,  factura.getTipo_documento() );
                st.setString(7,  factura.getDocumento()      );
                
                sql = st.toString();                
                
            }catch(Exception e){
                  throw new Exception( "updateCorrida " + e.getMessage());
            }
            finally{
                if(st!=null) st.close();
                this.desconectar(query);
            }
            return sql;
      }
      
      
      
      
      
     /**
     * M�todo que carga el sql para la actualizacion de la Serie
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... ImpresionCheque cheque, String usuario
     * @version..... 1.0.
     **/
   public  synchronized String ActualizarSeries(Cheque cheque, String usuario)throws Exception {
    
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        String            sql   = "";
        String            query = "SQL_UPDATE_SERIE";
        try{
         
            int proximoImprimir = cheque.getLastNumber() + 1;
            
      //--- realizamos la actualizacion
            st= this.crearPreparedStatement(query);   
            st.setInt   (1, proximoImprimir            );
            st.setString(2, usuario                    );
            st.setString(3, cheque.getDistrito()       );
            st.setString(4, cheque.getBanco()        );
            st.setString(5, cheque.getSucursal()  );
            sql = st.toString();
            
            
        }catch(Exception e){
            throw new SQLException("No se pudo actualizar la SERIE " + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return sql;
    }

   /**
     * M�todo que finaliza la serie
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... ImpresionCheque cheque, String usuario
     * @version..... 1.0.
     **/
   public String FinalizarSeriesCorridasCero(Cheque cheque, String usuario)throws Exception {
        PreparedStatement st     = null;
        String            sql    = "";
        String            query  = "SQL_UPDATE_FINALIZAR_SERIE";
        try{
            st= this.crearPreparedStatement(query); 
                st.setString(1, usuario);
                st.setString(2, cheque.getDistrito() );
                st.setString(3, cheque.getMonto()==0?"BANCO_CERO":cheque.getBanco()       );
                st.setString(4, cheque.getMonto()==0?"SUCURSAL_CERO":cheque.getSucursal() );
                
           sql = st.toString();
           
        }catch(Exception e){
            throw new SQLException("No se pudo finalizar la SERIE "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            this.desconectar(query);
        }
        return sql;
    }
   
    /**
     * M�todo que verifica si existe la corrida dada
         teniendo en cuenta distrito y agencia del usuario.
     * @autor.......Osvaldo P�rez Ferrer
     * @throws......Exception
     * @version.....1.0.
     **/
    public  boolean existeCorridasConFacturasPago( String corrida, String distrito, String agencia, String banco, String sucursal, String nit) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        boolean existe = false;
        String             query   = "SQL_EXISTE_CORRIDA_CON_FACTURAS_APROBADAS";
        try{
            
            con = this.conectar(query);
            String newAgencia  =   getAgencia(agencia);
            String sqlNit      =   (nit.equals("ALL"))?"":" AND beneficiario = '" + nit +"'";
            String sql         =   this.obtenerSQL( query ).replaceAll("#AGENCIA#", newAgencia ).replaceAll("#NIT#", sqlNit );
            st                 =   con.prepareStatement( sql );
            
            //System.out.println(  sql.toString() );
            
            st= con.prepareStatement(sql);
            st.setString( 1, distrito   );
            st.setInt   ( 2, Integer.parseInt(corrida ) ) ;
            st.setString( 3, banco      );
            st.setString( 4, sucursal   );
            
            rs=st.executeQuery();
            if(rs.next()){
                existe = true;
            }
            
        }catch(Exception e){
            throw new Exception( "existeCorridasConFacturasPago " + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return existe;
    }
    
    /***************************************************************************
       *  M�todo que busca los distintos beneficiarios a los cuales se le han aprobado para pago
       *  las facturas dentro de una corrida. Tener en cuenta distrito y agencia del usuario.
       *  Reutilaza la funcion getBancoProveedor de la clase  ChequeXFacturaDAO, para extraer datos del banco 
       *  del proveedor, ya que el cheque hay que generarselo  en la moneda de su banco.
       *  NOTA: este metodo tiene el mismo prop�sito que getBeneficiariosFacturasCorrida, difiere en que
       *        trae el nit del beneficiario aunque no presente banco, esto para el programa de impresion
       *        de cheques por facturas corridas.
       * @autor.......Ing. Osvaldo P�rez Ferrer
       * @throws......Exception
       * @version.....1.0.
       ***************************************************************************/
      public  List getBeneficiariosFacturasCorrida2(String distrito, String corrida, String banco, String sucursal, String nit, String agencia) throws Exception{
            Connection         con     = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            List               lista   = new LinkedList();
            String             query   = "SQL_BENEFICIARIOS_FACTURA_CORRIDA";
            ChequeXFacturaDAO  dao     = new ChequeXFacturaDAO();
            try{
                
                con = this.conectar(query);
                String newAgencia  =   getAgencia(agencia);
                String sqlNit      =   (nit.equals("ALL"))?"":" AND beneficiario = '" + nit +"'";
                String sql         =   this.obtenerSQL( query ).replaceAll("#AGENCIA#", newAgencia ).replaceAll("#NIT#", sqlNit );
                st                 =   con.prepareStatement( sql );
                
                
                st.setString(1, distrito   );
                st.setString(2, corrida    );
                st.setString(3, banco      );
                st.setString(4, sucursal   );
                 
                rs=st.executeQuery(); 
                while(rs.next()){
                    String     no            =  rs.getString("nit");
                    Hashtable  datoProveedor =  dao.getBancoProveedor2(distrito, no );
                    if( datoProveedor!=null  )
                        lista.add  (datoProveedor);
                }
                
            }catch(Exception e){
                  throw new Exception( " getBeneficiariosFacturasCorrida " +e.getMessage());
            }
            finally{
                if(st!=null) st.close();
                if(rs!=null) rs.close();
                this.desconectar(query);
            }
            return lista; 
      }
      /**
       * M�todo que carga datos del sql para la factura
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public FacturasCheques load(ResultSet   rs )throws Exception{
          FacturasCheques    factura = new FacturasCheques();
          try{
              
                factura.setDstrct            (  reset( rs.getString("dstrct")          ) );
                factura.setCorrida           (  reset( rs.getString("corrida")         ) );
                factura.setTipo_documento    (  reset( rs.getString("tipo_documento")  ) );
                factura.setDocumento         (  reset( rs.getString("documento")       ) );
                factura.setDescripcion       (  reset( rs.getString("descripcion")     ) );                
                factura.setProveedor         (  reset( rs.getString("beneficiario")    ) );
                factura.setNomProveedor      (  reset( rs.getString("nombre")          ) );
                factura.setVlr_saldo         (         rs.getDouble("valor")             );                
                factura.setVlr_saldo_me      (         rs.getDouble("valor_me")          );                
                factura.setBanco             (  reset( rs.getString("banco")           ) );
                factura.setSucursal          (  reset( rs.getString("sucursal")        ) );                    
                factura.setAgencia           (  reset( rs.getString("agencia_banco")   ) );
                factura.setMoneda            (  reset( rs.getString("moneda")          ) );
                factura.setFecha_aprobacion  (  reset( rs.getString("pago")            ) );
                factura.setUsuario_aprobacion(  reset( rs.getString("usuario_pago")    ) );
                factura.setBase              (  reset( rs.getString("base")            ) );
                
                factura.setMonedaBanco       (  reset( rs.getString("moneda_banco")    ) );
                factura.setOc                (  reset( rs.getString("planilla")        ) );   
                factura.setFecha_documento( reset( rs.getString("fecha_documento") ));//AMATURANA 26.02.2007
                
          }catch(Exception e){
              throw new Exception(e.getMessage());
          }
          return factura;
      }
      
    /**
     * M�todo que verifica si las corridas permiten factura
     *        con valores en 0
     * @autor.......Jose de la rosa
     * @throws......Exception
     * @version.....1.0.
     **/
    public  boolean permiteCorridasConFacturasCero( String distrito, String corrida ) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        boolean existe = false;
        String             query   = "SQL_BUSCAR_INFO_CORRIDAS";
        try{
            
            con = this.conectar(query);
            String sql         =   this.obtenerSQL( query );
            st                 =   con.prepareStatement( sql );
            
            st= con.prepareStatement(sql);
            st.setString( 1, distrito   );
            st.setInt   ( 2, Integer.parseInt(corrida ) ) ;
            
            rs=st.executeQuery();
            existe = rs.next();
            
        }catch(Exception e){
            throw new Exception( " permiteCorridasConFacturasCero " + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return existe;
    }
    
    /**
     * M�todo que carga el sql para la actualizacion de la Serie
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... ImpresionCheque cheque, String usuario
     * @version..... 1.0.
     **/
   public  synchronized String ActualizarSeriesCorridaCero(Cheque cheque, String usuario)throws Exception {
    
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        String            sql   = "";
        String            query = "SQL_UPDATE_SERIE_ID";
        try{
         
            int proximoImprimir = cheque.getLastNumber() + 1;
            
      //--- realizamos la actualizacion
            st= this.crearPreparedStatement(query);   
            st.setInt   ( 1, proximoImprimir            );
            st.setString( 2, usuario                    );
            st.setInt(    3, cheque.getSerie().getId() );
            sql = st.toString();
            
            
        }catch(Exception e){
            throw new SQLException("No se pudo actualizar la SERIE " + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return sql;
    }
      
    
   
    /**
     * M�todo que finaliza la serie
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... ImpresionCheque cheque, String usuario
     * @version..... 1.0.
     **/
   public String FinalizarSeries(Cheque cheque, String usuario)throws Exception {
        PreparedStatement st     = null;
        String            sql    = "";
        String            query  = "SQL_UPDATE_FINALIZAR_SERIE";
        try{
            st= this.crearPreparedStatement(query); 
                st.setString(1, usuario);
                st.setString(2, cheque.getDistrito() );
                st.setString(3, cheque.getBanco()       );
                st.setString(4, cheque.getSucursal() );
                
           sql = st.toString();
           
        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("No se pudo finalizar la SERIE "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            this.desconectar(query);
        }
        return sql;
    }
   

}
