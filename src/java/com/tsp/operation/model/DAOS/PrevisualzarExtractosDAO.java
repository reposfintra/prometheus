/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.CmbGeneralScBeans;
import com.tsp.operation.model.beans.PrevisualizarExtractos;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author lcanchila
 */
public interface PrevisualzarExtractosDAO {

    public ArrayList<PrevisualizarExtractos> previsualizarExtractoMicro(String periodo, String accion, String usuario, String unegocio, String[] vencimientos, String ciclo, String fechaHoy);

    public ArrayList<PrevisualizarExtractos> previsualizarExtractoFenalco(String periodo, String unegocio, String accion, String usuario, String[] vencimientos, String ciclo, String fechaHoy);

    public String insertarNegociosExtracto(String negocio, String nit, String periodo, String unegocio, String login, int num_ciclo);

    public void limpiarRegistros(String periodo, String unegocio, int num_ciclo);

    public boolean isExtractoGenerado(String periodo, String unegocio);

    public boolean hayRegistrosAGenerar(String periodo, String unegocio, int num_ciclo);

    public ArrayList<PrevisualizarExtractos> exportarExtractoFenalco(String periodo, String unegocio, String vencMayor, String[] vencimientos, String ciclo);

    public ArrayList<PrevisualizarExtractos> exportarExtractoMicro(String periodo, String unegocio, String vencMayor, String[] vencimientos, String ciclo);

    public int obtenerNumCicloPago(String fechaHoy);
    
    public ArrayList<CmbGeneralScBeans> cargarPeriodos();

    public String obtenerFechaMaximaCiclo(String fechaHoy);

    public String tomarFotoCiclo(String periodo, String ciclo);

    public boolean isMIGenerado();

    public boolean isFotoGenerada(String periodo, String ciclo);

    public ArrayList<PrevisualizarExtractos> obtenerDetalleSanciones(String periodo, String tipo, String[] listUnd);

    public boolean isSancionesGeneradas(String periodo, String tipo);

    public String copiarSancionesMes(String periodo, String login);

    public ArrayList<String> insertSancionesMes(String login, String periodo, ArrayList<PrevisualizarExtractos> lista, String tipo);

    public ArrayList<PrevisualizarExtractos> obtenerDetalleCondonaciones(String periodo, String tipo, String[] listUnd);

    public ArrayList insertCondonacionesMes(String login, String periodo, ArrayList<PrevisualizarExtractos> lista, String tipo);
    
    /**
     * Metodo que toma la foto general para la line de fenalco
     * @return
     */
    public String tomarFotoMes();

}
