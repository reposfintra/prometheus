/*********************************************************************************
 * Nombre clase :      ReporteFacturaDestinatarioDAO.java                        *
 * Descripcion :       DAO del ReporteFacturaDestinatarioDAO.java                *
 *                     Clase que maneja los DAO (Data Access Object) los cuales  *
 *                     contienen los metodos que interactuan con la B.D.         *
 * Autor :             LREALES                                                   *
 * Fecha :             03 de abril de 2006, 11:52 AM                             *
 * Version :           1.0                                                       *
 * Copyright :         Fintravalores S.A.                                   *
 *********************************************************************************/

package com.tsp.operation.model.DAOS;

import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;
import java.util.Date;
import java.text.*;
import com.tsp.util.connectionpool.PoolManager;

public class ReporteFacturaDestinatarioDAO extends MainDAO {
    
    private Vector planilla;   
    private Vector remesa;
    private HojaReportes hr = null;
    
    /** Creates a new instance of ReporteFacturaDestinatarioDAO */
    public ReporteFacturaDestinatarioDAO () {
        
        super( "ReporteFacturaDestinatarioDAO.xml" );
        
    }
       
     /**
     * Setter for property planilla.
     * @param plaman New value of property planilla.
     */
    public void setPlanilla (java.util.Vector planilla) {
        
        this.planilla = planilla;
        
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.util.Vector getPlanilla () {
        
        return planilla;
        
    }
    
    /**
     * Setter for property remesa.
     * @param plaman New value of property remesa.
     */
    public void setRemesa (java.util.Vector remesa) {
        
        this.remesa = remesa;
        
    }
    
    /**
     * Getter for property remesa.
     * @return Value of property remesa.
     */
    public java.util.Vector getRemesa () {
        
        return remesa;
        
    }
    
    /**
     * Getter for property hr.
     * @return Value of property hr.
     */
    public HojaReportes getHojRep () {
        
        return this.hr;
        
    }     
    
    
    /** Funcion publica que obtiene algunos datos de la planilla */
    public void listaPlanilla ( String numpla ) throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_REPORTE_PLANILLA" );
            st.setString( 1, numpla );
            rs = st.executeQuery();
            
            this.planilla = new Vector();
            
            while( rs.next() ){
                
                hr = new HojaReportes();
                
                hr.setTipo("PLANILLA");
                hr.setNumero(rs.getString("numpla"));
                hr.setDestinatario(rs.getString("destinatario"));
                hr.setTipo_documento(rs.getString("tipo_doc"));
                hr.setDocumento(rs.getString("documento"));
                hr.setTipo_doc_rel(rs.getString("tipo_doc_rel"));
                hr.setDoc_rel(rs.getString("documento_rel"));
                String d = "0099-01-01 00:00";
                String fc = rs.getString("feccum");
                if (fc.equals(d))
                    hr.setFecha_cum("");
                else
                    hr.setFecha_cum( rs.getString("feccum") );
                String cantidad_unidad = ( (rs.getString("cantidad")!=null)?rs.getString("cantidad"):"" ) + " " + ( (rs.getString("unidad")!=null)?rs.getString("unidad"):"" );
                hr.setCantidad_cum(cantidad_unidad);
                hr.setDiscrepancia(rs.getString("num_discre"));
                hr.setObservacion(rs.getString("observacion"));
                
                planilla.add ( hr );
                
            }
            
        } catch( SQLException e ){
            
            throw new SQLException( "ERROR DURANTE LA BUSQUEDA DE LA PLANILLA" + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_REPORTE_PLANILLA" );
            
        }
        
    }
    
    /** Funcion publica que obtiene algunos datos de la remesa */
    public void listaRemesa ( String numrem ) throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_REPORTE_REMESA" );
            st.setString( 1, numrem );
            rs = st.executeQuery();
            
            this.remesa = new Vector();
            
            while( rs.next() ){
                
                hr = new HojaReportes();
                
                hr.setTipo("REMESA");
                hr.setNumero(rs.getString("numrem"));
                hr.setDestinatario(rs.getString("destinatario"));
                hr.setTipo_documento(rs.getString("tipo_doc"));
                hr.setDocumento(rs.getString("documento"));
                hr.setTipo_doc_rel(rs.getString("tipo_doc_rel"));
                hr.setDoc_rel(rs.getString("documento_rel"));
                String d = "0099-01-01 00:00";
                String fc = rs.getString("feccum");
                if (fc.equals(d))
                    hr.setFecha_cum("");
                else
                    hr.setFecha_cum( rs.getString("feccum") );
                String cantidad_unidad = ( (rs.getString("cantidad")!=null)?rs.getString("cantidad"):"" ) + " " + ( (rs.getString("unidad")!=null)?rs.getString("unidad"):"" );
                hr.setCantidad_cum(cantidad_unidad);
                hr.setDiscrepancia(rs.getString("num_discre"));
                hr.setObservacion(rs.getString("observacion"));
                
                remesa.add ( hr );
                
            }
            
        } catch( SQLException e ){
            
            throw new SQLException( "ERROR DURANTE LA BUSQUEDA DE LA REMESA" + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_REPORTE_REMESA" );
            
        }
        
    }
    
    /** Funcion publica que verifica si existe la planilla */
    public boolean existePlanilla ( String numpla ) throws SQLException {
        
        boolean boo = false;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_EXISTE_PLANILLA" );
            st.setString( 1, numpla );
            
            rs = st.executeQuery();            
            hr = new HojaReportes();
            
            while( rs.next() ){
                
                boo = true;
                hr.setNumero( rs.getString( "numpla" ) );
                
            }
            
            
        } catch( SQLException e ){
            
            throw new SQLException( "ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS POR NUMERO DE PLANILLA" + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_EXISTE_PLANILLA" );
            
        }
        
        return boo;
        
    }
    
    /** Funcion publica que verifica si existe la remesa */
    public boolean existeRemesa ( String numrem ) throws SQLException {
        
        boolean boo = false;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_EXISTE_REMESA" );
            st.setString( 1, numrem );
            
            rs = st.executeQuery();
            hr = new HojaReportes();
            
            while( rs.next() ){
                
                boo = true;
                hr.setNumero( rs.getString( "numrem" ) );
                
            }
            
        } catch( SQLException e ){
            
            throw new SQLException( "ERROR DURANTE LA BUSQUEDA DE LAS REMESAS POR NUMERO DE REMESA" + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_EXISTE_REMESA" );
            
        }
        
        return boo;
        
    }
    
}