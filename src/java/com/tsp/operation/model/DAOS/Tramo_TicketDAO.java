/*
 * Nombre        Tramo_TicketDAO.java
 * Autor         Ing Jesus Cuestas
 * Modificado    Ing Sandra Escalante   
 * Fecha         25 de junio de 2005, 10:12 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;
import com.tsp.operation.model.DAOS.MainDAO;

public class Tramo_TicketDAO extends MainDAO{
    
    Tramo_Ticket ticket;
    Vector tickets;
    private TreeMap peajes;
    
    private static String SQL_INSERT = "INSERT INTO tramo_ticket (  rec_status, " +
    "                                                               cia, origen, " +
    "                                                               destino, " +
    "                                                               ticket_id, " +
    "                                                               cantidad_maximo_ticket, " +
    "                                                               last_update, " +
    "                                                               user_update, " +
    "                                                               creation_date, " +
    "                                                               creation_user, " +
    "                                                               base    ) " +
    "                                   VALUES ('',?,?,?,?,?, 'now()',?, 'now()',?,?)";
    
    private static String SQL_ANULAR = "UPDATE  tramo_ticket " +
    "                                   SET     rec_status='A', " +
    "                                           last_update = 'now()', " +
    "                                           user_update = ? " +
    "                                   WHERE   cia = ? " +
    "                                           AND origen = ? " +
    "                                           AND destino = ? " +
    "                                           AND ticket_id = ?";
    
    private static String SQL_BUSCAR = "SELECT  t.cia, " +
    "                                           COALESCE (t.ticket_id, '' ) AS ticket_id," +
    "                                           t.cantidad_maximo_ticket,                " +
    "                                           get_nombreciudad(t.origen) AS norigen, " +
    "                                           get_nombreciudad(t.destino) AS ndestino, " +
    "                                           COALESCE (p.description, 'NO REGISTRA') AS descripcion" +
    "                                   FROM 	tramo_ticket t LEFT JOIN peajes p ON ( p.ticket_id = t.ticket_id )                " +
    "                                   WHERE   t.rec_status != 'A'  " +
    "                                           AND t.cia = ?  " +
    "                                           AND t.origen = ?  " +
    "                                           AND t.destino = ? " +
    "                                           AND t.ticket_id = ? ";
    
    private static String SQL_LISTAR = "SELECT  t.*," +                
    "                                           get_nombreciudad(t.origen) AS nomorigen," +
    "                                           get_nombreciudad(t.destino) AS nomdest" +
    "                                   FROM    tramo_ticket t " +                
    "                                   WHERE   t.rec_status != 'A' " +    
    "                                           AND t.cia = ? " +
    "                                           AND t.origen=? " +
    "                                           AND t.destino = ?";    
    
    private static String SQL_UPDATE = "UPDATE  tramo_ticket " +
    "                                   SET     cantidad_maximo_ticket = ?, " +
    "                                           last_update = 'now()', " +
    "                                           user_update = ?, " +
    "                                           rec_status = '' " +
    "                                   WHERE   cia = ? " +
    "                                           AND origen = ? " +
    "                                           AND destino = ? " +
    "                                           AND ticket_id = ?";
    
    private static String SQL_VERIFICAR2 = " SELECT  *  " +
    "                                       FROM    tramo_ticket " +
    "                                       WHERE   cia = ? " +
    "                                               AND origen = ? " +
    "                                               AND destino = ? " +
    "                                               AND ticket_id = ? " +
    "                                               AND rec_status = 'A' ";
    
    //sescalante
    private static String SQL_LISTAR_PEAJES = " SELECT  ticket_id, " +
    "                                                   description " +
    "                                           FROM    peajes " +
    "                                           WHERE   dstrct = ?";
    
    //sescalante
    private static String SQL_VERIFICAR = " SELECT  ticket_id " +
    "                                       FROM    peajes " +
    "                                       WHERE   dstrct  = ? " +
    "                                               AND ticket_id = ? ";
    /** Creates a new instance of Tramo_Ticket_DAO */
    public Tramo_TicketDAO() {
        super("Tramo_TicketDAO.xml");
    }
    
    /**
     * Getter for property ticket.
     * @return Value of property ticket.
     */
    public com.tsp.operation.model.beans.Tramo_Ticket getTicket() {
        return ticket;
    }
    
    /**
     * Setter for property ticket.
     * @param ticket New value of property ticket.
     */
    public void setTicket(com.tsp.operation.model.beans.Tramo_Ticket ticket) {
        this.ticket = ticket;
    }
    
    /**
     * Getter for property tickets.
     * @return Value of property tickets.
     */
    public java.util.Vector getTickets() {
        return tickets;
    }
    
    /**
     * Setter for property tickets.
     * @param tickets New value of property tickets.
     */
    public void setTickets(java.util.Vector tickets) {
        this.tickets = tickets;
    }
    
    /**
     * Metodo <tt>agregarTramo_Ticket</tt>, registra un ticket asociado a un tramo
     * @autor : Ing. Jesus Cuestas
     * @version : 1.0
     */
    public void agregarTramo_Ticket()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        String query = "SQL_INSERT";
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, ticket.getCia());
                st.setString(2, ticket.getOrigen());
                st.setString(3, ticket.getDestino());
                st.setString(4, ticket.getTicket_id());
                st.setFloat(5, ticket.getCantidad());
                st.setString(6, ticket.getUsuario());
                st.setString(7, ticket.getUsuario());
                st.setString(8, ticket.getBase());
                st.executeUpdate();
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CREACION DEL TRAMO_TICKET " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    /**
     * Metodo <tt>anularTramo_Ticket</tt>, anula un tiquete del tramo
     * @autor : Ing. Jesus Cuestas
     * @version : 1.0
     */
    public void anularTramo_Ticket()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        String query = "SQL_ANULAR";
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, ticket.getUsuario());
                st.setString(2, ticket.getCia());
                st.setString(3, ticket.getOrigen());
                st.setString(4, ticket.getDestino());
                st.setString(5, ticket.getTicket_id());
                st.executeUpdate();
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DEL TRAMO_TICKET " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    /**
     * Metodo <tt>buscarTramo_Ticket</tt>, obtiene un ticket especifico de un tramo
     * @autor : Ing. Jesus Cuestas
     * @param distrito, origen, destino del tramo y id del tiquete (String)
     * @version : 1.0
     */
    public void buscarTramo_Ticket(String cia, String origen, String destino, String ticket_id)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR";
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1,cia);
                st.setString(2,origen);
                st.setString(3,destino);
                st.setString(4,ticket_id);
                rs= st.executeQuery();
                if(rs.next()){
                    ticket = new Tramo_Ticket();
                    ticket.setCia(rs.getString("cia"));
                    ticket.setOrigen(rs.getString("norigen"));
                    ticket.setDestino(rs.getString("ndestino"));
                    ticket.setTicket_id(rs.getString("ticket_id"));
                    ticket.setCantidad(rs.getFloat("cantidad_maximo_ticket"));
                    ticket.setDtiquete(rs.getString("descripcion"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL TRAMO_TICKET " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>listarTramos_Ticket</tt>, obtiene los tiquetes de un tramo
     * @autor : Ing. Jesus Cuestas
     ** @param distrito, origen, destino del tramo
     * @version : 1.0
     */
    public void listarTramos_Ticket(String cia, String origen, String destino)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR";
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1,cia);
                st.setString(2,origen);
                st.setString(3,destino);
                rs= st.executeQuery();
                tickets = new Vector();
                while(rs.next()){
                    ticket = new Tramo_Ticket();
                    ticket.setCia(rs.getString("cia"));
                    ticket.setOrigen(rs.getString("nomorigen"));
                    ticket.setDestino(rs.getString("nomdest"));
                    ticket.setTicket_id(rs.getString("ticket_id"));
                    ticket.setCantidad(rs.getFloat("cantidad_maximo_ticket"));
                    tickets.add(ticket);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DEL TRAMO_TICKET " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    /**
     * Metodo <tt>modificarTramo_Ticket</tt>, modifica un tiquete de un tramo
     * @autor : Ing. Jesus Cuestas
     * @version : 1.0
     */
    public void modificarTramo_Ticket()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        String query = "SQL_UPDATE";
        try {
            con  = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setFloat(1, ticket.getCantidad());
                st.setString(2, ticket.getUsuario());
                st.setString(3, ticket.getCia());
                st.setString(4, ticket.getOrigen());
                st.setString(5, ticket.getDestino());
                st.setString(6, ticket.getTicket_id());
                st.executeUpdate();
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACION DEL TRAMO_TICKET " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    /**
     * Metodo <tt>existeTramo_Ticket</tt>, verifica la existencia de un tiquete en un tramo
     * @autor : Ing. Jesus Cuestas
     * @param distrito, origen, destino del tramo y id del tiquete (String)
     * @return boolean
     * @version : 1.0
     */
    public boolean existeTramo_Ticket(String cia, String origen, String destino, String ticket_id)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;//no existe
        String query = "SQL_VERIFICAR2";
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1,cia);
                st.setString(2,origen);
                st.setString(3,destino);
                st.setString(4,ticket_id);
                //////System.out.println("ST " + st);
                rs = st.executeQuery();
                if(rs.next()){
                    sw = true;//si existe
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR VERIFICACION EL TRAMO_TICKET " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
    
    /**
     * Metodo <tt>existeIDTicket</tt>, verifica la existencia de un id ticket en peajes
     * @autor : Ing. Sandra Escalante          
     * @param disrito (String), id ticke (String) 
     * @return boolean
     * @version : 1.0
     */
    public boolean existeTicketID(String dstrct, String ticket_id)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;//no existe
        String query = "SQL_VERIFICAR";
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1,dstrct);
                st.setString(2,ticket_id);                              
                rs = st.executeQuery();
                if(rs.next()){
                    sw = true;//si existe
                }
            }            
        }catch(SQLException e){
            throw new SQLException("ERROR VERIFICANDO TIQUETE EN PEAJES" + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
    
   
    /**
     * Metodo <tt>listarPeajes</tt>, obtiene la lista de peajes del sistema
     * @autor : Ing. Sandra Escalante
     * @param disrito (String)
     * @version : 1.0
     */
    public void listarPeajes(String dstrct)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        peajes = null;
        String query = "SQL_LISTAR_PEAJES";
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1,dstrct);                                           
                rs = st.executeQuery();
                peajes = new TreeMap();
                while(rs.next()){
                    peajes.put(rs.getString("description"), rs.getString("ticket_id")) ;
                }
                
            }            
        }catch(SQLException e){
            throw new SQLException("ERROR VERIFICANDO EL TRAMO " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Getter for property peajes.
     * @return Value of property peajes.
     */
    public java.util.TreeMap getPeajes() {
        return peajes;
    }
}
