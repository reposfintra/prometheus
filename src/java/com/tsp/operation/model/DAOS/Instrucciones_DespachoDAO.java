/************************************************************************
 * Nombre clase: Instrucciones_DespachoDAO.java                         *
 * Descripci�n: Clase que maneja las consultas de la tabla donde se     *
 *              cargan las instrucciones de despacho                    *
 * Autor: Ing. Karen Reales                                             *
 * Fecha: Created on 23 de Enero de 2007, 09:25 AM                      *
 * Versi�n: Java 1.0                                                    *
 * Copyright: Fintravalores S.A. S.A.                              *
 ***********************************************************************/

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.MainDAO;
/**
 *
 * @author  Kreales
 */
public class Instrucciones_DespachoDAO extends MainDAO{
    
    /** Creates a new instance of CumplidoDAO */
    public Instrucciones_DespachoDAO() {
        super("Instrucciones_DespachoDAO.xml");
    }
    
    private Instrucciones_Despacho inst;
    private Vector lista;
    private Vector remesas;
    private Vector causas;
    
    
    /**
     * Metodo: ListarOrdenCompra, lista las ordenes de compra que no tienen
     *          relacionada una remesa
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public void ListarOrdenCompra(String dstrct)throws SQLException{
        
        PreparedStatement st = null, st2=null;
        ResultSet rs = null, rs2=null;
        lista = new Vector();
        
        
        try {
            Connection con = this.conectar("SQL_LISTA_OCOMPRA");
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL("SQL_LISTA_OCOMPRA"));
                st.setString(1, dstrct);
                rs = st.executeQuery();
                
                while(rs.next()){
                    Instrucciones_Despacho inst = new Instrucciones_Despacho();
                    inst.setOrder_no(rs.getString("order_no"));
                    inst.setOrder_qty(rs.getInt("order_qty"));
                    
                    st2 = con.prepareStatement(this.obtenerSQL("SQL_LISTA_REFERENCIAS"));
                    st2.setString(1, dstrct);
                    st2.setString(2, rs.getString("order_no"));
                    rs2 = st2.executeQuery();
                    String modelos = "";
                    while (rs2.next()){
                        modelos = modelos + rs2.getString("modelo")+",";
                    }
                    inst.setModelo(modelos);
                    
                    lista.add(inst);
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO LA LISTA DE ORDENES DE COMPRA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_LISTA_OCOMPRA");
        }
        
    }
    
    /**
     * Getter for property lista.
     * @return Value of property lista.
     */
    public java.util.Vector getLista() {
        return lista;
    }
    
    /**
     * Setter for property lista.
     * @param lista New value of property lista.
     */
    public void setLista(java.util.Vector lista) {
        this.lista = lista;
    }
    
    /**
     * Metodo: ListarRemesas, lista las remesas de una planilla
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public void ListarRemesas(String numpla)throws SQLException{
        
        PreparedStatement st = null, st2=null;
        ResultSet rs = null, rs2=null;
        remesas = new Vector();
        
        
        try {
            Connection con = this.conectar("SQL_LISTA_REMESAS");
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL("SQL_LISTA_REMESAS"));
                st.setString(1, numpla);
                rs = st.executeQuery();
                
                while(rs.next()){
                    Remesa rem = new Remesa();
                    rem.setNumpla(rs.getString("numpla"));
                    rem.setNumrem(rs.getString("numrem"));
                    rem.setPesoReal(rs.getFloat("pesoreal"));
                    rem.setDescripcion(rs.getString("descripcion"));
                    rem.setDocumento(rs.getString("order_no")!=null?rs.getString("order_no"):"");
                    rem.setDocumento_rel(rs.getString("factura")!=null?rs.getString("factura"):"");
                    rem.setFecha_factura(rs.getString("fecha_factura")!=null?rs.getString("fecha_factura"):"");
                    rem.setDistrito(rs.getString("cia"));
                    rem.setFecha_cargue("");
                    String fecha_cargue = "";
                    
                    try{
                        if(rs.getString("appoint_date")!=null){
                            if(rs.getString("appoint_date").length()==8 && rs.getString("appoint_time").length()==4){
                                int dias = Integer.parseInt(rs.getString("appoint_date"));
                                int horas = Integer.parseInt(rs.getString("appoint_time"));
                                
                                String fecha =rs.getString("appoint_date").substring(0,4)+"-"+rs.getString("appoint_date").substring(4,6)+ "-"+rs.getString("appoint_date").substring(6,8);
                                String time =rs.getString("appoint_time").substring(0,2)+":"+rs.getString("appoint_time").substring(2,4);
                                
                                fecha_cargue = fecha+" "+time;
                            }
                        }
                    }catch(NumberFormatException e){
                        fecha_cargue = "";
                    }
                    rem.setFecha_cargue(fecha_cargue);
                    rem.setFecha_realcargue(rs.getString("fecha_realcargue")!=null?rs.getString("fecha_realcargue"):"0099-01-01 00:00:00");
                    
                    st2 = con.prepareStatement(this.obtenerSQL("SQL_LISTA_DESTINATARIOS"));
                    st2.setString(1, rs.getString("numrem"));
                    rs2 = st2.executeQuery();
                    String destinatario = "";
                    String cod_destinatario = "";
                    if (rs2.next()){
                        destinatario =rs2.getString("nombre");
                        cod_destinatario = rs2.getString("codigo");
                        
                    }
                    rem.setNom_destinatario(destinatario);
                    rem.setDestinatario(cod_destinatario);
                    rem.setReferencias(buscarReferencias(rem));
                    remesas.add(rem);
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO LA LISTA DE REMESAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_LISTA_REMESAS");
        }
        
    }
    
    /**
     * Getter for property remesas.
     * @return Value of property remesas.
     */
    public java.util.Vector getRemesas() {
        return remesas;
    }
    
    /**
     * Setter for property remesas.
     * @param remesas New value of property remesas.
     */
    public void setRemesas(java.util.Vector remesas) {
        this.remesas = remesas;
    }
    /**
     * Metodo: existeOrdenCompra, COnsulta que retorta true o false si la
     *orden de compra existe.
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public boolean existeOrdenCompra(String dstrct, String ocompra)throws SQLException{
        
        PreparedStatement st = null, st2=null;
        ResultSet rs = null, rs2=null;
        boolean sw=false;
        
        
        try {
            Connection con = this.conectar("SQL_EXIST_OCOMPRA");
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL("SQL_EXIST_OCOMPRA"));
                st.setString(1, dstrct);
                st.setString(2, ocompra);
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO SI LA ORDEN DE COMPRA EXISTE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_EXIST_OCOMPRA");
        }
        return sw;
        
    }
    /**
     * Metodo: actualizarOCompra,actualiza en la tabla la orden de compra
     *con el numero de la remesa, el de la factura y la fecha de la factura.
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public String actualizarOCompra(Remesa r)throws SQLException{
        
        PreparedStatement st = null, st2=null;
        String sql="";
        try {
            Connection con = this.conectar("SQL_ACTUALIZAR_OCOMPRA");
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL("SQL_ACTUALIZAR_OCOMPRA"));
                st.setString(1,r.getNumrem());
                st.setString(2,r.getDocumento_rel());
                st.setString(3,r.getFecha_factura());
                st.setString(4,r.getDestinatario());
                st.setString(5,r.getDistrito());
                st.setString(6,r.getDocumento());
                sql = st.toString();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR ACTUALIZANDO LA FACTURA Y EL NUMERO DE REMESA EN LA ORDEN DE CARGUE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            
            this.desconectar("SQL_ACTUALIZAR_OCOMPRA");
        }
        return sql;
    }
    
    /**
     * Getter for property inst.
     * @return Value of property inst.
     */
    public com.tsp.operation.model.beans.Instrucciones_Despacho getInst() {
        return inst;
    }
    
    /**
     * Setter for property inst.
     * @param inst New value of property inst.
     */
    public void setInst(com.tsp.operation.model.beans.Instrucciones_Despacho inst) {
        this.inst = inst;
    }
    /**
     * Metodo: agregarDocumento, agrega o modifica documentos internos
     *de una remesa
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public String agregarDocumento(Remesa r)throws SQLException{
        
        PreparedStatement st = null, st2=null;
        String sql="";
        ResultSet rs = null, rs2=null;
        try {
            Connection con = this.conectar("SQL_ESTA_DOCUMENTO");
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL("SQL_ESTA_DOCUMENTO"));
                st.setString(1,r.getNumrem());
                st.setString(2,r.getDocumento());
                st.setString(3,r.getDestinatario());
                rs= st.executeQuery();
                if(rs.next()){
                    st2 = con.prepareStatement(this.obtenerSQL("SQL_ACTUALIZAR_DOCUMENTO"));
                    st2.setString(1,r.getDocumento_rel());
                    st2.setString(2,r.getDocumento_rel().equals("")?"":"FAC");
                    st2.setString(3,r.getNumrem());
                    st2.setString(4,r.getDocumento());
                    st2.setString(5,r.getDestinatario());
                    sql = st2.toString();
                }
                else{
                    st2 = con.prepareStatement(this.obtenerSQL("SQL_INSERTAR_DOCUMENTO"));
                    st2.setString(1,r.getDistrito());
                    st2.setString(2,r.getNumrem());
                    st2.setString(3,"OC");
                    st2.setString(4,r.getDocumento());
                    st2.setString(5,r.getDocumento_rel().equals("")?"":"FAC");
                    st2.setString(6,r.getDocumento_rel());
                    st2.setString(7,r.getDestinatario());
                    sql = st2.toString();
                }
                
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AGREGANDO EL DOCUMENTO INTERNO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            
            this.desconectar("SQL_ESTA_DOCUMENTO");
        }
        return sql;
    }
    /**
     * Metodo: borrarDocumento, borra de la tabla remesadocto un documento
     * interno de una remesa
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public String borrarDocumento(Remesa r)throws SQLException{
        
        PreparedStatement st = null, st2=null;
        String sql="";
        ResultSet rs = null, rs2=null;
        try {
            Connection con = this.conectar("SQL_BORRAR_DOCUMENTO");
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL("SQL_BORRAR_DOCUMENTO"));
                st.setString(1,r.getNumrem());
                st.setString(2,r.getDocuinterno());
                st.setString(3,r.getDestinatario());
                sql = st.toString();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR BORRANDO EL DOCUMENTO INTERNO "+r.getDocuinterno() + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            
            this.desconectar("SQL_BORRAR_DOCUMENTO");
        }
        return sql;
    }
    
    /**
     * Metodo: desmarcarRemesa, actualiza en la tabla la orden de compra
     *con el numero de la remesa, el de la factura y la fecha de la factura en default
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public String desmarcarRemesa(Remesa r)throws SQLException{
        
        PreparedStatement st = null, st2=null;
        String sql="";
        try {
            Connection con = this.conectar("SQL_ACTUALIZAR_OCOMPRA");
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL("SQL_ACTUALIZAR_OCOMPRA"));
                st.setString(1,"");
                st.setString(2,"");
                st.setString(3,"0099-01-01 00:00:00");
                st.setString(4,"");
                st.setString(5,r.getDistrito());
                st.setString(6,r.getDocuinterno());
                sql = st.toString();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR ACTUALIZANDO EN BLANCO LA FACTURA Y LA REMESA DE UNA ORDEN DE COMPRA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            
            this.desconectar("SQL_ACTUALIZAR_OCOMPRA");
        }
        return sql;
    }
    
    /**
     * Metodo: buscarReferencias, busca el vector de referencias que se encuentran
     *relacionadas a una orden de compra.
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public Vector buscarReferencias(Remesa r)throws SQLException{
        
        PreparedStatement st = null, st2=null;
        Vector vec = new Vector();
        ResultSet rs = null, rs2=null;
        try {
            Connection con = this.conectar("SQL_REFRENCIAS");
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL("SQL_REFRENCIAS"));
                st.setString(1,r.getDistrito());
                st.setString(2,r.getDocumento());
                rs = st.executeQuery();
                while (rs.next()){
                    Instrucciones_Despacho inst = new Instrucciones_Despacho();
                    inst.setModelo(rs.getString("modelo"));
                    inst.setOrder_no(rs.getString("order_no"));
                    inst.setOrder_qty(rs.getInt("order_qty"));
                    inst.setCant_despachada(rs.getInt("cant_despachada"));
                    inst.setCausa(rs.getString("causa"));
                    inst.setDesc_causa(rs.getString("descausa")!=null?rs.getString("descausa"):"");
                    vec.add(inst);
                    
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO LAS REFERENCIAS DE UNA PLANILLA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            
            this.desconectar("SQL_REFRENCIAS");
        }
        return vec;
    }
    
    /**
     * Getter for property causas.
     * @return Value of property causas.
     */
    public java.util.Vector getCausas() {
        return causas;
    }
    
    /**
     * Setter for property causas.
     * @param causas New value of property causas.
     */
    public void setCausas(java.util.Vector causas) {
        this.causas = causas;
    }
    /**
     * Metodo: ListarCausas, lista las causas del cargue incompleto
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public void ListarCausas()throws SQLException{
        
        PreparedStatement st = null, st2=null;
        ResultSet rs = null, rs2=null;
        causas = new Vector();
        
        
        try {
            Connection con = this.conectar("SQL_CAUSAS");
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL("SQL_CAUSAS"));
                rs = st.executeQuery();
                TablaGen t = new TablaGen();
                t.setTable_code("");
                t.setDescripcion("Seleccione un Item");
                causas.add(t);
                while(rs.next()){
                    t = new TablaGen();
                    t.setTable_code(rs.getString("table_code"));
                    t.setDescripcion(rs.getString("descripcion"));
                    causas.add(t);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO LA LISTA DE CAUSAS DEL CARGUE INCOMPLETO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_CAUSAS");
        }
        
    }
    /**
     * Metodo: actualizarCantCargue,actualiza la tabla con la cantidad
     *de despacho y la causa si aplica
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public String actualizarCantCargue(Instrucciones_Despacho inst)throws SQLException{
        
        PreparedStatement st = null, st2=null;
        String sql="";
        try {
            Connection con = this.conectar("SQL_ACTUALIZAR_CCARGUE");
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL("SQL_ACTUALIZAR_CCARGUE"));
                st.setInt(1,inst.getCant_despachada());
                st.setString(2,inst.getCausa());
                st.setString(3, inst.getDstrct());
                st.setString(4, inst.getOrder_no());
                st.setString(5, inst.getModelo());
                
                sql = st.toString();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL ACTUALIZAR LA CANTIDAD DEL DESPACHO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            
            this.desconectar("SQL_ACTUALIZAR_CCARGUE");
        }
        return sql;
    }
    /**
     * Metodo: actualizarFechaCargue,actualiza la tabla con la fecha real
     *de cargue
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public String actualizarFechaCargue(Remesa rem)throws SQLException{
        
        PreparedStatement st = null, st2=null;
        String sql="";
        try {
            Connection con = this.conectar("SQL_ACTUALIZAR_FCARGUE");
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL("SQL_ACTUALIZAR_FCARGUE"));
                st.setString(1,rem.getFecha_realcargue());
                st.setString(2, rem.getDistrito());
                st.setString(3, rem.getNumrem());
                
                sql = st.toString();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL ACTUALIZAR LA FECHA REAL DE CARGUE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            
            this.desconectar("SQL_ACTUALIZAR_FCARGUE");
        }
        return sql;
    }
    
   
    
    /**
     * Metodo: reporteNovedad, Genera el reporte de novedades
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public void reporteNovedad(String condicion, String fecha, String mes)throws SQLException{
        
        PreparedStatement st = null, st2=null;
        ResultSet rs = null, rs2=null;
        lista = new Vector();
        
        
        try {
            Connection con = this.conectar("SQL_LISTA_NOVEDAD");
            if(con!=null){
                String query = this.obtenerSQL("SQL_LISTA_NOVEDAD");
                query = query.replace("#CONDICION#",condicion);
                //System.out.println("QUery "+query);
                st = con.prepareStatement(query);
                st.setString(1, fecha);
                rs = st.executeQuery();
                //System.out.println("St "+st);
                while(rs.next()){
                    Instrucciones_Despacho inst = new Instrucciones_Despacho();
                    inst.setDstrct(rs.getString("dstrct"));
                    inst.setFecha_novedad(rs.getString("fechanovedad"));
                    inst.setNumrem(rs.getString("numrem")!=null?rs.getString("numrem"):"");
                    inst.setUbicacion_mercancia(rs.getString("ciudad"));
                    inst.setDesp(rs.getInt("desp"));
                    inst.setOrder_no(rs.getString("order_no"));
                    inst.setFactura(rs.getString("factura"));
                    inst.setFecha_factura(rs.getString("fecha_factura").equals("0099-01-01")?"":rs.getString("fecha_factura"));
                    inst.setShip_to_name(rs.getString("ship_to_name"));
                    inst.setModelo(rs.getString("modelo"));
                    inst.setAddr1(rs.getString("addr1"));
                    inst.setCiudad(rs.getString("ciudad"));
                    inst.setObservacion(rs.getString("observacion"));
                    inst.setBodega(rs.getString("bodega"));
                    inst.setCodigo_novedad(rs.getString("num_discre"));
                    inst.setCantida_novedad(rs.getDouble("cantidad"));
                    inst.setMotivo_novedad(rs.getString("causa_dev"));
                    inst.setSolucion_novedad(rs.getString("cod_sol"));
                    inst.setNom_solucion_novedad(rs.getString("nom_sol"));
                    inst.setFecha_solucion(rs.getString("fecha_solnovedad").equals("0099-01-01")?"":rs.getString("fecha_solnovedad"));
                    inst.setMes(mes);
                    lista.add(inst);
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO EL REPORTE DE TRAKIN " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_LISTA_NOVEDAD");
        }
        
    }
    /**
     * Metodo: ListarSolucion, lista las posibles soluciones de novedades
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public void ListarSolucion()throws SQLException {
        
        PreparedStatement st = null, st2=null;
        ResultSet rs = null, rs2=null;
        causas = new Vector();
        
        
        try {
            Connection con = this.conectar("SQL_SOLUCIONES");
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL("SQL_SOLUCIONES"));
                rs = st.executeQuery();
                TablaGen t = new TablaGen();
                t.setTable_code("");
                t.setDescripcion("Seleccione un Item");
                causas.add(t);
                while(rs.next()){
                    t = new TablaGen();
                    t.setTable_code(rs.getString("table_code"));
                    t.setDescripcion(rs.getString("descripcion"));
                    causas.add(t);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO LA LISTA DE SOLUCIONES DE LA NOVEDAD" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_SOLUCIONES");
        }
        
    }
    /**
     * Metodo: actualizarNovedada,actualiza en la tabla la orden de compra
     *con la fecha y solucion de la novedad
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public String actualizarNovedad(Instrucciones_Despacho i)throws SQLException{
        
        PreparedStatement st = null, st2=null;
        String sql="";
        try {
            Connection con = this.conectar("SQL_ACTUALIZAR_NOVEDAD");
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL("SQL_ACTUALIZAR_NOVEDAD"));
                st.setString(1,i.getSolucion_novedad().equals("")?"0099-01-01":"now()");
                st.setString(2,i.getSolucion_novedad());
                st.setString(3,i.getDstrct());
                st.setString(4,i.getOrder_no());
                st.setString(5,i.getModelo());
                sql = st.toString();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR ACTUALIZANDO LA SOLUCION DE LA NOVEDAD " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            
            this.desconectar("SQL_ACTUALIZAR_NOVEDAD");
        }
        return sql;
    }
     /**
     * Metodo: BuscarNumeroFactura, busca el numero de una factura
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public String BuscarNumeroFactura(String condicion)throws SQLException{
        
        PreparedStatement st = null, st2=null;
        ResultSet rs = null, rs2=null;
        String factura="";
        
        
        try {
            Connection con = this.conectar("SQL_BUSCAR_NOFAC");
            
            if(con!=null){
                
                String query = this.obtenerSQL("SQL_BUSCAR_NOFAC");
                query = query.replaceAll("#CONDICION#",condicion);
                
                st = con.prepareStatement(query);
                rs = st.executeQuery();
                
                if(rs.next()){
                    factura= rs.getString("documento");
                   
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO EL NUMERO DE UNA FACTURA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_BUSCAR_NOFAC");
        }
        return factura;
    }
    /**
     * Funcion publica que actualiza la informacion de una entrada planeada especifica.
     */
    public void buscarFactura( String distrito, String tipo_documento, String documento ) throws SQLException {

        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_FACTURA" );
            
            st.setString( 1, distrito );
            st.setString( 2, tipo_documento );
            st.setString( 3, documento );
            //System.out.println( "+++ SQL_FACTURA: " + st.toString() );            
            rs = st.executeQuery();
            
            this.lista = new Vector();
            
            while ( rs.next() ) {
                
                String codcli = rs.getString( "codcli" )!=null?rs.getString( "codcli" ).toUpperCase():""; 
                String nombre_cliente = rs.getString( "nombre_cliente" )!=null?rs.getString( "nombre_cliente" ).toUpperCase():""; 
                String agencia_facturacion = rs.getString( "agencia_facturacion" )!=null?rs.getString( "agencia_facturacion" ).toUpperCase():""; 
                String moneda_fact = rs.getString( "moneda_fact" )!=null?rs.getString( "moneda_fact" ).toUpperCase():""; 
                String nit = rs.getString( "nit" )!=null?rs.getString( "nit" ).toUpperCase():""; 
                
                String fecha_factura = rs.getString( "fecha_factura" )!=null?rs.getString( "fecha_factura" ):""; 
                String valor_tasa = rs.getString( "valor_tasa" )!=null?rs.getString( "valor_tasa" ):""; 
                String forma_pago = rs.getString( "forma_pago" )!=null?rs.getString( "forma_pago" ).toUpperCase():""; 
                String plazo = rs.getString( "plazo" )!=null?rs.getString( "plazo" ):""; 
                String valor_factura = rs.getString( "valor_factura" )!=null?rs.getString( "valor_factura" ):""; 
                
                String descripcion_fact = rs.getString( "descripcion_fact" )!=null?rs.getString( "descripcion_fact" ).toUpperCase():""; 
                String observacion = rs.getString( "observacion" )!=null?rs.getString( "observacion" ).toUpperCase():""; 
                String item = rs.getString( "item" )!=null?rs.getString( "item" ):""; 
                String numero_remesa = rs.getString( "numero_remesa" )!=null?rs.getString( "numero_remesa" ).toUpperCase():""; 
                String creation_date = rs.getString( "creation_date" )!=null?rs.getString( "creation_date" ):""; 
                
                String descripcion_item = rs.getString( "descripcion_item" )!=null?rs.getString( "descripcion_item" ).toUpperCase():""; 
                String cantidad = rs.getString( "cantidad" )!=null?rs.getString( "cantidad" ):""; 
                String valor_unitariome = rs.getString( "valor_unitariome" )!=null?rs.getString( "valor_unitariome" ):""; 
                String moneda_item = rs.getString( "moneda_item" )!=null?rs.getString( "moneda_item" ).toUpperCase():""; 
                String valor_itemme = rs.getString( "valor_itemme" )!=null?rs.getString( "valor_itemme" ):""; 
                
                String codigo_cuenta_contable = rs.getString( "codigo_cuenta_contable" )!=null?rs.getString( "codigo_cuenta_contable" ).toUpperCase():""; 
                String concepto = rs.getString( "concepto" )!=null?rs.getString( "concepto" ).toUpperCase():""; 
                String auxiliar = rs.getString( "auxiliar" )!=null?rs.getString( "auxiliar" ).toUpperCase():""; 
                
                BeanGeneral bean = new BeanGeneral ();
                
                //INFO CABECERA
                bean.setValor_01( codcli );
                bean.setValor_02( nombre_cliente );
                bean.setValor_03( agencia_facturacion );
                bean.setValor_04( moneda_fact );
                bean.setValor_05( nit );
                bean.setValor_06( fecha_factura.equals("0099-01-01")?"":fecha_factura );
                bean.setValor_07( valor_tasa );
                bean.setValor_08( forma_pago );
                bean.setValor_09( plazo );
                bean.setValor_10( valor_factura );                
                bean.setValor_11( descripcion_fact );
                bean.setValor_12( observacion );
                
                //INFO ITEMS
                bean.setValor_13( item );
                bean.setValor_14( numero_remesa );
                bean.setValor_15( creation_date.equals("0099-01-01 00:00:00")?"":creation_date );
                bean.setValor_16( descripcion_item );
                bean.setValor_17( cantidad );
                bean.setValor_18( valor_unitariome );
                bean.setValor_19( moneda_item );
                bean.setValor_20( valor_itemme );                
                bean.setValor_21( codigo_cuenta_contable );
                bean.setValor_22( concepto );
                bean.setValor_23( auxiliar );
                
                //INFO CAPTURADA
                bean.setValor_24( distrito );        // *Escogido por el usuario
                bean.setValor_25( tipo_documento );  // *Escogido por el usuario
                bean.setValor_26( documento );       // *Escogido por el usuario
                
                //INFO OCOMPRA
                bean.setValor_27(rs.getString("order_no")!=null?rs.getString("order_no"):"");
                bean.setValor_28(rs.getString("factura")!=null?rs.getString("factura"):"");
                
               
                               
                //INFO PARA REPORTE EN EXCEL
                bean.setValor_29(rs.getString("desp"));
                bean.setValor_30(rs.getString("ship_to_name"));
                bean.setValor_31(rs.getString("order_qty"));
                bean.setValor_32(rs.getString("Cub_Unit"));
                bean.setValor_33(rs.getString("total_volumen"));
                bean.setValor_34(rs.getString("addr1"));
                bean.setValor_35(rs.getString("ciudad"));
                bean.setValor_36(rs.getString("inst_especiales"));
                bean.setValor_37(rs.getString("factura"));
                this.lista.add( bean );
                
            }
                        
        } catch( SQLException e ) {
            
            e.printStackTrace();
            
        }
        
        finally {
            
            if ( st != null ) {
                
                try{
                    
                    st.close();
                    
                } catch( Exception e ) {
                    
                    e.printStackTrace();
                    throw new SQLException( "ERROR DURANTE 'buscarFactura()' - [ReporteFacturasClientesDAO].. " + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_FACTURA" );
            
        }
        
    }
   
      /**
     * Metodo: inventario, Genera el reporte de inventario
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public void inventario(String ocompra, String factura, String dstrct)throws SQLException{
        
        PreparedStatement st = null, st2=null;
        ResultSet rs = null, rs2=null;
        lista = new Vector();
        
        
        try {
            Connection con = this.conectar("SQL_INVENTARIO");
            if(con!=null){
                
                st = con.prepareStatement(this.obtenerSQL("SQL_INVENTARIO"));
                st.setString (1,dstrct);      
                st.setString(2, factura+"%");
                st.setString(3, ocompra+"%");
                  
                rs = st.executeQuery();
                //System.out.println("St "+st);
                
                while(rs.next()){
                    Instrucciones_Despacho inst = new Instrucciones_Despacho();
                    inst.setBodega(rs.getString("descripcion"));
                    inst.setCod_ubicacion(rs.getString("cod_ubicacion"));
                    inst.setModelo(rs.getString("material"));
                    inst.setDescripcionMaterial(rs.getString("desc_tmaterial"));
                    inst.setCantida_entrada(rs.getDouble("cantidad_entrada"));
                    inst.setCantida_salida(rs.getDouble("cantidad_salida"));
                    inst.setCantida_saldo(rs.getDouble("cantidad_saldo"));
                    inst.setUnidad(rs.getString("desc_unidad"));
                    lista.add(inst);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO EL REPORTE DE INVENTARIO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_INVENTARIO");
        }
        
    }   
    
      /**
     * Funcion publica que actualiza la informacion de una entrada planeada especifica.
     */
    public void buscarListaFactura( String fecha1, String fecha2 ) throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        remesas = new Vector();
        try {
            
            st = crearPreparedStatement( "SQL_LISTA_FACTURA" );
            
            st.setString( 1, fecha1 );
            st.setString( 2, fecha2 );
            
            //System.out.println( "+++ SQL_LISTA_FACTURA: " + st.toString() );
            rs = st.executeQuery();
            
            
            while ( rs.next() ) {
                
                Remesa rem = new Remesa();
                rem.setNumpla(rs.getString("numpla"));
                rem.setNumrem(rs.getString("numrem"));
                rem.setDocumento(rs.getString("order_no")!=null?rs.getString("order_no"):"");
                rem.setDocumento_rel(rs.getString("factura")!=null?rs.getString("factura"):"");
                rem.setFactura(rs.getString("documento"));
                remesas.add(rem);
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            
        }
        
        finally {
            
            if ( st != null ) {
                
                try{
                    
                    st.close();
                    
                } catch( Exception e ) {
                    
                    e.printStackTrace();
                    throw new SQLException( "ERROR DURANTE 'buscarFactura()' - [ReporteFacturasClientesDAO].. " + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_LISTA_FACTURA" );
            
        }
        
    }
    
     /**
     * Metodo: buscarRemesa, lista las remesas de una planilla
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public void buscarRemesa(String numrem)throws SQLException{
        
        PreparedStatement st = null, st2=null;
        ResultSet rs = null, rs2=null;
        remesas = new Vector();
        
        
        try {
            Connection con = this.conectar("SQL_BUSCAR_REMESAS");
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL("SQL_BUSCAR_REMESAS"));
                st.setString(1, numrem);
                rs = st.executeQuery();
                
                while(rs.next()){
                    Remesa rem = new Remesa();
                    rem.setNumpla(rs.getString("numpla")!=null?rs.getString("numpla"):"");
                    rem.setNumrem(rs.getString("numrem"));
                    rem.setPesoReal(rs.getFloat("pesoreal"));
                    rem.setDescripcion(rs.getString("descripcion"));
                    rem.setDocumento(rs.getString("order_no")!=null?rs.getString("order_no"):"");
                    rem.setDocumento_rel(rs.getString("factura")!=null?rs.getString("factura"):"");
                    rem.setFecha_factura(rs.getString("fecha_factura")!=null?rs.getString("fecha_factura"):"");
                    rem.setDistrito(rs.getString("cia"));
                    rem.setFecha_cargue("");
                    String fecha_cargue = "";
                    
                    try{
                        if(rs.getString("appoint_date")!=null){
                            if(rs.getString("appoint_date").length()==8 && rs.getString("appoint_time").length()==4){
                                int dias = Integer.parseInt(rs.getString("appoint_date"));
                                int horas = Integer.parseInt(rs.getString("appoint_time"));
                                
                                String fecha =rs.getString("appoint_date").substring(0,4)+"-"+rs.getString("appoint_date").substring(4,6)+ "-"+rs.getString("appoint_date").substring(6,8);
                                String time =rs.getString("appoint_time").substring(0,2)+":"+rs.getString("appoint_time").substring(2,4);
                                
                                fecha_cargue = fecha+" "+time;
                            }
                        }
                    }catch(NumberFormatException e){
                        fecha_cargue = "";
                    }
                    rem.setFecha_cargue(fecha_cargue);
                    rem.setFecha_realcargue(rs.getString("fecha_realcargue")!=null?rs.getString("fecha_realcargue"):"0099-01-01 00:00:00");
                    
                    st2 = con.prepareStatement(this.obtenerSQL("SQL_LISTA_DESTINATARIOS"));
                    st2.setString(1, rs.getString("numrem"));
                    rs2 = st2.executeQuery();
                    String destinatario = "";
                    String cod_destinatario = "";
                    if (rs2.next()){
                        destinatario =rs2.getString("nombre");
                        cod_destinatario = rs2.getString("codigo");
                        
                    }
                    rem.setNom_destinatario(destinatario);
                    rem.setDestinatario(cod_destinatario);
                    rem.setReferencias(buscarReferencias(rem));
                    remesas.add(rem);
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO UNA REMESA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_BUSCAR_REMESAS");
        }
        
    }
    /**
     * Metodo: buscarRemesa, lista las remesas de una planilla
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public void buscarOcompra(String ocompra, String cia)throws SQLException{
        
        PreparedStatement st = null, st2=null;
        ResultSet rs = null, rs2=null;
        remesas = new Vector();
        
        
        try {
            Connection con = this.conectar("SQL_BUSCAR_OCOMPRA");
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL("SQL_BUSCAR_OCOMPRA"));
                st.setString(1, ocompra);
                rs = st.executeQuery();
                
                while(rs.next()){
                    Remesa rem = new Remesa();
                    
                    rem.setDocumento(rs.getString("order_no")!=null?rs.getString("order_no"):"");
                    rem.setDocumento_rel(rs.getString("factura")!=null?rs.getString("factura"):"");
                    rem.setFecha_factura(rs.getString("fecha_factura")!=null?rs.getString("fecha_factura"):"");
                    rem.setDistrito(cia);
                    rem.setReferencias(buscarReferencias(rem));
                    //System.out.println("Tama�o referencias "+rem.getReferencias().size());
                    remesas.add(rem);
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO UNA OCOMPRA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_BUSCAR_OCOMPRA");
        }
        
    }
    
    /**
     * Metodo: inventario, Genera el reporte de inventario
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public void detalleInventario(String condicion, String dstrct, String ubicacion, String material)throws SQLException{
        
        PreparedStatement st = null, st2=null;
        ResultSet rs = null, rs2=null;
        lista = new Vector();
        
        
        try {
            Connection con = this.conectar("SQL_DETALLE_INVENTARIO");
            if(con!=null){
                String query = this.obtenerSQL("SQL_DETALLE_INVENTARIO");
                query = query.replace("#CONDICION#",condicion);
                //System.out.println("QUery "+query);
                
                st = con.prepareStatement(query);
                st.setString(1,dstrct);
                st.setString(2, ubicacion);
                st.setString(3, material);
                
                rs = st.executeQuery();
                //System.out.println("St "+st);
                
                while(rs.next()){
                    Instrucciones_Despacho inst = new Instrucciones_Despacho();
                    inst.setBodega(rs.getString("descripcion"));
                    inst.setModelo(rs.getString("material"));
                    inst.setDescripcionMaterial(rs.getString("desc_tmaterial"));
                    inst.setCantida_entrada(rs.getDouble("cifra1_predominante"));
                    inst.setUnidad(rs.getString("desc_unidad"));
                    inst.setDocumento(rs.getString("documento"));
                    inst.setFactura(rs.getString("nro_fact_comercial"));
                    inst.setOrder_no(rs.getString("ocompra"));
                    inst.setNumpla(rs.getString("referencia"));
                    inst.setTipo_doc(rs.getString("tipo_doc"));
                    lista.add(inst);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO EL REPORTE DE INVENTARIO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_DETALLE_INVENTARIO");
        }
        
    }
    
    /**
     * Metodo: reporteTrakin, Genera el reporte de trakin
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public void reporteTrakin(String ocompra, String factura, String destinatario, String origen, String destino, String fecha1, String fecha2)throws SQLException{
        
        PreparedStatement st = null, st2=null;
        ResultSet rs = null, rs2=null;
        lista = new Vector();
        
        
        try {
            Connection con = this.conectar("SQL_REPORTE_TRAKIN");
            if(con!=null){
                String condicion="";
                if(!origen.equals("")){
                    condicion = " and orirem = '" +origen+"'";
                }
                if(!destino.equals("")){
                    condicion =condicion+ " and desrem = '" +destino+"'";
                }
                String query = this.obtenerSQL("SQL_REPORTE_TRAKIN");
                query = query.replace("#ORIDEST#",condicion);
                System.out.println("QUery "+query);
                st = con.prepareStatement(query);
                st.setString(1, fecha1);
                st.setString(2, fecha2);
                st.setString(3, ocompra+"%");
                st.setString(4, factura+"%");
                st.setString(5, destinatario+"%");
                
                rs = st.executeQuery();
                System.out.println("St "+st);
                while(rs.next()){
                    Instrucciones_Despacho inst = new Instrucciones_Despacho();
                    inst.setDesp(rs.getInt("desp"));
                    inst.setExpire_date(rs.getString("expire_date"));
                    inst.setOrder_no(rs.getString("order_no"));
                    inst.setFactura(rs.getString("factura"));
                    inst.setShip_to_name(rs.getString("ship_to_name"));
                    inst.setModelo(rs.getString("modelo"));
                    inst.setOrder_qty(rs.getInt("order_qty"));
                    inst.setCub_unit(rs.getDouble("cub_unit"));
                    inst.setTotal_volumen(rs.getDouble("total_volumen"));
                    inst.setFecha_factura(rs.getString("fecha_factura").equals("0099-01-01")?"":rs.getString("fecha_factura"));
                    inst.setAddr1(rs.getString("addr1"));
                    inst.setCiudad(rs.getString("ciudad"));
                    inst.setBodega(rs.getString("bodega"));
                    inst.setObservacion(rs.getString("causaCargue"));
                    inst.setInst_especiales(rs.getString("inst_especiales"));
                    inst.setItem(rs.getString("item"));
                    inst.setLoc(rs.getString("loc"));
                    inst.setAppoint_date(rs.getString("appoint_date"));
                    inst.setAppoint_time(rs.getString("appoint_time"));
                    inst.setNumrem(rs.getString("numrem")!=null?rs.getString("numrem"):"");
                    inst.setFecha_cargue(rs.getString("fecha_realcargue").equals("0099-01-01 00:00:00")?"":rs.getString("fecha_realcargue"));
                    
                    inst.setCant_despachada(rs.getInt("cant_despachada"));
                    inst.setCantida_entregada(inst.getCant_despachada());
                    //se inicializan estas variables
                    inst.setNumpla("");
                    inst.setFechaDesp("");
                    inst.setNumplaRex("");
                    inst.setFechaDespRex("");
                    inst.setPlaca("");
                    inst.setConductor("");
                    inst.setCantida_entregada(0);
                    inst.setObservacion_novedad("");
                    inst.setCodigo_novedad("");
                    inst.setMotivo_novedad("");
                    inst.setCantida_novedad(0);
                    inst.setFecha_novedad("");
                    inst.setFecha_devolucion("");
                    inst.setObservacion_devolucion("");
                    
                    if(!inst.getNumrem().equals("")){
                        Vector planillas=new Vector();
                        try{
                            
                            st2 = con.prepareStatement(this.obtenerSQL("SQL_REPORTE_TRAKIN_DESPACHO"));
                            st2.setString(1,inst.getNumrem());
                            rs2 = st2.executeQuery();
                            //    System.out.println("Segundo query "+st2);
                            while(rs2.next()){
                                planillas.add(rs2.getString("numpla"));
                                
                            }
                        }catch(SQLException e){
                            throw new SQLException("ERROR BUSCANDO EL REPORTE DE SQL_REPORTE_TRAKIN_DESPACHO" + e.getMessage() + " " + e.getErrorCode());
                        }
                        //  System.out.println("Termine el segundo query");
                        if(planillas!=null){
                            //      System.out.println("Planillas no es null");
                            if(planillas.size()>0){
                                String numpla =(String) planillas.elementAt(0);
                                
                                try{
                                    
                                    st2 = con.prepareStatement(this.obtenerSQL("SQL_REPORTE_DATOS_DESPACHO"));
                                    st2.setString(1,numpla);
                                    st2.setString(2,numpla);
                                    rs2 = st2.executeQuery();
                                    
                                    
                                    //     System.out.println("tercer query "+st2);
                                    if(rs2.next()){
                                        inst.setNumpla(rs2.getString("numpla"));
                                        inst.setFechaDesp(rs2.getString("fecdsp"));
                                        inst.setPlaca(rs2.getString("plaveh"));
                                        inst.setConductor(rs2.getString("cedcon")+" "+rs2.getString("nombre"));
                                    }
                                }catch(SQLException e){
                                    throw new SQLException("ERROR BUSCANDO EL REPORTE DE SQL_REPORTE_DATOS_DESPACHO " + e.getMessage() + " " + e.getErrorCode());
                                }
                                
                                if(planillas.size()>0){
                                    numpla = (String) planillas.elementAt(planillas.size()-1);
                                    try{
                                        st2 = con.prepareStatement(this.obtenerSQL("SQL_REPORTE_DATOS_DESPACHO"));
                                        st2.setString(1,numpla);
                                        st2.setString(2,numpla);
                                        rs2 = st2.executeQuery();
                                        
                                        if(rs2.next()){
                                            inst.setFechaDespRex(rs2.getString("fecdsp"));
                                            inst.setFechaEntrega(rs2.getString("fechareporte")!=null?rs2.getString("fechareporte"):"");
                                        }
                                    }catch(SQLException e){
                                        throw new SQLException("ERROR BUSCANDO EL REPORTE DE SQL_REPORTE_DATOS_DESPACHO 2" + e.getMessage() + " " + e.getErrorCode());
                                    }
                                    
                                    try{
                                        for(int k =0; k< planillas.size();k++){
                                            
                                            numpla =(String) planillas.elementAt(k);
                                            
                                            st2 = con.prepareStatement(this.obtenerSQL("SQL_REPORTE_DATOS_DISCREPANCIA"));
                                            st2.setString(1,numpla);
                                            st2.setString(2,inst.getOrder_no());
                                            st2.setString(3,inst.getModelo());
                                            rs2 = st2.executeQuery();
                                            
                                            if(rs2.next()){
                                                inst.setCantida_entregada(inst.getCant_despachada()-rs2.getDouble("cantidad"));
                                                inst.setObservacion_novedad(rs2.getString("observacion"));
                                                inst.setCodigo_novedad(rs2.getString("num_discre"));
                                                inst.setMotivo_novedad(rs2.getString("causa_dev"));
                                                inst.setCantida_novedad(rs2.getDouble("cantidad"));
                                                inst.setFecha_novedad(rs2.getString("fecha"));
                                                
                                            }
                                        }
                                    }catch(SQLException e){
                                        throw new SQLException("ERROR BUSCANDO EL REPORTE DE SQL_REPORTE_DATOS_DISCREPANCIA" + e.getMessage() + " " + e.getErrorCode());
                                    }
                                    
                                    //REMESA DE DEVOLUCION...
                                    try{
                                        System.out.println("Numrem "+inst.getNumrem());
                                        st2 = con.prepareStatement(this.obtenerSQL("SQL_REMESA_DEVOL"));
                                        st2.setString(1,inst.getNumrem());
                                        st2.setString(2,inst.getNumrem());
                                        rs2 = st2.executeQuery();
                                        System.out.println("DEVOL " +st2);
                                        if(rs2.next()){
                                            inst.setFecha_devolucion(rs2.getString("fechareporte")!=null?rs2.getString("fechareporte"):"");
                                            inst.setObservacion_devolucion(rs2.getString("observacion"));
                                            
                                        }
                                    }catch(SQLException e){
                                        throw new SQLException("ERROR BUSCANDO EL REPORTE DE SQL_REMESA_DEVOL" + e.getMessage() + " " + e.getErrorCode());
                                    }
                                }
                            }
                        }
                    }
                    lista.add(inst);
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO EL REPORTE DE TRAKIN " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_REPORTE_TRAKIN");
        }
        
    }
    
     /**
     * Metodo: actualizarOCompra,actualiza en la tabla la orden de compra
     *con el numero de la remesa, el de la factura y la fecha de la factura.
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public String liberarOCompra(String numrem)throws SQLException{
        
        PreparedStatement st = null, st2=null;
        String sql="";
        try {
            Connection con = this.conectar("SQL_LIBERAROCOMPRA");
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL("SQL_LIBERAROCOMPRA"));
                st.setString(1,numrem);
                sql = st.toString();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR LIBERANDO EL NUMERO DE REMESA EN LA ORDEN DE CARGUE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            
            this.desconectar("SQL_LIBERAROCOMPRA");
        }
        return sql;
    }
    
}
