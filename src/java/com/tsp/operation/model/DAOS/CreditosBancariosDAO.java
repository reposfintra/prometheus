package com.tsp.operation.model.DAOS;

import com.tsp.finanzas.contab.model.beans.Cuentas;
import com.tsp.operation.model.beans.Cmc;
import com.tsp.operation.model.beans.CreditoBancario;
import com.tsp.operation.model.beans.CreditoBancarioDetalle;
import com.tsp.operation.model.beans.CupoBanco;
import com.tsp.operation.model.beans.CupoCreditoBancario;
import com.tsp.operation.model.beans.LineaCredito;
import com.tsp.operation.model.beans.PuntosBasicosBanco;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * DAO para el programa de manejo de creditos bancarios
 * @author darrieta - geotech
 */
public class CreditosBancariosDAO extends MainDAO {

    public CreditosBancariosDAO(){
        super("CreditosBancariosDAO.xml");
    }
    public CreditosBancariosDAO(String dataBaseName){
        super("CreditosBancariosDAO.xml", dataBaseName);
    }

    /**
     * Obtiene el dtf de la semana
     * @return double con el dtf de la semana o 0 si no existe
     * @throws Exception
     */
    public double obtenerDTFActual() throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "DTF_ACTUAL";
        double dtf = 0;
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            rs = st.executeQuery();
            if (rs.next()) {
                dtf = rs.getDouble("dtf");
            }
        } catch (Exception e) {
            throw new Exception("Error en obtenerDTFActual[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return dtf;
    }

    /**
     * Obtiene el dtf de la semana
     * @return double con el dtf de la semana o 0 si no existe
     * @throws Exception
     */
    public String crearCreditoBancario(CreditoBancario credito) throws Exception{
        StringStatement st = null;
        String query = "INSERTAR_CREDITO_BANCARIO";
        String sql;

        try{
            String tasaCobrada = "null";
            String intereses = "null";
            if(credito.getTasa_cobrada()!=0){
                tasaCobrada = String.valueOf(credito.getTasa_cobrada());
            }
            if(credito.getVlr_intereses()!=0){
                intereses = String.valueOf(credito.getVlr_intereses());
            }

            st = new StringStatement(this.obtenerSQL(query).replaceAll("#TASA_COBRADA#", tasaCobrada).replaceAll("#VLR_INTERESES#", intereses), true);
            int param = 1;
            st.setString(param++, credito.getNit_banco());
            st.setString(param++, credito.getDocumento());
            st.setString(param++, credito.getDstrct());
            st.setString(param++, credito.getRef_credito());
            st.setString(param++, credito.getLinea_credito());
            st.setDouble(param++, credito.getDtf());
            st.setDouble(param++, credito.getPuntos_basicos());
            st.setInt(param++, credito.getPeriodicidad());
            st.setDouble(param++, credito.getVlr_credito());
            st.setString(param++, credito.getFecha_inicial());
            st.setString(param++, credito.getFecha_vencimiento());
            st.setString(param++, credito.getCupo());
            st.setString(param++, credito.getCreation_user());
            st.setString(param++, credito.getTipo_dtf());
            st.setString(param++, credito.getHc());
            st.setString(param++, credito.getCuenta());
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en crearCreditoBancario() -->> [CreditosBancariosDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }


    /**
     * Obtiene los puntos basicos para un banco y linea de credito
     * @throws Exception
     */
    public double obtenerPuntosBasicos(String banco, String linea) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_PUNTOS_BASICOS";
        double puntos_basicos = 0;
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, banco);
            st.setString(2, linea);
            rs = st.executeQuery();
            if (rs.next()) {
                puntos_basicos = rs.getDouble("puntos_basicos");
            }
        } catch (Exception e) {
            throw new Exception("Error en obtenerPuntosBasicos[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return puntos_basicos;
    }

    /**
     * Verifica si existen puntos basicos para un banco y linea de credito
     * @throws Exception
     */
    public boolean existePuntosBasicos(String banco, String linea) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_PUNTOS_BASICOS";
        boolean existe = false;
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, banco);
            st.setString(2, linea);
            rs = st.executeQuery();
            if (rs.next()) {
                existe = true;
            }
        } catch (Exception e) {
            throw new Exception("Error en existePuntosBasicos[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return existe;
    }

    /**
     * Obtiene el listado de puntos basicos
     * @throws Exception
     */
    public ArrayList<PuntosBasicosBanco> listarPuntosBasicos() throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_TODOS_PUNTOS_BASICOS";
        ArrayList<PuntosBasicosBanco> lista = new ArrayList<PuntosBasicosBanco>();
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            rs = st.executeQuery();
            while (rs.next()) {
                PuntosBasicosBanco pb = new PuntosBasicosBanco();
                pb.setBanco(rs.getString("cod_banco"));
                pb.setNombreBanco(rs.getString("nom_banco"));
                pb.setLineaCredito(rs.getString("cod_linea"));
                pb.setDescripcionLinea(rs.getString("nom_linea"));
                pb.setPuntosBasicos(rs.getDouble("puntos_basicos"));
                lista.add(pb);
            }
        } catch (Exception e) {
            throw new Exception("Error en listarPuntosBasicos[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return lista;
    }

    /**
     * Inserta un registro en la tabla de puntos basicos
     * @param ptoBasico bean con los datos a ingresar
     * @param usuario usuario en sesion
     * @throws Exception
     */
    public void insertarPuntosBasicos(PuntosBasicosBanco ptoBasico, Usuario usuario) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        String query = "INSERTAR_PUNTOS_BASICOS";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, ptoBasico.getBanco());
            st.setString(2, ptoBasico.getLineaCredito());
            st.setDouble(3, ptoBasico.getPuntosBasicos());
            st.setString(4, usuario.getDstrct());
            st.setString(5, usuario.getLogin());

            st.executeUpdate();

        } catch (Exception e) {
            throw new Exception("Error en insertarPuntosBasicos[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
    }

    /**
     * Edita un registro en la tabla de puntos basicos
     * @param ptoBasico bean con los datos a ingresar
     * @param usuario usuario en sesion
     * @throws Exception
     */
    public void editarPuntosBasicos(PuntosBasicosBanco ptoBasico, Usuario usuario) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        String query = "EDITAR_PUNTOS_BASICOS";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setDouble(1, ptoBasico.getPuntosBasicos());
            st.setString(2, usuario.getLogin());
            st.setString(3, ptoBasico.getBanco());
            st.setString(4, ptoBasico.getLineaCredito());
            st.setString(5, usuario.getDstrct());

            st.executeUpdate();

        } catch (Exception e) {
            throw new Exception("Error en editarPuntosBasicos[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
    }


    /**
     * Obtiene el listado de puntos basicos
     * @throws Exception
     */
    public ArrayList<CupoBanco> listarCupos() throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_TODOS_CUPOS";
        ArrayList<CupoBanco> lista = new ArrayList<CupoBanco>();
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            rs = st.executeQuery();
            while (rs.next()) {
                CupoBanco pb = new CupoBanco();
                pb.setBanco(rs.getString("cod_banco"));
                pb.setNombreBanco(rs.getString("nom_banco"));
                pb.setLineaCupo(rs.getString("cod_linea"));
                pb.setDescripcionLinea(rs.getString("nom_linea"));
                pb.setCupo(rs.getDouble("cupo"));
                pb.setSaldo(rs.getDouble("saldo"));
                lista.add(pb);
            }
        } catch (Exception e) {
            throw new Exception("Error en listarCupos[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return lista;
    }
    
    
    /**
     * Verifica si existe el registro de cupo para un banco y linea
     * @throws Exception
     */
    public boolean existeCupo(String banco, String linea) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_CUPO";
        boolean existe = false;
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, banco);
            st.setString(2, linea);
            rs = st.executeQuery();
            if (rs.next()) {
                existe = true;
            }
        } catch (Exception e) {
            throw new Exception("Error en existeCupo[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return existe;
    }


    /**
     * Inserta un registro en la tabla de puntos basicos
     * @param cupo bean con los datos a ingresar
     * @param usuario usuario en sesion
     * @throws Exception
     */
    public void insertarCupo(CupoBanco cupo, Usuario usuario) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        String query = "INSERTAR_CUPO";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, cupo.getBanco());
            st.setString(2, cupo.getLineaCupo());
            st.setDouble(3, cupo.getCupo());
            st.setString(4, usuario.getDstrct());
            st.setString(5, usuario.getLogin());

            st.executeUpdate();

        } catch (Exception e) {
            throw new Exception("Error en insertarCupo[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
    }

    /**
     * Edita un registro en la tabla de puntos basicos
     * @param cupo bean con los datos a ingresar
     * @param usuario usuario en sesion
     * @throws Exception
     */
    public void editarCupo(CupoBanco cupo, Usuario usuario) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        String query = "EDITAR_CUPO";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setDouble(1, cupo.getCupo());
            st.setString(2, usuario.getLogin());
            st.setString(3, cupo.getBanco());
            st.setString(4, cupo.getLineaCupo());
            st.setString(5, usuario.getDstrct());

            st.executeUpdate();

        } catch (Exception e) {
            throw new Exception("Error en editarCupo[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
    }

    /**
     * Consulta los datos para realizar la causacion de los intereses
     * @throws Exception
     */
    public ArrayList<CreditoBancario> consultarDatosCausacion() throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_DATOS_CAUSACION";
        ArrayList<CreditoBancario> creditos = new ArrayList<CreditoBancario>();

        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            rs = st.executeQuery();
            while (rs.next()) {
                CreditoBancario credito = new CreditoBancario();
                credito.setDetalles(new ArrayList<CreditoBancarioDetalle>());
                CreditoBancarioDetalle detalle = new CreditoBancarioDetalle();
                credito.setRef_credito(rs.getString("ref_credito"));
                credito.setTipo_dtf(rs.getString("tipo_dtf"));
                credito.setNit_banco(rs.getString("nit_banco"));
                credito.setNombre_banco(rs.getString("nombre_banco"));
                credito.setDocumento(rs.getString("documento"));
                credito.setPuntos_basicos(rs.getDouble("puntos_basicos"));
                credito.setPeriodicidad(rs.getInt("periodicidad"));
                credito.setTasa_cobrada(rs.getDouble("tasa_cobrada"));
                detalle.setDtf(rs.getDouble("dtf"));
                detalle.setFecha_inicial(rs.getString("fecha_inicial"));
                detalle.setFecha_final(rs.getString("fecha_final"));
                detalle.setCapital_inicial(rs.getDouble("capital"));
                detalle.setInteres_acumulado(rs.getDouble("interes_acumulado"));
                detalle.setPago_intereses(rs.getDouble("pago_intereses"));
                detalle.setSaldo_inicial(rs.getDouble("saldo_inicial"));
                credito.setSaldo_cxp(rs.getDouble("saldo_cxp"));
                credito.setBaseAno(rs.getInt("base_ano"));
                creditos.add(credito);
                credito.getDetalles().add(detalle);
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarDatosCausacion[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return creditos;
    }

    /**
     * Obtiene el dtf de la semana
     * @return double con el dtf de la semana o 0 si no existe
     * @throws Exception
     */
    public String insertarDetalleCredito(CreditoBancarioDetalle credito) throws Exception{
        StringStatement st = null;
        String query = "INSERTAR_DETALLE_CREDITO";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            int param = 1;
            st.setString(param++, credito.getDstrct());
            st.setString(param++, credito.getNit_banco());
            st.setString(param++, credito.getDocumento());
            st.setString(param++, credito.getDstrct());
            st.setString(param++, credito.getNit_banco());
            st.setString(param++, credito.getDocumento());
            st.setString(param++, credito.getFecha_inicial());
            st.setString(param++, credito.getFecha_final());
            st.setDouble(param++, credito.getDtf());
            st.setDouble(param++, credito.getSaldo_inicial());
            st.setDouble(param++, credito.getCapital_inicial());
            st.setDouble(param++, credito.getIntereses());
            st.setDouble(param++, credito.getAjuste_intereses());
            st.setDouble(param++, credito.getPago_capital());
            st.setDouble(param++, credito.getPago_intereses());
            st.setString(param++, credito.getCreation_user());
            st.setDouble(param++, credito.getInteres_acumulado());
            st.setString(param++, credito.getDoc_intereses());
            st.setString(param++, credito.getDoc_pago());
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en insertarCausacion() -->> [CreditosBancariosDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }


    /**
     * Obtiene los creditos agrupados por banco
     * @param tipo tipo de credito N - nacional o E - extranjero
     * @param banco nit del banco
     * @param vigente false para consultar registro con saldo cero o true para saldo > 0
     * @param finicial fecha inicial del rango para buscar la fecha de inicio del credito
     * @param ffinal fecha final del rango para buscar la fecha de inicio del credito
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<CreditoBancario> consultarSaldosBancos(String tipo, String banco, boolean vigente, String finicial, String ffinal) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "FILTRO_CREDITOS";
        ArrayList<CreditoBancario> creditos = new ArrayList<CreditoBancario>();

        try {

            String filtro = "";
            if(tipo!=null && tipo.equals("N")){
                filtro += " and n.codpais = 'CO'";
            }
            if(tipo!=null && tipo.equals("E")){
                filtro += " and n.codpais != 'CO'";
            }
            if(banco!=null && !banco.equals("")){
                filtro += " and cb.nit_banco = '"+ banco +"'";
            }
            if(vigente){
                filtro += " and cd.vlr_saldo > 0";
            }else{
                filtro += " and cd.vlr_saldo = 0";
                filtro += " and cb.fecha_inicial between '"+finicial+"' and '"+ffinal+"'";
            }

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#FILTRO#", filtro));
            rs = st.executeQuery();
            while (rs.next()) {
                CreditoBancario credito = new CreditoBancario();
                credito.setNit_banco(rs.getString("nit_banco"));
                credito.setNombre_banco(rs.getString("nombre_banco"));
                credito.setVlr_credito(rs.getDouble("saldo"));
                creditos.add(credito);
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarCreditos[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return creditos;
    }

    /**
     * Obtiene los creditos de un banco
     * @param nitBanco nit del banco a buscar
     * @param vigente false para consultar registro con saldo cero o true para saldo > 0
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<CreditoBancario> consultarCreditosBanco(String nitBanco, boolean vigente) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CREDITOS_BANCO";
        ArrayList<CreditoBancario> creditos = new ArrayList<CreditoBancario>();

        try {
            String vigencia = "cd.vlr_saldo > 0";
            if(!vigente){
                vigencia = "cd.vlr_saldo = 0";
            }

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#VIGENCIA#", vigencia));
            st.setString(1, nitBanco);
            rs = st.executeQuery();
            while (rs.next()) {
                CreditoBancario credito = new CreditoBancario();
                credito.setDocumento(rs.getString("documento"));
                credito.setVlr_credito(rs.getDouble("saldo"));
                credito.setLinea_credito(rs.getString("linea_credito"));
                credito.setCupo(rs.getString("cupo"));
                creditos.add(credito);
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarCreditosBanco[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return creditos;
    }

    /**
     * Obtiene los datos de un credito bancario
     * @param dstrct dstrito del usuario en sesion
     * @param nitBanco nit del banco que realizo el credito
     * @param documento numero del credito
     * @return bean CreditoBancario con los datos del credito consultado o null si no existe
     * @throws Exception
     */
    public CreditoBancario consultarCreditoBancario(String dstrct, String nitBanco, String documento) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTA_CREDITO_BANCARIO";
        CreditoBancario credito = null;

        try {

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, dstrct);
            st.setString(2, nitBanco);
            st.setString(3, documento);
            rs = st.executeQuery();
            if (rs.next()) {
                credito = new CreditoBancario();
                credito = credito.Load(rs);
                credito.setNombre_banco(rs.getString("nombre_banco"));
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarCreditoBancario[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return credito;
    }

    /**
     * Obtiene los movimientos de un credito bancario
     * @param dstrct dstrito del usuario en sesion
     * @param nitBanco nit del banco que realizo el credito
     * @param documento numero del credito
     * @return ArrayList con los datos del credito consultado
     * @throws Exception
     */
    public ArrayList<CreditoBancarioDetalle> consultarDetallesCreditoBancario(String dstrct, String nitBanco, String documento) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTA_DETALLES_CREDITO_BANCARIO";
        ArrayList<CreditoBancarioDetalle> detalles = new ArrayList<CreditoBancarioDetalle>();

        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, dstrct);
            st.setString(2, nitBanco);
            st.setString(3, documento);
            rs = st.executeQuery();
            while (rs.next()) {
                CreditoBancarioDetalle credito = CreditoBancarioDetalle.Load(rs);
                detalles.add(credito);
            }
        } catch (Exception e) {
            throw new Exception("Error en consultarDetallesCreditoBancario[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return detalles;
    }

    /**
     * Obtiene las ND realizadas al credito por fuera del programa
     * @param dstrct dstrito del usuario en sesion
     * @param nitBanco nit del banco que realizo el credito
     * @param documento numero del credito
     * @return ArrayList con los movimientos datos del credito consultado
     * @throws Exception
     */
    public ArrayList<CreditoBancario> consultarNDExternas(String dstrct, String nitBanco, String documento) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTA_ND_EXTERNAS";
        ArrayList<CreditoBancario> detalles = new ArrayList<CreditoBancario>();

        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, nitBanco);
            st.setString(2, documento);
            st.setString(3, dstrct);
            st.setString(4, nitBanco);
            st.setString(5, documento);
            rs = st.executeQuery();
            while (rs.next()) {
                CreditoBancario credito = new CreditoBancario();
                credito.setFecha_inicial(rs.getString("fecha_documento"));
                credito.setDocumento(rs.getString("documento"));
                credito.setVlr_credito(rs.getDouble("vlr_neto"));
                detalles.add(credito);
            }
        } catch (Exception e) {
            throw new Exception("Error en consultarNDExternas[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return detalles;
    }

    /**
     * Obtiene los comprobantes realizados al credito por fuera del programa
     * @param dstrct dstrito del usuario en sesion
     * @param nitBanco nit del banco que realizo el credito
     * @param documento numero del credito
     * @return ArrayList con los movimientos datos del credito consultado
     * @throws Exception
     */
    public ArrayList<CreditoBancario> consultarComprobantesExternos(String dstrct, String nitBanco, String documento) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTA_COMPROBANTES_EXTERNOS";
        ArrayList<CreditoBancario> detalles = new ArrayList<CreditoBancario>();

        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, documento);
            st.setString(2, nitBanco);
            st.setString(3, dstrct);
            st.setString(4, nitBanco);
            st.setString(5, documento);
            rs = st.executeQuery();
            while (rs.next()) {
                CreditoBancario credito = new CreditoBancario();
                credito.setFecha_inicial(rs.getString("fechadoc"));
                credito.setDocumento(rs.getString("numdoc"));
                credito.setVlr_credito(rs.getDouble("total_debito"));
                detalles.add(credito);
            }
        } catch (Exception e) {
            throw new Exception("Error en consultarComprobantesExternos[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return detalles;
    }

    /**
     * Obtiene los egresos realizados al credito por fuera del programa
     * @param dstrct dstrito del usuario en sesion
     * @param nitBanco nit del banco que realizo el credito
     * @param documento numero del credito
     * @return ArrayList con los movimientos datos del credito consultado
     * @throws Exception
     */
    public ArrayList<CreditoBancario> consultarEgresosExternos(String dstrct, String nitBanco, String documento) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTA_EGRESOS_EXTERNOS";
        ArrayList<CreditoBancario> detalles = new ArrayList<CreditoBancario>();

        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, nitBanco);
            st.setString(2, documento);
            st.setString(3, dstrct);
            st.setString(4, nitBanco);
            st.setString(5, documento);
            rs = st.executeQuery();
            while (rs.next()) {
                CreditoBancario credito = new CreditoBancario();
                credito.setFecha_inicial(rs.getString("fecha_cheque"));
                credito.setDocumento(rs.getString("document_no"));
                credito.setVlr_credito(rs.getDouble("vlr"));
                detalles.add(credito);
            }
        } catch (Exception e) {
            throw new Exception("Error en consultarEgresosExternos[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return detalles;
    }
    
    /**
     * Obtiene el listado de las l�neas de Cr�ditos vigentes
     * @throws Exception
     */
    public ArrayList<LineaCredito> listarLineasCredito() throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_LINEAS_CREDITO";
        ArrayList<LineaCredito> lista = new ArrayList<LineaCredito>();
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            rs = st.executeQuery();
            while (rs.next()) {
                LineaCredito lc = new LineaCredito();
                lc.setId(rs.getInt("id"));
                lc.setLinea(rs.getString("linea"));
                lc.setHc(rs.getString("hc"));
                lista.add(lc);
            }
        } catch (Exception e) {
            throw new Exception("Error en listarCupos[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return lista;
    }



    public boolean existeLineaCredito(String linea) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_LINEA_CREDITO";
        boolean existe = false;
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, linea);
            rs = st.executeQuery();
            if (rs.next()) {
                existe = true;
            }
        } catch (Exception e) {
            throw new Exception("Error en existeLineaCredito[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return existe;
    }

    public void insertarLineaCredito(LineaCredito linea, Usuario usuario) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        String query = "INSERTAR_LINEA_CREDITO";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, linea.getLinea());
            st.setString(2, linea.getHc());
            st.setString(3, usuario.getDstrct());
            st.setString(4, usuario.getLogin());

            st.executeUpdate();

        } catch (Exception e) {
            throw new Exception("Error en insertarLineaCredito[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
    }

    public void editarLineaCredito(LineaCredito linea, Usuario usuario) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        String query = "EDITAR_LINEA_CREDITO";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, linea.getLinea());
            st.setString(2, linea.getHc());
            st.setString(3, usuario.getLogin());
            st.setInt(4, linea.getId());
            
            st.executeUpdate();

        } catch (Exception e) {
            throw new Exception("Error en insertarLineaCredito[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
    }

    public ArrayList<CupoCreditoBancario> listarCuposCreditos() throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_CUPOS_CREDITO_BANCARIO";
        ArrayList<CupoCreditoBancario> lista = new ArrayList<CupoCreditoBancario>();
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            rs = st.executeQuery();
            while (rs.next()) {
                CupoCreditoBancario cb = new CupoCreditoBancario();
                cb.setId(rs.getInt("id"));
                cb.setNombre(rs.getString("nombre"));
                cb.setCuenta(rs.getString("cuenta"));
                lista.add(cb);
            }
        } catch (Exception e) {
            throw new Exception("Error en listarCuposCreditos[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return lista;
    }

    public boolean existeCupoCredito(String nombre) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_CUPO_CREDITO";
        boolean existe = false;
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, nombre);
            rs = st.executeQuery();
            if (rs.next()) {
                existe = true;
            }
        } catch (Exception e) {
            throw new Exception("Error en existeCupoCredito[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return existe;
    }

    public void insertarCupoCredito(CupoCreditoBancario cupo, Usuario usuario) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        String query = "INSERTAR_CUPO_CREDITO";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, cupo.getNombre());
            st.setString(2, cupo.getCuenta());
            st.setString(3, usuario.getDstrct());
            st.setString(4, usuario.getLogin());

            st.executeUpdate();

        } catch (Exception e) {
            throw new Exception("Error en insertarCupoCredito[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
    }

    public void editarCupoCredito(CupoCreditoBancario cupo, Usuario usuario) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        String query = "EDITAR_CUPO_CREDITO";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, cupo.getNombre());
            st.setString(2, cupo.getCuenta());
            st.setString(3, usuario.getLogin());
            st.setInt(4, cupo.getId());
            
            st.executeUpdate();

        } catch (Exception e) {
            throw new Exception("Error en editarCupoCredito[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
    }

    public ArrayList<Cmc> listarCmc() throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "LISTAR_CMC";
        ArrayList<Cmc> lista = new ArrayList<Cmc>();
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            rs = st.executeQuery();
            while (rs.next()) {
                Cmc cmc = new Cmc();
                cmc.setCmc(rs.getString("cmc"));
                cmc.setCuenta(rs.getString("cuenta"));
                lista.add(cmc);
            }
        } catch (Exception e) {
            throw new Exception("Error en listarCuposCreditos[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return lista;
    }

    public String obtenerDTFCuota(String fecha) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "OBTENER_DTF_CUOTA";
        String dtf = "";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, fecha);
             st.setString(2, fecha);
            rs = st.executeQuery();
            while (rs.next()) {
                dtf = rs.getString("descripcion");
                
            }
        } catch (Exception e) {
            throw new Exception("Error en obtenerDTFCuota[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return dtf;
    }

    public ArrayList<Cuentas> listarCuentas(String cuenta) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "LISTAR_CUENTAS";
        ArrayList<Cuentas> lista = new ArrayList<Cuentas>();
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, cuenta+"%");
            rs = st.executeQuery();
            while (rs.next()) {
                Cuentas cuentas = new Cuentas();
                cuentas.setCuenta(rs.getString("cuenta"));
                cuentas.setNombre_corto(rs.getString("nombre_corto"));
                lista.add(cuentas);
            }
        } catch (Exception e) {
            throw new Exception("Error en listarCuposCreditos[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return lista;
    }

    public ArrayList<Cuentas> obtenerCuentas() throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "OBTENER_CUENTAS";
        ArrayList<Cuentas> lista = new ArrayList<Cuentas>();
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            rs = st.executeQuery();
            while (rs.next()) {
                Cuentas cuentas = new Cuentas();
                cuentas.setCuenta(rs.getString("cuenta"));
                cuentas.setNombre_corto(rs.getString("nombre_corto"));
                lista.add(cuentas);
            }
        } catch (Exception e) {
            throw new Exception("Error en listarCuposCreditos[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return lista;
    }
}