/*
 * MigracionRemesasAnuladasDAO.java
 *
 * Created on 21 de julio de 2005, 01:25 PM
 */

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import java.net.URL;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.beans.*;
//import com.tsp.propiedades.Propiedades;

/**
 *
 * @author  Henry
 */
public class MigracionRemesasModificacionOTDAO extends MainDAO {
    
    private static String archivo_sincronizado="";
    private static String archivo_recibido="";
    private static Properties dbProps = new Properties();
    //dbastidas 20-01-2006
    private Vector remesasnf;
    private Vector remesasOTs;
    //private Propiedades propiedades = new Propiedades();;
    
    
    
    /** Creates a new instance of MigracionRemesasAnuladasDAO */
    public MigracionRemesasModificacionOTDAO() {
        super("MigracionRemesasModificacionOTDAO.xml");
    }
    
    /*public void cargarPropiedades() throws IOException{
        InputStream is =  propiedades.getClass().getResourceAsStream("general.properties");
        dbProps.load(is);
    }*/
    public String getUltimaFechaProceso(){
        String fecha="";
        fecha = dbProps.getProperty("ultima_fecha_proceso");
        return fecha;
    }
    public void setUltimafechaCreacion(String fecha) throws Exception{
        try{
            //SE ACTUALIZAN LOS DATOS EN EL ARCHIVO DE CONFIGURACION..
            dbProps.setProperty("ultima_fecha_proceso", fecha);
            URL u = null;//propiedades.getClass().getResource("general.properties");
            dbProps.store(new FileOutputStream(u.getPath()),"");
            ////System.out.println("Listo escribi en el archivo...");
            
        }catch(IOException e){
            throw new IOException("ERROR ESCRIBIENDO EN EL ARCHIVO DE PROPIEDADES LA FECHA DE CORTE "+e.getMessage());
        }
    }
    public Vector getDocInternos(String numrem) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Vector docs = new Vector();
        
        try {
            st = this.crearPreparedStatement("SQL_DOC_INT");
            st.setString(1,numrem);
            rs = st.executeQuery();
            while(rs.next()){
                docs.addElement(rs.getString("documento"));
            }
            st = null; rs=null;
            st =  this.crearPreparedStatement("SQL_DOC_INT2");
            st.setString(1, numrem);
            rs = st.executeQuery();
            while (rs.next()){
                docs.addElement(rs.getString("documento_rel"));
            }
            
            return docs;
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS REMESAS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            this.desconectar("SQL_DOC_INT");
            this.desconectar("SQL_DOC_INT2");
        }
    }
    public Vector getFacturasComerciales(String numrem) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Vector docs = new Vector();
        try {
            
            st =this.crearPreparedStatement("SQL_FACT");
            st.setString(1,numrem);
            rs = st.executeQuery();
            while(rs.next()){
                docs.addElement(rs.getString("documento"));
            }
            st = null; rs=null;
            st = this.crearPreparedStatement("SQL_FACT2");
            st.setString(1, numrem);
            rs = st.executeQuery();
            while (rs.next()){
                docs.addElement(rs.getString("documento_rel"));
            }
            return docs;
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS REMESAS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            this.desconectar("SQL_FACT");
            this.desconectar("SQL_FACT2");
        }
    }
    
    //Modificacion Henry 08.11.05
    public void obtenerRemesasModificacionOTs(String fecini, String fecfin) throws java.sql.SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        String docInterno = "", factura = "", cont = "", precinto = "";
        int posPuntos = 0;
        int posComa = 0 , posComa2 = 0;
        remesasOTs = null;
        remesasOTs = new Vector();
        int num = 0;
        try {
            st = this.crearPreparedStatement("SQL_LISTAR_MIGRACION_OT");
            st.setString(1,fecini);
            st.setString(2,fecfin);
            ////System.out.println("SQL MODIF 620: "+st);
            rs = st.executeQuery();
            while(rs.next()){
                num++;
                Vector datos = new Vector();
                String numrem = rs.getString(1);
                ////System.out.println("REMESA: "+numrem);
                datos.addElement(numrem);    //1
                
                String valor = rs.getString(2);
                double val= 0;
                if (!valor.equals(""))
                    val = Double.parseDouble(valor);
                
                datos.addElement(""+Util.redondear(val,2)); //2
                //datos.addElement(rs.getString(3)); //3
                datos.addElement(""); //UW VACIA
                //5vacios
                datos.addElement("");datos.addElement(""); //4,5,6,7,8
                datos.addElement("");datos.addElement("");
                datos.addElement("");
                docInterno = rs.getString(4);
                //Docs Internos
                Vector docus = this.getDocInternos(numrem);
                int tama = docus.size();
                int resto=0;
                if(tama>15)
                    tama=15;
                else{
                    tama = docus.size();
                    resto = 15-tama;
                }
                for(int h=0; h<tama; h++){
                    datos.addElement(docus.elementAt(h));//9-23
                }
                for(int h=0; h<resto; h++){
                    datos.addElement("");
                }
                factura = rs.getString(5);
                //FACTURAS
                Vector facts = this.getFacturasComerciales(numrem);
                int ta = facts.size();
                resto=0;
                if(ta>15)
                    ta=15;
                else {
                    ta = facts.size();
                    resto = 15-ta;
                }
                for(int h=0; h<ta; h++){
                    datos.addElement(facts.elementAt(h));//24-38
                }
                for(int h=0; h<resto; h++){
                    datos.addElement("");
                }
                //9 vacios despues de factura
                datos.addElement("");datos.addElement("");//39-47
                datos.addElement("");datos.addElement("");
                datos.addElement("");datos.addElement("");
                datos.addElement("");datos.addElement("");
                datos.addElement("");
                String valorU = rs.getString(6);
                double uni = Double.parseDouble(""+valorU.substring(0,valorU.length()-1));
                datos.addElement(""+Util.redondear(uni,2));//48
                datos.addElement("");//49
                //////System.out.println("unidad: "+datos.elementAt(42));
                //Extracci�n de contenedores separadas por coma.
                //EJ:cont001,cont002,etc.
                cont = rs.getString("CONTENEDORES");
                if (cont==null || cont.equals("")){
                    datos.addElement("");
                    datos.addElement("");
                } else {
                    posComa = 0;
                    posComa = cont.indexOf(",",posComa);
                    ////System.out.println("Posicion COMA: "+posComa);
                    if(cont.length()>0 && posComa==-1)
                        datos.addElement(cont.substring(0));
                    else if(posComa!=-1 && posComa!=0)
                        datos.addElement(cont.substring(0,posComa));
                    else
                        datos.addElement("");
                }//50
                ////System.out.println("CONTENEDOR1:"+datos.size());
                //CONTAINEr2
                if(posComa>0)
                    datos.addElement(cont.substring(posComa+1));
                else
                    datos.addElement("");    //51
                ////System.out.println("CONTENEDOR2:"+datos.size());
                precinto = rs.getString(8);
                //Extracci�n de precintos separadas por coma.
                //EJ:cont001,cont002,etc.
                if (precinto==null){
                    for (int j = 1; j<=5; j++){//52-56
                        datos.addElement("");
                    }
                } else {
                    posComa = 0;
                    posComa = precinto.indexOf(",",posComa);
                    if (precinto.length()>0 && posComa==-1)
                        datos.addElement(precinto.substring(0));
                    else if (posComa!=-1 && posComa>0)
                        datos.addElement(precinto.substring(0,posComa));
                    else
                        datos.addElement("");
                    for (int k=2; k<=5; k++){
                        if(posComa2!=-1 && posComa!=-1){
                            posComa2 = precinto.indexOf(",",posComa+1);
                            if (posComa2!=-1 && posComa!=-1) {
                                datos.addElement(precinto.substring(posComa+1,posComa2));
                            } else if(posComa2==-1 && posComa!=-1) {
                                datos.addElement(precinto.substring(posComa+1));
                            } else
                                datos.addElement("");
                            posComa  = posComa2;
                        } else
                            datos.addElement("");
                    }
                }
                posPuntos = 0;
                posComa=0;
                posComa2=0;
                datos.addElement("");//57
                datos.addElement("");//58
                ////System.out.println("TAMA�O DATOS: "+datos.size());
                remesasOTs.addElement(datos);
            }
            ////System.out.println("NUM VECES ENTRO: "+num);
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS REMESAS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            this.desconectar("SQL_LISTAR_MIGRACION_OT");
        }
    }
    public Vector getRemesasModificacionOTs() {
        return this.remesasOTs;
    }
    
    /**
     * Metodo obtenerRemesasNF, lista los registros de las remesa no facturadas
     * deacuerdo a un rango de fecha
     * @param: fecha inicio, fecha fin
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    
    public Vector obtenerRemesasNF(String fecini, String fecfin) throws java.sql.SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        remesasnf = null;
        try {
            st =  this.crearPreparedStatement("SQL_REMESANF");
            st.setString(1,fecini);
            st.setString(2,fecfin);
            st.setString(3,fecini);
            st.setString(4,fecfin);
            rs = st.executeQuery();
            
            remesasnf = new Vector();
            while(rs.next()){
                RemesaAnulada remesa = new RemesaAnulada();
                remesa.setNumero_remesa(rs.getString("numrem"));
                remesa.setFecha_creacion(rs.getString("creation_date"));
                remesa.setUsuario_creacion(rs.getString("nit"));
                remesasnf.addElement(remesa);
            }
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS REMESAS NF" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            this.desconectar("SQL_REMESANF");
        }
        return remesasnf;
    }
}
