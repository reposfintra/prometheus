/*
 * FacturaDAO.java
 *
 * Created on 2 de mayo de 2006, 07:18 PM
 */
package com.tsp.operation.model.DAOS;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;
import java.util.Date;
import java.text.*;
//import com.tsp.util.connectionpool.PoolManager;
import java.io.*;

/**
 *
 * @author  Andres Martinez
 */
public class FacturaDAO extends MainDAO{
    Vector remesas;
    Vector standar;
    Vector todasRemesas;
    Vector remesasFacturar;
    Vector remesasModificadas;

    Vector vFacturas;
    Vector vtipodoc;
    Vector vDocumentos;

    Vector filtroRutas;
    Vector filtroEstandares;
    Vector filtroProductos;
    Vector filtroTipoFactura;
    Vector telementos;
    Vector tunidades;

    factura factu= new factura();
    String Cliente;
    String monedaLocal;
    String dst;
    Vector vector;

    Vector remesasEliminadas;

    String numfac ="";

    ArrayList nitsx;
    boolean swNit;

    public FacturaDAO() {
        super( "FacturacionDAO.xml" );
    }
    
    public FacturaDAO(String dataBaseName) {
        super( "FacturacionDAO.xml",dataBaseName );
    }
    

/**
 *
 * @param codcli
 * @param cumplidas
 * @param dstrct
 * @throws SQLException
 */
    public void buscarRemesas(String codcli,String cumplidas,String dstrct)throws SQLException{

        remesas = new Vector();
        standar = new Vector();
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        try{
            String sql = this.obtenerSQL("SQL_BUSCAR_REMESAS").replaceAll("#ESTADO#",cumplidas);
            con = this.conectarJNDI("SQL_BUSCAR_REMESAS");//JJCastro fase2
            if(con!=null){
            st = con.prepareStatement(sql);
            st.setString(1, dstrct);
            st.setString(2, codcli);

            //ystem.out.println("QUERY: "+st);
            rs = st.executeQuery();
            while (rs.next()) {
                Vector a = new Vector();

                factura_detalle factu_deta = new factura_detalle();
                factu_deta.Load(rs);
                String cuenta = (rs.getString("cuenta")!=null)?rs.getString("cuenta"):"";
                factu_deta.setCodigo_cuenta_contable(cuenta);//factu.getCcuenta()

                Movrem mov = this.buscarMovrem(dstrct,factu_deta.getNumero_remesa(), "TOTAL");
                if(mov != null){
                    factu_deta.setCantidad( new Double(mov.getPesoreal()));
                    factu_deta.setValor_unitario  (mov.getQty_value());
                    factu_deta.setValor_unitariome(mov.getQty_value());
                    factu_deta.setValor_item      (mov.getVlrrem() ) ;
                    factu_deta.setValor_unitariome(mov.getVlrrem() ) ;
                }

                remesas.add(factu_deta);
            }

            sql = this.obtenerSQL("SQL_STANDARES").replaceAll("#ESTADO#",cumplidas);
            st = con.prepareStatement(sql);
            st.setString(1, dstrct);
            st.setString(2, codcli);
            rs = st.executeQuery();
            while (rs.next()) {
                factura_detalle factu = new factura_detalle();
                factu.setStd_job_no(rs.getString("std_job_no"));
                standar.add(factu);
            }
            //falta agregar lo demas para cargar los estandarts en la jsp filtros

        }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR REMESAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


/**
 *
 * @param codcli
 * @param cumplidas
 * @param dstrct
 * @throws SQLException
 */
    public void buscarTodasRemesas(String codcli,String cumplidas,String dstrct)throws SQLException{

        todasRemesas = new Vector();
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con =null;
        try{
            String sql = this.obtenerSQL("SQL_BUSCAR_REMESAS").replaceAll("#JOIN#",cumplidas);
            con = this.conectarJNDI("SQL_BUSCAR_REMESAS");//JJCastro fase2
            if(con!=null){
            st = con.prepareStatement(sql);
            st.setString(1, dstrct);
            st.setString(2, codcli);
            //ystem.out.println("QUERY: "+st);
            rs = st.executeQuery();
            while (rs.next()) {
                Vector a = new Vector();
                factura_detalle factu_deta = new factura_detalle();
                factu_deta.Load(rs);

                Movrem mov = this.buscarMovrem(dstrct,factu_deta.getNumero_remesa(), "TOTAL");
                if(mov != null){
                    factu_deta.setCantidad( new Double(mov.getPesoreal()));
                    factu_deta.setValor_unitario  (mov.getQty_value());
                    factu_deta.setValor_unitariome(mov.getQty_value());
                    factu_deta.setValor_item      (mov.getVlrrem() ) ;
                    factu_deta.setValor_unitariome(mov.getVlrrem() ) ;
                }
                todasRemesas.add(factu_deta);
            }

        }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR TODAS LAS REMESAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }




/**
 *
 * @param distrito
 * @param codcli
 * @throws SQLException
 */
    public void buscarFacturaDetalles(String distrito,String codcli)throws SQLException{
        Connection con = null;//JJCastro fase2
        remesasFacturar = new Vector();
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_FACTURA_DETALLES";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito);
            st.setString(2, codcli);

            //ystem.out.println("QUERY: "+st);
            rs = st.executeQuery();
            while (rs.next()) {
                factura_detalle factu_deta = new factura_detalle();

                factu_deta.Load2(rs);
                remesasFacturar.add(factu_deta);
            }

            }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


/**
 *
 * @param distrito
 * @param cod
 * @param fecha1
 * @param fecha2
 * @throws SQLException
 */
    public void buscarFacturasImpresion1(String distrito,String cod, String fecha1, String fecha2)throws SQLException{
        Connection con = null;
        vFacturas = new Vector();
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_FACTURAS_IMPRESION1";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito);
            st.setString(2, cod);
            st.setString(3, distrito);
            st.setString(4, cod);
            st.setString(5, fecha1);
            st.setString(6, fecha2);

            //ystem.out.println("QUERY: "+st);
            rs = st.executeQuery();
            while (rs.next()) {
                factura factu = new factura();

                factu.Load2(rs);
                vFacturas.add(factu);
            }

            }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


/**
 *
 * @param distrito
 * @param fecha1
 * @param fecha2
 * @throws SQLException
 */
    public void buscarFacturasImpresion2(String distrito,String fecha1, String fecha2)throws SQLException{
        Connection con = null;
        vFacturas = new Vector();
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_FACTURAS_IMPRESION2";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito);
            st.setString(2, fecha1);
            st.setString(3, fecha2);
            //ystem.out.println("QUERY: "+st);
            rs = st.executeQuery();
            while (rs.next()) {
                factura factu = new factura();

                factu.Load2(rs);
                vFacturas.add(factu);
            }

            }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

/**
 *
 * @param a
 * @param b
 * @param c
 * @return
 * @throws SQLException
 */
    public double valorConvertido(String a,double b, String c)throws SQLException{
        Connection con = null;
        double total=0;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_VALOR_CONVERTIDO";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, a);
            st.setDouble(2, b);
            st.setString(3, c);
            st.setString(4, "FINV");
            //ystem.out.println("QUERY: "+st);
            rs = st.executeQuery();
            while (rs.next()) {
                total=rs.getDouble("convertir_moneda");
            }
        }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return total;
    }


/**
 *
 * @param a
 * @param b
 * @param c
 * @param d
 * @param e
 * @return
 * @throws SQLException
 */
    public double valorTasaVal(String a, String b, String c, String d, String e)throws SQLException{
        Connection con = null;
        double tasa=0;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_TASAOVALOR_CONVER";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, a);//moneda1
            st.setString(2, b);//valor a convertir
            st.setString(3, c);//moneda2
            st.setString(4, d);//
            st.setString(5, e);
            //ystem.out.println("QUERY: "+st);
            rs = st.executeQuery();
            while (rs.next()) {
                tasa=rs.getDouble("convertir_moneda");
            }
            }}catch(SQLException g){
            g.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + g.getMessage()+"" + g.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException g){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + g.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException g){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + g.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException g){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + g.getMessage()); }}
        }
        return tasa;
    }

 /**
  *
  * @param a
  * @param b
  * @return
  * @throws SQLException
  */
    public double valorTasa(String a, String b)throws SQLException{
        Connection con = null;
        double tasa=0;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_TASA";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, a);
            st.setString(2, b);
            st.setString(3, b);
            st.setString(4, a);
            //ystem.out.println("QUERY: "+st);
            rs = st.executeQuery();
            while (rs.next()) {
                tasa=rs.getDouble("vlr_conver");
            }
        }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return tasa;
    }



/**
 *
 * @param a
 * @throws SQLException
 */
    public void buscarMoneda(String a)throws SQLException{
        Connection con = null;
        String moneda="";
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_MONEDA";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, a);
            //ystem.out.println("QUERY: "+st);
            rs = st.executeQuery();
            while (rs.next()) {

                monedaLocal=rs.getString("moneda");

            }
            }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

 /**
  *
  * @param a
  * @param agencia
  * @return
  * @throws SQLException
  */
    public boolean buscarCliente(String a,String agencia)throws SQLException{
        System.out.println("a:"+a+"age4ncia:"+agencia);
        boolean cli= false;
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String sql = this.obtenerSQL("SQL_BUSCAR_CLIENTE");
        try{//ystem.out.println("AGENCIA  "+ agencia);
        if(agencia.equals("OP")){
            sql = sql.replaceAll("#AGENCIA#", "");
        }else{
            sql = sql.replaceAll("#AGENCIA#", "AND c.agfacturacion='"+agencia+"'");
        }
        con = this.conectarJNDI("SQL_BUSCAR_CLIENTE");
        if(con!=null){
        st = con.prepareStatement(sql);
        st.setString(1, a);
        //ystem.out.println("QUERY: "+st);
        rs = st.executeQuery();
        while (rs.next()) {
            factu.Load(rs);
            factu.setCodcli(a);
            cli=true;
        }
        }}catch(SQLException e){
            e.printStackTrace();
            return false;
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return cli;
    }


   /**
    *
    * @return
    * @throws SQLException
    */
    public String[] buscarSerie()throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String[] serie={"",""};
        String query = "SQL_BUSCAR_SERIE";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            while (rs.next()) {
                serie[0]=rs.getString("prefix");
                serie[1]=rs.getString("last_number");

            }
            }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return serie;
    }


    /**
     * Getter for property remesas.
     * @return Value of property remesas.
     */
    public java.util.Vector getRemesas() {
        return remesas;
    }


    /**
     * Getter for property remesasFacturar.
     * @return Value of property remesasFacturar.
     */
    public java.util.Vector getRemesasFacturar() {
        return remesasFacturar;
    }

    /**
     * Setter for property remesasFacturar.
     * @param remesasFacturar New value of property remesasFacturar.
     */
    public void setRemesasFacturar(java.util.Vector remesasFacturar) {
        this.remesasFacturar = remesasFacturar;
    }

    /**
     * Getter for property factu.
     * @return Value of property factu.
     */
    public com.tsp.operation.model.beans.factura getFactu() {
        return factu;
    }

    /**
     * Setter for property factu.
     * @param factu New value of property factu.
     */
    public void setFactu(com.tsp.operation.model.beans.factura factu) {
        this.factu = factu;
    }

    /**
     * Setter for property remesas.
     * @param remesas New value of property remesas.
     */
    public void setRemesas(java.util.Vector remesas) {
        this.remesas = remesas;
    }

    /**
     * Getter for property monedaLocal.
     * @return Value of property monedaLocal.
     */
    public java.lang.String getMonedaLocal() {
        return monedaLocal;
    }

    /**
     * Setter for property monedaLocal.
     * @param monedaLocal New value of property monedaLocal.
     */
    public void setMonedaLocal(java.lang.String monedaLocal) {
        this.monedaLocal = monedaLocal;
    }

    /**
     * Getter for property remesasModificadas.
     * @return Value of property remesasModificadas.
     */
    public java.util.Vector getRemesasModificadas() {
        return remesasModificadas;
    }

    /**
     * Setter for property remesasModificadas.
     * @param remesasModificadas New value of property remesasModificadas.
     */
    public void setRemesasModificadas(java.util.Vector remesasModificadas) {
        this.remesasModificadas = remesasModificadas;
    }


    //jose de la rosa 2007-01-22
    public boolean statusaFacturaDetalles(String distrito,String tipodocu,String docu)throws SQLException{
        PreparedStatement st = null;
        Connection con = null;
        String serie="";
        String query = "SQL_STATUSA_FACTURA_DETALLES";//JJCastro fase2
        String sql = "";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,distrito);
            st.setString(2,tipodocu);
            st.setString(3,docu);
            st.setString(4,distrito);
            st.setString(5,tipodocu);
            st.setString(6,docu);
            st.executeUpdate();
            sql =  this.obtenerSQL("SQL_UPDATE_COSTO_REEMBOLSABLE2");
            st =  con.prepareStatement(sql);
            st.setString(1, docu);
            st.executeUpdate();

            }}catch(SQLException e){
            e.printStackTrace();
            return false;
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return true;
    }



/**
 *
 * @param distrito
 * @param tipodocu
 * @param docu
 * @return
 * @throws SQLException
 */
    public boolean statusaFactura(String distrito,String tipodocu,String docu)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        String serie="";
        String query = "SQL_STATUSA_FACTURA";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,distrito);
            st.setString(2,tipodocu);
            st.setString(3,docu);
            //ystem.out.println("QUERY: "+st);
            st.executeUpdate();

            }}catch(SQLException e){
            e.printStackTrace();
            return false;
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return true;
    }


    /**
     * Metodo : funcion que guarda un objeto tipo Factura detalle en un Archivo
     * @autor : Ing. Andres Martinez
     * @param : objeto ingreso, un vector de ingreso, nombre del archivo, el usuario
     * @version : 1.0
     */
    public void escribirArchivo(factura ingreso, Vector items, String nombreArchivo, String user)throws IOException{
        try{
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            String Ruta  = path + "/exportar/migracion/" + user;
            String RutaA = Ruta + "/"+nombreArchivo;

            File archivo = new File(Ruta);
            archivo.mkdirs();
            File archivoR =  new File(RutaA);
            //ystem.out.println("archivo:"+RutaA);
            FileOutputStream  salidaArchivoR =   new FileOutputStream(archivoR);
            ObjectOutputStream salidaObjetoR =   new ObjectOutputStream(salidaArchivoR);
            salidaObjetoR.writeObject(ingreso);
            salidaObjetoR.writeObject(items);
            salidaObjetoR.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    /**
     * Metodo: funcion que Lee  un objeto tipo ingreso detalle en un Archivo
     * @autor : Ing. Andres MArtinez
     * @param : nombre del archivo y el usuario
     * @version : 1.0
     * @return : objeto de tipo ingreso detalle.
     */
    public factura leerArchivo(String nombreArchivo, String user)throws IOException,ClassNotFoundException{

        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String path = rb.getString("ruta");
        String Ruta  = path + "/exportar/migracion/" + user + "/"+nombreArchivo;
        File archivo = new File(Ruta);
        FileInputStream entradaArchivo = new FileInputStream(archivo);
        ObjectInputStream entradaObjeto= new ObjectInputStream(entradaArchivo);
        try{
            factu = (factura) entradaObjeto.readObject();
        }
        catch(ClassNotFoundException e){
            factu = null;
            remesasFacturar=null;
            //ystem.out.println ("Error no se ha encontrado la clase");
        }
        entradaObjeto.close();
        return factu;
    }

    /**
     * Metodo: funcion que Lee  un objeto tipo Vector en un Archivo
     * @autor : Ing. Jose de la rosa
     * @param : nombre del archivo y el usuario
     * @version : 1.0
     * @return : Objeto tipo Vector con los Items del ingreso detalle.
     */
    public Vector leerArchivoItems(String nombreArchivo,String user)throws IOException,ClassNotFoundException{


        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String path = rb.getString("ruta");
        String Ruta  = path + "/exportar/migracion/" + user + "/"+nombreArchivo;
        File archivo = new File(Ruta);
        FileInputStream entradaArchivo = new FileInputStream(archivo);
        ObjectInputStream entradaObjeto= new ObjectInputStream(entradaArchivo);
        try{
            factu = (factura) entradaObjeto.readObject();
            remesasFacturar = (Vector) entradaObjeto.readObject();
        }
        catch(ClassNotFoundException e){
            factu=null;
            remesasFacturar=null;
            //ystem.out.println ("Error no se ha encontrado la clase");
        }
        entradaObjeto.close();
        return remesasFacturar;
    }

    /**
     * Getter for property vFacturas.
     * @return Value of property vFacturas.
     */
    public java.util.Vector getVFacturas() {
        return vFacturas;
    }

    /**
     * Setter for property vFacturas.
     * @param vFacturas New value of property vFacturas.
     */
    public void setVFacturas(java.util.Vector vFacturas) {
        this.vFacturas = vFacturas;
    }

    /**
     * Getter for property vtipodoc.
     * @return Value of property vtipodoc.
     */
    public java.util.Vector getVtipodoc() {
        return vtipodoc;
    }

    /**
     * Setter for property vtipodoc.
     * @param vtipodoc New value of property vtipodoc.
     */
    public void setVtipodoc(java.util.Vector vtipodoc) {
        this.vtipodoc = vtipodoc;
    }


/**
 *
 * @param tipo
 * @return
 * @throws SQLException
 */
    public Vector buscarDocumentos(String tipo)throws SQLException{
        Vector d=new Vector();
        //ystem.out.println("Entro a dao");
        Connection con = null;

        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            con = this.conectarJNDI("SQL_BUSCAR_DOCUMENTOS");//JJCastro fase2
            if (con != null) {
                String query = this.obtenerSQL("SQL_BUSCAR_DOCUMENTOS");
                String numre = "";
                for (int i = 0; i < remesas.size(); i++) {
                    factura_detalle fd = (factura_detalle) remesas.get(i);
                    numre += "'" + fd.getNumero_remesa() + "'";
                }
                numre = numre.replaceAll("''", "','");
                query = query.replaceAll("#NUMREM#", numre);
                st = con.prepareStatement(query);
                st.setString(1, tipo);
                //ystem.out.println("QUERY: "+st);

                rs = st.executeQuery();
                while (rs.next()) {
                    factura_detalle fdeta = new factura_detalle();
                    fdeta.setNumero_remesa(rs.getString("numrem"));
                    fdeta.setTipo_doc(rs.getString("tipo_doc"));
                    fdeta.setDocumento(rs.getString("documento"));
                    d.add(fdeta);
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return d;
    }


/**
 *
 * @param f
 * @throws SQLException
 */
    public void updateFechaImpresion(String f)throws SQLException{
        Connection con = this.conectarJNDI("SQL_UPDATE_FECHA_IMPRESION");//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        try{
            //ystem.out.println("Impresion ..........................");
            String query = this.obtenerSQL("SQL_UPDATE_FECHA_IMPRESION");
            String numre="";

            if(f.equals("")){
                for(int i=0; i< vFacturas.size();i++){
                    factura fd= (factura)vFacturas.get(i);
                    if(fd.getFecha_impresion().equals("0099-01-01 00:00:00")||fd.getFecha_contabilizacion().equals("0099-01-01 00:00:00")){
                        numre+="'"+fd.getFactura()+"'";
                    }
                }
                numre = numre.replaceAll("''","','");
            }
            else
                numre+="'"+f+"'";
            query = query.replaceAll("#NUMREM#", numre);
            st = con.prepareStatement(query);
            //ystem.out.println("QUERY: "+st);

            st.executeUpdate();

        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

 /**
  *
  * @throws SQLException
  */
    public void buscartelementos()throws SQLException{
        Connection con = null;
        telementos=new Vector();
        PreparedStatement st = null;
        ResultSet rs = null;
        String serie="";
        String query = "SQL_BUSCAR_TELEMENTOS";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            while (rs.next()) {
                telementos.add(rs.getString("table_code")+"-"+rs.getString("descripcion"));

            }
        }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

    }


/**
 *
 * @throws SQLException
 */
    public void buscartunidades()throws SQLException{
        Connection con = null;
        tunidades=new Vector();
        PreparedStatement st = null;
        ResultSet rs = null;
        String serie="";
        String query = "SQL_BUSCAR_TUNIDADES";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            while (rs.next()) {
                //serie=rs.getString("prefix");
                tunidades.add(rs.getString("table_code"));

            }
        }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    /**
     * Getter for property vDocumentos.
     * @return Value of property vDocumentos.
     */
    public java.util.Vector getVDocumentos() {
        return vDocumentos;
    }

    /**
     * Setter for property vDocumentos.
     * @param vDocumentos New value of property vDocumentos.
     */
    public void setVDocumentos(java.util.Vector vDocumentos) {
        this.vDocumentos = vDocumentos;
    }

    /**
     * Getter for property telementos.
     * @return Value of property telementos.
     */
    public java.util.Vector getTelementos() {
        return telementos;
    }

    /**
     * Setter for property telementos.
     * @param telementos New value of property telementos.
     */
    public void setTelementos(java.util.Vector telementos) {
        this.telementos = telementos;
    }

    /**
     * Getter for property tunidades.
     * @return Value of property tunidades.
     */
    public java.util.Vector getTunidades() {
        return tunidades;
    }

    /**
     * Setter for property tunidades.
     * @param tunidades New value of property tunidades.
     */
    public void setTunidades(java.util.Vector tunidades) {
        this.tunidades = tunidades;
    }

    public String[] buscarplancuentas(String a)throws SQLException{
        Connection con = null;
        String cli[]= {"",""};
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_PLANCUENTAS";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, a);
            //ystem.out.println("SQL_BUSCAR_PLANCUENTAS: "+st);
            rs = st.executeQuery();
            while (rs.next()) {

                cli[0]=rs.getString("nombre_largo");
                cli[1]=rs.getString("subledger");
            }
            }}catch(SQLException e){
            e.printStackTrace();

        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return cli;
    }



/**
 *
 * @param a
 * @param id
 * @return
 * @throws SQLException
 */
    public String  buscarsubledger(String a,String id)throws SQLException{
        Connection con = null;
        String cli ="";
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_SUBLEDGER";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, a);
            st.setString(2, id);
            //ystem.out.println("QUERY: "+st);
            rs = st.executeQuery();
            while (rs.next()) {

                cli=rs.getString("nombre");

            }
            }}catch(SQLException e){
            e.printStackTrace();

        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return cli;
    }

    /**
     * Getter for property todasRemesas.
     * @return Value of property todasRemesas.
     */
    public java.util.Vector getTodasRemesas() {
        return todasRemesas;
    }

    /**
     * Setter for property todasRemesas.
     * @param todasRemesas New value of property todasRemesas.
     */
    public void setTodasRemesas(java.util.Vector todasRemesas) {
        this.todasRemesas = todasRemesas;
    }

    public void buscarItemPdf(String factura)throws SQLException{

        remesasFacturar = new Vector();
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        try{

       con = this.conectarJNDI("SQL_BUSCAR_ITEMSPDF");//JJCastro fase2
       if(con!=null){
       String sql = this.obtenerSQL("SQL_BUSCAR_ITEMSPDF");
        st =  con.prepareStatement(sql);
        st.setString(1,factura);
        rs = st.executeQuery();
        while (rs.next()) {
            Vector a = new Vector();
            factura_detalle factu_deta = new factura_detalle();
            factu_deta.Load3(rs);

            sql =  this.obtenerSQL("SQL_BUSCAR_PLACA");
            st =  con.prepareStatement(sql);
            st.setString(1,factu_deta.getNumero_remesa());
            ResultSet rs2 = null;
            rs2 = st.executeQuery();
            while (rs2.next()) {
                factu_deta.setPlaveh(rs2.getString("plaveh"));
            }
            remesasFacturar.add(factu_deta);
        }

        }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }




/**
 *
 * @return
 * @throws SQLException
 */
    public TreeMap listarMonedas() throws SQLException{
        Connection con = null;
        PreparedStatement sttm = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_NOMBRE_MONEDAS";//JJCastro fase2
        TreeMap mone = new TreeMap();
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            sttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = sttm.executeQuery();
            while (rs.next()) {
                mone.put(rs.getString(2), rs.getString(1));
            }
        }}
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR AGENCIA" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (sttm  != null){ try{ sttm.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return mone;
    }



    /**
     * Metodo SQL_UPDATE_COSTO_REEMBOLSABLE.
     * @autor Ing. Andr�s Martinez*/
/**
 *
 * @param fac
 * @param query
 * @throws SQLException
 */
    public void updatecostoreembolsable(String fac,String query) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;

        try {

            con = this.conectarJNDI("SQL_UPDATE_COSTO_REEMBOLSABLE");//JJCastro fase2
            if(con!=null){
            String sql = this.obtenerSQL("SQL_UPDATE_COSTO_REEMBOLSABLE");
            //ystem.out.println("SQL_UPDATE_COSTO_REEMBOLSABLE:"+query);
            sql+=" "+query;

            st = con.prepareStatement(sql);
            st.setString(1, fac);
            //ystem.out.println("query:"+st.toString());
            st.executeUpdate();


        }}catch(SQLException e) {
            throw new SQLException("ERROR EN update costos reembolsables " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }



/**
 *
 * @param codcli
 * @return
 * @throws SQLException
 */
    public String buscarAgenciaCobro(String codcli)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String agencia ="";
        String query = "SQL_BUSCAR_AGENCIA_COBRO";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, codcli);

            //ystem.out.println("QUERY: "+st);
            rs = st.executeQuery();
            if (rs.next()) {
                agencia = rs.getString("agencia_cobro");
            }

            }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR SQL_BUSCAR_AGENCIA_COBRO " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return agencia;
    }
    //Modificacion Juan 28 dic
    /**
     *
     * @param distrito
     * @param cod
     * @param query
     * @throws SQLException
     */
    public void buscarFacturas(String distrito,String cod, String query)throws SQLException{

        vFacturas = new Vector();
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;

        try{
            con = this.conectarJNDI("SQL_BUSCAR_FACTURAS");//JJCastro fase2
            String sql = this.obtenerSQL("SQL_BUSCAR_FACTURAS");
            sql= sql.replaceAll("#QUE#",query );
            st = con.prepareStatement(sql);
            st.setString(1, distrito);
            st.setString(2, distrito);
            st.setString(3, cod);
            st.setString(4, cod);
            //ystem.out.println("QUERY: "+st);
            rs = st.executeQuery();

            while (rs.next()) {
                factura factu = new factura();
                factu.LoadBusqueda(rs);
                vFacturas.add(factu);
            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }



/**
 *
 * @param f
 * @param distrito
 * @throws SQLException
 */
    public void updateFechaImpresion( String f , String distrito )throws SQLException{
        Connection con = this.conectarJNDI("SQL_UPDATE_FECHA_IMPRESION");//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        try{
            String query = this.obtenerSQL("SQL_UPDATE_FECHA_IMPRESION");
            String numre="";

            if(f.equals("")){
                for(int i=0; i< vFacturas.size();i++){
                    factura fd= (factura)vFacturas.get(i);
                    if(fd.getFecha_impresion().equals("0099-01-01 00:00:00")||fd.getFecha_contabilizacion().equals("0099-01-01 00:00:00")){
                        numre+="'"+fd.getFactura()+"'";
                    }
                }
                numre = numre.replaceAll("''","','");
            }
            else
                numre+="'"+f+"'";
            query = query.replaceAll("#NUMREM#", numre);
            st = con.prepareStatement(query);
            st.setString(1, distrito);
            //ystem.out.println("QUERY: "+st);

            st.executeUpdate();

        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


/**
 *
 * @param factura
 * @param distrito
 * @throws SQLException
 */
    public void buscarItemPdf(String factura, String distrito )throws SQLException{

        remesasFacturar = new Vector();
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        try{
        con = this.conectarJNDI("SQL_BUSCAR_ITEMSPDF");//JJCastro fase2
        if(con!=null){
        String sql = this.obtenerSQL("SQL_BUSCAR_ITEMSPDF");
        st =  con.prepareStatement(sql);

        st.setString(1,distrito);
        st.setString(2,factura);

        rs = st.executeQuery();
        while (rs.next()) {
            Vector a = new Vector();
            factura_detalle factu_deta = new factura_detalle();

            factu_deta.Load3(rs);

//            sql =  this.obtenerSQL("SQL_BUSCAR_PLACA");
//            st =  con.prepareStatement(sql);
//            st.setString(1,factu_deta.getNumero_remesa());
//            ResultSet rs2 = null;
//            rs2 = st.executeQuery();
//            while (rs2.next()) {
//                factu_deta.setPlaveh(rs2.getString("plaveh"));
//            }
            remesasFacturar.add(factu_deta);
        }

        }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


/**
 *
 * @param factura
 * @param distrito
 * @throws SQLException
 */
    public void buscarItemsCarbon(String factura, String distrito )throws SQLException{
        Connection con = null;
        remesasFacturar         = new Vector();
        PreparedStatement st    = null;
        ResultSet rs            = null;
        String query            = "SQL_ITEMSCARBON";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,distrito);
            st.setString(2,factura);
            rs = st.executeQuery();
            while (rs.next()) {
                factura_detalle obj = new factura_detalle();
                obj.LoadCarbon(rs);
                remesasFacturar.add(obj);
            }

            }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }



    //jose de la rosa 2007-01-22
    public String anularFactura( String distrito, String tipodocu, String docu, String login)throws SQLException{
        StringStatement st = null;
        String serie="";
        String query = "SQL_ANULAR_FACTURA";
        String sql = "" ;
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1,login);//AMATURANA 28.03.2007
            st.setString(2,distrito);
            st.setString(3,docu);
            st.setString(4,tipodocu);
            st.setString(5,login);//AMATURANA 28.03.2007
            st.setString(6,login);
            st.setString(7,distrito);
            st.setString(8,docu);
            st.setString(9,tipodocu);

            serie = st.getSql();//JJCastro fase2
            st = null;
            sql =  this.obtenerSQL("SQL_UPDATE_COSTO_REEMBOLSABLE2");
            st = new StringStatement (sql, true);//JJCastro fase2
            st.setString(1, docu);
            serie += st.getSql();//JJCastro fase2
            //ystem.out.println("RRR "+serie);
            }catch(Exception e){
            e.printStackTrace();
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return serie;
    }

    //jose de la rosa 2007-01-22
/**
 *
 * @param distrito
 * @param tipodocu
 * @param docu
 * @param trans
 * @return
 * @throws SQLException
 */
    public String anularTransaccion( String distrito, String tipodocu, String docu, int trans)throws SQLException{
        StringStatement st = null;
        Connection con = null;
        String serie="";
        String query = "SQL_ANULAR_TRANSACCION";
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setInt(1,trans);
            st.setString(2,distrito);
            st.setString(3,tipodocu);
            st.setString(4,docu);
            //serie = st.toString();//20100702
            serie=st.getSql();//20100702
        }catch(Exception e){
            e.printStackTrace();
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return serie;
    }

    /**
     * Metodo existeRemesa busca la remesa
     * @autor Ing. ivan  gomez
     * @param codcli C�digo del cliente pagador y kla remesa
     * @throws SQLException si ocurre un error en la cone�i�n con la Base de Datos
     * @version 1.0.
     */
    public boolean existeRemesa(String codcli,String remesa)throws SQLException{
        Connection con = null;
        todasRemesas = new Vector();
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean existe = false;
        String query = "SQL_EXISTE_REMESA";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, codcli);
            st.setString(2, remesa);

            //ystem.out.println("QUERY: "+st);
            rs = st.executeQuery();
            if (rs.next()) {
                existe = true;
            }

            }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR TSQL_EXISTE_REMESA " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return existe;
    }



    /**
     * Getter for property standar.
     * @return Value of property standar.
     */
    public java.util.Vector getStandar() {
        return standar;
    }

    /**
     * Setter for property standar.
     * @param standar New value of property standar.
     */
    public void setStandar(java.util.Vector standar) {
        this.standar = standar;
    }
    /**
     * Metodo buscarCiudad_Cliente busca la ciudad de un cliente en la tabla usuarios
     * @autor Ing. Juan M. Escand�n P.
     * @param  idusuario - login del usuario
     * @throws SQLException si ocurre un error en la cone�i�n con la Base de Datos
     * @version 1.0.
     */
    public String buscarCiudad_Cliente( String idusuario )throws SQLException{
        Connection con = null;
        String ciudad           = "";
        PreparedStatement st    = null;
        ResultSet rs            = null;
        String query            = "SQL_BUSCAR_CIUDAD_CLI";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,idusuario);
            rs = st.executeQuery();
            while (rs.next()) {
                ciudad  = ( rs.getString("ciudad") != null )?rs.getString("ciudad"):"";
            }

            }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR buscarCiudad_Cliente " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return ciudad;
    }



    /************************************************************/
    //MODIFICACION
    /************************************************************/
    public void ReportePagosClientes(String fechaI,String fechaF ) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        factura a = null;
        vector = new Vector();
        String Nfact= "";
        String query = "Reporte_PCliente";//JJCastro fase2

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, fechaI );
            st.setString( 2, fechaF );

            rs = st.executeQuery();
            //ystem.out.println("----->>>   "+st.toString());
            String fec1 ;
            String fec2;
            String plaz;
            String facdoc = "";
            int valor = 0;
            while(rs.next()){
                a = new factura();
                a.setDocumento((rs.getString("documento")!=null)?rs.getString("documento"):"");
                a.setFecha_factura((rs.getString("fecha_factura")!=null)?rs.getString("fecha_factura"):"");
                a.setValor_factura(rs.getDouble("valor_factura"));
                a.setValor_abonome(rs.getDouble("valor_abonome"));
                fec1 = (rs.getString("fecha_ultimo_pago")!=null)?rs.getString("fecha_ultimo_pago"):"0099-01-01";
                if(fec1.equals("0099-01-01")){
                    fec1 = "";
                }
                a.setFecha_ultimo_pago(fec1);
                fec2= (rs.getString("fecha_vencimiento")!=null)?rs.getString("fecha_vencimiento"):"099-01-01";
                if(fec2.equals("0099-01-01")){
                    fec2 = "";
                }
                a.setFecha_vencimiento(fec2);
                plaz = (rs.getString("plazo")!=null)?rs.getString("plazo"):"";
                valor = Integer.parseInt(plaz);
                if (valor <= -1000){
                    plaz = "";
                }
                a.setPlazo(plaz);
                facdoc = (rs.getString("nro_aplicada")!=null)?rs.getString("nro_aplicada"):"";
                a.setDocumen_ing((rs.getString("fac_documento")!=null)?rs.getString("fac_documento"):"");
                a.setTipo_documento((rs.getString("tipo_doc")!=null)?rs.getString("tipo_doc"):"");
                a.setNum_ingreso((rs.getString("nro_aplicada")!=null)?rs.getString("nro_aplicada"):"");
                a.setVlr_ingreso((rs.getString("valor_nc")!=null)?rs.getString("valor_nc"):"");
                a.setMoneda((rs.getString("moneda")!=null)?rs.getString("moneda"):"");
                if (!facdoc.equals("")){
                    Nfact=  this.buscarFacturasCliente(facdoc);
                }
                a.setNfacturas(Nfact);
                Nfact = "";


                this.vector.addElement( a );
            }
            //return BeanCliente;
        }}
        catch(Exception e){
            e.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

    }

    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return vector;
    }

    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        this.vector = vector;
    }


    public String buscarFacturasCliente(String Factu)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        factura a = null;
        vector = new Vector();
        String facTotal = "";
        String query = "buscarNfacturas";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, Factu );
            rs = st.executeQuery();
            //ystem.out.println("----->>1>   "+st.toString());
            String fec1 ;
            String fec2;
            String plaz;
            int valor = 0;
            String fac= "";
            int cont1 = 1;
            int cont2 = 4;
            while(rs.next()){
                fac = (rs.getString("factura")!=null)?rs.getString("factura"):"";
                facTotal = facTotal +"  -  "+ fac;
                if (cont2 == cont1){
                    cont2=cont2 + 4 ;
                    facTotal = "\n"+ facTotal;
                }

                cont1++;
            }
            }}catch(SQLException e){
            e.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return facTotal;
    }

    /**
     * Metodo buscarNcCliente busca otros datos ingresos
     * @autor Ing. Enrique De Lavalle
     * @param  factura a
     * @throws SQLException si ocurre un error en la cone�i�n con la Base de Datos
     * @version 1.0.
     */
    public factura buscarNcCliente(factura a)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Vector vector_nc = new Vector();
        String ncAplic="";
        double vlrNC=0;
        String query = "buscarNcCliente";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, a.getNum_ingreso());
            rs = st.executeQuery();
            //ystem.out.println("----->>1>   "+st.toString());

            while(rs.next()){

                ncAplic  = (rs.getString("num_ingreso")!=null)?rs.getString("num_ingreso"):"";
                vlrNC = (rs.getDouble("vlr_ingreso_me"));
                a.setTipo_documento(ncAplic);//aqui se maneja la NC aplicada.
                a.setValor_saldo(vlrNC);//aqui se maneja el valor de la NC


            }

            }}catch(SQLException e){
            e.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return a;
    }

    /**
     * Metodo ReportePagosClientes busca los ingresos de un cliente
     * @autor Ing. Enrique De Lavalle
     * @param  String fechaI,String fechaF, String codcli
     * @throws SQLException si ocurre un error en la cone�i�n con la Base de Datos
     * @version 1.0.
     */
    public void ReportePagosClientes(String fechaI,String fechaF, String codcli ) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        factura a = null; //SE MANEJA POR INGRESO
        vector = new Vector();
        Vector vectorNC = new Vector();
        String Nfact= "", vlrNC="", moneda="", ncAplic="";
        String query = "Reporte_PCliente";
        Connection con = null;
        try {

            con = this.conectarJNDI(query);
            if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, fechaI+" 00:00");
            st.setString( 2, fechaF+" 23:59");
            st.setString( 3, codcli);

            rs = st.executeQuery();
            //ystem.out.println("----->>>   "+st.toString());
            String fec1 ;
            String fec2;
            String plaz;
            String facdoc = "";
            int valor = 0;
            while(rs.next()){
                a = new factura();
                a.setCodcli((rs.getString("nCliente")!=null)?rs.getString("nCliente"):"");//guarda el nombre del cliente
                a.setDocumento((rs.getString("documento")!=null)?rs.getString("documento"):"");
                a.setFecha_factura((rs.getString("fecha_factura")!=null)?rs.getString("fecha_factura"):"");
                a.setValor_factura(rs.getDouble("valor_factura"));
                a.setValor_abonome(rs.getDouble("valor_abonome"));
                fec1 = (rs.getString("fecha_ultimo_pago")!=null)?rs.getString("fecha_ultimo_pago"):"0099-01-01";
                if(fec1.equals("0099-01-01")){
                    fec1 = "";
                }
                a.setFecha_ultimo_pago(fec1);
                fec2= (rs.getString("fecha_vencimiento")!=null)?rs.getString("fecha_vencimiento"):"099-01-01";
                if(fec2.equals("0099-01-01")){
                    fec2 = "";
                }
                a.setFecha_vencimiento(fec2);
                plaz = (rs.getString("plazo")!=null)?rs.getString("plazo"):"";
                a.setPlazo(plaz);

                a.setNum_ingreso((rs.getString("num_ingreso")!=null)?rs.getString("num_ingreso"):"");
                a.setValor_abono(rs.getDouble("valor_ing"));//guarda el valor del ingreso no el abono
                a.setFecha_ingreso((rs.getString("fecha_ingreso")!=null)?rs.getString("fecha_ingreso"):"");
                a.setValor_saldome(rs.getDouble("saldo_facturaIngreso_me"));
                moneda = (rs.getString("codmoneda")!=null)?rs.getString("codmoneda"):"";
                a.setMoneda(moneda);

                a.setBanco(rs.getString("banco"));
                a.setSucursal(rs.getString("sucursal"));
                this.vector.addElement(  buscarNcCliente(a) );
            }
            //return BeanCliente;
            }}
        catch(Exception e){
            e.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

    }
    /**
     * Metodo inicialisa la Lista de Docuemntos por pagar en factura .
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param codcli C�digo del cliente
     * @param agc C�digo de la agencia de facturaci�n del cliente
     * @param agc_duena C�digo de la agencia due�a
     * @param agc_cobro C�digo de la agencia de cobro
     * @throws SQLException si ocurre un error en la cone�i�n con la Base de Datos
     * @version 1.0.
     */
    public void facturasNoPagadas(String cliente, String agc, String fecha, String dstrct, String agc_duena, String agc_cobro,String prov,String cccli,String vto,String cfact,String hc,String lim1,String lim2,String codigocliente,String convenio) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        this.vDocumentos = null;
        this.vDocumentos = new Vector();

        Logger logger = Logger.getLogger(this.getClass());
        logger.info("......... CLIENTE: " + cliente);
        logger.info("......... AGENCIA FACTUR.: " + agc);
        logger.info("......... AGENCIA DUE�A: " + agc_duena);
        logger.info("......... AGENCIA COBRO: " + agc_cobro);

        try {

            con = this.conectarJNDI("SQL_NOPAG_CLI");
            if(con!=null){
            String sql = this.obtenerSQL("SQL_NOPAG_CLI");

            sql = cliente.length()!=0 ? sql.replaceAll("#1"," AND fra.codcli = '" + cliente + "'") : sql.replaceAll("#1", "");
            sql = agc.length()!=0 ? sql.replaceAll("#2"," AND fra.agencia_facturacion = '" + agc  + "'") : sql.replaceAll("#2", "");
            sql = agc_duena.length()!=0 ? sql.replaceAll("#3"," AND cli.agduenia = '" + agc_duena  + "'") : sql.replaceAll("#3", "");
            sql = agc_cobro.length()!=0 ? sql.replaceAll("#4"," AND fra.agencia_cobro = '" + agc_cobro  + "'") : sql.replaceAll("#4", "");
            sql = prov.length()!=0 ? sql.replaceAll("#5"," AND negocios.nit_tercero = '" + prov  + "'") : sql.replaceAll("#5", "");
            sql = cccli.length()!=0 ? sql.replaceAll("#6"," AND cli.nit = '" + cccli  + "'") : sql.replaceAll("#6", "");
            sql = codigocliente.length()!=0 ? sql.replaceAll("#B#"," AND fra.codcli = '" + codigocliente  + "'") : sql.replaceAll("#B#", "");//20100629

            sql = convenio.length()!=0 ? sql.replaceAll("#convenio#"," AND negocios.id_convenio in (" + convenio  + ")") : sql.replaceAll("#convenio#", "");//201404. JPACOSTA
            
            if(vto.equals("0"))
            {
                sql=sql.replaceAll("#7", "");
            }
            else{
                if(vto.equals("1"))
                {
                    sql=sql.replaceAll("#7", "AND (-1 * (fra.fecha_vencimiento - ?) )  > 0");
                }
                else
                {
                    if(vto.equals("2"))
                    {
                        sql=sql.replaceAll("#7", "AND (-1 * (fra.fecha_vencimiento - ?) )  <= 0");
                    }
                }
            }
            sql = !cfact.equals("...") ? sql.replaceAll("#8"," AND fra.documento like '" + cfact  + "%'") : sql.replaceAll("#8", "");
            sql = !hc.equals("...") ? sql.replaceAll("#9"," AND fra.cmc = '" + hc  + "'") : sql.replaceAll("#9", "");
            sql = lim1.length()!=0  ?sql.replaceAll("#A#"," AND -1 * (fra.fecha_vencimiento - '"+fecha+"') between '"+lim1+"' and '"+lim2+"'  ") : sql.replaceAll("#A#", "");

            st = con.prepareStatement(sql);
            st.setString(1, fecha);
            st.setString(2, dstrct);
            if(vto.equals("1"))
                {
                    st.setString(3, fecha);
                }
                else
                {
                    if(vto.equals("2"))
                    {
                        st.setString(3, fecha);
                    }
                }


            logger.info("FRAS NO PAGADAS: " + st);
            System.out.print(st.toString());
            //ystem.out.println("Q--- "+st.toString());
            rs = st.executeQuery();

            while (rs.next()) {
                RepGral obj = new RepGral();
                obj.setAgcfacturacion(rs.getString("agfacturacion"));
                obj.setCodagcfacturacion(rs.getString("codagfacturacion"));
                obj.setCodcli(rs.getString("codcli"));
                obj.setNomcli(rs.getString("nomcli"));
                obj.setVlr_saldo_me(rs.getDouble("valor_saldome"));
                obj.setVlr_saldo(rs.getDouble("valor_saldo"));
                obj.setFecha_fra(rs.getString("fecha_factura"));
                obj.setFecha_venc_fra(rs.getString("fecha_vencimiento"));
                obj.setFactura(rs.getString("documento"));
                obj.setMoneda(rs.getString("moneda_cia"));
                obj.setNdias(rs.getInt("ndias"));
                obj.setNitter(rs.getString("afiliado"));
                obj.setDespla(rs.getString("desemb"));
                obj.setRegistros(rs.getString("tasa"));
                obj.setNitpro(rs.getString("ident"));
                obj.setNom_costo_2(rs.getString("tel"));
                obj.setDocu(rs.getString("docu"));
                obj.setConcepto_desc(rs.getString("com"));
                //AMATURANA 08.03.2007
                obj.setAgc_cobro(rs.getString("agencia_cobro"));
                obj.setAgc_duenia(rs.getString("agduenia"));
                //AMATURANA 22 - 23.03.2007
                obj.setNom_costo_4(rs.getString("obs"));
                obj.setNom_costo_3(rs.getString("ndiasf"));//dia de fenalco
                obj.setValor_factura(rs.getDouble("valor_factura"));
                obj.setValor_facturame(rs.getDouble("valor_facturame"));
                if( agc_cobro.equals("VA") ){
                    obj.setVlr_saldo(rs.getDouble("valor_saldome"));
                    obj.setMoneda(rs.getString("moneda"));
                    obj.setValor_factura(obj.getValor_facturame());
                }
                obj.setFecha_corte(fecha);
                obj.setNumaval(rs.getString("aval"));
                obj.setCodbco(rs.getString("bank"));
                obj.setRefcl(rs.getString("ref"));
                obj.setCmc(rs.getString("cmc"));
                //jpinedo
                obj.setCod_cuenta(rs.getString("cuenta"));
                obj.setTipoRef1(rs.getString("tipo_ref1"));
                obj.setRef1(rs.getString("ref1"));
                obj.setTipoRef2(rs.getString("tipo_ref2"));
                obj.setRef2(rs.getString("ref2"));
                obj.setNo_solicitud(rs.getInt("no_solicitud"));
                obj.setCiclo(rs.getInt("ciclo"));
                obj.setFechaFacturacion(rs.getString("fecha_facturacion"));
                obj.setProveedor(rs.getString("nit_afiliado"));
                obj.setNegocio(rs.getString("cod_neg"));
                obj.setNumero_pagare(rs.getString("numero_pagare"));
                obj.setNegocio_rel(rs.getString("negocio_rel"));
                
                //egonzalez nuevos campos reporte cartera.
                obj.setGestion(rs.getString("gestion"));
                obj.setProxima_accion(rs.getString("proxima_accion"));
                obj.setFecha_proxima_accion(rs.getString("fecha_proxima_accion"));
                obj.setEstado_cliente(rs.getString("estado_cliente"));
                obj.setDireccion(rs.getString("direccion"));
                obj.setBarrio(rs.getString("barrio"));
                obj.setCiudad(rs.getString("ciudad"));
                obj.setCedula_estudiante(rs.getString("cedula_estudiante"));
                obj.setNombre_estudiante(rs.getString("nombre_estudiante"));
                obj.setTelefono_estudiante(rs.getString("telefono_estudiante"));
                obj.setDireccion_estudiante(rs.getString("direccion_estudiante"));
                obj.setCedula_codeudor(rs.getString("cedula_codeudor"));
                obj.setNombre_codeudor(rs.getString("nombre_codeudor"));
                obj.setTelefono_codeudor(rs.getString("telefono_codeudor"));
                obj.setDireccion_codeudor(rs.getString("direccion_codeudor"));
                obj.setAsesor(rs.getString("asesor"));
                obj.setFecha_ultimo_pago(rs.getString("fecha_ultimo_pago"));
                obj.setVlr_ultimo_pago(rs.getDouble("vlr_ultimo_pago"));
                obj.setRango(rs.getString("rango"));
                obj.setId_convenio(rs.getString("id_convenio"));
                obj.setVlr_capital(rs.getDouble("capital"));
                obj.setVlr_cuota_manejo(rs.getDouble("cuota_manejo"));
                obj.setValor_interes(rs.getDouble("interes"));
                obj.setValor_cat(rs.getDouble("cat"));
                obj.setValor_seguro(rs.getDouble("seguro"));
                obj.setCapital_aval(rs.getDouble("capital_aval"));
                obj.setInteres_aval(rs.getDouble("interes_aval"));
                obj.setValor_aval(rs.getDouble("valor_aval"));
                obj.setAltura_mora(rs.getString("Altura_mora"));
                obj.setEstado_indemnizar(rs.getString("Estado_Indemnizado"));
                obj.setPlazo(rs.getString("plazo"));
                obj.setTasa(rs.getDouble("tasan"));
                obj.setPlan_al_Dia(rs.getString("plan_al_Dia"));
                obj.setAgencia(rs.getString("agencia"));
                
                this.vDocumentos.add(obj);
            }
        }}catch(SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR EN [facturasNoPagadas] " + e.getMessage()+"" + e.getErrorCode());
        } finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    /**
     * Metodo : Funcion que retorna facturas de acuerdo a los filtros de fecha, y de estado de impresion
     * @autor : Ing. Juan M. Escandon P
     * @param : String distrito, String query, String agencia
     * @version : 1.0
     */
public String  updateRemesa(String dstrct,String remesa,String tipodocu,String documento)throws SQLException{
        StringStatement st = null;
        Connection con = null;
        String query = "SQL_UPDATE_REMESA";
        String sql = "";
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1,tipodocu);
            st.setString(2,documento);
            st.setString(3,dstrct);
            st.setString(4,remesa);
            sql = st.getSql();//JJCastro fase2

        }catch(Exception e){
            e.printStackTrace();
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return sql;
    }



    /**
     * Metodo SQL_UPDATE_COSTO_REEMBOLSABLE.
     * @autor Ing. Andr�s Martinez*/
    public String updatecostoreembolsable(String fac, String tipo_costo, String codigo,String numpla,String std_job_no,String creation_date, String remesa) throws SQLException {
        Connection con = null;
        StringStatement st = null;
        ResultSet rs = null;
        String query = "SQL_UPDATE_COSTO_REEMBOLSABLE";
        String sql="";
        try {
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2

            st.setString(1,  fac);
            st.setString(2,  remesa);
            st.setString(3,  tipo_costo);
            st.setString(4,  codigo);
            st.setString(5,  std_job_no);
            st.setString(6,  creation_date);
            sql = st.getSql()+";";

        }catch(Exception e)
            {
                 throw new SQLException("ERROR DURANTE LAS APROBACION DE NEGOCIOS " + e.getMessage() );
             }
	    finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
            return sql;
    }


    public String aumentarSerie(int a )throws SQLException{

        StringStatement st = null;
        String query = "SQL_AUMENTAR_SERIE";

        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1,""+(a+1));
            return  st.getSql()+";";

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
    }

    /**
     * Metodo costoReembolsable.
     * @autor Ing. Andr�s Martinez*/

    public Vector costoReembolsable(String remesa) throws SQLException {
        Connection con = null;
        PreparedStatement sttm = null;
        ResultSet rs = null;
        Vector costosrem = new Vector();
        String query = "SQL_COSTO_REEMBOLSABLE";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            sttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            sttm.setString(1, factu.getCodcli());
            sttm.setString(2, remesa);
            rs = sttm.executeQuery();
            while (rs.next()) {

                factura_detalle a = new factura_detalle();
                String tip = rs.getString("tipo_costo");
                if(tip.equals("E")){
                    String vlr = rs.getString("vlr_ingreso");
                    if(!vlr.equals("000000")){
                        a.setEstraflete(true);
                        a.setNumero_remesa("");
                        a.setTipo_costo(tip);
                        a.setRemesa_costo(remesa);
                        a.setPlanilla(rs.getString("numpla"));
                        a.setStd_job_no(rs.getString("std_job_no"));
                        a.setConcepto(rs.getString("codigo"));
                        a.setFecha_cost_reemb(rs.getString("creation_date"));
                        a.setFecrem((rs.getString("creation_date")!=null)?rs.getString("creation_date").substring(0,10):"");
                        //a.setFecrem(rs.getString("creation_date")+" #numrem='"+remesa+"' and std_job_no='"+a.getStd_job_no()+"' and codigo='"+a.getConcepto()+"' and reg_status!='A'#");
                        //ystem.out.println("fechaaaaaaaaa:"+a.getFecrem());
                        a.setUnidad("");
                        a.setCantidad(new Double("1"));
                        a.setValor_unitario(rs.getDouble("vlr_ingreso"));
                        a.setValor_unitariome(rs.getDouble("vlr_ingreso"));
                        a.setValor_item(rs.getDouble("vlr_ingreso"));
                        a.setValor_itemme(rs.getDouble("vlr_ingreso"));
                        a.setDescripcion(rs.getString("descripcion")+"  "+remesa);
                        if(rs.getString("cuenta").equals("")){
                            a.setCodigo_cuenta_contable("");
                            a.setTiposubledger("");
                            a.setAuxliliar("");
                            a.setAux("N");
                        }
                        else{
                            a.setCodigo_cuenta_contable(rs.getString("cuenta"));
                            a.setTiposubledger("IT");
                            a.setAuxliliar(factu.getNit());
                            a.setAux("S");
                        }

                        a.setMoneda(rs.getString("moneda_costo"));
                        a.setOrigen("");
                        a.setDestino("");
                        // a.setStd_job_no("");
                        a.setCodtipocarga("");

                        costosrem.add(a);
                    }
                }
                else if(tip.equals("R")) {
                    a.setEstraflete(true);
                    a.setNumero_remesa("");
                    a.setTipo_costo(tip);
                    a.setRemesa_costo(remesa);
                    a.setPlanilla(rs.getString("numpla"));
                    a.setStd_job_no(rs.getString("std_job_no"));
                    a.setConcepto(rs.getString("codigo"));
                    a.setFecha_cost_reemb(rs.getString("creation_date"));
                    a.setFecrem((rs.getString("creation_date")!=null)?rs.getString("creation_date").substring(1,10):"");
                    //a.setFecrem(rs.getString("creation_date")+" #numrem='"+remesa+"' and std_job_no='"+a.getStd_job_no()+"' and codigo='"+a.getConcepto()+"' and reg_status!='A'#");
                    //ystem.out.println("fechaaaaaaaaa:"+a.getFecrem());
                    a.setUnidad("");
                    a.setCantidad(new Double("1"));
                    a.setValor_unitario(rs.getDouble("vlr_ingreso"));
                    a.setValor_unitariome(rs.getDouble("vlr_ingreso"));
                    a.setValor_item(rs.getDouble("vlr_ingreso"));
                    a.setValor_itemme(rs.getDouble("vlr_ingreso"));
                    a.setDescripcion(rs.getString("descripcion")+"  "+remesa);
                    if(rs.getString("cuenta").equals("")){
                        a.setCodigo_cuenta_contable("");
                        a.setTiposubledger("");
                        a.setAuxliliar("");
                        a.setAux("N");
                    }
                    else{
                        a.setCodigo_cuenta_contable(rs.getString("cuenta"));
                        a.setTiposubledger("IT");
                        a.setAuxliliar(factu.getNit());
                        a.setAux("S");
                    }

                    a.setMoneda(rs.getString("moneda_costo"));
                    a.setOrigen("");
                    a.setDestino("");
                    //a.setStd_job_no("");
                    a.setCodtipocarga("");

                    costosrem.add(a);
                }
            }
        }}
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR AGENCIA" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (sttm  != null){ try{ sttm.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return costosrem;
    }

    /**
     * Getter for property numfac.
     * @return Value of property numfac.
     */
    public java.lang.String getNumfac() {
        return numfac;
    }

    /**
     * Setter for property numfac.
     * @param numfac New value of property numfac.
     */
    public void setNumfac(java.lang.String numfac) {
        this.numfac = numfac;
    }

    /**
     * Metodo : Funcion que permite obtener informacion adicional, acerca de la descripcion de las Facturas de Carbon
     * @autor : Ing. Juan M. Escandon
     * @param : Distrito , Factura
     * @version : 1.0
     */
    public factura buscarDescripcionCarbon( String distrito, factura fac )throws SQLException{
        Connection con = null;
        PreparedStatement st    = null;
        ResultSet rs            = null;
        String query            = "SQL_DESCRIPCION_CARBON";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito);
            st.setString(2, fac.getFactura() );

            rs = st.executeQuery();
            while (rs.next()) {
                fac.setDescripcion_ruta    ( rs.getString("descripcion_ruta") );
                fac.setVlr_freight         ( rs.getDouble("vlr_freight") );
                fac.setSimbolo_descripcion( rs.getString("simbolo") );
            }

            }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR buscarDescripcionCarbon " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return fac;
    }





    /*Igomez 2007-04-11*/
    /**
     *
     * @param distrito
     * @param tipodocu
     * @param docu
     * @return
     * @throws SQLException
     */
    public String borrarFacturaDetalles(String distrito,String tipodocu,String docu)throws SQLException{
        StringStatement st = null;
        String serie="";
        String query = "SQL_BORRAR_FACTURA_DETALLES";
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1,distrito);
            st.setString(2,tipodocu);
            st.setString(3,docu);
            return st.getSql()+";";//JJCastro fase2

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR AL MODIFICAR LOS ITEM DE LA FACTURA" + e.getMessage() );
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
         }

    }
    /**
     *modifcarFactura
     * * descripcion: uctualiza el numero de revision de la publicacion del usuario en la tabla publicacion_novencida
     * @autor : Ing. Andres Martinez
     * @version : 1.0
     * @return :void
     */
    public String modificarFactura(factura factu,String distrito,String usuario,String codfac) throws SQLException {
        StringStatement st = null;
        String query = "SQL_MODIFICAR_FACTURA";//JJCastro fase2
        try{


            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2

            st.setString(1,""+factu.getNit());
            st.setString(2,""+factu.getCodcli());
            st.setString(3,"TR");
            st.setString(4, ""+factu.getFecha_factura());
            st.setString(5, ""+Util.fechaFinalYYYYMMDD(factu.getFecha_factura(),Integer.parseInt(factu.getPlazo())));
            st.setString(6, ""+factu.getDescripcion());
            st.setString(7, ""+factu.getObservacion());
            st.setString(8,""+factu.getValor_factura());

            st.setString(9,""+factu.getValor_abono());

            double f= factu.getValor_factura()-factu.getValor_abono();
            st.setString(10,""+f);
            st.setString(11,""+factu.getValor_facturame());
            st.setString(12,""+factu.getValor_tasa());
            st.setString(13, ""+factu.getMoneda());
            st.setString(14,""+remesasFacturar.size());
            st.setString(15, ""+factu.getForma_pago());

            st.setString(16, ""+factu.getAgencia_facturacion());
            st.setString(17, ""+factu.getAgencia_cobro());
            st.setString(18, ""+usuario);
            st.setString(19, ""+factu.getRif());
            st.setString(20, factu.getZona());
            st.setString(21, factu.getZona());
            st.setString(22, factu.getHc());

            st.setString(23,""+distrito);
            st.setString(24,codfac);

            //ystem.out.println(" query : "+st.toString ());
            return st.getSql()+";";//JJCastro fase2

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("Error al Modificar la factura" + e.getMessage() );
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }


    }

    /**
     * insertarFactura
     * * descripcion: uctualiza el numero de revision de la publicacion del usuario en la tabla publicacion_novencida
     * @autor : Ing. Andres Martinez
     * @version : 1.0
     * @return :void
     */
    /*public String ingresarFacturaDetalle(factura factu,String distrito,String usuario,String base,String f) throws Exception {
        Connection con= null;
        PreparedStatement ps = null;
        String SQL="";
        try{
            con = this.conectar("SQL_INGRESAR_FACTURA_DETALLE");
            if (con != null){
                ps = con.prepareStatement(this.obtenerSQL("SQL_INGRESAR_FACTURA_DETALLE"));
                for(int i=0; i<remesasFacturar.size();i++){
                    ps.clearParameters();
                    factura_detalle factud = (factura_detalle)remesasFacturar.get(i);
                    if(factud.getFecrem()==null)
                        factud.setFecrem("");

                    if(factud.isEstraflete()){
                        SQL += this.updatecostoreembolsable(f, factud.getTipo_costo(), factud.getConcepto(), factud.getPlanilla(),factud.getStd_job_no(),factud.getFecha_cost_reemb(),factud.getRemesa_costo());
                    }

                    ps.setString(1,""+distrito);
                    ps.setString(2,""+f);
                    ps.setString(3,""+(i+1));
                    ps.setString(4,""+factu.getNit());

                    if(factud.getConcepto()==null){
                        factud.setConcepto("TR");
                    }

                    ps.setString(5, factud.getConcepto());

                    ps.setString(6,""+factud.getNumero_remesa());
                    ps.setString(7,""+ factud.getDescripcion());

                    String auxili ="";
                    if(factud.getAux().equals("S")){
                        auxili=factud.getTiposubledger()+"-"+factud.getAuxliliar();
                    }

                    ps.setString(8,""+ factud.getCodigo_cuenta_contable());
                    ps.setString(9,auxili);


                    ps.setString(10,""+factud.getCantidad());
                    ps.setString(11,""+factud.getValor_unitario());
                    ps.setString(12, ""+factud.getValor_unitariome());
                    ps.setString(13,""+factud.getValor_item());
                    ps.setString(14, ""+factud.getValor_itemme());
                    ps.setString(15, ""+factud.getValor_tasa());
                    ps.setString(16,""+factu.getMoneda());
                    ps.setString(17,""+base);
                    ps.setString(18,""+usuario);
                    ps.setString(19,""+usuario);

                    SQL += ps.toString()+";";
                    ps.clearParameters();
                    ystem.out.println("REMESA-->"+factud.getNumero_remesa() );
                    if(!factud.getNumero_remesa().equals("")){
                        //ps = this.updateRemesa(factud.getNumero_remesa(), "FAC", f).toString();
                        SQL += this.updateRemesa(distrito,factud.getNumero_remesa(), "FAC", f).toString()+";";
                        SQL += this.updateMovrem(distrito,factud.getNumero_remesa(), "FAC", f);

                    }
                }


                ystem.out.println("REMESA ELIMINADAS SIZE--->"+remesasEliminadas.size());
                for(int i=0; i<remesasEliminadas.size();i++){
                    factura_detalle factud = (factura_detalle)remesasEliminadas.get(i);
                    if(!factud.getNumero_remesa().equals("")){
                        SQL += this.updateRemesa(distrito,factud.getNumero_remesa(), "", "").toString()+";";
                        SQL += this.updateMovrem(distrito,factud.getNumero_remesa(), "", "");

                    }
                }

            }
        }catch(Exception e){

            e.printStackTrace();
            throw new Exception(e.getMessage());

        }finally{
            if(ps != null){
                ps.close();
            }
            desconectar("SQL_INGRESAR_FACTURA_DETALLE");
        }
        return SQL;
    }*/

/**
 *
 * @param factu
 * @param distrito
 * @param usuario
 * @param base
 * @param f
 * @return
 * @throws Exception
 */
    public String ingresarFacturaDetalle(factura factu,String distrito,String usuario,String base,String f) throws Exception {
        Connection con= null;
        String nomsql="";
        StringStatement ps = null;
        String SQL="";
        try{
            if (factu.getCmc()!=null && factu.getCmc().equals("GC")){
                nomsql="SQL_INGRESAR_FACTURA_DETALLE_G";
            }else{
                nomsql="SQL_INGRESAR_FACTURA_DETALLE";
            }


                for(int i=0; i<remesasFacturar.size();i++){
                    ps = new StringStatement (this.obtenerSQL(nomsql), true);//JJCastro fase2
                    factura_detalle factud = (factura_detalle)remesasFacturar.get(i);
                    if(factud.getFecrem()==null)
                        factud.setFecrem("");

                    if(factud.isEstraflete()){
                        SQL += this.updatecostoreembolsable(f, factud.getTipo_costo(), factud.getConcepto(), factud.getPlanilla(),factud.getStd_job_no(),factud.getFecha_cost_reemb(),factud.getRemesa_costo());
                    }

                    ps.setString(1,""+distrito);
                    ps.setString(2,""+f);
                    ps.setString(3,""+(i+1));
                    ps.setString(4,""+factu.getNit());

                    if(factud.getConcepto()==null){
                        factud.setConcepto("TR");
                    }

                    ps.setString(5, factud.getConcepto());

                    ps.setString(6,""+factud.getNumero_remesa());
                    ps.setString(7,""+ factud.getDescripcion());

                    String auxili ="";
                    if(factud.getAux().equals("S")){
                        auxili=factud.getTiposubledger()+"-"+factud.getAuxliliar();
                    }

                    ps.setString(8,""+ factud.getCodigo_cuenta_contable());
                    ps.setString(9,auxili);


                    ps.setString(10,""+factud.getCantidad());
                    ps.setString(11,""+factud.getValor_unitario());
                    ps.setString(12, ""+factud.getValor_unitariome());
                    ps.setString(13,""+factud.getValor_item());
                    ps.setString(14, ""+factud.getValor_itemme());
                    ps.setString(15, ""+factud.getValor_tasa());
                    ps.setString(16,""+factu.getMoneda());
                    ps.setString(17,""+base);
                    ps.setString(18,""+usuario);
                    ps.setString(19,""+usuario);

                    SQL += ps.getSql()+";";//JJCastro fase2
                    ps = null ;
                    //ystem.out.println("REMESA-->"+factud.getNumero_remesa() );
                    if(!factud.getNumero_remesa().equals("")){
                        //ps = this.updateRemesa(factud.getNumero_remesa(), "FAC", f).toString();
                        SQL += this.updateRemesa(distrito,factud.getNumero_remesa(), "FAC", f)+";";//JJCastro fase2
                        SQL += this.updateMovrem(distrito,factud.getNumero_remesa(), "FAC", f);

                    }
                }


                //ystem.out.println("REMESA ELIMINADAS SIZE--->"+remesasEliminadas.size());
                for(int i=0; i<remesasEliminadas.size();i++){
                    factura_detalle factud = (factura_detalle)remesasEliminadas.get(i);
                    if(!factud.getNumero_remesa().equals("")){
                        SQL += this.updateRemesa(distrito,factud.getNumero_remesa(), "", "")+";";//JJCastro fase2
                        SQL += this.updateMovrem(distrito,factud.getNumero_remesa(), "", "");

                    }
                }


        }catch(Exception e){

            e.printStackTrace();
            throw new Exception(e.getMessage());

        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return SQL;
    }

    /**
     * Metodo : Funcion que retorna facturas de acuerdo a los filtros de fecha, y de estado de impresion
     * @autor : Ing. Juan M. Escandon P
     * @param : String distrito, String query, String agencia
     * @version : 1.0
     */

    public void buscarFacturasRangoFechas(  String distrito, String query, String agencia)throws SQLException{

        vFacturas = new Vector();
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;

        try{
            con = this.conectarJNDI("SQL_BUSCAR_FACTURAS_RANGOFECHAS");//JJCastro fase2
            if(con!=null){
            String sql = this.obtenerSQL("SQL_BUSCAR_FACTURAS_RANGOFECHAS");
            sql= sql.replaceAll("#QUE#",query );
            if(agencia.equals("OP"))
                sql = sql.replaceAll("#AGENCIA#","");
            else
                sql = sql.replaceAll("#AGENCIA#", " AND a.agencia_impresion ='"+agencia+"'");

            st = con.prepareStatement(sql);
            st.setString(1, distrito);
            st.setString(2, distrito);
            rs = st.executeQuery();

            while (rs.next()) {
                factura factu = new factura();
                factu.LoadBusqueda(rs);
                factu.setSimbolo( rs.getString("simbolo")!=null?rs.getString("simbolo"):"" );
                vFacturas.add(factu);
            }
        }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR buscarFacturasRangoFechas " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


    public void buscarFacturas(String distrito,String cod, String query,String agencia)throws SQLException{

        vFacturas = new Vector();
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;

        try{
            con = this.conectarJNDI("SQL_BUSCAR_FACTURAS");
            String sql = this.obtenerSQL("SQL_BUSCAR_FACTURAS");
            sql= sql.replaceAll("#QUE#",query );
            //ystem.out.println("AGENCIA " + agencia);
            if(agencia.equals("OP"))
                sql = sql.replaceAll("#AGENCIA#","");
            else
                sql = sql.replaceAll("#AGENCIA#", " AND a.agencia_impresion ='"+agencia+"'");

            st = con.prepareStatement(sql);
            st.setString(1, distrito);
            st.setString(2, distrito);
            st.setString(3, cod);
            st.setString(4, cod);
            rs = st.executeQuery();

            while (rs.next()) {
                factura factu = new factura();
                factu.LoadBusqueda(rs);
                factu.setSimbolo( rs.getString("simbolo")!=null?rs.getString("simbolo"):"" );
                vFacturas.add(factu);
            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }




    /**
     * insertarFactura
     * * descripcion: uctualiza el numero de revision de la publicacion del usuario en la tabla publicacion_novencida
     * @autor : Ing. Andres Martinez
     * @version : 1.0
     * @return :void
     */
    /*public String ingresarFactura(factura factu,String distrito,String usuario,String base) throws Exception {
        PreparedStatement st = null;

        String[] f={"",""};
        String ff="";
        String sql ="";
        try{

            st = this.crearPreparedStatement("SQL_INGRESAR_FACTURA");
            f= this.buscarSerie();
            sql = this.aumentarSerie(Integer.parseInt(f[1]));
            ff=f[0];
            for(int i=f[1].length();i<7;i++){
                ff+="0";
            }
            st.setString(1,""+distrito);
            st.setString(2,ff+f[1]);
            st.setString(3,""+factu.getNit());
            st.setString(4,""+factu.getCodcli());
            st.setString(5,"TR");
            st.setString(6, ""+factu.getFecha_factura());
            st.setString(7, ""+Util.fechaFinalYYYYMMDD(factu.getFecha_factura(),Integer.parseInt(factu.getPlazo())));
            st.setString(8, ""+factu.getDescripcion());
            st.setString(9, ""+factu.getObservacion());
            st.setString(10,""+factu.getValor_factura());
            st.setString(11,"0");
            st.setString(12,""+factu.getValor_factura());
            st.setString(13,""+factu.getValor_facturame());
            st.setString(14,""+factu.getValor_facturame());
            st.setString(15,""+factu.getValor_tasa());
            st.setString(16, ""+factu.getMoneda());
            st.setString(17,""+remesasFacturar.size());
            st.setString(18, ""+factu.getForma_pago());

            st.setString(19, ""+factu.getAgencia_facturacion());
            st.setString(20, ""+factu.getAgencia_cobro());
            st.setString(21, ""+base);
            st.setString(22, ""+usuario);
            st.setString(23, ""+usuario);
            st.setString(24, ""+factu.getRif());
            st.setString(25, ""+factu.getCmc());
            st.setString(26, ""+factu.getZona());
            st.setString(27, ""+factu.getAgencia_facturacion());
            //ystem.out.println(" query : "+st.toString ());
            sql +=  st.toString()+";";

        }catch(Exception e){
            e.printStackTrace();
            throw new Exception("ERROR AL GRABAR LA FACTURA" + e.getMessage() );
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(Exception e){
                    throw new Exception("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            this.desconectar( "SQL_INGRESAR_FACTURA" );
        }
        numfac = ff+f[1];
        return sql;
    }*/

    public String ingresarFactura(factura factu,String distrito,String usuario,String base) throws Exception {
        StringStatement st = null;

        String[] f={"",""};
        String ff="";
        String sql ="";
        String nombre_sql="";
        try{
            //ystem.out.println("cmc"+factu.getCmc());
            if (factu.getCmc()!=null && factu.getCmc().equals("GC")){
                //st = this.crearPreparedStatement("SQL_INGRESAR_FACTURA_G");
                nombre_sql="SQL_INGRESAR_FACTURA_G";
            }else{
                //st = this.crearPreparedStatement("SQL_INGRESAR_FACTURA");
                nombre_sql="SQL_INGRESAR_FACTURA";
            }

            st = new StringStatement (this.obtenerSQL(nombre_sql), true);//JJCastro fase2

            if (factu.getCmc()!=null && factu.getCmc().equals("GC")){
                f= this.buscarSerieG();
            }else{
                f= this.buscarSerie();
            }
            if (factu.getCmc()!=null && factu.getCmc().equals("GC")){
                sql = this.aumentarSerieG(Integer.parseInt(f[1]));
            }else{
                sql = this.aumentarSerie(Integer.parseInt(f[1]));
            }

            ff=f[0];
            for(int i=f[1].length();i<7;i++){
                ff+="0";
            }
            st.setString(1,""+distrito);
            st.setString(2,ff+f[1]);
            st.setString(3,""+factu.getNit());
            st.setString(4,""+factu.getCodcli());
            st.setString(5,"TR");
            st.setString(6, ""+factu.getFecha_factura());
            st.setString(7, ""+Util.fechaFinalYYYYMMDD(factu.getFecha_factura(),Integer.parseInt(factu.getPlazo())));
            st.setString(8, ""+factu.getDescripcion());
            st.setString(9, ""+factu.getObservacion());
            st.setString(10,""+factu.getValor_factura());
            st.setString(11,"0");
            st.setString(12,""+factu.getValor_factura());
            st.setString(13,""+factu.getValor_facturame());
            st.setString(14,""+factu.getValor_facturame());
            st.setString(15,""+factu.getValor_tasa());
            st.setString(16, ""+factu.getMoneda());
            st.setString(17,""+remesasFacturar.size());
            st.setString(18, ""+factu.getForma_pago());

            st.setString(19, ""+factu.getAgencia_facturacion());
            st.setString(20, ""+factu.getAgencia_cobro());
            st.setString(21, ""+base);
            st.setString(22, ""+usuario);
            st.setString(23, ""+usuario);
            st.setString(24, ""+factu.getRif());
            st.setString(25, ""+factu.getCmc());
            st.setString(26, ""+factu.getZona());
            st.setString(27, ""+factu.getAgencia_facturacion());
            //ystem.out.println(" query : "+st.toString ());
            sql +=  st.getSql()+";";

        }catch(Exception e){
            e.printStackTrace();
            throw new Exception("ERROR AL GRABAR LA FACTURA" + e.getMessage() );
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        numfac = ff+f[1];
        return sql;
    }



    /**
     * buscarFacturaCambio
     * * descripcion: metodo para buscar informacion de una factura para el programa  cambiode agencia y formato
     * @autor : Ing. Ivan Gomez
     * @version : 1.0
     * @return :obj factura
     */
    public void buscarFacturaCambio(String dstrct,String factura)throws SQLException{
        Connection con = null;
        vFacturas = new Vector();
        PreparedStatement st = null;
        ResultSet rs = null;
        factu = null;
        String query = "SQL_BUSCAR_FACTURA_CAMBIO";
        try{

            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, factura);
            //ystem.out.println("SQLLLLLLL---->"+ st.toString());
            rs = st.executeQuery();

            if (rs.next()) {
                factu = new factura();
                factu.setFactura            ( rs.getString("documento"));
                factu.setCodcli             ( rs.getString("codcli"));
                factu.setAgencia_facturacion( rs.getString("nom_agencia_facturacion"));
                factu.setAgencia_impresion  ( rs.getString("nom_agencia_impresion"));
                factu.setValor_factura      ( rs.getDouble("valor_facturame"));
                factu.setFormato            ( rs.getString("formato"));
                factu.setFecha_factura      ( rs.getString("fecha_factura"));
                factu.setMoneda             ( rs.getString("moneda"));

            }

        }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR EN SQL_BUSCAR_FACTURA_CAMBIO " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


    /**
     * Metodo updateImpresion, permite actualizar los parametros de impresion
     * @autor : Ing. Ivan Dario Gomez
     * @param : String distrito, String factura, String agencia_impresion, String formato
     * @version : 1.0
     */
    public void updateImpresion(String distrito, String factura, String agencia_impresion, String formato) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_UPDATE_IMPRESION";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,agencia_impresion);
            st.setString(2,formato);
            st.setString(3,distrito);
            st.setString(4,factura);
            st.executeUpdate();
        }}catch(SQLException e){
            throw new SQLException("ERROR AL SQL_UPDATE_IMPRESION " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }



    /**
     * buscarDatosRemesa
     * * descripcion: metodo para buscar informacion de una remesa
     * @autor : Ing. Ivan Gomez
     * @version : 1.0
     * @return :obj factura
     */
    public Vector buscarDatosRemesa(Vector vector)throws SQLException{

        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Vector nuevo = new Vector();
        String query = "SQL_BUSCAR_DATOS_REMESA";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            for ( int i = 0; i< vector.size(); i++){
                boolean sw = false;
                factura_detalle factu_deta = (factura_detalle) vector.get(i);
                if(!factu_deta.getNumero_remesa().equals("")){
                    st.setString(1, factu_deta.getNumero_remesa());
                    rs = st.executeQuery();
                    while (rs.next()) {
                        sw = true;
                        factu_deta.setPlanilla(rs.getString("planilla"));
                        factu_deta.setOrigen(rs.getString("origen"));
                        factu_deta.setDestino(rs.getString("destino"));
                        factu_deta.setPlaveh(rs.getString("placa"));
                        factu_deta.setPeso_real_cargado(rs.getDouble("peso_real_cargado"));
                        factu_deta.setPeso_cobrar(rs.getDouble("peso_cobrar"));
                        factu_deta.setUnit_packed(rs.getString("unit_packed"));
                        nuevo.add(factu_deta);
                    }
                }

                if(!sw)
                    nuevo.add(factu_deta);
            }




            }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR EN SQL_BUSCAR_DATOS_REMESA " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return nuevo;
    }


    /**
     * buscarMovrem
     * * descripcion: metodo para buscar informacion de una remesa
     * @autor : Ing. Ivan Gomez
     * @param : String numero de remesa y String tipo de busqueda
     *          tipo :  "TOTAL" para traer los valores totales de la remesa
     *                  "MOVIMIENTOS" para traer solo la sumatoria de los movimientos
     * @version : 1.0
     * @return :obj factura
     */
    public Movrem buscarMovrem(String dstrct,String numrem, String tipo)throws SQLException{

        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Movrem mov = null;
        String query = "SQL_BUSCAR_VALORES_ORIGINALES";
        String sql = "" ;
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, numrem);
            rs = st.executeQuery();
            if (rs.next()) {
                mov = new Movrem();
                mov.setNumrem   (numrem);
                mov.setPesoreal(rs.getDouble("pesoreal"));
                mov.setQty_value(rs.getDouble("qty_value"));
                mov.setVlrrem   (rs.getDouble("vlrrem"));
                mov.setVlrrem2  (rs.getDouble("vlrrem2"));
                mov.setTasa     ( (mov.getVlrrem()!=0? mov.getVlrrem2() / mov.getVlrrem():0)  );


                if(tipo.equals("TOTAL")){

                    sql = this.obtenerSQL("SQL_BUSCAR_MOVIMIENTOS_REM");
                    st = con.prepareStatement(sql);
                    st.setString(1, dstrct);
                    st.setString(2, mov.getNumrem());
                    rs = st.executeQuery();
                    if(rs.next()){
                        mov.setPesoreal(mov.getPesoreal()  + rs.getDouble("pesoreal"));
                        mov.setQty_value(mov.getQty_value() + rs.getDouble("qty_value"));
                        mov.setVlrrem   (mov.getVlrrem()    + rs.getDouble("vlrrem"));
                        mov.setVlrrem2  (mov.getVlrrem2()   + rs.getDouble("vlrrem2"));
                        mov.setTasa     (mov.getTasa()      + rs.getDouble("tasa"));


                    }

                }else if (tipo.equals("MOVIMIENTOS") ){

                    sql = this.obtenerSQL("SQL_BUSCAR_MOVIMIENTOS_REM");
                    st = con.prepareStatement(sql);
                    st.setString(1, dstrct);
                    st.setString(2, mov.getNumrem());
                    rs = st.executeQuery();
                    if(rs.next()){
                        mov.setPesoreal(rs.getDouble("pesoreal"));
                        mov.setQty_value(rs.getDouble("qty_value"));
                        mov.setVlrrem   (rs.getDouble("vlrrem"));
                        mov.setVlrrem2  (rs.getDouble("vlrrem2"));
                        mov.setTasa     (rs.getDouble("tasa"));

                    }else{
                        mov.setPesoreal(0);
                        mov.setQty_value(0);
                        mov.setVlrrem   (0);
                        mov.setVlrrem2  (0);
                        mov.setTasa     (0);
                    }

                }

            }



        }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR EN SQL_BUSCAR_VALORES_ORIGINALES " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return mov;
    }

    /**
     * Metodo : Funcion que retorna facturas de acuerdo a los filtros de fecha, y de estado de impresion
     * @autor : Ing. Juan M. Escandon P
     * @param : String distrito, String query, String agencia
     * @version : 1.0
     */
    public String updateMovrem(String dstrct,String remesa,String tipodocu,String documento)throws SQLException{
        StringStatement st = null;
        String query = "SQL_UPDATE_MOVREM";
        String sql = "" ;
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1,tipodocu);
            st.setString(2,documento);
            st.setString(3,dstrct);
            st.setString(4,remesa);
            sql = st.getSql();//JJCastro fase2
            //ystem.out.println("QUERY: "+st.toString());
        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR EN SQL_UPDATE_MOVREM " + e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return sql+";";
    }



    /**
     * Getter for property remesasEliminadas.
     * @return Value of property remesasEliminadas.
     */
    public java.util.Vector getRemesasEliminadas() {
        return remesasEliminadas;
    }

    /**
     * Setter for property remesasEliminadas.
     * @param remesasEliminadas New value of property remesasEliminadas.
     */
    public void setRemesasEliminadas(java.util.Vector remesasEliminadas) {
        this.remesasEliminadas = remesasEliminadas;
    }



/**
 *
 * @return
 * @throws SQLException
 */
    public String[] buscarSerieG()throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String[] serie={"",""};
        String query = "SQL_BUSCAR_SERIE_G";//JJCastro fase2
        try{

            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            while (rs.next()) {
                serie[0]=rs.getString("prefix");
                serie[1]=rs.getString("last_number");

            }
            }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return serie;
    }


/**
 *
 * @param a
 * @return
 * @throws SQLException
 */
    public String aumentarSerieG(int a )throws SQLException{
        StringStatement st = null;
        String query = "SQL_AUMENTAR_SERIE_G";
        String sql = "";
        try{
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1,""+(a+1));
            sql = st.getSql()+";";

            }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;

    }


 /**
  *
  * @param cliente
  * @param agc
  * @param fecha
  * @param dstrct
  * @param agc_duena
  * @param agc_cobro
  * @param prov
  * @param cccli
  * @param vto
  * @param cfact
  * @param hc
  * @param lim1
  * @param lim2
  * @throws SQLException
  */
    public void facturasCorficolombiana( String cliente, String agc, String fecha, String dstrct,
                                         String agc_duena, String agc_cobro, String prov, String cccli,
                                         String vto, String cfact, String hc, String lim1,
                                         String lim2, String nit_fiducia) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        this.vDocumentos = null;
        this.vDocumentos = new Vector();

        try {
            con = this.conectarJNDI("SQL_FAC_CORFICOLOMBIANA");//JJCastro fase2
            if(con!=null){
            String sql = this.obtenerSQL("SQL_FAC_CORFICOLOMBIANA");

            sql = cliente.length()!=0 ? sql.replaceAll("#1"," AND fra.codcli = '" + cliente + "'") : sql.replaceAll("#1", "");
            sql = agc.length()!=0 ? sql.replaceAll("#2"," AND fra.agencia_facturacion = '" + agc  + "'") : sql.replaceAll("#2", "");
            sql = agc_duena.length()!=0 ? sql.replaceAll("#3"," AND cli.agduenia = '" + agc_duena  + "'") : sql.replaceAll("#3", "");
            sql = agc_cobro.length()!=0 ? sql.replaceAll("#4"," AND fra.agencia_cobro = '" + agc_cobro  + "'") : sql.replaceAll("#4", "");
            sql = prov.length()!=0 ? sql.replaceAll("#5"," AND negocios.nit_tercero = '" + prov  + "'") : sql.replaceAll("#5", "");
            sql = cccli.length()!=0 ? sql.replaceAll("#6"," AND cli.nit = '" + cccli  + "'") : sql.replaceAll("#6", "");

            if(vto.equals("0")){
                sql=sql.replaceAll("#7", "");
            }else{
                if(vto.equals("1")){
                    sql=sql.replaceAll("#7", "AND (-1 * (fra.fecha_vencimiento - ?) )  > 0");
                }else{
                    if(vto.equals("2")){
                        sql=sql.replaceAll("#7", "AND (-1 * (fra.fecha_vencimiento - ?) )  <= 0");
                    }
                }
            }
            sql = !cfact.equals("...") ? sql.replaceAll("#8"," AND fra.documento like '" + cfact  + "%'") : sql.replaceAll("#8", " AND (fra.documento like 'FL%' OR fra.documento like 'FC%' OR fra.documento like 'FG%')");
            //sql = !cfact.equals("...") ? sql.replaceAll("#8"," AND fra.documento like '" + cfact  + "%'") : sql.replaceAll("#8", " AND (fra.documento like 'FL%' OR fra.documento like 'FC%')");
            sql = !hc.equals("...") ? sql.replaceAll("#9"," AND fra.cmc = '" + hc  + "'") : sql.replaceAll("#9", "");
            sql = lim1.length()!=0  ?sql.replaceAll("#A#"," AND -1 * (fra.fecha_vencimiento - date '"+fecha+"') between '"+lim1+"' and '"+lim2+"'  ") : sql.replaceAll("#A#", "");
            st = con.prepareStatement(sql);
            st.setString(1, fecha);
            st.setString(2, dstrct);
            st.setString(3, nit_fiducia);
            if(vto.equals("1")){
                   st.setString(4, fecha);
            }else{
                if(vto.equals("2")){
                   st.setString(4, fecha);
                }
            }
            System.out.println("Q--- "+st.toString());
            rs = st.executeQuery();

            while (rs.next()) {
                    RepGral obj = new RepGral();
                    obj.setAgcfacturacion(rs.getString("agfacturacion"));
                    obj.setCodagcfacturacion(rs.getString("codagfacturacion"));
                    obj.setCodcli(rs.getString("codcli"));
                    obj.setNomcli(rs.getString("nomcli"));
                    obj.setVlr_saldo_me(rs.getDouble("valor_saldome"));
                    obj.setVlr_saldo(rs.getDouble("valor_saldo"));
                    obj.setFecha_fra(rs.getString("fecha_factura"));
                    obj.setFecha_venc_fra(rs.getString("fecha_vencimiento"));
                    obj.setFactura(rs.getString("documento"));
                    obj.setMoneda(rs.getString("moneda_cia"));
                    obj.setNdias(rs.getInt("ndias"));
                    obj.setNitter(rs.getString("afiliado"));
                    obj.setDespla(rs.getString("desemb"));
                    obj.setRegistros(rs.getString("tasa"));
                    obj.setNitpro(rs.getString("ident"));
                    obj.setNom_costo_2(rs.getString("tel"));
                    obj.setDocu(rs.getString("docu"));
                    obj.setConcepto_desc(rs.getString("com"));
                    obj.setAgc_cobro(rs.getString("agencia_cobro"));
                    obj.setAgc_duenia(rs.getString("agduenia"));
                    obj.setNom_costo_4(rs.getString("obs"));
                    obj.setNom_costo_3(rs.getString("ndiasf"));
                    obj.setValor_factura(rs.getDouble("valor_factura"));
                    obj.setValor_facturame(rs.getDouble("valor_facturame"));
                    if( agc_cobro.equals("VA") ){
                        obj.setVlr_saldo(rs.getDouble("valor_saldome"));
                        obj.setMoneda(rs.getString("moneda"));
                        obj.setValor_factura(obj.getValor_facturame());
                    }
                    obj.setFecha_corte(fecha);
                    obj.setNumaval(rs.getString("aval"));
                    obj.setCodbco(rs.getString("bank"));
                    obj.setRefcl(rs.getString("ref"));
                    obj.setCmc(rs.getString("cmc"));
                    obj.setNegocio(rs.getString("negasoc"));
                    obj.setDescripcion_rxp(rs.getString("descripcion")); //contiene factura.descripcion
                    obj.setDocumento(rs.getString("convenio"));
                    obj.setTipo_documento(rs.getString("tipo_documento"));

                    this.vDocumentos.add(obj);
            }

            }}catch(SQLException e) {
                e.printStackTrace();
                throw new SQLException("ERROR EN [facturasCorficolombiana] " + e.getMessage()+"" + e.getErrorCode());
            }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }




/**
 *
 * @return
 * @throws SQLException
 */
    public Vector getVectorFactura() throws SQLException{
        Vector d = new Vector();
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_FACT_CORFI";
        try{

            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();

            while (rs.next()) {
                factura fac = new factura();
                fac.setNit(rs.getString("nit"));
                fac.setCodcli(rs.getString("codcli"));
                fac.setValor_factura(rs.getDouble("valor_factura"));
                fac.setFecha_factura(rs.getString("fecha_factura"));
                fac.setDescripcion(rs.getString("descripcion"));
                fac.setDocumento(rs.getString("documento"));
                fac.setValor_saldo(rs.getDouble("valor_saldo"));

                d.add(fac);
                fac = null;//Liberar Espacio JJCastro
            }
            }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return d;
    }






    public void inicializarNits(){
        nitsx=new ArrayList();
    }

     //Factura que se endoza a corficolombiana
    /**
     *
     * @param fac
     * @param documento
     * @param usuario
     * @return
     * @throws Exception
 */
    public ArrayList<String> ingresarNewFactura(factura fac, String documento, String usuario) throws Exception {
        Connection con = null;
        PreparedStatement ps = null; //Consultar la vieja factura
        StringStatement st = null; //JJCastro fase2
        ResultSet rs = null;
        ArrayList<String> sql =new ArrayList<>();
        String query = "SQL_FIND_FACT_AND_DETALLE";
        int tam=0;
        try{

            con = this.conectarJNDI(query);
            if(con!=null){
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, documento);
            rs = ps.executeQuery();

            while (rs.next()) {
                tam++;
                                    //Creacion de subledgers
                                    swNit=true;
                                    for (int i=0 ; i<nitsx.size();i++){
                                        if (((String)(nitsx.get(i))).equals(fac.getNit())){
                                            swNit=false;
                                            break;
                                        }
                                    }
                                    if (swNit){
                                        ArrayList<String> subledQuerys = subled(fac.getNit(), usuario);
                                        for(String getQuerys : subledQuerys) {
                                            sql.add(getQuerys);
                                        }
                                        nitsx.add(fac.getNit());
                                    }
                                    if(tam==1){
                                        st = new StringStatement(this.obtenerSQL("SQL_ADD_FACT"), true);//JJCastro fase2
                                        st.setString(1,"FINV");                                 //dstrct
                                        st.setString(2, fac.getDocumento());                    //documento
                                        st.setString(3, fac.getNit());                          //nit
                                        st.setString(4, fac.getCodcli());                       //codcli
                                        st.setString(5, rs.getString("concepto"));              //concepto
                                        st.setString(6, fac.getFecha_factura());                //fecha_factura
                                        st.setString(7, rs.getString("fecha_vencimiento"));     //fecha_vencimiento
                                        st.setString(8, fac.getDescripcion());                  //descripcion
                                        st.setString(9, rs.getString("observacion"));           //observacion
                                        st.setString(10,""+fac.getValor_factura());             //valor_factura
                                        st.setString(11,"0");        //valor_abono
                                        st.setString(12,""+fac.getValor_factura());        //valor_saldo
                                        st.setString(13,""+fac.getValor_factura());    //valor_facturame
                                        st.setString(14,""+fac.getValor_factura());      //valor_saldome
                                        st.setString(15,""+rs.getDouble("valor_tasa"));         //valor_tasa
                                        st.setString(16, rs.getString("moneda"));               //moneda
                                        st.setString(17,""+rs.getInt("cantidad_items"));        //cantidad_items
                                        st.setString(18, rs.getString("forma_pago"));           //forma_pago
                                        st.setString(19, rs.getString("agencia_facturacion"));  //agencia_facturacion
                                        st.setString(20, rs.getString("agencia_cobro"));        //agencia_cobro
                                        st.setString(21, rs.getString("base"));                 //base
                                        st.setString(22, usuario);                              //user_update
                                        st.setString(23, usuario);                              //creation_user
                                        st.setString(24, rs.getString("rif"));                  //rif
                                        st.setString(25, fac.getCmc());                         //cmc
                                        st.setString(26, rs.getString("formato"));              //formato
                                        st.setString(27, rs.getString("agencia_impresion"));    //agencia_impresion
                                        st.setString(28, "0");     //valor_abonome
                                        st.setString(29, rs.getString("negasoc"));              //negasoc
                                        st.setString(30, rs.getString("num_doc_fen"));          //num_doc_fen
                                        st.setString(31, rs.getString("obs"));                  //obs
                                        
                                        sql.add(st.getSql());//JJCastro fase2
                                    }

                                    //Insertar los detalles de la factura
                                    st = null;
                                    st = new StringStatement (this.obtenerSQL("SQL_INGRESAR_FACTURA_DETALLE"), true);//JJCastro fase2
                                    st.setString(1,"FINV");                                 //dstrct
                                    st.setString(2, fac.getDocumento());                    //documento
                                    st.setInt(3, rs.getInt("item"));                        //item
                                    st.setString(4, fac.getNit());                          //nit
                                    st.setString(5, rs.getString("concepto"));              //concepto
                                    st.setString(6, documento);                             //documento anterior
                                    st.setString(7, rs.getString("descripcion"));              //descripcion
                                    st.setString(8, fac.getCcuenta());                      //codigo_cuenta_contable
                                    st.setString(9, "RD-"+fac.getNit());                     //auxiliar
                                    st.setDouble(10, rs.getDouble("cantidad"));             //cantidad
                                    st.setString(11,""+ rs.getDouble("valor_unitario"));       //valor_unitario
                                    st.setString(12,""+rs.getDouble("valor_unitariome"));      //valor_unitariome
                                    st.setString(13, ""+rs.getDouble("valor_item"));           //valor_item
                                    st.setString(14,""+rs.getDouble("valor_itemme"));         //valor_itemme
                                    st.setString(15,""+rs.getDouble("valor_tasa"));         //valor_tasa
                                    st.setString(16, rs.getString("moneda"));               //moneda
                                    st.setString(17, rs.getString("base"));                 //base
                                    st.setString(18, usuario);                              //user_update
                                    st.setString(19, usuario);                              //creation_user
                                    sql.add(st.getSql());//JJCastro fase2
            }

        }}catch(Exception e){
            e.printStackTrace();
            throw new Exception("ERROR AL GRABAR LA FACTURA" + e.getMessage() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sql;
    }





        //Corficolombiana - add Subledgers
    /**
     *
     * @param nit
     * @param user
     * @return
     * @throws Exception
     */
    public ArrayList <String> subled(String nit, String user)throws Exception{

            StringStatement st = null;
            ArrayList <String> sql=new ArrayList<String>();
            String val="";
            String query = "SQL_SUBLED_CLI";
            try {
                st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
                val = ValSub("16252001", "RD", nit);

                if (val.equals("A")) {
                    st.setString(1, "16252001");
                    st.setString(2, nit);
                    st.setString(3, nit);
                    st.setString(4, user);

                    sql.add(st.getSql());//JJCastro fase2

                    st = null;
                    st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
                    st.setString(1, "16252002");
                    st.setString(2, nit);
                    st.setString(3, nit);
                    st.setString(4, user);
                    sql.add(st.getSql());//JJCastro fase2
                }
            }
            catch(SQLException e)
            {
                 throw new SQLException("ERROR DURANTE LAS INSERCION DE SUBLEDGERS " + e.getMessage() + " " + e.getErrorCode());
             }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
            return sql;
        }




    /**
     *
     * @param a
     * @param b
     * @param c
     * @return
     * @throws Exception
     */
       public String ValSub(String a,String b,String c ) throws Exception{
           Connection con = null;
           PreparedStatement ps = null;
            ResultSet rs = null;
            String  query = "SQL_VALIDAR_SUBLED";
            String res="A";
            try{
                con = this.conectarJNDI(query);
                if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, a);
                ps.setString(2, b);
                ps.setString(3, c);
                rs = ps.executeQuery();
                if (rs.next())
                {
                    res=rs.getString(1);
                }
            }
            }catch (Exception ex)
            {
                ex.printStackTrace();
                throw new Exception(ex.getMessage());
            }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
            return res;
     }

     public java.util.TreeMap getPrefijosCxcs() throws Exception    {//20100628
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String  query = "SQL_PREFIJOS_CXCS";
        TreeMap prefijoscxcs = new TreeMap();
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = ps.executeQuery();
                while (rs.next())                {
                    prefijoscxcs.put(rs.getString(1) , rs.getString(1));
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
            System.out.println("error en getPrefijosCxcs:"+ex.toString());
            throw new Exception(ex.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return prefijoscxcs;
    }

     public java.util.TreeMap getHcs() throws Exception    {//20100628
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String  query = "SQL_HCS";
        TreeMap hcs = new TreeMap();
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = ps.executeQuery();
                while (rs.next())                {
                    hcs.put(rs.getString(1) , rs.getString(2));//20100630
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
            System.out.println("error en getHcs:"+ex.toString());
            throw new Exception(ex.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return hcs;
    }
     
/**
  *
  * @param cliente
  * @param fecha
  * @param dstrct
  * @param prov
  * @param cccli
  * @param vto
  * @param cfact
  * @param hc
  * @param lim1
  * @param lim2
  * @throws SQLException
  */
public void facturasEndoso( String cliente,String fecha, String dstrct, String prov, String cccli, String cfact,  String lim1,
                                         String lim2, String factura, String tercero) throws SQLException {

        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        this.vDocumentos = null;
        this.vDocumentos = new Vector();

        try {
            con = this.conectarJNDI("SQL_FAC_ENDOSO");//JJCastro fase2
            if(con!=null){
            String sql = this.obtenerSQL("SQL_FAC_ENDOSO");

            sql = cliente.length()!=0 ? sql.replaceAll("#1"," AND fra.codcli = '" + cliente + "'") : sql.replaceAll("#1", "");
            sql = prov.length()!=0 ? sql.replaceAll("#2"," AND negocios.nit_tercero = '" + prov  + "'") : sql.replaceAll("#2", "");
            sql = cccli.length()!=0 ? sql.replaceAll("#3"," AND cli.nit = '" + cccli  + "'") : sql.replaceAll("#3", "");
            sql = !cfact.equals("...") ? sql.replaceAll("#4"," AND fra.documento like '" + cfact  + "%'") : sql.replaceAll("#4", "");
            sql = factura.length()!=0  ? sql.replaceAll("#5"," AND fra.documento = '" + factura.toUpperCase()  + "'") : sql.replaceAll("#5", "");
            sql = fecha.length()!=0  ? sql.replaceAll("#6"," AND fra.fecha_vencimiento = '" + fecha  + "'") : sql.replaceAll("#6", "");
            sql = !tercero.equals("") ? sql.replaceAll("#7"," AND id_redescuento = '"+tercero+"'") : sql.replaceAll("#7", "");
            sql = lim1.length()!=0  ?sql.replaceAll("#A#"," AND -1 * (fra.fecha_vencimiento - to_date(now(),'YYYY-MM-DD')) between '"+lim1+"' and '"+lim2+"'  ") : sql.replaceAll("#A#", "");
            st = con.prepareStatement(sql);
            st.setString(1, dstrct);


            //ystem.out.println("Q--- "+st.toString());
            rs = st.executeQuery();

            while (rs.next()) {
                    RepGral obj = new RepGral();
                    obj.setCodcli(rs.getString("codcli"));
                    obj.setNomcli(rs.getString("nomcli"));
                    obj.setVlr_saldo(rs.getDouble("valor_saldo"));
                    obj.setFecha_fra(rs.getString("fecha_factura"));
                    obj.setFecha_venc_fra(rs.getString("fecha_vencimiento"));
                    obj.setFactura(rs.getString("documento"));
                    obj.setNitter(rs.getString("afiliado"));
                    obj.setNitpro(rs.getString("ident"));
                    obj.setDocu(rs.getString("docu"));
                    obj.setValor_factura(rs.getDouble("valor_factura"));
                    obj.setNumaval(rs.getString("aval"));
                    obj.setNegocio(rs.getString("negasoc"));
                    obj.setDescripcion_rxp(rs.getString("descripcion")); //contiene factura.descripcion
                    obj.setDocumento(rs.getString("convenio"));

                    this.vDocumentos.add(obj);
            }

            }}catch(SQLException e) {
                e.printStackTrace();
                throw new SQLException("ERROR EN [facturasEndoso] " + e.getMessage()+"" + e.getErrorCode());
            }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    public String cargarListas(String tipo, String filtro) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = tipo.equals("convenios")?"SQL_convenios":"SQL_unegocios";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                query = this.obtenerSQL(query);
                query = query.replace("filtro",(filtro != null && !filtro.equals(""))
                                     ?"and ruc.id_unid_negocio in ("+filtro+")":"");
                ps = con.prepareStatement(query);
                rs = ps.executeQuery();
                while (rs.next()) {
                    JsonObject element = new JsonObject();
                    element.addProperty("id", rs.getString("id"));
                    element.addProperty("nombre", rs.getString("nombre"));
                    element.addProperty("padre", rs.getString("padre"));
                    obj.add(rs.getString("id"), element);
                } 
            }query = gson.toJson(obj);
        } catch( SQLException exc) {
            query = "{}";
        } finally {
            return query;
        }
    }

}
