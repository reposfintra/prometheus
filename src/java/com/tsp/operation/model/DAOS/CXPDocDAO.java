/***********************************************************************************
 * Nombre clase : ............... CXPDocDao.java                                   *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.                                       *
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                     *
 * Fecha :....................... 8 de octubre de 2005, 11:02 AM                   *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/
package com.tsp.operation.model.DAOS;

import com.tsp.opav.model.services.ClientesVerService;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
//import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.Util;
import javax.swing.*;
import java.text.*;
import java.util.Date;

public class CXPDocDAO extends MainDAO {

    public CXPDocDAO() {
        super("CXPDocDAO.xml");
    }
    public CXPDocDAO(String dataBaseName) {
        super("CXPDocDAO.xml", dataBaseName);
    }
    private Vector vecCxp_doc;
    private Vector info;
    private CXP_Doc factura = new CXP_Doc();
    private List facturas;
    //Modificado por sandra 17-03-2006
    //Ivan Dario gomez
    private static final String UPT_NUM_OBS_AUTORIZADOR = "Update fin.cxp_doc "
            + "     set num_obs_autorizador=? "
            + "where "
            + "    dstrct=? and"
            + "    proveedor=? and"
            + "    tipo_documento=? and"
            + "    documento=?";
    private static final String NUM_OBS_AUTORIZADOR = "Select "
            + "     max(num_obs_autorizador) as num "
            + "from"
            + "    fin.cxp_doc "
            + "where "
            + "    dstrct=? and"
            + "    proveedor=? and"
            + "    tipo_documento=? and"
            + "    documento=?";
    private static final String SQL_BUSC_FACTX_USUARIO =
            "SELECT "
            + "      C.dstrct,"
            + "      C.proveedor,"
            + "      N.nombre as nomprov,"
            + "      tipo_documento,"
            + "      T.document_name, "
            + "      documento, "
            + "      fecha_documento, "
            + "      fecha_vencimiento, "
            + "      descripcion, "
            + "      vlr_saldo,"
            + "      nommoneda as moneda,"
            + "      A.nombre as agencia,"
            + "      C.creation_user, "
            + "      C.creation_date-fecha_documento as dias, "
            + "      num_obs_autorizador, "
            + "      num_obs_pagador,"
            + "      num_obs_registra "
            + "from "
            + "      cxp_doc C, "
            + "      agencia A, "
            + "      Monedas M, "
            + "      Nit N,"
            + "      tbldoc T "
            + "where "
            + "      C.reg_status<>'A' and"
            + "      usuario_aprobacion = ? and"
            + "      T.document_type=C.tipo_documento and "
            + "      C.agencia=A.id_agencia and "
            + "      M.codmoneda=c.moneda and "
            + "      N.cedula=C.proveedor and "
            + "      fecha_aprobacion='0099-01-01 00:00:00'";
    private static final String AUTORIZAR_PAGO = "Update fin.cxp_doc"
            + "      set usuario_aprobacion=?, "
            + "      fecha_aprobacion=? "
            + "where "
            + "      dstrct=? and"
            + "      proveedor=? and"
            + "      tipo_documento=? and"
            + "      documento=?";
    //David
    private static final String LISTAPROVEEDORES = "SELECT proveedor FROM cxp_doc GROUP BY proveedor";
    private static final String NOMBREDOC = "SELECT document_name FROM tbldoc WHERE document_type=?";
    private static final String LISTADOC = "SELECT * FROM cxp_doc WHERE proveedor=?";
    private static final String LISTADOC1 = "SELECT * FROM cxp_doc WHERE proveedor=? AND banco=? AND sucursal=?";
    private static final String BVALOR = " SELECT (1/vlr_conver) as valor FROM   tasa WHERE  fecha =? AND moneda1 = ?  AND moneda2 = ?";
    private static final String NUMERO_F_POR_PROVEEDOR = "SELECT documento FROM cxp_doc WHERE proveedor=? and vlr_neto_me > 0";
    //jose 26/12/2005
    private static final String SQL_CAMPOS_PLANILLA = "SELECT DISTINCT "
            + "p.nitpro, pro.id_mims, pro.hc, pro.branch_code, pro.bank_account_no, b.currency, u.id_agencia "
            + "FROM planilla p "
            + "left join proveedor pro on ( pro.nit = p.nitpro) "
            + "left join banco b on ( pro.branch_code = b.branch_code AND pro.bank_account_no = b.bank_account_no AND pro.dstrct = b.dstrct) "
            + "left join  usuarios u on ( u.idusuario = p.despachador) "
            + "WHERE p.numpla = ? "
            + "GROUP BY p.nitpro, pro.id_mims, pro.hc, pro.branch_code, pro.bank_account_no, b.currency, u.id_agencia limit 1";
    private static final String SQL_INSERT_CXP_DOC = "INSERT INTO cxp_doc "
            + "(dstrct, proveedor, tipo_documento, documento, descripcion, agencia, handle_code, id_mims, fecha_documento, usuario_aprobacion, fecha_vencimiento, "
            + "banco, sucursal, moneda, vlr_neto, vlr_saldo, usuario_contabilizo, fecha_contabilizacion, observacion, creation_date, creation_user, base) "
            + "VALUES (?,?,'038',?,?,?,?,?,'now()',?,'now()',?,?,?,?,?,?,'now()',?,'now()',?,?)";

    /**
     * Metodo que instancia un Objeto tipo CXDOC en CXPDocDAO
     * @autor.......David Lamadrid
     * @param.......CXPDoc factura(objeto de CXPDoc)
     * @see.........CXPDocDAO.class
     * @throws......Clase no encontrada.
     * @version.....1.0.
     * @return.......
     */
    public void setFactura(com.tsp.operation.model.beans.CXP_Doc factura) {
        this.factura = factura;
    }

    /* ANULAR FACTURA */
    /**
     * Metodo:          ExisteCXP_Items_Doc
     * Descriocion :    Funcion publica que nos dice si existe un documento en cuentas por pagar
     * @autor :         LREALES
     * @param :         distrito, proveedor, tipo de documento y documento
     * @return:         boo ( retorna un boolean )
     * @version :       1.0
     */
    public boolean ExisteCXP_Items_Doc(String distrito, String proveedor, String tipo_documento, String documento) throws SQLException {
        Connection con = null;//JJCastro fase2
        CXP_Doc cXP_Doc = new CXP_Doc();
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean boo = false;
        String query = "SQL_ConsultarCXP_Items_Doc";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2

                st.setString(1, distrito);
                st.setString(2, proveedor);
                st.setString(3, tipo_documento);
                st.setString(4, documento);

                rs = st.executeQuery();

                if (rs.next()) {
                    boo = true;
                }

            }
        } catch (SQLException e) {

            throw new SQLException("ERROR DURANTE ExisteCXP_Items_Doc.. " + e.getMessage() + " " + e.getErrorCode());

        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return boo;

    }

    /**
     * Metodo:          ExisteCXP_Imp_Doc
     * Descriocion :    Funcion publica que nos dice si existe un documento en cuentas por pagar
     * @autor :         LREALES
     * @param :         distrito, proveedor, tipo de documento y documento
     * @return:         boo ( retorna un boolean )
     * @version :       1.0
     */
    public boolean ExisteCXP_Imp_Doc(String distrito, String proveedor, String tipo_documento, String documento) throws SQLException {
        Connection con = null;
        CXP_Doc cXP_Doc = new CXP_Doc();
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean boo = false;
        String query = "SQL_ConsultarCXP_Imp_Doc";//JJCastro fase2

        try {

            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito);
                st.setString(2, proveedor);
                st.setString(3, tipo_documento);
                st.setString(4, documento);
                rs = st.executeQuery();

                if (rs.next()) {
                    boo = true;
                }

            }
        } catch (SQLException e) {

            throw new SQLException("ERROR DURANTE ExisteCXP_Imp_Doc.. " + e.getMessage() + " " + e.getErrorCode());

        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return boo;

    }

    /**
     * Metodo:          ExisteCXP_Imp_Item
     * Descriocion :    Funcion publica que nos dice si existe un documento en cuentas por pagar
     * @autor :         LREALES
     * @param :         distrito, proveedor, tipo de documento y documento
     * @return:         boo ( retorna un boolean )
     * @version :       1.0
     */
    public boolean ExisteCXP_Imp_Item(String distrito, String proveedor, String tipo_documento, String documento) throws SQLException {
        Connection con = null;
        CXP_Doc cXP_Doc = new CXP_Doc();
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean boo = false;
        String query = "SQL_ConsultarCXP_Imp_Item";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito);
                st.setString(2, proveedor);
                st.setString(3, tipo_documento);
                st.setString(4, documento);

                rs = st.executeQuery();

                if (rs.next()) {
                    boo = true;
                }

            }
        } catch (SQLException e) {

            throw new SQLException("ERROR DURANTE ExisteCXP_Imp_Item.. " + e.getMessage() + " " + e.getErrorCode());

        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return boo;

    }

    /**
     * Metodo:          AnularCXP_Items_Doc
     * Descriocion :    Funcion publica que nos coloca un documento de cuentas por pagar en estado 'ANULADO'
     * @autor :         LREALES
     * @param :         cXP_Doc ( documento de cuentas por pagar )
     * @return:         nada.
     * @version :       1.0
     */
    public void AnularCXP_Items_Doc(CXP_Doc cXP_Doc) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_AnularCXP_Items_Doc";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cXP_Doc.getUser_update());
                st.setString(2, cXP_Doc.getDstrct());
                st.setString(3, cXP_Doc.getProveedor());
                st.setString(4, cXP_Doc.getTipo_documento());
                st.setString(5, cXP_Doc.getDocumento());

                st.executeUpdate();

            }
        } catch (SQLException e) {

            throw new SQLException("ERROR DURANTE AnularCXP_Items_Doc.. " + e.getMessage() + "" + e.getErrorCode());

        } finally {//JJCastro fase2
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

    }

    /**
     * Metodo:          AnularCXP_Imp_Doc
     * Descriocion :    Funcion publica que nos coloca un documento de cuentas por pagar en estado 'ANULADO'
     * @autor :         LREALES
     * @param :         cXP_Doc ( documento de cuentas por pagar )
     * @return:         nada.
     * @version :       1.0
     */
    public void AnularCXP_Imp_Doc(CXP_Doc cXP_Doc) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_AnularCXP_Imp_Doc";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cXP_Doc.getUser_update());
                st.setString(2, cXP_Doc.getDstrct());
                st.setString(3, cXP_Doc.getProveedor());
                st.setString(4, cXP_Doc.getTipo_documento());
                st.setString(5, cXP_Doc.getDocumento());

                st.executeUpdate();

            }
        } catch (SQLException e) {

            throw new SQLException("ERROR DURANTE AnularCXP_Imp_Doc.. " + e.getMessage() + "" + e.getErrorCode());

        } finally {//JJCastro fase2
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

    }

    /**
     * Metodo:          AnularCXP_Imp_Item
     * Descriocion :    Funcion publica que nos coloca un documento de cuentas por pagar en estado 'ANULADO'
     * @autor :         LREALES
     * @param :         cXP_Doc ( documento de cuentas por pagar )
     * @return:         nada.
     * @version :       1.0
     */
    public void AnularCXP_Imp_Item(CXP_Doc cXP_Doc) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_AnularCXP_Imp_Item";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cXP_Doc.getUser_update());
                st.setString(2, cXP_Doc.getDstrct());
                st.setString(3, cXP_Doc.getProveedor());
                st.setString(4, cXP_Doc.getTipo_documento());
                st.setString(5, cXP_Doc.getDocumento());

                st.executeUpdate();

            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE AnularCXP_Imp_Item.. " + e.getMessage() + "" + e.getErrorCode());
        } finally {//JJCastro fase2
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

    }
    /*FIN ANULAR*/

    /**
     * Metodo que retorna un boolean para verificar la existencia de un Documento por pagar en el sistema
     * retorna true si existe y false si no existe.
     * @autor.......David Lamadrid
     * @param.......String dis(Distrito),String proveedor,String tipoDoc,String docuemnto.
     * @see.........CXP_Doc.class
     * @throws.......
     * @version.....1.0.
     * @return......boolean.
     */
    public boolean existeDoc(String dis, String proveedor, String tipoDoc, String documento) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "SQL_EXISTEDOC";//JJCastro fase2

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dis);
                st.setString(2, proveedor);
                st.setString(3, tipoDoc);
                st.setString(4, documento);
                rs = st.executeQuery();

                while (rs.next()) {
                    sw = true;
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL ENCONTRAR DOCUMENTO " + e.getMessage() + "" + e.getErrorCode());
        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sw;
    }

    /**
     * Metodo que Actualiza los datos de la factura .
     * @autor.......Ivan Gomez
     * @param.......CXDoc doc(Objeto tipo CXPDoc), Vector vItems(Vector de objetos CxPItem),Vector vImpuestos(Vector de Objetos CXPImpDoc).
     * @throws......Existe Documento en el Sistema.
     * @version.....1.0.
     * @return.......
     */
    public void UpdateFactura(CXP_Doc doc, Vector vItems, Vector vImpuestosDoc, String agencia) throws Exception {
        Connection con = null;
        Statement st = null;
        StringStatement ps = null;
        ResultSet rs = null;
        String sql = "";
        String SQL_INSERTAROCUMENTO = "";
        String SQL_INSERTARITEMDOCUMENTO = "";
        String SQL_INSERTARIMPPORITEM = "";
        String SQL_INSERTARIMPDOC = "";
        String SQL_UPDATE_NOTA_DEBITO = "";
        String SQL_UPDATE_NOTA_CREDITO = "";
        try {
            con = this.conectarJNDI("SQL_INSERTAROCUMENTO");//JJCastro fase2
            con.setAutoCommit(false);
            st = con.createStatement();
            SQL_INSERTAROCUMENTO = this.obtenerSQL("SQL_INSERTAROCUMENTO");
            SQL_INSERTARITEMDOCUMENTO = this.obtenerSQL("SQL_INSERTARITEMDOCUMENTO");
            SQL_INSERTARIMPPORITEM = this.obtenerSQL("SQL_INSERTARIMPPORITEM");
            SQL_INSERTARIMPDOC = this.obtenerSQL("SQL_INSERTARIMPDOC");

            //Ivan NOTA DEBITO
            SQL_UPDATE_NOTA_DEBITO = this.obtenerSQL("SQL_UPDATE_NOTA_DEBITO");
            SQL_UPDATE_NOTA_CREDITO = this.obtenerSQL("SQL_UPDATE_NOTA_CREDITO");

            sql = this.obtenerSQL("SQL_DELETE_FACTURA").replace("#DISTRITO#", doc.getDstrct()).replace("#PROVEEDOR#", doc.getProveedor()).replace("#DOC#", doc.getDocumento()).replaceAll("#TIPDOC#", doc.getTipo_documento());
            ps = new StringStatement(sql);//JJCastro fase2
            String[] vectorSql=ps.getSql().split(";");
            for (int i = 0; i < vectorSql.length; i++) {
                st.addBatch(vectorSql[i]);
            }
     

            ps = new StringStatement(SQL_INSERTAROCUMENTO, true);//JJCastro fase2
            ps.setString(1, doc.getDstrct());
            ps.setString(2, doc.getProveedor());
            ps.setString(3, doc.getTipo_documento());
            ps.setString(4, doc.getDocumento());
            ps.setString(5, doc.getDescripcion());
            ps.setString(6, agencia);
            ps.setString(7, doc.getHandle_code());
            ps.setString(8, doc.getId_mims());
            ps.setString(9, doc.getFecha_documento());
            ps.setString(10, doc.getTipo_documento_rel());
            ps.setString(11, doc.getDocumento_relacionado());
            ps.setString(12, doc.getFecha_aprobacion());
            ps.setString(13, doc.getAprobador());
            ps.setString(14, doc.getUsuario_aprobacion());
            ps.setString(15, doc.getFecha_vencimiento());
            ps.setString(16, doc.getUltima_fecha_pago());
            ps.setString(17, doc.getBanco());
            ps.setString(18, doc.getSucursal());
            ps.setString(19, doc.getMoneda());
            ps.setDouble(20, doc.getVlr_neto());
            ps.setDouble(21, doc.getVlr_total_abonos());
            ps.setDouble(22, doc.getVlr_saldo());
            ps.setDouble(23, doc.getVlr_neto_me());
            ps.setDouble(24, doc.getVlr_total_abonos_me());
            ps.setDouble(25, doc.getVlr_saldo_me());
            ps.setDouble(26, doc.getTasa());
            ps.setString(27, doc.getUsuario_contabilizo());
            ps.setString(28, doc.getFecha_contabilizacion());
            ps.setString(29, doc.getUsuario_anulo());
            ps.setString(30, doc.getFecha_anulacion());
            ps.setString(31, doc.getFecha_contabilizacion_anulacion());
            ps.setString(32, doc.getObservacion());
            ps.setDouble(33, doc.getNum_obs_autorizador());
            ps.setDouble(34, doc.getNum_obs_pagador());
            ps.setDouble(35, doc.getNum_obs_registra());
            ps.setString(36, doc.getCreation_user());
            ps.setString(37, doc.getUser_update());
            ps.setString(38, doc.getBase());
            ps.setString(39, doc.getMoneda_banco());
            ps.setString(40, doc.getClase_documento_rel());
            ps.setString(41, doc.getTipo_nomina());
            ps.setString(42, doc.getTipodoc());
            ps.setString(43, doc.getMultiservicio());
            
            st.addBatch(ps.getSql());//JJCastro fase2

            double saldo = doc.getVlr_saldo() - doc.getValor_saldo_anterior();
            double saldo_me = doc.getVlr_saldo_me() - doc.getValor_saldo_me_anterior();

            if (doc.getTipo_documento().equals("ND")) {
                ps = new StringStatement(SQL_UPDATE_NOTA_DEBITO, true);//JJCastro fase2
                ps.setDouble(1, saldo);
                ps.setDouble(2, saldo_me);
                ps.setDouble(3, saldo);
                ps.setDouble(4, saldo_me);

                ps.setString(5, doc.getDstrct());
                ps.setString(6, doc.getProveedor());
                ps.setString(7, doc.getTipo_documento_rel());
                ps.setString(8, doc.getDocumento_relacionado());
                //System.out.println("NOTA DEBITO --->"+ ps.toString());
                st.addBatch(ps.getSql());//JJCastro fase2

            } else if (doc.getTipo_documento().equals("NC")) {
                ps = new StringStatement(SQL_UPDATE_NOTA_CREDITO, true);//JJCastro fase2
                ps.setDouble(1, saldo);
                ps.setDouble(2, saldo_me);
                ps.setDouble(3, saldo);
                ps.setDouble(4, saldo_me);

                ps.setString(5, doc.getDstrct());
                ps.setString(6, doc.getProveedor());
                ps.setString(7, doc.getTipo_documento_rel());
                ps.setString(8, doc.getDocumento_relacionado());
                //System.out.println("NOTA CREDITO --->"+ ps.toString());
                st.addBatch(ps.getSql());//JJCastro fase2
            }

            for (int i = 0; i < vItems.size(); i++) {
                CXPItemDoc item = (CXPItemDoc) vItems.elementAt(i);
                if (item.getDescripcion() != null) {
                    ps = new StringStatement(SQL_INSERTARITEMDOCUMENTO, true);//JJCastro fase2
                    ps.setString(1, item.getDstrct());
                    ps.setString(2, item.getProveedor());
                    ps.setString(3, item.getTipo_documento());
                    ps.setString(4, item.getDocumento());
                    ps.setString(5, item.getItem());
                    ps.setString(6, item.getDescripcion());
                    ps.setDouble(7, item.getVlr());
                    ps.setDouble(8, item.getVlr_me());
                    ps.setString(9, item.getCodigo_cuenta());
                    ps.setString(10, item.getCodigo_abc());
                    ps.setString(11, item.getPlanilla());
                    ps.setString(12, item.getUser_update());
                    ps.setString(13, item.getCreation_user());
                    ps.setString(14, item.getBase());
                    ps.setString(15, item.getCodcliarea());
                    ps.setString(16, item.getTipcliarea());
                    ps.setString(17, item.getConcepto());
                    if (item.getTipoSubledger().equals("")) {
                        ps.setString(18, item.getAuxiliar());
                    } else {
                        ps.setString(18, item.getTipoSubledger() + "-" + item.getAuxiliar());
                    }
                    //System.out.println("CONSULTA 2 " + ps);
                    st.addBatch(ps.getSql());//JJCastro fase2


                    Vector vImpDoc = item.getVItems();
                    for (int ii = 0; ii < vImpDoc.size(); ii++) {
                        CXPImpItem impItem = (CXPImpItem) vImpDoc.elementAt(ii);
                        ps = new StringStatement(SQL_INSERTARIMPPORITEM, true);//JJCastro fase2
                        ps.setString(1, impItem.getDstrct());
                        ps.setString(2, impItem.getProveedor());
                        ps.setString(3, impItem.getTipo_documento());
                        ps.setString(4, impItem.getDocumento());
                        ps.setString(5, impItem.getItem());
                        ps.setString(6, impItem.getCod_impuesto());
                        ps.setDouble(7, impItem.getPorcent_impuesto());
                        ps.setDouble(8, impItem.getVlr_total_impuesto());
                        ps.setDouble(9, impItem.getVlr_total_impuesto_me());
                        ps.setString(10, impItem.getCreation_user());
                        ps.setString(11, impItem.getUser_update());
                        ps.setString(12, impItem.getBase());
                        //System.out.println("CONSULTA 3 " + ps);
                        st.addBatch(ps.getSql());//JJCastro fase2
                    }
                }
            }

            for (int i = 0; i < vImpuestosDoc.size(); i++) {
                CXPImpDoc impDoc = (CXPImpDoc) vImpuestosDoc.elementAt(i);
                ps = new StringStatement(SQL_INSERTARIMPDOC, true);//JJCastro fase2
                ps.setString(1, impDoc.getDstrct());
                ps.setString(2, impDoc.getProveedor());
                ps.setString(3, impDoc.getTipo_documento());
                ps.setString(4, impDoc.getDocumento());
                ps.setString(5, impDoc.getCod_impuesto());
                ps.setDouble(6, impDoc.getPorcent_impuesto());
                ps.setDouble(7, impDoc.getVlr_total_impuesto());
                ps.setDouble(8, impDoc.getVlr_total_impuesto_me());
                ps.setString(9, impDoc.getCreation_user());
                ps.setString(10, impDoc.getUser_update());
                ps.setString(11, impDoc.getBase());
                //System.out.println("CONSULTA 4 " + ps);
                st.addBatch(ps.getSql());//JJCastro fase2
            }

            st.executeBatch();

            con.commit();
            con.setAutoCommit(true);

        } catch (Exception e) {
            e.printStackTrace();
            if (con != null) {
                con.rollback();
            }
            throw new SQLException("Error en rutina UpdateFactura  [insertarCXPDoc].... \n" + e.getMessage());
        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

    }

    /**
     * Metodo: obtenerNumeroFXP , obtiene un Vector con los Numero de facturas no canceladas a un proveedor
     * @autor : Ing. David Lamadrid
     * @param : String proveedor.
     * @version : 1.0
     */
    public void obtenerNumeroFXP(String proveedor) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_NUMERO_F_POR_PROVEEDOR";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, proveedor);
                rs = st.executeQuery();
                this.vFacturas = new Vector();
                while (rs.next()) {
                    vFacturas.add(rs.getString("documento"));
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE LA LISTA DE lOS CAMPOS DE LAS PLANILLAS " + e.getMessage() + " " + e.getErrorCode());
        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Metodo que genera un Vector de Objetos tipo CXPImpDoc y lo retorna.
     * @autor.......David Lamadrid
     * @param.......Vector de Objetos tipo CXPImpDoc
     * @see........ CXPImpDoc.class
     * @throws.......
     * @version.....1.0.
     * @return......Vector vImpuestos.
     */
    public Vector generarVImpuestosDoc(Vector vImpuestos) {
        Vector resultado = new Vector();
        for (int i = 0; i < vImpuestos.size(); i++) {
            CXPImpDoc impDoc = (CXPImpDoc) vImpuestos.elementAt(i);
            String cod_impuesto = impDoc.getCod_impuesto();
            double valor = impDoc.getVlr_total_impuesto();
            double valor_me = impDoc.getVlr_total_impuesto_me();
            int ban = 0;
            for (int x = 0; x < resultado.size(); x++) {
                CXPImpDoc impDoc1 = (CXPImpDoc) resultado.elementAt(x);
                String cod_impuesto1 = impDoc1.getCod_impuesto();
                double valor1 = impDoc1.getVlr_total_impuesto();
                double valor_me1 = impDoc1.getVlr_total_impuesto_me();
                if (cod_impuesto.equals(cod_impuesto1)) {
                    valor1 = valor1 + valor;
                    valor_me1 = valor_me1 + valor_me;
                    impDoc1.setVlr_total_impuesto(valor1);
                    impDoc1.setVlr_total_impuesto_me(valor_me1);
                    resultado.setElementAt(impDoc1, x);
                    ban = 1;
                }
            }
            if (ban == 0) {
                resultado.add(impDoc);
            }
        }
        return resultado;
    }

    /**
     *
     *
     *
     * /**
     * Metodo actualizarNumeroObsAutorizador, Actualiza el numero de la observaci�n del
     * Pagador o Registrador* de acuerdo a la opcion que recibe por parametro y el usuario
     * a actualizar.
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : String opcion, String usuario, String distrito, String proveedor, String tipo de documento, String documento
     * @version : 1.0
     */
    public void actualizarNumeroObsPagadorRegistrador(String opcion, String usuario, String dstrct, String proveedor, String tipo_documento, String documento) throws SQLException {
        int num = 0;
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String usu = "";
        String query1 = "SQL_MAX_NUM_OBS_PAGADOR"; //JJCastro fase2
        String query2 = "UPDATE_CXP_DOC"; //JJCastro fase2


        if (usuario.equals("PAGADOR")) {
            usu = "num_obs_pagador";
        } else {
            usu = "num_obs_registra";
        }
        try {

            con = this.conectarJNDI(query1); //JJCastro fase2
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query1));//JJCastro fase2

                st.setString(1, dstrct);
                st.setString(2, proveedor);
                st.setString(3, tipo_documento);
                st.setString(4, documento);
                rs = st.executeQuery();
                if (rs.next()) {
                    num = rs.getInt("num");
                }
                st = null;
                st = con.prepareStatement(this.obtenerSQL(query2));//JJCastro fase2
                if (opcion.equals("SUMAR")) {
                    st.setInt(1, num + 1);
                } else {
                    st.setInt(1, num - 1);
                }
                st.setString(2, dstrct);
                st.setString(3, proveedor);
                st.setString(4, tipo_documento);
                st.setString(5, documento);
                st.execute();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR AL ACTUALIZAR NUMERO DE OBS PAGADOR REGISTRADOR" + e.getMessage() + "" + e.getErrorCode());
        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Metodo autorizarCuentaPorPagar, permite autorizar el pago de la factura que
     * recibe por parametro
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : CXP_Doc la factura a autorizar
     * @version : 1.0
     */
    public void autorizarCuentaPorPagar(CXP_Doc cuenta) throws SQLException {
        int num = 0;
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_AUTORIZAR_PAGO";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2 
                st.setString(1, cuenta.getUsuario_aprobacion());
                st.setString(2, cuenta.getFecha_aprobacion());
                st.setString(3, cuenta.getDstrct());
                st.setString(4, cuenta.getProveedor());
                st.setString(5, cuenta.getTipo_documento());
                st.setString(6, cuenta.getDocumento());
                st.execute();
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL Autorizar pago " + e.getMessage() + "" + e.getErrorCode());
        } finally {//JJCastro fase2
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Metodo vectorFacturas. Retorna el vector con las facturas que va autorizar el usuario en session.
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @version : 1.0
     */
    public Vector vectorFacturas() {
        return vecCxp_doc;
    }

    /**
     * Metodo reporteFacturasXLS, obtiene las facturas creadas segun parametros ingresados
     * @autor : Ing. Jose de la Rosa
     * @modificado: Ing Sandra Escalante
     * @param : distrito y agencia del usuario en sesion (String), fecha inicial y final
     *          de realizacion del proceso
     * @return List
     * @version : 2.0
     */
    public void reporteFacturasXLS(String dstrct, String agencia, String fchi, String fchf) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_FACTURAS_XLS";
        facturas = null;
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dstrct);
                st.setString(2, agencia);
                st.setString(3, fchi + " 00:00:00");
                st.setString(4, fchf + " 23:59:59");
                rs = st.executeQuery();
                facturas = new LinkedList();
                while (rs.next()) {
                    CXP_Doc fac = new CXP_Doc();
                    fac.setProveedor(rs.getString("proveedor"));
                    fac.setHandle_code(rs.getString("handle_code"));
                    fac.setId_mims(rs.getString("id_mims"));
                    fac.setNomProveedor(rs.getString("nompro"));
                    fac.setTipo_documento(rs.getString("tipdoc"));
                    fac.setDocumento(rs.getString("nrodoc"));
                    fac.setFecha_documento(rs.getString("fechdoc"));
                    fac.setTipo_documento_rel(rs.getString("tipdocrel"));
                    fac.setDocumento_relacionado(rs.getString("docrel"));
                    fac.setUsuario_aprobacion(rs.getString("usuapro"));
                    fac.setBanco(rs.getString("banco"));
                    fac.setSucursal(rs.getString("sucursal"));
                    fac.setVlr_neto(rs.getDouble("vlr_neto"));
                    fac.setVlr_neto_me(rs.getDouble("vlr_neto_me"));
                    fac.setMoneda(rs.getString("moneda"));
                    fac.setTasa(rs.getFloat("tasa"));
                    fac.setCreation_user(rs.getString("ucreacion"));
                    fac.setUcagencia(rs.getString("ucagencia"));
                    facturas.add(fac);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    public List getFacturas() {

        return this.facturas;
    }

    /**
     * Metodo:          ConsultarCXP_Doc
     * Descriocion :    Funcion publica que nos arroja la informacion de un documento en cuentas por pagar
     * @autor :         Leonardo Parody
     * @modificado por: LREALES - mayo del 2006
     * @param :         distrito y documento
     * @return:         cXP_Doc ( retorna un Objeto )
     * @version :       1.0
     */
    public CXP_Doc ConsultarCXP_Doc(String documento, String distrito) throws SQLException {

        Connection con = null;
        CXP_Doc cXP_Doc = new CXP_Doc();
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ConsultarCXP_Doc";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2

                st.setString(1, documento);
                st.setString(2, distrito);
                rs = st.executeQuery();

                while (rs.next()) {
                    cXP_Doc = cXP_Doc.loadCXP_Doc(rs);
                    cXP_Doc.setMoneda_banco(rs.getString("moneda_banco"));
                }

            }
        } catch (SQLException e) {

            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL DOCUMENTO DE CUENTAS POR PAGAR " + e.getMessage() + " " + e.getErrorCode());

        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return cXP_Doc;

    }

    /**
     * Metodo que retorna un Vector de objetos CXPDoc ordenados por proveedor.
     * @autor.......David Lamadrid
     * @param.......ninguno.
     * @see.........CXP_Doc.class
     * @throws......No Existen Documentos
     * @version.....1.0.
     * @return.......Vector vProveedor(Vector de Objetos CXP_Doc)
     */
    public Vector vProveedor() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Vector vProveedores = new Vector();
        String query = "SQL_LISTAPROVEEDORES";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                while (rs.next()) {
                    vProveedores.add(rs.getString("proveedor"));
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL ENCONTRAR DOCUMENTO " + e.getMessage() + "" + e.getErrorCode());
        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return vProveedores;
    }

    /**
     * Metodo que retorna un Vector de objetos CXPDoc dado el proveedor.
     * @autor.......David Lamadrid, Xml: Andr�s Maturana De La Cruz
     * @param.......String id(Proveedor).
     * @see.........CXP_Doc.class
     * @throws......No Existen Documentos
     * @version.....1.0.
     * @return.......Vector vProveedor(Vector de Objetos CXP_Doc)
     */
    public Vector vProveedorPorId(String id) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Vector vProveedores = new Vector();
        String query = "SQL_PROVEEDORESPORID";//JJCastro fase2

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, id);

                rs = st.executeQuery();

                while (rs.next()) {
                    vProveedores.add(rs.getString("proveedor"));
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL ENCONTRAR DOCUMENTO " + e.getMessage() + "" + e.getErrorCode());
        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return vProveedores;
    }

    /**
     * Metodo que retorna el nombre de un documento dado el tipo de docuemnto
     * @autor.......David Lamadrid
     * @param.......String tipo.
     * @see.........CXP_Doc.class
     * @throws......no existe tipo de docuemto.
     * @version.....1.0.
     * @return......String nombreDocumento
     */
    public String nombreDocumento(String tipo) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String nombre = "";
        String query = "SQL_NOMBREDOC";
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, tipo);
                rs = st.executeQuery();

                while (rs.next()) {
                    nombre = rs.getString("document_name");
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL ENCONTRAR DOCUMENTO " + e.getMessage() + "" + e.getErrorCode());
        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return nombre;
    }

    /**
     * Metodo que retorna un Objeto tipo CXDOC en CXPDocDAO
     * @autor.......David Lamadrid
     * @param.......ninguno
     * @see.........CXP_Doc.class
     * @throws......Clase no encontrada.
     * @version.....1.0.
     * @return......CXPDoc.
     */
    public com.tsp.operation.model.beans.CXP_Doc getFactura() {
        return factura;
    }

    /**
     * Metodo que retorna un Vector de  Objetos tipo CXDOC en CXPDocDAO
     * @autor.......David Lamadrid
     * @param.......ninguno.
     * @see.........CXPDocDAO.class
     * @throws......Clase no encontrada.
     * @version.....1.0.
     * @return.......Vector vCXPDoc.
     */
    public java.util.Vector getVecCxp_doc() {
        return vecCxp_doc;
    }

    /**
     * Metodo que setea un Vector de  Objetos tipo CXDOC en CXPDocDAO
     * @autor.......David Lamadrid
     * @param.......Vector vecCxp_doc(Vector de objetos de CXPDoc)
     * @see..........
     * @throws......Clase no encontrada.
     * @version.....1.0.
     * @return......Vector de Objetos CXPDoc
     */
    public void setVecCxp_doc(java.util.Vector vecCxp_doc) {
        this.vecCxp_doc = vecCxp_doc;
    }

    /**
     * Metodo que retorna un String con fecha generada de una fecha inicial y un numero de dias
     * @autor.......David Lamadrid
     * @param.......ninguno
     * @see.........Util.class
     * @throws.......
     * @version.....1.0.
     * @return......String fechaFinal.
     */
    public String fechaFinal(String fechai, int n) {
        String timeStamp = fechai;
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Calendar c = Util.crearCalendar(timeStamp);
        c.add(c.DATE, n);
        java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(c.getTime().getTime());
        String fechaf = "" + sqlTimestamp;
        return fechaf;
    }

    /**
     * Metodo que devuele el valor de cambio dado dos monedas .
     * @autor.......David Lamadrid
     * @param.......String dis(Distrito),String proveedor,String tipoDoc,String docuemnto.
     * @see.........
     * @throws.......
     * @version.....1.0.
     * @return......float valor.
     */
    public float buscarValor(String moneda1, String moneda2, String fecha, float valor) throws SQLException {

        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        float valor_cambiado = 0;
        String query = "SQL_BVALOR";

        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, fecha);
                st.setString(2, moneda1);
                st.setString(3, moneda2);
                rs = st.executeQuery();

                if (rs.next()) {
                    valor_cambiado = com.tsp.util.Util.redondear1(valor * rs.getFloat("valor"), 0);
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL BUSCAR LA TASA" + e.getMessage() + "" + e.getErrorCode());
        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return valor_cambiado;
    }

    /**
     * Metodo: obtenerCamposPlanilla, permite obtener los campos de un planilla
     * @autor : Ing. Jose de la rosa
     * @param : numero de planilla.
     * @version : 1.0
     */
    public void obtenerCamposPlanilla(String planilla) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_CAMPOS_PLANILLA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, planilla);
                rs = st.executeQuery();
                String nit = "", agencia = "", mims = "", banco = "", sucursal = "", moneda = "", hc = "";
                while (rs.next()) {
                    nit = (rs.getString("nitpro") != null) ? rs.getString("nitpro") : "";
                    agencia = (rs.getString("id_agencia") != null) ? rs.getString("id_agencia") : "";
                    mims = (rs.getString("id_mims") != null) ? rs.getString("id_mims") : "";
                    banco = (rs.getString("branch_code") != null) ? rs.getString("branch_code") : "";
                    sucursal = (rs.getString("bank_account_no") != null) ? rs.getString("bank_account_no") : "";
                    moneda = (rs.getString("currency") != null) ? rs.getString("currency") : "";
                    hc = (rs.getString("hc") != null) ? rs.getString("hc") : "";
                    factura.setProveedor(nit);
                    factura.setAgencia(agencia);
                    factura.setId_mims(mims);
                    factura.setBanco(banco);
                    factura.setSucursal(sucursal);
                    factura.setMoneda(moneda);
                    factura.setHandle_code(hc);
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE LA LISTA DE lOS CAMPOS DE LAS PLANILLAS " + e.getMessage() + " " + e.getErrorCode());
        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Metodo que Inserta un Documento por pagar en el Sistema .
     * @autor.......David Lamadrid
     * @param.......CXDoc doc(Objeto tipo CXPDoc), Vector vItems(Vector de objetos CxPItem),Vector vImpuestos(Vector de Objetos CXPImpDoc).
     * @throws......Existe Documento en el Sistema.
     * @version.....1.0.
     * @return.......
     */
    public void insertarCXPDoc(CXP_Doc doc, Vector vItems, Vector vImpuestosDoc, String agencia) throws Exception {
        Connection con = null;
        Statement st = null;
        StringStatement ps = null;
        ResultSet rs = null;
        String SQL_INSERTAROCUMENTO = "";
        String SQL_INSERTARITEMDOCUMENTO = "";
        String SQL_INSERTARIMPPORITEM = "";
        String SQL_INSERTARIMPDOC = "";
        String SQL_UPDATE_NOTA_DEBITO = "";
        String SQL_UPDATE_NOTA_CREDITO = "";

        try {
            con = conectarJNDI("SQL_INSERTAROCUMENTO");

            con.setAutoCommit(false);
            st = con.createStatement();
            SQL_INSERTAROCUMENTO = this.obtenerSQL("SQL_INSERTAROCUMENTO");
            SQL_INSERTARITEMDOCUMENTO = this.obtenerSQL("SQL_INSERTARITEMDOCUMENTO");
            SQL_INSERTARIMPPORITEM = this.obtenerSQL("SQL_INSERTARIMPPORITEM");
            SQL_INSERTARIMPDOC = this.obtenerSQL("SQL_INSERTARIMPDOC");

            //Ivan NOTA DEBITO
            SQL_UPDATE_NOTA_DEBITO = this.obtenerSQL("SQL_UPDATE_NOTA_DEBITO");
            SQL_UPDATE_NOTA_CREDITO = this.obtenerSQL("SQL_UPDATE_NOTA_CREDITO");

            ps = new StringStatement(SQL_INSERTAROCUMENTO, true);//JJCastro fase2
            ps.setString(1, doc.getDstrct());
            ps.setString(2, doc.getProveedor());
            ps.setString(3, doc.getTipo_documento());
            ps.setString(4, doc.getDocumento());
            ps.setString(5, doc.getDescripcion());
            ps.setString(6, agencia);
            ps.setString(7, doc.getHandle_code());
            ps.setString(8, doc.getId_mims());
            ps.setString(9, doc.getFecha_documento());
            ps.setString(10, doc.getTipo_documento_rel());
            ps.setString(11, doc.getDocumento_relacionado());
            ps.setString(12, doc.getFecha_aprobacion());
            ps.setString(13, doc.getAprobador());
            ps.setString(14, doc.getUsuario_aprobacion());
            ps.setString(15, doc.getFecha_vencimiento());
            ps.setString(16, doc.getUltima_fecha_pago());
            ps.setString(17, doc.getBanco());
            ps.setString(18, doc.getSucursal());
            ps.setString(19, doc.getMoneda());
            ps.setDouble(20, doc.getVlr_neto());
            ps.setDouble(21, doc.getVlr_total_abonos());
            ps.setDouble(22, doc.getVlr_saldo());
            ps.setDouble(23, doc.getVlr_neto_me());
            ps.setDouble(24, doc.getVlr_total_abonos_me());
            ps.setDouble(25, doc.getVlr_saldo_me());
            ps.setDouble(26, doc.getTasa());
            ps.setString(27, doc.getUsuario_contabilizo());
            ps.setString(28, doc.getFecha_contabilizacion());
            ps.setString(29, doc.getUsuario_anulo());
            ps.setString(30, doc.getFecha_anulacion());
            ps.setString(31, doc.getFecha_contabilizacion_anulacion());
            ps.setString(32, doc.getObservacion());
            ps.setDouble(33, doc.getNum_obs_autorizador());
            ps.setDouble(34, doc.getNum_obs_pagador());
            ps.setDouble(35, doc.getNum_obs_registra());
            ps.setString(36, doc.getCreation_user());
            ps.setString(37, doc.getUser_update());
            ps.setString(38, doc.getBase());
            ps.setString(39, doc.getMoneda_banco());
            ps.setString(40, doc.getClase_documento_rel());
            ps.setString(41, doc.getTipo_nomina());
            ps.setString(42, doc.getTipodoc());
            ps.setString(43, doc.getMultiservicio());

            st.addBatch(ps.getSql());//JJCastro fase2
            System.out.println("TIPO DOC-->" + doc.getTipo_documento());
            if (doc.getTipo_documento().equals("ND")) {
                ps = new StringStatement(SQL_UPDATE_NOTA_DEBITO, true);//JJCastro fase2
                ps.setDouble(1, doc.getVlr_saldo());
                ps.setDouble(2, doc.getVlr_saldo_me());
                ps.setDouble(3, doc.getVlr_saldo());
                ps.setDouble(4, doc.getVlr_saldo_me());

                ps.setString(5, doc.getDstrct());
                ps.setString(6, doc.getProveedor());
                ps.setString(7, doc.getTipo_documento_rel());
                ps.setString(8, doc.getDocumento_relacionado());
                System.out.println("NOTA DEBITO --->" + ps.toString());
                st.addBatch(ps.getSql());//JJCastro fase2

            } else if (doc.getTipo_documento().equals("NC")) {
                ps = new StringStatement(SQL_UPDATE_NOTA_CREDITO, true);//JJCastro fase2
                ps.setDouble(1, doc.getVlr_saldo());
                ps.setDouble(2, doc.getVlr_saldo_me());
                ps.setDouble(3, doc.getVlr_saldo());
                ps.setDouble(4, doc.getVlr_saldo_me());

                ps.setString(5, doc.getDstrct());
                ps.setString(6, doc.getProveedor());
                ps.setString(7, doc.getTipo_documento_rel());
                ps.setString(8, doc.getDocumento_relacionado());
                System.out.println("NOTA CREDITO --->" + ps.toString());
                st.addBatch(ps.getSql());//JJCastro fase2
            }

            for (int i = 0; i < vItems.size(); i++) {
                CXPItemDoc item = (CXPItemDoc) vItems.elementAt(i);
                if (item.getDescripcion() != null) {
                    ps = new StringStatement(SQL_INSERTARITEMDOCUMENTO, true);//JJCastro fase2
                    ps.setString(1, item.getDstrct());
                    ps.setString(2, item.getProveedor());
                    ps.setString(3, item.getTipo_documento());
                    ps.setString(4, item.getDocumento());
                    ps.setString(5, item.getItem());
                    ps.setString(6, item.getDescripcion());
                    ps.setDouble(7, item.getVlr());
                    ps.setDouble(8, item.getVlr_me());
                    ps.setString(9, item.getCodigo_cuenta());
                    ps.setString(10, item.getCodigo_abc());
                    ps.setString(11, item.getPlanilla());
                    ps.setString(12, item.getUser_update());
                    ps.setString(13, item.getCreation_user());
                    ps.setString(14, item.getBase());
                    ps.setString(15, item.getCodcliarea());
                    ps.setString(16, item.getTipcliarea());
                    ps.setString(17, item.getConcepto());
                    if (item.getTipoSubledger().equals("")) {
                        ps.setString(18, item.getAuxiliar());
                    } else {
                        ps.setString(18, item.getTipoSubledger() + "-" + item.getAuxiliar());
                    }
                    st.addBatch(ps.getSql());//JJCastro fase2


                    Vector vImpDoc = item.getVItems();
                    for (int ii = 0; ii < vImpDoc.size(); ii++) {
                        CXPImpItem impItem = (CXPImpItem) vImpDoc.elementAt(ii);
                        ps = new StringStatement(SQL_INSERTARIMPPORITEM, true);//JJCastro fase2
                        ps.setString(1, impItem.getDstrct());
                        ps.setString(2, impItem.getProveedor());
                        ps.setString(3, impItem.getTipo_documento());
                        ps.setString(4, impItem.getDocumento());
                        ps.setString(5, impItem.getItem());
                        ps.setString(6, impItem.getCod_impuesto());
                        ps.setDouble(7, impItem.getPorcent_impuesto());
                        ps.setDouble(8, impItem.getVlr_total_impuesto());
                        ps.setDouble(9, impItem.getVlr_total_impuesto_me());
                        ps.setString(10, impItem.getCreation_user());
                        ps.setString(11, impItem.getUser_update());
                        ps.setString(12, impItem.getBase());
                        st.addBatch(ps.getSql());//JJCastro fase2
                    }
                }
            }

            for (int i = 0; i < vImpuestosDoc.size(); i++) {
                CXPImpDoc impDoc = (CXPImpDoc) vImpuestosDoc.elementAt(i);
                ps = new StringStatement(SQL_INSERTARIMPDOC, true);//JJCastro fase2
                ps.setString(1, impDoc.getDstrct());
                ps.setString(2, impDoc.getProveedor());
                ps.setString(3, impDoc.getTipo_documento());
                ps.setString(4, impDoc.getDocumento());
                ps.setString(5, impDoc.getCod_impuesto());
                ps.setDouble(6, impDoc.getPorcent_impuesto());
                ps.setDouble(7, impDoc.getVlr_total_impuesto());
                ps.setDouble(8, impDoc.getVlr_total_impuesto_me());
                ps.setString(9, impDoc.getCreation_user());
                ps.setString(10, impDoc.getUser_update());
                ps.setString(11, impDoc.getBase());
                st.addBatch(ps.getSql());//JJCastro fase2
            }

            st.executeBatch();

            con.commit();
            con.setAutoCommit(true);

        } catch (Exception e) {
            e.printStackTrace();
            if (con != null) {
                con.rollback();
            }
            throw new SQLException("Error en rutina INSERT  [insertarCXPDoc].... \n" + e.getMessage());
        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }

        }

    }

    /**
     * Metodo ingresarCXP_doc, permite ingrear una factura
     * recibe por parametro
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void ingresarCXP_doc() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_INSERT_CXP_DOC";//JJCastro fase2
        try {

            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, factura.getDstrct());
                st.setString(2, factura.getProveedor());
                st.setString(3, factura.getDocumento());
                st.setString(4, factura.getDescripcion());
                st.setString(5, factura.getAgencia());
                st.setString(6, factura.getHandle_code());
                st.setString(7, factura.getId_mims());
                st.setString(8, factura.getUsuario_aprobacion());
                st.setString(9, factura.getBanco());
                st.setString(10, factura.getSucursal());
                st.setString(11, factura.getMoneda());
                st.setDouble(12, factura.getVlr_neto());
                st.setDouble(13, factura.getVlr_saldo());
                st.setString(14, factura.getUsuario_contabilizo());
                st.setString(15, factura.getObservacion());
                st.setString(16, factura.getCreation_user());
                st.setString(17, factura.getBase());
                st.execute();
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL Ingresar CXP DOC " + e.getMessage() + "" + e.getErrorCode());
        } finally {//JJCastro fase2
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Metodo: getCXP_doc, permite retornar un objeto de registros de CXP_doc.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public CXP_Doc getCXP_doc() {
        return factura;
    }

    /**
     * Metodo: getCXP_doc, permite obtener un objeto de registros de CXP_doc.
     * @autor : Ing. Jose de la rosa
     * @param : objeto
     * @version : 1.0
     */
    public void setCXP_doc(CXP_Doc factura) {
        this.factura = factura;
    }

    /**
     * Setter for property facturas.
     * @param facturas New value of property facturas.
     */
    public void setFacturas(java.util.Vector facturas) {
        this.facturas = facturas;
    }

    /**
     * Getter for property vFacturas.
     * @return Value of property vFacturas.
     */
    public java.util.Vector getVFacturas() {
        return vFacturas;
    }

    /**
     * Setter for property vFacturas.
     * @param vFacturas New value of property vFacturas.
     */
    public void setVFacturas(java.util.Vector vFacturas) {
        this.vFacturas = vFacturas;
    }
    private Vector vFacturas;

    /**
     * Metodo inicialisa la Lista de Docuemntos por pagar en CXPDocDAO .
     * @autor.......David Lamadrid
     * @param.......ninguna
     * @see.........CXP_Doc.class
     * @throws......No Existen Documentos en el Sistema.
     * @version.....1.0.
     * @return.......
     */
    public void listaDoc() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        vecCxp_doc = new Vector();
        Vector vProveedor = new Vector();
        String query = "SQL_LISTADOC_ALL";

        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
                for (int i = 0; i < this.vProveedor().size(); i++) {
                    vProveedor = new Vector();
                    st = con.prepareStatement(this.LISTADOC);
                    st.setString(1, "" + this.vProveedor().elementAt(i));
                    rs = st.executeQuery();
                    while (rs.next()) {
                        vProveedor.add(factura.loadCXP_Doc(rs));
                    }
                    vecCxp_doc.add(vProveedor);
                }
            }

        } catch (SQLException e) {
            throw new SQLException("ERROR AL ENCONTRAR DOCUMENTO " + e.getMessage() + "" + e.getErrorCode());
        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }//Fin del Metodo

    /**
     * Metodo inicialisa la Lista de Docuemntos por pagar en CXPDocDAO .
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param proveedor NIT del proveedor
     * @agc C�digo de la agencia del usuario
     * @throws SQLException si ocurre un error en la cone�i�n con la Base de Datos
     * @version 1.0.
     */
    public void facturasNoPagadas(String proveedor, String agc) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        this.vecCxp_doc = new Vector();
        String query = "SQL_NOPAG";
        String query2 = "SQL_NOPAG_PROV";
        try {

            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
                if (proveedor.length() == 0) {
                    st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                    st.setString(1, agc);
                } else {
                    st = con.prepareStatement(this.obtenerSQL(query2));//JJCastro fase2
                    st.setString(1, proveedor);
                }

                rs = st.executeQuery();

                while (rs.next()) {
                    factura = new CXP_Doc();
                    factura = factura.loadCXP_Doc(rs);
                    factura.setNomProveedor(rs.getString("nomprov"));
                    this.vecCxp_doc.add(factura);
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR EN [facturasNoPagadas] " + e.getMessage() + "" + e.getErrorCode());
        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }
    /*ANULAR FACTURA 15 - JUL - 2006*/

    /**
     * Metodo:          ExisteCXP_Doc
     * Descriocion :    Funcion publica que nos dice si existe un documento en cuentas por pagar
     * @autor :         Leonardo Parody
     * @modificado por: LREALES - mayo del 2006
     * @param :         distrito, proveedor, tipo de documento y documento
     * @return:         boo ( retorna un boolean )
     * @version :       1.0
     */
    public boolean ExisteCXP_Doc(String distrito, String proveedor, String tipo_documento, String documento) throws SQLException {
        Connection con = null;
        CXP_Doc cXP_Doc = new CXP_Doc();
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean boo = false;
        String query = "SQL_ConsultarCXP_Doc";//JJCastro fase2

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito);
                st.setString(2, proveedor);
                st.setString(3, tipo_documento);
                st.setString(4, documento);

                rs = st.executeQuery();

                if (rs.next()) {
                    boo = true;
                }

            }
        } catch (SQLException e) {

            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA EXISTENCIA DEL DOCUMENTO DE CUENTAS POR PAGAR " + e.getMessage() + " " + e.getErrorCode());

        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return boo;

    }

    /**
     * Metodo:          ConsultarCXP_Doc
     * Descriocion :    Funcion publica que nos arroja la informacion de un documento en cuentas por pagar
     * @autor :         Leonardo Parody
     * @modificado por: LREALES - mayo del 2006
     * @param :         distrito, proveedor, tipo de documento y documento
     * @return:         cXP_Doc ( retorna un Objeto )
     * @version :       1.0
     */
    public CXP_Doc ConsultarCXP_Doc(String distrito, String proveedor, String tipo_documento, String documento) throws SQLException {
        Connection con = null;
        CXP_Doc cXP_Doc = new CXP_Doc();
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ConsultarCXP_Doc";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito);
                st.setString(2, proveedor);
                st.setString(3, tipo_documento);
                st.setString(4, documento);
                rs = st.executeQuery();
                vFacturas = new Vector();

                while (rs.next()) {

                    cXP_Doc = cXP_Doc.loadCXP_Doc(rs);

                }

            }
        } catch (SQLException e) {

            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL DOCUMENTO DE CUENTAS POR PAGAR " + e.getMessage() + " " + e.getErrorCode());

        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return cXP_Doc;

    }

    /**
     * Metodo:          AnularCXP_Doc
     * Descriocion :    Funcion publica que nos coloca un documento de cuentas por pagar en estado 'ANULADO'
     * @autor :         Leonardo Parody
     * @modificado por: LREALES - mayo del 2006
     * @param :         cXP_Doc ( documento de cuentas por pagar )
     * @return:         nada.
     * @version :       1.0
     */
    public void AnularCXP_Doc(CXP_Doc cXP_Doc) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_AnularCXP_Doc";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cXP_Doc.getUser_update());
                st.setString(2, cXP_Doc.getUser_update());
                st.setString(3, cXP_Doc.getDstrct());
                st.setString(4, cXP_Doc.getProveedor());
                st.setString(5, cXP_Doc.getTipo_documento());
                st.setString(6, cXP_Doc.getDocumento());

                st.executeUpdate();

            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE LA ANULACION DEL DOCUMENTO " + e.getMessage() + "" + e.getErrorCode());
        } finally {//JJCastro fase2
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

    }

    /**
     * Metodo:          SQL_Nombre_Proveedor
     * Descriocion :    Funcion publica que obtiene el nombre de un proveedor
     * @autor :         LREALES
     * @param :         distrito y proveedor
     * @return:         nombre ( retorna un String )
     * @version :       1.0
     */
    public String SQL_Nombre_Proveedor(String distrito, String proveedor) throws SQLException {
        Connection con = null;
        CXP_Doc cXP_Doc = new CXP_Doc();
        PreparedStatement st = null;
        ResultSet rs = null;
        String nombre = "";
        String query = "SQL_Nombre_Proveedor";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito);
                st.setString(2, proveedor);
                rs = st.executeQuery();

                if (rs.next()) {
                    nombre = rs.getString("payment_name");
                }

            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL NOMBRE DEL PROVEEDOR " + e.getMessage() + " " + e.getErrorCode());
        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return nombre;
    }

    /**
     * Metodo:          AnularMovPla
     * Descriocion :    Funcion publica que actualiza un movimiento de planilla a estado anulado.
     * @autor :         LREALES - junio del 2006
     * @param :         vlr_disc_ht, vlr_ht, vlr_for_ht, user_update_ht, user_anul_ht, concept_code_ht, planilla_ht, pla_owner_ht, y creation_date_ht.
     * @return:         -
     * @version :       1.0
     */
    public void AnularMovPla(String vlr_disc_ht, String vlr_ht, String vlr_for_ht, String user_update_ht, String user_anul_ht, String concept_code_ht, String planilla_ht, String pla_owner_ht, String creation_date_ht) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_AnularMovPla";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, vlr_disc_ht);// * (-1)
                st.setString(2, vlr_ht);// * (-1)
                st.setString(3, vlr_for_ht);// * (-1)
                st.setString(4, user_update_ht);// usuario_session
                st.setString(5, user_anul_ht);// usuario_session
                st.setString(6, concept_code_ht);// =
                st.setString(7, planilla_ht);// =
                st.setString(8, pla_owner_ht);// =
                st.setString(9, creation_date_ht);// =
                st.executeUpdate();

            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE LA ANULACION DEL MOVIMIENTO DE LA PLANILLA " + e.getMessage() + " " + e.getErrorCode());
        } finally {//JJCastro fase2
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Metodo:          DuplicarMovPla
     * Descriocion :    Funcion publica que nos inserta un duplicado del movimiento de la planilla
     * @autor :         LREALES - junio del 2006
     * @param :         un Hashtable, , vlr_disc_htd, vlr_htd, y vlr_for_htd.
     * @return:         -
     * @version :       1.0
     */
    public void DuplicarMovPla(Hashtable ht_movpla, String vlr_disc_htd, String vlr_htd, String vlr_for_htd) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_DuplicarMovPla";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, ht_movpla.get("reg_status_ht").toString());
                st.setString(2, ht_movpla.get("dstrct_ht").toString());
                st.setString(3, ht_movpla.get("agency_id_ht").toString());
                st.setString(4, ht_movpla.get("document_type_ht").toString());
                st.setString(5, ht_movpla.get("document_ht").toString());
                st.setString(6, ht_movpla.get("item_ht").toString());
                st.setString(7, ht_movpla.get("concept_code_ht").toString());
                st.setString(8, ht_movpla.get("pla_owner_ht").toString());
                st.setString(9, ht_movpla.get("planilla_ht").toString());
                st.setString(10, ht_movpla.get("supplier_ht").toString());
                st.setString(11, ht_movpla.get("date_doc_ht").toString());
                st.setString(12, ht_movpla.get("applicated_ind_ht").toString());
                st.setString(13, ht_movpla.get("application_ind_ht").toString());
                st.setString(14, ht_movpla.get("ind_vlr_ht").toString());
                st.setString(15, vlr_disc_htd);
                st.setString(16, vlr_htd);
                st.setString(17, vlr_for_htd);
                st.setString(18, ht_movpla.get("currency_ht").toString());
                st.setString(19, ht_movpla.get("last_update_ht").toString());
                st.setString(20, ht_movpla.get("user_update_ht").toString());
                st.setString(21, ht_movpla.get("creation_user_ht").toString());
                st.setString(22, ht_movpla.get("proveedor_anticipo_ht").toString());
                st.setString(23, ht_movpla.get("sucursal_ht").toString());
                st.setString(24, ht_movpla.get("base_ht").toString());
                st.setString(25, ht_movpla.get("corte_ht").toString());
                st.setString(26, ht_movpla.get("branch_code_ht").toString());
                st.setString(27, ht_movpla.get("bank_account_no_ht").toString());
                st.setString(28, ht_movpla.get("fech_anul_ht").toString());
                st.setString(29, ht_movpla.get("user_anul_ht").toString());
                st.setString(30, ht_movpla.get("ch_remplazo_ht").toString());
                st.setString(31, ht_movpla.get("branch_code_remplazo_ht").toString());
                st.setString(32, ht_movpla.get("bank_account_no_remplazo_ht").toString());
                st.setString(33, ht_movpla.get("fecha_cheque_ht").toString());
                st.setString(34, ht_movpla.get("fecha_migracion_ht").toString());
                st.setString(35, ht_movpla.get("cantidad_ht").toString());
                st.setString(36, ht_movpla.get("reanticipo_ht").toString());
                st.setString(37, ht_movpla.get("transaccion_ht").toString());
                st.setString(38, ht_movpla.get("fecha_contabilizacion_ht").toString());
                //System.out.println("*****SQL_DuplicarMovPla: "+st);
                st.execute();

            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE LA DUPLICACION DEL MOVIMIENTO DE LA PLANILLA " + e.getMessage() + " " + e.getErrorCode());
        } finally {//JJCastro fase2
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Metodo:          ConsultarMovPla
     * Descriocion :    Funcion publica que nos arroja la informacion de un movimiento de planilla
     * @autor :         LREALES - junio del 2006
     * @param :         un Hashtable
     * @return:         todos los campos de la tabla movpla. ( Hashtable )
     * @version :       1.0
     */
    public void ConsultarMovPla(Hashtable ht_items) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ConsultarMovPla";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, ht_items.get("concepto_ht").toString());
                st.setString(2, ht_items.get("planilla_ht").toString());
                rs = st.executeQuery();
                vector_movpla = new Vector();

                while (rs.next()) {

                    ht_movpla = new Hashtable();
                    String reg_status_ht = rs.getString("reg_status");
                    String dstrct_ht = rs.getString("dstrct");
                    String agency_id_ht = rs.getString("agency_id");
                    String document_type_ht = rs.getString("document_type");
                    String document_ht = rs.getString("document");
                    String item_ht = rs.getString("item");
                    String concept_code_ht = rs.getString("concept_code");
                    String pla_owner_ht = rs.getString("pla_owner");
                    String planilla_ht = rs.getString("planilla");
                    String supplier_ht = rs.getString("supplier");
                    String date_doc_ht = rs.getString("date_doc");
                    String applicated_ind_ht = rs.getString("applicated_ind");
                    String application_ind_ht = rs.getString("application_ind");
                    String ind_vlr_ht = rs.getString("ind_vlr");
                    String vlr_disc_ht = rs.getString("vlr_disc");
                    String vlr_ht = rs.getString("vlr");
                    String vlr_for_ht = rs.getString("vlr_for");
                    String currency_ht = rs.getString("currency");
                    String last_update_ht = rs.getString("last_update");
                    String user_update_ht = rs.getString("user_update");
                    String creation_date_ht = rs.getString("creation_date");
                    String creation_user_ht = rs.getString("creation_user");
                    String proveedor_anticipo_ht = rs.getString("proveedor_anticipo");
                    String sucursal_ht = rs.getString("sucursal");
                    String base_ht = rs.getString("base");
                    String corte_ht = rs.getString("corte");
                    String branch_code_ht = rs.getString("branch_code");
                    String bank_account_no_ht = rs.getString("bank_account_no");
                    String fech_anul_ht = rs.getString("fech_anul");
                    String user_anul_ht = rs.getString("user_anul");
                    String ch_remplazo_ht = rs.getString("ch_remplazo");
                    String branch_code_remplazo_ht = rs.getString("branch_code_remplazo");
                    String bank_account_no_remplazo_ht = rs.getString("bank_account_no_remplazo");
                    String fecha_cheque_ht = rs.getString("fecha_cheque");
                    String fecha_migracion_ht = rs.getString("fecha_migracion");
                    String cantidad_ht = rs.getString("cantidad");
                    String reanticipo_ht = rs.getString("reanticipo");
                    String transaccion_ht = rs.getString("transaccion");
                    String fecha_contabilizacion_ht = rs.getString("fecha_contabilizacion");

                    ht_movpla.put("reg_status_ht", reg_status_ht);
                    ht_movpla.put("dstrct_ht", dstrct_ht);
                    ht_movpla.put("agency_id_ht", agency_id_ht);
                    ht_movpla.put("document_type_ht", document_type_ht);
                    ht_movpla.put("document_ht", document_ht);
                    ht_movpla.put("item_ht", item_ht);
                    ht_movpla.put("concept_code_ht", concept_code_ht);
                    ht_movpla.put("pla_owner_ht", pla_owner_ht);
                    ht_movpla.put("planilla_ht", planilla_ht);
                    ht_movpla.put("supplier_ht", supplier_ht);
                    ht_movpla.put("date_doc_ht", date_doc_ht);
                    ht_movpla.put("applicated_ind_ht", applicated_ind_ht);
                    ht_movpla.put("application_ind_ht", application_ind_ht);
                    ht_movpla.put("ind_vlr_ht", ind_vlr_ht);
                    ht_movpla.put("vlr_disc_ht", vlr_disc_ht);
                    ht_movpla.put("vlr_ht", vlr_ht);
                    ht_movpla.put("vlr_for_ht", vlr_for_ht);
                    ht_movpla.put("currency_ht", currency_ht);
                    ht_movpla.put("last_update_ht", last_update_ht);
                    ht_movpla.put("user_update_ht", user_update_ht);
                    ht_movpla.put("creation_date_ht", creation_date_ht);
                    ht_movpla.put("creation_user_ht", creation_user_ht);
                    ht_movpla.put("proveedor_anticipo_ht", proveedor_anticipo_ht);
                    ht_movpla.put("sucursal_ht", sucursal_ht);
                    ht_movpla.put("base_ht", base_ht);
                    ht_movpla.put("corte_ht", corte_ht);
                    ht_movpla.put("branch_code_ht", branch_code_ht);
                    ht_movpla.put("bank_account_no_ht", bank_account_no_ht);
                    ht_movpla.put("fech_anul_ht", fech_anul_ht);
                    ht_movpla.put("user_anul_ht", user_anul_ht);
                    ht_movpla.put("ch_remplazo_ht", ch_remplazo_ht);
                    ht_movpla.put("branch_code_remplazo_ht", branch_code_remplazo_ht);
                    ht_movpla.put("bank_account_no_remplazo_ht", bank_account_no_remplazo_ht);
                    ht_movpla.put("fecha_cheque_ht", fecha_cheque_ht);
                    ht_movpla.put("fecha_migracion_ht", fecha_migracion_ht);
                    ht_movpla.put("cantidad_ht", cantidad_ht);
                    ht_movpla.put("reanticipo_ht", reanticipo_ht);
                    ht_movpla.put("transaccion_ht", transaccion_ht);
                    ht_movpla.put("fecha_contabilizacion_ht", fecha_contabilizacion_ht);

                    this.vector_movpla.add(ht_movpla);

                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE LA BUSQUEDA EN MOVPLA " + e.getMessage() + " " + e.getErrorCode());
        } finally {//JJCastro fase2
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }
    Hashtable ht_items = new Hashtable();
    private Vector vector_items;

    /**
     * Getter for property vector_items.
     * @return Value of property vector_items.
     */
    public java.util.Vector getVector_items() {
        return vector_items;
    }

    /**
     * Setter for property vector_items.
     * @param vector_items New value of property vector_items.
     */
    public void setVector_items(java.util.Vector vector_items) {
        this.vector_items = vector_items;
    }

    /**
     * Metodo:          ConsultarCXP_Items_Doc
     * Descriocion :    Funcion publica que nos arroja la informacion de un item de un documento en cuentas por pagar
     * @autor :         LREALES - junio del 2006
     * @param :         distrito, proveedor, tipo de documento y documento
     * @return:         distrito, documento, item, planilla y concepto. ( Hashtable )
     * @version :       1.0
     */
    public void ConsultarCXP_Items_Doc(String distrito, String proveedor, String tipo_documento, String documento) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ConsultarCXP_Items_Doc";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito);
                st.setString(2, proveedor);
                st.setString(3, tipo_documento);
                st.setString(4, documento);
                rs = st.executeQuery();

                vector_items = new Vector();

                while (rs.next()) {
                    ht_items = new Hashtable();
                    String distrito_ht = rs.getString("dstrct").toUpperCase();
                    String documento_ht = rs.getString("documento").toUpperCase();
                    String item_ht = rs.getString("item").toUpperCase();
                    String planilla_ht = rs.getString("planilla").toUpperCase();
                    String concepto_ht = rs.getString("concepto").toUpperCase();

                    ht_items.put("distrito_ht", distrito_ht);
                    ht_items.put("documento_ht", documento_ht);
                    ht_items.put("item_ht", item_ht);
                    ht_items.put("planilla_ht", planilla_ht);
                    ht_items.put("concepto_ht", concepto_ht);
                    this.vector_items.add(ht_items);

                }

            }
        } catch (SQLException e) {

            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL ITEM DEL DOCUMENTO " + e.getMessage() + " " + e.getErrorCode());

        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

    }
    Hashtable ht_tblcon = new Hashtable();
    private Vector vector_tblcon;

    /**
     * Getter for property vector_tblcon.
     * @return Value of property vector_tblcon.
     */
    public java.util.Vector getVector_tblcon() {
        return vector_tblcon;
    }

    /**
     * Setter for property vector_tblcon.
     * @param vector_tblcon New value of property vector_tblcon.
     */
    public void setVector_tblcon(java.util.Vector vector_tblcon) {
        this.vector_tblcon = vector_tblcon;
    }

    /**
     * Metodo:          ConsultarTblCon
     * Descriocion :    Funcion publica que nos arroja la informacion de un concepto
     * @autor :         LREALES - junio del 2006
     * @param :         un Hashtable
     * @return:         concept_desc y tipocuenta. ( Hashtable )
     * @version :       1.0
     */
    public void ConsultarTblCon(Hashtable ht_items) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ConsultarTblCon";

        try {

            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, ht_items.get("distrito_ht").toString());
                st.setString(2, ht_items.get("concepto_ht").toString());
                rs = st.executeQuery();
                vector_tblcon = new Vector();

                while (rs.next()) {

                    ht_tblcon = new Hashtable();

                    String concept_desc_ht = rs.getString("concept_desc").toUpperCase();
                    String tipocuenta_ht = rs.getString("tipocuenta").toUpperCase();

                    ht_tblcon.put("concept_desc_ht", concept_desc_ht);
                    ht_tblcon.put("tipocuenta_ht", tipocuenta_ht);

                    this.vector_tblcon.add(ht_tblcon);

                }

            }
        } catch (SQLException e) {

            throw new SQLException("ERROR DURANTE LA BUSQUEDA EN TBLCON " + e.getMessage() + " " + e.getErrorCode());

        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

    }
    Hashtable ht_movpla = new Hashtable();
    private Vector vector_movpla;

    /**
     * Getter for property vector_movpla.
     * @return Value of property vector_movpla.
     */
    public java.util.Vector getVector_movpla() {
        return vector_movpla;
    }

    /**
     * Setter for property vector_movpla.
     * @param vector_movpla New value of property vector_movpla.
     */
    public void setVector_movpla(java.util.Vector vector_movpla) {
        this.vector_movpla = vector_movpla;
    }

    public void listaDocPorProveedor(String id, String banco, String sucursal) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        vecCxp_doc = new Vector();
        Vector vProveedor = new Vector();
        Connection con = null;
        String query = "SQL_LISTADOC";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                for (int i = 0; i < this.vProveedorPorId(id).size(); i++) {
                    vProveedor = new Vector();
                    if (banco.equals("")) {
                        st = con.prepareStatement(this.obtenerSQL("SQL_LISTADOC"));
                        st.setString(1, "" + this.vProveedorPorId(id).elementAt(i));
                    } else {
                        st = con.prepareStatement(this.obtenerSQL("SQL_LISTADOC1"));
                        st.setString(1, "" + this.vProveedorPorId(id).elementAt(i));
                        st.setString(2, banco);
                        st.setString(3, sucursal);
                    }
                    rs = st.executeQuery();
                    int sw = 0;
                    while (rs.next()) {
                        sw = 1;
                        vProveedor.add(factura.loadCXP_Doc(rs));
                    }
                    if (sw == 1) {
                        vecCxp_doc.add(vProveedor);
                    }
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL ENCONTRAR DOCUMENTO " + e.getMessage() + "" + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    //Ivan  22 julio 2006
    /**
     * Metodo buscarCuenta , para buscar los datos de la cuenta
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : String dstrct String cuenta
     * @throws  : SQLException
     * @version : 1.0
     */
    public Hashtable buscarCuenta(String dstrct, String cuenta) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Hashtable Cuenta = null;
        String query = "SQL_BUSCAR_CUENTA";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dstrct);
                st.setString(2, cuenta);
                rs = st.executeQuery();
                if (rs.next()) {
                    Cuenta = new Hashtable();
                    Cuenta.put("cuenta", rs.getString("cuenta"));
                    Cuenta.put("subledger", rs.getString("subledger"));
                    Cuenta.put("tercero", rs.getString("tercero"));
                }

            }
        } catch (SQLException ex) {
            throw new SQLException("Error en buscarCuenta --> [CXPDocDAO] " + ex.getMessage() + " " + ex.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return Cuenta;
    }

    /**
     * Metodo obtenerAgencias , Metodo para buscar las agencias
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @throws  : SQLException
     * @version : 1.0
     */
    public Vector obtenerAgencias() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Vector agencias = new Vector();
        String query = "SQL_BUSCAR_AGENCIAS";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                while (rs.next()) {
                    Agencia agencia = new Agencia();
                    agencia.setId_agencia((rs.getString("id_agencia") != null) ? rs.getString("id_agencia") : "");
                    agencia.setNombre((rs.getString("nombre") != null) ? rs.getString("nombre") : "");
                    agencia.setAgenciaContable((rs.getString("age_contable") != null) ? rs.getString("age_contable") : "");
                    agencia.setUnidadNegocio((rs.getString("unidad") != null) ? rs.getString("unidad") : "");

                    agencias.addElement(agencia);
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE LA CONSULTA SQL_BUSCAR_AGENCIAS" + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return agencias;
    }

    /**
     * Metodo ExisteABC , para verificar si existe el codigo ABC
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @throws  : SQLException
     * @version : 1.0
     */
    public boolean ExisteABC(String codigo) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean existe = false;
        String query = "SQL_VALIDAR_ABC";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, codigo);
                rs = st.executeQuery();
                if (rs.next()) {
                    existe = true;
                }

            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL COD ABC" + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return existe;
    }

    /**
     * Metodo buscarFacturasxUsuario, recibe el nombre del usuario en session,
     * permite buscar las facturas a autorizar o autorizadas por el usuario autorizador en session
     * por un rango de fechas
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : String identificaci�n del usuario, distrito, Agencia, fecha inicial,
     *          fecha final y boolean para saber si es el listado de autorizadas o no autorizadas
     * @version : 1.0
     */
    public void buscarFacturasxUsuario(String idusuario, String Dstrct, String Agencia, String Fecini, String Fecfin, boolean Autorizadas) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        vecCxp_doc = null;
        vecCxp_doc = new Vector();
        factura = null;
        String query = "SQL_BUSC_FACTX_USUARIO";
        String sql = "";
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
                sql = this.obtenerSQL(query);
                if (Autorizadas) {
                    sql = sql.replaceAll("#OPERADOR#", "!=");
                } else {
                    sql = sql.replaceAll("#OPERADOR#", "=");
                }

                if (Agencia.equals("TODAS")) {
                    sql = sql.replaceAll("#AGENCIA#", "");
                } else {
                    sql = sql.replaceAll("#AGENCIA#", "AND C.agencia = '" + Agencia + "'");
                }

                st = con.prepareStatement(sql);
                st.setString(1, Dstrct);
                st.setString(2, idusuario);
                st.setString(3, Fecini + " 00:00:00");
                st.setString(4, Fecfin + " 23:59:59");
                rs = st.executeQuery();
                while (rs.next()) {
                    factura = new CXP_Doc();
                    factura.setDstrct(rs.getString("dstrct"));
                    factura.setProveedor(rs.getString("proveedor"));
                    factura.setNomProveedor(rs.getString("nomprov"));
                    factura.setTipo_documento(rs.getString("tipo_documento"));
                    factura.setDocument_name(rs.getString("document_name"));
                    factura.setDocumento(rs.getString("documento"));
                    factura.setFecha_documento(rs.getString("fecha_documento"));
                    factura.setDescripcion(rs.getString("descripcion"));
                    factura.setVlr_saldo(rs.getDouble("vlr_saldo"));
                    factura.setMoneda(rs.getString("moneda"));
                    factura.setAgencia(rs.getString("agencia"));
                    factura.setCreation_user(rs.getString("creation_user"));
                    factura.setNumDias(rs.getString("dias"));
                    factura.setFecha_vencimiento(rs.getString("fecha_vencimiento"));
                    int numObsAutorizador = rs.getInt("num_obs_autorizador");
                    int numObsPagador = rs.getInt("num_obs_pagador");
                    factura.setNum_obs_autorizador(numObsAutorizador);
                    factura.setNum_obs_pagador(numObsPagador);
                    if (numObsAutorizador > 0 && numObsPagador != numObsAutorizador) {
                        factura.setBanderaRoja(true);
                    } else {
                        factura.setBanderaRoja(false);
                    }
                    if (numObsPagador == numObsAutorizador && numObsPagador != 0) {
                        factura.setBanderaVerde(true);
                    } else {
                        factura.setBanderaVerde(false);
                    }
                    if (numObsPagador == 0 && numObsAutorizador == 0) {
                        factura.setBanderaVerde(false);
                        factura.setBanderaVerde(false);
                    }
                    vecCxp_doc.addElement(factura);
                }

            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL BUSCAR CUENTAS X USUARIO" + e.getMessage() + "" + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Metodo actualizarNumeroObsAutorizador, Actualiza el numero de la observaci�n del Autorizador
     * de acuerdo a la opcion que recibe por parametro, las cuales pueden ser sumar o restar
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : String opcion, String distrito, String proveedor, String tipo de documento, String documento
     * @version : 1.0
     */
    public void actualizarNumeroObsAutorizador(String opcion, String dstrct, String proveedor, String tipo_documento, String documento) throws SQLException {
        int num = 0;
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_NUM_OBS_AUTORIZADOR";
        String query2 = "SQL_UPT_NUM_OBS_AUTORIZADOR";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dstrct);
                st.setString(2, proveedor);
                st.setString(3, tipo_documento);
                st.setString(4, documento);
                rs = st.executeQuery();
                if (rs.next()) {
                    num = rs.getInt("num");
                }
                st = null;
                st = con.prepareStatement(this.obtenerSQL(query2));//JJCastro fase2
                st.setInt(1, num + 1);
                st.setString(2, dstrct);
                st.setString(3, proveedor);
                st.setString(4, tipo_documento);
                st.setString(5, documento);
                st.execute();
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL ACTUALIZAR NUMERO DE OBS AUTORIZADOR" + e.getMessage() + "" + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Metodo BuscarFactObsPagador, recibe el nombre del usuario en session,
     * permite buscar las facturas con observacion para el Usuario pagador
     * @autor : Ing. Ivan Dario Gomez Vanegas
     * @param : String identificaci�n del usuario y el distrito
     * @version : 1.0
     */
    public void BuscarFactObsPagador(String idusuario, String Dstrct, String Agencia, String Fecini, String Fecfin) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        vecCxp_doc = null;
        vecCxp_doc = new Vector();
        factura = null;
        Connection con = null;
        String query = "SQL_FACTURAS_OBSERVACION";
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
                String sql = this.obtenerSQL("SQL_FACTURAS_OBSERVACION");

                if (Agencia.equals("TODAS")) {
                    sql = sql.replaceAll("#AGENCIA#", "");
                } else {
                    sql = sql.replaceAll("#AGENCIA#", "AND C.agencia = '" + Agencia + "'");
                }

                st = con.prepareStatement(sql);
                st.setString(1, Dstrct);
                st.setString(2, idusuario);
                st.setString(3, Fecini + " 00:00:00");
                st.setString(4, Fecfin + " 23:59:59");
                //System.out.println("Query: "+st.toString());
                rs = st.executeQuery();
                while (rs.next()) {
                    factura = new CXP_Doc();
                    factura.setDstrct(rs.getString("dstrct"));
                    factura.setProveedor(rs.getString("proveedor"));
                    factura.setNomProveedor(rs.getString("nomprov"));
                    factura.setTipo_documento(rs.getString("tipo_documento"));
                    factura.setDocument_name(rs.getString("document_name"));
                    factura.setDocumento(rs.getString("documento"));
                    factura.setFecha_documento(rs.getString("fecha_documento"));
                    factura.setDescripcion(rs.getString("descripcion"));
                    factura.setVlr_saldo(rs.getDouble("vlr_saldo"));
                    factura.setMoneda(rs.getString("moneda"));
                    factura.setAgencia(rs.getString("agencia"));
                    factura.setCreation_user(rs.getString("creation_user"));
                    factura.setNumDias(rs.getString("dias"));
                    factura.setFecha_vencimiento(rs.getString("fecha_vencimiento"));
                    factura.setBanderaRoja(true);
                    vecCxp_doc.addElement(factura);

                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL BUSCAR OBSERVACION PAGADOR " + e.getMessage() + "" + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Metodo actualizarNumeroObsAutorizador, Actualiza el numero de la observaci�n del
     * Pagador o Registrador* de acuerdo a la opcion que recibe por parametro y el usuario
     * a actualizar.
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : String opcion, String usuario, String distrito, String proveedor, String tipo de documento, String documento
     * @version : 1.0
     */
    public void actualizarNumeroObsPagadorRegistrador(String opcion, String dstrct, String proveedor, String tipo_documento, String documento) throws SQLException {
        int num = 0;
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_MAX_NUM_OBS_FIN_PAGADOR";
        String query2 = "SQL_UPDATE_FINCXPDOC";

        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dstrct);
                st.setString(2, proveedor);
                st.setString(3, tipo_documento);
                st.setString(4, documento);
                rs = st.executeQuery();
                if (rs.next()) {
                    num = rs.getInt("num");
                }
                st = null;
                st = con.prepareStatement(this.obtenerSQL(query2));//JJCastro fase2
                st.setInt(1, num + 1);
                st.setString(2, dstrct);
                st.setString(3, proveedor);
                st.setString(4, tipo_documento);
                st.setString(5, documento);
                st.execute();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR AL ACTUALIZAR NUMERO DE OBS PAGADOR REGISTRADOR" + e.getMessage() + "" + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    //Ivan Dario Gomez 08 agosto 2006
    /**
     * Metodo obtenerAgencias , Metodo para buscar las agencias
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @throws  : SQLException
     * @version : 1.0
     */
    public List BuscarDocumentos(String dstrct, String proveedor, String tipoDoc) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        List documentos = new LinkedList();
        String query = "SQL_BUSCAR_DOCUMENTOS";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dstrct);
                st.setString(2, proveedor);
                st.setString(3, tipoDoc);
                //System.out.println("QUERY DOCUMENTOS-->"+st.toString());
                rs = st.executeQuery();
                while (rs.next()) {
                    CXP_Doc doc = new CXP_Doc();
                    doc.setDocumento(rs.getString("documento"));
                    doc.setProveedor(rs.getString("proveedor"));
                    doc.setBanco(rs.getString("banco"));
                    doc.setMoneda(rs.getString("moneda"));
                    doc.setSucursal(rs.getString("sucursal"));
                    doc.setPlazo(rs.getInt("plazo"));
                    doc.setMoneda_banco(rs.getString("moneda_banco"));
                    doc.setUsuario_aprobacion(rs.getString("aprobador"));
                    doc.setHandle_code(rs.getString("handle_code"));

                    //Ivan DArio Gomez NOTA DEBITO 2006 11 07 
                    doc.setAgenciaBanco(rs.getString("agencia"));
                    /////////////////////////////////////////////

                    documentos.add(doc);

                }

            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE LA CONSULTA SQL_BUSCAR_DOCUMENTOS" + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return documentos;
    }

    /**
     * Metodo TieneNotasDEB_CRE , Metodo para verificar si una factura
     * tiene notas debitos o notas creditos
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @throws  : SQLException
     * @version : 1.0
     */
    public boolean TieneNotasDEB_CRE(String factura) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean tiene = false;
        String query = "SQL_BUSCAR_NOTAS";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, factura);
                rs = st.executeQuery();
                while (rs.next()) {
                    tiene = true;
                }

            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE LA CONSULTA SQL_BUSCAR_NOTAS" + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return tiene;
    }

    /**
     * Metodo: BuscarSaldoDocRel, busca el saldo del documento relacionado
     * @autor : Ing. Ivan Gomez
     * @param :String distrito, String codigo del proveedor y numero de la factura
     * @version : 1.0
     */
    public double BuscarSaldoDocRel(String dstrct, String proveedor, String documento, String tipo_doc) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        double saldo = 0;
        String query = "SQL_BUSCAR_SASLDO_DOC_REL";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dstrct);
                st.setString(2, proveedor);
                st.setString(3, tipo_doc);
                st.setString(4, documento);
                rs = st.executeQuery();
                if (rs.next()) {
                    saldo = rs.getDouble("vlr_saldo_me");
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE LA CONSULTA SQL_BUSCAR_SASLDO_DOC_REL " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return saldo;
    }

    /**
     * Metodo: BuscarSaldoDocRel, busca el saldo del documento relacionado
     * @autor : Ing. Ivan Gomez
     * @param :String distrito, String codigo del proveedor y numero de la factura
     * @version : 1.0
     */
    public String BuscarOriPla(String planilla) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String Origen = "";
        String query = "SQL_BUSCAR_ORIPLA";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, planilla);
                //System.out.println("SQL BUSCAR ORIPLA  - ---- " + st.toString());
                rs = st.executeQuery();
                if (rs.next()) {
                    Origen = rs.getString("oripla");
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE LA CONSULTA SQL_BUSCAR_ORIPLA " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return Origen;
    }

    /**
     * Metodo BuscarHC , Metodo para buscar los hc
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @throws  : SQLException
     * @version : 1.0
     */
    public List BuscarHC() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        List hc = new LinkedList();
        String query = "SQL_HC";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                while (rs.next()) {
                    String hcs = rs.getString("table_code");
                    hc.add(hcs);

                }


            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE LA CONSULTA SQL_HC" + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return hc;
    }

    /**
     * Metodo BuscarHC , Metodo para buscar la planilla de una OP
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @throws  : SQLException
     * @version : 1.0
     */
    public String BuscarPlanillaOP(String Dstrct, String Proveedor, String Tipo_documento, String Documento) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String planilla = "";
        String query = "SQL_BUSCAR_PLANILLA_OP";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, Dstrct);
                st.setString(2, Proveedor);
                st.setString(3, Tipo_documento);
                st.setString(4, Documento);
                rs = st.executeQuery();
                while (rs.next()) {
                    planilla = rs.getString("planilla");
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE LA CONSULTA SQL_BUSCAR_PLANILLA_OP" + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return planilla;
    }

    /**
     * Metodo buscarTotalIMP , Metodo para buscar el total del impuesto de una factura
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @throws  : SQLException
     * @version : 1.0
     */
    public double buscarTotalIMP(String Dstrct, String Proveedor, String Tipo_documento, String Documento, String agencia, String FechaFac, String TipoIMP) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        double imp = 0;
        String query = "SQL_BUSCAR_TOTAL_IMP";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, Dstrct);
                st.setString(2, Proveedor);
                st.setString(3, Tipo_documento);
                st.setString(4, Documento);
                st.setString(5, agencia);
                st.setString(6, FechaFac);
                st.setString(7, TipoIMP);
                rs = st.executeQuery();
                while (rs.next()) {
                    imp = rs.getDouble("imp");
                }


            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE LA CONSULTA SQL_BUSCAR_TOTAL_IMP" + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return imp;
    }

    /**
     * Metodo para buscar los docs relacionados a una factura
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public List buscarDocsRelacionados(
            String dstrct,
            String proveedor,
            String doc) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        List hc = new LinkedList();
        String query = "SQL_REPFRA_DOCREL";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dstrct);
                st.setString(2, proveedor);
                st.setString(3, doc);

                org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
                logger.info("SQL CONSULTA FACTURA: " + st);
                rs = st.executeQuery();
                System.gc();
                while (rs.next()) {
                    factura = null;
                    factura = new CXP_Doc();

                    factura.setDstrct(rs.getString("dstrct"));
                    factura.setProveedor(rs.getString("proveedor"));
                    factura.setTipo_documento(rs.getString("tipo_documento"));
                    factura.setDocumento(rs.getString("documento"));
                    factura.setMoneda(rs.getString("moneda"));
                    factura.setVlr_neto(rs.getDouble("vlr_neto"));
                    factura.setVlr_neto_me(rs.getDouble("vlr_neto_me"));
                    factura.setVlr_saldo_me(rs.getDouble("vlr_saldo_me"));
                    hc.add(factura);

                }


            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE buscarDocsRelacionados" + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return hc;
    }

    /**
     * Insertar item
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public String insertarItemCXPDocMigracion(CXPItemDoc item) throws SQLException {
        Connection con = null;
        StringStatement st = null;
        ResultSet rs = null;
        String query = "SQL_INSERT_ITEM_DOCUMENTO_MIG";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, item.getDstrct());
            st.setString(2, item.getProveedor());
            st.setString(3, item.getTipo_documento());
            st.setString(4, item.getDocumento());
            st.setString(5, item.getItem());
            st.setString(6, item.getDescripcion());
            st.setDouble(7, item.getVlr());
            st.setDouble(8, item.getVlr_me());
            st.setString(9, item.getCodigo_cuenta());
            st.setString(10, item.getCodigo_abc());
            st.setString(11, item.getPlanilla());
            st.setString(12, item.getCreation_user());
            st.setString(13, item.getAuxiliar());

            return st.toString();
        } catch (Exception e) {
            throw new SQLException("ERROR DURANTE buscarDocsRelacionados" + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
    }

    /**
     * Verifica si existe la factura sin importar su estado
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public boolean existeCxPDoc(CXP_Doc doc) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_FACTURA";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, doc.getDstrct());
                st.setString(2, doc.getProveedor());
                st.setString(3, doc.getTipo_documento());
                st.setString(4, doc.getDocumento());

                rs = st.executeQuery();

                if (rs.next()) {
                    return true;
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE buscarDocsRelacionados" + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return false;
    }

    /**
     * Insertar el impuesto del item
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public String insertarImpuestoItemCXPDocMigracion(CXPImpItem impItem) throws SQLException {

        StringStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_INSERTARIMPPORITEM";
        try {
            ps = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
            ps.setString(1, impItem.getDstrct());
            ps.setString(2, impItem.getProveedor());
            ps.setString(3, impItem.getTipo_documento());
            ps.setString(4, impItem.getDocumento());
            ps.setString(5, impItem.getItem());
            ps.setString(6, impItem.getCod_impuesto());
            ps.setDouble(7, impItem.getPorcent_impuesto());
            ps.setDouble(8, impItem.getVlr_total_impuesto());
            ps.setDouble(9, impItem.getVlr_total_impuesto_me());
            ps.setString(10, impItem.getCreation_user());
            ps.setString(11, impItem.getCreation_user());
            ps.setString(12, "");

            return ps.getSql();//JJCastro fase2
        } catch (Exception e) {
            throw new SQLException("ERROR DURANTE buscarDocsRelacionados" + e.getMessage());
        } finally {
            if (ps != null) {
                try {
                    ps = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
    }

    /**
     * Insertar los impuestos del documento
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public String insertarImpuestoCXPDocMigracion(CXPImpDoc impDoc) throws SQLException {

        StringStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_INSERTARIMPDOC";//JJCastro fase2
        try {
            ps = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
            ps.setString(1, impDoc.getDstrct());
            ps.setString(2, impDoc.getProveedor());
            ps.setString(3, impDoc.getTipo_documento());
            ps.setString(4, impDoc.getDocumento());
            ps.setString(5, impDoc.getCod_impuesto());
            ps.setDouble(6, impDoc.getPorcent_impuesto());
            ps.setDouble(7, impDoc.getVlr_total_impuesto());
            ps.setDouble(8, impDoc.getVlr_total_impuesto_me());
            ps.setString(9, impDoc.getCreation_user());
            ps.setString(10, impDoc.getCreation_user());
            ps.setString(11, "");

            return ps.getSql();
        } catch (Exception e) {
            throw new SQLException("ERROR DURANTE buscarDocsRelacionados" + e.getMessage());
        } finally {
            if (ps != null) {
                try {
                    ps = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
    }

    /**
     * Insertar Cabecera
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public String insertarCXPDocMigracion(CXP_Doc doc) throws SQLException {
        Connection con = null;
        StringStatement st = null;
        ResultSet rs = null;
        String query = "SQL_INSERT_DOCUMENTO_MIG";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, doc.getDstrct());
            st.setString(2, doc.getProveedor());
            st.setString(3, doc.getTipo_documento());
            st.setString(4, doc.getDocumento());
            st.setString(5, doc.getDescripcion());
            st.setString(6, doc.getAgencia());
            st.setString(7, doc.getHandle_code());
            st.setString(8, doc.getId_mims());
            st.setString(9, doc.getAprobador());
            st.setString(10, doc.getUsuario_aprobacion());
            st.setInt(11, doc.getPlazo());
            st.setString(12, doc.getBanco());
            st.setString(13, doc.getSucursal());
            st.setString(14, doc.getMoneda());
            st.setDouble(15, doc.getVlr_neto());
            st.setDouble(16, doc.getVlr_neto_me());
            st.setDouble(17, doc.getTasa());
            st.setString(18, doc.getMoneda_banco());
            st.setString(19, doc.getClase_documento());
            st.setString(20, doc.getCreation_user());
            st.setDouble(21, doc.getVlr_saldo());
            st.setDouble(22, doc.getVlr_saldo_me());
            return st.getSql();
        } catch (Exception e) {
            throw new SQLException("ERROR DURANTE buscarDocsRelacionados" + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
    }
    /*Modificado Igomez 22 Febrero*/

    /**
     * Metodo: BuscarItemFactura, busca los datos de la factura
     * @autor : Ing. Ivan Gomez
     * @param : String distrito, String codigo del proveedor y numero de la factura
     * @version : 1.0
     */
    public Vector BuscarItemFactura(CXP_Doc factura, Vector vTipImpuestos) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        PreparedStatement st2 = null;
        ResultSet rs2 = null;
        PreparedStatement st3 = null;
        ResultSet rs3 = null;
        Vector vItems = new Vector();
        String agencia = factura.getAgencia().equals("OP") ? "BQ" : factura.getAgencia();
        Connection con = null;

        try {
            con = this.conectarJNDI("SQL_IMP_X_ITEM");
            if (con != null) {
                String SQL_IMP_X_ITEM = this.obtenerSQL("SQL_IMP_X_ITEM");
                String SQL_ITEM_FACTURA = this.obtenerSQL("SQL_ITEM_FACTURA");
                st3 = con.prepareStatement(SQL_IMP_X_ITEM);
                st = con.prepareStatement(SQL_ITEM_FACTURA);
                st.setString(1, factura.getDstrct());
                st.setString(2, factura.getProveedor());
                st.setString(3, factura.getTipo_documento());
                st.setString(4, factura.getDocumento());
                rs = st.executeQuery();
                double totalFac = 0;
                while (rs.next()) {
                    String iva = "";
                    String riva = "";
                    String rica = "";
                    String rfte = "";
                    String[] codigos = {"-", "--", "---", "---", "----"};
                    CXPItemDoc item = new CXPItemDoc();
                    item.setConcepto(rs.getString("concepto"));
                    item.setDescripcion(rs.getString("descripcion"));
                    item.setCodigo_cuenta(rs.getString("codigo_cuenta"));
                    //System.out.println("CUENTA -->"+item.getCodigo_cuenta() );
                    if (item.getCodigo_cuenta().length() == 13) {
                        codigos[0] = item.getCodigo_cuenta().substring(0, 1);
                        codigos[1] = item.getCodigo_cuenta().substring(1, 3);
                        codigos[2] = item.getCodigo_cuenta().substring(3, 6);
                        codigos[3] = item.getCodigo_cuenta().substring(6, 9);
                        codigos[4] = item.getCodigo_cuenta().substring(9, 13);
                    }

                    if ((item.getCodigo_cuenta().length() == 12) && (item.getCodigo_cuenta().substring(0, 8).equals("28150502"))) {
                        item.setRee("C");
                        item.setRef3(item.getCodigo_cuenta().substring(8, item.getCodigo_cuenta().length()));
                        item.setRef4("");
                    } else if ((item.getCodigo_cuenta().length() == 14) && (item.getCodigo_cuenta().substring(0, 10).equals("1370100121"))) {
                        item.setRee("P");
                        item.setRef3("");
                        item.setRef4(item.getCodigo_cuenta().substring(10, item.getCodigo_cuenta().length()));
                    } else {
                        item.setRee("");
                        item.setRef3("");
                        item.setRef4("");
                    }
                    item.setCodigos(codigos);
                    item.setCodigo_abc(rs.getString("codigo_abc"));
                    item.setPlanilla(rs.getString("planilla"));
                    item.setVlr_me(rs.getDouble("vlr_me"));

                    double VlrTotal = rs.getDouble("vlr_me");
                    item.setTipcliarea(rs.getString("tipcliarea"));
                    item.setCodcliarea(rs.getString("codcliarea"));
                    item.setItem(rs.getString("item"));
                    item.setDescliarea("");
                    //Ivan Gomez 22 julio 2006
                    item.setAuxiliar(rs.getString("auxiliar"));
                    String vec[] = item.getAuxiliar().split("-");
                    if (vec.length > 1) {
                        item.setTipoSubledger(vec[0]);
                        item.setAuxiliar(vec[1]);
                    } else {
                        item.setTipoSubledger("");
                    }

                    //st2 = crearPreparedStatement("SQL_TIPO_IMPUESTOS");
                    //rs2 = st2.executeQuery();
                    Vector vImpuestosPorItem = new Vector();

                    for (int x = 0; x < vTipImpuestos.size(); x++) {
                        String ag = agencia;
                        String imp = (String) vTipImpuestos.elementAt(x);
                        ag = imp.equals("RICA") ? ag : "";
                        CXPImpItem impuestoItem = new CXPImpItem();
                        st3.clearParameters();
                        st3.setString(1, factura.getDstrct());
                        st3.setString(2, factura.getProveedor());
                        st3.setString(3, factura.getTipo_documento());
                        st3.setString(4, factura.getDocumento());
                        st3.setString(5, item.getItem());
                        //st3.setString(6,ag );
                        st3.setString(6, imp);
                        //System.out.println("SQL- "+ st3.toString());
                        rs3 = st3.executeQuery();
                        if (rs3.next()) {
                            impuestoItem.setCod_impuesto(rs3.getString("cod_impuesto"));
                            vImpuestosPorItem.add(impuestoItem);

                            if (imp.equals("IVA")) {

                                VlrTotal += rs3.getDouble("vlr_total_impuesto_me");
                                iva = rs3.getString("porcent_impuesto");
                            } else if (imp.equals("RIVA")) {
                                riva = rs3.getString("porcent_impuesto");
                            } else if (imp.equals("RICA")) {
                                rica = rs3.getString("porcent_impuesto");
                            } else if (imp.equals("RFTE")) {
                                rfte = rs3.getString("porcent_impuesto");
                            }
                        } else {
                            impuestoItem.setCod_impuesto("");
                            vImpuestosPorItem.add(impuestoItem);
                        }

                    }
                    //System.out.println("IMP IVA  "+ iva);
                    totalFac += VlrTotal;
                    item.setVlr_total(VlrTotal);
                    item.setIva(iva);

                    item.setPorc_riva(riva);
                    item.setPorc_rica(rica);
                    item.setPorc_rfte(rfte);

                    item.setVItems(vImpuestosPorItem);
                    item.setVCopia(vImpuestosPorItem);
                    vItems.add(item);
                    //item.setVlr_total     (valor_t); tengo que extraer los valores de los impuestos y aplicarlos
                }
                factura.setVlr_total(totalFac);
                factura.setVlr_neto(totalFac);

            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS ITEM " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (rs2 != null) {
                try {
                    rs2.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (rs3 != null) {
                try {
                    rs3.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (st2 != null) {
                try {
                    st2.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (st3 != null) {
                try {
                    st3.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return vItems;
    }

    /**
     * Metodo: BuscarFactura, busca los datos de la factura
     * @autor : Ing. Ivan Gomez
     * @param :String distrito, String codigo del proveedor y numero de la factura
     * @version : 1.0
     */
    public void BuscarFactura(String dstrct, String proveedor, String documento, String tipo_doc) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        factura = null;
        String query = "SQL_BUSCAR_FACTURA";
        try {

            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dstrct);
                st.setString(2, proveedor);
                st.setString(3, tipo_doc);
                st.setString(4, documento);
                rs = st.executeQuery();
                if (rs.next()) {
                    factura = new CXP_Doc();
                    factura.setTipo_documento(rs.getString("tipo_documento"));
                    factura.setDocumento(rs.getString("documento"));
                    factura.setProveedor(rs.getString("proveedor"));
                    factura.setTipo_documento_rel(rs.getString("tipo_documento_rel"));
                    factura.setDocumento_relacionado(rs.getString("documento_relacionado"));
                    factura.setFecha_documento(rs.getString("fecha_documento"));
                    factura.setBanco(rs.getString("banco"));
                    factura.setVlr_neto(rs.getDouble("vlr_neto_me"));
                    factura.setVlr_total(rs.getDouble("vlr_saldo_me"));
                    factura.setMoneda(rs.getString("moneda"));
                    factura.setSucursal(rs.getString("sucursal"));
                    factura.setDescripcion(rs.getString("descripcion"));
                    factura.setObservacion(rs.getString("observacion"));
                    factura.setUsuario_aprobacion(rs.getString("aprobador"));
                    factura.setPlazo(rs.getInt("plazo"));
                    factura.setFecha_contabilizacion(rs.getString("fecha_contabilizacion"));
                    factura.setFecha_aprobacion(rs.getString("fecha_aprobacion"));
                    factura.setAgencia(rs.getString("agencia"));
                    factura.setDstrct(dstrct);
                    factura.setMoneda_banco(rs.getString("moneda_banco"));
                    factura.setValor_saldo_anterior(rs.getDouble("vlr_saldo"));
                    factura.setValor_saldo_me_anterior(rs.getDouble("vlr_saldo_me"));
                    factura.setTotal_neto(rs.getDouble("vlr_saldo_me"));
                    factura.setCorrida(rs.getString("corrida"));
                    //Ivan Dario 282 Octubre 2006
                    factura.setAgenciaBanco(rs.getString("agencia"));
                    factura.setClase_documento(rs.getString("clase_documento"));
                    factura.setAprobador(rs.getString("usuario_aprobacion"));
                    factura.setHandle_code(rs.getString("handle_code"));

                    //Ivan 2007-03-09*****************************
                    factura.setNeto(rs.getDouble("vlr_neto_me"));
                    factura.setSaldo(rs.getDouble("vlr_saldo_me"));
                    /**********************************************/
                    factura.setTipo_nomina(rs.getString("factura_tipo_nomina"));
                    factura.setTipo_referencia_1(rs.getString("tipo_referencia_1"));
                    factura.setReferencia_1(rs.getString("referencia_1"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA FACTURA " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Metodo existeEnPrecheque , Metodo PARA SABER SI UNA FACTURA ESTA DISPONIBLE PARA MODIFICAR,
     * SI ESTA EN PRECHEQUE_DETALLE Y EL REG_STATUS ='' NO SE PUEDE MODIFICAR LA FACTURA
     * tiene notas debitos o notas creditos
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : String dstrct,String proveedor, String tipo_doc,String factura
     * @throws  : SQLException
     * @version : 1.0
     */
    public boolean existeEnPrecheque(String dstrct, String proveedor, String tipo_doc, String factura) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean existe = false;
        String query = "SQL_EXISTE_PRECHEQUE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dstrct);
                st.setString(2, proveedor);
                st.setString(3, tipo_doc);
                st.setString(4, factura);

                rs = st.executeQuery();
                while (rs.next()) {
                    existe = true;
                }

            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE LA CONSULTA SQL_EXISTE_PRECHEQUE" + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return existe;
    }

    /**
     * Getter for property info.
     * @return Value of property info.
     */
    public java.util.Vector getInfo() {
        return info;
    }

    /**
     * Setter for property info.
     * @param info New value of property info.
     */
    public void setInfo(java.util.Vector info) {
        this.info = info;
    }

    /**
     * Obtiene los egresos generados para la factura determinada
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct Distrito
     * @param nit Nit del proveedor
     * @param tipodoc Tipo de Documento
     * @param doc Documento
     * @throws SQLException
     * @version 1.0
     */
    public void getEgresosFactura(String dstrct, String nit, String tipodoc, String doc) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_EGRESOS_FRA";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, dstrct);
                ps.setString(2, nit);
                ps.setString(3, tipodoc);
                ps.setString(4, doc);

                rs = ps.executeQuery();

                //this.vecCxp_doc = new Vector();
                this.info = new Vector();
                while (rs.next()) {
                    Hashtable ht = new Hashtable();
                    ht.put("dstrct", rs.getString("dstrct"));
                    ht.put("document_no", rs.getString("document_no"));
                    ht.put("branch_code", rs.getString("branch_code"));
                    ht.put("bank_account_no", rs.getString("bank_account_no"));
                    ht.put("currency", rs.getString("currency"));
                    ht.put("vlr_for", rs.getString("vlr_for"));

                    //this.vecCxp_doc.add(ht);
                    this.info.add(ht);
                    ht = null;//Liberar Espacio JJCastro
                }


            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE buscarDocsRelacionados" + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Obtiene los egresos pendientes ( precheques) generados para la factura determinada
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct Distrito
     * @param nit Nit del proveedor
     * @param tipodoc Tipo de Documento
     * @param doc Documento
     * @throws SQLException
     * @version 1.0
     */
    public void getEgresosPendientesFactura(String dstrct, String nit, String tipodoc, String doc) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_EGRESOS_PEND_FRA";
        org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, dstrct);
                ps.setString(2, nit);
                ps.setString(3, tipodoc);
                ps.setString(4, doc);
                logger.info("?sql cheques pendientes: " + ps);

                rs = ps.executeQuery();

                //this.vecCxp_doc = new Vector();
                this.info = new Vector();
                while (rs.next()) {
                    Hashtable ht = new Hashtable();
                    ht.put("dstrct", rs.getString("dstrct"));
                    ht.put("document_no", rs.getString("id"));
                    ht.put("branch_code", rs.getString("banco"));
                    ht.put("bank_account_no", rs.getString("sucursal"));
                    ht.put("currency", rs.getString("moneda"));
                    ht.put("vlr_for", rs.getString("valor"));
                    ht.put("agencia", rs.getString("nomagc"));
                    ht.put("beneficiario", rs.getString("beneficiario"));
                    ht.put("nom_beneficiario", rs.getString("nom_beneficiario"));
                    ht.put("creation_user", rs.getString("creation_user"));
                    ht.put("creation_date", rs.getString("fecha_creacion"));

                    //this.vecCxp_doc.add(ht);
                    this.info.add(ht);
                }


            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE buscarDocsRelacionados" + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Metodo:          CuentaModuloCXP 
     * Descripcion :    M�todo que permite verificar si existe o no una cuenta para el modulo 1.
     * @autor :        Ivan Gomez
     * @param:          distrito y numero de la cuenta.
     * @return:         un switch tipo boolean
     * @throws:         SQLException
     * @version :       1.0
     */
    public boolean CuentaModuloCXP(String distrito, String cuenta) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "SQL_SEARCH";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito);
                st.setString(2, cuenta);
                rs = st.executeQuery();
                if (rs.next()) {
                    sw = true;
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE 'existCuenta' - [PlanDeCuentaDAO].. " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return sw;

    }

    /**
     * Muestra el detalle de la factura
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public void detalleItemFactura(CXP_Doc factura, Vector vTipImpuestos) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PreparedStatement st3 = null;
        ResultSet rs3 = null;
        String planilla = "";
        boolean sw = true;
        String query = "SQL_DETALLE_ITEM_FACTURA";
        String query1 = "SQL_IMP_X_ITEM";

        //vecCxp_doc = new Vector();
        this.info = new Vector();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st3 = con.prepareStatement(this.obtenerSQL(query1));//JJCastro fase2

                st.setString(1, factura.getDstrct());
                st.setString(2, factura.getProveedor());
                st.setString(3, factura.getTipo_documento());
                st.setString(4, factura.getDocumento());
                // logger.info("SQL: " + st.toString());
                rs = st.executeQuery();
                String AGE = factura.getAgencia();
                while (rs.next()) {
                    CXPItemDoc item = new CXPItemDoc();
                    item.setConcepto(rs.getString("concepto"));
                    item.setDescripcion(rs.getString("descripcion"));
                    item.setCodigo_cuenta(rs.getString("codigo_cuenta"));
                    item.setCodigo_abc(rs.getString("codigo_abc"));
                    item.setPlanilla(rs.getString("planilla"));
                    item.setVlr_me(rs.getDouble("vlr_me"));
                    item.setVlr_total(rs.getDouble("vlr_me"));
                    item.setTipcliarea(rs.getString("tipcliarea"));
                    item.setCodcliarea(rs.getString("codcliarea"));
                    item.setItem(rs.getString("item"));
                    item.setCodigo_cuenta(rs.getString("codigo_cuenta"));
                    item.setAuxiliar(rs.getString("auxiliar"));
                    if (sw) {
                        planilla = item.getPlanilla();
                        sw = false;
                        AGE = (factura.isOP()) ? BuscarOriPla(planilla) : factura.getAgencia();
                    }
                    //item.setDescliarea("");
                /* IMPUESTOS */
                    double VlrTotal = 0;
                    String iva = "";
                    Vector vImpuestosPorItem = new Vector();
                    for (int x = 0; x < vTipImpuestos.size(); x++) {
                        String imp = (String) vTipImpuestos.elementAt(x);
                        CXPImpItem impuestoItem = new CXPImpItem();
                        impuestoItem.setTipo_impuesto(imp);
                        st3.clearParameters();

                        String AGE_IMP = (imp.equals("RFTE") || imp.equals("IVA") || imp.equals("RIVA")) ? "" : AGE;

                        st3.setString(1, factura.getDstrct());
                        st3.setString(2, factura.getProveedor());
                        st3.setString(3, factura.getTipo_documento());
                        st3.setString(4, factura.getDocumento());
                        st3.setString(5, item.getItem());
                        // st3.setString(6, AGE_IMP  );
                        st3.setString(6, imp);
                        //System.out.println("SQL IMPUESTOS " + st3.toString()); 
                        rs3 = st3.executeQuery();
                        if (rs3.next()) {
                            impuestoItem.setCod_impuesto(rs3.getString("cod_impuesto"));
                            impuestoItem.setVlr_total_impuesto(rs3.getDouble("vlr_total_impuesto_me"));
                            vImpuestosPorItem.add(impuestoItem);

                            if (imp.equals("IVA") && !factura.isOP()) {
                                VlrTotal += rs3.getDouble("vlr_total_impuesto_me");
                                iva = rs3.getString("cod_impuesto");
                            }

                        } else {
                            impuestoItem.setCod_impuesto("");
                            impuestoItem.setVlr_total_impuesto(0);
                            vImpuestosPorItem.add(impuestoItem);
                        }
                    }

                    item.setVlr(item.getVlr_me() + VlrTotal);
                    item.setIva(iva);
                    item.setVItems(vImpuestosPorItem);
                    item.setVCopia(vImpuestosPorItem);
                    /***************/
                    //vecCxp_doc.add(item);
                    this.info.add(item);
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA FACTURA " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (rs3 != null) {
                try {
                    rs3.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (st3 != null) {
                try {
                    st3.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Obtiene el nit de un cliente de una remesa para facturar
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param numrem N�mero de la remesa
     * @throws SQLException
     * @version 1.0
     */
    public String getNitClienteRemesa(String numrem) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_NITCLIENTE_NUMREM";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, numrem);

                rs = ps.executeQuery();

                while (rs.next()) {
                    return rs.getString("nitcli");
                }


            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE getNitClienteRemesa " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return null;
    }

    /**
     * Obtiene la unidad de negocios mas los 3 ultimos digitos del codigo del cliente
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param numrem N�mero de la remesa
     * @throws SQLException
     * @version 1.0
     */
    public String getParteCuentaRemesa(String numrem) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_PARTECUENTA_NUMREM";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, numrem);

                rs = ps.executeQuery();

                while (rs.next()) {
                    return rs.getString("cuenta");
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE getParteCuentaRemesa " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return null;
    }

    /**
     * Obtiene la 1ra remesa no anulada de la oc
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param oc N�mero de la planilla
     * @throws SQLException
     * @version 1.0
     */
    public String obtenerOt(String oc) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_NUMREM";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, oc);
                rs = ps.executeQuery();

                while (rs.next()) {
                    return rs.getString("numrem");
                }


            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE obtenerOt " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return null;
    }

    /**
     * Metodo:          ConsultarCXP_Doc
     * Descriocion :    Funcion publica que nos arroja la informacion de las facturas Asociadas a ua Factura recurrente
     * @autor :         Ing. Juan M. Escand�n P.
     * @param :         distrito, proveedor, tipo de documento y documento
     * @return:         Vector ( retorna un Objeto )
     * @version :       1.0
     */
    public Vector consultarFacturasAsociadasRxp(String distrito, String proveedor, String tipo_documento, String documento) throws SQLException {
        Connection con = null;
        CXP_Doc fac = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_FacturasAsociadasRXP";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito);
                st.setString(2, proveedor);
                st.setString(3, tipo_documento);
                st.setString(4, documento + "_%");

                rs = st.executeQuery();
                vFacturas = new Vector();

                while (rs.next()) {
                    fac = new CXP_Doc();
                    fac = fac.loadCXP_Doc(rs);
                    vFacturas.add(fac);
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL DOCUMENTO DE CUENTAS POR PAGAR " + e.getMessage() + " " + e.getErrorCode());

        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return vFacturas;
    }

    /**
     * Obtiene la unidad de negocios mas los 3 ultimos digitos del codigo del cliente
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct Distrito
     * @param nit Nit del proveedor
     * @param doc Numero de la factura
     * @param apl Inidica si aplica retenci�n al pago
     * @throws SQLException
     * @version 1.0
     */
    public String aplicaRetencionPago(String dstrct, String nit, String doc, String apl) throws SQLException {
        Connection con = null;
        StringStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_SET_RET_PAGO";
        try {
            ps = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
            ps.setString(1, apl);
            ps.setString(2, dstrct);
            ps.setString(3, nit);
            ps.setString(4, doc);
            return ps.getSql();//JJCastro fase2
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE aplicaRetencionPago " + e.getMessage());
        } finally {
            if (ps != null) {
                try {
                    ps = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
    }

    /**
     * Metodo obtenerAgencias , Metodo para buscar las agencias
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public void consultarDocumento(
            String dstrct,
            String fra,
            String tipo_doc,
            String numpla,
            String agc,
            String banco,
            String sucursal,
            String nit,
            String fechai,
            String fechaf,
            String pagada,
            String placa,
            String ret_pago) throws SQLException {


        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        vecCxp_doc = null;
        vecCxp_doc = new Vector();
        System.gc();
        try {

            con = this.conectarJNDI("SQL_CONSULTA_CXP");//JJCastro fase2
            if (con != null) {
                String sql = this.obtenerSQL("SQL_CONSULTA_CXP");
                sql = (tipo_doc.length() != 0) ? sql.replaceAll("#0", " AND fra.tipo_documento = '" + tipo_doc + "' ") : sql.replaceAll("#0", " AND fra.tipo_documento IN ('FAP','ND','NC') ");
                sql = (fra.length() != 0) ? sql.replaceAll("#1", " AND fra.documento = '" + fra + "' ") : sql.replaceAll("#1", "");
                sql = (nit.length() != 0) ? sql.replaceAll("#2", " AND fra.proveedor = '" + nit + "' ") : sql.replaceAll("#2", "");
                sql = (numpla.length() != 0) ? sql.replaceAll("#3", " AND pla.numpla = '" + numpla + "'") : sql.replaceAll("#3", "");
                sql = (agc.length() != 0) ? sql.replaceAll("#4", " AND fra.agencia = '" + agc + "' ") : sql.replaceAll("#4", "");
                sql = (banco.length() != 0) ? sql.replaceAll("#5", " AND fra.banco = '" + banco + "' ") : sql.replaceAll("#5", "");
                sql = (sucursal.length() != 0) ? sql.replaceAll("#6", " AND fra.sucursal = '" + sucursal + "' ") : sql.replaceAll("#6", "");
                sql = (fechai.length() != 0 && fechaf.length() != 0) ? sql.replaceAll("#7", " AND fra.fecha_documento BETWEEN '" + fechai + "' AND '" + fechaf + "' ") : sql.replaceAll("#7", "");
                if (tipo_doc.equals("FAP")) {
                    sql = (pagada.length() != 0)
                            ? (pagada.equals("S")) ? sql.replaceAll("#8", " AND fra.vlr_saldo = 0 ") : sql.replaceAll("#8", " AND fra.vlr_saldo != 0 ")
                            : sql.replaceAll("#8", "");
                } else {
                    sql = sql.replaceAll("#8", "");
                }
                sql = (placa.length() != 0) ? sql.replaceAll("#9", " AND pla.plaveh in ( " + placa + " )") : sql.replaceAll("#9", "");
                sql = (placa.length() != 0) ? sql.replaceAll("#COND", " INNER ") : sql.replaceAll("#COND", " LEFT ");
                sql = (ret_pago.length() != 0) ? sql.replaceAll("#RET_PAGO", " AND fra.ret_pago = '" + ret_pago + "' ") : sql.replaceAll("#RET_PAGO", "");
                st = con.prepareStatement(sql);
                st.setString(1, dstrct);
                org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
                logger.info("? consulta cxp: " + st);
                System.out.println("Query consulta cxp" + st.toString());
                rs = st.executeQuery();
                while (rs.next()) {
                    factura = new CXP_Doc();
                    factura.setDstrct(rs.getString("dstrct"));

                    factura.setProveedor(rs.getString("proveedor"));
                    factura.setNomProveedor(rs.getString("nomprov"));
                    factura.setNumpla(rs.getString("numpla"));

                    factura.setDocumento(rs.getString("documento"));
                    factura.setTipo_documento(rs.getString("tipo_documento"));
                    factura.setDocumento_relacionado(rs.getString("documento_relacionado"));
                    factura.setTipo_documento_rel(rs.getString("tipo_documento_rel"));
                    factura.setFecha_documento(rs.getString("fecha_documento"));
                    factura.setDescripcion(rs.getString("descripcion"));
                    factura.setObservacion(rs.getString("observacion"));
                    factura.setVlr_saldo(rs.getDouble("vlr_saldo"));
                    factura.setVlr_saldo_me(rs.getDouble("vlr_saldo_me"));
                    factura.setCorrida(rs.getString("corrida"));
                    factura.setCheque(rs.getString("cheque"));

                    factura.setMoneda(rs.getString("moneda"));
                    factura.setVlr_neto(rs.getDouble("vlr_neto"));
                    factura.setVlr_neto_me(rs.getDouble("vlr_neto_me"));

                    factura.setAgencia(rs.getString("nomagc"));
                    factura.setBanco(rs.getString("banco"));
                    factura.setSucursal(rs.getString("sucursal"));

                    factura.setCreation_user(rs.getString("creation_user"));
                    factura.setFecha_vencimiento(rs.getString("fecha_vencimiento"));
                    factura.setUltima_fecha_pago(rs.getString("ultima_fecha_pago"));

                    factura.setDocumentos_relacionados(rs.getString("relacionados"));//Osvaldo

                    factura.setFecha_aprobacion(rs.getString("fecha_aprobacion"));
                    factura.setAprobador(rs.getString("aprobador"));
                    factura.setUsuario_aprobacion(rs.getString("usuario_aprobacion"));

                    factura.setUsuario_contabilizo(rs.getString("usuario_contabilizo"));
                    factura.setFecha_contabilizacion(rs.getString("fecha_contabilizacion"));
                    factura.setTransaccion(rs.getInt("transaccion"));
                    //System.out.println("en el dao  transaccion "+factura);
                    factura.setPeriodo(rs.getString("periodo"));

                    if (factura.getCorrida().compareTo("NO REGISTRA") == 0) {
                        /* Tipo de Pago - Corrida */
                        factura.setProv_banco_transfer(rs.getString("corr_banco_transfer"));
                        factura.setProv_cedula_cuenta(rs.getString("corr_cedula_cuenta"));
                        factura.setProv_nombre_cuenta(rs.getString("corr_nombre_cuenta"));
                        factura.setProv_no_cuenta(rs.getString("corr_no_cuenta"));
                        factura.setProv_suc_transfer(rs.getString("corr_suc_transfer"));
                        factura.setProv_tipo_cuenta(rs.getString("corr_tipo_cuenta"));
                        factura.setProv_tipo_pago(rs.getString("corr_tipo_pago"));
                    } else {
                        /* Tipo de Pago - Proveedor */
                        factura.setProv_banco_transfer(rs.getString("prov_banco_transfer"));
                        factura.setProv_cedula_cuenta(rs.getString("prov_cedula_cuenta"));
                        factura.setProv_no_cuenta(rs.getString("prov_no_cuenta"));
                        factura.setProv_nombre_cuenta(rs.getString("prov_nombre_cuenta"));
                        factura.setProv_suc_transfer(rs.getString("prov_suc_transfer"));
                        factura.setProv_tipo_cuenta(rs.getString("prov_tipo_cuenta"));
                        factura.setProv_tipo_pago(rs.getString("prov_tipo_pago"));
                    }
                    //idevia
                    factura.setClase_documento(rs.getString("clase_documento"));
                    factura.setReg_status(rs.getString("reg_status"));
                    factura.setUsuario_anulo(rs.getString("usuario_anulo"));
                    factura.setFecha_anulacion(rs.getString("fecha_anulacion"));
                    factura.setTransaccion_anulacion(rs.getInt("transaccion_anulacion"));

                    //AMATURANA 07.02.2007
                    factura.setEgresos_relacionados(rs.getString("egresos_rel"));

                    //AMATURANA 15.02.2007
                    factura.setCreation_date(rs.getString("creation_date_fra"));
                    factura.setCreation_user(rs.getString("creation_user"));
                    factura.setVlr_bruto_me(rs.getDouble("vlr_bruto_me"));
                    factura.setVlr_bruto_ml(rs.getDouble("vlr_bruto_ml"));

                    //AMATURANA 24.02.2007
                    factura.setEgresos_pendientes(rs.getString("egresos_pend"));

                    //AMATURANA 10.03.2007
                    factura.setPlaca(rs.getString("plaveh"));
                    factura.setRetencion_pago(rs.getString("ret_pago"));

                    factura.setFecha_contabilizacion_anulacion(rs.getString("fecha_contabilizacion_anulacion"));
                    vecCxp_doc.addElement(factura);
                }
                System.gc();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CONSULTA SQL_BUSCAR_DOCUMENTOS" + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Obtiene el propietario de una placa
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param numpla N�mero de la planilla
     * @throws SQLException
     * @version 1.0
     */
    public String propietarioPlacaPlanilla(String numpla) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_PROP_PLACA";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, numpla);
                rs = ps.executeQuery();

                if (rs.next()) {
                    return rs.getString("nitpro");
                }


            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE propietarioPlacaPlanilla " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return null;
    }

    /**
     * Obtiene el propietario de una placa
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param numpla N�mero de la planilla
     * @throws SQLException
     * @version 1.0
     */
    public boolean existeAgenciaContable(String agecont) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_EXISTE_AGECONT";//JJCastro fase2
        boolean existe = false;
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, agecont);
                rs = ps.executeQuery();
                if (rs.next()) {
                    existe = true;
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE existeAgenciaContable " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return existe;
    }

    /**
     * Obtiene el propietario de una placa
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param numpla N�mero de la planilla
     * @throws SQLException
     * @version 1.0
     */
    public boolean existeCuentaContable(String dstrct, String cuenta) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_EXISTE_CUENTA";
        boolean existe = false;
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, dstrct);
                ps.setString(2, cuenta);

                rs = ps.executeQuery();

                if (rs.next()) {
                    existe = true;
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE existeAgenciaContable " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return existe;
    }

    /**
     * Verifica si existe la relaci�n OT-OC
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param oc N�mero de la planilla
     * @param ot N�mero de la remesa
     * @throws SQLException
     * @version 1.0
     */
    public boolean relacionOtOc(String oc, String ot) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_RELACION_OT_OC";
        boolean existe = false;
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, ot);
                ps.setString(2, oc);
                rs = ps.executeQuery();

                if (rs.next()) {
                    existe = true;
                }


            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE relacionOtOc " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return existe;
    }

    /**
     * Verifica si existe la OT
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param ot N�mero de la remesa
     * @throws SQLException
     * @version 1.0
     */
    public boolean existeOT(String ot) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean existe = false;
        String query = "SQL_EXISTE_OT";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, ot);

                rs = ps.executeQuery();

                if (rs.next()) {
                    existe = true;
                }


            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE existeOT " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return existe;
    }

    /**
     * Verifica si existe la OC
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param oc N�mero de la planilla
     * @throws SQLException
     * @version 1.0
     */
    public boolean existeOC(String oc) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_EXISTE_OC";
        boolean existe = false;

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, oc);
                rs = ps.executeQuery();

                if (rs.next()) {
                    existe = true;
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE existeOC " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return existe;
    }

    /**
     * Verifica si existe la placa
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param placa Placa
     * @throws SQLException
     * @version 1.0
     */
    public boolean existePlaca(String placa) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean existe = false;
        String query = "SQL_EXISTE_PLACA";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, placa);
                rs = ps.executeQuery();

                if (rs.next()) {
                    existe = true;
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE existePlaca " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return existe;
    }

    /**
     * Verifica si existe la relaci�n Placa-OC
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param oc N�mero de la planilla
     * @param placa N�mero de la placa
     * @throws SQLException
     * @version 1.0
     */
    public boolean relacionPlacaOc(String oc, String plaveh) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_RELACION_OC_PLACA";
        boolean existe = false;
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, oc);
                ps.setString(2, plaveh);
                rs = ps.executeQuery();

                if (rs.next()) {
                    existe = true;
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE relacionPlacaOc " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return existe;
    }

    /**
     * Muestra el detalle de la factura
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public void detalleFactura(String dstrct, String proveedor, String documento, String tipo_doc) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        factura = null;
        String query = "SQL_DETALLE_FACTURA";
        try {

            org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dstrct);
                st.setString(2, proveedor);
                st.setString(3, tipo_doc);
                st.setString(4, documento);
                rs = st.executeQuery();
                if (rs.next()) {
                    factura = new CXP_Doc();
                    //Jescandon 09/04/07
                    factura.setReg_status(rs.getString("reg_status"));//Set Reg Status 

                    factura.setTipo_documento(rs.getString("tipo_documento"));
                    factura.setDocument_name(rs.getString("tipo_documento_des"));
                    factura.setDocumento(rs.getString("documento"));
                    factura.setProveedor(rs.getString("proveedor"));
                    factura.setTipo_documento_rel(rs.getString("tipo_documento_rel"));
                    factura.setDocumento_relacionado(rs.getString("documento_relacionado"));
                    factura.setDocumento_rel_name(rs.getString("tipo_documento_rel_des"));
                    factura.setFecha_documento(rs.getString("fecha_documento"));
                    factura.setBanco(rs.getString("banco"));
                    factura.setVlr_neto_me(rs.getDouble("vlr_neto_me"));
                    factura.setVlr_total(rs.getDouble("vlr_saldo_me"));
                    factura.setMoneda(rs.getString("moneda"));
                    factura.setSucursal(rs.getString("sucursal"));
                    factura.setDescripcion(rs.getString("descripcion"));
                    factura.setObservacion(rs.getString("observacion"));
                    factura.setUsuario_aprobacion(rs.getString("aprobador"));
                    factura.setPlazo(rs.getInt("plazo"));
                    factura.setFecha_contabilizacion(rs.getString("fecha_contabilizacion"));
                    factura.setFecha_aprobacion(rs.getString("fecha_aprobacion"));
                    factura.setAgencia(rs.getString("agencia"));
                    factura.setDstrct(dstrct);
                    factura.setMoneda_banco(rs.getString("moneda_banco"));
                    factura.setValor_saldo_anterior(rs.getDouble("vlr_saldo"));
                    factura.setValor_saldo_me_anterior(rs.getDouble("vlr_saldo_me"));
                    factura.setCorrida(rs.getString("corrida"));
                    factura.setNomProveedor(rs.getString("nomprov"));
                    factura.setClase_documento(rs.getString("clase_documento"));
                    factura.setDstrct(rs.getString("dstrct"));
                    //logger.info("detalleFactura-->DSTRCT: " + rs.getString("dstrct"));
                    factura.setHandle_code(rs.getString("handle_code"));
                    factura.setBeneficiario(rs.getString("beneficiario"));
                    factura.setVlr_bruto_me(rs.getDouble("vlr_bruto_me"));//AMATURANA 03.03.2007
                    factura.setDocumentos_relacionados(rs.getString("relacionados"));//Osvaldo
                    factura.setEgresos_relacionados(rs.getString("egresos_rel"));//Osvaldo
                    factura.setFecha_vencimiento(rs.getString("fecha_vencimiento"));
                    factura.setTipodoc(rs.getString("tipo_referencia_1"));
                    factura.setMultiservicio(rs.getString("referencia_1"));

                    if (factura.isOP()) {
                        String planilla = this.BuscarPlanillaOP(factura.getDstrct(), factura.getProveedor(), factura.getTipo_documento(), factura.getDocumento());
                        String origen = this.BuscarOriPla(planilla);

                        //AMATURANA 06.03.2007
                        factura.setPlanilla(planilla);
                        factura.setRemesa(this.obtenerOt(factura.getPlanilla()));

                        factura.setTotalIva(this.buscarTotalIMP(factura.getDstrct(), factura.getProveedor(), factura.getTipo_documento(), factura.getDocumento(), origen, factura.getFecha_documento(), "IVA"));
                        factura.setTotalRiva(this.buscarTotalIMP(factura.getDstrct(), factura.getProveedor(), factura.getTipo_documento(), factura.getDocumento(), origen, factura.getFecha_documento(), "RIVA"));
                        factura.setTotalRica(this.buscarTotalIMP(factura.getDstrct(), factura.getProveedor(), factura.getTipo_documento(), factura.getDocumento(), origen, factura.getFecha_documento(), "RICA"));
                        factura.setTotalRfte(this.buscarTotalIMP(factura.getDstrct(), factura.getProveedor(), factura.getTipo_documento(), factura.getDocumento(), origen, factura.getFecha_documento(), "RFTE"));
                    } else {
                        factura.setTotalIva(this.buscarTotalIMP(factura.getDstrct(), factura.getProveedor(), factura.getTipo_documento(), factura.getDocumento(), factura.getAgencia(), factura.getFecha_documento(), "IVA"));
                        factura.setTotalRiva(this.buscarTotalIMP(factura.getDstrct(), factura.getProveedor(), factura.getTipo_documento(), factura.getDocumento(), factura.getAgencia(), factura.getFecha_documento(), "RIVA"));
                        factura.setTotalRica(this.buscarTotalIMP(factura.getDstrct(), factura.getProveedor(), factura.getTipo_documento(), factura.getDocumento(), factura.getAgencia(), factura.getFecha_documento(), "RICA"));
                        factura.setTotalRfte(this.buscarTotalIMP(factura.getDstrct(), factura.getProveedor(), factura.getTipo_documento(), factura.getDocumento(), factura.getAgencia(), factura.getFecha_documento(), "RFTE"));
                    }

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA FACTURA " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Metodo updateBanco, permite actualizar el banco de una factura
     * recibe por parametro
     * @autor : Ing. Ivan Dario Gomez
     * @param : String distrito, String proveedor, String tipo_doc, String documento, String banco,String sucursal,String moneda
     * @version : 1.0
     */
    public void updateBanco(String distrito, String proveedor, String tipo_doc, String documento, String banco, String sucursal, String fecha_vencimiento) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String moneda = "";
        String agencia = "";
        String query = "SQL_UPDATE_BANCO";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                String[] mon_age = getMonedaBanco(banco, sucursal).split(",");
                moneda = mon_age[0];
                agencia = mon_age[1];
                st.setString(1, banco);
                st.setString(2, sucursal);
                st.setString(3, moneda);
                st.setString(4, fecha_vencimiento);
                st.setString(5, agencia);
                st.setString(6, distrito);
                st.setString(7, proveedor);
                st.setString(8, tipo_doc);
                st.setString(9, documento);
                st.executeUpdate();
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL SQL_UPDATE_BANCO " + e.getMessage() + "" + e.getErrorCode());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Metodo getMonedaBanco, retorna la moneda del banco - sucursal
     * recibe por parametro
     * @autor : Ing. Ivan Dario Gomez
     * @param : String banco,String sucursal
     * @version : 1.0
     */
    public String getMonedaBanco(String banco, String sucursal) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String moneda = "";
        String query = "SQL_GET_MONEDA";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, banco);
                st.setString(2, sucursal);

                rs = st.executeQuery();
                if (rs.next()) {
                    moneda = rs.getString("currency") != null ? rs.getString("currency") : "";
                    moneda += "," + rs.getString("agency_id");
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL SQL_GET_MONEDA " + e.getMessage() + "" + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return moneda;
    }

    /**
     * Metodo: BuscarFactura, busca los datos de la factura
     * @autor : Ing. Ivan Gomez
     * @param :String distrito, String codigo del proveedor y numero de la factura
     * @version : 1.0
     */
    public String BuscarClaseDocumento(String dstrct, String proveedor, String documento, String tipo_doc) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String clase = "";
        String query = "SQL_BUSCAR_FACTURA";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dstrct);
                st.setString(2, proveedor);
                st.setString(3, tipo_doc);
                st.setString(4, documento);
                rs = st.executeQuery();
                if (rs.next()) {
                    clase = rs.getString("clase_documento");

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA CLASE DOCUMENTO " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return clase;
    }

    /**
     * Metodo que Inserta un Documento por pagar en el Sistema -- Modificado para que retorne el SQL.
     * @autor.......David Lamadrid
     * @param.......CXDoc doc(Objeto tipo CXPDoc), Vector vItems(Vector de objetos CxPItem),Vector vImpuestos(Vector de Objetos CXPImpDoc).
     * @throws......Existe Documento en el Sistema.
     * @version.....1.0.
     * @return.......
     * @Modificado .... jbarros
     * @Fech modificacion 05/03/2007
     */
    public ArrayList<String> insertarCXPDoc_SQL(CXP_Doc doc, Vector vItems, Vector vImpuestosDoc, String agencia, String tipo) throws Exception {
        String SQL = "";
        //Connection con = null;
        //Statement st = null;
        //PreparedStatement ps = null;
        //ResultSet rs         = null;
        StringStatement ps = null;;
        String SQL_INSERTAROCUMENTO = "";
        String SQL_INSERTARITEMDOCUMENTO = "";
        String SQL_INSERTARIMPPORITEM = "";
        String SQL_INSERTARIMPDOC = "";
        String SQL_UPDATE_NOTA_DEBITO = "";
        String SQL_UPDATE_NOTA_CREDITO = "";
        ArrayList<String> listQuery = new ArrayList<String>();
        System.out.println("comenzando el proceso de insercion");
        try {
            //con = conectar("SQL_INSERTAROCUMENTO");
            //con.setAutoCommit(false);
            //st = con.createStatement();
            SQL_INSERTAROCUMENTO = this.obtenerSQL("SQL_INSERTAROCUMENTO");
            SQL_INSERTARITEMDOCUMENTO = this.obtenerSQL("SQL_INSERTARITEMDOCUMENTO");
            SQL_INSERTARIMPPORITEM = this.obtenerSQL("SQL_INSERTARIMPPORITEM");
            SQL_INSERTARIMPDOC = this.obtenerSQL("SQL_INSERTARIMPDOC");

            //Ivan NOTA DEBITO
            SQL_UPDATE_NOTA_DEBITO = this.obtenerSQL("SQL_UPDATE_NOTA_DEBITO");
            SQL_UPDATE_NOTA_CREDITO = this.obtenerSQL("SQL_UPDATE_NOTA_CREDITO");
            ps = new StringStatement(SQL_INSERTAROCUMENTO, true);
            //System.out.println("despues del ps ");
            //ps = con.prepareStatement(SQL_INSERTAROCUMENTO);
            //System.out.println("distrc "+doc.getDstrct());
            ps.setString(1, doc.getDstrct());
            ps.setString(2, doc.getProveedor());
            ps.setString(3, doc.getTipo_documento());
            ps.setString(4, doc.getDocumento());
            ps.setString(5, doc.getDescripcion());
            ps.setString(6, doc.getAgencia());
            ps.setString(7, doc.getHandle_code());
            ps.setString(8, doc.getId_mims());
            ps.setString(9, doc.getFecha_documento());
            ps.setString(10, doc.getTipo_documento_rel());
            ps.setString(11, doc.getDocumento_relacionado());
            ps.setString(12, doc.getFecha_aprobacion());
            ps.setString(13, doc.getAprobador());
            ps.setString(14, doc.getUsuario_aprobacion());
            ps.setString(15, doc.getFecha_vencimiento());
            ps.setString(16, doc.getUltima_fecha_pago());
            ps.setString(17, doc.getBanco());
            ps.setString(18, doc.getSucursal());
            ps.setString(19, doc.getMoneda());
            ps.setDouble(20, doc.getVlr_neto());
            ps.setDouble(21, doc.getVlr_total_abonos());
            ps.setDouble(22, doc.getVlr_saldo());
            ps.setDouble(23, doc.getVlr_neto_me());
            ps.setDouble(24, doc.getVlr_total_abonos_me());
            ps.setDouble(25, doc.getVlr_saldo_me());
            ps.setDouble(26, doc.getTasa());
            ps.setString(27, doc.getUsuario_contabilizo());
            ps.setString(28, doc.getFecha_contabilizacion());
            ps.setString(29, doc.getUsuario_anulo());
            ps.setString(30, doc.getFecha_anulacion());
            ps.setString(31, doc.getFecha_contabilizacion_anulacion());
            ps.setString(32, doc.getObservacion());
            ps.setDouble(33, doc.getNum_obs_autorizador());
            ps.setDouble(34, doc.getNum_obs_pagador());
            ps.setDouble(35, doc.getNum_obs_registra());
            ps.setString(36, doc.getCreation_user());
            ps.setString(37, doc.getUser_update());
            ps.setString(38, doc.getBase());
            ps.setString(39, doc.getMoneda_banco());
            ps.setString(40, doc.getClase_documento_rel());

            //System.out.println("CONSULTA 1 " + ps);
            //st.addBatch(ps.toString());
            //SQL += ps.getSql() + ";";
            listQuery.add(ps.getSql());
            if (doc.getTipo_documento().equals("ND")) {
                //ps = con.prepareStatement(SQL_UPDATE_NOTA_DEBITO);
                if (!tipo.equals("pago")) {
                    ps = new StringStatement(SQL_UPDATE_NOTA_DEBITO, true);
                    ps.setDouble(1, doc.getVlr_saldo());
                    ps.setDouble(2, doc.getVlr_saldo_me());
                    ps.setDouble(3, doc.getVlr_saldo());
                    ps.setDouble(4, doc.getVlr_saldo_me());

                    ps.setString(5, doc.getDstrct());
                    ps.setString(6, doc.getProveedor());
                    ps.setString(7, doc.getTipo_documento_rel());
                    ps.setString(8, doc.getDocumento_relacionado());
                    //System.out.println("NOTA DEBITO --->"+ ps.toString());
                    //st.addBatch(ps.toString());
                   // SQL += ps.getSql() + ";";
                } 
            } else if (doc.getTipo_documento().equals("NC")) {
                //ps = con.prepareStatement(SQL_UPDATE_NOTA_CREDITO);
                ps = new StringStatement(SQL_UPDATE_NOTA_CREDITO, true);
                ps.setDouble(1, doc.getVlr_saldo());
                ps.setDouble(2, doc.getVlr_saldo_me());
                ps.setDouble(3, doc.getVlr_saldo());
                ps.setDouble(4, doc.getVlr_saldo_me());

                ps.setString(5, doc.getDstrct());
                ps.setString(6, doc.getProveedor());
                ps.setString(7, doc.getTipo_documento_rel());
                ps.setString(8, doc.getDocumento_relacionado());
                //System.out.println("NOTA CREDITO --->"+ ps.toString());
                //st.addBatch(ps.toString());
                //SQL += ps.getSql() + ";";
                listQuery.add(ps.getSql());
            }

            for (int i = 0; i < vItems.size(); i++) {
                CXPItemDoc item = (CXPItemDoc) vItems.elementAt(i);
                if (item.getDescripcion() != null) {
                    //ps = con.prepareStatement(SQL_INSERTARITEMDOCUMENTO);
                    ps = new StringStatement(SQL_INSERTARITEMDOCUMENTO, true);
                    ps.setString(1, item.getDstrct());
                    ps.setString(2, item.getProveedor());
                    ps.setString(3, item.getTipo_documento());
                    ps.setString(4, item.getDocumento());
                    ps.setString(5, item.getItem());
                    ps.setString(6, item.getDescripcion());
                    ps.setDouble(7, item.getVlr());
                    ps.setDouble(8, item.getVlr_me());
                    ps.setString(9, item.getCodigo_cuenta());
                    ps.setString(10, item.getCodigo_abc());
                    ps.setString(11, item.getPlanilla());
                    ps.setString(12, item.getUser_update());
                    ps.setString(13, item.getCreation_user());
                    ps.setString(14, item.getBase());
                    ps.setString(15, item.getCodcliarea());
                    ps.setString(16, item.getTipcliarea());
                    ps.setString(17, item.getConcepto());
                    if (item.getTipoSubledger().equals("")) {
                        ps.setString(18, item.getAuxiliar());
                    } else {
                        ps.setString(18, item.getTipoSubledger() + "-" + item.getAuxiliar());
                    }
                    //SQL += ps.getSql() + ";";
                    listQuery.add(ps.getSql());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("Error en insertarCXPDoc_SQL....... " + e.toString() + e.getMessage());

        } finally {
            if (ps != null) {
                try {
                    ps = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return listQuery;
    }

    /**
     * 
     * @param distrito
     * @param proveedor
     * @param banco
     * @param sucursal
     * @param act_proveedor
     * @return
     * @throws SQLException
     */
    public int updateBancoXProveedor(String distrito, String proveedor, String banco, String sucursal, boolean act_proveedor) throws SQLException {
        PreparedStatement st = null;
        PreparedStatement st2 = null;
        String moneda = "";
        String agencia = "";
        Connection con = null;
        try {
            String SQL_UPDATE_PROVEEDOR = this.obtenerSQL("SQL_UPDATE_PROVEEDOR");
            String SQL_UPDATE_BANCO_X_PROVEEDOR = this.obtenerSQL("SQL_UPDATE_BANCO_X_PROVEEDOR");
            con = this.conectarJNDI("SQL_UPDATE_PROVEEDOR");

            st2 = con.prepareStatement(SQL_UPDATE_PROVEEDOR);
            st = con.prepareStatement(SQL_UPDATE_BANCO_X_PROVEEDOR);

            String[] mon_age = getMonedaBanco(banco, sucursal).split(",");
            moneda = mon_age[0];
            agencia = mon_age[1];
            st.setString(1, banco);
            st.setString(2, sucursal);
            st.setString(3, moneda);
            st.setString(4, agencia);
            st.setString(5, distrito);
            st.setString(6, proveedor);

            if (act_proveedor) {
                st2.setString(1, banco);
                st2.setString(2, sucursal);
                st2.setString(3, banco.equals("CREDITO") ? "T" : "B");
                st2.setString(4, proveedor);
                st2.executeUpdate();
            }
            return st.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR AL SQL_UPDATE_BANCO_X_PROVEEDOR " + e.getMessage() + "" + e.getErrorCode());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * 
     * @param dstrct
     * @param fra
     * @param tipo_doc
     * @param numpla
     * @param agc
     * @param banco
     * @param sucursal
     * @param nit
     * @param fechai
     * @param fechaf
     * @param pagada
     * @param placa
     * @param ret_pago
     * @param iniciox
     * @throws SQLException
     */
    public void consultarDocumento(
            String dstrct,
            String fra,
            String tipo_doc,
            String numpla,
            String agc,
            String banco,
            String sucursal,
            String nit,
            String fechai,
            String fechaf,
            String pagada,
            String placa,
            String ret_pago, String iniciox, String hc, String usuario) throws SQLException, Exception {


        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        vecCxp_doc = null;
        vecCxp_doc = new Vector();
        System.gc();
        try {

            con = this.conectarJNDI("SQL_CONSULTA_CXP");//JJCastro fase2
            if (con != null) {
                String sql = this.obtenerSQL("SQL_CONSULTA_CXP");
                sql = (tipo_doc.length() != 0) ? sql.replaceAll("#0", " AND fra.tipo_documento = '" + tipo_doc + "' ") : sql.replaceAll("#0", " AND fra.tipo_documento IN ('FAP','ND','NC') ");
                sql = (fra.length() != 0) ? sql.replaceAll("#1", " AND fra.documento = '" + fra + "' ") : sql.replaceAll("#1", "");
                sql = (nit.length() != 0) ? sql.replaceAll("#2", " AND fra.proveedor = '" + nit + "' ") : sql.replaceAll("#2", "");
                sql = (agc.length() != 0) ? sql.replaceAll("#4", " AND fra.agencia = '" + agc + "' ") : sql.replaceAll("#4", "");
                sql = (banco.length() != 0) ? sql.replaceAll("#5", " AND fra.banco = '" + banco + "' ") : sql.replaceAll("#5", "");
                sql = (sucursal.length() != 0) ? sql.replaceAll("#6", " AND fra.sucursal = '" + sucursal + "' ") : sql.replaceAll("#6", "");
                sql = (fechai.length() != 0 && fechaf.length() != 0) ? sql.replaceAll("#7", " AND fra.fecha_documento BETWEEN '" + fechai + "' AND '" + fechaf + "' ") : sql.replaceAll("#7", "");

               if (tipo_doc.equals("FAP")) {
                    sql = (pagada.length() != 0)
                            ? (pagada.equals("S")) ? sql.replaceAll("#8", " AND fra.vlr_saldo = 0 ") : sql.replaceAll("#8", " AND fra.vlr_saldo != 0 ")
                            : sql.replaceAll("#8", "");
                } else {
                    sql = sql.replaceAll("#8", "");
                }
                sql = (placa.length() != 0) ? sql.replaceAll("#9", " AND pla.plaveh in ( " + placa + " )") : sql.replaceAll("#9", "");
                sql = (hc.length() != 0) ? sql.replaceAll("#hc", " AND fra.handle_code in ('" + hc + "')") : sql.replaceAll("#hc", "");

                ClientesVerService clvsrv = new ClientesVerService(this.getDatabaseName());
                String perfil = clvsrv.getPerfil(usuario);
                sql = (clvsrv.ispermitted(perfil, "64") != true) ? sql.replaceAll("#restriccion_hc", " AND fra.handle_code != 'NP'") : sql.replaceAll("#restriccion_hc", "");



                sql = (iniciox.length() != 0) ? sql.replaceAll("inix", " AND fra.documento LIKE   '" + iniciox + "%'") : sql.replaceAll("inix", "");

                sql = (placa.length() != 0) ? sql.replaceAll("#COND", " INNER ") : sql.replaceAll("#COND", " LEFT ");
                sql = (ret_pago.length() != 0) ? sql.replaceAll("#RET_PAGO", " AND fra.ret_pago = '" + ret_pago + "' ") : sql.replaceAll("#RET_PAGO", "");
                st = con.prepareStatement(sql);
                st.setString(1, dstrct);
                org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
                logger.info("? consulta cxp: " + st);
                System.out.println("Query consulta cxp" + st.toString());
                rs = st.executeQuery();
                while (rs.next()) {
                    factura = new CXP_Doc();
                    factura.setDstrct(rs.getString("dstrct"));

                    factura.setProveedor(rs.getString("proveedor"));
                    factura.setNomProveedor(rs.getString("nomprov"));
                    factura.setNumpla(rs.getString("numpla"));

                    factura.setDocumento(rs.getString("documento"));
                    factura.setTipo_documento(rs.getString("tipo_documento"));
                    factura.setDocumento_relacionado(rs.getString("documento_relacionado"));
                    factura.setTipo_documento_rel(rs.getString("tipo_documento_rel"));
                    factura.setFecha_documento(rs.getString("fecha_documento"));
                    factura.setDescripcion(rs.getString("descripcion"));
                    factura.setObservacion(rs.getString("observacion"));
                    factura.setVlr_saldo(rs.getDouble("vlr_saldo"));
                    factura.setVlr_saldo_me(rs.getDouble("vlr_saldo_me"));
                    factura.setCorrida(rs.getString("corrida"));
                    factura.setCheque(rs.getString("cheque"));

                    factura.setMoneda(rs.getString("moneda"));
                    factura.setVlr_neto(rs.getDouble("vlr_neto"));
                    factura.setVlr_neto_me(rs.getDouble("vlr_neto_me"));

                    factura.setAgencia(rs.getString("nomagc"));
                    factura.setBanco(rs.getString("banco"));
                    factura.setSucursal(rs.getString("sucursal"));

                    factura.setCreation_user(rs.getString("creation_user"));
                    factura.setFecha_vencimiento(rs.getString("fecha_vencimiento"));
                    factura.setUltima_fecha_pago(rs.getString("ultima_fecha_pago"));

                    factura.setDocumentos_relacionados(rs.getString("relacionados"));//Osvaldo

                    factura.setFecha_aprobacion(rs.getString("fecha_aprobacion"));
                    factura.setAprobador(rs.getString("aprobador"));
                    factura.setUsuario_aprobacion(rs.getString("usuario_aprobacion"));

                    factura.setUsuario_contabilizo(rs.getString("usuario_contabilizo"));
                    factura.setFecha_contabilizacion(rs.getString("fecha_contabilizacion"));
                    factura.setTransaccion(rs.getInt("transaccion"));
                    //System.out.println("en el dao  transaccion "+factura);
                    factura.setPeriodo(rs.getString("periodo"));
                    factura.setTipodoc(rs.getString("tipo_referencia_1"));
                    factura.setMultiservicio(rs.getString("referencia_1"));

                    if (factura.getCorrida().compareTo("NO REGISTRA") == 0) {
                        /* Tipo de Pago - Corrida */
                        factura.setProv_banco_transfer(rs.getString("corr_banco_transfer"));
                        factura.setProv_cedula_cuenta(rs.getString("corr_cedula_cuenta"));
                        factura.setProv_nombre_cuenta(rs.getString("corr_nombre_cuenta"));
                        factura.setProv_no_cuenta(rs.getString("corr_no_cuenta"));
                        factura.setProv_suc_transfer(rs.getString("corr_suc_transfer"));
                        factura.setProv_tipo_cuenta(rs.getString("corr_tipo_cuenta"));
                        factura.setProv_tipo_pago(rs.getString("corr_tipo_pago"));
                    } else {
                        /* Tipo de Pago - Proveedor */
                        factura.setProv_banco_transfer(rs.getString("prov_banco_transfer"));
                        factura.setProv_cedula_cuenta(rs.getString("prov_cedula_cuenta"));
                        factura.setProv_no_cuenta(rs.getString("prov_no_cuenta"));
                        factura.setProv_nombre_cuenta(rs.getString("prov_nombre_cuenta"));
                        factura.setProv_suc_transfer(rs.getString("prov_suc_transfer"));
                        factura.setProv_tipo_cuenta(rs.getString("prov_tipo_cuenta"));
                        factura.setProv_tipo_pago(rs.getString("prov_tipo_pago"));
                    }
                    //idevia
                    factura.setClase_documento(rs.getString("clase_documento"));
                    factura.setReg_status(rs.getString("reg_status"));
                    factura.setUsuario_anulo(rs.getString("usuario_anulo"));
                    factura.setFecha_anulacion(rs.getString("fecha_anulacion"));
                    factura.setTransaccion_anulacion(rs.getInt("transaccion_anulacion"));

                    //AMATURANA 07.02.2007
                    factura.setEgresos_relacionados(rs.getString("egresos_rel"));

                    //AMATURANA 15.02.2007
                    factura.setCreation_date(rs.getString("creation_date_fra"));
                    factura.setCreation_user(rs.getString("creation_user"));
                    factura.setVlr_bruto_me(rs.getDouble("vlr_bruto_me"));
                    factura.setVlr_bruto_ml(rs.getDouble("vlr_bruto_ml"));

                    //AMATURANA 24.02.2007
                    factura.setEgresos_pendientes(rs.getString("egresos_pend"));

                    //AMATURANA 10.03.2007
                    factura.setPlaca(rs.getString("plaveh"));
                    factura.setRetencion_pago(rs.getString("ret_pago"));

                    factura.setFecha_contabilizacion_anulacion(rs.getString("fecha_contabilizacion_anulacion"));
                    vecCxp_doc.addElement(factura);
                }
                System.gc();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CONSULTA SQL_BUSCAR_DOCUMENTOS" + e.getMessage() + " " + e.getErrorCode());
        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Obtiene los documentos relacionados a una factura
     * @param proveedor nit del proveedor
     * @param tipo_documento Tipo de documento a buacar (ND o NC)
     * @param documento_relacionado numero del documento que se va a buscar
     * @param tipo_documento_relacionado tipo de documento que se va a buscar
     * @return ArrayList con los resultados obtenidos
     * @throws Exception 
     */
    public ArrayList<CXP_Doc> buscarDocsRelacionados(String dstrct, String proveedor, String tipo_documento, String documento_relacionado, String tipo_documento_relacionado) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_DOCREL";
        ArrayList<CXP_Doc> documentos = new ArrayList<CXP_Doc>();

        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, dstrct);
            st.setString(2, proveedor);
            st.setString(3, tipo_documento);
            st.setString(4, documento_relacionado);
            st.setString(5, tipo_documento_relacionado);
            rs = st.executeQuery();
            while (rs.next()) {
                CXP_Doc doc = CXP_Doc.loadCXP_Doc(rs);
                documentos.add(doc);
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarDatosCausacion[CreditosBancariosDAO] " + e.toString());
        } finally {
            if (rs != null) {
                try {
                    rs = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }
        return documentos;
    }

    /**
     * Obtiene las facturas pendientes por aprobar por el usuario en session
     *
     * @param usuario
     * @param fechaInicial
     * @param fechaFinal
     * @return Lista de facturas de proveedor
     * @throws SQLException
     */
    public ArrayList<CXP_Doc> buscar_facturas_no_aprobadas(String usuario, String fechaInicial, String fechaFinal, String agencia) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "SQL_FACTURAS_NO_APROBADAS";
        String consulta = "";
        ArrayList<CXP_Doc> cxp_facturas = null;
        int key = 0;
        try {

            con = this.conectarJNDI(sql);
            consulta = this.obtenerSQL(sql);

            if (agencia.equals("TODAS")) {
                consulta = consulta.replaceAll("#AGENCIA#", "");
            } else {
                consulta = consulta.replaceAll("#AGENCIA#", "AND C.agencia = '" + agencia + "'");
            }

            
            st = con.prepareStatement(consulta);

            st.setString(1, usuario);
            st.setString(2, fechaInicial + " 00:00:00");
            st.setString(3, fechaFinal  + " 23:59:59");

            rs = st.executeQuery();
            cxp_facturas = new ArrayList();

            while (rs.next()) {
                CXP_Doc cxp = CXP_Doc.loadCXP_Doc_factura(rs);
                cxp.setKey(key);
                cxp_facturas.add(cxp);
                key++;
            }

        } catch (SQLException e) {
            throw new SQLException("ERROR AL BUSCAR CUENTAS X USUARIO" + e.getMessage() + "" + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return cxp_facturas;
    }

    public String autorizarFactura(String usuario, String dstrct, String proveedor, String tipo_documento, String documento) throws SQLException, Exception {

        //Connection con = null;
        StringStatement st = null;
        String query = "SQL_AUTORIZAR_FACTURA";
        String sql = "";

        try {
            //con = this.conectarJNDI(query);
            //if (con != null) {

                st = new StringStatement(obtenerSQL(query));
                st.setString(usuario);
                st.setString(dstrct);
                st.setString(proveedor);
                st.setString(tipo_documento);
                st.setString(documento);
                sql = st.getSql();
            //}
        } catch (Exception ex) {
            throw new Exception("ERROR en confirmarRetiro[CaptacionInversionistaDAO] " + ex.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return sql;
    }
    
    
    public ArrayList<CXP_Doc> buscarFacturasAutorizadas(String usuario, String fechaInicial, String fechaFinal, String agencia) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "SQL_FACTURA_AUTORIZADAS";
        String consulta = "";
        ArrayList<CXP_Doc> cxp_facturas = null;
        int key = 0;
        try {

            con = this.conectarJNDI(sql);
            consulta = this.obtenerSQL(sql);

            if (agencia.equals("TODAS")) {
                consulta = consulta.replaceAll("#AGENCIA#", "");
            } else {
                consulta = consulta.replaceAll("#AGENCIA#", "AND C.agencia = '" + agencia + "'");
            }


            st = con.prepareStatement(consulta);

            st.setString(1, usuario);
            st.setString(2, fechaInicial+" 00:00:00");
            st.setString(3, fechaFinal+" 23:59:59");

            rs = st.executeQuery();
            cxp_facturas = new ArrayList();

            while (rs.next()) {
                CXP_Doc cxp = CXP_Doc.loadCXP_Doc_factura(rs);
                cxp.setKey(key);
                cxp_facturas.add(cxp);
                key++;
            }

        } catch (SQLException e) {
            throw new SQLException("ERROR AL BUSCAR CUENTAS X USUARIO" + e.getMessage() + "" + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return cxp_facturas;
    }
    
    /**
     * Metodo:          ActualizarSaldosDocumento
     * Descriocion :    restablecer saldos cxp
     * @autor :         Jesus Pinedo
     * @param :         cXP_Doc ( documento de cuentas por pagar )
     * @return:         .
     * @version :       1.0
     */
    public void ActualizarSaldosDocumento(CXP_Doc cXP_Doc) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String query = "UPDATE_SALDOS_CXP_DOC";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cXP_Doc.getUsuario_anulo());
                st.setDouble(2, cXP_Doc.getVlr_total_abonos());
                st.setDouble(3, cXP_Doc.getVlr_total_abonos());
                st.setDouble(4, cXP_Doc.getVlr_saldo());
                st.setDouble(5, cXP_Doc.getVlr_saldo_me());
                st.setString(6, cXP_Doc.getDstrct());
                st.setString(7, cXP_Doc.getProveedor());
                st.setString(8, cXP_Doc.getTipo_documento());
                st.setString(9, cXP_Doc.getDocumento());

                st.executeUpdate();

            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE LA ANULACION DEL DOCUMENTO " + e.getMessage() + "" + e.getErrorCode());
        } finally {//JJCastro fase2
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

    }
    
    /**
     * JAcosta, agosto 2013
     * @param prex prefijo del codigo con 'nextval' se incrementra la serie
     * @return el codigo del documento o vacio
     * @throws Exception 
     */
    public String generar_Id_CXP(String prex) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "FUNCION_CXP_ID";
        String resultado = "";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, prex);
                st.setString(2, "#");
                rs = st.executeQuery();
    
                while (rs.next()) {
                    resultado = rs.getString(1);
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE LA GENERACION NUMERO DEL DOCUMENTO " + e.getMessage() + "" + e.getErrorCode());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
            return resultado;
        }
    }
    
}
