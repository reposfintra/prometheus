/***********************************************************************************
 * Nombre clase : ............... DespachoManualDAO.java                                   *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.                                       *
 * Autor :....................... Ing. David Lamadrid                               *
 * Fecha :....................... 15 de noviembre de 2005, 11:02 AM                   *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  dlamadrid
 */
public class DespachoManualDAO
{
    
    
    /** Creates a new instance of DespachoManualDAO */
    public DespachoManualDAO ()
    {
    }
    
    //private ConductorDAO conDAO;
    private Vector vDespacho;
    private DespachoManual despacho;
    private Vector pc;
    private Vector jus;
    
    private String eliminarDespacho=" update despacho_manual set reg_status = 'A', last_update = 'now()', user_update = ? where dstrct = ? and placa = ? and fecha_despacho = ? ";

    
    private String insertar ="insert into despacho_manual(reg_status,dstrct, placa, fecha_despacho, fecha_salida,conductor, ruta,cliente,carga,creation_date,creation_user,descripcion,justificacion) values('',?,?,?,?,?,?,?,?,now(),?,?,?)";
    private String existeDespacho="select placa from despacho_manual where dstrct=? and placa=? and fecha_despacho=?";
    //private String despacho_id="select *,get_nombrecliente(cliente) as nombreCliente,get_nombrenit(conductor) as nombreConductor from despacho_manual where dstrct=? and placa=? and fecha_despacho=?";
    private String despacho_id="SELECT A.*,B.cliente as nombreCliente, B.nomcond as nombreConductor" +
    " from despacho_manual A " +
    " LEFT JOIN ingreso_trafico B ON (B.planilla = A.numpla)" +
    " where " +
    "	A.dstrct= ? and " +
    "	A.placa= ? and " +
    "	A.fecha_despacho= ?";
    
        private String listaDespacho =
        "SELECT " +
            "d.*, get_nombrecliente(d.cliente) as nombreCliente, get_nombrenit(d.conductor) as nombreConductor " +
        "FROM " +
            "trafico t, despacho_manual d " +
        "WHERE " +
            "t.tipo_despacho = 'M' " +
            "AND t.reg_status != 'A' " +
            "AND t.planilla = d.numpla " +
        "ORDER BY " +
            "d.numpla DESC " ;
   
    private String insertar_trafico="insert into trafico(reg_status,dstrct,planilla,placa,cedcon,cedprop,origen,destino,zona,escolta,caravana,fecha_despacho,pto_control_ultreporte,fecha_ult_reporte,pto_control_proxreporte,fecha_prox_reporte,ult_observacion,last_update,user_update,creation_date,creation_user,base,tipo_despacho,fecha_salida,cel_cond,cliente) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now(),?,now(),?,?,?,?,?,?)";
    private String insertar_i_trafico="insert into ingreso_trafico(reg_status,dstrct,planilla,placa,cedcon,nomcond,cedprop,nomprop,origen,nomorigen,destino,nomdestino,zona,nomzona,escolta,caravana,fecha_despacho,pto_control_ultreporte,nompto_control_ultreporte,fecha_ult_reporte,pto_control_proxreporte,nompto_control_proxreporte,fecha_prox_reporte,ult_observacion,last_update,user_update,creation_date,creation_user,base,demora,via,fecha_salida,cel_cond,cliente,tipo_despacho) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,CAST(? AS TIMESTAMP) + CAST(? AS INTERVAL),?,now(),?,now(),?,?,?,?,?,?,?,?)";

   /**
     * Querys 
     * Autor: Andres Martinez
     */
    private String SQL_GET_VIA = "SELECT VIA,descripcion FROM VIA WHERE ORIGEN=? AND DESTINO=? AND SECUENCIA=?";
    private String SQL_GET_CODZONA = "SELECT ZONA FROM CIUDAD WHERE CODCIU=?";
    private String SQL_GET_NOMCIU = "SELECT nomciu FROM CIUDAD WHERE CODCIU=?";
    private String SQL_GET_TIEMPO_PROX_PTO = "SELECT TIEMPO FROM TRAMO WHERE origin=? AND destination=?";
    private String SQL_GET_JUS = "SELECT descripcion FROM TABLAGEN WHERE TABLE_TYPE='TJUS'";
    
    private String eliminarTrafico="delete from trafico where  planilla=?";
    private String eliminarITrafico="delete from ingreso_trafico where planilla=?";
    
    /* LREALES - 16 AGOSTO 2006 */
    private String SQL_NOMBRE_CONDUCTOR = "SELECT nomcond FROM ingreso_trafico WHERE planilla = ?";
    private String SQL_NOMBRE_CLIENTE = "SELECT cliente FROM ingreso_trafico WHERE planilla = ?";
    
    /**
     * Metodo insertarDespacho (), recibe un objeto tipo DespachoManual seteado y realiza una transaccion
     * para insertar en las tablas de despacho_maunal ,trafico e ingreso_trafcico,
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public boolean insertarDespacho () throws SQLException
    {
        
        boolean boo = true;
        PoolManager poolManager = PoolManager.getInstance ();
        Connection con = poolManager.getConnection ("fintra");
        if (con == null)
            throw new SQLException ("Sin conexion");
        Statement st = null;
        PreparedStatement ps = null;
        ResultSet rs         = null;
        try
        {
            con.setAutoCommit (false);
            st = con.createStatement ();
            
            ps = con.prepareStatement (this.insertar);//inserta en depacho_manual
            ps.setString (1, despacho.getDstrct ());
            ps.setString (2, despacho.getPlaca ());
            ps.setString (3, despacho.getFecha_despacho ());
            ps.setString (4, despacho.getFecha_salida());
            ps.setString (5, despacho.getConductor ());
            ps.setString (6, despacho.getRuta ());
            ps.setString (7, despacho.getCliente ());
            ps.setString (8, despacho.getCarga ());
            ps.setString (9, despacho.getCreation_user ());
            ps.setString (10, despacho.getObservacion());
            ps.setString (11, despacho.getJustificacion());
            ////System.out.println("**** query insertra: "+ps.toString());
            st.execute(ps.toString ());
            //st.addBatch (ps.toString ());
            
       
            
            String origen="";
            String destino="";
           
            String planilla=(this.generarPlanilla (con));
            
            
            
            Vector vPropietario= this.obtenerPropietario (despacho.getPlaca (),con);
            ////System.out.println("Vector "+vPropietario.size());
            
            origen=despacho.getRuta ().substring (0,2);
           
            destino=despacho.getRuta ().substring (2,4);
            
           
            String nomOrigen=this.obtenerCiudad (origen,con);
          
            String nomDestino = this.obtenerCiudad (destino,con);
             
            String cel = this.obtenerCelularConductor(despacho.getConductor());
            
            ps = con.prepareStatement (this.insertar_trafico);//inserta en trafico
            ps.setString (1, "");
            ps.setString (2, despacho.getDstrct ());
            ps.setString (3, ""+planilla);
            ps.setString (4, despacho.getPlaca ());
            ps.setString (5, despacho.getConductor ());            
            ps.setString (6, vPropietario.size() > 0 ? (String) vPropietario.elementAt (0):"" );
            ps.setString (7, origen);
            ps.setString (8, destino);
            ps.setString (9, despacho.getZona());
            ps.setString (10, "");
            ps.setString (11, "");
            ps.setString (12, despacho.getFecha_despacho ());
            ps.setString (13, "");
            ps.setString (14, "0099-01-01 00:00:00");
            ps.setString (15,origen);//punto de control
            ps.setString (16, despacho.getFecha_despacho ());//fecha proximo reporte
            ps.setString (17, "");//ultima observacion
            ps.setString (18, "");
            ps.setString (19, despacho.getCreation_user ());
            ps.setString (20, "COL");
            ps.setString (21, "M");
            ps.setString (22, despacho.getFecha_salida());
            ps.setString (23, cel);
            ps.setString (24, despacho.getCliente());
            ////System.out.println("******** insertar_trafico: "+ps.toString());
            st.addBatch (ps.toString ());
           
            
            String codprox="";
            String codult="";
            for(int i=0;i<pc.size();i++){
                Vector tmp = (Vector)(pc.get(i)); 
              
                if(despacho.getNproxipto().equals(tmp.get(1))){
                       
                        codprox=(String)tmp.get(0);
                      
                }
                if(despacho.getNultimopto().equals(tmp.get(1))){
                       
                        codult=(String)tmp.get(0);
                        
                }
            }
                     
            double hora =0;
            hora=obtenerTiempoProximoPto(codult,codprox);
            
            ps = con.prepareStatement (this.insertar_i_trafico);//inserta en ingreso_trafico
            ps.setString (1, "");
            ps.setString (2, despacho.getDstrct ());
            ps.setString (3, ""+planilla);
            ps.setString (4, despacho.getPlaca ());
            ps.setString (5, despacho.getConductor ());
            ps.setString (6, despacho.getNombreConductor ());
            ps.setString (7, vPropietario.size() > 0 ? (String) vPropietario.elementAt (0) :"");
            ps.setString (8, vPropietario.size() > 0 ? (String) vPropietario.elementAt (1) :"");
            ps.setString (9, origen);
            ps.setString (10, nomOrigen);
            ps.setString (11, destino);
            ps.setString (12, nomDestino);
            ps.setString ( 13, despacho.getZona() );//zona // LREALES -12 agosto 2006
            ps.setString ( 14, despacho.getZona_nombre() );//nomzona // LREALES -12 agosto 2006
            ps.setString (15, "");//escolta
            ps.setString (16, "");//caravana
            ps.setString (17, despacho.getFecha_despacho ());
            ps.setString (18, codult);//puesto de control ultimo reporte*******
            ps.setString (19, despacho.getNultimopto());//nombre puesto de ultimo ultimo reporte
            ps.setString (20, despacho.getFecha_despacho ());//fecha ultimo reporte
            ps.setString (21, codprox);//puesto de control proximo reporte******
            ps.setString (22, despacho.getNproxipto());//nombre puesto de proximo ultimo reporte
            ps.setString (23, despacho.getFecha_despacho ());//fecha proximo reporte *******
            
            ps.setString(24,hora+" HOUR");
            
            ps.setString (25, "");
            ps.setString (26, "");
            ps.setString (27, despacho.getCreation_user ());
            ps.setString (28, "COL");
            ps.setInt (29, 0);
            ps.setString (30, despacho.getRuta());
            ps.setString (31, despacho.getFecha_salida());
            ps.setString (32, cel);
            ////System.out.println("nombrecliente:"+despacho.getNombreCliente());
            ps.setString (33, despacho.getNombreCliente());
            ps.setString (34, "M");
            
            ////System.out.println("******** insertar_i_trafico "+ps.toString());
            st.addBatch (ps.toString ());
            
            st.executeBatch ();
            con.commit ();
            con.setAutoCommit (true);
        }
        catch(SQLException e)
        {   
            boo=false;
            e.printStackTrace();
            if(con!=null)
            {
                con.rollback ();
            }
            
        }
        finally
        {
            if(st!=null) st.close ();
            if(rs!=null) rs.close ();
            poolManager.freeConnection ("fintra",con);
        }
        return boo;
    }
    
        /**
     * Metodo obtenerTiempoProximoPto(),
     * descripcion:Metodo que retorna el tiempo entre dos puestos de control
     * @autor : Ing. Andres Martinez
     * @param : String codigo origen, String codigo destino
     * @version : 1.0
     */
    public double obtenerTiempoProximoPto(String co,String cd)throws SQLException{
        double t=0;
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
      
        
        try
        {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null)
            {
                st = con.prepareStatement (this.SQL_GET_TIEMPO_PROX_PTO);
                st.setString (1, co);
                st.setString (2, cd);
              
                rs = st.executeQuery ();
                
                while (rs.next ())
                {
                    t=rs.getDouble("tiempo");    
                }
            }
        }
        catch(SQLException e)
        {
            throw new SQLException ("ERROR AL ENCONTRAR DOCUMENTO " + e.getMessage ()+"" + e.getErrorCode ());
        }
        finally
        {
            if(st != null)
            {
                try
                {
                    st.close ();
                }
                catch(SQLException e)
                {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null)
            {
                poolManager.freeConnection ("fintra", con );
            }
        }
     
        return t;
    }
    
    
    
    
    
    /**
     * Metodo actualizarDespacho, Metodo que permite la actualizacion de un despacho_manual en la bd
     * @autor : Ing. David Lamadrid
     * @param : String d,String p,String f,String conductor,String ruta,String cliente,String carga
     * @version : 1.0
     */
    private String actualizarDespacho="update despacho_manual set placa=?,conductor=?,ruta=?,cliente=?,carga=? ,user_update =?,last_update=now(),fecha_despacho=? ,fecha_salida=?,justificacion=?,descripcion=? where numpla=?";
    //private String actualizarTrafico ="update trafico         set  placa=?,cedcon=?,origen=?,destino=?,fecha_despacho=?,cedprop=? , user_update =?,last_update=now() where planilla=?";
    //private String actualizarITrafico="update ingreso_trafico set  placa=?,cedcon=?,nomcond=?,origen=?,destino=?,fecha_despacho=? ,cedprop=?,nomprop=? ,user_update=?,last_update=now() where planilla=?";
    
    private String actualizarITrafico ="update ingreso_trafico set reg_status=?,dstrct=?,placa=?,cedcon=?,nomcond=?,cedprop=?,nomprop=?,origen=?,nomorigen=?,destino=?,nomdestino=?,zona=?,nomzona=?,escolta=?,caravana=?,fecha_despacho=?,pto_control_ultreporte=?,nompto_control_ultreporte=?,fecha_ult_reporte=?,pto_control_proxreporte=?,nompto_control_proxreporte=?,fecha_prox_reporte=CAST(? AS TIMESTAMP) + CAST(? AS INTERVAL),ult_observacion=?,last_update=now(),user_update=?,creation_date=now(),creation_user=?,base=?,demora=?,via=?,fecha_salida=?,cel_cond=?,cliente=?,tipo_despacho=? WHERE planilla =? ";

    private String actualizarTrafico="update trafico set reg_status=?,dstrct=?,placa=?,cedcon=?,cedprop=?,origen=?,destino=?,zona=?,escolta=?,caravana=?,fecha_despacho=?,pto_control_ultreporte=?,fecha_ult_reporte=?,pto_control_proxreporte=?,fecha_prox_reporte=?,ult_observacion=?,last_update=now(),user_update=?,creation_date=now(),creation_user=?,base=?,tipo_despacho=?,fecha_salida=?,cel_cond=?,cliente=? WHERE planilla=?";
  
    
    public boolean actualizarDespacho () throws SQLException
    {   boolean boo=true;
        PoolManager poolManager = PoolManager.getInstance ();
        Connection con = poolManager.getConnection ("fintra");
        if (con == null)
            throw new SQLException ("Sin conexion");
        Statement st = null;
        PreparedStatement ps = null;
        ResultSet rs         = null;
        
        try
        {
            con.setAutoCommit (false);
            st = con.createStatement ();
            
          
            ps = con.prepareStatement (this.actualizarDespacho);
            ps.setString (1,despacho.getPlaca());
            
            ps.setString (2,despacho.getConductor());
            ps.setString (3,despacho.getRuta());
            ps.setString (4,despacho.getCliente());
            ps.setString (5,despacho.getCarga());
            ps.setString (6,despacho.getUser_update());
            ps.setString (7,despacho.getFecha_despacho());
            ps.setString (8,despacho.getFecha_salida());
            ps.setString (9,despacho.getJustificacion());
            ps.setString (10,despacho.getObservacion());
            ps.setInt (11,despacho.getNPlanilla());
            st.addBatch (ps.toString ());            
            
        
              Vector vPropietario= this.obtenerPropietario (despacho.getPlaca (),con);
            
              String origen=despacho.getRuta ().substring (0,2);
            
              String destino=despacho.getRuta ().substring (2,4);
           
           
            String nomOrigen=this.obtenerCiudad (origen,con);
           
            String nomDestino = this.obtenerCiudad (destino,con);
            
            String cel = this.obtenerCelularConductor(despacho.getConductor());
            
           
             
            ps = con.prepareStatement (this.actualizarTrafico);
            
            ps.setString (1, "");
            ps.setString (2, despacho.getDstrct ());
            ps.setString (3, despacho.getPlaca ());
            ps.setString (4, despacho.getConductor ());
            ps.setString (5, vPropietario.size() > 0 ? (String) vPropietario.elementAt (0):"" );
            ps.setString (6, origen);
            ps.setString (7, destino);
            ps.setString (8, despacho.getZona());
            ps.setString (9, "");
            ps.setString (10, "");
            ps.setString (11, despacho.getFecha_despacho ());
            ps.setString (12, "");
            ps.setString (13, "0099-01-01 00:00:00");
            ps.setString (14,origen);//punto de control
            ps.setString (15, despacho.getFecha_despacho ());//fecha proximo reporte
            ps.setString (16, "");//ultima observacion
            ps.setString (17, "");
            ps.setString (18, despacho.getCreation_user ());
            ps.setString (19, "COL");
            ps.setString (20, "M");
            ps.setString (21, despacho.getFecha_salida());
            ps.setString (22, cel);
            ps.setString (23, despacho.getCliente());
            ps.setString (24,""+ despacho.getNPlanilla());
            
            st.addBatch (ps.toString ());
             
            
             
            String codprox="";
            String codult="";
            for(int i=0;i<pc.size();i++){
                Vector tmp = (Vector)(pc.get(i)); 
              
                if(despacho.getNproxipto().equals(tmp.get(1))){
                       
                        codprox=(String)tmp.get(0);
                       
                }
                if(despacho.getNultimopto().equals(tmp.get(1))){
                       
                        codult=(String)tmp.get(0);
                        
                }
            }
                   
            double hora =0;
            hora=obtenerTiempoProximoPto(codult,codprox); 
             
            ps = con.prepareStatement (this.actualizarITrafico);
            ps.setString (1, "");
            ps.setString (2, despacho.getDstrct ());
            ps.setString (3, despacho.getPlaca ());
            ps.setString (4, despacho.getConductor ());
            ps.setString (5, despacho.getNombreConductor ());
            ps.setString (6, vPropietario.size() > 0 ? (String) vPropietario.elementAt (0) :"");
            ps.setString (7, vPropietario.size() > 0 ? (String) vPropietario.elementAt (1) :"");
            ps.setString (8, origen);
            ps.setString (9, nomOrigen);
            ps.setString (10, destino);
            ps.setString (11, nomDestino);
            ps.setString (12, despacho.getZona());//zona
            ps.setString (13, "");//nomzona
            ps.setString (14, "");//escolta
            ps.setString (15, "");//caravana
            ps.setString (16, despacho.getFecha_despacho ());
            ps.setString (17, codult);//puesto de control ultimo reporte*******
            ps.setString (18, despacho.getNultimopto());//nombre puesto de ultimo ultimo reporte
            ps.setString (19, despacho.getFecha_despacho ());//fecha ultimo reporte
            ps.setString (20, codprox);//puesto de control proximo reporte******
            ps.setString (21, despacho.getNproxipto());//nombre puesto de proximo ultimo reporte
            ps.setString (22, despacho.getFecha_despacho ());//fecha proximo reporte *******
            
            ps.setString(23,hora+" HOUR");
            
            ps.setString (24, "");
            ps.setString (25, "");
            ps.setString (26, despacho.getCreation_user ());
            ps.setString (27, "COL");
            ps.setInt (28, 0);
            ps.setString (29, despacho.getRuta());
            ps.setString (30, despacho.getFecha_salida());
            ps.setString (31, cel);
            ps.setString (32, despacho.getNombreCliente());
            ps.setString (33, "M");
            ps.setString (34, ""+ despacho.getNPlanilla());
            st.addBatch (ps.toString ());
            
            st.executeBatch ();
            
            con.commit ();
            con.setAutoCommit (true);    
            
        }catch(SQLException e)
        {
            boo=false;
            throw new SQLException ("ERROR AL INSERTAR Despacho" + e.getMessage ()+"" + e.getErrorCode ());
        }finally
        {
            if(st != null)
            {
                try
                {
                    st.close ();
                }
                catch(SQLException e)
                {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null)
            {
                poolManager.freeConnection ("fintra", con );
            }
            
        }
        return boo;
    }
    
    
public void eliminarDespacho (String distrito,String placa,String fechaDespacho,String  planilla) throws SQLException
    {
        PoolManager poolManager = PoolManager.getInstance ();
        Connection con = poolManager.getConnection ("fintra");
        if (con == null)
            throw new SQLException ("Sin conexion");
        Statement st = null;
        PreparedStatement ps = null;
        ResultSet rs         = null;
        try
        {
            
            con.setAutoCommit (false);
            st = con.createStatement ();
          
            ps = con.prepareStatement (this.eliminarDespacho);
            ps.setString (1, despacho.getUser_update());
            ps.setString (2, distrito);
            ps.setString (3, placa);
            ps.setString (4, fechaDespacho);
            st.addBatch (ps.toString ());
            
            ps = con.prepareStatement (this.eliminarITrafico);
            ps.setString(1,planilla);
            st.addBatch (ps.toString ());
            
            ps = con.prepareStatement (this.eliminarTrafico);
            ps.setString(1,planilla);
            st.addBatch (ps.toString ());
            
            
            st.executeBatch ();
            
            con.commit ();
            con.setAutoCommit (true);
            
        }catch(SQLException e)
        {
            throw new SQLException ("ERROR AL INSERTAR Despacho" + e.getMessage ()+"" + e.getErrorCode ());
        }finally
        {
            if(st != null)
            {
                try
                {
                    st.close ();
                }
                catch(SQLException e)
                {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null)
            {
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
  
    
    /**
     * Metodo existeDespacho, Metodo que permite la verificacion del la existencia de un despacho_manual en la bd
     * @autor : Ing. David Lamadrid
     * @param : String distrito,String placa,String fechaDespacho
     * @version : 1.0
     */
    public boolean existeDespacho (String distrito,String placa,String fechaDespacho ) throws SQLException
    {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        
        try
        {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null)
            {
                st = con.prepareStatement (this.existeDespacho);
                st.setString (1, distrito);
                st.setString (2, placa);
                st.setString (3,fechaDespacho);
                ////System.out.println("query: "+st.toString());
                rs = st.executeQuery ();
                
                if (rs.next ()) {
                    sw = true;
                }
            }
        }
        catch(SQLException e) {
            e.printStackTrace();
            throw new SQLException ("ERROR AL ENCONTRAR DOCUMENTO " + e.getMessage ()+"" + e.getErrorCode ());
        }
        finally
        {
            if(st != null)
            {
                try
                {
                    st.close ();
                }
                catch(SQLException e)
                {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null)
            {
                poolManager.freeConnection ("fintra", con );
            }
        }
        return sw;
    }
    
     /**
     * Metodo obtenerCelularConductor(),
     * descripcion:Metodo que retorna el celular del conductor dependiendo de la cedula 
     * @autor : Ing. Andres Martinez
     * @param : String cedula
     * @version : 1.0
     */
    public String obtenerCelularConductor(String cedula) throws SQLException{
        PoolManager poolManager = PoolManager.getInstance ();
        Connection con = poolManager.getConnection ("fintra");
        if (con == null)
            throw new SQLException ("Sin conexion");
        
        PreparedStatement st = null;
        ResultSet rs = null;
        String celular="";
        try
        {
            
            st = con.prepareStatement ("select celular from nit where cedula=?");
            st.setString (1, cedula);
            rs = st.executeQuery ();
            
            while (rs.next ())
            {
                celular= rs.getString ("celular");
            } 
     }catch(SQLException e)
        {
            throw new SQLException ("ERROR AL ENCONTRAR celular " + e.getMessage ()+"" + e.getErrorCode ());
        }
        finally
        {
            if(st != null)
            {
                try
                {
                    st.close ();
                }
                catch(SQLException e)
                {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null)
            {
                poolManager.freeConnection ("fintra", con );
            }
        }
        return celular;
    }
      /**
     * Metodo BuscarPuestoControl(),
     * descripcion:Metodo que busca el puestos de control 
     * @autor : Ing. Andres Martinez
     * @param : String codigo numpla
     * @version : 1.0
     */
    String SQL_GET_PUESTO_CONTROL="SELECT nompto_control_ultreporte FROM ingreso_trafico WHERE planilla=?";
    
    public String BuscarPuestoControl(String numpla) throws SQLException{
        PoolManager poolManager = PoolManager.getInstance ();
        Connection con = poolManager.getConnection ("fintra");
         PreparedStatement ps = null;
        ResultSet rs = null;
        String puesto="";
        try{
             ps = con.prepareStatement(this.SQL_GET_PUESTO_CONTROL);
             ps.setString(1, numpla);
             rs = ps.executeQuery ();
             while (rs.next ())
            {
                puesto= rs.getString ("nompto_control_ultreporte");
            } 
        }
        catch(Exception e){
        e.printStackTrace();
        }
        finally{
            if(ps != null)
            {
                try
                {
                    ps.close ();
                }
                catch(SQLException e)
                {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null)
            {
                poolManager.freeConnection ("fintra", con );
            }
        }
        return puesto;
    }
      /**
     * Metodo BuscarPuestosControl(),
     * descripcion:Metodo que setea el vector de puestos de control 
     * @autor : Ing. Andres Martinez
     * @param : String codigo ruta
     * @version : 1.0
     */
    public void BuscarPuestosControl(String ruta) throws SQLException{
        PoolManager poolManager = PoolManager.getInstance ();
        Connection con = poolManager.getConnection ("fintra");
        pc = new Vector();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try{
            
            if(ruta!=null && ruta.length()>=8){
                ////System.out.println("se metio en el if!!!:"+ruta);
                String origen = ruta.substring(0,2);
                String destino = ruta.substring(2,4);
                ////System.out.println("origen:"+origen+" destino:"+destino);
                String secuencia = ruta.substring(4,8);
                String via = "";
               
                ps = con.prepareStatement(this.SQL_GET_VIA);
                ps.setString(1, origen);
                ps.setString(2, destino);
                ps.setString(3, secuencia);
                rs = ps.executeQuery();
                
                while(rs.next()){
                    via = rs.getString("VIA");
                    
                }
                ////System.out.println("via:"+via);
                int cont = 0;
                for(int i=0;i<via.length();i++){
                    cont++;
                    if(cont==2){
                        
                        Vector fila = new Vector();
                        String codciu = via.substring(i-1,i+1);
                        ////System.out.println("codciu:"+codciu);
                        fila.add(codciu);
                        String nomciu = getNombreCiudad(codciu);
                        ////System.out.println("nomviu:"+nomciu);
                        if(nomciu==null){
                            fila.add(codciu+" [No existe]");
                        }else{
                            fila.add(nomciu);
                            fila.add(getZona(codciu));
                            
                        }
                        pc.add(fila);
                        cont = 0;
                    }
                }
                
            }
            ////System.out.println("pc:"+pc.size());
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL BUSCAR LOS PUESTOS DE CONTROL, "+ex.getMessage()+", "+ex.getErrorCode());
        }
        finally {
            if(ps != null)
            {
                try
                {
                    ps.close ();
                }
                catch(SQLException e)
                {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null)
            {
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
    
      /**
     * Metodo Buscarjustificaciones(),
     * descripcion:Metodo que setea el vector de puestos de control 
     * @autor : Ing. Andres Martinez
     * @param : String codigo ruta
     * @version : 1.0
     */
    public void Buscarjustificaciones() throws SQLException{
        PoolManager poolManager = PoolManager.getInstance ();
        Connection con = poolManager.getConnection ("fintra");
        jus = new Vector();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try{
            
                String des = "";
               
                ps = con.prepareStatement(this.SQL_GET_JUS);
                rs = ps.executeQuery();
                
                while(rs.next()){
                    des = rs.getString("descripcion");
                    jus.add(des);
                    
                }
              
            
            
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL BUSCAR LOS PUESTOS DE CONTROL, "+ex.getMessage()+", "+ex.getErrorCode());
        }
        finally {
            if(ps != null)
            {
                try
                {
                    ps.close ();
                }
                catch(SQLException e)
                {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null)
            {
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
     /**
     * Metodo getZona(), 
     * descripcion:Metodo que retorna la zona de una ciudad
     * @autor : Ing. Andres Martinez
     * @param : String codigo ciudad
     * 
     */
    public String getZona(String codciu)throws SQLException{
        PoolManager poolManager = PoolManager.getInstance ();
        Connection con = poolManager.getConnection ("fintra");
        PreparedStatement ps = null;
        ResultSet rs = null;
        String zona="";
        try{
            
            ps = con.prepareStatement(SQL_GET_CODZONA);
            ps.setString(1, codciu);
            rs = ps.executeQuery();
            while(rs.next()){
                zona = rs.getString(1);
            }
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL BUSCAR LA ZONA DE LA CIUDAD, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return zona;
    }
      /**
     * Metodo getNombreCiudad(),
     * descripcion:Metodo que retorna el nombre de la ciudad dependiendo del codigo de la ciudad
     * @autor : Ing. Andres Martinez
     * @param : String codigo ciudad
     * @version : 1.0
     */
    public String getNombreCiudad(String codciu) throws SQLException {
        
        PoolManager poolManager = PoolManager.getInstance ();
        Connection con = poolManager.getConnection ("fintra");
        PreparedStatement ps = null;
        ResultSet rs = null;
        String nombre=null;
        try{
            ps=con.prepareStatement(SQL_GET_NOMCIU);
            ps.setString(1, codciu);
            rs = ps.executeQuery();
            while(rs.next()){
                nombre = rs.getString(1);
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL BUSCAR EL NOMBRE DE LA CIUDAD, "+ex.getMessage()+", "+ex.getErrorCode());
        }    
        finally
        {
            if(ps != null)
            {
                try
                {
                    ps.close ();
                }
                catch(SQLException e)
                {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null)
            {
                poolManager.freeConnection ("fintra", con );
            }
        }
        return nombre;
    }
    
    /**
     * Metodo obtenerPropietario , Metodo que retorna un vector con el nombre  y la cedula de un propietario dada la placa
     * @autor : Ing. David Lamadrid
     * @param : String placa,Connection con
     * @version : 1.0
     */
    
    public Vector obtenerPropietario (String placa,Connection con) throws SQLException
    {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        Vector vPropietario=new Vector ();
        try
        {
            
            
            st = con.prepareStatement ("select propietario, get_nombrenit(propietario) as nombreP from placa where placa=?");
            st.setString (1, placa);
            rs = st.executeQuery ();
            
            while (rs.next ())
            {
                vPropietario.add (rs.getString ("propietario"));
                vPropietario.add (rs.getString ("nombreP"));
            }
            
        }
        catch(SQLException e)
        {
            throw new SQLException ("ERROR AL ENCONTRAR DOCUMENTO " + e.getMessage ()+"" + e.getErrorCode ());
        }
        finally
        {
            if(st != null)
            {
                try
                {
                    st.close ();
                }
                catch(SQLException e)
                {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
        }
        return vPropietario;
    }
    
    /**
     * Metodo obtenerCiudad , Metodo que retorna el nombre una ciudad dado el nombre de la ciudad
     * @autor : Ing. David Lamadrid
     * @param : String id,Connection con
     * @version : 1.0
     */
    public String obtenerCiudad (String id,Connection con) throws SQLException
    {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        String nombre="";
        try
        {
            
            
            st = con.prepareStatement ("select nomciu from ciudad where codciu=?");
            st.setString (1, id);
            rs = st.executeQuery ();
            
            while (rs.next ())
            {
                nombre= rs.getString ("nomciu");
            }
            
        }
        catch(SQLException e)
        {
            throw new SQLException ("ERROR AL ENCONTRAR DOCUMENTO " + e.getMessage ()+"" + e.getErrorCode ());
        }
        finally
        {
            if(st != null)
            {
                try
                {
                    st.close ();
                }
                catch(SQLException e)
                {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
        }
        return nombre;
    }
    
    
    /**
     * Metodo generarPlanilla , Metodo que retorna un entero con el codigo de la planilla generado por la insercion
     *del despesacho manual ,para la asignacion de ese cadigo en trafico e ingreso_trafico
     * @autor : Ing. David Lamadrid
     * @param : Connection con
     * @version : 1.0
     */
    public String  generarPlanilla (Connection con) throws SQLException
    {
        PreparedStatement st = null;
        ResultSet rs = null;
        String numpla="";
        try
        {
            //st = con.prepareStatement ("select max(numpla) as maximo from despacho_manual");
            
            st = con.prepareStatement ("select numpla from despacho_manual where dstrct=? AND placa=? AND fecha_despacho=?");
            st.setString (1, despacho.getDstrct ());
            st.setString (2, despacho.getPlaca ());
            st.setString (3, despacho.getFecha_despacho ());
            rs = st.executeQuery ();
            if (rs.next ())
            {
                numpla = rs.getString ("numpla");
            }
            
        }
        catch(SQLException e)
        {
            throw new SQLException ("ERROR AL ENCONTRAR DOCUMENTO " + e.getMessage ()+"" + e.getErrorCode ());
        }
        finally
        {
            if(st != null)
            {
                try
                {
                    st.close ();
                }
                catch(SQLException e)
                {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
        }
        return numpla;
    }
    
    /**
     * Metodo despachoPorCodigo , Metodo que setea un objeto tipo DespahoManual dada la llave primaria
     * de despacho manual en la base de daTOS
     * @autor : Ing. David Lamadrid
     * @param : Connection con
     * @version : 1.0
     */
    public void despachoPorCodigo (String distrito,String placa,String fechaDespacho ) throws SQLException
    {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        
        
        try
        {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null)
            {
                st = con.prepareStatement (this.despacho_id);
                st.setString (1, distrito);
                st.setString (2, placa);
                st.setString (3,fechaDespacho);
                rs = st.executeQuery ();
                ////System.out.println ("Busco la consulta por codigo "+st);
                //DespachoManual d = new DespachoManual();
                
                
                while (rs.next ())
                {
                    
                    
                    // d.load (rs);
                    
                   
                   despacho = DespachoManual.load (rs);
                     
                     despacho.setDescripcionRuta(getdescripcionRuta( despacho.getRuta()));
                     
                     
                     vDespacho.add(despacho);
                   
                }
                
                ////System.out.println ("despacho placa "+ despacho.getPlaca ());
            }
        }
        catch(SQLException e)
        {
            throw new SQLException ("ERROR AL ENCONTRAR DOCUMENTO " + e.getMessage ()+"" + e.getErrorCode ());
        }
        finally
        {
            if(st != null)
            {
                try
                {
                    st.close ();
                }
                catch(SQLException e)
                {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null)
            {
                poolManager.freeConnection ("fintra", con );
            }
        }
        
    }
    
    /**
     * Metodo listaDespacho  , Metodo que setea un vector de objetos tipo DespahoManual de regitros
     * de despacho_manual en la base de daTOS
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public void listaDespacho () throws SQLException
    {   
        ////System.out.println("lista despacho service");
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        vDespacho = new Vector ();
       
        
        try
        {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null)
            {
                
                
                st = con.prepareStatement (this.listaDespacho);
                ////System.out.println("query:"+st.toString());
                rs = st.executeQuery ();
                while (rs.next ())
                {
                     despacho = DespachoManual.load (rs);
                     
                     despacho.setDescripcionRuta(getdescripcionRuta( despacho.getRuta()));
                     
                     
                     //this.setDespacho (despacho);
                     vDespacho.add(despacho);
                } 
            }   
        }
        catch(SQLException e)
        {
            throw new SQLException ("ERROR AL ENCONTRAR DOCUMENTO " + e.getMessage ()+"" + e.getErrorCode ());
        }
        finally
        {
            if(st != null)
            {
                try
                {
                    st.close ();
                }
                catch(SQLException e)
                {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null)
            {
                poolManager.freeConnection ("fintra", con );
            }
        }
    }//Fin del Metodo
    
    /**
     * Metodo despachosPorPlaca , Metodo que setea un vector de  objetos tipo DespahoManual dada la placa
     * @autor : Ing. David Lamadrid
     * @param : String placa
     * @version : 1.0
     */
    public void despachosPorPlaca (String placa) throws SQLException
    {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        vDespacho = new Vector ();
        
        
        try
        {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null)
            {
                
                
                st = con.prepareStatement ("select *,get_nombrecliente(cliente) as nombreCliente,get_nombrenit(conductor) as nombreConductor from despacho_manual where usuario_legalisa='' and placa=?");
                st.setString (1, placa);
                ////System.out.println (st);
                rs = st.executeQuery ();
                while (rs.next ())
                {
                    despacho = DespachoManual.load (rs);
                     
                     despacho.setDescripcionRuta(getdescripcionRuta( despacho.getRuta()));
                     
                     
                     vDespacho.add(despacho);
                 }
            }
            
        }
        catch(SQLException e)
        {
            throw new SQLException ("ERROR AL ENCONTRAR DOCUMENTO " + e.getMessage ()+"" + e.getErrorCode ());
        }
        finally
        {
            if(st != null)
            {
                try
                {
                    st.close ();
                }
                catch(SQLException e)
                {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null)
            {
                poolManager.freeConnection ("fintra", con );
            }
        }
    }//Fin del Metodo
    
    
    /**
     * Metodo despachosPorRuta , Metodo que setea un vector de  objetos tipo DespahoManual dada la ruta
     * @autor : Ing. David Lamadrid
     * @param : String ruta
     * @version : 1.0
     */
    public void despachosPorRuta (String ruta) throws SQLException
    {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        vDespacho = new Vector ();
      
        
        try
        {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null)
            {
                
                
                st = con.prepareStatement ("select *,get_nombrecliente(cliente) as nombreCliente,get_nombrenit(conductor) as nombreConductor from despacho_manual where usuario_legalisa='' and ruta=?");
                st.setString (1,ruta);
                ////System.out.println (st);
                rs = st.executeQuery ();
                while (rs.next ())
                {
                   despacho = DespachoManual.load (rs);
                     
                     despacho.setDescripcionRuta(getdescripcionRuta( despacho.getRuta()));
                     
                      vDespacho.add(despacho);
                }
           }
            
        }
        catch(SQLException e)
        {
            throw new SQLException ("ERROR AL ENCONTRAR DOCUMENTO " + e.getMessage ()+"" + e.getErrorCode ());
        }
        finally
        {
            if(st != null)
            {
                try
                {
                    st.close ();
                }
                catch(SQLException e)
                {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null)
            {
                poolManager.freeConnection ("fintra", con );
            }
        }
    }//Fin del Metodo
    
    /**
     * Metodo despachosPorCliente , Metodo que setea un vector de  objetos tipo DespahoManual dado el codigo del cliente
     * @autor : Ing. David Lamadrid
     * @param : String cliente
     * @version : 1.0
     */
    public void despachosPorCliente (String cliente) throws SQLException
    {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        vDespacho = new Vector ();
       
        
        try
        {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null)
            {
                
                
                st = con.prepareStatement ("select *,get_nombrecliente(cliente) as nombreCliente,get_nombrenit(conductor) as nombreConductor from despacho_manual where usuario_legalisa='' and cliente=?");
                st.setString (1, cliente);
                ////System.out.println (st);
                rs = st.executeQuery ();
                while (rs.next ())
                {
                   despacho = DespachoManual.load (rs);
                     
                     despacho.setDescripcionRuta(getdescripcionRuta( despacho.getRuta()));
                     
                     
                     vDespacho.add(despacho);
                }
            }
            
        }
        catch(SQLException e)
        {
            throw new SQLException ("ERROR AL ENCONTRAR DOCUMENTO " + e.getMessage ()+"" + e.getErrorCode ());
        }
        finally
        {
            if(st != null)
            {
                try
                {
                    st.close ();
                }
                catch(SQLException e)
                {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null)
            {
                poolManager.freeConnection ("fintra", con );
            }
        }
    }//Fin del Metodo
    
    /**
     * Getter for property despacho.
     * @return Value of property despacho.
     */
    public com.tsp.operation.model.beans.DespachoManual getDespacho ()
    {
        return despacho;
    }
    
    /**
     * Setter for property despacho.
     * @param despacho New value of property despacho.
     */
    public void setDespacho (com.tsp.operation.model.beans.DespachoManual despacho)
    {
        this.despacho = despacho;
    }
    
    /**
     * Getter for property vDespacho.
     * @return Value of property vDespacho.
     */
    public java.util.Vector getVDespacho ()
    {   
        //////System.out.println("entro dos dao get despacho DESC:"+despacho.getDescripcionRuta());
        return vDespacho;
    }
    
    /**
     * Setter for property vDespacho.
     * @param vDespacho New value of property vDespacho.
     */
    public void setVDespacho (java.util.Vector vDespacho)
    {
        this.vDespacho = vDespacho;
    }
    
    /**
     * Getter for property pc.
     * @return Value of property pc.
     */
    public java.util.Vector getPc() {
        return pc;
    }
    
    /**
     * Setter for property pc.
     * @param pc New value of property pc.
     */
    public void setPc(java.util.Vector pc) {
        this.pc = pc;
    }
    
    
      /**
     * Metodo getdescripcionRuta(),
     * descripcion:Metodo que retorna la descripcion de una ruta
     * @autor : Ing. Andres Martinez
     * @param : String ruta
     * @version : 1.0
     */
    public String getdescripcionRuta(String ruta)throws SQLException{
                
        PoolManager poolManager = PoolManager.getInstance ();
        Connection con = poolManager.getConnection ("fintra");
        pc = new Vector();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String descripcion="";
        try{
            
            if(ruta!=null && ruta.length()>=8){
             
                String origen = ruta.substring(0,2);
                String destino = ruta.substring(2,4);
                
                String secuencia = ruta.substring(4,8);
             
               
                ps = con.prepareStatement(this.SQL_GET_VIA);
                ps.setString(1, origen);
                ps.setString(2, destino);
                ps.setString(3, secuencia);
                rs = ps.executeQuery();
                
                while(rs.next()){
                    descripcion = rs.getString("descripcion");
                    
                }
            }
          
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL BUSCAR LOS PUESTOS DE CONTROL, "+ex.getMessage()+", "+ex.getErrorCode());
        }
        finally {
            if(ps != null)
            {
                try
                {
                    ps.close ();
                }
                catch(SQLException e)
                {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null)
            {
                poolManager.freeConnection ("fintra", con );
            }
        }
        /**/
        return descripcion;
    }
    
    /**
     * Getter for property jus.
     * @return Value of property jus.
     */
    public java.util.Vector getJus() throws SQLException{
        try{
      
        this.Buscarjustificaciones();
       
        }catch(SQLException e){
            e.printStackTrace();
        }
        return jus;
    }
    
    /**
     * Setter for property jus.
     * @param jus New value of property jus.
     */
    public void setJus(java.util.Vector jus) {
        this.jus = jus;
    }
    
    /*************************************************************************************/
    
     /**
     * Metodo obtenerNombreZona
     * descripcion:Metodo que retorna el nombre de una zona 
     * @autor : LREALES
     * @param : String zona
     * @version : 1.0
     */
    public String obtenerNombreZona( String zona ) throws SQLException {
        
        PoolManager poolManager = PoolManager.getInstance ();
        Connection con = poolManager.getConnection ( "fintra" );
        
        if ( con == null )
            throw new SQLException ("Sin conexion");
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        String zona_nombre = "";
        
        try {
            
            st = con.prepareStatement ("SELECT " +
                                                "desczona " +
                                        "FROM " +
                                                "zona " +
                                        "WHERE " +
                                                "codzona = ?");
            
            st.setString ( 1, zona );
            
            rs = st.executeQuery ();
            
            if ( rs.next () ) {
                
                zona_nombre= rs.getString ("desczona")!=null?rs.getString ("desczona"):"";
                
            } 
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException ( "ERROR EN [obtenerNombreZona] - DespachoManualDAO.. " + e.getMessage () + " - " + e.getErrorCode () );
        
        }
        
        finally {
            
            if( st != null ) {
                try {
                    st.close ();
                } catch( SQLException e ) {
                    throw new SQLException ( "ERROR AL CERRAR EL ESTAMENTO " + e.getMessage () );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection ( "fintra", con );
            }
            
        }
        
        return zona_nombre;
        
    }
    
    /**
     * Metodo buscarNombreConductor
     * descripcion: busca el nombre del conductor en ingreso trafico
     * @autor : LREALES
     * @param : String planilla
     * @version : 1.0
     */
    public String buscarNombreConductor ( String planilla ) throws SQLException{
        
        PoolManager poolManager = PoolManager.getInstance ();
        Connection con = poolManager.getConnection ("fintra");
        
        if (con == null)
            throw new SQLException ("Sin conexion");
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        String nomcon = "";
        
        try {            
            st = con.prepareStatement (this.SQL_NOMBRE_CONDUCTOR);
            
            st.setString (1, planilla);
            
            rs = st.executeQuery ();
            
            while ( rs.next () ) {
                nomcon = rs.getString ("nomcond")!=null?rs.getString ("nomcond"):"";
            }             
        } catch(SQLException e) {
            e.printStackTrace();
            throw new SQLException ( "ERROR AL ENCONTRAR buscarNombreConductor.. " + e.getMessage () + " - " + e.getErrorCode () );
        }
        finally {
            if(st != null) {
                try {
                    st.close ();
                } catch(SQLException e) {
                    throw new SQLException ( "ERROR AL CERRAR EL ESTAMENTO " + e.getMessage () );
                }
            }
            if ( con != null) {
                poolManager.freeConnection ( "fintra", con );
            }
        }
        
        return nomcon;
        
    }
    
    /**
     * Metodo buscarNombreCliente
     * descripcion: busca el nombre del cliente en ingreso trafico
     * @autor : LREALES
     * @param : String planilla
     * @version : 1.0
     */
    public String buscarNombreCliente ( String planilla ) throws SQLException{
        
        PoolManager poolManager = PoolManager.getInstance ();
        Connection con = poolManager.getConnection ("fintra");
        
        if (con == null)
            throw new SQLException ("Sin conexion");
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        String nomcli = "";
        
        try {            
            st = con.prepareStatement (this.SQL_NOMBRE_CLIENTE);
            
            st.setString (1, planilla);
            
            rs = st.executeQuery ();
            
            while ( rs.next () ) {
                nomcli = rs.getString ("cliente")!=null?rs.getString ("cliente"):"";
            }             
        } catch(SQLException e) {
            e.printStackTrace();
            throw new SQLException ( "ERROR AL ENCONTRAR buscarNombreCliente.. " + e.getMessage () + " - " + e.getErrorCode () );
        }
        finally {
            if(st != null) {
                try {
                    st.close ();
                } catch(SQLException e) {
                    throw new SQLException ( "ERROR AL CERRAR EL ESTAMENTO " + e.getMessage () );
                }
            }
            if ( con != null) {
                poolManager.freeConnection ( "fintra", con );
            }
        }
        
        return nomcli;
        
    }
    
}