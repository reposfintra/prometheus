/***********************************************************************************
 * Nombre clase : ............... Proveedor_EscoltaDAO.java                        *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la tabla proveedor escolta.                  *
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                     *
 * Fecha :....................... 26 de Noviembre de 2005, 09:00 AM                *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.Util;

public class Proveedor_EscoltaDAO {
    
    private Proveedor_Escolta  proveedor;
    
    private Vector vecProv;
    
    private static final String INS_PROV_ESCOLTA  = 
    "Insert into proveedor_escolta (dstrct,nit,origen,destino,tarifa,cod_contable,creation_user,base) " +
    "values(?,?,?,?,?,?,?,?)";
    
    private static final String EXISTE_PROVEEDOR = 
    "Select nit from proveedor_escolta where nit=? and origen=? and destino=?";
    
    private String BUSCAR_PROVEEDORES = 
    "select" +
    "     P.nit," +
    "     nombre," +
    "     P.origen," +
    "     A.nomciu as nomorigen," +
    "     P.destino," +
    "     B.nomciu as nomdestino," +
    "     P.tarifa," +
    "     P.reg_status," +
    "     P.cod_contable " +
    "from " +
    "     proveedor_escolta P, " +
    "     nit," +
    "     Ciudad A," +
    "     Ciudad B " +
    "where " +
    "     cedula   = nit and" +
    "     A.codciu = origen and" +
    "     B.codciu = destino ";
    
    private static final String UPDATE_PROVEEDOR= 
    "Update proveedor_escolta set tarifa=?, cod_contable=?, user_update=?, reg_status=? where nit=? and origen=? and destino=? ";
    
     /**
     * Metodo insertarProveedorEscolta, ingresa una tarifa o precio del escolta de proveedores     
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : Proveedor Escolta  proveedor
     * @version : 1.0
     */
    public void insertarProveedorEscolta(Proveedor_Escolta prov) throws SQLException {        
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(INS_PROV_ESCOLTA);
                st.setString(1, prov.getDstrct());
                st.setString(2, prov.getNit());
                st.setString(3, prov.getOrigen());
                st.setString(4, prov.getDestino());
                st.setDouble(5, prov.getTarifa());
                st.setString(6, prov.getCod_contable());
                st.setString(7, prov.getCreation_user());
                st.setString(8, prov.getBase());
                st.execute();
            }          
        }catch(SQLException e){
             throw new SQLException("ERROR AL ANULAR ASIGNACION" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }          
        
    }
     /**
     * Metodo existeProveedorEscolta, verifica que no exista una tarifa ya ingresada por un 
     * proveedor para un origen y destino determinado
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : Proveedor Escolta  proveedor
     * @version : 1.0
     */
    public boolean existeProveedorEscolta(Proveedor_Escolta prov) throws SQLException {        
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(EXISTE_PROVEEDOR);                
                st.setString(1, prov.getNit());
                st.setString(2, prov.getOrigen());
                st.setString(3, prov.getDestino());                
                rs = st.executeQuery();
                return rs.next();
            }          
            return false;
        }catch(SQLException e){
             throw new SQLException("ERROR AL ANULAR ASIGNACION" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }                  
    }
     /**
     * Metodo buscarProveedorEscolta, busca tarifa(s) de un proveedor especifico
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : String nit, String origen 
     * @version : 1.0
     */
    public void buscarProveedorEscolta(String nit, String origen) throws SQLException {        
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;         
        vecProv = new Vector();
        String resto = "";
        resto = "and nit='"+nit+"'";
        if (!origen.equals("")) {
            resto+= " and origen='"+origen+"'";
        }
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(BUSCAR_PROVEEDORES+resto);                         
                rs = st.executeQuery();
                while (rs.next()) {
                    proveedor = new Proveedor_Escolta();
                    proveedor.setNit(rs.getString("nit"));
                    proveedor.setReg_status(rs.getString("reg_status"));
                    proveedor.setNit(rs.getString("nit"));
                    proveedor.setOrigen(rs.getString("origen"));
                    proveedor.setDestino(rs.getString("destino"));
                    proveedor.setTarifa(rs.getDouble("tarifa"));
                    proveedor.setNomProveedor(rs.getString("nombre"));
                    proveedor.setNomOrigen(rs.getString("nomorigen"));
                    proveedor.setNomDestino(rs.getString("nomdestino"));
                    proveedor.setCod_contable(rs.getString("cod_contable"));
                    vecProv.addElement(proveedor);
                }
            }          
        }catch(SQLException e){
             throw new SQLException("ERROR AL ANULAR ASIGNACION" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        } 
    }
     /**
     * Metodo updateProveedorEscolta, actualiza la tarifa o el codigo contable de un proveedor de
     * escolta especifico
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : Proveedor Escolta  proveedor
     * @version : 1.0
     */
     public void updateProveedorEscolta(Proveedor_Escolta prov) throws SQLException {        
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;        
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(UPDATE_PROVEEDOR);                         
                st.setDouble(1, prov.getTarifa());
                st.setString(2, prov.getCod_contable());
                st.setString(3, prov.getUser_update());
                st.setString(4, prov.getReg_status());
                st.setString(5, prov.getNit());
                st.setString(6, prov.getOrigen());
                st.setString(7, prov.getDestino());
                st.execute();                
            }          
        }catch(SQLException e){
             throw new SQLException("ERROR AL ANULAR ASIGNACION" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        } 
    }
    public Vector getVectorProveedores() {
        return this.vecProv;
    }
}

