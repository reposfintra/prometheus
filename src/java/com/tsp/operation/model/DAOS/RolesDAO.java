/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.Rol;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

/**
 *
 * @author Roberto Parra
 */
public class RolesDAO extends MainDAO {
    
   /** Creates a new instance of PerfilDAO */
    public RolesDAO() {
        super("RolesDAO.xml", "fintra");
    }
    

    private Rol Rol;
    private Vector vRoles;


    
       public Vector getVRoles() throws SQLException{
        return vRoles;
    }
      public void ListarRoles()throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_ROLES";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                //st.setString( 1, usuario );
                rs = st.executeQuery();
                vRoles = new Vector();                
                while(rs.next()){
                    Rol = new Rol();
                    Rol.setId(rs.getString("id"));
                    Rol.setDescripcion(rs.getString("descripcion"));
                    Rol.setEliminar("Eliminar");
                    Rol.setModificar("Modificar");
                    vRoles.add(Rol);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL ROL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
    }
      
    public String CrearRol(String descripcion, String  dstrct, 
                         String creation_user)throws SQLException 
                             {
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_CREAR_ROL";
        String respuesta="";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
               // vRoles = new Vector();        
              if(descripcion.equals("")  ||dstrct.equals("") ||creation_user.equals("") )
              {
               // vRoles.add( "");
                respuesta="TODOS LOS CAMPOS SON OBLIGATORIOS";
              }else{
                 
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, descripcion );
                st.setString( 2, dstrct );
                st.setString( 3, creation_user );
                
             int est= st.executeUpdate();
             
            if(est>0){
                //Guardar el texto en un vector
               //   vRoles.add( "");
                respuesta="ok";
            }
            else{
                // vRoles.add( "");
                respuesta="ERROR AL CREAR EL ROL";
            }
           
              }  

            }else{
             respuesta="ERROR AL CREAR EL ROL";
            }
        }catch(SQLException e){
            String mensaje=e.getMessage();
            int nombreDuplicado= mensaje.indexOf("duplicate key violates unique constraint \"uq_descripcion_rol");
            if(nombreDuplicado!=-1){
               respuesta="YA SE ENCENTRA REGISTRADO UN ROL CON EL MISMO NOMBRE";
            }else{
               respuesta="ERROR AL CREAR EL ROL";
            }
          //  throw new SQLException("ERROR DURANTE LA OBTENCION DEL ROL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return respuesta;
    }
    
     
      public void DatosRol(String id)throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_ROL";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString( 1, id );
                rs = st.executeQuery();
                vRoles = new Vector();                
                while(rs.next()){
                    Rol = new Rol();
                    Rol.setId(rs.getString("id"));
                    Rol.setDescripcion(rs.getString("descripcion"));
                    vRoles.add(Rol);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL ROL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
    }
     
          
        
     
   public String ModificarRol(String id_rol,String  descripcion,  String  dstrct, 
                                   String user_update)throws SQLException 
                             {
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_MODIFICAR_ROL";
        String respuesta="";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
              //  vRoles = new Vector();        
              if(id_rol.equals("") || descripcion.equals("") || dstrct.equals("") ||user_update.equals("") )
              {
                respuesta="Todos los campos son obligatorios";
              }else{
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, descripcion );
                st.setString(2, dstrct );
                st.setString(3, user_update );
                st.setInt(4, Integer.parseInt(id_rol) );
                
             int est= st.executeUpdate();
             
            if(est>0){

                  respuesta="ok";
            }
            else{
                 respuesta="Error al modificar el Rol";
            }
           
              }  

            }else{
            respuesta="Error al modificar el Rol";
            }
        }catch(SQLException e){
             String mensaje=e.getMessage();
            int nombreDuplicado= mensaje.indexOf("duplicate key violates unique constraint \"uq_descripcion_rol");
            if(nombreDuplicado!=-1){
               respuesta="ya se encuentra registrado un rol con el mismo nombre";
            }else{
               respuesta="Error al modificar el Rol";
            }
            //throw new SQLException("ERROR DURANTE LA MODIFICACION DEL ROL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;
    }
   
        public String EliminarRol(String id_rol)throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ELIMINAR_ROL";
        String respuesta="";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString( 1, id_rol );
             int est= st.executeUpdate();    
            if(est>0){
                  respuesta="Se ha eliminado correctamente el rol";
            }
            else{
                  respuesta="Error al eliminar el rol";
            }
            }else{
            respuesta="Error al eliminar el rol";
            }
        }catch(SQLException e){
             respuesta="Error al eliminar el rol";
           // throw new SQLException("ERROR DURANTE LA OBTENCION DEL ROL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return respuesta;
    } 
   public String ObtenerRol(String idusuario)throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_ROL_DEL_ANALISTA";
        String respuesta="";
        try {
            con = this.conectarJNDIFintra();
                        if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString( 1, idusuario );
                rs = st.executeQuery();
               // vAnalistas = new Vector();                
                while(rs.next()){
                 respuesta=  rs.getString("idrol");
                }
            }
        }catch(SQLException e){
           // throw new SQLException("ERROR DURANTE LA OBTENCION DEL ROL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
         return respuesta;
    }
   
}


