/***********************************************
 * Nombre       Clase  LiquidarOCDAO.java 
 * Autor        ING FERNEL VILLACOB DIAZ
 * Modificado   Ing Sandra Escalante
 * Fecha        21/12/2005
 * Copyright    Transportes Sanchez Polo S.A.
 **********************************************/


package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;

public class LiquidarOCDAO extends MainDAO{
    
    private Planilla pla;
    
       
    public LiquidarOCDAO() {
        super("LiquidarOCDAO.xml");  
    }
    
    
    
    
    
    
    /**
     * Metodo que busca pais de la placa
     * @autor fvillacob
     * @throws Exception.
     */
    public String  paisPlaca(String placa) throws Exception{
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        String            query  = "SQL_PAIS_PLACA";
        String            pais   = "";
        
        try{
            st = this.crearPreparedStatement(query);
            st.setString(1, placa);
            rs = st.executeQuery();
            if (rs.next())
                pais = rs.getString("pais");
                
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close(); 
            this.desconectar(query);
        }
        return pais;
    } 
    
    
    
    
    
    
    
    /**
     * M�todo liquidar, busqueda de la informacion de la planilla
     * @autor   fvillacob
     * @param   numero de la planilla (String)
     * @return  Planilla (Objeto)
     * @throws  Exception
     * @version 2.0.
     **/
    public Planilla liquidar(String planilla) throws SQLException {
        PreparedStatement  st     = null;       
        ResultSet          rs     = null;        
        pla = null;
        try{            
            st= crearPreparedStatement("SQL_DATOS_OC");
            st.setString(1, planilla);
            //////System.out.println("Busqueda de OC " + st);
            rs=st.executeQuery();
            if(rs.next()){
                pla   = new Planilla(); //sescalante
                pla.setNumpla( rs.getString("oc") );
                pla.setAgcpla( rs.getString("agencia") );
                pla.setNomori( rs.getString("origen") );
                pla.setNomdest( rs.getString("destino") );
                pla.setPlaveh( rs.getString("plaveh")  ); 
                pla.setFecdsp(rs.getString("fecdsp"));
                pla.setReg_status(rs.getString("reg_status"));
            }
            if (pla != null)
                buscarRemesas(planilla, pla);
        }catch(Exception e){
            throw new SQLException("DAO:  liquidar() : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();        
            if(rs!=null) rs.close();            
            this.desconectar("SQL_DATOS_OC");
        }
        return pla;
    }
    
    
    /**
     * M�todo getOP, obtiene la OP de una planilla
     * @autor   fvillacob
     * @param   numero de la planilla (String)
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    public List getOP(String oc) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        List              lista     = null;
        OpDAO             opDAO     = new OpDAO();
        try {
            
            st= crearPreparedStatement("SQL_SEARCH_OC");
            st.setString(1, oc);
            rs=st.executeQuery();
            while(rs.next()){
                if( lista==null) lista = new LinkedList();
                OP  op  = opDAO.loadOp(rs);
                lista.add(op);
            }
        }
        catch(SQLException e){
            throw new SQLException(" DAO: No se pudo buscar OP.-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            desconectar("SQL_SEARCH_OC");
        }
        return lista;
    }
    
    /**
     * Getter for property pla.
     * @return Value of property pla.
     */
    public com.tsp.operation.model.beans.Planilla getPla() {
        return pla;
    }
    
    /**
     * Setter for property pla.
     * @param pla New value of property pla.
     */
    public void setPla(com.tsp.operation.model.beans.Planilla pla) {
        this.pla = pla;
    }
    
    /**
     * M�todo buscarRemesas, busqueda de las remesas de una planilla
     * @autor   sescalante
     * @param   numero de la planilla (String)
     * @throws  Exception
     * @version 1.0.
     **/
    public void buscarRemesas(String planilla, Planilla p) throws SQLException {
        PreparedStatement  st     = null;        
        ResultSet          rs     = null;             
        String rems = "";
        
        try{            
            st= crearPreparedStatement("SQL_REMPLA");
            st.setString(1, planilla);
            rs = st.executeQuery();
            ////System.out.println("Busqueda de remesas de la planilla " + st);
            while (rs.next()){
                rems += " - " + rs.getString("remesa");
                p.setClientes(rs.getString("cliente"));   
  
            }
            p.setNumrem(rems);
        }catch(Exception e){
            throw new SQLException("DAO:  buscarRemesas() : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();            
           desconectar("SQL_REMPLA");
        }
    }
    
       
    
    /**
     * Metodo para extraer los descuentos asociados a un propietario de MIMS
     * @autor mfontalvo
     * @param proveedor
     * @return Vector vector de descuentos.
     * @throws Exception.
     */
    public Vector obtenerAjustesProveedorMIMS (String proveedor) throws Exception{
        PreparedStatement st = null;
        ResultSet         rs = null;
        Vector            descuentos = new Vector();
        try{
            st = this.crearPreparedStatement("SQL_SEARCH_AJUSTES_PROPIETARIO");
            st.setString(1, proveedor);
            rs = st.executeQuery();
            while (rs.next()){
                DescuentoMIMS desc = new DescuentoMIMS();
                desc.setTipoRegistro("AJPRO");
                
                desc.setDistrito    ( Util.coalesce(rs.getString(1 ),"") );
                desc.setPlaca       ( Util.coalesce(rs.getString(2 ),"") );
                desc.setFactura     ( Util.coalesce(rs.getString(3 ),"") );
                desc.setFactura_ext ( Util.coalesce(rs.getString(4 ),"") );
                desc.setFecha       ( Util.coalesce(rs.getString(5 ),"") );
                desc.setEstado      ( Util.coalesce(rs.getString(6 ),"") );
                desc.setTipo        ( Util.coalesce(rs.getString(7 ),"") );
                desc.setBanco       ( Util.coalesce(rs.getString(8 ),"") );
                desc.setSucursal    ( Util.coalesce(rs.getString(9 ),"") );
                desc.setValor       ( rs.getDouble(10) );
                desc.setMoneda      ( Util.coalesce(rs.getString(11),"") );
                desc.setId_mims     ( Util.coalesce(rs.getString(12),"") );
                desc.setItem        ( Util.coalesce(rs.getString(13),"") );
                desc.setDescripcion ( Util.coalesce(rs.getString(14),"") );
                desc.setPlanilla    ( Util.coalesce(rs.getString(15),"") );
                desc.setValor_item  ( rs.getDouble(16) );
                desc.setRetefuente  ( rs.getDouble(17) );
                desc.setIva         ( rs.getDouble(18) );
                desc.setRiva        ( rs.getDouble(19) );
                desc.setRica        ( rs.getDouble(20) );
                desc.setAccount     ( Util.coalesce(rs.getString(21),"") );
                desc.setElemento    ( Util.coalesce(rs.getString(22),"") );
                desc.setTipoCuenta  ( Util.coalesce(rs.getString(23),"") );
                desc.setCheque      ( Util.coalesce(rs.getString(24),"") );
                
                desc.setCodConcepto ( "99" );
                desc.setDesConcepto ( "DESCUENTOS EN MIMS" );
                desc.setAsignador   ( "S" ); // porcentaje o valor
                desc.setIndicador   ( "V" ); // saldo o valor
                descuentos.add(desc);
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ( "obtenerAjustesProveedorMIMS " + ex.getMessage());
        }
        finally{
            if(st!=null) st.close();        
            if(rs!=null) rs.close();              
            this.desconectar("SQL_SEARCH_AJUSTES_PROPIETARIO");
        }
        return descuentos;
    }
    
    
    
    /**
     * Metodo para extraer los descuentos asociados a una planilla de MIMS
     * @autor mfontalvo
     * @param planilla
     * @return Vector vector de descuentos.
     * @throws Exception.
     */
    public Vector obtenerAjustesPlanillaMIMS (String planilla) throws Exception{
        PreparedStatement st = null;
        ResultSet         rs = null;
        Vector            descuentos = new Vector();
        try{
            st = this.crearPreparedStatement("SQL_SEARCH_AJUSTES_PLANILLA");
            st.setString(1, planilla);
            rs = st.executeQuery();
            while (rs.next()){
                DescuentoMIMS desc = new DescuentoMIMS();
                desc.setTipoRegistro("AJPLA");
                
                desc.setDistrito     ( Util.coalesce(rs.getString( 1), "") );
                desc.setPlaca        ( Util.coalesce(rs.getString( 2), "") );
                desc.setId_mims      ( Util.coalesce(rs.getString( 3), "") );
                desc.setNombre       ( Util.coalesce(rs.getString( 4), "") );
                desc.setNit          ( Util.coalesce(rs.getString( 5), "") );
                desc.setFactura      ( Util.coalesce(rs.getString( 6), "") );
                desc.setFactura_ext  ( Util.coalesce(rs.getString( 7), "") );
                desc.setFecha        ( Util.coalesce(rs.getString( 8), "") );
                desc.setEstado       ( Util.coalesce(rs.getString( 9), "") );
                desc.setTipo         ( Util.coalesce(rs.getString(10), "") );
                desc.setValor        ( rs.getDouble(11) );
                desc.setItem         ( Util.coalesce(rs.getString(12), "") );
                desc.setDescripcion  ( Util.coalesce(rs.getString(13), "") );
                desc.setValor_item   ( rs.getDouble(14) );
                desc.setMoneda       ( Util.coalesce(rs.getString(15), "") );
                desc.setBanco        ( Util.coalesce(rs.getString(16), "") );
                desc.setSucursal     ( Util.coalesce(rs.getString(17), "") );
                desc.setOrigen       ( Util.coalesce(rs.getString(18), "") );
                desc.setOrigenNombre ( Util.coalesce(rs.getString(19), "") );
                desc.setDestino      ( Util.coalesce(rs.getString(20), "") );
                desc.setDestinoNombre( Util.coalesce(rs.getString(21), "") );
                desc.setCliente      ( Util.coalesce(rs.getString(22), "") );                
                desc.setClienteNombre( Util.coalesce(rs.getString(23), "") );
                desc.setRemesa       ( Util.coalesce(rs.getString(24), "") );
                desc.setTonelaje     ( rs.getDouble(25) );
                desc.setPlanilla     ( Util.coalesce(rs.getString(26), "") );
                desc.setAccount      ( Util.coalesce(rs.getString(27), "") );
                desc.setElemento     ( Util.coalesce(rs.getString(28), "") ); 
                desc.setTipoCuenta   ( Util.coalesce(rs.getString(29), "") ); // tipo de cuenta C � E
                desc.setCheque       ( Util.coalesce(rs.getString(30), "") ); 
                desc.setIndicador    ( "V" ); // porcentaje o valor                
                if (desc.getTipo().equals("A")){
                    desc.setCodConcepto ( "01" );
                    desc.setDesConcepto ( "ANTICIPO EN MIMS" );
                    desc.setAsignador   ( "S" ); // saldo o valor
                } else if ( desc.getTipo().equals("4") && desc.getElemento().equals("9005") )  {
                    desc.setCodConcepto ( "09" );
                    desc.setDesConcepto ( "AJUSTE PLANILLA EN MIMS" );
                    desc.setAsignador   ( "V" ); // saldo o valor   
                } else if ( desc.getTipo().equals("4") && !desc.getElemento().equals("9005") )  {
                    desc.setCodConcepto ( "99" );
                    desc.setDesConcepto ( "DESCUENTOS EN MIMS" );
                    desc.setAsignador   ( "S" ); // porcentaje o valor
                }desc.setDesConcepto( "[MIMS] " + desc.getDescripcion() );
                
                
                int signo = obtenerIndicadorSigno( desc.getCodConcepto() );
                desc.setValor_item( desc.getValor_item() * signo );
                descuentos.add(desc);
                
                
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ( "obtenerAjustesPlanillaMIMS " + ex.getMessage());
        }
        finally{
            if(st!=null) st.close();        
            if(rs!=null) rs.close();              
            this.desconectar("SQL_SEARCH_AJUSTES_PLANILLA");
        }
        return descuentos;
    }    
    
    
    /**
     * Metodo de verifiacion de anticipos y renaticipos no impresos
     * @autor mfontalvo
     * @param planilla, numero de planilla
     * @param proveedor, nit
     * @return boolean, true si tiene anticipos no impresos, false lo contrario
     * @throws Exception.
     */
    public boolean verificarAnticipos (String planilla, String proveedor) throws Exception{
        PreparedStatement st = null;
        ResultSet         rs = null;
        boolean           retorno = false;
        try{
            st = this.crearPreparedStatement("SQL_VERIFICACION_ANTICIPOS_NO_IMPRESOS");
            st.setString(1, planilla);
            st.setString(2, proveedor);
            rs = st.executeQuery();
            if (rs.next()){
                retorno = true;
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
        finally{
            if(st!=null) st.close();        
            if(rs!=null) rs.close();              
            this.desconectar("SQL_VERIFICACION_ANTICIPOS_NO_IMPRESOS");
        }
        return retorno;
    } 
 
    
    
    
     /**
     * Metodo que busca la equivalencia del impuesto en tabla general
     * @autor fvillacob
     * @throws Exception.
     */
    public String  codigoImp_tablaGen(String impuesto) throws Exception{
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        String            query  = "SQL_CAMBIO_IMPUESTO";
        String            code   = impuesto;
        
        try{
            st = this.crearPreparedStatement(query);
            st.setString(1, impuesto);
            rs = st.executeQuery();
            if (rs.next())
                code = rs.getString("referencia");
                
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close(); 
            this.desconectar(query);
        }
        return code;
    } 
    
    
    
    /**
     * Metodo de busqueda del indicador de signo de un concepto
     * @autor mfontalvo
     * @param concepto
     * @return signo, signo del concepto
     * @throws Exception.
     */
    public int obtenerIndicadorSigno (String concepto) throws Exception{
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        int               signo = 1;
        try{
            st = this.crearPreparedStatement( "SQL_OBTENER_INDICADOR_DE_SIGNO" );
            st.setString(1, concepto );
            rs = st.executeQuery();
            if (rs.next()){
                signo = rs.getInt("ind_signo");
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
        finally{
            if(st!=null) st.close();        
            if(rs!=null) rs.close();              
            this.desconectar("SQL_OBTENER_INDICADOR_DE_SIGNO");
        }
        return signo;
    }  
    
    
    /**
     * M�todo buscar datos de planillas a liquidar
     * @autor   igomez
     * @param   parametro de busqueda
     * @param   tipo, tipo de filtro
     * @throws  Exception
     * @version 1.0.
     **/
    public Vector buscarDatosALiquidarREP(String numpla, String proveedor) throws Exception {
        Connection         con    = null;
        PreparedStatement  st     = null;        
        ResultSet          rs     = null;             
        Vector             planillas = new Vector();
        try{            
            String SQL = this.obtenerSQL("SQL_PLANILLAS_REP").replaceAll("#PLANILLAS#",numpla);
            con = this.conectar("SQL_PLANILLAS_REP");
            st= con.prepareStatement(SQL);
            st.setString(1, proveedor);
            rs = st.executeQuery();
            while (rs.next()){
                Planilla p = new Planilla();
                p.setNumpla    ( rs.getString("numpla" ));
                p.setCia       ( rs.getString("cia"    ));
                p.setNitpro    ( rs.getString("nitpro" ));
                p.setPlaveh    ( rs.getString("plaveh" ));
                p.setProveedor ( rs.getString("id_mims"));
                p.setNomprop   ( rs.getString("nombre" ));
                p.setNomori    ( rs.getString("origen" ));
                p.setNomdest   ( rs.getString("destino"));
                p.setBase      ( rs.getString("base"   ));
                planillas.add(p);
            }
        }catch(Exception e){
            throw new SQLException("DAO:  buscarDatosALiquidar() : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();            
           desconectar("SQL_PLANILLAS_REP");
        }
        return planillas;
    }
    
    
     /**
     * Metodo para obtener el beneficiario
     * @autor mfontalvo
     * @throws Exception.
     */
    public String getBeneficiario(String nit) throws Exception{
        PreparedStatement st           = null;
        ResultSet         rs           = null;
        String            query        = "SQL_GET_BENEFICIARIO";
        String            beneficiario = null;
        
        try{
            st = this.crearPreparedStatement(query);
            st.setString(1, nit);
            rs = st.executeQuery();
            if (rs.next())
                beneficiario = rs.getString("beneficiario");
                
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close(); 
            this.desconectar(query);
        }
        return beneficiario;
    }     
    
    
 
    
    /**
     * Metodo para verificar el estado de una op
     * @autor mfontalvo
     * @param planilla
     * @return String estado.
     * @throws Exception.
     */
    public String  obtenerEstadoOP (String planilla) throws Exception{
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        String            estado = "";
        try{
            st = this.crearPreparedStatement("SQL_OBTENER_ESTADO_OP");
            st.setString(1, planilla);
            rs = st.executeQuery();
            if (rs.next()){
                estado = rs.getString(7);
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ( "obtenerEstadoOP " + ex.getMessage());
        }
        finally{
            if(st!=null) st.close();        
            if(rs!=null) rs.close();              
            this.desconectar("SQL_OBTENER_ESTADO_OP");
        }
        return estado;
    }   
  
     //JBarros
    /**
     * M�todo loadOP, carga datos obtneidos
     * @autor   fvillacob
     * @Modificado jbarros
     * @throws  Exception
     * @version 1.0.
     * @Modificado Jbarros
     **/
    public OP  loadOp(ResultSet  rs)throws Exception{
        OP  op = new OP();
        try{
               op.setOc                   (  (  rs.getString("oc")          )  );  
               op.setDstrct               (  (  rs.getString("distrito")    )  );  
               op.setProveedor            (  (  rs.getString("propietario") )  );
               op.setPlaca                (  (  rs.getString("placa")       )  );
               op.setFechaCumplido        (  (  rs.getString("feccum")      )  );
               op.setOrigenOC             (  (  rs.getString("origenoc")    )  );
               op.setBase                 (  (  rs.getString("base")        )  );
               op.setId_mims              (  (  rs.getString("idmims")      )  );
               op.setAgencia              (  (  rs.getString("agencia")     )  );
               op.setBanco                (  (  rs.getString("banco")       )  );
               op.setSucursal             (  (  rs.getString("sucursal")    )  );
               op.setMoneda               (  (  rs.getString("monedabanco") )  );
               op.setHandle_code          (  (  rs.getString("handlecode")  )  );
               op.setReteFuente           (  (  rs.getString("retefuente")  )  );
               op.setReteIca              (  (  rs.getString("reteica")     )  );
               op.setReteIva              (  (  rs.getString("reteiva")     )  );
               op.setRetenedor            (  (  rs.getString("agente_retenedor")    ) );
               op.setPlazo                (          rs.getInt   ("plazo")                 );
               op.setMoneda               (  (  rs.getString("monedabanco")         ) );               
               op.setFecha_documento      (  (  rs.getString("fechaDocumento")      ) );
               op.setFecha_vencimiento    (  (  rs.getString("fechaVencimiento")    ) );                              
               op.setDescripcion          ( "FACTURA ADICION DE OP " + (  rs.getString("factura") ) );
               op.setDocumento            ( "FP" +  op.getOc()                 );
               op.setDocumento_relacionado( (  rs.getString("factura")       ) );
               op.setTipo_documento_rel   ( "010"                              );
               op.setTipo_documento       ( ""                                 );
               op.setClase_documento      ( "4"                                );  // Tipo 4.
               op.setEstado              (   (  rs.getString("estado")                 ));
               
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return op;
    }
    
    
    /**
     * Metodo para extraer los descuentos asociados a un propietario de WEB
     * @autor jbarros
     * @param proveedor
     * @return Vector vector de descuentos.
     * @throws Exception.
     */
    public Vector obtenerAjustesProveedorWEB (String proveedor) throws Exception{
        PreparedStatement st = null;
        ResultSet         rs = null;
        Vector            descuentos = new Vector();
        try{
            st = this.crearPreparedStatement("SQL_SEARCH_AJUSTES_PROPIETARIO_WEB");
            st.setString(1, proveedor);
            rs = st.executeQuery();
            while (rs.next()){
                DescuentoMIMS desc = new DescuentoMIMS();
                desc.setTipoRegistro("AJPRO");
                
                desc.setDistrito    ( Util.coalesce(rs.getString("dstrct" ),"") );
                desc.setPlaca       ( Util.coalesce(rs.getString("plaveh" ),"") );
                desc.setFactura     ( Util.coalesce(rs.getString("documento" ),"") );
                desc.setFactura_ext ( "" );
                desc.setFecha       ( Util.coalesce(rs.getString("fecha_documento" ),"") );
                desc.setEstado      ( Util.coalesce(rs.getString("reg_status" ),"") );
                desc.setTipo        ( Util.coalesce(rs.getString("clase_documento" ),"") );
                desc.setBanco       ( Util.coalesce(rs.getString("banco" ),"") );
                desc.setSucursal    ( Util.coalesce(rs.getString("sucursal" ),"") );
                desc.setValor       ( rs.getDouble              ("vlr_saldo_me") );
                desc.setMoneda      ( Util.coalesce(rs.getString("moneda"),"") );
                desc.setId_mims     ( Util.coalesce(rs.getString("id_mims"),"") );
                desc.setItem        ( Util.coalesce(rs.getString("item"),"") );
                desc.setDescripcion ( Util.coalesce(rs.getString("descripcion"),"") );
                desc.setPlanilla    ( Util.coalesce(rs.getString("planilla"),"") );
                desc.setValor_item  ( rs.getDouble("vlr_me") );
                desc.setRetefuente  ( rs.getDouble("Retefuente") );
                desc.setIva         ( rs.getDouble("Iva") );
                desc.setRiva        ( rs.getDouble("Riva") );
                desc.setRica        ( rs.getDouble("Rica") );
                desc.setAccount     ( Util.coalesce(rs.getString("codigo_cuenta"),"") );
                desc.setElemento    ( Util.coalesce(rs.getString("elemento"),"") );
                desc.setTipoCuenta  ( Util.coalesce(rs.getString("tcuenta"),"") );
                desc.setCheque      ( Util.coalesce(rs.getString("cheque"),"") );
                
                desc.setCodConcepto ( "99" );
                desc.setDesConcepto ( "DESCUENTOS EN WEB" );
                desc.setAsignador   ( "S" ); // porcentaje o valor
                desc.setIndicador   ( "V" ); // saldo o valor
                descuentos.add(desc);
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ( "obtenerAjustesProveedorWEB " + ex.getMessage());
        }
        finally{
            if(st!=null) st.close();        
            if(rs!=null) rs.close();              
            this.desconectar("SQL_SEARCH_AJUSTES_PROPIETARIO_WEB");
        }
        return descuentos;
    }
    
    
    
    /**
     * Metodo para extraer los descuentos asociados a un propietario de WEB
     * @autor jbarros
     * @param proveedor
     * @return Vector vector de descuentos.
     * @throws Exception.
     */
    public Vector FacturasProveedorCorrida (String proveedor) throws Exception{
        System.out.println("Comensando DAO");
        PreparedStatement st = null;
        ResultSet         rs = null;
        Vector    descuentos = new Vector();
        Vector   corridasNeg = new Vector();
        String Cor="'";
        int i =0;
        try{
            st = this.crearPreparedStatement("SQL_FACTURAS_PROVEEDOR_CORRIDA_NEG");
            st.setString(1, proveedor);
            rs = st.executeQuery();
            while (rs.next()){
                if(i!=0)
                    Cor+=",'";
                Cor+=rs.getString("corrida").trim()+"'";
                corridasNeg.add( Util.coalesce(rs.getString("corrida"),"") );
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ( "obtenerCorridas Negativas " + ex.getMessage());
        }
        finally{
            if(st!=null) st.close();        
            if(rs!=null) rs.close();              
            this.desconectar("SQL_FACTURAS_PROVEEDOR_CORRIDA_NEG");
        }
        if(!Cor.trim().equals("'")){
          descuentos=FacturasCorridaNegativa (Cor,proveedor);
        }else{
            System.out.println("No hay corridas ");
        }
        return descuentos;  
    }
    
    
    /**
     * Metodo para extraer los descuentos asociados a un propietario de WEB
     * @autor jbarros
     * @param proveedor
     * @return Vector vector de descuentos.
     * @throws Exception.
     */
    public Vector FacturasCorridaNegativa (String corridasNegativas,String proveedor) throws Exception{
        System.out.println("Comensando DAO 2 ");
        Connection         con    = null;
        PreparedStatement  st     = null;        
        ResultSet          rs     = null;
        Vector            descuentos = new Vector();
        try{
            String SQL = this.obtenerSQL("SQL_FACTURAS_CORRIDA_NEG").replaceAll("#CORRIDAS#",corridasNegativas);
            con = this.conectar("SQL_FACTURAS_CORRIDA_NEG");
            st= con.prepareStatement(SQL);
            st.setString(1, proveedor);
            
            System.out.println("SQL --> "+st.toString() );
            rs = st.executeQuery();
            while (rs.next()){
                System.out.println("Planilla 1 "+rs.getString("documento" ));
                DescuentoMIMS desc = new DescuentoMIMS();
                desc.setTipoRegistro("CRRNEG");
                
                desc.setDistrito    ( "" );
                desc.setPlaca       ( "" );
                desc.setFactura     ( Util.coalesce(rs.getString("documento" ),"") );
                desc.setFactura_ext ( Util.coalesce(rs.getString("corrida" ),"") );
                desc.setFecha       ( "" );
                desc.setEstado      ( Util.coalesce(rs.getString("reg_status" ),"") );
                desc.setTipo        ( "" );
                desc.setBanco       ( "" );
                desc.setSucursal    ( "" );
                desc.setValor       ( rs.getDouble              ("vlr_saldo_me") );
                desc.setMoneda      ( Util.coalesce(rs.getString("moneda"),"") );
                desc.setId_mims     ( "" );
                desc.setItem        ( "" );
                desc.setDescripcion ( Util.coalesce(rs.getString("descripcion"),"") );
                desc.setPlanilla    ( "" );
                desc.setValor_item  ( rs.getDouble              ("vlr_saldo_me") );
                desc.setRetefuente  ( 0 );
                desc.setIva         ( 0 );
                desc.setRiva        ( 0 );
                desc.setRica        ( 0 );
                desc.setAccount     ( "" );
                desc.setElemento    ( "" );
                desc.setTipoCuenta  ( "" );
                desc.setCheque      ( "" );
                
                desc.setCodConcepto ( "99" );
                desc.setDesConcepto ( "CORRIDA NEGATIVA" );
                desc.setAsignador   ( "S" ); // porcentaje o valor
                desc.setIndicador   ( "V" ); // saldo o valor
                descuentos.add(desc);
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ( "obtenerFacturasdeCorridas Negativas " + ex.getMessage());
        }
        finally{
            if(st!=null) st.close();        
            if(rs!=null) rs.close();              
            this.desconectar("SQL_FACTURAS_CORRIDA_NEG");
        }
        return descuentos;  
    }
    
    
    public Vector getFacturasRelacionadas(String Distrito,String OP,String proveedor) throws Exception {
        Connection         con    = null;
        PreparedStatement  st     = null;        
        ResultSet          rs     = null;             
        Vector           facturas = new Vector();
        try{
            st = this.crearPreparedStatement("SQL_FACTURAS_ASOCIADAS");
            st.setString(1, Distrito    );
            st.setString(2, "010"       );
            st.setString(3, OP          );
            st.setString(4, proveedor   );
            rs = st.executeQuery();
            while (rs.next()){
                Vector tupla = new Vector();
                tupla.add(rs.getString("documento") );
                tupla.add(rs.getString("tipo_documento"));
                facturas.add( tupla);
            }
            
        }catch(Exception e){
            throw new SQLException("DAO:  getFacturasRelacionadas() : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();            
           desconectar("SQL_FACTURAS_ASOCIADAS");
        }
        return facturas;
    }
    
    
    /** 
     * M�todo busca las planillas con OP y que tenga con movimiento pendientes en movpla
     * @autor   jbarros
     * @throws  Exception
     **/
    public OP getOCMovPendientes(String distrito,String Oc) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        List              lista     = new LinkedList();
        OP                op1        = new OP();
        String            query     = "SQL_BUSCAR_PLANILLAS_WEB";
        try {
            OpDAO          OPDAO  = new  OpDAO();
            ChequeXFacturaDAO dao = new ChequeXFacturaDAO();
            String monedaLocal    = dao.getMonedaLocal(distrito);
            st= crearPreparedStatement(query);
            st.setString(1, Oc );
            st.setString(2, distrito );
            rs =st.executeQuery();
            if (rs.next()){
		op1=loadOp(rs);
                op1.setMonedaLocal( monedaLocal );
                String  msj = "";
               // Validamos si la planilla tiene Discrepancia   
                if( OPDAO.getDiscrepancia( op1.getOc() ) )
                     msj = " Presenta discrepancia ";
               // Validamos si la placa esta vetada  
                if(  OPDAO.getVetoPlaca(  op1.getPlaca()  )  )
                     msj += "La placa " +  op1.getPlaca()  +" se encuentra vetada ";
                op1.setComentario( msj ); 
            }
            System.gc();
        }
        catch(SQLException e){
            throw new SQLException(" DAO: getOCMovPendientes -->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            desconectar(query);
        }
        return op1;
    }
    
      
    /**
     * Metodo de verifiacion de planillas con op en mims o en web
     * @autor mfontalvo
     * @param planilla, numero de planilla
     * @return boolean, true si tiene anticipos no impresos, false lo contrario
     * @throws Exception.
     * @modificado jbarros
     */
    public boolean tieneOP (String planilla) throws Exception{
        //int     fec              = Integer.parseInt(Util.getFechaActual_String(8));
        //int     fechaC           = 20070227;
        PreparedStatement st = null;
        ResultSet         rs = null;
        Connection        conn = null;
        //if (fechaC <= fec ){//20070301
            String queryWEB  = "SQL_TIENE_OP_EN_WEB";
            try{
                conn = this.conectar(queryWEB);
                st = conn.prepareStatement(  this.obtenerSQL(queryWEB));
                st.setString(1, planilla);
                rs = st.executeQuery();
                if (rs.next()){
                    return true;
                }
            }catch (Exception ex){
                ex.printStackTrace();
                throw new Exception (ex.getMessage());
            }
            finally{
                if(st!=null) st.close();        
                if(rs!=null) rs.close();              
                this.desconectar(queryWEB );
            }
            // si no encontro op 
            return false;
       }
    
    /**
     * Metodo de verifiacion de anticipos y renaticipos no impresos
     * @autor jbarros
     * @param planilla, numero de planilla
     * @param proveedor, nit
     * @return boolean, true si tiene anticipos no impresos, false lo contrario
     * @throws Exception.
     */
    public boolean verificarAnticiposOP (String planilla, String proveedor) throws Exception{
        PreparedStatement st = null;
        ResultSet         rs = null;
        boolean           retorno = false;
        try{
            st = this.crearPreparedStatement("SQL_VERIFICACION_ANTICIPOS_OP_NO_IMPRESOS");
            st.setString(1, planilla);
            st.setString(2, proveedor);
            rs = st.executeQuery();
            if (rs.next()){
                retorno = true;
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
        finally{
            if(st!=null) st.close();        
            if(rs!=null) rs.close();              
            this.desconectar("SQL_VERIFICACION_ANTICIPOS_OP_NO_IMPRESOS");
        }
        return retorno;
    } 
    
    
       /**
     * M�todo buscar datos de planillas a liquidar
     * @autor   mfontalvo
     * @param   parametro de busqueda
     * @param   tipo, tipo de filtro
     * @param   base, indica si debe o no tomar las planillas cumplidas.
     * @throws  Exception
     * @version 1.0.
     **/
    public Vector buscarDatosALiquidar(String parametro, int tipo, String base) throws Exception {
        Connection         con    = null;
        PreparedStatement  st     = null;        
        ResultSet          rs     = null;             
        Vector             planillas = new Vector();
        String             SQL ;
        try{            
            String restriccion = (tipo==0?" pl.numpla = ? ": tipo==1 ? " pl.nitpro = ? " : tipo==2 ? "pr.id_mims = ? " : " pl.plaveh = ? " );
            restriccion += " and pl.reg_status " + (base.equalsIgnoreCase("") ?"= 'C' " : "!= 'A' ");
            if ( base.equalsIgnoreCase("") ){
                SQL = this.obtenerSQL("SQL_DATOS_A_LIQUIDAR_PP").replaceAll("#RESTRICCION#",restriccion);
                con = this.conectar("SQL_DATOS_A_LIQUIDAR_PP");
            }else{
                SQL = this.obtenerSQL("SQL_DATOS_A_LIQUIDAR").replaceAll("#RESTRICCION#",restriccion);
                con = this.conectar("SQL_DATOS_A_LIQUIDAR");
            }
                st= con.prepareStatement(SQL);
                st.setString(1, parametro);
                ////System.out.println("SQL: " + st.toString());
                rs = st.executeQuery();
                while (rs.next()){
                    Planilla p = new Planilla();
                    p.setNumpla    ( rs.getString("numpla" ));
                    p.setCia       ( rs.getString("cia"    ));
                    p.setNitpro    ( rs.getString("nitpro" ));
                    p.setPlaveh    ( rs.getString("plaveh" ));
                    p.setProveedor ( rs.getString("id_mims"));
                    p.setNomprop   ( rs.getString("nombre" ));
                    p.setNomori    ( rs.getString("origen" ));
                    p.setNomdest   ( rs.getString("destino"));
                    p.setBase      ( rs.getString("base"   ));
                    p.setFactura   ( rs.getString("factura"));
                    planillas.add(p);
                }
        }catch(Exception e){
            throw new SQLException("DAO:  buscarDatosALiquidar() : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close(); 
            if ( base.equalsIgnoreCase("") ){
                desconectar("SQL_DATOS_A_LIQUIDAR_PP");
            }else{
                desconectar("SQL_DATOS_A_LIQUIDAR");
            }
        }
        return planillas;
    }
        
}
