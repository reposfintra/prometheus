/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.Usuario;

/**
 *
 * @author HaroldC
 */
public interface EstadoCuentaDAO {
      
    public JsonElement EstadoCuentaApoteosys(JsonObject jsonData, String Documento);
    
    public JsonElement TerceroPlaca(JsonObject jsonData, String Placa);
    
}
