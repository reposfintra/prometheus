/*
* Nombre        TablaGeneralManagerDAO.java
* Descripci�n   Clase DAO para tabla general
* Autor         FERNEL VILLACOB DIAZ
* Fecha         20 de Agosto de 2007
* Versi�n       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/




package com.tsp.operation.model.DAOS;



import java.sql.*;
import java.util.LinkedList;
import com.tsp.operation.model.beans.TablaGen;
import java.util.*;
import com.tsp.operation.model.beans.*;





public class TablaGeneralManagerDAO extends MainDAO {
    
    
    
    private LinkedList  tablas;
    private LinkedList  registros;
    
    
    
    
    
    /** Creates a new instance of TablaGenManagerDAO */
    public TablaGeneralManagerDAO(String dataBaseName) {
        super("TablaGeneralManagerDAO.xml",dataBaseName);
    }
    
    
     public String reset(String val){
        return val==null?"":val;
    }
     
     
     
    
     /**
     * Devuelve la lista de las tablas cargadas por el m�todo obtenerTablas()
     * @return La lista de tablas
     */    
    public LinkedList obtenerTablas(){
        return tablas;
    }
    
    
    
    /**
     * Devuelve una lista con los registros cargados por el m�todo buscarRegistros
     * @return La lista de registros.
     */    
    public LinkedList obtenerRegistrosDeTabla(){
        return registros;
    }
    
    
    
    
    
    
     /**
     * Busca las tablas registradas en tabla gen y las carga en la lista de tablas
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */    
    public void buscarTablas() throws SQLException{
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        Connection        con   = null;
        String            query = "SQL_OBTENER_TABLAS";
        tablas                  = new LinkedList();
        try {
                        
            con        = conectarJNDI( query );
            if( con != null ){
                
                String sql = obtenerSQL  ( query );
                st         = con.prepareStatement( sql );
                rs         = st.executeQuery();            
                while( rs.next() ){
                    TablaGen t = new TablaGen();
                        t.setTable_type  ( reset( rs.getString("table_code") ));
                        t.setDescripcion ( reset( rs.getString("descripcion")));
                    tablas.add(t);
                    t = null; // Liberar Espacio
                }
                
            }else
               throw new SQLException("No hay conexion");
            
        } catch( Exception ex ){
            ex.printStackTrace();
            throw new SQLException("ERROR OBTENIENDO TABLAS: "+ex.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}    
        }
    }
    
    
    
    
    /**
     * Agrega una tabla nueva en tablagen
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @throws InformationException si la tabla a agregar ya existe
     */    
     public void agregarTabla(String tipo, String descripcion, String user, String obs)throws SQLException, com.tsp.exceptions.InformationException {
        PreparedStatement st    = null;
        Connection        con   = null;
        String            query = "SQL_AGREGAR_TABLA";
        try {
            
            con        = conectarJNDI( query );
           
            if( con != null ){
                String sql  = obtenerSQL( query );            
                st          = con.prepareStatement( sql ); 

                st.setString(1, tipo);
                st.setString(2, descripcion);
                st.setString(3, obs);
                st.setString(4, user);

                st.executeUpdate();
            }else
               throw new SQLException("No hay conexion");
            
        }
        catch( SQLException ex ){
            ex.printStackTrace();
            if ( ex.getSQLState().equals("23505") ) {
                throw new com.tsp.exceptions.InformationException("La tabla "+tipo+" ya existe!");
            }
            throw new SQLException("ERROR AGREGANDO TABLA EN TABLAGEN: "+ex.getMessage());
        }
        finally{
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
         }
    }
    
    
    
    
    
    /**
     * Permite eliminar una o m�s tablas
     * @param tablas los nombre o tipos de las tablas
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */    
    public void eliminarTablas(String [] tablas)throws SQLException {
        PreparedStatement  st    =  null;
        Connection         con   =  null;
        String             query = "SQL_ELIMINAR_TABLAS";
        try {
            
            
            StringBuffer sb = new StringBuffer();
            if ( tablas.length > 1 ) {
                sb.append(tablas[0]+"',");
                for( int i=1; i< tablas.length-1; i++ ){
                    sb.append("'"+tablas[i]+"',");
                }
                sb.append("'"+tablas[tablas.length-1]);
            }
            else {
                sb.append(tablas[0]);
            }            
            
            
            con        = conectarJNDI( query );
            String sql = this.obtenerSQL  ( query ).replaceAll( "#TABLAS#" , sb.toString() );
            st         = con.prepareStatement(sql);
            st.executeUpdate();
            
            
        }
        catch( SQLException ex ){
            throw new SQLException("ERROR ELIMINANDO TABLAS EN TABLAGEN: "+ex.getMessage());
        }
        finally{
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }} 
       }
    }
    
    
    
    
    
    /**
     * Obtiene el registro correspondiente a una tabla sin sus registros, es decir
     * el nombre de la tabla y su descripci�n
     * @param type el nombre de la tabla, o el tipo
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @return Un bean <CODE>TablaGen</CODE> con los datos de la tabla
     */    
    public TablaGen obtenerTabla(String type)throws SQLException{
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        Connection        con   = null;
        String            query = "SQL_BUSCAR_TABLA";
        tablas                  = new LinkedList();
        try {
            
            con        = conectarJNDI( query );
            String sql  = obtenerSQL( query );            
            st          = con.prepareStatement( sql );            
            st.setString(1, type);
            rs          = st.executeQuery();            
            if (rs.next()){
                TablaGen t = new TablaGen();
                    t.setTable_type   ( reset( rs.getString("table_code") ));
                    t.setDescripcion  ( reset( rs.getString("descripcion")));
                    t.setDato         ( reset( rs.getString("dato")       ));
                return t;
            }
            return null;
        }
        catch( Exception ex ){
            throw new SQLException("ERROR BUSCANDO TABLA DE TABLAGEN: "+ex.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}      
        }
    }
    
    
    
    
    
    
     /**
     * Permite modificar una tabla dentro de tablgen, es decir su nombre y su descripci�n.
     * Cuando se modifica el nombre, tambi�n son modificados los registros que pertencen a
     * esa tabla. */    
    public void editarTabla(String tipo, String descripcion, String tipoViejo, String user, String obs)throws SQLException, com.tsp.exceptions.InformationException{
        PreparedStatement  st    = null;
        Connection         con   = null;
        String             query = "SQL_EDITAR_TABLA";
        try {
            
            con        = conectarJNDI( query );
            String sql  = obtenerSQL( query );            
            st          = con.prepareStatement( sql );
            
            st.setString(1, tipo);
            st.setString(2, descripcion);
            st.setString(3, obs);
            st.setString(4, user);
            st.setString(5, tipoViejo);
            
            st.setString(6, tipo);
            st.setString(7, user);
            st.setString(8, tipoViejo);
          
            st.executeUpdate();
            
        }
        catch( SQLException ex ){
            if ( ex.getSQLState().equals("23505") ) {
                throw new com.tsp.exceptions.InformationException("La tabla "+tipo+" ya existe!");
            }
            throw new SQLException("ERROR EDITANDO TABLA EN TABLAGEN: "+ex.getMessage());
        }
        finally{
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }} 
        }
    }
    
    
    
    
    
    
    /**
     * busca los registros de tablagen cuyo table_type sea igual al dado
     * @param table_type el tipo de la tabla o el nombre de la sub-tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */   
    public void buscarRegistros(String table_type)throws SQLException {
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        Connection        con   = null;
        String            query = "SQL_OBTENER_REGISTROS";
        registros               = new LinkedList();
        try {
            
            con        = conectarJNDI( query );
            String sql  = obtenerSQL( query );            
            st          = con.prepareStatement( sql ); 
            st.setString(1, table_type);
            rs = st.executeQuery();            
            while(rs.next()){
                
                TablaGen bean = new TablaGen ();
                    bean.setDescripcion ( reset( rs.getString ("descripcion")));
                    bean.setReferencia  ( reset( rs.getString ("referencia")));
                    bean.setTable_code  ( reset( rs.getString ("table_code")));
                    bean.setTable_type  ( reset( rs.getString ("table_type")));
                    bean.setOid         ( reset( rs.getString ("oid")  ));
                    bean.setDato        ( reset( rs.getString ("dato") ));
                    
                registros.add( bean );
            }
            
            
        } catch( Exception ex ){
            throw new SQLException("ERROR OBTENIENDO REGISTROS DE TABLAGEN: "+ex.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}  
        }
    }
    
    
    
    
     /**
     * Agregae un nuevo registro a una subtabla de tablagen
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @throws InformationException si el registro a agregar corresponde a uno ya existente
     */   
     public void agregarRegistro(String tipo, String codigo, String referencia, String descripcion, String user, String dato)throws SQLException, com.tsp.exceptions.InformationException {
        PreparedStatement st    = null;
        Connection        con   = null;
        String            query = "SQL_AGREGAR_REGISTRO";
        try {
            con        = conectarJNDI( query );
            String sql  = obtenerSQL( query );            
            st          = con.prepareStatement( sql ); 
            
            st.setString(1, tipo);
            st.setString(2, codigo);
            st.setString(3, referencia);
            st.setString(4, descripcion);
            st.setString(5, dato);            
            st.setString(6, user);
            
            st.executeUpdate(); 
            
        } catch( SQLException ex ){
            if ( ex.getSQLState().equals("23505") ) {
                throw new com.tsp.exceptions.InformationException("El registro ingresado para la tabla "+tipo+" ya existe!");
            }
            throw new SQLException("ERROR AGREGANDO REGISTRO EN TABLAGEN: "+ex.getMessage());
        }
        finally{
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}    
        }
    }
    
    
     /**
     * Permite eliminar uno o m�s registros
     * @param oids Los identificadores �nicos de los registros a eliminar.
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */  
    public void eliminarRegistros(String [] oids)throws SQLException {
        PreparedStatement  st    =  null;
        Connection         con   =  null;
        String             query = "SQL_ELIMINAR_REGISTRO";
        try {
            
            StringBuffer sb = new StringBuffer();
            if ( oids.length > 1 ) {
                sb.append(oids[0]+"',");
                for( int i=1; i< oids.length-1; i++ ){
                    sb.append("'"+oids[i]+"',");
                }
                sb.append("'"+oids[oids.length-1]);
            }
            else {
                sb.append(oids[0]);
            }
            
            con        = conectarJNDI( query );
            String sql = obtenerSQL  ( query ).replaceAll("'oids'","'"+sb.toString()+"'");
            st         = con.prepareStatement(sql);
            st.executeUpdate();
            
        }
        catch( SQLException ex ){
            throw new SQLException("ERROR ELIMINANDO REGISTRO EN TABLAGEN: "+ex.getMessage());
        }
        finally{
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }} 
        }
    }
    
    
    
    /**
     * perimte obtener un registro completo que coincida con el oid dado
     * @param oid el identificador �nico del registro
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @return un bean tipo <CODE>TablaGen</CODE> correspondiente al registro
     */    
    public TablaGen obtenerRegistro(String oid)throws SQLException{
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        Connection        con   = null;
        String            query = "SQL_BUSCAR_REGISTRO";
        tablas                  = new LinkedList();
        try {
            
            con        = conectarJNDI( query );
            String sql  = obtenerSQL( query );            
            st          = con.prepareStatement( sql );            
            st.setString(1, oid);
            rs          = st.executeQuery();            
            if (rs.next()){
                return TablaGen.load(rs);
            }
            return null;
        }
        catch( Exception ex ){
            throw new SQLException("ERROR BUSCANDO REGISTRO DE TABLAGEN: "+ex.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }} 
        }
    }
    
    
    
    
    
    
    
    
    
     /**
     * Permite modificar un registro de una subtabla de tablagen.
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @throws InformationException Si el nuevo nombre del registro, la referencia y la descripci�n
     * corresponden a un registro ya existente.
     */    
   public void editarRegistro (String oid,String tipo, String codigo, String referencia, String descripcion, String user, String dato)throws SQLException, com.tsp.exceptions.InformationException {
        PreparedStatement  st    = null;
        Connection         con   = null;
        String             query = "SQL_EDITAR_REGISTRO";
        try {
            con        = conectarJNDI( query );
            String sql  = obtenerSQL( query );            
            st          = con.prepareStatement( sql );         
            
            st.setString (1, tipo);
            st.setString (2, codigo);
            st.setString (3, referencia);
            st.setString (4, descripcion);
            st.setString (5, dato);            
            st.setString (6, user);            
            st.setString (7, oid);
            st.executeUpdate ();
            
        }
        catch( SQLException ex ){
            if ( ex.getSQLState ().equals ("23505") ) {
                throw new com.tsp.exceptions.InformationException ("El registro editado para la tabla "+tipo+" ya existe!");
            }
            throw new SQLException ("ERROR EDITANDO REGISTRO EN TABLAGEN: "+ex.getMessage ());
        }
        finally{
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }} 
        }
    }
    
    
    
    
    
//---------------------------------------------------------------------------------------------------------    
//                METODOS ESPECIFICOS POR PROGRAMAS
//---------------------------------------------------------------------------------------------------------
   
   
   
   /**
     * perimte obtener un registro referenciados para fintra
     */    
    public Hashtable obtenerRegistroReferenciado_fintra( String table_type, String referencia )throws SQLException{
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        Connection        con   = null;
        String            query = "SQL_BUSCAR_REFERENCIADO";
        Hashtable         datos = null;
        try {
            
            con        = conectarJNDI( query );
            String sql  = obtenerSQL( query );            
            st          = con.prepareStatement( sql );            
            st.setString(1, table_type );
            st.setString(2, referencia );
           // System.out.println(st.toString());
            rs          = st.executeQuery();            
            while ( rs.next() ){
                if( datos==null )
                    datos = new Hashtable();
                datos.put( reset(rs.getString("leyenda")), reset(rs.getString("contenido")) );
            }
            
        } catch( Exception ex ){
            throw new SQLException("ERROR BUSCANDO REGISTRO REFERENCIADO FINTRA: "+ex.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }} 
        }
        return datos;
    }
    
    
    
    
    /**
     * perimte obtener un registro referenciados para fintra
     */    
    public String getName( String nit )throws SQLException{
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        Connection        con   = null;
        String            query = "SQL_BUSCAR_NIT";
        String            name  = "";
        try {
            
            con        = conectarJNDI( query );
            String sql  = obtenerSQL( query );            
            st          = con.prepareStatement( sql );            
            st.setString(1, nit );
            rs          = st.executeQuery();            
            if ( rs.next() )
                name =  reset( rs.getString("payment_name")  );
            
        } catch( Exception ex ){
            throw new SQLException("ERROR getName: "+ex.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }} 
        }
        return name;
    }
    
    
    public String getDato(String table_type, String table_code) throws Exception{
        String dato = "";
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        String query = "DATO_TBG";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, table_type);
            ps.setString(2, table_code);
            rs = ps.executeQuery();
            if(rs.next()){
                dato  = rs.getString("dato");
            }
        }
        catch (Exception e) {
            throw new Exception("Error al buscar datos: "+e.toString());
        }
        finally {
            if(ps!=null){ try{ps.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return dato;
    }
    
    
    
    
    
//*********************************************************************************************************
//*********************************************************************************************************
//*********************************************************************************************************

    
    
    
    
    
}
