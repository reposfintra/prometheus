/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;


import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

/**
 *
 * @author dvalencia
 */
public interface AplicacionPagosMasivosDAO {

    /**
     * Leer el archivo para el pago masivo
     * @param is stream de datos del archivo
     * @param usuario usuario en sesi�n
     * @param dstrct distrito
     * @return true si genera el archivo correctamente
     * @throws IOException
     * @throws SQLException 
     */
    public boolean leerArchivoPagoMasivo(InputStream is, String usuario, String dstrct) throws IOException, SQLException;   

    public String CargarPagosMasivos(String usuario,String lote, String logica_aplicacion);

    public String AplicarPagosMasivos(String usuario,String lote, String logica_aplicacion);

    public String CargarComboLotes(String usuario);
    
    /**
     * 
     * @param usuario
     * @param lote
     * @return String
     */
    public String CargarComboLogicaApli(String usuario,String lote);

    public String EliminarLotePagoMasivo(String usuario, String lote);
    
    /**
     * 
     * @param lote
     * @param usuario
     * @return boolean
     */
    public boolean buscarLotePagos(String lote, String usuario);
    
    /**
     * 
     * @param usuario
     * @return 
     */
    public String CierrePlanAlDia(String usuario);

    public String ReclasificarLotes(String login, String lote);

    public String resumenArchivoAsobancaria(String Usuario,String idArchivo);

    public String cargarArchivoGenerarPago(String usuario);

    public String mostrarRefeInconsistencia(String idArchivo);

    public String generarLotePagos(String usuario,String idArchivo);

    public String updateRefrenciaNegocio(String idArchivo, String idNegocio, String idOrden, String idRows, String login);

    public String mostrarLogReferenciaPagos(String idArchivo);

}
