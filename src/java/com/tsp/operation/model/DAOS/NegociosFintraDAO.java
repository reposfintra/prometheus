/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.google.gson.JsonArray;
import com.tsp.operation.model.beans.ConceptoFactura;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.VariablesRefinanciacion;

/**
 --*
 * @author mcamargo
 */
public interface NegociosFintraDAO {
    
    public String cargarNegociosReliquidar();
    
    public String cargarLiquidacion(String negocio,String valor,String cuota,String convenio,String fechacuota,String identificacion);
  
    public String insert_document_neg_aceptados(String negocio,String valor,String cuota,String convenio,String fechacuota, String usuario,String identificacion);

    public String validar_negocio_activo(String numero_solicitud);

    public String CargarReportesSaldoCartera(String periodo, String unidad_negocio);

    public String CargarReporteNegociosNuevos(String fechaini, String fechafin, String unidad_negocio);

    public String Cargar_Combo_Aseguradoras();

    public String Cargar_Combo_Polizas();

    public String Cargar_Cxp_Aseguradoras(String nit_aseguradora, String id_poliza, String periodo);

    public String Generar_Cxp_consolidada_Aseguradora(String[] ArrayCxp, String login, String aseguradora,String poliza);

    public String Cargar_combo_cajas();

    public String Cargar_detalle_cxc_caja(String caja, String fecha);

    public String Generar_cxc_caja_recaudo(String[] ArrayCxp, String login,String caja,String fecha);

    public String cargar_solicitudes_reasignar(String usuario);

    public String cargar_combo_asesores();

    public String update_asesores(String[] ArraySolicitudes, String asesor, String login);

    public String cargar_negocios_refinanciar(String tipo_busqueda, String documento,String tipo_refi);

    public String cargar_detalle_cartera_refinanciar(String negocio,String tipo_refi,String fecha);

    public String ver_cartera_refinanciar(String negocio, String valor_refinanciar, String tipo_refi);
    
    public String proyeccion_refinanciacion_negocios(VariablesRefinanciacion refinanciacion,Usuario usuario);

    public String buscar_fecha_refi(String negocio,String tipo_refi,String fecha);

    public String ver_cartera_refinanciar_refi(String valor_refinanciar, String no_cuotas, String fecha, String ciudad, String compra_cartera, String cuota_inicio,String negocio);

    public String buscar_negocios_aprobar_refi(String documento, String tipo_busqueda, String estado, Usuario user);

    public String buscar_liquidacion_negocios_aprobar_refi(String negocio);

    public String aprobar_refinanciacion(Usuario usuario, String negocio, String tipo_refi, String key_ref,String fecha_vencimiento_acuerdo,String observacion);

    public String cargarComboUnidadNegocio(String usuario);

    public String cargarReporteNegociosCartera(String id_unidad_negocio,String id_entidad_recaudo);

    public String buscarCarteraPorAsignar(String id_unidad_negocio, String id_agencia, String id_altura_mora, String id_responsable_cuenta,String id_ciudad,String id_barrio);

    public String asignarCartera(String empresa_responsable, String id_unidad_negocio, String id_agencia, String id_altura_mora,String user,String[] ArrayNeg);

    public String exportarNegociosReporteCartera(String id_unidad_negocio, String id_entidad_recaudo);

    public String generarArchivoAsobancaria(String id_archivo, String user);

    public String cargarComboEntidadRecaudo();

    public String cargarRecaudos(String id_entidad_recaudo, String select_fecha);

    public String cargarRecaudoDetalle(String id_archivo);

    public String cargarRecaudadorTercero();

    public String cargarCarteraPorAsignar(String id_unidad_negocio, String id_entidad_recaudo);

    public String aprobarCarteraPorAsignar(String altura_mora_actual, String lote, String user);

    public String cargarComboResponsableCuenta(String sucursal);

    public String cargarComboCiudad(String sucursal);

    public String cargarComboBarrio(String id_ciudad);

    public String cargarCasasCobranza();

    public String cambiarEstadoCasaCobranza(String idCasaCobranza, Usuario usuario);

    public String updateCasaCobranza(String id_casa, String nit, String casa_cobranza, String direccion, String telefono, String nombre_contacto1, String telefono_contacto1, String cargo_contacto1, String email_contacto1, String nombre_contacto2, String telefono_contacto2, String cargo_contacto2, String email_contacto2, String nombre_contacto3, String telefono_contacto3, String cargo_contacto3, String email_contacto3, Usuario usuario);

    public String insertCasaCobranza(String id_casa, String nit, String casa_cobranza, String direccion, String telefono, String nombre_contacto1, String telefono_contacto1, String cargo_contacto1, String email_contacto1, String nombre_contacto2, String telefono_contacto2, String cargo_contacto2, String email_contacto2, String nombre_contacto3, String telefono_contacto3, String cargo_contacto3, String email_contacto3, Usuario usuario);

    public String rechazarCarteraPorAsignar(String altura_mora_actual, String lote, String login);

    public String generarCartasCobro(String id_unidad_negocio, String id_agencia, String id_altura_mora, Usuario user, String id_dia);

    public String cargarComboMora();
        
    public String getCuotaInicialApagar(ConceptoFactura fac, String tipo_ref);
   
    public String imprimirExtracto(double valorInicial, double ixm, double gac,  String key_ref, Usuario usuario);

    public String cargarComboEstrategiaCartera();

    public String verCarteraRefinanciar(String valor_capital, String no_cuotas, String fecha, String ciudad, String compra_cartera, String cuota_inicio, String tasa, String saldo_cat, String tipo_refi);
    
    public String anularRefinanciacion(String keyRefinaciacion,Usuario usuario, String observacion);

    public String cargarObservacionSimulacion(String key_ref);
    
}
