/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.ReasignacionNegAna;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Roberto Parra
 */
public class ReasignarNegociosDAO extends MainDAO{
       /** Creates a new instance of PerfilDAO */
    public ReasignarNegociosDAO() {
        super("ReasignarNegociosDAO.xml", "fintra");
    }
    
    private ReasignacionNegAna analistaNegocio;

    
      public ArrayList<ReasignacionNegAna> ListarAnalistas()throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_ANALISTAS";
        ArrayList<ReasignacionNegAna> lista= new ArrayList<>();
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                //st.setString( 1, usuario );
                rs = st.executeQuery();
               // vAnalistas = new Vector();                
                while(rs.next()){
                    analistaNegocio = new ReasignacionNegAna();
                    analistaNegocio.setAnalista(rs.getString("analista"));
                    lista.add(analistaNegocio);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return lista;
    }
      
      public ArrayList<ReasignacionNegAna> ListarAnalistasReasignar(String analista)throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_ANALISTAS_REASIGNAR";
        ArrayList<ReasignacionNegAna> lista= new ArrayList<>();
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString( 1, analista );
                rs = st.executeQuery();                
                while(rs.next()){
                    analistaNegocio = new ReasignacionNegAna();
                    analistaNegocio.setAnalista(rs.getString("analista"));
                    lista.add(analistaNegocio);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return lista;
    }
      
      public ArrayList<ReasignacionNegAna> RetornarNegociosAnalistaAgr(String analista)throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "";
        ArrayList<ReasignacionNegAna> lista= new ArrayList<>();
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                if(analista.equals("todos")){
                    query = "SQL_LISTAR_NEGOCIOS_DE_TODOS_LOS_ANALISTAS_AGRUPADOS";                    
                    st = con.prepareStatement(this.obtenerSQL(query));
                 }else{ 
                    query = "SQL_LISTAR_NEGOCIOS_DE_UN_ANALISTA_AGRUPADOS";    
                    st = con.prepareStatement(this.obtenerSQL(query));
                    st.setString( 1, analista );
                }
                rs = st.executeQuery();                
                while(rs.next()){
                    analistaNegocio = new ReasignacionNegAna();
                    //analistaNegocio.setAnalista(rs.getString("analista_asignado"));
                    analistaNegocio.setEstado_negocio(rs.getString("estado_negocio"));
                    analistaNegocio.setCant_cred_asig(rs.getString("cant_cred_asig"));
                    lista.add(analistaNegocio);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return lista;
    }
      
         
      public ArrayList<ReasignacionNegAna> RetornarNegociosAReasignar(String analistaAsignado,String analistaAReasignar)throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_RETORNAR_NEGOCIOS_A_REASIGNAR";
        ArrayList<ReasignacionNegAna> lista= new ArrayList<>();
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString( 1, analistaAsignado );
                st.setString( 2, analistaAReasignar );
                rs = st.executeQuery();                
                while(rs.next()){
                    analistaNegocio = new ReasignacionNegAna();
                    analistaNegocio.setNegocio(rs.getString("negocio"));
                    analistaNegocio.setEstado_negocio(rs.getString("estado_negocio"));
                    analistaNegocio.setAnalista(rs.getString("analista_asignado"));
                    analistaNegocio.setAnalista_a_reasignar(rs.getString("analista_a_reasignar"));
                    analistaNegocio.setId_cliente(rs.getString("id_cliente"));
                    analistaNegocio.setCliente(rs.getString("cliente"));
                    analistaNegocio.setValor(rs.getString("valor"));
                    analistaNegocio.setUnidad(rs.getString("unidad"));
                    analistaNegocio.setPerfil(rs.getString("perfil"));
                    analistaNegocio.setId_perfil(rs.getString("id_perfil"));
                    analistaNegocio.setMonto_minimo(rs.getString("monto_minimo"));
                    analistaNegocio.setMonto_maximo(rs.getString("monto_maximo"));
                    analistaNegocio.setCant_max_cred(rs.getString("cant_max_cred"));
                    analistaNegocio.setFecha_asignacion(rs.getString("fecha_asignacion"));
                    lista.add(analistaNegocio);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return lista;
    }
      
     public ArrayList<ReasignacionNegAna> RetornarNegociosAnalista(String analistaAsignado)throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "";
        ArrayList<ReasignacionNegAna> lista= new ArrayList<>();
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                
               if(analistaAsignado.equals("todos")){
                query = "SQL_RETORNAR_NEGOCIOS_DE_TODOS_LOS_ANALISTAS";
                st = con.prepareStatement(this.obtenerSQL(query));
                }else{
                query = "SQL_RETORNAR_NEGOCIOS_DE_UN_ANALISTA";
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString( 1, analistaAsignado );
                }

                rs = st.executeQuery();                
                while(rs.next()){
                    analistaNegocio = new ReasignacionNegAna();
                    analistaNegocio.setNegocio(rs.getString("negocio"));
                    analistaNegocio.setAnalista(rs.getString("analista_asignado"));
                    analistaNegocio.setEstado_negocio(rs.getString("estado_negocio"));
                    analistaNegocio.setId_cliente(rs.getString("id_cliente"));
                    analistaNegocio.setCliente(rs.getString("cliente"));
                    analistaNegocio.setValor(rs.getString("valor"));
                    analistaNegocio.setUnidad(rs.getString("unidad"));
                    analistaNegocio.setPerfil(rs.getString("perfil"));
                    analistaNegocio.setMonto_minimo(rs.getString("monto_minimo"));
                    analistaNegocio.setMonto_maximo(rs.getString("monto_maximo"));
                    analistaNegocio.setCant_max_cred(rs.getString("cant_max_cred"));
                    analistaNegocio.setFecha_asignacion(rs.getString("fecha_asignacion"));
                    lista.add(analistaNegocio);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return lista;
    }  
       
     public ArrayList<ReasignacionNegAna> RetornarCantidadNegocios(String analistaAsignado,
                                                                   String perfil)throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_CANTIDAD_MAXIMA_NEGOCIOS_POR_PERFIL";
        ArrayList<ReasignacionNegAna> lista= new ArrayList<>();
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString( 1, analistaAsignado );
                st.setInt(2, Integer.parseInt(perfil) );
                rs = st.executeQuery();                
                while(rs.next()){
                    analistaNegocio = new ReasignacionNegAna();
                    analistaNegocio.setAnalista(rs.getString("analista"));
                    analistaNegocio.setPerfil(rs.getString("idPerfil"));
                    analistaNegocio.setCant_cred_asig(rs.getString("cant_cred_asig"));
                    analistaNegocio.setCant_max_cred(rs.getString("cant_max_cred"));
                    lista.add(analistaNegocio);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return lista;
    }  


     public String buscarNegocio (String negocio)throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_NEGOCIO_EN_TEMPORAL";
        String respuesta="";
       // ArrayList<ReasignacionNegAna> lista= new ArrayList<>();
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString( 1, negocio );
                rs = st.executeQuery();                
                while(rs.next()){
                    analistaNegocio = new ReasignacionNegAna();
                    analistaNegocio.setNegocio(rs.getString("cod_neg"));
                    respuesta= analistaNegocio.getNegocio();
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return respuesta;
    } 




     
    public String ActualizarNegocio(String analistaAReasignar,
                                    String  negocio)throws SQLException 
                             {
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ACTUALIZAR_ASIGNACION_EN_TABLA_NEGOCIO";
        String respuesta="";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
               // vAnalistas = new Vector();        
              if( analistaAReasignar.equals("") ||negocio.equals("") )
              {
                  respuesta ="Todos los campos son obligatorios";
              }else{
                 
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, analistaAReasignar );
                st.setString(2, negocio );
                
             int est= st.executeUpdate();
             
            if(est>0){
                 respuesta ="ok";
            }else{  
                respuesta ="Error al actualizar el negocio";
            }
              }  
            }else{  
                respuesta ="Error al actualizar el negocio";
            }
        }catch(SQLException e){
             respuesta ="Error al actualizar el negocio";
           // throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        
        return respuesta;
    }
    
      
    public String RegistrarTemporal(String  analistaAReasignar,
                                    String  perfil,
                                    String  negocio, 
                                    String  dstrct, 
                                    String  update_user)throws SQLException 
                             {
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_REGISTRAR_ASIGNACION_EN_TABLA_TEMPORAL";
        String respuesta="";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
               // vAnalistas = new Vector();        
              if(analistaAReasignar.equals("") ||negocio.equals("")|| dstrct.equals("") ||update_user.equals("") )
              {
                  respuesta ="Todos los campos son obligatorios";
              }else{
                 
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, analistaAReasignar );
                st.setInt(2, Integer.parseInt(perfil)  );
                st.setString(3, negocio );
                st.setString(4, update_user );
                st.setString(5, dstrct );
                
             int est= st.executeUpdate();
             
            if(est>0){
                 respuesta ="ok";
            }else{  
                respuesta ="Error al actualizar la asignacion en la tabla temporal";
            }
              }  
            }else{  
                respuesta ="Error al actualizar la asignacion en la tabla temporal";
            }
        }catch(SQLException e){
             respuesta ="Error al actualizar la asignacion en la tabla temporal";
           // throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        
        return respuesta;
    }
        
    public String ActualizarTemporal(
                                    String  analistaAReasignar,
                                    String  perfil,
                                    String  negocio, 
                                    String  dstrct, 
                                    String  update_user)throws SQLException 
                             {
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ACTUALIZAR_ASIGNACION_EN_TABLA_TEMPORAL";
        String respuesta="";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
               // vAnalistas = new Vector();        
              if(analistaAReasignar.equals("") ||negocio.equals("")|| dstrct.equals("") ||update_user.equals("") )
              {
                  respuesta ="Todos los campos son obligatorios";
              }else{
                 
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, analistaAReasignar );
                st.setInt(2, Integer.parseInt(perfil)  );
                st.setString(3, update_user );
                st.setString(4, dstrct );
                st.setString(5, negocio );
                
             int est= st.executeUpdate();
             
            if(est>0){
                 respuesta ="ok";
            }else{  
                respuesta ="Error al actualizar la asignacion en la tabla temporal";
            }
              }  
            }else{  
                respuesta ="Error al actualizar la asignacion en la tabla temporal";
            }
        }catch(SQLException e){
             respuesta ="Error al actualizar la asignacion en la tabla temporal";
           // throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        
        return respuesta;
    }
    
         
    public String ActualizarHistorico(String  negocio )throws SQLException 
                             {
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_REGISTRAR_REASIGNACION_EN_TABLA_HISTORICO";
        String respuesta="";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
               // vAnalistas = new Vector();        
              if(negocio.equals("") )
              {
                  respuesta ="Todos los campos son obligatorios";
              }else{
                 
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, negocio );
                
             int est= st.executeUpdate();
             
            if(est>0){
                 respuesta ="ok";
            }else{  
                respuesta ="Error al actualizar el historico de la reasignacion";
            }
              }  
            }else{  
                respuesta ="Error al actualizar el historico de la reasignacion";
            }
        }catch(SQLException e){
             respuesta ="Error al actualizar el historico de la reasignacion";
           // throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        
        return respuesta;
    }
}
