   /***************************************
    * Nombre Clase ............. ImagenService.java
    * Descripci�n  .. . . . . .  Servicio para manipular las imagenes
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  10/10/2005
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/



package com.tsp.operation.model.DAOS;





import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;



public class ImagenDAO extends MainDAO{
    
    
    /* ________________________________________________________________________________________
     *                                  ATRIBUTOS
     * ________________________________________________________________________________________ */
    
    
    
    private static String ORDER_IMAGEN = " ORDER BY id DESC ";
    
    
    
      
    /* ________________________________________________________________________________________
     *                                  METODOS
     * ________________________________________________________________________________________ */
   
    
      
    public ImagenDAO() {
      super("ImagenDAO.xml");
    }
    
      
    
    
   
 
   
    
    
 /**
 * Devolvemos el listado de la relacion entre documentos y actividades
 * @autor: ....... Fernel Villacob
 * @throws ....... SQLException
 * @version ...... 1.0
 */

    public List searchActDoc() throws SQLException {
        Connection con = null;
        PreparedStatement  st    = null;
        ResultSet          rs    = null;
        String             query = "SQL_SEARCH_ACT_DOC";
        List               lista = null;     
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs=st.executeQuery();
            while(rs.next()){
                if( lista == null) lista = new LinkedList();
                String relacion = rs.getString(1) +"-"+ rs.getString(2)+"-"+ rs.getString(3);
                lista.add( relacion );                
            }
            
            }}catch(Exception e){
            throw new SQLException("Error searchActDoc() : "+ e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    
    
     
  /**
 * Devolvemos el listado de Imagenes que cumpla con los criterios  establecidos
 * @autor: ....... Fernel Villacob
 * @throws ....... SQLException
 * @version ...... 1.0
 */
    
     public List searcImagenes(  String checkActividad      , 
                                 String checkTipoDocumento  , 
                                 String checkDocumento      ,  
                                 String checkboxAgencia     , 
                                 String checkboxFecha       ,
                                 String checkboxCantidad    ,                                                                 
                                                                  
                                 String actividad          ,
                                 String tipoDocumento      , 
                                 String documento          , 
                                 String Agencia            ,
                                 String fechaIni           ,
                                 String fechaFin           ,
                                 String cantidad           ,                                 
                                 String ruta
                                 
                               ) throws SQLException {
                                   
       
                                   
        PreparedStatement  st    = null;
        ResultSet          rs    = null;
        List               lista = null;   
        String             query = "SQL_SEARCH_IMAGENES";
        Connection         con   = null;
        
        String             sql   = "";
        
        try{
            
            this.createDir(ruta);

            con = this.conectarJNDI(query);//JJCastro fase2
if(con!=null){
            sql          =  this.obtenerSQL( query );
            
            String where = " WHERE  reg_status ='' ";
            String resto = "";
            if( checkActividad     != null ||   checkTipoDocumento != null ||  checkDocumento     != null ||                
                checkboxAgencia    != null ||   checkboxFecha      != null ){
                    if ( checkActividad     !=null )  resto += " and  activity_type          = '"+ actividad      + "' "; 
                    if ( checkTipoDocumento !=null )  resto += " and  document_type          = '"+ tipoDocumento  + "' "; 
                    if ( checkDocumento     !=null )  resto += " and  upper(document)               = upper('"+ documento + "') "; 
                    if ( checkboxAgencia    !=null )  resto += " and  agencia                = '"+ Agencia        + "' "; 
                    if ( checkboxFecha      !=null )  resto += " and  creation_date  between   '"+ fechaIni       + " 00:00:00' and '" + fechaFin +" 23:59:59'";                    
            }
            sql += where + resto;            
            sql += this.ORDER_IMAGEN;
            
            if ( checkboxCantidad   !=null ) {
                sql += " LIMIT " + cantidad;
            }
            
            //jemartinez 
            if(cantidad!=null){
                sql += "LIMIT "+cantidad;
            }
            
            System.out.println("La consulta sql es: "+sql);
            st  =   con.prepareStatement( sql ); 
            rs  =   st.executeQuery();
            while(rs.next()){
                if( lista == null) lista = new LinkedList();
                Imagen imagen = new Imagen();
                   imagen.setActividad     ( rs.getString(1)      );
                   imagen.setTipoDocumento ( rs.getString(2)      );
                   imagen.setDocumento     ( rs.getString(3)      );
                   imagen.setNameImagen    ( rs.getString(4)      );
                   imagen.setBinary        ( rs.getBinaryStream(5));
                   imagen.setCreation_user ( rs.getString("creation_user"));
                   String fileName        = imagen.getNameImagen();                   
                   String[] name          = fileName.split(".");
                   if(name.length>0)                   
                          fileName        = name[0] + rs.getString(7).replaceAll(" |.|-|:","") + name[1];
                   
                   imagen.setFileName      ( fileName);
                   imagen.setFecha_creacion (rs.getString("creation_date").substring(0,16));
                   try{
                       
                 //--- Eliminamos la Imagen
                 //      delete(ruta,fileName);
                       
                 //--- Escribimos el archivo :               
                       InputStream      in   = rs.getBinaryStream(5);
                       int data;
                       File             f    = new File( ruta + fileName );
                       FileOutputStream out  = new FileOutputStream(f);
                       while( (data = in.read()) != -1 )
                           out.write( data );

                       in.close();
                       out.close();                       
                   }catch(Exception k){ throw new Exception(" FILE: " + k.getMessage());} 
                   
                   
                lista.add( imagen );                
            }
            
            
        }}catch(Exception e){
            throw new SQLException("Error searcImagenes() [DAO] : " + sql +" ->"+ e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
     
     
     
    
      
 /**
 * Insertamos la imagen a la bd
 * @autor: ....... Fernel Villacob
 * @param ........ ByteArrayInputStream bfin, int longitud,  Dictionary fields
 * @throws ....... SQLException
 * @version ...... 1.0
 */
    
     public void insertImagen(ByteArrayInputStream bfin, int longitud,  Dictionary fields ) throws SQLException {
        Connection con = null;
        PreparedStatement  st    = null;   
        String             query = "SQL_INSERT_IMAGEN";
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
              String actividad      =  (String)fields.get("actividad");  
              String tipoDocumento  =  (String)fields.get("tipoDocumento");  
              String documento      =  (String)fields.get("documento"); 
              String user           =  (String)fields.get("usuario");
              String agencia        =  (String)fields.get("agencia"); 
              String filename       =  (String)fields.get("imagen"); 
              
              if(documento.equals("")) 
                   documento = Utility.getHoy("");
            
              st.setString       (1, actividad);
              st.setString       (2, tipoDocumento);
              st.setString       (3, documento);  
              st.setString       (4, filename);
              
              st.setBinaryStream (5, bfin, longitud);
              st.setString       (6, user);
              st.setString       (7, agencia);
              
            
            st.execute();
            
        }}catch(Exception e){
            throw new SQLException("Error insertImagen() : "+ e.getMessage());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
     
     
     
     
     
 /**
 * Generamos  el sql para insertar la imagen
 * @autor: ....... Fernel Villacob
 * @param ........ ByteArrayInputStream bfin, int longitud,  Dictionary fields
 * @throws ....... SQLException
 * @version ...... 1.0
 */
    
     public String  insertImagenBatch(ByteArrayInputStream bfin, int longitud,  Dictionary fields ) throws SQLException {
         
        StringStatement  st    = null;
        String             query = "SQL_INSERT_IMAGEN";
        String             sql   = "";
        
        try{
               st = new StringStatement(this.obtenerSQL(query));//JJCastro fase2
             
              String actividad      =  (String)fields.get("actividad");  
              String tipoDocumento  =  (String)fields.get("tipoDocumento");  
              String documento      =  (String)fields.get("documento"); 
              String user           =  (String)fields.get("usuario");
              String agencia        =  (String)fields.get("agencia"); 
              String filename       =  (String)fields.get("imagen"); 
              
              if(documento.trim().equals("")) 
                   documento = Utility.getHoy("");
            
              st.setString       (actividad);
              st.setString       (tipoDocumento);
              st.setString       (documento);  
              st.setString       (filename);
              
              st.setBinaryStream ( bfin, longitud);
              st.setString       (user);
              st.setString       ( agencia);
              
           sql = st.getSql() +"; ";
         
            
        }catch(Exception e){
            throw new SQLException("Error insertImagenBatch() : "+ e.getMessage());
        }
        finally{
           if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return  sql;
    }
     
     
     
     
     
     
     
     
    
 /**
 * Devuelve el listado de Agencias
 * @autor: ....... Fernel Villacob
 * @throws ....... SQLException
 * @version ...... 1.0
 */
     public List searchAgencias() throws SQLException {
         Connection con = null;
         PreparedStatement  st    = null;
        ResultSet          rs    = null;
        String             query = "SQL_SEARCH_AGENCIAS";
        List               lista = null;     
        try{
            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs=st.executeQuery();
            while(rs.next()){
                if( lista == null) lista = new LinkedList();
                String agencia = rs.getString(1) +"-"+ rs.getString(2);
                lista.add( agencia );                
            }
            
            }}catch(Exception e){
            throw new SQLException("Error searchActividades() : "+ e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
     
       
 /**
 * Devuelve el listado de documentos
 * @autor: ....... Fernel Villacob
 * @throws ....... SQLException
 * @version ...... 1.0
 */
    
    public List searchDocumentos() throws SQLException {
        Connection con = null;
        PreparedStatement  st    = null;
        ResultSet          rs    = null;
        String             query = "SQL_SEARCH_DOCUMENTOS";
        List               lista = null;     
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs=st.executeQuery();
            while(rs.next()){
                if( lista == null) lista = new LinkedList();
                String actividad = rs.getString(1) +"-"+ rs.getString(2);
                lista.add( actividad );                
            }
            
            }}catch(Exception e){
            throw new SQLException("Error searchDocumentos() : "+ e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    
 /**
 * devuelve el listado de activiades
 * @autor: ....... Fernel Villacob
 * @throws ....... SQLException
 * @version ...... 1.0
 */
    
    public List searchActividades() throws SQLException {
        Connection con = null;
        PreparedStatement  st    = null;
        ResultSet          rs    = null;
        String             query = "SQL_SEARCH_ACTIVIDADES";
        List               lista = null;     
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs=st.executeQuery();
            while(rs.next()){
                if( lista == null) lista = new LinkedList();
                String actividad = rs.getString(1) +"-"+ rs.getString(2);
                lista.add( actividad ); 
            }
            
        }}catch(Exception e){
            throw new SQLException("Error searchActividades() : "+ e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    
    
    
    
        
     
 /**
 * Creamos el directorio del usuario, para poder mostrar las imagese encontradas
 * @autor: ....... Fernel Villacob
 * @param ........ (dir) directorio
 * @throws ....... Exception
 * @version ...... 1.0
 */   
  public void createDir(String dir) throws Exception{
   try{
        File f = new File(dir); 
        if(! f.exists() ) f.mkdir();
     /*   else{
            File []arc =  f.listFiles(); 
            while( arc.length >0  ){
               File  imagen = arc[0];
               imagen.delete();
               arc =  f.listFiles();
            }
        }*/
  }catch(Exception e){ throw new Exception ( e.getMessage());}
}
   
  
  
 /**
 * Eliminamos la imagen si ya existe
 * @autor: ....... Fernel Villacob
 * @param ........ (dir) directorio, (imagen)
 * @throws ....... Exception
 * @version ...... 1.0
 */   
  public void delete(String dir, String imagen) throws Exception{
   try{
        File f = new File(dir); 
        File []arc =  f.listFiles(); 
        for (int i=0;i<arc.length;i++){
            if(arc[i].getName().equals(imagen) )
               arc[i].delete();
       }
        
  }catch(Exception e){ throw new Exception ( e.getMessage());}
}
  
    
 
    
    
     
    
 
 /**
 * existeImagen, verifica en la tabla de imagen si existe la imagen.
 * @autor: ....... Ing. Diogenes Bastidas
 * @param ........ codigos, tipo actividad, tipo documento y docuemnto
 * @throws ....... Exception
 * @version ...... 1.0
 */   
  public boolean existeImagen(String act, String tipodoc, String doc) throws SQLException {
        Connection con = null;
        PreparedStatement st    = null;
        ResultSet         rs    = null;   
        boolean           sw    = false;
        String            query = "SQL_EXISTE_IMAGEN";
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, act );
                st.setString(2, tipodoc );
                st.setString(3, doc );                
                rs = st.executeQuery();
                if (rs.next()){
                    sw = true; 
                }
                
            }}catch(SQLException e){
             throw new SQLException("ERROR VERIFICAR SI EXISTE LA IMAGEN" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
    
    




/**
 * Realizamos la verificacion del documento
 * @autor: ....... Fernel Villacob
 * @throws ....... Exception
 * @version ...... 1.0
 */ 
    
   public boolean  validoDoc(String tipoDoc, String documento) throws Exception {
       
        PreparedStatement  st     = null;
        ResultSet          rs     = null;
        PreparedStatement  st2    = null;
        ResultSet          rs2    = null;
        boolean            estado = false;
        
        String             query1 = "SQL_BUSCAR_TABLA_VALIDACION";
        String             query2 = "SQL_EXISTENCIA_DOCUMENTO";
        Connection         con    = null;
        
        try{
         con = this.conectarJNDI(query1);//JJCastro fase2
        if(con!=null){
         //1. buscamos la tabla y la columna
            st = con.prepareStatement(this.obtenerSQL(query1));
            st.setString(1, tipoDoc);
            rs = st.executeQuery();
            String tabla    = "";
            String columna  = "";  
            int    swtable  = 0;
            if ( rs.next() ){
               tabla    =  rs.getString(1);
               columna  =  rs.getString(2);
               swtable  =  1;
            }
        //2. validamos en la tabla la existencia del documento
            if(swtable==1){ 
                String sql = this.obtenerSQL( query2).replaceAll("#COLUMNA#",  columna).replaceAll("#TABLA#", tabla);
                st2 =   con.prepareStatement( sql );
                st2.setString(1, documento);
                rs2=st2.executeQuery();
                while(rs2.next())
                    estado = true;                
                
            }else{
                estado = true;    /* No necesita tabla para validar */
            }
            
        }}catch(Exception e){
            throw new SQLException("Error searchActDoc() : "+ e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return estado;
    }

}
