/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * CodeudorDAO.java : clase que accede a los datos concernientes al codeudor
 * Copyright 2010 Rhonalf Martinez Villa <rhonaldomaster@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.tsp.operation.model.DAOS;
import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;
/**
 *
 * @author rhonalf
 */
public class CodeudorDAO extends MainDAO {

    public CodeudorDAO() {
        super("CodeudorDAO.xml");
    }

    /**
     * Busca un dato basado en un filtro
     * @param filtro el filtro a aplicar
     * @param cadena el dato a buscar
     * @return ArrayList con objetos Codeudor con los datos encontrados
     * @throws Exception cuando hay error
     */
    public ArrayList listaResults(String filtro,String cadena) throws Exception{
        ArrayList lista = new ArrayList();
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        Codeudor codeudor = null;
        String query = "BUSCAR_COD";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#filtro", filtro);
            sql = sql.replaceAll("#cadena", "'%"+cadena+"%'");
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while(rs.next()){
                codeudor = new Codeudor();
                codeudor.setCarrera(rs.getString("carrera"));
                codeudor.setCel(rs.getString("cel"));
                codeudor.setCiudad(rs.getString("ciudad"));
                codeudor.setCod(rs.getString("cod"));
                codeudor.setDireccion(rs.getString("direccion"));
                codeudor.setExpedicion_id(rs.getString("expedicion_id"));
                codeudor.setId(rs.getString("id"));
                codeudor.setNombre(rs.getString("nombre"));
                codeudor.setSemestre(rs.getString("semestre"));
                codeudor.setTelefono(rs.getString("telefono"));
                codeudor.setTipo_id(rs.getString("tipo_id"));
                codeudor.setUniversidad(rs.getString("universidad"));
                lista.add(codeudor);
                codeudor = null;
            }
            rs.close();
            st.close();
            this.desconectar(con);
        }
        catch (Exception e) {
            throw new Exception("Error en listaResults en codeudordao: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    /**
     * Busca los datos de un codeudor
     * @param filtro nombre de la columna por la cual se hace el fitro
     * @param cadena el dato a buscar en la columna especificada
     * @return objeto Codeudor con los datos
     * @throws Exception cuando hay un error
     */
    public Codeudor buscarDatosCodeudor(String filtro,String cadena) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        Codeudor codeudor = null;
        String query = "SRCH_COD";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#filtro", filtro);
            sql = sql.replaceAll("#cadena", ""+cadena+"");
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            if(rs.next()){
                codeudor = new Codeudor();
                codeudor.setCarrera(rs.getString("carrera"));
                codeudor.setCel(rs.getString("cel"));
                codeudor.setCiudad(rs.getString("ciudad"));
                codeudor.setCod(rs.getString("cod"));
                codeudor.setDireccion(rs.getString("direccion"));
                codeudor.setExpedicion_id(rs.getString("expedicion_id"));
                codeudor.setId(rs.getString("id"));
                codeudor.setNombre(rs.getString("nombre"));
                codeudor.setSemestre(rs.getString("semestre"));
                codeudor.setTelefono(rs.getString("telefono"));
                codeudor.setTipo_id(rs.getString("tipo_id"));
                codeudor.setUniversidad(rs.getString("universidad"));
            }
            rs.close();
            st.close();
            this.desconectar(con);
        }
        catch (Exception e) {
            throw new Exception("Error en buscarDatosCodeudor en codeudordao: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return codeudor;
    }

    /**
     * Actualiza los datos de un codeudor
     * @param codeudor Objeto Codeudor con los datos
     * @throws Exception Cuando hay un error
     */
    public void actualizarCodeudor(Codeudor codeudor) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        String query = "UPD_COD";
        String sql="";
        int filas = 0;
        try {
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#cadena", ""+codeudor.getId()+"");
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, codeudor.getTipo_id());
            st.setString(2, codeudor.getNombre());
            st.setString(3, codeudor.getExpedicion_id());
            st.setString(4, codeudor.getCel());
            st.setString(5, codeudor.getDireccion());
            st.setString(6, codeudor.getCiudad());
            st.setString(7, codeudor.getTelefono());
            st.setString(8, codeudor.getUniversidad());
            st.setString(9, codeudor.getCarrera());
            st.setString(10, codeudor.getSemestre());
            st.setString(11, codeudor.getCod());
            st.setString(12, codeudor.getUser_update());
            filas = st.executeUpdate();
            if(filas>0){
                System.out.println("Se ha actualizado el codeudor "+codeudor.getId()+" correctamente");
            }
            else{
                System.out.println("No se pudo actualizar el codeudor "+codeudor.getId());
            }
            st.close();
            this.desconectar(con);
        }
        catch (Exception e) {
            throw new Exception("Error en actualizarCodeudor en codeudordao: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
    }

}
