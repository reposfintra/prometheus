/*
 * Tipo_proveedorDAO.java
 *
 * Created on 22 de septiembre de 2005, 08:33 PM
 */

package com.tsp.operation.model.DAOS;


import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.*;
/**
 *
 * @author  Jm
 */
public class Tipo_proveedorDAO extends MainDAO{
    
    /*****************************************CONSULTA********************************/
    private static String SQLINSERT =   "   INSERT INTO TIPO_PROVEEDOR ( DSTRCT, CODIGO_PROVEEDOR, DESCRIPCION, CREATION_USER  ) VALUES ( ?, ?, ?, ? ) ";
    
    private static String SQLUPDATE =   "   UPDATE TIPO_PROVEEDOR SET CODIGO_PROVEEDOR = ? , DESCRIPCION = ?, USER_UPDATE = ?, LAST_UPDATE = NOW() WHERE CODIGO_PROVEEDOR = ? AND DESCRIPCION = ? ";
    
    private static String SQLDELETE =   "   DELETE FROM TIPO_PROVEEDOR WHERE CODIGO_PROVEEDOR = ? AND DESCRIPCION = ? ";
    
    private static String SQLSEARCH =   "   SELECT REG_STATUS, DSTRCT, CODIGO_PROVEEDOR, DESCRIPCION FROM TIPO_PROVEEDOR WHERE CODIGO_PROVEEDOR = ? AND DESCRIPCION = ? ";
    
    private static String SQLUPDATEESTADO   =   " UPDATE TIPO_PROVEEDOR SET REG_STATUS = ?, USER_UPDATE = ?, LAST_UPDATE = NOW() WHERE CODIGO_PROVEEDOR = ? AND DESCRIPCION = ?   ";
    
    private static String SQLLIST   =   "   SELECT REG_STATUS, DSTRCT, CODIGO_PROVEEDOR, DESCRIPCION FROM TIPO_PROVEEDOR    ";
    /** Creates a new instance of Tipo_proveedorDAO */
    public Tipo_proveedorDAO() {
        super("Tipo_ProveedorDAO.xml");
    }
    
    
    /*INSERT*/
    public void INSERT_TP(String dstrct, String codigo_proveedor , String descripcion, String usuario) throws SQLException {
        Connection con = null;
        PreparedStatement st          = null;
        String query = "SQL_INSERT";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, codigo_proveedor);
            st.setString(3, descripcion);
            st.setString(4, usuario);
            st.executeUpdate();
            }}
        catch(SQLException e) {
            throw new SQLException("Error en rutina INSERT_TP [Tipo_proveedorDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
     }
        
      /*UPDATE*/
    /**
     * 
     * @param ncodigo_proveedor
     * @param ndescripcion
     * @param usuario
     * @param codigo_proveedor
     * @param descripcion
     * @throws SQLException
     */
     public void UPDATE_TP(String ncodigo_proveedor, String ndescripcion, String usuario, String codigo_proveedor, String descripcion ) throws SQLException {
         Connection con = null;
         PreparedStatement st          = null;
         String query = "SQL_UPDATE";//JJCastro fase2
         try {
             con = this.conectarJNDI(query);
             if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, ncodigo_proveedor);
            st.setString(2, ndescripcion);
            st.setString(3, usuario);
            st.setString(4, codigo_proveedor);
            st.setString(5, descripcion);
            ////System.out.println("CONSULTA " + st);
            st.executeUpdate();
             }}
        catch(SQLException e) {
            throw new SQLException("Error en rutina UPDATE_TP [Tipo_proveedorDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
     
    /*SEARCH*/
     /**
      * 
      * @param codigo_proveedor
      * @param descripcion
      * @return
      * @throws SQLException
      */
    public Tipo_proveedor SEARCH_TP(String codigo_proveedor, String descripcion ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        Tipo_proveedor datos = null;
        String query = "SQL_SEARCH";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, codigo_proveedor);
            st.setString(2, descripcion);
            rs = st.executeQuery();
            while(rs.next()){
                datos = Tipo_proveedor.load(rs);
                break;
            }
            }}
        catch(Exception e) {
            throw new SQLException("Error en rutina SEARCH_TP [Tipo_proveedorDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return datos;
    }
    
    /*EXISTE*/
    /**
     *
     * @param codigo_proveedor
     * @param descripcion
     * @return
     * @throws SQLException
     */
    public boolean EXISTE_TP(String codigo_proveedor, String descripcion ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        boolean flag = false;
        String query = "SQL_SEARCH";
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, codigo_proveedor);
            st.setString(2, descripcion);
            rs = st.executeQuery();
            while(rs.next()){
                flag = true;
                break;
            }
        }}
        catch(Exception e) {
            throw new SQLException("Error en rutina EXISTE_TP [Tipo_proveedorDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return flag;
    }
    
    /*DELETE*/
    /**
     * 
     * @param codigo_proveedor
     * @param descripcion
     * @throws SQLException
     */
    public void DELETE_TP(String codigo_proveedor, String descripcion ) throws SQLException {
        Connection con = null;
        PreparedStatement st          = null;
        String query = "SQL_DELETE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,  codigo_proveedor);
            st.setString(2, descripcion);
            st.executeUpdate();
        }}
        catch(SQLException e) {
            throw new SQLException("Error en rutina DELETE_TP [Tipo_proveedorDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /*UPDATEESTADO*/
    /**
     * 
     * @param reg_status
     * @param codigo_proveedor
     * @param descripcion
     * @param usuario
     * @throws SQLException
     */

    public void UPDATEESTADO_TP(String reg_status, String codigo_proveedor, String descripcion,  String usuario ) throws SQLException {
        Connection con = null;
        PreparedStatement st          = null;
        String query = "SQL_UPDATEESTADO";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, reg_status);
            st.setString(2, usuario);
            st.setString(3, codigo_proveedor);
            st.setString(4, descripcion);
            st.executeUpdate();
            }}
        catch(SQLException e) {
            throw new SQLException("Error en rutina UPDATEESTADO_TP [Tipo_proveedorDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /*LIST*/
   /**
    * 
    * @return
    * @throws Exception
    */
    public List LIST() throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        String query = "SQL_LIST";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(Tipo_proveedor.load(rs));
                }
            }
            }}catch(Exception e){
            throw new SQLException("Error en rutina LIST [Tipo_proveedorDAO].... \n"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
}
