/***********************************************************************************
 * Nombre clase : ............... PlanillaSinCumplidoDAO.java                      *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD para generar los datos del reporte     *
 *                                de planillas sin cumplido                        *
 * Autor :....................... Ing. David Velasquez Gonzalez 
 * Modified:......................Ing. Enrique De Lavalle
 * Fecha :........................ 24 de noviembre de 2006, 11:35 AM               *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.*;

public class PlanillaSinCumplidoDAO extends MainDAO {                                                                                                                                             
    
   
    public PlanillaSinCumplidoDAO() {
        super("PlanillaDAO.xml");
        
    }   
    
    /**
     * Metodo listPlanillaSinCumplido, lista todas las planillas que se encuentran sin cumplido 
     * entre un rango de fechas como parametros de busqueda y un cliente como paramentros
     * adicionales
     * @autor : Ing. David Velasquez Gonzalez
     * @modif:  Ing. Enrique De Lavalle R.
     * @param : String fecha inicial , String fecha final, String cliente, String usuario
     * @version : 1.0
     */
    public List listPlanillaSinCumplido( String fechaInicial, String fechaFinal, String cliente) throws Exception{
        // conexion Base de Datos

        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;      
        List rList           = new LinkedList();     
        PlanillaSinCumplido reporte = new PlanillaSinCumplido();      
        String query = "SQL_PLANILLA_SIN_CUMPLIDO";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            
            st.setString(1, fechaInicial+ " 00:00");           
            st.setString(2, fechaFinal+" 23:59");        
            st.setString(3, cliente);
            System.out.println("query sin cumplido "+ st.toString());
            rs=st.executeQuery();
            
            if(rs!=null){
               
               while(rs.next()){   
                           
                    reporte = PlanillaSinCumplido.load(rs);
                    
                    //se define los datos de la columna dias descontando los dias festivos de la diferencia de la fecha del reporte a la 

                    int temporal = 0;          
                    int festivos = getContarDiasFes(reporte.getFechareporte(), Util.getFechaActual_String(9),reporte.getPais());
                    int dias = reporte.getDias() - festivos;                        
                    temporal = dias;
                    reporte.setDias(temporal);
                    if(temporal<=2){
                        reporte.setCalificacion(1.0);
                        reporte.setClasificacion("Menos de 2 Dias");
                    }   
                    else if(temporal>=3 && temporal<=5){
                        reporte.setCalificacion(0.5);                        
                        reporte.setClasificacion("Entre 3 y 5 Dias");
                    }
                    else if(temporal>5){
                        reporte.setCalificacion(0.0);
                        reporte.setClasificacion("Mas de 5 Dias");
                    }                    
                                                 
                    if (reporte.getDiasDeGen()>0){                        
                        int festivos2 = getContarDiasFes(reporte.getFechareporte(), reporte.getFecha_gen(),reporte.getPais());                         
                        reporte.setDiasDeGen(reporte.getDiasDeGen()-festivos2);                                                                  
                    }               
                                                                          
                    if (rList.isEmpty()){
                       reporte.setContaroc(1); 
                       rList.add(reporte);
                    }
                    else{
                        if(!existeNumPla(reporte.getNumpla(),rList))
                            reporte.setContaroc(1); 
                        else
                            reporte.setContaroc(0); 
                        
                        if (!rList.contains(reporte)){
                            rList.add(reporte);
                        }
                    }  
                   
                }
            }
            
            
            }}catch(Exception e){
            e.printStackTrace();
            throw new SQLException("Error en rutina LIST [PlanillaSinCumplidoDAO].... \n"+ e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return rList;
    }
    
      /* existeNumPla
     * @autor : Ing. Enrique De Lavalle Rizo
     * @param : String, List 
     */
    public boolean existeNumPla (String numPla, List rList){
        PlanillaSinCumplido  reporte;
        for (int i=0; i<rList.size();i++){
            reporte = (PlanillaSinCumplido) rList.get(i);
            if (numPla.equals(reporte.getNumpla())){
                return true;
            }
        }                    
        return false;
    }
    /* getFestivos
     * @autor : Ing. Enrique De Lavalle Rizo
     * @param : String, String, String 
     */
    public int getContarDiasFes( String fechai, String fechaf, String pais ) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_SACAR_FESTIVOS";
        int festivos = 0;
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, fechai );           
            st.setString(2, fechaf );        
            st.setString(3, pais );
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    festivos = rs.getInt("cant_festivos");                    
                }
            }
            }}catch(Exception e){
            e.printStackTrace();
            throw new SQLException("Error en rutina secuencia [ReporteOCAnticipoDAO].... \n"+ e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return festivos;
    }
    
}
