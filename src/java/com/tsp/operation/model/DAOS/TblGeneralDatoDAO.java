/******************************************************************
* Nombre ......................TblGeneralDatoDAO.java             *
* Descripci�n..................Clase DAO para Tabla General Dato  *
* Autor........................Ing. Armando Oviedo                *
* Fecha........................21/12/2005                         *
* Versi�n......................1.0                                *
* Coyright.....................Transportes Sanchez Polo S.A.      *
*******************************************************************/

package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import java.sql.*;
import java.util.*;

public class TblGeneralDatoDAO {
    
    private TblGeneralDato tgd;
    private Connection con;
    private PoolManager pm; 
    private Vector tblgendat;    
    
    /****  SQL QUERIES *****/
    
    private static String SQL_INSERT =  "INSERT " +
                                        "INTO " +
                                        "   tablagen_dato(dstrct, table_type, secuencia, leyenda, tipo, longitud, user_update, creation_user, base) " +
                                        "VALUES(?,?,?,?,?,?,?,?,?)";
    
    private static String SQL_UPDATE = "UPDATE " +
                                       "    tablagen_dato " +
                                       "SET " +
                                       "    last_update='now()', user_update=?, secuencia=?, leyenda=?, tipo=?, longitud=? " +                                       
                                       "WHERE " + 
                                       "    table_type=? AND secuencia=?";
    
    private static String SQL_BUSCAR = "SELECT table_type as codtabla, leyenda, tipo, longitud, secuencia, user_update, creation_user, dstrct, creation_date, base, last_update, reg_status FROM tablagen_dato WHERE table_type=? AND secuencia=?";
    
    private static String SQL_UPDEXIST = "UPDATE " +
                                         "  tablagen_dato " +
                                         "SET " +
                                         "  reg_status='', leyenda=?, tipo=?, longitud=?," +
                                         "  creation_date='now()', creation_user=?, last_update='now()', user_update=?, base=?, dstrct=? " +
                                         "WHERE " +
                                         "  table_type=? and secuencia=?";
    
    private static String SQL_EXISTE = "SELECT table_type as codtabla,secuencia FROM tablagen_dato WHERE table_type=? AND secuencia=? AND reg_status!='A'";
    
    private static String SQL_EXISTEANULADO = "SELECT table_type as codtabla,secuencia FROM tablagen_dato WHERE table_type=? AND secuencia=? AND reg_status='A'";
    
    private static String SQL_DELETE = "DELETE FROM tablagen_dato WHERE table_type=? AND secuencia=?";
    
    private static String SQL_BUSCARTODOSTBLGENDATO = "SELECT table_type as codtabla, secuencia, leyenda, tipo, longitud FROM tablagen_dato WHERE reg_status!='A' ORDER BY table_type, secuencia";
    
    private static String SQL_SEARCHSEQUENCE = "SELECT MAX(secuencia) FROM tablagen_dato WHERE table_type=?";
    
    private static String SQL_SEARCHCODIGO = "SELECT table_type as codtabla, secuencia, leyenda, tipo, longitud FROM tablagen_dato WHERE table_type=? AND reg_status!='A' ORDER BY secuencia";
    
    private static String SQL_SEARCHCODIGO2 = "SELECT table_type as codtabla, leyenda, tipo, longitud, secuencia, user_update, creation_user, dstrct, creation_date, base, last_update, reg_status FROM tablagen_dato WHERE table_type=? AND reg_status!='A' ORDER BY table_type, secuencia";
    
    private static String SQL_DELETESECUENCIAS = "DELETE FROM tablagen_dato WHERE table_type=? AND reg_status!='A'";    
    
    //diogenes 10-01-2006
    private static String SQL_LEYENDAS_TABLATYPE = "SELECT leyenda, tipo, longitud FROM tablagen_dato WHERE table_type = ? AND reg_status != 'A' ORDER BY secuencia::int ";
    
    /** Creates a new instance of TblGeneralDatoDAO */
    public TblGeneralDatoDAO() {
    }
    
    /**
     * M�todo que setea un objeto TblGeneralDato
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......TblGeneralDato tgd
     **/     
    public void setTablaGeneralDato(TblGeneralDato tgd){
        this.tgd = tgd;
    }
    
    /**
     * M�todo que retorna un objeto TblGeneralDato
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @return.......TblGeneralDato tgd
     **/     
    public TblGeneralDato getTablaGeneralDato(){
        return tgd;
    }
    
    /**
     * M�todo que busca y setea un objeto TblGeneralDato
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......TblGeneralDato de cargado con el set
     **/ 
    public void buscarTablaGeneralDato() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        TblGeneralDato tmp = null;
        try{            
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(SQL_BUSCAR);
            ps.setString(1, tgd.getCodTabla());
            ps.setString(2, tgd.getSecuencia());
            rs = ps.executeQuery();
            while(rs.next()){
                tmp = new TblGeneralDato();
                /*tmp.setCodTabla(rs.getString("codtabla"));        
                tmp.setSecuencia(rs.getString("secuencia"));
                tmp.setLeyenda(rs.getString("leyenda"));
                tmp.setTipo(rs.getString("tipo"));
                tmp.setLongitud(rs.getString("longitud"));*/
                tmp = tmp.loadResultSet(rs);                                                            
            }                        
            setTablaGeneralDato(tmp);            
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(rs != null){
                try{
                    rs.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }            
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * M�todo que retorna un boolean si existe el TblGeneralDato o no
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......c�digo de la tabla general dato cargado con el set
     **/     
    public boolean existe() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{            
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(SQL_EXISTE);            
            ps.setString(1, tgd.getCodTabla());
            ps.setString(2, tgd.getSecuencia());
            rs = ps.executeQuery();            
            return rs.next();                            
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(rs != null){
                try{
                    rs.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
        return false; 
    }
    
    /**
     * M�todo que retorna un boolean si existe un item TblGeneralDato, previamente anulado
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......c�digo de tabla general dato cargado con el set
     **/   
    public boolean existeAnulado() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{            
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(SQL_EXISTEANULADO);            
            ps.setString(1, tgd.getCodTabla());
            ps.setString(2, tgd.getSecuencia());
            rs = ps.executeQuery();            
            return rs.next();                            
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(rs != null){
                try{
                    rs.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
        return false;        
    }
    
    /**
     * M�todo que actualiza el reg_status de un TblGeneralDato anulado
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void addTablaGeneralDatoAnulado() throws SQLException{
        PreparedStatement ps = null;
        try{            
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(SQL_UPDEXIST);            
            ps.setString(1,tgd.getLeyenda());
            ps.setString(2,tgd.getTipo());
            ps.setString(3,tgd.getLongitud());            
            ps.setString(4,tgd.getCreationUser());            
            ps.setString(5,tgd.getUserUpdate());
            ps.setString(6,tgd.getBase());
            ps.setString(7,tgd.getDstrct());
            ps.setString(8,tgd.getCodTabla());
            ps.setString(9,tgd.getSecuencia());
            ps.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * M�todo que agrega un TblGeneralDato
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void addTablaGeneralDato() throws SQLException{
        PreparedStatement ps = null;
        try{            
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(SQL_INSERT);            
            ps.setString(1,tgd.getDstrct());
            ps.setString(2,tgd.getCodTabla());
            ps.setString(3,tgd.getSecuencia());
            ps.setString(4,tgd.getLeyenda());
            ps.setString(5,tgd.getTipo());
            ps.setString(6,tgd.getLongitud());
            ps.setString(7,tgd.getUserUpdate());            
            ps.setString(8,tgd.getCreationUser());
            ps.setString(9,tgd.getBase());            
            ps.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }        
    }
    
    /**
     * M�todo que modifica un TblGeneralDato
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void update() throws SQLException{
        PreparedStatement ps = null;
        try{
            pm =  PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setString(1, tgd.getUserUpdate());
            ps.setString(2, tgd.getSecuencia());
            ps.setString(3, tgd.getLeyenda());
            ps.setString(4, tgd.getTipo());
            ps.setString(5, tgd.getLongitud());
            ps.setString(6, tgd.getCodTabla());
            ps.setString(7, tgd.getSecuencia());
            ps.executeUpdate();           
        }catch(SQLException ex){       
            ex.printStackTrace();
        }
        finally{
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }        
    }
    
    /**
     * M�todo que elimina un TblGeneralDato
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void delete() throws SQLException{
        PreparedStatement ps = null;
        try{
            pm =  PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(SQL_DELETE);                        
            ps.setString(1, tgd.getCodTabla());
            ps.setString(2, tgd.getSecuencia());
            ps.executeUpdate();            
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * M�todo que setea un vector que contiene todos los objetos de la tabla
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void buscarTodosTablaGeneralDato() throws SQLException{        
        PreparedStatement ps = null;
        ResultSet rs = null;
        tblgendat = new Vector();
        try{
            pm =  PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(SQL_BUSCARTODOSTBLGENDATO);            
            rs = ps.executeQuery();            
            while(rs.next()){
                TblGeneralDato tmp = new TblGeneralDato();
                tmp.setCodTabla(rs.getString(1));
                tmp.setSecuencia(rs.getString(2));
                tmp.setLeyenda(rs.getString(3));
                tmp.setTipo(rs.getString(4));
                tmp.setLongitud(rs.getString(5));
                tblgendat.add(tmp);
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(rs != null){
                try{
                    rs.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * M�todo que guetea todos los descuentosequipos
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     * @return......Vector de objetos TblGeneralDato
     **/     
    public Vector getTodosTablaGeneralDato(){
        return this.tblgendat;
    }  
    
    /**
     * M�todo que guetea una secuencia dado el c�digo
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     * @return......Secuencia
     **/     
    public int getSequencyNumber(String codtabla) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;        
        int seq = 0;
        try{
            pm =  PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(SQL_SEARCHSEQUENCE);  
            ps.setString(1, codtabla);
            rs = ps.executeQuery();            
            while(rs.next()){
                seq = rs.getInt(1);
            }
            seq++;
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(rs != null){
                try{
                    rs.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
        return seq;
    }
    
    /**
     * M�todo que setea un Vector que contiene objetos tipo TblGeneralDato
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     */     
    public void buscarItemsCodigoTabla(String codtabla) throws SQLException{        
        PreparedStatement ps = null;
        ResultSet rs = null;
        tblgendat = new Vector();
        try{
            pm =  PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(SQL_SEARCHCODIGO);            
            ps.setString(1, codtabla);
            rs = ps.executeQuery();            
            while(rs.next()){
                TblGeneralDato tmp = new TblGeneralDato();
                tmp.setCodTabla(rs.getString(1));
                tmp.setSecuencia(rs.getString(2));
                tmp.setLeyenda(rs.getString(3));
                tmp.setTipo(rs.getString(4));
                tmp.setLongitud(rs.getString(5));
                tblgendat.add(tmp);
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(rs != null){
                try{
                    rs.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * M�todo que reordena las secuencias que han sido modificadas
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @param.......String secuencias[], String codtabla
     * @version.....1.0.
     */ 
    public void reordenarSecuenciasModificadas(String secuencias[], String codtabla) throws SQLException{
        PreparedStatement ps = null;        
        Vector tmp = new Vector();
        for(int i=1;i<secuencias.length;i++){
            
            TblGeneralDato tgdtmp = new TblGeneralDato();
            tgdtmp.setCodTabla(codtabla);
            tgdtmp.setSecuencia(secuencias[i]);
            setTablaGeneralDato(tgdtmp);
            buscarTablaGeneralDato();
            tmp.add(getTablaGeneralDato());  
            
        }            
        reordenarSecuencias(tmp, codtabla);                
        
    }
    /**
     * M�todo que reordena las secuencias si alguna ha sido eliminada
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @param.......String codtabla
     * @version.....1.0.
     */ 
    public void recalcularSecuenciasEliminadas(String codtabla) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        Vector tmp = new Vector();        
        try{
            pm =  PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(SQL_SEARCHCODIGO2);               
            ps.setString(1, codtabla);
            ////System.out.println(ps);
            rs = ps.executeQuery();
            while(rs.next()){
                TblGeneralDato tgdtmp = new TblGeneralDato();                
                tgdtmp = tgdtmp.loadResultSet(rs);                
                tmp.add(tgdtmp);
            }
            ////System.out.println("********************* tama�o vector: "+tmp.size());
            reordenarSecuencias(tmp, codtabla);                        
            
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(rs != null){
                try{
                    rs.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }               
    }
    
    /**
     * M�todo que reordena las secuencias dado un vector de objeto TblGeneralDato
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @param.......String codtabla
     * @version.....1.0.
     */ 
    public void reordenarSecuencias(Vector vectgd, String codtabla) throws SQLException{
        PreparedStatement ps = null;
        try{
            pm =  PoolManager.getInstance();
            con = pm.getConnection("fintra");
            
            // Eliminar todas las secuencias pertenecientes a este c�digo
            ps = con.prepareStatement(SQL_DELETESECUENCIAS);            
            ps.setString(1, codtabla);
            ps.executeUpdate();
            // ................
            
            //Ingresar nuevamente las secuencias de este c�digo en orden
            for(int i=0;i<vectgd.size();i++){
                TblGeneralDato tgdtmp = (TblGeneralDato)(vectgd.elementAt(i));
                tgdtmp.setSecuencia(String.valueOf(i+1));
                setTablaGeneralDato(tgdtmp);
                addTablaGeneralDato();
            }            
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{            
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
    }
    

    /**
     * buscarLeyendaTable_type, busca las leyendas dependiendo al cod tabletype
     * @autor       Diogenes Bastidas Morales       
     * @throws      SQLException
     * @version     1.0.
     * @param       codigo tablatype
     **/ 
    public Vector buscarLeyendaTable_type(String tabletype) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        TblGeneralDato tmp = null;
        try{            
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(SQL_LEYENDAS_TABLATYPE);
            ps.setString(1, tabletype);
            ////System.out.println(ps);
            rs = ps.executeQuery();
            tblgendat = new Vector();
            while(rs.next()){
                tmp = new TblGeneralDato();
                tmp.setLeyenda(rs.getString("leyenda"));
                tmp.setTipo(rs.getString("tipo"));
                tmp.setLongitud(""+rs.getInt("longitud"));
                tblgendat.add(tmp);                                                         
            }                        
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(rs != null){
                try{
                    rs.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL BUSCAR LEYENDA" + e.getMessage() ); 
                   }
            }
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }            
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
        return tblgendat;
    }
    
}
