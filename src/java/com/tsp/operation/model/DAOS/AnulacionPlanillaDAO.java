/***********************************************************************************
 * Nombre clase : ............... AnulacionPlanillaDAO.java                        *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD para generar los datos del reporte     *
 *                                de planillas anuladas.                           *
 * Autor :....................... Ing. Juan Manuel Escandon Perez                  *
 * Modificado:....................Ing. Iv�n Dar�o Devia Acosta                     *
 * Fecha :........................ 23 de noviembre de 2005, 11:36 AM               *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.*; 




public class AnulacionPlanillaDAO extends MainDAO{
    
    private Vector vlr_recup;
    private Vector anticipos;
        
    /** Creates a new instance of AnulacionPlanillaDAO */
    public AnulacionPlanillaDAO() {
        super("AnulacionPlanillaDAO.xml");
    }
  
    
    /* LISTADO
     * @autor : Ing. Iv�n Dar�o Devia Acosta
     * @param : String numpla
     */
    public String getDiscrepancias( String numpla ) throws Exception{
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_DISCREPANCIA";
        String discrepancia  = "";
        AnulacionPlanilla reporte = new AnulacionPlanilla();
        try{
            st = this.crearPreparedStatement( query );
            st.setString(1, numpla);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    reporte.setDiscrepancia(rs.getString("discrepancia"));
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina secuencia [ReporteOCAnticipoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar( query );
        }
        return discrepancia;
    }    
   
    
     /* LISTADO
     * @autor : Ing. Iv�n Dar�o Devia Acosta
     * @param : String numpla
     */
    public String getTipoDiscrepancias( String numpla ) throws Exception{
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_DISCREPANCIA";
        String tipodiscrepancia  = "";
        AnulacionPlanilla reporte = new AnulacionPlanilla();
        try{
            st = this.crearPreparedStatement( query );
            st.setString(1, numpla);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    reporte.setTipodiscrepancia(rs.getString("tipodiscrepancia"));
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina secuencia [ReporteOCAnticipoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar( query );
        }
        return tipodiscrepancia;
    }    
   
    
    /**
     * Getter for property vlr_recup.
     * @return Value of property vlr_recup.
     */
    public java.util.Vector getVlr_recup() {
        return vlr_recup;
    }
    
    /**
     * Setter for property vlr_recup.
     * @param vlr_recup New value of property vlr_recup.
     */
    public void setVlr_recup(java.util.Vector vlr_recup) {
        this.vlr_recup = vlr_recup;
    }
    
    /**
     * Getter for property anticipos.
     * @return Value of property anticipos.
     */
    public java.util.Vector getAnticipos() {
        return anticipos;
    }
    
    /**
     * Setter for property anticipos.
     * @param anticipos New value of property anticipos.
     */
    public void setAnticipos(java.util.Vector anticipos) {
        this.anticipos = anticipos;
    }
   
 
    
      /* LISTADO
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param String numpla
     */
    public void anticiposPlanilla( String numpla ) throws Exception{
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_ANTICIPOS_PLANILLA";
        AnulacionPlanilla reporte = new AnulacionPlanilla();
        this.anticipos = null;
        this.anticipos = new Vector();
        
        try{
            st = this.crearPreparedStatement( query );
            st.setString(1, numpla);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    anticipos.add(AnulacionPlanilla.loadAnticipo(rs));
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina secuencia [ReporteOCAnticipoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar( query );
        }
    }
     /* LISTADO
     * @autor : Ing. Iv�n Dar�o Devia Acosta
     * @param : String numpla
     */
    public void getValorRecuperar( String numpla ) throws Exception{
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_VALOR_RECUPERAR";
        double valorrecuperar  = 0;
        vlr_recup = null; 
        vlr_recup = new Vector();
        
        try{
            st = this.crearPreparedStatement( query );
            st.setString(1, numpla);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    valorrecuperar = rs.getDouble("valorrecuperar");
                    this.vlr_recup.add(Double.valueOf(String.valueOf(valorrecuperar)));
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina secuencia [ReporteOCAnticipoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar( query );
        }
    } 
     /**
     * Metodo listAnulacionPlanillas, lista todas las planillas que se encuentran anuladas 
     * entre un rango de fechas como parametros de busqueda y una agencia y usuario como paramentros
     * adicionales
     * @autor : Ing. Juan Manuel Escandon Perez
     * @modificado: Ing. Iv�n Dar�o Devia Acosta
     * @param : String fecha inicial , String fecha final, String agencia, String usuario
     * @version : 1.0
     */
    public List listAnulacionPlanillas( String fechaInicial, String fechaFinal, String agencia, String usuario ) throws Exception{
        String query = "SQLCONSULTA";        
        PreparedStatement st = null;
        ResultSet rs = null;       
        PreparedStatement st2 = null;
        ResultSet rs2 = null;
        List lista = new LinkedList();
        Connection con = null;
        org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
        
        try{
            con = this.conectar(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            
            st.setString(1, fechaInicial + " 00:00:00");
            st.setString(2, fechaFinal + " 23:59:59");
            st.setString(3, agencia);
            st.setString(4, usuario);
            
            logger.info("?sql ocs anuladas: " + st);
            
            ////System.out.println("OC ANULADAS " + st.toString() );
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    
                    st2 = con.prepareStatement(this.obtenerSQL("SQL_OT_OC"));   
                    st2.setString(1, rs.getString("planilla"));
                    rs2 = st2.executeQuery();
                    
                    AnulacionPlanilla reporte = AnulacionPlanilla.load(rs);
                    
                    boolean sw = false;
                    while( rs2.next() ){
                        
                        reporte.setRemesa(rs2.getString("numrem"));
                        reporte.setCliente(rs2.getString("cliente"));
                        reporte.setDescripcion(rs2.getString("descripcion"));
                        reporte.setFacturacliente(rs2.getString("facturacliente"));
                            
                        if( !rs2.getString("reg_status").equals("A") ){
                            sw = true;
                            break;
                        }
                    }                 
                    
                    if( !sw ){
                        if( rs2.next() ){
                            reporte.setRemesa(rs2.getString("numrem"));
                            reporte.setCliente(rs2.getString("cliente"));
                            reporte.setDescripcion(rs2.getString("descripcion"));
                            reporte.setFacturacliente(rs2.getString("facturacliente"));
                        }
                    }
                    
                    logger.info("? tipo recup : " + reporte.getTipo_recuperacion());
                    double val = 0;
                    boolean s = false;
                    this.anticiposPlanilla(reporte.getNumpla());
                    logger.info("NUMPLA: " + reporte.getNumpla() + " - anticipos: " + this.anticipos.size());
                    
                    if( this.anticipos.size()==1 ){//AMATURANA 25.04.2007
                        if( reporte.getTipo_recuperacion().equals("CA") ){
                            val = 0;
                            reporte.setFormarecuperacion("CA - CHEQUE TSP ANULADO ");
                        } else if ( reporte.getTipo_recuperacion().equals("") ) {
                            reporte.setFormarecuperacion("");
                            val = 0;
                        } else if ( reporte.getTipo_recuperacion().equals("") ) {
                            reporte.setFormarecuperacion("");
                            val = 0;
                        } else if ( reporte.getTipo_recuperacion().equals("RC") ) {
                            reporte.setFormarecuperacion("RC - RECUPERACION CAMBISTA");
                            val = reporte.getValorforaneoanticipo();
                        } else if ( reporte.getTipo_recuperacion().equals("XP") || reporte.getTipo_recuperacion().equals("DE") ) {
                            if( reporte.getTipo_recuperacion().equals("XP") ){
                                reporte.setFormarecuperacion("XP - RECUPERACION PARCIAL EXTRACTO ");
                            } else {
                                reporte.setFormarecuperacion("DE - RECUPERACION TOTAL EXTRACTO");
                            }
                            this.getValorRecuperar(reporte.getNumpla());
                            if( this.vlr_recup.size()>0 ){
                                double vr = ((Double) this.vlr_recup.elementAt(0)).doubleValue();
                                if( reporte.getReanticipo().equals("N") )
                                    val = reporte.getValorforaneoanticipo() + vr;
                                else
                                    val = vr;
                            } else {
                                val = 0;
                            }
                        }
                    } else {
                        if( rs.getString("ESTADOANULACION")!=null ){
                            val = 0;
                            reporte.setFormarecuperacion("CA - CHEQUE TSP ANULADO ");
                        } else if ( reporte.getTipo_recuperacion().equals("") ) {
                            reporte.setFormarecuperacion("");
                            val = 0;
                        } else {
                            this.getValorRecuperar(reporte.getNumpla());
                            //if( s ) this.vlr_recup.removeElementAt(0);
                            if( this.vlr_recup.size()>0 ){
                                Integer index = null;
                                for( int i = 0; i < this.anticipos.size(); i++){
                                    AnulacionPlanilla aux = (AnulacionPlanilla) this.anticipos.elementAt(i);
                                    if( aux.getBanco().equals(reporte.getBanco())
                                    && aux.getSucursal().equals(reporte.getSucursal())
                                    && aux.getChequeanticipo().equals(reporte.getChequeanticipo())
                                    && aux.getValorforaneoanticipo() == reporte.getValorforaneoanticipo() ){
                                        index = new Integer(i);
                                        logger.info("?: " + aux.getValorforaneoanticipo() + "==" + reporte.getValorforaneoanticipo() + ", Index: " + i);
                                        break;
                                    }
                                }
                                //logger.info("? index: " + index.intValue() + ", size: " + this.vlr_recup.size() + " . Vlr: " + reporte.getValorforaneoanticipo());
                                //index = new Integer(index.intValue()==0 ? 0 : index.intValue() - 1);
                                //logger.info("? *** index: " + index.intValue() + ", size: " + this.vlr_recup.size());
                                if( index!=null && this.vlr_recup.size()>index.intValue() ){
                                    double vr = ((Double) this.vlr_recup.elementAt(index.intValue())).doubleValue();
                                    logger.info("*** vlr recup: " + vr);
                                    if( reporte.getReanticipo().equals("N") )
                                        val = reporte.getValorforaneoanticipo() + vr;
                                    else
                                        val = vr;
                                    if( val == 0 )
                                        reporte.setFormarecuperacion("DE - RECUPERACION TOTAL EXTRACTO ");
                                    else
                                        reporte.setFormarecuperacion("XP - RECUPERACION PARCIAL EXTRACTO ");
                                } else {
                                    reporte.setFormarecuperacion("DE - RECUPERACION TOTAL EXTRACTO");
                                    val = 0;
                                }
                            } else {
                                reporte.setFormarecuperacion("DE - RECUPERACION TOTAL EXTRACTO");
                                val = 0;
                            }
                            
                        }
                    }
                        
                    double valor = reporte.getValorforaneoanticipo() - val;
                    //logger.info("?vr_fo anticipo: " + reporte.getValorforaneoanticipo() + ", val: " + val);
                    reporte.setValorrecuperar(val);
                    reporte.setDiscrepancia(this.getDiscrepancias(reporte.getNumpla()));
                    reporte.setTipodiscrepancia(this.getTipoDiscrepancias(reporte.getNumpla()));
                                        
                    lista.add(reporte);                                                            
                }
            }
        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("Error en rutina LIST [AnulacionPlanillaDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            if(st2!=null) st2.close();
            if(rs2!=null) rs2.close();
            this.desconectar(query);
        }
        return lista;
    }
}
// tito 1 Marzo 2007
