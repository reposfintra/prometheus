/*
 * ReporteBancoTranferencia.java
 *
 * Created on 24 de octubre de 2006, 11:42 AM
 */

package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  EQUIPO12
 */
public class ReporteBancoTransferenciaDAO extends MainDAO {
    
    /** Creates a new instance of ReporteBancoTranferencia */
    public ReporteBancoTransferenciaDAO() {
        super("ReporteBancoTransferenciaDAO.xml");
    }
    public ReporteBancoTransferenciaDAO(String dataBaseName) {
        super("ReporteBancoTransferenciaDAO.xml", dataBaseName);
    }
    
    
    /**
     * M�todo que retorna el nombre de un puesto de control
     * @autor.......Ivan Gomez
     * @throws......SQLException
     * @version.....1.0.
     * @return.......Lista con los nit
     **/
    public List buscarNit() throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String nit = "";
        String nombre ="";
        List lista = null;
        String query = "SQL_BUSCAR_NIT";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = ps.executeQuery();
            if(rs!=null){
                lista = new LinkedList();
                while(rs.next()){
                    nit    = rs.getString("nit");
                    nombre = rs.getString("nombre");
                    Hashtable cl = new Hashtable();
                      cl.put("nit",    nit    );
                      cl.put("nombre", nombre );
                    
                    lista.add( cl);
                }
            }
            
        }}catch(SQLException ex){
            throw new SQLException("ERROR EN  buscarNit() SQL_BUSCAR_NIT, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    
    
    
    
    /**
     * M�todo que retorna el nombre de un puesto de control
     * @autor.......Ivan Gomez
     * @param.......c�digo pc
     * @throws......SQLException
     * @version.....1.0.
     * @return.......Nombre del puesto de control
     **/
    public List buscarPlanillas(String nit) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String planilla ="";
        List lista = null;
        String query = "SQL_BUSCAR_PLANILLAS";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, nit);
            rs = ps.executeQuery();
            if(rs != null){
                lista = new LinkedList();
                while(rs.next()){
                    planilla = rs.getString("numpla");
                    lista.add(planilla);  
                }
            }
            
        }}catch(SQLException ex){
            throw new SQLException("ERROR EN  SQL_BUSCAR_PLANILLAS, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
}
