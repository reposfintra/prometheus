/*
* Nombre        ReporteDevolucionesDAO.java
* Descripci�n   Clase para el acceso a los datos del reporte de devoluciones
* Autor         Alejandro Payares
* Fecha         12 de septiembre de 2005, 04:23 PM
* Versi�n       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/


package com.tsp.operation.model.DAOS;

import java.sql.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.xml.*;
import java.util.*;


/**
 * DAO para obtener datos necesarios para correr el reporte de devoluciones. Este
 * DAO utiliza el sistema de lectura de sentencias SQL a traves de archivos XML.
 * @author Alejandro Payares
 */
public class ReporteDevolucionesDiscrepDAO extends MainDAO {
    
    
    /**
     * Un array de tipo String para almacenar los nombres de los campos en la base de
     * datos que conforman el reporte de devoluciones.
     */    
    private String [] camposReporte;
    /**
     * Una lista que almacena todos los datos del reporte.
     */    
    private LinkedList datosDevoluciones;
    
    /**
     * Construye una nueva instancia de ReporteDevolucionesDAO
     * @throws SQLException si algun problema ocurre en la creacion del DAO
     */
    public ReporteDevolucionesDiscrepDAO() throws SQLException {
        super("ReporteDevolucionesDiscrepDAO.xml");
    }    
    
    /**
     * Buscar las devoluciones hechas por un cliente determinado.
     * @param loggedUser Usuario de la sesi�n activa.
     * @param args Criterios de b�squeda. Son los siguientes:
     * <OL>
     *  <LI>Cliente (Requerido)
     *  <LI>Fecha Inicial (Opcional)
     *  <LI>Fecha Final (Opcional)
     *  <LI>Tipo de Devoluci�n (Requerido)
     *  <LI>N�mero del Pedido (Este anula los dem�s)
     * </OL>
     * @throws SQLException Si un error de acceso a la base de datos ocurre.
     */
    public void buscarDevoluciones(Usuario loggedUser, String[] args) throws SQLException {
        
             
        if (loggedUser == null )
            throw new SQLException("*** ERROR: Objeto Usuario es nulo!! ***");
        
        ResultSet rs = null;
        PreparedStatement ps = null;
        
        datosDevoluciones = null;
        String cliente   = args[0];
        String fechaIni  = args[1];
        String fechaFin  = args[2];
        String tipoDev   = args[3];
        String numPedido = args[4];
        String origen     = args[5];
        String destino    = args[6];
        
        String sql = obtenerSQL("SQL_REPORTE_DEVOLUCIONES");
        
        StringBuffer cSql = new StringBuffer(sql);
        cSql.append(" ");
        if( !numPedido.equals("") )
            cSql.append( "factcomercial = '" + numPedido + "' ");
        else{
            cSql.append( "rm.cliente = '" + cliente + "' ");
            if( !fechaIni.equals("") )
                cSql.append( "AND dp.fec_devolucion BETWEEN '" + fechaIni + "' AND '" + fechaFin + "' ");
            
            //--------------------------------------------//
            // Criterio "tipo devoluci�n" queda pendiente.
            //--------------------------------------------//
        }
        if(tipoDev.equals("OPEN"))
            cSql.append( "AND dp.fecha_cierre ='0099-01-01 00:00:00'");
        if(tipoDev.equals("CLOSED"))
            cSql.append( "AND dp.fecha_cierre !='0099-01-01 00:00:00'");
        
        if(loggedUser.getTipo().equals("DEST"))
            cSql.append( "AND rm.destinatario = '" + loggedUser.getNombreClienteDestinat() + "' ");
        if ( origen != null && origen.trim().length() > 0 && !origen.equals("ALL") ) {
            cSql.append(" AND pl.oripla = '"+ origen +"' ");
        }
        if ( destino != null && destino.trim().length() > 0 && !destino.equals("ALL") ) {
            cSql.append(" AND pl.despla = '"+ destino +"' ");
        }
        cSql.append( "ORDER BY dp.fec_devolucion");
        
        try {
            // OJO: AQUI USO ESTE SISTEMA POR QUE NECESITABA CREAR EL PREPARED STATEMENT DESPUES
            // DE CONCATENAR CONDICIONES EXTRAS AL WHERE QUE NO SE ENCUENTRAN EN EL ARCHIVO
            // XML, YA QUE DICHAS CONDICIONES SON VARIABLES DE ACUERDO AL TIPO DE CLIENTE.
            Connection con = conectar("SQL_REPORTE_DEVOLUCIONES");
            ps = con.prepareStatement( cSql.toString() );
            //System.out.println("QUERY DEVOLUCIONES: "+ps);
            rs = ps.executeQuery();
            this.buscarCamposReporteDevoluciones(cliente, "devolucionesDiscrep", convertirTipoUsuario(loggedUser),loggedUser.getLogin());
            String camposReporte [] = obtenerCamposReporteDevoluciones();
            if ( rs.next() ){
                datosDevoluciones = new LinkedList();
                do {
                    Hashtable fila = new Hashtable();
                    for( int i=0; i<camposReporte.length; i++ ){
                        try {
                            String valor = rs.getString(camposReporte[i]);
                            if ( camposReporte[i].equals("ultimoreporte") ) {
                                valor = valor.endsWith("-E")?valor.substring(0,8):"Sin entrega";
                            }
                            fila.put(camposReporte[i], valor == null?"":valor);
                        }
                        catch(Exception e){
                            if ( camposReporte[i].equals("bodegadev2") ){
                                fila.put(camposReporte[i], rs.getString("bodegadev"));
                            }
                            else if ( camposReporte[i].equals("refcode") ){
                                fila.put(camposReporte[i], obtenerPedidoFactura(rs.getString("numremesa")));
                            }
                        }
                    }
                    datosDevoluciones.add(fila);
                }while(rs.next());
            }
        }
        catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("REPORTE DE DEVOLUCIONES: ERROR BUSCANDO DEVOLUCIONES -> " + e.getMessage());
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        finally{
            if (ps != null) try { ps.close(); }catch(SQLException e){}
            desconectar("SQL_REPORTE_DEVOLUCIONES");
            
        }
        
    }
    
    /**
     * Permite obtener todos los pedidos asociados a una remesa
     * @param ot El numero de la remesa
     * @throws SQLException Si ocurre algun problema con el acceso a la base de datos
     * @return Una cadena con los pedidos separados por comas.
     */    
    public String obtenerPedidoFactura(String ot) throws SQLException{
        ResultSet rs         = null;
        PreparedStatement ps = null;
        StringBuffer pedido  = new StringBuffer();
        try{
            ps = crearPreparedStatement("SQL_FACTURA_PEDIDO");
            ps.setString(1, "WKO" );
            ps.setString(2, "1TSP " + ot );
            ps.setString(3, "002" );
            //System.out.println("obteniendo pedidos de factura: "+ps);
            rs = ps.executeQuery();
            //System.out.println("facturas obtenidas!");
            while(rs.next()){
                pedido.append(rs.getString(1));
                pedido.append(",");
            }
            if ( pedido.length() > 0 ){
                pedido.deleteCharAt(pedido.length()-1);
            }
        }
        catch(SQLException e){
            throw new SQLException("REPORTES DE CLIENTES: ERROR OBTENIENDO PEDIDOS DE FACTURA -> " + e.getMessage());
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        finally{
            if (ps != null) try { ps.close(); }catch(SQLException e){}
            desconectar("SQL_FACTURA_PEDIDO");
            return pedido.toString();
        }
    }
    
    /**
     * Permite obtener la lista donde estan almacenados los datos del reporte,
     * esta lista es llenada por el metodo buscarDevoluciones(...). , la lista
     * contiene en cada nodo un objeto Hashtable cuyas claves son los nombres de
     * los campos del reporte. Para obtener acceso a cada registro, basta con
     * recorrer la lista y extraer cada elemento Hashtable, y para obtener el valor
     * de un campo, basta con pasarle por parametro al metodo get(...) del Hashtable
     * el nombre del campo requerido, un ejemplo de esto ser�a asi:
     * <code>
     * <u>
     *    dao.buscarDevoluciones(... parametros ...);
     *    String [] campos = dao.obtenerCamposReporteDevoluciones();
     *    LinkedList lista = obtenerDatosDevoluciones();
     *    Iterator ite = lista.iterator();
     *    while( ite.hasNext() ) {
     *    <u>
     *        Hashtable fila = (Hashtable) ite.next();
     *        for( int i = 0; i < campos.length; i++ ) {
     *        <u>
     *            String valor = ((String)fila.get(campos[i]))
     *            //System.out.println(campos[i] + " = " + valor );
     *        </u>
     *        }
     *    </u>
     *    }
     * </u>
     * <code>
     * @return La lista con los datos del reporte.
     */    
    public LinkedList obtenerDatosDevoluciones(){
        return datosDevoluciones;
    }
    
    /**
     * Este m�todo permite obtener los titulos del encabezado del reporte de
     * devoluciones. El Hashtable que es devuelto contiene los titulos de todos
     * los campos del reporte y la clave de cada titulo es su respectivo nombre
     * de campo en la Base de datos, para obtener los nombre de los campos del
     * reporte utilize el metodo <CODE>String [] obtenerCamposReporteDevoluciones()</CODE>.
     * @throws SQLException Si algun problema ocurre en el acceso a la Base de Datos.
     * @return Una tabla con los titulos de los campos del reporte de devoluciones.
     */    
    public Hashtable obtenerTitulosDeReporteDevoluciones() throws SQLException{
        ConsultasGeneralesDeReportesDAO cgrd = new ConsultasGeneralesDeReportesDAO();
        return cgrd.obtenerTitulosDeReporte("devolucionesDiscrep");
    }
    
    /**
     * Este m�todo solo puede ser accesado dentro del DAO, y permite buscar los
     * nombres de los campos del reporte de devoluciones en la base de datos. Es
     * invocado por el m�todo <code>void buscarDevoluciones()</code> y el resultado
     * de la busqueda el devuelto por el m�todo
     * <CODE>String [] obtenerCamposReporteDevoluciones()</CODE>.
     * @param codigoCliente El codigo del cliente que ver� el reporte, este codigo es obtenido del usuario
     * que est� actualmente activo en la sesi�n.
     * @param codigoReporte El codigo del reporte de devoluciones.
     * @param tipocliente El tipo de cliente que ver� el reporte, este tipo depende del usuario que est�
     * actualmente activo en la sesi�n.
     * @throws SQLException Si algun problema ocurre con el acceso a la Base de datos.
     */    
    private void buscarCamposReporteDevoluciones(String codigoCliente,String codigoReporte, String tipocliente, String loggedUser) throws SQLException{
        ConsultasGeneralesDeReportesDAO cgr = new ConsultasGeneralesDeReportesDAO();
        camposReporte = cgr.buscarCamposDeReporte(codigoCliente, codigoReporte, tipocliente,loggedUser);
    }
    
    /**
     * Metodo de utilidad que permite convertir un tipo de usuario de sesi�n
     * a un tipo de usuario compatible con el tipo de usuario de un reporte.
     * @param loggedUser El usuario activo en la sesi�n actual, del cual se obtendr� el tipo.
     * @return El tipo de usuario compatible con el reporte.
     */    
    public static String convertirTipoUsuario(Usuario loggedUser){
        String tipo = loggedUser.getTipo();
        return (tipo.startsWith("CLIENTE")? "C" : (tipo.equals("DEST")? "D" : "U"));
    }
    
    /**
     * Devuelve un array de tipo String [] donde se encuentran almacenados los nombres
     * de los campos del reporte de devoluciones.
     * @return El array con los nombres de los campos del reporte de devoluciones.
     */    
    public String [] obtenerCamposReporteDevoluciones(){
        return camposReporte;
    }
}
