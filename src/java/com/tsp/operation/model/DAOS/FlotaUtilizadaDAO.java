/************************************************************************************
 * Nombre clase : ............... FlotaUtilizadaDAO.java                            *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )   *
 *                                los cuales contienen los metodos que interactuan  *
 *                                con la tabla Flota Utilizada.                     *
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                      *
 * Fecha :....................... 9 de diciembre de 2005, 09:00 AM                  *
 * Version :..................... 1.0                                               *
 * Copyright :................... Fintravalores S.A.                           *
 ************************************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.Util;

public class FlotaUtilizadaDAO {
    private int cont = 0;
    private Vector vecContador = new Vector();
    private FlotaUtilizada flota;
    
    private Vector vecFlotas;
    
    
    private static final String BUSQ_PLANILLAS_ENTREGADAS  =
    " SELECT  " +
    "         Distinct " +
    "         planilla.plaveh,  " +
    "         planilla.numpla,    " +
    "         tra.feinftr as fecha,  " +
    "         planilla.oripla,  " +
    "         planilla.despla,  " +
    "         ciudad.agasoc," +
    "	 ciudad2.agasoc,   " +
    "         cliente.nomcli, " +
    "         recursos.recurso   " +
    "    FROM   " +
    "         (SELECT   " +
    "                distinct (PLANILLA),  " +
    "                substr(get_ultimoreportetrafico(planilla),1,14)::timestamp as feinftr       " +
    "          FROM   " +
    "                trafimo   " +
    "          WHERE  tipomtr='E' and " +
    "                feinftr    " +
    "                between ? and ?" +
    "                and case when substr(get_ultimoreportetrafico(planilla),1,14)<>'-' then true else false end = true) tra   " +
    "    INNER JOIN planilla  on(planilla.vlrpla<>0 and planilla.reg_status<>'A' and tra.planilla=planilla.numpla)  " +
    "    LEFT OUTER JOIN ciudad on (planilla.despla=ciudad.codciu)   " +
    "    LEFT OUTER JOIN ciudad as ciudad2 on (planilla.oripla=ciudad2.codciu)   	" +
    "    INNER JOIN  plarem on (planilla.numpla=plarem.numpla)   " +
    "    LEFT OUTER JOIN remesa on (plarem.numrem=remesa.numrem)   " +
    "    INNER JOIN cliente on (cliente.codcli=remesa.cliente and codcli!='000073')   " +
    "    INNER JOIN placa   on (placa.placa=planilla.plaveh) " +
    "    INNER JOIN recursos on (recursos.equipo=placa.recurso) " +
    "    where ciudad.agasoc<>ciudad2.agasoc" +
    " order by planilla.numpla";
    
    private static final String BUSQ_PLANILLAS_SALIDAS  =
    "SELECT  " +
    "     Distinct  " +
    "     planilla.plaveh, " +
    "     planilla.numpla,   " +
    "     tra.feinftr as fecha, " +
    "     planilla.oripla, " +
    "     planilla.despla, " +
    "     ciudad.agasoc, " +
    "	 ciudad2.agasoc,   " +
    "     cliente.nomcli," +
    "     recursos.recurso   " +
    "FROM " +
    "     (select          " +
    "         distinct (PLANILLA),  " +
    "                 feinftr       " +
    "          from   " +
    "                trafimo   " +
    "          where   " +
    "                feinftr " +
    "                between ? and ?  " +
    "                and dirmotr='S'" +
    "   AND ESTADO<>'A'" +
    "	order by feinftr desc) tra  " +
    "inner join planilla  on(planilla.vlrpla<>0 and planilla.reg_status<>'A' and tra.planilla=planilla.numpla) " +
    "left outer join ciudad on (planilla.oripla=ciudad.codciu) " +
    "    LEFT OUTER JOIN ciudad as ciudad2 on (planilla.despla=ciudad2.codciu)   	" +
    "inner join plarem on (planilla.numpla=plarem.numpla)  " +
    "left outer join remesa on (plarem.numrem=remesa.numrem)  " +
    "inner join cliente on (cliente.codcli=remesa.cliente and codcli!='000073')  " +
    "INNER JOIN placa   on (placa.placa=planilla.plaveh)" +
    "INNER JOIN recursos on (recursos.equipo=placa.recurso)" +
    "    where ciudad.agasoc<>ciudad2.agasoc" +
    " order by planilla.numpla";
    
    private static final String INS_FLOTA_UTILIZADA =
    "Insert into flota_utilizada (tipo,numpla,placa,fecha,origen,destino,ori_dest,cliente,recurso)" +
    "Values (?,?,?,?,?,?,?,?,?)";
    
    private static final String VEH_ENTREGA_SALIDA =
    " SELECT" +
    "       a.*     " +
    " FROM" +
    "     ( SELECT" +
    "    	   A.*," +
    "  	           case when ( duracion>='00:00:00' and duracion<='5 days' ) then  1 else  0 end  as  clasificacion " +
    "       FROM" +
    "     	 (SELECT	    " +
    " 		        a.placa,  " +
    "                   a.numpla as plentrega," +
    "                   b.numpla as plsalida," +
    "    		a.nombre," +
    "  		        a.entrega," +
    "                   clentrega," +
    "                   b.fecha  as salida, " +
    "                   b.cliente as clsalida,"+
    "                   a.origen, " +
    "                   c.nomciu as destino, "+
    " 		       (b.fecha - a.entrega ) as duracion," +
    "                   b.recurso" +
    "	          FROM" +
    " 		       ( SELECT" +
    " 			       a.placa," +
    "                          a.numpla," +
    "			       g.nombre,   " +
    "			       a.ori_dest," +
    "			       a.fecha  as entrega," +
    "                          a.cliente as clentrega,"+
    "                          b.nomciu as origen"+
    "		          FROM " +
    " 			       FLOTA_UTILIZADA  a, Agencia g, Ciudad B" +
    "  		          WHERE" +
    "			       a.tipo = 'E' and a.ori_dest=g.id_agencia  and a.origen=B.codciu" +
    "		        ) a," +
    "		        FLOTA_UTILIZADA  b, Ciudad c " +
    " 	          WHERE" +
    " 		        a.placa     =  b.placa and " +
    " 		        a.ori_dest  =  b.ori_dest and " +
    "                   a.numpla    !=  b.numpla and"+
    " 		        b.tipo      =  'S' and " +
    "                   b.destino   = c.codciu " +
    "                   and substring(b.recurso,1,1) ='T' " +
    "            ) A" +
    "            ORDER BY 4,1,2" +
    "       )A where clasificacion='1' and duracion>='00:00:00' and duracion<='5 days' ";
    
    
    private static final String SOLO_VEH_ENTRGA =
    "select " +
    "      A.nombre," +
    "      F.placa, " +
    "      F.numpla as planillaL," +
    "      F.cliente,"+
    "      F.fecha  as fechaE," +
    "      B.nomciu as origen," +
    "      F.recurso " +
    "from" +
    "      flota_utilizada F, " +
    "      Agencia A, " +
    "      Ciudad B " +
    "where" +
    "      F.ori_dest = A.id_agencia and " +
    "      F.tipo='E' and " +
    "      F.origen = B.codciu " +
    "      and substring(f.recurso,1,1) ='T' order by numpla";
    
    private static final String SOLO_VEH_SALIDA =
    "Select " +
    "      A.nombre," +
    "      F.placa as placaD,     " +
    "      F.numpla as planillaD," +
    "      F.cliente,"+
    "      F.fecha  as fechaD,  " +
    "      B.nomciu as destino," +
    "      F.recurso " +
    "from " +
    "      flota_utilizada F, " +
    "      Agencia A, " +
    "      Ciudad B " +
    "where" +
    "     F.ori_dest = A.id_agencia and " +
    "     F.tipo='S' and " +
    "     F.placa in (Select placa from Flota_Utilizada where tipo='E') and " +
    "     F.destino = B.codciu " +
    "     and substring(f.recurso,1,1) ='T' " +
    " order by F.numpla";
    
    
    
    private static final String BUSQ_DATOS_RESUMEN=
    "SELECT" +
    "      T.nombre,  " +
    "      disp, " +
    "      util, " +
    "      ((util::numeric/S.total::numeric)*100)::numeric(15,2) as percutil, " +
    "      ((util::numeric/disp::numeric)*100)::numeric(15,2) as perretorno,  " +
    "      S.total      " +
    "FROM" +
    "     (select count(ori_dest)as total ,nombre from flota_utilizada,agencia a where tipo='S' and a.id_agencia=ori_dest group by nombre) S,  " +
    "     (select " +
    "            A.nombre, " +
    "            count(f.placa) as disp " +
    "      from   " +
    "           agencia A, flota_utilizada F " +
    "      where " +
    "           F.ori_dest=A.id_agencia and " +
    "           F.tipo='E' group by A.nombre order by A.nombre ) T left join  " +
    "     (SELECT " +
    "            nombre," +
    "            sum( aux )  as util " +
    "      FROM" +
    "           (" +
    "            SELECT" +
    "                  a.placa,  " +
    "                  a.nombre," +
    "                  a.entrega," +
    "                 clasificacion  as aux," +
    "                 count(*)" +
    "             FROM" +
    "                 ( " +
    "                  SELECT" +
    "               	    A.*," +
    "           	    case when ( duracion>='00:00:00' and duracion<='5 days ' ) then  1 else  0 end  as  clasificacion " +
    "                  FROM(" +
    "               	    SELECT	    " +
    "           		   a.placa,  " +
    "           		   a.nombre," +
    "                              a.entrega," +
    "           		   b.fecha  as salida," +
    "           		   (b.fecha  - a.entrega ) as duracion" +
    "           	    FROM" +
    "           		   ( SELECT" +
    "           			  a.placa," +
    "                              a.numpla," +
    "           			  g.nombre," +
    "           			  a.ori_dest," +
    "           			  a.fecha  as entrega" +
    "           		     FROM " +
    "           			  FLOTA_UTILIZADA  a, Agencia g" +
    "           		     WHERE" +
    "           			  a.tipo = 'E' and g.id_agencia=a.ori_dest) a," +
    "           		   FLOTA_UTILIZADA  b " +
    "           	    WHERE" +
    "           		  a.placa     =  b.placa and " +
    "           		  a.ori_dest  =  b.ori_dest and " +
    "                             a.numpla    != b.numpla and "+
    "           		  b.tipo      =  'S' ) A" +
    "                      ORDER BY 1,2,3) A" +
    "                 GROUP BY 1,2,3,4) a " +
    "        group by nombre order by nombre) R  " +
    "     on (T.nombre=R.nombre) " +
    " WHERE" +
    "     s.nombre=t.nombre";
    
    private static final String DELETE_FLOTAS =
    "Delete from flota_utilizada";
    
    /**
     * Metodo buscarPlanillasEntregadas, Busca las planillas que llegaron de la tabla trafimo
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : String fecha inicial String fecha final
     * @version : 1.0
     */
    public void buscarPlanillasEntregadas(String fechai, String fechaf) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        vecFlotas = new Vector();
        flota = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(DELETE_FLOTAS);
                st.execute();
                st = con.prepareStatement(BUSQ_PLANILLAS_ENTREGADAS);
                st.setString(1, fechai.replaceAll("-",""));
                st.setString(2, fechaf.replaceAll("-",""));
                ////System.out.println("Query ENTRADA:"+st.toString());
                rs = st.executeQuery();
                while (rs.next()) {
                    flota = new FlotaUtilizada();
                    flota.setTipo("E");
                    flota.setNumpla(rs.getString("numpla"));
                    flota.setPlaca(rs.getString("plaveh"));
                    flota.setFecha(rs.getString("fecha"));
                    flota.setOrigen(rs.getString("oripla"));
                    flota.setDestino(rs.getString("despla"));
                    flota.setOri_dest(rs.getString("agasoc"));
                    flota.setCliente(rs.getString("nomcli"));
                    flota.setRecurso(rs.getString("recurso"));
                    vecFlotas.addElement(flota);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR PLANILLAS ENTREGADAS" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    /**
     * Metodo buscarPlanillasSalidas, Busca las planillas despachadas de la tabla planilla
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : String fecha inicial String fecha final
     * @version : 1.0
     */
    public void buscarPlanillasSalidas(String fechai, String fechaf) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        vecFlotas = new Vector();
        flota = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(BUSQ_PLANILLAS_SALIDAS);
                st.setString(1, fechai);
                st.setString(2, fechaf);
                ////System.out.println("Query SALIDA : "+st.toString());
                rs = st.executeQuery();
                while (rs.next()) {
                    flota = new FlotaUtilizada();
                    flota.setTipo("S");
                    flota.setNumpla(rs.getString("numpla"));
                    flota.setPlaca(rs.getString("plaveh"));
                    flota.setFecha(rs.getString("fecha"));
                    flota.setOrigen(rs.getString("oripla"));
                    flota.setDestino(rs.getString("despla"));
                    flota.setOri_dest(rs.getString("agasoc"));
                    flota.setCliente(rs.getString("nomcli"));
                    flota.setRecurso(rs.getString("recurso"));
                    vecFlotas.addElement(flota);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR PLANILLAS DE SALIDA " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    /**
     * Metodo ingresarFlotaUtilizada, Agrega una flota a la tabla de flota_utilizada
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : String fecha inicial String fecha final
     * @version : 1.0
     */
    public void ingresarFlotaUtilizada(FlotaUtilizada flota) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        ResultSet rs=null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                int sw=0;
                st = con.prepareStatement("SELECT * FROM flota_utilizada where  tipo=? and  placa=? and numpla=?  and cliente=?");
                st.setString(1,  flota.getTipo());
                st.setString(2,  flota.getPlaca());
                st.setString(3,  flota.getNumpla());
                st.setString(4,  flota.getCliente());
                rs= st.executeQuery();
                if(rs.next()){
                    sw=1;
                    
                }
                if(sw==0){
                    st = con.prepareStatement(INS_FLOTA_UTILIZADA);
                    st.setString(1,  flota.getTipo());
                    st.setString(2,  flota.getNumpla());
                    st.setString(3,  flota.getPlaca());
                    st.setString(4,  flota.getFecha());
                    st.setString(5,  flota.getOrigen());
                    st.setString(6,  flota.getDestino());
                    st.setString(7,  flota.getOri_dest());
                    st.setString(8,  flota.getCliente());
                    st.setString(9,  flota.getRecurso());
                    st.execute();
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL IMGRESAR FLOTA UTILIZADA " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    public Vector getVectorFlotas() {
        return vecFlotas;
    }
    /**
     * Metodo buscarVehiculosConEntradaSalida, Guarda en un vector los vehiculos que
     * hayan entrado y despues hayan sido despachados de una angencia dada
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @version : 1.0
     */
    public void buscarVehiculosConEntradaSalida() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        vecFlotas = new Vector();
        flota = null;
        String clasificacion = "";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(VEH_ENTREGA_SALIDA);
                rs = st.executeQuery();
                while (rs.next()) {
                    flota = new FlotaUtilizada();
                    flota.setOri_dest(rs.getString("nombre"));
                    flota.setNumpla(rs.getString("plentrega"));
                    flota.setFecha(rs.getString("entrega"));
                    flota.setCliente(rs.getString("clentrega"));
                    flota.setOrigen(rs.getString("origen"));
                    flota.setPlaca(rs.getString("placa"));
                    flota.setPlanilla_Salida(rs.getString("plsalida"));
                    flota.setFecha_salida(rs.getString("salida"));
                    flota.setClienteSalida(rs.getString("clsalida"));
                    flota.setDestino(rs.getString("destino"));
                    flota.setRecurso(rs.getString("recurso"));
                    if (!existeRegistro(flota) && !existePlacaPlanillaSalida(flota)) {
                        vecFlotas.addElement(flota);
                    }
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR VEHICULOS CON ENTRADA Y SALIDA " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    /**
     * Metodo buscarVehiculosConEntradaSinSalida,Agreega al vector de floas los vehiculos
     * que tienen tipo Enrega y no  se les dio salida.
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @version : 1.0
     */
    public void buscarVehiculosConEntradaSinSalida() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SOLO_VEH_ENTRGA);
                rs = st.executeQuery();
                while (rs.next()) {
                    flota = new FlotaUtilizada();
                    flota.setOri_dest(rs.getString("nombre"));
                    flota.setNumpla(rs.getString("planillaL"));
                    flota.setCliente(rs.getString("cliente"));
                    flota.setFecha(rs.getString("fechaE"));
                    flota.setPlaca(rs.getString("placa"));
                    flota.setOrigen(rs.getString("origen"));
                    flota.setPlanilla_Salida("");
                    flota.setFecha_salida("");
                    flota.setRecurso(rs.getString("recurso"));
                    if (!this.existePlacaPlanilla(flota)) {
                        vecFlotas.addElement(flota);
                    }
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR VEHICULOS CON ENTRADA SIN SALIDA " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    e.printStackTrace();
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    /**
     * Metodo buscarVehiculosConSalidaSinEntrada, Agrega al vector de flotas los vehiculos
     * que tienen salida desde la tabla de planillas
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @version : 1.0
     */
    public void buscarVehiculosConSalidaSinEntrada() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SOLO_VEH_SALIDA);
                rs = st.executeQuery();
                while (rs.next()) {
                    flota = new FlotaUtilizada();
                    flota.setOri_dest(rs.getString("nombre"));
                    flota.setNumpla("");
                    flota.setFecha("");
                    flota.setPlanilla_Salida(rs.getString("planillaD"));
                    flota.setClienteSalida(rs.getString("cliente"));
                    flota.setFecha_salida(rs.getString("fechaD"));
                    flota.setPlaca(rs.getString("placaD"));
                    flota.setDestino(rs.getString("destino"));
                    flota.setRecurso(rs.getString("recurso"));
                    if (!this.existePlacaPlanillaSalida(flota)) {
                        vecFlotas.addElement(flota);
                    }
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR VEHICULOS CON SALIDA SIN ENTRADA " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    /**
     * Metodo buscarDatosResumen, busca los vehiculos disponibles, los utilizados y los despachados
     * realiza un conteo y los guarda en un vector.
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @version : 1.0
     */
    public Vector buscarDatosResumen() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Vector resumen = new Vector();
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(BUSQ_DATOS_RESUMEN);
                ////System.out.println("SQL: "+st);
                rs = st.executeQuery();
                while (rs.next()) {
                    ResumenFlota r = new ResumenFlota();
                    r.setAgencia(rs.getString("nombre"));
                    r.setVehDisponibles((rs.getString("disp")!=null && !rs.getString("disp").equals("")) ? rs.getString("disp") :"0");
                    r.setVehUtilizados((rs.getString("util")!=null && !rs.getString("util").equals("")) ? rs.getString("util") :"0");
                    r.setPerUtilidad((rs.getString("percutil")!=null && !rs.getString("percutil").equals("")) ? rs.getString("percutil") :"0");
                    r.setPerRetorno((rs.getString("perretorno")!=null && !rs.getString("perretorno").equals("")) ? rs.getString("perretorno") :"0");
                    r.setTotal(rs.getDouble("total"));
                    resumen.addElement(r);
                }
            }
            return resumen;
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR DATOS DEL RESUMEN " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    /**
     * Metodo existeRegistro, verifica que no se haya agregado al vector de flotas
     * un registro con una placa y fecha especifica
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @version : 1.0
     */
    private boolean existeRegistro(FlotaUtilizada flota) {
        for (int i=0; i<vecFlotas.size(); i++ ){
            FlotaUtilizada fl = (FlotaUtilizada)vecFlotas.elementAt(i);
            if (fl.getPlaca().equals(flota.getPlaca()) &&
            fl.getFecha().equals(flota.getFecha()))  {
                return true;
            }
        }
        return false;
    }
    /**
     * Metodo existePlacaPlanilla, verifica que no se haya agregado al vector de flotas
     * un registro con una placa y Planilla especifica
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @version : 1.0
     */
    private boolean existePlacaPlanilla(FlotaUtilizada flota) {
        for (int i=0; i<vecFlotas.size(); i++ ){
            FlotaUtilizada fl = (FlotaUtilizada)vecFlotas.elementAt(i);
            if (fl.getPlaca().equals(flota.getPlaca()) &&
            fl.getNumpla().equals(flota.getNumpla()))  {
                return true;
            }
        }
        return false;
    }
    /**
     * Metodo existePlacaPlanillaSalida, verifica que no se haya agregado al vector de flotas
     * un registro con una placa y Planilla de Salida especifica
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @version : 1.0
     */
    private boolean existePlacaPlanillaSalida(FlotaUtilizada flota) {
        for (int i=0; i<vecFlotas.size(); i++ ){
            FlotaUtilizada fl = (FlotaUtilizada)vecFlotas.elementAt(i);
            if (fl.getPlaca().equals(flota.getPlaca()) &&
            fl.getPlanilla_Salida().equals(flota.getPlanilla_Salida()))  {
                return true;
            }
        }
        return false;
    }
    /**
     * Metodo getResumenFlota, Recorre el vector de flotas y saca la informacion para
     * el resumen de Vehiculos utilizados
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @version : 1.0
     */
    public  Vector getResumenFlota() throws SQLException {
        Vector resumen = buscarDatosResumen();
        String agencia = "";
        Vector vecDetalle = new Vector();
        //procedimientos para agregar las agencias a un vector de agencias
        for (int i=0; i<resumen.size(); i++) {
            agencia = ((ResumenFlota) resumen.elementAt(i)).getAgencia();
            Vector vecAgencias = new Vector();
            for (int j=0; j<vecFlotas.size(); j++ ){
                FlotaUtilizada fl = (FlotaUtilizada) vecFlotas.elementAt(j);
                if(agencia.equals(fl.getOri_dest())) {
                    vecAgencias.addElement(fl);
                }
            }
            vecDetalle.addElement(vecAgencias);
        }
        //Creamos un objeto Resumen
        Vector ag = new Vector();
        int disp = 0;    int   remeDesp = 0;
        int util = 0;    double perUtil = 0;
        double ret= 0;
        String nomAG = "";
        Vector resumenAgencias = new Vector();
        for (int i=0; i<vecDetalle.size(); i++) {
            ag = (Vector) vecDetalle.elementAt(i);
            for(int j=0; j<ag.size(); j++){
                FlotaUtilizada fl = (FlotaUtilizada) ag.elementAt(j);
                nomAG = fl.getOri_dest();
                if(!fl.getNumpla().equals("")) {
                    disp++;
                }
                if(!fl.getNumpla().equals("") && !fl.getPlanilla_Salida().equals("")) {
                    util++;
                }
                if(!fl.getPlanilla_Salida().equals("")) {
                    remeDesp++;
                }
            }
            ResumenFlota resFlota = new ResumenFlota();
            resFlota.setAgencia(nomAG);
            resFlota.setVehDisponibles(disp+"");
            resFlota.setVehUtilizados(util+"");
            resFlota.setTotal(remeDesp);
            perUtil = (util/(double)remeDesp)*100;
            ret     = (util/(double)disp)*100;
            resFlota.setPerRetorno(Util.customFormat(ret,2)+"");
            resFlota.setPerUtilidad(Util.customFormat(perUtil,2)+"");
            ////System.out.println(util+" "+disp+" "+remeDesp);
            resumenAgencias.addElement(resFlota);
            disp = 0;
            util = 0;
            remeDesp = 0;
        }
        return resumenAgencias;
    }
    
}
