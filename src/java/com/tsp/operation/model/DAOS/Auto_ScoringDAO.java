/*opav/model/daos
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.CmbGeneralScBeans;
import com.tsp.operation.model.beans.Unidad_Negocio;
import com.tsp.operation.model.beans.UnidadesNegocio;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.Proveedor;
import com.tsp.operation.model.beans.variable_mercado;
import com.tsp.operation.model.beans.ValorPredeterminadoUn;
import com.tsp.opav.model.beans.UnidadMedida;
import java.util.ArrayList;

/**
 *
 * @author fnunez
 */
public interface Auto_ScoringDAO {

    /**
     *
     * @param insumo
     * @param empresa
     * @param statusoperation.
     * @return
     */
    
    public abstract  ArrayList<String> cargarNom_subCat(String insumo );
    
    public abstract JsonObject modificar_esp(JsonObject info);
    
    public abstract JsonObject modificar_espVal(JsonObject info);
    
    public abstract JsonObject modificar_espVal_edit(JsonObject info);
    
    public abstract JsonObject modificar_Val(JsonObject info);
    
    public ArrayList<UnidadMedida> cargarunidadmedida(String empresa,String status);
    
    public ArrayList<ValorPredeterminadoUn> listar_valorespredeterminados(String empresa,String status);
    
    public ArrayList<ValorPredeterminadoUn> listar_valorespredeterminados2(String empresa,String status);
    
    public ArrayList<ValorPredeterminadoUn> listar_valorespredeterminados_filtro(String empresa,String status, String id, String ids);
    
    public ArrayList<ValorPredeterminadoUn> listar_valorespredeterminados_filtro_edit(String empresa,String status, String id);
    
    public ArrayList<ValorPredeterminadoUn> listar_valorespredeterminados_edit(String empresa,String status, String id);
    
    public ArrayList<Proveedor> cargarproveedores(String empresa,String status);
    
    public ArrayList<Unidad_Negocio> cargarProcesosMeta(String empresa, String status,String insumo);
    
    public ArrayList<CmbGeneralScBeans> cargarComboEmpresa();

    public boolean existeMetaProceso(String empresa, String descripcion);

    public boolean existeEspecificacion(String empresa, String descripcion);

    public String guardarMetaProceso(String insumo,String empresa, String nombre, String descripcion, String usuario);
    
    public String guardarValorPredeterminado(String empresa, String nombre, String usuario);

    public String guardarEspecificacion(String empresa, String nombre, String descripcion, String usuario,String idtipo);

    public String actualizarMetaProceso(String empresa, String nombre, String descripcion, String idProceso, String usuario);
    
    public String obtenerEspecificacionesColumnas(String subcategoria);
    
    public String cargarComboProcesoMeta(String empresa);
    
    public String cargarComboInsumos(String empresa);

    public ArrayList<Unidad_Negocio> cargarProcesoInterno();

    public boolean existeProcesoInterno(String procesoMeta, String descripcion);
    
    public boolean existeSubcategoria(String descripcion, String dstrct);
    
    public int guardarSubcategoria(String nombre, String usuario, String dstrct);
    
    public boolean existeRelCatSubcategoria(int idsubcategoria, String idcategoria);
    
    public int guardarRelCatSubcategoria(String idsubcategoria, int idcategoria, String usuario, String dstrct);
    
    public int actualizarRelCatSubcategoria(String idsubcategoria, int idcategoria, String dstrct);
    
    public int obtenerIdSubcategoria(String nombre, String dstrct);
    
    public String guardarProcesoInterno(String procesoMeta, String nombre, String descripcion, String usuario, String empresa);

    public int obtenerIdProcesoInterno(String nomProceso, String idProMeta);

    public String insertarRelUnidadProInterno(int idProInterno, String string, String string2, String empresa, int idProMetaEdit);
    
    public String insertarRelValPred(int idProInterno, String string, String empresa, String usuario);

    public ArrayList<variable_mercado> cargarUndNegocioProinterno(String idProinterno, String idProMetaEdit);
    
    public String cargarCategorias( );

    public String actualizarProcesoInterno(int idProinterno, String nombre, int idProMeta, String usuario, String empresa, int idSubcategoria);

    public String eliminarUndProinterno(String idProinterno, String idUnidad);

    public ArrayList<Usuario> listarUsuario();

    public ArrayList<Unidad_Negocio> listarProinternoUsuario(int parseInt);

    public ArrayList<Unidad_Negocio> listarProinterno();

    public String insertarRelProInternoUser(String idProInt, int codUsuario, String login, String empresa);
    
    public boolean existeMetaProceso(String empresa, String descripcion, int idProceso);
    
    public boolean existeProcesoInterno(String procesoMeta, String descripcion, int idProceso);
    
    public ArrayList<Unidad_Negocio> cargarProcesoInterno(int idMetaProceso, String status);
    
    public ArrayList<variable_mercado> cargarUnidadesNegocio(int parseInt, int idProMetaEdit);
       
    public String anularMetaProceso(int idProceso,String login);
     
    public String anularProcesoInterno(int idProinterno,int idProcesoMeta,String login);
    
    public ArrayList<Usuario> listarUsuariosProinterno(int parseInt);
    
    public ArrayList<Usuario> listarUsuariosRelProInterno(int idProceso);
    
    public String eliminarUsuarioProinterno(int idProinterno, String idUsuario);
    
    public boolean existenUsuariosRelProceso(int idProceso, String tipo);
    
    public String activarMetaProceso(int idProceso);
    
    public String activarProcesoInterno(int idProceso);
    
    public String actualizarModerador(int idProceso, int idusuario, String moderador);
    
    public ArrayList<Usuario> listarUsuariosRelProInterno();
    
    public ArrayList<Unidad_Negocio> cargarProcesosRelUsuario(String usuario);
    
    public ArrayList<Unidad_Negocio> cargarTipoRequisicionRelUsuario(String usuario);
    
    public String eliminarRelProcesoReqUser(int idUsuario);
    
    public String insertarRelProcesoReqUser(String idProceso, int codUsuario, String login, Usuario usuario);
    
    public String eliminarRelTipoReqUser(int idUsuario);
    
    public String insertarRelTipoReqUser(String idTipoReq, int codUsuario, String login, Usuario usuario);

    public JsonObject cargar_combo(JsonObject info);
    
    public JsonObject consultar(String idsubcategoria);
    
    public boolean verificaExisteSub(int categoria);

    public ArrayList<String> cargarNom_Especificacion(String idcategoria);
    
    public boolean anularInsumo(int id);
    
    public String cargarInfoScoring(String id_und_negocio);
    
    public String actualizarScoreUndNegocio(int id_uidad_negocio,String puntaje_maximo,String fieldUpdate, String valor);
    
    public String actualizarScoring(int id_conf_pred,String puntaje, String status, String fieldUpdate);
    
    public String cargarGridSimuladorScoring(String id_und_negocio);
    
    public String cargarValoresPredeterminados(String id_und_negocio);
    
     public void guardarValoresVM(String id_unidad_negocio, String fieldNames, String values, String usuario);
     
     public String get_Next_Consecutivo_Simulador();
     
     public void actualizaConsecutivoSimulador();
     
     public JsonObject cargarInfoSimulacion(String id_und_negocio, String num_solicitud);
     
     public String simularScoring(String num_solicitud, String id_und_negocio,int tipo);
     
     public void simulaScoring(String num_solicitud, String id_und_negocio,int tipo);
     
     public ArrayList<ValorPredeterminadoUn> listar_valoresVariableMercado(String id_variable_mercado);
     
     public ArrayList<ValorPredeterminadoUn> listar_valoresVariableMercado2(String id_variable_mercado);
     
    public JsonObject eliminar_Val_Pred(String id_variable_mercado, String idVP);
    
    public String eliminarRelValPred(String id_variable_mercado, String idVP);
    
}


