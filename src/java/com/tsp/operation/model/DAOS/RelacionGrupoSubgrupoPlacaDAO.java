/********************************************************************
 *  Nombre Clase.................   RelacionGrupoSubgrupoPlacaDAO.java
 *  Descripci�n..................   DAO de la tabla group_subgroup_placa
 *  Autor........................   Ing. Leonardo Parodi
 *  Fecha........................   16.01.2006
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import javax.swing.*;
import java.text.*;
/**
 *
 * @author  EQUIPO12
 */
public class RelacionGrupoSubgrupoPlacaDAO {
        private RelacionGrupoSubgrupoPlaca relacionGrupoSubgrupoPlaca;
        
        /** Creates a new instance of RelacionGrupoSubgrupoPlacaDAO */
        public RelacionGrupoSubgrupoPlacaDAO() {
        }
        
        private final String SQL_INSERT = " INSERT INTO group_subgroup_placa (wgroup, subgroup, placa, "+
        "last_update , user_update, creation_date, creation_user, reg_status, dstrct, base ) "+
        "VALUES (?,?,?,now(),'',now(),?,'',?,?)";
        
        private final String SQL_UPDATE = "UPDATE group_subgroup_placa SET subgroup = ?, wgroup = ?, reg_status = '', user_update = ?, last_update = now() WHERE placa = ? AND wgroup = ? AND subgroup = ?";
        
        private final String SQL_CONSULT = "SELECT placa, wgroup, subgroup, reg_status FROM group_subgroup_placa WHERE placa = ? AND wgroup = ? AND subgroup = ?";
        
        private final String SQL_ANULAR = "UPDATE group_subgroup_placa SET reg_status = 'A' WHERE wgroup=? AND subgroup = ? AND placa = ?";
       
        private final String SQL_ELIMINAR = "DELETE FROM group_subgroup_placa WHERE wgroup=? AND subgroup = ? AND placa = ?";
       
        private final String SQL_CONSULTINSERT =
                                   "SELECT                                 "+ 
                                   "     a.*, n.nombre AS propietario      "+
                                   "FROM                                   "+      
                                   "     (SELECT                           "+       
                                   "             g.*,                      "+           
                                   "             p.propietario AS cedula   "+     
                                   "      FROM                             "+
                                   "         ( SELECT                      " +
                                   "              e.*, tg.descripcion AS descripcionsg" +
                                   "           FROM                           "+
                                   "            (SELECT                       "+ 
                                   "                  gr.*, t.descripcion AS descripcionwg "+     
                                   "             FROM                         "+     
                                   "                 group_subgroup_placa gr, tablagen t  "+
                                   "             WHERE  gr.wgroup = t.table_code)e, tablagen tg" +
                                   "            WHERE                      " +
                                   "                  e.subgroup = tg.table_code AND placa = ? AND wgroup = ? AND subgroup = ?  "+
		                   "                            ) g        "+     
                                   "      LEFT JOIN                        "+                               
                                   "           placa p                     "+        
                                   "      ON                               "+                                       
                                   "           g.placa = p.placa) a        "+  
                                   "LEFT JOIN                              "+                              
                                   "      nit n                            "+                              
                                   "ON                                     "+                                             
                                   "      a.cedula = n.cedula              ";
        /**
         * Metodo InsertarRelacion , Metodo que Ingresa una Relacion de Grupo, subgrupo y placa.
         * @autor : Ing. Leonardo Parody
         * @throws : SQLException
         * @version : 1.0
         */
        public void InsertRelacion() throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;
                PoolManager poolManager = null;
                boolean sw = false;
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection("fintra");
                        if (con != null){
                                //////System.out.println("VOY A EJECUTAR EL QUERY");
                                st = con.prepareStatement(SQL_INSERT);
                                st.setString(1, relacionGrupoSubgrupoPlaca.getGroup());
                                st.setString(2, relacionGrupoSubgrupoPlaca.getSubgroup());
                                st.setString(3, relacionGrupoSubgrupoPlaca.getPlaca());
                                st.setString(4, relacionGrupoSubgrupoPlaca.getCreation_user());
                                st.setString(5, relacionGrupoSubgrupoPlaca.getDstrct());
                                st.setString(6, relacionGrupoSubgrupoPlaca.getBase());
                                //////System.out.println("Query  "+st);
                                st.executeUpdate();
                                //////System.out.println("YA EJECUTE EL QUERY");
                        }
                        
                }catch(SQLException e){
                        throw new SQLException("ERROR AL REALIZAR EL INSERT" + e.getMessage()+"" + e.getErrorCode());
                }finally{
                        if(st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                                }
                        }
                        if ( con != null){
                                poolManager.freeConnection("fintra", con );
                        }
                }
        }
        /**
         * Metodo updateRelacion , Metodo que Modifica el subgrupo asignado a una placa.
         * @autor : Ing. Leonardo Parody
         * @param : String placa
         * @param : String subgroup
         * @throws : SQLException
         * @version : 1.0
         */
        public void UpdateRelacion(String placa, String subgroup, String grupo) throws SQLException{
                Connection con = null;
                PreparedStatement st = null;
                ResultSet rs = null;
                PoolManager poolManager = null;
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection("fintra");
                        if (con != null){
                                st = con.prepareStatement(this.SQL_UPDATE);
                                st.setString(1, this.relacionGrupoSubgrupoPlaca.getSubgroup());
                                st.setString(2, this.relacionGrupoSubgrupoPlaca.getGroup());
                                st.setString(3,  this.relacionGrupoSubgrupoPlaca.getUser_update());
                                st.setString(4, placa);
                                st.setString(5, grupo);
                                st.setString(6, subgroup);
                                //////System.out.println("Update = "+st);
                                st.executeUpdate();
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR DURANTE LA MODIFICACI�N DE LAS RELACIONES" + e.getMessage() + " " + e.getErrorCode());
                }
                finally{
                        if (st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                                }
                        }
                        if (con != null){
                                poolManager.freeConnection("fintra", con);
                        }
                }
        }
        
        /**
         * Metodo AnularRelacion , Metodo que Anula el subgrupo asignado a una placa.
         * @autor : Ing. Leonardo Parody
         * @param : String placa
         * @param : String subgroup
         * @throws : SQLException
         * @version : 1.0
         */
        public void AnularRelacion(String placa, String subgroup, String grupo) throws SQLException{
                Connection con = null;
                PreparedStatement st = null;
                ResultSet rs = null;
                PoolManager poolManager = null;
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection("fintra");
                        if (con != null){
                                st = con.prepareStatement(this.SQL_ANULAR);
                                st.setString(1, grupo);
                                st.setString(2, subgroup);
                                st.setString(3, placa);
                                //////System.out.println("Update Anular = "+st);
                                st.executeUpdate();
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR DURANTE LA MODIFICACI�N DE LAS RELACIONES" + e.getMessage() + " " + e.getErrorCode());
                }
                finally{
                        if (st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                                }
                        }
                        if (con != null){
                                poolManager.freeConnection("fintra", con);
                        }
                }
        }
        
        /**
         * Metodo EliminarRelacion , Metodo que Anula el subgrupo asignado a una placa.
         * @autor : Ing. Leonardo Parody
         * @param : String placa
         * @param : String subgroup
         * @throws : SQLException
         * @version : 1.0
         */
        public void EliminarRelacion() throws SQLException{
                Connection con = null;
                PreparedStatement st = null;
                ResultSet rs = null;
                PoolManager poolManager = null;
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection("fintra");
                        if (con != null){
                                st = con.prepareStatement(this.SQL_ELIMINAR);
                                st.setString(1, relacionGrupoSubgrupoPlaca.getGroup());
                                st.setString(2, relacionGrupoSubgrupoPlaca.getSubgroup());
                                st.setString(3, relacionGrupoSubgrupoPlaca.getPlaca());
                                ////System.out.println("Update Eliminar = "+st);
                                st.executeUpdate();
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR DURANTE LA ELIMINACION DE LAS RELACIONES" + e.getMessage() + " " + e.getErrorCode());
                }
                finally{
                        if (st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                                }
                        }
                        if (con != null){
                                poolManager.freeConnection("fintra", con);
                        }
                }
        }

        /**
         * Metodo ExisteRelacion , Metodo que verifica si existe la relacion grupo, subgrupo, placa.
         * @autor : Ing. Leonardo Parody
         * @param : String placa
         * @param : String subgroup
         * @param : String grupo
         * @throws : SQLException
         * @version : 1.0
         */
        public boolean ExisteRelacion(String placa, String subgroup, String group) throws SQLException{
                boolean existe=false;
                Connection con = null;
                PreparedStatement st = null;
                ResultSet rs = null;
                PoolManager poolManager = null;
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection("fintra");
                        if (con != null){
                                st = con.prepareStatement(this.SQL_CONSULT);
                                //////System.out.println("group = "+group+"  subgroup = "+subgroup+" placa = "+placa);
                                st.setString(1, placa);
                                st.setString(2, group);
                                st.setString(3, subgroup);
                                rs = st.executeQuery();
                                if (rs.next()) {
                                        existe = true;
                                }
                        }
                        return existe;
                }catch(SQLException e){
                        throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS RELACIONES" + e.getMessage() + " " + e.getErrorCode());
                }
                finally{
                        if (st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                                }
                        }
                        if (con != null){
                                poolManager.freeConnection("fintra", con);
                        }
                }
        }
        
        /**
         * Metodo ConsultaRelacion , Metodo que verifica si existe la relacion grupo, subgrupo, placa.
         * @autor : Ing. Leonardo Parody
         * @param : String placa
         * @param : String subgroup
         * @param : String grupo
         * @throws : SQLException
         * @version : 1.0
         */
        public List ConsultaRelacion(String placa, String subgroup, String group) throws SQLException{
                LinkedList relaciones = new LinkedList();
                String consulta;
                RelacionGrupoSubgrupoPlaca relacion = new RelacionGrupoSubgrupoPlaca();
                boolean existe=false;
                Connection con = null;
                PreparedStatement st = null;
                ResultSet rs = null;
                PoolManager poolManager = null;
                consulta = this.Consulta(group, subgroup, placa);
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection("fintra");
                        if (con != null){
                                st = con.prepareStatement(consulta);
                                //////System.out.println("group = "+group+"  subgroup = "+subgroup+" placa = "+placa);
                                rs = st.executeQuery();
                                while (rs.next()) {
                                        relacion = relacion.Load(rs);
                                        relaciones.add(relacion);
                                }
                        }
                        return relaciones;
                }catch(SQLException e){
                        throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS RELACIONES" + e.getMessage() + " " + e.getErrorCode());
                }
                finally{
                        if (st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                                }
                        }
                        if (con != null){
                                poolManager.freeConnection("fintra", con);
                        }
                }
        }
        /**
         * Metodo ConsultaInsertRelacion , Metodo que verifica si existe la relacion grupo, subgrupo, placa.
         * @autor : Ing. Leonardo Parody
         * @param : String placa
         * @param : String subgroup
         * @param : String grupo
         * @throws : SQLException
         * @version : 1.0
         */
        public List ConsultaInsertRelacion(String placa, String subgroup, String group) throws SQLException{
                LinkedList relaciones = new LinkedList();
                String consulta;
                RelacionGrupoSubgrupoPlaca relacion = new RelacionGrupoSubgrupoPlaca();
                boolean existe=false;
                Connection con = null;
                PreparedStatement st = null;
                ResultSet rs = null;
                PoolManager poolManager = null;
                
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection("fintra");
                        if (con != null){
                                st = con.prepareStatement(SQL_CONSULTINSERT);
                                //////System.out.println("group = "+group+"  subgroup = "+subgroup+" placa = "+placa);
                                st.setString(1, placa);
                                st.setString(2, group);
                                st.setString(3, subgroup);
                                rs = st.executeQuery();
                                while (rs.next()) {
                                        relacion = relacion.Load(rs);
                                        relaciones.add(relacion);
                                }
                        }
                        return relaciones;
                }catch(SQLException e){
                        throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS RELACIONES PARA EL INSERT" + e.getMessage() + " " + e.getErrorCode());
                }
                finally{
                        if (st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                                }
                        }
                        if (con != null){
                                poolManager.freeConnection("fintra", con);
                        }
                }
        }
        /**
         * Metodo Consulta , Metodo que arma una consulta segun el criterio de busqueda.
         * @autor : Ing. Leonardo Parody
         * @param : String placa
         * @param : String subgroup
         * @param : String grupo
         * @throws : SQLException
         * @return : String consulta
         * @version : 1.0
         */
        private String Consulta(String grupo, String subgrupo, String placa){
                String consulta = "";
                //////System.out.println("grupo = "+grupo+" subgrupo = "+subgrupo+" placa = "+placa);
                String consulta1 = "SELECT                                 "+ 
                                   "     a.*, n.nombre AS propietario      "+
                                   "FROM                                   "+      
                                   "     (SELECT                           "+       
                                   "             g.*,                      "+           
                                   "             p.propietario AS cedula   "+     
                                   "      FROM                             "+
                                   "         ( SELECT                      " +
                                   "              e.*, tg.descripcion AS descripcionsg" +
                                   "           FROM                           "+
                                   "            (SELECT                       "+ 
                                   "                  gr.*, t.descripcion AS descripcionwg "+     
                                   "             FROM                         "+     
                                   "                 group_subgroup_placa gr, tablagen t  "+
                                   "             WHERE  gr.wgroup = t.table_code)e, tablagen tg" +
                                   "            WHERE                      " +
                                   "                  e.subgroup = tg.table_code AND e.reg_status <> 'A' ";
		                                            
                String consulta2 = "                            ) g        "+     
                                   "      LEFT JOIN                        "+                               
                                   "           placa p                     "+        
                                   "      ON                               "+                                       
                                   "           g.placa = p.placa) a        "+  
                                   "LEFT JOIN                              "+                              
                                   "      nit n                            "+                              
                                   "ON                                     "+                                             
                                   "      a.cedula = n.cedula              ";
               if (placa!=null) {
                       consulta = consulta1 + " AND  e.placa = '"+placa+"' "+consulta2;
               }else if (grupo!=null){
                       consulta = consulta1 + " AND  e.wgroup = '"+grupo+"' "+consulta2;
               }else if (subgrupo!=null){
                       consulta = consulta1 + " AND  e.subgroup = '"+subgrupo+"' "+consulta2;
               }
                //////System.out.println("consulta = " + consulta);
                return consulta;
        }
        
       
       
              
        /**
         * Setter for Object precinto.
         * @param precinto New value of Object precinto.
         */
        public void setRelacionGrupoSubgrupoPlaca(RelacionGrupoSubgrupoPlaca Relacion) {
                this.relacionGrupoSubgrupoPlaca = Relacion;
        }
        
}
