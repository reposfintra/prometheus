/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.Usuario;

/**
 *
 * @author dvalencia
 */
public interface AdministracionFintraDAO {

    public String cargarNegocios(String cliente, String negocio);

    public String generarPdf(String usuario, String negocio);

    public String cargaPresolicitudesMicrocredito(String fechaInicio, String fechaFin, String identificacion, String numero_solicitud, String estado_solicitud, String etapa, String r_operaciones);

    public String rechazarnegociomicrocredito(Usuario usuario, String numero_solicitud);

    public String standbytrazabilidadpresolicitudes(Usuario usuario, String numero_solicitud, String causal, String estado);

    public String generarPdfConsolidado(String usuario, String negocio);

    public String cargarPagadurias();

    public String cargarNegocioLibranza(String negocio);

    public String actualizarPagaduriaNegocio(String negocio, String pagaduria, String user);

    public String cargarTrazabilidadPagaduria(String negocio);

    public String buscarScoreSolicitudesCliente(int idCliente, String unidadNegocio);

    public String buscarScoreSolicitud(int numeroSolicitud, String unidadNegocio);
    
    public String FiltrosDuros(String opcionBuscar, int numeroSolicitud);
}
