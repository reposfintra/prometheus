/************************************************************************************
 * Nombre clase :                 Wgroup_PlacaDAO.java                              *
 * Descripcion :                  Clase que maneja los DAO ( Data Access Object )   *
 *                                los cuales contienen los metodos que interactuan  *
 *                                con la BD para manipular las diferentes funciones *
 *                                de mantenimiento de la tabla work_group - placa   *
 * Autor :                        Ing. Juan Manuel Escandon Perez                   *
 * Fecha :                        13 de enero de 2006, 02:35 PM                     *
 * Version :                      1.0                                               *
 * Copyright :                     Fintravalores S.A.                          *
 ************************************************************************************/
package com.tsp.operation.model.DAOS;
import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.*;

public class Wgroup_PlacaDAO extends MainDAO{
    /*****************************************CONSULTA********************************/
    
    private String Insert   =   "   INSERT INTO wgroup_placa ( work_group, placa, creation_user, dstrct )   "+
    "   VALUES ( ?, ?, ?, ? )                                                   ";
    
    private String Update   =   "   UPDATE  wgroup_placa SET work_group = ?, placa = ?                      "+
    "   WHERE work_group = ? AND placa = ?                                      ";
    
    private String Delete   =   "   DELETE FROM wgroup_placa WHERE work_group = ?                           "+
    "   AND placa = ?                                                           ";
    
    private String List     =   "   SELECT work_group, placa, reg_status FROM wgroup_placa                              ";
    
    private String Status   =   "   UPDATE  wgroup_placa SET reg_status = ? WHERE work_group = ?            "+
    "   AND placa = ?                                                           ";
    
    private String Search   =   "   SELECT work_group, placa, reg_status FROM wgroup_placa WHERE work_group = ?         "+
    "   AND placa = ?                                                           ";
    
    /*****************************************FIN CONSULTA********************************/
    
    /** Creates a new instance of Wgroup_PlacaDAO */
    public Wgroup_PlacaDAO(){
        super("Wgroup_PlacaDAO.xml");
    }
    
    /**
     * Metodo Insert, permite insertar un registro nuevo a la tabla wgroup_placa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String work_group , String placa, String usuario , String dstrct
     * @version : 1.0
     */
    public void Insert(String wgroup , String placa, String usuario, String dstrct) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.Insert);
            st.setString(1, wgroup);
            st.setString(2, placa);
            st.setString(3, usuario);
            st.setString(4, dstrct);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina Insert [Wgroup_PlacaDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    
    /**
     * Metodo Update, permite modificar un registro en la tabla wgroup_placa dada
     * un work_group y una placa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String nwgroup , String nplaca, String wgroup , String placa
     * @version : 1.0
     */
    public void Update(String nwgroup , String nplaca, String wgroup, String placa) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.Update);
            st.setString(1, nwgroup);
            st.setString(2, nplaca);
            st.setString(3, wgroup);
            st.setString(4, placa);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina Update [Wgroup_PlacaDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    
    /**
     * Metodo Search, devuelve un objeto de tipo  Wgroup_Placa, dado
     * un work_group y una placa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String wgroup , String placa
     * @version : 1.0
     */
    public Wgroup_Placa Search(String wgroup, String placa ) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        Wgroup_Placa datos   = null;
        
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.Search);
            st.setString(1, wgroup);
            st.setString(2, placa);
            rs = st.executeQuery();
            while(rs.next()){
                datos = Wgroup_Placa.load(rs);
                break;
            }
        }
        catch(Exception e) {
            throw new SQLException("Error en rutina Search [Wgroup_PlacaDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return datos;
    }
    
    /**
     * Metodo Existe, Busca si existe un registro en la tabla wgroup - placa, dado
     * un work_group y una placa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String wgroup , String placa
     * @version : 1.0
     */
    public boolean Existe(String wgroup, String placa ) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        boolean flag = false;
        
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.Search);
            st.setString(1, wgroup);
            st.setString(2, placa);
            rs = st.executeQuery();
            while(rs.next()){
                flag = true;
                break;
            }
        }
        catch(Exception e) {
            throw new SQLException("Error en rutina Existe [Wgroup_PlacaDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return flag;
    }
    
    /**
     * Metodo Delete, Elimina un registro en la tabla wgroup - placa, dado
     * un work_group y una placa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String wgroup , String placa
     * @version : 1.0
     */
    public void Delete(String wgroup, String placa ) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.Delete);
            st.setString(1,  wgroup);
            st.setString(2, placa);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina Delete [Wgroup_PlacaDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    /**
     * Metodo Status, Anula o activa un registro de la tabla wgroup - placa, dado
     * un work_group y una placa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String wgroup , String placa
     * @version : 1.0
     */
    public void Status(String reg_status, String wgroup, String placa ) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.Status);
            st.setString(1, reg_status);
            st.setString(2, wgroup);
            st.setString(3, placa);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina Status [Wgroup_PlacaDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    /**
     * Metodo List, Lista todos los registros de la tabla wgroup - placa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public List List() throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        try{
            st = con.prepareStatement(this.List);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(Wgroup_Placa.load(rs));
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina List [Wgroup_PlacaDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
        return lista;
    }
    
    /* Metodo wGroupXPlaca, obtiene los work_group a los que pertenece una placa
     * @author : Osvaldo P�rez Ferrer
     * @param : placa, placa a la que se le buscar� sus workgroup
     * @throws : en caso de que un error en la base de datos ocurra
     */
    public List wGroupXPlaca(String placa) throws Exception{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        List w = new LinkedList();
        try{
            st = this.crearPreparedStatement("SQL_WORKGROUP_X_PLACA");
            st.setString( 1, placa );
            
            rs = st.executeQuery();
            
            while ( rs.next() ){
                w.add(Wgroup_Placa.load(rs));
            }
            
        }catch(Exception e){
            throw new Exception("ERROR en wGroupXPlaca " + e.getMessage() );
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            this.desconectar("SQL_WORKGROUP_X_PLACA");
        }
        return w;        
    }
    
    
}
