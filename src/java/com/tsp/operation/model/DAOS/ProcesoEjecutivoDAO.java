/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.DAOS;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.UnidadesNegocio;
import com.tsp.operation.model.beans.Usuario;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author mcastillo
 */
public interface ProcesoEjecutivoDAO {
    /***
     * @param ref
     * @return 
     * @throws java.sql.SQLException 
     */
    public ArrayList<UnidadesNegocio> cargarUndNegocios(String ref) throws SQLException;
    
    public String asignarDesasignarUndProceso(int idUndNegocio, String ref) throws SQLException;
    
    public ArrayList<BeanGeneral> cargarEstadosCarteraxUnd(int id_unidad_negocio) throws SQLException;
    
    public JsonObject listarEstadosCartera() throws SQLException;
    
    public String cargarEstadosCartera();    
   
    public String guardarEstadoCartera(String nombre, String descripcion, String usuario, String dstrct);

    public String actualizarEstadoCartera(String nombre, String descripcion, String idEstado, String usuario);

    public String activaInactivaEstadoCartera(String id, String estado, String usuario);
   
    public boolean existeEstadoCartera(String nombre);
    
    public boolean existeEstadoCartera(String nombre, int idEstado);
    
    public String actualizarConfigEstadosCartera(int idUndNegocio, int idIntervaloMora, int idEstadoCartera, String usuario) throws SQLException;
     
    public String insertarConfigIniEstadosCartera(int idUndNegocio, int idEstadoCartera, String usuario) throws SQLException;
    
    public String eliminarRelUndEstadoCartera(int idUndNegocio) throws SQLException;
    
    public boolean existenDemandasRelUndNegocio(int idUndNegocio);
     
    public String cargarEtapas(boolean showAll);    
   
    public String guardarEtapa(String nombre, String descripcion, int estimadoDias, String usuario, String dstrct);

    public String actualizarEtapa(String nombre, String descripcion, int estimadoDias, String idEtapa, String usuario);

    public String activaInactivaEtapa(String id, String estado, String usuario);
       
    public boolean existeEtapa(String nombre, int idEtapa);
    
    public String cargarRespuestasEtapa(int idEtapa);    
   
    public String guardarRespuestaEtapa(int idEtapa, String nombre, String descripcion, int estimadoDias, int secuencia, String finaliza_proceso, String usuario, String dstrct);

    public String actualizarRespuestaEtapa(int id, int idEtapa, String nombre, String descripcion, int estimadoDias, int secuencia, String finaliza_proceso, String usuario);

    public String activaInactivaRespuestaEtapa(String id, String estado, String usuario);
    
    public String cargarCostosEtapa(int idEtapa);    
   
    public String guardarCostoEtapa(int idEtapa, String concepto, String tipo, double valor, String solo_automotor, String usuario, String dstrct);

    public String actualizarCostoEtapa(int id, int idEtapa, String concepto, String tipo, double valor, String solo_automotor, String usuario);

    public String activaInactivaCostoEtapa(String id, String estado, String usuario);
    
    public String cargarReporteJuridica(String etapa,int undNegocio, String negocio, String idCliente); 
    
    public String actualizarEtapaNegocio(String negocio, String idEtapa, String actualiza_fecha, String usuario);
    
    public String actualizarEtapaDemanda(String idDemanda, String idEtapa, String usuario);
    
    public String cargarCostosNegocioXEtapa(String etapa,String negocio);     
    
    public String cargarCostosXEtapa(String etapa,String IsAutomotor);     
    
    public boolean existeRelCostoNegEtapa(int idEtapa, String negocio, int idCosto);
    
    public String insertarRelCostoNegXEtapa(int idEtapa, String negocio, double valor_saldo, int idCosto, String tipo, double valor, String usuario, String dstrct);

    public String actualizarRelCostoNegXEtapa(int id,  double valor, String usuario);

    public String eliminarRelCostoNegXEtapa(int id);
    
    public String cargarCboRespuestasEtapa (int idEtapa);
     
    public String insertarRespuestaProcTraz(int idEtapa, String negocio, int idRespuesta, String respuesta, String comentarios, String usuario, String dstrct);
    
    public String cargarConfigDocs();     
    
    public String guardarConfigDocs(int tipo_doc,String header_info, String initial_info,String footer_info, String signing_info, String footer_page,String aux_1,String aux_2,String aux_3,String aux_4,String aux_5, String usuario, String dstrct);

    public String guardarConfigDocsDet(int tipo_doc, String tipo, String titulo, String nombre, String descripcion, String usuario, String dstrct);
    
    public String eliminarConfigDocsDet(int tipo_doc);
    
    public boolean existeConfigDocs(int tipo_doc);
    
    public String cargarTipoActores();
    
    public String guardarTipoActor(String descripcion, String usuario, String dstrct);
    
   public String actualizarTipoActor(int id, String descripcion, String usuario);
   
    public boolean existeTipoActor(String descripcion);

    public boolean existeTipoActor(String descripcion, int idTipo);

    public String activaInactivaTipoActor(String id, String estado, String usuario);

    public String cargarActores();

    public String cargarCboTipoActores();
      
    public String cargarTiposIdentificacion();

    public String guardarActor(int tipoActor, String tipoDoc, String documento, String nombre, String ciudad, String dpto, String pais, String direccion,  String telefono, String extension, String celular, String email, String tarjeta_prof, String lugarExpCed, String reg_status, String usuario, String dstrct);

    public String actualizarActor(int id, int tipoActor, String tipoDoc, String documento, String nombre, String ciudad, String dpto, String pais, String direccion, String telefono, String extension, String celular,  String email,  String tarjeta_prof, String lugarExpCed, String reg_status, String usuario);

    public String activaInactivaActor(String id, String estado, String usuario);

    public String cargarEquivalencias();
    
    public boolean existeDemandaDocs(String negocio, boolean genero_PDF);
    
    /**
     * Metodo para la creacion de la demanda. 
     * @param jsonArrNeg
     * @param usuario
     * @return 
     * 
     */
    public String crearDemanda(JsonArray jsonArrNeg, Usuario usuario);

    public String cargarDemandaDocs(String negocio);

    public String guardarDemandaDocs(int id_doc, int id_demanda, int tipo_doc, String header_info, String initial_info, String footer_info, String signing_info, String footer_page, String aux_1, String aux_2, String aux_3, String aux_4, String aux_5, String usuario, String dstrct);

    public String guardarDemandaDocsDet(String id_det_doc, int id_doc, String tipo, String titulo, String descripcion, String usuario, String dstrct);
    
    public boolean generarDemandaDocs(int id_demanda, String ruta, String usuario, String dstrct);

    public String eliminarDemandaDocsDet(int id_doc, String tipo);
    
    public String cargarCostosAutorizacion();
    
    public String cargarCboAutorizacionCostos();
    
    public String actualizaAprobCostoEtapa(int idCosto, String estado, String usuario, String dstrct);
    
    public String insertarRespuestaTrazabilidad(int idEtapa, String negocio, int idRespuesta, String respuesta, String coments, String usuario, String dstrct);
    
    public String actualizaRespuestaDemanda(String idDemanda, int idRespuesta, String usuario);
    
    public String actualizaCampoPdfGenerado(String idDemanda, String pdf_generado, String usuario);
    
    public String listarEtapaConfigTraz(String negocio);
    
    public String listarProcesoTrazabilidad(String negocio);
    
    public boolean copiarArchivo(String negocio, String rutaOrigen, String rutaDestino, String filename);
    
    public String cargarJuzgados();    

    public String guardarJuzgado(String nombre, String usuario, String dstrct);

    public String actualizarJuzgado(String nombre, String idJuzgado, String usuario);

    public String activaInactivaJuzgado(String id, String estado, String usuario);
    
    public boolean existeJuzgado(String nombre);
    
    public boolean existeJuzgado(String nombre, int idJuzgado);
    
    public String cargarCboJuzgados ();
    
    public String actualizaRadicadoJuzgado(String idDemanda, int idJuzgado, String radicado, String usuario);
    
    public String generarActaPagares(JsonArray jsonArrNeg, String ruta, String usuario);
    
    public String listarCondicionesEspeciales(String idItem);
    
    public String guardarCondicionEspecial(String id_condicion, String id_item, String usuario, String dstrct);
   
    public String eliminarCondicionesEspeciales(String id_item);
    
    public String eliminarConfigCondicionEspecial(String id_item);
    
}

