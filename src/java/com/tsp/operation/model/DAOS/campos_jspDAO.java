/********************************************************************
 *      Nombre Clase.................   campos_jspDAO.java
 *      Descripci�n..................   DAO de la tabla campos_jsp
 *      Autor........................   Rodrigo Salazar
 *      Fecha........................   22.06.2005
 *      Versi�n......................   1.5
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.model.DAOS;

import java.io.*;
import javax.swing.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

import org.apache.log4j.*;

public class campos_jspDAO {
    
    Logger logger = Logger.getLogger(this.getClass());
    
    private campos_jsp campos_jsp;
    private Vector campos_jsps;
    private List tcontactos;
    
    /** Creates a new instance of campos_jspDAO */
    public campos_jspDAO() {
    }
    
    /**
     * Obtiene la propiedad campos_jsp
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public campos_jsp getcampos_jsp() {
        return campos_jsp;
    }
    
    /**
     * Establece el valor de la propiedad campos_jsp
     * @param campos Valor de la propiedad campos_jsp
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public void setcampos_jsp(campos_jsp campos) {
        this.campos_jsp = campos;
        
    }
    
    /**
     * Obtiene la propiedad campos_jsps
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public Vector getcampos_jsps() {
        return campos_jsps;
    }
    
    /**
     * Establece el valor de la propiedad campos_jsps
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public void setcampos_jsps(Vector campos_jsps) {
        this.campos_jsps = campos_jsps;
    }
    
    /**
     * Inserta un nuevi registro en la tabla campos_jsp
     * @autor Rodrigo Salazar
     * @throws SQLException
     * @version 1.0
     */
    public void insertcampos_jsp() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("Insert into campos_jsp (pagina,campo,creation_user,creation_date,user_update,last_update,cia,rec_status,tipo_campo) values(?,?,?,'now()',?,'now()',?,' ',?)");
                //////System.out.println("dao"+campos_jsp.getPagina());
                st.setString(1,campos_jsp.getPagina());
                st.setString(2,campos_jsp.getCampo());
                st.setString(3,campos_jsp.getCreation_user());
                st.setString(4,campos_jsp.getUser_update());
                st.setString(5,campos_jsp.getCia());
                st.setString(6,(campos_jsp.getTipo_campo()==null)?"":campos_jsp.getTipo_campo());
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DEL TIPO DE UBICACI�N" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Busca todos los registros en la tabla campos_jsp con el c�digo de
     * la p�gina JSP dada
     * @autor Rodrigo Salazar
     * @param cod C�digo de la p�gina JSP
     * @throws SQLException
     * @version 1.0
     */
    public void searchcampos_jsp(String cod)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from campos_jsp where pagina=? and rec_status != 'A'");
                st.setString(1,cod);
                rs= st.executeQuery();
                campos_jsps = new Vector();
                while(rs.next()){
                    campos_jsp = new campos_jsp();
                    campos_jsp.setPagina(rs.getString("pagina"));
                    campos_jsp.setCampo(rs.getString("campo"));
                    campos_jsp.setTipo_campo(rs.getString("tipo_campo"));
                    //////System.out.println("....................SE ENCONTRO: " + campos_jsp.getCampo());
                    campos_jsps.add(campos_jsp);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DEL TIPO DE UBICACI�N " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Establce si existe un determinado campo en la tabla
     * la p�gina JSP dada
     * @autor Rodrigo Salazar
     * @param jsp C�digo de la p�gina JSP
     * @param campo Nombre del campo
     * @throws SQLException
     * @version 1.0
     */
    public boolean existcampos_jsp(String jsp, String campo) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("select * from campos_jsp where pagina=? and campo=?");
                st.setString(1,jsp);
                st.setString(2,campo);
                rs = st.executeQuery();
                if (rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL CODIGO DEL TIPO DE UBICACI�N " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
    /**
     * Lista todos lo registros de la tabla campos_jsp
     * @autor Rodrigo Salazar
     * @throws SQLException
     * @version 1.0
     */
    public void listcampos_jsp()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from campos_jsp where rec_status != 'A'");
                rs= st.executeQuery();
                campos_jsps = new Vector();
                while(rs.next()){
                    campos_jsp = new campos_jsp();
                    campos_jsp.setPagina(rs.getString("pagina"));
                    campos_jsp.setCampo(rs.getString("campo"));
                    campos_jsps.add(campos_jsp);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA UNIDAD " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Actualiza los registros en la tabla campos_jsp relacionados
     * a una p�gina determinada.
     * @autor Rodrigo Salazar
     * @param cod C�digo de la p�gina JSP
     * @param desc Nombre del campo
     * @param usu Login del usuario
     * @throws SQLException
     * @version 1.0
     */
    public void updatecampos_jsp(String cod, String desc, String usu) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("update campos_jsp set campo=?, last_update='now()', user_update=?, rec_status = '' where pagina = ?");
                st.setString(1,desc);
                st.setString(2,usu);
                st.setString(3,cod);
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACI�N DEL TIPO DE UBICACI�N" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Anula un registro en la tabla campos_jsp
     * @autor Rodrigo Salazar
     * @param cod C�digo de la p�gina JSP
     * @param campo Nombre del campo
     * @throws SQLException
     * @version 1.0
     */
    public void anularcampos_jsp(String cod,String campo) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("delete from campos_jsp where pagina = ? and campo = ?");
                st.setString(1,cod);
                st.setString(2,campo);
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACI�N DEL TIPO DEL UBICI�N" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Anula los registros en la tabla campos_jsp relacionados
     * a una p�gina determinada.
     * @autor Rodrigo Salazar
     * @param cod C�digo de la p�gina JSP
     * @throws SQLException
     * @version 1.0
     */
    public void anulartodoscampos_jsp(String cod) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("delete from campos_jsp where pagina = ?");
                st.setString(1,cod);
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACI�N DEL TIPO DE LOS CAMPOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Obtiene los registros en la tabla campos_jsp relacionados
     * a una p�gina determinada.
     * @autor Rodrigo Salazar
     * @param cod C�digo de la p�gina JSP
     * @throws SQLException
     * @version 1.0
     */
    public Vector searchDetallecampos_jsps(String cod) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        campos_jsps=null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            boolean sw = false;
            if(con!=null){
                
                st = con.prepareStatement("Select * from campos_jsp where pagina = ?  and rec_status != 'A'");
                st.setString(1, cod);
                //////System.out.println(st.toString());
                
                rs = st.executeQuery();
                campos_jsps = new Vector();
                while(rs.next()){
                    campos_jsp = new campos_jsp();
                    campos_jsp.setPagina(rs.getString("pagina"));
                    campos_jsp.setCampo(rs.getString("campo"));
                    campos_jsp.setRec_status(rs.getString("rec_status"));
                    campos_jsp.setUser_update(rs.getString("user_update"));
                    campos_jsp.setCreation_user(rs.getString("creation_user"));
                    campos_jsp.setTipo_campo(rs.getString("tipo_campo"));//tamatu 17.11.2005
                    campos_jsps.add(campos_jsp);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS TIPOS DE CONTACTO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return campos_jsps;
    }
    
    /**
     * Obtiene los registros en la tabla campos_jsp relacionados
     * a una p�gina determinada.
     * @autor Rodrigo Salazar
     * @param pag C�digo de la p�gina JSP
     * @throws SQLException
     * @version 1.0
     */
    public Vector listarCampos_jsp(String pag)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        campos_jsps = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("select coalesce(tipo_campo,'') as tipo_campo,* from campos_jsp where rec_status =' ' and pagina = ? order by campo");
                st.setString(1,pag);
                
                rs = st.executeQuery();
                campos_jsps = new Vector();
                
                while(rs.next()){
                    campos_jsp = new campos_jsp();
                    campos_jsp.setPagina(rs.getString("pagina"));
                    campos_jsp.setCampo(rs.getString("campo"));
                    campos_jsp.setTipo_campo(rs.getString("tipo_campo"));//Tito Andr�s 12.11.2005
                    campos_jsps.add(campos_jsp);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS TIPOS DE DOCS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return campos_jsps;
    }
    
    /**
     * Lee una archivo JSP y carga los campos y los almacena en la tabla campos_jsp
     * @autor Rodrigo Salazar
     * @param cod C�digo de la p�gina JSP
     * @param carpeta Ruta f�sica del archivo
     * @param jsp Nombre de la p�gina JSP
     * @param user Login del usuario
     * @throws SQLException
     * @version 1.5
     */
    public void leeArchivo(String cod,String carpeta,String jsp,String user) throws SQLException{
        String linea="";
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        poolManager = PoolManager.getInstance();
        con = poolManager.getConnection("fintra");
        //String archivo = "c:/Archivos de programa/Apache Software Foundation/Tomcat 5.5/webapps/VCC/"+carpeta+"/"+jsp;
        //tamatu 26.10.2005
        String archivo = carpeta + jsp;
        //////System.out.println("---------------------------------------> archivo: " + archivo);
        
        //////System.out.println(archivo);
        try{
            
            BufferedReader input = new BufferedReader(new FileReader(archivo));
            int a,b;
            String tipo;
            while ((linea=input.readLine())!=null){
                a=0;
                b=0;
                tipo = "";
                for(int i=0; i<(linea.length()-1);i++){
                    if(((linea.charAt(i)=='c')||(linea.charAt(i)=='i')||(linea.charAt(i)=='s')||(linea.charAt(i)=='l')||(linea.charAt(i)=='b'))&&(linea.charAt(i+1)=='_')&&(linea.charAt(i-1)=='%')){
                        tipo = ""+linea.charAt(i);
                        a=i+2;
                    }
                    else if((a!=0)&&(linea.charAt(i)=='"')){
                        b=i;
                        break;
                    }
                }
                if(a!=0){
                    //////System.out.println(linea.substring(a,b));
                    
                    try{
                        if( !this.existcampos_jsp(cod, linea.substring(a,b))){//tamatu 26.10.2005
                            if (con != null){
                                st = con.prepareStatement("insert into campos_jsp ( pagina, campo, rec_status, last_update, user_update, creation_date, creation_user, tipo_campo ) " +
                                "values (?, ?, ' ' , 'now()', ?, 'now()', ?, ?)");
                                st.setString(1, cod);
                                st.setString(2, linea.substring(a,b));
                                st.setString(3, user);
                                st.setString(4, user);
                                st.setString(5, tipo);
                                //////System.out.println(st);
                                st.executeUpdate();
                                st.close();
                                
                            }
                        }
                    }
                    catch(SQLException e){
                        throw new SQLException("ERROR AL INSERTAR EL CAMPO" + e.getMessage()+"" + e.getErrorCode());
                    }
                }
                //////System.out.println(linea);
            }
        }
        catch(IOException e){
            //////System.out.println("No se pudo abrir el archivo");
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Lee una archivo JSP y carga los campos y los almacena en la tabla campos_jsp
     * @autor Tito Andr�s Maturana D.
     * @param cod C�digo de la p�gina JSP
     * @param carpeta Ruta f�sica del archivo
     * @param jsp Nombre de la p�gina JSP
     * @param user Login del usuario
     * @return Arreglo de tipo <code>Vector</code> con los campos enconrtrados
     * @throws SQLException
     * @version 1.0
     */
//    public Vector cargarCampos(String cod, String carpeta, String jsp, String user) throws SQLException{
//        
//        org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
//        
//        String linea="";
//        Connection con= null;
//        PreparedStatement st = null;
//        ResultSet rs = null;
//        PoolManager poolManager = null;
//        poolManager = PoolManager.getInstance();
//        con = poolManager.getConnection("fintra");
//        //String archivo = "c:/Archivos de programa/Apache Software Foundation/Tomcat 5.5/webapps/VCC/"+carpeta+"/"+jsp;
//        String archivo = carpeta + jsp;
//        //////System.out.println("---------------------------------------> archivo: " + archivo);
//        
//        Vector campos = new Vector();
//        campos_jsp field = null;
//        
//        //////System.out.println(archivo);
//        try{
//            
//            BufferedReader input = new BufferedReader(new FileReader(archivo));
//            int a,b;
//            String tipo;
//            while ((linea=input.readLine())!=null){
//                a=0;
//                b=0;
//                tipo = "";
//                
//                String ln = linea;
//                while( ln.indexOf("<input")!=-1 ){
//                    String nomcampo = "";
//                    String tipocampo = "";
//                    
//                    int campo = ln.indexOf("<input:");
//                    
//                    if(campo==-1){
//                        campo = ln.indexOf("<input");
//                    } else {
//                        String lnaux = ln.substring(campo);
//                        int tip = lnaux.indexOf(" ");
//                        ////////System.out.println("...............TAGLIB: " + lnaux.substring(7, tip));
//                        tipocampo = "input:" + lnaux.substring(7, tip);
//                    }
//                    
//                    String subln = ln.substring(campo);
//                    int nc = subln.indexOf("name=");
//                    int tc = subln.indexOf("type=");
//                    
//                    nomcampo = subln.substring(nc + 5);
//                    if( nomcampo.startsWith("'") ){
//                        nc = nomcampo.substring(1).indexOf("'");
//                        nomcampo = nomcampo.substring(1, nc+=1);
//                    } else if ( nomcampo.startsWith("\"") ){
//                        nc = nomcampo.substring(1).indexOf("\"");
//                        nomcampo = nomcampo.substring(1, nc+=1);
//                    } else {
//                        nc = nomcampo.indexOf(" ");
//                        nomcampo = nomcampo.substring(0, nc+=1);
//                    }
//                    
//                    if( tc!=-1 ){
//                        tipocampo = subln.substring(tc + 5);
//                        if( tipocampo.startsWith("'") ){
//                            tc = tipocampo.substring(1).indexOf("'");
//                            tipocampo = tipocampo.substring(1, tc+=1);
//                        } else if ( tipocampo.startsWith("\"") ){
//                            tc = tipocampo.substring(1).indexOf("\"");
//                            tipocampo = tipocampo.substring(1, tc+=1);
//                        } else {
//                            tc = tipocampo.indexOf(" ");
//                            tipocampo = tipocampo.substring(0, tc+=1);
//                        }
//                    } else if ( tipocampo.length()==0 ){
//                        tipocampo = "text";
//                    }
//                    ln = ln.substring(campo+=6);
//                    field = new campos_jsp();
//                    field.setTipo_campo(tipocampo.toLowerCase());
//                    field.setCampo(nomcampo);
//                    
//                    if( field.getCampo().trim().length()!=0 &&
//                    ( !field.getTipo_campo().matches("radiobutton") &&
//                    field.getTipo_campo().compareTo("hidden")!=0 &&
//                    !field.getTipo_campo().matches("submit")) &&
//                    !field.getTipo_campo().matches("input:hidden") &&
//                    !field.getTipo_campo().matches("input:radio") ){
//                        campos.add(field);
//                        //////System.out.println(".......................................CAMPO: " + nomcampo + ", TIPO: " + tipocampo);
//                    }
//                }
//                
//                ln = linea;
//                while(  ln.indexOf("<img")!=-1 ){
//                    String nomcampo = "";
//                    int campo = ln.indexOf("<img");
//                    String subln = ln.substring(campo);
//                    int nc = subln.indexOf("name=");
//                    
//                    int corte = -1;
//                    for( int i=0; i < subln.length(); i++){
//                        if(i!=0 && subln.charAt(i) == '>' && subln.charAt(i-1) != '%' ){
//                            corte = i;
//                            break;
//                        }
//                    }
//                    
//                    if( corte!=-1 && subln.substring(0, corte).indexOf("name=")!=-1 ) {
//                        nc = subln.substring(0, corte).indexOf("name=");
//                        nomcampo = subln.substring(nc + 5);
//                        if( nomcampo.startsWith("'") ){
//                            nc = nomcampo.substring(1).indexOf("'");
//                            nomcampo = nomcampo.substring(1, nc+=1);
//                        } else if ( nomcampo.startsWith("\"") ){
//                            nc = nomcampo.substring(1).indexOf("\"");
//                            nomcampo = nomcampo.substring(1, nc+=1);
//                        } else {
//                            nc = nomcampo.indexOf(" ");
//                            nomcampo = nomcampo.substring(0, nc+=1);
//                        }
//                    }
//                    
//                    
//                    ln = ln.substring(campo+=3);
//                    field = new campos_jsp();
//                    field.setTipo_campo("imagen");
//                    field.setCampo(nomcampo);
//                    
//                    if( field.getCampo().trim().length()!=0 ){
//                        //////System.out.println(".......................................CAMPO: " + nomcampo + ", TIPO: IMG");
//                        campos.add(field);
//                    }
//                }
//                
//                ln = linea;
//                while(  ln.indexOf("<select")!=-1 ){
//                    String nomcampo = "";
//                    int campo = ln.indexOf("<select");
//                    String subln = ln.substring(campo);
//                    int nc = subln.indexOf("name=");
//                    
//                    nomcampo = subln.substring(nc + 5);
//                    if( nomcampo.startsWith("'") ){
//                        nc = nomcampo.substring(1).indexOf("'");
//                        nomcampo = nomcampo.substring(1, nc+=1);
//                    } else if ( nomcampo.startsWith("\"") ){
//                        nc = nomcampo.substring(1).indexOf("\"");
//                        nomcampo = nomcampo.substring(1, nc+=1);
//                    } else {
//                        nc = nomcampo.indexOf(" ");
//                        nomcampo = nomcampo.substring(0, nc+=1);
//                    }
//                    
//                    ln = ln.substring(campo+=3);
//                    field = new campos_jsp();
//                    field.setTipo_campo("select");
//                    field.setCampo(nomcampo);
//                    
//                    campos.add(field);
//                }
//                
//                ln = linea;
//                while(  ln.indexOf("<a")!=-1 ){
//                    logger.info("LN: " + ln);
//                    String nomcampo = "";
//                    int campo = ln.indexOf("<a");
//                    String subln = ln.substring(campo);
//                    int nc = -1;
//                    /*int nc = subln.indexOf("\">");
//                    if( nc==-1){
//                        nc = subln.indexOf(" >");
//                    }*/
//                    int corte = -1;
//                    for( int i=0; i < subln.length(); i++){
//                        if(i!=0 && subln.charAt(i) == '>' && subln.charAt(i-1) != '%' ){
//                            corte = i;
//                            break;
//                        }
//                    }
//                    
//                    nomcampo = subln.substring(corte+=1);//+=2
//                    nc = nomcampo.indexOf("<");
//                    /*logger.info("corte: " + corte);
//                    logger.info("nc: " + nc);
//                    logger.info("subln: " + subln);
//                    logger.info("nomcampo: " + nomcampo);*/
//                    nomcampo = nomcampo.substring(0, nc);//!!
//                    
//                    ln = ln.substring(campo+=3);
//                    field = new campos_jsp();
//                    field.setTipo_campo("link");
//                    field.setCampo(nomcampo);
//                    
//                    if( field.getCampo().trim().length()!=0 ){
//                        campos.add(field);
//                    }
//                }
//                
//                //////System.out.println(linea);
//            }
//        }
//        catch(IOException e){
//            //////System.out.println("No se pudo abrir el archivo");
//        }
//        finally{
//            if (st != null){
//                try{
//                    st.close();
//                }
//                catch(SQLException e){
//                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
//                }
//            }
//            if (con != null){
//                poolManager.freeConnection("fintra", con);
//            }
//        }
//        
//        return campos;
//    }
    
    public Vector cargarCampos(String cod, String carpeta, String jsp, String user) throws SQLException{
        
        org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
        
        String linea="";
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        poolManager = PoolManager.getInstance();
        con = poolManager.getConnection("fintra");
        //String archivo = "c:/Archivos de programa/Apache Software Foundation/Tomcat 5.5/webapps/VCC/"+carpeta+"/"+jsp;
        String archivo = carpeta + jsp;
        //////System.out.println("---------------------------------------> archivo: " + archivo);
        
        Vector campos = new Vector();
        campos_jsp field = null;
        
        //////System.out.println(archivo);
        try{
            
            BufferedReader input = new BufferedReader(new FileReader(archivo));
            int a,b;
            String tipo;
            String ln0 = "";
            String ln;
            while ((linea=input.readLine())!=null){
                a=0;
                b=0;
                tipo = "";
                
                ln0 += linea;
            }
            
            linea = ln0;
            //logger.info("LINEA: " + linea);
            ln = linea;
            
            while( ln.indexOf("<input")!=-1 ){
                String nomcampo = "";
                String tipocampo = "";
                
                int campo = ln.indexOf("<input:");
                
                if(campo==-1){
                    campo = ln.indexOf("<input");
                } else {
                    String lnaux = ln.substring(campo);
                    int tip = lnaux.indexOf(" ");
                    ////////System.out.println("...............TAGLIB: " + lnaux.substring(7, tip));
                    tipocampo = "input:" + lnaux.substring(7, tip);
                }
                
                String subln = ln.substring(campo);
                int nc = subln.indexOf("name=");
                int tc = subln.indexOf("type=");
                
                nomcampo = subln.substring(nc + 5);
                if( nomcampo.startsWith("'") ){
                    nc = nomcampo.substring(1).indexOf("'");
                    nomcampo = nomcampo.substring(1, nc+=1);
                } else if ( nomcampo.startsWith("\"") ){
                    nc = nomcampo.substring(1).indexOf("\"");
                    nomcampo = nomcampo.substring(1, nc+=1);
                } else {
                    nc = nomcampo.indexOf(" ");
                    nomcampo = nomcampo.substring(0, nc+=1);
                }
                
                if( tc!=-1 ){
                    tipocampo = subln.substring(tc + 5);
                    if( tipocampo.startsWith("'") ){
                        tc = tipocampo.substring(1).indexOf("'");
                        tipocampo = tipocampo.substring(1, tc+=1);
                    } else if ( tipocampo.startsWith("\"") ){
                        tc = tipocampo.substring(1).indexOf("\"");
                        tipocampo = tipocampo.substring(1, tc+=1);
                    } else {
                        tc = tipocampo.indexOf(" ");
                        tipocampo = tipocampo.substring(0, tc+=1);
                    }
                } else if ( tipocampo.length()==0 ){
                    tipocampo = "text";
                }
                ln = ln.substring(campo+=6);
                field = new campos_jsp();
                field.setTipo_campo(tipocampo.toLowerCase());
                field.setCampo(nomcampo);
                
                if( field.getCampo().trim().length()!=0 &&
                ( !field.getTipo_campo().matches("radiobutton") &&
                field.getTipo_campo().compareTo("hidden")!=0 &&
                !field.getTipo_campo().matches("submit")) &&
                !field.getTipo_campo().matches("input:hidden") &&
                !field.getTipo_campo().matches("input:radio") ){
                    campos.add(field);
                    //////System.out.println(".......................................CAMPO: " + nomcampo + ", TIPO: " + tipocampo);
                }
            }
            
            ln = linea;
            while(  ln.indexOf("<img")!=-1 ){
                String nomcampo = "";
                int campo = ln.indexOf("<img");
                String subln = ln.substring(campo);
                int nc = subln.indexOf("name=");
                
                int corte = -1;
                for( int i=0; i < subln.length(); i++){
                    if(i!=0 && subln.charAt(i) == '>' && subln.charAt(i-1) != '%' ){
                        corte = i;
                        break;
                    }
                }
                
                if( corte!=-1 && subln.substring(0, corte).indexOf("name=")!=-1 ) {
                    nc = subln.substring(0, corte).indexOf("name=");
                    nomcampo = subln.substring(nc + 5);
                    if( nomcampo.startsWith("'") ){
                        nc = nomcampo.substring(1).indexOf("'");
                        nomcampo = nomcampo.substring(1, nc+=1);
                    } else if ( nomcampo.startsWith("\"") ){
                        nc = nomcampo.substring(1).indexOf("\"");
                        nomcampo = nomcampo.substring(1, nc+=1);
                    } else {
                        nc = nomcampo.indexOf(" ");
                        nomcampo = nomcampo.substring(0, nc+=1);
                    }
                }
                
                
                ln = ln.substring(campo+=3);
                field = new campos_jsp();
                field.setTipo_campo("imagen");
                field.setCampo(nomcampo);
                
                if( field.getCampo().trim().length()!=0 ){
                    //////System.out.println(".......................................CAMPO: " + nomcampo + ", TIPO: IMG");
                    campos.add(field);
                }
            }
            
            ln = linea;
            while(  ln.indexOf("<select")!=-1 ){
                String nomcampo = "";
                int campo = ln.indexOf("<select");
                String subln = ln.substring(campo);
                int nc = subln.indexOf("name=");
                
                nomcampo = subln.substring(nc + 5);
                if( nomcampo.startsWith("'") ){
                    nc = nomcampo.substring(1).indexOf("'");
                    nomcampo = nomcampo.substring(1, nc+=1);
                } else if ( nomcampo.startsWith("\"") ){
                    nc = nomcampo.substring(1).indexOf("\"");
                    nomcampo = nomcampo.substring(1, nc+=1);
                } else {
                    nc = nomcampo.indexOf(" ");
                    nomcampo = nomcampo.substring(0, nc+=1);
                }
                
                ln = ln.substring(campo+=3);
                field = new campos_jsp();
                field.setTipo_campo("select");
                field.setCampo(nomcampo);
                
                campos.add(field);
            }
            
            ln = linea;
            while(  ln.indexOf("<a")!=-1 ){
                //logger.info("LN: " + ln);
                String nomcampo = "";
                int campo = ln.indexOf("<a");
                String subln = ln.substring(campo);
                int nc = -1;
                    /*int nc = subln.indexOf("\">");
                    if( nc==-1){
                        nc = subln.indexOf(" >");
                    }*/
                int corte = -1;
                for( int i=0; i < subln.length(); i++){
                    if(i!=0 && subln.charAt(i) == '>' && subln.charAt(i-1) != '%' ){
                        corte = i;
                        break;
                    }
                }
                
                nomcampo = subln.substring(corte+=1);//+=2
                nc = nomcampo.indexOf("<");
                    /*logger.info("corte: " + corte);
                    logger.info("nc: " + nc);
                    logger.info("subln: " + subln);
                    logger.info("nomcampo: " + nomcampo);*/
                nomcampo = nomcampo.substring(0, nc);//!!
                
                ln = ln.substring(campo+=3);
                field = new campos_jsp();
                field.setTipo_campo("link");
                field.setCampo(nomcampo);
                
                if( field.getCampo().trim().length()!=0 ){
                    campos.add(field);
                }
            }
                
                //////System.out.println(linea);
        }
        catch(IOException e){
            //////System.out.println("No se pudo abrir el archivo");
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        return campos;
    }
    
    /**
     * Lee una archivo JSP y rescribe los tags segun los tipos de campos.
     * @autor Leonardo Parody Ponce.
     * @param nombre del campo
     * @param tipo del campo
     * @param jsp de la p�gina JSP
     * @param carpeta Ruta f�sica del archivo
     * @param user Login del usuario
     * @throws SQLException
     * @version 1.0
     */
    public void archivosjsp(String carpeta,String jsp,String campo, String tipo) throws SQLException{
        LinkedList lineas = new LinkedList();
        LinkedList lineasaux = new LinkedList();
        int cent = 0;
        String linea="";
        String linea1="";
        String lineawrite="";
        String archivo = carpeta + jsp;
        //////System.out.println("Campo = "+campo+"--- tipo = "+tipo);
        //////System.out.println("---------------------------------------> archivo: " + archivo);
        //////System.out.println(archivo);
        
        try{
            BufferedReader input = new BufferedReader(new FileReader(archivo));
            //////System.out.println("YA ABRI EL ARCHIVO");
            int l=0;
            int a,b;
            int swU=0;
            int swV=0;
            int swL=0;
            int begin = 0;
            while ((linea1=input.readLine())!=null){
                //////System.out.println(linea1);
                //////System.out.println("GUARDANDO LINEA POR LINEA");
                lineas.add(this.escribeArchivo(linea1,campo,tipo));
            }
            for (int i =0; i<lineas.size(); i++) {
                linea = (String) lineas.get(i);
                int sw = linea.indexOf("<form");
                int sw1 = linea.indexOf("<FORM");
                int sw2 = linea.indexOf("<table");
                int sw3 = linea.indexOf("<TABLE");
                //////System.out.println("sw----"+sw+"-------  sw1----"+sw1);
                if ((sw!=-1)||(sw1!=-1)||(sw2!=-1)||(sw3!=-1)) {
                    //////System.out.println("BREAK SW SW1");
                    begin = i;
                    break;
                } else {
                    //////System.out.println("LIN----"+linea);
                    lineasaux.add(linea);
                    if (swU <= 0) {
                        swU = linea.indexOf("session.getAttribute(\"Usuario\")");
                    }
                    if (swV <= 0) {
                        swV = linea.indexOf("Vector pvpag");
                    }
                    if (swL <= 0) {
                        swL = linea.indexOf("usuario.getLogin()");
                    }
                    //////System.out.println("------swU---"+swU+"---swV"+swV+"---swL--"+swL);
                }
            }
            if (swU<=0) {
                lineasaux.add("<%Usuario usuario = (Usuario) session.getAttribute(\"Usuario\");%>");
            }
            if (swL<=0) {
                lineasaux.add("<%String user = usuario.getLogin();%>");
            }
            if (swV<=0) {
                lineasaux.add("<%Vector pvpag = model.perfil_vistaService.consultPerfil_vistas(user,\""+jsp+"\");%>");
            }
            for (int i = begin; i < lineas.size();i++){
                lineasaux.add(lineas.get(i));
            }
            BufferedWriter output = new BufferedWriter(new FileWriter(archivo));
            PrintWriter pntw = new PrintWriter(output);
            for (int i=0; i<lineasaux.size(); i++) {
                pntw.println((String)lineasaux.get(i));
            }
            pntw.close();
        }
        catch(IOException e){
            //////System.out.println("No se pudo abrir el archivo");
        }
    }
    /**
     * Lee una archivo JSP y rescribe los tags segun los tipos de campos.
     * @autor Leonardo Parody Ponce.
     * @param nombre del campo
     * @param  tipo del campo
     * @param nombre de la p�gina JSP
     * @param carpeta Ruta f�sica del archivo
     * @param user Login del usuario
     * @throws SQLException
     * @version 1.0
     */
    
    public String escribeArchivo(String linea, String campo, String tipo) throws SQLException{
        
        org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
        String linearet="";
        
        try {
            
            LinkedList lineas = new LinkedList();
            LinkedList lineasaux = new LinkedList();
            int cent = 0;
            String linea1="";
            int nc=0;
            int tc=0;
            String nomcampo = "";
            String tipocampo = "";
            String lineawhile = linea;
            cent = 0;
            int index1 = 0;
            int index = lineawhile.indexOf("<input");
            if(index!=-1) cent += index;
            //////System.out.println("INDEX"+index);
            if ((tipo.equals("text"))||(tipo.equals("checkbox"))||(tipo.equals("button"))){
                while (index!=-1){
                    //////System.out.println("INPUT");
                    String subln = lineawhile.substring(index);
                    nc = subln.indexOf("name=");
                    //////System.out.println("INDEX NAME"+nc);
                    tc = subln.indexOf("type=");
                    nomcampo = subln.substring(nc + 5);
                    //int ind = cent + nc;//tamatu 12.12.2005;
                    if( nomcampo.startsWith("'") ){
                        nc = nomcampo.substring(1).indexOf("'");
                        nomcampo = nomcampo.substring(1, nc+=1);
                        //////System.out.println("NOMBRE1"+nomcampo);
                    } else if ( nomcampo.startsWith("\"") ){
                        nc = nomcampo.substring(1).indexOf("\"");
                        nomcampo = nomcampo.substring(1, nc+=1);
                        //////System.out.println("NOMBRE2"+nomcampo);
                    } else {
                        nc = nomcampo.indexOf(" ");
                        nomcampo = nomcampo.substring(1, nc+=1);
                        //////System.out.println("NOMBRE3"+nomcampo);
                    }
                    if (nomcampo.equals(campo)){
                        //////System.out.println("IF NOMCAMPO------"+nomcampo+"--------"+campo+"------TIPO-----"+tipo);
                        //////System.out.println("IF TIPO"+tipo);
                        int ind = lineawhile.indexOf("name");
                        
                        //////System.out.println("sbln-----"+subln);
                        String aux = subln.substring(0, ind);
                        //////System.out.println("aux----"+aux);
                        int mod = aux.indexOf("<%=model.perfil_vistaService.property");
                        if (mod<0) {
                            //////System.out.println("cent = "+cent+" --- index1 = "+index1+" --- index = "+index+"  --- ind = "+ind);
                            linearet = linea.substring(0,index1+ind-1) + " <%=model.perfil_vistaService.property(\"\",\""+campo+"\",pvpag,\"\")%> " + linea.substring(index1+ind);
                            break;
                        }else {
                            linearet = linea;
                            break;
                        }
                    }else{
                        lineawhile = linea.substring(lineawhile.indexOf(">")+1);
                        //////System.out.println("lineawhile------"+lineawhile+"-------lineawhile indexof---"+lineawhile.indexOf(">"));
                    }
                    cent += subln.indexOf(">")+1;
                    //////System.out.println("cent----"+cent);
                    lineawhile = linea.substring(cent);
                    index=lineawhile.indexOf("<input");
                    lineawhile = linea.substring(cent+index);
                    index1 = cent + index;
                    index = lineawhile.indexOf("<input");
                    //////System.out.println("lineawhile--"+lineawhile);
                }
                //***********************************
                //********** Combo box **************
                //***********************************
                
            }else if (tipo.equals("select")){
                linearet = "";
                lineawhile = linea;
                index = lineawhile.indexOf("<select");
                int sel1 = lineawhile.indexOf("<select");
                //////System.out.println("index inicial ="+index);
                if (index != -1){
                    lineawhile = lineawhile.substring(index);
                }
                cent += index;
                //////System.out.println("while =  "+(index != -1));
                while (index != -1){
                    String subln = lineawhile;
                    //////System.out.println("lineawhile primera = "+lineawhile);
                    nc = subln.indexOf("name=");
                    tc = subln.indexOf("type=");
                    nomcampo = subln.substring(nc + 5);
                    //int ind = cent + nc;//tamatu 12.12.2005;
                    if( nomcampo.startsWith("'") ){
                        nc = nomcampo.substring(1).indexOf("'");
                        nomcampo = nomcampo.substring(1, nc+=1);
                    } else if ( nomcampo.startsWith("\"") ){
                        nc = nomcampo.substring(1).indexOf("\"");
                        nomcampo = nomcampo.substring(1, nc+=1);
                    } else {
                        nc = nomcampo.indexOf(" ");
                        nomcampo = nomcampo.substring(1, nc+=1);
                    }
                    if (nomcampo.equals(campo)){
                        //////System.out.println("sel1 = "+sel1+"---index ="+index);
                        if (sel1>=0){
                            int ind = subln.indexOf("name");
                            //////System.out.println("IF NOMCAMPO------"+nomcampo+"--------"+campo+"------TIPO-----"+tipo);
                            //////System.out.println("IF TIPO"+tipo);
                            //////System.out.println("sbln-----"+subln);
                            String aux = subln.substring(0, ind);
                            //////System.out.println("aux----"+aux);
                            int mod = aux.indexOf("<%=model.perfil_vistaService.property");
                            if (mod<0) {
                                //////System.out.println("cent = "+cent+" --- index1 = "+index1+" --- index = "+index+"  --- ind = "+ind);
                                //////System.out.println(linea.substring(0,cent+ind-1));
                                linearet = linea.substring(0,cent+ind-1) + " <%=model.perfil_vistaService.property(\"\",\""+campo+"\",pvpag,\"\")%> " + linea.substring(cent+ind-1);
                                break;
                            } else {
                                linearet = linea;
                                break;
                            }
                        }
                    }
                    int lwhl = lineawhile.indexOf(">");
                    lineawhile = lineawhile.substring(lwhl);
                    cent += lwhl;
                    //////System.out.println("cent----"+cent);
                    //////System.out.println("lineawhile al final="+lineawhile);
                    index=lineawhile.indexOf("<select");
                    //////System.out.println("");
                    sel1 = lineawhile.indexOf("<select");
                    cent += index;
                    if ( index != -1 ) {
                        lineawhile = lineawhile.substring(index);
                    }
                    //////System.out.println("lineawhile--"+lineawhile);
                    //////System.out.println("lineawhilefinal final="+lineawhile+"    cent="+cent+"    index"+index);
                }
                //***********************************
                //********** input: taglibrary **************
                //***********************************
            } else if ((tipo.equals("input:select")||(tipo.equals("input:text"))||(tipo.equals("input:checkbox")))){
                linearet = "";
                lineawhile = linea;
                cent = 0;
                index = lineawhile.indexOf("<input:");
                int sel2 = lineawhile.indexOf("<input:");
                //////System.out.println("index inicial ="+index);
                if (index != -1){
                    lineawhile = lineawhile.substring(index);
                }
                cent += index;
                //////System.out.println("while =  "+(index != -1));
                while (index != -1){
                    String subln = lineawhile;
                    //////System.out.println("lineawhile primera = "+lineawhile);
                    nc = subln.indexOf("name=");
                    tc = subln.indexOf("type=");
                    nomcampo = subln.substring(nc + 5);
                    if( nomcampo.startsWith("'") ){
                        nc = nomcampo.substring(1).indexOf("'");
                        nomcampo = nomcampo.substring(1, nc+=1);
                    } else if ( nomcampo.startsWith("\"") ){
                        nc = nomcampo.substring(1).indexOf("\"");
                        nomcampo = nomcampo.substring(1, nc+=1);
                    } else {
                        nc = nomcampo.indexOf(" ");
                        nomcampo = nomcampo.substring(1, nc+=1);
                    }
                    if (nomcampo.equals(campo)){
                        if (sel2>=0){
                            int nprop = subln.indexOf(">");
                            String propiedad = subln.substring(0,nprop);
                            int n1prop = propiedad.indexOf("<%=model.perfil_vistaService.property");
                            if (n1prop == -1){
                                int selind = subln.indexOf("attributesText=");
                                if (selind>-1) {
                                    String linauxsel = subln.substring(selind);
                                    int selind1 = linauxsel.indexOf("\"");
                                    String linauxsel1 = linauxsel.substring(selind1+1);
                                    int selind2 = linauxsel1.indexOf("\"");
                                    //String attribute = linauxsel.substring(0, selind1+selind2+2);
                                    String attribute = linauxsel.substring(selind1, selind1+selind2+2);//AMATURANA 30.11.2006
                                    //logger.info("ATTRIBUTE: " + attribute );
                                    //logger.info("RESTO: " + linauxsel1.substring(selind2+2));
                                    ////System.out.println("atribute-----------"+attribute);
                                    //linearet = linea.substring(0, cent + selind+16) + " <%=model.perfil_vistaService.property(\"\",\""+campo+"\",pvpag,\"\")%> " + linea.substring(cent+selind+selind1+1);
                                    //linearet = linea.substring(0, cent + selind+16) + linea.substring(cent+selind+selind1+1);
                                    linearet = linea.substring(0, cent + selind+16) + "<%=model.perfil_vistaService.property(\"\",\""+campo+"\",pvpag,\"\") + " +
                                    attribute + "%>\" " + linauxsel1.substring(selind2+2);//AMATURANA 29.11.2006
                                    logger.info("LNRET: " + linearet);
                                } else {
                                    linearet = linea.substring(0,cent + nc-1) + " attributeText=\"<%=model.perfil_vistaService.property(\"\",\""+campo+"\",pvpag,\"\")%>\" " + linea.substring(cent+nc-1);
                                }
                                break;
                            }
                        }
                    }
                    int lwhl = lineawhile.indexOf(">");
                    lineawhile = lineawhile.substring(lwhl);
                    cent += lwhl;
                    //////System.out.println("cent----"+cent);
                    //////System.out.println("lineawhile al final="+lineawhile);
                    index=lineawhile.indexOf("<select");
                    //////System.out.println("");
                    sel2 = lineawhile.indexOf("<select");
                    cent += index;
                    if ( index != -1 ) {
                        lineawhile = lineawhile.substring(index);
                    }
                    //////System.out.println("lineawhile--"+lineawhile);
                    //////System.out.println("lineawhilefinal final="+lineawhile+"    cent="+cent+"    index"+index);
                }
                
                
                
                //***********************************
                //********** Imagen    **************
                //***********************************
            } else if (tipo.equals("imagen")){
                //////System.out.println("ENTRE AL IF DE IMAGEN");
                linearet = "";
                lineawhile = linea;
                
                //logger.info("LINEA IMG = "+linea);
                cent = 0;
                index = lineawhile.indexOf("<img");
                //////System.out.println("index inicial ="+index);
                if (index != -1){
                    lineawhile = lineawhile.substring(index);
                }
                cent += index;
                //////System.out.println("while =  "+(index != -1));
                while (index != -1){
                    //logger.info("WHILE IMAGEN " + index);
                    String subln = lineawhile;
                    //logger.info("lineawhile primera = "+lineawhile);
                    nc = subln.indexOf("name=");
                    tc = subln.indexOf("type=");
                    if (nc == -1) {
                        break;
                    }
                    nomcampo = subln.substring(nc + 5);
                    if( nomcampo.startsWith("'") ){
                        nc = nomcampo.substring(1).indexOf("'");
                        nomcampo = nomcampo.substring(1, nc+=1);
                    } else if ( nomcampo.startsWith("\"") ){
                        nc = nomcampo.substring(1).indexOf("\"");
                        nomcampo = nomcampo.substring(1, nc+=1);
                    } else {
                        nc = nomcampo.indexOf(" ");
                        nomcampo = nomcampo.substring(1, nc+=1);
                    }
                    if (nomcampo.equals(campo)){
                        //onclick
                        int oind = subln.indexOf("onclick");
                        if (oind < 0) {
                            oind = subln.indexOf("Onclick");
                            //////System.out.println("1OIND----"+oind);
                            if (oind < 0) {
                                oind = subln.indexOf("onClick");
                                //////System.out.println("2OIND----"+oind);
                                if (oind < 0) {
                                    oind = subln.indexOf("OnClick");
                                    //////System.out.println("3OIND----"+oind);
                                    
                                    if (oind < 0) {
                                        oind = subln.indexOf("ONCLICK");
                                        //////System.out.println("4OIND----"+oind);
                                        
                                    }
                                }
                            }
                        }
                        String linaux = subln.substring(oind);
                        int oind1 = subln.indexOf("\"");
                        String linaux1 = linaux.substring(oind1+1);
                        int oind2 = linaux1.indexOf("\"");
                        //////System.out.println("LOS OIND---"+oind+"-"+linea.substring(oind)+"----"+oind1+"----"+linea.substring(oind1)+"----"+oind2+"----"+linea.substring(oind2));
                        //String onclick = linaux.substring(0,oind1+oind2+2);
                        String onclick = linaux.substring(0,oind1+oind2+2);
                        onclick = onclick.charAt(onclick.length() - 1 ) == '>' ? onclick.substring(0, onclick.length() - 1) : onclick ;//AMATURANA
                        logger.info("ONCLICK-----------"+onclick);
                        
                        //src
                        int sind = subln.indexOf("src");
                        String linauxs = subln.substring(sind);
                        int sind1 = linauxs.indexOf("\"");
                        String linauxs1 = linauxs.substring(sind1+1);
                        int sind2 = linauxs1.indexOf("\"");
                        String src = linauxs.substring(0, sind1+sind2+2);
                        //logger.info("SRC-----------"+src);
                        //onMouseOut
                        int omoind = subln.indexOf("onMouseOut");
                        if (omoind < 0) {
                            omoind = subln.indexOf("OnMouseOut");
                            //////System.out.println("1OIND----"+omoind);
                            if (omoind < 0) {
                                omoind = subln.indexOf("onmouseOut");
                                //////System.out.println("2OIND----"+omoind);
                                if (omoind < 0) {
                                    omoind = subln.indexOf("onMouseout");
                                    //////System.out.println("3OIND----"+omoind);
                                    
                                }
                            }
                        }
                        //logger.info(" subln: " + subln + " - omoind: " + omoind);
                        String onmouseout = "";
                        if( omoind > -1 ){
                            String linauxo = subln.substring(omoind);//!!
                            int omoind1 = linauxo.indexOf("\"");
                            String linauxo1 = linauxo.substring(omoind1+1);
                            int omoind2 = linauxo1.indexOf("\"");
                            onmouseout = linauxo.substring(0, omoind1+omoind2+2);
                        }
                        //////System.out.println("onmouseout-----------"+onmouseout);
                        //onMouseOver
                        int omvind = subln.indexOf("onMouseOver");
                        if (omvind < 0) {
                            omvind = subln.indexOf("OnMouseOver");
                            //////System.out.println("1OIND----"+omvind);
                            if (omvind < 0) {
                                omvind = subln.indexOf("onmouseOver");
                                //////System.out.println("2OIND----"+omvind);
                                if (omoind < 0) {
                                    omvind = subln.indexOf("onMouseover");
                                    //////System.out.println("3OIND----"+omvind);
                                    
                                }
                            }
                        }
                        String onmouseover = "";
                        if( omvind > -1 ){
                            String linauxv = subln.substring(omvind);//!!
                            int omvind1 = linauxv.indexOf("\"");
                            String linauxv1 = linauxv.substring(omvind1+1);
                            int omvind2 = linauxv1.indexOf("\"");
                            onmouseover = linauxv.substring(0, omvind1+omvind2+2);
                        }
                        //logger.info("onmouseover-----------"+onmouseover);
                        int ind = subln.indexOf("name");
                        String auxnom = subln.substring(ind);
                        int ind1 = auxnom.indexOf("\"");
                        String aux = subln.substring(0, ind);
                        //////System.out.println("aux----"+aux);
                        int mod = aux.indexOf("<%=model.perfil_vistaService.property");
                        int corte = -1;
                        for( int i=0; i < subln.length(); i++){
                            if(i!=0 && subln.charAt(i) == '>' && subln.charAt(i-1) != '%' ){
                                corte = i;
                                break;
                            }
                        }
                        //name
                        if (mod<0) {
                            if (linea.substring(cent,cent+sind).indexOf("name")!=-1) {
                                //////System.out.println("corte="+( (corte!=-1) ? subln.substring(corte) : "" ));
                                linearet = linea.substring(0,cent+sind)+" <%=model.perfil_vistaService.property(\""+Convertirfunciones(src)+"\",\""+campo+"\",pvpag, \""+Convertirfunciones(onclick)+" "+Convertirfunciones(onmouseout)+" "+Convertirfunciones(onmouseover)+"\")%>" /*+"> "*/ + ( (corte!=-1) ? subln.substring(corte) : "" );
                            }else{
                                //////System.out.println("corte="+( (corte!=-1) ? subln.substring(corte) : "" ));
                                //////System.out.println("ind = "+ind+"---- ind1="+ind1+"--- subln="+subln.substring(ind)+"-----auxnom = "+auxnom);
                                //logger.info("onmouseover: " + onmouseover);
                                //logger.info("Convertirfunciones(onmouseover): " + Convertirfunciones(onmouseover));
                                linearet = linea.substring(0,cent+sind)+" <%=model.perfil_vistaService.property(\""+Convertirfunciones(src)+"\",\""+campo+"\",pvpag, \""+Convertirfunciones(onclick)+" "+Convertirfunciones(onmouseout)+" "+Convertirfunciones(onmouseover)+"\")%>" + " name=\""+nomcampo+"\"" /*+"> "*/ + ( (corte!=-1) ? subln.substring(corte) : "" );
                                logger.info("LINEA RET1: " + linearet);
                            }
                            break;
                        }else {
                            linearet = linea;
                            //logger.info("LINEA RET2: " + linearet);
                            break;
                        }
                    }
                    int lwhl = lineawhile.indexOf(">");
                    lineawhile = lineawhile.substring(lwhl);
                    cent += lwhl;
                    //////System.out.println("cent----"+cent);
                    //////System.out.println("lineawhile al final="+lineawhile);
                    index=lineawhile.indexOf("<img");
                    //////System.out.println("");
                    cent += index;
                    if (index != -1){
                        lineawhile = lineawhile.substring(index);
                    }else{
                        break;
                    }
                    //////System.out.println("lineawhile--"+lineawhile);
                    //////System.out.println("lineawhilefinal final="+lineawhile+"    cent="+cent+"    index"+index);
                }
                //***********************************
                //**********   Link    **************
                //***********************************
            } else if (tipo.equals("link")){
                //////System.out.println("ENTRE AL IF DE IMAGEN");
                linearet = "";
                cent = 0;
                lineawhile = linea;
                index = lineawhile.indexOf("<a");
                //////System.out.println("index inicial ="+index);
                if (index != -1){
                    lineawhile = lineawhile.substring(index);
                }
                //////System.out.println("while =  "+(index != -1));
                while (index != -1){
                    String subln = linea.substring(index);
                    //nc = subln.indexOf("name=");
                    tc = subln.indexOf("type=");
                   /*nomcampo = subln.substring(nc + 5);
                   if( nomcampo.startsWith("'") ){
                       nc = nomcampo.substring(1).indexOf("'");
                       nomcampo = nomcampo.substring(1, nc+=1);
                   } else if ( nomcampo.startsWith("\"") ){
                       nc = nomcampo.substring(1).indexOf("\"");
                       nomcampo = nomcampo.substring(1, nc+=1);
                   } else {
                       nc = nomcampo.indexOf(" ");
                       nomcampo = nomcampo.substring(1, nc+=1);
                   }*/
                    nc = -1;
                    /*int nc = subln.indexOf("\">");
                    if( nc==-1){
                        nc = subln.indexOf(" >");
                    }*/
                    int corte = -1;
                    for( int i=0; i < subln.length(); i++){
                        if(i!=0 && subln.charAt(i) == '>' && subln.charAt(i-1) != '%' ){
                            corte = i;
                            break;
                        }
                    }
                    
                    nomcampo = subln.substring(corte+=1);//+=2
                    nc = nomcampo.indexOf("<");
                    nomcampo = nomcampo.substring(0, nc);
                    //////System.out.println("NOMBRE---"+nomcampo);
                    if (nomcampo.equals(campo)){
                        //href
                        String href = "";
                        int oindh = -1;
                        int oind1h = -1;
                        int oind2h = -1;
                        if( subln.indexOf("href")!=-1 ){
                            oindh = subln.indexOf("href");
                            String linauxh = subln.substring(oindh);
                            oind1h = linauxh.indexOf("\"");
                            String linaux1h = linauxh.substring(oind1h+1);
                            oind2h = linaux1h.indexOf("\"");
                            href = linauxh.substring(0, oind1h+oind2h+2);
                            //////System.out.println("HREF-----------"+href);
                        }
                        
                        //onclick
                        int oindl = subln.indexOf("onclick");
                        if (oindl < 0) {
                            oindl = subln.indexOf("Onclick");
                            //////System.out.println("1OIND----"+oindl);
                            if (oindl < 0) {
                                oindl = subln.indexOf("onClick");
                                //////System.out.println("2OIND----"+oindl);
                                if (oindl < 0) {
                                    oindl = subln.indexOf("OnClick");
                                    //////System.out.println("3OIND----"+oindl);
                                    
                                }
                            }
                        }
                        
                        String onclick = "";
                        int oind1l = -1;
                        int oind2l = -1;
                        if( oindl!=-1 ){
                            String linauxl = subln.substring(oindl);
                            oind1l = linauxl.indexOf("\"");
                            String linaux1l = linauxl.substring(oind1l+1);
                            oind2l = linaux1l.indexOf("\"");
                            //////System.out.println("LOS OIND---"+oindl+"-"+subln.substring(oindl)+"----"+oind1l+"----"+subln.substring(oind1l)+"----"+oind2l+"----"+subln.substring(oind2l));
                            onclick = linauxl.substring(0,oind1l+oind2l+2);
                            //////System.out.println("ONCLICK-----------"+onclick);
                        }
                        
                        //////System.out.println("subln = " + subln);
                        int auxpropnum = subln.indexOf(">");
                        //logger.info("auxpropnum= "+auxpropnum);
                        String auxprop = subln.substring(0,auxpropnum);
                        int mod = auxprop.indexOf("<%=model.perfil_vistaService.property");
                        if (mod<0) {
                            if ( !href.equals("") && !onclick.equals("") ) {
                                if (oindl>oindh) {
//                                    logger.info("Cent= "+cent+ " - Index = "+index);
//                                    logger.info("oindh: " + oindh);
//                                    logger.info("Linea = (" + (cent+index+oindh+oind1h+oind2h+2) + "," + (cent+index+oindl) + "):");
//                                    logger.info("LINEA: " + linea.substring(cent+index+oindh+oind1h+oind2h+2,cent+index+oindl));
//                                    logger.info("Linea = (" + (cent+index+oindl+cent+oind1l+oind2l+2) + "):");
//                                    logger.info("LINEA: " + linea.substring(cent+index+oindl+cent+oind1l+oind2l+2));
                                    linearet = linea.substring(0,cent+index+2)+
                                            " <%=model.perfil_vistaService.property(\"\",\""+campo+"\",pvpag, \""+
                                            Convertirfunciones(href)+"  "+
                                            Convertirfunciones(onclick)+"\")%>"+
                                            //linea.substring(cent+index+2,cent+oindh)+
                                            linea.substring(cent+index+2,cent+index+oindh)+//AMATURANA
                                            //linea.substring(cent+oindh+oind1h+oind2h+2,cent+oindl)+
                                            linea.substring(cent+index+oindh+oind1h+oind2h+2,cent+index+oindl)+//AMATURANA
                                            //linea.substring(cent+oindl+cent+oind1l+oind2l+3);
                                            linea.substring(cent+index+oindl+cent+oind1l+oind2l+2);//AMATURANA
                                }else{
                                    linearet = linea.substring(0,cent+index+2)+" <%=model.perfil_vistaService.property(\"\",\""+campo+"\",pvpag, \""+Convertirfunciones(href)+"  "+Convertirfunciones(onclick)+"\")%>" + linea.substring(cent+index+2,cent+oindl) + linea.substring(cent+oindl+oind1l+oind2l+3,cent+oindl) + linea.substring(cent+oindh+oind1h+oind2h+2);
                                }
                                break;
                            } else if ( href.equals("") ) {
                                //////System.out.println("oindl="+oindl+"   oindl1="+oind1l+"   oind2l="+oind2l);
                                linearet = linea.substring(0,cent+index+2)+" <%=model.perfil_vistaService.property(\"\",\""+campo+"\",pvpag, \""+Convertirfunciones(href)+"  "+Convertirfunciones(onclick)+"\")%>" + linea.substring(cent+index+2,cent+oindl) + linea.substring(cent+oindl+cent+oind1l+oind2l+3);
                                break;
                                //          <a href="<%=CONTROLLER%>?estado=Ver&accion=Estadogeneral&pagina=VerEstadoGeneral.jsp&carpeta=jsp/trafico/estado"  target="_self">Ver Todos</a>
                            } else if ( onclick.equals("") ) {
                                //////System.out.println("........index=" + index + "  oindh="+oindh+"   oindlh="+oind1h+"   oind2h="+oind2h+"   cent="+cent);
                                try{
                                    linea.substring(cent+oindh+oind1h+oind2h+2);
                                }
                                catch(Exception e){
                                    //////System.out.println("..........el error esta en linea.substring(cent+oindh+oind1h+oind2h+2)");
                                }
                                try{
                                    linea.substring(cent+index+2,cent+oindh+index);
                                }catch(Exception exc){
                                    //////System.out.println(".......... el error esta en linea.substring(cent+index+2,cent+oindh): " + (cent+index+2) +"," + (cent+oindh+index) );
                                }
                                linearet = linea.substring(0,cent+index+2)+" <%=model.perfil_vistaService.property(\"\",\""+campo+"\",pvpag, \""+Convertirfunciones(href)+" \")%>" + linea.substring(cent+index+2,cent+oindh+index) +  linea.substring(cent+oindh+oind1h+oind2h+2+index);
                                break;
                            }
                        }else{
                            linearet = linea;
                            break;
                        }
                        
                    }
                    int lwhl = lineawhile.indexOf(">");
                    lineawhile = lineawhile.substring(lwhl);
                    cent += lwhl;
                    //////System.out.println("cent----"+cent);
                    //////System.out.println("lineawhile al final="+lineawhile);
                    index=lineawhile.indexOf("<a");
                    //////System.out.println("");
                    cent += index;
                    if (index != -1){
                        lineawhile = lineawhile.substring(index);
                    }else{
                        break;
                    }
                    //////System.out.println("lineawhile--"+lineawhile);
                    //////System.out.println("lineawhilefinal final="+lineawhile+"    cent="+cent+"    index"+index);
                }
            }else{
                linearet = linea;
                //////System.out.println("lineawrite---------"+linearet);
            }
            if (linearet.equals("")){
                linearet = linea;
            }
            
        } catch ( Exception exc ){
            exc.printStackTrace();
            throw new SQLException(exc.getMessage());
        }
        //logger.info("LINEARET 3 : "+linearet);
        return linearet;
    }
    
    
    /**
     * Lee un String en formato Jsp y lo convierte a formato String.
     * @autor Leonardo Parody Ponce.
     * @param string
     * @throws SQLException
     * @return String con el formaro corregido.
     * @version 1.0
     */
    
    private String Convertirfunciones(String funcion){
        String function="";
        if ( !funcion.equals("") ) {
            for (int i=0; i < funcion.length(); i++) {
                if ((funcion.charAt(i)=='<')&&(funcion.charAt(i+1)=='%')){
                    function = funcion.substring(0,i)+"\"+"+funcion.substring(i+3);
                    funcion = function;
                    
                }else if ((funcion.charAt(i)=='%')&&(funcion.charAt(i+1)=='>')){
                    function = funcion.substring(0,i)+"+\""+funcion.substring(i+2);
                    funcion = function;
                    i++;
                }else if ((funcion.charAt(i)=='"')){
                    function = funcion.substring(0,i)+"\\"+funcion.substring(i);
                    funcion = function;
                    i++;
                }
                //////System.out.println("i---------"+i);
                //////System.out.println("function----"+function+"--------funcion----"+funcion);
                
            }
        }
        
        //org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
        //logger.info("convertir funcion (" + funcion + ") : " + function);
        
        return function;
    }
    
    /**
     * Establce si existe un determinado campo en la tabla
     * la p�gina JSP dada
     * @autor Ibg. Andr�s Maturana
     * @param jsp C�digo de la p�gina JSP
     * @param campo Nombre del campo
     * @throws SQLException
     * @version 1.0
     */
    public String existecampos_jsp(String jsp, String campo) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("select * from campos_jsp where pagina=? and campo=?");
                st.setString(1,jsp);
                st.setString(2,campo);
                rs = st.executeQuery();
                if (rs.next()){
                    return rs.getString("rec_status");
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL CODIGO DEL TIPO DE UBICACI�N " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        return null;
    }
}


