/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

/**
 *
 * @author Alvaro
 */


import java.sql.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
import com.tsp.util.LogWriter;
import com.tsp.util.Util;


public class FiduciaDAO extends MainDAO {


    /** Creates a new instance of ConstanteDAO */
    public FiduciaDAO() {
        super("FiduciaDAO.xml");
    }
    public FiduciaDAO(String dataBaseName) {
        super("FiduciaDAO.xml", dataBaseName);
    }

    private List listaFacturaNM =  new LinkedList();

    /**
     * Crea una lista de facturas NM que seran las que se pasaran a PM
     */
    public void getFacturaNM()throws SQLException{

        PreparedStatement st        = null;
        Connection        con       = null;
        ResultSet         rs        = null;
        
        listaFacturaNM.clear();


        String            query    = "SQL_GET_NM";


        try {

            con = this.conectarJNDI( query );

            
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();

            while (rs.next()){
                listaFacturaNM.add(NM.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR AL BUSCAR LAS NM PARA PASARLAS A PM. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

    }
    
    /**
     * Crea una lista de facturas PM que seran las que se pasaran a PM Fintra
     */
    public void getFacturaPM(String fiduciaria, String tipo)throws SQLException{

        PreparedStatement st        = null;
        Connection        con       = null;
        ResultSet         rs        = null;
        
        listaFacturaNM.clear();


        String query = "SQL_GET_PM";


        try {

            con = this.conectarJNDI( query );

            
            String  sql  = obtenerSQL( query );
            sql = sql.replace("#FIDUCIA#",((fiduciaria.equalsIgnoreCase("NaO"))
                    ?""
                    :"and a.clasificacion1 = '"+fiduciaria+"'"));
            sql = sql.replace("#TIPO#",tipo);
            st           = con.prepareStatement( sql );
            //st.setString (1, fiduciaria);
            rs = st.executeQuery();

            while (rs.next()){
                listaFacturaNM.add(NM.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR AL BUSCAR LAS PM PARA PASARLAS A PM Fintra. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

    }
    
    /**
     * Retorna una Lista de ofertas pendientes de facturar al cliente
     */
    public List getListaFacturaNM() {
        return listaFacturaNM;
    }
    

    



    /**
     * Forma un comando sql para insertar la cabecera en cxc
     * @FacturaCabeceraCxC Objeto con todos los campos de la cabecera
     */


    public String insertarFacturaCabeceraCxC(FacturaCabeceraCxC cabecera, LogWriter logWriter)throws SQLException{

        StringStatement st       = null;
        Connection        con      = null;

        String comando_sql  = "";

        String            query    = "SQL_INSERTA_FACTURA_CABECERA_CXC";

        try {
            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1,cabecera.getReg_status());
            st.setString(2,cabecera.getDstrct());
            st.setString(3,cabecera.getTipo_documento());
            st.setString(4,cabecera.getDocumento());
            st.setString(5,cabecera.getNit());
            st.setString(6,cabecera.getCodcli());
            st.setString(7,cabecera.getConcepto());
            st.setString(8,cabecera.getFecha_factura());
            st.setString(9,cabecera.getFecha_vencimiento());
            st.setString(10,cabecera.getFecha_ultimo_pago());
            st.setString(11,cabecera.getFecha_impresion());
            st.setString(12,cabecera.getDescripcion());
            st.setString(13,cabecera.getObservacion());
            st.setDouble(14,cabecera.getValor_factura());
            st.setDouble(15,cabecera.getValor_abono());
            st.setDouble(16,cabecera.getValor_saldo());
            st.setDouble(17,cabecera.getValor_facturame());
            st.setDouble(18,cabecera.getValor_abonome());
            st.setDouble(19,cabecera.getValor_saldome());
            st.setDouble(20,cabecera.getValor_tasa());
            st.setString(21,cabecera.getMoneda());
            st.setInt(22,cabecera.getCantidad_items());
            st.setString(23,cabecera.getForma_pago());
            st.setString(24,cabecera.getAgencia_facturacion());
            st.setString(25,cabecera.getAgencia_cobro());
            st.setString(26,cabecera.getZona());
            st.setString(27,cabecera.getClasificacion1());
            st.setString(28,cabecera.getClasificacion2());
            st.setString(29,cabecera.getClasificacion3());
            st.setInt(30,cabecera.getTransaccion());
            st.setInt(31,cabecera.getTransaccion_anulacion());
            st.setString(32,cabecera.getFecha_contabilizacion());
            st.setString(33,cabecera.getFecha_anulacion());
            st.setString(34,cabecera.getFecha_contabilizacion_anulacion());
            st.setString(35,cabecera.getBase());
            st.setString(36,cabecera.getLast_update());
            st.setString(37,cabecera.getUser_update());
            st.setString(38,cabecera.getCreation_date());
            st.setString(39,cabecera.getCreation_user());
            st.setString(40,cabecera.getFecha_probable_pago());
            st.setString(41,cabecera.getFlujo()); 
            st.setString(42,cabecera.getRif());
            st.setString(43,cabecera.getCmc());
            st.setString(44,cabecera.getUsuario_anulo());
            st.setString(45,cabecera.getFormato());
            st.setString(46,cabecera.getAgencia_impresion());
            st.setString(47,cabecera.getPeriodo());
            st.setDouble(48,cabecera.getValor_tasa_remesa());
            st.setString(49,cabecera.getNegasoc());
            st.setString(50,cabecera.getNum_doc_fen());
            st.setString(51,cabecera.getObs());
            st.setString(52,cabecera.getPagado_fenalco());
            st.setString(53,cabecera.getCorficolombiana());
            st.setString(54,cabecera.getTipo_ref1());
            st.setString(55,cabecera.getRef1());
            st.setString(56,cabecera.getTipo_ref2());
            st.setString(57,cabecera.getRef2());
            st.setString(58,cabecera.getDstrct_ultimo_ingreso());
            st.setString(59,cabecera.getTipo_documento_ultimo_ingreso());
            st.setString(60,cabecera.getNum_ingreso_ultimo_ingreso());
            st.setInt(61,cabecera.getItem_ultimo_ingreso());
            st.setString(62,cabecera.getFec_envio_fiducia());
            st.setString(63,cabecera.getNit_enviado_fiducia());
            st.setString(64,cabecera.getTipo_referencia_1());
            st.setString(65,cabecera.getReferencia_1());
            st.setString(66,cabecera.getTipo_referencia_2());
            st.setString(67,cabecera.getReferencia_2());
            st.setString(68,cabecera.getTipo_referencia_3());
            st.setString(69,cabecera.getReferencia_3());
            st.setString(70,cabecera.getNc_traslado());
            st.setString(71,cabecera.getFecha_nc_traslado());
            st.setString(72,cabecera.getTipo_nc());
            st.setString(73,cabecera.getNumero_nc());
            st.setString(74,cabecera.getFactura_traslado());
            st.setString(75,cabecera.getFactoring_formula_aplicada());
            st.setString(76,cabecera.getNit_endoso());
            st.setString(77,cabecera.getDevuelta());
            st.setString(78,cabecera.getFc_eca());
            st.setString(79,cabecera.getFc_bonificacion());
            st.setString(80,cabecera.getIndicador_bonificacion());
            st.setString(81,cabecera.getFi_bonificacion());
        
            
            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA insertarFacturaCabeceraCxC. \n " + e.getMessage());
        }
        finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return comando_sql;
    }

    
    
    

    



    /**
     * Forma un comando sql para insertar del detalle en cxc
     * @FacturaDetalleCxC Objeto con todos los campos del detalle
     */


    public String insertarFacturaDetalleCxC(FacturaDetalleCxC detalle, LogWriter logWriter)throws SQLException{

        StringStatement st       = null;
        Connection        con      = null;

        String comando_sql  = "";

        String            query    = "SQL_INSERTA_FACTURA_DETALLE_CXC";

        try {
            st = new StringStatement (this.obtenerSQL(query), true);
            
            st.setString(1,detalle.getReg_status());
            st.setString(2,detalle.getDstrct());
            st.setString(3,detalle.getTipo_documento());
            st.setString(4,detalle.getDocumento());
            st.setInt(5,detalle.getItem());
            st.setString(6,detalle.getNit());
            st.setString(7,detalle.getConcepto());
            st.setString(8,detalle.getNumero_remesa());
            st.setString(9,detalle.getDescripcion());
            st.setString(10,detalle.getCodigo_cuenta_contable());
            st.setDouble(11,detalle.getCantidad());
            st.setDouble(12,detalle.getValor_unitario());
            st.setDouble(13,detalle.getValor_unitariome());
            st.setDouble(14,detalle.getValor_item());
            st.setDouble(15,detalle.getValor_itemme());
            st.setDouble(16,detalle.getValor_tasa());
            st.setString(17,detalle.getMoneda());        
            st.setString(18,detalle.getLast_update());
            st.setString(19,detalle.getUser_update());
            st.setString(20,detalle.getCreation_date());
            st.setString(21,detalle.getCreation_user());
            st.setString(22,detalle.getBase());       
            st.setString(23,detalle.getAuxiliar());
            st.setDouble(24,detalle.getValor_ingreso());
            st.setString(25,detalle.getTipo_documento_rel());
            st.setInt(26,detalle.getTransaccion());
            st.setString(27,detalle.getDocumento_relacionado());
            st.setString(28,detalle.getTipo_referencia_1());
            st.setString(29,detalle.getReferencia_1());
            st.setString(30,detalle.getTipo_referencia_2());
            st.setString(31,detalle.getReferencia_2());
            st.setString(32,detalle.getTipo_referencia_3());
            st.setString(33,detalle.getReferencia_3());
        
            
            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA insertarFacturaDetalleCxC. \n " + e.getMessage());
        }
        finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return comando_sql;
    }

    
    



    /**
     * Forma un comando sql para insertar la cabecera de una factura PM en cxc
     * 
     */


    public String insertarFacturaPM(String distrito,  String documento, int numeroItem, String fiduciaSeleccionada, String tipoDocumento, String siglaFactura, String usuario, String fechaCreacion,
                                    String cmc , LogWriter logWriter)throws SQLException{

        StringStatement st       = null;
        Connection        con      = null;

        String comando_sql  = "";

        String            query    = "SQL_INSERTA_CABECERA_PM";

        try {
            st = new StringStatement (this.obtenerSQL(query), true);
            st.setString(1,siglaFactura);
            st.setInt(2,numeroItem);
            st.setString(3,fiduciaSeleccionada);
            st.setString(4,fechaCreacion);
            st.setString(5,usuario);
            st.setString(6,fechaCreacion);
            st.setString(7,usuario);
            st.setString(8,cmc);
            st.setString(9,distrito);
            st.setString(10,tipoDocumento);
            st.setString(11,documento);
            
            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA insertarFacturaPM. \n " + e.getMessage());
        }
        finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return comando_sql;
    }

    
    
    

    public List seleccionaConceptoNM(String distrito,  String documento, int numero_item, String descripcion_concepto, String condicion, String tipoDocumento, String siglaFactura, String usuario, String fechaCreacion,
                                        String cmc , String cuenta_contable, LogWriter logWriter)throws SQLException{

        PreparedStatement st  = null;
        Connection        con = null;
        ResultSet         rs  = null;
        List listaFacturaDetalleCxC= null;
        
        String     query    = "SQL_SELECCIONA_CONCEPTO_NM";
        FacturaDetalleCxC facturaDetalleCxC = null;

        try {

            con          = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );            
            sql          = sql.replaceAll("#CONDICION_DESCRIPCION#" , condicion);
            st           = con.prepareStatement( sql );
            
            st.setString(1,siglaFactura);
            st.setInt(2,numero_item);
            st.setString(3,descripcion_concepto);
            st.setString(4,cuenta_contable);
            st.setString(5,fechaCreacion);
            st.setString(6,usuario);
            st.setString(7,fechaCreacion);
            st.setString(8,usuario);
            st.setString(9,distrito);
            st.setString(10,tipoDocumento);    
            st.setString(11,distrito);
            st.setString(12,tipoDocumento); 
            st.setString(13,documento);
           
            rs = st.executeQuery();
            listaFacturaDetalleCxC =  new LinkedList();
            
            while (rs.next()){
                listaFacturaDetalleCxC.add(FacturaDetalleCxC.load(rs));
                
            }            

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA seleccionaConceptoNM \n " + e.getMessage());
        }
        finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listaFacturaDetalleCxC;
    }

        
    
    
    
    
    
    

    /**
     * Forma un comando sql para insertar del detalle de una factura PM en cxc
     * @FacturaDetalleCxC Objeto con todos los campos del detalle
     */


    public String insertarFacturaDetallePM(FacturaDetalleCxC itemDetalle, LogWriter logWriter)throws SQLException{

        StringStatement st       = null;
        Connection        con    = null;

        String comando_sql  = "";
        String  sql         = "";

        String            query    = "SQL_INSERTA_DETALLE_PM";

        sql = this.obtenerSQL(query);
        
        try {

            st = new StringStatement (this.obtenerSQL(query), true);
            
            st.setString(1,itemDetalle.getReg_status());
            st.setString(2,itemDetalle.getDstrct());
            st.setString(3,itemDetalle.getTipo_documento());
            st.setString(4,itemDetalle.getDocumento());
            st.setInt(5,itemDetalle.getItem());
            st.setString(6,itemDetalle.getNit());
            st.setString(7,itemDetalle.getConcepto());
            st.setString(8,itemDetalle.getNumero_remesa());
            st.setString(9,itemDetalle.getDescripcion());
            st.setString(10,itemDetalle.getCodigo_cuenta_contable());
            st.setDouble(11,itemDetalle.getCantidad());
            st.setDouble(12,itemDetalle.getValor_unitario());
            st.setDouble(13,itemDetalle.getValor_unitariome());
            st.setDouble(14,itemDetalle.getValor_item());
            st.setDouble(15,itemDetalle.getValor_itemme());
            st.setDouble(16,itemDetalle.getValor_tasa());
            st.setString(17,itemDetalle.getMoneda());
            st.setString(18,itemDetalle.getLast_update());
            st.setString(19,itemDetalle.getUser_update());
            st.setString(20,itemDetalle.getCreation_date());
            st.setString(21,itemDetalle.getCreation_user());
            st.setString(22,itemDetalle.getBase());
            st.setString(23,itemDetalle.getAuxiliar());
            st.setDouble(24,itemDetalle.getValor_ingreso());
            st.setString(25,itemDetalle.getTipo_documento_rel());
            st.setInt(26,itemDetalle.getTransaccion());
            st.setString(27,itemDetalle.getDocumento_relacionado());
            st.setString(28,itemDetalle.getTipo_referencia_1());
            st.setString(29,itemDetalle.getReferencia_1());
            st.setString(30,itemDetalle.getTipo_referencia_2());
            st.setString(31,itemDetalle.getReferencia_2());
            st.setString(32,itemDetalle.getTipo_referencia_3());
            st.setString(33,itemDetalle.getReferencia_3());            
           
            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA insertarFacturaDetallePM. \n " + e.getMessage());
        }
        finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return comando_sql;
    }

    
    
 
    public String insertarIngresoPM(String distrito,  String documento, String tipoDocumento, String tipoDocumentoIngreso, String usuario, String fechaCreacion,
                                    String cuentaIngreso, String cmc , LogWriter logWriter)throws SQLException{

        StringStatement st       = null;
        Connection        con      = null;

        String comando_sql  = "";

        String            query    = "SQL_INSERTA_INGRESO_PM";

        try {
            st = new StringStatement (this.obtenerSQL(query), true);
            
            st.setString(1,tipoDocumentoIngreso);
            st.setString(2,cuentaIngreso);
            st.setString(3,usuario);
            st.setString(4,fechaCreacion);
            st.setString(5,usuario);
            st.setString(6,fechaCreacion);
            st.setString(7,cmc);
            st.setString(8,distrito);
            st.setString(9,tipoDocumento);
            st.setString(10,distrito);
            st.setString(11,tipoDocumento); 
            st.setString(12,documento);      

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA insertarIngresoPM. \n " + e.getMessage());
        }
        finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return comando_sql;
    }

   
    
    
 
    public String insertarIngresoDetallePM(String distrito,  String documento, String tipoDocumento, String tipoDocumentoIngreso, String usuario, String fechaCreacion,
                                    String cuentaIngreso, LogWriter logWriter)throws SQLException{

        StringStatement st       = null;
        Connection        con      = null;

        String comando_sql  = "";

        String            query    = "SQL_INSERTA_INGRESO_DETALLE_PM";

        try {
            st = new StringStatement (this.obtenerSQL(query), true);
            
            st.setString(1,distrito);
            st.setString(2,tipoDocumentoIngreso);
            st.setString(3,tipoDocumento);
            st.setString(4,usuario);
            st.setString(5,fechaCreacion);
            st.setString(6,usuario);
            st.setString(7,fechaCreacion);
            
            st.setString(8,cuentaIngreso);
            st.setString(9,distrito);
            st.setString(10,tipoDocumento);
            st.setString(11,distrito);
            st.setString(12,tipoDocumento); 
            st.setString(13,documento); 

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA insertarIngresoPM. \n " + e.getMessage());
        }
        finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return comando_sql;
    }

   
    
    
    
 
    public String actualizarFacturaNM(String  distrito,  String documento, String tipoDocumento, String usuario, LogWriter logWriter)throws SQLException{

        StringStatement st       = null;
        Connection        con      = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZAR_FACURA_NM";

        try {
            String sql = this.obtenerSQL(query).replace("#PARAMETROS#", "valor_abono = valor_factura,"
                                                                        + "valor_saldo = 0.00,"
                                                                        + "valor_abonome = valor_factura,"
                                                                        + "valor_saldome = 0.00");
            st = new StringStatement (sql, true);
            
            st.setString(1,usuario);
            st.setString(2,distrito);
            st.setString(3,tipoDocumento);
            st.setString(4,distrito);
            st.setString(5,tipoDocumento); 
            st.setString(6,documento);             
            
            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA insertarIngresoPM. \n " + e.getMessage());
        }
        finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return comando_sql;
    }

   
    
        
        
    
 
    public String actualizarFacturaRefNM(String  distrito,  String documento, String tipoDocumento, String usuario, LogWriter logWriter)throws SQLException{

        StringStatement st       = null;
        Connection        con      = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZAR_REF_FACURA_NM";

        try {
            String sql = this.obtenerSQL(query);
            st = new StringStatement (sql, true);
            
            st.setString(1,distrito); 
            st.setString(2,tipoDocumento);
            st.setString(3,documento);
            st.setString(4,usuario);            
            
            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA insertarIngresoPM. \n " + e.getMessage());
        }
        finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return comando_sql;
    }  
    
    public String retornarFiducia(String distrito, String documento, String tipoDocumento, String usuario, LogWriter logWriter)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet       rs = null;
        String comando_sql  = "";

        String            query    = "RETORNAR_PM_A_FINTRA";

        try {
            String sql = this.obtenerSQL(query);
            con = this.conectarJNDI( query );
            st         = con.prepareStatement( sql );
                       
            st.setString(1,distrito); 
            st.setString(2,tipoDocumento);
            st.setString(3,documento);
            st.setString(4,usuario); 
            
            rs = st.executeQuery();

            while (rs.next()){
                comando_sql = rs.getString(1);
            }
            

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA insertarIngresoPM. \n " + e.getMessage());
        }
        finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return comando_sql;
    }  
    public String fintraaFiducia(String distrito, String documento, String tipoDocumento, String fiducia, String usuario)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet       rs = null;
        String comando_sql  = "";

        String            query    = "PM_FINTRA_A_FIDUCIA";

        try {
            String sql = this.obtenerSQL(query);
            con = this.conectarJNDI( query );
            st         = con.prepareStatement( sql );
                       
            st.setString(1,distrito); 
            st.setString(2,tipoDocumento);
            st.setString(3,documento);
            st.setString(4,fiducia); 
            st.setString(5,usuario); 
            
            rs = st.executeQuery();

            while (rs.next()){
                comando_sql = rs.getString(1);
            }
            

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA insertarIngresoPM. \n " + e.getMessage());
        }
        finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return comando_sql;
    }  
}
    
    
    
        
