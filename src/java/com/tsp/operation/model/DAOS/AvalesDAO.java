/*
 * AvalesDAO.java
 * Created on 1 de mayo de 2009, 15:34
 */

package com.tsp.operation.model.DAOS;
import java.sql.*;
import java.util.ArrayList;
import com.tsp.operation.model.beans.Aval;

// @author  Fintra
public class AvalesDAO  extends MainDAO {
    ArrayList listAvales;
    public AvalesDAO() {
        super("AvalesDAO.xml");
    }
    public AvalesDAO(String dataBaseName) {
        super("AvalesDAO.xml", dataBaseName);
    }
    
   
    
    public ArrayList searchAvales(String fechaIni, String fechaFin,String numAval) throws Exception{ 
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_AVALES";
        listAvales=new ArrayList();
        
        String whereremix="";
        
        boolean respuesta = false;
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
                String sql = this.obtenerSQL(query);
                if (numAval == null || (numAval.equals(""))) {
                    whereremix = " AND creation_date BETWEEN '" + fechaIni + "' AND '" + fechaFin + " 23:59:59' ";
                } else {
                    whereremix = " AND no_aval= '" + numAval + "' ";
                }
                sql = sql.replaceAll("whereremix", whereremix);
                st = con.prepareStatement(sql);
                rs = st.executeQuery();

                while (rs.next()) {
                    Aval aval = new Aval();
                    aval.Load(rs);
                    listAvales.add(aval);
                }
           
            }}catch(Exception e){
            System.out.println("error en searchAvales..."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return listAvales;
        
    }
    
    public ArrayList getAvales() throws Exception
    {
        return listAvales;
    }



/**
 * 
 * @param avales
 * @param userx
 * @return
 * @throws Exception
 */
    public String aprobarAvales(String[] avales,String userx) throws Exception
    {
        String respuesta = "mal";
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_APROBAR_AVALES";
        String nums = "";
        try {
            for (int i = 0; i < avales.length; i++) {
                nums = nums + "'" + avales[i] + "',";
            }
            nums = nums.substring(0, nums.length() - 1);
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
                String sql = this.obtenerSQL(query);
                sql = sql.replaceAll("numsremix", nums);
                st = con.prepareStatement(sql);
                st.setString(1, userx);
                int affected = st.executeUpdate();
            }
        }catch(Exception e){
            System.out.println("error en aprobar avales..."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;        
    }
    
}
