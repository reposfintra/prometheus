 /*
 * Nombre        PerfilDAO.java
 * Autor         Ing. Sandra M. Escalante G.
 * Fecha         10 de marzo de 2005, 03:48 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;

/**
 *
 * @author  Administrador
 */
public class PerfilDAO extends MainDAO{
    
   /** Creates a new instance of PerfilDAO */
    public PerfilDAO() {
        super("PerfilDAO.xml", "fintra");
    }
       
    private Perfil perfil;
    private PerfilOpcion po;
    private List lperfiles;
    private Vector vperfiles;
    private PerfilOpcion pov;
    private Vector opsactxprf;
    
    private String S_PINSERT = "INSERT INTO perfil_trf (id_perfil, " +
    "                                                   nombre, " +
    "                                                   rec_status, " +
    "                                                   last_update, " +
    "                                                   user_update, " +
    "                                                   creation_date, " +
    "                                                   creation_user) " +
    "                           VALUES(?,?,'','now()',?,'now()',?)";
    
    private String S_PACTIVAR = "UPDATE perfil_trf " +
    "                           SET     nombre=?, " +
    "                                   user_update=?, " +
    "                                   last_update='now()', " +
    "                                   rec_status='' " +
    "                           WHERE id_perfil=?";
    
    private String S_POBTENER = "SELECT * " +
    "                           FROM    perfil_trf " +
    "                           WHERE   id_perfil = ? " +
    "                                   AND rec_status='' " +
    "                           ORDER BY id_perfil";
    
    private String S_PANULAR = "UPDATE perfil_trf " +
    "                           SET rec_status='A', " +
    "                               user_update= ?, " +
    "                               last_update='now()' " +
    "                           WHERE id_perfil= ?";
    
    private String S_PUPDATE = "UPDATE  perfil_trf " +
    "                           SET     nombre=?, " +
    "                                   user_update=?, " +
    "                                   last_update='now()'" +
    "                           WHERE   id_perfil=?";
    
    private String S_EPP = "SELECT  * " +
    "                       FROM    perfil_trf " +
    "                       WHERE   id_perfil = ? " +
    "                               AND rec_status=''";
    
    private String S_PXDETALLE = "  SELECT  *  " +
    "                               FROM    perfil_trf " +
    "                               WHERE   id_perfil like ? " +
    "                                       AND nombre like ? " +
    "                                       AND rec_status = '' " +
    "                               ORDER BY id_perfil";
    
    private String S_PXP = "SELECT  id_perfil, " +
    "                               nombre " +
    "                       FROM    perfil_trf " +
    "                       WHERE   rec_status ='' " +
    "                       ORDER BY id_perfil";
    
    private String S_POBUSCAR = "SELECT id_perfil " +
    "                           FROM    perfilopcion_trf " +
    "                           WHERE   id_perfil=? " +
    "                                   AND id_opcion=? " +
    "                                   AND rec_status=''";
    
    private String S_POBUSCAR1 ="SELECT id_opcion " +
    "                           FROM    perfilopcion_trf " +
    "                           WHERE   id_perfil=? " +
    "                                   AND rec_status='A'";
    
    private String S_POBUSCAR2 =    "SELECT id_opcion " +
    "                               FROM    perfilopcion_trf " +
    "                               WHERE   id_perfil=? " +
    "                                       AND rec_status=''";
    
    private String S_POBUSCAR3 =    "SELECT id_opcion, " +
    "                                       id_perfil " +
    "                               FROM    perfilopcion_trf " +
    "                               WHERE   id_opcion=? " +
    "                                       AND rec_status=''";
    
    private String S_POINSERT =     "INSERT INTO perfilopcion_trf " +
    "                               VALUES (?,?,'', 'now()',?,'now()',?)";
    
    private String S_POESTA =   "SELECT id_opcion " +
    "                           FROM    perfilopcion_trf " +
    "                           WHERE   id_opcion = ? " +
    "                                   AND id_perfil = ?";
    
    private String S_POESTA2 =  "SELECT id_opcion, " +
    "                                   id_perfil " +
    "                           FROM    perfilopcion_trf " +
    "                           WHERE   id_opcion = ? " +
    "                                   AND id_perfil = ? " +
    "                                   AND rec_status=''";
    
    private String S_PEA =  "SELECT * " +
    "                       FROM    perfil_trf " +
    "                       WHERE   id_perfil = ? " +
    "                               AND rec_status='A'";
    
    private String S_POACTIVAR =    "UPDATE perfilopcion_trf " +
    "                               SET     rec_status='', " +
    "                                       user_update= ?, " +
    "                                       last_update='now()' " +
    "                               WHERE   id_opcion=? " +
    "                                       AND id_perfil= ?";
    
    private String S_POANULAR = "UPDATE perfilopcion_trf " +
    "                           SET     rec_status='A', " +
    "                                   user_update= ?, " +
    "                                   last_update='now()' " +
    "                           WHERE   id_opcion=? " +
    "                                   AND id_perfil= ?";
    
    private String S_POEOPCIONA =   "SELECT * " +
    "                               FROM    perfilopcion_trf " +
    "                               WHERE   id_opcion = ? " +
    "                                       AND id_perfil = ? " +
    "                                       AND rec_status='A'";
    
    
    
    private static String S_OPACTXPERFIL = "SELECT  po.id_perfil, " +
    "                                               po.id_opcion " +
    "                                       FROM    menu_dinamico m " +
    "                                               JOIN  perfilopcion_trf po ON (po.id_opcion = m.id_opcion) " +
    "                                       WHERE   id_perfil = ?" +
    "                                               AND m.submenu_programa = '2'" +
    "                                               AND m.rec_status != 'A' " +
    "                                               AND po.rec_status != 'A'" ;
    private String SQL_PERFILES_USUARIO =   "SELECT p.perfil, t.nombre  " +
    "                                        FROM perfilusuarios p INNER JOIN perfil_trf t ON ( p.perfil = t.id_perfil ) " +
    "                                        WHERE   upper(p.usuarios) = upper(?)   " +
    "                                                AND p.status != 'A' " +
    "                                                AND t.rec_status != 'A' " +
    "                                        ORDER BY t.nombre";
    /**
     * Setter for property perfil.
     * @param opsactxprf New value of property perfil.
     */
    public void setPerfil(Perfil perfil){
        this.perfil = perfil;
    }
    
    /**
     * Getter for property perfil.
     * @return Value of property perfil.
     */
    public Perfil getPerfil() throws SQLException{
        return perfil;
    }
    
    /**
     * Metodo <tt>agregarPerfil()</tt>, registra un nuevo perfil en el sistema
     * @autor : Ing. Sandra Escalante
     * @version : 1.0
     */
    public void agregarPerfil()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "S_PINSERT";//JJCastro fase2
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, perfil.getId());
                st.setString(2, perfil.getNombre());
                st.setString(3, perfil.getCreado_por());
                st.setString(4, perfil.getCreado_por());
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>agregarPerfilOpcion</tt>, registra la relacion de una opcion con un perfil especifico
     * @autor : Ing. Sandra Escalante
     * @param: idperfil (id del perfil), idopcion (id de la opcion), us (usuario en sesion)
     * @version : 1.0
     */
    public void agregarPerfilOpcion(String idperfil, int idopcion, String us)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        PreparedStatement st1 = null;
        ResultSet rs = null;
        String query = "S_POBUSCAR";
        String query2 ="S_POINSERT" ;

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString( 1, idperfil);
                st.setInt( 2, idopcion);
                rs = st.executeQuery();
                
                if( !rs.next() ){
                    st1 = con.prepareStatement(this.obtenerSQL(query2));//JJCastro fase2
                    st1.setString( 1, idperfil);
                    st1.setInt( 2, idopcion);
                    st1.setString( 3, us);
                    st1.setString( 4, us);
                    st1.executeUpdate();
                    st1.close();
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DEL PERFIL-OPCION " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>anularPerfil()</tt>, anula un perfil de la tabla
     * @autor : Ing. Sandra Escalante
     * @version : 1.0
     */
    public void anularPerfil()throws SQLException{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "S_PANULAR";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, perfil.getUser_update());
                st.setString(2, perfil.getId());
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>anularPerfilopcion()</tt>, anula un registro perfil-opcion de la tabla
     * @autor : Ing. Sandra Escalante
     * @param uu (usuario en sesion), idprf (id del perfil), idop (id de la opcion)
     * @version : 1.0
     */
    public void anularPerfilOpcion(String uu, String idprf, int idop)throws SQLException{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "S_POANULAR";
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, uu);
                st.setInt(2, idop);
                st.setString(3, idprf);
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DEL PERFIL-OPCION " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>updatePerfil()</tt>, actualiza los datos de un perfil en la tabla
     * @autor : Ing. Sandra Escalante     
     * @version : 1.0
     */
    public void updatePerfil()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "S_PUPDATE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, perfil.getNombre());
                st.setString(2, perfil.getUser_update());
                st.setString(3, perfil.getId());
                
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>existePerfilxPropietario()</tt>, verifica la existencia de un perfil en la tabla
     * @autor : Ing. Sandra Escalante
     * @param idp (id del perfil)
     * @return boolean
     * @version : 1.0
     */
    public boolean existePerfilxPropietario( String idp)throws SQLException{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "S_EPP";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, idp);                
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PERFIL (EXISTEPERFILXPROPIETARIO) " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
    
    /**
     * Metodo <tt>existePerfilxPropietarioAnulado()</tt>, verifica la existencia de un perfil anulado en la tabla
     * @autor : Ing. Sandra Escalante
     * @param idp (id del perfil)
     * @return boolean
     * @version : 1.0
     */
    public boolean existePerfilxPropietarioAnulado( String idp )throws SQLException{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "S_PEA";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, idp);
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PERFIL ANULADO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
    
    /**
     * Metodo <tt>existePerfilOpcionAnulado()</tt>, verifica la existencia de la relacion
     *  perfil-opcion anulada en la tabla
     * @autor : Ing. Sandra Escalante
     * @param idprf (id del perfil), idop (id de la opcion)
     * @return boolean
     * @version : 1.0
     */
    public boolean existePerfilOpcionAnulado( String idprf, int idop)throws SQLException{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "S_POEOPCIONA";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setInt(1, idop);
                st.setString(2, idprf);
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PERFIL-OPCION ANULADO" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
    
    /**
     * Metodo <tt>estaPerfilopcion()</tt>, verifica la existencia de la relacion perfil-opcion en la tabla
     * @autor : Ing. Sandra Escalante
     * @param idprf (id del perfil), idop (id de la opcion)
     * @return boolean
     * @version : 1.0
     */
    public boolean estaPerfilOpcion( String idprf, int idop)throws SQLException{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "S_POESTA";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setInt(1, idop);
                st.setString(2, idprf);
                rs = st.executeQuery();
                if(rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PERFIL-OPCION " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
    
    /**
     * Metodo <tt>activarPerfil</tt>, activa un perfil (rec-status='') que habia sido anulado anteriormente 
     * @autor : Ing. Sandra Escalante     
     * @version : 1.0
     */
    public void activarPerfil()throws SQLException{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "S_PACTIVAR";
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, perfil.getNombre());
                st.setString(2, perfil.getUser_update());
                st.setString(3, perfil.getId());
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTIVACION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>activarPerfilOpcion()</tt>, activa una relacion perfil-opcion (rec_status='') anulada con anterioridad
     * @autor : Ing. Sandra Escalante
     * @param uu (usuario en sesion), idprf (id del perfil), idop (id de la opcion)
     * @version : 1.0
     */
    public void activarPerfilOpcion(String uu, String idprf, int idop)throws SQLException{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "S_POACTIVAR";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, uu);
                st.setInt(2, idop);
                st.setString(3, idprf);
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTIVACION DEL PERFIL_OPCION " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>buscarPerfilesxPropietarioenDetalle</tt>, obtiene los perfiles dado unos parametros de busqueda
     * @autor : Ing. Sandra Escalante
     * @param idp (id del perfil), nom (nombre del perfil)
     * @version : 1.0
     */
    public void buscarPerfilesxPropietarioenDetalle(String idp, String nom) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        vperfiles = null;
        String query = "S_PXDETALLE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, idp+"%");
                st.setString(2, nom+"%");
                
                rs = st.executeQuery();
                vperfiles = new Vector();
                
                while(rs.next()){
                    perfil = new Perfil();
                    perfil.setId(rs.getString("id_perfil"));
                    perfil.setNombre(rs.getString("nombre"));
                    vperfiles.add(perfil);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS PERFILES EN DETALLE " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>obtenerPerfilxPropietario()</tt>, obtiene un perfil dado un codigo
     * @autor : Ing. Sandra Escalante
     * @param idp (codigo del perfil)
     * @version : 1.0
     */
    public void obtenerPerfilxPropietario( String idp )throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "S_POBTENER";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, idp);
                rs = st.executeQuery();
                if(rs.next()){
                    perfil = Perfil.load(rs);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
   /**
     * Getter for property vperfiles.
     * @return Value of property vperfiles.
     */
    public Vector getVPerfil(){
        return vperfiles;
    }
    
    /**
     * Metodo <tt>listarPerfilesxpropietario</tt>, obtiene los perfiles del sistema
     * @autor : Ing. Sandra Escalante
     * @version : 1.0
     */    
    public void listarPerfilesxPropietario( )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "S_PXP";
        
        try {
                con = this.conectarJNDI(query);
                if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                vperfiles = new Vector();
                
                while(rs.next()){
                    perfil = new Perfil();
                    perfil.setId(rs.getString("id_perfil"));
                    perfil.setNombre(rs.getString("nombre"));
                    
                    vperfiles.add(perfil);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LOS PERFILES " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>listarPOxPerfil</tt>, obtiene los regitros perfil-opcion anulados dado un perfil
     * @autor : Ing. Sandra Escalante
     * @param perfil (id del perfil)
     * @version : 1.0
     */
    public void listarPOxPerfil( String perfil)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "S_POBUSCAR1";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, perfil);                
                rs = st.executeQuery();
                vperfiles = new Vector();
                
                while(rs.next()){
                    po = new PerfilOpcion();
                    po.setId_opcion(rs.getInt("id_opcion"));
                    vperfiles.add(po);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS PERFILOPCION " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>listarPerfilOpcion</tt>, obtiene los regitros perfil-opcion dado un perfil
     * @autor : Ing. Sandra Escalante
     * @param perfil (id del perfil)
     * @version : 1.0
     */
    public void listarPerfilOpcion( String perfil )throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "S_POBUSCAR2";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, perfil);
                rs = st.executeQuery();
                vperfiles = new Vector();
                
                while(rs.next()){
                    po = new PerfilOpcion();
                    po.setId_opcion(rs.getInt("id_opcion"));
                    vperfiles.add(po);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS PERFILOPCION " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>estaPOActivo</tt>, verifica si un registro perfil-opcion se encuentra activo (rec_status != A)
     * @autor : Ing. Sandra Escalante
     * @param idprf (id del perfil), idop (id de la opcion)
     * @return boolean
     * @version : 1.0
     */
    public boolean estaPOActivo( String idprf, int idop)throws SQLException{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        po = new PerfilOpcion();
        String query = "S_POESTA2";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setInt(1, idop);
                st.setString(2, idprf);
                rs = st.executeQuery();
                if(rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
    
    /**
     * Metodo <tt>listarPOxOpcion</tt>, obtiene los registros perfil-opcion donde la opcion sea la dada
     * @autor : Ing. Sandra Escalante
     * @param opcion (id de la opcion)
     * @version : 1.0
     */
    public void listarPOxOpcion( int opcion )throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "S_POBUSCAR3";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setInt(1, opcion);
                rs = st.executeQuery();
                vperfiles = new Vector();
                while(rs.next()){
                    po = new PerfilOpcion();
                    po.setId_opcion(rs.getInt("id_opcion"));
                    po.setId_perfil(rs.getString("id_perfil"));
                    vperfiles.add(po);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS PERFILOPCION POR OPCION " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>opcionesActivasxPerfil</tt>, obtiene una lista de las opciones activas
     *  de una perfil (rec_status='')
     * @autor : Ing. Sandra M Escalante G
     * @param : perfil (id del perfil)
     * @version : 1.0
     */
    public void opcionesActivasxPerfil(String prf) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "S_OPACTXPERFIL";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, prf);
                rs = st.executeQuery();
                opsactxprf = new Vector();
                while(rs.next()){
                    PerfilOpcion o = new PerfilOpcion();
                    o.setId_opcion(rs.getInt("id_opcion"));
                    o.setEstado("A");
                    opsactxprf.add(o);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE OPCIONES ACTIVAS POR PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Getter for property opsactxprf.
     * @return Value of property opsactxprf.
     */
    public java.util.Vector getOpsactxprf() {
        return opsactxprf;
    }
    
    /**
     * Setter for property opsactxprf.
     * @param opsactxprf New value of property opsactxprf.
     */
    public void setOpsactxprf(java.util.Vector opsactxprf) {
        this.opsactxprf = opsactxprf;
    }
    /**
     * M�todo que obtiene los perfiles de un usuario dado
     * @autor David Pi�a Lopez
     * @throws SQLException
     * @version 1.0
     * @param idusuario El login del usuario
     */
    public void getPerfilesUsuario(String idusuario)throws SQLException{
        //System.out.println("getPerfilesUsuario");
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_PERFILES_USUARIO";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString( 1, idusuario );
                rs = st.executeQuery();
                vperfiles = new Vector();                
                while(rs.next()){
                    perfil = new Perfil();
                    perfil.setId(rs.getString("perfil"));
                    perfil.setNombre(rs.getString("nombre"));                    
                    vperfiles.add( perfil );
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    public void getPerfilesUsuario(String idusuario, String distrito, String proyecto)throws SQLException{
        //System.out.println("getPerfilesUsuario");
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_PERFILES_USUARIO_PROYECTO";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString( 1, distrito );
                st.setString( 2, proyecto );
                st.setString( 3, idusuario );
                rs = st.executeQuery();
                vperfiles = new Vector();                
                while(rs.next()){
                    perfil = new Perfil();
                    perfil.setId(rs.getString("perfil"));
                    perfil.setNombre(rs.getString("nombre"));                    
                    vperfiles.add( perfil );
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    public void setVPerfil( Vector p){
        vperfiles = p;
    }
    public void getCompania(String usuario)throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_COMPANIAS_USUARIO";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString( 1, usuario );
                rs = st.executeQuery();
                vperfiles = new Vector();                
                while(rs.next()){
                    perfil = new Perfil();
                    perfil.setId(rs.getString("distrito"));
                    perfil.setNombre(rs.getString("nombre")); 
                    perfil.setBase(rs.getString("proyecto"));
                    vperfiles.add( perfil );
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
    }
}//cierra clase