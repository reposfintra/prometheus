/***************************************
* Nombre Clase ............. PrestamoDAO.java
* Descripci�n  .. . . . . .  Ofrece los sql para los prestamos
* Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
* Fecha . . . . . . . . . .  09/02/2006
* versi�n . . . . . . . . .  1.0
* Copyright ...Transportes Sanchez Polo S.A.
*******************************************/



package com.tsp.operation.model.DAOS;


import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;


public class ResumenPrestamoDAO extends MainDAO{
    
   
    
    
    
    public ResumenPrestamoDAO() {
         super("ResumenPrestamoDAO.xml");
    }
    
    
    
    public String reset(String val){
        if(val==null)
           val="";
        return val;
    }
    
    
    
     /**
     * M�todo que busca el acumulado de los prestamos para un tercero de un distrito
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... String distrito, String tercero
     * @version..... 1.0.
     **/
     public List getAcumuladoPrestamos (String distrito, String tercero, String clasificacion)throws Exception{
         Connection con = null;
         PreparedStatement st          = null;
        ResultSet         rs          = null;
        List              lista       = new LinkedList();
        String query = "SQL_ACUM_PRESTAMOS_BENEFICIARIOS";
        try{
            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,  distrito       );
            st.setString(2,  tercero        );
            st.setString(3,  clasificacion  );
            rs = st.executeQuery();
            while(rs.next()){
                ResumenPrestamo rp = new ResumenPrestamo();   
                   rp.setDistrito          (   distrito );
                   rp.setTercero           (   tercero  );
                   rp.setNit               (   reset(rs.getString("nit")   ));
                   rp.setNombre            (   reset(rs.getString("nombre")));
                   rp.setValor             (   rs.getDouble("capital")   );
                   rp.setIntereses         (   rs.getDouble("intereses") ); 
                   rp.setValorDesc         (   rs.getDouble("capdesc")   );
                   rp.setInteresesDesc     (   rs.getDouble("intdesc")   );
                   rp.setVlrMigradoMims    (   rs.getDouble("migmims")   );
                   rp.setVlrRegistradoMims (   rs.getDouble("regmims")   );
                   rp.setVlrDescontadoProp (   rs.getDouble("descprop")  );
                   rp.setVlrPagadoFintra   (   rs.getDouble("pagfintra") );
                lista.add(rp);
            }
            
        }}catch(Exception e) {
             throw new SQLException(" getAcumuladoPrestamos  -> " +e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }     
     
     
     
     
     
    
}
