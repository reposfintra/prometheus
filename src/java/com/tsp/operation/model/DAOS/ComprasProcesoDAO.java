/*
    Document   : Modulo de Compras y Servicios.
    Created on : 10/02/2017, 11:00:00 AM
    Author     : hcuello
*/

package com.tsp.operation.model.DAOS;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.TransaccionService;

import com.tsp.operation.model.beans.ComprasProcesoBeans; //
import com.tsp.operation.model.beans.Usuario;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Statement;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.CallableStatement;
import javax.swing.JOptionPane;

public class ComprasProcesoDAO extends MainDAO {

    public ComprasProcesoDAO(String dataBaseName) {
       super("ComprasProcesoDAO.xml",dataBaseName);
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------------- 
    public ArrayList<ComprasProcesoBeans> GetComprasProcesoListado(String Query, String NmLogin, String id_sol, String accione, String codSol) throws Exception {
        
        ArrayList ListadoReqs = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            String sql = this.obtenerSQL(Query);
            /*
            if ( !codSol.equals("") ) {
                sql = sql.replaceAll("#ESTADO#", " b.id_solicitud = " + TipoSolicitud);
            }else{
                sql = sql.replaceAll("#ESTADO#", "");
            }    

            if ( !Proyecto.equals("") ) {
                sql = sql.replaceAll("#PROCESO#", " AND and tp.id = " + Proyecto);
            }else{
                sql = sql.replaceAll("#PROCESO#", "");
            }*/

            con = conectarJNDI(Query);
            ps = con.prepareStatement(sql);
            
            ps.setString(1, id_sol);
            ps.setString(2, NmLogin);
            ps.setString(3, accione);
            ps.setString(4, codSol);
            
            System.out.println(ps);
                
            ListadoReqs = new ArrayList();
            
            rs = ps.executeQuery();
            while (rs.next()) {
                
                ComprasProcesoBeans LstReq = new ComprasProcesoBeans();
                
                LstReq.setFiltroInsumo(rs.getString("filtro_insumo"));
                LstReq.setInsumoAdicional(rs.getString("insumo_adicional"));
                LstReq.setResponsable(rs.getString("responsable"));
                LstReq.setIdSolicitud(rs.getString("id_solicitud"));
                
                LstReq.setTipoInsumo(rs.getString("tipo_insumo"));
                LstReq.setCodigoMaterial(rs.getString("codigo_insumo"));
                LstReq.setDescripcionInsumo(rs.getString("descripcion_insumo"));
                LstReq.setNombreUnidadInsumo(rs.getString("nombre_unidad_insumo"));

                LstReq.setInsumosTotal(rs.getString("insumos_total"));
                LstReq.setInsumosSolicitados(rs.getString("insumos_solicitados"));
                LstReq.setInsumosDisponibles(rs.getString("insumos_disponibles"));
                LstReq.setSolicitadoTemporal(rs.getString("solicitado_temporal"));
                LstReq.setObservacion_material(rs.getString("observacion_xinsumo"));
                LstReq.setReferenciaExterna(rs.getString("referencia_externa"));
                
                ListadoReqs.add(LstReq);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        
        return ListadoReqs;
    }
    
    public ArrayList<ComprasProcesoBeans> GetInsumosOCS(String Query, String NmLogin) throws Exception {
        
        ArrayList ListadoReqs = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            String sql = this.obtenerSQL(Query);
            /*
            if ( !TipoSolicitud.equals("") ) {
                sql = sql.replaceAll("#ESTADO#", " b.id_solicitud = " + TipoSolicitud);
            }else{
                sql = sql.replaceAll("#ESTADO#", "");
            }    

            if ( !Proyecto.equals("") ) {
                sql = sql.replaceAll("#PROCESO#", " AND and tp.id = " + Proyecto);
            }else{
                sql = sql.replaceAll("#PROCESO#", "");
            }*/

            con = conectarJNDI(Query);
            ps = con.prepareStatement(sql);
            
            ps.setString(1, NmLogin);
            
            System.out.println(ps);
                
            ListadoReqs = new ArrayList();
            
            rs = ps.executeQuery();
            while (rs.next()) {
                
                ComprasProcesoBeans LstReq = new ComprasProcesoBeans();
                
                LstReq.setInsumoAdicional(rs.getString("insumo_adicional"));
                LstReq.setResponsable(rs.getString("responsable"));
                LstReq.setCodSolicitud(rs.getString("cod_solicitud"));
                
                LstReq.setTipoInsumo(rs.getString("tipo_insumo"));
                LstReq.setCodigoMaterial(rs.getString("codigo_insumo"));
                LstReq.setDescripcionInsumo(rs.getString("descripcion_insumo"));
                LstReq.setNombreUnidadInsumo(rs.getString("nombre_unidad_medida"));
                LstReq.setReferenciaExterna(rs.getString("referencia_externa"));

                LstReq.setCantidadTotal(rs.getString("cantidad_total"));
                LstReq.setCantidadSolicitada(rs.getString("cantidad_solicitada"));
                LstReq.setCantidadDisponible(rs.getString("cantidad_disponible"));
                LstReq.setSolicitadoTemporal(rs.getString("cantidad_temporal"));
                LstReq.setCostoPresupuesto(rs.getString("costo_presupuestado"));
                
                ListadoReqs.add(LstReq);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        
        return ListadoReqs;
    }


    public ArrayList<ComprasProcesoBeans> GetInsumosxApu(String Query, String NmLogin, String IdSolicitud, String CodInsumo) throws Exception {
        
        ArrayList ListadoReqs = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            String sql = this.obtenerSQL(Query);

            con = conectarJNDI(Query);
            ps = con.prepareStatement(sql);
            
            ps.setString(1, NmLogin);
            ps.setString(2, IdSolicitud);
            ps.setString(3, CodInsumo);
            
            System.out.println(ps);
                
            ListadoReqs = new ArrayList();
            
            rs = ps.executeQuery();
            while (rs.next()) {
                
                ComprasProcesoBeans LstReq = new ComprasProcesoBeans();
                 
                LstReq.setCodigoMaterial(rs.getString("codigo_insumo"));
                LstReq.setDescripcionInsumo(rs.getString("descripcion_insumo"));
                LstReq.setNombreApu(rs.getString("nombre_apu"));

                LstReq.setCantidadTotal(rs.getString("insumos_total"));
                LstReq.setCantidadSolicitada(rs.getString("insumos_solicitados"));
                LstReq.setCantidadDisponible(rs.getString("insumos_disponibles"));
                LstReq.setSolicitadoTemporal(rs.getString("solicitado_temporal"));
                
                ListadoReqs.add(LstReq);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        
        return ListadoReqs;
    }

    public ArrayList<ComprasProcesoBeans> GetInsumosCatalogo(String Query) throws Exception {
        
        ArrayList ListadoReqs = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            String sql = this.obtenerSQL(Query);

            con = conectarJNDI(Query);
            ps = con.prepareStatement(sql);
            
            System.out.println(ps);
                
            ListadoReqs = new ArrayList();
            
            rs = ps.executeQuery();
            while (rs.next()) {
                
                ComprasProcesoBeans LstReq = new ComprasProcesoBeans();
                
                LstReq.setTipoInsumo(rs.getString("tipo_insumo"));
                LstReq.setCodigoMaterial(rs.getString("codigo_insumo"));
                LstReq.setDescripcionInsumo(rs.getString("descripcion_insumo"));
                //LstReq.setNombreUnidadInsumo(rs.getString("nombre_unidad_insumo"));

                ListadoReqs.add(LstReq);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        
        return ListadoReqs;
    }
    
    public ArrayList<ComprasProcesoBeans> GetInsumosOCSEdit(String Query, String NmLogin, String OdnCmpra) throws Exception {
        
        ArrayList ListadoReqs = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            String sql = this.obtenerSQL(Query);
            /*
            if ( !TipoSolicitud.equals("") ) {
                sql = sql.replaceAll("#ESTADO#", " b.id_solicitud = " + TipoSolicitud);
            }else{
                sql = sql.replaceAll("#ESTADO#", "");
            }    

            if ( !Proyecto.equals("") ) {
                sql = sql.replaceAll("#PROCESO#", " AND and tp.id = " + Proyecto);
            }else{
                sql = sql.replaceAll("#PROCESO#", "");
            }*/

            con = conectarJNDI(Query);
            ps = con.prepareStatement(sql);
            
            ps.setString(1, NmLogin);
            ps.setString(2, OdnCmpra);
            
            System.out.println(ps);
                
            ListadoReqs = new ArrayList();
            
            rs = ps.executeQuery();
            while (rs.next()) {
                
                ComprasProcesoBeans LstReq = new ComprasProcesoBeans();
                
                LstReq.setInsumoAdicional(rs.getString("insumo_adicional"));
                LstReq.setResponsable(rs.getString("responsable"));
                LstReq.setCodSolicitud(rs.getString("cod_solicitud"));
                
                LstReq.setTipoInsumo(rs.getString("tipo_insumo"));
                LstReq.setCodigoMaterial(rs.getString("codigo_insumo"));
                LstReq.setDescripcionInsumo(rs.getString("descripcion_insumo"));
                LstReq.setNombreUnidadInsumo(rs.getString("nombre_unidad_medida"));
                LstReq.setReferenciaExterna(rs.getString("referencia_externa"));

                LstReq.setCantidadTotal(rs.getString("cantidad_total"));
                LstReq.setCantidadSolicitada(rs.getString("cantidad_solicitada"));
                LstReq.setCantidadDisponible(rs.getString("cantidad_disponible"));
                LstReq.setSolicitadoTemporal(rs.getString("cantidad_temporal"));
                LstReq.setCostoPresupuesto(rs.getString("costo_presupuestado"));
                
                ListadoReqs.add(LstReq);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        
        return ListadoReqs;
    }

    public ArrayList<ComprasProcesoBeans> GetInsumosDespacho(String Query, String NmLogin, String OdnCmpra) throws Exception {
        
        ArrayList ListadoReqs = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            String sql = this.obtenerSQL(Query);
            /*
            if ( !TipoSolicitud.equals("") ) {
                sql = sql.replaceAll("#ESTADO#", " b.id_solicitud = " + TipoSolicitud);
            }else{
                sql = sql.replaceAll("#ESTADO#", "");
            }    

            if ( !Proyecto.equals("") ) {
                sql = sql.replaceAll("#PROCESO#", " AND and tp.id = " + Proyecto);
            }else{
                sql = sql.replaceAll("#PROCESO#", "");
            }*/

            con = conectarJNDI(Query);
            ps = con.prepareStatement(sql);
            
            ps.setString(1, OdnCmpra);
            ps.setString(2, NmLogin);
            
            System.out.println(ps);
                
            ListadoReqs = new ArrayList();
            
            rs = ps.executeQuery();
            while (rs.next()) {
                
                ComprasProcesoBeans LstReq = new ComprasProcesoBeans();
                
                LstReq.setRegStatus(rs.getString("reg_status"));
                LstReq.setIdOcs(rs.getString("id_ocs"));
                LstReq.setIdOcsDetalle(rs.getString("id"));

                LstReq.setResponsable(rs.getString("responsable"));
                
                LstReq.setCodigoMaterial(rs.getString("codigo_insumo"));
                LstReq.setDescripcionInsumo(rs.getString("descripcion_insumo"));
                LstReq.setNombreUnidadInsumo(rs.getString("nombre_unidad_medida"));
                LstReq.setReferenciaExterna(rs.getString("referencia_externa"));

                LstReq.setCantidadRecibida(rs.getString("cantidad_solicitada"));
                LstReq.setCostoUnitarioRecibido(rs.getString("costo_unitario_compra"));
                LstReq.setCostoTotalRecibido(rs.getString("costo_total_compra"));
                
                ListadoReqs.add(LstReq);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        
        return ListadoReqs;
    }
    

    public ArrayList<ComprasProcesoBeans> GetOrdenCompra(String Query, String OdnCmpra) throws Exception {
        
        ArrayList ListadoReqs = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            String sql = this.obtenerSQL(Query);
            con = conectarJNDI(Query);
            ps = con.prepareStatement(sql);
            
            ps.setString(1, OdnCmpra);
            
            System.out.println(ps);
                
            ListadoReqs = new ArrayList();
            
            rs = ps.executeQuery();
            while (rs.next()) {
                
                ComprasProcesoBeans LstReq = new ComprasProcesoBeans();
                
                LstReq.setRegStatus(rs.getString("reg_status"));
                LstReq.setOrdenCompra(rs.getString("cod_ocs"));
                LstReq.setResponsable(rs.getString("responsable"));
                LstReq.setCodProveedor(rs.getString("cod_proveedor"));
                LstReq.setNombreProveedor(rs.getString("nombre_proveedor"));
                LstReq.setFormaPago(rs.getString("forma_pago"));
                LstReq.setEstadoOcompraApoteosys(rs.getString("estado_apoteosys"));
                
                ListadoReqs.add(LstReq);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        
        return ListadoReqs;
    }

    
}

