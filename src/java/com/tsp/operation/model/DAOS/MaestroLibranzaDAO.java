/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.Usuario;
import org.bouncycastle.util.Strings;

/**
 *
 * @author egonzalez
 */
public interface MaestroLibranzaDAO {

    public String CargarSalarioMinimo();

    public String guardarSalarioMinimo(Usuario usuario, String ano, String salario_minimo_diario, String salario_minimo_mensual, String variacion_anual);

    public String actualizarSalarioMinimo(Usuario usuario, String ano, String salario_minimo_diario, String salario_minimo_mensual, String variacion_anual, String id);

    public String cambiarEstadoSalarioMinimo(String id);

    public String cargarPagadurias();

    public String guardarPagadurias(String razon_social, String documento, String digito_verif, String municipio, String direccion, String telefono, String correo, String usuario, String dstrct);

    public String actualizarPagadurias(String id, String razon_social, String documento, String digito_verif, String municipio, String direccion, String telefono, String correo, String usuario);

    public String activaInactivaPagadurias(String id, String usuario);

    public String CargarOcupacionLaboral();

    public String guardarOcupacionLaboral(Usuario usuario, String descripcion);

    public String actualizarOcupacionLaboral(Usuario usuario, String descripcion, String id);

    public String cambiarEstadoOcupacionLaboral(String id);

    public String CargarDescuentoLey();

    public String cargarComboOcupacionLab();
    
    public String cargarComboOcupacionLabMicro();

    public String guardarDescuentoLey(Usuario usuario, String ocupacion_laboral, String descripcion, String smlv_inicial, String smlv_final, String totaldesc);

    public String actualizarDescuentoLey(Usuario usuario, String ocupacion_laboral, String descripcion, String smlv_inicial, String smlv_final, String totaldesc, String id);

    public String cambiarEstadoDescuentoLey(String id);

    public String CargarExtraprimaLibranza();

    public String guardarExtraprimaLibranza(Usuario usuario, String ocupacion_laboral, String descripcion, String edad_inicial, String edad_final, String perc_extraprima);

    public String actualizarExtraprimaLibranza(Usuario usuario, String ocupacion_laboral, String descripcion, String edad_inicial, String edad_final, String perc_extraprima, String id);

    public String cambiarEstadoExtraprimaLibranza(String id);

    public String CargarOpBancariaLibranza();

    public String guardarOpBancariaLibranza(Usuario usuario, String tipo_documento, String descripcion, String cmc, String cuenta_cxp, String cuenta_detalle, String tipo_operacion);

    public String actualizarOpBancariaLibranza(Usuario usuario, String tipo_documento, String descripcion, String cmc, String cuenta_cxp, String cuenta_detalle, String id, String tipo_operacion);

    public String cambiarEstadoOpBancariaLibranza(String id);

    public String cargarComboTipodoc();

    public String cargarComboHC(String tipodoc);

    public String CargarCuentaHC(String cmc, String tipodoc);

    public boolean existeValorEnPagadurias(String campo, String valor, String id);

    public String CargardDeduccionesLibranza();
    
    public String CargardDeduccionesMicrocredito();

    public String guardarDeduccionesLibranza(Usuario usuario, String operacion_bancaria, String descripcion, String desembolso_inicial, String desembolso_final, String valor_cobrar, String perc_cobrar, String n_xmil,  String ocupacion_laboral);
    
    public String guardarDeduccionesMicrocredito(Usuario usuario, String operacion_bancaria, String descripcion, String desembolso_inicial, String desembolso_final, String valor_cobrar, String perc_cobrar, String n_xmil,  String ocupacion_laboral);

    public String actualizarDeduccionesLibranza(Usuario usuario, String operacion_bancaria, String descripcion, String desembolso_inicial, String desembolso_final, String valor_cobrar, String perc_cobrar, String n_xmil, String ocupacion_laboral, String id);
    
    public String actualizarDeduccionesMicrocredito(Usuario usuario, String operacion_bancaria, String descripcion, String desembolso_inicial, String desembolso_final, String valor_cobrar, String perc_cobrar, String n_xmil, String ocupacion_laboral, String id);

    public String cargarComboOpBancariaLibranza(String operacion);

    public String cargarConfigLibranza();

    public String cargarFirmasRegistradas(String id_config_libranza);

    public String guardarConfigLibranza(String nom_conv_pagaduria,String id_convenio,String id_pagaduria,String id_ocupacion_laboral,String tasa_mensual,String tasa_anual,String tasa_renovacion,String monto_minimo,String monto_maximo,String plazo_minimo,String plazo_maximo,String colchon,String factor_seguro,String porc_descuento,String dia_ent_novedades,String dia_pago,String periodo_gracia,String requiere_anexo, String usuario, String dstrct);

    public String actualizarConfigLibranza(String id, String nom_conv_pagaduria,String id_convenio,String id_pagaduria,String id_ocupacion_laboral,String tasa_mensual,String tasa_anual,String tasa_renovacion,String monto_minimo,String monto_maximo,String plazo_minimo,String plazo_maximo,String colchon,String factor_seguro,String porc_descuento,String dia_ent_novedades,String dia_pago,String periodo_gracia,String requiere_anexo, String usuario);

    public String activaInactivaConfigLibranza(String id, String usuario);

    public String guardarFirmaRegistrada(String id_config_libranza, String nombre, String documento, String telefono, String correo, String usuario, String dstrct);

    public String actualizarFirmaRegistrada(String id, String id_config_libranza, String nombre, String documento, String telefono, String correo, String usuario);

    public String activaInactivaFirmaRegistrada(String id, String usuario);

    public String cambiarEstadoDeduccionesLibranza(String id);
    
    public String cambiarEstadoDeduccionesMicrocredito(String id);

    public String cargarComboPagadurias();

    public String cargarComboConvenios();
   
    public String cargarComboConcepto();

    public String CargarInformacionNegocio(String negocio);

    public String formalizarLibranza(String negocio, Usuario usuario, String concepto, String coment);
    
    public String cargarComboTipoOpBancariaLibranza();
     
    public String calcularTasaAnual(String tasa_mensual);
    
    public String cargarperfeccionarCompraCartera();
    
    public String cargarCHequeCompraCartera(String doc_rel);
    
    public String perfeccionamientoCompraCarteraTrazabilidad(String negocio, Usuario usuario,  String coment);
    
    public String perfeccionamientoCompraCarteraNegocios(String negocio, Usuario usuario);

    public void actualizarReferencia(String documento);
    
    public String CargardEntidadesCompraCartera();
    
    public String cargarProveedoresEntidades(String nombre);
    
    public String CambiarEstadoEntidadesCompraCartera(String id);
    
    public String guardarEntidadesCompraCartera(Usuario usuario, String nombre, String nit, String digver);
    
    public String CargarObligacionesCompra(String numero_solicitud);
    
    public String guardarObligacionesCompra(Usuario usuario, String operacion, String numero_solicitud, String diferencia);

    public String verificarNit( String documento);
    
    public String actualizarObligacionesCompra(Usuario usuario, JsonArray informacion);
    
    public String guardarProveedor(String documento,String razon_social, String municipio, String digito_verif ,Usuario usuario);

    public String auditoriaLibranzas(String fechaini, String fechafin);

    public Object searchNombresArchivos(String rutaOrigen, String documento);

    public boolean almacenarArchivoEnCarpetaUsuario(String documento, String rutaOrigen, String rutaDestino, String nomarchivo);

    public String cargarEmpresasPagaduria(String id_pagaduria);

    public String guardarEmpresaPagaduria(String id_pagaduria, String razon_social, String documento, String digito_verificacion, String telefono, String direccion, Usuario usuario);

    public String actualizarEmpresaPagaduria(String id, String id_pagaduria, String razon_social, String documento, String digito_verificacion, String telefono, String direccion, Usuario usuario);

    public String activaInactivaEmpresaPagaduria(String id, Usuario usuario);
    
    public String cargarClientesLibranza(String fecha_ini, String fecha_fin, String id_convenio);
    
    public String cargarComboConveniosPagaduria();   

    public String cargarComboConveniosReliquidacion();

    public String cargarFormularioReliquidacion(String busqueda, String dato);

    public String cargarConveniosLibranza(String cliente);

    public String cargarempresasLibranza(String id_pagaduria);

    public String cargarTipoCuota();

    public String cargarTipoTituloValor();

    public String reliquidacionLibranza(Usuario usuario, JsonObject obj);

    public String informacionReliquidacionLibranza(String valor_desembolso, String numero_cuotas, String tipo_cuota, String fecha_calculo, String fecha_primera_cuota, int numero_solicitud);

    public String reliquidarLibranza(String valor_desembolso, String numero_cuotas, String tipo_cuota, String fecha_calculo, String fecha_primera_cuota, String formulario, Usuario usuario ,String valorFianza);

    public String calculo_fecha_primera_cuota_reliquidacion(String formulario);

    public String actualizarTasaNegocio(String cod_neg, String tasa_convenio);

}
