/*
 * DatosEcaDAO.java
 * Created on 20 de mayo de 2009, 9:12
 */
package com.tsp.operation.model.DAOS;

import java.sql.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
/**
 * @author  Fintra
 */
public class DatosEcaDAO extends MainDAO  {
    private List listaRegistros;
    private List listaRegistros3;//20100602
    /** Creates a new instance of DatosEcaDAO */
    public DatosEcaDAO() {
        super("DatosEcaDAO.xml");
    }
    public DatosEcaDAO(String dataBaseName) {
        super("DatosEcaDAO.xml", dataBaseName);
    }

    public void obtainRegistrosArchivo()throws SQLException{
        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;
        String            query    = "SQL_GET_REGISTROS_ARCHIVO";
        listaRegistros = null;
        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );

            rs = st.executeQuery();

            listaRegistros =  new LinkedList();

            while (rs.next()){
                listaRegistros.add(DatosEca.load(rs));
            }

        }catch(Exception e){
            System.out.println("errrorrrr"+e.toString());
            //e.printStackTrace();
            throw new SQLException("ERROR DURANTE obtainRegistrosArchivo \n " + e.getMessage());
        }
        finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    public List getRegistrosArchivo(){
        return listaRegistros;
    }

    /**
     *
     * @param datosEca
     * @return
     * @throws SQLException
     */
    public List obtainFacturasEca(DatosEca datosEca) throws SQLException{
        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;
        String            query    = "SQL_GET_FACTURAS_ECA";
        List listaFacturas= null;
        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1,datosEca.getSv());
            /*st.setString(2,datosEca.getFecha().substring(0,10));
            st.setString(3,datosEca.getSv());
            st.setString(4,datosEca.getFecha().substring(0,10));
            st.setString(5,datosEca.getFecha().substring(0,10));*/
            rs = st.executeQuery();

            listaFacturas =  new LinkedList();

            while (rs.next()){
                listaFacturas.add(FacturaEca.load(rs));
            }

        }catch(Exception e){
            System.out.println("eror:"+e.toString());
            throw new SQLException("ERROR DURANTE obtainFacturasEca \n " + e.getMessage());
        }
        finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listaFacturas;
    }

/**
 *
 * @param de
 * @param facturasEca
 * @param num_ingreso
 * @return
 * @throws SQLException
 */
    public Vector cruzarFacIng(DatosEca de,  List facturasEca,String num_ingreso  ) throws SQLException{

        Vector respuesta =new Vector();

        if (facturasEca==null || de==null || facturasEca.size()==0){
            return respuesta;
        }

        StringStatement st       = null;
        Connection        con      = null;
        String comando_sql = "";
        String query1="SQL_UPDATE_FAC_ECA";

        FacturaEca facturaEca=(FacturaEca)facturasEca.get(0);

        String ing="",ing_det="",updateFac="",updateDatoEca="";
        double abono=0,ing_eca,val_fac;

        ing_eca=Double.parseDouble(de.getSaldo());
        val_fac=Double.parseDouble(facturaEca.getSaldo());
        if (ing_eca<=val_fac){//val_excel<=val_fac1 entonces X=val_excel
            abono=ing_eca;
        }else{//X=val_fac1
            abono=val_fac;
        }

        try {

            st = new StringStatement (this.obtenerSQL(query1), true);//JJCastro fase2
            st.setDouble(1,abono);
            st.setDouble(2,abono);
            st.setDouble(3,abono);
            st.setDouble(4,abono);
            st.setString(5,de.getFecha().substring(0,10));
            st.setString(6,de.getFecha().substring(0,10));
            st.setString(7,facturaEca.getDoc());
            comando_sql = st.getSql();
            respuesta.add(comando_sql);

            st = new StringStatement (this.obtenerSQL("SQL_UPDATE_ING_ECA" ), true);//JJCastro fase2

            st.setDouble(1,abono);
            st.setString(2,facturaEca.getDoc());
            st.setString(3,de.getId());
            comando_sql = st.getSql();
            respuesta.add(comando_sql);

            st = new StringStatement (this.obtenerSQL("SQL_ING" ), true);//JJCastro fase2

            st.setString(1,num_ingreso);
            st.setString(2,facturaEca.getCodCli());
            st.setString(3,facturaEca.getNit());
            st.setString(4,de.getFecha());

            st.setString(5,"recaudo de simbolo variable "+de.getSv()+" del archivo "+de.getFecha()+" de la factura "+facturaEca.getDoc()+" ");

            st.setString(6,de.getValor());
            st.setString(7,de.getValor());

            if (Double.parseDouble(de.getValor())>Double.parseDouble(facturaEca.getSaldo())){
                st.setString(8,"2");
            }else{
                st.setString(8,"1");
            }
            comando_sql = st.getSql();
            respuesta.add(comando_sql);

            st = new StringStatement (this.obtenerSQL("SQL_ING_DET"  ), true);//JJCastro fase2
            st.setString(1,num_ingreso);
            st.setString(2,"1");
            st.setString(3,facturaEca.getNit());
            st.setDouble(4,abono);
            st.setDouble(5,abono);
            st.setString(6, facturaEca.getDoc());
            st.setString(7, facturaEca.getFechaFactura());
            st.setString(8, facturaEca.getDoc());
            st.setString(9, "13109703");
            st.setString(10, "recaudo de simbolo variable "+de.getSv()+" del archivo "+de.getFecha()+" de la factura "+facturaEca.getDoc()+" ");
            st.setString(11, facturaEca.getSaldo());

            comando_sql = st.getSql();
            respuesta.add(comando_sql);

            if (Double.parseDouble(de.getValor())>Double.parseDouble(facturaEca.getSaldo())){
                //st.setString(7,"2");
                st = new StringStatement (this.obtenerSQL("SQL_ING_DET"  ), true);//JJCastro fase2
                st.setString(1,num_ingreso);
                st.setString(2,"2");
                st.setString(3,facturaEca.getNit());
                st.setDouble(4,Double.parseDouble(de.getValor())-abono);
                st.setDouble(5,Double.parseDouble(de.getValor())-abono);
                st.setString(6, facturaEca.getDoc());
                st.setString(7, facturaEca.getFechaFactura());
                st.setString(8, facturaEca.getDoc());
                st.setString(9, "27055556");
                st.setString(10, "residuo de recaudo de simbolo variable "+de.getSv()+" del archivo "+de.getFecha()+" de la factura "+facturaEca.getDoc()+" ");
                st.setString(11, facturaEca.getSaldo());
                comando_sql = st.getSql();
                respuesta.add(comando_sql);
            }


        }catch(Exception e){
            System.out.println("errorcit:"+e.toString());
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA ACTUALIZAR FECHA FACTURA CONTRATISTA. \n " + e.getMessage());
        }
        finally{//JJCastro fase2
           if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        //.out.println("respuesta"+respuesta);
        return respuesta;
    }




/**
 *
 * @param de
 * @param facturasEca
 * @param num_ingreso
 * @param SwUltimaCxcEs0
 * @param indice
 * @return
 * @throws SQLException
 */
    public Vector cruzarFacIng(DatosEca de,  List facturasEca,String num_ingreso, int SwUltimaCxcEs0 ,int indice ) throws SQLException{

        Vector respuesta =new Vector();
        double saldo_de=Double.parseDouble(de.getSaldo());//el saldo del recaudo

        if (facturasEca==null || de==null || facturasEca.size()==0){
            return respuesta;
        }

        StringStatement st       = null;
        Connection        con      = null;
        String comando_sql = "";
        String query1="SQL_UPDATE_FAC_ECA";
        FacturaEca facturaEca=(FacturaEca)facturasEca.get(indice);//la factura numero j

        String ing="",ing_det="",updateFac="",updateDatoEca="";
        double abono=0,ing_eca,val_fac;

        ing_eca=Double.parseDouble(de.getSaldo());//el saldo del recaudo
        val_fac=Double.parseDouble(facturaEca.getSaldo());//el saldo de la factura
        if (ing_eca<=val_fac){//si el saldo del recaudo es menor que el saldo de la factura
            abono=ing_eca;//el abono es el saldo del recaudo
        }else{
            abono=val_fac;//el abono es el saldo de la factura
        }

        try {
            st = new StringStatement (this.obtenerSQL(query1), true);//JJCastro fase2
            st.setDouble(1,abono);
            st.setDouble(2,abono);
            st.setDouble(3,abono);
            st.setDouble(4,abono);
            st.setString(5,de.getFecha().substring(0,10));
            st.setString(6,de.getFecha().substring(0,10));
            st.setString(7,facturaEca.getDoc());
            comando_sql = st.getSql();
            respuesta.add(comando_sql);


            st = new StringStatement (this.obtenerSQL("SQL_UPDATE_ING_ECA"), true);//JJCastro fase2
            st.setDouble(1,abono);
            st.setString(2,facturaEca.getDoc());
            st.setString(3,de.getId());
            comando_sql = st.getSql();
            de.setSaldo(""+(Double.parseDouble(de.getSaldo())-abono));//
            respuesta.add(comando_sql);


            st = new StringStatement (this.obtenerSQL("SQL_ING"), true);//JJCastro fase2
            st.setString(1,num_ingreso);
            st.setString(2,facturaEca.getCodCli());
            st.setString(3,facturaEca.getNit());
            st.setString(4,de.getFecha());

            st.setString(5,"recaudo de simbolo variable "+de.getSv()+" del archivo "+de.getFecha()+" de la factura "+facturaEca.getDoc()+" ");

            if (saldo_de>Double.parseDouble(facturaEca.getSaldo()) && SwUltimaCxcEs0==0){//si el saldo del recaudo es mayor que el saldo de la cxc y es la ultima cxc
                st.setString(6,""+(saldo_de));//el valor del ingreso es el saldo del ingreso
                st.setString(7,""+(saldo_de));
            }else{
                st.setString(6,""+(abono));//el valor del ingreso es el abono
                st.setString(7,""+(abono));
            }


            if (saldo_de>Double.parseDouble(facturaEca.getSaldo()) && SwUltimaCxcEs0==0){//si el saldo del recaudo es mayor que el saldo de la cxc y es la ultima cxc
                st.setString(8,"2");//2 items tendrá
            }else{
                st.setString(8,"1");//1 item
            }
            comando_sql = st.getSql();
            respuesta.add(comando_sql);

            st = new StringStatement (this.obtenerSQL("SQL_ING_DET"), true);//JJCastro fase2
            st.setString(1,num_ingreso);
            st.setString(2,"1");
            st.setString(3,facturaEca.getNit());
            st.setDouble(4,abono);
            st.setDouble(5,abono);
            st.setString(6, facturaEca.getDoc());
            st.setString(7, facturaEca.getFechaFactura());
            st.setString(8, facturaEca.getDoc());
            st.setString(9, "13109703");
            st.setString(10, "recaudo de simbolo variable "+de.getSv()+" del archivo "+de.getFecha()+" de la factura "+facturaEca.getDoc()+" ");
            st.setString(11, facturaEca.getSaldo());

            comando_sql = st.getSql();
            respuesta.add(comando_sql);

            if (saldo_de>Double.parseDouble(facturaEca.getSaldo()) && SwUltimaCxcEs0==0){
                //st.setString(7,"2");
                st = new StringStatement (this.obtenerSQL("SQL_ING_DET"), true);//JJCastro fase2
                st.setString(1,num_ingreso);
                st.setString(2,"2");
                st.setString(3,facturaEca.getNit());
                st.setDouble(4,saldo_de-abono);
                st.setDouble(5,saldo_de-abono);
                st.setString(6, facturaEca.getDoc());
                st.setString(7, facturaEca.getFechaFactura());
                st.setString(8, facturaEca.getDoc());
                st.setString(9, "27055556");
                st.setString(10, "residuo de recaudo de simbolo variable "+de.getSv()+" del archivo "+de.getFecha()+" de la factura "+facturaEca.getDoc()+" "+"por el recaudo:"+de.getId());
                st.setString(11, facturaEca.getSaldo());
                comando_sql = st.getSql();
                respuesta.add(comando_sql);
            }


        }catch(Exception e){
            System.out.println("errorcit2:"+e.toString());
            e.printStackTrace();
            throw new SQLException("ERROR2 DURANTE LA CONFORMACION DEL COMANDO SQL PARA ACTUALIZAR FECHA FACTURA CONTRATISTA. \n " + e.getMessage());
        }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        //.out.println("respuesta"+respuesta);
        return respuesta;
    }



/**
 *
 * @throws SQLException
 */
    public void obtainRegistrosArchivo2()throws SQLException{
        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;
        String            query    = "SQL_GET_REGISTROS_ARCHIVO2";
        listaRegistros = null;
        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();
            listaRegistros =  new LinkedList();
            while (rs.next()){
                listaRegistros.add(DatosEca.load(rs));
            }

        }catch(Exception e){
            System.out.println("errrorrrr2"+e.toString());
            //e.printStackTrace();
            throw new SQLException("ERROR DURANTE obtainRegistrosArchivo2 \n " + e.getMessage());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    public List obtainFacturasEca2(DatosEca datosEca) throws SQLException{
        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;
        String            query    = "SQL_GET_FACTURAS_ECA2";
        List listaFacturas= null;
        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1,datosEca.getCxcExcel());
            /*st.setString(2,datosEca.getFecha().substring(0,10));
            st.setString(3,datosEca.getSv());
            st.setString(4,datosEca.getFecha().substring(0,10));
            st.setString(5,datosEca.getFecha().substring(0,10));*/
            rs = st.executeQuery();

            listaFacturas =  new LinkedList();

            while (rs.next()){
                listaFacturas.add(FacturaEca.load(rs));
            }

        }catch(Exception e){
            System.out.println("eror2:"+e.toString());
            throw new SQLException("ERROR DURANTE obtainFacturasEca2 \n " + e.getMessage());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listaFacturas;
    }


 /**
  *
  * @param de
  * @param facturasEca
  * @param num_ingreso
  * @param SwUltimaCxcEs0
  * @param indice
  * @return
  * @throws SQLException
  */
    public Vector cruzarFacIng2(DatosEca de,  List facturasEca,String num_ingreso, int SwUltimaCxcEs0 ,int indice ) throws SQLException{

        Vector respuesta =new Vector();
        double saldo_de=Double.parseDouble(de.getSaldo());//el saldo del recaudo

        if (facturasEca==null || de==null || facturasEca.size()==0){
            return respuesta;
        }
        StringStatement st       = null;
        Connection        con      = null;
        String comando_sql = "";
        String query1="SQL_UPDATE_FAC_ECA";
        FacturaEca facturaEca=(FacturaEca)facturasEca.get(indice);//la factura numero j
        String ing="",ing_det="",updateFac="",updateDatoEca="";
        double abono=0,ing_eca,val_fac;

        ing_eca=Double.parseDouble(de.getSaldo());//el saldo del recaudo
        val_fac=Double.parseDouble(facturaEca.getSaldo());//el saldo de la factura
        if (ing_eca<=val_fac){//si el saldo del recaudo es menor que el saldo de la factura
            abono=ing_eca;//el abono es el saldo del recaudo
        }else{
            abono=val_fac;//el abono es el saldo de la factura
        }

        try {

            st = new StringStatement (this.obtenerSQL(query1), true);//JJCastro fase2
            st.setDouble(1,abono);
            st.setDouble(2,abono);
            st.setDouble(3,abono);
            st.setDouble(4,abono);
            st.setString(5,de.getFecha().substring(0,10));
            st.setString(6,de.getFecha().substring(0,10));
            st.setString(7,facturaEca.getDoc());
            comando_sql = st.getSql();
            respuesta.add(comando_sql);

            st = new StringStatement (this.obtenerSQL("SQL_UPDATE_ING_ECA" ), true);//JJCastro fase2
            st.setDouble(1,abono);
            st.setString(2,facturaEca.getDoc());
            st.setString(3,de.getId());
            comando_sql = st.getSql();
            String exsaldo=de.getSaldo();
            de.setSaldo(""+(Double.parseDouble(de.getSaldo())-abono));//
            respuesta.add(comando_sql);

            //inicio de para intereses de mora
            String periodo_recaudo=de.getFecha().substring(0,7);
            String periodo_vencimiento=facturaEca.getFechaVencimiento().substring(0,7);
            if (!(periodo_recaudo.equals(periodo_vencimiento))){//si el periodo del recaudo es diferente al periodo de la fecha de vencimiento de la cxc hay que meter en la tabla de intereses de mora

                st = new StringStatement (this.obtenerSQL("SQL_INT_MORA" ), true);//JJCastro fase2
                st.setString(1,facturaEca.getDoc());
                st.setString(2,"FAC");
                st.setString(3,facturaEca.getFechaVencimiento());
                st.setString(4,facturaEca.getValorFactura());
                st.setString(5,facturaEca.getSaldo());
                st.setString(6,facturaEca.getMs());
                st.setString(7,de.getId());
                st.setString(8,de.getFecha());
                st.setString(9,de.getValor());
                st.setString(10,exsaldo);
                st.setString(11,de.getMsExcel());

                st.setString(12,"SROBINSON");

                st.setString(13,"");
                st.setString(14,"");
                st.setString(15,"FINV");

                comando_sql = st.getSql();
                respuesta.add(comando_sql);
            }
            //fin de para intereses de mora

            st = new StringStatement (this.obtenerSQL("SQL_ING" ), true);//JJCastro fase2
            st.setString(1,num_ingreso);
            st.setString(2,facturaEca.getCodCli());
            st.setString(3,facturaEca.getNit());
            st.setString(4,de.getFecha());
            st.setString(5,"recaudo de simbolo variable "+de.getSv()+" del archivo "+de.getFecha()+" de la factura "+facturaEca.getDoc()+" ");

            if (saldo_de>Double.parseDouble(facturaEca.getSaldo()) && SwUltimaCxcEs0==0){//si el saldo del recaudo es mayor que el saldo de la cxc y es la ultima cxc
                st.setString(6,""+(saldo_de));//el valor del ingreso es el saldo del ingreso
                st.setString(7,""+(saldo_de));
            }else{
                st.setString(6,""+(abono));//el valor del ingreso es el abono
                st.setString(7,""+(abono));
            }


            if (saldo_de>Double.parseDouble(facturaEca.getSaldo()) && SwUltimaCxcEs0==0){//si el saldo del recaudo es mayor que el saldo de la cxc y es la ultima cxc
                st.setString(8,"2");//2 items tendrá
            }else{
                st.setString(8,"1");//1 item
            }
            comando_sql = st.getSql();
            respuesta.add(comando_sql);

            st = new StringStatement (this.obtenerSQL("SQL_ING_DET" ), true);//JJCastro fase2
            st.setString(1,num_ingreso);
            st.setString(2,"1");
            st.setString(3,facturaEca.getNit());
            st.setDouble(4,abono);
            st.setDouble(5,abono);
            st.setString(6, facturaEca.getDoc());
            st.setString(7, facturaEca.getFechaFactura());
            st.setString(8, facturaEca.getDoc());
            st.setString(9, "13109703");
            st.setString(10, "recaudo de simbolo variable "+de.getSv()+" del archivo "+de.getFecha()+" de la factura "+facturaEca.getDoc()+" ");
            st.setString(11, facturaEca.getSaldo());

            comando_sql = st.getSql();
            respuesta.add(comando_sql);

            if (saldo_de>Double.parseDouble(facturaEca.getSaldo()) && SwUltimaCxcEs0==0){
                //st.setString(7,"2");
                st = new StringStatement (this.obtenerSQL("SQL_ING_DET" ), true);//JJCastro fase2
                st.setString(1,num_ingreso);
                st.setString(2,"2");
                st.setString(3,facturaEca.getNit());
                st.setDouble(4,saldo_de-abono);
                st.setDouble(5,saldo_de-abono);
                st.setString(6, facturaEca.getDoc());
                st.setString(7, facturaEca.getFechaFactura());
                st.setString(8, facturaEca.getDoc());
                st.setString(9, "27055556");
                st.setString(10, "residuo de recaudo de simbolo variable "+de.getSv()+" del archivo "+de.getFecha()+" de la factura "+facturaEca.getDoc()+" "+"por el recaudo:"+de.getId());
                st.setDouble(11, (saldo_de-abono)/*facturaEca.getSaldo()*/);
                comando_sql = st.getSql();
                respuesta.add(comando_sql);
            }


        }catch(Exception e){
            System.out.println("errorcit2:"+e.toString());
            e.printStackTrace();
            throw new SQLException("ERROR2 DURANTE LA CONFORMACION DEL COMANDO SQL PARA ACTUALIZAR FECHA FACTURA CONTRATISTA. \n " + e.getMessage());
        }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        //.out.println("respuesta"+respuesta);
        return respuesta;
    }




    /**
     *
     * @param de
     * @param facturasEca
     * @param num_ingreso
     * @param SwUltimaCxcEs0
     * @param indice
     * @return
     * @throws SQLException
     */
    public Vector cruzarFacIngPms2(DatosEca de,  List facturasEca,String num_ingreso, int SwUltimaCxcEs0 ,int indice ) throws SQLException{

        Vector respuesta =new Vector();
        double saldo_de=Double.parseDouble(de.getSaldo());//el saldo del recaudo

        if (facturasEca==null || de==null || facturasEca.size()==0){
            return respuesta;
        }

        StringStatement st       = null;
        Connection        con      = null;
        String comando_sql = "";
        String query1="SQL_UPDATE_FAC_ECA";


        FacturaEca facturaEca=(FacturaEca)facturasEca.get(indice);//la factura numero j

        String ing="",ing_det="",updateFac="",updateDatoEca="";
        double abono=0,ing_eca,val_fac;

        ing_eca=Double.parseDouble(de.getSaldo());//el saldo del recaudo
        val_fac=Double.parseDouble(facturaEca.getSaldo());//el saldo de la factura
        if (ing_eca<=val_fac){//si el saldo del recaudo es menor que el saldo de la factura
            abono=ing_eca;//el abono es el saldo del recaudo
        }else{
            abono=val_fac;//el abono es el saldo de la factura
        }

        try {
            st = new StringStatement (this.obtenerSQL(query1), true);//JJCastro fase2
            st.setDouble(1,abono);
            st.setDouble(2,abono);
            st.setDouble(3,abono);
            st.setDouble(4,abono);
            st.setString(5,de.getFecha().substring(0,10));
            st.setString(6,de.getFecha().substring(0,10));
            st.setString(7,facturaEca.getDoc());
            comando_sql = st.getSql();
            respuesta.add(comando_sql);


            st = new StringStatement (this.obtenerSQL("SQL_UPDATE_ING_ECA"), true);//JJCastro fase2
            st.setDouble(1,abono);
            st.setString(2,facturaEca.getDoc());
            st.setString(3,de.getId());
            comando_sql = st.getSql();
            String exsaldo=de.getSaldo();
            de.setSaldo(""+(Double.parseDouble(de.getSaldo())-abono));//
            respuesta.add(comando_sql);

            //inicio de para intereses de mora
            String periodo_recaudo=de.getFecha().substring(0,7);
            String periodo_vencimiento=facturaEca.getFechaVencimiento().substring(0,7);
            if (!(periodo_recaudo.equals(periodo_vencimiento))){//si el periodo del recaudo es diferente al periodo de la fecha de vencimiento de la cxc hay que meter en la tabla de intereses de mora
                st = new StringStatement (this.obtenerSQL( "SQL_INT_MORA"), true);//JJCastro fase2
                st.setString(1,facturaEca.getDoc());
                st.setString(2,"FAC");
                st.setString(3,facturaEca.getFechaVencimiento());
                st.setString(4,facturaEca.getValorFactura());
                st.setString(5,facturaEca.getSaldo());
                st.setString(6,facturaEca.getMs());
                st.setString(7,de.getId());
                st.setString(8,de.getFecha());
                st.setString(9,de.getValor());
                st.setString(10,exsaldo);
                st.setString(11,de.getMsExcel());

                st.setString(12,"SROBINSON");

                st.setString(13,"");
                st.setString(14,"");
                st.setString(15,"FINV");

                comando_sql = st.getSql();
                respuesta.add(comando_sql);
            }
            //fin de para intereses de mora
            st = new StringStatement (this.obtenerSQL( "SQL_ICA"), true);//JJCastro fase2
            st.setString(1,num_ingreso);
            st.setString(2,facturaEca.getCodCli());
            st.setString(3,facturaEca.getNit());
            st.setString(4,de.getFecha());

            st.setString(5,"recaudo de simbolo variable "+de.getSv()+" del archivo "+de.getFecha()+" de la factura "+facturaEca.getDoc()+" ");

            if (saldo_de>Double.parseDouble(facturaEca.getSaldo()) && SwUltimaCxcEs0==0){//si el saldo del recaudo es mayor que el saldo de la cxc y es la ultima cxc
                st.setString(6,""+(saldo_de));//el valor del ingreso es el saldo del ingreso
                st.setString(7,""+(saldo_de));
            }else{
                st.setString(6,""+(abono));//el valor del ingreso es el abono
                st.setString(7,""+(abono));
            }


            if (saldo_de>Double.parseDouble(facturaEca.getSaldo()) && SwUltimaCxcEs0==0){//si el saldo del recaudo es mayor que el saldo de la cxc y es la ultima cxc
                st.setString(8,"2");//2 items tendrá
            }else{
                st.setString(8,"1");//1 item
            }
            comando_sql = st.getSql();
            respuesta.add(comando_sql);

            st = new StringStatement (this.obtenerSQL( "SQL_ICA_DET"), true);//JJCastro fase2
            st.setString(1,num_ingreso);
            st.setString(2,"1");
            st.setString(3,facturaEca.getNit());
            st.setDouble(4,abono);
            st.setDouble(5,abono);
            st.setString(6, facturaEca.getDoc());
            st.setString(7, facturaEca.getFechaFactura());
            st.setString(8, facturaEca.getDoc());
            st.setString(9, "16252071");
            st.setString(10, "recaudo de simbolo variable "+de.getSv()+" del archivo "+de.getFecha()+" de la factura "+facturaEca.getDoc()+" ");
            st.setString(11, facturaEca.getSaldo());

            comando_sql = st.getSql();
            respuesta.add(comando_sql);

            if (saldo_de>Double.parseDouble(facturaEca.getSaldo()) && SwUltimaCxcEs0==0){
                //st.setString(7,"2");
                st = new StringStatement (this.obtenerSQL( "SQL_ICA_DET"), true);//JJCastro fase2
                st.setString(1,num_ingreso);
                st.setString(2,"2");
                st.setString(3,facturaEca.getNit());
                st.setDouble(4,saldo_de-abono);
                st.setDouble(5,saldo_de-abono);
                st.setString(6, facturaEca.getDoc());
                st.setString(7, facturaEca.getFechaFactura());
                st.setString(8, facturaEca.getDoc());
                st.setString(9, "16252082");
                st.setString(10, "residuo de recaudo de simbolo variable "+de.getSv()+" del archivo "+de.getFecha()+" de la factura "+facturaEca.getDoc()+" "+"por el recaudo:"+de.getId());
                st.setDouble(11, (saldo_de-abono)/*facturaEca.getSaldo()*/);
                comando_sql = st.getSql();
                respuesta.add(comando_sql);
            }


        }catch(Exception e){
            System.out.println("errorcit2:"+e.toString());
            e.printStackTrace();
            throw new SQLException("ERROR2 DURANTE LA CONFORMACION DEL COMANDO SQL PARA ACTUALIZAR FECHA FACTURA CONTRATISTA. \n " + e.getMessage());
        }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        //.out.println("respuesta"+respuesta);
        return respuesta;
    }

    //inicio de 20100602
    public void obtainRegistrosArchivo3()throws SQLException{
        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;
        String            query    = "SQL_GET_REGISTROS_ARCHIVO3";
        listaRegistros3 = null;
        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );

            rs = st.executeQuery();

            listaRegistros3 =  new LinkedList();

            while (rs.next()){
                listaRegistros3.add(DatosEca.load(rs));
            }

        }catch(Exception e){
            System.out.println("errrorrrr3"+e.toString());
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE obtainRegistrosArchivo2 \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
    }

    public List getRegistrosArchivo3(){
        return listaRegistros3;
    }

    public List obtainFacturasEca3(DatosEca datosEca) throws SQLException{
        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;
        String            query    = "SQL_GET_FACTURAS_ECA3";
        List listaFacturas3= null;
        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1,datosEca.getCxcExcel());

            rs = st.executeQuery();

            listaFacturas3 =  new LinkedList();

            while (rs.next()){
                listaFacturas3.add(FacturaEca.load3(rs));
            }

        }catch(Exception e){
            System.out.println("eror3:"+e.toString());
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE obtainFacturasEca3 \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return listaFacturas3;
    }

    public Vector cruzarFacIngPms3(DatosEca de,  List facturasEca,String num_ingreso, int SwUltimaCxcEs0 ,int indice, String usr ) throws SQLException{

        Vector respuesta =new Vector();

        double saldo_de=Double.parseDouble(de.getSaldo());//el saldo del recaudo

        if (facturasEca==null || de==null || facturasEca.size()==0){
            return respuesta;
        }

        PreparedStatement st       = null;
        Connection        con      = null;
        String comando_sql = "";
        String query1="SQL_UPDATE_FAC_ECA3";

        FacturaEca facturaEca=(FacturaEca)facturasEca.get(indice);//la factura numero j

        String ing="",ing_det="",updateFac="",updateDatoEca="";
        double abono=0,ing_eca,val_fac;

        try {

            ing_eca=Double.parseDouble(de.getSaldo());//el saldo del recaudo
            val_fac=Double.parseDouble(facturaEca.getSaldo());//el saldo de la factura
            if (ing_eca<=val_fac){//si el saldo del recaudo es menor que el saldo de la factura
                abono=ing_eca;//el abono es el saldo del recaudo
            }else{
                abono=val_fac;//el abono es el saldo de la factura
            }

            con   = this.conectar( query1 );
            updateFac=obtenerSQL( query1 );//se le cambia el saldo a la cxc
            st           = con.prepareStatement( updateFac );
            st.setDouble(1,abono);
            st.setDouble(2,abono);
            st.setDouble(3,abono);
            st.setDouble(4,abono);
            st.setString(5,de.getFecha().substring(0,10));
            st.setString(6,de.getFecha().substring(0,10));
            st.setString(7,facturaEca.getDoc());
            comando_sql = st.toString();
            respuesta.add(comando_sql);

            updateDatoEca=obtenerSQL( "SQL_UPDATE_ING_ECA3" );//se le cambia el saldo al recaudo
            st           = con.prepareStatement( updateDatoEca );
            st.setString(1,usr);
            st.setDouble(2,abono);
            st.setString(3,facturaEca.getDoc());
            st.setString(4,de.getId());
            comando_sql = st.toString();
            String exsaldo=de.getSaldo();
            de.setSaldo(""+(Double.parseDouble(de.getSaldo())-abono));//
            respuesta.add(comando_sql);

            //inicio de para intereses de mora
            String periodo_recaudo=de.getFecha().substring(0,7);
            String periodo_vencimiento=facturaEca.getFechaVencimiento().substring(0,7);
            if (!(periodo_recaudo.equals(periodo_vencimiento))){//si el periodo del recaudo es diferente al periodo de la fecha de vencimiento de la cxc hay que meter en la tabla de intereses de mora
                String int_mora=obtenerSQL( "SQL_INT_MORA3" );//se crea el ingreso con el valor del abono
                st           = con.prepareStatement( int_mora );
                st.setString(1,facturaEca.getDoc());//20100602
                //st.setString(2,"FAC");//20100602
                st.setString(2,facturaEca.getFechaVencimiento());//20100602
                st.setString(3,facturaEca.getValorFactura());//20100602
                st.setString(4,facturaEca.getSaldo());//20100602
                st.setString(5,facturaEca.getMs());//20100602
                st.setString(6,de.getId());//20100602
                st.setString(7,de.getFecha());//20100602
                st.setString(8,de.getValor());//20100602
                st.setString(9,exsaldo);//20100602
                //st.setString(11,de.getMsExcel());//20100602

                st.setString(10,usr);//20100602

                st.setString(11,"");//20100602
                st.setString(12,"");//20100602
                //st.setString(13,"FINV");//20100602

                comando_sql = st.toString();
                respuesta.add(comando_sql);
            }
            //fin de para intereses de mora

            ing=obtenerSQL( "SQL_ICA3" );//se crea el ingreso con el valor del abono
            st           = con.prepareStatement( ing );

            st.setString(1,num_ingreso);
            st.setString(2,facturaEca.getCodCli());
            st.setString(3,facturaEca.getNit());
            st.setString(4,de.getFecha());


            st.setString(5,"recaudo del archivo "+de.getFecha()+" de la factura "+facturaEca.getDoc()+" ");

            if (saldo_de>Double.parseDouble(facturaEca.getSaldo()) && SwUltimaCxcEs0==0){//si el saldo del recaudo es mayor que el saldo de la cxc y es la ultima cxc
                st.setString(6,""+(saldo_de));//el valor del ingreso es el saldo del ingreso
                st.setString(7,""+(saldo_de));
            }else{
                st.setString(6,""+(abono));//el valor del ingreso es el abono
                st.setString(7,""+(abono));
            }


            if (saldo_de>Double.parseDouble(facturaEca.getSaldo()) && SwUltimaCxcEs0==0){//si el saldo del recaudo es mayor que el saldo de la cxc y es la ultima cxc
                st.setString(8,"2");//2 items tendrÃ¡
            }else{
                st.setString(8,"1");//1 item
            }
            
            //aqui validamos el numero de cuanta que va en el ingreso dependiendo si es colpatria o corfi.
            //colpatria- FIDCOP : 16252116 , corfi - FIDCOR: 16252088 parametro 9 en el xml, TSP : 16252170
            //String cuenta = (facturaEca.getClasificacion1().equals("FIDCOP") ? "16252116" : "16252088");

            String cuenta = "";
            if ( facturaEca.getClasificacion1().equals("FIDCOP") ) {
               cuenta = "16252116";
            }else if ( facturaEca.getClasificacion1().equals("FIDFIV") ) {
               cuenta = "13050705";
            }else if ( facturaEca.getClasificacion1().equals("FIDPVC") ) {
                cuenta = "83251002";
            }else if ( facturaEca.getClasificacion1().equals("FIDFUP") ) {
                cuenta = "13050728";
            }else if ( facturaEca.getClasificacion1().equals("FIDTSP") ) {
                cuenta = "16252170";
            }else{
               if( facturaEca.getRef1().startsWith("ACON")) {
                   cuenta = "13050722";
               } else {
                   cuenta = "16252088";
               }         
            }            
                                    
            st.setString(9, cuenta);

            //st.setString(9,facturaEca.getCmc());//20100604
            //st.setString(10,facturaEca.getCmc());//20100604

            comando_sql = st.toString();
            respuesta.add(comando_sql);

            ing_det=obtenerSQL( "SQL_ICA_DET3" );
            st           = con.prepareStatement( ing_det );

            st.setString(1,num_ingreso);
            st.setString(2,"1");
            st.setString(3,facturaEca.getNit());
            st.setDouble(4,abono);
            st.setDouble(5,abono);
            st.setString(6, facturaEca.getDoc());
            st.setString(7, facturaEca.getFechaFactura());

            st.setString(8, "FAC");//20100611

            st.setString(9, facturaEca.getDoc());//20100611
            //st.setString(9, "16252071");//se debe modificar dependiendo si es nueva o vieja la cxc

            st.setString(10, facturaEca.getCmc()+"1");//20100611
            st.setString(11, facturaEca.getCmc()+"1");//20100611
            st.setString(12, facturaEca.getCmc()+"1");//20100611
            st.setString(13, facturaEca.getCmc()+"1");//20130321//NUEVO FP1
            st.setString(14, facturaEca.getCmc()+"1");//20130321//NUEVO RA1
            st.setString(15, facturaEca.getCmc()+"1");//20130321//NUEVO CV1
            st.setString(16, facturaEca.getCmc()+"1");//20130321//NUEVO SR1
            st.setString(17, facturaEca.getCmc()+"1");//20100611//NUEVO SV1
            st.setString(18, facturaEca.getCmc()+"1");//20100611//NUEVO AS1
            st.setString(19, facturaEca.getCmc()+"1");//20100611//NUEVO FU1
            st.setString(20, facturaEca.getCmc()+"1");//20180530//NUEVO DC1
            st.setString(21, facturaEca.getCmc()+"1");//20180530//NUEVO RT1
            st.setString(22, facturaEca.getCmc()+"1");//20100611
            st.setString(23, facturaEca.getCmc()+"1");//20100611
            st.setString(24, facturaEca.getCmc()+"1");//20130321
            st.setString(25, facturaEca.getCmc()+"1");//20130321//NUEVO FP1
            st.setString(26, facturaEca.getCmc()+"1");//20130321//NUEVO RA1
            st.setString(27, facturaEca.getCmc()+"1");//20130321//NUEVO CV1
            st.setString(28, facturaEca.getCmc()+"1");//20130321//NUEVO SR1
            st.setString(29, facturaEca.getCmc()+"1");//20130321//NUEVO SV1
            st.setString(30, facturaEca.getCmc()+"1");//20130321//NUEVO AS1
            st.setString(31, facturaEca.getCmc()+"1");//20130321//NUEVO FU1
            st.setString(32, facturaEca.getCmc()+"1");//20180530//NUEVO DC1
            st.setString(33, facturaEca.getCmc()+"1");//20180530//NUEVO RT1
            
            st.setString(34, "recaudo del archivo "+de.getFecha()+" de la factura "+facturaEca.getDoc()+" ");//20100611
            st.setString(35, facturaEca.getSaldo());//20100611

            st.setString(36, "");//20100811

            comando_sql = st.toString();
            respuesta.add(comando_sql);

            if ( saldo_de > Double.parseDouble(facturaEca.getSaldo()) && SwUltimaCxcEs0 == 0 ){ //Cuando sobra dinero, entra en esta parte y se guarda el excedente en otras cuentas
                //st.setString(7,"2");
                ing_det=obtenerSQL( "SQL_ICA_DET3" );
                st           = con.prepareStatement( ing_det );
                st.setString(1,num_ingreso);
                st.setString(2,"2");
                st.setString(3,facturaEca.getNit());
                st.setDouble(4,saldo_de-abono);
                st.setDouble(5,saldo_de-abono);
                //st.setString(6, facturaEca.getDoc());//parece que debe colocarse vacio 20100603
                st.setString(6, "");//20100603
                st.setString(7, facturaEca.getFechaFactura());
                //st.setString(8, facturaEca.getDoc());//parece que debe colocarse vacio 20100603
                st.setString(8, "");//20100611
                st.setString(9, "");//20100611
                //st.setString(9, "16252082");//se debe modificar dependiendo si es nueva o vieja la cxc

                st.setString(10, facturaEca.getCmc()+"2");//20100611
                st.setString(11, facturaEca.getCmc()+"2");//20100611
                st.setString(12, facturaEca.getCmc()+"2");//20100611
                st.setString(13, facturaEca.getCmc()+"2");//20130321 //NUEVO FP2
                st.setString(14, facturaEca.getCmc()+"2");//20130321 //NUEVO RA2
                st.setString(15, facturaEca.getCmc()+"2");//20130321 //NUEVO CV2
                st.setString(16, facturaEca.getCmc()+"2");//20130321 //NUEVO SR2
                st.setString(17, facturaEca.getCmc()+"2");//20100611//NUEVO SV2
                st.setString(18, facturaEca.getCmc()+"2");//20100611//NUEVO AS2
                st.setString(19, facturaEca.getCmc()+"2");//20100611//NUEVO FU2
                st.setString(20, facturaEca.getCmc()+"2");//20180530//NUEVO DC2
                st.setString(21, facturaEca.getCmc()+"2");//20180530//NUEVO RT2
                st.setString(22, facturaEca.getCmc()+"2");//20100611
                st.setString(23, facturaEca.getCmc()+"2");//20100611
                st.setString(24, facturaEca.getCmc()+"2");//20130321 
                st.setString(25, facturaEca.getCmc()+"2");//20130321 //NUEVO FP2
                st.setString(26, facturaEca.getCmc()+"2");//20130321 //NUEVO RA2
                st.setString(27, facturaEca.getCmc()+"1");//20180530//NUEVO RT2
                st.setString(28, facturaEca.getCmc()+"2");//20130321 //NUEVO CV2
                st.setString(29, facturaEca.getCmc()+"2");//20130321 //NUEVO SR2
                st.setString(30, facturaEca.getCmc()+"2");//20130321 //NUEVO SV2
                st.setString(31, facturaEca.getCmc()+"2");//20130321 //NUEVO AS2
                st.setString(32, facturaEca.getCmc()+"2");//20130321 //NUEVO FU2
                st.setString(33, facturaEca.getCmc()+"2");//20180530//NUEVO DC2

                st.setString(34, "residuo de recaudo del archivo "+de.getFecha()+" de la factura "+facturaEca.getDoc()+" "+"por el recaudo:"+de.getId());//20100611
                st.setDouble(35, (saldo_de-abono)/*facturaEca.getSaldo()*/);//20100611

                st.setString(36, facturaEca.getDoc());//20100811

                comando_sql = st.toString();
                respuesta.add(comando_sql);
            }


        }catch(Exception e){
            System.out.println("errorcit3:"+e.toString());
            e.printStackTrace();
            throw new SQLException("ERROR3 DURANTE LA CONFORMACION DEL COMANDO SQL PARA ACTUALIZAR FECHA FACTURA CONTRATISTA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(query1);
        }
        //.out.println("respuesta"+respuesta);
        return respuesta;
    }


    public String generarRe() throws SQLException{
        PreparedStatement st       = null;
        Connection        con      = null;
        String comando_sql = "";
        String query1="GENERAR_RE3";
        String respuesta="nada";
        String generare="";
        ResultSet rs=null;//20100610

        try {
            con   = this.conectar( query1 );
            generare=obtenerSQL( query1 );//
            st           = con.prepareStatement( generare );

            comando_sql = st.toString();

            //inicio de 20100610
            rs = st.executeQuery();

            if (rs.next()){
                respuesta=rs.getString(1);
            }
            //st.execute();
            //fin de 20100610

            //respuesta=comando_sql;
        }catch(Exception e){
            System.out.println("errorcit genera_re:"+e.toString());
            e.printStackTrace();
            throw new SQLException("ERROR3 DURANTE LA CONFORMACION DEL COMANDO SQL PARA GENERAR_RE3. \n " + e.getMessage());
        }finally{
            st.close();
            this.desconectar(query1);
        }
       return respuesta;
    }

    //fin de 20100602

}