/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.GarantiasCreditosAutommotor;
import com.tsp.operation.model.beans.Usuario;
import java.util.ArrayList;

/**
 *
 * @author edgargonzalezmendoza
 */
public interface GarantiasCreditosDAO {
    
    /**
     *
     * @param ruta
     * @param u
     * @param filename
     * @return
     * @throws Exception
     */
    public ArrayList<String> cargarArchivoPreProceso(String ruta,Usuario u,String filename)throws Exception;
    
    /**
     *
     * @param q
     * @return
     */
    public String cargarMarca(String q);
    
    /**
     *
     * @param parametro1
     * @param parametro2
     * @param marca
     * @param option
     * @return
     */
    public String cargarCombox(String parametro1,String parametro2,String marca, int option);
   
    /**
     *
     * @param marca
     * @param clase
     * @param ref1
     * @param ref2
     * @param ref3
     * @return
     */
    public  String infoFasecolda(String marca, String clase,String ref1,String ref2,String ref3);
    
    /**
     *
     * @param gca
     * @param user
     * @return
     */
    public  String guardarGarantiaAutomotor(GarantiasCreditosAutommotor gca, Usuario user);
    
    /**
     *
     * @param negocio
     * @param user
     * @return
     */
    public  String ListaGarantiaAutomotor(String negocio, Usuario user);
    
    /**
     *
     * @param gca
     * @param id
     * @param user
     * @return
     */
    public  String actualizarGarantias(GarantiasCreditosAutommotor gca,int id, Usuario user);
    
    /**
     *
     * @param usuario
     * @return
     */
    public String cargaraSeguradora(Usuario usuario);
    
    /**
     *
     * @param usuario
     * @return
     */
    public String visualizarPrecarga(Usuario usuario);
    
    /**
     *
     * @param usuario
     * @return
     */
    public String crearMaestrosFasecolda(Usuario usuario);
    
    /**
     *
     * @param usuario
     * @return
     */
    public String limpiarPrecargaFasecolda(Usuario usuario);
}
