/********************************************************************
 *      Nombre Clase.................   ConceptoDAO.java    
 *      Descripci�n..................   DAO de la tabla otros_conceptos    
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   27.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.Util;

public class ConceptoDAO {
        
        private Concepto concepto;
        private Vector conceptos;
        
        private static final String SQL_INSERT = "insert into otros_conceptos(reg_status, dstrct, tabla," +
                " descripcion, last_update, user_update, creation_date, creation_user, base) " +
                "values('',?,?,?,'now()',?,'now()',?,?)";
        
        private static final String SQL_UPDATE = "update otros_conceptos set reg_status=?," +
                " tabla=?, descripcion=?, last_update='now()', user_update=?" +
                " where tabla=? and descripcion=?";
        
        private static final String SQL_OBTENER = "select * from otros_conceptos where tabla=? and descripcion=?";
        
        private static final String SQL_LISTAR = "select * from otros_conceptos where reg_status<>'A'";
        
        /** Creates a new instance of ConceptoDAO */
        public ConceptoDAO() {
        }
        
        /**
         * Getter for property concepto.
         * @return Value of property concepto.
         */
        public com.tsp.operation.model.beans.Concepto getConcepto() {
                return concepto;
        }
        
        /**
         * Setter for property concepto.
         * @param concepto New value of property concepto.
         */
        public void setConcepto(com.tsp.operation.model.beans.Concepto concepto) {
                this.concepto = concepto;
        }
        
        /**
         * Getter for property conceptos.
         * @return Value of property conceptos.
         */
        public java.util.Vector getConceptos() {
                return conceptos;
        }
        
        /**
         * Setter for property conceptos.
         * @param conceptos New value of property conceptos.
         */
        public void setConceptos(java.util.Vector conceptos) {
                this.conceptos = conceptos;
        }

        /**
         * Inserta un nuevo registro
         * @autor Tito Andr�s Maturana
         * @throws SQLException
         * @version 1.0
         */
        public void insertar() throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;       
                PoolManager poolManager = null;
        
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection("fintra");
                        if (con != null){
                                st = con.prepareStatement(this.SQL_INSERT);
                                st.setString(1, concepto.getDistrito());
                                st.setString(2, concepto.getC_tabla());
                                st.setString(3, concepto.getC_descripcion());
                                st.setString(4, concepto.getUsuario_creacion());
                                st.setString(5, concepto.getUsuario_creacion());
                                st.setString(6, concepto.getBase());

                                st.executeUpdate();
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL INSERTAR EL CONCEPTO: " + e.getMessage()+"" + e.getErrorCode());
                }finally{
                        if(st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                                }
                        }
                        if ( con != null){
                                poolManager.freeConnection("fintra", con );
                        }
                }      
        }
        
        /**
         * Obtiene un registro nuevo registro
         * @autor Tito Andr�s Maturana
         * @param tabla Nombre de la tabla
         * @param descripcion Descripci�n del concepto
         * @throws SQLException
         * @version 1.0
         */
        public void obtener(String tabla, String descripcion) throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;       
                PoolManager poolManager = null;
                this.concepto = null;
        
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection("fintra");
                        if (con != null){
                                st = con.prepareStatement(this.SQL_OBTENER);
                                
                                st.setString(1, tabla);
                                st.setString(2, descripcion);
                                
                                rs = st.executeQuery();
                                
                                if( rs.next() ){
                                        this.concepto = new Concepto();
                                        this.concepto.Load(rs);
                                }
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL OBTENER EL CONCEPTO: " + e.getMessage()+"" + e.getErrorCode());
                }finally{
                        if(st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                                }
                        }
                        if ( con != null){
                                poolManager.freeConnection("fintra", con );
                        }
                }                  
                
        }

        /**
         * Actualiza un registro
         * @autor Tito Andr�s Maturana
         * @throws SQLException
         * @version 1.0
         */
        public void actualizar() throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;       
                PoolManager poolManager = null;
        
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection("fintra");
                        if (con != null){
                                st = con.prepareStatement(this.SQL_UPDATE);
                                                                
                                st.setString(1, this.concepto.getEstado());
                                st.setString(2, this.concepto.getNtabla());
                                st.setString(3, this.concepto.getNdescripcion());
                                st.setString(4, this.concepto.getUsuario_modificacion());
                                st.setString(5, this.concepto.getC_tabla());
                                st.setString(6, this.concepto.getC_descripcion());

                                st.executeUpdate();
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL ACTUALIZAR LA APLICACION: " + e.getMessage()+"" + e.getErrorCode());
                }finally{
                        if(st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                                }
                        }
                        if ( con != null){
                                poolManager.freeConnection("fintra", con );
                        }
                }      
        }
        
        /**
         * Lista los registros
         * @autor Tito Andr�s Maturana
         * @throws SQLException
         * @version 1.0
         */
        public void listar() throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;       
                PoolManager poolManager = null;
                this.concepto = null;
                this.conceptos = new Vector();
        
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection("fintra");
                        if (con != null){
                                st = con.prepareStatement(this.SQL_LISTAR);
                                                               
                                rs = st.executeQuery();
                                
                                while( rs.next() ){
                                        this.concepto = new Concepto();
                                        this.concepto.Load(rs);
                                        this.conceptos.add(this.concepto);
                                }
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL LISTAR LAS APLICACIONES: " + e.getMessage()+"" + e.getErrorCode());
                }finally{
                        if(st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                                }
                        }
                        if ( con != null){
                                poolManager.freeConnection("fintra", con );
                        }
                }                  
                
        }
        
}
