/************************************************************************
 * Nombre clase: Cumplidos_DocumentosDAO.java                           *
 * Descripci�n: Clase que maneja las consultas de los cumplidos.        *
 * Autor: Ing. Jose de la rosa                                          *
 * Fecha: 27 de octubre de 2005, 05:27 PM                               *
 * Versi�n: Java 1.0                                                    *
 * Copyright: Fintravalores S.A. S.A.                              *
 ***********************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  Jose
 */
public class Cumplidos_DocumentosDAO {
    private Cumplidos_documentos cumplidos_documentos;
    private Vector Cumplidos_DocumentosVec;  
    private static final String SQL_INSERT_CUMPLIDOS_DOCUMENTOS = "insert into cumplidos_documentos (numpla, numrem, document_type, std_job_no, dstrct, rec_status, last_update, user_update, creation_date, creation_user, base) values "+
        "(?,?,?,?,?,'','now()',?, 'now()',?,?)";
    private static final String SQL_EXISTE_CUMPLIDOS_DOCUMENTOS = "select * from cumplidos_documentos where numpla=? and numrem=? and document_type=? and std_job_no=? and dstrct=? and rec_status != 'A'";
    /** Creates a new instance of Cumplidos_DocumentosDAO */
    public Cumplidos_DocumentosDAO() {
    }
    public Cumplidos_documentos getCumplidos_Documento() {
        return cumplidos_documentos;
    }
    
    public void setCumplidos_Documento(Cumplidos_documentos codigo_discrepancia) {
        this.cumplidos_documentos = codigo_discrepancia;
    }
    
    public Vector getCumplidos_Documentos() {
        return Cumplidos_DocumentosVec;
    }
    
    public void setCumplidos_Documentos(Vector Cumplidos_DocumentosVec) {
        this.Cumplidos_DocumentosVec = Cumplidos_DocumentosVec;
    }
    
    public void insertCumplidos_Documentos() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_INSERT_CUMPLIDOS_DOCUMENTOS);
                st.setString(1,cumplidos_documentos.getNumpla());
                st.setString(2,cumplidos_documentos.getNumrem());
                st.setString(3,cumplidos_documentos.getDocument_type());
                st.setString(4,cumplidos_documentos.getStd_job_no());
                st.setString(5, cumplidos_documentos.getDistrito());
                st.setString(6, cumplidos_documentos.getUsuario_modificacion());
                st.setString(7, cumplidos_documentos.getUsuario_creacion());  
                st.setString(8, cumplidos_documentos.getBase());
                ////System.out.println(st+"****");
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DE CUMPLIDOS DOCUMENTOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }    
    public boolean existCumplidos_Documentos(String numpla, String numrem, String document_type, String std_job_no,String distrito) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_EXISTE_CUMPLIDOS_DOCUMENTOS);
                st.setString(1,numpla);
                st.setString(2,numrem);
                st.setString(3,document_type);
                st.setString(4,std_job_no);
                st.setString(5,distrito);
                rs = st.executeQuery();
                if (rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE CUMPLIDOS DOCUMENTOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
}
