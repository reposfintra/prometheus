package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import java.util.Date;

public class ElectricaribeOfertaDAO extends MainDAO {

    private DatosOferta oe = new DatosOferta();
    private String numOferta;
    private ArrayList<TablaItem> tisM;
    private ArrayList<TablaItem> tisD;
    private ArrayList<TablaItem> tisO;

    public ElectricaribeOfertaDAO(){
        super("ElectricaribeOfertaDAO.xml");
    }
    public ElectricaribeOfertaDAO(String dataBaseName){
        super("ElectricaribeOfertaDAO.xml", dataBaseName);
    }

    public void getEcaInfo() throws Exception{
        Connection          con      = null;
        PreparedStatement   ps       = null;
        ResultSet           rs       = null;
        String              query    = "SQL_OFERTA_INFO";
        String              sql      = "";

        try{
            oe          = null;
            oe          = new DatosOferta();

            con         = this.conectar(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, this.getNumOferta());
            rs          = ps.executeQuery();

            if(rs.next()){
                oe.setId_solicitud(this.getNumOferta());
                oe.setOferta(rs.getString("nombre_solicitud"));
                oe.setElaboradoPor(rs.getString("nombre_elaborado"));
                oe.setFechaGeneracion(rs.getString("fecha_oferta"));
                oe.setAprobadoPor(rs.getString("nombre_aprobado"));
                oe.setEjecutivo(rs.getString("nombre_ejecutivo"));
                oe.setCliente(rs.getString("nombre_cliente"));
                oe.setNIC(rs.getString("nic"));
                oe.setConsecutivo(rs.getString("consecutivo_oferta"));
                oe.setCiudad(rs.getString("ciudad"));
                oe.setDepartamento(rs.getString("departamento"));
                oe.setDireccion(rs.getString("direccion"));
                oe.setRepresentante(rs.getString("nombre_representante"));
                oe.setNIT(rs.getString("nit"));
                oe.setOtras_consideraciones(rs.getString("otras_consideraciones"));
                oe.setValorAgregado(rs.getString("noesvaloragregado"));
                oe.setTipo_cliente(rs.getString("tipo"));
                oe.setOficial(rs.getString("esoficial"));
                oe.setAviso(rs.getString("aviso"));
                oe.setTipo_solicitud(rs.getString("tipo_solicitud"));
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    public void getEcaInfoForPDF() throws Exception{
        Connection          con      = null;
        PreparedStatement   ps       = null;
        ResultSet           rs       = null;
        String              query    = "SQL_OFERTA_INFO_PDF";
        String              sql      = "";

        try{
            oe          = null;
            oe          = new DatosOferta();

            con         = this.conectar(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, this.getNumOferta());
            rs          = ps.executeQuery();

            if(rs.next()){
                oe.setId_solicitud(this.getNumOferta());
                oe.setOferta(rs.getString("nombre_solicitud"));
                oe.setElaboradoPor(rs.getString("nombre_elaborado"));
                oe.setFechaGeneracion(rs.getString("fecha_oferta"));
                oe.setAprobadoPor(rs.getString("nombre_aprobado"));
                oe.setEjecutivo(rs.getString("nombre_ejecutivo"));
                oe.setCliente(rs.getString("nombre_cliente"));
                oe.setNIC(rs.getString("nic"));
                oe.setConsecutivo(rs.getString("consecutivo_oferta"));
                oe.setCiudad(rs.getString("ciudad"));
                oe.setDepartamento(rs.getString("departamento"));
                oe.setDireccion(rs.getString("direccion"));
                oe.setRepresentante(rs.getString("nombre_representante"));
                oe.setNIT(rs.getString("nit"));
                oe.setOtras_consideraciones(rs.getString("otras_consideraciones"));
                oe.setValorAgregado(rs.getString("noesvaloragregado"));
                oe.setTipo_cliente(rs.getString("tipo"));
                oe.setOficial(rs.getString("esoficial"));
                oe.setAviso(rs.getString("aviso"));
                oe.setTipo_solicitud(rs.getString("tipo_solicitud"));
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    public ArrayList<AccionesEca> getAcciones() throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query   = "SQL_ACCIONES";
        String                  sql     = "";
        ArrayList<AccionesEca>  aecas   = new ArrayList<AccionesEca>();
        AccionesEca             aeca;

        try{
            con         = this.conectar(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, this.getNumOferta());
            rs          = ps.executeQuery();

            while(rs.next()){
                aeca = new AccionesEca();
                aeca.setId_accion(rs.getString("id_accion"));
                aeca.setDescripcion(rs.getString("descripcion"));
                aeca.setAlcance(rs.getString("alcances"));
                aeca.setAdministracion(rs.getString("administracion"));
                aeca.setImprevisto(rs.getString("imprevisto"));
                aeca.setUtilidad(rs.getString("utilidad"));
                aeca.setPorc_administracion(rs.getString("porc_administracion"));
                aeca.setPorc_imprevisto(rs.getString("porc_imprevisto"));
                aeca.setPorc_utilidad(rs.getString("porc_utilidad"));
                aecas.add(aeca);
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return aecas;
    }

    public ArrayList<MaterialEca> getMaterialesPorTipo(String accion, String tipo) throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query   = "SQL_TODOS_POR_TIPO";
        String                  sql     = "";
        ArrayList<MaterialEca>  mecas   = new ArrayList<MaterialEca>();
        MaterialEca             meca;

        try{
            DateFormat dateFormat = new SimpleDateFormat("yyyyMM");
            Date dat = new Date();

            con         = this.conectar(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, this.getNumOferta());
            ps.setString(2, accion);
            ps.setString(3, tipo);

            System.out.print("\n\n"+ ps.toString() +"\n\n");

            rs          = ps.executeQuery();

            while(rs.next()){
                meca = new MaterialEca();
                meca.setDescripcion(rs.getString("descripcion"));
                meca.setUnidad(rs.getString("medida"));
                meca.setCantidad(rs.getString("cantidad"));
                meca.setVlr_unitario(rs.getString("precio_venta"));
                meca.setVlr_total(rs.getString("valor_total"));
                mecas.add(meca);
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return mecas;
    }

    public ArrayList<TablaGen> getConsideraciones() throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query1   = "SQL_CONSIDERACIONES";
        String                  query2   = "SQL_CONSIDERACIONES2";
        String                  sql     = "";

        ArrayList<TablaGen>     consis  = new ArrayList<TablaGen>();
        TablaGen                consi;
        try{
            con         = this.conectar(query1);
            sql         = this.obtenerSQL(query1);
            ps          = con.prepareStatement(sql);
            ps.setString(1, this.getNumOferta());
            rs          = ps.executeQuery();

            while(rs.next()){
                consi = new TablaGen();
                consi.setTable_code(rs.getString("table_code"));
                consi.setDescripcion(rs.getString("descripcion"));
                consi.setDato("true");
                consis.add(consi);
            }

            this.desconectar(query1);

            con         = this.conectar(query2);
            sql         = this.obtenerSQL(query2);
            ps          = con.prepareStatement(sql);
            ps.setString(1, this.getNumOferta());
            rs          = ps.executeQuery();

            while(rs.next()){
                consi = new TablaGen();
                consi.setTable_code(rs.getString("table_code"));
                consi.setDescripcion(rs.getString("descripcion"));
                consi.setDato("false");
                consis.add(consi);
            }

        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query2); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return consis;
    }

    public ArrayList<TablaGen> getConsideracionesByOffer() throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query1   = "SQL_CONSIDERACIONES";
        String                  sql     = "";

        ArrayList<TablaGen>     consis  = new ArrayList<TablaGen>();
        TablaGen                consi;

        try{
            con         = this.conectar(query1);
            sql         = this.obtenerSQL(query1);
            ps          = con.prepareStatement(sql);
            ps.setString(1, this.getNumOferta());
            rs          = ps.executeQuery();

            while(rs.next()){
                consi = new TablaGen();
                consi.setTable_code(rs.getString("table_code"));
                consi.setDescripcion(rs.getString("descripcion"));
                consi.setDato("true");
                consis.add(consi);
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query1); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return consis;
    }

    public DatosOferta getOferta(){
        return oe;
    }

    public String getNumOferta(){
        return numOferta;
    }

    public String getOfferValue(String id_sol) throws Exception{

        Connection          con     = null;
        PreparedStatement   ps      = null;
        ResultSet           rs      = null;
        String              query1  = "SQL_GET_OFFER_VALUE";
        String              sql     = "";
        String              number  = "";

        try{
            con         = this.conectar(query1);
            sql         = this.obtenerSQL(query1);
            ps          = con.prepareStatement(sql);
            ps.setString(1, id_sol);
            System.out.print(ps.toString());
            rs          = ps.executeQuery();

            if(rs.next()){
                number = rs.getString("suma");
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              }   catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              }   catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query1); }  catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return number;
    }

    public float getIncremento(String periodo) throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query1  = "SQL_GET_INCREMENTO";
        String                  sql     = "";
        float                   number  = 0;

        try{
            con         = this.conectar(query1);
            sql         = this.obtenerSQL(query1);
            ps          = con.prepareStatement(sql);
            ps.setString(1, periodo);
            ps.setString(2, periodo);
            ps.setString(3, this.getNumOferta());
            rs          = ps.executeQuery();

            if(rs.next()){
                number = rs.getFloat("tasa");
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query1); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return number;
    }

    public String getMesesOficial() throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query1  = "SQL_GET_MESES";
        String                  sql     = "";
        String                  number  = "No tiene";

        try{
            con         = this.conectar(query1);
            sql         = this.obtenerSQL(query1);
            ps          = con.prepareStatement(sql);
            ps.setString(1, this.getNumOferta());
            System.out.print(ps.toString());
            rs          = ps.executeQuery();

            if(rs.next()){
                if(rs.getString("consecutivo_oferta").length() > 0){
                    number = rs.getString("esoficial");
                }
            }

        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query1); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return number;
    }

    public void setNumOferta(String numOferta) {
        this.numOferta = numOferta;
    }

    public ArrayList<TablaItem> getTisM() {
        return tisM;
    }

    public ArrayList<TablaItem> getTisD() {
        return tisD;
    }

    public ArrayList<TablaItem> getTisO() {
        return tisO;
    }

    /* Los procedimientos siguientes son para
     * la actualizacion de las ofertas
     */

    /**
     * Este metodo me verifica si la oferta es actualizable
     * dependiendo del estado.
     *
     * @param num
     * @return
     * @throws Exception
     */
    public boolean isOfferReady(String num) throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query   = "SQL_ACCIONES_TO_UPDATE";
        String                  sql     = "";
        boolean                 ok      = true;

        try{
            con = this.conectar(query);
            sql = this.obtenerSQL(query);
            ps  = con.prepareStatement(sql);
            ps.setString(1, num);
            rs  = ps.executeQuery();

            if(rs.next()){
                ok = false;
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return ok;
    }

    /**
     * Este metodo actualiza toda la informacion acerca de la oferta
     * incluyendo sus consideraciones y sus cotizaciones.
     *
     * @param oeca Es el bean de la oferta a modificar.
     * @param flag Indica si se va a reemplazar el consecutivo.
     * @throws Exception
     */
    public void actualizarOferta(String user, OfertaElca oeca, boolean flag) throws Exception{
        Connection con          = null;
        PreparedStatement st    = null;
        ResultSet rs            = null;
        String query            = "SQL_UPDATE_OFERTAS";
        String query5           = "SQL_GET_OPAV_NUM";
        String query6           = "SQL_GET_TIPO_OPAV";
        String query4           = "SQL_UPDATE_ACCIONES";
        String query1           = "SQL_DELETE_CONSIDERACIONES";
        String query2           = "SQL_INSERT_CONSIDERACIONES";
        String consid           = "";
        String sql              = "";
        String sqlGlobal        = "";
        String conseOpav        = "";

        try {

            con = this.conectar(query5);
            con.setAutoCommit(false);

            if(flag){
                st = con.prepareStatement(this.obtenerSQL(query5));
                rs = st.executeQuery();

                if(rs.next()){
                    conseOpav = rs.getString("numero");
                    conseOpav = "10.OPA" + conseOpav;
                }

                st = null;
                rs = null;
                st = con.prepareStatement(this.obtenerSQL(query6).replaceAll("param1", oeca.getId_solicitud()));
                rs = st.executeQuery();

                if(rs.next()){
                    conseOpav = conseOpav + rs.getString("tipo");
                }

                st = null;
                rs = null;
            }
            else{
                conseOpav = "none";
            }

            //-----------------------------------------------------------------------

            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("param1", oeca.getNombre_solicitud());
            sql = sql.replaceAll("param2", oeca.getOtras_consideraciones());
            sql = sql.replaceAll("param3", oeca.getId_solicitud());
            if(conseOpav.equals("none")){
                sql = sql.replaceAll(",consecutivo_oferta = 'param4'","");
            }
            else{
                sql = sql.replaceAll("param4", conseOpav);
            }
            sql = sql.replaceAll("param5", user);
            sql = sql.replaceAll("param6", oeca.getOficial());
            sqlGlobal += sql + ";";

            sql = this.obtenerSQL(query4);
            sql = sql.replaceAll("param1", oeca.getId_solicitud());
            sql = sql.replaceAll("param2", user);
            sqlGlobal += sql + ";";

            sql = this.obtenerSQL(query1);
            sql = sql.replaceAll("param1", oeca.getId_solicitud());
            sqlGlobal += sql + ";";

            if(oeca.getConsideraciones().length()>0){
                consid = oeca.getConsideraciones();
                StringTokenizer tokens = new StringTokenizer(consid, ",");
                while (tokens.hasMoreTokens()) {
                    sql = this.obtenerSQL(query2);
                    sql = sql.replaceAll("param1", oeca.getId_solicitud());
                    sql = sql.replaceAll("param2", tokens.nextToken());
                    sqlGlobal += sql + ";";
                }
            }

            DateFormat dateFormat = new SimpleDateFormat("yyyyMM");
            Date dat = new Date();



            sqlGlobal += actPrecio_venta(oeca.getId_solicitud(),dateFormat.format(dat));

            System.out.print("\n\n"+sqlGlobal+"\n\n");

            st  = con.prepareStatement(sqlGlobal);
            st.executeUpdate(sqlGlobal);

            con.commit();
        }
        catch (SQLException e) {
            con.rollback();
            System.out.println("\n\nROLLBACK...");
            throw new SQLException("\n\nERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS:\n" + e.getMessage() + "La siguiente exception es : ----> " + e.getNextException());
        }
        finally {
            if (st  != null){ try{ st.close();                  } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query5);    } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    /* Los procedimientos siguientes son para
     * la anulacion de las ofertas
     */

    public boolean isAnulable(String num) throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query   = "SQL_ACCIONES_TO_ANULAR";
        String                  sql     = "";
        boolean                 ok      = true;

        try{
            con = this.conectar(query);
            sql = this.obtenerSQL(query);
            ps  = con.prepareStatement(sql);
            ps.setString(1, num);
            rs  = ps.executeQuery();

            if(rs.next()){
                ok = false;
            }
        }
        catch(Exception e){
            System.out.println("error en isAnulable"+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return ok;
    }

    public String actualizarAnulacionOferta(OfertaElca oeca, boolean flag,String userx) throws Exception{
        String respuesta="Denegacion pendiente.";
        Connection con          = null;
        PreparedStatement st    = null;
        ResultSet rs            = null;
        String query            = "SQL_ANULAR_OFERTAS";
        //String query5           = "SQL_GET_OPAV_NUM";
        //String query6           = "SQL_GET_TIPO_OPAV";
        String query4           = "SQL_DENEGAR_ACCIONES";
        String query1           = "SQL_DELETE_RAZONES_ANULACIONES";
        String query2           = "SQL_INSERT_RAZONES_ANULACIONES";
        //String query3           = "SQL_UPDATE_COTDETS";
        String consid           = "";
        String sql              = "";
        String sqlGlobal        = "";
        String conseOpav        = "";

        try {

            con = this.conectar(query);
            con.setAutoCommit(false);

            //-----------------------------------------------------------------------

            sql = this.obtenerSQL(query);
            //sql = sql.replaceAll("param1", oeca.getNombre_solicitud());
            sql = sql.replaceAll("param2", oeca.getOtras_consideraciones());
            sql = sql.replaceAll("param3", oeca.getId_solicitud());
            sql = sql.replaceAll("param5", userx);
            /*if(conseOpav.equals("none")){
                sql = sql.replaceAll(",consecutivo_oferta = 'param4'","");
            }
            else{
                sql = sql.replaceAll("param4", conseOpav);
            } */
            sqlGlobal += sql + ";";

            sql = this.obtenerSQL(query4);
            sql = sql.replaceAll("param1", oeca.getId_solicitud());
            sql = sql.replaceAll("param5", userx);
            //.out.println("sql::"+sql);
            sqlGlobal += sql + ";";

            sql = this.obtenerSQL(query1);
            sql = sql.replaceAll("param1", oeca.getId_solicitud());
            sqlGlobal += sql + ";";

            if(oeca.getConsideraciones().length()>0){
                consid = oeca.getConsideraciones();
                StringTokenizer tokens = new StringTokenizer(consid, ",");
                while (tokens.hasMoreTokens()) {
                    sql = this.obtenerSQL(query2);
                    sql = sql.replaceAll("param1", oeca.getId_solicitud());
                    sql = sql.replaceAll("param2", tokens.nextToken());
                    sql = sql.replaceAll("param5", userx);
                    sqlGlobal += sql + ";";
                }
            }

            /*sql = this.obtenerSQL(query3);
            sql = sql.replaceAll("param1", oeca.getId_solicitud());
            sqlGlobal += sql + ";";*/

            st  = con.prepareStatement(sqlGlobal);
            st.executeUpdate(sqlGlobal);

            con.commit();
            respuesta="Oferta denegada.";
        }
        catch (SQLException e) {
            System.out.println("errorsql en anular oferta: "+e.toString()+"__"+e.getMessage());
            con.rollback();
            System.out.println("\n\nROLLBACK...");
            throw new SQLException("\n\nERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS:\n" + e.getMessage() + "La siguiente exception es : ----> " + e.getNextException());
        }catch(Exception ee){
            System.out.println("error en anular oferta: "+ee.toString()+"__"+ee.getMessage());
        }finally {
            if (st  != null){ try{ st.close();                  } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query);    } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;
    }

    public DatosOferta ofertaInfoAnul(String num) throws Exception{
        Connection          con      = null;
        PreparedStatement   ps       = null;
        ResultSet           rs       = null;
        String              query    = "SQL_OFERTA_INFO";
        String              sql      = "";
        DatosOferta oe2=new DatosOferta();
        try{
            oe2          = null;
            oe2          = new DatosOferta();

            con         = this.conectar(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, num);
            rs          = ps.executeQuery();

            if(rs.next()){
                oe2.setId_solicitud(num);
                oe2.setOferta(rs.getString("nombre_solicitud"));
                oe2.setElaboradoPor(rs.getString("nombre_elaborado"));
                oe2.setFechaGeneracion(rs.getString("fecha_oferta"));
                oe2.setAprobadoPor(rs.getString("nombre_aprobado"));
                oe2.setEjecutivo(rs.getString("nombre_ejecutivo"));
                oe2.setCliente(rs.getString("nombre_cliente"));
                oe2.setNIC(rs.getString("nic"));
                oe2.setConsecutivo(rs.getString("consecutivo_oferta"));
                oe2.setCiudad(rs.getString("ciudad"));
                oe2.setDepartamento(rs.getString("departamento"));
                oe2.setDireccion(rs.getString("direccion"));
                oe2.setRepresentante(rs.getString("nombre_representante"));
                oe2.setNIC(rs.getString("nit"));
                oe2.setOtras_consideraciones(rs.getString("otras_anulaciones"));
            }
        }
        catch(Exception e){
            System.out.println("errore en ofertaInfoAnul"+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return oe2;
    }

    public ArrayList<TablaGen> getAnulaciones(String num_of) throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query1   = "SQL_ANULACIONES";
        String                  query2   = "SQL_ANULACIONES2";
        String                  sql     = "";

        ArrayList<TablaGen>     anuls  = new ArrayList<TablaGen>();
        TablaGen                anul;
        try{
            try{
                con         = this.conectar(query1);
                sql         = this.obtenerSQL(query1);
                ps          = con.prepareStatement(sql);
                ps.setString(1, num_of);
                //.out.println("ps1"+ps.toString());
                rs          = ps.executeQuery();

                while(rs.next()){
                    anul = new TablaGen();
                    anul.setTable_code(rs.getString("table_code"));
                    anul.setDescripcion(rs.getString("descripcion"));
                    anul.setDato("true");
                    anuls.add(anul);
                }

            }finally{
                this.desconectar(query1);
            }
            con         = this.conectar(query2);
            sql         = this.obtenerSQL(query2);
            ps          = con.prepareStatement(sql);
            ps.setString(1, num_of);
            ////.out.println("ps2"+ps.toString());
            rs          = ps.executeQuery();

            while(rs.next()){
                anul = new TablaGen();
                anul.setTable_code(rs.getString("table_code"));
                anul.setDescripcion(rs.getString("descripcion"));
                anul.setDato("false");
                anuls.add(anul);
            }

        }
        catch(Exception e){
            System.out.println("error en getAnulaciones"+e.toString());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query2); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return anuls;
    }

    public void actualizarCotizacion(String id_solicitud) throws Exception{
        Connection          con         = null;
        PreparedStatement   ps          = null;
        ResultSet           rs          = null;
        String              query       = "SQL_UPDATE_COTDETS";
        DateFormat          dateFormat  = new SimpleDateFormat("yyyyMM");
        Date                dat         = new Date();
        String              sql         = "";

        try{
            con         = this.conectarJNDI(query);
            sql         = this.obtenerSQL(query);
            sql = sql.replaceAll("param1", id_solicitud);
            sql = sql.replaceAll("param2", dateFormat.format(dat));
            ps          = con.prepareStatement(sql);

            System.out.println("\n\n"+sql);

            ps.executeUpdate();
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con);   } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    public String actPrecio_venta(String id_solicitud,String fecha)throws Exception{
            String query3           = "SQL_UPDATE_COTDETS";
            String sql = this.obtenerSQL(query3);
            sql = sql.replaceAll("param1", id_solicitud);
            sql = sql.replaceAll("'param2'", fecha);
            return sql + ";";
    }
}