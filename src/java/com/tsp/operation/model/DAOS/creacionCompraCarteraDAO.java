/*
 * creacionCompraCarteraDAO.java
 *
 * Created on 1 de enero de 2008, 05:55 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.tsp.operation.model.DAOS;

import java.sql.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
import java.io.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.lang.*;

/**
 *
 * @author NAVI
 */
public class creacionCompraCarteraDAO extends MainDAO  {
    
    /** Creates a new instance of creacionCompraCarteraDAO */
    public creacionCompraCarteraDAO() {
        super("creacionCompraCarteraDAO.xml");
        //System.out.println("despues de super en dao");
    }
    public creacionCompraCarteraDAO(String dataBaseName) {
        super("creacionCompraCarteraDAO.xml", dataBaseName);
        //System.out.println("despues de super en dao");
    }
    
    
    public ArrayList obtenerDatosChequeCartera(String fecha_consignacion1,String fechainicio1,String proveedor1,String valor1,String porte,String nit1,String modalidadRemesa1) throws SQLException{
        
        double remesa_minima=8655.0;
        
        ArrayList respuesta=new ArrayList();
        String dias="";
        String tasanominal="";
        String tasaefectiva="";
        String factor="";
        String valorsincustodia="";
        String descuento="";
        String custodia="";
        String porcentaje_remesa="";
        String remesa="";
        String valorgirable="";
        String valorgirablesinremesaniporte="";
        String nombre_proveedor="",nombre_nit="";
        PreparedStatement ps = null;
        ResultSet rs = null;
        //System.out.println("antes de ps");
        try{
            ps = this.crearPreparedStatement("SQL_OBTENER_DATOS_CHEQUE_CARTERA");
            //System.out.println("despues de ps");            
            ps.setString(1, fecha_consignacion1);
            ps.setString(2, fechainicio1); 
            ps.setString(3, nit1);
            ps.setString(4, proveedor1);
            //System.out.println("fecha_consignacion1"+fecha_consignacion1);
            //System.out.println("fechainicio1"+fechainicio1);
            //System.out.println("proveedor"+proveedor1);
            //System.out.println("nit1"+nit1);
            ////System.out.println("query clientes: "+ps);
            rs = ps.executeQuery();
           
            if (rs.next()) {
                dias=rs.getString(1);
                tasanominal=rs.getString(2);
                //System.out.println("tasanominal"+tasanominal);
                tasaefectiva=""+((Math.pow(1.0+(Double.parseDouble(tasanominal)/100.0),12))-1);
                //System.out.println("tasaefectiva"+tasaefectiva);
                
                factor=""+(1.0/(Math.pow((1.0+Double.parseDouble(tasaefectiva)),(Double.parseDouble(dias)/365.0))));
                
                //System.out.println("factor"+factor);
                valorsincustodia=""+(Double.parseDouble(factor)*Double.parseDouble(valor1));
                descuento=""+(Double.parseDouble(valor1)-Double.parseDouble(valorsincustodia));
                custodia=rs.getString(3);
                porcentaje_remesa=rs.getString(4);                
                
                nombre_proveedor=rs.getString(5);
                nombre_nit=rs.getString(6);
                //System.out.println("nombre_nit"+nombre_nit);
                if (modalidadRemesa1 != null && !(modalidadRemesa1.equals("2"))){
                    remesa=""+((Double.parseDouble(porcentaje_remesa)/100.0)*Double.parseDouble(valor1));
                    
                    if (Double.parseDouble(remesa)<remesa_minima){
                        remesa=""+remesa_minima;
                    }                    
                    
                }else{
                    remesa="0";
                }
                valorgirablesinremesaniporte=""+(Double.parseDouble(valorsincustodia)-Double.parseDouble(custodia));
                //System.out.println("valorgirablesinremesaniporte="+valorgirablesinremesaniporte);
                valorgirable=""+(Double.parseDouble(valorsincustodia)-Double.parseDouble(custodia)-Double.parseDouble(remesa)-Double.parseDouble(porte));
                respuesta.add(dias);   
                //respuesta.add(tasaefectiva);             
                respuesta.add(factor);   
                respuesta.add(valorsincustodia);  
                respuesta.add(descuento);
                respuesta.add(custodia); 
                respuesta.add(remesa);
                respuesta.add(valorgirable);
                respuesta.add(valorgirablesinremesaniporte);
                respuesta.add(tasanominal);
                
                respuesta.add(""+(Double.parseDouble(tasaefectiva))*100.0);
                respuesta.add(porcentaje_remesa);
                respuesta.add(nombre_proveedor);
                respuesta.add(nombre_nit);
                
            }
        }
        catch(Exception e){
            System.out.println("error al obtener dias" +e.toString()+"...."+e.getMessage());
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR CLIENTES" + e.getMessage());            
        }
        finally{
            if ( ps != null ){
                try {ps.close();}catch(SQLException ex){}
            }
            desconectar("SQL_OBTENER_DATOS_CHEQUE_CARTERA");
        }
        
        return respuesta;
    }
    
    public String aceptarConjuntoDeCheques(ArrayList cheques,String fechadesembolso1,Usuario usuario,String total_valorgirable,String modalidadcustodia,String modalidadremesa,String total_valor_cheque,String tpr,String tdes,String tipodocx)throws SQLException{
        
        //System.out.println("aceptarConjuntoDeCheques en dao");
        String respuesta="";
        String cod_negocio="";
        //se inserta el negocio
        PreparedStatement st = null;
        chequeCartera chc;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String usuari=usuario.getLogin();
        String distrit=usuario.getDstrct();
        //System.out.println("usuario"+usuari);
        //System.out.println("distrit"+distrit);
        try{
            
            ps = this.crearPreparedStatement("SQL_NUEVO_CODIGO_NEGOCIO");
            ////System.out.println("query clientes: "+ps);
            rs = ps.executeQuery();
            if (rs.next()) {
                cod_negocio=rs.getString(1);
            }            
        }catch(Exception e){
            System.out.println("error al obtener nuevo codigo de negocio" +e.toString()+"...."+e.getMessage());
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR CLIENTES" + e.getMessage());            
        }
        finally{
            if ( ps != null ){
                try {ps.close();}catch(SQLException ex){}
            }
            desconectar("SQL_NUEVO_CODIGO_NEGOCIO");
        }
        
        
        try{
            chc=(chequeCartera)cheques.get(0);
            
            st = crearPreparedStatement ("SQL_INSERTAR_NEGOCIO_CABECERA_DE_CHEQUES");
            
            st.setString (1,chc.getNit());
            st.setString (2,fechadesembolso1);
            st.setString (3,""+cheques.size());
            //System.out.println("total_valorgirable"+total_valorgirable);
            double valgir=Util.roundByDecimal(Double.parseDouble( total_valorgirable ),2);
            //System.out.println("valgir"+valgir);
            st.setDouble (4, valgir);
            //System.out.println("chc.getCustodia() "+chc.getCustodia() );
            double custo=Util.roundByDecimal(Double.parseDouble( chc.getCustodia() ),2);
            //System.out.println("custo"+custo);
            st.setDouble (5, custo);
            
            if (modalidadcustodia !=null && modalidadcustodia.equals("0")){
                st.setString (6,"1");
            }else{
                st.setString (6,"0");
            }
            
            //System.out.println("chc.getPorcentajeRemesa()"+chc.getPorcentajeRemesa());
            double porcrem=Util.roundByDecimal(Double.parseDouble( chc.getPorcentajeRemesa() )/100.0,5);
            //System.out.println("porcrem"+porcrem);
            if (modalidadremesa.equals("2")){                
                st.setDouble(7,Double.parseDouble("0"));
                st.setString(9,"0");
            }else{
                st.setDouble (7,porcrem);
                st.setString(9,"1");
            }
            if (modalidadremesa.equals("2")){
                st.setString (8,"1");
            }else{
                st.setString (8,"0");
            }
                        
            st.setString (10,usuari);
            st.setString (11,usuari);
            st.setString (12,chc.getProveedor());
            st.setString (13,cod_negocio);
            //System.out.println("total_valor_cheque"+total_valor_cheque);
            double totval=Util.roundByDecimal(Double.parseDouble( total_valor_cheque ),2);
            //System.out.println("totval"+totval);
            
            if (tipodocx!=null){
                tipodocx="02";
            }else{
                tipodocx="01";
            }
            st.setString (14,tipodocx);
            
            st.setDouble (15,totval); 
            
            st.setString (16,"02");
            
            st.setString (17,fechadesembolso1);
            
            st.setString (18,tpr);
            
            st.setString (19,tdes);
            
            if (modalidadremesa.equals("0")){
                st.setString (20,"1");
            }
            if (modalidadremesa.equals("1")){
                st.setString (20,"0");
            }
            if (modalidadremesa.equals("2")){
                st.setString (20,"");
            }
            st.setString(21, ""+(Math.pow((Double.parseDouble(chc.getTasaNominal())/100.0)+1,(0.083333333333))-1.0)*100.0);
            
            st.setString(22,(""+chc.getTasaEfectiva()).substring(0,6));
            
            st.executeUpdate ();
        }catch(SQLException ex){
            System.out.println("ex"+ex.toString());
            throw new SQLException ("ERROR AL INSERTAR NEGOCIO CON CHEQUES,  "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_INSERTAR_NEGOCIO_CABECERA_DE_CHEQUES");
        }

        int i =0;
        try{
            i =0;
            
            while (i<cheques.size()){
                chc=(chequeCartera)cheques.get(i);
            
                st = crearPreparedStatement ("SQL_INSERTAR_CHEQUE_DE_NEGOCIO");
                //cod_negocio=chc.getCodigoNegocio();
                st.setString (1,cod_negocio);
                //System.out.println("cod_negocio"+cod_negocio);
                st.setString (2,chc.getItem());
                //System.out.println("getItem"+chc.getItem());
                st.setString (3,chc.getFechaConsignacion());
                st.setDouble(4,Double.parseDouble(chc.getValor()));
                st.setDouble(5,Double.parseDouble(chc.getPorte()));
                st.setDouble(6,Double.parseDouble(chc.getDias()));
                st.setDouble(7,Double.parseDouble(chc.getFactor()));
                
                st.setDouble(8,Double.parseDouble(chc.getRemesa()));
                                
                st.setDouble(9,Double.parseDouble(chc.getDescuento()));
                st.setDouble(10,Double.parseDouble(chc.getValorSinCustodia()));
                st.setDouble(11,Double.parseDouble(chc.getCustodia()));
                
                st.setDouble(12,Double.parseDouble(chc.getValorGirable()));
                
                st.setDouble(13,Double.parseDouble(chc.getTasaEfectiva()));
                //System.out.println("chc.getTasaEfectiva()"+chc.getTasaEfectiva());
                st.setDouble(14,Double.parseDouble(chc.getTasaNominal()));
                //System.out.println("chc.getFechaCheque()"+chc.getFechaCheque());
                st.setString (15,chc.getFechaCheque());
                
                //se inserta 1 cheque
                st.executeUpdate ();
            
                i=i+1;
            } 
                
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL INSERTAR CHEQUES,  "+ex.toString()+",   "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_INSERTAR_CHEQUE_DE_NEGOCIO");
        }
        respuesta="ok";
        return respuesta;
    }
    
    public ArrayList obtenerCheques(String codigo_negocio1)throws SQLException{
        ArrayList respuesta=new ArrayList();
        String dias="";
        String tasanominal="";
        String tasaefectiva="";
        String factor="";
        String valorsincustodia="";
        String descuento="";
        String custodia="";
        String porcentaje_remesa="";
        String remesa="";
        String valorgirable="";
        String valorgirablesinremesaniporte="";
        String fechacheque="",fechaconsignacion="",porte="",valor="",nit="",proveedor="",fechainicio="";
        String item="";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String codigo_negocio=codigo_negocio1;
        String mod_remesa="",mod_custodia="";
        String nombre_pro="",nombre_cli="";
        String tipodocx="";
        String mod_avalx="";
        try{
            ps = this.crearPreparedStatement("SQL_OBTENER_CHEQUES_DE_NEGOCIO");
            
            ps.setString(1, codigo_negocio);//ojooooooooooooooo
            
            ////System.out.println("query clientes: "+ps);
            rs = ps.executeQuery();
           
            while (rs.next()) {
                codigo_negocio=rs.getString(1);
                item=rs.getString(2);
                fechaconsignacion=rs.getString(3);
                valor=rs.getString(4);
                porte=rs.getString(5);
                dias=rs.getString(6);
                
                factor=rs.getString(7);
                valorsincustodia=rs.getString(10);
                descuento=rs.getString(9);
                custodia=rs.getString(11);
                nit=rs.getString(16);
                remesa=rs.getString(8);
                valorgirable=rs.getString(12);
                fechacheque=rs.getString(15);
                valorgirablesinremesaniporte="";
                tasanominal=rs.getString(14);
                tasaefectiva=rs.getString(13);
                fechainicio=rs.getString(17);
                mod_remesa=rs.getString(18);
                mod_custodia=rs.getString(19);
                
                nombre_pro=rs.getString(20);
                nombre_cli=rs.getString(21);
                porcentaje_remesa="";
                porcentaje_remesa=rs.getString(22);
                proveedor=rs.getString(23);
                tipodocx=rs.getString(24);
                mod_avalx=rs.getString(25);
                //respuesta.add(tasaefectiva);   
                chequeCartera chc =new chequeCartera(item,fechacheque,fechaconsignacion,porte,valor,nit,proveedor,fechainicio,dias, factor, descuento, valorsincustodia, custodia, remesa, valorgirable,valorgirablesinremesaniporte,tasanominal,tasaefectiva,codigo_negocio,porcentaje_remesa,nombre_pro,nombre_cli,mod_remesa,mod_custodia,tipodocx,mod_avalx);
                                                    //item,fechacheque,fechaconsignacion,porte,valor,nit,proveedor,fechainicio,dias, factor, descuento,  valorsincustodia, custodia, remesa, valorgirable,valorgirablesinremesaniporte,tasanominal,tasaefectiva,codigo_negocio,porcentaje_remesa,nombre_proveedor,nombre_nit);
                respuesta.add(chc);   
                
                  
            }
        }
        catch(Exception e){
            System.out.println("error al obtener cheques de negocio" +e.toString()+"...."+e.getMessage());
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR CLIENTES" + e.getMessage());            
        }
        finally{
            if ( ps != null ){
                try {ps.close();}catch(SQLException ex){}
            }
            desconectar("SQL_OBTENER_CHEQUES_DE_NEGOCIO");
        }
        return respuesta;    
    }
    
    public String obtenerPorte()throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        String respuesta="";
        try{
            ps = this.crearPreparedStatement("SQL_OBTENER_PORTE");
                       
            rs = ps.executeQuery();
           
            if (rs.next()) {
                respuesta=rs.getString(1);
            }
        }
        catch(Exception e){
            System.out.println("error al obtener porte" +e.toString()+"...."+e.getMessage());
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR CLIENTES" + e.getMessage());            
        }
        finally{
            if ( ps != null ){
                try {ps.close();}catch(SQLException ex){}
            }
            desconectar("SQL_OBTENER_PORTE");
        }
        return respuesta;  
    }
}
