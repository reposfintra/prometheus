/***************************************
 * Nombre Clase ............. AdicionAjustesPlanillasDAO.java
 * Descripci�n  .. . . . . .  Generamos SQL para adicionar Ajustes
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  05/12/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/





package com.tsp.operation.model.DAOS;



import java.io.*;
import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;

public class AdicionAjustesPlanillasDAO extends MainDAO{
    
    
    public AdicionAjustesPlanillasDAO() {
        super("AdicionAjustesPlanillasDAO.xml");
    }
    public AdicionAjustesPlanillasDAO(String dataBaseName) {
        super("AdicionAjustesPlanillasDAO.xml", dataBaseName);
    }
    
    
    
    
    public String reset(String val){ return (val==null)?"":val; }
    
    
    
     
      /**
       * M�todo que obtiene datos de la planilla
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  Ajuste searchPlanilla(String distrito, String oc) throws Exception{
           
            PreparedStatement  st       = null;
            ResultSet          rs       = null;
            String             query    = "SQL_DATOS_PLANILLA";
            Ajuste             ajuste   = null;
            
            try{
                
                st= crearPreparedStatement(query);
                st.setString(1, oc       );
                st.setString(2, distrito );
                rs=st.executeQuery();
                if(rs.next()){
                   ajuste  =  new  Ajuste();
                   
                   ajuste.setDistrito       (   reset( rs.getString("cia")        )   );                   
                   ajuste.setPlanilla       (   reset( rs.getString("numpla")     )   );
                   ajuste.setPlaca          (   reset( rs.getString("plaveh")     )   );
                   ajuste.setOrigen         (   reset( rs.getString("origen")     )   );
                   ajuste.setDestino        (   reset( rs.getString("destino")    )   );                   
                   ajuste.setConductor      (   reset( rs.getString("cedcon")     )   );
                   ajuste.setNameConductor  (   reset( rs.getString("conductor")  )   );                   
                   ajuste.setPropietario    (   reset( rs.getString("nitpro")     )   );
                   ajuste.setNamePropietario(   reset( rs.getString("propietrio") )   );
                   ajuste.setFactura        (   reset( rs.getString("factura")    )   );
                   ajuste.setEstado         (   reset( rs.getString("reg_status") )   );
                   ajuste.setValor          (          rs.getDouble("valor")          );
                   ajuste.setMoneda         (   reset( rs.getString("currency")   )   );
                   ajuste.setBase           (   reset( rs.getString("base")       )   );
                            
                   
                }
                
                
            }catch(Exception e){
                  throw new Exception( "searchPlanilla " + e.getMessage());
            }
            finally{
                if(st!=null) st.close();
                if(rs!=null) rs.close();
                this.desconectar(query);
            }
            return ajuste; 
      }
      
      
      
      
      
      
      
      
       /**
       * M�todo que lista de Extrafletes
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  List loadExtrafletes() throws Exception{           
            PreparedStatement  st       = null;
            ResultSet          rs       = null;
            String             query    = "SQL_LOAD_EXTRAFLETES";
            List               lista    = new LinkedList();
            
            try{
                
                st= crearPreparedStatement(query);
                rs=st.executeQuery();
                while(rs.next()){
                   Hashtable  hc =  new Hashtable();
                     hc.put("codigo",           reset(  rs.getString("codextraflete")   )  );
                     hc.put("descripcion",      reset(  rs.getString("descripcion")     )  );   
                     hc.put("aplica",           reset(  rs.getString("ind_application") )  );
                   lista.add(hc);                   
                }                
                
            }catch(Exception e){
                  throw new Exception( "loadExtrafletes " + e.getMessage());
            }
            finally{
                if(st!=null) st.close();
                if(rs!=null) rs.close();
                this.desconectar(query);
            }
            return lista; 
      }
      
      
      
      
      /**
       * M�todo que lista las Monedas
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  List loadMonedas() throws Exception{           
            PreparedStatement  st       = null;
            ResultSet          rs       = null;
            String             query    = "SQL_MONEDAS";
            List               lista    = new LinkedList();            
            try{
                
                st= crearPreparedStatement(query);
                rs=st.executeQuery();
                while(rs.next()){
                   Hashtable  hc =  new Hashtable();
                     hc.put("codigo",      reset( rs.getString("codmoneda")   )  );
                     hc.put("descripcion", reset( rs.getString("nommoneda")   )  );                   
                   lista.add(hc);                   
                }
                
            }catch(Exception e){
                  throw new Exception( "loadMonedas " + e.getMessage());
            }
            finally{
                if(st!=null) st.close();
                if(rs!=null) rs.close();
                this.desconectar(query);
            }
            return lista; 
      }
      
      
      
      
      
       /**
       * M�todo que busca la moneda Local dependiendo el distrito, defult peso.
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  String getMonedaLocal( String distrito ) throws Exception{           
            PreparedStatement  st       = null;
            ResultSet          rs       = null;
            String             query    = "SQL_MONEDA_LOCAL";
            String             moneda   = "PES";
            try{
                
                st= crearPreparedStatement(query);
                st.setString(1, distrito );
                rs=st.executeQuery();
                if( rs.next() )
                    moneda =  rs.getString("moneda");
                
            }catch(Exception e){
                  throw new Exception( "getMonedaLocal " + e.getMessage());
            }
            finally{
                if(st!=null) st.close();
                if(rs!=null) rs.close();
                this.desconectar(query);
            }
            return moneda; 
      }
      
      
      
      
      
      
      
       /**
       * M�todo que lista de Movimientos de la Planilla
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  List loadMovPlanilla(String distrito, String oc) throws Exception{
           
            PreparedStatement  st       = null;
            ResultSet          rs       = null;
            String             query    = "SQL_MOVS_PLANILLA";
            List               lista    = new LinkedList();
            
            try{
                
                st= crearPreparedStatement(query);
                st.setString(1, oc       );
                st.setString(2, distrito );
                rs=st.executeQuery();
                while(rs.next()){
                    MoviOC  mov = new  MoviOC();
                       mov.setConcepto       (  reset( rs.getString("concept_code")     )   );
                       mov.setDescripcion    (  reset( rs.getString("concept_desc")     )   );
                       mov.setAplica         (  reset( rs.getString("application_ind")  )   );
                       mov.setIndicador_valor(  reset( rs.getString("ind_vlr")          )   );
                       mov.setValor          (         rs.getDouble("vlr")                  );
                       mov.setValor_me       (         rs.getDouble("vlr_for")              );
                       mov.setMoneda         (  reset( rs.getString("currency")         )   );                       
                       mov.setProveedor      (  reset( rs.getString("proveedor_anticipo") ) );
                       mov.setNameProveedor  (  reset( rs.getString("nombreProveedor")  )   );
                       mov.setFecha          (  reset( rs.getString("creation_date")    )   );
                       mov.setUsuario        (  reset( rs.getString("creation_user")    )   );
                       mov.setFactura        (  reset( rs.getString("factura")          )   );
                       
                       if(  mov.getIndicador_valor().equals("P") &&  mov.getValor_me()==0  )
                            mov.setValor_me( mov.getValor() );
                       
                       mov.setVlrReal        ( mov.getValor()    );
                       
                                           
                    lista.add(mov);                   
                    
                }
                
                
            }catch(Exception e){
                  throw new Exception( "loadMovPlanilla " + e.getMessage());
            }
            finally{
                if(st!=null) st.close();
                if(rs!=null) rs.close();
                this.desconectar(query);
            }
            return lista; 
      }
      
      
      
      
      
      
      /**
       * M�todo que permite adicionar ajuste a la planilla
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  String adicionarAjuste(      
                                      String dstrct,
                                      String agency_id,
                                      String document_type ,
                                      String concept_code ,
                                      String pla_owner ,
                                      String planilla ,
                                      String supplier ,  
                                      String applicated_ind  ,    
                                      String application_ind ,    
                                      String ind_vlr ,            
                                      double vlr_disc  ,          
                                      double vlr ,                
                                      double vlr_for ,            
                                      String currency  ,            
                                      String creation_user ,      
                                      String proveedor_anticipo , 
                                      String base   
                                      
                                    ) throws Exception{           
            PreparedStatement  st       = null;
            String             query    = "SQL_INSERT_AJUSTE";  
            String             msj      = "Ajuste aplicado exitosamente...";
            try{
                
                st= crearPreparedStatement(query);
                  st.setString(1,  dstrct             );
                  st.setString(2,  agency_id          );
                  st.setString(3,  document_type      );
                  st.setString(4,  concept_code       );
                  st.setString(5,  pla_owner          );
                  st.setString(6,  planilla           );
                  st.setString(7,  supplier           );
                  st.setString(8,  applicated_ind     );
                  st.setString(9,  application_ind    );
                  st.setString(10, ind_vlr            );
                  st.setDouble(11, vlr_disc           );
                  st.setDouble(12, vlr                );
                  st.setDouble(13, vlr_for            );
                  st.setString(14, currency           );
                  st.setString(15, creation_user      );
                  st.setString(16, proveedor_anticipo );
                  st.setString(17, base               );
                  
               st.execute();
                
                
            }catch(Exception e){
                 msj = "ERROR : " + e.getMessage();
            }
            finally{
                if(st!=null) st.close();
                this.desconectar(query);
            }
            return msj;
      }
      
      
    
}
