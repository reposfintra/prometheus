/***************************************
 * Nombre Clase ............. ChequeXFacturaDAO.java
 * Descripci�n  .. . . . . .  Genera el Cheque para una factura
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  22/12/2005
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/


package com.tsp.operation.model.DAOS;




import java.io.*;
import java.util.*;
import java.sql.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.TasaService;
import com.tsp.util.*;



public class ChequeXFacturaDAO extends MainDAO{
    
    
    /** ____________________________________________________________________________________________
     *                                             ATRIBUTOS
     * ____________________________________________________________________________________________ */
    
    
    
    public  static String OFICINA_PPAL  = "OP";
    
    public  static String CODE_PROGRAMA = "CXF";
    
    public  static String TIPO_FACTURA  = "F";
    
    public  static String TIPO_CHEQUE   = "C";
    
    private Vector facturas;
    
    private Egreso egreso;
    
    
    
    
    
    
    /** ____________________________________________________________________________________________
     *                                              METODOS
     * ____________________________________________________________________________________________ */
    
    
    
    
    
    public ChequeXFacturaDAO() {
        super("ChequeXFacturaDAO.xml");
    }
    public ChequeXFacturaDAO(String dataBaseName) {
        super("ChequeXFacturaDAO.xml", dataBaseName);
    }
    
    
    
    
    /**
     * M�todos que setea valores nulos
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public String reset(String val){
        if(val==null)
           val = "";
        return val;
    }
    
      /**
       * M�todo que busca los proveedores de la agencia y distrito del usuario
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
     
      public List getProveedores(String distrito, String agencia)throws Exception{          
          Connection con = null;
          PreparedStatement st = null;
          ResultSet rs = null;
          List lista = new LinkedList();
          String query = "SQL_SEARCH_DISTINCT_PROVEEDORES";
          try {

              con = this.conectarJNDI(query);//JJCastro fase2
              if (con != null) {

                  String filtro = (agencia.equals(this.OFICINA_PPAL)) ? " like '%' " : " ='" + agencia + "'";
                  String sql = this.obtenerSQL(query).replaceAll("#AGENCIA#", filtro);
                  st = con.prepareStatement(sql);
                  st.setString(1, distrito);
                  rs = st.executeQuery();

                  while (rs.next()) {
                      Hashtable proveedor = new Hashtable();
                      proveedor.put("nit", rs.getString(1));
                      proveedor.put("nombre", rs.getString(2));
                      lista.add(proveedor);
                  }

              }}catch(Exception e){
                  throw new Exception(e.getMessage());
            }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
            return lista;          
      }
    
    
      
      
    
    
      
      /**
       * M�todo que busca los tipo documentos para el programa, teniendo en cuenta el cod. del programa
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
     
      public List getDocumentos(String distrito)throws Exception{          
            Connection con = null;
            PreparedStatement  st      = null;
            ResultSet rs = null;
          List lista = new LinkedList();
          String query = "SQL_DOCUMENTOS";

          try {
              con = this.conectarJNDI(query);
              if (con != null) {
                  st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                  st.setString(1, distrito);
                  st.setString(2, "CXP");
                  rs = st.executeQuery();

                  while (rs.next()) {
                      Hashtable doc = new Hashtable();
                      doc.put("code", rs.getString(1));
                      doc.put("descripcion", rs.getString(2));
                      lista.add(doc);
                  }

              }
          }catch(Exception e){
                  throw new Exception(e.getMessage());
            }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
            return lista;          
      }
    
      
      
      
      
      
       /**
       * M�todo que busca la facturas del proveedor, teniendo en cuenta distrito y agencia
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
     
      public List getFacturasProveedor(String distrito, String proveedor, String tipoFactura, String agencia)throws Exception{          
            Connection         con     = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            List               lista   = new LinkedList();
            String             query   = "SQL_BUSCAR_FACTURAS_PROVEEDOR";
            try{ 
                con = this.conectarJNDI(query);
                if (con != null) {
                
                String filtro =   ( agencia.equals( this.OFICINA_PPAL ) )?" like '%' " : " ='" + agencia + "'";
                String sql    =   this.obtenerSQL( query ).replaceAll("#AGENCIA#", filtro );
                
                st            =   con.prepareStatement( sql ); 
                
                st.setString(1, distrito      );
                st.setString(2, proveedor     );
                st.setString(3, tipoFactura   );     
               
                rs=st.executeQuery();  
                while(rs.next()){
                    FacturasCheques factura = this.load(rs);   
                    lista.add(factura);
               } }
                
            }catch(Exception e){
                  throw new Exception( "Dao: getFacturasProveedor "+e.getMessage());
            }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
            return lista;          
      }
            
      
      
      
      
       /**
       * M�todo que busca los documentos relacionados a la factura
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
     
      public List getFacturasRelacionadas(String distrito, String tipoDoc, String documento, String proveedor )throws Exception{          
            Connection con = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            PreparedStatement  st2     = null;
            ResultSet          rs2     = null;
            List               lista   = new LinkedList();
            String             query1  = "SQL_BUSCAR_FACTURA_RELACIONADAS";
            String             query2  = "SQL_BUSCAR_CHEQUES_RELACIONADOS";
            try{   
                
              // FACTURAS:
                
                con = this.conectarJNDI(query1);
                if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query1));//JJCastro fase2
                
                st.setString(1, distrito    );
                st.setString(2, tipoDoc     );
                st.setString(3, documento   );             
                st.setString(4, proveedor   );             
                rs=st.executeQuery();                
                while(rs.next()){
                    FacturasCheques factura = this.load(rs);  
                    factura.setTipoObjecto( this.TIPO_FACTURA );
                    lista.add(factura);
                    factura = null;//Liberar Espacio JJCastro
                }
                
                
             // CHEQUES:
                st2 = con.prepareStatement(this.obtenerSQL(query2));//JJCastro fase2
                st2.setString(1, distrito    );
                st2.setString(2, tipoDoc     );
                st2.setString(3, documento   );             
                st2.setString(4, proveedor   );             
                rs2=st2.executeQuery();                
                while(rs2.next()){
                    FacturasCheques factura = this.load(rs2); 
                    factura.setTipoObjecto( this.TIPO_CHEQUE );
                    lista.add(factura);
                    factura = null;//Liberar Espacio JJCastro
                }
                
                
            }}catch(Exception e){
                  throw new Exception( "Dao: getFacturasRelacionadas "+e.getMessage());
            }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (rs2  != null){ try{ rs2.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
            return lista;          
      }
      
      
      /**
       * M�todo que carga datos del sql para la factura
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public FacturasCheques load(ResultSet   rs )throws Exception{
          FacturasCheques    factura = new FacturasCheques();
          try{
              
                factura.setDstrct         (  reset( rs.getString("dstrct")          ) );
                factura.setProveedor      (  reset( rs.getString("proveedor")       ) );
                factura.setTipo_documento (  reset( rs.getString("tipo_documento")  ) );
                factura.setDocumento      (  reset( rs.getString("documento")       ) );
                factura.setDescripcion    (  reset( rs.getString("descripcion")     ) );
                factura.setAgencia        (  reset( rs.getString("agencia")         ) );
                factura.setFecha_documento(  reset( rs.getString("fecha_documento") ) );
                factura.setBanco          (  reset( rs.getString("banco")           ) );
                factura.setSucursal       (  reset( rs.getString("sucursal")        ) );                    
                factura.setMoneda         (  reset( rs.getString("moneda")          ) );
                factura.setVlr_neto       (         rs.getDouble("vlr_neto")          );
                factura.setVlr_neto_me    (         rs.getDouble("vlr_neto_me")       );
                factura.setVlr_saldo      (         rs.getDouble("vlr_saldo")         );
                factura.setVlr_saldo_me   (         rs.getDouble("vlr_saldo_me")      );     
                factura.setNomProveedor   (  reset( rs.getString("payment_name")    ) );
                factura.setBase           (  reset( rs.getString("base")            ) );
                
                factura.setMonedaBanco    (  reset( rs.getString("moneda_banco")    ) ); 
                factura.setOc             (  reset( rs.getString("planilla")        ) ); 
                                                         
          }catch(Exception e){
              throw new Exception(e.getMessage());
          }
          return factura;
      }
      
      
       /**
       * M�todo que actualiza la factura, campos abono, saldo, y/o fecha y usuario de cancelacion
       * @autor.......fvillacob
       * @parameter   FacturasCheques    factura, double abono
       * @throws......Exception
       * @version.....1.0.
       **/
      public String updateCXP_DOC  (String cheque, FacturasCheques    factura, double abono, Usuario user)throws Exception{
          StringStatement  st      = null;
          String             sql     = "";
          String            query    = "SQL_UPDATE_CXP_DOC";
          try{
            
              
              TasaService  svc   =  new  TasaService(user.getBd());
              String  hoy        = Util.getFechaActual_String(4);
              
              
              String monedaLocal = factura.getMonedaLocal();
              String monedaME    = factura.getMoneda();
              String monedaBanco = factura.getMonedaBanco();              
              
              double vlrLocal    = abono;
              double vlrME       = abono;              
              
              
              
              if( !monedaBanco.equals( monedaME )  ){
                  Tasa  obj  =  svc.buscarValorTasa(monedaLocal,  monedaBanco , monedaME , hoy);
                  if(obj!=null){
                        vlrME =  abono *  obj.getValor_tasa();
                  } 
              }
              
              if( !monedaBanco.equals( monedaLocal )  ){
                  Tasa obj  =  svc.buscarValorTasa(monedaLocal, monedaBanco  , monedaLocal , hoy);
                  if(obj!=null){
                        vlrLocal = abono * obj.getValor_tasa();
                  }
              }
              
              
              vlrME     =  ( monedaME.equals("PES")     ||  monedaME.equals("BOL")     ) ? (int)Math.round(vlrME)    : Util.roundByDecimal(vlrME, 2);
              vlrLocal  =  ( monedaLocal.equals("PES")  ||  monedaLocal.equals("BOL")  ) ? (int)Math.round(vlrLocal) : Util.roundByDecimal(vlrLocal, 2);                        
              
               st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
                st.setDouble(1,   vlrLocal );
                st.setDouble(2,   vlrLocal );
                st.setDouble(3,   vlrME    );
                st.setDouble(4,   vlrME    );
                st.setString(5,   user.getLogin()); 
                st.setString(6,   cheque   );
                st.setString(7,   factura.getDstrct()         );
                st.setString(8,   factura.getProveedor()      );
                st.setString(9,   factura.getTipo_documento() );
                st.setString(10,  factura.getDocumento()      );
              
              sql = st.getSql();
              
              }catch(Exception e){
              throw new Exception( " updateCXP_DOC: " + e.getMessage());
          }finally{//JJCastro fase2
                    if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
          }
          return sql;
      }
      
      
      
      
      
      
      
      /**
     * M�todo que carga el sql para la actualizacion de la Serie
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... ImpresionCheque cheque, String usuario
     * @version..... 1.0.
     **/
   public  synchronized String ActualizarSeries(Cheque cheque, String usuario)throws Exception {
        StringStatement st    = null;
        ResultSet         rs    = null;
        String            sql   = "";
        String            query = "SQL_UPDATE_SERIE";
        try{
            
            Series serie  = cheque.getSerie(); 
            int    ultimo = serie.getLast_number();
            
      //--- realizamos la actualizacion
            st = new StringStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setInt   (1, ultimo + 1               );
            st.setString(2, usuario                  );
            st.setString(3, cheque.getDistrito()     );
            st.setString(4, cheque.getBanco()        );
            st.setString(5, cheque.getSucursal()     );
            sql = st.getSql();//JJCastro fase2
            
            
            }catch(Exception e){
            throw new SQLException("No se pudo actualizar la SERIE " + e.getMessage());
        }finally{//JJCastro fase2
                    if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
         }
        return sql;
    }
    
    
    
    
     /**
     * M�todo que finaliza la serie
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... ImpresionCheque cheque, String usuario
     * @version..... 1.0.
     **/
   public void FinalizarSeries(Cheque cheque, String usuario)throws Exception {
       Connection con = null;
       PreparedStatement st     = null;
        ResultSet         rs     = null;
        String            query  = "SQL_UPDATE_FINALIZAR_SERIE";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, usuario);
                st.setString(2, cheque.getDistrito() );
                st.setString(3, cheque.getBanco()    );
                st.setString(4, cheque.getSucursal() );
            st.executeUpdate();
            ////System.out.println("Finalizar serie: "+st.toString());
        }}catch(Exception e){
            throw new SQLException("No se pudo finalizar la SERIE "+ e.getMessage());
        }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    } 
      
      /**
       * M�todo que busca los valores items de la factura
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
     
      public double getVlrItemFactura(FacturasCheques factura )throws Exception{          
          Connection con = null;
          PreparedStatement  st      = null;
            ResultSet          rs      = null;
            double             vlr     = 0;
            String             query   = "SQL_BUSCAR_ITEM_FACTURA";
            try{                   
                con = this.conectarJNDI(query);
                if (con != null) {
                    st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                    st.setString(1, factura.getDstrct()           );
                    st.setString(2, factura.getProveedor()        );
                    st.setString(3, factura.getTipo_documento()   );             
                    st.setString(4, factura.getDocumento()        );            
                rs=st.executeQuery();                
                while(rs.next()){  
                    vlr += rs.getDouble("valor");
                }
                
            }}catch(Exception e){
                  throw new Exception( "Dao: getItemFactura "+e.getMessage());
            }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
            return vlr;          
      }
      
      
      
      
      
      /**
       * M�todo que busca los valores de Impuestos de  la factura, dependiendo el tipo de Impuesto
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
     
      public double getVlrImpuestosFactura(FacturasCheques factura , String tipoImpuesto)throws Exception{          
            Connection con = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            double             vlr     = 0;
            String             query   = "SQL_BUSCAR_IMPUESTOS_FACTURA";
            try{            
                con = this.conectarJNDI(query);
                if (con != null) {
                    st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                    st.setString(1, factura.getDstrct()           );
                    st.setString(2, factura.getProveedor()        );
                    st.setString(3, factura.getTipo_documento()   );
                    st.setString(4, factura.getDocumento()        ); 
                    st.setString(5, factura.getAgencia()          );                    
                    st.setString(6, factura.getFecha_documento()  );
                    st.setString(7, tipoImpuesto                  );
                    
                rs=st.executeQuery();                
                while(rs.next()){  
                    vlr += rs.getDouble("valor");
                }
                
                }}catch(Exception e){
                  throw new Exception( "Dao: getVlrImpuestosFactura "+e.getMessage());
            }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
            return vlr;          
      }
      
      
      
      
      
      
       /**
       * M�todo que busca la moneda local
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
     
      public String getMonedaLocal(String distrito)throws Exception{
          Connection con = null;
          PreparedStatement  st      = null;
            ResultSet          rs      = null;
            String             moneda  = "";  
            String             query   = "SQL_MONEDA_LOCAL";
            try{                   
                con = this.conectarJNDI(query);
                if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito );
                rs=st.executeQuery();   
                if(rs.next())
                    moneda = rs.getString(1);
                
                }}catch(Exception e){
                  throw new Exception( "Dao: getMonedaLocal "+e.getMessage());
            }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
            return moneda;          
      }
      
      
      
      
      
      /**
       * M�todo que busca la base del distrito del cheque
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/     
      public String getBase(String distrito)throws Exception{
          Connection con = null;
          PreparedStatement  st      = null;
            ResultSet          rs      = null;
            String             base    = "";  
            String             query   = "SQL_BASE";
            try{                   
                con = this.conectarJNDI(query);
                if (con != null) {
               st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito );
                rs=st.executeQuery();   
                if(rs.next())
                    base = rs.getString(1);
                
            }}catch(Exception e){
                  throw new Exception( "Dao: getBase "+e.getMessage());
            }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
            return base;          
      }
      
      
      
      /**
       * M�todo que busca el banco del proveedor, esto para llevar las facturas a la moneda de dicho banco
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/     
      public Hashtable getBancoProveedor(String distrito, String proveedor)throws Exception{          
            Connection con = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            Hashtable          banco   = null;  
            String             query   = "SQL_BUSCAR_BANCO_PROVEEDOR";
            try{                
               
                con = this.conectarJNDI(query);
                if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito      );
                st.setString(2, proveedor     );  
                rs=st.executeQuery();                  
                if(rs.next()){
                    banco  = new  Hashtable();
                    banco.put("distrito",           reset(  rs.getString("dstrct")           ) );
                    banco.put("nit",                reset(  rs.getString("nit")              ) );
                    banco.put("id_mims",            reset(  rs.getString("id_mims")          ) );
                    banco.put("nombre",             reset(  rs.getString("payment_name")     ) );
                    banco.put("banco",              reset(  rs.getString("branch_code")      ) );
                    banco.put("sucursal",           reset(  rs.getString("bank_account_no")  ) );
                    banco.put("moneda",             reset(  rs.getString("currency")         ) );
                    banco.put("agencia",            reset(  rs.getString("agency_id")        ) );         
                    
                    String nit_beneficiario = reset(  rs.getString("nit_beneficiario") );//Osvaldo
                    
                    banco.put("nit_beneficiario",   nit_beneficiario.equals("")? reset( rs.getString("nit") ) : nit_beneficiario           );//Osvaldo
                    banco.put("beneficiario",       nit_beneficiario.equals("")? reset( rs.getString("payment_name") ) : reset( rs.getString("beneficiario") ) );//Osvaldo                    
                }
                
                
            }}catch(Exception e){
                  throw new Exception( "Dao: getBancoProveedor "+e.getMessage());
            }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
            return banco;          
      }      
       

     /***************************************************************************
       * M�todo que busca el banco del proveedor, si no presenta banco, retorna
       *        el Hashtable con un mensaje, esto es para el programa de impresion
       *        de cheques por facturas corridas
       * @autor.......Ing. Osvaldo P�rez Ferrer
       * @throws......Exception
       * @version.....1.0.
       *************************************************************************/     
      public Hashtable getBancoProveedor2(String distrito, String proveedor)throws Exception{          
            Connection con = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            Hashtable          banco   = null;  
            String             query   = "SQL_BUSCAR_BANCO_PROVEEDOR";
            try{                
               
                con = this.conectarJNDI(query);
                if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito      );
                st.setString(2, proveedor     );  
                rs=st.executeQuery();                  
                if(rs.next()){
                    banco  = new  Hashtable();
                    banco.put("distrito",           reset(  rs.getString("dstrct")           ) );
                    banco.put("nit",                reset(  rs.getString("nit")              ) );
                    banco.put("id_mims",            reset(  rs.getString("id_mims")          ) );
                    banco.put("nombre",             reset(  rs.getString("payment_name")     ) );
                    banco.put("banco",              reset(  rs.getString("branch_code")      ) );
                    banco.put("sucursal",           reset(  rs.getString("bank_account_no")  ) );
                    banco.put("moneda",             reset(  rs.getString("currency")         ) );
                    banco.put("agencia",            reset(  rs.getString("agency_id")        ) );         
                    
                    String nit_beneficiario = reset(  rs.getString("nit_beneficiario") );
                    
                    banco.put("nit_beneficiario",   nit_beneficiario.equals("")? reset( rs.getString("nit") ) : nit_beneficiario           );
                    banco.put("beneficiario",       nit_beneficiario.equals("")? reset( rs.getString("payment_name") ) : reset( rs.getString("beneficiario") ) );
                }else{
                    
                    banco  = new  Hashtable();
                    banco.put("distrito",           "" );
                    banco.put("nit",                "" );
                    banco.put("id_mims",            "" );
                    banco.put("nombre",             " El NIT proveedor " + proveedor + " No presenta banco " );
                    banco.put("banco",              "" );
                    banco.put("sucursal",           "" );
                    banco.put("moneda",             "" );
                    banco.put("agencia",            "" );                                               
                    
                    banco.put("nit_beneficiario",   "" );
                    banco.put("beneficiario",       "" );
                    
                }
                
                
                }}catch(Exception e){
                  throw new Exception( "Dao: getBancoProveedor "+e.getMessage());
            }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
            return banco;          
      } 
      
       /**
       * Establece si el cheque digitado existe y fue impreso
       * @autor Ing. Andr�s Maturana
       * @throws Exception
       * @version 1.0
       **/     
      public boolean chequeImpreso(String dstrct, String banco, String sucursal, String cheque)throws Exception{          
            Connection con = null;
            PreparedStatement  st = null;
            ResultSet rs = null; 
            String query = "SQL_CHEQUE_IMPRESO";
            try{                
               
                con = this.conectarJNDI(query);
                if (con != null) {
               st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dstrct);
                st.setString(2, banco);  
                st.setString(3, sucursal);  
                st.setString(4, cheque);  
                
                rs = st.executeQuery();                  
                if(rs.next()){
                    if( rs.getString("reg_status").equals("A") ){
                        return false;
                    }
                    return true;
                }
                
                
            }} catch(Exception e){
                e.printStackTrace();
                throw new Exception( "Dao: chequeImpreso "+e.getMessage());
            }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
            
            return false;          
      }
      
      
      /**
       * Retorna el sql de anulacion del egreso
       * @autor Ing. Andr�s Maturana
       * @throws Exception
       * @version 1.0
       **/     
      public String anularCheque(String dstrct, String banco, String sucursal, String cheque, String usuario)throws Exception{          
            StringStatement  st = null;
            String query = "SQL_UPDATE_EGRESO";
            
            try{                
                
                Series serie = this.getSeries(dstrct, banco, sucursal);
                Cheque paymnt = new Cheque();
                paymnt.setDistrito(dstrct);
                paymnt.setBanco(banco);
                paymnt.setSucursal(sucursal);
                
                if( serie != null ){
                    st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2 sin
                    st.setString(1, dstrct);
                    st.setString(2, banco);
                    st.setString(3, sucursal);
                   st.setString(4, cheque);
                
                } else {
                    throw new Exception("No se encuentra la serie para el banco: " + banco.toUpperCase() + " y sucursal: " + sucursal.toUpperCase() );
                }
                
            } catch(Exception e){
                e.printStackTrace();
                throw new Exception( "Dao: insertChequeNegativoCabecera "+e.getMessage());
            }finally{//JJCastro fase2
                    if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
           }
            
            return st.getSql();
      }
      
      /**
       * Elimina la corrida de la fra relacionada a un cheque
       * @autor Ing. Andr�s Maturana
       * @throws Exception
       * @version 1.0
       **/     
      public String deleteCorridaChequeCxP(String dstrct, String banco, String sucursal, String cheque)throws Exception{          
       
            StringStatement  st = null;
            String query = "SQL_DELETE_CORRIDA";
            try{                               
                st = new StringStatement(this.obtenerSQL(query));//JJCastro fase2 sin
                st.setString(1, dstrct);
                st.setString(2, banco);  
                st.setString(3, sucursal);  
                st.setString(4, cheque);                  
                
            } catch(Exception e){
                e.printStackTrace();
                throw new Exception( "Dao: deleteCorridaChequeCxP "+e.getMessage());
            }finally{//JJCastro fase2
                    if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
           }
            
            return st.getSql();//JJCastro fase2
      }
      
      /**
       * Actualiza la corrida y el cheque de la fra relacionada a un cheque
       * @autor Ing. Andr�s Maturana
       * @throws Exception
       * @version 1.0
       **/     
      public String updateCxP_DocCheque(String dstrct, String banco, String sucursal, String cheque)throws Exception{          
       
            StringStatement  st = null;
            String query = "SQL_UPDATE_FACTURA_ANULACION";
            try{                
               
                st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2 sin
                st.setString(1, dstrct);
                st.setString(2, banco);  
                st.setString(3, sucursal);  
                st.setString(4, cheque);                  
                
            } catch(Exception e){
                e.printStackTrace();
                throw new Exception( "Dao: updateCxP_DocCheque "+e.getMessage());
            }finally{//JJCastro fase2
                    if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
           }
            return st.getSql();
      }
      
      
      /**
       * Obtiene un egreso
       * @autor Ing. Andr�s Maturana
       * @throws Exception
       * @version 1.0
       **/     
      public void getCheque(String dstrct, String banco, String sucursal, String cheque)throws Exception{          
            Connection con = null;
            PreparedStatement  st = null;
            ResultSet rs = null; 
            String query = "SQL_GET_EGRESO";
            this.egreso = null;
                        
            try{                
                con = this.conectarJNDI(query);
                if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dstrct);
                st.setString(2, banco);  
                st.setString(3, sucursal);  
                st.setString(4, cheque);  
                
                rs = st.executeQuery();                  
                if(rs.next()){
                    egreso = new Egreso();
                    egreso.setAgency_id(rs.getString("agency_id"));
                    egreso.setBank_account_no(rs.getString("bank_account_no"));
                    egreso.setBranch_code(rs.getString("branch_code"));
                    egreso.setCurrency(rs.getString("currency"));
                    egreso.setDocument_no(rs.getString("document_no"));
                    egreso.setDstrct(rs.getString("dstrct"));
                    egreso.setNit(rs.getString("nit"));
                    egreso.setPayment_name(rs.getString("payment_name"));
                    egreso.setVlr_for(rs.getFloat("vlr_for"));
                    egreso.setCreation_date(rs.getString("creation_date"));
                    egreso.setCreation_date(rs.getString("creation_user"));
                    egreso.setBase(rs.getString("base"));
                    egreso.setTransaccion(rs.getInt("transaccion"));                    
                }
                
                
                }} catch(Exception e){
                e.printStackTrace();
                throw new Exception( "Dao: chequeImpreso "+e.getMessage());
            }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
            
      }
      
      /**
       * Getter for property egreso.
       * @return Value of property egreso.
       */
      public com.tsp.operation.model.beans.Egreso getEgreso() {
          return egreso;
      }      
    
      /**
       * Setter for property egreso.
       * @param egreso New value of property egreso.
       */
      public void setEgreso(com.tsp.operation.model.beans.Egreso egreso) {
          this.egreso = egreso;
      }
       
     
      //24 Febrero 2007
      /*Jescandon - 15-02-07*/
    public Cheque getEgreso( String distrito, String banco, String sucursal, String chk )throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             sql     = "";
        String            query    = "SQL_CONSULTA_EGRESO";
        Cheque            cheque   = null;
        try{
            
            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,  distrito       );
            st.setString(2,  banco          );
            st.setString(3,  sucursal       );
            st.setString(4,  chk            );
            
            rs = st.executeQuery();
            if(rs.next()){
                cheque = new Cheque();
                cheque.setDistrito        (   rs.getString("dstrct")              );
                cheque.setBanco           (   rs.getString("branch_code")         );
                cheque.setSucursal        (   rs.getString("bank_account_no")     );
                cheque.setNumero          (   rs.getString("document_no")         );
                cheque.setNit             (   rs.getString("nit")                 );
                cheque.setBeneficiario    (   rs.getString("payment_name")        );
                cheque.setAgencia         (   rs.getString("agency_id")           );
                cheque.setPmt_date        (   rs.getString("pmt_date")            );
                cheque.setPrinter_date    (   rs.getString("printer_date")        );
                cheque.setConcept_code    (   rs.getString("concept_code")        );
                cheque.setVlr             (   rs.getDouble("vlr")                 );
                cheque.setVlr_for         (   rs.getDouble("vlr_for")             );
                cheque.setMoneda          (   rs.getString("currency")            );
                cheque.setCreation_user   (   rs.getString("creation_user")       );
                cheque.setBase            (   rs.getString("base")                );
                cheque.setTipo_documento  (   rs.getString("tipo_documento")      );
                cheque.setFecha_cheque    (   rs.getString("fecha_cheque")        );
                cheque.setUsuario_impresion(  rs.getString("usuario_impresion")   );
                cheque.setNitProveedor    (   rs.getString("nit_proveedor")       );
                cheque.setNit_beneficiario(   rs.getString("nit_beneficiario")    );
                cheque.setTasa            (   rs.getDouble("tasa")                );
            }
            
        }}catch(Exception e){
            throw new Exception( " getEgreso: " + e.getMessage());
        }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return cheque;
    }
    
 /**
  *
  * @param cheque
  * @param user
  * @param numchk
  * @return
  * @throws Exception
  */
    public String insertEgreso   (Cheque cheque, String user, String numchk )throws Exception{
        
        StringStatement  st      = null;
        String             sql     = "";
        String            query    = "SQL_ACTUALIZAR_EGRESO_REIMPRESION";
        try{
                        
            st= new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2 sin
            st.setString(1,  cheque.getDistrito()        );
            st.setString(2,  cheque.getBanco()           );
            st.setString(3,  cheque.getSucursal()        );
            st.setString(4,  numchk                      );
            st.setString(5,  cheque.getNit()             );
            st.setString(6,  cheque.getBeneficiario()    );
            st.setString(7,  cheque.getAgencia()         );
            st.setString(8,  cheque.getPmt_date()        );
            st.setString(9,  cheque.getPrinter_date()    );
            st.setString(10, "FAC"                       );   // concept_code
            st.setDouble(11, cheque.getVlr()             );
            st.setDouble(12, cheque.getVlr_for()         );
            st.setString(13, cheque.getMoneda()          );
            st.setString(14, user                        );
            st.setString(15, cheque.getBase()            );   // base
            st.setString(16, "004"                       );   // tipo doc
            st.setString(17, cheque.getFecha_cheque()    );
            st.setString(18, user                        );
            st.setDouble(19, cheque.getTasa()            );
            st.setString(20, cheque.getNit_beneficiario()); //Osvaldo
            st.setString(21, cheque.getNitProveedor()    ); //Osvaldo
            st.setString(22, cheque.getReimpresion()     );
            
            sql = st.getSql();
            
        }catch(Exception e){
            throw new Exception( " insertEgreso: " + e.getMessage());
        }finally{//JJCastro fase2
                    if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
           }
        return sql;
    }
    
 /**
  *
  * @param cheque
  * @param user
  * @param numchk
  * @return
  * @throws Exception
  */
    public String insertEgresoDet(Cheque cheque, String user, String numchk )throws Exception{
        StringStatement  st      = null;
        String             sql     = "";
        String            query    = "SQL_ACTUALIZAR_EGRESODET";
        try{
                        
            String desc = cheque.getBanco() + "/" + cheque.getSucursal()+ "/" + "Reemplazo CH " + cheque.getNumero();
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2 sin
            
            st.setString(1,  cheque.getDistrito()   );
            st.setString(2,  cheque.getBanco()      );
            st.setString(3,  cheque.getSucursal()   );
            st.setString(4,  numchk                 );
            st.setString(5,  "001"                  );
            st.setString(6,  "45"                   );  //concept_code
            st.setString(7,  "004"                  );
            st.setString(8,  cheque.getNumero()     );
            st.setString(9,  desc );
            st.setDouble(10, cheque.getVlr()        );
            st.setDouble(11, cheque.getVlr_for()    );
            st.setString(12, cheque.getMoneda()     );
            st.setString(13, user                   );
            st.setString(14, cheque.getBase()       );
            st.setDouble(15, cheque.getTasa()       );
            st.setString(16, ""                     );
            st.setString(17, ""                     );
            
            sql = st.getSql();//JJCastro fase2
            
        }catch(Exception e){
            throw new Exception( " insertEgresoDet: " + e.getMessage());
        }finally{//JJCastro fase2
                    if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
           }
        return sql;
    }


/**
 *
 * @param dstrct
 * @param banco
 * @param sucursal
 * @param cheque
 * @param usuario
 * @return
 * @throws Exception
 */
      public String anularChequeDetalles(String dstrct, String banco, String sucursal, String cheque, String usuario)throws Exception{
        
        StringStatement  st = null;
        String query = "SQL_UPDATE_EGRESODET";
        
        try{                     
            
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2 sin
            st.setString(1, dstrct);
            st.setString(2, banco);
            st.setString(3, sucursal);
            st.setString(4, cheque);


        } catch(Exception e){
            e.printStackTrace();
            throw new Exception( "Dao: insertChequeNegativoCabecera "+e.getMessage());
        }finally{//JJCastro fase2
                    if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
           }        
        return st.getSql();
    }
      
      
     /////////////////////////////////////////////////////////////////
    
    /*********************************************************************
     * Metodo obtenerChequesReimprimibles, obtiene una lista con datos de
     *      cheques que se pueden reimprimir, del banco y sucursal dados
     * @param: banco, banco del cheque
     * @param: sucursal, sucursal del cheque
     * @param: ini, rango inicial de cheque
     * @param: fin, rango final de cheque
     *********************************************************************/
    public Vector obtenerChequesReimprimibles( String dstrct, String banco, String sucursal, String ini, String fin ) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query      = "SQL_OBTENER_REIMPRIMIBLES";
        Vector v             = new Vector();
        
        try{
            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, dstrct );
            st.setString( 2, banco );
            st.setString( 3, sucursal );
            st.setString( 4, ini );
            st.setString( 5, fin );
            rs = st.executeQuery();
            
            while( rs.next() ){
                
                Hashtable h = new Hashtable();
                h.put( "cheque",        reset(rs.getString("document_no")) );
                h.put( "prove",         reset(rs.getString("proveedor")) );
                h.put( "nom_prove",     reset(rs.getString("nom_proveedor")) );
                h.put( "benef",         reset(rs.getString("beneficiario")) );
                h.put( "nom_benef",     reset(rs.getString("nom_beneficiario")) );
                h.put( "fecha_imp",     reset(rs.getString("printer_date")) );
                h.put( "usuario_imp",   reset(rs.getString("usuario_impresion")) );
                h.put( "creation_date", reset(rs.getString("creation_date")) );
                
                double valor = Double.parseDouble(reset(rs.getString("vlr_for")));
                h.put( "valor", com.tsp.util.Util.customFormat( valor ) );
                h.put( "moneda", reset(rs.getString("currency")) );
                
                v.add( h );
                h = null;//Liberar Espacio JJCastro
            }
            
        }}catch (Exception ex){
            throw new Exception( "Error en ChequeXFacturaDAO.obtenerChequesReimprimibles" + ex.getMessage() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
        return v;
    }
 
    
      /*********************************************************************
     * Metodo obtenerDocRelFacturas, obtiene las notas debito/credito o cheques
     *          asociados a la factura
     * @param: Cheque, objeto con el numero del cheque
     * @throws: en caso de un error de BD
     *********************************************************************/
    public FacturasCheques obtenerDocRelFacturas( FacturasCheques f, String cheque, String moneda_local, String moneda_banco ) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query      = "SQL_OBTENER_ITEMS_FACTURA_CHEQUE";
        
        String sql = "";
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            sql = this.obtenerSQL(query);//JJCastro fase2
            sql = sql.replaceAll( "#DSTRCT#", f.getDstrct() );
            sql = sql.replaceAll( "#TIPO_DOC#", f.getTipo_documento() );
            sql = sql.replaceAll( "#DOCUMENTO#", f.getDocumento() );
            sql = sql.replaceAll( "#PROVEEDOR#", f.getProveedor() );
            sql = sql.replaceAll( "#CHEQUE#", cheque );
            
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            
            List list_docrel = new LinkedList();
            
            while( rs.next() ){
                
                FacturasCheques rel = new FacturasCheques();
                
                rel.setDocumento(   reset( rs.getString("documento") ) );
                rel.setDescripcion( reset( rs.getString("descripcion") ) );
                rel.setTipo_documento( reset( rs.getString("tipo_documento") ) );
                rel.setProveedor( f.getProveedor() );
                rel.setDstrct( f.getDstrct() );
                
                if( rel.getTipo_documento().matches("035|036") ){ //Si son notas debito o credito
                    rel.setVlrFactura( this.convertir( moneda_local, f.getMoneda(), moneda_banco, this.getVlrItemFactura( rel ) ) );                    
                }else{//Si es cheque
                    rel.setVlrFactura(  rs.getDouble("vlr") );
                }
                
                rel.setValor_iva(   rs.getDouble("vlr_iva") );
                rel.setValor_riva(  rs.getDouble("vlr_riva") );
                rel.setValor_rica(  rs.getDouble("vlr_rica") );
                rel.setValor_rtfe(  rs.getDouble("vlr_rfte") );
                rel.setVlrNetoPago( rs.getDouble("vlr_neto_me") );
                
                list_docrel.add( rel );
                 rel = null;//Liberar Espacio JJCastro
            }
            
            f.setDocumnetosRelacionados( list_docrel );
            
        }}catch (Exception ex){
            throw new Exception( "Error en ChequeXFacturaDAO.obtenerDocRelFacturas " + ex.getMessage() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
        return f;
    }
    
    /***************************************************************************
     * Metodo convertir, convierte el valor dado de la moneda de la factura a
     *                   la moneda del banco
     * @return el valor convertido, 0 si no hay tasa
     * @throws : En caso de un error de BD
     **************************************************************************/
    
    public double convertir( String m_local, String m_factura, String m_banco, double value )throws Exception{
        
        double tasa         =  1;
        String  hoy         = Util.getFechaActual_String(4);
        
        if( !m_factura.equals( m_banco ) ){
            
            TasaService  svc  =  new  TasaService();
            Tasa         obj  = svc.buscarValorTasa( m_local,  m_factura , m_banco , hoy);
            
            if(obj!=null){
                
                tasa        = obj.getValor_tasa();
                
                if( m_banco.equals("DOL") ){
                    value *= tasa;
                }else{
                    value = Math.round( value * tasa );
                }
                
            }else{                
                return 0;
            }
            
        }
        return value;
    }
    
    /*********************************************************************
     * Metodo marcarReimpreso, deshabilita el cheque para reimpresion
     * @author: Ing. Osvaldo P�rez Ferrer
     *********************************************************************/
    public void marcarReimpreso( Cheque c, String usuario) throws Exception{
        Connection con = null;
        PreparedStatement st = null;        
        String query      = "SQL_MARCAR_REIMPRESO";
        Vector v             = new Vector();
        
        try{
            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, usuario );
            st.setString( 2, c.getDistrito() );
            st.setString( 3, c.getBanco() );
            st.setString( 4, c.getSucursal() );
            st.setString( 5, c.getNumero() );
            
            st.executeUpdate();
                       
        }}catch (Exception ex){
            throw new Exception( "Error en marcarReimpreso.obtenerChequesReimprimibles" + ex.getMessage() );
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
                
    }
    
    /**
       * M�todo que busca la facturas del proveedor, que se encuentren 
                pendientes por imprimir teniendo en cuenta distrito y agencia
       * @autor.......Ing. Osvaldo P�rez Ferrer
       * @throws......Exception
       * @version.....1.0.
       **/
     
      public List getFacturasProveedorFromPrecheque( String id,String distrito, String proveedor, String tipoFactura, String agencia )throws Exception{          
            Connection         con     = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            List               lista   = new LinkedList();
            String             query   = "SQL_BUSCAR_FACTURAS_PROVEEDOR_FROM_PRECHEQUE";
            try{ 
                
                con = this.conectarJNDI(query);
                if (con != null) {
                String filtro =   ( agencia.equals( this.OFICINA_PPAL ) )?"" : "AND pc.agencia = ?";
                String sql    =   this.obtenerSQL( query ).replaceAll("#COMMENT#", filtro );
                st            =   con.prepareStatement( sql ); 
                st.setString( 1, id );
                st.setString( 2, distrito      );
                st.setString( 3, proveedor     );
                st.setString( 4, tipoFactura   );
                if(! agencia.equals( this.OFICINA_PPAL ) ) {
                    st.setString( 5, agencia );
                }
               
                rs=st.executeQuery();  
                while(rs.next()){
                    FacturasCheques factura = this.load(rs);
                    factura.setTipo_pago( reset( rs.getString("tipo_pago") ) );
                    lista.add(factura);
                factura = null;//Liberar Espacio JJCastro
                }
                
            }}catch(Exception e){
                  throw new Exception( "Dao: getFacturasProveedor "+e.getMessage());
            }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
            return lista;          
      }

      /*********************************************************************
     * Metodo obtenerFacturasCheque, obtiene una lista con las facturas
     *         que se incluyeron en el cheque
     * @param: Cheque, objeto con el numero del cheque
     * @throws: en caso de un error de BD
     *********************************************************************/
    public Cheque obtenerFacturasCheque( Cheque cheque, String moneda_local ) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_FACTURAS_CHEQUE";
        List lista = new LinkedList();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cheque.getNumero());

                rs = st.executeQuery();

                while (rs.next()) {

                    cheque.setNit_beneficiario(reset(rs.getString("nit_beneficiario")));
                    cheque.setNom_beneficiario(reset(rs.getString("nom_beneficiario")));
                    cheque.setNitProveedor(reset(rs.getString("nit_proveedor")));
                    cheque.setNombre(reset(rs.getString("nom_proveedor")));
                    cheque.setMoneda(reset(rs.getString("currency")));
                    cheque.setMonto(rs.getDouble("monto_cheque"));
                    cheque.setBanco(reset(rs.getString("branch_code")));
                    cheque.setSucursal(reset(rs.getString("bank_account_no")));
                    cheque.setDistrito(rs.getString("dstrct"));
                    cheque.setAno(Util.getFechaActual_String(1));
                    cheque.setMes(Util.getFechaActual_String(3));
                    cheque.setDia(Util.getFechaActual_String(5));

                    FacturasCheques f = new FacturasCheques();

                    f.setDstrct(reset(rs.getString("dstrct")));
                    f.setTipo_documento(reset(rs.getString("tipo_documento")));
                    f.setDocumento(reset(rs.getString("documento")));
                    f.setProveedor(reset(rs.getString("nit_proveedor")));
                    f.setVlrPagar(rs.getDouble("abono_fact"));
                    f.setMoneda(reset(rs.getString("moneda_factura")));
                    f.setFecha_documento(reset(rs.getString("fecha_documento")));
                    f.setDescripcion(reset(rs.getString("descripcion")));
                    f.setValor_iva(this.convertir(moneda_local, f.getMoneda(), cheque.getMoneda(), rs.getDouble("vlr_IVA")));
                    f.setValor_riva(this.convertir(moneda_local, f.getMoneda(), cheque.getMoneda(), rs.getDouble("vlr_RIVA")));
                    f.setValor_rica(this.convertir(moneda_local, f.getMoneda(), cheque.getMoneda(), rs.getDouble("vlr_RICA")));
                    f.setValor_rtfe(this.convertir(moneda_local, f.getMoneda(), cheque.getMoneda(), rs.getDouble("vlr_RFTE")));

                    f.setVlrFactura(this.convertir(moneda_local, f.getMoneda(), cheque.getMoneda(), this.getVlrItemFactura(f)));

                    f.setVlrNetoPago(f.getVlrFactura() + f.getValor_iva() + f.getValor_riva() + f.getValor_rica() + f.getValor_rtfe());

                    f = this.obtenerDocRelFacturas(f, cheque.getNumero(), moneda_local, cheque.getMoneda());

                    lista.add(f);
                    f = null;//Liberar Espacio JJCastro
                }

                cheque.setFacturas(lista);

            }}catch  (Exception ex) {
            throw new Exception( "Error en ChequeXFacturaDAO.obtenerFacturasCheque " + ex.getMessage() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return cheque;

    }
    
     /**
     * Actualiza la corrida y el cheque de la fra relacionada a un cheque
     * @autor Ing. Andr�s Maturana
     * @throws Exception
     * @version 1.0
     **/
    public void obtenerCxP_DocCheque(String dstrct, String banco, String sucursal, String cheque)throws Exception{
        Connection con = null;
        PreparedStatement  st = null;
        String query = "SQL_FRAS_CHEQUE";
        ResultSet rs = null;
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, banco);
            st.setString(3, sucursal);
            st.setString(4, cheque);
            
            org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
            logger.info("? obtenerCxP_DocCheque: " + st);
            rs = st.executeQuery();
            this.facturas = new Vector();
            
            while ( rs.next() ){
                RepGral obj = new RepGral();
                obj.loadChequeCxP(rs);
                this.facturas.add(obj);
            }
            
            }} catch(Exception e){
            e.printStackTrace();
            throw new Exception( "Dao: obtenerCxP_DocCheque "+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Actualiza los saldos y abonos de la fra relacionada a un cheque
     * @autor Ing. Andr�s Maturana
     * @throws Exception
     * @version 1.0
     **/
    public String updateSaldosCxP_DocCheque(String dstrct, String proveedor, String tipo_documento, String documento, double vlr, double vlr_for)throws Exception{
        
        StringStatement  st = null;
        String query = "SQL_UPDATE_SALDOS_FACTURA_ANULACION";
        try{
            
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2 sin
            st.setDouble(1, vlr);
            st.setDouble(2, vlr_for);
            st.setDouble(3, vlr);
            st.setDouble(4, vlr_for);
            st.setString(5, dstrct);
            st.setString(6, proveedor);
            st.setString(7, tipo_documento);
            st.setString(8, documento);
            
        } catch(Exception e){
            e.printStackTrace();
            throw new Exception( "Dao: updateCxP_DocCheque "+e.getMessage());
        }finally{//JJCastro fase2
                    if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        
        return st.getSql();//JJCastro fase2
    }
    
    /**
     * Getter for property facturas.
     * @return Value of property facturas.
     */
    public java.util.Vector getFacturas() {
        return facturas;
    }
    
    /**
     * Setter for property facturas.
     * @param facturas New value of property facturas.
     */
    public void setFacturas(java.util.Vector facturas) {
        this.facturas = facturas;
    }    
     
     
     /**
       * M�todo que busca los proveedores de la agencia y distrito del usuario
       * @autor.......Osvaldo P�rez Ferrer
       * @throws......Exception
       * @version.....1.0.
       **/
     
      public List getProveedores(String nombre, String distrito, String agencia)throws Exception{          
            Connection         con     = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            List               lista   = new LinkedList();
            String             query   = "SQL_SEARCH_DISTINCT_PROVEEDORES_NOMBRE";
            try{
                
                con = this.conectarJNDI(query);
                if (con != null) {
                    String filtro =   ( agencia.equals( this.OFICINA_PPAL ) )?" like '%' " : " ='" + agencia + "'";
                    String sql    =   this.obtenerSQL( query ).replaceAll("#AGENCIA#", filtro );
                    sql = sql.replaceAll("#NOMBRE#", "'%"+ nombre +"%'" );
                    
                    st            =   con.prepareStatement( sql ); 
                    
                    st.setString(1, distrito);
                    rs=st.executeQuery();
                    
                    while(rs.next()){
                        Hashtable proveedor = new Hashtable();
                            proveedor.put("nit",     rs.getString(1));
                            proveedor.put("nombre",  rs.getString(2));
                            proveedor.put("retpagoProveedor", rs.getString(3));
                        lista.add(proveedor);
                   proveedor = null;//Liberar Espacio JJCastro
                    }
                    
                }}catch(Exception e){
                  throw new Exception(e.getMessage());
            }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
            return lista;          
      }
      
      /**
     * Inserta en anulacion egreso
     * @autor Ing. Juan M. Escandon
     * @throws Exception
     * @version 1.0
     **/
    public String insertAnulacion_egreso(
    String dstrct,
    String banco,
    String sucursal,
    String cheque,
    String login,
    String causa,
    String observacion,
    String trecuperacion,
    String base, String clasificacion ) throws Exception{
        
        StringStatement  st = null;
        String query = "SQL_INSERT_ANULACION_EGRESO_REEMPLAZO";
        this.egreso = null;
        
        try{
            
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2 sin
            st.setString(1, dstrct);
            st.setString(2, banco);
            st.setString(3, sucursal);
            st.setString(4, cheque);
            st.setString(5, login);
            st.setString(6, causa);
            st.setString(7, observacion);
            st.setString(8, trecuperacion);
            st.setString(9, login);
            st.setString(10, login);
            st.setString(11, base);
            st.setString(12, clasificacion );
            
        } catch(Exception e){
            e.printStackTrace();
            throw new Exception( "Dao: insertAnulacion_egreso "+e.getMessage());
        } finally{//JJCastro fase2
                    if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return st.getSql();//JJCastro fase2
        
    }
    
     /**
       * Inserta en anulacion egreso
       * @autor Ing. Andr�s Maturana
       * @throws Exception
       * @version 1.0
       **/     
      public String insertAnulacion_egreso(
            String dstrct, 
            String banco, 
            String sucursal, 
            String cheque, 
            String login, 
            String causa, 
            String observacion, 
            String trecuperacion, 
            String base) throws Exception{          
       
            StringStatement  st = null;
            String query = "SQL_INSERT_ANULACION_EGRESO";
            this.egreso = null;
                        
            try{                
               
                st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2 sin
                st.setString(1, dstrct);
                st.setString(2, banco);  
                st.setString(3, sucursal);  
                st.setString(4, cheque);    
                st.setString(5, login);    
                st.setString(6, causa);    
                st.setString(7, observacion);    
                st.setString(8, trecuperacion);    
                st.setString(9, login);      
                st.setString(10, login);      
                st.setString(11, base);                
                
            } catch(Exception e){
                e.printStackTrace();
                throw new Exception( "Dao: insertAnulacion_egreso "+e.getMessage());
            }finally{//JJCastro fase2
                    if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
            return st.getSql();//JJCastro fase2            
      }
      
         /**
       * Establece si el cheque digitado pertenece a un pago a Fra
       * @autor Ing. Andr�s Maturana
       * @throws Exception
       * @version 1.0
       **/     
      public boolean isChequeCXP (String dstrct, String banco, String sucursal, String cheque)throws Exception{          
            Connection con = null;
            PreparedStatement  st = null;
            ResultSet rs = null; 
            String query = "SQL_IS_CHEQUE_CXP";
            boolean ische = false;
            try{
                con = this.conectarJNDI(query);
                if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dstrct);
                st.setString(2, banco);  
                st.setString(3, sucursal);  
                st.setString(4, cheque);  
                
                rs = st.executeQuery();                  
                if(rs.next()){
                    ische = true;
                }
                
                
            }}catch(Exception e){
                e.printStackTrace();
                throw new Exception( "Dao: chequeImpreso "+e.getMessage());
            }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
            return ische;
      }
      
      
      
      /**
       * M�todo que crea detalle del cheque  asociado a la factura
       * @autor.......fvillacob
       * @parameter   FacturasCheques    factura, double abono
       * @throws......Exception
       * @version.....1.0.
       **/
      public String insertEgresoDet(Cheque cheque, FacturasCheques  factura, String item,  double abono, String user)throws Exception{
          StringStatement  st      = null;
          String             sql     = "";
          String            query    = "SQL_ACTUALIZAR_EGRESODET";
          try{
              
              TasaService  svc      =  new  TasaService();
              String       hoy      = Utility.getHoy("-");
              double       vlrLocal = abono;
              double       vlrME    = abono;
              double       tasa     = 1;
              
              String monedaLocal = cheque.getMonedaLocal();
              String monedaBanco = cheque.getMoneda();               
              
              if(!monedaLocal.equals(monedaBanco) ){
                  Tasa  obj  =  svc.buscarValorTasa(monedaLocal,  monedaBanco , monedaLocal , hoy);
                  if(obj!=null){
                        tasa      =  obj.getValor_tasa();
                        vlrLocal  =  abono *  tasa;
                        vlrLocal  =  ( monedaLocal.equals("PES")  ||  monedaLocal.equals("BOL")  ) ? (int)Math.round(vlrLocal) : Util.roundByDecimal(vlrLocal, 2);                        
                  }
              }            
              
           
              
              String desc = "CHEQUE A LA FACTURA ";
              st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2 sin
                
                st.setString(1,  cheque.getDistrito()         );
                st.setString(2,  cheque.getBanco()            );
                st.setString(3,  cheque.getSucursal()         );
                st.setString(4,  cheque.getNumero()           );
                st.setString(5,  Utility.rellenar(item, 3)    );  
                st.setString(6,  vlrME==0?"CC":"FAC"          );  //concept_code
                st.setString(7,  factura.getTipo_documento()  );
                st.setString(8,  factura.getDocumento()       );
                st.setString(9,  desc + factura.getDocumento());
                st.setDouble(10, vlrLocal                    );
                st.setDouble(11, vlrME                       );
                st.setString(12, cheque.getMoneda()          );
                st.setString(13, user                        );
                st.setString(14, factura.getBase()           );
                st.setDouble(15, tasa                        ); 
                st.setString(16, factura.getTipo_pago()      ); 
                st.setString(17, factura.getOc()             ); 
                
              sql = st.getSql();//JJCastro fase2
              
          }catch(Exception e){
              throw new Exception( " insertEgresoDet: " + e.getMessage());
          }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}

        }
          return sql;
      }

 
      /**
       * M�todo que crea el cheque  asociado a la factura por el valor del abono
       * @autor.......fvillacob
       * @parameter   FacturasCheques    factura, double abono
       * @throws......Exception
       * @version.....1.0.
       **/
      public String insertEgreso   (Cheque cheque, double abono, String user)throws Exception{
     
          StringStatement  st      = null;
          String             sql     = "";
          String            query    = "SQL_ACTUALIZAR_EGRESO";
          try{
              
              TasaService  svc      =  new  TasaService();
              String       hoy      = Utility.getHoy("-");
              double       vlrLocal = abono;
              double       vlrME    = abono;
              double       tasa     = 1;
              
              String monedaLocal = cheque.getMonedaLocal();
              String monedaBanco = cheque.getMoneda();               
              
              if(!monedaLocal.equals(monedaBanco) ){
                  Tasa  obj  =  svc.buscarValorTasa(monedaLocal,  monedaBanco , monedaLocal , hoy);
                  if(obj!=null){
                        tasa     =  obj.getValor_tasa();
                        vlrLocal =  abono *  tasa;
                        vlrLocal  =  ( monedaLocal.equals("PES")  ||  monedaLocal.equals("BOL")  ) ? (int)Math.round(vlrLocal) : Util.roundByDecimal(vlrLocal, 2);                        
                  }
              }              
           
              
              String base = getBase( cheque.getDistrito() ) ;
              
              st= new StringStatement(this.obtenerSQL(query),true);//JJCastro fase2 sin
                st.setString(1,  cheque.getDistrito()        );
                st.setString(2,  cheque.getBanco()           );
                st.setString(3,  cheque.getSucursal()        ); 
                st.setString(4,  cheque.getNumero()          );
                st.setString(5,  cheque.getBeneficiario()    );  
                st.setString(6,  cheque.getNombre()          );  
                st.setString(7,  cheque.getAgencia()         );  
                st.setString(8,  hoy                         );  
                st.setString(9,  hoy                         );  
                st.setString(10, vlrME==0?"CC":"FAC"         );   // concept_code
                st.setDouble(11, vlrLocal                    );  
                st.setDouble(12, vlrME                       );
                st.setString(13, cheque.getMoneda()          );
                st.setString(14, user                        );
                st.setString(15, base                        );   // base
                st.setString(16, "004"                       );   // tipo doc
                st.setString(17, hoy                         );
                st.setString(18, user                        );
                st.setDouble(19, tasa                        );
                st.setString(20, cheque.getNit_beneficiario()); //Osvaldo
                st.setString(21, cheque.getNitProveedor()    ); //Osvaldo
              
              sql = st.getSql();//JJCastro fase2
              
          }catch(Exception e){
              throw new Exception( " insertEgreso: " + e.getMessage());
          }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
          }
          return sql;
      }
      
       /*********************************************************************
     * Metodo obtenerFacturasCheque, obtiene una lista con las facturas
     *         que se incluyeron en el cheque
     * @param: Cheque, objeto con el numero del cheque
     * @throws: en caso de un error de BD
     *********************************************************************/
    public Cheque obtenerFacturasCheque( Cheque cheque, String moneda_local, String banco, String sucursal ) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query      = "SQL_OBTENER_FACTURAS_CHEQUE";
        List lista           = new LinkedList();
        
        try{
            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, cheque.getNumero() );
            st.setString( 2, banco );
            st.setString( 3, sucursal );
            rs = st.executeQuery();
            
            while( rs.next() ){
                
                cheque.setNit_beneficiario( reset(rs.getString( "nit_beneficiario" ) ) );
                cheque.setNom_beneficiario( reset(rs.getString( "nom_beneficiario" ) ) );
                cheque.setNitProveedor(     reset(rs.getString( "nit_proveedor" ) ) );
                cheque.setNombre(           reset(rs.getString( "nom_proveedor" ) ) );
                cheque.setMoneda(           reset(rs.getString( "currency" ) ) );
                cheque.setMonto(            rs.getDouble("monto_cheque" ) );
                cheque.setBanco(            reset(rs.getString( "branch_code" ) ) );
                cheque.setSucursal(         reset(rs.getString( "bank_account_no" ) ) );
                cheque.setDistrito(         rs.getString("dstrct" ) );
                cheque.setAno              ( Util.getFechaActual_String(1) );
                cheque.setMes              ( Util.getFechaActual_String(3) );
                cheque.setDia              ( Util.getFechaActual_String(5) );
                
                FacturasCheques f = new FacturasCheques();
                
                f.setDstrct( reset(         rs.getString("dstrct") ) );
                f.setTipo_documento(        reset( rs.getString("tipo_documento") ) );
                f.setDocumento(             reset( rs.getString("documento") ) );
                f.setProveedor(             reset( rs.getString("nit_proveedor") ) );
                f.setVlrPagar(              rs.getDouble("abono_fact") );
                f.setMoneda(                reset( rs.getString("moneda_factura") ) );
                f.setFecha_documento(       reset( rs.getString("fecha_documento" ) ) );
                f.setDescripcion(           reset( rs.getString( "descripcion" ) ) );
                f.setValor_iva(             this.convertir( moneda_local, f.getMoneda(), cheque.getMoneda(), rs.getDouble( "vlr_IVA" ) ) );
                f.setValor_riva(            this.convertir( moneda_local, f.getMoneda(), cheque.getMoneda(), rs.getDouble( "vlr_RIVA" ) ) );
                f.setValor_rica(            this.convertir( moneda_local, f.getMoneda(), cheque.getMoneda(), rs.getDouble( "vlr_RICA" ) ) );
                f.setValor_rtfe(            this.convertir( moneda_local, f.getMoneda(), cheque.getMoneda(), rs.getDouble("vlr_RFTE") ) );                                
                
                f.setVlrFactura( this.convertir( moneda_local, f.getMoneda(), cheque.getMoneda(), this.getVlrItemFactura( f ) ) );
                
                f.setVlrNetoPago( f.getVlrFactura() + f.getValor_iva() + f.getValor_riva() + f.getValor_rica() + f.getValor_rtfe() );
                
                f = this.obtenerDocRelFacturas( f, cheque.getNumero(), moneda_local , cheque.getMoneda(), banco, sucursal );
                
                lista.add( f );
                f  = null;//Liberar Espacio JJCastro
                
            }
            
            cheque.setFacturas( lista );
            
        }}catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en ChequeXFacturaDAO.obtenerFacturasCheque " + ex.getMessage() );
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }        
        return cheque;
    }
      /*********************************************************************
     * Metodo obtenerDocRelFacturas, obtiene las notas debito/credito o cheques
     *          asociados a la factura
     * @param: Cheque, objeto con el numero del cheque
     * @throws: en caso de un error de BD
     *********************************************************************/
    public FacturasCheques obtenerDocRelFacturas( FacturasCheques f, String cheque, String moneda_local, String moneda_banco, String banco, String sucursal ) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_OBTENER_ITEMS_FACTURA_CHEQUE";
        
        String sql = "";        
        try{            
            con = this.conectarJNDI(query);
            if (con != null) {

            sql = this.obtenerSQL(query);
            sql = sql.replaceAll( "#DSTRCT#", f.getDstrct() );
            sql = sql.replaceAll( "#TIPO_DOC#", f.getTipo_documento() );
            sql = sql.replaceAll( "#DOCUMENTO#", f.getDocumento() );
            sql = sql.replaceAll( "#PROVEEDOR#", f.getProveedor() );
            sql = sql.replaceAll( "#CHEQUE#", cheque );
            sql = sql.replaceAll( "#BANCO#", banco );
            sql = sql.replaceAll( "#SUCURSAL#", sucursal );

            st = con.prepareStatement(sql);//JJCastro fase2
            rs = st.executeQuery();
            
            List list_docrel = new LinkedList();
            
            while( rs.next() ){
                
                FacturasCheques rel = new FacturasCheques();
                
                rel.setDocumento(   reset( rs.getString("documento") ) );
                rel.setDescripcion( reset( rs.getString("descripcion") ) );
                rel.setTipo_documento( reset( rs.getString("tipo_documento") ) );
                rel.setProveedor( f.getProveedor() );
                rel.setDstrct( f.getDstrct() );
                
                if( rel.getTipo_documento().matches("035|036") ){ //Si son notas debito o credito
                    rel.setVlrFactura( this.convertir( moneda_local, f.getMoneda(), moneda_banco, this.getVlrItemFactura( rel ) ) );                    
                }else{//Si es cheque
                    rel.setVlrFactura(  rs.getDouble("vlr") );
                }
                
                rel.setValor_iva(   rs.getDouble("vlr_iva") );
                rel.setValor_riva(  rs.getDouble("vlr_riva") );
                rel.setValor_rica(  rs.getDouble("vlr_rica") );
                rel.setValor_rtfe(  rs.getDouble("vlr_rfte") );
                rel.setVlrNetoPago( rs.getDouble("vlr_neto_me") );
                
                list_docrel.add( rel );
                rel = null;//Liberar Espacio JJCastro
            }
            
            f.setDocumnetosRelacionados( list_docrel );
            
        }}catch (Exception ex){
            throw new Exception( "Error en ChequeXFacturaDAO.obtenerDocRelFacturas " + ex.getMessage() );
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return f;
    }
    
    /**
       * M�todo que crea el cheque  asociado a la factura por el valor del abono
       * @autor.......Jdelarosa
       * @parameter   FacturasCheques    factura, double abono
       * @throws......Exception
       * @version.....1.0.
       **/
      public String insertEgreso   (Cheque cheque, double abono, String user, String tipo)throws Exception{
     
          StringStatement  st      = null;
          String             sql     = "";
          String            query    = "SQL_ACTUALIZAR_EGRESO";
          try{
              
              TasaService  svc      =  new  TasaService();
              String       hoy      = Utility.getHoy("-");
              double       vlrLocal = abono;
              double       vlrME    = abono;
              double       tasa     = 1;
              
              String monedaLocal = cheque.getMonedaLocal();
              String monedaBanco = cheque.getMoneda();               
              
              if(!monedaLocal.equals(monedaBanco) ){
                  Tasa  obj  =  svc.buscarValorTasa(monedaLocal,  monedaBanco , monedaLocal , hoy);
                  if(obj!=null){
                        tasa     =  obj.getValor_tasa();
                        vlrLocal =  abono *  tasa;
                        vlrLocal  =  ( monedaLocal.equals("PES")  ||  monedaLocal.equals("BOL")  ) ? (int)Math.round(vlrLocal) : Util.roundByDecimal(vlrLocal, 2);                        
                  }
              }              
           
              
              String base = cheque.getBase();
              
               st= new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2 sin
                st.setString(1,  cheque.getDistrito()        );
                st.setString(2,  cheque.getBanco()           );
                st.setString(3,  cheque.getSucursal()        ); 
                st.setString(4,  cheque.getNumero()          );
                st.setString(5,  cheque.getBeneficiario()    );  
                st.setString(6,  cheque.getNombre()          );  
                st.setString(7,  cheque.getAgencia()         );  
                st.setString(8,  hoy                         );  
                st.setString(9,  hoy                         );  
                st.setString(10, tipo                        );   // concept_code
                st.setDouble(11, vlrLocal                    );  
                st.setDouble(12, vlrME                       );
                st.setString(13, cheque.getMoneda()          );
                st.setString(14, user                        );
                st.setString(15, base                        );   // base
                st.setString(16, "004"                       );   // tipo doc
                st.setString(17, hoy                         );
                st.setString(18, user                        );
                st.setDouble(19, tasa                        );
                st.setString(20, cheque.getNit_beneficiario()); //Osvaldo
                st.setString(21, cheque.getNitProveedor()    ); //Osvaldo
              
              sql = st.getSql();//JJCastro fase2
              
          }catch(Exception e){
              throw new Exception( " insertEgreso: " + e.getMessage());
          }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
          }
          return sql;
      }
      
      /**
       * M�todo que actualiza el banco de la factura
       * @autor.......Jdelarosa
       * @parameter   FacturasCheques    factura, double abono
       * @throws......Exception
       * @version.....1.0.
       **/
      public String updateBancoCXP_DOC  (Cheque cheque, FacturasCheques    factura, String user)throws Exception{
       
          StringStatement  st      = null;
          String             sql     = "";
          String            query    = "SQL_UPDATE_BANCO_CXP_DOC";
          try{
            
            st= new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2 sin
            st.setString(1,   cheque.getBanco()         );
            st.setString(2,   cheque.getSucursal()      );
            st.setString(3,   user                      ); 
            st.setString(4,   cheque.getMoneda()        );
            st.setString(5,   cheque.getAgencia()       );
            st.setString(6,   factura.getDstrct()       );
            st.setString(7,   factura.getProveedor()    );
            st.setString(8,   factura.getTipo_documento());
            st.setString(9,   factura.getDocumento()    );
            sql = st.getSql();
              
          }catch(Exception e){
              e.printStackTrace();
              throw new Exception( " updateBancoCXP_DOC: " + e.getMessage());
          }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
          }
          return sql;
      } 
      
      /**
       * M�todo que crea detalle del cheque  asociado a la factura
       * @autor.......Jdelarosa
       * @parameter   FacturasCheques    factura, double abono
       * @throws......Exception
       * @version.....1.0.
       **/
      public String insertEgresoDet(Cheque cheque, FacturasCheques  factura, String item,  double abono, String user, String tipo)throws Exception{
          StringStatement  st      = null;
          String             sql     = "";
          String            query    = "SQL_ACTUALIZAR_EGRESODET";
          try{
              
              TasaService  svc      =  new  TasaService();
              String       hoy      = Utility.getHoy("-");
              double       vlrLocal = abono;
              double       vlrME    = abono;
              double       tasa     = 1;
              
              String monedaLocal = cheque.getMonedaLocal();
              String monedaBanco = cheque.getMoneda();               
              
              if(!monedaLocal.equals(monedaBanco) ){
                  Tasa  obj  =  svc.buscarValorTasa(monedaLocal,  monedaBanco , monedaLocal , hoy);
                  if(obj!=null){
                        tasa      =  obj.getValor_tasa();
                        vlrLocal  =  abono *  tasa;
                        vlrLocal  =  ( monedaLocal.equals("PES")  ||  monedaLocal.equals("BOL")  ) ? (int)Math.round(vlrLocal) : Util.roundByDecimal(vlrLocal, 2);                        
                  }
              }            
              
           
              
               String desc = "TRANSFERENCIA A LA FACTURA ";
               st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
                st.setString(1,  cheque.getDistrito()         );
                st.setString(2,  cheque.getBanco()            );
                st.setString(3,  cheque.getSucursal()         );
                st.setString(4,  cheque.getNumero()           );
                st.setString(5,  Utility.rellenar(item, 3)    );  
                st.setString(6,  tipo                         );  //concept_code
                st.setString(7,  factura.getTipo_documento()  );
                st.setString(8,  factura.getDocumento()       );
                st.setString(9,  desc + factura.getDocumento());
                st.setDouble(10, vlrLocal                    );
                st.setDouble(11, vlrME                       );
                st.setString(12, cheque.getMoneda()          );
                st.setString(13, user                        );
                st.setString(14, factura.getBase()           );
                st.setDouble(15, tasa                        ); 
                st.setString(16, factura.getTipo_pago()      ); 
                st.setString(17, factura.getOc()             ); 
                
              sql = st.getSql();//JJCastro fase2
              
          }catch(Exception e){
              throw new Exception( " insertEgresoDet: " + e.getMessage());
          }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
          }
          return sql;
      } 



    /**
     *
     * @param distrito
     * @param banco
     * @param agenciaBanco
     * @return
     * @throws Exception
     */
      public Series  getSeries(String distrito,  String banco, String agenciaBanco) throws Exception{
        Connection con = null;
        PreparedStatement st      = null;
        ResultSet         rs      = null;
        Series            serie   = null;
        String            query   = "SQL_SEARCH_SERIES";
        try{

            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito     );
            st.setString(2, banco        );
            st.setString(3, agenciaBanco );
            rs=st.executeQuery();           
            while(rs.next()){       

                int ultimo = rs.getInt   ("ULTIMO");
                int tope   = rs.getInt   ("TOPE")  ;

                if( tope >= ultimo){                
                    serie = new Series();

                    serie.setDistrito        ( rs.getString("DISTRITO")  );
                    serie.setBanco           ( rs.getString("BANCO")     );
                    serie.setAgencia_Banco   ( rs.getString("SUCURSAL")  );
                    serie.setCuenta          ( rs.getString("CUENTA")    );                    
                    serie.setPrefijo         ( ( rs.getString("PREFIJO") != null )?rs.getString("PREFIJO"):"");
                    serie.setSerial_fished_no( ( rs.getString("TOPE")    != null )?rs.getString("TOPE")   :"");
                    serie.setLast_number     ( rs.getInt   ("ULTIMO")    );                

                    //Jescandon 15-03-07
                    serie.setSerial_initial_no(( rs.getString("PRIMERO") != null )?rs.getString("PRIMERO"):"");//Primero
                    serie.setId              ( rs.getInt("id") );
                    break;
                }
            }

            }}catch(Exception e){
            throw new SQLException("Error en getSeries --->" + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return serie;
    }    
      
       /**
     * Retorna el sql de anulacion del egreso
     * @autor Ing. Juan M. Escandon Perez
     * @throws Exception
     * @version 1.0
     **/
    public String anularEgreso(String dstrct, String banco, String sucursal, String cheque, String usuario)throws Exception{
        
        StringStatement  st = null;
        String query = "SQL_UPDATE_EGRESO";
        
        try{
            
            Cheque paymnt = new Cheque();
            paymnt.setDistrito(dstrct);
            paymnt.setBanco(banco);
            paymnt.setSucursal(sucursal);
            
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2 sin
            st.setString(1, dstrct);
            st.setString(2, banco);
            st.setString(3, sucursal);
            st.setString(4, cheque);
            
        } catch(Exception e){
            e.printStackTrace();
            throw new Exception( "Dao: anularEgreso "+e.getMessage());
        }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
          }
        return st.getSql();//JJCastro fase2
    }



}
