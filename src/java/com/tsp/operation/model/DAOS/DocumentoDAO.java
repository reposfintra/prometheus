/********************************************************************
 *      Nombre Clase.................   DocumentoDAO.java
 *      Descripci�n..................   DAO de la tabla tbldoc
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   13.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;

/**
 *
 * @author  Andres
 */
public class DocumentoDAO extends MainDAO{
    
    private Documento doc;
    private Vector docs;
    private Vector vtdocumentos;
    
    private static final String SQL_INSERT = "insert into tbldoc(reg_status, dstrct, document_type," +
    " document_name, banco, ref_1, sigla, last_update, user_update, creation_date, creation_user, base) " +
    "values('',?,?,?,?,?,?,'now()',?,'now()',?,?)";
    
    private static final String SQL_UPDATE = "update tbldoc set reg_status=?, document_type=?," +
    " document_name=?, banco=?, ref_1=?, sigla=?, last_update='now()', user_update=?" +
    " where dstrct=? and document_type=?";
    
    private static final String SQL_ANULAR = "update tbldoc set reg_status='A' " +
    " where dstrct=? and document_type=?";
    
    private static final String SQL_OBTENER = "select * from tbldoc where dstrct=? and document_type=?";
    
    private static final String SQL_LISTAR = "select * from tbldoc where reg_status<>'A' order by document_type";
    
    private static final String SQL_LISTAR_CIA = "select * from tbldoc where reg_status<>'A' and dstrct=? order by document_type";
    
    
    /** Creates a new instance of DocumentoDAO */
    public DocumentoDAO() {
        super("DocumentoDAO.xml");//JJCastro fase2
    }
    public DocumentoDAO(String dataBaseName) {
        super("DocumentoDAO.xml", dataBaseName);//JJCastro fase2
    }
    
    /**
     * Inserta un nuevo registro.
     * @autor Tito Andr�s Maturana
     * @throws SQLException
     * @version 1.0
     */
    public void insertarDocumento() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_INSERT";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, doc.getDistrito());
                st.setString(2, doc.getC_document_type().toUpperCase());
                st.setString(3, doc.getC_document_name().toUpperCase());
                st.setString(4, doc.getC_banco());
                st.setString(5, doc.getC_ref_1());
                st.setString(6, doc.getC_sigla());
                st.setString(7, doc.getUsuario_creacion());
                st.setString(8, doc.getUsuario_creacion());
                st.setString(9, doc.getBase());
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL INSERTAR EL DOCUMENTO: " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Actualiza un registro.
     * @autor Tito Andr�s Maturana
     * @throws SQLException
     * @version 1.0
     */
    public void actualizarDocumento() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_UPDATE";//JJCastro fase2

        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, doc.getEstado());
                st.setString(2, doc.getC_ndocument_type().toUpperCase());
                st.setString(3, doc.getC_document_name().toUpperCase());
                st.setString(4, doc.getC_banco());
                st.setString(5, doc.getC_ref_1());
                st.setString(6, doc.getC_sigla());
                st.setString(7, doc.getUsuario_modificacion());
                st.setString(8, doc.getDistrito());
                st.setString(9, doc.getC_document_type().toUpperCase());
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL ACTUALIZAR EL DOCUMENTO: " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Anula un registro. Establece el valor del reg_status en 'A'
     * @autor Tito Andr�s Maturana
     * @param cia Distrito
     * @param codigo doctype Codigo del documento
     * @throws SQLException
     * @version 1.0
     */
    public void anularDocumento(String cia, String doctype) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ANULAR";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cia);
                st.setString(2, doctype.toUpperCase());
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL ANULAR EL DOCUMENTO: " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Obtiene un registro.
     * @autor Tito Andr�s Maturana
     * @param cia Distrito
     * @param codigo doctype Codigo del documento
     * @throws SQLException
     * @version 1.0
     */
    public void obtenerDocumento(String cia, String doctype) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        this.doc = null;
        String query = "SQL_OBTENER";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cia);
                st.setString(2, doctype.toUpperCase());
                rs = st.executeQuery();
                
                if( rs.next() ){
                    doc = new Documento();
                    doc.Load(rs);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL OBTENER EL DOCUMENTO: " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    /**
     * Obtiene una lista de registros.
     * @autor Tito Andr�s Maturana
     * @throws SQLException
     * @version 1.0
     */
    public void obtenerDocumentos() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        this.doc = null;
        this.docs = new Vector();
        String query = "SQL_LISTAR";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                
                while( rs.next() ){
                    doc = new Documento();
                    doc.Load(rs);
                    docs.add(doc);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL LISTAR LOS DOCUMENTOS: " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    /**
     * Obtiene una lista de registrode del mismo distrito
     * @autor Tito Andr�s Maturana
     * @param cia Distrito
     * @throws SQLException
     * @version 1.0
     */
    public void obtenerDocumentos(String distrito) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        this.doc = null;
        this.docs = new Vector();
        String query = "SQL_LISTAR_CIA";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito);
                rs = st.executeQuery();
                
                while( rs.next() ){
                    doc = new Documento();
                    doc.Load(rs);
                    docs.add(doc);
                    doc = null;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL LISTAR LOS DOCUMENTOS: " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    /**
     * Getter for property doc.
     * @return Value of property doc.
     */
    public com.tsp.operation.model.beans.Documento getDoc() {
        return doc;
    }
    
    /**
     * Setter for property doc.
     * @param doc New value of property doc.
     */
    public void setDoc(com.tsp.operation.model.beans.Documento doc) {
        this.doc = doc;
    }
    
    /**
     * Getter for property docs.
     * @return Value of property docs.
     */
    public java.util.Vector getDocs() {
        return docs;
    }
    
    /**
     * Setter for property docs.
     * @param docs New value of property docs.
     */
    public void setDocs(java.util.Vector docs) {
        this.docs = docs;
    }
    //David 23.12.05
    /**
     * Este m�todo genera un Vector con los tipos de socumentos de la bdEste m�todo genera un Vector con los tipos de socumentos de la bda 
     * @param String agencia 
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    private static final String LISTAR_DOC="select document_name,document_type from tbldoc";

/**
 * 
 * @return
 * @throws SQLException
 */
    public Vector listarTDocumentos()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        vtdocumentos = null;
        String query = "LISTAR_DOC";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                vtdocumentos = new Vector();
                
                while(rs.next()){
                    doc = new Documento();
                    doc.setC_document_name(rs.getString("document_name"));
                    doc.setC_document_type(rs.getString("document_type"));
                    vtdocumentos.add(doc);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS TIPOS DE DOCS " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return vtdocumentos;
    }
    
    /**
     * Este m�todo genera un Vector con los tipos de documento pertenecientes a CXPDOC
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    private static final String LISTAR_DOC_APLICA="select document_name,document_type from tbldoc as a,tblapl_doc as b  where b.documento=a.document_type and b.aplicacion='CXP' ";


/**
 *
 * @return
 * @throws SQLException
 */
    public Vector listarTDocumentosPorAplicacion()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        vtdocumentos = null;
        String query = "LISTAR_DOC_APLICA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                vtdocumentos = new Vector();     
                while(rs.next()){
                    doc = new Documento();
                    doc.setC_document_name(rs.getString("document_name"));
                    doc.setC_document_type(rs.getString("document_type"));
                    vtdocumentos.add(doc);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS TIPOS DE DOCS " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return vtdocumentos;
    }
}
