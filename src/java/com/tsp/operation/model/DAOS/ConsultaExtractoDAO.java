/*
 * ConsultaExtractoDAO.java
 *
 * Created on 15 de enero de 2007, 10:13 AM
 */

package com.tsp.operation.model.DAOS;


import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;
/**
 *
 * @author  equipo
 */
public class ConsultaExtractoDAO extends MainDAO {
    
    /** Creates a new instance of ConsultaExtractoDAO */
    public ConsultaExtractoDAO() {
        super("ConsultaExtractoDAO.xml");
    }
    public ConsultaExtractoDAO(String dataBaseName) {
        super("ConsultaExtractoDAO.xml", dataBaseName);
    }
    
    /**
     * Metodo para obtener los egresos de un propietario a partir de un 
     * periodo dado
     * @param nit, nit del propietario
     * @param dstrct, distrito del propietario
     * @param finicial, fecha inicial de la consulta
     * @param ffinal, fecha final de la consulta
     * @throws Exception.
     * @return Vector de egresos
     */
    public Vector obtenerEgresos(String dstrct, String nit, String finicial, String ffinal) throws Exception {
        Connection        con = null;
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        Vector            lista = new Vector();
        String            query = "SQL_OBTENER_EGRESOS";
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con==null)
                throw new Exception("Sin conexion");
            
            
            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, dstrct   );
            st.setString(2, nit      );
            st.setString(3, finicial + " 00:00:00" );
            st.setString(4, ffinal   + " 23:59:59" );
            rs = st.executeQuery();
            while(rs.next()){
                Egreso eg = new Egreso();
                eg.setDstrct         ( Util.coalesce(rs.getString("dstrct")         , "" ) );
                eg.setBranch_code    ( Util.coalesce(rs.getString("branch_code")    , "" ) );
                eg.setBank_account_no( Util.coalesce(rs.getString("bank_account_no"), "" ) );
                eg.setDocument_no    ( Util.coalesce(rs.getString("document_no")    , "" ) );
                eg.setNit            ( Util.coalesce(rs.getString("nit")            , "" ) );
                eg.setPayment_name   ( Util.coalesce(rs.getString("payment_name")   , "" ) );
                eg.setAgency_id      ( Util.coalesce(rs.getString("agency_id")      , "" ) );
                eg.setPmt_date       ( Util.coalesce(rs.getString("pmt_date")       , "0099-01-01" ) );
                eg.setPrinter_date   ( Util.coalesce(rs.getString("printer_date")   , "0099-01-01" ) );
                eg.setConcept_code   ( Util.coalesce(rs.getString("concept_code")   , "" ) );
                eg.setConcept_name   ( Util.coalesce(rs.getString("concept_desc")   , "" ) );
                eg.setVlr            ( rs.getFloat("vlr"));
                eg.setVlr_for        ( rs.getFloat("vlr_for"));
                eg.setCurrency       ( Util.coalesce(rs.getString("currency")      , "" ) );
                eg.setCreation_date  ( Util.coalesce(rs.getString("creation_date") , "0099-01-01 00:00:00" ) );
                eg.setFechaEntrega   ( Util.coalesce(rs.getString("fecha_entrega") , "0099-01-01 00:00:00" ) );
                eg.setFechaEnvio     ( Util.coalesce(rs.getString("fecha_envio")   , "0099-01-01 00:00:00" ) );
                eg.setFechaRecibido  ( Util.coalesce(rs.getString("fecha_recibido"), "0099-01-01 00:00:00" ) );
                
                if (!this.egresoAnulado(eg) ){
                    lista.add(eg);
                }
            }
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    
    /**
     * Metodo para obtener el detalle de los egresos de un propietario a partir de un 
     * periodo dado
     * @param dstrct, distrito
     * @param banco, banco del egreso
     * @param sucursal, sucursal del egreso
     * @param documento, numero del egreso
     * @throws Exception.
     * @return Vector de egresos
     */
    public Vector obtenerDetalleEgresos(String dstrct, String banco, String sucursal, String documento) throws Exception {
        Connection        con = null;
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        Vector            lista = new Vector();
        String            query = "SQL_OBTENER_DETALLE_EGRESOS";
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con==null)
                throw new Exception("Sin conexion");
            
            
            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, dstrct     );
            st.setString(2, banco      );
            st.setString(3, sucursal   );
            st.setString(4, documento  );
            rs = st.executeQuery();
            while(rs.next()){
                Egreso eg = new Egreso();
                String agencia = Util.coalesce(rs.getString("agencia") , "" );
                agencia = (agencia.equals("")?"": " - " + agencia );
                
                eg.setDstrct         ( Util.coalesce(rs.getString("dstrct")         , "" ) );
                eg.setBranch_code    ( Util.coalesce(rs.getString("branch_code")    , "" ) );
                eg.setBank_account_no( Util.coalesce(rs.getString("bank_account_no"), "" ) );
                eg.setDocument_no    ( Util.coalesce(rs.getString("document_no")    , "" ) );
                eg.setItem_no        ( Util.coalesce(rs.getString("item_no")    , "" ) );
                eg.setConcept_code   ( Util.coalesce(rs.getString("concept_code")   , "" ) );
                eg.setConcept_name   ( Util.coalesce(rs.getString("concept_desc")   , "" ) );
                eg.setVlr            ( rs.getFloat("vlr"));
                eg.setVlr_for        ( rs.getFloat("vlr_for"));
                eg.setCurrency       ( Util.coalesce(rs.getString("currency")      , "" ) );
                eg.setOc             ( Util.coalesce(rs.getString("oc")            , "" ) );
                eg.setDescripcion    ( Util.coalesce(rs.getString("description")   , "" ) + agencia );
                eg.setTasa           ( rs.getDouble("tasa"));
                eg.setTipo_documento ( Util.coalesce(rs.getString("tipo_documento"), "" ) );
                eg.setDocumento      ( Util.coalesce(rs.getString("documento")     , "" ) );
                eg.setTipo_pago      ( Util.coalesce(rs.getString("tipo_pago")     , "" ) );
                
                lista.add(eg);
                eg = null;//Liberar Espacio JJCastro
            }
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    
   /**
     * Metodo para verificar si un egreso esta anulado
     * @param Egreso, 
     * @throws Exception.
     * @return boolean true si esta anulado y false en caso contrario.
     */
    public boolean egresoAnulado(Egreso eg) throws Exception {
        Connection        con = null;
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        boolean           status = false;
        String            query  = "SQL_VERIFICAR_EGRESO";
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con==null)
                throw new Exception("Sin conexion");
            
            
            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, eg.getDstrct() );
            st.setString(2, eg.getBranch_code() );
            st.setString(3, eg.getBank_account_no() );
            st.setString(4, eg.getDocument_no()  );
            rs = st.executeQuery();
            if (rs.next()){
                status = true;
            }
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return status;
    }    
        
    /**
     * Metodo para obtener las facturas pendientes por cancelatar a un propietario
     * @param dstrct, distrito
     * @param proveedor, nit del propietario 
     * @throws Exception.
     * @return Vector de egresos
     */
    public Vector obtenerFacturasPendientes(String dstrct, String proveedor) throws Exception {
        Connection        con = null;
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        Vector            lista = new Vector();
        String            query = "SQL_FACTURAS_PENDIENTES";
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con==null)
                throw new Exception("Sin conexion");
            
            
            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, dstrct    );
            st.setString(2, proveedor );
            rs = st.executeQuery();
            while(rs.next()){
                CXP_Doc fact = new CXP_Doc();
                fact.setDstrct              ( Util.coalesce(rs.getString("dstrct")         , "" ) );
                fact.setProveedor           ( Util.coalesce(rs.getString("proveedor")      , "" ) );
                fact.setTipo_documento      ( Util.coalesce(rs.getString("tipo_documento") , "" ) );
                fact.setDocumento           ( Util.coalesce(rs.getString("documento")      , "" ) );
                fact.setDescripcion         ( Util.coalesce(rs.getString("descripcion")    , "" ) );
                fact.setAgencia             ( Util.coalesce(rs.getString("agencia")        , "" ) );                
                fact.setVlr_neto            ( rs.getDouble("vlr_neto") );
                fact.setVlr_total_abonos    ( rs.getDouble("vlr_total_abonos"));
                fact.setVlr_saldo           ( rs.getDouble("vlr_saldo"));
                fact.setVlr_neto_me         ( rs.getDouble("vlr_neto_me") );
                fact.setVlr_total_abonos_me ( rs.getDouble("vlr_total_abonos_me"));
                fact.setVlr_saldo_me        ( rs.getDouble("vlr_saldo_me"));
                fact.setTasa                ( rs.getDouble("tasa"));                
                fact.setMoneda              ( Util.coalesce(rs.getString("moneda")         , "" ) );                
                fact.setBanco               ( Util.coalesce(rs.getString("banco")          , "" ) );
                fact.setSucursal            ( Util.coalesce(rs.getString("sucursal")       , "" ) );
                fact.setMoneda_banco        ( Util.coalesce(rs.getString("moneda_banco")   , "" ) );
                
                fact.setClase_documento     ( Util.coalesce(rs.getString("clase_documento"), "" ) );
                fact.setPlanilla            ( Util.coalesce(rs.getString("numpla")         , "" ) );
                fact.setPlaca               ( Util.coalesce(rs.getString("plaveh")         , "" ) );
                fact.setObservacion         ( Util.coalesce(rs.getString("ruta")           , "" ) );
                fact.setMoneda_dstrct       ( Util.coalesce(rs.getString("moneda_dstrct")  , "" ) );
                
                fact.setFecha_documento     ( Util.coalesce(rs.getString("fecha_documento"), "" ) );
                fact.setFecha_planilla      ( Util.coalesce(rs.getString("fecpla"), "" ) );                
                lista.add(fact);
                fact = null;//Liberar Espacio JJCastro
            }
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    
  
    /**
     * Metodo para obtener las facturas de un propietario
     * @param dstrct, distrito
     * @param proveedor, nit del propietario 
     * @param tipo_documento, tipo de docuemnto a consultar
     * @param documento, factura a consultar
     * @throws Exception.
     * @return Vector de egresos
     */
    public CXP_Doc obtenerFactura(String dstrct, String proveedor, String tipo_documento, String documento) throws Exception {
        Connection        con   = null;
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        CXP_Doc           fact  = null;
        String            query = "SQL_FACTURAS";
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con==null)
                throw new Exception("Sin conexion");
      
            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, dstrct    );
            st.setString(2, proveedor );
            st.setString(3, tipo_documento );
            st.setString(4, documento );
            rs = st.executeQuery();
            
            if (rs.next()){
                fact = new CXP_Doc();
                fact.setDstrct              ( Util.coalesce(rs.getString("dstrct")         , "" ) );
                fact.setProveedor           ( Util.coalesce(rs.getString("proveedor")      , "" ) );
                fact.setTipo_documento      ( Util.coalesce(rs.getString("tipo_documento") , "" ) );
                fact.setDocumento           ( Util.coalesce(rs.getString("documento")      , "" ) );
                fact.setDescripcion         ( Util.coalesce(rs.getString("descripcion")    , "" ) );
                fact.setAgencia             ( Util.coalesce(rs.getString("agencia")        , "" ) );                
                fact.setVlr_neto            ( rs.getDouble("vlr_neto") );
                fact.setVlr_total_abonos    ( rs.getDouble("vlr_total_abonos"));
                fact.setVlr_saldo           ( rs.getDouble("vlr_saldo"));
                fact.setVlr_neto_me         ( rs.getDouble("vlr_neto_me") );
                fact.setVlr_total_abonos_me ( rs.getDouble("vlr_total_abonos_me"));
                fact.setVlr_saldo_me        ( rs.getDouble("vlr_saldo_me"));
                fact.setTasa                ( rs.getDouble("tasa"));                
                fact.setMoneda              ( Util.coalesce(rs.getString("moneda")         , "" ) );                
                fact.setBanco               ( Util.coalesce(rs.getString("banco")          , "" ) );
                fact.setSucursal            ( Util.coalesce(rs.getString("sucursal")       , "" ) );
                fact.setMoneda_banco        ( Util.coalesce(rs.getString("moneda_banco")   , "" ) );
                fact.setFecha_documento     ( Util.coalesce(rs.getString("fecha_documento"), "" ) );
                fact.setClase_documento     ( Util.coalesce(rs.getString("clase_documento"), "" ) );
                fact.setPlanilla            ( Util.coalesce(rs.getString("numpla")         , "" ) );
                fact.setPlaca               ( Util.coalesce(rs.getString("plaveh")         , "" ) );
                fact.setObservacion         ( Util.coalesce(rs.getString("ruta")           , "" ) );
                fact.setFecha_documento     ( Util.coalesce(rs.getString("fecha_documento"), "" ) );
                fact.setFecha_planilla      ( Util.coalesce(rs.getString("fecpla"), "" ) );                
                
                
                fact.setItems( this.obtenerDetalleFacturas(fact) );
            }
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return fact;
    }    
    
    
  
  
    /**
     * Metodo para obtener las facturas de un propietario
     * @param CXP
     * @throws Exception.
     * @return Vector de egresos
     */
    public Vector obtenerDetalleFacturas(CXP_Doc fact) throws Exception {
        Connection        con = null;
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        Vector            lista = new Vector();
        String            query = "SQL_DETALLE_FACTURAS";
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con==null)
                throw new Exception("Sin conexion");
            
            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, fact.getDstrct()         );
            st.setString(2, fact.getProveedor()      );
            st.setString(3, fact.getTipo_documento() );
            st.setString(4, fact.getDocumento()      );
            rs = st.executeQuery();
            
            while(rs.next()){
                CXPItemDoc item = new CXPItemDoc();
                item.setDstrct        ( fact.getDstrct()         );
                item.setProveedor     ( fact.getProveedor()      );
                item.setTipo_documento( fact.getTipo_documento() );
                item.setDocumento     ( fact.getDocumento()      );
                item.setItem          ( Util.coalesce(rs.getString("item")           , "" ) );
                item.setDescripcion   ( Util.coalesce(rs.getString("descripcion")    , "" ) );
                item.setConcepto      ( Util.coalesce(rs.getString("concepto")    , "" ) );
                item.setVlr           ( rs.getDouble("vlr") );
                item.setVlr_me        ( rs.getDouble("vlr_me") );
                
                double [] iva  = this.obtenerImpuestoItem(item, "IVA" );
                double [] riva = this.obtenerImpuestoItem(item, "RIVA");
                double [] rica = this.obtenerImpuestoItem(item, "RICA");
                double [] rfte = this.obtenerImpuestoItem(item, "RFTE");
                
                item.setVlr_iva     ( iva [0] );
                item.setVlr_riva    ( riva[0] );
                item.setVlr_rica    ( rica[0] );
                item.setVlr_rfte    ( rfte[0] );
                
                item.setVlr_iva_me  ( iva [1] );
                item.setVlr_riva_me ( riva[1] );
                item.setVlr_rica_me ( rica[1] );
                item.setVlr_rfte_me ( rfte[1] );    
                
                
                if (item.getConcepto().equals("00")){
                    Planilla p = this.obtenerPlanilla(fact.getPlanilla());
                    if (p!=null){
                        item.setDescripcion( item.getDescripcion() + (!p.getRemision().equals("") ? ", " + p.getRemision():"" ) ); 
                        item.setDescripcion( item.getDescripcion() +  ", " + Util.customFormat(p.getVlrParcial()) + " " + p.getUnidcam() );                        
                    }
                }
                
                lista.add(item);
            item = null;//Liberar Espacio JJCastro
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }    
    
    
    
    
    
    /**
     * Metodo para obtener el impuesto de un item apartir del item, y el tipo de imp.
     * @param CXPItemDoc , item que se lengthbuscara el impuesto
     * @param tipo_impuesto, tipo de impuesto a consultar
     * @throws Exception.
     * @return double, vlaor del impuesto
     */
    public double[] obtenerImpuestoItem(CXPItemDoc item, String tipo_impuesto) throws Exception {
        Connection        con = null;
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        String            query = "SQL_OBTENER_IMPUESTO_ITEM";
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con==null)
                throw new Exception("Sin conexion");
            
            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, tipo_impuesto     );
            st.setString(2, item.getDstrct()    );
            st.setString(3, item.getProveedor() );
            st.setString(4, item.getTipo_documento());
            st.setString(5, item.getDocumento() );
            st.setString(6, item.getItem()      );
            rs = st.executeQuery();
            
            if (rs.next()){
                return new double [] { rs.getDouble("vlr_total_impuesto"), rs.getDouble("vlr_total_impuesto_me") };
            }
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return new double [] {0,0};
    }      

    
    
    /**
     * Metodo para obtener las facturas de un propietario
     * @param dstrct, distrito
     * @param proveedor, nit del propietario 
     * @param documentos, lista de facturas.
     * @throws Exception.
     * @return Vector de egresos
     */
    public Vector obtenerPlanillasPendientes(String dstrct, String proveedor) throws Exception {
        Connection        con = null;
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        Vector            lista = new Vector();
        String            query = "SQL_PLANILLAS_A_LIQUIDAR";
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con==null)
                throw new Exception("Sin conexion");
        
            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, dstrct    );
            st.setString(2, proveedor );
            rs = st.executeQuery();
            
            while(rs.next()){
                Planilla p = new Planilla();
                p.setDistrito            ( Util.coalesce(rs.getString("cia")            , "" ) );
                p.setNumpla              ( Util.coalesce(rs.getString("numpla")         , "" ) );
                p.setFecdsp              ( Util.coalesce(rs.getString("fecdsp")         , "" ) );
                p.setFecplanilla         ( Util.coalesce(rs.getString("fecpla")         , "" ) );
                p.setRuta_pla            ( Util.coalesce(rs.getString("ruta")           , "" ) );
                p.setAgcpla              ( Util.coalesce(rs.getString("agcpla")         , "" ) );
                p.setCedcon              ( Util.coalesce(rs.getString("cedcon")         , "" ) );
                p.setNitpro              ( Util.coalesce(rs.getString("nitpro")         , "" ) );
                p.setTipoviaje           ( Util.coalesce(rs.getString("nitpro")         , "" ) );
                p.setVlrpla              ( rs.getFloat("vlrpla") );
                p.setVlrpla2             ( rs.getFloat("vlrpla2") );
                p.setUnit_vlr            ( Util.coalesce(rs.getString("unit_vlr")       , "" ) );
                p.setUnit_cost           ( rs.getFloat("unit_cost") );
                p.setCurrency            ( Util.coalesce(rs.getString("currency")       , "" ) );
                p.setCf_code             ( Util.coalesce(rs.getString("cf_code")        , "" ) );
                p.setReg_status          ( Util.coalesce(rs.getString("reg_status")     , "" ) );
                p.setFeccum              ( Util.coalesce(rs.getString("fecha_cumplido") , "" ) );
                p.setVlrParcial          ( rs.getFloat("cantidad_cumplido") ); //cantidad cumplida
                p.setUnidcam             ( Util.coalesce(rs.getString("unidad_cumplido"), "" ) );//unidad cumplido
                p.setPlaveh              ( Util.coalesce(rs.getString("plaveh")         , "" ) );
                p.setBase                ( Util.coalesce(rs.getString("base")           , "" ) );
                
                Remesa r = this.obtenerDatosRemesa( p.getNumpla() );
                if (r!=null){
                    p.setRemision( r.getRemision() );
                }
                    
                
                p.setRemesas( this.obtenerDetalleLiquidacion( p ) );
                lista.add(p);
                p = null;//Liberar Espacio JJCastro
            }
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }     
    
    
    
    
 
    
    /**
     * Metodo para obtener un planilla
     * @param numpla, numero de la planilla.
     * @throws Exception.
     * @return Objeto tipo Planilla
     */
    public Planilla obtenerPlanilla(String numpla) throws Exception {
        Connection        con = null;
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        Planilla          p   = null;
        String            query = "SQL_DATOS_PLANILLA";
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con==null)
                throw new Exception("Sin conexion");
        
            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, numpla    );
            rs = st.executeQuery();
            
            if(rs.next()){
                p = new Planilla();
                p.setDistrito            ( Util.coalesce(rs.getString("cia")            , "" ) );
                p.setNumpla              ( Util.coalesce(rs.getString("numpla")         , "" ) );
                p.setFecdsp              ( Util.coalesce(rs.getString("fecdsp")         , "" ) );
                p.setFecplanilla         ( Util.coalesce(rs.getString("fecpla")         , "" ) );
                p.setRuta_pla            ( Util.coalesce(rs.getString("ruta")           , "" ) );
                p.setAgcpla              ( Util.coalesce(rs.getString("agcpla")         , "" ) );
                p.setCedcon              ( Util.coalesce(rs.getString("cedcon")         , "" ) );
                p.setNitpro              ( Util.coalesce(rs.getString("nitpro")         , "" ) );
                p.setTipoviaje           ( Util.coalesce(rs.getString("nitpro")         , "" ) );
                p.setVlrpla              ( rs.getFloat("vlrpla") );
                p.setVlrpla2             ( rs.getFloat("vlrpla2") );
                p.setUnit_vlr            ( Util.coalesce(rs.getString("unit_vlr")       , "" ) );
                p.setUnit_cost           ( rs.getFloat("unit_cost") );
                p.setCurrency            ( Util.coalesce(rs.getString("currency")       , "" ) );
                p.setCf_code             ( Util.coalesce(rs.getString("cf_code")        , "" ) );
                p.setReg_status          ( Util.coalesce(rs.getString("reg_status")     , "" ) );
                p.setFeccum              ( Util.coalesce(rs.getString("fecha_cumplido") , "" ) );
                p.setVlrParcial          ( rs.getFloat("cantidad_cumplido") ); //cantidad cumplida
                p.setUnidcam             ( Util.coalesce(rs.getString("unidad_cumplido"), "" ) );//unidad cumplido
                p.setPlaveh              ( Util.coalesce(rs.getString("plaveh")         , "" ) );
                p.setBase                ( Util.coalesce(rs.getString("base")           , "" ) );
                
                Remesa r = this.obtenerDatosRemesa( p.getNumpla() );
                if (r!=null){
                    p.setRemision( r.getRemision() );
                }
            }
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return p;
    }     
    
    
    
    
    /**
     * Metodo para obtener los detalles de liquidacion de una planilla
     * @autor mfontalvo
     * @param numpla, numero de la planilla
     * @throws Exception.
     * @return Vector.
     */
    private Vector obtenerDetalleLiquidacion (Planilla pla) throws Exception {
        Connection        con = null;
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        Vector            lista = new Vector();
        String            query = "SQL_DETALLE_LIQUIDACION_PLANILLA";
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con==null)
                throw new Exception("Sin conexion");
        
            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, pla.getNumpla()    );
            st.setString(2, pla.getNumpla()    );
            rs = st.executeQuery();
            
            double total = 0;
            while(rs.next()){
                OPItems p = new OPItems();
                p.setItem         ( UtilFinanzas.rellenar( rs.getRow()+""        , "0", 3));
                p.setConcepto     ( Util.coalesce(rs.getString("concept_code")   , "" ) );
                p.setDescripcion  ( Util.coalesce(rs.getString("concept_desc")   , "" ) );
                p.setVlr_me       ( rs.getDouble("valor"));
                p.setPorc_rfte    ( Util.coalesce(rs.getString("p_rfte")         , "" ) );
                p.setPorc_rica    ( Util.coalesce(rs.getString("p_rica")         , "" ) );
                p.setMoneda       ( Util.coalesce(rs.getString("currency")       , "" ) );
                p.setAsignador    ( Util.coalesce(rs.getString("application_ind"), "" ) );
                p.setIndicador    ( Util.coalesce(rs.getString("ind_vlr")        , "" ) );                
                
                if (p.getConcepto().equals("00")){
                    p.setDescripcion( p.getDescripcion() + (!pla.getRemision().equals("") ? ", " + pla.getRemision():"" ) ); 
                    p.setDescripcion( p.getDescripcion() +  ", " + Util.customFormat(pla.getVlrParcial()) + " " + pla.getUnidcam() );
                }
                
                if (p.getIndicador().equals("V"))
                    total += p.getVlr_me();
                lista.add(p);
            }
            
            for (int i=0; i<lista.size(); i++){
                OPItems p = (OPItems) lista.get(i);
                if (p.getIndicador().equals("P"))
                    p.setVlr_me( (p.getVlr_me()/100.0) * total );
            }
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    

    
    /**
     * Metodo para obtener los datos de una remesa a traves de una planilla
     * @autor mfontalvo
     * @param numpla, numero de la planilla
     * @throws Exception.
     * @return Vector.
     */
    private Remesa obtenerDatosRemesa(String numpla) throws Exception {
        Connection        con = null;
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        Remesa            rem = null;
        String            query = "SQL_DATOS_REMESA";
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con==null)
                throw new Exception("Sin conexion");
        
            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, numpla    );
            rs = st.executeQuery();            
            if(rs.next()){
                rem = new Remesa();
                rem.setRemision( Util.coalesce( rs.getString("remision") ,""));
            }
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return rem;
    }
        
}
