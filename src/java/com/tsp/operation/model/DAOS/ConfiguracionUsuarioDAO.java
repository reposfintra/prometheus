/********************************************************************
 *      Nombre Clase.................   ConfiguracionUsuarioDAO.java
 *      Descripci�n..................   lase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  dlamadrid
 */
public class ConfiguracionUsuarioDAO {
    private Vector vConf;
    private TreeMap tConf;
    /** Creates a new instance of ConfiguracionUsuarioDAO */
    public ConfiguracionUsuarioDAO () {
    }
    
    /**
     * Getter for property vConf.
     * @return Value of property vConf.
     */
    public java.util.Vector getVConf () {
        return vConf;
    }
    
    /**
     * Setter for property vConf.
     * @param vConf New value of property vConf.
     */
    public void setVConf (java.util.Vector vConf) {
        this.vConf = vConf;
    }
    
    private static final String INSERTAR = "insert into configuracion_usuario(nombre,configuracion,linea,var1,var2,last_update,user_update,creation_date,creation_user,base,dstrct) values(?,?,?,?,?,'0099-01-01 00:00:00','',now(),?,'',?)";
    private static final String CONFPORID="select * from configuracion_usuario where id=?";
    private static final String ELIMINAR = "delete from configuracion_usuario where id=?";
    private static final String LISTAR="select id,nombre from configuracion_usuario where creation_user=?";
    private static final String LISTAR_CAMP = "select id, linea from configuracion_usuario where creation_user=?";
    
    
     /**
     * Este m�todo se encarga de insertar una configuracion hecha por el usuario en el programa de control trafico
     * @author  ing David lamadrid
     * @version 1.0
     * @param String id
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void insertarConfiguracion (ConfiguracionUsuario c) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (INSERTAR);
                st.setString (1, c.getNombre ());
                st.setString (2, c.getConfiguracion ());
                st.setString (3, c.getLinea ());
                st.setString (4, c.getVar1 ());
                st.setString (5, c.getVar2 ());
                st.setString (6, c.getCreation_user ());
                st.setString (7, c.getDstrct ());
                st.executeUpdate ();
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL LISTAR IDENTIDAD" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
    
     /**
     * Este m�todo se encarga de eliminar  una configuracion dado el codigo de esta
     * @author  ing David lamadrid
     * @version 1.0
     * @param String id
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void eliminarConfiguracion (String id) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (ELIMINAR);
                st.setString (1, id);
                st.executeUpdate ();
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL LISTAR IDENTIDAD" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
        
    }
    
      /**
     * Este m�todo se encarga de listar la configuracion hecha por el usuario previamente al uso del programa de control trafico
     * @author  ing David lamadrid
     * @version 1.0
     * @param String usuario
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void listarConfiguracion (String usuario) throws SQLException {
        ////System.out.println ("entro a generar filtros Usuarios");
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        this.vConf = new Vector ();
        
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (this.LISTAR);
                st.setString (1, usuario);
                rs = st.executeQuery ();
                
                while (rs.next ()){
                    Vector fila = new Vector ();
                    fila.add (rs.getString ("id"));
                    fila.add (rs.getString ("nombre"));
                    vConf.add (fila);
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL LISTAR IDENTIDAD" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
    
    /**
     * Este m�todo se encarga de listar la configuracion hecha por el usuario dado el codigo de la configuracion
     * @author  ing David lamadrid
     * @version 1.0
     * @param String id
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public ConfiguracionUsuario listarConfiguracionPorId (String id) throws SQLException {
        ////System.out.println ("entro a generar filtros Usuarios");
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        // Vector v = new Vector();
        ConfiguracionUsuario conf= new ConfiguracionUsuario ();
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (this.CONFPORID);
                int i=0;
                i= Integer.parseInt (id);
                st.setInt (1,i);
                rs = st.executeQuery ();
                
                while (rs.next ()){
                    ////System.out.println ("ENTRO EN EL seteo del objeto");
                    //  v.add (conf.load (rs));
                    conf= conf.load (rs);
                    //////System.out.println("CONFIGURACION  "+conf.getConfiguracion () );
                }
                ////System.out.println ("CONFIGURACION  "+conf.getConfiguracion () );
                ////System.out.println ("LINEA " +conf.getLinea ());
                ////System.out.println ("VAR1 "+conf.getVar1 ());
                ////System.out.println ("VAR2"+conf.getVar2 ());
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL LISTAR IDENTIDAD" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
        return conf;
    }
    
    /**
     * Getter for property tConf.
     * @return Value of property tConf.
     */
    public java.util.TreeMap getTConf () {
        return tConf;
    }
    
    /**
     * Setter for property tConf.
     * @param tConf New value of property tConf.
     */
    public void setTConf (java.util.TreeMap tConf) {
        this.tConf = tConf;
    }
    
    /**
     * Este m�todo se encarga de cargar un TreeMap con las configuraciones realizadas por el usuario en el programa de control trafico 
     * @author  ing David lamadrid
     * @version 1.0
     * @param String usuario
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void tConfiguracionesPorUsuario (String usuario) throws SQLException {
        this.tConf = new TreeMap ();
        
        //Vector banks = this.obtenerBancos();
        
        this.listarConfiguracion (usuario);
        Vector vc=this.getVConf ();
        for(int i=0; i< vc.size (); i++) {
            Vector fila = (Vector)vc.elementAt (i);
            tConf.put (fila.elementAt (1), fila.elementAt (0));
        }
    }
    
    /**
     * Obtiene las configuraciones de pantalla de un usuario
     * @param usuario Login del usuario
     * @author  Ing. Andr�s Maturana De La Cruz
     * @param usuario Login del usuario
     * @version 1.0
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void camposConfiguracionesUsuario (String usuario) throws SQLException {
        ////System.out.println ("entro a generar los campos de los filtros Usuarios");
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        this.vConf = new Vector ();
        
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (this.LISTAR_CAMP);
                st.setString (1, usuario);
                rs = st.executeQuery ();
                
                while (rs.next ()){
                    Vector ln = new Vector();
                    ln.add(rs.getString ("id"));
                    ln.add(rs.getString ("linea"));
                    vConf.add(ln);
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL LISTAR CAMPOS DE LAS CONFIGURACIONES DEL USUARIO" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
}
