/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.google.gson.JsonObject;

import com.tsp.operation.model.beans.Usuario;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

/**
 *
 * @author dvalencia
 */
public interface AdmonRecursosHumanosDAO {

    public String cargarTipoProveedor();

    public String guardarTipoProveedor(Usuario usuario, String codigo, String descripcion);

    /**
     *
     * @param usuario
     * @param descripcion
     * @param id
     * @return 
     */
    public String actualizarTipoProveedor(Usuario usuario, String codigo, String descripcion, String id);

    public String cambiarEstadoTipoProveedor(Usuario usuario, String id);

    public String cargarEPS();

    public String guardarEPS(Usuario usuario, String descripcion);

    public String actualizarEPS(Usuario usuario, String descripcion, String id);

    public String cambiarEstadoEPS(Usuario usuario, String id);

    public String cargarARL();

    public String guardarARL(Usuario usuario, String descripcion);

    public String actualizarARL(Usuario usuario, String descripcion, String id);

    public String cambiarEstadoARL(Usuario usuario, String id);

    public String cargarAFP();
    
    public String cargarAFP_CES();

    public String guardarAFP(Usuario usuario, String descripcion);

    public String actualizarAFP(Usuario usuario, String descripcion, String id);

    public String cambiarEstadoAFP(Usuario usuario, String id);

    public String cargarCCF();

    public String guardarCCF(Usuario usuario, String descripcion);

    public String actualizarCCF(Usuario usuario, String descripcion, String id);

    public String cambiarEstadoCCF(Usuario usuario, String id);

    public String cargarDependencias();

    public String guardarDependencias(Usuario usuario, String descripcion);

    public String actualizarDependencias(Usuario usuario, String descripcion, String id);

    public String cambiarEstadoDependencias(Usuario usuario, String id);

    public String cargarCargos();

    public String guardarCargos(Usuario usuario, String descripcion, String codigo);

    public String actualizarCargos(Usuario usuario, String descripcion, String codigo, String id);

    public String cambiarEstadoCargos(Usuario usuario, String id);

    public String cargarTiposContrato();

    public String guardarTiposContrato(Usuario usuario, String descripcion);

    public String actualizarTiposContrato(Usuario usuario, String descripcion, String id);

    public String cambiarEstadoTiposContrato(Usuario usuario, String id);

    public String cargarTiposRiesgos();
    
    public String guardarTiposRiesgos(Usuario usuario, String intensidad, String porcentaje, String actividades);

    public String actualizarTiposRiesgos(Usuario usuario, String intensidad, String porcentaje, String actividades, String id);

    public String cambiarEstadoTiposRiesgos(Usuario usuario, String id);

    public String cargarEmpleados();

    public String cambiarEstadoEmpleados(Usuario usuario, String id);

//    public String cargarCesantias();
//
//    /**
//     *
//     * @param usuario
//     * @param id
//     * @return
//     */
//    public String cambiarEstadoCesantias(Usuario usuario, String id);

    public String cargarTipoDoc();

    public String cargarEstadoCivil();

    public String cargarDptoExp();

    public String cargarCiudadExp(String dpto_expedicion);

    public String cargarNivelEstudio();

    public String cargarDptoNac();

    public String cargarCiudadNac(String dpto_nacimiento);

    public String cargarNivelesJerarquicos();

    public String guardarNivelesJerarquicos(Usuario usuario, String descripcion);

    public String actualizarNivelesJerarquicos(Usuario usuario, String descripcion, String id);

    public String cambiarEstadoNivelesJerarquicos(Usuario usuario, String id);

    public String cargarProfesiones();

    public String guardarProfesiones(Usuario usuario, String descripcion);

    public String actualizarProfesiones(Usuario usuario, String descripcion, String id);

    public String cambiarEstadoProfesiones(Usuario usuario, String id);

    public String cargarDptoResidencia();

    public String cargarCiudadResidencia(String dpto);

    public String guardarEmpleados(Usuario usuario, String proceso_meta, String proceso_interno, String linea_negocio, String producto, String nivel_jerarquico, String tipo_contrato, String duracion,String nombre_completo, String tipo_doc, String identificacion, String sexo, String fecha_expedicion, String dpto_expedicion, String ciudad_expedicion, String libreta_militar, String salario, String fecha_ingreso, String banco, String tipo_cuenta, String no_cuenta, String cargo, String riesgo, String eps, String afp, String arl, String cesantias, String ccf, String dpto, String ciudad, String direccion, String barrio, String telefono, String celular, String email, String fecha_nacimiento, String dpto_nacimiento, String ciudad_nacimiento, String estado_civil, String nivel_estudio, String profesion, String personas_a_cargo, String num_de_hijos, String total_grupo_familiar, String tipo_vivienda, String fecha_retiro, String causal_retiro, String observaciones);

    public String cargarBancos();

    public String guardarProveedor(Usuario usuario, String nombre_completo, String tipo_doc, String identificacion, String banco, String tipo_cuenta, String no_cuenta);

    public String guardarNit(Usuario usuario, String nombre_completo, String identificacion, String direccion, String ciudad, String dpto, String telefono, String celular, String email, String sexo, String fecha_nacimiento, String tipo_doc, String estado_civil, String ciudad_nacimiento, String libreta_militar, String ciudad_expedicion);

    public String actualizarEmpleados(Usuario usuario, String proceso_meta, String proceso_interno, String linea_negocio, String producto, String nivel_jerarquico, String tipo_contrato, String duracion, String nombre_completo, String tipo_doc, String identificacion, String sexo, String fecha_expedicion, String dpto_expedicion, String ciudad_expedicion, String libreta_militar, String salario, String fecha_ingreso, String banco, String tipo_cuenta, String no_cuenta, String cargo, String riesgo, String eps, String afp, String arl, String cesantias, String ccf, String dpto, String ciudad, String direccion, String barrio, String telefono, String celular, String email, String fecha_nacimiento, String dpto_nacimiento, String ciudad_nacimiento, String estado_civil, String nivel_estudio, String profesion, String personas_a_cargo, String num_de_hijos, String total_grupo_familiar, String tipo_vivienda, String fecha_retiro, String causal_retiro, String observaciones, String idEmpleado);

    public String actualizarProveedor(Usuario usuario, String nombre_completo, String tipo_doc, String identificacion, String banco, String tipo_cuenta, String no_cuenta);

    public String actualizarNit(Usuario usuario, String nombre_completo, String identificacion, String direccion, String ciudad, String dpto, String telefono, String celular, String email, String sexo, String fecha_nacimiento, String tipo_doc, String estado_civil, String ciudad_nacimiento, String libreta_militar, String ciudad_expedicion);

    public Object searchNombresArchivos(String rutaOrigen, String identificacion);

    public boolean almacenarArchivoEnCarpetaUsuario(String identificacion, String rutaOrigen, String rutaDestino, String nomarchivo);

    public String cargarMacroprocesos();

    public String cargarProcesos(String macroproceso);

    public String cargarLineasNegocio(String proceso);

    public String cargarProductos(String linea_negocio);

    public String cargarTipoProveedor2();

    public String listarEmpleados();

    public String cargarIncapacidades(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String status, String usuario);

    public String cargarTipoNovedad();

    public String cargarEnfermedades(String cod_enfermedad);

    public String cargarRazon(String tipo_novedad);

    public String cargarNombre(String identificacion);

    public String cargarJefes(Usuario usuario, String bd);

    public String guardarNovedadesIncapacidad(JsonObject obj, Usuario usuario);

    public String actualizarNovedadesIncapacidad(Usuario usuario, String tipo_novedad, String origen, String razon, String identificacion, String fecha_solicitud, String fecha_ini, String fecha_fin, String duracion_dias, String cod_enfermedad, String jefe_directo, String descripcion, String id);

    public String cambiarEstadoNovedad(Usuario usuario, String id);

    public String cargarNovedadesPorAprobar(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String status, String usuario);


    public String aprobarNovedad(Usuario usuario, String aprobado, String comentario, String id);

    public String cargarNovedadesAprobadas(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String remunerada);
    
    public String vistoBuenoNovedadIncapacidad(Usuario usuario, String observaciones, String id, Double valor_recobro_eps, Double valor_recobro_arl);

    public Object getNombresArchivosNovedades(String rutaOrigen, String identificacion);

    public boolean almacenarArchivoEnCarpetaUsuarioNovedades(String identificacion, String string, String string0, String nomarchivo);

//    public String buscarEmpleado(String identificacion, Usuario usuario);

    public String novedadTramitada(Usuario usuario, String id);

    public String calcularDiasFestivos(String fechaIni, String fechaFin);

    public String novedadNoTramite(Usuario usuario, String id);

    public String cargarTiposHorasExtras();

    public String cargarHorasExtras(String usuario);

    public String guardarHorasExtras(JsonObject obj, Usuario usuario);

    public String actualizarHorasExtras(Usuario usuario, String tipo_hora, String identificacion, String fecha_solicitud, String fecha_trabajo, String hora_ini, String hora_fin, String duracion_horas, String jefe_directo, String descripcion, String id);

    public String cambiarEstadoHorasExtras(Usuario usuario, String id);

    public String cargarHorasExtrasPorAprobar(String usuario);

    public String aprobarHorasExtras(Usuario usuario, String aprobado, String comentario, String id);

    public String cargarHorasExtrasAprobadas();

    public String vistoBuenoHorasExtras(Usuario usuario, String remunerado, String observaciones, String id);

    public String cargarNovedadesAprobadasUsuario(String usuario);

    public String cargarOpcionesMenu(String usuario);

    public JsonObject getInfoCuentaEnvioCorreo();

    public String getInfoCuentaEnvioCorreoDestino(String destinatario);

    public String cargarNombreTipoNovedad(String tipo_novedad);
    
    public String getInfoCuentaEnvioCorreoDestino2();

    public String cargarNombreJefeDirecto(String jefe_directo);

    public String getInfoCuentaEnvioCorreoDestino3(String identificacion);

    public String cargarNombreTipoHora(String tipo_hora);

    public String cargarListaEnfermedades();
    
    public String numeroSolicitudNovedad();
    
    /**
     *
     * @param usuario
     * @return
     */
    public String pocesoActualTrabajador(String identificacion);
    
    /**
     *
     * @param usuario
     * @return
     */
    public String pocesoActual(Usuario usuario);

    public String cargarTipoIncapacidad();

    public String cargarEPSoARL(String origen);

    public String insertarTrazabilidadEmpleados(Usuario usuario, String proceso_meta, String proceso_interno, String linea_negocio, String producto, String nivel_jerarquico, String tipo_contrato, String duracion, String nombre_completo, String tipo_doc, String identificacion, String sexo, String fecha_expedicion, String dpto_expedicion, String ciudad_expedicion, String libreta_militar, String salario, String fecha_ingreso, String banco, String tipo_cuenta, String no_cuenta, String cargo, String riesgo, String eps, String afp, String arl, String cesantias, String ccf, String dpto, String ciudad, String direccion, String barrio, String telefono, String celular, String email, String fecha_nacimiento, String dpto_nacimiento, String ciudad_nacimiento, String estado_civil, String nivel_estudio, String profesion, String personas_a_cargo, String num_de_hijos, String total_grupo_familiar, String tipo_vivienda, String fecha_retiro, String causal_retiro, String observaciones);

    public String cargarTrazabilidad(String identificacion2);

    public String cargarOpcionesMenuGestion(String usuario);

    public String cargarIncapacidades2(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, Usuario usuario, String bd);

    public String cargarIncapacidades3(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String remunerado);

    public String cargarOpcionesMenuVistoBueno(String usuario);

    public String cargarTipoVacaciones();

    public String cargarVacaciones(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String status, String usuario);

    public String calcularDiasSinFestivos(String fechaIni, String duracion_dias);

    public String cargarSaldo(String identificacion);

    public String cargarPasivoVacacional(String identificacion);
    
    public String cargarFechaIngreso(String identificacion);

    public String cargarNombreEPS(String identificacion);

    public String cargarNombreARL(String identificacion);

    public String cargarSalario(String identificacion);

    public String ingresarCXCRecobro(String documento, String nit, String descripcion, Double valor, String numsolicitud);

    public String ingresarDetalleCXCRecobro(String documento, String nit, String descripcion, Double valor);

    public String UpCP(String document_type);

//    public String actualizarPeriodoActual(String identificacion);

    public String guardarNovedadesVacaciones(JsonObject obj, Usuario usuario);

    public String aprobarSolicitudVacaciones(Usuario usuario, String comentario, String id);

    public String cargarVacacionesPorAprobar(String fechaInicio, String fechaFin, String identificacion, String status, String tipo_novedad, Usuario usuario, String bd);

    public String rechazarSolicitudVacaciones(Usuario usuario, String comentario, String id);

    public String cargarVacacionesPorVistoBueno(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String remunerado, String bd);

    public String vistoBuenoNovedadVacaciones(Usuario usuario, String observaciones, String id);

    public String actualizarPasivo(Usuario usuario, String identificacion, String duracion_dias, String dias_compensados);

    public String calcularAcumuladoPasivo(String identificacion);

    public String mostrarPeriodo(String identificacion);

    public String cargarPermisos(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String status, String usuario);

    public String cargarTipoPermisos();

    public String guardarNovedadesPermisos(JsonObject obj, Usuario usuario);
    
    public String editarNovedadesPermisos(JsonObject obj, Usuario usuario); 
    
    public String anularNovedadesPermisos(JsonObject obj, Usuario usuario);

    public String cargarPermisosPorAprobar(String fechaInicio, String fechaFin, String identificacion, String status, String tipo_novedad, Usuario usuario, String bd);

    public String aprobarSolicitudPermisos(Usuario usuario, String comentario, String id);

    public String rechazarSolicitudPermisos(Usuario usuario, String comentario, String id);

    public String cargarPermisosPorVistoBueno(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String remunerado, String bd);

    public String vistoBuenoNovedadPermisos(Usuario usuario, String observaciones, String pagar, String id);

    public String cargarCausalesRetiro();

    public String cargarBarrios(String ciudad);

    public String cargarTipoLicencias();

    public String cargarClasesPermiso();

    public String guardarNovedadesLicencias(JsonObject obj, Usuario usuario);

    public String cargarClasesLicencias(String tipo_novedad);

    public String cargarLicencias(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String status, Usuario usuario, String bd);

    public String cargarLicenciasPorAprobar(String fechaInicio, String fechaFin, String identificacion, String status, String tipo_novedad, Usuario usuario, String bd);

    public String cargarLicenciasPorVistoBueno(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String remunerado, String bd);

    public String vistoBuenoNovedadLicencia(Usuario usuario, String observaciones, String id);

    public String cargarTipoOtrasLicencias();

    public String cargarOtrasLicencias(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String status, Usuario usuario, String bd);

    public String cargarNombreTipoLicencia(String razon);

    public String cargarOtrasLicenciasPorAprobar(String fechaInicio, String fechaFin, String identificacion, String status, String tipo_novedad, Usuario usuario, String bd);

    public String aprobarSolicitudLicencias(Usuario usuario, String comentario, String id);

    public String rechazarSolicitudLicencias(Usuario usuario, String comentario, String id);

    public String cargarOtrasLicenciasPorVistoBueno(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String remunerado, String bd);

    public String listarIncapacidades(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String remunerado, String bd);

    public String listarVacaciones(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String remunerado, String bd);

    public String listarPermisos(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String remunerado, String bd);

    public String listarLicencias(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String remunerado, String bd);

    public String listarOtrasLicencias(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String remunerado, String bd);

    public boolean guardarCargosTablaTemp(String bd);

    public boolean guardarEmpleadosTablaTemp(String bd);
    
    /**
     * Leer el archivo para la distribuci�n de la n�mina
     * @param is stream de datos del archivo
     * @param usuario usuario en sesi�n
     * @param dstrct distrito
     * @param periodo a�o-mes de la nomina
     * @param periodos son multiples periodos
     * @return true si genera el archivo correctamente
     * @throws IOException
     * @throws SQLException 
     */
    public boolean leerArchivoNomina(InputStream is, String usuario, String dstrct) throws IOException, SQLException;   

    /**
     * Genera el archivo de distribuci�n de n�mina
     * @param periodos true/false si es m�ltiples periodos en el archivo
     * @param periodo a�o-mes de la nomina
     * @param usuario usuario en sesi�n
     * @return true si genera el archivo correctamente
     * @throws IOException
     * @throws SQLException 
     */
    public boolean generarArchivoDistribucion(boolean periodos, String periodo, String usuario) throws IOException, SQLException;
}
