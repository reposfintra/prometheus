/***********************************************************************************
 * Nombre clase : ............... TiempoViajeCarbonDAO. java                       *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.                                       *
 * Autor :....................... Ing. Diogenes Antonio Bastidas Morales           *
 * Fecha :........................ 19 de noviembre de 2005, 10:13 AM               *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/
 

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.*;
/**
 *
 * @author  dbastidas
 */
public class TiempoViajeCarbonDAO {
    
    /** Creates a new instance of TiempoViajeCarbonDAO */
    public TiempoViajeCarbonDAO() {
    }
    
    private TiempoViajeCarbon tiempocarbon;
    private List listiempo;
    
        
    
    public static String SQL_TIEMPO =   " select p.*, 		                "+ 
                                        "       pro.nombre as Propietario,	"+
                                        "       con.nombre as conductor,        " +
                                        "       tg.descripcion as ruta2	        "+
                                        "  from					"+
                                        " (					"+
                                        " select                                " +
                                        "	 a.numpla,		        "+
                                        "        a.fecdsp ,			"+
                                        "        a.last_update,                 "+
                                        "        a.plaveh, 			"+
                                        "        a.platlr, 			"+
                                        "        a.cedcon, 			"+
                                        "        a.nitpro,			"+
                                        "        a.pesoreal,			"+
                                        "        c.remision,			"+
                                        "        c.std_job_no,			"+
                                        "        e.std_job_desc, 		"+
                                        "        d.full_weight    as lleno_pto, "+
                                        "        d.empty_weight   as vacio_pto,	"+
                                        "        d.full_weight_m  as lleno_min,	"+
                                        "        d.empty_weight_m as vacio_min,	"+
                                        "        (d.full_weight_m - d.full_weight) as neto_min, "+
                                        "        (d.full_weight_m - d.full_weight) - a.pesoreal as dif_pesos "+
                                        "  from					"+
                                        "      planilla  a,			"+
                                        "      plarem    b,			"+
                                        "      remesa    c, 			"+
                                        "      plaaux    d,			"+
                                        "             stdjob    e		"+
                                        " where 				"+
                                        "       a.feccum between ? and ?	"+
                                        "   and (a.base  = 'spo' or a.base = 'pco') "+
                                        "   and c.numrem = b.numrem		"+
                                        "   and b.numpla = a.numpla  		"+
                                        "   and d.dstrct = 'FINV'		"+
                                        "   and d.pla    = a.numpla		"+
                                        "   and e.dstrct_code = 'FINV'		"+
                                        "   and e.std_job_no = c.std_job_no	"+
                                        " ) p					"+
                                        " LEFT JOIN NIT PRO ON  ( p.NITPRO = PRO.CEDULA ) "+
                                        " LEFT JOIN NIT CON ON  ( p.CEDCON = CON.CEDULA ) " +
                                        " LEFT JOIN tablagen tg ON  ( p.std_job_no = tg.table_code and table_type = 'ALIASSJ' ) ";
    
    public static String BUSCAR_TIEMPO = " SELECT time_code as code, substring(date_time_traffic,1,16 ) as time  " +
                                         " FROM PLANILLA_TIEMPO  WHERE PLA = ?  " +
                                         " order by secuence";
    
    public static String TIEMPO_DESCARGUE ="SELECT hora_descargue from horario_descargue "+
                                           "    where std_job_no = ? "+
                                           "     and ? >= hora_inicio "+
                                           "     and  ? <= hora_fin";
    
    public static String BUSCARSJ ="SELECT sj," +
                                    "      COALESCE(A.descripcion,'') as descripcion,	" +
                                    "      COALESCE(B.dato, '') as dato" +
                                    " FROM stdjobsel" +
                                    " LEFT JOIN tablagen A ON (A.table_type='ALIASSJ' AND A.table_code = sj) " +
                                    " LEFT JOIN tablagen B ON (B.table_type='TCARBON' AND B.table_code = sj) " +
                                    " WHERE " +
                                    "    stdjobsel.reg_status <> 'A'" +
                                    " ORDER BY sj";
                                    
    /**
     * Metodo tiempoViajeCarbonXLS, lista la planillas con los vehiculos de carbon que esten dentro ,
     * de un rango de fechas 
     * @param: fecha inicial, fecha final 
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public List tiempoViajeCarbonXLS(String fecini, String fecfin) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_TIEMPO);
                st.setString(1, fecini+" 07:00");
                st.setString(2, fecfin+" 07:00");
                ////System.out.println(st);
                rs = st.executeQuery();
                listiempo = new LinkedList ();
                while(rs.next()){
                    tiempocarbon = new TiempoViajeCarbon();
                    tiempocarbon.setRemision(rs.getString("remision") );
                    tiempocarbon.setFec_ingre(rs.getString("fecdsp") );
                    tiempocarbon.setPlaca(rs.getString("plaveh"));
                    tiempocarbon.setTrayler(rs.getString("platlr"));
                    tiempocarbon.setCedcon(rs.getString("cedcon"));
                    tiempocarbon.setConductor(rs.getString("conductor"));
                    tiempocarbon.setCedpro(rs.getString("nitpro"));
                    tiempocarbon.setPropietario(rs.getString("propietario"));
                    tiempocarbon.setLleno_pto(rs.getString("lleno_pto"));
                    tiempocarbon.setVacio_pto(rs.getString("vacio_pto"));
                    tiempocarbon.setNeto_pto(rs.getString("pesoreal"));
                    tiempocarbon.setLleno_min(rs.getString("lleno_min"));
                    tiempocarbon.setVacio_min(rs.getString("vacio_min"));
                    tiempocarbon.setNeto_min(rs.getString("neto_min"));
                    tiempocarbon.setDif_pesos(rs.getString("dif_pesos"));
                    tiempocarbon.setRuta(rs.getString("std_job_desc"));
                    tiempocarbon.setRuta2(rs.getString("ruta2"));
                    tiempocarbon.setLast_update(rs.getString("last_update"));
                    //EJECUTO BUSQUEDA DE LOS TIEMPOS
                    st = con.prepareStatement(BUSCAR_TIEMPO);
                    st.setString(1, rs.getString("numpla") );
                    //////System.out.println(st);
                    rs1 = st.executeQuery();
                    while (rs1.next()){
                        String code = ( rs1.getString("code") != null )? rs1.getString("code") : "";
                        String time = (rs1.getString("time") != null)? rs1.getString("time").replaceAll("-","/") : "";
                        if ( code.equals("IVIA") ){
                            tiempocarbon.setInicio_viaje( time );
                        }
                        else if ( code.equals("EMIN") ){
                            tiempocarbon.setLlegada_mina( time );
                        }
                        else if( code.equals("ECAR") ){
                            tiempocarbon.setEntrada_cargue( time );
                        }
                        else if (code.equals("SCAR") ){
                            tiempocarbon.setSalida_mina( time );
                        }
                        else if (code.equals("LLY") ){
                            tiempocarbon.setLlegada_y( time );
                        }
                        else if (code.equals("EPUE") ){
                            tiempocarbon.setEntrada_puerto( time );
                            //Buscar tiempo Descargue
                            st = con.prepareStatement(TIEMPO_DESCARGUE);
                            st.setString(1, rs.getString("std_job_no") );
                            st.setString(2, time.substring(11,13) );
                            st.setString(3, time.substring(11,13) );
                            
                            rs2 = st.executeQuery();
                            //////System.out.println(st);
                            if(rs2.next()){
                                tiempocarbon.setRangohllegada( rs2.getInt("hora_descargue") );
                            }
                        } 
                        else if (code.equals("EDES") ){
                            tiempocarbon.setEntrada_descarga( time );
                        }
                        else if (code.equals("SDES") ){
                            tiempocarbon.setSalida_descar( time );
                        }
                        else if (code.equals("SPUE") ){
                            tiempocarbon.setSalida_puerto( time );
                        }
                    }
                
                   listiempo.add(tiempocarbon);    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS VEHICULOS DE CARBON " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
            System.gc();
        }
        return listiempo;
    } 
    
   /**
     * Metodo  buscarSJ, lista los stanadres de carbon
     * @param: 
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public Vector buscarSJ( ) throws Exception{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Vector SJ = new Vector();
        PoolManager poolManager = null;
        Hashtable fila =null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(BUSCARSJ);
                
                ////System.out.println(st);
                rs = st.executeQuery();
                
                while(rs.next()){
                   fila = new Hashtable();
                   fila.put("sj", rs.getString("sj"));
                   fila.put("descripcion", rs.getString("descripcion"));
                   fila.put("tiempos", rs.getString("dato"));
                   SJ.add(fila);
                   
                }
            }
            ////System.out.println("DAO "+SJ.size());
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LOS STANDARES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
            
        }
        
        return SJ;
    } 
    
    

    
}
