/*
 * Nombre        ReporteUbicacionVehicularDAO.java
 * Descripci�n   Clase para el acceso a los datos del reporte de ubicaci�n vehicular de equipos en ruta
 * Autor         Alejandro Payares
 * Fecha         4 de octubre de 2005, 03:43 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.DAOS;

import java.util.LinkedList;
import java.util.Vector;
import java.util.Iterator;
import java.util.Hashtable;

import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.Statement;
import com.tsp.util.*;
import java.text.*;


import com.tsp.operation.model.beans.ReporteUbicacionEquipoRuta;

/**
 * Clase para el acceso a los datos del reporte de ubicaci�n vehicular de equipos en ruta
 * @author  Alejandro Payares
 */
public class ReporteUbicacionVehicularDAO extends MainDAO {
    
    
    private LinkedList datosReporte;
    
    //calculo de tiempos
    private String estado = "";
    private String ultrep = "";
    private double dempla = 0;
    private String fecultrep = "0099-01-01 00:00:00";
    
    /**
     * Un array de tipo String para almacenar los nombres de los campos en la base de
     * datos que conforman el reporte de ubicaci�n vehicular.
     */
    private String [] camposReporte;
    
    /** Creates a new instance of ReporteUbicacionVehicularDAO */
    public ReporteUbicacionVehicularDAO() {
        super("ReporteUbicacionVehicularDAO.xml");
    }
    
    /**
     * Busca las ciudades relacionadas con la ciudad pasada como parametro.
     * @param codCiudad Codigo de ciudad.
     * @throws SQLException Si un error de acceso a la base de datos ocurre.
     * @return
     */
    public LinkedList getCiudadesRelacionadas(String codCiudad)throws SQLException {
        ResultSet rs = null;
        PreparedStatement ps = null;
        LinkedList asociadas = null;
        try {
            ps = this.crearPreparedStatement("SQL_CIUDADES_ASOCIADAS");
            ps.setString(1, codCiudad );
            rs = ps.executeQuery();
            asociadas = new LinkedList();
            while( rs.next() ) {
                asociadas.add( rs.getString("CODCIU" ) );
            }
        } finally {
            if (rs != null) rs.close();
            if ( ps != null ) ps.close();
            desconectar("SQL_CIUDADES_ASOCIADAS");
            return asociadas;
        }
    }
    
    /**
     * Este m�todo genera los registros que hacen parte del reporte de ubicaci�n
     * y llegada de equipos en ruta y los guarda en una lista.
     * @param ubicVehArgs Criterios de b�squeda. Son los siguientes:
     *        ubicVehArgs[0] = C�digo del cliente.
     *        ubicVehArgs[1] = Fecha Inicial
     *        ubicVehArgs[2] = Fecha Final
     *        ubicVehArgs[3] = Tipos de Viaje (NA, RM, RC, RE, DM, DC)
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @author  Alejandro Payares
     */
    public void buscarDatosReporte(String [] ubicVehArgs )throws Exception {
        Statement ps = null;
        ResultSet rs = null;
        
        try {
            
            //          String cliente        = ubicVehArgs[0]; //request.getParameter("cliente");
            String fechaini       = ubicVehArgs[0]; //request.getParameter("fechaini");
            String fechafin       = ubicVehArgs[1]; //request.getParameter("fechafin");
            String listaTipoViaje = ubicVehArgs[2]; //request.getParameter("listaTipoViaje");
            String estadoViajes   = ubicVehArgs[3]; // Estado de los viajes (En Ruta, Por confirmar salida y Con entrega)
            String grupoequipo    = ubicVehArgs[4]; // Grupo del equipo
            String placasTrailersList = ubicVehArgs[5]; // Placa(s) / Trailer(s) separadas por comas
            String planillas      = ubicVehArgs[6]; // Lista de planillas separadas por comas
            String origen         = ubicVehArgs[7]; // Origen
            String destino        = ubicVehArgs[8]; // Destino
            String tipoReporte = ubicVehArgs[9]; // El tipo de reporte seleccionado (ultimoRep | soloVarados).
            String asociadasorigen= ubicVehArgs[10]; // Mostrar despachos con ciudades relacionadas a la agencia origen
            String asociadasdestino= ubicVehArgs[11]; // Mostrar despachos con ciudades relacionadas a la agencia destino
            String tipoBusqueda   = ubicVehArgs[12]; // Determina si el valor del par�metro "placasTrailers"
            // contiene valores de placas o de trailers.
            
            //                logger.info("******* Ultimo Reporte: " + showUltReporte );
            String planillasList = "";
            String [] planillasArray = planillas.split(",");
            if (!planillas.equals("") ) {
                for(int idx = 0; idx < planillasArray.length; idx++ ) {
                    String oc = planillasArray[idx].trim();
                    if( !oc.equals("") )
                        planillasList += (planillasList.equals("") ? "'" + oc + "'" : ",'" + oc + "'");
                }
            }
            String condirem       = "";
            Vector cond = new Vector(4, 1);
            
            StringBuffer query = new StringBuffer(obtenerSQL("SQL_PRINCIPAL"));
            String aux = (planillasList.equals("") ?
            "\n\t\t WHERE fecdsp BETWEEN '" + fechaini + " 00:00' AND '" + fechafin + " 23:59' AND vlrpla <> 0 AND status_220 <> 3 ) pl " :
                "\n\t\t WHERE numpla IN (" + planillasList + ")) pl ");
                String str = query.toString();
                str = str.replaceAll("'where'",aux);
                query = new StringBuffer(str);
                
                query.append(" ");
                // EL FILTRO ANTIGUO PARA LOS ESTADOS DE VIAJES NO FUNCIONA CORRECTAMENTE!!!
                // POR ESO, SE REDEFINIO UNO NUEVO, UN POCO MAS DEMORADO, PERO M�S EFECTIVO.
                if (!estadoViajes.equals("TODOS")) {
                    // FILTRO ANTIGUO
                    //if (estadoViajes.equals("RUTA")) {
                    //  cond.add(" position('EN VIA' IN pl.observacion) > 0 ");
                    //} else if (estadoViajes.equals("CONENTREGA")) {
                    //  cond.add(" position('FIN DE VIAJE' IN pl.observacion) > 0 ");
                    //} else if (estadoViajes.equals("PORCONF")) {
                    //  cond.add(" position('PROGRAMADO PARA SALIR' IN pl.observacion) > 0 ");
                    //}
                    
                    // NUEVO FILTRO
                    if (estadoViajes.equals("RUTA")) {
                        cond.add(
                        "\n\t\t\t get_ultimoreportetrafico(pl.numpla,pl.cia) <> '' AND " +
                        " get_ultimoreportetrafico(pl.numpla,pl.cia) <> '-' AND " +
                        " SUBSTR(get_ultimoreportetrafico(pl.numpla,pl.cia)," +
                        " LENGTH(get_ultimoreportetrafico(pl.numpla,pl.cia)) - 1, 2) <> '-E' "
                        );
                    } else if (estadoViajes.equals("CONENTREGA")) {
                        cond.add(
                        //" POSITION('FIN DE VIAJE' IN pl.observacion) > 0 AND " +
                        "\n\t\t\t SUBSTR(get_ultimoreportetrafico(pl.numpla,pl.cia)," +
                        "\n\t\t\t LENGTH(get_ultimoreportetrafico(pl.numpla,pl.cia)) - 1, 2) = '-E' "
                        );
                    } else if (estadoViajes.equals("PORCONF")) {
                        cond.add(
                        //" POSITION('PROGRAMADO PARA SALIR' IN pl.observacion) > 0 AND " +
                        "\n\t\t\t (get_ultimoreportetrafico(pl.numpla,pl.cia) = '' OR" +
                        "  get_ultimoreportetrafico(pl.numpla,pl.cia) = '-') "
                        );
                    }
                }
                //******* FIN BLOQUE TEMPORAL *********************************************
                // Se excluyen los viajes de SAFERBO (paqueteo) porque no tienen seguimiento
                // en trafico
                cond.add(" pl.plaveh NOT IN ('AIH388','TIZ628','TKD233','TNE245','SAFERB')");
                // Excluimos las planillas que no tengan Trafico
                //cond.add(" LTRIM(pl.ultimoreporte) <> '' ");
                // Excluimos las planillas ANULADAS
                //cond.add(" pl.vlrpla <> 0 ");
                //******* FIN BLOQUE TEMPORAL ********************************************
                if (!listaTipoViaje.equals("TODOS")) {
                    String [] tiposDeViaje = listaTipoViaje.split(",");
                    String condition = ((tiposDeViaje.length > 1)? "(" : "");
                    
                    for( int tv = 0; tv < tiposDeViaje.length; tv++ ) {
                        if( tiposDeViaje.length > 1 ) {
                            condition += "rm.tipoviaje = '" + tiposDeViaje[tv] + "'";
                            condition += ((tv + 1 < tiposDeViaje.length)? " OR " : "");
                        } else {
                            condition += "rm.tipoviaje = '" + tiposDeViaje[tv] + "'";
                        }
                    }
                    condition += ((tiposDeViaje.length > 1)? ")" : "");
                    cond.add( condition );
                }
                
                for( int cnd = 0; cnd < cond.size(); cnd++ ) {
                    if( cnd == 0 ) {
                        query.append((String)cond.get(cnd));
                    } else {
                        query.append("\n\t\t\t AND " + ((String)cond.get(cnd)));
                    }
                }
                
                concatenarCiudades( asociadasorigen, query, origen, "oripla");
                concatenarCiudades( asociadasdestino, query, destino, "despla");
                
                // Filtra por grupo de equipos cuando se llene el campo
                if ( !grupoequipo.equals("") ){
                    query.append( "\n\t\t\t AND ( pk.grupoid = '" + grupoequipo +
                    "' \n\t\t\t OR pk2.grupoid = '" + grupoequipo + "' )");
                }
                
                // Filtra por el (los) n�mero(s) de placa(s) del equipo / trailer(s)
                // digitados por pantalla (si fue digitado alguno).
                String [] placasTrailers = placasTrailersList.split(",");
                if( placasTrailers.length > 1 || (placasTrailers.length == 1 && !placasTrailers[0].trim().equals("")) ) {
                    query.append((tipoBusqueda.equals("placas") ?
                    "\n\t\t\t AND pl.plaveh IN (" : "\n\t\t\t AND pl.platlr IN ("));
                    StringBuffer plTrCsv = new StringBuffer("");
                    for(int pltr = 0; pltr < placasTrailers.length; pltr++) {
                        String placaTrailer = placasTrailers[pltr].trim();
                        plTrCsv.append(placaTrailer.equals("") ? "" : "'" + placaTrailer + "',");
                    }
                    if ( plTrCsv.toString().endsWith(",") ){
                        plTrCsv = (StringBuffer)plTrCsv.deleteCharAt(plTrCsv.length() - 1 );
                    }
                    ////////System.out.println("condici�n placas: "+plTrCsv);
                    query.append(plTrCsv + ")");
                }
                
                
                if ( tipoReporte.equals("ultimoRep") ) {
                    // En caso que solo se quiera mostrar el ultimo viajes de las placas
                    // (ultimo reporte) se debe modificar el orden de la consulta.
                    // En el programa ReporteUbicaEquipoRuta.jsp se debe tener en cuenta este orden.
                    // Nestor Parejo - Nov.25/2004
                    query.append("\n\t\t ORDER BY pl.plaveh,  pl.fecdsp DESC, pl.ultimoreporte DESC");
                }
                else if ( tipoReporte.equals("soloVarados") ) {
                    query.append("\n\t\t\t AND (get_ultimaobservaciontrafico(pl.numpla,'obs') LIKE '%VARADO%' OR get_ultimaobservaciontrafico(pl.numpla,'obs') LIKE '%DEMORADO%')");
                    query.append("\n\t\t ORDER BY pl.fecdsp");
                }
                else {
                    query.append("\n\t\t ORDER BY pl.fecdsp");
                }
                
                Connection con = this.conectar("SQL_PRINCIPAL");
                ps = con.createStatement();
                ////////System.out.println(query.toString());
                ////////System.out.println("Ejecutando query...");
                //System.out.println("buscarDatosReporte - query -->\n"+query.toString());
                rs =  ps.executeQuery(query.toString());
                
                datosReporte = new LinkedList();
                String placaAnt = "";
                this.buscarCamposDeReporte("(default)");
                while (rs.next()) {
                    if ( tipoReporte.equals("ultimoRep") ) {
                        // En caso que se debe mostrar solo los ultimos reportes de las
                        // placas la informacion debe venir organizada por placa y
                        // ultimo reporte en orden descendente. Nestor Parejo. Nov.25/2004
                        if (!placaAnt.equals( rs.getString("EQUIPO").trim().toUpperCase() )) {
                            datosReporte.add(ReporteUbicacionEquipoRuta.load(rs,camposReporte,fechaEstimadaLlegada(rs.getString("cia"),rs.getString("numpla")) ) );
                            
                            placaAnt = rs.getString("EQUIPO").trim().toUpperCase();
                        }
                    } else {
                        datosReporte.add(ReporteUbicacionEquipoRuta.load(rs,camposReporte,fechaEstimadaLlegada(rs.getString("cia"),rs.getString("numpla")) ) );
                    }
                    
                }
                ////////System.out.println("query ejecutado exitosamente!");
                //      }catch (SQLException e){
                //          e.printStackTrace();
                
        }catch(Exception ex){
            ex.printStackTrace();
            throw new Exception("ERROR SQL_PRINCIPAL  :  "+ex.getMessage());
        } finally {
            if (rs != null) rs.close();
            if ( ps != null ) ps.close();
            desconectar("SQL_PRINCIPAL");
        }
    }
    
    private void concatenarCiudades(String asociadas, StringBuffer query, String ciudad, String nombreCampo) throws SQLException {
        LinkedList ciudadRelList = getCiudadesRelacionadas(ciudad);
        if (asociadas.equals("S") && ciudadRelList.size() > 0) {
            // En caso que se deban tomar las ciudades relacionadas al origen.
            // el metodo getCiudadesRelacionadasOrigen se debio ejecutar previamente
            query.append(" AND pl."+nombreCampo+" IN (");
            
            Iterator ciudadRelIt = ciudadRelList.iterator();
            String codigoCiudad = "";
            for(int i=0; ciudadRelIt.hasNext(); i++ )  {
                codigoCiudad = (String)ciudadRelIt.next();
                query.append("'"+codigoCiudad+"',");
            }
            if ( !ciudadRelList.isEmpty() ){
                query.setCharAt(query.length()-1,')');
            }
        } else if ( !ciudad.toUpperCase().equals("TODOS") ) {
            // Filtra por campo de planilla
            query.append(" AND pl."+nombreCampo+" = '" + ciudad + "'");
        }
    }
    
    /**
     * Este m�todo solo puede ser accesado dentro del DAO, y permite buscar los
     * nombres de los campos del reporte de informaci�n al cliente en la base de datos. Es
     * invocado por el m�todo <code>void buscarDatosDeReporte(String [])</code> y el resultado
     * de la busqueda el devuelto por el m�todo
     * <CODE>String [] obtenerCamposReporte()</CODE>.
     * @param codigoCliente El codigo del cliente que ver� el reporte, este codigo es obtenido del usuario
     * que est� actualmente activo en la sesi�n.
     * @param codigoReporte El codigo del reporte de devoluciones.
     * @throws SQLException Si algun problema ocurre con el acceso a la Base de datos.
     */
    private void buscarCamposDeReporte(String codigoCliente) throws SQLException{
        ConsultasGeneralesDeReportesDAO cgr = new ConsultasGeneralesDeReportesDAO();
        camposReporte = cgr.buscarCamposDeReporte(codigoCliente, "ubicacionEquipoRuta", "C", "(ninguno)" );
    }
    
    /**
     * Devuelve un array de tipo String [] donde se encuentran almacenados los nombres
     * de los campos del reporte de informaci�n al cliente.
     * @return El array con los nombres de los campos del reporte de informaci�n al cliente.
     */
    public String [] obtenerCamposDeReporte(){
        return camposReporte;
    }
    
    /**
     * Este m�todo permite obtener los titulos del encabezado del reporte de
     * ubicaci�n. El Hashtable que es devuelto contiene los titulos de todos
     * los campos del reporte y la clave de cada titulo es su respectivo nombre
     * de campo en la Base de datos, para obtener los nombre de los campos del
     * reporte utilize el metodo <CODE>String [] obtenerCamposDeReporte()</CODE>.
     * @throws SQLException Si algun problema ocurre en el acceso a la Base de Datos.
     * @return Una tabla con los titulos de los campos del reporte de ubicaci�n vehicular.
     */
    public Hashtable obtenerTitulosDeReporte() throws SQLException{
        ConsultasGeneralesDeReportesDAO cgrd = new ConsultasGeneralesDeReportesDAO();
        return cgrd.obtenerTitulosDeReporte("ubicacionEquipoRuta");
    }
    
    /**
     * Permite obtener la lista donde estan almacenados los datos del reporte,
     * esta lista es llenada por el metodo buscarDatosDeReporte(...). , la lista
     * contiene en cada nodo un objeto Hashtable cuyas claves son los nombres de
     * los campos del reporte. Para obtener acceso a cada registro, basta con
     * recorrer la lista y extraer cada elemento Hashtable, y para obtener el valor
     * de un campo, basta con pasarle por parametro al metodo get(...) del Hashtable
     * el nombre del campo requerido, un ejemplo de esto ser�a asi:
     * <code>
     * <u>
     *    dao.buscarDatosDeReporte(... parametros ...);
     *    String [] campos = dao.obtenerCamposDeReporte();
     *    LinkedList lista = obtenerDatosReporte();
     *    Iterator ite = lista.iterator();
     *    while( ite.hasNext() ) {
     *    <u>
     *        Hashtable fila = (Hashtable) ite.next();
     *        for( int i = 0; i < campos.length; i++ ) {
     *        <u>
     *            String valor = ((String)fila.get(campos[i]))
     *            ////////System.out.println(campos[i] + " = " + valor );
     *        </u>
     *        }
     *    </u>
     *    }
     * </u>
     * <code>
     * @return La lista con los datos del reporte.
     */
    public LinkedList obtenerDatosReporte(){
        return datosReporte;
    }
    
    /**
     * busca la ruta de una planilla
     * @autor Ing. Diogenes Bastidas
     * @param planilla
     * @throws SQLException
     * @version 1.0.
     **/
    public String buscarRutaPlanilla(String planilla) throws SQLException{
        Connection con =null;
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SQL_BUSCAR_VIA";
        String via = "";
        try{
            ps = crearPreparedStatement(sql);
            ps.setString(1, planilla);
            rs = ps.executeQuery();
            if (rs.next()){
                estado = rs.getString("reg_status");
                ultrep = rs.getString("pto_control_ultreporte");
                dempla = rs.getDouble("demora");
                via = rs.getString("via");
                fecultrep = rs.getString("fecha_ult_reporte");
            }
        }catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR LA RUTA"+ex.getMessage()+", "+ex.getErrorCode());
        } finally {
            if(ps != null) {
                ps.close();
            }
            desconectar(sql);
        }
        return via;
    }
    
    
    /**
     * busca el tiempo entre un puesto de control y otro
     * @autor Ing. Diogenes Bastidas
     * @param distrito, origen,destino
     * @throws SQLException
     * @version 1.0.
     **/
    public double buscarTiempo(String dstrct, String origen, String destino) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SQL_TIEMPO";
        double tiempo = 0;
        try{
            ps = crearPreparedStatement(sql);
            ps.setString(1, dstrct);
            ps.setString(2, origen);
            ps.setString(3, destino);
            
            rs = ps.executeQuery();
            if (rs.next()){
                tiempo = rs.getDouble("tiempo");
            }
        }catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR TIEMPO DE LOS PC"+ex.getMessage()+", "+ex.getErrorCode());
        } finally {
            if(ps != null) {
                ps.close();
            }
            desconectar(sql);
        }
        return tiempo;
    }
    
    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public java.lang.String getEstado() {
        return estado;
    }
    
    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }
    
    /**
     * Getter for property ultrep.
     * @return Value of property ultrep.
     */
    public java.lang.String getUltrep() {
        return ultrep;
    }
    
    /**
     * Setter for property ultrep.
     * @param ultrep New value of property ultrep.
     */
    public void setUltrep(java.lang.String ultrep) {
        this.ultrep = ultrep;
    }
    
    /**
     * Getter for property dempla.
     * @return Value of property dempla.
     */
    public double getDempla() {
        return dempla;
    }
    
    /**
     * Setter for property dempla.
     * @param dempla New value of property dempla.
     */
    public void setDempla(double dempla) {
        this.dempla = dempla;
    }
    
    /**
     * Getter for property fecultrep.
     * @return Value of property fecultrep.
     */
    public java.lang.String getFecultrep() {
        return fecultrep;
    }
    
    /**
     * Setter for property fecultrep.
     * @param fecultrep New value of property fecultrep.
     */
    public void setFecultrep(java.lang.String fecultrep) {
        this.fecultrep = fecultrep;
    }
    
  
    /**
     * M�todo que retorna la fecha que esta en reporte trafico
     * @autor       Diogenes Bastidas
     * @throws      Exception
     * @version     1.0.
     * @return      Fecha
     **/
    public String buscarReporteTrafico(String planilla) throws SQLException{
        Connection con =null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SQL_REPORTE_TRAFICO";
        String val = "";
        try{
            ps = crearPreparedStatement(sql);
            ps.setString(1, planilla);
            rs = ps.executeQuery();
            if (rs.next()){
                val = rs.getString("fechareporte");
            }
        }catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR ULTIMO REPORTE"+ex.getMessage()+", "+ex.getErrorCode());
        } finally {
            if(ps != null) {
                ps.close();
            }
            desconectar(sql);
        }
        return val;
    }
    
    
    /**
     * M�todo que retorna la fecha de estimada de llegada de una planilla
     * @autor       Diogenes Bastidas
     * @throws      Exception
     * @version     1.0.
     * @return      Fecha
     **/
    public String fechaEstimadaLlegada(String dstrct, String planilla)throws Exception{
        int i=0, inicia = 0;
        double horas = 0;
        String fecha_ult_rep = "";
        Vector pc_control = new Vector();
        //busca la via de la planilla
        String via = buscarRutaPlanilla(planilla);
        if(via.equals("")){
           return fecha_ult_rep = buscarReporteTrafico(planilla);
        }
        //obtengo la fecha ultimo reporte
        fecha_ult_rep = (fecultrep.equals("0099-01-01 00:00:00"))?Utility.getDate(8):fecultrep;
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        java.util.Date fecha = fmt.parse(fecha_ult_rep);
        
        //obtengo el reg_status
        horas = dempla;
        //System.out.println(" Estado "+estado + "Demora "+horas +" Fecha "+fecha_ult_rep+ " Puesto "+ultrep);
        //System.out.println("Via "+via);
        if(!estado.equals("D")){
            for(i=0; i<via.length();i+=2){
                pc_control.add(via.substring(i,i+2));
                if(ultrep.equals(via.substring(i,i+2))){
                    inicia=i;
                }
            }
            //sumo los tiempos
            for(i=inicia; i<pc_control.size();i++){
                int sig = i+1;
                if(sig<pc_control.size()){
                    horas += buscarTiempo(dstrct, (String) pc_control.get(i),(String) pc_control.get( sig ));
                    //System.out.println((String) pc_control.get(i)+" - "+ (String) pc_control.get( sig )+" "+buscarTiempo(dstrct, (String) pc_control.get(i),(String) pc_control.get( sig )));
                }
            }
        }
        else{
            fecha = fmt.parse(Utility.getDate(8));
            horas = 0;
        }
            fecha.setTime(  fecha.getTime() + (long) ( horas * 60 * 60 * 1000 )  );
        return fmt.format(fecha);
    }
}


