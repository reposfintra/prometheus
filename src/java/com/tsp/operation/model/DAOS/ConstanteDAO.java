/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.DAOS;

/**
 *
 * @author Alvaro
 */



import java.sql.*;
import com.tsp.operation.model.beans.*;
import java.util.*;


public class ConstanteDAO extends MainDAO {


    /** Creates a new instance of ConstanteDAO */
    public ConstanteDAO() {
        super("ConstanteDAO.xml");
    }
    public ConstanteDAO(String dataBaseName) {
        super("ConstanteDAO.xml", dataBaseName);
    }





    /**
     * Crea una lista de ofertas pendientes por facturar a los clientes
     */
    public Constante getConstante(String dstrct, String codigo, String dataBaseName)throws SQLException{

        PreparedStatement st        = null;
        Connection        con       = null;
        ResultSet         rs        = null;
        Constante         constante = new Constante();

        String            query    = "SQL_GET_CONSTANTE";

        try {

            con = this.conectarJNDI( query , dataBaseName );

            
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );

            st.setString(1,dstrct);
            st.setString(2,codigo);

            rs = st.executeQuery();


            if (rs.next()){

                constante = Constante.load(rs);
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR UNA CONSTANE. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return constante;
    }


    /**
     * Crea una lista de ofertas pendientes por facturar a los clientes
     */
    public String getDescripcion(String dstrct, String codigo, String dataBaseName)throws SQLException{

        PreparedStatement st          = null;
        Connection        con         = null;
        ResultSet         rs          = null;
        Constante         constante   = null;

        String            descripcion = "";

        String            query    = "SQL_GET_CONSTANTE";

        try {
            if (dataBaseName.isEmpty() ) {
                con = this.conectarJNDI( query);
            }
            else {
                con = this.conectarJNDI( query , dataBaseName ); 
            }
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );

            st.setString(1,dstrct);
            st.setString(2,codigo);

            rs = st.executeQuery();


            if (rs.next()){

                constante = Constante.load(rs);
                descripcion = constante.getDescripcion();
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR UNA DESCRIPCION DE UNA CONSTANTE. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return descripcion;
    }





    /**
     * Crea una lista de ofertas pendientes por facturar a los clientes
     */
    public String getValor(String dstrct, String codigo, String dataBaseName)throws SQLException{

        PreparedStatement st          = null;
        Connection        con         = null;
        ResultSet         rs          = null;
        Constante         constante   = null;

        String            valor = "";

        String            query    = "SQL_GET_CONSTANTE";

        try {
            if (dataBaseName.isEmpty() ) {
                con = this.conectarJNDI( query);
            }
            else {
                con = this.conectarJNDI( query , dataBaseName );
            }
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );

            st.setString(1,dstrct);
            st.setString(2,codigo);

            rs = st.executeQuery();


            if (rs.next()){

                constante = Constante.load(rs);
                valor     = constante.getValor();
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR UN VALOR DE UNA CONSTANTE. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return valor;
    }




    /**
     * 
     */
    public String getCuentaHCIngreso(String codigo)throws SQLException{

        PreparedStatement st          = null;
        Connection        con         = null;
        ResultSet         rs          = null;

        String            valor = "";

        String            query    = "SQL_GET_CUENTA_CMC";

        try {

            con = this.conectarJNDI(query, "fintra");
            if (con != null) {

                String sql = obtenerSQL(query);
                st = con.prepareStatement(sql);

                st.setString(1, codigo);

                rs = st.executeQuery();


                if (rs.next()) {

                    valor = rs.getString("cuenta");
                }
            }
        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR UN VALOR DE UNA CONSTANTE. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return valor;
    }


}
