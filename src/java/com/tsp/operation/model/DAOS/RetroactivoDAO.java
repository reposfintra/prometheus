/***********************************************************************************
 * Nombre clase : RetroactivoDAO.java                    
 * Descripcion :                   Clase que maneja los DAO ( Data Access Object )  
 *                                los cuales contienen los metodos que interactuan 
 *                                con la BD.                                       
 * Autor :                         Ing. Diogenes Antonio Bastidas Morales          
 * Fecha :                          Created on 3 de febrero de 2006, 02:41 PM
 * Version :  1.0          
 * Copyright : Fintravalores S.A.                         
 ***********************************************************************************/


package com.tsp.operation.model.DAOS;
import java.io.*; 
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;


public class RetroactivoDAO {
    
    
    public RetroactivoDAO() {        
    }
    
    private Retroactivo retroact;
    private Vector vecrec;
    
    private static final String SQL_INSERTAR = "INSERT INTO retroactivos (std_job_no, ruta, fecha1,fecha2,valor_retro,valor_stdjob,reg_status,dstrct,last_update,user_update,creation_date,creation_user,base)" +
                                                " VALUES (?, ?,?,?,?,?,'',?,'now()',?,'now()',?,?)";
    
    private static final String SQL_MODIFICAR = "UPDATE retroactivos " +
                                                " SET reg_status = ?, " +
                                                "      valor_retro = ?," +
                                                "      valor_stdjob = ?," +
                                                "      last_update = 'now()'," +
                                                "      user_update  = ?" +
                                                " WHERE" +
                                                "      std_job_no = ? " +
                                                " AND  ruta = ?" +
                                                " AND  fecha1 = ?" +
                                                " AND  fecha2 = ?"; 
    
    private static final String SQL_ANULAR = "UPDATE retroactivos " +
                                                " SET reg_status = 'A', " +
                                                "      last_update = 'now()'," +
                                                "      user_update  = ?" +
                                                " WHERE" +
                                                "      std_job_no = ? " +
                                                " AND  ruta = ?" +
                                                " AND  fecha1 = ?" +
                                                " AND  fecha2 = ?"; 
    
    
    private static final String SQL_EXISTE  = "SELECT std_job_no " +
                                                " FROM retroactivos " +
                                                "  WHERE" +
                                                "      std_job_no = ? " +
                                                " AND  ruta = ?" +
                                                " AND  fecha1 = ?" +
                                                " AND  fecha2 = ?" +
                                                " AND  reg_status = '' "; 
    
    private static final String SQL_EXISTE_ANULADO  = "SELECT std_job_no " +
                                                " FROM retroactivos " +
                                                "  WHERE" +
                                                "      std_job_no = ? " +
                                                " AND  ruta = ?" +
                                                " AND  fecha1 = ?" +
                                                " AND  fecha2 = ?" +
                                                " AND  reg_status = 'A' "; 
    
    private static final String SQL_BUSCAR  = "SELECT a.*," +
                                                "    b.std_job_desc " +
                                                " FROM retroactivos a," +
                                                "      stdjob b " +
                                                "  WHERE" +
                                                "      a.std_job_no = ? " +
                                                " AND  a.ruta = ?" +
                                                " AND  a.fecha1 = ?" +
                                                " AND  a.fecha2 = ?" +
                                                " AND  a.reg_status = '' " +
                                                " AND  a.std_job_no = b.std_job_no"; 
    
    
    
    private static final String SQL_BUSQUEDA  = "SELECT a.std_job_no," +
                                                " a.ruta,    " +
                                                " a.fecha1,  " +
                                                " a.fecha2," +
                                                " a.valor_retro," +
                                                " a.valor_stdjob, " +
                                                " b.std_job_desc"+ 
                                              "   FROM retroactivos a," +
                                              "        stdjob b "+
                                              "    WHERE  "+
                                              "        a.std_job_no like ?  "+
                                              "   AND  a.ruta like ? "+
                                              "   AND  a.reg_status = '' " +
                                              "   AND  a.std_job_no = b.std_job_no"; 
    
    /**
     * Metodo setRetroactivo, setea el objeto de tipo retroactivo
     * @param: objeto retroactivo
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void setRetroactivo(Retroactivo recact){
        this.retroact = recact;
    }
    
    /**
     * Metodo setRetroactivo, setea el objeto de tipo retroactivo
     * @param: objeo retroactivo
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public Retroactivo getRetroactivo(){
        return retroact;
    }
    /**
     * Metodo getRetroactivos, obtien la lista de retroactivos
     * @param: objeo retroactivo
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public Vector getRetroactivos(){
        return this.vecrec;
    }
    
    /**
     * Metodo insertarRetroactivo, ingresa un registro en la tabla retroactividad
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void insertarRetroactivo() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_INSERTAR);
                st.setString(1, retroact.getStd_job_no());
                st.setString(2, retroact.getRuta());
                st.setString(3, retroact.getFecha1());
                st.setString(4, retroact.getFecha2());
                st.setDouble(5, retroact.getValor_retro());
                st.setDouble(6, retroact.getValor_stdjob());
                st.setString(7, retroact.getDstrct());
                st.setString(8, retroact.getCreation_user());
                st.setString(9, retroact.getUser_update());
                st.setString(10, retroact.getBase());
                
                 ////System.out.println(st);

                
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL INSERTAR LA RETROACTIVO" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }      
    }
        
    /**
     * Metodo modificarRetroactivo, modifica un registro en la tabla retroactivo
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void modificarRetroactivo() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_MODIFICAR);
                st.setString(1, retroact.getEstado());
                st.setDouble(2, retroact.getValor_retro());
                st.setDouble(3, retroact.getValor_stdjob());
                st.setString(4, retroact.getUser_update());
                st.setString(5, retroact.getStd_job_no());
                st.setString(6, retroact.getRuta());
                st.setString(7, retroact.getFecha1());
                st.setString(8, retroact.getFecha2());
                
                 ////System.out.println(st);

                
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL MODIFICAR EL RETROACTIVO" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }      
    }
    
    /**
     * Metodo anularRetroactivo, modifica un registro en la tabla retroactivo
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void anularRetroactivo() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_ANULAR);
                st.setString(1, retroact.getUser_update());
                st.setString(2, retroact.getStd_job_no());
                st.setString(3, retroact.getRuta());
                st.setString(4, retroact.getFecha1());
                st.setString(5, retroact.getFecha2());
                
                 ////System.out.println(st);

                
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL ANULAR EL RETROACTIVO " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }      
    }
    
     /**
     * Metodo existeRetroactivo, verifica si existe un registro
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean existeRetroactivo(String stdjob, String ruta, String fecha1, String fecha2 ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_EXISTE);
                st.setString(1, stdjob);
                st.setString(2, ruta);
                st.setString(3, fecha1);
                st.setString(4, fecha2);           
                rs = st.executeQuery();
                
                if (rs.next()){
                   sw = true;
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL VERIFICAR EL RETROACTIVO" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }   
        return sw; 
    }
    
    /**
     * Metodo listarRetroactivo, buscar el registro en retroactivo
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void listarRetroactivo(String stdjob, String ruta ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_BUSQUEDA);
                st.setString(1, stdjob+"%");
                st.setString(2, ruta+"%");
                      
                rs = st.executeQuery();
                vecrec = new Vector();
                while (rs.next()){
                   retroact = new Retroactivo();
                   retroact.setStd_job_no(rs.getString("std_job_no"));
                   retroact.setDesStdjob(rs.getString("std_job_desc"));
                   retroact.setRuta(rs.getString("ruta"));
                   retroact.setFecha1(rs.getString("fecha1"));
                   retroact.setFecha2(rs.getString("fecha2"));
                   retroact.setValor_retro(rs.getDouble("valor_retro"));
                   retroact.setValor_stdjob(rs.getDouble("valor_stdjob"));
                   
                   vecrec.add(retroact);
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL BUSCAR EL RETROACTIVO" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }      
    }
    
     /**
     * Metodo buscarRetroactivo, buscar el registro en retroactivo
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarRetroactivo(String stdjob, String ruta, String fecha1, String fecha2 ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        retroact = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_BUSCAR);
                st.setString(1, stdjob);
                st.setString(2, ruta);
                st.setString(3, fecha1);
                st.setString(4, fecha2);           
                rs = st.executeQuery();
                
                if (rs.next()){
                   retroact = new Retroactivo();
                   retroact.setStd_job_no(rs.getString("std_job_no"));
                   retroact.setRuta(rs.getString("ruta"));
                   retroact.setFecha1(rs.getString("fecha1"));
                   retroact.setFecha2(rs.getString("fecha2"));
                   retroact.setValor_retro(rs.getDouble("valor_retro"));
                   retroact.setValor_stdjob(rs.getDouble("valor_stdjob"));
                   retroact.setEstado(rs.getString("reg_status"));
                   retroact.setDstrct(rs.getString("dstrct"));
                   retroact.setLast_update(rs.getString("last_update"));
                   retroact.setUser_update(rs.getString("user_update"));
                   retroact.setCreation_date(rs.getString("creation_date"));
                   retroact.setCreation_user(rs.getString("creation_user"));
                   retroact.setBase(rs.getString("base"));
                   retroact.setDesStdjob(rs.getString("std_job_desc"));
                   this.retroact = retroact;
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL BUSCAR EL RETROACTIVO" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }      
    }
    
     /**
     * Metodo existeRetroactivoAnulado, verifica si existe un registro anulado
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean existeRetroactivoAnulado(String stdjob, String ruta, String fecha1, String fecha2 ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_EXISTE_ANULADO);
                st.setString(1, stdjob);
                st.setString(2, ruta);
                st.setString(3, fecha1);
                st.setString(4, fecha2);           
                rs = st.executeQuery();
                
                if (rs.next()){
                   sw = true;
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL VERIFICAR EL RETROACTIVO" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }   
        return sw; 
    }
    
}
