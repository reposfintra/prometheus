/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.TransportadorasLogBeans;
import java.util.ArrayList;

/**
 *
 * @author mariana
 */
public interface AdministracionEtesDAO {

    /**
     *
     * @return
     */
    public String cargarTipoManifiesto();

    /**
     *
     * @param usuario
     * @param perfil
     * @return
     */
    public String cargarEmpresas(String usuario, String perfil);

    /**
     *
     * @param transportadora
     * @return
     */
    public String cargarFechasCXCTrans(String transportadora);

    /**
     *
     * @param fechaInicio
     * @param fechaFinal
     * @param Tipo
     * @param Empresa
     * @param Estado
     * @return
     */
    public ArrayList<TransportadorasLogBeans> buscarLogTrama(String fechaInicio, String fechaFinal, String Tipo, String Empresa, String Estado, String planilla);

    /**
     *
     * @param Id
     * @return
     */
    public String cargarjsonTrama(String Id);

    public ArrayList<TransportadorasLogBeans> cargarGrillaProductosTrans();

    public ArrayList<TransportadorasLogBeans> cargarGrillaAgenciasTrans();
    
    /**
     *
     * @param cia
     * @param nombre_producto
     * @return
     */
    public String guardarProductosEds(String cia, String nombre_producto);

    /**
     *
     * @param id
     * @return
     */
    public String cambiarEstadoProductoTrans(String id);

    /**
     *
     * @param descripcion
     * @param id
     * @return
     */
    public String actualizarProductoTrans(String descripcion, String id);

    public String cargarTransportadoras();

    public ArrayList<TransportadorasLogBeans> cargarGrillaTipoDescuento();

    /**
     *
     * @param info
     * @return
     */
    public String guardarRelProductosTransportadoras(JsonObject info);

    /**
     *
     * @param idtrans
     * @return
     */
    public ArrayList<TransportadorasLogBeans> cargarGrillaRelProTrans(String idtrans);

    public String cargarProductosTrans();

    /**
     *
     * @param id
     * @return
     */
    public String actualizarRelProTrans(String id);

    public String cargarTipoDescuento();

    /**
     *
     * @param id
     * @return
     */
    public String cambiarEstadoTransPro(String id);

    /**
     *
     * @param cia
     * @param nomtipdes
     * @return
     */
    public String guardarTipoDescuento(String cia, String nomtipdes);

    /**
     *
     * @param id
     * @param nombretipodes
     * @return
     */
    public String actualizarTipoDescuento(String id, String nombretipodes);

    /**
     *
     * @param id
     * @return
     */
    public String cambiarEstadoTipoDescuento(String id);

    public ArrayList<TransportadorasLogBeans> cargarGrillaTransportadoras();

    /**
     *
     * @param nombretran
     * @param nittran
     * @param direcciontran
     * @param correo
     * @param nombreencargado
     * @param identificacion
     * @param usuario
     * @param cia
     * @param periodicidad
     * @param idusuario
     * @param cuporot //cupo totativo
     * @return
     */
    public String guardarTransportadoras(String nombretran, String nittran, String direcciontran, String correo, String nombreencargado, String identificacion, String usuario, String cia, String periodicidad, String idusuario,String cuporot);

    /**
     *
     * @param pais
     * @return
     */
    public String cargarDepartamento(String pais);

    /**
     *
     * @param departamento
     * @return
     */
    public String cargarCiudad(String departamento);

    public String cargarPais();

    public String cargarPeriodicidad();

    /**
     *
     * @param nombretran
     * @param direcciontran
     * @param correo
     * @param nombreencargado
     * @param identificacion
     * @param idusuario
     * @param periodicidad
     * @param usuario
     * @param id
     * @param cuporot //cupo rotativo
     * @return
     */
    public String actualizarTransportadoras(String nombretran, String direcciontran, String correo, String nombreencargado, String identificacion, String idusuario, String periodicidad, String usuario, String cuporot,String id);

    /**
     *
     * @param id
     * @return
     */
    public String cambiarEstadoTransportadoras(String id);

    /**
     *
     * @param transportadoras
     * @param fecha
     * @param fecha_fin
     * @return
     */
    public ArrayList<TransportadorasLogBeans> cargarCxcGeneradasTransportadoras(String transportadoras, String fecha,String fecha_fin);

    /**
     *
     * @param transportadoras
     * @param fecha
     * @param cxc
     * @return
     */
    public ArrayList<TransportadorasLogBeans> cargarCxcDetallesGeneradasTransportadoras(String transportadoras, String fecha, String cxc);
    
    /**
     *
     * @param nombreagencia
     * @param transportadoras
     * @param correo
     * @param direccion
     * @param usuario
     * @param ciudad
     * @return
     */
    public String guardarAgencia(String nombreagencia, String transportadoras,String correo,String direccion,String ciudad,String usuario);
    
    /**
     *
     * @param nombreagencia
     * @param transportadoras
     * @param correo
     * @param direccion
     * @param ciudad
     * @param usuario
     * @param id
     * @return
     */
    public String actualizarAgencia(String nombreagencia, String transportadoras,String correo,String direccion,String ciudad,String usuario,String id);
     
    /**
     *
     * @param id
     * @return
     */
    public String cambiarEstadoAgencia(String id);
    
    public String verificarUsuarioTrans(String nittrans);
    
    public String AutorizarVenta(String id);
    
    public String desactivarUsuario(String estado,String usuario);
}
