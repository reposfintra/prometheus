/*
 * EquivalenciaCargaDAO.java
 *
 * Created on 12 de enero de 2007, 09:35 AM
 */

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  JDELAROSA
 */
public class EquivalenciaCargaDAO extends MainDAO{
    
    private EquivalenciaCarga equivalencia;
    private Vector Vecequivalencia;
    
    /** Creates a new instance of EquivalenciaCargaDAO */
    public EquivalenciaCargaDAO() {
        super ( "EquivalenciaCargaDAO.xml" );
    }
    
    /**
     * Getter for property equivalencia.
     * @return Value of property equivalencia.
     */
    public EquivalenciaCarga getEquivalencia() {
        return equivalencia;
    }
    
    /**
     * Setter for property equivalencia.
     * @param equivalencia New value of property equivalencia.
     */
    public void setEquivalencia(EquivalenciaCarga equivalencia) {
        this.equivalencia = equivalencia;
    }
    
    /**
     * Getter for property Vecequivalencia.
     * @return Value of property Vecequivalencia.
     */
    public Vector getVecequivalencia() {
        return Vecequivalencia;
    }
    
    /**
     * Setter for property Vecequivalencia.
     * @param Vecequivalencia New value of property Vecequivalencia.
     */
    public void setVecequivalencia(Vector Vecequivalencia) {
        this.Vecequivalencia = Vecequivalencia;
    }
    
     /**
     * Metodo: insertEquivanlenciaCarga, permite ingresar un registro a la tabla equivalencia_carga.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void insertEquivalenciaCarga () throws SQLException{
        PreparedStatement st = null;
        try{
            st = crearPreparedStatement ("SQL_INSERT");
            st.setString (1,equivalencia.getCodigo_carga());
            st.setString (2,equivalencia.getDescripcion_carga());
            st.setString (3,equivalencia.getCodigo_legal());
            st.setString (4,equivalencia.getTipo_carga());
            st.setDouble (5,equivalencia.getValor_mercancia());
            st.setString (6,equivalencia.getUnidad_mercancia());
            st.setString (7,equivalencia.getDescripcion_corta());
            st.setString (8,equivalencia.getUsuario ());
            st.setString (9,equivalencia.getBase ());
            st.setString (10,equivalencia.getDistrito());
            st.executeUpdate ();
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL INGRESAR UNA EQUIVALENCIA DE CARGA, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_INSERT");
        }
    }
    
    
     /**
     * Metodo: searchEquivalenciaCarga, permite buscar una equivalencia de carga dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : codigo y distrito
     * @version : 1.0
     */
    public void searchEquivalenciaCarga ( String codigo, String distrito )throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        equivalencia = null;
        try {
            st = crearPreparedStatement ("SQL_SERCH");
            st.setString (1,codigo);
            st.setString (2,distrito);
            rs= st.executeQuery ();
            if(rs.next ()){
                equivalencia = new EquivalenciaCarga ();
                equivalencia.setCodigo_carga        ( rs.getString("codigo_carga")!=null?rs.getString("codigo_carga"):"" );
                equivalencia.setDescripcion_carga   ( rs.getString("descripcion_carga")!=null?rs.getString("descripcion_carga"):"" );
                equivalencia.setCodigo_legal        ( rs.getString("carga_legal")!=null?rs.getString("carga_legal"):"" );
                equivalencia.setTipo_carga          ( rs.getString("clas_carga")!=null?rs.getString("clas_carga"):"" );
                equivalencia.setValor_mercancia     ( (int)rs.getDouble("vlr_mercancia") );
                equivalencia.setUnidad_mercancia    ( rs.getString("und_mercancia")!=null?rs.getString("und_mercancia"):"" );
                equivalencia.setDescripcion_corta   ( rs.getString("descripcion_corta")!=null?rs.getString("descripcion_corta"):"" );
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR UNA EQUIVALENCIA DE CARGA, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_SERCH");
        }
    }
    
    /**
     * Metodo: existEquivalenciaCarga, permite buscar una equivalencia de carga dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : codigo y distrito.
     * @version : 1.0
     */
    public boolean existEquivalenciaCarga ( String codigo, String distrito ) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        try {
            st = crearPreparedStatement ("SQL_SERCH");
            st.setString (1,codigo);
            st.setString (2,distrito);
            rs = st.executeQuery ();
            if(rs.next ())
                sw = true;
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR SI EXISTE UNA EQUIVALENCIA DE CARGA, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_SERCH");
        }
        return sw;
    }
    
    /**
     * Metodo: listEquivalenciaCarga, permite listar todas los esquemas de equivalencia
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void listEquivalenciaCarga (String distrito, String codigo, String car_legal, String tipo_carga)throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Vecequivalencia = null;
        try {
            st = crearPreparedStatement ("SQL_LIST");
            st.setString (1,distrito);
            st.setString (2,codigo+"%");
            st.setString (3,car_legal+"%");
            st.setString (4,tipo_carga+"%");
            rs= st.executeQuery ();
            Vecequivalencia = new Vector ();
            while(rs.next ()){
                equivalencia = new EquivalenciaCarga ();
                equivalencia.setCodigo_carga        ( rs.getString("codigo_carga")!=null?rs.getString("codigo_carga"):"" );
                equivalencia.setDescripcion_carga   ( rs.getString("descripcion_carga")!=null?rs.getString("descripcion_carga"):"" );
                equivalencia.setCodigo_legal        ( rs.getString("carga_legal")!=null?rs.getString("carga_legal"):"" );
                equivalencia.setTipo_carga          ( rs.getString("clas_carga")!=null?rs.getString("clas_carga"):"" );
                equivalencia.setValor_mercancia     ( rs.getDouble("vlr_mercancia") );
                equivalencia.setUnidad_mercancia    ( rs.getString("und_mercancia")!=null?rs.getString("und_mercancia"):"" );
                equivalencia.setDescripcion_corta   ( rs.getString("descripcion_corta")!=null?rs.getString("descripcion_corta"):"" );
                Vecequivalencia.add ( equivalencia );
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL LISTAR LAS EQUIVALENCIAS DE CARGA, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_LIST");
        }
    }
    
    /**
     * Metodo: updateEquivalenciaCarga, permite actualizar un esquema de equivalencia dados unos parametros.
     * @autor : Ing. Jose de la rosa
     * @param : 
     * @version : 1.0
     */
    public void updateEquivalenciaCarga () throws SQLException{
        PreparedStatement st = null;
        try{
            st = crearPreparedStatement ("SQL_UPDATE");
            st.setString (1,equivalencia.getDescripcion_carga());
            st.setString (2,equivalencia.getCodigo_legal());
            st.setString (3,equivalencia.getTipo_carga());
            st.setDouble (4,equivalencia.getValor_mercancia());
            st.setString (5,equivalencia.getUnidad_mercancia());
            st.setString (6,equivalencia.getDescripcion_corta());
            st.setString (7,equivalencia.getUsuario ());
            st.setString (8,equivalencia.getCodigo_carga());
            st.setString (9,equivalencia.getDistrito());
            st.executeUpdate ();
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL ACTUALIZAR UNA EQUIVALENCIA DE CARGA, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_UPDATE");
        }
    }
    
    /**
     * Metodo: anularEquivalenciaCarga, permite anular un esquema de equivalencia dados unos parametros.
     * @autor : Ing. Jose de la rosa
     * @param : 
     * @version : 1.0
     */
    public void anularEquivalenciaCarga () throws SQLException{
        PreparedStatement st = null;
        try{
            st = crearPreparedStatement ("SQL_ANULAR");
            st.setString (1,equivalencia.getUsuario ());
            st.setString (2,equivalencia.getCodigo_carga());
            st.setString (3,equivalencia.getDistrito());
            
            st.executeUpdate ();
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL ANULAR UNA EQUIVALENCIA DE CARGA, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_ANULAR");
        }
    }
}
