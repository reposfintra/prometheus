/*********************************************************************************
 * Nombre clase :      ReporteDemorasDAO.java                                    *
 * Descripcion :       DAO del ReporteDemorasDAO.java                            *
 *                     Clase que maneja los DAO (Data Access Object) los cuales  *
 *                     contienen los metodos que interactuan con la B.D.         *
 * Autor :             LREALES                                                   *
 * Fecha :             26 de septiembre de 2006, 10:00 AM                        *
 * Version :           1.0                                                       *
 * Copyright :         Fintravalores S.A.                                   *
 *********************************************************************************/

package com.tsp.operation.model.DAOS;

import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;
import java.util.Date;
import java.text.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.UtilFinanzas.*;

public class ReporteDemorasDAO extends MainDAO {
    
    private BeanGeneral bean;
    
    private Vector vector;
    private Vector vector_clientes;
    
    //FORMATO DATE
    private SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    //private SimpleDateFormat fmt = new SimpleDateFormat("dd hh:mm:ss");
            
    /** Creates a new instance of ReporteDemorasDAO */
    public ReporteDemorasDAO () {
        super ( "ReporteDemorasDAO.xml" );
    }
    public ReporteDemorasDAO (String dataBaseName) {
        super ( "ReporteDemorasDAO.xml", dataBaseName);
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector () {
        
        return vector;
        
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector ( java.util.Vector vector ) {
        
        this.vector = vector;
        
    }
    
    /**
     * Getter for property vector_clientes.
     * @return Value of property vector_clientes.
     */
    public java.util.Vector getVector_clientes () {
        
        return vector_clientes;
        
    }
    
    /**
     * Setter for property vector_clientes.
     * @param vector_clientes New value of property vector_clientes.
     */
    public void setVector_clientes ( java.util.Vector vector_clientes ) {
        
        this.vector_clientes = vector_clientes;
        
    }
    
    /** 
     * Funcion publica que obtiene la información que se va a generar
     * en el reporte de todos los proveedores. 
     */
    public void reporteEspecifico ( String cod_cli, String fecini, String fecfin ) throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_REPORTE_DEMORAS_ESPECIFICO" );
            
            st.setString( 1, cod_cli );
            st.setString( 2, fecini );
            st.setString( 3, fecfin );
            
            rs = st.executeQuery();
            
            this.vector = new Vector();
            
            while( rs.next() ){
                
                String planilla = rs.getString( "planilla" )!=null?rs.getString( "planilla" ).toUpperCase():"";           
                String nomagencia = rs.getString( "nomagencia" )!=null?rs.getString( "nomagencia" ).toUpperCase():"";
                String nomciuorip = rs.getString( "nomciuorip" )!=null?rs.getString( "nomciuorip" ).toUpperCase():"";
                String nomciudesp = rs.getString( "nomciudesp" )!=null?rs.getString( "nomciudesp" ).toUpperCase():"";
                String numequi = rs.getString( "numequi" )!=null?rs.getString( "numequi" ).toUpperCase():"";
                String nomconduc = rs.getString( "nomconduc" )!=null?rs.getString( "nomconduc" ).toUpperCase():"";
                String fecdes = rs.getString( "fecdes" )!=null?rs.getString( "fecdes" ).toUpperCase():"";                
                String numwork = rs.getString( "numwork" )!=null?rs.getString( "numwork" ).toUpperCase():"";
                String nomciuorir = rs.getString( "nomciuorir" )!=null?rs.getString( "nomciuorir" ).toUpperCase():"";
                String nomciudesr = rs.getString( "nomciudesr" )!=null?rs.getString( "nomciudesr" ).toUpperCase():"";
                String estandar = rs.getString( "estandar" )!=null?rs.getString( "estandar" ).toUpperCase():"";
                String nomcliente = rs.getString( "nomcliente" )!=null?rs.getString( "nomcliente" ).toUpperCase():"";
                String nomdestin = rs.getString( "nomdestin" )!=null?rs.getString( "nomdestin" ).toUpperCase():"";
                String fechareporte = rs.getString( "fechareporte" )!=null?rs.getString( "fechareporte" ).toUpperCase():""; 
                String nomsupe = rs.getString( "nomsupe" )!=null?rs.getString( "nomsupe" ).toUpperCase():""; 
                String nomdemo = rs.getString( "nomdemo" )!=null?rs.getString( "nomdemo" ).toUpperCase():""; 
                
                String factura = rs.getString( "factura" )!=null?rs.getString( "factura" ).toUpperCase():""; 
                String documento = rs.getString( "documento" )!=null?rs.getString( "documento" ).toUpperCase():""; 
                String comercial = factura.equals( "" )?documento:factura;
                String observacion = rs.getString( "observacion" )!=null? rs.getString( "observacion" ) : ""; //Osvaldo
                
                bean = new BeanGeneral ();
                
                bean.setValor_01( planilla );
                bean.setValor_02( fechareporte );// fecha inicio demora
                String fecha = fechaProxRepMovTra ( planilla, fechareporte ) != null?fechaProxRepMovTra ( planilla, fechareporte ):"";
                bean.setValor_03( fecha );// fecha fin demora
                bean.setValor_04( nomagencia );
                bean.setValor_05( nomciuorip );
                bean.setValor_06( nomciudesp );
                bean.setValor_07( numequi );
                bean.setValor_08( nomconduc );
                bean.setValor_09( fecdes );
                bean.setValor_10( numwork );
                bean.setValor_11( nomciuorir );
                bean.setValor_12( nomciudesr );
                bean.setValor_13( estandar );
                bean.setValor_14( nomcliente );
                bean.setValor_15( nomdestin );
                
                bean.setValor_16( fecini );// fecini
                bean.setValor_17( fecfin );// fecfin
                     
                bean.setValor_18( nomsupe );// usuario creo
                bean.setValor_19( nomdemo );// descripcion de la demora
                bean.setValor_20( comercial );// numero de la factura
                
                if ( fecha.equals("") ) {
                    bean.setValor_21( "" );
                } else {
                    double dif_real = ((double)( fmt.parse( fecha ).getTime() - fmt.parse( fechareporte ).getTime())/(double)(1000*60*60*24));
                    bean.setValor_21( "" + UtilFinanzas.customFormat("##0.00000",dif_real, 5) );
                }
                bean.setValor_22( observacion );
                
                /*
                String difdiasdem = duracionDemora ( planilla, fechareporte ) != null?duracionDemora ( planilla, fechareporte ):"";
                bean.setValor_21( difdiasdem );// diferencia en dias de la demora
                */
                this.vector.add( bean );
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            
        }
        
        finally{
            
            if ( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    e.printStackTrace();
                    throw new SQLException( "ERROR DURANTE 'reporteEspecifico()' - [ReporteDemorasDAO].. " + e.getMessage() + " " + e.getErrorCode() );
                    
                }
                
            }
            
            this.desconectar( "SQL_REPORTE_DEMORAS_ESPECIFICO" );
            
        }
        
    }
    
    /** 
     * Funcion publica que obtiene la información que se va a generar
     * en el reporte de todos los proveedores. 
     */
    public void reporteTodos ( String fecini, String fecfin ) throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_REPORTE_DEMORAS_TODOS" );
            
            st.setString( 1, fecini );
            st.setString( 2, fecfin );
            
            rs = st.executeQuery();
            
            this.vector = new Vector();
            
            while( rs.next() ){
                
                String planilla = rs.getString( "planilla" )!=null?rs.getString( "planilla" ).toUpperCase():"";           
                String nomagencia = rs.getString( "nomagencia" )!=null?rs.getString( "nomagencia" ).toUpperCase():"";
                String nomciuorip = rs.getString( "nomciuorip" )!=null?rs.getString( "nomciuorip" ).toUpperCase():"";
                String nomciudesp = rs.getString( "nomciudesp" )!=null?rs.getString( "nomciudesp" ).toUpperCase():"";
                String numequi = rs.getString( "numequi" )!=null?rs.getString( "numequi" ).toUpperCase():"";
                String nomconduc = rs.getString( "nomconduc" )!=null?rs.getString( "nomconduc" ).toUpperCase():"";
                String fecdes = rs.getString( "fecdes" )!=null?rs.getString( "fecdes" ).toUpperCase():"";                
                String numwork = rs.getString( "numwork" )!=null?rs.getString( "numwork" ).toUpperCase():"";
                String nomciuorir = rs.getString( "nomciuorir" )!=null?rs.getString( "nomciuorir" ).toUpperCase():"";
                String nomciudesr = rs.getString( "nomciudesr" )!=null?rs.getString( "nomciudesr" ).toUpperCase():"";
                String estandar = rs.getString( "estandar" )!=null?rs.getString( "estandar" ).toUpperCase():"";
                String nomcliente = rs.getString( "nomcliente" )!=null?rs.getString( "nomcliente" ).toUpperCase():"";
                String nomdestin = rs.getString( "nomdestin" )!=null?rs.getString( "nomdestin" ).toUpperCase():"";
                String fechareporte = rs.getString( "fechareporte" )!=null?rs.getString( "fechareporte" ).toUpperCase():""; 
                String nomsupe = rs.getString( "nomsupe" )!=null?rs.getString( "nomsupe" ).toUpperCase():""; 
                String nomdemo = rs.getString( "nomdemo" )!=null?rs.getString( "nomdemo" ).toUpperCase():""; 
                
                String factura = rs.getString( "factura" )!=null?rs.getString( "factura" ).toUpperCase():""; 
                String documento = rs.getString( "documento" )!=null?rs.getString( "documento" ).toUpperCase():""; 
                String comercial = factura.equals( "" )?documento:factura;
                String observacion = rs.getString( "observacion" )!=null? rs.getString( "observacion" ) : ""; //Osvaldo
                
                bean = new BeanGeneral ();
                
                bean.setValor_01( planilla );
                bean.setValor_02( fechareporte );// fecha inicio demora
                String fecha = fechaProxRepMovTra ( planilla, fechareporte ) != null?fechaProxRepMovTra ( planilla, fechareporte ):"";
                bean.setValor_03( fecha );// fecha fin demora
                bean.setValor_04( nomagencia );
                bean.setValor_05( nomciuorip );
                bean.setValor_06( nomciudesp );
                bean.setValor_07( numequi );
                bean.setValor_08( nomconduc );
                bean.setValor_09( fecdes );
                bean.setValor_10( numwork );
                bean.setValor_11( nomciuorir );
                bean.setValor_12( nomciudesr );
                bean.setValor_13( estandar );
                bean.setValor_14( nomcliente );
                bean.setValor_15( nomdestin );
                
                bean.setValor_16( fecini );// fecini
                bean.setValor_17( fecfin );// fecfin
                     
                bean.setValor_18( nomsupe );// usuario creo
                bean.setValor_19( nomdemo );// descripcion de la demora
                bean.setValor_20( comercial );// numero de la factura
                                
                                
                if ( fecha.equals("") ) {
                    bean.setValor_21( "" );
                } else {
                    
                    
                    double dif_real = ((double)( fmt.parse( fecha ).getTime() - fmt.parse( fechareporte ).getTime())/(double)(1000*60*60*24));
                    bean.setValor_21( "" + UtilFinanzas.customFormat("##0.00000",dif_real,5) );
                }
                bean.setValor_22( observacion );
                
                /*
                String difdiasdem = duracionDemora ( planilla, fechareporte ) != null?duracionDemora ( planilla, fechareporte ):"";
                bean.setValor_21( difdiasdem );// diferencia en dias de la demora
                */
                this.vector.add( bean );
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            
        }
        
        finally{
            
            if ( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    e.printStackTrace();
                    throw new SQLException( "ERROR DURANTE 'reporteTodos()' - [ReporteDemorasDAO].. " + e.getMessage() + " " + e.getErrorCode() );
                    
                }
                
            }
            
            this.desconectar( "SQL_REPORTE_DEMORAS_TODOS" );
            
        }
        
    }
    
    /** 
     * Funcion publica que obtiene la lista de los codigos y nombres de clientes
     */
    public void clientes ( String nom_cli ) throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_CLIENTE" );
            
            st.setString( 1, nom_cli + "%" );
            
            rs = st.executeQuery();
            
            this.vector_clientes = new Vector();
            
            while( rs.next() ){
                
                Vector info = new Vector();
                
                String codcli = rs.getString( "codcli" )!=null?rs.getString( "codcli" ).toUpperCase():"";
                String nomcli = rs.getString( "nomcli" )!=null?rs.getString( "nomcli" ).toUpperCase():"";
                
                info.add( codcli );
                info.add( nomcli );
                                                
                this.vector_clientes.add( info );
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'clientes()' - [ReporteDemorasDAO].. " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_CLIENTE" );
            
        }
        
    }
    
    /** 
     * Funcion publica que obtiene la fecha del proximo
     * reporte de movimiento de trafico.
     */
    public String fechaProxRepMovTra ( String planilla, String fecha ) throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        String fechareporte = "";
        
        try {
            
            st = crearPreparedStatement( "SQL_FECHA_PROX_REPORTE" );
            
            st.setString( 1, planilla );
            st.setString( 2, fecha );
            
            rs = st.executeQuery();
                        
            if ( rs.next() ){
                
                fechareporte = rs.getString( "fechareporte" )!=null?rs.getString( "fechareporte" ).toUpperCase():"";
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'fechaProxRepMovTra()' - [ReporteDemorasDAO].. " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_FECHA_PROX_REPORTE" );
            
        }
        
        return fechareporte;
        
    }
    
    /** 
     * Funcion publica que obtiene la fecha del proximo
     * reporte de movimiento de trafico.
     */
    /*
    public String duracionDemora ( String planilla, String fecha ) throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        String duracion = "";
        
        try {
            
            st = crearPreparedStatement( "SQL_DURACION_DEMORA" );
            
            st.setString( 1, fecha );
            st.setString( 2, planilla );
            st.setString( 3, fecha );
                        
            rs = st.executeQuery();
                        
            if ( rs.next() ){
                
                duracion = rs.getString( "duracion" )!=null?rs.getString( "duracion" ).toUpperCase():"";
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'duracionDemora()' - [ReporteDemorasDAO].. " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_DURACION_DEMORA" );
            
        }
        
        return duracion;
        
    }
    */
       
}