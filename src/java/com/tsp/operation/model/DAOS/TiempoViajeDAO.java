/*Created on 28 de junio de 2005, 02:56 PM*/

package com.tsp.operation.model.DAOS;

/** @author  fvillacob */


import java.sql.*;
import java.text.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Utility;
import com.tsp.util.connectionpool.*;




public class TiempoViajeDAO extends MainDAO{
    
    
   /* ______________________________________________________________________
                             ATRIBUTOS
      ______________________________________________________________________*/
    
    
    private static String ULTIMO_REPORTE = " ";
    
    
   /* ______________________________________________________________________
                             METODOS
      ______________________________________________________________________*/
    
    
    public TiempoViajeDAO() {
        super("TiempoViajeDAO.xml");
    }
    
    
    public Vector searchOTs(String tipo,  String fechai,String fechaf,String cliente)throws Exception{
        Connection con = null;
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        Vector              lista = null;
        String            sql   = "";
        String query = "QUERY_OTS";
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
            tipo         = (tipo.equals("ALL")   )?" like '%' " : (  (tipo.equals("NA"))?" ='NA'":" <>'NA'" );
            String QUERY_OTS = this.obtenerSQL("QUERY_OTS");
            sql = QUERY_OTS.replaceAll("#FECHA#",fechai).replaceAll("#FECHAF#",fechaf).replaceAll("#TIPO#",tipo).replaceAll("#CLIENTE#",cliente);
            
            st= con.prepareStatement(sql);
            ////System.out.println("Query Timepos de viajes: "+st);
            rs= st.executeQuery();
            if(rs!=null){
                lista= new Vector();
                int cont = 1;
                while(rs.next()){
                    OTs ot         = new OTs();
                    ULTIMO_REPORTE = " ";
                    ot.setOT            ( rs.getString(1));
                    ot.setDesc          ( rs.getString(2));
                    ot.setDespacho      ( rs.getString(3));
                    ot.setAgencia       ( rs.getString(4));
                    ot.setClienteId     ( rs.getString(5));
                    ot.setTipoViaje     ( rs.getString(6));
                    ot.setValor         ( rs.getDouble(7));
                    ot.setPadre         ( rs.getString(8));
                    ot.setSJ            ( rs.getString(9));
                    ot.setOrigen        ( rs.getString(10));
                    ot.setDestino       ( rs.getString(11));
                    ot.diferenciaFecha  ( rs.getString(12));
                    ot.setUltimoReporte( rs.getString(13));
                    ot.setFactComercial( rs.getString(14));
                    ot.setFechaentrega(rs.getString(15));
                    ot.setID(cont);
                    lista.add(ot);
                    cont++;
                    
                }
            }
        }}catch(Exception e){
            throw new SQLException("No se pudo buscar listado de OTs : "+ tipo+fechai+ sql + " ->" +  e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    
/**
 * 
 * @param agencia
 * @return
 * @throws Exception
 */
    public Vector searchClientes(String agencia)throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        Vector lista           = null;
        String query = "QUERY_CLIENTES_AGENCIA";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,agencia);
            rs=st.executeQuery();
            if(rs!=null){
                lista = new Vector();
                while(rs.next()){
                    
                    Cliente c = new Cliente();
                    c.setCodcli(rs.getString("codcli"));
                    c.setNomcli(rs.getString("nomcli"));
                    lista.add(c);
                }
            }
        }}catch(Exception e){
            throw new SQLException("No se pudo buscar listado de Clientes : "+  e.getMessage());
        }
        finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
}
