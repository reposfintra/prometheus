/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * GestionCondicionesDAO.java :
 * Copyright 2010 Rhonalf Martinez Villa <rhonaldomaster@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.tsp.operation.model.DAOS;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.TransaccionService;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;

/**
 *
 * @author rhonalf
 */
public class GestionCondicionesDAO extends MainDAO {

    public GestionCondicionesDAO(){
        super("GestionCondicionesDAO.xml");
    }
    public GestionCondicionesDAO(String dataBaseName){
        super("GestionCondicionesDAO.xml", dataBaseName);
    }

    /**
     * Busca los tipos de titulo valor
     * @return Listado con los resultados encontrados
     * @throws Exception Cuando hay error
     */
    public ArrayList<String> titulosValor() throws Exception{
        ArrayList<String> lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "LIST_TITV";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<String>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while(rs.next()){
                lista.add(rs.getString("dato")+";_;"+rs.getString("referencia"));
            }
        }
        catch (Exception e) {
            throw new Exception("Error en titulosValor en GestionCondicionesDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    /**
     * Busca los tipos de plazo de cuota
     * @return Listado con los resultados encontrados
     * @throws Exception Cuando hay error
     */
    public ArrayList<String> plazoCuota() throws Exception{
        ArrayList<String> lista = null;
        String plazo = "";
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "PLAZO_CUOT";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<String>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while(rs.next()){
               plazo = rs.getString("dato")+";_;"+rs.getString("referencia");
               lista.add(plazo);
            }
        }
        catch (Exception e) {
            throw new Exception("Error en plazoCuota en GestionCondicionesDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

     /**
     * Busca datos de convenios del afiliado
     * @param nitprov Nit del afiliado
     * @return Listado con los datos encontrados
     * @throws Exception Cuando hay error
     */
    public ArrayList<BeanGeneral> conveniosProv(String nitprov) throws Exception{
        ArrayList<BeanGeneral> lista = null;
        BeanGeneral bean = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONV_PROV";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<BeanGeneral>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, nitprov);
            rs = st.executeQuery();
            while(rs.next()){
                bean = new BeanGeneral();
                bean.setValor_01(rs.getString("id_prov_convenio"));
                bean.setValor_02(rs.getString("id_convenio"));
                bean.setValor_03(rs.getString("cod_sector"));
                bean.setValor_04(rs.getString("cod_subsector"));
                //darrieta 08/09/2010
                bean.setValor_05(rs.getString("nom_convenio"));
                bean.setValor_06(rs.getString("nom_sector"));
                bean.setValor_07(rs.getString("nom_subsector"));
                bean.setValor_08(rs.getString("factura_tercero"));
                bean.setValor_09(rs.getString("nit_tercero"));
                bean.setValor_10(rs.getString("redescuento"));
                lista.add(bean);
            }
        }
        catch (Exception e) {
            throw new Exception("Error en conveniosProv en GestionCondicionesDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    /**
     * Busca el nombre de un sector
     * @param id_sector El codigo del sector
     * @return Cadena con el nombre
     * @throws Exception Cuando hay error
     */
    public String nomSector(String id_sector) throws Exception{
        String nombre = "";
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "NOM_SECT";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, id_sector);
            rs = st.executeQuery();
            if(rs.next()){
                nombre = rs.getString("nombre");
            }
        }
        catch (Exception e) {
            throw new Exception("Error en nomSector en GestionCondicionesDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return nombre;
    }

    /**
     * Busca el nombre de un subsector
     * @param id_sector El codigo del subsector
     * @return Cadena con el nombre
     * @throws Exception Cuando hay error
     */
    public String nomSubSector(String id_sector,String subsector ) throws Exception{
        String nombre = "";
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "NOM_SUBSECT";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, id_sector);
            st.setString(2, subsector);
            rs = st.executeQuery();
            if(rs.next()){
                nombre = rs.getString("nombre");
            }
        }
        catch (Exception e) {
            throw new Exception("Error en nomSector en GestionCondicionesDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return nombre;
    }

    /**
     * Busca el nombre de un tipo de titulo valor
     * @param id_titulo Codigo del titulo
     * @return Cadena con el nombre
     * @throws Exception Cuando hay error
     */
    public String nomTipoTitulo(String id_titulo) throws Exception{
        String nombre = "";
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "NOM_TIT";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, id_titulo);
            rs = st.executeQuery();
            if(rs.next()){
                nombre = rs.getString("descripcion");
            }
        }
        catch (Exception e) {
            throw new Exception("Error en nomTipoTitulo en GestionCondicionesDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return nombre;
    }

    /**
     * Busca los rangos asociados con las condiciones
     * @param conv codigo del convenio
     * @param tipotit tipo de titulo valor
     * @param plazoprimer plazo del primer titulo
     * @param prop si es propietario o no
     * @return Lista con los datos encontrados
     * @throws Exception Cuando hay error
     */
   public ArrayList<BeanGeneral> rangosConvenio(String conv,String tipotit,String plazoprimer,boolean prop) throws Exception{
        ArrayList<BeanGeneral> lista = null;
        BeanGeneral bean = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "BUSCAR_RANGOS_AVAL";
        try {
            lista = new ArrayList<BeanGeneral>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setInt(1, Integer.parseInt(conv));
            st.setString(2, tipotit);
            st.setInt(3, Integer.parseInt(plazoprimer));
            st.setBoolean(4, prop);
            rs = st.executeQuery();
            while(rs.next()){
                bean = new BeanGeneral();
                bean.setValor_01(rs.getString("dias_ini"));
                bean.setValor_02(rs.getString("dias_fin"));
                bean.setValor_03(rs.getString("porcentaje_aval"));
                bean.setValor_04(rs.getString("id_aval"));
                lista.add(bean);
            }
        }
        catch (Exception e) {
            throw new Exception("Error en rangosConvenio en GestionCondicionesDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    /**
     * Actualiza los rangos de una condicion
     * @param listarangos Daros a actualizar
     * @throws Exception Cuando hay error
     */
    public void modificarRangos(ArrayList<String> listarangos,String codconv,String user) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        String query = "RAN_DEL";
        String sql="";
        String sq2l="";
        String[] arreglo = null;
        ResultSet rs = null;
        int k = 0;
        try {
            TransaccionService scv = new TransaccionService(this.getDatabaseName());
            scv.crearStatement();
            if(!codconv.equals("")){
                sql = this.obtenerSQL(query);
                sql = sql.replaceAll("#aval", codconv);
                scv.getSt().addBatch(sql);
            }
            else{
                con = this.conectarJNDI(query);
                st = con.prepareStatement("SELECT nextval('condiciones_aval_id_aval_seq');");
                rs = st.executeQuery();
                if(rs.next()) codconv = rs.getString(1);
                con.close();
            }
            query = "RAN_INS";
            sq2l = this.obtenerSQL(query);
            StringStatement sst = null;
            for (int i = 0; i < listarangos.size(); i++) {
                sst = new StringStatement(sq2l,true);
                arreglo = (listarangos.get(i)).split(";_;");
                sst.setString(1, codconv);
                sst.setString(2, arreglo[0]);
                sst.setString(3, arreglo[1]);
                sst.setString(4, arreglo[2]);
                sst.setString(5, user);
                scv.getSt().addBatch(sst.getSql());
            }
            scv.execute();
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("No se modificaron registros en modificarRangos"); 
            throw new Exception("Error en modificarRangos en GestionCondicionesDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            //if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
    }

    /**
     * Inserta los rangos de una condicion
     * @param listarangos Datos a actualizar
     * @throws Exception Cuando hay error
     */
    public void insertarRangos(ArrayList<String> listarangos) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        String query = "RAN_INS";
        String sql="";
        String cadenatotal = "";
        String[] arreglo = null;
        int k = 0;
        try {
            sql = this.obtenerSQL(query);
            StringStatement sst = null;
            for (int i = 0; i < listarangos.size(); i++) {
                sst = new StringStatement(sql,true);
                arreglo = (listarangos.get(i)).split(";_;");
                sst.setString(1, arreglo[0]);
                sst.setString(2, arreglo[1]);
                sst.setString(3, arreglo[2]);
                cadenatotal += sst.getSql();
            }
            con = this.conectarJNDI(query);
            st = con.prepareStatement(cadenatotal);
            k = st.executeUpdate();
            if(k==0){ System.out.println("No se modificaron registros en insertarRangos"); }
        }
        catch (Exception e) {
            throw new Exception("Error en insertarRangos en GestionCondicionesDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
    }

    /**
     * Busca proveedores dependiendo de un filtro
     * @param dato El dato a buscar
     * @param filtro Filtro a aplicar
     * @return Listado de datos que coinciden con el dato y el filtro
     * @throws Exception Cuando hay error
     */
    public ArrayList<Proveedor> buscarProv(String dato,String filtro) throws Exception{
        ArrayList<Proveedor> lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "BUSC_AFIL";
        String sql="";
        Proveedor prov = null;
        try {
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#filtro", filtro);
            sql = sql.replaceAll("#param", dato.toUpperCase());
            lista = new ArrayList<Proveedor>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while(rs.next()){
                prov = new Proveedor();
                prov.setC_nit(rs.getString("nit"));
                prov.setC_payment_name(rs.getString("payment_name"));
                lista.add(prov);
                prov = null;
            }
        }
        catch (Exception e) {
            throw new Exception("Error en buscarProv en GestionCondicionesDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    /**
     * Busca los el codigo de las condiciones
     * @param conv codigo del convenio
     * @param tipotit tipo de titulo valor
     * @param plazoprimer plazo del primer titulo
     * @param prop si es propietario o no
     * @return Cadena con el dato encontrado
     * @throws Exception Cuando hay error
     */
    public String codigoCond(String conv,String tipotit,String plazoprimer,boolean prop) throws Exception{
        String cod = "";
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "COD_CONV";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, conv);
            st.setString(2, tipotit);
            st.setString(3, plazoprimer);
            st.setBoolean(4, prop);
            rs = st.executeQuery();
            if(rs.next()){
                cod = rs.getString("id_aval");
            }
        }
        catch (Exception e) {
            throw new Exception("Error en codigoCond en GestionCondicionesDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return cod;
    }

    /**
     * Busca el codigo de la relacion convenio-sector-subsector
     * @param id_convenio Codigo del convenio
     * @param cod_sector Codigo del sector
     * @param cod_subsector Codigo del subsector
     * @return Cadena con el codigo obtenido
     * @throws Exception Cuando hay error
     */
    public String codigoCv(String id_convenio,String cod_sector,String cod_subsector) throws Exception{
        String cod = "";
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "COD_PROV_CONV";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, id_convenio);
            st.setString(2, cod_sector);
            st.setString(3, cod_subsector);
            rs = st.executeQuery();
            if(rs.next()){
                cod = rs.getString("id_prov_convenio");
            }
        }
        catch (Exception e) {
            throw new Exception("Error en codigoCv en GestionCondicionesDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return cod;
    }

    public void insertHeaderConv(String codcv,String tipotit,String plazoprimer,boolean prop,String user) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        String query = "INS_COND_P";
        String sql="";
        int k = 0;
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, codcv);
            st.setString(2, tipotit);
            st.setString(3, plazoprimer);
            st.setBoolean(4, prop);
            st.setString(5, user);
            System.out.println("sql: "+st.toString());
            k = st.executeUpdate();
        }
        catch (Exception e) {
            throw new Exception("Error en insertHeaderConv en GestionCondicionesDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
    }

    /**
     * Busca la informacion de los convenios, sector y subsector para un afiliado y un usuario
     * @param nitprov nit del proveedor
     * @param idUsuario id del usuario
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<BeanGeneral> obtenerConveniosProvUsuario(String nitprov, String idUsuario) throws Exception{
        ArrayList<BeanGeneral> lista = null;
        BeanGeneral bean = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONVENIOS_PROVEEDOR_USUARIO";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<BeanGeneral>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, nitprov);
            st.setString(2, idUsuario);
            rs = st.executeQuery();
            while(rs.next()){
                bean = new BeanGeneral();
                bean.setValor_01(rs.getString("id_prov_convenio"));
                bean.setValor_02(rs.getString("id_convenio"));
                bean.setValor_03(rs.getString("cod_sector"));
                bean.setValor_04(rs.getString("cod_subsector"));
                bean.setValor_05(rs.getString("nom_convenio"));
                bean.setValor_06(rs.getString("nom_sector"));
                bean.setValor_07(rs.getString("nom_subsector"));
                bean.setValor_08(rs.getString("factura_tercero"));
                bean.setValor_09(rs.getString("nit_tercero"));
                bean.setValor_10(rs.getString("redescuento"));
                lista.add(bean);
            }
        }
        catch (Exception e) {
            throw new Exception("Error en obtenerConveniosProvUsuario en GestionCondicionesDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

        public ArrayList<String> titulosValorConvenio(String idConvenio) throws Exception{
        ArrayList<String> lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "TITULOSVALOR_CONVENIO";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<String>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, idConvenio);
            rs = st.executeQuery();
            while(rs.next()){
                lista.add(rs.getString("titulo_valor")+";_;"+rs.getString("referencia")+";_;"+rs.getString("genera_remesa"));
            }
        }
        catch (Exception e) {
            throw new Exception("Error en titulosValorConvenio en GestionCondicionesDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    public ArrayList<Proveedor> buscarAfiliadosUsuario(String idUsuario) throws Exception{
        ArrayList<Proveedor> lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "BUSCAR_AFILIADOS_USUARIO";
        Proveedor prov = null;
        try {

            lista = new ArrayList<Proveedor>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, idUsuario);
            rs = st.executeQuery();
            while(rs.next()){
                prov = new Proveedor();
                prov.setC_nit(rs.getString("nit_proveedor"));
                prov.setC_payment_name(rs.getString("payment_name"));
                lista.add(prov);
                prov = null;
            }
        } catch (Exception e) {
            throw new Exception("Error en buscarAfiliadosUsuario en GestionCondicionesDAO.java: "+e.toString());
        } finally {
            if(rs!=null){ rs.close();}
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    public ArrayList<BeanGeneral> obtenerBancos() throws Exception{
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
        BeanGeneral bean = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "BUSCAR_BANCOS";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            rs = st.executeQuery();
            while(rs.next()){
                bean = new BeanGeneral();
                bean.setValor_01(rs.getString("codigo"));
                bean.setValor_02(rs.getString("nombre"));
                lista.add(bean);
            }
        }
        catch (Exception e) {
            throw new Exception("Error en obtenerBancos en GestionCondicionesDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(con!=null && !con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    public double buscarPorcentajeAval(int idProvConvenio, String tipoTitulo, int plazo, boolean propietario, int numCuotas) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        double aval = 0;
        String query = "BUSCAR_PORCENTAJE_AVAL";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setInt(1,idProvConvenio);
            st.setString(2,tipoTitulo);
            st.setInt(3,plazo);
            st.setBoolean(4,propietario);
            st.setInt(5,numCuotas);
            st.setInt(6,numCuotas);
            rs = st.executeQuery();
            while(rs.next()){
                aval = rs.getDouble("porcentaje_aval");
            }
        }
        catch (Exception e) {
            throw new Exception("Error en buscarPorcentajeAval en GestionCondicionesDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return aval;
    }
    
    public ArrayList buscarRangosAval(int idProvConvenio, String tipoTitulo, int plazo, boolean propietario) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        ArrayList avales = new ArrayList();
        String query = "BUSCAR_RANGOS_AVAL";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setInt(1,idProvConvenio);
            st.setString(2,tipoTitulo);
            st.setInt(3,plazo);
            st.setBoolean(4,propietario);
            rs = st.executeQuery();
            while(rs.next()){
                String porc = rs.getString("porcentaje_aval");
                int ini = (int)Math.ceil(rs.getDouble("dias_ini")/30);
                int fin =(int)Math.floor(rs.getDouble("dias_fin")/30);
                for (int i= ini; i <= fin; i++) {
                    avales.add(porc);
                }
            }
         
             avales.add(avales.get(avales.size()-1)); //Repite el ultimo   
         
        }
        catch (Exception e) {
            throw new Exception("Error en buscarRangosAval en GestionCondicionesDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return avales;
    }
    
    
    
    
    
    
    
    
    
    
    /**
     * Busca la informacion de los convenios, sector y subsector para un afiliado y un usuario
     * @param nitprov nit del proveedor
     * @param idUsuario id del usuario
     * @param idconvenio id del convenio
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<BeanGeneral> obtenerSectoresProvUsuario(String nitprov, String idUsuario, String idconvenio ) throws Exception{
        ArrayList<BeanGeneral> lista = null;
        BeanGeneral bean = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SECTORES_PROVEEDOR_USUARIO";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<BeanGeneral>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, nitprov);
            st.setString(2, idUsuario);
            st.setString(3, idconvenio);
            rs = st.executeQuery();
            while(rs.next()){
                bean = new BeanGeneral();
                bean.setValor_01(rs.getString("id_prov_convenio"));
                bean.setValor_02(rs.getString("cod_sector"));
                bean.setValor_03(rs.getString("cod_subsector"));
                bean.setValor_04(rs.getString("nom_sector"));
                bean.setValor_05(rs.getString("nom_subsector"));
                lista.add(bean);
            }
        }
        catch (Exception e) {
            throw new Exception("Error en obtenerConveniosProvUsuario en GestionCondicionesDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    public JsonObject getInfoPlanDePagos(int numero_solicitud) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        JsonObject jsonObject = null;
        String query = "GET_INFO_PLAN_DE_PAGOS";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setInt(1,numero_solicitud);           
            rs = st.executeQuery();
      
        if (con != null) { 
            jsonObject = new JsonObject();
            if(rs.next()) {                
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    
                    //System.out.println("rs.getMetaData().getColumnLabel(i): "+rs.getMetaData().getColumnLabel(i)+" "+"rs.getMetaData().getColumnTypeName(i): "+rs.getMetaData().getColumnTypeName(i));
                    switch(rs.getMetaData().getColumnTypeName(i)){
                        case "numeric":
                            jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getDouble(rs.getMetaData().getColumnLabel(i)));
                            break;
                        case "int4":
                            jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getInt(rs.getMetaData().getColumnLabel(i)));
                            break;    
                        default:
                            jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                    
                    
                }
            }
        } 
        
        return jsonObject;
            
        }
        catch (Exception e) {
            throw new Exception("Error en getInfoPlanDePagos: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
    
    }

    public JsonArray getDetallePlanDePagos(int numero_solicitud) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        JsonArray lista = null;
        JsonObject jsonObject = null;
        String query = "GET_DETALLE_PLAN_DE_PAGOS";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setInt(1,numero_solicitud);
            rs = st.executeQuery();
      
        if (con != null) { 
            lista = new JsonArray();
            while (rs.next()) {
                jsonObject = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                     switch(rs.getMetaData().getColumnTypeName(i)){
                        case "numeric":
                            jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getDouble(rs.getMetaData().getColumnLabel(i)));
                            break;
                        case "int4":
                            jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getInt(rs.getMetaData().getColumnLabel(i)));
                            break;    
                        case "bool":
                            jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getBoolean(rs.getMetaData().getColumnLabel(i)));
                            break;    
                        default:
                            jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                   
                }
                lista.add(jsonObject);
            }
        } 
        return lista;
        }
        catch (Exception e) {
            throw new Exception("Error en getDetallePlanDePagos: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
    }

    public JsonObject getTotalDetallePlanDePagos(int numero_solicitud) throws Exception {
       PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        JsonObject jsonObject = null;
        String query = "GET_TOTAL_DETALLE_PLAN_DE_PAGOS";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setInt(1,numero_solicitud);           
            rs = st.executeQuery();
      
        if (con != null) { 
            jsonObject = new JsonObject();
            if(rs.next()) {                
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    
                    //System.out.println("rs.getMetaData().getColumnLabel(i): "+rs.getMetaData().getColumnLabel(i)+" "+"rs.getMetaData().getColumnTypeName(i): "+rs.getMetaData().getColumnTypeName(i));
                    switch(rs.getMetaData().getColumnTypeName(i)){
                        case "numeric":
                            jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getDouble(rs.getMetaData().getColumnLabel(i)));
                            break;
                        case "int4":
                            jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getInt(rs.getMetaData().getColumnLabel(i)));
                            break;    
                        default:
                            jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                    
                    
                }
            }
        } 
        
        return jsonObject;
            
        }
        catch (Exception e) {
            throw new Exception("Error en getInfoPlanDePagos: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
    }
    

}