/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.extractoDuplicadoBeans;
import java.util.ArrayList;

/**
 *
 * @author mariana
 */
public interface extractoDuplicadoDAO {

    /**
     * metodo que buscar la informacion del negocio
     * @param cedula
     * @param query
     * @param user
     * @return
     */
    public ArrayList<extractoDuplicadoBeans> buscarNegociosCliente(String cedula, String query, Usuario user);

    /**
     *
     * @param negocio
     * @param user
     * @return
     */
    public ArrayList<extractoDuplicadoBeans> detalleNegocios(String negocio, Usuario user);
    
    /**
     *metodo para guardar la informacion que se incluira en el extracto
     * @param lista
     * @param duplicadoBeans
     * @param user
     * @return
     */
    public String guardardocumentosExt(JsonArray lista ,extractoDuplicadoBeans duplicadoBeans,Usuario user );
    
    /**
     * Metodo que busca las unidades de negocio para crear el extracto
     * @param user
     * @return
     */
    public String getLineaNegocio(Usuario user);

}
