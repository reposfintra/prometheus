/******************************************************************
* Nombre                        ReporteDevolucionDAO.java
* Descripci�n                   Clase DAO para el reporte de devolucion
* Autor                         ricardo rosero
* Fecha                         19/01/2006
* Versi�n                       1.0
* Coyright                      Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.DAOS;
import java.util.*;
import java.text.*;
import java.sql.*;
import java.util.Arrays;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;
/**
 *
 * @author  rrosero
 */
public class ReporteDevolucionDAO extends MainDAO{

 public    ReporteDevolucionDAO(){
     super("ReporteDevolucionDAO.xml");
 }

    //atributos
    //vectores
    private Vector vecReporteDev;
    //conexion bd
    private Connection con;
    //private PoolManager pm;
    //bean
    private ReporteDevolucion rd;
    //querys
    private static String SQL1 = "SELECT " +
    "e.numrem, get_nombreciudad(rem.orirem) as orirem, get_nombreciudad(rem.desrem) as desrem, " +
    "cli.nomcli AS cliente," +
    "e.numpla,coalesce(e.fec_devolucion,'0099-01-01'::timestamp) as fec_devolucion," +
    "coalesce(e.documento,'') as documento, coalesce(e.tipo_doc, '') as tipo_doc," +
    "coalesce(e.ubicacion,'') as ubicacion,coalesce(e.codprod,'') as codprod, coalesce(e.producto,'') as producto, " +
    "coalesce(e.tipoemp, '') as tipoemp, coalesce(e.cantdev,0) as cantdev, coalesce(e.nombredev,'') as nombredev, " +
    "coalesce(e.motivodev,'') as motivodev, coalesce(e.respondev,'') as respondev, coalesce(e.notasxom,'') as notasxom, " +
    "coalesce(e.fechdevbod,'0099-01-01'::timestamp) as fechdevbod," +
    "coalesce(e.fecrechazo,'0099-01-01'::timestamp) as fecrechazo," +
    "coalesce(e.numrechazo,'') as numrechazo, coalesce(e.nombrerep,'') as nombrerep," +
    "e.fechareporte, e.plaveh, cum.creation_date AS fec_cumplido " +
    "FROM " +
    "(SELECT " +
    "pr.numrem, d.numpla,d.fec_devolucion, d.documento, d.tipo_doc, d.ubicacion," +
    "d.codprod, d.producto, d.tipoemp, d.cantdev, d.nombredev, d.motivodev," +
    "d.respondev, d.notasxom, d.fechdevbod, d.fecrechazo, d.numrechazo, d.nombrerep," +
    "d.fechareporte, d.plaveh " +
    "FROM " +
    "(SELECT " +
    "c.numpla, c.numrem, c.fec_devolucion, c.documento, c.tipo_doc, c.ubicacion," +
    "dev.codprod, dev.producto, dev.tipoemp, dev.cantdev, dev.nombredev, dev.motivodev," +
    "dev.respondev, dev.notasxom, dev.fechdevbod, dev.fecrechazo, dev.numrechazo, dev.nombrerep," +
    "c.fechareporte, c.plaveh " +
    "FROM " +
    "(SELECT " +
    "a.numpla, dis.numrem, dis.fec_devolucion, dis.documento, dis.tipo_doc, " +
    "dis.ubicacion, a.fechareporte, a.plaveh " +
    "FROM " +
    "(SELECT     pla.plaveh, pl.numpla, pl.fechareporte " +
    "FROM " +
    "(SELECT numpla, fechareporte " +
    "FROM " +
    "rep_mov_trafico " +
    "WHERE " +
    "( fechareporte BETWEEN ? AND ? )" +
    ") pl LEFT JOIN planilla pla " +
    "ON pla.numpla = pl.numpla ) a LEFT JOIN discrepancia dis " +
    "ON a.numpla = dis.numpla ) c LEFT JOIN devolucion dev " +
    "ON dev.numpla = c.numpla )d LEFT JOIN plarem pr " +
    "ON pr.numpla = d.numpla )e, remesa rem, cumplido cum, cliente cli " +
    "WHERE e.numrem = rem.numrem AND rem.starem = 'S' " +
    "AND cum.tipo_doc = '002' AND rem.cliente = cli.codcli";
    
    private static String SQL2 = "SELECT " +
    "COUNT(*) AS registros " +
    "FROM " +
    "(SELECT " +
    "pr.numrem, d.numpla,d.fec_devolucion, d.documento, d.tipo_doc, d.ubicacion," +
    "d.codprod, d.producto, d.tipoemp, d.cantdev, d.nombredev, d.motivodev," +
    "d.respondev, d.notasxom, d.fechdevbod, d.fecrechazo, d.numrechazo, d.nombrerep," +
    "d.fechareporte, d.plaveh " +
    "FROM " +
    "(SELECT " +
    "c.numpla, c.numrem, c.fec_devolucion, c.documento, c.tipo_doc, c.ubicacion," +
    "dev.codprod, dev.producto, dev.tipoemp, dev.cantdev, dev.nombredev, dev.motivodev," +
    "dev.respondev, dev.notasxom, dev.fechdevbod, dev.fecrechazo, dev.numrechazo, dev.nombrerep," +
    "c.fechareporte, c.plaveh " +
    "FROM " +
    "(SELECT " +
    "a.numpla, dis.numrem, dis.fec_devolucion, dis.documento, dis.tipo_doc, " +
    "dis.ubicacion, a.fechareporte, a.plaveh " +
    "FROM " +
    "(SELECT     pla.plaveh, pl.numpla, pl.fechareporte " +
    "FROM " +
    "(SELECT numpla, fechareporte " +
    "FROM " +
    "rep_mov_trafico " +
    "WHERE " +
    "( fechareporte BETWEEN ? AND ? )" +
    ") pl LEFT JOIN planilla pla " +
    "ON pla.numpla = pl.numpla ) a LEFT JOIN discrepancia dis " +
    "ON a.numpla = dis.numpla ) c LEFT JOIN devolucion dev " +
    "ON dev.numpla = c.numpla )d LEFT JOIN plarem pr " +
    "ON pr.numpla = d.numpla )e, remesa rem, cumplido cum, cliente cli " +
    "WHERE e.numrem = rem.numrem AND rem.starem = 'S' " +
    "AND cum.tipo_doc = '002' AND rem.cliente = cli.codcli";
    
    /**
     * Genera el reporte de devoluciones
     * @autor                       ricardo rosero
     * @throws                      SQLException
     * @version                     1.0
     */
    public Vector obtenerInforme(String fechai, String fechaf) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Vector informe = new Vector();
        String query = "SQL_CONSULTA1";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, fechai);
                st.setString(2, fechaf);
                rs = st.executeQuery();
                while(rs.next()){//////System.out.println("............... rs es null?" + rs==null + " : ");
                    rd = new ReporteDevolucion();
                    rd = rd.load(rs);
                    informe.add(rd);
                }
            }
        }catch(SQLException e){//e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA GENERACION DEL REPORTE\n " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }        
        return informe;
    }
    
    /**
     * Obtiene el numero de registros de la consulta del reporte
     * @autor                       ricardo rosero
     * @throws                      SQLException
     * @version                     1.0
     */
    public int obtenerRegistros(String fechai, String fechaf) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        int total = 0;
        String query = "SQL_CONSULTA2";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, fechai);
                st.setString(2, fechaf);
                ////System.out.println("consulta: " + st.toString() );
                rs = st.executeQuery();
                while(rs.next()){
                    total = rs.getInt("registros");
                }
                ////System.out.println("ResultSet----> " + total);
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA GENERACION DEL REPORTE " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
        return total;
    }
    
    
    /**
     * Getter for property rd.
     * @return Value of property rd.
     */
    public com.tsp.operation.model.beans.ReporteDevolucion getRd() {
        return rd;
    }
    
    /**
     * Setter for property rd.
     * @param rd New value of property rd.
     */
    public void setRd(com.tsp.operation.model.beans.ReporteDevolucion rd) {
        this.rd = rd;
    }
    
    /**
     * Getter for property vecReporteDev.
     * @return Value of property vecReporteDev.
     */
    public java.util.Vector getVecReporteDev() {
        return vecReporteDev;
    }
    
    /**
     * Setter for property vecReporteDev.
     * @param vecReporteDev New value of property vecReporteDev.
     */
    public void setVecReporteDev(java.util.Vector vecReporteDev) {
        this.vecReporteDev = vecReporteDev;
    }
    
}
