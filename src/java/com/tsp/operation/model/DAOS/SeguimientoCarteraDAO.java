/*
    Document   : Modulo de Requisiciones Fintra
    Created on : 10/07/2013, 11:00:00 AM
    Author     : hcuello
*/

package com.tsp.operation.model.DAOS;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.CmbGeneralScBeans;
import com.tsp.operation.model.beans.Politicas;
import com.tsp.operation.model.beans.ReporteSanciones;
import com.tsp.operation.model.beans.SeguimientoCarteraBeans;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.util.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SeguimientoCarteraDAO extends MainDAO {

    public SeguimientoCarteraDAO(){
        super("SeguimientoCarteraDAO.xml");
    }
    public SeguimientoCarteraDAO(String dataBaseName){
        super("SeguimientoCarteraDAO.xml", dataBaseName);
    }
    
    //------------------------------------------------------------    
    
    public ArrayList<CmbGeneralScBeans> GetComboGenerico(String Query, String IdCmb, String DescripcionCmb, String wuereCmb) throws Exception {
        
        ArrayList combo = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            con = conectarJNDI(Query);
            String sql = this.obtenerSQL(Query);
            
            ps = con.prepareStatement(sql);
            if ( wuereCmb != "") {
                ps.setString(1, wuereCmb);
            }
            
            rs = ps.executeQuery();
            combo = new ArrayList();
            while (rs.next()) {
                CmbGeneralScBeans cmb = new CmbGeneralScBeans();
                cmb.setIdCmb(rs.getInt(IdCmb));
                cmb.setDescripcionCmb(rs.getString(DescripcionCmb));
                combo.add(cmb);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return combo;
    }
    
    //------------------------------------------------------------    
    
    public ArrayList<CmbGeneralScBeans> GetComboGenericoStr(String Query, String IdCmb, String DescripcionCmb, String wuereCmb) throws Exception {
        
        ArrayList combo = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            con = conectarJNDI(Query);
            String sql = this.obtenerSQL(Query);
            
            ps = con.prepareStatement(sql);
            if ( wuereCmb != "") {
                ps.setString(1, wuereCmb);
            }
            
            rs = ps.executeQuery();
            combo = new ArrayList();
            while (rs.next()) {
                CmbGeneralScBeans cmb = new CmbGeneralScBeans();
                cmb.setIdCmbStr(rs.getString(IdCmb)); //cmb.setIdCmb(rs.getInt(IdCmb)); getIdCmbStr-idCombostr
                cmb.setDescripcionCmb(rs.getString(DescripcionCmb));
                
                if ( Query.equals("SQL_OBTENER_TRAMOS") ) {
                    cmb.setDiasMesCmb(rs.getInt("diasmes"));
                }
            
                combo.add(cmb);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return combo;
    }

    //------------------------------------------------------------

    public ArrayList<SeguimientoCarteraBeans> GetSeguimientoCarteraConsolidadoBeans(String Query, String unidad_negocio, int periodo_foto, String wHrItem) throws Exception {
        
        ArrayList ListadoReqs = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            String sql = this.obtenerSQL(Query);
            
            con = conectarJNDI(Query);
            ps = con.prepareStatement(sql);

            ps.setInt(1, periodo_foto);
            ps.setString(2, unidad_negocio);
            
            rs = ps.executeQuery();
            ListadoReqs = new ArrayList();
            
            while (rs.next()) {
                SeguimientoCarteraBeans LstReq = new SeguimientoCarteraBeans();
                
                LstReq.setVencimientoMayor(rs.getString("vencimiento_mayor"));
                LstReq.setValorAsignado(rs.getString("valor_asignado"));
                LstReq.setPercValorAsignado(rs.getString("perc_valor_asignado"));
                LstReq.setCantidadAsignada(rs.getString("cantidad_asignada"));
                LstReq.setPercCantAsignada(rs.getString("perc_cantidad_asignada"));
                
                ListadoReqs.add(LstReq);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        
        return ListadoReqs;
    }

    //------------------------------------------------------------

    public ArrayList<SeguimientoCarteraBeans> GetSCarteraTramoAnteriorBeans(String Query, String unidad_negocio, int periodo_foto, String wHrItem) throws Exception {
        
        ArrayList ListadoReqs = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            String sql = this.obtenerSQL(Query);
            
            con = conectarJNDI(Query);
            ps = con.prepareStatement(sql);

            ps.setInt(1, periodo_foto);
            ps.setString(2, unidad_negocio);
            
            rs = ps.executeQuery();
            ListadoReqs = new ArrayList();
            
            while (rs.next()) {
                SeguimientoCarteraBeans LstReq = new SeguimientoCarteraBeans();
                
                LstReq.setVencimientoMayor(rs.getString("vencimiento_mayor"));
                LstReq.setValorAsignado(rs.getString("valor_asignado"));
                LstReq.setPercValorAsignado(rs.getString("perc_valor_asignado"));
                LstReq.setCantidadAsignada(rs.getString("cantidad_asignada"));
                LstReq.setPercCantAsignada(rs.getString("perc_cantidad_asignada"));
                LstReq.setTramoAnterior(rs.getString("tramo_anterior"));
                LstReq.setSumaTramoAnterior(rs.getString("sumatramo_anterior"));
                LstReq.setCantTramoAnterior(rs.getString("canttramo_anterior"));
                LstReq.setMaxRegTramo(rs.getString("max_reg_tramo"));
                LstReq.setMarcaX(rs.getString("marca"));
                
                
                ListadoReqs.add(LstReq);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        
        return ListadoReqs;
    }
    

    //------------------------------------------------------------

    public ArrayList<SeguimientoCarteraBeans> GetSCarteraRecaudoConsolidadoBeans(String Query, String unidad_negocio, int periodo_foto, String UserUso, String wHrItem) throws Exception {
        
       ArrayList ListadoReqs = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String fechaCorte = "";
        String fechaHoy = "";
        int dias = 0;
        
        try {
            fechaHoy = Util.getFechahoy();
            if(periodo_foto == Integer.parseInt(fechaHoy.substring(0,4)+fechaHoy.substring(5,7))){
                fechaCorte = fechaHoy;
            }else{
                String mes = String.valueOf(periodo_foto).substring(4,6);
                String anio = String.valueOf(periodo_foto).substring(0,4);
                dias = Util.diasMesB(Integer.parseInt(anio),Integer.parseInt(mes));
                fechaCorte = String.valueOf(periodo_foto).substring(0,4) + "-" + String.valueOf(Integer.parseInt(mes)) +"-"+ dias;
            }
            
            String sql = this.obtenerSQL(Query);
            
            con = conectarJNDI(Query);
            ps = con.prepareStatement(sql);
            
            ps.setInt(1, periodo_foto);
            ps.setString(2, unidad_negocio);
            ps.setString(3, UserUso);
            
            if(Query.equals("SQL_OBTENER_CONSOLIDADO_RECAUDO_TENDENCIA")){
                ps.setString(4, fechaCorte);
            }
            
            
            
            rs = ps.executeQuery();
            ListadoReqs = new ArrayList();
            
            while (rs.next()) {
                SeguimientoCarteraBeans LstReq = new SeguimientoCarteraBeans();
                
                LstReq.setVencimientoMayor(rs.getString("vencimiento_mayor"));
                LstReq.setStatus(rs.getString("status"));
                LstReq.setValorSaldo(rs.getString("valor_saldo"));
                LstReq.setDebidoCobrar(rs.getString("debido_cobrar"));
                LstReq.setRecaudoxCuota(rs.getString("recaudoxcuota"));
                LstReq.setCumplimiento(rs.getString("cumplimiento"));
                
                ListadoReqs.add(LstReq);
            }
            
        } catch (Exception e) {
            e.printStackTrace();            
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        
        return ListadoReqs;
    }
    
    //------------------------------------------------------------

    public ArrayList<SeguimientoCarteraBeans> GetSCarteraCargarSeguimientoBeans(String Query, String unidad_negocio, int periodo_foto, String aldia, String avencer, String vencido, String SuperHaving, String StatusVcto, String wHrItem, String UserUso, String DiaMes) throws Exception {
        
        ArrayList ListadoReqs = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            String sql = this.obtenerSQL(Query);
            sql = sql.replaceAll("#HAVING#", SuperHaving);
            sql = sql.replaceAll("#STATUSVCTO#", StatusVcto);
            sql = sql.replaceAll("#DIAPAGO#", DiaMes);
            
            con = conectarJNDI(Query);
            ps = con.prepareStatement(sql);

            ps.setInt(1, periodo_foto);
            ps.setString(2, unidad_negocio);
            ps.setString(3, UserUso);
            ps.setString(4, aldia);
            ps.setString(5, avencer);
            ps.setString(6, vencido);
            

            
            rs = ps.executeQuery();
            ListadoReqs = new ArrayList();
            
            while (rs.next()) {
                SeguimientoCarteraBeans LstReq = new SeguimientoCarteraBeans();
                
                LstReq.setDiaPago(rs.getString("dia_pago"));
                LstReq.setCedula(rs.getString("cedula"));
                LstReq.setNombreCliente(rs.getString("nombre_cliente"));
                LstReq.setDireccion(rs.getString("direccion"));
                LstReq.setBarrio(rs.getString("barrio"));
                LstReq.setCodDpto(rs.getString("coddpto"));
                LstReq.setCodCiudad(rs.getString("codciu"));
                LstReq.setCiudad(rs.getString("ciudad"));
                LstReq.setTelefono(rs.getString("telefono"));
                LstReq.setTelContacto(rs.getString("telcontacto"));
                LstReq.setNegocio(rs.getString("negocio"));
                LstReq.setConvenio(rs.getString("id_convenio"));
                LstReq.setUndNegocio(rs.getString("id_convenio"));
                LstReq.setVencimientoMayor(rs.getString("vencimiento_mayor"));
                LstReq.setValorAsignado(rs.getString("valor_asignado"));
                LstReq.setDebidoCobrar(rs.getString("debido_cobrar"));
                LstReq.setRecaudoxCuota(rs.getString("recaudosxcuota"));
                LstReq.setCumplimiento(rs.getString("cumplimiento"));
                LstReq.setValoraPagar(rs.getString("valor_a_pagar"));
                LstReq.setFechaUltimoCompromiso(rs.getString("fecha_ult_compromiso"));
                LstReq.setReestructuracion(rs.getString("reestructuracion"));
                LstReq.setJuridica(rs.getString("juridica"));
                LstReq.setPagaduria(rs.getString("pagaduria"));
                        
                ListadoReqs.add(LstReq);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        
        return ListadoReqs;
    }
    
     public ArrayList<SeguimientoCarteraBeans> GetSCarteraCargarSeguimientoClienteBeans(String Query, String unidad_negocio, int periodo_foto, String cedula, String UserUso, String aldia, String avencer, String vencido) throws Exception {
        
        ArrayList ListadoReqs = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            String sql = this.obtenerSQL(Query);
      
            
            con = conectarJNDI(Query);
            ps = con.prepareStatement(sql);

            ps.setInt(1, periodo_foto);
            ps.setString(2, unidad_negocio);
            ps.setString(3, UserUso);
            ps.setString(4, cedula);
            ps.setString(5, aldia);
            ps.setString(6, avencer);
            ps.setString(7, vencido);
            

            
            rs = ps.executeQuery();
            ListadoReqs = new ArrayList();
            
            while (rs.next()) {
                SeguimientoCarteraBeans LstReq = new SeguimientoCarteraBeans();
                
                LstReq.setCedula(rs.getString("cedula"));
                LstReq.setNombreCliente(rs.getString("nombre_cliente"));
                LstReq.setDireccion(rs.getString("direccion"));
                LstReq.setBarrio(rs.getString("barrio"));
                LstReq.setCodDpto(rs.getString("coddpto"));
                LstReq.setCodCiudad(rs.getString("codciu"));
                LstReq.setCiudad(rs.getString("ciudad"));
                LstReq.setTelefono(rs.getString("telefono"));
                LstReq.setTelContacto(rs.getString("telcontacto"));
                LstReq.setNegocio(rs.getString("negocio"));
                LstReq.setConvenio(rs.getString("id_convenio"));
                LstReq.setUndNegocio(rs.getString("nm_convenio"));
                LstReq.setVencimientoMayor(rs.getString("vencimiento_mayor"));
                LstReq.setValorAsignado(rs.getString("valor_asignado"));
                LstReq.setDebidoCobrar(rs.getString("debido_cobrar"));
                LstReq.setRecaudoxCuota(rs.getString("recaudosxcuota"));
                LstReq.setCumplimiento(rs.getString("cumplimiento"));
                LstReq.setValoraPagar(rs.getString("valor_a_pagar"));
                LstReq.setFechaUltimoCompromiso(rs.getString("fecha_ult_compromiso"));
                LstReq.setReestructuracion(rs.getString("reestructuracion"));
                LstReq.setJuridica(rs.getString("juridica"));
                LstReq.setPagaduria(rs.getString("pagaduria"));
                LstReq.setRefinanciacion(rs.getString("refinanciacion"));
                        
                ListadoReqs.add(LstReq);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        
        return ListadoReqs;
    }
    
    
    //------------------------------------------------------------

    public ArrayList<SeguimientoCarteraBeans> GetSCarteraDetalleCuentaBeans(String Query, String unidad_negocio, int periodo_foto, String wHrItem) throws Exception {
        
        ArrayList ListadoReqs = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            String sql = this.obtenerSQL(Query);
            
            
            con = conectarJNDI(Query);
            ps = con.prepareStatement(sql);

            ps.setInt(1, periodo_foto);
            ps.setString(2, unidad_negocio);
            ps.setString(3, wHrItem);
            
            rs = ps.executeQuery();
            ListadoReqs = new ArrayList();
            
            while (rs.next()) {
                SeguimientoCarteraBeans LstReq = new SeguimientoCarteraBeans();
                
                LstReq.setNegocio(rs.getString("negocio"));
                LstReq.setCedula(rs.getString("cedula"));
                LstReq.setNombreCliente(rs.getString("nombre_cliente"));
                LstReq.setCuota(rs.getString("cuota"));
                LstReq.setFechaVencimiento(rs.getString("fecha_vencimiento"));
                LstReq.setDiasVencidos(rs.getString("dias_vencidos"));
                LstReq.setDias_ven_hoy(rs.getString("dias_vencido_hoy"));
                LstReq.setStatus(rs.getString("status"));
                LstReq.setValorAsignado(rs.getString("valor_asignado"));
                LstReq.setDebidoCobrar(rs.getString("debido_cobrar"));
                LstReq.setInteres_mora(rs.getFloat("interes_mora"));
                LstReq.setGasto_cobranza(rs.getFloat("gasto_cobranza"));
                ListadoReqs.add(LstReq);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        
        return ListadoReqs;
    }

    //------------------------------------------------------------

    public ArrayList<SeguimientoCarteraBeans> GetSCarteraDetallePagosBeans(String Query, int periodo_foto, String wHrItem) throws Exception {
        
        ArrayList ListadoReqs = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            String sql = this.obtenerSQL(Query);
            
            
            con = conectarJNDI(Query);
            ps = con.prepareStatement(sql);
            
            ps.setInt(1, periodo_foto);
            ps.setString(2, wHrItem);
            
            rs = ps.executeQuery();
            ListadoReqs = new ArrayList();
            
            while (rs.next()) {
                SeguimientoCarteraBeans LstReq = new SeguimientoCarteraBeans();
                
                LstReq.setNegocio(rs.getString("negocio"));
                LstReq.setCedula(rs.getString("cedula"));
                LstReq.setNombreCliente(rs.getString("nombre_cliente"));
//                LstReq.setDocumento(rs.getString("documento"));
                LstReq.setCuota(rs.getString("cuota"));
//                LstReq.setFechaVencimiento(rs.getString("fecha_vencimiento"));
//                LstReq.setDiasVencidos(rs.getString("dias_vencidos"));
//                LstReq.setValorFactura(rs.getString("valor_factura"));
//                LstReq.setValorSaldo(rs.getString("valor_saldo"));
                LstReq.setIngreso(rs.getString("ingreso"));
                LstReq.setBranchCode(rs.getString("branch_code"));
                LstReq.setBankAccountNo(rs.getString("bank_account_no"));
                LstReq.setFechaIngreso(rs.getString("fecha_ingreso"));
                LstReq.setFechaConsignacion(rs.getString("fecha_consignacion"));
                LstReq.setDescripcionIngreso(rs.getString("descripcion_ingreso"));
                LstReq.setValorIngreso(rs.getString("valor_ingreso"));
                        
                ListadoReqs.add(LstReq);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        
        return ListadoReqs;
    }

      
   public ArrayList<SeguimientoCarteraBeans> GetSCarteraDetalleCuentaBeans2(String unidad_negocio, int periodo_foto, String wHrItem, String cuota) throws Exception {
        
        ArrayList ListadoReqs = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            String sql = this.obtenerSQL("SQL_DETALLE_CLIENTE2");
            
            
            con = conectarJNDI("SQL_DETALLE_CLIENTE2");
            ps = con.prepareStatement(sql);

            ps.setInt(1, periodo_foto);
            ps.setString(2, unidad_negocio);
            ps.setString(3, wHrItem);
            ps.setString(4, cuota);
            
            rs = ps.executeQuery();
            ListadoReqs = new ArrayList();
            
            while (rs.next()) {
                SeguimientoCarteraBeans LstReq = new SeguimientoCarteraBeans();
                
                LstReq.setNegocio(rs.getString("negocio"));
                LstReq.setCedula(rs.getString("cedula"));
                LstReq.setNombreCliente(rs.getString("nombre_cliente"));
                LstReq.setCuota(rs.getString("cuota"));
                LstReq.setFechaVencimiento(rs.getString("fecha_vencimiento"));
                LstReq.setDiasVencidos(rs.getString("dias_vencidos"));
                LstReq.setStatus(rs.getString("status"));
                LstReq.setValorAsignado(rs.getString("valor_asignado"));
                LstReq.setDebidoCobrar(rs.getString("debido_cobrar"));
                LstReq.setDocumento(rs.getString("documento"));
                LstReq.setInteres_mora(rs.getFloat("interes_mora"));
                LstReq.setGasto_cobranza(rs.getFloat("gasto_cobranza"));
                ListadoReqs.add(LstReq);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        
        return ListadoReqs;
    }
   
     public ArrayList<SeguimientoCarteraBeans> GetSCarteraDetallePagosBeans2(int periodo_foto, String wHrItem, String num_ingreso) throws Exception {
        
        ArrayList ListadoReqs = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            String sql = this.obtenerSQL("SQL_DETALLE_PAGOS2");
            
            
            con = conectarJNDI("SQL_DETALLE_PAGOS2");
            ps = con.prepareStatement(sql);
            
            ps.setString(1, wHrItem);
            ps.setInt(2, periodo_foto);
            ps.setInt(3, periodo_foto);
            ps.setInt(4, periodo_foto);
            ps.setString(5, num_ingreso);
            
            rs = ps.executeQuery();
            ListadoReqs = new ArrayList();
            
            while (rs.next()) {
                SeguimientoCarteraBeans LstReq = new SeguimientoCarteraBeans();
               
                LstReq.setFechaVencimiento(rs.getString("fecha_vencimiento"));
                LstReq.setDocumento(rs.getString("documento"));
                LstReq.setValorFactura(rs.getString("valor_factura"));
                LstReq.setValorSaldo(rs.getString("valor_saldo"));
                LstReq.setCuota(rs.getString("cuota"));
                LstReq.setValorIngreso(rs.getString("valor_ingreso")); 
                LstReq.setDiasVencidos(rs.getString("dias_vencidos"));
                LstReq.setIngreso(rs.getString("ingreso"));
                LstReq.setDescripcionIngreso(rs.getString("descripcion_ingreso"));
                LstReq.setBranchCode(rs.getString("branch_code"));
                LstReq.setBankAccountNo(rs.getString("bank_account_no"));
                LstReq.setFechaIngreso(rs.getString("fecha_ingreso"));
                LstReq.setFechaConsignacion(rs.getString("fecha_consignacion"));
                
                        
                ListadoReqs.add(LstReq);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        
        return ListadoReqs;
    }
     
     
       public ArrayList<SeguimientoCarteraBeans> GetSCarteraDetallePagosBeansTodos(int periodo_foto, String wHrItem, String num_ingreso) throws Exception {
        
        ArrayList ListadoReqs = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            String sql = this.obtenerSQL("SQL_DETALLE_PAGOS3");
            
            
            con = conectarJNDI("SQL_DETALLE_PAGOS3");
            ps = con.prepareStatement(sql);
            
            ps.setString(1, wHrItem);
            ps.setString(2, num_ingreso);
            
            rs = ps.executeQuery();
            ListadoReqs = new ArrayList();
            
            while (rs.next()) {
                SeguimientoCarteraBeans LstReq = new SeguimientoCarteraBeans();
                LstReq.setCuenta(rs.getString("cuenta"));
                LstReq.setFechaVencimiento(rs.getString("fecha_vencimiento"));
                LstReq.setDocumento(rs.getString("documento"));
                LstReq.setValorFactura(rs.getString("valor_factura"));
                LstReq.setValorSaldo(rs.getString("valor_saldo"));
                LstReq.setCuota(rs.getString("cuota"));
                LstReq.setValorIngreso(rs.getString("valor_ingreso")); 
                LstReq.setDiasVencidos(rs.getString("dias_vencidos"));
                LstReq.setIngreso(rs.getString("ingreso"));
                LstReq.setDescripcionIngreso(rs.getString("descripcion_ingreso"));
                LstReq.setBranchCode(rs.getString("branch_code"));
                LstReq.setBankAccountNo(rs.getString("bank_account_no"));
                LstReq.setFechaIngreso(rs.getString("fecha_ingreso"));
                LstReq.setFechaConsignacion(rs.getString("fecha_consignacion"));
                
                        
                ListadoReqs.add(LstReq);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        
        return ListadoReqs;
    }
   
       public ArrayList<SeguimientoCarteraBeans> GetResumenConsolidado(int periodo_foto, String uni_neg, String user) throws Exception {
        
        ArrayList ListadoReqs = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            String sql = this.obtenerSQL("SQL_OBTENER_RESUMEN_CONSOLIDADO");
            
            
            con = conectarJNDI("SQL_OBTENER_RESUMEN_CONSOLIDADO");
            ps = con.prepareStatement(sql);
            
            ps.setInt(1, periodo_foto);
            ps.setString(2, uni_neg);
            ps.setString(3, user);
            ps.setInt(4, periodo_foto);
            
            rs = ps.executeQuery();
            ListadoReqs = new ArrayList();
            
            while (rs.next()) {
                SeguimientoCarteraBeans LstReq = new SeguimientoCarteraBeans();
               
                LstReq.setVencimientoMayor(rs.getString("vencimiento_mayor"));
                LstReq.setNegocio(rs.getString("negocio"));
                LstReq.setCumplimiento(rs.getString("cumplimiento"));
                                        
                ListadoReqs.add(LstReq);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        
        return ListadoReqs;
    }

    public ArrayList<SeguimientoCarteraBeans> GetObtenerDetalleCarteraUndNegocio(String periodo, String undNeg, String iduser) throws SQLException {
        ArrayList ListadoReqs = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            String sql = this.obtenerSQL("SQL_DETALLE_CARTERA_UNDNEGOCIO");
            
            con = conectarJNDI("SQL_DETALLE_CARTERA_UNDNEGOCIO");
            ps = con.prepareStatement(sql);
            
            ps.setString(1, periodo);
            ps.setInt(2, Integer.parseInt(periodo));
            ps.setString(3, undNeg);
            ps.setString(4, iduser);
            ps.setString(5, undNeg);
            
            rs = ps.executeQuery();
            ListadoReqs = new ArrayList();
            
            while (rs.next()) {
                SeguimientoCarteraBeans LstReq = new SeguimientoCarteraBeans();
                
                LstReq.setUndNegocio(rs.getString("descripcion"));
                LstReq.setPeriodoFoto(rs.getString("periodo_foto"));
                LstReq.setCedula(rs.getString("cedula"));
                LstReq.setNombreCliente(rs.getString("nombre_cliente"));
                LstReq.setDireccion(rs.getString("direccion"));
                LstReq.setBarrio(rs.getString("barrio"));
                LstReq.setCiudad(rs.getString("ciudad"));
                LstReq.setTelefono(rs.getString("telefono"));
                LstReq.setTelContacto(rs.getString("telcontacto"));
                LstReq.setNegocio(rs.getString("negocio"));
                LstReq.setConvenio(rs.getString("id_convenio"));
                LstReq.setCuota(rs.getString("cuota"));
                LstReq.setValorAsignado(rs.getString("valor_asignado"));
                LstReq.setFechaVencimiento(rs.getString("fecha_vencimiento"));
                LstReq.setPeriodo(rs.getString("periodo_vcto"));
                LstReq.setVencimientoMayor(rs.getString("vencimiento_mayor"));
                LstReq.setDiasVencidos(rs.getString("dias_vencidos"));
                LstReq.setDiaPago(rs.getString("dia_pago"));
                LstReq.setStatus(rs.getString("status"));
                LstReq.setStatus_vencimiento(rs.getString("status_vencimiento"));
                LstReq.setDebidoCobrar(rs.getString("debido_cobrar"));
                LstReq.setRecaudoxcuotaFiducia(rs.getString("recaudosxcuota_fiducia"));
                LstReq.setRecaudoxcuotaFenal(rs.getString("recaudosxcuota_fenalco"));
                LstReq.setRecaudoxCuota(rs.getString("recaudosxcuota"));
                LstReq.setAgente(rs.getString("agente"));
                LstReq.setPagaduria(rs.getString("pagaduria"));
                        
                ListadoReqs.add(LstReq);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        
        return ListadoReqs;
    }

    public ArrayList<ReporteSanciones> generarSancionesFenalco(String periodo, String und_negocio) throws SQLException {
        ArrayList<ReporteSanciones> listaSanciones = new ArrayList<ReporteSanciones>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "";
        try {
            
            query = "SQL_REPORTE_DE_SANCIONES_FENALCO";
            
            String sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            
            ps.setInt(1, Integer.parseInt(periodo));
            ps.setInt(2, Integer.parseInt(periodo));
            ps.setString(3, und_negocio);
            ps.setInt(4, Integer.parseInt(und_negocio));
                        
            rs = ps.executeQuery();
                        
            while (rs.next()) {
                ReporteSanciones sanc = new ReporteSanciones();
                sanc.setUnidad_negocio(rs.getString("unidad_negocio"));
                sanc.setPeriodo_foto(rs.getString("periodo_foto"));
                sanc.setCedula(rs.getString("cedula"));
                sanc.setNombre_cliente(rs.getString("nombre_cliente"));
                sanc.setCod_negocio(rs.getString("negocio"));
                sanc.setVencimiento_mayor(rs.getString("vencimiento_mayor"));
                sanc.setFecha_pago_ingreso(rs.getString("fecha_pago_ingreso"));
                sanc.setFecha_vencimiento_mayor(rs.getString("fecha_vencimiento_mayor"));
                sanc.setDiferencia_pago(rs.getString("diferencia_pago"));
                sanc.setValor_saldo_foto(rs.getDouble("valor_saldo_foto"));
                sanc.setInteres_mora(rs.getDouble("interes_mora"));
                sanc.setGasto_cobranza(rs.getDouble("gasto_cobranza"));
                sanc.setNum_ingreso(rs.getString("num_ingreso"));
                sanc.setValor_ingreso(rs.getDouble("valor_ingreso"));
                sanc.setValor_cxc_ingreso(rs.getDouble("valor_cxc_ingreso"));
                sanc.setValor_ixm_ingreso(rs.getDouble("valor_ixm_ingreso"));
                sanc.setValor_gac_ingreso(rs.getDouble("valor_gac_ingreso"));
                sanc.setCuenta1(rs.getString("G16252145"));
                sanc.setCuenta2(rs.getString("G94350302"));
                sanc.setCuenta3(rs.getString("GI010010014205"));
                sanc.setCuenta4(rs.getString("I16252147"));
                sanc.setCuenta5(rs.getString("I94350301"));
                sanc.setCuenta6(rs.getString("II010010014170"));
                
                listaSanciones.add(sanc);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        
        return listaSanciones;
    }

    public ArrayList<ReporteSanciones> generarSancionesMicro(String periodo, String und_negocio) throws SQLException {
        ArrayList<ReporteSanciones> listaSanciones = new ArrayList<ReporteSanciones>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "";
        try {
            query = "SQL_REPORTE_DE_SANCIONES_MICROCREDITO";
            String sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            
            ps.setInt(1, Integer.parseInt(periodo));
            ps.setInt(2, Integer.parseInt(periodo));
            ps.setString(3, und_negocio);
            ps.setInt(4, Integer.parseInt(und_negocio));
                        
            rs = ps.executeQuery();
                        
            while (rs.next()) {
                ReporteSanciones sanc = new ReporteSanciones();
                sanc.setUnidad_negocio(rs.getString("unidad_negocio"));
                sanc.setPeriodo_foto(rs.getString("periodo_foto"));
                sanc.setCedula(rs.getString("cedula"));
                sanc.setNombre_cliente(rs.getString("nombre_cliente"));
                sanc.setCod_negocio(rs.getString("negocio"));
                sanc.setVencimiento_mayor(rs.getString("vencimiento_mayor"));
                sanc.setFecha_pago_ingreso(rs.getString("fecha_pago_ingreso"));
                sanc.setFecha_vencimiento_mayor(rs.getString("fecha_vencimiento_mayor"));
                sanc.setDiferencia_pago(rs.getString("diferencia_pago"));
                sanc.setValor_saldo_foto(rs.getDouble("valor_saldo_foto"));
                sanc.setInteres_mora(rs.getDouble("interes_mora"));
                sanc.setGasto_cobranza(rs.getDouble("gasto_cobranza"));
                sanc.setNum_ingreso(rs.getString("num_ingreso"));
                sanc.setValor_ingreso(rs.getDouble("valor_ingreso"));
                sanc.setValor_cxc_ingreso(rs.getDouble("valor_cxc_ingreso"));
                sanc.setValor_ixm_ingreso(rs.getDouble("valor_ixm_ingreso"));
                sanc.setValor_gac_ingreso(rs.getDouble("valor_gac_ingreso"));
                sanc.setCuenta1(rs.getString("GI010130014205"));
                sanc.setCuenta2(rs.getString("GI010010014205"));
                sanc.setCuenta3(rs.getString("I010130024170"));
                sanc.setCuenta4(rs.getString("I010010014170"));
                
                listaSanciones.add(sanc);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        
        return listaSanciones;
    }
    
    public String cargarCompromisosPago(String fechaini, String fechafin, String agente, String tipo, String domicilio, String usuario){
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();
        String filtro="where fecha_a_pagar::date between '"+fechaini+"' and '"+ fechafin+"'";
        filtro += (domicilio.equals("S")) ? " AND c.direccion not in('') ": " AND c.direccion = ''";
        String agenteFiltro = (agente.equals("") && !tipo.equals("coordinador")) ? usuario: agente;
        filtro += (agente.equals("") && tipo.equals("coordinador")) ? "": " AND  t1.agente = '" + agenteFiltro + "'";
        Gson gson = new Gson();
        String query = "SQL_OBTENER_COMPROMISOS_PAGO";
        //String filtro = (!showAll) ? " WHERE reg_status = ''": "" ;
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            
            ps = con.prepareStatement(query);  
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();
                fila.addProperty("id", rs.getInt("id"));
                fila.addProperty("negocio", rs.getString("negocio"));
                fila.addProperty("cedula", rs.getString("cedula"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("valor_pagar", rs.getDouble("valor_a_pagar"));
                fila.addProperty("fecha_pagar", rs.getString("fecha_a_pagar"));
                fila.addProperty("direccion", rs.getString("direccion"));
                fila.addProperty("barrio", rs.getString("barrio"));
                fila.addProperty("codciudad", rs.getString("codciudad"));
                fila.addProperty("ciudad", rs.getString("ciudad"));
                fila.addProperty("observacion", rs.getString("observacion"));
                fila.addProperty("agente", rs.getString("agente"));
                fila.addProperty("gestor", rs.getString("gestor"));
                fila.addProperty("rango_mora", rs.getString("rango_mora"));
                fila.addProperty("linea_negocio", rs.getString("linea_negocio"));
                
                datos.add(fila);
            }
           obj.add("rows", datos);
        }catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {               
                ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);
    }
    
    public String tipoAgenteCartera(String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String resp = "";
        String query = "SQL_TIPO_AGENTE_CARTERA";      
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setString(1, usuario);    
            
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = rs.getString("tipo_agente");
            }
        }catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
               ex.printStackTrace();
            }
        }
         return resp;
    }
    
    public String guardarReciboCaja(Integer consecutivo,String fecha_entrega,String asesor,String area,String estado_recibo, String usuario) {
        Connection con = null;
        StringStatement st = null;     
        String query = "SQL_INSERTAR_RC";
        String  sql = "";
      
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);         
          
            st.setInt(1, consecutivo);
            st.setString(2, fecha_entrega);
            st.setString(3, asesor);
            st.setString(4, area);
            st.setString(5, estado_recibo);
            st.setString(6, usuario);         
            sql = st.getSql();
       
        }catch (Exception e) {                         
                throw new SQLException("ERROR guardarReciboCaja \n" + e.getMessage());           
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
               
            } catch (SQLException ex) {
            }
            return sql;  
       }
    }

    public String actualizarRecibosCaja(String consecutivo,String fecha_entrega,String asesor,String area,String estado_recibo, String fecha_recibido, String id_cliente, 
            String nombre_cliente, String tipo_recaudo, String valor_recaudo, String cod_neg, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "SQL_ACTUALIZAR_RC";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, fecha_entrega);
            ps.setString(2, asesor);
            ps.setString(3, area);
            ps.setString(4, estado_recibo);
            ps.setString(5, (fecha_recibido.equals("")) ? "0099-01-01" :fecha_recibido );
            ps.setString(6, id_cliente);
            ps.setString(7, nombre_cliente);
            ps.setInt(8, Integer.parseInt(tipo_recaudo));
            ps.setDouble(9, Double.parseDouble(valor_recaudo));
            ps.setString(10, usuario);   
            ps.setString(11, cod_neg);
            ps.setString(12, consecutivo);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarRecibosCaja \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }
    
    public String anularReciboCaja(String consecutivo) {
        Connection con = null;
        PreparedStatement ps = null;       
        
        String   query = "SQL_ANULAR_RC";
        String  respuestaJson = "{}";
           
        try{
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));  
            ps.setString(1, consecutivo);         
            ps.executeUpdate();
           
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {   
              respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
              throw new SQLException("ERROR anularReciboCaja \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
       }
    }
    
     public String cargarInfoRecibo(String num_recibo){
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();
        String filtro="WHERE num_recibo = '"+num_recibo+"' AND id_estado_recibo in(SELECT table_code FROM tablagen "
                + " where table_type = 'ESTADOSRC' AND referencia = 'REGISTRO')";
        Gson gson = new Gson();
        String query = "SQL_CARGAR_RECIBOS_CAJA";     
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);            
            ps = con.prepareStatement(query);  
            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();               
                fila.addProperty("num_recibo", rs.getString("num_recibo"));
                fila.addProperty("fecha_entrega", rs.getString("fecha_entrega"));
                fila.addProperty("asesor", rs.getString("asesor"));
                fila.addProperty("area", rs.getString("area"));
                fila.addProperty("id_estado_recibo", rs.getString("id_estado_recibo"));
                fila.addProperty("fecha_recibido", rs.getString("fecha_recibido"));
                fila.addProperty("id_cliente", rs.getString("id_cliente"));
                fila.addProperty("nombre_cliente", rs.getString("nombre_cliente"));
                fila.addProperty("id_tipo_recaudo", rs.getString("id_tipo_recaudo"));
                fila.addProperty("valor_recaudo", rs.getDouble("valor_recaudo"));
                fila.addProperty("cod_neg", rs.getString("cod_neg"));
            }
            obj = fila;
        }catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);
    }
     
        
     public String cargarRecibosCaja(String asesor, String fechaini, String fechafin, String estado, String num_recibo){
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();
        String filtro="WHERE rc.creation_date::date between '"+fechaini+"' and '"+ fechafin+"'";
        filtro += (asesor.equals("")) ? "": " AND  asesor = '" + asesor + "'";
        filtro += (estado.equals("")) ? "": " AND  id_estado_recibo = '" + estado + "'";
        filtro += (num_recibo.equals("")) ? "": " AND  num_recibo = '" + num_recibo + "'";
        Gson gson = new Gson();
        String query = "SQL_CARGAR_RECIBOS_CAJA";     
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            
            ps = con.prepareStatement(query);  
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();           
                fila.addProperty("num_recibo", rs.getString("num_recibo"));
                fila.addProperty("fecha_entrega", rs.getString("fecha_entrega"));
                fila.addProperty("asesor", rs.getString("asesor"));
                fila.addProperty("area", rs.getString("area"));
                fila.addProperty("id_estado_recibo", rs.getString("id_estado_recibo"));
                fila.addProperty("estado_recibo", rs.getString("estado_recibo"));
                fila.addProperty("fecha_recibido", rs.getString("fecha_recibido"));
                fila.addProperty("id_cliente", rs.getString("id_cliente"));
                fila.addProperty("nombre_cliente", rs.getString("nombre_cliente"));
                fila.addProperty("id_tipo_recaudo", rs.getInt("id_tipo_recaudo"));
                fila.addProperty("tipo_recaudo", rs.getString("tipo_recaudo"));
                fila.addProperty("valor_recaudo", rs.getDouble("valor_recaudo"));
                fila.addProperty("creation_user", rs.getString("creation_user"));
                fila.addProperty("creation_date", rs.getString("creation_date"));
                fila.addProperty("comentario", rs.getString("comment"));
                fila.addProperty("cod_neg", rs.getString("cod_neg"));
                datos.add(fila);
            }
           obj.add("rows", datos);
        }catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
               ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);
    }
     
    public String cambiarEstadoReciboCaja(String num_recibo, String estado_recibo, String usuario){       
        Connection con = null;
        StringStatement st;       
        String sql = "";
        String query = "SQL_ACTUALIZAR_ESTADO_RC";      
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            st = new StringStatement(query, true);
            st.setString(1, estado_recibo);
            st.setString(2, usuario);
            st.setString(3, num_recibo);
            
            sql = st.getSql();
            
        }catch (Exception e) {
            try {
                throw new Exception("Error en cambiarEstadoReciboCaja " + e.toString());
            } catch (Exception ex) {
               ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
         return sql;   
    
    }
    
    public boolean existe_Recibo_Caja(String num_recibo, String sWhere) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resp = false;
        String query = "SQL_EXISTE_RC";   
        String filtro = sWhere;
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            
            ps = con.prepareStatement(query);
            ps.setString(1, num_recibo);         
            
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en existe_Recibo_Caja " + e.toString());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
         return resp;
    }

    
    public boolean isAllowToApplyRC(String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resp = false;
        String query = "SQL_VALIDAR_USER_APLICACION_RC";     
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setString(1, usuario);         
            
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en isAutToApplyRC " + e.toString());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
         return resp;
    }
    
    public String fn_get_Estado_Recibo_Caja(String num_recibo, String sWhere) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resp = false;
        String query = "SQL_EXISTE_RC";   
        String filtro = sWhere;
        String estado = "";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            
            ps = con.prepareStatement(query);
            ps.setString(1, num_recibo);         
            
            rs = ps.executeQuery();
            
            if (rs.next()){
                estado = rs.getString("id_estado_recibo");
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en fn_get_Estado_Recibo_Caja " + e.toString());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
         return estado;
    }

    public String get_Next_Recibo_Caja() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resp = false;
        String query = "SQL_TRAER_NEXT_CONSECUTIVO_RC";   
        String respuestaJson = "{}";
        String new_Recibo = "";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query); 
            rs = ps.executeQuery();
            
            if (rs.next()){
                new_Recibo = rs.getString("num_recibo");
            }
            respuestaJson = "{\"respuesta\":\""+new_Recibo+"\"}";
        }catch (Exception e) {
            try {
                throw new Exception("Error en get_Next_Recibo_Caja " + e.toString());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
                ex.printStackTrace();
            }
        }
         return respuestaJson;
    }
    
    public String get_Nombre_Cliente(String cedula) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resp = false;
        String query = "SQL_OBTENER_NOMBRE_CLIENTE";   
        String respuestaJson = "{}";
        String nomcli = "";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query); 
            ps.setString(1, cedula);
            rs = ps.executeQuery();
            
            if (rs.next()){
                nomcli = rs.getString("nombre");
            }
            respuestaJson = "{\"respuesta\":\""+nomcli+"\"}";
        }catch (Exception e) {
            try {
                throw new Exception("Error en get_Nombre_Cliente " + e.toString());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
                ex.printStackTrace();
            }
        }
         return respuestaJson;
    }
       
       
    
    public void actualizaConsecutivoRC() {
        Connection con = null;
        PreparedStatement ps = null;
        boolean resp = false;
        String query = "SQL_ACTUALIZA_SECUENCIA_RC";   
        String respuestaJson = "{}";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query); 
            ps.executeUpdate();            
         
        }catch (Exception e) {
            try {
                throw new Exception("Error en actualizaConsecutivoRC " + e.toString());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
     }
    
     public String reasignarAsesor(String num_recibo, String asesor, String usuario){       
        Connection con = null;
        StringStatement st;       
        String sql = "";
        String query = "SQL_REASIGNAR_ASESOR";      
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            st = new StringStatement(query, true);
            st.setString(1, asesor);
            st.setString(2, usuario);
            st.setString(3, num_recibo);
            
            sql = st.getSql();
            
        }catch (Exception e) {
            try {
                throw new Exception("Error en reasignarAsesor " + e.toString());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
         return sql;   
    
    }
     
    public String actualizaComentarioRC(String consecutivo, String comment, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;       
        
        String   query = "SQL_UPD_COMMENT_RC";
        String  respuestaJson = "{}";
           
        try{
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));  
            ps.setString(1, comment);
            ps.setString(2, usuario);
            ps.setString(3, consecutivo);         
            ps.executeUpdate();
           
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {   
              respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
              throw new SQLException("ERROR actualizaComentarioRC \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
       }
    }
   
    /**
     * Obtiene los negocios vigentes de un cliente
     * @param idCliente c�dula o nit del cliente
     * @return JSON con los c�digos de los negocios
     */
    public String cargarNegociosCliente(String idCliente) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray respuestaJson = new JsonArray();
        
        try {
            conn = conectarJNDIFintra();
            ps = conn.prepareStatement(obtenerSQL("GET_NEGOCIOS_CLIENTE"));
            ps.setString(1, idCliente);
            rs = ps.executeQuery();
            
            JsonObject negocio;
            while(rs.next()) {
                negocio  = new JsonObject();
                negocio.addProperty("codigo", rs.getString(1));
                respuestaJson.add(negocio);
            }
        } catch (SQLException e) {   
              System.err.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    this.desconectar(conn);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
       }
       return respuestaJson.toString();
    }
}      
