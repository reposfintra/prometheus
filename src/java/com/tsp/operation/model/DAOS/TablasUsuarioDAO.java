/***********************************************************************************
 * Nombre clase : ............... TablaUsuarioDAO.java                                   *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.                                       *
 * Autor :....................... Ing. David Lamadrid                               *
 * Fecha :....................... 4 de diciembre de 2005, 11:02 AM                   *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  dlamadrid
 */
public class TablasUsuarioDAO {
    private Vector vTablas;
    private Vector vNombreTablas;
    // private Vector vLetras=new Vector();
    //vLetras=["a","b"];
    private TreeMap tTablas;
    private TablasUsuario tabla;
    private TreeMap tTabla;
    
    private String ELIMINAR="delete from tablas_usuario where codigo=?";
    private String LISTAR_POR_U="select codigo,nombre,descripcion,usuario,creation_user,creation_date from tablas_usuario where usuario=?";
    private String INSERTAR="insert into tablas_usuario(reg_status,dstrct,nombre,descripcion,usuario,last_update,creation_date,creation_user) values('',?,?,?,upper(?),'0099-01-01 00:00:00',now(),?) ";
    private String TABLAS_POR_USUARIO="select * from tablas_usuario where usuario=?";
    private String LISTAR_TODO="select *  from tablas_usuario";
    private String EXISTE="select codigo from tablas_usuario where nombre=? and usuario=?";
    private static final String BUSCAR_TABLAS="select tablename from pg_tables where schemaname='public' and tablename like(?)";
    
    /** Creates a new instance of TablasUsuarioDAO */
    public TablasUsuarioDAO() {
    }
    
    /**
     * inserta un registro en la tabla tablas_usuario
     * @autor David Lamadrid
     * @throws SQLException
     * @version 1.0
     */
    public void insertar() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.INSERTAR);
                st.setString(1,tabla. getDstrct());
                st.setString(2,tabla.getNombre());
                st.setString(3,tabla.getDescripcion());
                st.setString(4,tabla.getUsuario());
                st.setString(5,tabla.getCreation_user());
                st.executeUpdate();
            }
        }catch(SQLException e) {
            throw new SQLException("ERROR AL INSERTAR EL CONSULTA: " + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * elimina un registro en la tabla tablas_usuario dada la llave primaria del registro
     * @autor David Lamadrid
     * @throws SQLException
     * @version 1.0
     */
    public void eliminar(int codigo) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.ELIMINAR);
                st.setInt(1,codigo);
                st.executeUpdate();
            }
        }catch(SQLException e) {
            throw new SQLException("ERROR AL INSERTAR EL CONSULTA: " + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * Lista los registros de tablas_usuario por usuario
     * @autor David Lamadrid
     * @throws SQLException
     * @version 1.0
     */
    public void listarPorU(String codigo) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        this.vTablas = new Vector();
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.LISTAR_POR_U);
                st.setString(1, codigo);
                rs = st.executeQuery();
                
                while( rs.next() ) {
                    this.tabla = new TablasUsuario();
                    this.tabla =this.tabla.load(rs);
                    this.vTablas.add(this.tabla);
                }
            }
        }catch(SQLException e) {
            throw new SQLException("ERROR AL LISTAR LAS APLICACIONES: " + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * Lista todos los registros del la tabla tablas_usuario
     * @autor David Lamadrid
     * @throws SQLException
     * @version 1.0
     */
    public void listarTodo() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        this.vTablas = new Vector();
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.LISTAR_TODO);
                rs = st.executeQuery();
                while( rs.next() ) {
                    this.tabla = new TablasUsuario();
                    this.tabla = this.tabla.load(rs);
                    this.vTablas.add(this.tabla);
                }
            }
        }catch(SQLException e) {
            throw new SQLException("ERROR AL LISTAR LAS APLICACIONES: " + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    
    /**
     * Metodo que retorna false si no exite el registro y true si un usuario ya tiene una tabla asignada con el mismo nombre
     * @autor David Lamadrid
     * @throws SQLException
     * @version 1.0
     */
    public boolean existePorU(String nombre,String usuario) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        this.vTablas = new Vector();
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.EXISTE);
                st.setString(1, nombre);
                st.setString(2, usuario);
                rs = st.executeQuery();
                while( rs.next() ) {
                    sw=true;
                }
            }
        }catch(SQLException e) {
            throw new SQLException("ERROR AL LISTAR LAS APLICACIONES: " + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        return sw;
    }
    
    /**
     * Getter for property vTablas.
     * @return Value of property vTablas.
     */
    public java.util.Vector getVTablas() {
        return vTablas;
    }
    
    /**
     * Setter for property vTablas.
     * @param vTablas New value of property vTablas.
     */
    public void setVTablas(java.util.Vector vTablas) {
        this.vTablas = vTablas;
    }
    
    /**
     * Getter for property tTablas.
     * @return Value of property tTablas.
     */
    public java.util.TreeMap getTTablas() {
        return tTablas;
    }
    
    /**
     * Setter for property tTablas.
     * @param tTablas New value of property tTablas.
     */
    public void setTTablas(java.util.TreeMap tTablas) {
        this.tTablas = tTablas;
    }
    
    /**
     * Getter for property tabla.
     * @return Value of property tabla.
     */
    public com.tsp.operation.model.beans.TablasUsuario getTabla() {
        return tabla;
    }
    
    /**
     * Setter for property tabla.
     * @param tabla New value of property tabla.
     */
    public void setTabla(com.tsp.operation.model.beans.TablasUsuario tabla) {
        this.tabla = tabla;
    }
    
    /**
     * Metodo que retorna true si existe una tabla en el sitema dado el nombre de la tabla
     * @autor.......David Lamadrid
     * @param.......String nombreTabla
     * @throws......registro no existe en el sitema
     * @version.....1.0.
     * @return.......boolen x(true si existe ,false si no existe)
     */
    public  boolean existeTabla(String nombre ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        this.vTablas = new Vector();
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                DatabaseMetaData dbmd = con.getMetaData();
                rs = dbmd.getColumns(null, null, nombre, null);
                if ( rs.next() ) {
                    sw=true;
                }
            }
        }catch(SQLException e) {
            throw new SQLException("ERROR AL LISTAR LAS APLICACIONES: " + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        return sw;
    }
    /**
     * Metodo que retorna true si existe una tabla en el sitema dado el nombre de la tabla
     * @autor.......David Lamadrid
     * @param.......String nombreTabla
     * @see.........
     * @throws......registro no existe en el sitema
     * @version.....1.0.
     * @return.......boolen x(true si existe ,false si no existe)
     */

        public void buscarTablas (String nombre ) throws SQLException{
            Connection con= null;
            PreparedStatement st = null;
            ResultSet rs = null;
            PoolManager poolManager = null;
            this.vNombreTablas = new Vector();
            try{
                poolManager = PoolManager.getInstance ();
                con = poolManager.getConnection ("fintra");
                if (con != null){

                    st = con.prepareStatement (BUSCAR_TABLAS);
                    st.setString (1, nombre+"%");
                    ////System.out.println ("query: "+st);
                    rs = st.executeQuery ();

                    while( rs.next () ){
                        this.vNombreTablas.add (""+rs.getString ("tablename"));
                    }

                }
            }catch(Exception e) {
                 throw new SQLException("ERROR en tablasUsuarioDAO.buscarTablas : " + e.getMessage() );
            }finally{
                 if(st != null) {
                     try {
                         st.close();
                     }catch(SQLException e) {
                         throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                     }
                 }
                 if ( con != null) {
                     poolManager.freeConnection("fintra", con );
                 }
            }
        }
    
    
    /**
     * Getter for property tTabla.
     * @return Value of property tTabla.
     */
    public java.util.TreeMap getTTabla() {
        return tTabla;
    }
    
    /**
     * Setter for property tTabla.
     * @param tTabla New value of property tTabla.
     */
    public void setTTabla(java.util.TreeMap tTabla) {
        this.tTabla = tTabla;
    }
    
    
    /**
     * metodo que setea el TreeMap tConsultas con el Vector vConsultas que contiene una lista de Objetos con los registros de consultas_usuarios consernientes a un usuario
     * @autor David Lamadrid
     * @throws SQLException
     * @version 1.0
     */
    public void tTablasPorUsuario(String usuario) throws SQLException {
        this.tTabla = new TreeMap();
        
        //Vector banks = this.obtenerBancos();
        
        this.listarPorU(usuario);
        Vector vc=this.getVTablas();
        for(int i=0; i< vc.size(); i++) {
            TablasUsuario tabla = (TablasUsuario)vc.elementAt(i);
            // ////System.out.println("campoooooooooooooooooo    en treemap         "+ tabla.getNombre ());
            this.tTabla.put(tabla.getNombre(),tabla.getNombre()+"");
        }
    }
    
    /**
     * Getter for property vNombreTablas.
     * @return Value of property vNombreTablas.
     */
    public java.util.Vector getVNombreTablas() {
        return vNombreTablas;
    }
    
    /**
     * Setter for property vNombreTablas.
     * @param vNombreTablas New value of property vNombreTablas.
     */
    public void setVNombreTablas(java.util.Vector vNombreTablas) {
        this.vNombreTablas = vNombreTablas;
    }
    //Fin del Metodo
}
