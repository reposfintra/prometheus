 /**************************************************************************
 * Nombre clase: ReportePODDAO.java                                           *
 * Autor: Ing. Ivan DArio Gomez Vanegas                                    *
 * Fecha: Created on 1 de octubre de 2005, 08:20 AM                        *
 * Versi�n: Java 1.0                                                       *
 * Copyright: Fintravalores S.A. S.A.                                 *
 ***************************************************************************/



package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;

public class ReportePODDAO extends MainDAO{
    
    /** Creates a new instance of ReportePODDAO */
    public ReportePODDAO() {
        super("ReportePODDAO.xml");
    }
    
     /************************************************************************
     * Metodo   : searchCliente, retorna una lista de clientes.
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   :
     * @version : 1.0
     *************************************************************************/
    public List searchReporte(String fecini, String fecfin, String cliente) throws SQLException{
        Connection con = null;
        PreparedStatement st          = null;
        ResultSet         rs    = null;
        List listado =null;
        String query = "SQL_LISTAR";
        try {
           con = this.conectarJNDI(query);
           if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
           st.setString(1,fecini);
           st.setString(2,fecfin);
           st.setString(3,cliente);
           rs= st.executeQuery();        
           if(rs!=null){
              listado = new LinkedList();
              while(rs.next()){
                 ReportePOD  reporte  = new ReportePOD();
                   reporte.setCliente     (rs.getString("nomcli"));
                   reporte.setPlanilla    (rs.getString("numpla"));
                   reporte.setRemesa      (rs.getString("numrem"));
                   reporte.setFechaRemesa (rs.getString("fecrem"));
                   reporte.setPlaca       (rs.getString("plaveh"));
                   reporte.setRuta        (rs.getString("ruta"));
                   reporte.setDestinatario(rs.getString("destinatario"));
                   reporte.setFaccial     (rs.getString("factura"));
                   reporte.setFechaLlegada(rs.getString("fechallegada"));
                   reporte.setNota_entrega(rs.getString("nota_entrega"));
                   reporte.setNum_viaje   (rs.getString("num_viaje"));
                listado.add(reporte); 
              }
           }
        }} catch(SQLException e) {
            throw new SQLException("Error en rutina searchReporte  [ReportePODDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listado;
     }
    
}
