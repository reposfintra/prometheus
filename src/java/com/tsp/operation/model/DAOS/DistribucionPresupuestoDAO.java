/*
 * DistribucionPresupuesto.java
 *
 * Created on 14 de abril de 2005, 07:18 AM
 */
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.UtilFinanzas;

import java.sql.*;
import java.util.*;
import java.text.SimpleDateFormat;
/**
 *
 * @author  Mario Fontalvo
 */
public class DistribucionPresupuestoDAO {
    
    private String SQL_CREATE_PTO_VENTAS = " CREATE TABLE FIN.PTO_VENTAS(                            " +
    "   ESTADO       VARCHAR (1) NOT NULL DEFAULT '',     " +
    "   DSTRCT_CODE  VARCHAR (4) NOT NULL DEFAULT '',     " +
    "   ANO          VARCHAR (4) NOT NULL DEFAULT '',     " +
    "   MES          VARCHAR (2) NOT NULL DEFAULT '',     " +
    "   TIPO         VARCHAR (1) NOT NULL DEFAULT '',     " +
    "   STD_JOB_NO   VARCHAR (6) NOT NULL DEFAULT '',     " +
    "   AGENCIA      VARCHAR (5) NOT NULL DEFAULT '',     " +
    "   CLIENTE      VARCHAR (6) NOT NULL DEFAULT '',     " +
    "   CMENSUAL     NUMERIC (5) NOT NULL DEFAULT '0',    " +
    "   CSEM01       NUMERIC (5) NOT NULL DEFAULT '0',    " +
    "   CSEM02       NUMERIC (5) NOT NULL DEFAULT '0',    " +
    "   CSEM03       NUMERIC (5) NOT NULL DEFAULT '0',    " +
    "   CSEM04       NUMERIC (5) NOT NULL DEFAULT '0',    " +
    "   CSEM05       NUMERIC (5) NOT NULL DEFAULT '0',    " +
    "   CDIA01       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA02       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA03       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA04       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA05       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA06       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA07       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA08       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA09       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA10       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA11       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA12       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA13       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA14       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA15       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA16       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA17       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA18       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA19       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA20       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA21       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA22       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA23       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA24       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA25       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA26       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA27       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA28       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA29       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA30       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   CDIA31       NUMERIC (4) NOT NULL DEFAULT '0',    " +
    "   USUARIO_CREACION      VARCHAR (10) NOT NULL DEFAULT '',     " +
    "   FECHA_CREACION        TIMESTAMP NOT NULL DEFAULT NOW(),     " +
    "   USUARIO_ACTUALIZACION VARCHAR (10) NOT NULL DEFAULT '',     " +
    "   FECHA_ACTUALIZACION   TIMESTAMP NOT NULL DEFAULT NOW(),     " +
    "   CONSTRAINT PK_PTO_VENTAS PRIMARY KEY (ESTADO, DSTRCT_CODE, ANO, MES, STD_JOB_NO, FECHA_ACTUALIZACION)) " ;
    
    private String SQL_INSERT_PTO_VENTAS = " INSERT INTO FIN.PTO_VENTAS                                                                              " +
    " (ESTADO, DSTRCT_CODE, ANO, MES ,TIPO, STD_JOB_NO, AGENCIA ,CLIENTE,                                 " +
    "  CMENSUAL, CSEM01 ,CSEM02, CSEM03, CSEM04, CSEM05,                                                  " +
    "  CDIA01, CDIA02, CDIA03, CDIA04, CDIA05, CDIA06, CDIA07, CDIA08, CDIA09, CDIA10,                    " +
    "  CDIA11, CDIA12, CDIA13, CDIA14, CDIA15, CDIA16, CDIA17, CDIA18, CDIA19, CDIA20,                    " +
    "  CDIA21, CDIA22, CDIA23, CDIA24, CDIA25, CDIA26, CDIA27, CDIA28, CDIA29, CDIA30,                    " +
    "  CDIA31, USUARIO_CREACION, FECHA_CREACION, USUARIO_ACTUALIZACION, FECHA_ACTUALIZACION)              " +
    " VALUES                                                                                              " +
    " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)   " ;
    private String SQL_ANULAR_PTO_VENTAS     = " UPDATE FIN.PTO_VENTAS SET ESTADO = 'A'    WHERE DSTRCT_CODE = ? AND AGENCIA = ? AND CLIENTE = ? AND STD_JOB_NO = ? AND ANO = ? AND MES = ? ";
    private String SQL_BUSCAR_FECHA_CREACION = " SELECT FECHA_CREACION, FECHA_ACTUALIZACION FROM FIN.PTO_VENTAS WHERE DSTRCT_CODE = ? AND AGENCIA = ? AND CLIENTE = ? AND STD_JOB_NO = ? AND ANO = ? AND MES = ? AND ESTADO <> 'A' ";
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // CONSULTAS
    
    private final String SQL_ESTANDARES = " SELECT                                            " +
    "       A.DSTRCT_CODE,                              " +
    "       A.STD_JOB_NO,                               " +
    "       A.STD_JOB_DESC,                             " +
    "       C.AGDUENIA,                                 " +
    "       B.NOMCIU,                                   " +
    "       '000'||SUBSTR(A.STD_JOB_NO,1,3),            " +
    "       C.NOMCLI,                                   " +
    "       A.ORIGIN_CODE,                              " +
    "       D.NOMCIU    ,                               " +
    "       A.DESTINATION_CODE,                         " +
    "       E.NOMCIU,                                   " +
    "       A.UNIT_OF_WORK,                             " +
    "       A.MAINT_TYPE, A.RECURSO1, A.RECURSO2, A.RECURSO3, A.RECURSO4 , A.RECURSO5," +
    "       A.UNIDAD,                                   " +
    "       A.UNITS_REQUIRED                            " +
    " FROM                                              " +
    "     STDJOB  A,                                    " +
    "	CIUDAD  B,                                    " +
    "	CLIENTE C,                                    " +
    "	CIUDAD  D,                                    " +
    "	CIUDAD  E                                     " +
    " WHERE                                             " +
    "          A.STD_JOB_NO LIKE '___G%'                          " +
    "	 and C.CODCLI = '000'||SUBSTR(A.STD_JOB_NO,1,3)         " +
    "	 and B.CODCIU = C.AGDUENIA                              " +
    "	 and D.CODCIU = A.ORIGIN_CODE                           " +
    "	 and E.CODCIU = A.DESTINATION_CODE                      " +
    "      and C.AGDUENIA      LIKE ('#AGENCIA#')                 " +
    "      and C.CODCLI        LIKE ('#CLIENTE#')                 " +
    "      and A.STD_JOB_NO    LIKE ('#STDJOB#')                  " +
    " ORDER BY A.DSTRCT_CODE, B.NOMCIU, C.NOMCLI, A.STD_JOB_NO    " ;
    
    
    // DEVUELVE TARIFA, MONEDA - PARAMETRO DE ENTRADA UNIT_OF_WORK (UNIDAD DE TRABAJO)
    private final String SQL_TAR_MON    = " SELECT SUBSTR(B.ASSOC_REC,1,11)/10000, SUBSTR(B.ASSOC_REC,16,3)  " +
    " FROM MSF010 B  WHERE B.TABLE_TYPE='UW' AND B.TABLE_CODE = ?      ";
    
    
    private final String SQL_COLUMNA_SEMANAL = ", COALESCE(CSEM#INDICE#,'0')";
    private final String SQL_COLUMNA_DIARIA  = ", COALESCE(CDIA#INDICE#,'0')";
    
    private final String SQL_VENTASA   = "SELECT COALESCE(CMENSUAL,'0') #RESTO# FROM FIN.PTO_VENTAS WHERE DSTRCT_CODE = ? AND STD_JOB_NO = ? AND  ANO = ? AND MES = ? AND ESTADO <> 'A'";
    
    private final String SQL_VENTASP   = "SELECT COALESCE(CMENSUAL,'0') #RESTO# , USUARIO_ACTUALIZACION, FECHA_ACTUALIZACION FROM FIN.PTO_VENTAS WHERE DSTRCT_CODE = ? AND STD_JOB_NO = ? AND  ANO = ? AND MES = ? AND ESTADO = 'A' ORDER BY FECHA_ACTUALIZACION DESC";
    
    private final String SQL_HISTORIAL = "SELECT COALESCE(CMENSUAL,'0') #RESTO# , USUARIO_ACTUALIZACION, FECHA_ACTUALIZACION FROM FIN.PTO_VENTAS WHERE DSTRCT_CODE = ? AND STD_JOB_NO = ? AND  ANO = ? AND MES = ? ORDER BY FECHA_ACTUALIZACION DESC";
    
    private final String SQL_AGEN_CLIENTE = "SELECT NOMCLI, AGDUENIA FROM CLIENTE WHERE CODCLI = ? ";
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    private final String SQL_DATOS_STDJOB =
    " SELECT  C.AGDUENIA,  B.NOMCIU, '000'||SUBSTR(A.STD_JOB_NO,1,3),     " +
    "	  C.NOMCLI,  A.STD_JOB_NO,  A.STD_JOB_DESC,  A.ORIGIN_CODE,   " +
    "	  D.NOMCIU , A.DESTINATION_CODE, E.NOMCIU                     " +
    " FROM    STDJOB  A,                                                  " +
    "	  CIUDAD  B,                                                  " +
    "	  CLIENTE C,                                                  " +
    "	  CIUDAD  D,                                                  " +
    "	  CIUDAD  E                                                   " +
    " WHERE     A.STD_JOB_NO LIKE '___G%'                                 " +
    "	and C.CODCLI = '000'||SUBSTR(A.STD_JOB_NO,1,3)                " +
    "	and B.CODCIU = C.AGDUENIA                                     " +
    "	and D.CODCIU = A.ORIGIN_CODE                                  " +
    "	and E.CODCIU = A.DESTINATION_CODE                             " +
    "	and C.AGDUENIA LIKE ?                                         " +
    "       and C.CODCLI   LIKE ?                                         " +
    " ORDER BY B.NOMCIU , C.NOMCLI                                        " ;
    
    
    private final String SQL_TASA         = " SELECT ANO , MES, DOLAR, BOLIVAR, DTF FROM FIN.PTO_TASA WHERE ANO = ? ";
    
    private final String SQL_BORRAR_PTO   = " delete from fin.pto_ventas where dstrct_code = ? and ano = ? and mes = ? ";
    
    private final String SQL_PTOCARBON ="SELECT a.*   " +
    "      FROM                 " +
    "      fin.pto_ventas a, " +
    "      stdjob b     " +
    "     WHERE    " +
    "        b.dstrct_code = ? " +
    "        AND b.std_job_no = ? " +
    "        AND b.stdjobgen = a.std_job_no" +
    "        AND a.ano||a.mes  BETWEEN ? AND ? " +
    "        AND a.estado = ''  ";
    
    /** Creates a new instance of FlujoCajaDAO */
    public DistribucionPresupuestoDAO() {
    }
    
    public void CREATE_TABLE()throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try{
            st = conPostgres.prepareStatement(this.SQL_CREATE_PTO_VENTAS);
            st.executeUpdate();
        }
        catch(SQLException e){
        }
        finally {
            if(st!=null) st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    public String INSERT(Connection conPostgres, String args[]) throws SQLException {
        PoolManager       poolManager = null;
        PreparedStatement st          = null;
        boolean liberar   = false;
        
        
        SimpleDateFormat fmt    = new SimpleDateFormat("yyyy/MM/dd kk:mm:ss");
        java.util.Date[] Fechas = null;
        java.util.Date Sistema  = null;
        
        if (conPostgres==null){
            poolManager = PoolManager.getInstance();
            conPostgres    = poolManager.getConnection("fintra");
            liberar     = true;
        }
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            
            
            // DISTRITO  //AGENCIA   //CLIENTE   //STDJOB   //ANO   //MES
            String Params[] = {args[1], args[6], args[7], args[5], args[2], args[3]};
            
            
            // BUSQUEDA FECHA CREACION y ACTUALIZACION
            Fechas = BuscarFechas(conPostgres, Params );
            Sistema  = new java.util.Date(  fmt.format(new java.util.Date())   );
            
            if (Fechas[1]!=null){
                while (Sistema.getTime() <= Fechas[1].getTime() ){
                    Sistema.setSeconds(Sistema.getSeconds()+1);
                }
            }
            
            // ANULACION DEL VIAJE
            AnularViaje(conPostgres, Params);
            
            // INSERT
            st = conPostgres.prepareStatement(this.SQL_INSERT_PTO_VENTAS);
            for (int i=0;i<args.length;i++)
                st.setString((i+1),  args[i]);
            st.setString(args.length + 1, (Fechas[0]==null?  UtilFinanzas.getFechaActual_String(6): fmt.format(Fechas[0])   ));
            st.setString(args.length + 2,  args[args.length-1]);
            st.setString(args.length + 3, fmt.format(Sistema)   );
            
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina INSERT [DistribuicionPresupuestoDAO]... \n"+e.getMessage());
        }
        finally {
            if (st!=null) st.close();
            if (liberar)  poolManager.freeConnection("fintra",conPostgres);
        }
        return fmt.format(Sistema);
    }
    
    
    public java.util.Date [] BuscarFechas(Connection conPostgres, String args[]) throws SQLException {
        PoolManager       poolManager = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        boolean liberar   = false;
        java.util.Date [] Fechas = {null,null};
        if (conPostgres==null){
            poolManager = PoolManager.getInstance();
            conPostgres    = poolManager.getConnection("fintra");
            liberar     = true;
        }
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            // BUSQUEDA FECHA CREACION
            st = conPostgres.prepareStatement(this.SQL_BUSCAR_FECHA_CREACION);
            for (int i=0;i<args.length;i++)
                st.setString((i+1),  args[i]);
            rs = st.executeQuery();
            while (rs.next()){
                Fechas[0] = new java.util.Date(rs.getString(1).replaceAll("-","/"));
                Fechas[1] = new java.util.Date(rs.getString(2).replaceAll("-","/"));
                break;
            }
            
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina BuscarFechaCreacion [DistribuicionPresupuestoDAO]... \n"+e.getMessage());
        }
        finally {
            if (rs!=null) rs.close();
            if (st!=null) st.close();
            if (liberar)  poolManager.freeConnection("fintra",conPostgres);
        }
        return Fechas;
    }
    
    
    public void AnularViaje(Connection conPostgres, String args[]) throws SQLException {
        PoolManager       poolManager = null;
        PreparedStatement st          = null;
        boolean liberar   = false;
        if (conPostgres==null){
            poolManager = PoolManager.getInstance();
            conPostgres    = poolManager.getConnection("fintra");
            liberar     = true;
        }
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            // BUSQUEDA FECHA CREACION
            st = conPostgres.prepareStatement(this.SQL_ANULAR_PTO_VENTAS);
            for (int i=0;i<args.length;i++)
                st.setString((i+1),  args[i]);
            // //System.out.println("SQL:" + st.toString());
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina BuscarFechaCreacion [DistribuicionPresupuestoDAO]... \n"+e.getMessage());
        }
        finally {
            if (st!=null) st.close();
            if (liberar)  poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Tipo 0: Vista Mesual, 1: Vista Semanal, 2: Vista Diaria
    public Hashtable BuscarPresupuesto(int Tipo, String Ano, String Mes, String Stdjob, String Cliente , String Agencia , boolean Filtrar) throws Exception {
        Hashtable Registros = new Hashtable();
        List      Valores   = new LinkedList();
        Hashtable Totales   = new Hashtable();
        PreparedStatement st = null;
        ResultSet         rs = null;
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        Connection conOracle    = poolManager.getConnection("oracle");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            ////////////////////////////////////////////////////////////////////////////////////////////
            st = conPostgres.prepareStatement( SQL_ESTANDARES.replaceAll("#AGENCIA#", Agencia).replaceAll("#CLIENTE#", Cliente).replaceAll("#STDJOB#", Stdjob));
            ////System.out.println("SQL:"+st.toString());
            rs = st.executeQuery();
            ResultSetMetaData rsmd = rs.getMetaData();
            while(rs.next()){
                DatosView datos = new DatosView(); //= DatosView.loadDatos(rs, 17 ,rsmd.getColumnCount());
                datos.addDescriptor(rs.getString(1) ,  "DISTRITO"     );
                datos.addDescriptor(rs.getString(2) ,  "STDJOBNO"     );
                datos.addDescriptor(rs.getString(3) ,  "STDJOBDESC"   );
                datos.addDescriptor(rs.getString(4) ,  "CODIGOAGENCIA");
                datos.addDescriptor(rs.getString(5) ,  "NOMBREAGENCIA");
                datos.addDescriptor(rs.getString(6) ,  "CODIGOCLIENTE");
                datos.addDescriptor(rs.getString(7) ,  "NOMBRECLIENTE");
                datos.addDescriptor(rs.getString(8) ,  "CODIGOORIGEN" );
                datos.addDescriptor(rs.getString(9) ,  "NOMBREORIGEN" );
                datos.addDescriptor(rs.getString(10),  "CODIGODESTINO");
                datos.addDescriptor(rs.getString(11),  "NOMBREDESTINO");
                datos.addDescriptor(rs.getString(12),  "UW"           );
                datos.addDescriptor(rs.getString(13),  "TIPOVIAJE"    );
                
                String recurso = rs.getString(14);
                for (int k=15;k<=18;k++) recurso += (!recurso.equals("") && !rs.getString(k).equals("") ? "; " + rs.getString(k): "");
                datos.addDescriptor(recurso,  "RECURSO"  );
                
                
                datos.addDescriptor(rs.getString(19),  "UNIDAD_R"     );
                datos.addDescriptor(rs.getString(20),  "CANTIDAD_R"   );
                
                datos = this.BuscarTarifaMoneda(datos, conOracle);
                datos = this.BuscarVentas(Tipo, Ano, Mes, datos, conPostgres);
                
                if ((!Filtrar) || (Filtrar && Double.parseDouble(datos.getValor("TOTAL"))>0 )){
                    datos.addDescriptor(String.valueOf( Double.parseDouble(datos.getValor("TOTAL")) * Double.parseDouble(datos.getValor("TARIFA")) *  Double.parseDouble(datos.getValor("CANTIDAD_R")) )   , "TOTALPTO" );
                    Valores.add(datos);
                }
                Totales = UtilFinanzas.Acumulado(Totales, datos.getListaDatos(), (Mes.equals("TD")?12:1)*(Tipo==0?1:(Tipo==1?5:31)) );
            }
            Registros.put("nrocols", String.valueOf((Mes.equals("TD")?12:1)*(Tipo==0?1:(Tipo==1?5:31))) );
            Registros.put("valores", Valores);
            Registros.put("totales", Totales);
            ////////////////////////////////////////////////////////////////////////////////////////////
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarPresupuesto [DistribucionPresupuestoDAO]... \n"+e.getMessage());
        }
        finally {
            if(rs!=null)  rs.close();
            if(st!=null)  st.close();
            poolManager.freeConnection("oracle"  ,conOracle);
            poolManager.freeConnection("fintra",conPostgres);
        }
        return Registros;
        
    }
    
    public List AgenciasClientesStandar(String agencia, String cliente, int Modo)throws Exception{
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        List              Lista = new LinkedList();
        
        PoolManager  poolManager = PoolManager.getInstance();
        Connection   conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try{
            st = conPostgres.prepareStatement(
            this.SQL_DATOS_STDJOB + ", " +
            (Modo==0 ? "A.STD_JOB_DESC" : "D.NOMCIU ,  E.NOMCIU ")
            );
            st.setString(1, agencia);
            st.setString(2, cliente);
            rs = st.executeQuery();
            while(rs.next()){
                DatosEstandar datos = new DatosEstandar();
                datos.setCodigoAgencia      (rs.getString(1));
                datos.setDescripcionAgencia(rs.getString(2));
                datos.setCodigoCliente      (rs.getString(3));
                datos.setDescripcionCliente(rs.getString(4));
                datos.setCodigoEstandar     (rs.getString(5));
                datos.setDescripcionEstandar(rs.getString(6));
                datos.setCodigoOrigen       (rs.getString(7));
                datos.setDescripcionOrigen  (rs.getString(8));
                datos.setCodigoDestino      (rs.getString(9));
                datos.setDescripcionDestino(rs.getString(10));
                Lista.add(datos);
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina AgenciasClientesStandar [DistribucionPresupuestoDAO]... \n"+e.getMessage());
        }
        finally {
            if(rs!=null)  rs.close();
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return Lista;
    }
    
    public Hashtable Tasa(String Ano) throws Exception{
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        Hashtable         Lista = InitTasa(Ano);
        
        PoolManager  poolManager = PoolManager.getInstance();
        Connection   conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try{
            st = conPostgres.prepareStatement(this.SQL_TASA);
            st.setString(1, Ano);
            rs = st.executeQuery();
            while(rs.next()){
                Beans datos = new Beans();
                datos.addValor("ANO"  ,rs.getString(1));
                datos.addValor("MES"  ,rs.getString(2));
                datos.addValor("DOL"  ,rs.getString(3));
                datos.addValor("BOL"  ,rs.getString(4));
                datos.addValor("DTF"  ,rs.getString(5));
                Lista.put(rs.getString(2), datos);
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina Tasa [DistribucionPresupuestoDAO]... \n"+e.getMessage());
        }
        finally {
            if(rs!=null)  rs.close();
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return Lista;
    }
    
    private Hashtable InitTasa(String Ano){
        Hashtable         Lista = new Hashtable();
        for (int i=1;i<=12;i++){
            Beans datos = new Beans();
            datos.addValor("ANO"  ,Ano);
            datos.addValor("MES"  ,(i<10?"0":"") + String.valueOf(i));
            datos.addValor("DOL"  ,"0.00");
            datos.addValor("BOL"  ,"0.00");
            datos.addValor("DTF"  ,"0.00");
            Lista.put((i<10?"0":"") + String.valueOf(i), datos);
        }
        return Lista;
    }
    
    
    public DatosView BuscarTarifaMoneda(DatosView datos, Connection conOracle) throws Exception{
        PoolManager       poolManager = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        boolean           liberar     = false;
        if (conOracle==null){
            poolManager = PoolManager.getInstance();
            conOracle   = poolManager.getConnection("oracle");
            liberar     = true;
        }
        if (conOracle == null)
            throw new SQLException("Sin conexion");
        try{
            datos.addValor("0","TARIFA"); //tarifa
            datos.addValor("" ,"MONEDA"); // moneda
            st = conOracle.prepareStatement(this.SQL_TAR_MON);
            st.setString(1, datos.getValor("UW"));
            rs = st.executeQuery();
            while(rs.next()){
                datos.addValor(rs.getString(1),"TARIFA"); //tarifa
                datos.addValor(rs.getString(2),"MONEDA"); // moneda
                break;
            }
        }catch(Exception e){
            throw new Exception("Error en la rutina BuscarTarifaMoneda [DistribucionPresupuestoDAO]...\n"+e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if (liberar) poolManager.freeConnection("oracle",conOracle);
        }
        return datos;
    }
    
    public DatosView BuscarVentas(int Tipo, String Ano, String Mes, DatosView datos,  Connection conPostgres) throws Exception{
        PoolManager       poolManager = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        boolean           liberar     = false;
        if (conPostgres==null){
            poolManager = PoolManager.getInstance();
            conPostgres    = poolManager.getConnection("fintra");
            liberar     = true;
        }
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try{
            /****************************************************************************************************************
             * MODO MENSUAL
             ****************************************************************************************************************/
            if (Tipo == 0){
                double total = 0;
                for (int i = (Mes.equals("TD")?1:Integer.parseInt(Mes)), j = 0;  i<= (Mes.equals("TD")?12:Integer.parseInt(Mes)) ; i++, j++){
                    datos.addValor("0", String.valueOf(j));          // valor actual
                    datos.addValor("0", "VP"   + String.valueOf(j)); // valor previo
                    datos.addValor("",  "UP"   + String.valueOf(j)); // usuario modificacion previa
                    datos.addValor("",  "FP"   + String.valueOf(j)); // fecha modificacion previa
                    datos.addValor("M", "TIPO" + String.valueOf(j));
                    /////////////////////////////////////////////////////////////////////////////////////
                    // VENTAS ACTUALES
                    st = conPostgres.prepareStatement(this.SQL_VENTASA.replaceAll("#RESTO#","" + ", TIPO "));
                    st.setString(1, datos.getValor("DISTRITO"));
                    st.setString(2, datos.getValor("STDJOBNO"));
                    st.setString(3, Ano );
                    st.setString(4, UtilFinanzas.DiaFormat(i) );
                    //System.out.println("SQL: "+st.toString());
                    rs = st.executeQuery();
                    while (rs.next()){
                        datos.addValor(rs.getString(1), String.valueOf(j));
                        datos.addValor(rs.getString(2), "TIPO" + String.valueOf(j));
                        total += rs.getDouble(1);
                        break;
                    }
                    datos.addValor(String.valueOf(total), "TOTAL");
                    /////////////////////////////////////////////////////////////////////////////////////
                    // VENTAS PREVIAS
                    st = conPostgres.prepareStatement(this.SQL_VENTASP.replaceAll("#RESTO#",""));
                    st.setString(1, datos.getValor("DISTRITO"));
                    st.setString(2, datos.getValor("STDJOBNO"));
                    st.setString(3, Ano );
                    st.setString(4, UtilFinanzas.DiaFormat(i) );
                    //System.out.println("SQL: "+st.toString());
                    rs = st.executeQuery();
                    while (rs.next()){
                        datos.addValor(rs.getString(1), "VP" + String.valueOf(j));
                        datos.addValor(rs.getString(2), "UP" + String.valueOf(j));
                        datos.addValor(rs.getString(3), "FP" + String.valueOf(j));
                        break;
                    }
                    //////////////////////////////////////////////////////////////////////////////////////
                }
            }else{
                /****************************************************************************************************************
                 * MODO SEMANAL Y DIARIA
                 ****************************************************************************************************************/
                String Cols = "";
                datos.addValor("0", "TOTAL");
                datos.addValor("",  "UP"); // usuario modificacion previa
                datos.addValor("",  "FP"); // fecha modificacion previa
                
                datos.addValor((Tipo==1?"S":"D"), "TIPO");
                for (int cols=1; cols<= (Tipo==1?5:31) ; cols++){
                    Cols += (Tipo==1? this.SQL_COLUMNA_SEMANAL : this.SQL_COLUMNA_DIARIA ).replaceAll("#INDICE#", (cols<10?"0":"")+cols );
                    datos.addValor("0", String.valueOf(cols-1)); // valor actual
                    datos.addValor("0", "VP"    + String.valueOf(cols-1)); // valor previo
                    datos.addValor("",  "UP"    + String.valueOf(cols-1)); // valor previo
                    datos.addValor("",  "FP"    + String.valueOf(cols-1)); // valor previo
                }
                Cols += ", TIPO ";
                //////////////////////////////////////////////////////////////////////////////////////////////
                // VENTAS ACTAULES
                st = conPostgres.prepareStatement(this.SQL_VENTASA.replaceAll("#RESTO#", Cols));
                st.setString(1, datos.getValor("DISTRITO"));
                st.setString(2, datos.getValor("STDJOBNO"));
                st.setString(3, Ano);
                st.setString(4, Mes);
                //System.out.println("SQL: "+ st.toString());
                rs = st.executeQuery();
                while (rs.next()){
                    datos.addValor(rs.getString(1), "TOTAL");
                    for (int cols=2; cols<= (Tipo==1?5:31)+1 ; cols++)
                        datos.addValor(rs.getString(cols), String.valueOf(cols-2));
                    datos.addDescriptor(rs.getString((Tipo==1?5:31) + 2), "TIPO");
                    break;
                }
                //////////////////////////////////////////////////////////////////////////////////////////////
                // VENTAS PREVIAS
                st = conPostgres.prepareStatement(this.SQL_VENTASP.replaceAll("#RESTO#", Cols));
                st.setString(1, datos.getValor("DISTRITO"));
                st.setString(2, datos.getValor("STDJOBNO"));
                st.setString(3, Ano);
                st.setString(4, Mes);
                //System.out.println("SQL: "+ st.toString());
                rs = st.executeQuery();
                while (rs.next()){
                    for (int cols=2; cols<= (Tipo==1?5:31)+1 ; cols++){
                        datos.addValor(rs.getString(cols), "VP" + String.valueOf(cols-2));
                        datos.addDescriptor(rs.getString((Tipo==1?5:31) + 3), "UP" + String.valueOf(cols-2));
                        datos.addDescriptor(rs.getString((Tipo==1?5:31) + 4), "FP" + String.valueOf(cols-2));
                    }
                    break;
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////
            }
        }catch(Exception e){
            throw new Exception("Error en la rutina BuscarVentas [DistribucionPresupuestoDAO]...\n"+e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if (liberar) poolManager.freeConnection("fintra",conPostgres);
        }
        return datos;
    }
    
    public List Historial(String Distrito, String Estandar, String Ano, String Mes, Connection conPostgres) throws Exception{
        PoolManager       poolManager = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        boolean           liberar     = false;
        
        List lista = new LinkedList();
        
        if (conPostgres==null){
            poolManager = PoolManager.getInstance();
            conPostgres    = poolManager.getConnection("fintra");
            liberar     = true;
        }
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try{
            String Cols = "";
            for (int cols=1; cols<= 31 ; cols++) Cols += SQL_COLUMNA_DIARIA.replaceAll("#INDICE#", UtilFinanzas.DiaFormat(cols));
            
            st = conPostgres.prepareStatement(this.SQL_HISTORIAL.replaceAll("#RESTO#", Cols));
            st.setString(1, Distrito);
            st.setString(2, Estandar);
            st.setString(3, Ano);
            st.setString(4, Mes);
            //System.out.println("SQL: "+ st.toString());
            rs = st.executeQuery();
            while (rs.next()){
                DatosView datos = new DatosView();
                for (int cols=2; cols<= 32 ; cols++)
                    datos.addValor(rs.getString(cols), String.valueOf(cols-2));
                datos.addDescriptor(rs.getString(33), "USUARIO");
                datos.addDescriptor(rs.getString(34), "FECHA");
                lista.add(datos);
            }
        }catch(Exception e){
            throw new Exception("Error en la rutina Historial [DistribucionPresupuestoDAO]...\n"+e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if (liberar) poolManager.freeConnection("fintra",conPostgres);
        }
        return lista;
    }
    
    /////////////////////////////////
    // tipo 0:nombre; 1:agencia due�a
    public String infoCliente(Connection conPostgres, String CodCliente, int tipo) throws SQLException {
        PoolManager       poolManager = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            infoCliente = "";
        boolean liberar   = false;
        if (conPostgres==null){
            poolManager = PoolManager.getInstance();
            conPostgres = poolManager.getConnection("fintra");
            liberar     = true;
        }
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            // BUSQUEDA FECHA CREACION
            st = conPostgres.prepareStatement(this.SQL_AGEN_CLIENTE);
            st.setString(1 ,  CodCliente);
            rs = st.executeQuery();
            if (rs.next()) infoCliente = rs.getString(tipo + 1);
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina infoCliente [DistribuicionPresupuestoDAO]... \n"+e.getMessage());
        }
        finally {
            if (rs!=null) rs.close();
            if (st!=null) st.close();
            if (liberar)  poolManager.freeConnection("fintra",conPostgres);
        }
        return infoCliente;
    }
    
    public void BORRAR_PTO(String Distrito, String Ano, String Mes)throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try{
            st = conPostgres.prepareStatement(this.SQL_BORRAR_PTO);
            st.setString(1, Distrito);
            st.setString(2, Ano     );
            st.setString(3, Mes     );
            st.executeUpdate();
        }
        catch(SQLException e){
            throw new SQLException("Error en la rutina BORRAR_PTO [DistribucionPresupuestoDAO] : \n" + e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    /**
     * Metodo buscarptoCarbon, busca los viajes planeados de carbon de acuerdo  un standar especifico
     * @autor : Diogenes Bastidas Morales.
     * @param : ano,mes,dia de las 2 fechas y distrito
     * @return: Vector pto carbon
     * @version : 1.0
     */
    public int[] buscarptoCarbon( String ano1, String mes1, String ano2, String mes2,String stdjob,String dstrct  )throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        int [] pto = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        
        
        String CDia="CDIA",a="",M1="",M2="",m="0";
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_PTOCARBON);
                st.setString(1,dstrct);
                st.setString(2, stdjob);
                st.setString(3,ano1+mes1);
                st.setString(4,ano2+mes2);
                //System.out.println("-- pto carbon -> "+st);
                rs = st.executeQuery();
                
                
                while (rs.next()){
                    for (int i = 1; i<=31; i++ ){  //recorro los campos de los dias de un registro
                        CDia = ( i < 10 )?CDia+"0"+i:CDia+i;
                        pto[i-1] = rs.getInt(CDia);
                        CDia="CDIA";
                    }
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA PTO_VENTA " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return pto;
    }
}

