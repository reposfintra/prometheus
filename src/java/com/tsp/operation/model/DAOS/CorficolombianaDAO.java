/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.DAOS;

import java.sql.*;
import java.util.*;

/**
 *
 * @author maltamiranda
 */
public class CorficolombianaDAO extends MainDAO{

    public CorficolombianaDAO(){
        super("CorficolombianaDAO.xml");
    }

    public ArrayList getReportes() throws Exception {
        Connection con=null;
        PreparedStatement st=null;
        String sql="";
        ResultSet rs=null;
        ArrayList arl=null;
        try{
            sql=this.obtenerSQL("SQL_GET_REPORTES");
            con=this.conectarJNDI("SQL_GET_REPORTES");
            st=con.prepareStatement(sql);
            rs=st.executeQuery();
            arl=new ArrayList();
            
            while(rs.next())
            {   arl.add(new String[]{rs.getString("id_reporte"),rs.getString("reporte")});
                
            }
        }
        catch(Exception e){
            e.printStackTrace();
            e.toString();
        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(st!=null){st.close();}
            if(rs!=null){rs.close();}
        }

        return arl;
    }

    /*public String rstotbl (ResultSet rs) throws Exception {
        String tabla="<table border=2>";
        while(rs.next()){
           tabla+="<tr>";
           for(int i=1;i<=rs.getMetaData().getColumnCount();i++)
           {    tabla+="<td>"+rs.getString(i)+"</td>";
           }
           tabla+="</tr>";
        }
        tabla+="</tabla>";
        return tabla;
    }*/

    public ArrayList rstotbl (ResultSet rs) throws Exception {
        ArrayList tabla=new ArrayList();
        while(rs.next()){
           ArrayList tabla2=new ArrayList();
           for(int i=1;i<=rs.getMetaData().getColumnCount();i++)
           {    tabla2.add(rs.getString(i));
           }
           tabla.add(tabla2);
        }
        return tabla;
    }
public ArrayList getConsulta(String id_reporte,String tipo, String periodo,String parametro)throws Exception{

        Connection con=null;
        PreparedStatement st=null;
        String sql="";
        ArrayList resultado=new ArrayList();
        ResultSet rs=null;
        String sql2="";
        try{
            sql=this.obtenerSQL("SQL_GET_CONSULTA");
            con=this.conectarJNDI("SQL_GET_CONSULTA");
            st=con.prepareStatement(sql);
            st.setString(1, id_reporte);
            rs=st.executeQuery();
            if(rs.next())
            {   sql2=rs.getString(tipo);
            }
            if(st!=null)
            {
                st.close();
            }
            if(rs!=null)
            {
                rs.close();
            }
            st=con.prepareStatement(sql2);
            st.setString(1, periodo);
            if(parametro!=null && !parametro.equals("") && !parametro.equals("GENERAR_REPORTE")){
                st.setString(2, parametro);
            }
            rs=st.executeQuery();
            resultado=this.rstotbl(rs);
        }
        catch(Exception e){
            e.printStackTrace();
            e.toString();
        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(st!=null){st.close();}
            if(rs!=null){rs.close();}
        }
        return resultado;

    }
}
