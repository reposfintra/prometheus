/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.AuditoriaVentasBeans;
import com.tsp.operation.model.beans.EDSPropietarioBeans;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.reporteColocacionBeans;
import java.util.ArrayList;

/**
 *
 * @author mariana
 */
public interface AdministracionEdsDAO {

    /**
     *
     * @param nombreprop
     * @param direccion
     * @param correo
     * @param telefono
     * @param pais
     * @param nit
     * @param ciudad
     * @param estadousu
     * @param idusuario
     * @param passw
     * @param cia
     * @param cambioclav
     * @param base
     * @param perfil
     * @return
     */
    public String guardarUsuario(String nombreprop, String direccion, String correo, String telefono, String pais, String nit, String ciudad, String estadousu, String idusuario, String passw, String cia, String cambioclav, String base, String perfil);

    /**
     *
     * @param nombreprop
     * @param direccion
     * @param correo
     * @param telefono
     * @param pais
     * @param ciudad
     * @param passw
     * @param usuariosedit
     * @return
     */
    public String actualizarUsuario(String nombreprop, String direccion, String correo, String telefono, String pais, String ciudad, String passw, String usuariosedit);

    /**
     *
     * @param cia
     * @param idusuario
     * @param usuario
     * @param base
     * @return
     */
    public String guardarUsuario(String cia, String idusuario, String usuario, String base);

    /**
     *
     * @param idusuario
     * @param usuario
     * @param idusuarioedit
     * @return
     */
    public String actualizarUsuarioProyecto(String idusuario, String usuario, String idusuarioedit);

    /**
     *
     * @param perfil
     * @param idusuario
     * @param usuario
     * @param cia
     * @return
     */
    public String guardarUsuarioPerfil(String perfil, String idusuario, String usuario, String cia);

    /**
     *
     * @param cia
     * @param nit
     * @param nombre
     * @param usuario
     * @param tipodoc
     * @param tipoper
     * @param gcontribuyente
     * @param aretenedor
     * @param auretefunete
     * @param auiva
     * @param auica
     * @param base
     * @param banco
     * @param sedepago
     * @param banagencia
     * @param tcuenta
     * @param ptransfer
     * @param bancotransfer
     * @param numcuenta
     * @param hc
     * @param suctransfer
     * @param regimen
     * @param nombre_cuenta
     * @param cedula_cuenta
     * @param digitover //digito de verificacion
     * @return
     */
    public String guardarProveedor(String cia, String nit, String nombre, String usuario, String tipodoc,
            String tipoper, String gcontribuyente, String aretenedor, String auretefunete, String auiva,
            String auica, String base, String banco, String sedepago, String banagencia, String tcuenta,
            String ptransfer, String bancotransfer, String numcuenta, String hc,
            String suctransfer, String regimen, String nombre_cuenta, String cedula_cuenta,String digitover);

    /**
     *
     * @param nomcliente
     * @param base
     * @param nit
     * @param cia
     * @param hc
     * @param direccion
     * @param telefono
     * @param nomcontacto
     * @param telcontacto
     * @param emailcontacto
     * @param dircontacto
     * @param rif
     * @param ciudad
     * @param pais
     * @param usuario
     * @return
     */
    public String guardarCliente(String nomcliente, String base, String nit, String cia, String hc, String direccion, String telefono, String nomcontacto, String telcontacto, String emailcontacto, String dircontacto, String rif, String ciudad, String pais, String usuario);

    /**
     *
     * @param idusuario
     * @param usuario
     * @param idusuarioedit
     * @return
     */
    public String actualizarUsuarioPerfil(String idusuario, String usuario, String idusuarioedit);

    /**
     *
     * @param cia
     * @param tipopersona
     * @param nitem
     * @param nombreprop
     * @param direccion
     * @param correo
     * @param nit
     * @param representante
     * @param usuario
     * @return
     */
    public String gPropietarioEds(String cia, String tipopersona, String nitem, String nombreprop, String direccion, String correo, String nit, String representante, String usuario);

    /**
     *
     * @param nombreprop
     * @param direccion
     * @param correo
     * @param nit
     * @param representante
     * @param usuario
     * @param id
     * @return
     */
    public String actualizarPropietarioEds(String nombreprop, String direccion, String correo, String nit, String representante, String usuario, String id);

    /**
     *
     * @param nombre
     * @param usuario
     * @param tipoper
     * @param gcontribuyente
     * @param aretenedor
     * @param auretefunete
     * @param auiva
     * @param auica
     * @param banco
     * @param sedepago
     * @param banagencia
     * @param tcuenta
     * @param ptransfer
     * @param bancotransfer
     * @param numcuenta
     * @param hc
     * @param nombre_cuenta
     * @param cedula_cuenta
     * @param suctransfer
     * @param nitedit
     * @param regimen
     * @return
     */
    public String actualizarProveedor(String nombre, String usuario, String gcontribuyente,
            String aretenedor, String auretefunete, String auiva, String auica, String banco,
            String sedepago, String banagencia, String tcuenta, String ptransfer, String bancotransfer,
            String numcuenta, String hc, String suctransfer, String nitedit, String regimen, String nombre_cuenta, String cedula_cuenta);

    public String actualizarProveedor(String nit, String nombre, String usuario);

    /**
     *
     * @param nomcliente
     * @param nit
     * @param hc
     * @param direccion
     * @param telefono
     * @param nomcontacto
     * @param telcontacto
     * @param emailcontacto
     * @param dircontacto
     * @param ciudad
     * @param pais
     * @param usuario
     * @param codcli
     * @return
     */
    public String actualizarCliente(String nomcliente, String hc, String direccion,
            String telefono, String nomcontacto, String telcontacto, String emailcontacto,
            String dircontacto, String ciudad, String pais, String usuario, String codcli);
    
    public String actualizarCliente(String nomcliente, String direccion, String telefono, String ciudad, String pais, String usuario,String codcli);

    /**
     *
     * @param id
     * @param usuario
     * @return
     */
    public String cambiarEstadoEds(String id, String usuario);

    /**
     *
     * @param id
     * @return
     */
    public String cambiarEstadoProducto(String id);

    /**
     *
     * @param propietario
     * @return
     */
    public String cargarComboEDS(String propietario);

    public String cargarComboEDS();

    public String cargarCompania();

    public String cargarPais();

    public String cargarBandera();

    /**
     *
     * @param ciudad
     * @param niteds
     * @param nombreds
     * @param idusuario
     * @param telefono
     * @param correo
     * @param direccion
     * @param usuario
     * @param id
     * @return
     */
    public String actualizarEds(String nombreds, String niteds, String ciudad, String direccion, String telefono, String correo, String idusuario, String usuario, String id);

    /**
     *
     * @param edsusuario
     * @return
     */
    public String verificarUsuario(String edsusuario);
    
      public String verificarusuarioEDS(String nit);
     
      //public String verificarusuarioPrp(String nitem);

    /**
     *
     * @param nitem
     * @param tipodocumento
     * @return
     */
          public ArrayList<EDSPropietarioBeans> verificarusuarioPrp(String nitem,String tipodocumento);
    /**
     *
     * @param usuario
     * @param tipo
     * @param user
     * @return
     */
    public String cargarPropietario(String usuario, String tipo, Usuario user);

    /**
     *
     * @param pais
     * @return
     */
    public String cargarDepartamento(String pais);

    /**
     *
     * @param departamento
     * @return
     */
    public String cargarCiudad(String departamento);

    public String cargarproductosEds();

    public String cargarUnidadMedidaPrp();

    public ArrayList<EDSPropietarioBeans> cargarGrillaProductosEDS();

    /**
     *
     * @param usuario
     * @param tipousu
     * @param user
     * @return
     */
    public ArrayList<EDSPropietarioBeans> cargarEDS(String usuario, String tipousu);

    public ArrayList<EDSPropietarioBeans> cargarPropietarioEds();

    /**
     *
     * @param idpropietario
     * @param idbandera
     * @param nombreds
     * @param niteds
     * @param municipio
     * @param direccion
     * @param telefono
     * @param correo
     * @param estadouser
     * @param idusuario
     * @param createusuario
     * @return
     */
    public String guardarEds(String idpropietario, String idbandera, String nombreds, String niteds, String municipio, String direccion, String telefono, String correo, String estadouser, String idusuario, String createusuario);

    /**
     *
     * @param id
     * @return
     */
    public ArrayList<EDSPropietarioBeans> cargarEds(String id);

    /**
     *
     * @param cia
     * @param nombre_producto
     * @param unidadmed
     * @return
     */
    public String guardarProductosEds(String cia, String nombre_producto, String unidadmed);

    /**
     *
     * @param descripcion
     * @param uniMedida
     * @param id
     * @return
     */
    public String actualizarProducto(String descripcion, String uniMedida, String id);

    public ArrayList<EDSPropietarioBeans> cargarGrillaBanderas();

    /**
     *
     * @param id
     * @return
     */
    public String cambiarEstadoBandera(String id);

    /**
     *
     * @param cia
     * @param razonspcial
     * @param nit
     * @param tipoper
     * @param direccion
     * @param correo
     * @param replegal
     * @param docreplegal
     * @param usuario
     * @return
     */
    public String guardarBanderas(String cia, String razonspcial, String nit, String tipoper, String direccion, String correo, String replegal, String docreplegal, String usuario);

    /**
     *
     * @param razonspcial
     * @param nit
     * @param tipoper
     * @param direccion
     * @param correo
     * @param replegal
     * @param docreplegal
     * @param usuario
     * @param idbandera
     * @return
     */
    public String actualizarBanderas(String razonspcial, String nit, String tipoper, String direccion, String correo, String replegal, String docreplegal, String usuario, String idbandera);

    /**
     *
     * @param id
     * @return
     */
    public ArrayList<EDSPropietarioBeans> cargarProductosRelEDS(String id);

    public ArrayList<EDSPropietarioBeans> cargarGrillaUnidadMedida();

    /**
     *
     * @param cia
     * @param nomunmed
     * @param medicion
     * @return
     */
    public String guardarUnidadMedida(String cia, String nomunmed, String medicion);

    /**
     *
     * @param nomunmed
     * @param medicion
     * @param idum
     * @return
     */
    public String actualizarUnidadMedida(String nomunmed, String medicion, String idum);

    /**
     *
     * @param id
     * @return
     */
    public String cambiarEstadoUnidadMedida(String id);

    public String cargarBanco();

    /**
     *
     * @param banco
     * @return
     */
    public String cargarAgencia(String banco);

    public String cargarSedePago();

    public String cargarBancotransfer();

    public String cargarHC();

    public String cargarSucursal();

    public String cargarFechaCXPEds(String eds);

    public ArrayList<EDSPropietarioBeans> CXPEds(String eds, String fechainicio, String fechafin, String cxp);

    public ArrayList<EDSPropietarioBeans> CXPEds_detalle(String cxp);

    public ArrayList<AuditoriaVentasBeans> auditoriaVentas(String eds, String fechainicio, String fechafin, String planilla);

    public ArrayList<AuditoriaVentasBeans> auditoriaVentas(String numventa);

    /**
     *
     * @param cxp
     * @param numero_nota
     * @param creation_date
     * @param id_eds
     * @return
     */
    public ArrayList<EDSPropietarioBeans> cargarNotaCredito(String cxp, String numero_nota,String creation_date,int id_eds);
    
    public ArrayList<reporteColocacionBeans> reporteColocacion(String planilla, String fechainicio, String fechafin,String eds);
}
