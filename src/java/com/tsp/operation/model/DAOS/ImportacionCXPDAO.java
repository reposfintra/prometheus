/**********************************************************
 * Nombre:        ImportacionCXPDAO.java                  *
 * Descripci�n:   Accesso a la base de datos              *
 * Autor:         Ing. Mario Fontalvo                     *
 * Fecha:         24 de octubre de 2005, 02:16 PM         *
 * Versi�n:       Java  1.0                               *
 * Copyright:     Fintravalores S.A. S.A.            *
 **********************************************************/

package com.tsp.operation.model.DAOS;


import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.Util;

import java.sql.*;
import java.util.*;

public class ImportacionCXPDAO extends MainDAO {
    
    
    /** Creates a new instance of ImportacionCXP */
    public ImportacionCXPDAO() {
        super ("ImportacionCXPDAO.xml");
    }
    public ImportacionCXPDAO(String dataBaseName) {
        super ("ImportacionCXPDAO.xml", dataBaseName);
    }
    
    
    /** busca el valor de la tasa en una fecha determinada */
    
    public double searchTasa(String cia, String fecha, String MonBase, String MonConv) throws SQLException {
        
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        double            tasa        = 0;
        try {
            st = this.crearPreparedStatement("SQL_SEARCH_TASA");
            st.setString( 1 , MonBase );
            st.setString( 2 , MonConv );
            st.setString( 3 , fecha.substring(0,10));
            st.setString( 4 , cia   );            
            rs = st.executeQuery();
            if (rs.next()){
                tasa = rs.getDouble("tasa");
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina searchTasa [ImportacionCXPDAO]... \n"+e.getMessage());
        }
        finally {
            if (rs!=null)  rs.close();
            if (st!=null)  st.close();
            this.desconectar("SQL_SEARCH_TASA");
        }
        return tasa;
    }
    

    
    /** busca el valor de la tasa en una fecha determinada */
    
    public Tipo_impuesto searchValorImpuesto(String dstrct, String codigoImpuesto, String concepto, String tipo, String fecha_documento, String agencia) throws SQLException {
        
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        Tipo_impuesto     imp   = null;
        try {
            st = this.crearPreparedStatement("SQL_SEARCH_VALOR_IMPUESTO");
            st.setString( 1 , dstrct          );
            st.setString( 2 , codigoImpuesto  );
            st.setString( 3 , concepto        );
            st.setString( 4 , tipo            );
            st.setString( 5 , fecha_documento );
            st.setString( 6 , agencia         );
            rs = st.executeQuery();
            if (rs.next()){
                imp = new Tipo_impuesto();
                imp.setCodigo_impuesto( rs.getString("codigo_impuesto"));
                imp.setPorcentaje1    ( rs.getDouble("porcentaje1")  );
                imp.setInd_signo      ( rs.getInt   ("ind_signo")    );
                imp.setTipo_impuesto  ( rs.getString("tipo_impuesto"));
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina searchValorImpuesto [ImportacionCXPDAO]... \n"+e.getMessage());
        }
        finally {
            if (rs!=null)  rs.close();
            if (st!=null)  st.close();
            this.desconectar("SQL_SEARCH_VALOR_IMPUESTO");
        }
        return imp;
    }
    
    /** busca el valor de la tasa en una fecha determinada */
    
    public boolean searchPlanilla(String planilla) throws SQLException {
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        boolean           existe = false;
        try {
            st = this.crearPreparedStatement("SQL_SEARCH_PLANILLA");
            st.setString( 1 , planilla  );
            rs = st.executeQuery();
            if (rs.next()){
                existe = true;
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina searchPlanilla [ImportacionCXPDAO]... \n"+e.getMessage());
        }
        finally {
            if (rs!=null)  rs.close();
            if (st!=null)  st.close();
            this.desconectar("SQL_SEARCH_PLANILLA");
        }
        return existe;
    }
    
    
    /** busca general el tipo de documento */
    
    public TreeMap searchTipoDocumento() throws SQLException {
        
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        TreeMap           documento   = new TreeMap();
        try {
            st = this.crearPreparedStatement("SQL_SEARCH_TBLDOC");
            rs = st.executeQuery();
            while (rs.next()){
                documento.put( rs.getString(1), rs.getString(2) );
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina searchTipoDocumento [ImportacionCXPDAO]... \n"+e.getMessage());
        }
        finally {
            if (rs!=null)  rs.close();
            if (st!=null)  st.close();
            this.desconectar("SQL_SEARCH_TBLDOC");
        }
        return documento;
    }
    
    
    
    /** verifica la existencia de la agencia */
    
    public boolean searchAgencia(String id_agencia) throws SQLException {
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        boolean           existe      = false;
        try {
            st = this.crearPreparedStatement("SQL_SEARCH_AGENCIA");
            st.setString( 1 , id_agencia    );
            rs = st.executeQuery();
            if (rs.next()){
                existe = true;
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina searchAgencia [ImportacionCXPDAO]... \n"+e.getMessage());
        }
        finally {
            if (rs!=null)  rs.close();
            if (st!=null)  st.close();
            this.desconectar("SQL_SEARCH_AGENCIA");
        }
        return existe;
    }
    
    
    
    /** verifica la existencia de banco-sucursal */
    
    public Banco searchBanco(String Distrito, String Banco, String Sucursal) throws SQLException {
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        Banco             banco       = null;
        try {
            st = this.crearPreparedStatement("SQL_SEARCH_BANCO");
            st.setString( 1 , Distrito );
            st.setString( 2 , Banco    );
            st.setString( 3 , Sucursal );
            rs = st.executeQuery();
            if (rs.next()){
                banco = new Banco ();
                banco.setCodigo_Agencia( rs.getString("agency_id"));
                banco.setMoneda( rs.getString("currency"));
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina searchBanco [ImportacionCXPDAO]... \n"+e.getMessage());
        }
        finally {
            if (rs!=null)  rs.close();
            if (st!=null)  st.close();
            this.desconectar("SQL_SEARCH_BANCO");
        }
        return banco;
    }
    
    
    /** complemento y validacion de datos Proveedor */
    
    public Proveedor searchDatosProveedor(String dstrct, String proveedor) throws SQLException {
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        Proveedor         datos    = null;
        try {
            st = this.crearPreparedStatement("SQL_SEARCH_PROVEEDOR");
            st.setString( 1 , dstrct    );
            st.setString( 2 , proveedor );
            rs = st.executeQuery();
            if (rs.next()){
                datos = new Proveedor();
                datos.setC_branch_code       (rs.getString("branch_code"));
                datos.setC_bank_account      (rs.getString("bank_account_no"));
                datos.setC_hc                (rs.getString("hc"));
                datos.setC_idMims            (rs.getString("id_mims"));
                datos.setC_agency_id         (rs.getString("agency_id"));
                datos.setC_currency_bank     (rs.getString("currency"));
                datos.setC_agente_retenedor  (rs.getString("agente_retenedor"));
                datos.setC_autoretenedor_iva (rs.getString("autoret_rfte"));
                datos.setC_autoretenedor_ica (rs.getString("autoret_iva"));
                datos.setC_autoretenedor_rfte(rs.getString("autoret_ica"));
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina searchDatosProveedor [ImportacionCXPDAO]... \n"+e.getMessage());
        }
        finally {
            if (rs!=null)  rs.close();
            if (st!=null)  st.close();
            this.desconectar("SQL_SEARCH_PROVEEDOR");
        }
        return datos;
    }
    
    
    /** busca el valor de las monedas de las compa�ias */
    
    public TreeMap searchMonedasCias() throws SQLException {
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        TreeMap           monedas= new TreeMap();
        try {
            st = this.crearPreparedStatement("SQL_SEARCH_MON_CIA");
            rs = st.executeQuery();
            while (rs.next()){
                CIA cia = new CIA();
                cia.setDistrito(rs.getString("dstrct"));
                cia.setMoneda  (rs.getString("moneda"));
                cia.setBase    (rs.getString("base"));
                monedas.put    (rs.getString("dstrct"),  cia );
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina searchMonedasCias [ImportacionCXPDAO]... \n"+e.getMessage());
        }
        finally {
            if (rs!=null)  rs.close();
            if (st!=null)  st.close();
            this.desconectar("SQL_SEARCH_MON_CIA");
        }
        return monedas;
    }
    
    
    /** verificacion de exiastencia de una factura */
    
    public CXP_Doc searchCXPDoc(String Distrito, String Proveedor, String TipoDocumento, String Documento) throws SQLException {
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        CXP_Doc           documento   = null; 
        boolean           existe      = false;
        try {
            st = this.crearPreparedStatement("SQL_SEARCH_CXP_DOC");
            st.setString( 1 , Distrito      );
            st.setString( 2 , Proveedor     );
            st.setString( 3 , TipoDocumento );
            st.setString( 4 , Documento     );
            rs = st.executeQuery();
            if (rs.next()){
                documento = new CXP_Doc();
                documento.setDstrct         ( rs.getString("dstrct") );
                documento.setProveedor      ( rs.getString("proveedor") );
                documento.setTipo_documento ( rs.getString("tipo_documento") );
                documento.setDocumento      ( rs.getString("documento") );
                documento.setVlr_saldo_me   ( rs.getDouble("vlr_saldo_me") );
                documento.setClase_documento( rs.getString("clase_documento") );
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina searchCXPDoc [ImportacionCXPDAO]... \n"+e.getMessage());
        }
        finally {
            if (rs!=null)  rs.close();
            if (st!=null)  st.close();
            this.desconectar("SQL_SEARCH_CXP_DOC");
        }
        return documento;
    }
    
    
    /** busca el valor de las monedas de las compa�ias */
    
    public List searchFacturasPendientes(String usuario) throws SQLException {
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        List              facturas     = new LinkedList();
        try {
            st = this.crearPreparedStatement("SQL_SEARCH_IMPORT");
            st.setString(1, usuario);
            rs = st.executeQuery();
            while (rs.next()){
                facturas.add( CXPImportacion.load(rs));
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina searchFacturasPendientes [ImportacionCXPDAO]... \n"+e.getMessage());
        }
        finally {
            if (rs!=null)  rs.close();
            if (st!=null)  st.close();
            this.desconectar("SQL_SEARCH_IMPORT");
        }
        return facturas;
    }
    
    
    /** Actualizacion de la factura migrada */
    
    public void updateFacturasMigrada(CXP_Doc factura) throws SQLException {
        PreparedStatement st          = null;
        try {
            st = this.crearPreparedStatement("SQL_UPDATE");
            st.setString(1, factura.getDstrct() );
            st.setString(2, factura.getProveedor() );
            st.setString(3, factura.getTipo_documento() );
            st.setString(4, factura.getDocumento() );
            st.setString(5, factura.getCreation_user() );
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina updateFacturasMigrada [ImportacionCXPDAO]... \n"+e.getMessage());
        }
        finally {
            if (st!=null)  st.close();
            this.desconectar("SQL_UPDATE");
        }
    }    
    
    
    
    
    /** de la cuenta y del auxiliar asignado */
    public String searchCuenta(String dstrct, String cuenta, String tipoauxiliar, String auxiliar) throws SQLException {
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            msg         = "Cuenta no encontrada....";
        try {
            st = this.crearPreparedStatement("SQL_CUENTA");
            st.setString( 1 , tipoauxiliar  );
            st.setString( 2 , auxiliar      );
            st.setString( 3 , dstrct        );
            st.setString( 4 , cuenta        );      
            rs = st.executeQuery();
            if (rs.next()){
                
                if ( rs.getString("detalle").equals("N"))
                    msg = "Se requiere una cuenta de detalle, la que esta ingresando no es valida";
                else if ( rs.getString("tiene_auxiliar").equals("NOT-FOUND") && rs.getString("subledger").equals("S") )
                    msg = "Tipo auxiliar requerido, o el que asigno no esta relacionado a la cuenta";
                else if ( (!rs.getString("tiene_auxiliar").equals("NOT-FOUND") || !auxiliar.equals("") ) && rs.getString("subledger").equals("N") )
                    msg = "No se requiere auxiliar";
                else
                    msg = "OK";
            }
        }
        catch(Exception e) {
            throw new SQLException("Error en rutina searchCuenta [ImportacionCXPDAO]... \n"+e.getMessage());
        }
        finally {
            if (rs!=null)  rs.close();
            if (st!=null)  st.close();
            this.desconectar("SQL_CUENTA");
        }
        return msg;
    }
    
    
    
    /** busca el codigo ABC */
    public boolean searchABC(String abc) throws SQLException {
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        boolean           existe = false;
        try {
            st = this.crearPreparedStatement("SQL_ABC");
            st.setString( 1 , abc  );
            rs = st.executeQuery();
            if (rs.next()){
                existe = true;
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina searchABC [ImportacionCXPDAO]... \n"+e.getMessage());
        }
        finally {
            if (rs!=null)  rs.close();
            if (st!=null)  st.close();
            this.desconectar("SQL_ABC");
        }
        return existe;
    }    
    
    
    /** busca el codigo ABC */
    public boolean searchHC(String hc) throws SQLException {
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        boolean           existe = false;
        try {
            st = this.crearPreparedStatement("SQL_HANDLE_CODE");
            st.setString( 1 , hc  );
            rs = st.executeQuery();
            if (rs.next()){
                existe = true;
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina searchHC [ImportacionCXPDAO]... \n"+e.getMessage());
        }
        finally {
            if (rs!=null)  rs.close();
            if (st!=null)  st.close();
            this.desconectar("SQL_HANDLE_CODE");
        }
        return existe;
    } 
    
    
    /** busca el codigo ABC */
    public boolean searchAutorizador(String usuario) throws SQLException {
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        boolean           existe = false;
        try {
            st = this.crearPreparedStatement("SQL_AUTORIZADOR");
            st.setString( 1 , usuario  );
            rs = st.executeQuery();
            if (rs.next()){
                existe = true;
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina searchAutorizador [ImportacionCXPDAO]... \n"+e.getMessage());
        }
        finally {
            if (rs!=null)  rs.close();
            if (st!=null)  st.close();
            this.desconectar("SQL_AUTORIZADOR");
        }
        return existe;
    }     
    /**
     * verificacion de existencia de una factura 
     * @autor mfontalvo
     * @throws Exception.
     * @return verdadero si la factura exites en RXP si no falso
     */
    public boolean searchRXPDoc(String Distrito, String Proveedor, String TipoDocumento, String Documento) throws SQLException {
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        boolean           existe      = false;
        try {
            st = this.crearPreparedStatement("SQL_SEARCH_RXP_DOC");
            st.setString( 1 , Distrito      );
            st.setString( 2 , Proveedor     );
            st.setString( 3 , TipoDocumento );
            st.setString( 4 , Documento     );
            rs = st.executeQuery();
            if (rs.next()){
                existe = true;
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina searchRXPDoc [ImportacionCXPDAO]... \n"+e.getMessage());
        }
        finally {
            if (rs!=null)  rs.close();
            if (st!=null)  st.close();
            this.desconectar("SQL_SEARCH_RXP_DOC");
        }
        return existe;
    }    
    
    /**
     * Metodo q busca las facturas recurrentes para la migracion
     * @autor mfontalvo
     * @param usuario, usuario de migracion
     * @throws Exception.
     * @List lista de facturas a migrar.
     */
    
    public List searchFacturasPendientesRXP(String usuario) throws SQLException {
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        List              facturas     = new LinkedList();
        try {
            st = this.crearPreparedStatement("SQL_SEARCH_IMPORT_RXP");
            st.setString(1, usuario);
            rs = st.executeQuery();
            while (rs.next()){
                facturas.add( CXPImportacion.loadRXP(rs));
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina searchFacturasPendientesRXP [ImportacionCXPDAO]... \n"+e.getMessage());
        }
        finally {
            if (rs!=null)  rs.close();
            if (st!=null)  st.close();
            this.desconectar("SQL_SEARCH_IMPORT_RXP");
        }
        return facturas;
    }
    
    
}
