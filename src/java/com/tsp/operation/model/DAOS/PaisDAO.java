/*
 * PaisDAO.java
 *
 * Created on 24 de febrero de 2005, 09:00 AM
 */

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;



/**
 *
 * @author  DIBASMO
 */
public class PaisDAO extends MainDAO{
          
    public PaisDAO() {
        super("PaisDAO.xml");//JJCastro fase2
    }
          
    public PaisDAO(String dataBaseName) {
        super("PaisDAO.xml", dataBaseName);//JJCastro fase2
    }
    
    private Pais pais;
    private List Paises;
    private Vector VecPais; 
    private static final  String Select_Consulta = "select * from pais where reg_status = '' order by country_name";
    private TreeMap listpais;
    
    public void setPais(Pais pais){
        
        this.pais = pais;
        
    }
    
    // insertar pais en BD
    public void insertarPais() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_INSERT_PAIS";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, pais.getCountry_code());
                st.setString(2, pais.getCountry_name());
                st.setString(3, pais.getUser_update());
                st.setString(4, pais.getCreation_user());
                
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL INSERTAR EL PAIS" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


    // retorna true si el pais exito y false de lo contrario
/**
 * 
 * @param cod
 * @return
 * @throws SQLException
 */
    public boolean existPais (String cod) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        boolean sw = false;
        String query = "SQL_EXISTE_PAIS";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cod);
                rs = st.executeQuery();
                if (rs.next()){
                    sw = true; 
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR EL PAIS" + e.getMessage() + "" + e.getErrorCode() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }



   /**
    *
    * @param cod
    * @return
    * @throws SQLException
    */
    public boolean existPaisAnulado (String cod) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        boolean sw = false;
        String query = "SQL_EXISTE_ANULADO";//JJCastro fase2

        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cod);
                rs = st.executeQuery();
                
                if (rs.next()){
                    sw = true; 
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR EL PAIS" + e.getMessage() + "" + e.getErrorCode() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }


/**
 *
 * @param nom
 * @return
 * @throws SQLException
 */
    public boolean existPaisnom (String nom) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        boolean sw = false;
        String query = "SQL_EXISTE_PAIS_NOMBRE";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, nom);
                rs = st.executeQuery();
                
                if (rs.next()){
                    sw = true; 
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR EL PAIS" + e.getMessage() + "" + e.getErrorCode() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
    //obtiene la lista de paises
    public List obtenerpaises (){
        return Paises;
    }
    public Vector obtpaises (){
        return VecPais;
    }



    //carga a la lista los paises de  BD
/**
 * 
 * @throws SQLException
 */
    public void listarpaises ()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        Paises = null;
        String query = "Select_Consulta";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                
                Paises =  new LinkedList();
                
                while (rs.next()){
                    Paises.add(Pais.load(rs));
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }



    public Pais obtenerpais()throws SQLException{
        return pais;
    }


    

    public void paises ()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        VecPais = null;
        String query = "Select_Consulta";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                
                VecPais =  new Vector();
                
                while (rs.next()){
                    VecPais.add(Pais.load(rs));
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }


    // retorna el objeto pais de pendiendo el codigo
/**
 * 
 * @param cod
 * @throws SQLException
 */
    public void buscarpais (String cod) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        boolean sw = false;
        String query = "SQL_BUSCAR_PAIS";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cod);
                rs = st.executeQuery();
                
                if (rs.next()){
                   pais = Pais.load(rs);                                     
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR EL PAIS" + e.getMessage() + "" + e.getErrorCode() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }        
    }
    

/**
 * 
 * @param nom
 * @throws SQLException
 */
    public void buscarpaisnombre (String nom) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        boolean sw = false;
        VecPais = null;
        String query = "SQL_PAIS_NOMBRE";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, nom);
                rs = st.executeQuery();
                
                VecPais =  new Vector();
                
                while (rs.next()){
                    VecPais.add(Pais.load(rs));
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR EL PAIS" + e.getMessage() + "" + e.getErrorCode() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }


    //modifica el nombre,fecha de ult. actualizacion y el usuario (todo del pais)
/**
 * 
 * @throws SQLException
 */
    public void modificarpais () throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        String query = "SQL_MODIFICAR_PAIS";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, pais.getCountry_name());
                st.setString(2, pais.getUser_update());
                st.setString(3, pais.getCountry_code());
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL MODIFICAR EL PAIS" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }



/**
 * 
 * @throws SQLException
 */
    public void Activarpais () throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        String query = "SQL_ACTIVAR_PAIS";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, pais.getUser_update());
                st.setString(2, pais.getCountry_code());
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL MODIFICAR EL PAIS" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    

/**
 * 
 * @throws SQLException
 */
    public void anularpais () throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        String query = "SQL_ANULAR_PAIS";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, pais.getUser_update());
                st.setString(2, pais.getCountry_code());
                
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL ANULAR EL PAIS" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }

/**
 * 
 * @return
 * @throws SQLException
 */
     public boolean existePaises()throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
         String query = "SQL_EXISTE_PAIS_GENERAL";//JJCastro fase2

         try {
             con = this.conectarJNDI(query);
             if (con != null) {
                 st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS PAISES " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
      /**
     *Metodo que busca la lista de paises y los almacena en un treemap
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void listarPaises()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "Select_Consulta";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                
                listpais =  new TreeMap();
                
                listpais.put("Seleccione Algun item","");
                while (rs.next()){
                    listpais.put(rs.getString("country_name"),rs.getString("country_code"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL TREEMAP DE LOS PAISES " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    /**
     * Getter for property listpais.
     * @return Value of property listpais.
     */
    public java.util.TreeMap getListpais() {
        return listpais;
    }
    
    /**
     * Setter for property listpais.
     * @param listpais New value of property listpais.
     */
    public void setListpais(java.util.TreeMap listpais) {
        this.listpais = listpais;
    }
}
