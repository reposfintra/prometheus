/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.Asesores;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.StringStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author ivargas
 * 21/12/2011
 */
public class AsesoresDAO extends MainDAO {

    public AsesoresDAO() {
        super("AsesoresDAO.xml");
    }
    public AsesoresDAO(String dataBaseName) {
        super("AsesoresDAO.xml", dataBaseName);
    }

    /**
     * Busca listado de asesores
     * @return listado de asesores
     * @throws Exception cuando hay error
     */
    public ArrayList<Asesores> buscarAsesoresActivos(String convenio) throws Exception {
        ArrayList<Asesores> lista = new ArrayList<Asesores>();

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_ASESORES_ACTIVOS";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, convenio);
            rs = ps.executeQuery();
            while (rs.next()) {
                Asesores asesor = new Asesores();
                asesor.setNombre(rs.getString("nombre"));
                asesor.setIdusuario(rs.getString("idusuario"));
                asesor.setIdentificacion(rs.getString("nit"));
                lista.add(asesor);
            }
        } catch (Exception e) {
            System.out.println("Error en buscarAsesoresActivos: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error buscarAsesoresActivos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }
    
    public ArrayList<Asesores> cargarAsesoresConvenio( String convenio) {
        ArrayList cadena = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_ASESORES_ACTIVOS";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, convenio);
            rs = ps.executeQuery();
            while (rs.next()) {
                cadena.add(rs.getString("idusuario") + ";_;" + rs.getString("nombre"));
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return  cadena;

    }

    /**
     * Busca listado de asesores
     * @return listado de asesores
     * @throws Exception cuando hay error
     */
    public ArrayList<Asesores> buscarAsesores() throws Exception {
        ArrayList<Asesores> lista = new ArrayList<Asesores>();

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_ASESORES";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Asesores asesor = new Asesores();
                asesor = asesor.load(rs);
                lista.add(asesor);
            }
        } catch (Exception e) {
            System.out.println("Error en buscarAsesores: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error buscarAsesores: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    /**
     * Sql para insertar asesor
     * @return sql
     * @throws Exception cuando hay error
     */
    public String insertAsesor(Asesores asesor) {
        String cadena = "";
        String query = "INS_ASESOR";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, asesor.getIdusuario());
            st.setString(2, asesor.getCreationUser());
            st.setString(3, asesor.getDstrct());
            cadena = st.getSql();
            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql insertAsesor: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    /**
     * Sql para actualizar asesor
     * @return sql
     * @throws Exception cuando hay error
     */
    public String updateAsesor(Asesores asesor) {
        String cadena = "";
        String query = "UPD_ASESOR";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, asesor.getRegStatus());
            st.setString(2, asesor.getCreationUser());
            st.setString(3, asesor.getIdusuario());
            cadena = st.getSql();
            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql updateAsesor: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    /**
     * Inserta y actualiza listado de asesores
     * @throws Exception cuando hay error
     */
    public void grabarAsesores(ArrayList<Asesores> listamod, ArrayList<Asesores> listains) throws Exception {
        Connection con = null;
        Statement stmt = null;
        String query = "UPD_ASESOR";
        con = conectarJNDI(query);
        con.setAutoCommit(false);
        stmt = con.createStatement();
        try {

            for (int i = 0; i < listamod.size(); i++) {
                stmt.addBatch("\n" + this.updateAsesor(listamod.get(i)));
            }
            for (int i = 0; i < listains.size(); i++) {
                stmt.addBatch("\n" + this.insertAsesor(listains.get(i)));
            }

            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();
        } catch (Exception e) {
            System.out.println("Error en grabarAsesores: " + e.toString());
            con.rollback();
            if (e instanceof SQLException) {
                throw new SQLException("ERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS:\n" + e.getMessage() + "La siguiente exception es : ----> " + ((SQLException) e).getNextException());
            } else {
                e.printStackTrace();
            }
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
    }

     /**
     * M�todo que trae los negocios de un asesor
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public ArrayList<BeanGeneral> negociosAsesor(String asesor) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SRC_NEGOCIOS_ASESORES";
        ArrayList<BeanGeneral> beans = new ArrayList<BeanGeneral>();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                String sql = this.obtenerSQL(query);
                st = con.prepareStatement(sql);
                st.setString(1, asesor);
                rs = st.executeQuery();

                while (rs.next()) {
                    BeanGeneral bean = new BeanGeneral();
                    bean.setValor_01(rs.getString("cod_cli"));
                    bean.setValor_02(rs.getString("cliente"));
                    bean.setValor_03(rs.getString("f_desem"));                   
                    bean.setValor_04(rs.getString("cod_neg"));
                    bean.setValor_05(rs.getString("vr_negocio"));
                    bean.setValor_06(rs.getString("saldo"));
                    bean.setValor_07(rs.getString("numero_solicitud"));
                    beans.add(bean);
                }
            }

        } catch (Exception e) {
            System.out.println("error en negociosAsesor() " + e.toString() + "__" + e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return beans;

    }

    /**
     * M�todo que trae el query para la actualizacion del asesor al formulario
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public String updateAsesorFormulario(String formulario, String asesor) throws SQLException {
        String query = "SQL_UPD_FORM_ASESOR";
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, asesor);
            st.setString(2, formulario);
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en updateAsesorFormulario \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public ArrayList<Asesores> cargarAsesores() throws Exception {
        ArrayList<Asesores> lista = new ArrayList<Asesores>();

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_ASESORES";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Asesores asesor = new Asesores();
                asesor.setNombre(rs.getString("asesor"));
                asesor.setIdusuario(rs.getString("idusuario"));
                //asesor.setIdentificacion(rs.getString("nit"));
                lista.add(asesor);
            }
        } catch (Exception e) {
            System.out.println("Error en buscarAsesores: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error buscarAsesores: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }
    
 
        
        
    }

