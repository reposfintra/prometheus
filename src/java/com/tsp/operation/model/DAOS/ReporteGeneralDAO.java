/************************************************************************
 * Nombre clase: ReporteGeneralDAO.java                                 *
 * Descripci�n: Clase que maneja las consultas de los reportes.         *
 * Autor: Ing. Jose de la rosa                                          *
 * Fecha: 15 de junio de 2006, 05:54 PM                                 *
 * Versi�n: Java 1.0                                                    *
 * Copyright: Fintravalores S.A. S.A.                              *
 ***********************************************************************/

package com.tsp.operation.model.DAOS;

import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;
import java.util.Date;
import java.text.*;
import com.tsp.util.connectionpool.PoolManager;
import java.io.*;

public class ReporteGeneralDAO extends MainDAO{
    private ReporteDiferenciaTiempos reporte;
    public Vector VecReporteGeneral = new Vector();
    private Vector vector;
    private Vector vectorPlanilla;
    private Vector vectorPlaca;
    
    /** Creates a new instance of ReporteGeneralDAO */
    public ReporteGeneralDAO () {
        super ( "ReporteGeneralDAO.xml" );
    }

    /**
     * Metodo: consultaDiferenciaViajes, permite generar el reporte de diferencia de tiempos de carge y descarge de un cliente en un intervalo de tiempo.
     * @autor : Ing. Jose de la rosa
     * @param : fecha de inicio, fecha fin y cliente
     * @version : 1.0
     */
    public Vector consultaDiferenciaViajes (String fecha_ini, String fecha_fin, String cliente) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = crearPreparedStatement ("SQL_CONSULTA_DIFERENCIA");
            ps.setString (1, fecha_ini +" 00:00%");
            ps.setString (2, fecha_fin +" 23:59%");
            ps.setString (3, cliente);
            rs = ps.executeQuery ();
            VecReporteGeneral = new Vector();
            while(rs.next ()){
                reporte = new ReporteDiferenciaTiempos();
                VecReporteGeneral.add ( reporte.load (rs) );
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR EN LA CONSULTA DE REPORTE DE DIFERENCIA DE TIEMPOS CUMPLIDOS, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(ps != null){
                try{
                    ps.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_CONSULTA_DIFERENCIA");
        }
        return VecReporteGeneral;
    }
    
     /**
     * Metodo: consultaTrailers, permite generar el reporte de trailers
     * @autor : Ing. Jose de la rosa
     * @param : fecha de inicio, fecha fin, trailer, cabezote
     * @version : 1.0
     */
    public void consultaTrailers (String fecha_ini, String fecha_fin, String trailer, String cabezote) throws SQLException{
        PreparedStatement   ps      = null;
        ResultSet           rs      = null;
        Connection          con     = null;
        reporte = null;
        String             query   = "SQL_CONSULTA_TRAILER";
        try{
            con         =   this.conectar(query);
            String sql  =   this.obtenerSQL( query ).replaceAll("#ORDEN#", ( cabezote.equals ("") && !trailer.equals ("") )?"trailer, cabezote, fechaDesp DESC":"cabezote, trailer, fechaDesp DESC" );
            ps          =   con.prepareStatement( sql );    
            ps.setString (1, fecha_ini +" 00:00");
            ps.setString (2, fecha_fin +" 23:59");
            ps.setString (3, trailer+"%");
            ps.setString (4, cabezote+"%");
            rs = ps.executeQuery ();
            VecReporteGeneral = new Vector();
            while(rs.next ()){
                reporte = new ReporteDiferenciaTiempos();
                reporte.setPlanilla         ( rs.getString ("planilla")     );
                reporte.setRemesa           ( rs.getString ("remesa")       );
                reporte.setPlaca            ( rs.getString ("trailer")      );
                reporte.setCabezote         ( rs.getString ("cabezote")     );
                reporte.setFecha_despacho   ( rs.getString ("fechaDesp")    );
                VecReporteGeneral.add ( reporte );
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR EN LA CONSULTA DE REPORTE DE TRAILERS, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(ps != null){
                try{
                    ps.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar (query);
        }
    }
    
    /**
     * Getter for property VecReporteGeneral.
     * @return Value of property VecReporteGeneral.
     */
    public java.util.Vector getVecReporteGeneral () {
        return VecReporteGeneral;
    }
    
    /**
     * Obtiene el porcentaje utilizado del rango autorizado de series de planilla
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct Distrito
     * @version 1.0
     */
    public double sentinelaSeriePlanillas (String dstrct) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        double porcent = -1;
        
        try{
            ps = crearPreparedStatement ("SQL_SERIES_PLANILLA");
            ps.setString (1, dstrct);
            rs = ps.executeQuery ();
            
            while(rs.next ()){
                porcent = rs.getDouble("x");
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR EN LA CONSULTA DEL PORCENTAJE UTILIZADO DE LA SERIE DE PLANILLAS, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(ps != null){
                try{
                    ps.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_SERIES_PLANILLA");
        }
        return porcent;
    }    
   
    /**
     * Metodo: consultaTramo1Discrepancia, permite sacar los tramos, placas y ruta de el tramo1 de la remesa
     * @autor : Ing. Jose de la rosa
     * @param : remesa
     * @version : 1.0
     */    
    public Vector consultaTramo1Discrepancia (String remesa) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        Vector vec = null;
        try{
            ps = crearPreparedStatement ("SQL_TRAMO1_DISCREPANCIA");
            ps.setString (1, remesa);
            rs = ps.executeQuery ();
            if(rs.next ()){
                vec = new Vector();
                vec.add ( rs.getString ("planilla_tramo1")  !=null?rs.getString ("planilla_tramo1"):"" );
                vec.add ( rs.getString ("placa_tramo1")     !=null?rs.getString ("placa_tramo1"):"" );
                vec.add ( rs.getString ("ruta1_tramo1")     !=null?rs.getString ("ruta1_tramo1"):"" );
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR EN LA FUNCION consultaTramo1Discrepancia (String remesa), "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(ps != null){
                try{
                    ps.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_TRAMO1_DISCREPANCIA");
        }
        return vec;
    }
     
      /**
     * Metodo: AjustesFletes, permite generar el reporte general de ajustes a fletes
     * @autor : Ing. Luis eduardo Frieri
     * @param : fecha de inicio, fecha fin
     * @version : 1.0
     */
     public Vector ajustesFletes (String fecha_ini, String fecha_fin, String distrito) throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        PreparedStatement st2 = null;
        ResultSet rs2 = null;
        PreparedStatement st3 = null;
        ResultSet rs3 = null;
        Connection con = null;
        String planilla ="";
        boolean sw  = true;
        String  query = "SQL_REPORTE_FLETES_1";
        String sql1,sql2,sql3 = "";

        this.VecReporteGeneral = new Vector();

        try{
            ////System.out.println("entro");
            con = this.conectar(query);  
            
            sql1 = this.obtenerSQL("SQL_REPORTE_FLETES_1");
            sql2 = this.obtenerSQL("SQL_REPORTE_FLETES_2");
            sql3 = this.obtenerSQL("SQL_REPORTE_FLETES_3");
            
            
            st  = con.prepareStatement(sql1);
            st2 = con.prepareStatement(sql2);
            st3 = con.prepareStatement(sql3);
            
            //System.out.println("entro2");
            st.setString(1, fecha_ini);
            st.setString(2, fecha_fin);
            rs = st.executeQuery();
            //System.out.println("entro3");
            while (rs.next()) {
                
                AjusteFlete aj = new AjusteFlete();
                aj.setAgoc(rs.getString("agencia"));
                aj.setAgduenaoc(rs.getString("agduena"));
                aj.setFecoc(rs.getString("fechaoc"));
                aj.setCreaoc(rs.getString("creaoc"));
                aj.setNumoc(rs.getString("numpla"));
                aj.setPlacaoc(rs.getString("placa"));
                aj.setRutaoc(rs.getString("origen") + " - " + rs.getString("destino"));
                aj.setRecoc(rs.getString("descripcionveh"));
                aj.setCantoc(rs.getString("pesoreal"));
                aj.setUnioc(rs.getString("unit"));
                aj.setOtoc(rs.getString("numrem"));
                aj.setClioc(rs.getString("cliente"));
                aj.setStgoc(rs.getString("estandar"));
                aj.setTipooc(rs.getString("tiporuta"));
                aj.setDecot(rs.getString("rdescripcion"));
                aj.setCantot(rs.getString("rpesoreal"));
                aj.setTarifaot(rs.getString("runit"));
                aj.setFetestd(rs.getString("cost"));
                aj.setMonflete(rs.getString("currency"));
                aj.setFletedepmonest(rs.getString("vlrpla"));
                aj.setFletedespes(rs.getString("vlrpla2"));
                //aj.setFletestdpes(rs.getString("FLETE_STD_PES"));
                //aj.setAjusteflete(rs.getString("AJUSTE_FLETE"));
                aj.setTarifapes(rs.getString("vlrrem2"));
                aj.setCodcf(rs.getString("cf_code"));
                //aj.setVarporcflete(rs.getString("VAR_PORC_FLETES"));
                aj.setBase(rs.getString("base"));
    
                st2.clearParameters();
                st2.setString(1,rs.getString("numpla"));
                ////System.out.println("SQL FLETE " + st2.toString()); 
                rs2 = st2.executeQuery();
                if(rs2.next()){
                    
                    aj.setAnegativo((rs2.getString("negativos"))!=null?rs2.getString("negativos").toString():"0");
                    aj.setApositivo((rs2.getString("positivos"))!=null?rs2.getString("positivos").toString():"0");
                    aj.setValneg((rs2.getString("vlrnegativos"))!=null?rs2.getString("vlrnegativos").toString():"0");
                    aj.setValpos((rs2.getString("vlrpositivos"))!=null?rs2.getString("vlrpositivos").toString():"0");
                     
                }
                st3.clearParameters();
                st3.setString(1,distrito);
                if(rs.getString("cf_code").equals("")){
                   st3.setString(2,"0");
                   st3.setString(3,"0");
                }else{
                   st3.setString(2,rs.getString("cf_code").substring(0,6));
                   st3.setString(3,rs.getString("cf_code"));
                }
            
                
                rs3 = st3.executeQuery();
                if(rs3.next()){
                    aj.setValor_unidad(rs3.getString(1));
                }else{
                    aj.setValor_unidad("0");
                }
                
                VecReporteGeneral.add(aj);
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null)st.close();
            if (rs != null)rs.close();
            desconectar(query);            
        }
        return this.VecReporteGeneral;
    }
     
     public Vector reporteControlDiscrepancia ( String distrito, String fecha_ini, String fecha_fin, String aprobado ) throws SQLException{
        PreparedStatement   ps      = null;
        ResultSet           rs      = null;
        Connection          con     = null;
        VecReporteGeneral           = null;
        String              query   = "SQL_CONTROL_DISCREPANCIA", sql = "";
        try{
            con         =   this.conectar(query);
            if( aprobado.equals ("AB") )
                sql  =   this.obtenerSQL( query ).replaceAll("#APROBADO#", "AND d.fecha_cierre = '0099-01-01 00:00:00'" );
            else if( aprobado.equals ("CE") )
                sql  =   this.obtenerSQL( query ).replaceAll("#APROBADO#", "AND d.fecha_cierre != '0099-01-01 00:00:00'" );
            else
                sql  =   this.obtenerSQL( query ).replaceAll("#APROBADO#", "" );
            ps          =   con.prepareStatement( sql );
            ps.setString (1, distrito);
            ps.setString (2, fecha_ini +" 00:00");
            ps.setString (3, fecha_fin +" 23:59");
            rs = ps.executeQuery ();
            VecReporteGeneral = new Vector();
            while(rs.next ()){
                Hashtable ht = new Hashtable();
                Vector v = consultaTramo1Discrepancia( rs.getString( "remesa" ) );
                ht.put( "agencia_despacho",     rs.getString( "agencia_despacho" )  !=null? rs.getString( "agencia_despacho" ):"" );
                ht.put( "agencia_cliente",      rs.getString( "agencia_cliente" )   !=null? rs.getString( "agencia_cliente" ):"" );
                ht.put( "nombre_cliente",       rs.getString( "nombre_cliente" )    !=null? rs.getString( "nombre_cliente" ):"" );
                ht.put( "remesa",               rs.getString( "remesa" )            !=null? rs.getString( "remesa" ):"" );
                ht.put( "fecha_discrepancia",   rs.getString( "fecha_discrepancia" )!=null? rs.getString( "fecha_discrepancia" ):"" );
                ht.put( "num_discre",           rs.getString( "num_discre" )        !=null? rs.getString( "num_discre" ):"" );
                ht.put( "observacion",          rs.getString( "observacion" )       !=null? rs.getString( "observacion" ):"" );
                ht.put( "desc_discrepancia",    rs.getString( "desc_discrepancia" ) !=null? rs.getString( "desc_discrepancia" ):"" );
                ht.put( "placa",                rs.getString( "placa" )             !=null? rs.getString( "placa" ):"" );
                ht.put( "planilla",             rs.getString( "planilla" )          !=null? rs.getString( "planilla" ):"" );
                ht.put( "cantidad",             rs.getString( "cantidad" )          !=null? rs.getString( "cantidad" ):"0" );
                ht.put( "unidad",               rs.getString( "unidad" )            !=null? rs.getString( "unidad" ):"" );
                ht.put( "tipo_discrepancia",    rs.getString( "tipo_discrepancia" ) !=null? rs.getString( "tipo_discrepancia" ):"" );
                ht.put( "usuario",              rs.getString( "usuario" )           !=null? rs.getString( "usuario" ):"" );
                ht.put( "agencia_discrepancia", rs.getString( "agencia_discrepancia")!=null? rs.getString( "agencia_discrepancia" ):"" );
                ht.put( "retenido_pago",        rs.getString( "retenido_pago" )     !=null? rs.getString( "retenido_pago" ):"" );
                ht.put( "recurso",              rs.getString( "recurso" )           !=null? rs.getString( "recurso" ):"" );
                ht.put( "tipo_recurso",         rs.getString( "tipo_recurso" )      !=null? rs.getString( "tipo_recurso" ):"" );
                ht.put( "tipo_ot",              rs.getString( "tipo_ot" )           !=null? rs.getString( "tipo_ot" ):"" );
                ht.put( "ruta_oc",              rs.getString( "ruta_oc" )           !=null? rs.getString( "ruta_oc" ):"" );
                
                ht.put( "diferencia",           rs.getString( "diferencia" )        !=null? rs.getString( "diferencia" ):"0");
                ht.put( "rango",                rs.getString( "rango" )             !=null? rs.getString( "rango" ):"" );
                ht.put( "clasificacion",        rs.getString( "clasificacion" )     !=null? rs.getString( "clasificacion" ):"0" );
                
                ht.put( "planilla_tramo1",      v                                   !=null && v.size () > 2?(String)v.get (0):"" );
                ht.put( "placa_tramo1",         v                                   !=null && v.size () > 2?(String)v.get (1):"" );
                ht.put( "ruta1_tramo1",         v                                   !=null && v.size () > 2?(String)v.get (2):"" );
                
                //jose 2007-02-15
                ht.put( "estado",               rs.getString( "estado" )            !=null? rs.getString( "estado" ):"" );
                ht.put( "fecha_cierre",         rs.getString( "fecha_cierre" )      !=null? rs.getString( "fecha_cierre" ):"" );
                ht.put( "valor",                rs.getString( "valor" )             !=null? rs.getString( "valor" ):"0" );
                VecReporteGeneral.add ( ht );
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR EN LA CONSULTA DE reporteControlDiscrepancia (String distrito, String fecha_ini, String fecha_fin, String aprobado), "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(ps != null){
                try{
                    ps.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar (query);
        }
        return VecReporteGeneral;
    }
     
     //ffernandez 18/04/2007
     /*FUNCION DE REPORTE ICA CREADA POR FILY STEVEN FERNANDEZ VALENCIA*/
     public void ReoporteIca(String FechaI,String FechaF) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        BeanGeneral BeanCliente = null;
        vector = new Vector();
        try{
            st = this.crearPreparedStatement("SQL_REPORTE_ICA");
            int cont = 1;
            st.setString(1, FechaI);
            st.setString(2, FechaF);
            rs = st.executeQuery();
            System.out.println("query cliente "+st.toString());
            while(rs.next()){
                BeanCliente = new BeanGeneral();
                BeanCliente.setValor_01 ((rs.getString("cliente")!=null)?rs.getString("cliente"):"");
                BeanCliente.setValor_02 ((rs.getString("ot")!=null)?rs.getString("ot"):"");
                BeanCliente.setValor_03 ((rs.getString("valorot")!=null)?rs.getString("valorot"):"");
                BeanCliente.setValor_04 ((rs.getString("factura")!=null)?rs.getString("factura"):"");
                BeanCliente.setValor_05 ((rs.getString("nom_ciudad")!=null)?rs.getString("nom_ciudad"):"");
                BeanCliente.setValor_06 ((rs.getString("ruta_ot")!=null)?rs.getString("ruta_ot"):"");
                BeanCliente.setValor_07 ((rs.getString("pais_destino")!=null)?rs.getString("pais_destino"):"");
                BeanCliente.setValor_08 ((rs.getString("descripcion_sj")!=null)?rs.getString("descripcion_sj"):"");
                BeanCliente.setValor_09 ((rs.getString("std_job")!=null)?rs.getString("std_job"):"");
                BeanCliente.setValor_10 ((rs.getString("creacion")!=null)?rs.getString("creacion"):"");
                BeanCliente.setValor_11 ((rs.getString("nombreCliente")!=null)?rs.getString("nombreCliente"):"");
                BeanCliente.setValor_12 ((rs.getString("inv_amount_l")!=null)?rs.getString("inv_amount_l"):"");
                BeanCliente.setValor_13 ((rs.getString("inv_amount_f")!=null)?rs.getString("inv_amount_f"):"");
                BeanCliente.setValor_14 ((rs.getString("oc")!=null)?rs.getString("oc"):"");
                BeanCliente.setValor_15 ((rs.getString("fecha_oc")!=null)?rs.getString("fecha_oc"):"");
                BeanCliente.setValor_16 ((rs.getString("valor_oc")!=null)?rs.getString("valor_oc"):"");
                BeanCliente.setValor_17 ((rs.getString("ruta_oc")!=null)?rs.getString("ruta_oc"):"");
                BeanCliente.setValor_18 ((rs.getString("placa")!=null)?rs.getString("placa"):"");
                BeanCliente.setValor_19 ((rs.getString("codigo_propietario")!=null)?rs.getString("codigo_propietario"):"");
                BeanCliente.setValor_20 ((rs.getString("nombrePropieatrio")!=null)?rs.getString("nombrePropieatrio"):"");
                BeanCliente.setValor_21 (""+cont);
                 cont++;  
                 this.vector.addElement( BeanCliente );

		cont++;
            }
            
        }catch(Exception e) {
            throw new Exception("Error al SQL_REPORTE_ICA[ReporteGeneralDAO.xml]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_REPORTE_ICA");
        }
    }
     
     /**
      * Getter for property vector.
      * @return Value of property vector.
      */
     public java.util.Vector getVector() {
         return vector;
     }
     
     /**
      * Setter for property vector.
      * @param vector New value of property vector.
      */
     public void setVector(java.util.Vector vector) {
         this.vector = vector;
     }
     
     /**
     * Metodo: consultaTrailers, permite generar el reporte de trailers
     * @autor : Ing. Jose de la rosa
     * @param : fecha de inicio, fecha fin, trailer, cabezote
     * @version : 1.0
     */
    public Vector reporteFormatoCumplimiento(String fecha_ini, String fecha_fin, String agencia, String cliente) throws SQLException{
        
        String             query   = "SQL_REPORTE_FORMATO_CUMPLIMIENTO";
        String             query2  = "SQL_REMESAS_DOC_RELACIONADOS";
        VecReporteGeneral = null;
        String pedido, peso, factura, delivery, otros = "";
        PreparedStatement   ps      = null;
        ResultSet           rs      = null;
        PreparedStatement   ps2     = null;
        ResultSet           rs2     = null;
        Connection          con     = null;
        reporte = null;
        try{
            con         =   this.conectar(query);
            String sql  =   this.obtenerSQL( query );
            
            //KREALES
            String condicion="";
            
            if(!agencia.equals("")){
                String ageVec[] = agencia.split(",");
                String conAgencia ="";
                for(int i =0; i<ageVec.length; i++){
                    
                    conAgencia = conAgencia+"'"+ageVec[i]+"'";
                    if(i<ageVec.length-1){
                        conAgencia=conAgencia+",";
                    }
                }
                condicion = "AND re.agcrem in ("+conAgencia+")";
            }
            
            if(!cliente.equals("")){
                String cliVec[] = cliente.split(",");
                String concliente="";
                for(int i =0; i<cliVec.length; i++){
                    
                    concliente = concliente+"'"+cliVec[i]+"'";
                    if(i<cliVec.length-1){
                        concliente=concliente+",";
                    }
                }
                condicion = condicion+" AND re.cliente in ("+concliente+")";
            }
            else{
                condicion =condicion+" AND re.cliente ='000369'";
            }
            sql = sql.replaceAll("#CONDICION#", condicion);
            
            ps          =   con.prepareStatement( sql );
            ps.setString(1, fecha_ini +" 00:00");
            ps.setString(2, fecha_fin +" 23:59");
            System.out.println("Ps "+ps);
            rs = ps.executeQuery();
            VecReporteGeneral = new Vector();
            while(rs.next()){
                pedido = ""; peso = ""; factura = ""; delivery = ""; otros = "";
                ReporteFormatoCumplimiendo r = new ReporteFormatoCumplimiendo();
                
                /*rs.getString ("document");//documento
                rs.getString ("tipo_doc");//tipo doc
                rs.getString ("ot");//remesa
                rs.getString ("cia");//distrito*/
                
                if( rs.getString("tipo_doc") !=null && rs.getString("tipo_doc").equals("FAC") )
                    factura = rs.getString("document")!=null?rs.getString("document"):"";
                else if( rs.getString("tipo_doc") !=null && rs.getString("tipo_doc").equals("DEL") )
                    delivery = rs.getString("document")!=null?rs.getString("document"):"";
                else if( rs.getString("tipo_doc") !=null && rs.getString("tipo_doc").equals("PES") )
                    peso = rs.getString("document")!=null?rs.getString("document"):"";
                else if( rs.getString("tipo_doc") !=null && rs.getString("tipo_doc").equals("PED") )
                    pedido = rs.getString("document")!=null?rs.getString("document"):"";
                else
                    otros = rs.getString("document")!=null?rs.getString("document"):"";
                    
                    String sql2  =  this.obtenerSQL     ( query2 );
                    ps2          =  con.prepareStatement( sql2   );
                    ps2.clearParameters();
                    ps2.setString(1, rs.getString("cia")!=null?rs.getString("cia"):"");
                    ps2.setString(2, rs.getString("ot")!=null?rs.getString("ot"):"");
                    ps2.setString(3, rs.getString("document")!=null?rs.getString("document"):"");
                    ps2.setString(4, rs.getString("tipo_doc")!=null?rs.getString("tipo_doc"):"");
                    System.out.println("SQL2 "+ps2);
                    rs2 = ps2.executeQuery();
                    while(rs2.next()){
                        if( rs2.getString("tipo_doc_rel") != null && rs2.getString("tipo_doc_rel").equals("FAC") )
                            factura = rs2.getString("documento_rel")!=null?rs2.getString("documento_rel").equals("")?rs.getString("document"):rs2.getString("documento_rel"):"";
                        else if( rs2.getString("tipo_doc_rel") != null && rs2.getString("tipo_doc_rel").equals("DEL") )
                            delivery = rs2.getString("documento_rel")!=null?rs2.getString("documento_rel").equals("")?rs.getString("document"):rs2.getString("documento_rel"):"";
                        else if( rs2.getString("tipo_doc_rel") != null && rs2.getString("tipo_doc_rel").equals("PES") )
                            peso = rs2.getString("documento_rel")!=null?rs2.getString("documento_rel").equals("")?rs.getString("document"):rs2.getString("documento_rel"):"";
                        else if( rs2.getString("tipo_doc_rel") != null && rs2.getString("tipo_doc_rel").equals("PED") )
                            pedido = rs2.getString("documento_rel")!=null?rs2.getString("documento_rel").equals("")?rs.getString("document"):rs2.getString("documento_rel"):"";
                        else
                            otros = rs2.getString("documento_rel")!=null?rs2.getString("documento_rel").equals("")?rs.getString("document"):rs2.getString("documento_rel"):"";
                    }
                    
                    r.setPedido                     ( pedido                        );//pedido
                    r.setCliente                    ( rs.getString("cliente")      );//cliente
                    r.setCiudad_destino             ( rs.getString("ciu_destino")  );//ciudad destino
                    r.setPeso                       ( peso                          );//peso
                    r.setTipo_vehiculo              ( rs.getString("tipo_veh")     );//tipo de vehiculo
                    r.setFecha_solicitud            ( rs.getString("fe_sol")       );//fecha de solicitud
                    r.setHora_solicitud             ( rs.getString("ho_sol")       );//hora de solicitud
                    r.setFecha_max_carge_24         ( rs.getString("fe_mcarge24")  );//fecha max carge mas 24 horas
                    r.setHora_max_carge_24          ( rs.getString("ho_mcarge24")  );//hora max carge mas 24 horas
                    r.setMax_carge_solicitud_24     ( rs.getString("fe_mcarge24")  );//max carge segun solicitud 24H
                    r.setFecha_max_carge_36         ( rs.getString("fe_mcarge36")  );//fecha max carge mas 36 horas
                    r.setHora_max_carge_36          ( rs.getString("ho_mcarge36")  );//hora max carge mas 36 horas
                    r.setMax_carge_solicitud_36     ( rs.getString("fe_mcarge36")  );//max carge segun solicitud 36H
                    r.setProgramado                 ( rs.getString("programado")   );//programado
                    r.setFecha_programada_carge     ( rs.getString("fe_mcarge36")  );//fecha programada carge
                    r.setPlaca                      ( rs.getString("placa_veh")    );//placa
                    r.setCedula                     ( rs.getString("ced_cond")     );//cedula conductor
                    r.setNombre_cond                ( rs.getString("nom_cond")     );//nombre conductor
                    r.setComentario                 ( rs.getString("comentario")   );//comentario
                    r.setFactura                    ( factura                       );//factura
                    r.setFecha_llegada_planta       ( rs.getString("fe_lleplan")   );//fecha llegada planta
                    r.setHora_llegada_planta        ( rs.getString("ho_llplan")    );//hora llegada planta
                    r.setHora_radicada_ocarge       ( rs.getString("hora_ocarge")  );//hora radicada orden de carge
                    r.setFecha_entrada_carge        ( rs.getString("fe_ecarge")    );//fecha entrada carge
                    r.setHora_entrada_carge         ( rs.getString("ho_ecarge")    );//hora entrada carge*******
                    r.setHora_terminacion_carge     ( rs.getString("ho_tcarge")    );//hora terminacion de carge
                    r.setHora_entrega_doc_xom       ( rs.getString("ho_edoc")      );//hora entrega documentos xom
                    r.setHora_salida_planta         ( rs.getString("ho_splan")     );//hora salida planta
                    r.setTiempo_espera              ( rs.getString("tiem_espera")  );//tiempo de espera
                    r.setTiempo_entrega_doc         ( rs.getString("tiem_edoc")    );//tiempo entrega documentos
                    r.setTiempo_carge               ( rs.getString("tiem_carge")   );//tiempo de carge
                    r.setOc                         ( rs.getString("oc")           );//oc
                    r.setOt                         ( rs.getString("ot")           );//ot
                    r.setFecha_carge                ( rs.getString("fecha_ocarge") );//fecha de carge
                    r.setFecha_diferencia_colocacion( rs.getString("dif_fcol_prog"));//diferencia fecha colocacion programada
                    r.setCargado                    ( rs.getString("cargado")      );//cargado
                    r.setCausa1                     ( ""                            );//causas
                    r.setDiferencia_fcarge_freal36  ( rs.getString("dif_fcol_prog"));//diferencia fecha carge - fecha real sol 36
                    r.setTiempo_carge_despues_sol   ( rs.getString("tiem_cardsol") );//tiempo carge despues de solicitud
                    r.setCausa2                     ( ""                            );
                    r.setDiferencia_fcarge_freal24  ( rs.getString("dif_fcar_real24"));//diferencia fecha carge - fecha real sol 24
                    r.setDelivery                   ( delivery                      );//delivery
                    r.setOtros                      ( otros                         );//otros
                    
                    //KREALES
                    r.setOripla(rs.getString("oripla"));
                    r.setOrirem(rs.getString("orirem"));
                    VecReporteGeneral.add( r );
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR EN LA CONSULTA DE REPORTE DE FORMATOS DE CUMPLIMIENTOS, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null || ps != null){
                try{
                    ps.close();
                    ps2.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar(query);
        }
        return VecReporteGeneral;
    }
    
     /**
     * Metodo: reporteProduccionEquipo, permite generar el reporte de produccion de equipos
     * @autor : Ing. Jose de la rosa
     * @param : fecha de inicio, fecha fin
     * @version : 1.0
     */    
    public Vector reporteProduccionEquipo ( String fecha_ini, String fecha_fin ) throws SQLException{
        PreparedStatement   ps      = null;
        ResultSet           rs      = null;
        Connection          con     = null;
        VecReporteGeneral           = null;
        String              query   = "SQL_PRODUCCION_EQUIPO";
        try{
            con         =   this.conectar(query);
            String sql  =   this.obtenerSQL( query );
            ps          =   con.prepareStatement( sql );
            ps.setString (1, fecha_ini +" 00:00");
            ps.setString (2, fecha_fin +" 23:59");
            rs = ps.executeQuery ();
            VecReporteGeneral = new Vector();
            while(rs.next ()){
                Hashtable ht = new Hashtable();
                ht.put( "fecdsp",               rs.getString( "fecdsp" )            !=null? rs.getString( "fecdsp" ):"" );
                ht.put( "tipo",                 rs.getString( "tipo" )              !=null? rs.getString( "tipo" ):"" );
                ht.put( "semiremolque",         rs.getString( "semiremolque" )      !=null? rs.getString( "semiremolque" ):"" );
                ht.put( "descripcion",          rs.getString( "descripcion" )       !=null? rs.getString( "descripcion" ):"" );
                ht.put( "cabezote",             rs.getString( "cabezote" )          !=null? rs.getString( "cabezote" ):"" );
                ht.put( "ot",                   rs.getString( "ot" )                !=null? rs.getString( "ot" ):"" );
                ht.put( "oc",                   rs.getString( "oc" )                !=null? rs.getString( "oc" ):"" );
                ht.put( "origenOC",             rs.getString( "origenOC" )          !=null? rs.getString( "origenOC" ):"" );
                ht.put( "destinoOC",            rs.getString( "destinoOC" )         !=null? rs.getString( "destinoOC" ):"" );
                ht.put( "Ninterno",             rs.getString( "Ninterno" )          !=null? rs.getString( "Ninterno" ):"" );
                ht.put( "km",                   rs.getString( "km" )                !=null? rs.getString( "km" ):"" );
                ht.put( "valorOC",              rs.getString( "valorOC" )           !=null? rs.getString( "valorOC" ):"0" );
                ht.put( "valorOT",              rs.getString( "valorOT" )           !=null? rs.getString( "valorOT" ):"0" );
                ht.put( "cliente",              rs.getString( "cliente")            !=null? rs.getString( "cliente" ):"" );
                ht.put( "destinoOT",            rs.getString( "destinoOT" )         !=null? rs.getString( "destinoOT" ):"" );
                ht.put( "origenOT",             rs.getString( "origenOT" )          !=null? rs.getString( "origenOT" ):"" );
                ht.put( "concepto",             rs.getString( "concepto" )          !=null? rs.getString( "concepto" ):"" );
                ht.put( "descuento",            rs.getString( "descuento" )         !=null? rs.getString( "descuento" ):"0" );
                VecReporteGeneral.add ( ht );
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR EN LA CONSULTA DE reporteProduccionEquipo (String fecha_ini, String fecha_fin), "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(ps != null){
                try{
                    ps.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar (query);
        }
        return VecReporteGeneral;
    }  
     
      /**
     * Metodo: reporteUtilizacionPlaca, permite generar el reporte Utilizacion de placas
     * @autor : Ing. Jose de la rosa
     * @param : fecha de inicio, fecha fin, agencia destino
     * @version : 1.0
     */
    public Vector reporteUtilizacionPlaca(String fecha_ini, String fecha_fin, String agencia) throws SQLException{
        
        String              query   = "SQL_UTILIZACION";
        String              query2  = "SQL_UTILIZACION_REEXPEDICION";
        VecReporteGeneral           = null;
        PreparedStatement   ps      = null;
        ResultSet           rs      = null;
        PreparedStatement   ps2     = null;
        ResultSet           rs2     = null;
        Connection          con     = null;
        reporte                     = null;
        try{
            con         =   this.conectar(query);
            String sql  =   this.obtenerSQL( query );
            
            ps          =   con.prepareStatement( sql );
            ps.setString(1, fecha_ini +" 00:00" );
            ps.setString(2, fecha_fin +" 23:59" );
            ps.setString(3, agencia.equals("")?"%":agencia );
            rs = ps.executeQuery();
            VecReporteGeneral = new Vector();
            while(rs.next()){
                    Hashtable ht = new Hashtable();
                    ht.put( "fecha_impresion",      rs.getString( "fecha_impresion" )   !=null? rs.getString( "fecha_impresion" ).equals("0099-01-01")?"": rs.getString( "fecha_impresion" ):"" );
                    ht.put( "placa",                rs.getString( "placa" )             !=null? rs.getString( "placa" ):"" );
                    ht.put( "tipoviaje",            rs.getString( "tipoviaje" )         !=null? rs.getString( "tipoviaje" ):"" );
                    ht.put( "oc",                   rs.getString( "oc" )                !=null? rs.getString( "oc" ):"" );
                    ht.put( "origen",               rs.getString( "origen" )            !=null? rs.getString( "origen" ):"" );
                    ht.put( "destino",              rs.getString( "destino" )           !=null? rs.getString( "destino" ):"" );
                    ht.put( "cliente",              rs.getString( "cliente" )           !=null? rs.getString( "cliente" ):"" );
                    ht.put( "fecha_reporte",        rs.getString( "fecha_reporte" )     !=null? rs.getString( "fecha_reporte" ):"" );
                    
                    String sql2  =  this.obtenerSQL     ( query2 );
                    ps2          =  con.prepareStatement( sql2   );
                    ps2.clearParameters();
                    ps2.setString(1, rs.getString("oc")!=null?rs.getString("oc"):"");
                    ps2.setString(2, rs.getString("placa")!=null?rs.getString("placa"):"");
                    ps2.setString(3, rs.getString("fecdsp")!=null?rs.getString("fecdsp"):"0099-01-01 00:00");
                    ps2.setString(4, rs.getString("agasoc")!=null?rs.getString("agasoc"):"");
                    rs2 = ps2.executeQuery();
                    while(rs2.next()){
                        ht.put( "Rfecha_impresion",      rs2.getString( "fecha_impresion" )   !=null? rs2.getString( "fecha_impresion" ).equals("0099-01-01")?"": rs2.getString( "fecha_impresion" ):"" );
                        ht.put( "Rplaca",                rs2.getString( "placa" )             !=null? rs2.getString( "placa" ):"" );
                        ht.put( "Rtipoviaje",            rs2.getString( "tipoviaje" )         !=null? rs2.getString( "tipoviaje" ):"" );
                        ht.put( "Roc",                   rs2.getString( "oc" )                !=null? rs2.getString( "oc" ):"" );
                        ht.put( "Rorigen",               rs2.getString( "origen" )            !=null? rs2.getString( "origen" ):"" );
                        ht.put( "Rdestino",              rs2.getString( "destino" )           !=null? rs2.getString( "destino" ):"" );
                        ht.put( "Rcliente",              rs2.getString( "cliente" )           !=null? rs2.getString( "cliente" ):"" );
                        ht.put( "Rfecha_reporte",        rs2.getString( "fecha_reporte" )     !=null? rs2.getString( "fecha_reporte" ):"" );
                    }
                    VecReporteGeneral.add(ht );
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR EN LA CONSULTA DE REPORTE DE reporteUtilizacionPlaca(String fecha_ini, String fecha_fin, String agencia), "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null || ps != null){
                try{
                    ps.close();
                    ps2.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar(query);
        }
        return VecReporteGeneral;
    }
    
    /*Reporte equipo 25 - 04 -2007*/
    /**
      * Getter for property tama�oVector.
      * @return Value of property tama�oVector.
      */
   public void ReoporteEquipo() throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ReporteEquipos EQ = null;
        vector = new Vector();
        try{
            st = this.crearPreparedStatement("SQL_REPORTE_EQUIPO_TRAILERS_PLACA");
            rs = st.executeQuery();
            String fechas = "0099-01-01";
            while(rs.next()){
                String f_d ="";
                String WGEquipo_cabezote = "";
                EQ = new ReporteEquipos();
               
                //query 1
                EQ.setPlacaTrailer((rs.getString("placa")!=null)?rs.getString("placa"):"");
                EQ.setWgEquipo((rs.getString("wgequipo")!=null)?rs.getString("wgequipo"):"");
              
                //query 2
                 this.MOV_Planilla(EQ.getPlacaTrailer());
                 Vector datosC = this.getVectorPlaca();
		 BeanGeneral beanPlaca = (BeanGeneral) datosC.elementAt(0);
                 
                 EQ.setOc(beanPlaca.getValor_02());
                 EQ.setFecha_despacho(beanPlaca.getValor_03());
                 if(EQ.getFecha_despacho().equals(fechas)){
                   EQ.setFecha_despacho("");    
                }
                 
                //query 3
                 if (EQ.getOc().equals("")){
                     EQ.setNoInterno("");
                       EQ.setEquipo("");
                       EQ.setCabezote("");
                       EQ.setOT("");
                       EQ.setOrigen_oc("");
                       EQ.setDestino_oc("");
                       EQ.setFecha_ult_reporte("");
                       EQ.setCliente("");
                       EQ.setObservaciones_trafico("");
                       EQ.setOrigen_ot("");
                       EQ.setDestino_ot("");
                       EQ.setZona_disponibilidad("");
                       EQ.setFecha_cumplido("");
                       EQ.setNombreCliente("");
                       EQ.setF_D("");
                      EQ.setWgEquipo_cabezote("");
                 }else{
                     this.Datos_planilla(EQ.getOc()); 
                     Vector datosP = this.getVectorPlanilla();
                     BeanGeneral beanPl = (BeanGeneral) datosP.elementAt(0);
                       EQ.setNoInterno(beanPl.getValor_01());
                       EQ.setEquipo(beanPl.getValor_02());
                       EQ.setCabezote(beanPl.getValor_03());
                       EQ.setOT(beanPl.getValor_04());
                       EQ.setOrigen_oc(beanPl.getValor_07());
                       EQ.setDestino_oc(beanPl.getValor_08());
                       EQ.setFecha_ult_reporte(beanPl.getValor_09());
                       if(EQ.getFecha_ult_reporte().equals(fechas)){
                         EQ.setFecha_ult_reporte("");    
                       }
                       EQ.setCliente(beanPl.getValor_10());
                       EQ.setObservaciones_trafico(beanPl.getValor_11());
                       EQ.setOrigen_ot(beanPl.getValor_12());
                       EQ.setDestino_ot(beanPl.getValor_13());
                       EQ.setZona_disponibilidad(beanPl.getValor_14());
                       EQ.setFecha_cumplido(beanPl.getValor_15());
                       if(EQ.getFecha_cumplido().equals(fechas)){
                            EQ.setFecha_cumplido("");  
                       }
                       EQ.setNombreCliente(beanPl.getValor_16());
                        if(EQ.getEquipo().equals("") ){
                         EQ.setF_D("");  
                       }else{
                       f_d = this.F_D(EQ.getEquipo());
                       EQ.setF_D(f_d);
                       }
                    //query 5
                       if(EQ.getCabezote().equals("")){
                           EQ.setWgEquipo_cabezote(""); 
                       }else{
                           WGEquipo_cabezote = this.Grupo_cabezote(EQ.getCabezote());   
                           EQ.setWgEquipo_cabezote(WGEquipo_cabezote); 
                       }
                 }
                
               this.vector.addElement( EQ );
             }
        }catch(Exception e) {
            throw new Exception("Error al SQL_REPORTE_EQUIPO_TRAILERS_PLACA[ReporteGeneralDAO.xml]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_REPORTE_EQUIPO_TRAILERS_PLACA");
        }
    }
   
   //sacar el ultimo movimiento de planilla de la placa trailers 2
   public void MOV_Planilla(String placa) throws Exception {
       PreparedStatement st = null;
        ResultSet rs = null;
        ReporteEquipos EQ = null;
        BeanGeneral BeanMov = null;
        vectorPlaca = new Vector();
        try{
            st = this.crearPreparedStatement("SQL_REPORTE_EQUIPO_TRAILERS_ULT_PLANILLA");
            st.setString(1, placa);
            rs = st.executeQuery();
            if(rs.next()){
                BeanMov = new BeanGeneral();
                BeanMov.setValor_01 ((rs.getString("placa")!=null)?rs.getString("placa"):"");
                BeanMov.setValor_02 ((rs.getString("numplanilla")!=null)?rs.getString("numplanilla"):"");
                BeanMov.setValor_03 ((rs.getString("fechaDespacho")!=null)?rs.getString("fechaDespacho"):"");
                this.vectorPlaca.addElement( BeanMov );

            }
           }catch(Exception e) {
            throw new Exception("Error al SQL_REPORTE_EQUIPO_TRAILERS_ULT_PLANILLA[ReporteGeneralDAO.xml]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_REPORTE_EQUIPO_TRAILERS_ULT_PLANILLA");
        }
        
    }
   
    /*funcion para sacar los datos de la planilla */
    public void Datos_planilla(String numpla) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ReporteEquipos EQ = null;
        BeanGeneral BeanPlanilla = null;
        vectorPlanilla = new Vector();
        try{
            st = this.crearPreparedStatement("SQL_REPORTE_EQUIPO_TRAILERS_DATOS_PLANILLA");
            st.setString(1, numpla);
            rs = st.executeQuery();
            if(rs.next()){
                BeanPlanilla = new BeanGeneral();
                BeanPlanilla.setValor_01 ((rs.getString("NoInterno")!=null)?rs.getString("NoInterno"):"");
                BeanPlanilla.setValor_02 ((rs.getString("equipo")!=null)?rs.getString("equipo"):"");
                BeanPlanilla.setValor_03 ((rs.getString("cabezote")!=null)?rs.getString("cabezote"):"");
                BeanPlanilla.setValor_04 ((rs.getString("ot")!=null)?rs.getString("ot"):"");
                BeanPlanilla.setValor_05 ((rs.getString("oc")!=null)?rs.getString("oc"):"");
                BeanPlanilla.setValor_06 ((rs.getString("fecha_despacho")!=null)?rs.getString("fecha_despacho"):"");
                BeanPlanilla.setValor_07 ((rs.getString("origen_oc")!=null)?rs.getString("origen_oc"):"");
                BeanPlanilla.setValor_08 ((rs.getString("destino_oc")!=null)?rs.getString("destino_oc"):"");
                BeanPlanilla.setValor_09 ((rs.getString("fecha_ultimo_reporte")!=null)?rs.getString("fecha_ultimo_reporte"):"");
                BeanPlanilla.setValor_10 ((rs.getString("cliente")!=null)?rs.getString("cliente"):"");
                BeanPlanilla.setValor_11 ((rs.getString("trafico_observacion")!=null)?rs.getString("trafico_observacion"):"");
                BeanPlanilla.setValor_12 ((rs.getString("origen_ot")!=null)?rs.getString("origen_ot"):"");
                BeanPlanilla.setValor_13 ((rs.getString("destino_ot")!=null)?rs.getString("destino_ot"):"");
                BeanPlanilla.setValor_14 ((rs.getString("zon_disponibilidad")!=null)?rs.getString("zon_disponibilidad"):"");
                BeanPlanilla.setValor_15 ((rs.getString("fecha_cumplido")!=null)?rs.getString("fecha_cumplido"):"");
                BeanPlanilla.setValor_16 ((rs.getString("nomCliente")!=null)?rs.getString("nomCliente"):"");
                
                this.vectorPlanilla.addElement( BeanPlanilla );

		
            }
            
        }catch(Exception e) {
             e.printStackTrace();
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_REPORTE_EQUIPO_TRAILERS_DATOS_PLANILLA");
        }
        
    }
   
    /*funcion para sacar el grupo de cabezote por medio de la placa*/
    public String F_D(String equipo) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ReporteEquipos EQ = null;
        String f_d = "";
        try{
            st = this.crearPreparedStatement("SQL_REPORTE_EQUIPO_TRAILERS_F_D");
            st.setString(1, equipo);
            rs = st.executeQuery();
            if(rs.next()){
               f_d = (rs.getString("f_d")!=null)?rs.getString("f_d"):"";
              
            }
           }catch(Exception e) {
            throw new Exception("Error al SQL_REPORTE_EQUIPO_TRAILERS_F_D[ReporteGeneralDAO.xml]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_REPORTE_EQUIPO_TRAILERS_F_D");
        }
        return f_d;
    }
   
   /*funcion para sacar el enganche y desemganche*/
    public String Grupo_cabezote(String placa) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ReporteEquipos EQ = null;
        String wg_cabezote = "";
        try{
            st = this.crearPreparedStatement("SQL_REPORTE_EQUIPO_TRAILERS_WGCABEZOTE");
            st.setString(1, placa);
            rs = st.executeQuery();
            if(rs.next()){
               wg_cabezote = (rs.getString("wgcabezote")!=null)?rs.getString("wgcabezote"):"";
              
            }
           }catch(Exception e) {
            throw new Exception("Error al SQL_REPORTE_EQUIPO_TRAILERS_WGCABEZOTE[ReporteGeneralDAO.xml]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_REPORTE_EQUIPO_TRAILERS_WGCABEZOTE");
        }
        return wg_cabezote;
    }
       
    /**
     * Getter for property vectorTemp.
     * @return Value of property vectorTemp.
     */
  
    
    /**
     * Getter for property vectorPlaca.
     * @return Value of property vectorPlaca.
     */
    public java.util.Vector getVectorPlaca() {
        return vectorPlaca;
    }
    
    /**
     * Setter for property vectorPlaca.
     * @param vectorPlaca New value of property vectorPlaca.
     */
    public void setVectorPlaca(java.util.Vector vectorPlaca) {
        this.vectorPlaca = vectorPlaca;
    }
    
    /**
     * Getter for property vectorPlanilla.
     * @return Value of property vectorPlanilla.
     */
    public java.util.Vector getVectorPlanilla() {
        return vectorPlanilla;
    }
    
    /**
     * Setter for property vectorPlanilla.
     * @param vectorPlanilla New value of property vectorPlanilla.
     */
    public void setVectorPlanilla(java.util.Vector vectorPlanilla) {
        this.vectorPlanilla = vectorPlanilla;
    }
     /**
     * Metodo: reporteRemesasNacionales, permite generar el reporte de remesas nacionales
     * @autor : Ing. Jose de la rosa
     * @param : fecha de inicio, fecha fin
     * @version : 1.0
     */
      public LinkedList reporteRemesasNacionalesCaso1(String fecha_ini, String fecha_fin) throws SQLException{
        String              query    = "SQL_REMESA_NACIONAL_CASO1";
        String              query2   = "SQL_REMESA_NACIONAL_CASO2";
        String              query3   = "SQL_REMESA_NACIONAL_CASO3";
        LinkedList VecReporteGeneral = null;
        PreparedStatement   ps       = null;
        ResultSet           rs       = null;
        PreparedStatement   ps2       = null;
        ResultSet           rs2       = null;
        PreparedStatement   ps3       = null;
        ResultSet           rs3       = null;
        Connection          con       = null;
        boolean             sw        = false;
        int                 mayor     = 0;
        int                 i         = 0;
        try{
            con         =   this.conectar(query);
            String sql  =   this.obtenerSQL( query );
            ps          =   con.prepareStatement( sql );
            ps.setString(1, fecha_ini +" 00:00" );
            ps.setString(2, fecha_fin +" 23:59" );
            rs = ps.executeQuery();
            VecReporteGeneral = new LinkedList();
            while(rs.next()){
                    Hashtable ht = new Hashtable();
                    sw           = false;

                    ht.put( "tipoOT",               rs.getString( "tipoOT" )            !=null? rs.getString( "tipoOT" ):"" );
                    ht.put( "ot",                   rs.getString( "ot" )                !=null? rs.getString( "ot" ):"" );
                    ht.put( "estadoOT",             rs.getString( "estadoOT" )          !=null? rs.getString( "estadoOT" ):"" );
                    ht.put( "fechaOT",              rs.getString( "fechaOT" )           !=null? rs.getString( "fechaOT" ).equals("0099-01-01")?"": rs.getString( "fechaOT" ):"" );
                    ht.put( "clienteOT",            rs.getString( "clienteOT" )         !=null? rs.getString( "clienteOT" ):"" );
                    ht.put( "origenOT",             rs.getString( "origenOT" )          !=null? rs.getString( "origenOT" ):"" );
                    ht.put( "destinoOT",            rs.getString( "destinoOT" )         !=null? rs.getString( "destinoOT" ):"" );
                    ht.put( "agenciaOT",            rs.getString( "agenciaOT" )         !=null? rs.getString( "agenciaOT" ):"" );
                    ht.put( "remesa_padre",         rs.getString( "remesa_padre" )      !=null? rs.getString( "remesa_padre" ):"" );
                    ht.put( "remesa_relacionada",   rs.getString( "remesa_relacionada" )!=null? rs.getString( "remesa_relacionada" ):"" );

                    String sql2  =  this.obtenerSQL     ( query2 );
                    ps2          =  con.prepareStatement( sql2   );
                    ps2.clearParameters();
                    ps2.setString(1, rs.getString("ot")!=null?rs.getString("ot"):"");
                    rs2 = ps2.executeQuery();
                    i = 0;
                    while( rs2.next() ){
                        String sql3  =  this.obtenerSQL     ( query3 );
                        ps3          =  con.prepareStatement( sql3   );
                        ps3.clearParameters();
                        ps3.setString(1, rs2.getString("numpla")!=null?rs2.getString("numpla"):"");
                        ps3.setString(2, rs2.getString("numpla")!=null?rs2.getString("numpla"):"");
                        rs3 = ps3.executeQuery();
                        sw = rs3.next();
                        if( sw )
                            i++;
                    }
                    if( i > mayor )
                            mayor = i;
                    ht.put( "tamano", ""+mayor);
                    if( sw )
                        VecReporteGeneral.add( ht );
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR EN LA CONSULTA DE REPORTE DE reporteRemesasNacionales(String fecha_ini, String fecha_fin), "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if( ps != null ){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar(query);
        }
        return VecReporteGeneral;
    } 
    
     /**
     * Metodo: reporteRemesasNacionales, permite generar el reporte de remesas nacionales
     * @autor : Ing. Jose de la rosa
     * @param : remesa
     * @version : 1.0
     */
    public Vector reporteRemesasNacionalesCaso2( String remesa ) throws SQLException{
        String              query   = "SQL_REMESA_NACIONAL_CASO2";
        VecReporteGeneral           = null;
        PreparedStatement   ps      = null;
        ResultSet           rs      = null;
        Connection          con     = null;
        try{
            con         =   this.conectar(query);
            String sql  =   this.obtenerSQL( query );
            ps          =   con.prepareStatement( sql );
            ps.setString(1, remesa );
            rs = ps.executeQuery();
            VecReporteGeneral = new Vector();
            while(rs.next()){
                VecReporteGeneral.add(  rs.getString( "numpla" )!=null? rs.getString( "numpla" ):"" );
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR EN LA CONSULTA DE REPORTE DE reporteRemesasNacionalesCaso2( String remesa ), "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if( ps != null ){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar(query);
        }
        return VecReporteGeneral;
    }      
    
     /**
     * Metodo: reporteRemesasNacionalesCaso3, permite generar el reporte de remesas nacionales
     * @autor : Ing. Jose de la rosa
     * @param : planilla
     * @version : 1.0
     */
    public LinkedList reporteRemesasNacionalesCaso3( String planilla ) throws SQLException{
        String              query    = "SQL_REMESA_NACIONAL_CASO3";
        LinkedList VecReporteGeneral = null;
        PreparedStatement   ps       = null;
        ResultSet           rs       = null;
        Connection          con      = null;
        try{
            con         =   this.conectar(query);
            String sql  =   this.obtenerSQL( query );
            ps          =   con.prepareStatement( sql );
            ps.setString(1, planilla );
            ps.setString(2, planilla );
            rs = ps.executeQuery();
            VecReporteGeneral = new LinkedList();
            while(rs.next()){
                    Hashtable ht = new Hashtable();
                    ht.put( "oc",                   rs.getString( "oc" )                !=null? rs.getString( "oc" ):"" );
                    ht.put( "fechaOC",              rs.getString( "fechaOC" )           !=null? rs.getString( "fechaOC" ):"" );
                    ht.put( "placaOC",              rs.getString( "placaOC" )           !=null? rs.getString( "placaOC" ):"" );
                    ht.put( "origenOC",             rs.getString( "origenOC" )          !=null? rs.getString( "origenOC" ):"" );
                    ht.put( "destinoOC",            rs.getString( "destinoOC" )         !=null? rs.getString( "destinoOC" ):"" );
                    ht.put( "anticipo",             rs.getString( "anticipo" )          !=null? rs.getString( "anticipo" ):"0" );
                    ht.put( "fecha_anticipo",       rs.getString( "fecha_anticipo" )    !=null? rs.getString( "fecha_anticipo" ).equals("0099-01-01")?"": rs.getString( "fecha_anticipo" ):"" );
                    ht.put( "ult_reporte",          rs.getString( "ult_reporte" )       !=null? rs.getString( "ult_reporte" ):"" );
                    ht.put( "caso",                 rs.getString( "caso" )              !=null? rs.getString( "caso" ):"" );
                    VecReporteGeneral.add(ht );
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR EN LA CONSULTA DE REPORTE DE reporteRemesasNacionalesCaso3( String planilla ), "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if( ps != null ){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar(query);
        }
        return VecReporteGeneral;
    }
    
}
