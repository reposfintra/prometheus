/*
 * HImportacionCFDAO.java
 *
 * Created on 7 de septiembre de 2006, 03:03 PM
 */

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.util.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
//import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.OpDAO;
import com.tsp.operation.model.DAOS.MainDAO;
/**
 *
 * @author  ALVARO
 */
public class HImportacionCFDAO extends MainDAO {
    
    /** Creates a new instance of HImportacionCFDAO */
    public HImportacionCFDAO() {
         super ( "HImportacionCFDAO.xml" );
    }
    
    
    public String InsertarFacturas (List listado,String u)  throws Exception{
        
        Connection con = null;
        PreparedStatement st = null; 
        String log1="";
        PreparedStatement st2 = null; 
        ResultSet rs = null; 
        String query = "SQL_REMESA_DOCTO";
        String query2 = "SQL_INSERT_DOCTO";
        try{ 

            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st2 = con.prepareStatement(this.obtenerSQL(query2));//JJCastro fase2

                for(int i=0;i<listado.size();i++){
                    st.clearParameters();
                    st2.clearParameters(); 
                    String [] datos = (String[])listado.get(i);
                    st.setString ( 1, datos[1] );
                    st.setString ( 2, datos[0] );
                    rs = st.executeQuery();
                    while (rs.next()) {
                        try{
                            st2.clearParameters();   
                            st2.setString(1,rs.getString(1));
                            st2.setString(2,rs.getString(2));
                            st2.setString(3,rs.getString(3));
                            st2.setString(4,rs.getString(4));
                            log1+="\ndocumento:"+rs.getString(4);
                            st2.setString(5, rs.getString(5));
                            st2.setString(6,datos[2]);
                            st2.setString(7,rs.getString(7));
                            log1+="documento rel:"+rs.getString(7);
                            st2.setString(8,rs.getString(8));
                            st2.setString(9,rs.getString(9));
                            st2.setString(10,rs.getString(10));
                            st2.setString(11, rs.getString(11));
                            st2.setString(12,rs.getString(12));
                            st2.setString(13,rs.getString(13));
                            st2.setString(14,u);
                            st2.setString(15,u);
                            st2.executeUpdate();
                            log1+=" insercion ok!";
                        }catch(Exception r){
                            log1+=" insercion fallida ya existe en la bd!";    
                        }
                    }

                }
                
            }}catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR REMESAS POSTGRES " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (st2  != null){ try{ st2.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return log1;
    }
}
