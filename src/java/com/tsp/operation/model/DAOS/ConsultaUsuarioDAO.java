/***********************************************************************************
 * Nombre clase : ............... ConsultaUsuarioDAO.java                                   *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.                                       *
 * Autor :....................... Ing. David Lamadrid                               *
 * Fecha :....................... 4 de diciembre de 2005, 11:02 AM                   *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  dlamadrid
 */
public class ConsultaUsuarioDAO {
    private Vector vConsultas;
    private Vector vCampos;
    private TreeMap tConsultas;
    private Vector vConuslta;
    private ConsultaUsuarios consulta;

    private String INSERTAR="insert into consulta_usuarios(reg_status,dstrct,descripcion,cselect,cfrom,cwhere,cotros,creation_date ,creation_user,base) values('',?,?,?,?,?,?,now(),?,?)";
    private String ELIMINAR="delete from consulta_usuarios where codigo=?";
    private String LISTAR_POR_U="select * from consulta_usuarios where creation_user=?";
    private String LISTAR_POR_CODIGO="select * from consulta_usuarios where codigo=?";
    private String ELIMINAR_POR_CODIGO="delete from consulta_usuarios where codigo=?";
    private String EXISTE="select codigo from consulta_usuarios where creation_user=? and consulta=?";

    /** Creates a new instance of ConsultaUsuarioDAO */
    public ConsultaUsuarioDAO () {
    }
    
    /**
    * inserta un registro en la tabla consulta_usuario
    * @autor David Lamadrid
    * @throws SQLException
    * @version 1.0
    */
    public void insertar () throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null) {
                st = con.prepareStatement (this.INSERTAR);
                st.setString (1,consulta.getDstrct ());
                st.setString (2,consulta.getDescripcion ());
                st.setString (3,consulta.getCselect ());
                st.setString (4,consulta.getCfrom ());
                st.setString (5,consulta.getCwhere ());
                st.setString (6, consulta.getCotros ());
                st.setString (7,consulta.getCreation_user ());
                st.setString (8,consulta.getBase ());
                //////System.out.println("INSERTAR EN DAO "+st);
                st.executeUpdate ();
            }
        }catch(SQLException e) {
            throw new SQLException ("ERROR AL INSERTAR EL CONSULTA: " + e.getMessage ()+"" + e.getErrorCode ());
        }finally {
            if(st != null) {
                try {
                    st.close ();
                }
                catch(SQLException e) {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null) {
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
     
    /**
    * Lista los registros de consulta_usuarios por usuario de creacion
    * @autor David Lamadrid
    * @throws SQLException
    * @version 1.0
    */
    public void listarPorU (String codigo) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        this.vConsultas = new Vector ();
        
        try {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null) {
                st = con.prepareStatement (this.LISTAR_POR_U);
                st.setString (1, codigo);
                rs = st.executeQuery ();
                while( rs.next () ) {
                    this.consulta=new ConsultaUsuarios ();
                    this.consulta=this.consulta.load (rs);
                    this.vConsultas.add (this.consulta);
                }
            }
        }catch(SQLException e) {
            throw new SQLException ("ERROR AL LISTAR LAS APLICACIONES: " + e.getMessage ()+"" + e.getErrorCode ());
        }finally {
            if(st != null) {
                try {
                    st.close ();
                }
                catch(SQLException e) {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null) {
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
    
    /**
    * Lista los registros de consulta_usuarios por codigo de consulta
    * @autor David Lamadrid
    * @throws SQLException
    * @version 1.0
    */
    public void listarPorCodigo (String codigo) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null) {
                st = con.prepareStatement (this.LISTAR_POR_CODIGO);
                st.setString (1, codigo);
                rs = st.executeQuery ();
                while( rs.next () ) {
                    this.consulta=new ConsultaUsuarios ();
                    this.consulta=this.consulta.load (rs);
                }
            }
        }catch(SQLException e) {
            throw new SQLException ("ERROR AL LISTAR LAS APLICACIONES: " + e.getMessage ()+"" + e.getErrorCode ());
        }finally {
            if(st != null) {
                try {
                    st.close ();
                }
                catch(SQLException e) {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null) {
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
    
    /**
    * retorna false si no existe el registro y true si existe un registro con usuario y consulta igual a los de la bd
    * @autor David Lamadrid
    * @throws SQLException
    * @version 1.0
    */
    public boolean exsite (String usuario,String consulta) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        boolean sw = false;
        
        try {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null) {
                st = con.prepareStatement (this.EXISTE);
                st.setString (1,usuario);
                st.setString (2,consulta);
                rs = st.executeQuery ();
                
                while( rs.next () ) {
                    sw= true;
                }
            }
        }catch(SQLException e) {
            throw new SQLException ("ERROR AL LISTAR LAS APLICACIONES: " + e.getMessage ()+"" + e.getErrorCode ());
        }finally {
            if(st != null) {
                try {
                    st.close ();
                }
                catch(SQLException e) {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null) {
                poolManager.freeConnection ("fintra", con );
            }
        }
        return sw;
    }
    
    /**
    * Metodo que retorna true si existe una tabla en el sitema dado el nombre de la tabla
    * @autor.......David Lamadrid
    * @param.......String nombreTabla
    * @see.........
    * @throws......registro no existe en el sitema
    * @version.....1.0.
    * @return.......boolen x(true si existe ,false si no existe)
    */
    public  boolean existeConsulta (String consulta ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean x = false;
        try {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null) {
                st = con.prepareStatement (consulta);
                ////System.out.println ("query: "+st);
                rs = st.executeQuery ();
                x=true;
            }
        }
        catch(org.postgresql.util.PSQLException e) {
            poolManager.freeConnection ("fintra", con );
        }
       finally {
            if(st != null) {
                try {
                    st.close ();
                }
                catch(SQLException e) {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null) {
                poolManager.freeConnection ("fintra", con );
            }
        }

        return x;
    }
    

    
    /**
    * retorna false si no existe el registro y true si existe un registro con usuario y consulta igual a los de la bd
    * @autor David Lamadrid
    * @throws SQLException
    * @version 1.0
    */
    public void generarConsulta (Vector campos,String consulta) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String campo="";
        try {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null) {
                st = con.prepareStatement (consulta);
                rs = st.executeQuery ();
                this.vConuslta= new Vector ();
                while( rs.next () ) {
                    Vector fila = new Vector ();
                    for(int i=0;i <campos.size ();i++){
                        campo= ""+campos.elementAt (i);
                        campo=campo.substring (2,campo.length ());
                        fila.add (rs.getString (campo));
                    }
                    this.vConuslta.add (fila);
                }
            }
        }catch(SQLException e) {
            throw new SQLException ("ERROR AL LISTAR LAS APLICACIONES: " + e.getMessage ()+"" + e.getErrorCode ());
        }finally {
            if(st != null) {
                try {
                    st.close ();
                }
                catch(SQLException e) {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null) {
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
    
    /**
    * Elimina el registro de consultas_usuario dado la llave primaria del registro
    * @autor David Lamadrid
    * @throws SQLException
    * @version 1.0
    */
    public void eliminar (int codigo) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null) {
                st = con.prepareStatement (this.ELIMINAR);
                st.setInt (1,codigo);
                st.executeUpdate ();
            }
        }catch(SQLException e) {
            throw new SQLException ("ERROR AL ACTUALIZAR LA APLICACION: " + e.getMessage ()+"" + e.getErrorCode ());
        }finally {
            if(st != null) {
                try {
                    st.close ();
                }
                catch(SQLException e) {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null) {
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
    
    /**
     * Getter for property tConsultas.
     * @return Value of property tConsultas.
     */
    public java.util.TreeMap getTConsultas () {
        return tConsultas;
    }
    
    /**
     * Setter for property tConsultas.
     * @param tConsultas New value of property tConsultas.
     */
    public void setTConsultas (java.util.TreeMap tConsultas) {
        this.tConsultas = tConsultas;
    }
    
    /**
    * metodo que setea el TreeMap tConsultas con el Vector vConsultas que contiene una lista de Objetos con los registros de consultas_usuarios consernientes a un usuario
    * @autor David Lamadrid
    * @throws SQLException
    * @version 1.0
    */
    public void tConfiguracionesPorUsuario (String usuario) throws SQLException {
        this.tConsultas = new TreeMap ();
        this.listarPorU (usuario);
        Vector vc=this.getVConsultas ();
        //////System.out.println ("TAMA�O DEL VECTOR EN TREEMAP"+vc.size ());
        for(int i=0; i< vc.size (); i++) {
            ConsultaUsuarios consulta = (ConsultaUsuarios)vc.elementAt (i);
            this.tConsultas.put (consulta.getDescripcion (), consulta.getCodigo ()+"");
        }
    }
    
    /**
    * Getter for property vConsultas.
    * @return Value of property vConsultas.
    */
    public java.util.Vector getVConsultas () {
        return vConsultas;
    }
    
    /**
    * Setter for property vConsultas.
    * @param vConsultas New value of property vConsultas.
    */
    public void setVConsultas (java.util.Vector vConsultas) {
        this.vConsultas = vConsultas;
    }
    
    /**
    * Getter for property consulta.
    * @return Value of property consulta.
    */
    public com.tsp.operation.model.beans.ConsultaUsuarios getConsulta () {
        return consulta;
    }
    
    /**
    * Setter for property consulta.
    * @param consulta New value of property consulta.
    */
    public void setConsulta (com.tsp.operation.model.beans.ConsultaUsuarios consulta) {
        this.consulta = consulta;
    }
    
    /**
    * retorna false si no existe el nombre de la tabla en la seleccion de campos
    * @autor David Lamadrid
    * @throws SQLException
    * @version 1.0
    */
    public boolean verificarCampos (String nombreTabla){
        boolean sw=false;
        if (this.vCampos != null){
            for(int i=0;i<vCampos.size ();i++){
                Vector fila=(Vector)vCampos.elementAt (i);
                String nombre = ""+fila.elementAt (0);
                //////System.out.println("VCAMPOS "+nombre);
                nombre = nombre.substring (0,nombre.indexOf ("."));
                if(nombre.equals (nombreTabla)){
                    sw=true;
                }
            }
        }
        return sw;
    }
    
    /**
    * Metodo que setea un vector con los campos y el tipo de campos de una tabla
    * @autor David Lamadrid
    * @throws SQLException
    * @version 1.0
    */
    public void obtenerCampos (String nombreTabla)throws Exception {
        PoolManager poolManager = PoolManager.getInstance ();
        Connection conPostgres  = poolManager.getConnection ("fintra");
        this.vCampos = new Vector ();
        if (conPostgres == null)
            throw new SQLException ("Sin conexion");
        PreparedStatement st=null;
        try{
            DatabaseMetaData dbmd = conPostgres.getMetaData ();
            ResultSet rs = dbmd.getColumns (null, null, nombreTabla, null);
            
            boolean seguir = rs.next ();
            while( seguir ) {
                String nombre = ""+rs.getString ("COLUMN_NAME");
                String tipo = ""+rs.getString ("TYPE_NAME");
                Vector fila = new Vector ();
                fila.add (nombre);
                fila.add (tipo);
                this.vCampos.add (fila);
                seguir = rs.next ();
            }
            
        }catch(Exception e){
            throw new SQLException (" No se pudo crear la tabla : h_trafico -->" + e.getMessage () );
        }
        finally{
            if(st!=null) st.close ();
            poolManager.freeConnection ("fintra",conPostgres);
        }
    }
    
    /**
    * Getter for property vCampos.
    * @return Value of property vCampos.
    */
    public java.util.Vector getVCampos () {
        return vCampos;
    }
    
    /**
    * Setter for property vCampos.
    * @param vCampos New value of property vCampos.
    */
    public void setVCampos (java.util.Vector vCampos) {
        this.vCampos = vCampos;
    }
    
    /**
    * Getter for property vConuslta.
    * @return Value of property vConuslta.
    */
    public java.util.Vector getVConuslta () {
        return vConuslta;
    }
    
    /**
    * Setter for property vConuslta.
    * @param vConuslta New value of property vConuslta.
    */
    public void setVConuslta (java.util.Vector vConuslta) {
        this.vConuslta = vConuslta;
    }
}
