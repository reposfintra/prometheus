/***********************************************************************************
 * Nombre clase : ............... DespachoDAO.java                                *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD de la accion de despacho.              *
 * Autor :....................... Ing.Karen Reales                                 *
 * Fecha :........................Enero 30 de 2006, 05:15 PM                       *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

public class DespachoDAO {
    
    private String dataBaseName  = "fintra";
    /** Creates a new instance of ActividadDAO */
    public DespachoDAO() {
    }
    public DespachoDAO(String dataBaseName) {
        this.dataBaseName = dataBaseName;
    }
    
    
    /**
     * Metodo InsertarDespacho, ingresa un registros en las diferentes tablas.
     * @param:
     * @autor : Ing. Karen Reales.
     * @version : 1.0
     */
    public void insertar(Vector comandos) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            if (con != null){
                
                con.setAutoCommit(false);
                Statement stmt = con.createStatement();
                for(int i=0; i<comandos.size();i++){
                    String comando =(String) comandos.elementAt(i);
                    ////System.out.println(comando);
                    stmt.executeUpdate(comando);
                    
                }
                
                con.commit();
            }
        }catch (SQLException e) {
            // Any error is grounds for rollback
            try {
                
                con.rollback();
            }
            catch (SQLException ignored) { 
                throw new SQLException("NO SE PUDO HACER ROLLBACK " + ignored.getMessage() + " " + ignored.getErrorCode()+" <br> La siguiente exception es : ----"+ignored.getNextException());
            }
            throw new SQLException("ERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS " + e.getMessage() + " " + e.getErrorCode()+" <br> La siguiente exception es : ----"+e.getNextException());
        }
        finally {
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        // Clean up.
        try {
            if (con != null) con.close();
        }
        catch (SQLException ignored) {
            
        }
    }
    
    
}
