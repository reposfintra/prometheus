/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.DAOS;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author maltamiranda
 */
public class SectorSubsectorDAO extends MainDAO {

    public SectorSubsectorDAO() {
        super("SectorSubsectorDAO.xml");
    }
    public SectorSubsectorDAO(String dataBaseName) {
        super("SectorSubsectorDAO.xml", dataBaseName);
    }


    public ArrayList getSectores(String bd)throws Exception {
        ArrayList sectores=null;
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        try{
            con=conectarJNDI("SQL_GET_SECTORES",bd);
            String sql=this.obtenerSQL("SQL_GET_SECTORES");
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            sectores=rstotbl(rs);
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println(e.toString());
        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(ps!=null){ps.close();}
            if(rs!=null){rs.close();}
        }
        return sectores;
    }

    public ArrayList rstotbl (ResultSet rs) throws Exception {
        ArrayList tabla=new ArrayList();
        while(rs.next()){
           ArrayList tabla2=new ArrayList();
           for(int i=1;i<=rs.getMetaData().getColumnCount();i++)
           {    tabla2.add(rs.getString(i));
           }
           tabla.add(tabla2);
        }
        return tabla;
    }

    public ArrayList getSubSectores(String id, String bd)throws Exception {
        ArrayList subsectores=null;
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        try{
            con=conectarJNDI("SQL_GET_SUBSECTORES",bd);
            String sql=this.obtenerSQL("SQL_GET_SUBSECTORES");
            ps=con.prepareStatement(sql);
            ps.setString(1, id);
            rs=ps.executeQuery();
            subsectores=rstotbl(rs);
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println(e.toString());
        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(ps!=null){ps.close();}
            if(rs!=null){rs.close();}
        }
        return subsectores;
    }

    public String agregar_sector(String nombre, String detalle, String codigo,String activo, String usuario, String bd, String reliquida, String codigo_alterno, String nombre_alterno)throws Exception {
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        try{
            con=conectarJNDI("SQL_INSERT_SECTORES",bd);
            String sql=this.obtenerSQL("SQL_INSERT_SECTORES");
            ps=con.prepareStatement(sql);
            ps.setString(1, nombre);
            ps.setString(2, detalle);
            ps.setString(3, codigo);
            ps.setString(4, activo);
            ps.setString(5, reliquida);
            ps.setString(6, codigo_alterno);
            ps.setString(7, usuario);
            ps.setString(8, nombre_alterno);
            ps.execute();
        }
        catch(Exception e){
            codigo="ERROR: Porfavor verifique los datos ingresados, recuerde que los codigos deben ser unicos...";
            e.printStackTrace();
            System.out.println(e.toString());
        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(ps!=null){ps.close();}
            if(rs!=null){rs.close();}
        }
        return codigo;
    }
    public String agregar_subsector(String nombre, String detalle, String codigo,String activo, String id, String usuario, String bd, String nombre_alterno) throws SQLException{
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        try{
            con=conectarJNDI("SQL_INSERT_SUBSECTORES",bd);
            String sql=this.obtenerSQL("SQL_INSERT_SUBSECTORES");
            ps=con.prepareStatement(sql);
            ps.setString(1, id);
            ps.setString(2, nombre);
            ps.setString(3, detalle);
            ps.setString(4, codigo);
            ps.setString(5, activo);
            ps.setString(6, usuario);
            ps.setString(7, nombre_alterno);
            ps.execute();
        }
        catch(Exception e){
            codigo="ERROR: Porfavor verifique los datos ingresados, recuerde que los codigos deben ser unicos...";
            e.printStackTrace();
            System.out.println(e.toString());
        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(ps!=null){ps.close();}
            if(rs!=null){rs.close();}
        }
        return codigo;
    }

    public void editar_sector(String id, String nombre, String detalle,String activo, String usuario, String bd, String reliquida, String codigo_alterno, String nombre_alterno)throws Exception {
        Connection con=null;
        PreparedStatement ps=null;
        try{
            con=conectarJNDI("SQL_UPDATE_SECTORES",bd);
            String sql=this.obtenerSQL("SQL_UPDATE_SECTORES");
            ps=con.prepareStatement(sql);
            ps.setString(1, nombre);
            ps.setString(2, detalle);
            ps.setString(3, activo);
            ps.setString(4, reliquida);
            ps.setString(5, codigo_alterno);
            ps.setString(6, usuario);
            ps.setString(7, nombre_alterno);
            ps.setString(8, id);
            ps.execute();
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println(e.toString());
        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(ps!=null){ps.close();}
        }
    }
    public void editar_subsector(String id, String idsub, String nombre, String detalle,String activo, String usuario, String bd, String nombre_alterno)throws Exception {
        Connection con=null;
        PreparedStatement ps=null;
        try{
            con=conectarJNDI("SQL_UPDATE_SUBSECTORES",bd);
            String sql=this.obtenerSQL("SQL_UPDATE_SUBSECTORES");
            ps=con.prepareStatement(sql);
            ps.setString(1, nombre);
            ps.setString(2, detalle);
            ps.setString(3, activo);
            ps.setString(4, usuario);
           ps.setString(5, nombre_alterno);
            ps.setString(6, id);
            ps.setString(7, idsub);
            ps.execute();
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println(e.toString());
        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(ps!=null){ps.close();}
        }
    }
    public String [] buscar_sector(String id, String bd)throws Exception {
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        String [] vec=new String[7];
        try{
            con=conectarJNDI("SQL_SEARCH_SECTORES",bd);
            String sql=this.obtenerSQL("SQL_SEARCH_SECTORES");
            ps=con.prepareStatement(sql);
            ps.setString(1, id);
            rs=ps.executeQuery();
            if(rs.next()){
                vec[0]=rs.getString("nombre");
                vec[1]=rs.getString("descripcion");
                vec[2]=rs.getString("cod_sector");
                vec[3]=rs.getString("reg_status");
                vec[4]=rs.getString("reliquida");
                vec[5]=rs.getString("codigo_alterno");
                vec[6]=rs.getString("nombre_alterno");
            }
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println(e.toString());
        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(ps!=null){ps.close();}
        }
        return vec;
    }
    public String [] buscar_subsector(String id,String idsub, String bd)throws Exception {
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        String [] vec=new String[5];
        try{
            con=conectarJNDI("SQL_SEARCH_SUBSECTORES",bd);
            String sql=this.obtenerSQL("SQL_SEARCH_SUBSECTORES");
            ps=con.prepareStatement(sql);
            ps.setString(1, id);
            ps.setString(2, idsub);
            rs=ps.executeQuery();
            if(rs.next()){
                vec[0]=rs.getString("nombre");
                vec[1]=rs.getString("descripcion");
                vec[2]=rs.getString("cod_subsector");
                vec[3]=rs.getString("reg_status");
                vec[4]=rs.getString("nombre_alterno");
                
            }
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println(e.toString());
        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(ps!=null){ps.close();}
        }
        return vec;
    }


}
