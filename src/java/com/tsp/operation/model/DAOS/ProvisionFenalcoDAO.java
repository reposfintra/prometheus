/*
 * ProvisionFenalcoDAO.java
 *
 * Created on 17 de junio de 2008, 04:27 PM
 */
package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.lang.*;
import com.tsp.operation.model.TransaccionService;
/**
 *
 * @author  jemartinez
 */
public class ProvisionFenalcoDAO extends MainDAO {
    
    private static final String INFO_TITULOS_VALOR = "INFO_TITULOS_VALOR";//Informacion de los titulos concordantes con la informacion de fintra
    private static final String INFO_FACTURAS = "INFO_FACTURAS";//Informacion de las facturas y negocios de fintra
    private static final String VALOR_CREDITO = "VALOR_CREDITO";//Valor credito cuenta 23352001
    private static final String ACTUALIZAR_PROVISION = "ACTUALIZAR_PROVISION";//Actualiza el estado de un titulo despues de pagado a fenalco
    
    private static final String INFO_TITULOS_VALOR_ALL = "INFO_TITULOS_VALOR_ALL";//INFORMACION DE LOS TITULOS COMPLETA
    private static final String INFO_FACTURAS_ALL = "INFO_FACTURAS_ALL";//INFORMACION DE LOS TITULOS CON LIMIT Y OFFSET
    
    private static final String LISTAR_AVALES  = "LISTAR_AVALES";//LISTA DE LOS AVALES EN LA BASE DE DATOS
    
    private static final String INSERTAR_CXP_ITEM  = "INSERTAR_CXP_ITEM";//INSERTAR ITEM EN CXP
    private static final String INSERTAR_CXP_DOC = "INSERTAR_CXP_DOC";//INSERTAR CXP
    
    private static final String CONTAR_TITULOS_FENALCO = "CONTAR_TITULOS_FENALCO";//CANTIDAD DE TITULOS DE FENALCO CONCORDANTES CON EL CRUCE FENALCO-NEGOCIOS
    private static final String CONTAR_TITULOS_FINTRA = "CONTAR_TITULOS_FINTRA";// A PARTIR DE UN NUMERO DE AVAL CUENTA LA CANTIDAD DE TITULOS EN con.factura QUE CONCUERDAN LA INFORMACION ENVIADA POR FENALCO public.provision_fenalco
    
    private static final int PROVISION_FENALCO = 1;//Consulta CONTAR_PROVISION_FINTRA
    private static final int PROVISION_FINTRA = 2;//Consulta CONTAR_PROVISION_FENALCO
    
    private static final String INFO_PROVEEDOR = "INFO_PROVEEDOR";
    private static final String VALOR_PROVISION_CXP_DOC = "VALOR_PROVISION_CXP_DOC";
    private static final String ACTUALIZAR_CXP_DOC = "ACTUALIZAR_CXP_DOC";
    private static final String LISTADO_PROVISION_SIN_CONCORDAR = "LISTADO_PROVISION_SIN_CONCORDAR";
    
    private BeanGeneral beanGeneral = new BeanGeneral();
    
    public void setBeanTitulos(BeanGeneral beanGeneral){
        this.beanGeneral = beanGeneral;
    }
    
    public BeanGeneral getBeanTitulos(){
        return this.beanGeneral;
    }
    
    public ProvisionFenalcoDAO(){     
        super("ProvisionFenalcoDAO.xml");
    }
    
    /**
     * Este metodo consigue la informacion de los titulos 
     * @param tipo fenalco o fintra 
     * @return un Vector con la informacion de la tabla
     * @throws Exception si ocurre un error en la base de datos
     */
    
    public String[] getInfoTitulos(int tipo) throws java.lang.Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet result = null;
        String[] table = null;
        String query = "";//JJCastro fase2

        try{
            if(tipo == PROVISION_FENALCO){
                query = "INFO_TITULOS_VALOR_ALL";
            }else{
                query = "INFO_FACTURAS_ALL";
            }
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2

            result = st.executeQuery();
            
            result.last();
            table = new String[result.getRow()];
            result.beforeFirst();
            
            int i = 0;
            
            while(result.next()){                
                if(tipo==PROVISION_FENALCO)
                    table[i] = result.getString(1)+";"+result.getString(2)+";"+result.getString(3)+";"+result.getString(4)+";"+result.getString(5)+";"+result.getString(6)+";"+result.getString(7)+";"+result.getString(8)+";"+result.getString(9)+";"+result.getString(10)+";"+result.getString(11)+";"+result.getString(12)+";"+result.getString(13);
                else
                     table[i] = result.getString(1)+";"+result.getString(2)+";"+result.getString(3)+";"+result.getString(4)+";"+result.getString(5)+";"
                +result.getString(6)+";"+result.getString(7)+";"+result.getString(8)+";"+result.getString(9)+";"+result.getString(10)+";"+result.getString(11);
                
                i++;
            }
            
            }}catch(java.lang.Exception e){
            throw new Exception("DAO: "+e.toString());
        }finally{//JJCastro fase2
            if (result  != null){ try{ result.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return table;
    }
    
    
     /**
     * Este metodo consigue la informacion de los titulos con limit y offset
     * @param tipo fenalco o fintra 
     * @param limit limite de la consulta
     * @param offset indice de inicio
     * @return un Vector con la informacion de la tabla
     * @throws Exception si ocurre un error en la base de datos
     */
    public String[] getInfoTitulos(int tipo, int limit, int offset) throws java.lang.Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement state = null;
        ResultSet result = null;
        String[] table = null;
        String query = "INFO_TITULOS_VALOR";

        try{
            con = this.conectarJNDI(query);
            if (con != null) {

                                if(tipo == PROVISION_FENALCO){
                                                query = "INFO_TITULOS_VALOR";
                                                state = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                                                state.setInt(1,limit);
                                                state.setInt(2,offset);
                                }else{
                                                query = "INFO_FACTURAS";
                                                state = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                                                state.setInt(1,limit);
                                                state.setInt(2,offset);
                                }
                                                result = state.executeQuery();
                                                result.last();
                                                table = new String[result.getRow()];
                                                result.beforeFirst();

                                int i = 0;

                                while(result.next()){

                                                if(tipo==PROVISION_FENALCO)
                                                    table[i] = result.getString(1)+";"+result.getString(2)+";"+result.getString(3)+";"+result.getString(4)+";"+result.getString(5)+";"+result.getString(6)+";"+result.getString(7)+";"+result.getString(8)+";"+result.getString(9)+";"+result.getString(10)+";"+result.getString(11)+";"+result.getString(12)+";"+result.getString(13);
                                                else
                                                     table[i] = result.getString(1)+";"+result.getString(2)+";"+result.getString(3)+";"+result.getString(4)+";"+result.getString(5)+";"
                                                +result.getString(6)+";"+result.getString(7)+";"+result.getString(8)+";"+result.getString(9)+";"+result.getString(10)+";"+result.getString(11);

                                                i++;
                                }
            
            } }catch(java.lang.Exception e){
            throw new Exception("DAO: "+e.toString());
        }finally{//JJCastro fase2
            if (result  != null){ try{ result.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (state  != null){ try{ state.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return table;
    }

    /**
     * Actualiza el estado de un titulo despues de haber generado la CXP a fenalco
     * @param noFactura lista de las facturas a actualizar
     * @param noDoc numero de Factura de Fenalco
     * @return mensaje de exito o error en la operacion
     * @throws Exception si ocurre un error en la base de datos
       Belleza5
     */

/*    public String setEstadoProvision(String[] noFactura, String noDoc) throws java.lang.Exception{

        PreparedStatement state = null;
         
        for(int i=0;i<noFactura.length;i++){
            try{
                state = this.crearPreparedStatement(ACTUALIZAR_PROVISION);
                state.setString(1, noDoc);
                state.setString(2, (noFactura[i].split(";"))[6]);
                //System.out.println("DAO: "+ (noFactura[i].split(";"))[6]);
                state.executeUpdate();       
            }catch(java.lang.Exception e){
                throw new java.lang.Exception("DAO: "+e.toString());
            }finally{
                if(state!=null) state.close();
                this.desconectar(VALOR_CREDITO);
            }
        }

        return "Exito al actualizar estado de titulo";
    }*/



        /**
     * Actualiza el estado de un titulo despues de haber generado la CXP a fenalco
     * @param noFactura lista de las facturas a actualizar
     * @param noDoc numero de Factura de Fenalco
     * @return mensaje de exito o error en la operacion
     * @throws Exception si ocurre un error en la base de datos
     */
    public String setEstadoProvision(String[] noFactura, String noDoc) throws java.lang.Exception{
        Connection con = null;
        PreparedStatement state = null;
        String query = "ACTUALIZAR_PROVISION";
        String consulta = "";

        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
                consulta = this.obtenerSQL(query);
                for (int i = 0; i < noFactura.length; i++) {
                    state = con.prepareStatement(consulta);//JJCastro fase2
                    state.setString(1, noDoc);
                    state.setString(2, (noFactura[i].split(";"))[6]);
                    state.executeUpdate();
                }
            }
        } catch (java.lang.Exception e) {
            throw new java.lang.Exception("DAO: " + e.toString());
        }finally{//JJCastro fase2
            if (state  != null){ try{ state.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return "Exito al actualizar estado de titulo";
    }

    
     /**
     * Lista los avales de Fenalco
      *@return lista con los avales de fenalco
      *@throws Exception si ocurre un error en la base de datos
     */
    public String[] getAvales() throws java.lang.Exception{
        Connection con = null;
        PreparedStatement state = null;
        ResultSet result = null;
        String[] avales = null;
        String query = "LISTAR_AVALES";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                state = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                result = state.executeQuery();
                result.last();
                avales = new String[result.getRow()];
                result.beforeFirst();
                int i = 0;
                while (result.next()) {
                    avales[i] = result.getString(1);
                    i++;
                }

            }
        } catch (java.lang.Exception e) {
        }finally{//JJCastro fase2
            if (result  != null){ try{ result.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (state != null){ try{ state.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return avales;
    }
    
    
    /**
     * Cantidad de registros por numero de aval
     * @param tipo cantidad en la lista de fenalco o en la lista de fintra
     * @param noAval numero de aval
     * @return cantidad de registros
     * @throws Exception si ocurre un error en la base de datos
     */
    public int getCountProvisionXAval(int tipo, String noAval) throws java.lang.Exception{
        Connection con = null;
        PreparedStatement state = null;
        ResultSet result = null;
        int count = 0;
        String query = "CONTAR_TITULOS_FENALCO";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            if(tipo==PROVISION_FENALCO){
                state = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                 state.setString(1, noAval);
            }else{
                query = "CONTAR_TITULOS_FINTRA";
                state = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                state.setString(1,noAval);
            }
            result = state.executeQuery();
            
            if(result.next()){
                count = result.getInt(1);
                //System.out.println("DAO entre: "+count);
            }
            
            }}catch(java.lang.Exception e){
            throw new java.lang.Exception("DAO: "+e.toString());
        }finally{//JJCastro fase2
            if (result  != null){ try{ result.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (state != null){ try{ state.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return count;
    }
    
    
    /**
     * Este metodo inserta en la tabla cxp_items_doc, la informacion de cada titulo de fenalco
     * @param titulos Un array con la informacion de cada titulo separada por el comodin ';'
     * @throws Exception Cuando ocurre un error en la base de datos
     */
    public void insertarCxpItemsDoc(String[] titulos) throws java.lang.Exception{
        Connection con = null;
        PreparedStatement state = null;
        String query = "INSERTAR_CXP_ITEM";//JJCastro fase2

        try {
            con = this.conectarJNDI(query);
            if (con != null) {

                for (int i = 0; i < titulos.length; i++) {
                    //nit_proveedor;item;no_factura;provision_fenalco+iva;usuario;num_aval;factura;nombre_proveedor;
                    String[] temp = titulos[i].split(";");

                    state = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                    state.setString(1, temp[0]);//Proveedor
                    state.setString(2, temp[2]);//Item
                    state.setString(3, temp[1]);//Documento
                    state.setString(4, temp[5] + "  " + temp[6] + "  " + temp[7]);//Descripcion
                    state.setDouble(5, Double.parseDouble(temp[3]));//Valor
                    state.setDouble(6, Double.parseDouble(temp[3]));//Valor moneda Extranjera
                    state.setString(7, temp[4]); //User Update
                    state.setString(8, temp[4]);// Creation User
                    state.executeUpdate();
                    //System.out.println("Exito cxp_items_doc");
                }
            }
        }catch(java.lang.Exception e){
                throw new java.lang.Exception("DAO: "+e.toString());
            }finally{//JJCastro fase2
            if (state != null){ try{ state.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    /**
     * Inserta una CXP en la tabla fin.cxp_doc
     * @param informacion un String con la informacion de la CXP separada por el comodin ";"
     * @throws Exception si ocurre un error en la base de datos
     */
   //nit, item, no_factura, provision+iva, usuario, numero_aval, factura, nombre_proveedor,fecha_factura,fecha_vencimiento, suma_provision+iva, banco, sucursal, agencia, plazo
    public void insertarCxpDoc(String informacion) throws java.lang.Exception{
        Connection con = null;
        PreparedStatement state = null;
        String[] temp = informacion.split(";");
        String query = "INSERTAR_CXP_DOC";//JJCastro fase2
 
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            state = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2

            state.setString(1,temp[0]);//NIT PROVEEDOR
            state.setString(2,temp[2]);//FACTURA FENALCO
            state.setString(3,"Pago avales Fenalco a: "+temp[8]);//NUM_AVAL, FACTURA, NOMBRE_PROVEEDOR
            state.setString(4,temp[13]);//AGENCIA
            state.setString(5,temp[4]);//USUARIO
            state.setString(6,temp[11]);//BANCO
            //state.setString(7, temp[12]);//SUCURSAL
            state.setString(7, "CPAG");//SUCURSAL
            state.setString(8, temp[10]);//SUMA PROVISION
            state.setString(9,temp[10] );//SUMA PROVISION
            state.setString(10,temp[10] );//SUMA PROVISION
            state.setString(11,temp[10] );//SUMA PROVISION
            state.setString(12,temp[10] );//SUMA PROVISION
            state.setString(13,temp[10] );//SUMA PROVISION
            state.setString(14,temp[4]);//USUARIO
            state.setString(15,temp[4]);//USUARIO
            state.setString(16,temp[8]);//FECHA FACTURA
            state.setString(17,temp[9]) ;//FECHA VENCIMIENTO
            state.executeUpdate();
            //System.out.println("Exito cxp_doc");
        }}catch(java.lang.Exception e){
            throw new java.lang.Exception("DAO: "+e.toString());
        }finally{//JJCastro fase2
            if (state != null){ try{ state.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    
    /**
     * Obtiene el banco, la sucursal y la agencia de Fenalco
     * @return un String separado por ; con la informacion
     * @throws Exception si ocurre un error en la base de datos
     */
    public String obtenerInformacionProveedor() throws java.lang.Exception{
        Connection con = null;
        PreparedStatement state = null;
        ResultSet result = null;
        String informacion = null;
        String query = "INFO_PROVEEDOR";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
             state = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            result = state.executeQuery();
            if(result.next()){
                //banco;sucursal;agencia
                informacion = result.getString(1)+";"+result.getString(2)+";"+result.getString(3);
            }
        }}catch(java.lang.Exception e){
            throw new java.lang.Exception("DAO: "+e.toString());
        }finally{//JJCastro fase2
            if (result  != null){ try{ result.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (state  != null){ try{ state.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return informacion;
    }
    
    /**
     * Obtiene el valor hasta el momento de una CXP_DOC
     * @param noFactura numero de la factura
     * @return informacion con el valor
     * @throws Exception si ocurre un error en la base de datos
     */
    
    public String obtenerValorCxpDoc(String noFactura) throws java.lang.Exception{
        Connection con = null;
        PreparedStatement state = null;
        ResultSet result = null;
        String informacion = null;
        String query = "VALOR_PROVISION_CXP_DOC";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            state = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            state.setString(1, noFactura);
            result = state.executeQuery();
            if(result.next()){
                informacion = result.getString(1);
            }
        }}catch(java.lang.Exception e){
            throw new java.lang.Exception("DAO: "+e.toString());
        }finally{//JJCastro fase2
            if (result  != null){ try{ result.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (state  != null){ try{ state.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return informacion;
    }
    
    /**
     * Actualiza el valor de la cxp
     * @param noFactura el numero de la factura de Fenalco
     * @param valor el valor a actualizar
     * @throws Exception si ocurre un error en la base de datos
     */
    
    public void actualizarValorCxpDoc(String noFactura, String valor) throws java.lang.Exception{
        Connection con = null;
        PreparedStatement state = null;
        ResultSet result = null;
        String informacion = null;
        String query = "ACTUALIZAR_CXP_DOC";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            state = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            state.setString(1, valor);
            state.setString(2, valor);
            state.setString(3, valor);
            state.setString(4, valor);
            state.setString(5, valor);
            state.setString(6, valor);
            state.setString(7, noFactura);
            state.executeUpdate();
        }}catch(java.lang.Exception e){
            throw new java.lang.Exception("DAO: "+e.toString());
        }finally{//JJCastro fase2
            if (state  != null){ try{ state.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Obtiene el listado  de la informacion sin concordar
     * @return una lista con toda la informacion de los titulos que no concuerdan
     * @throws Exception si ocurre un error en la base de datos
     */
    public String[] obtenerListadoFenalcoSinConcordar() throws java.lang.Exception{
        Connection con = null;
        PreparedStatement state = null;
        ResultSet result = null;
        String[] informacion = null;
        String query = "LISTADO_PROVISION_SIN_CONCORDAR";//JJCastro fase2

        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            state = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            result = state.executeQuery();
            result.last();
            informacion = new String[result.getRow()];
            result.beforeFirst();
            int i=0;
            while(result.next()){
                informacion[i]= result.getString(1)+";"+result.getString(2)+";"+result.getString(3)+";"+result.getString(4)+";"+result.getString(5)+";"+result.getString(6)+";"+result.getString(7)+";"+result.getString(8)+";"+result.getString(9);
                i++;
            }

        }}catch(java.lang.Exception e){
            throw new java.lang.Exception("DAO: "+e.toString());
        }finally{//JJCastro fase2
            if (result  != null){ try{ result.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (state  != null){ try{ state.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return informacion;
    } 
    
}
