 /***************************************
    * Nombre Clase ............. RevisionSoftwareDAO.java
    * Descripci�n  .. . . . . .  Generamos sql para la revision de software
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  12/01/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/


package com.tsp.operation.model.DAOS;



import java.io.*;
import java.util.*;
import java.sql.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;


public class RevisionSoftwareDAO {
    
    
 /** ____________________________________________________________________________________________
                                              ATRIBUTOS
     ____________________________________________________________________________________________ */
    

    
    public static final String SEPARADOR                   =    "-"; 
    
    
    public static  String SQL_EQUIPOS_PROGRAMAS            =    " SELECT  distinct(equipo) FROM  inv.programa  "; 
    
    
    public static  String SQL_SITIOS_EQUIPO_PROGRAMAS      =  
                                                               " SELECT  equipo, sitio_revision, count(*)  "+ 
                                                               " FROM    inv.programa                      "+ 
                                                               " GROUP BY 1,2                              "; 

    
    public static  String SQL_PROGRAMAS                    =    
                                                                " SELECT                   "+
                                                                "        codigo_programa,  "+
                                                                "        nombre,           "+
                                                                "        programador_asig, "+
                                                                "        equipo,           "+
                                                                "        sitio_revision,   "+
                                                                "        substr(creation_date,1,19) as creation_date,    "+
                                                                "        creation_user     "+
                                                                " FROM                     "+
                                                                "       inv.programa       "+ 
                                                                " WHERE                    "+ 
                                                                "      equipo         = ?  "+
                                                                "  AND sitio_revision = ?  ";

    
     public static  String SQL_REVISIONES_PROGRAMAS       =      
     
                                                                " SELECT                       "+
                                                                "   dstrct           ,         "+
                                                                "   tipo_reg         ,         "+
                                                                "   equipo           ,         "+
                                                                "   sitio            ,         "+
                                                                "   ruta             ,         "+
                                                                "   nombre_archivo   ,         "+
                                                                "   extension        ,         "+
                                                                "   fecha_revision   ,         "+
                                                                "   usuario_revision ,         "+
                                                                "   tipo_revision    ,         "+
                                                                "   observacion      ,         "+
                                                                "   aprobado                   "+
                                                                " FROM  inv.revision           "+
                                                                " WHERE                        "+
                                                                "       tipo_reg       = 'P'   "+
                                                                "   and equipo         = ?     "+
                                                                "   and sitio          = ?     "+
                                                                "   and nombre_archivo = ?     "+
                                                                "ORDER BY fecha_revision DESC  ";

    
    
     
     public static  String SQL_SAVE_REVISION       =       
     
                                                    " INSERT INTO  inv.revision  "+
                                                    " (                          "+
                                                    "   dstrct,                  "+
                                                    "   tipo_reg,                "+ 
                                                    "   equipo,                  "+
                                                    "   sitio,                   "+
                                                    "   ruta,                    "+
                                                    "   nombre_archivo,          "+
                                                    "   extension,               "+ 
                                                    "   fecha_revision,          "+
                                                    "   usuario_revision,        "+
                                                    "   tipo_revision,           "+
                                                    "   observacion,             "+
                                                    "   aprobado ,               "+
                                                    "   creation_user            "+
                                                    " )                          "+
                                                    " VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?) ";

     
     
     
      public static  String SQL_ARCHIVOS_PROGRAMAS       =  
      
                                                          " SELECT                                   "+
                                                          "          b.codigo_programa,              "+
                                                          "          c.nombre,                       "+
                                                          "          c.programador_asig,             "+
                                                          "          a.dstrct,                       "+
                                                          "          a.equipo,                       "+
                                                          "          a.sitio,                        "+
                                                          "          a.ruta,                         "+
                                                          "          a.nombre_archivo,               "+
                                                          "          a.extension,                    "+
                                                          "          a.tamano,                       "+
                                                          "          a.fecha,                        "+
                                                          "          a.fecha_modificado,             "+
                                                          "          a.tamano_modificado,            "+
                                                          "          a.fecha_eliminacion             "+
                                                          "  FROM                                    "+
                                                          "      inv.inventario_software  a,         "+
                                                          "      inv.programa_inventario  b,         "+
                                                          "      inv.programa             c           "+
                                                          "  WHERE                                   "+
                                                          "       b.equipo          = ?              "+
                                                          "   and b.sitio           = ?              "+
                                                          "   and b.codigo_programa = ?              "+
                                                          "   and b.equipo          = a.equipo       "+
                                                          "   and b.sitio           = a.sitio        "+
                                                          "   and b.ruta            = a.ruta         "+ 
                                                          "   and b.nombre_archivo  = a.nombre_archivo  "+
                                                          "   and b.extension       = a.extension       "+
                                                          "   and c.equipo          = b.equipo          "+
                                                          "   and c.sitio_revision  = b.sitio           "+
                                                          "   and c.codigo_programa = b.codigo_programa "+
                                                          "ORDER BY  a.nombre_archivo ,  a.extension    "; 

     
     
     
      
      
       public static  String SQL_REVISIONES_ARCHIVOS       =      
     
                                                                " SELECT                       "+
                                                                "   dstrct           ,         "+
                                                                "   tipo_reg         ,         "+
                                                                "   equipo           ,         "+
                                                                "   sitio            ,         "+
                                                                "   ruta             ,         "+
                                                                "   nombre_archivo   ,         "+
                                                                "   extension        ,         "+
                                                                "   fecha_revision   ,         "+
                                                                "   usuario_revision ,         "+
                                                                "   tipo_revision    ,         "+
                                                                "   observacion      ,         "+
                                                                "   aprobado                   "+
                                                                " FROM  inv.revision           "+
                                                                " WHERE                        "+
                                                                "       tipo_reg       = 'A'   "+
                                                                "   and equipo         = ?     "+
                                                                "   and sitio          = ?     "+
                                                                "   and nombre_archivo = ?     "+
                                                                "   and ruta           = ?     "+
                                                                "   and extension      = ?     "+
                                                                "ORDER BY fecha_revision DESC  ";

       
       
       
     
     
    
  /** ____________________________________________________________________________________________
                                              METODOS
     ____________________________________________________________________________________________ */
    

    
    
    public RevisionSoftwareDAO() {}
    
    
    
    
    
    
    
     /**
     * Busca los diferentes equipos establecidos en la tabla programa
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public List searchEquipos() throws Exception{
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("inventario");
          if (con == null)
                throw new SQLException("Sin conexion");
          PreparedStatement st        = null;
          ResultSet         rs        = null;
          List              lista     = new LinkedList();
          try {
              st= con.prepareStatement(this.SQL_EQUIPOS_PROGRAMAS);   
              rs=st.executeQuery();
              while(rs.next()){                  
                 String equipo = rs.getString(1);
                 lista.add(equipo);
              }
          }
          catch(SQLException e){
              throw new SQLException(" DAO:searchEquipos()  No se pudieron listar los Equipos -->"+ e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             if(rs!=null) rs.close();
             poolManager.freeConnection("inventario",con);
          } 
          return lista;
    }
     
     
     
     
     
     /**
     * Busca los diferentes sitios por equipos establecidos en la tabla programa
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public List searchSitios() throws Exception{
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("inventario");
          if (con == null)
                throw new SQLException("Sin conexion");
          PreparedStatement st        = null;
          ResultSet         rs        = null;
          List              lista     = new LinkedList();
          try {
              st= con.prepareStatement(this.SQL_SITIOS_EQUIPO_PROGRAMAS);   
              rs=st.executeQuery();
              while(rs.next()){                  
                 String equipo = rs.getString(1);
                 String sitio  = rs.getString(2);                 
                 lista.add(equipo + SEPARADOR + sitio);
              }
          }
          catch(SQLException e){
              throw new SQLException(" DAO:searchSitios()  No se pudieron listar los Sitios -->"+ e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             if(rs!=null) rs.close();
             poolManager.freeConnection("inventario",con);
          } 
          return lista;
    }
     
     
     
     
     
      /**
     * Busca los diferentes programas para un sitio y equipo
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public List searchProgram(String equipo, String sitio) throws Exception{
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("inventario");
          if (con == null)
                throw new SQLException("Sin conexion");
          PreparedStatement st        = null;
          ResultSet         rs        = null;
          List              lista     = new LinkedList();
          try {
              st= con.prepareStatement(this.SQL_PROGRAMAS);  
              st.setString(1, equipo);
              st.setString(2, sitio);
              rs=st.executeQuery();
              while(rs.next()){   
                 Programa  programa = new Programa();
                   programa.setPrograma    ( rs.getString("codigo_programa")  );
                   programa.setDescripcion ( rs.getString("nombre")           );
                   programa.setProgramador ( rs.getString("programador_asig") );
                   programa.setEquipo      ( rs.getString("equipo")           );
                   programa.setSitio       ( rs.getString("sitio_revision")   );                   
                   programa.setFecha       ( rs.getString("creation_date")    );
                   programa.setUser        ( rs.getString("creation_user")    );
                     
                 lista.add(programa);
              }
          }
          catch(SQLException e){
              throw new SQLException(" DAO:searchProgram()  No se pudieron listar los programas -->"+ e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             if(rs!=null) rs.close();
             poolManager.freeConnection("inventario",con);
          } 
          return lista;
    }
     
     
     
     
     
     /**
     * Busca las revisiones realizadas al programa
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public List searchRevision(Programa programa) throws Exception{
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("inventario");
          if (con == null)
                throw new SQLException("Sin conexion");
          PreparedStatement st        = null;
          ResultSet         rs        = null;
          List              lista     = new LinkedList();
          try {
              st= con.prepareStatement(this.SQL_REVISIONES_PROGRAMAS);
               st.setString(1, programa.getEquipo()   );
               st.setString(2, programa.getSitio()    );
               st.setString(3, programa.getPrograma() );
              rs=st.executeQuery();
              while(rs.next()){  
                  Revision  rev = new Revision();
                    rev.setDistrito    ( rs.getString("dstrct")            );
                    rev.setTipoRegistro( rs.getString("tipo_reg")          );
                    rev.setEquipo      ( rs.getString("equipo")            );
                    rev.setSitio       ( rs.getString("sitio")             );
                    rev.setRuta        ( rs.getString("ruta")              );
                    rev.setArchivo     ( rs.getString("nombre_archivo")    );
                    rev.setExtension   ( rs.getString("extension")         );
                    rev.setFecha       ( rs.getString("fecha_revision")    );
                    rev.setUser        ( rs.getString("usuario_revision")  );
                    rev.setTipoRevision( rs.getString("tipo_revision")     );
                    rev.setObservacion ( rs.getString("observacion")       );
                    rev.setAprovado    ( rs.getString("aprobado")          );                    
                    
                  lista.add(rev);                  
              }
              
          }
          catch(SQLException e){
              throw new SQLException(" DAO:searchProgram()  No se pudieron listar las revisiones del programas -->"+ e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             if(rs!=null) rs.close();
             poolManager.freeConnection("inventario",con);
          } 
          return lista;
    }
     
     
     
     
     
     
     /**
     * Graba la revision
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public void saveRevision(Revision rev) throws Exception{
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("inventario");
          if (con == null)
                throw new SQLException("Sin conexion");
          PreparedStatement st        = null;
          try {
              st= con.prepareStatement(this.SQL_SAVE_REVISION);
                  st.setString(1,  rev.getDistrito()    );
                  st.setString(2,  rev.getTipoRegistro());
                  st.setString(3,  rev.getEquipo()      );
                  st.setString(4,  rev.getSitio()       );
                  st.setString(5,  rev.getRuta()        );
                  st.setString(6,  rev.getArchivo()     );
                  st.setString(7,  rev.getExtension()   );
                  st.setString(8,  rev.getFecha()       );
                  st.setString(9,  rev.getUser()        );
                  st.setString(10, rev.getTipoRevision());
                  st.setString(11, rev.getObservacion() );
                  st.setString(12, rev.getAprovado()    );
                  st.setString(13, rev.getUser()        );
              st.execute();
          }
          catch(SQLException e){
              throw new SQLException(" DAO:saveRevision()  No se guardar la revision -->"+ e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             poolManager.freeConnection("inventario",con);
          } 
    }
     
     
     
     
     
     
     
     /**
     * Busca los archivos del programa
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public List searchFile(Programa programa) throws Exception{
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("inventario");
          if (con == null)
                throw new SQLException("Sin conexion");
          PreparedStatement st        = null;
          ResultSet         rs        = null;
          List              lista     = new LinkedList();
          try {
              st= con.prepareStatement(this.SQL_ARCHIVOS_PROGRAMAS);
               st.setString(1, programa.getEquipo()   );
               st.setString(2, programa.getSitio()    );
               st.setString(3, programa.getPrograma() );
              rs=st.executeQuery();
              int cont = 0;
              while(rs.next()){  
                     InventarioSoftware  inv  = new InventarioSoftware();
                       inv.setId         ( cont );
                       inv.setDstrct     ( rs.getString("dstrct")         );
                       inv.setEquipo     ( rs.getString("equipo")         );
                       inv.setSitio      ( rs.getString("sitio")          );
                       inv.setRuta       ( rs.getString("ruta")           );
                       inv.setNombre     ( rs.getString("nombre_archivo") );
                       inv.setExtension  ( rs.getString("extension")      );
                       inv.setTamano     ( rs.getLong  ("tamano")         );
                       inv.setFecha      ( rs.getDate  ("fecha")          );
                       inv.setFechaMod   ( rs.getString("fecha_modificado") );
                       inv.setTamanoMod  ( rs.getString("tamano_modificado"));
                       inv.setFechaDelete( rs.getString("fecha_eliminacion"));
                       
                       inv.setRevisiones( this.searchRevisionFile(inv) );
                       
                    lista.add(inv);
                    cont++;
              }
              
          }
          catch(SQLException e){
              throw new SQLException(" DAO:searchFile()  No se pudieron listar los archivos del programas -->"+ e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             if(rs!=null) rs.close();
             poolManager.freeConnection("inventario",con);
          } 
          return lista;
    }
     
     
     
     
     
     /**
     * Busca las revisiones realizadas de un archivo
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public List searchRevisionFile(InventarioSoftware  inv) throws Exception{
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("inventario");
          if (con == null)
                throw new SQLException("Sin conexion");
          PreparedStatement st        = null;
          ResultSet         rs        = null;
          List              lista     = new LinkedList();
          try {
              st= con.prepareStatement(this.SQL_REVISIONES_ARCHIVOS);
               st.setString(1, inv.getEquipo()   );
               st.setString(2, inv.getSitio()    );
               st.setString(3, inv.getNombre()   );
               st.setString(4, inv.getRuta()     );
               st.setString(5, inv.getExtension());   
              rs=st.executeQuery();
              while(rs.next()){  
                  Revision  rev = new Revision();
                    rev.setDistrito    ( rs.getString("dstrct")            );
                    rev.setTipoRegistro( rs.getString("tipo_reg")          );
                    rev.setEquipo      ( rs.getString("equipo")            );
                    rev.setSitio       ( rs.getString("sitio")             );
                    rev.setRuta        ( rs.getString("ruta")              );
                    rev.setArchivo     ( rs.getString("nombre_archivo")    );
                    rev.setExtension   ( rs.getString("extension")         );
                    rev.setFecha       ( rs.getString("fecha_revision")    );
                    rev.setUser        ( rs.getString("usuario_revision")  );
                    rev.setTipoRevision( rs.getString("tipo_revision")     );
                    rev.setObservacion ( rs.getString("observacion")       );
                    rev.setAprovado    ( rs.getString("aprobado")          );                    
                    
                  lista.add(rev);                  
              }
              
          }
          catch(SQLException e){
              throw new SQLException(" DAO:searchProgram()  No se pudieron las revisiones del archivo -->"+ e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             if(rs!=null) rs.close();
             poolManager.freeConnection("inventario",con);
          } 
          return lista;
    }
     
     
     
    
}
