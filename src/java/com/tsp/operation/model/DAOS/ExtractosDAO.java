/***************************************
 * Nombre Clase ............. ExtractosDAO.java
 * Descripci�n  .. . . . . .  Generamos Las corridas 
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  19/10/2005
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/



package com.tsp.operation.model.DAOS;



import java.io.*;
import java.util.*;
import java.sql.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;



public class ExtractosDAO extends MainDAO{
    
    /** ____________________________________________________________________________________________
     * ATRIBUTOS
     * ____________________________________________________________________________________________ */
    
    public  static String OFICINA_PPAL = "OP";
    
   
    private  FileWriter        fw;
    private  BufferedWriter    bf;
    private  PrintWriter       linea;
 
    /** ____________________________________________________________________________________________
     * METODOS
     * ____________________________________________________________________________________________ */
   
    public ExtractosDAO() {
      super("ExtractosDAO.xml");
    }
    public ExtractosDAO(String dataBaseName) {
      super("ExtractosDAO.xml", dataBaseName);
    }    
    
    /**
     * M�todo que setea valores nulos
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String reset(String val){
        if(val==null  )
            val = "";
        return val;
    }
    
    
    
    
     /**
     * M�todo que setea el url para generar el archivo de Error
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void initFile(String url)throws Exception{
        try{            
             this.fw         = new FileWriter    ( url      );
             this.bf         = new BufferedWriter( this.fw  );
             this.linea      = new PrintWriter   ( this.bf  );
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
    
    
     /**
     * M�todo que inserta informaci�n de la corrida
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String insertInfoCorrida( String distrito,
                                     String corrida,
                                     String tpago, 
                                     String tviaje,
                                     String fechacumini,
                                     String fechacumfin,
                                     String bancos,
                                     String fproveedor,
                                     String proveedores,
                                     String placas,
                                     String fechavenini,
                                     String fechavenfin,
                                     String usuario,
                                     String cheque_cero
                                  ) throws Exception{
        StringStatement st           =  null;
        String            queryDelete  =  "SQL_DELETE_INFOCORRIDA";
        String            queryInsert  =  "SQL_INSERT_INFOCORRIDA";
        String            sql          =  "";
        try {
            
            
            st= new StringStatement( this.obtenerSQL(queryDelete), true );//JJCastro fase2
            st.setString(1,  distrito     );
            st.setString(2,  corrida      );
            sql = st.getSql();//JJCastro fase2
            
            st= new StringStatement( this.obtenerSQL(queryInsert), true );//JJCastro fase2
            st.setString(1,  distrito     );
            st.setString(2,  corrida      );
            st.setString(3,  tpago        );
            st.setString(4,  tviaje       );            
            st.setString(5,  fechacumini  );
            st.setString(6,  fechacumfin  );
            st.setString(7,  bancos       );
            st.setString(8,  fproveedor   );
            st.setString(9,  proveedores  );
            st.setString(10, placas       );
            st.setString(11, fechavenini  );
            st.setString(12, fechavenfin  );
            st.setString(13, usuario      ); 
            st.setString(14, cheque_cero  );
            sql += st.getSql();//JJCastro fase2
            
            
            
        }catch(Exception e){
            throw new Exception(" DAO: insertInfoCorrida -->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st=null;            } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }
    
    
    
    
    
    
    
    
    
     /**
     * M�todo que valida la corrida a la cual se requiere incluir
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public Hashtable infoCorrida( String distrito, String corrida) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st           =  null;
        ResultSet         rs           =  null;
        String            query        =  "SQL_INFO_CORRIDAS";
        Hashtable         info         =  null; 
        try {
            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito );
            st.setString(2, corrida  );
            rs = st.executeQuery();
            if ( rs.next()  ){
                 info  =  new Hashtable();
                 info.put("dstrct",         reset(  rs.getString("dstrct")         )  ); 
                 info.put("corrida",        reset(  rs.getString("corrida")        )  ); 
                 info.put("tpago",          reset(  rs.getString("tpago")          )  ); 
                 info.put("tviaje",         reset(  rs.getString("tviaje")         )  );                 
                 info.put("fechacumini",    reset(  rs.getString("fechacumini")    )  );                 
                 info.put("fechacumfin",    reset(  rs.getString("fechacumfin")    )  );
                 info.put("bancos",         reset(  rs.getString("bancos")         )  );                 
                 info.put("fproveedor",     reset(  rs.getString("fproveedor")     )  );
                 info.put("proveedores",    reset(  rs.getString("proveedores")    )  );                 
                 info.put("placas",         reset(  rs.getString("placas")         )  );                 
                 info.put("fechavenini",    reset(  rs.getString("fechavenini")    )  );                 
                 info.put("fechavenfin",    reset(  rs.getString("fechavenfin")    )  ); 
                 info.put("creation_date",  reset(  rs.getString("creation_date")  )  );                 
                 info.put("creation_user",  reset(  rs.getString("creation_user")  )  );      
                 info.put("cheque_cero",    reset(rs.getString("cheque_cero")      )  );
                 List cheques = chequeCorrida( distrito, corrida);
                 info.put("cheques",        cheques  );
                 
            }
            
            
        }}catch(Exception e){
            throw new Exception(" DAO: infoCorrida -->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return info;
    }
    
    
    
    
     /**
     * M�todo que valida la corrida a la cual se requiere incluir
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public List chequeCorrida( String distrito, String corrida) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st           =  null;
        ResultSet         rs           =  null;
        String            query        =  "SQL_CHEQUES_CORRIDAS";
        List              lista        =  new LinkedList();
        try {
            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito );
            st.setString(2, corrida  );
            rs = st.executeQuery();
            while ( rs.next()  ){
                 Hashtable ch  =  new Hashtable();
                   ch.put("corrida",   reset( rs.getString("corrida")     )   );
                   ch.put("tipo_pago", reset( rs.getString("tipo_pago")   )   );
                   ch.put("banco",     reset( rs.getString("banco")       )   );
                   ch.put("sucursal",  reset( rs.getString("sucursal")    )   );
                   ch.put("fecha",     reset( rs.getString("fecha")       )   );
                   ch.put("user",      reset( rs.getString("user")        )   );                   
                   ch.put("cheque",    reset( rs.getString("cheque")      )   );
                 
                 lista.add( ch );
            }            
            
        }}catch(Exception e){
            throw new Exception(" DAO: chequeCorrida -->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    
    
    
    
    
    /**
     * M�todo que valida la corrida a la cual se requiere incluir
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String validarCorrida( String distrito, String corrida, String user) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st                  =  null;
        ResultSet         rs                  =  null;
        String            queryExistencia     =  "SQL_VALIDAR_EXISTENCIA_CORRIDA";
        String            queryNoCancelada    =  "SQL_VALIDAR_CORRIDA_NO_CANCELADA";
        String            msj                 =  "";
        try {
            
            int swExist  = 0;
            con = this.conectarJNDI(queryExistencia);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(queryExistencia));//JJCastro fase2
            st.setString(1, distrito );
            st.setString(2, corrida  );
            st.setString(3, user     );            
            rs = st.executeQuery();
            
            System.out.println("SQL EXIST" + st.toString());
            
            if ( rs.next() )
                 swExist = 1;
            
            
            if(  swExist == 0 )            
                 msj = "La corrida " + corrida + " no existe o fu� creada por otro usuario";
            
            else{ 
                
                int swPagadas  = 0;
                
                st= con.prepareStatement(this.obtenerSQL(queryNoCancelada));//JJCastro fase2
                st.setString(1, distrito );
                st.setString(2, corrida  );
                st.setString(3, user     );
                rs = st.executeQuery();
                if ( rs.next()  )
                     swPagadas = 1;                
                
                if (  swPagadas == 0 )
                     msj = "La corrida " + corrida + " se encuentra cancelada";
                
                
            }
            
            
            
            
        }}catch(Exception e){
            throw new Exception(" DAO: No se pudo verificar Existencia nit -->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return msj;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * M�todo que busca nombre del nit
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String getNombre( String nit) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st        =  null;
        ResultSet         rs        =  null;
        String            query     =  "SQL_VALIDAR_EXISTENCIA_NIT";
        String            name      =  null;
        try {
            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, nit );
            rs = st.executeQuery();
            if ( rs.next()  )
                 name  =  reset(  rs.getString("nombre")  );
                
            
        }}catch(Exception e){
            throw new Exception(" DAO: No se pudo verificar Existencia nit -->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return name;
    }
    
    
    
    
    /**
     * M�todo que busca nombre del nit
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String existePlaca( String placa) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st          =  null;
        ResultSet         rs          =  null;
        String            queryExist  =  "SQL_EXISTE_PLACA";
        String            queryVeto   =  "SQL_VETO_PLACA";
        String            msj         =  "";
        int               swE         =  0;
        int               swV         =  0;        
        try {

            con = this.conectarJNDI(queryExist);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(queryExist));//JJCastro fase2
            st.setString(1, placa );
            rs = st.executeQuery();
            if ( rs.next()  )
                 swE = 1;
            
            
            st= con.prepareStatement(this.obtenerSQL(queryVeto));//JJCastro fase2
            st.setString(1, placa );
            rs = st.executeQuery();
            if ( rs.next()  )
                 swV = 1;
                
            
            if(  swE == 0 )  msj  = "La placa "+ placa + " no existe o no est� activa ";
            if(  swV == 1 )  msj += "La placa "+ placa + " est� vetada ";
                        
            
        }}catch(Exception e){
            throw new Exception(" DAO: No se pudo verificar Existencia de la placa -->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return msj;
    }
    
    
    
    
    /**
     * M�todo que busca las facturas que cumplan el criterio de selecci�n, generaci�n de corridas
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public List getFacturasSQL( 
    
                              String SQL, 
                              String distrito, 
                              String filtrosViajes, 
                              String filtrosFacturasOPs, 
                              String filtrosFacturasTipo4, 
                              String filtrosHC, 
                              String filtroPlacas,                               
                              String urlFile, 
                              String tipoPago 
    
                             ) throws Exception{
                                 
                                 
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        List              lista     = new LinkedList(); 
        String            query     =  SQL;
        Connection        con       = null;
        
        try {
            
            
            String sql =  this.obtenerSQL( query ).replaceAll("#VIAJES#",    filtrosViajes        ).
                                                   replaceAll("#FILTROOPS#", filtrosFacturasOPs   ).
                                                   replaceAll("#FILTRO#",    filtrosFacturasTipo4 ).
                                                   replaceAll("#HC#",        filtrosHC            ).
                                                   replaceAll("#PLACAS#",    filtroPlacas         );
                                                   
            
            con = this.conectarJNDI(query);//JJCastro fase2
            st= con.prepareStatement(sql);
            if(  query.equals("SQL_BUSCAR_FACTURAS_VIAJES") ){            
                 st.setString(1, distrito );
                 st.setString(2, distrito );
            }else
                 st.setString(1, distrito );
            
            
                        
        // Iniciamos el Archivo de Novedades
            initFile(urlFile);
            
            
            int cont = 0;
            rs=st.executeQuery();
            while(rs.next()){
                cont ++;
                Corrida  factura  = new Corrida();
                    factura.setDistrito                  ( reset( rs.getString("distrito")    ));
                    factura.setProveedor                 ( reset( rs.getString("proveedor")   ));
                    factura.setNombre                    ( reset( rs.getString("nombre")      ));
                    factura.setDocumento                 ( reset( rs.getString("factura")     ));
                    factura.setTipoDoc                   ( reset( rs.getString("tipoDoc")     ));
                    factura.setBranch_code               ( reset( rs.getString("banco")       ));
                    factura.setBank_account_no           ( reset( rs.getString("sucursal")    ));
                    factura.setMoneda_banco              ( reset( rs.getString("moneda_banco")));
                    factura.setMoneda                    ( reset( rs.getString("moneda")      ));
                    factura.setAgencia                   ( reset( rs.getString("agencia")     ));                    
                    factura.setPlanilla                  ( reset( rs.getString("planilla")    ));
                    factura.setPlaca                     ( reset( rs.getString("placa")       ));
                    factura.setValor                     (        rs.getDouble("saldo")        );
                    factura.setValor_me                  (        rs.getDouble("saldo_me")     );
                    
                    factura.setTipoPago                  (  reset( rs.getString("tipo_pago")      )  );
                    factura.setBancoTransferencia        (  reset( rs.getString("banco_transfer") )  );
                    factura.setSucursalTransferencia     (  reset( rs.getString("suc_transfer")   )  );
                    factura.setNoCuentaTransferencia     (  reset( rs.getString("no_cuenta")      )  );
                    factura.setTipoCuentaTransferencia   (  reset( rs.getString("tipo_cuenta")    )  );
                    factura.setNombreCuentaTransferencia (  reset( rs.getString("nombre_cuenta")  )  );
                    factura.setCedulaCuentaTransferencia (  reset( rs.getString("cedula_cuenta")  )  );
                    factura.setBase                      (  reset( rs.getString("base")           )  );
                    
                    
                    if( isValida(factura, tipoPago )  )
                        lista.add(factura);

                    factura = null;//Liberar Espacio JJCastro
            }  
            
            if( cont==0){
                linea.println(" NO SE ENCONTRARON REGISTROS ACTIVOS ");
                linea.println(" SQL DE BUSQUEDA : ");
                linea.println( st.toString()     );
            }
            
            
        // Cerramos el archivo:
            this.linea.close();   
            
            
        }catch(Exception e){
            throw new Exception(" DAO: getFacturasSQL() -->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    
    
    
    
    
    
    /**
     * M�todo que busca las facturas que cumplan el criterio de selecci�n, SE USA EN AGREGAR FACTURAS A LA CORRIDA
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public List getFacturas( String distrito, String filtros ) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        List              lista     = new LinkedList(); 
        String            query     = "SQL_BUSCAR_FACTURAS";
        Connection        con       = null;
        
        try {
            
            String sql =  this.obtenerSQL( query ).replaceAll("#FILTRO#", filtros ).replaceAll("#PLACAS#", "" ); 
            
            con = this.conectarJNDI(query);//JJCastro fase2
            st= con.prepareStatement(sql);
            st.setString(1, distrito );
            
            
            int cont = 0;
            rs=st.executeQuery();
            while(rs.next()){
                cont ++;
                Corrida  factura  = new Corrida();
                    factura.setDistrito                  ( reset( rs.getString("distrito")  ));
                    factura.setProveedor                 ( reset( rs.getString("proveedor") ));
                    factura.setNombre                    ( reset( rs.getString("nombre")    ));
                    factura.setDocumento                 ( reset( rs.getString("factura")   ));
                    factura.setTipoDoc                   ( reset( rs.getString("tipoDoc")   ));
                    factura.setBranch_code               ( reset( rs.getString("banco")     ));
                    factura.setBank_account_no           ( reset( rs.getString("sucursal")  ));
                    factura.setMoneda                    ( reset( rs.getString("moneda")    ));
                    factura.setAgencia                   ( reset( rs.getString("agencia")   ));                    
                    factura.setPlanilla                  ( reset( rs.getString("planilla")  ));
                    factura.setPlaca                     ( reset( rs.getString("placa")     ));
                    factura.setValor                     (        rs.getDouble("saldo")      );
                    factura.setValor_me                  (        rs.getDouble("saldo_me")   );
                    
                    factura.setTipoPago                  (  reset( rs.getString("tipo_pago")      )  );
                    factura.setBancoTransferencia        (  reset( rs.getString("banco_transfer") )  );
                    factura.setSucursalTransferencia     (  reset( rs.getString("suc_transfer")   )  );
                    factura.setNoCuentaTransferencia     (  reset( rs.getString("no_cuenta")      )  );
                    factura.setTipoCuentaTransferencia   (  reset( rs.getString("tipo_cuenta")    )  );
                    factura.setNombreCuentaTransferencia (  reset( rs.getString("nombre_cuenta")  )  );
                    factura.setCedulaCuentaTransferencia (  reset( rs.getString("cedula_cuenta")  )  );
                    factura.setBase                      (  reset( rs.getString("base")           )  );
                    
                    
                    if( isValida( factura, factura.getTipoPago()  )  )
                        lista.add(factura);
                    
          factura = null;//Liberar Espacio JJCastro
            }
            
                        
            
        }catch(Exception e){
            throw new Exception(" DAO: getFacturas() -->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    
    
    
    
    
    
    
     /**
     * M�todo que determina si la factura es valida para la corrida o no, aqui validamos campos
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public boolean  isValida(Corrida  factura, String tipoPago)throws Exception{
        boolean estado = false;
        try{
             
          // Asigna tipo de pago
            factura.setTipoPago( tipoPago ); 
            
            
            String  llave =  "PROVEEDOR :" +  factura.getProveedor() +" FACTURA :" + factura.getDocumento();
            
            boolean estadoDatosFactura  = false;
            boolean estadoBancoPago     = false;
            boolean estadoTransferencia = false;
            
          
            
         // 1. Datos de la Factura:
             if(    !factura.getDistrito().trim().equals("")  &&  !factura.getProveedor().trim().equals("")  
                 && !factura.getNombre().trim().equals("")    &&  !factura.getDocumento().trim().equals("")
                 && !factura.getTipoDoc().trim().equals("")
               )
                  estadoDatosFactura = true;
            
            
         // 2. Datos de banco de pago: 
            if(     !factura.getBranch_code().trim().equals("")  &&  !factura.getBank_account_no().trim().equals("") 
                 && !factura.getMoneda().trim().equals("")       &&  !factura.getAgencia().trim().equals("")    
              )
                   estadoBancoPago = true;
            
            
         // 3. Datos de banco de Transferencia:
             if (  tipoPago.toUpperCase().equals("T")  ){
                   
                   if (    !factura.getBancoTransferencia().trim().equals("")        
                        && !factura.getNoCuentaTransferencia().trim().equals("")     &&  !factura.getTipoCuentaTransferencia().trim().equals("")
                        && !factura.getNombreCuentaTransferencia().trim().equals("") &&  !factura.getCedulaCuentaTransferencia().trim().equals("")
                      )
                      estadoTransferencia = true;
             }
             else                      
                      estadoTransferencia = true;
             
                    
                 
            
            
            if (  estadoDatosFactura  &&   estadoBancoPago  &&   estadoTransferencia  ){
                  
                  
               // VERIFICACION ERRORE DE LONGITUD CAMPOS:                  
                  int   longDstrct           =    4  ;                      
                  int   longTipo_documento   =   15  ;                     
                  int   longDocumento        =   30  ;                          
                  int   longBeneficiario     =   15  ;                        
                  int   longNombre           =  100  ;                 
                  int   longPlanilla         =   10  ;                      
                  int   longPlaca            =   12  ;                    
                  int   longBanco            =   30  ;                   
                  int   longSucursal         =   30  ;                   
                  int   longAgencia_banco    =   30  ;                   
                  int   longMoneda           =    3  ;  
                  int   longTipo_pago        =    2  ;                  
                  int   longBanco_transfer   =   15  ;
                  int   longSuc_transfer     =   15  ;
                  int   longTipo_cuenta      =    3  ;
                  int   longNo_cuenta        =   20  ;
                  int   longCedula_cuenta    =   15  ;
                  int   longNombre_cuenta    =   100 ;
                  int   longBase             =    3  ;
                  
                  int sw = 0;
                  
                  if(  factura.getDistrito()                 .length() > longDstrct          ){  sw = 1;   this.linea.println ( "El distrito           supera la longitud " + longDstrct          + " ->" + factura.getDistrito()                  ) ;  }                   
                  if(  factura.getTipoDoc()                  .length() > longTipo_documento  ){  sw = 1;   this.linea.println ( "El Tipo de documento  supera la longitud " + longTipo_documento  + " ->" + factura.getTipoDoc()                   ) ;  } 
                  if(  factura.getDocumento()                .length() > longDocumento       ){  sw = 1;   this.linea.println ( "El documento          supera la longitud " + longDocumento       + " ->" + factura.getDocumento()                 ) ;  } 
                  if(  factura.getProveedor()                .length() > longBeneficiario    ){  sw = 1;   this.linea.println ( "El proveedor          supera la longitud " + longBeneficiario    + " ->" + factura.getProveedor()                 ) ;  } 
                  if(  factura.getNombre()                   .length() > longNombre          ){  sw = 1;   this.linea.println ( "El nombre proveedor   supera la longitud " + longNombre          + " ->" + factura.getNombre()                    ) ;  } 
                  if(  factura.getPlanilla()                 .length() > longPlanilla        ){  sw = 1;   this.linea.println ( "La planilla           supera la longitud " + longPlanilla        + " ->" + factura.getPlanilla()                  ) ;  }
                  if(  factura.getPlaca()                    .length() > longPlaca           ){  sw = 1;   this.linea.println ( "La placa              supera la longitud " + longPlaca           + " ->" + factura.getPlaca()                     ) ;  } 
                  if(  factura.getBranch_code()              .length() > longBanco           ){  sw = 1;   this.linea.println ( "El banco              supera la longitud " + longBanco           + " ->" + factura.getBranch_code()               ) ;  } 
                  if(  factura.getBank_account_no()          .length() > longSucursal        ){  sw = 1;   this.linea.println ( "La sucursal           supera la longitud " + longSucursal        + " ->" + factura.getBank_account_no()           ) ;  }                   
                  if(  factura.getAgencia()                  .length() > longAgencia_banco   ){  sw = 1;   this.linea.println ( "La agencia banco      supera la longitud " + longAgencia_banco   + " ->" + factura.getAgencia()                   ) ;  } 
                  if(  factura.getMoneda()                   .length() > longMoneda          ){  sw = 1;   this.linea.println ( "La moneda             supera la longitud " + longMoneda          + " ->" + factura.getMoneda()                    ) ;  }                   
                  if(  factura.getTipoPago()                 .length() > longTipo_pago       ){  sw = 1;   this.linea.println ( "El tipo de pago       supera la longitud " + longTipo_pago       + " ->" + factura.getTipoPago()                  ) ;  }                   
                  if(  factura.getBancoTransferencia()       .length() > longBanco_transfer  ){  sw = 1;   this.linea.println ( "El banco de transfer  supera la longitud " + longBanco_transfer  + " ->" + factura.getBancoTransferencia()        ) ;  } 
                  if(  factura.getSucursalTransferencia()    .length() > longSuc_transfer    ){  sw = 1;   this.linea.println ( "La sucursal transfer  supera la longitud " + longSuc_transfer    + " ->" + factura.getSucursalTransferencia()     ) ;  } 
                  if(  factura.getTipoCuentaTransferencia()  .length() > longTipo_cuenta     ){  sw = 1;   this.linea.println ( "El tipo de cta        supera la longitud " + longTipo_cuenta     + " ->" + factura.getTipoCuentaTransferencia()   ) ;  } 
                  if(  factura.getNoCuentaTransferencia()    .length() > longNo_cuenta       ){  sw = 1;   this.linea.println ( "El n�mero de cta      supera la longitud " + longNo_cuenta       + " ->" + factura.getNoCuentaTransferencia()     ) ;  } 
                  if(  factura.getCedulaCuentaTransferencia().length() > longCedula_cuenta   ){  sw = 1;   this.linea.println ( "El nit de cta transf  supera la longitud " + longCedula_cuenta   + " ->" + factura.getCedulaCuentaTransferencia() ) ;  } 
                  if(  factura.getNombreCuentaTransferencia().length() > longNombre_cuenta   ){  sw = 1;   this.linea.println ( "El nombre cta         supera la longitud " + longNombre_cuenta   + " ->" + factura.getNombreCuentaTransferencia() ) ;  } 
                  if(  factura.getBase()                     .length() > longBase            ){  sw = 1;   this.linea.println ( "La base               supera la longitud " + longBase            + " ->" + factura.getBase()                      ) ;  } 
                           
                  
                                    
                  
               // Validamos que la Placa No tenga Veto aplicado
                   if(  this.getVetoPlaca( factura.getPlaca() ) ){
                       sw = 1;
                       this.linea.println("La placa  " + factura.getPlaca() + "  est� vetada "   );
                  }
                  
                  
               // Validamos que la planilla No tenga Discrepancia
                   //se comenta porque no sabemos que hace y esta poniendo problemas ojo revisa cachonnn jejejej...
//                  if(  this.getDiscrepancia( factura.getPlanilla() ) ){
//                       sw = 1;
//                       this.linea.println("La planilla " + factura.getPlanilla() + " presenta discrepancia "   );
//                  }
//                  
                  
                  
                  
                  if( sw==0 )
                       estado = true; 
                  
                  
            }
            else{  // ERROR DE DATOS                
                
            // Datos de la Factura:
                if( ! estadoDatosFactura   ){
                        if( factura.getDistrito().trim().equals("")  )     this.linea.println(  llave + " No presenta distrito")  ;
                        if( factura.getProveedor().trim().equals("") )     this.linea.println(  llave + " No presenta proveedor") ;                        
                        if( factura.getNombre().trim().equals("")    )     this.linea.println(  llave + " El proveedor no posee nombre ")  ;
                        if( factura.getDocumento().trim().equals("") )     this.linea.println(  llave + " No presenta n�mero de facturas") ;
                        if( factura.getTipoDoc().trim().equals("") )       this.linea.println(  llave + " No presenta tipo de documento")  ;
                }
           
            // Informacion da banco
                if( ! estadoBancoPago      ){
                        if( factura.getBranch_code().trim().equals("")      )     this.linea.println(  llave + " No presenta banco")  ;
                        if( factura.getBank_account_no().trim().equals("")  )     this.linea.println(  llave + " No presenta sucursal de banco")  ;
                        if( factura.getMoneda().trim().equals("")           )     this.linea.println(  llave + " No presenta moneda")  ;
                        if( factura.getAgencia().trim().equals("")          )     this.linea.println(  llave + " No presenta agencia")  ;
                }
                
                
             // Informacion da banco   para pago:
                if( ! estadoTransferencia  ){
                      if( factura.getBancoTransferencia().trim().equals("")          )     this.linea.println(  llave + " El propietario no presenta banco para transferencia")  ;                      
                      if( factura.getNoCuentaTransferencia().trim().equals("")       )     this.linea.println(  llave + " El propietario no presenta n�mero de cuenta para transferencia")  ;
                      if( factura.getTipoCuentaTransferencia().trim().equals("")     )     this.linea.println(  llave + " El propietario no presenta tipo de cuenta para transferencia")  ;
                      if( factura.getNombreCuentaTransferencia().trim().equals("")   )     this.linea.println(  llave + " El propietario no presenta nombre de cuenta para transferencia")  ;
                      if( factura.getCedulaCuentaTransferencia().trim().equals("")   )     this.linea.println(  llave + " El propietario no presenta nit de cuenta para transferencia")  ;
                }
                
                
            }
            
            
        }catch(Exception e){
            throw new Exception( " isValida " +e.getMessage() );
        }        
        return estado;
    }
    
    
    
    
    /**
     * M�todo, busca si la placa tiene veto asignado.
     * @autor   fvillacob
     * @throws  Exception
     * @version 1.0.
     **/
    public boolean  getVetoPlaca(String placa)throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st         = null;
        ResultSet         rs         = null;
        String            query      = "SQL_VETO_PLACA";
        boolean           estado     = false;
        try {
            
             if( !placa.equals("") ){
                 con = this.conectarJNDI(query);
                 if (con != null) {
                 st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                 st.setString(1, placa);
                 rs=st.executeQuery();
                 if ( rs.next() )
                      estado = true;
             }
            
        }} catch(SQLException e){
            throw new SQLException(" DAO: getVetoPlaca-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return estado;
    }
    
    
    
    
    /**
     * M�todo, busca la discrepancia activa de la planilla
     * @autor   fvillacob
     * @throws  Exception
     * @version 1.0.
     **/
    public boolean  getDiscrepancia(String oc)throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st         = null;
        ResultSet         rs         = null;
        String            query      = "SQL_DISCREPANCIA";
        boolean           estado     = false;
        try {
            
             if( !oc.equals("") ){
                 
                 con = this.conectarJNDI(query);
                 if (con != null) {
                 st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                 st.setString(1, oc);
                 rs=st.executeQuery();
                 if ( rs.next() )
                      estado = true;
                 
             }
             
        }} catch(SQLException e){
            throw new SQLException(" DAO: getDiscrepancia-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return estado;
    }
    
    
    
    
    
    /**
     * M�todo que busca el nuevo numero de corrida a generar
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String getNoCorrida( String distrito) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            no        = "1";
        String            query     =  "SQL_SEARCH_NO_CORRIDA";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito );
            rs = st.executeQuery();
            if ( rs.next()  )
                no =   String.valueOf( rs.getInt(1) + 1 );
                
            
        }}catch(Exception e){
            throw new Exception(" DAO: No se pudo buscar el No de la corrida-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return no;
    }
    
    
        
    
    
    /**
     * M�todo que inserta corrida y factura
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String insertExtractos( Corrida  factura  , String corrida, String user, String fecha ) throws Exception{
        StringStatement st        = null;
        String            query     =  "SQL_INSERT_CORRIDA";
        String            SQL       = ""; 
        try {
            
            st = new StringStatement(this.obtenerSQL(query), true );//JJCastro fase2
                    st.setString(1,  factura.getDistrito()        );
                    st.setString(2,  corrida                      );                
                    st.setString(3,  factura.getTipoDoc()         );                
                    st.setString(4,  factura.getDocumento()       );                
                    st.setString(5,  factura.getProveedor()       );
                    st.setString(6,  factura.getNombre()          );                
                    st.setDouble(7,  factura.getValor()           );
                    st.setDouble(8,  factura.getValor_me()        );
                    st.setString(9,  factura.getPlanilla()        );
                    st.setString(10,  factura.getPlaca()          );
                    st.setString(11, factura.getBranch_code()     );
                    st.setString(12, factura.getBank_account_no() );
                    st.setString(13, factura.getAgencia()         );                
                    st.setString(14, factura.getMoneda()          );
                    st.setString(15, user                         );
                    
                    st.setString(16, factura.getTipoPago()                  );                    
                    st.setString(17, factura.getBancoTransferencia()        );
                    st.setString(18, factura.getSucursalTransferencia()     );                    
                    st.setString(19, factura.getTipoCuentaTransferencia()   );
                    st.setString(20, factura.getNoCuentaTransferencia()     );
                    st.setString(21, factura.getCedulaCuentaTransferencia() );
                    st.setString(22, factura.getNombreCuentaTransferencia() );
                    st.setString(23, factura.getBase()                      );
                    st.setString(24, fecha                                  );
                    
             SQL = st.getSql();//JJCastro fase2
             
             
           
        } catch(Exception e){
            throw new Exception(" DAO: No se pudo insertar el registro de la corrida-->"+ e.getMessage());
        }
        finally{
        if (st  != null){ try{ st = null;            } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return SQL;
    }
    
    
    
    
    
    /**
     * M�todo que actualiza el campo corrida de la factura
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String updateFacturas( Corrida  factura, String corrida, String user) throws Exception{
        StringStatement st        = null;
        String            SQL       = "";
        String            query     =  "SQL_UPDATE_FACTURA";
        try {
            
            st =  new StringStatement(this.obtenerSQL(query), true);
                st.setString(1, corrida                     );
                st.setString(2, user                        );
                st.setString(3, factura.getDistrito()       );                    
                st.setString(4, factura.getTipoDoc()        );
                st.setString(5, factura.getDocumento()      );
                st.setString(6, factura.getProveedor()      );
                  
             SQL += st.getSql();//JJCastro fase2
             
        }
        catch(Exception e){
            throw new Exception(" DAO: No se pudieron actualizar las facturas-->"+ e.getMessage());
        }
        finally{
        if (st  != null){ try{ st = null;            } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return SQL;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    // --------------------------------------------------------------------------------------------------------------
    // GENERAMOS LOS  FILTROS DE BUSQUEDAS:
    // --------------------------------------------------------------------------------------------------------------
    
    
    
    
    
    /**
    * Metodo que permite obtener la lista  de distritos
    * @autor: ....... Fernel Villacob
    * @throws ....... Exception
    * @version ...... 1.0
    */
    public List getDistritos(String agencia, String dstrct) throws Exception{         
        Connection con = null;//JJCastro fase2
        PreparedStatement st        = null;
         ResultSet         rs        = null;
         List              lista     =  new LinkedList();
         String            query     =  "SQL_SEARCH_DISTINCT_CIA";
         try{
             con = this.conectarJNDI(query);
             if (con != null) {
                 st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                 
                 if( agencia.equals( OFICINA_PPAL )){                        
                        rs =  st.executeQuery();
                        while(rs.next()){                   
                            String distrito = rs.getString(1);
                            lista.add(distrito);
                        }
                 }
                 else
                        lista.add( dstrct );
        }}
        catch(Exception e){
            throw new SQLException(" DAO: No se pudieron listar los distritos-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    
    
    
    
    
    /**
    * Metodo que permite obtener la lista  de bancos dependiendo la sucursal
    * @autor: ....... Fernel Villacob
    * @parameter      String agencia
    * @throws ....... Exception
    * @version ...... 1.0
    */
    public List getBancos( String distrito , String agencia ) throws Exception{        
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        List              lista     = new LinkedList();
        String            query     = "SQL_SEARCH_DISTINCT_BANCOS";
        Connection        con       = null;
        
        try {
            
            con           =  this.conectarJNDI(query);//JJCastro fase2
            
            String filtro =  ( agencia.equals( this.OFICINA_PPAL ) )?" like '%' " : " ='" + agencia + "'";
            String sql    =  this.obtenerSQL( query ).replaceAll("#AGENCIA#", filtro );
             
            st= con.prepareStatement( sql );
            st.setString(1, distrito);
            rs=st.executeQuery();
            while(rs.next()){
                String banco = rs.getString(1);
                lista.add(banco);
            }
            
        }
        catch(SQLException e){
            throw new SQLException(" DAO: No se pudieron listar los Bancos-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    
    
    
    
    /**
    * Metodo que permite obtener la lista  de proveedores  dependiendo la sucursal
    * @autor: ....... Fernel Villacob
    * @parameter      String agencia
    * @throws ....... Exception
    * @version ...... 1.0
    */
     public List getProveedores(String distrito , String agencia) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        List              lista     = new LinkedList();
        String            query     = "SQL_SEARCH_DISTINCT_PROVEEDORES";
        Connection        con       = null;
        try {
            con  = this.conectarJNDI(query);//JJCastro fase2
            
            String filtro =  ( agencia.equals( this.OFICINA_PPAL ) )?" like '%' " : " ='" + agencia + "'";
            String sql    =  this.obtenerSQL( query ).replaceAll("#AGENCIA#", filtro );
            st = con.prepareStatement( sql );
            st.setString(1, distrito);
            rs = st.executeQuery();
            while(rs.next()){
                Hashtable proveedor = new Hashtable();
                    proveedor.put("nit",     rs.getString(1));
                    proveedor.put("nombre",  rs.getString(2));
                lista.add(proveedor);
            }
            
        }
        catch(SQLException e){
            throw new SQLException(" DAO: No se pudieron listar los proveedores-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    
    
    
     
     
     
    /**
    * Metodo que permite obtener la lista  de Tipos de proveedores en tabla general
    * @autor: ....... Fernel Villacob
    * @throws ....... Exception
    * @version ...... 1.0
    */
     public List getTipoProveedores() throws Exception{
         Connection con = null;//JJCastro fase2
         PreparedStatement st        = null;
        ResultSet         rs        = null;
        List              lista     = new LinkedList();
        String            query     = "SQL_TIPOS_PROVEEDORES";
        try {
             con = this.conectarJNDI(query);
             if (con != null) {
                 st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                 rs = st.executeQuery();
                 while (rs.next()) {
                     Hashtable tipo = new Hashtable();
                     tipo.put("codigo", reset(rs.getString("table_code")));
                     tipo.put("descripcion", reset(rs.getString("descripcion")));
                     lista.add(tipo);
                 }

             }
         }
        catch(SQLException e){
            throw new SQLException(" DAO: No se pudieron listar los tipo de proveedores-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
     
     
     
    
    /**
    * Metodo que permite obtener la lista  de proveedores  dependiendo la sucursal
    * @autor: ....... Fernel Villacob
    * @parameter      String agencia
    * @throws ....... Exception
    * @version ...... 1.0
    */
     public List getSucursales(String distrito , String bancos, String agencia) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        List              lista     = new LinkedList();
        String            query     = "SQL_SEARCH_SUCURSALES_BANCOS";
        Connection        con       = null;
        try {
            
            con  = this.conectarJNDI(query);//JJCastro fase2
            
            String filtro     =  ( agencia.equals( this.OFICINA_PPAL ) )?" like '%' " : " ='" + agencia + "'";
            String sql        =  this.obtenerSQL( query ).replaceAll("#BANCOS#", bancos ).replaceAll("#AGENCIA#", filtro );  
            
            st= con.prepareStatement( sql );
            st.setString(1, distrito);
            rs=st.executeQuery();
            while(rs.next()){
                Hashtable sucursales = new Hashtable();
                    sucursales.put("banco",       rs.getString(1));
                    sucursales.put("sucursal",    rs.getString(2));                    
                    sucursales.put("agencia",     rs.getString(3));                    
                lista.add(sucursales);
                sucursales = null;//Liberar Espacio JJCastro
            }
            
        }
        catch(SQLException e){
            throw new SQLException(" DAO: No se pudieron listar las sucursales -->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
     
     
     
     /**
    * Metodo que permite obtener la lista  de  placas de proveedores
    * @autor: ....... Fernel Villacob
    * @throws ....... Exception
    * @version ...... 1.0
    */
     public List getPlacas(String nits ) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        List              lista     = new LinkedList();
        String            query     = "SQL_SEARCH_PLACAS_NIT";
        Connection        con       = null;
        try {
            
            con           = this.conectarJNDI(query);//JJCastro fase2
            String sql    =  this.obtenerSQL( query ).replaceAll("#NITS#", nits );  
            
            st= con.prepareStatement( sql );
            rs=st.executeQuery();
            while(rs.next()){
                Hashtable placa = new Hashtable();
                    placa.put("placa",       rs.getString("placa")       );
                    placa.put("nit",         rs.getString("propietario") );                   
                    placa.put("nombre",      rs.getString("name")        );                   
                lista.add(placa);
            }
            
        }
        catch(SQLException e){
            throw new SQLException(" DAO: No se pudieron listar las placas -->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
     
    
}
