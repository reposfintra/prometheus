/*Created on 15 de septiembre de 2005, 02:05 PM
 */


package com.tsp.operation.model.DAOS;

/**@author  fvillacob
 */


import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;

public class ProyectoDAO extends MainDAO{
    
    
    /* _______________________________________________________________________________________________________________________________
     *                                 ATRIBUTOS
     * _______________________________________________________________________________________________________________________________ */
    
    
    
    
    private static final String PROYECTOS             = " SELECT project, description, dstrct,base  FROM proyecto     WHERE  reg_status=''    ORDER by 1   "; 
    private static final String USUARIOS              = " SELECT nombre,  idusuario                 FROM usuarios     WHERE  estado    ='A'   ORDER by 1   "; 
    
    
    
    private static final String PERFIL_USUARIO        = " SELECT  project, login, project    "+
                                                        " FROM    usuario_proyecto           "+
                                                        " WHERE                              "+
                                                        "         reg_status=''              "+
                                                        " ORDER BY #INDICE#                  ";
   
    
    
    private static final String SEARCH_PROYECTO_USUARIO = " SELECT login   FROM  usuario_proyecto                         WHERE  project=?  and  login=?        "; 
    
    private static final String INSERT_PROYECTO_USUARIO = " INSERT INTO usuario_proyecto (dstrct, project, login, creation_user, base) VALUES(?,?,?,?,?)        ";
      
    private static final String UPDATE_PROYECTO_USUARIO = " UPDATE usuario_proyecto  SET  reg_status='',  user_update=?   WHERE  project =? and  login=?        "; 
 
    private static final String VERIFICACION            = " SELECT login             FROM  usuario_proyecto               WHERE  project =? and  reg_status=''  ";
    
    private static final String ANULAR                  = " UPDATE usuario_proyecto  SET   reg_status='A', user_update=?  WHERE  project =? and  login=?        ";
         
    private static final String SEARCH_USUARIO          = " SELECT nombre            FROM usuarios                        WHERE  estado='A' and idusuario=?     ";

     
    
    
    private static final String CREATE=
                                        "    CREATE TABLE usuario_proyecto                                 "+
                                        "    (                                                             "+
                                        "    reg_status    varchar(1)  NOT NULL DEFAULT '',                "+
                                        "    dstrct        text        NOT NULL DEFAULT '',                "+
                                        "    login         varchar(12) NOT NULL DEFAULT '',                "+
                                        "    project       varchar(10) NOT NULL DEFAULT '',                "+
                                        "    last_update   timestamp   NOT NULL DEFAULT now(),             "+
                                        "    user_update   varchar(10) NOT NULL DEFAULT '',                "+
                                        "    creation_date timestamp   NOT NULL DEFAULT now(),             "+
                                        "    creation_user varchar(10) NOT NULL DEFAULT '',                "+
                                        "    base          varchar(3)  NOT NULL DEFAULT '',                "+
                                        "    CONSTRAINT usuario_proyecto_pkey PRIMARY KEY (dstrct, login, project) "+
                                        "    )                                                             ";


    
    
    
    private static final String DATOS_CIA_BASE   = " SELECT  cia , base  FROM usuarios   WHERE   idusuario= ? "; 
    
    
    
    
    private static int SW = 0;
    
    
    
    
    
    
    /* ______________________________________________________________________________________________________________________________
     *                                 METODOS
     * ______________________________________________________________________________________________________________________________ */
    
   
    
    public ProyectoDAO() {
        super("ProyectoDAO.xml");//JJCastro fase2
    }
    
    
    
/**
 * 
 * @throws SQLException
 */
     public void create() throws SQLException {
        Connection con          = null;
        String query = "CREATE";//JJCastro fase2
        PreparedStatement st = null;   
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.execute(); 
        }}catch(Exception e){
            throw new SQLException( e.getMessage());
        }finally{
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
   
     
     
     
/**
 * 
 * @return
 * @throws SQLException
 */
    public List getProyectos() throws SQLException {
        Connection con = null;//JJCastro fase2
        String query = "SQL_PROYECTOS";//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = null;     
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs=st.executeQuery();        
            if(rs!=null){
                lista = new LinkedList();
                while(rs.next()){
                   Perfil perfil = new Perfil();
                     perfil.setId      ( rs.getString(1) );
                     perfil.setNombre  ( rs.getString(2) );
                     perfil.setDistrito( rs.getString(3) );
                     perfil.setBase    ( rs.getString(4) );
                  lista.add(perfil);
                }
            }
            
        }}catch(Exception e){
            throw new SQLException( e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;     
    }
    
    
    
    
    
    
/**
 * 
 * @return
 * @throws SQLException
 */
    public List getUsuarios() throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        List lista = null;
        String query = "SQL_USUARIOS";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                if (rs != null) {
                    lista = new LinkedList();
                    while (rs.next()) {
                        Usuario usuario = new Usuario();
                        usuario.setNombre(rs.getString(1));
                        usuario.setLogin(rs.getString(2));
                        lista.add(usuario);
                    }
                }

            }
        }catch(Exception e){
            throw new SQLException( e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;     
    }
    
    
    
    
    
    
/**
 * 
 * @param parametro
 * @return
 * @throws SQLException
 */
     public List getProyectoUsuarios(String parametro) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = null;     
         String query = "SQL_PERFIL_USUARIO";//JJCastro fase2
        try{
            st= con.prepareStatement(this.obtenerSQL(query).replaceAll("#INDICE#",parametro));
            rs=st.executeQuery();        
            if(rs!=null){
                lista = new LinkedList();
                while(rs.next()){
                  PerfilUsuario usuario = new PerfilUsuario();
                    usuario.setPerfil  ( rs.getString(1));
                    usuario.setUsuarios( rs.getString(2));
                    usuario.setPerName ( rs.getString(3));
                  lista.add(usuario);
                }
            }
            
        }catch(Exception e){
            throw new SQLException( "DAO: "+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        lista = getNameUsuarios(lista);
        return lista;     
    }
     
     
     
     
/**
 * 
 * @param lista
 * @return
 * @throws SQLException
 */
     public List getNameUsuarios(List lista) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs         = null;   
        String query = "SQL_SEARCH_USUARIO";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            if(lista!=null && lista.size()>0){
                Iterator it = lista.iterator();
                while(it.hasNext()){
                    PerfilUsuario usuario = (PerfilUsuario) it.next();  
                    st.setString(1, usuario.getUsuarios());
                    rs=st.executeQuery(); 
                    while(rs.next())
                       usuario.setUserName( rs.getString(1));
                }
            }
            }}catch(Exception e){
            throw new SQLException( e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;     
    }
    
    
     
     
     
     //--------
/**
 * 
 * @param GRUPOS
 * @param USUARIOS
 * @param user
 * @throws Exception
 */
    public void Insert (String[] GRUPOS, String[] USUARIOS, String user) throws Exception {
        Connection con         = null;//JJCastro fase2
        try{
            con = this.conectarBDJNDI("fintra");//JJCastro fase2
            if (con != null) {

            for(int i=0;i<=GRUPOS.length-1;i++){
               String perfil   = GRUPOS[i];
               for(int j=0;j<=USUARIOS.length-1;j++){
                 String usuario =  USUARIOS[j]; 
                 if(!existe(perfil,usuario,con))  Insert(perfil, usuario, con, user); 
                 else                              update(perfil, usuario, con, user);
               }
               verificar(perfil,USUARIOS,con,user);
            }
        }}
        catch(SQLException e){
            throw new SQLException("Error en Insert [MenuDAO] " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    
    
/**
 *
 * @param PERFIL
 * @param USUARIO
 * @param conn
 * @return
 * @throws Exception
 */
     public boolean existe(String PERFIL, String USUARIO, Connection conn) throws Exception {
         Connection con = null;//JJCastro fase2
         PreparedStatement stmt = null;
         ResultSet rs = null;
         boolean estado = false;
         String query = "SEARCH_PROYECTO_USUARIO";//JJCastro fase2
         try {
             con = this.conectarJNDI(query);
             if (con != null) {
                 stmt = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                 stmt.setString(1, PERFIL);
                 stmt.setString(2, USUARIO);
                 rs = stmt.executeQuery();
                 while (rs.next()) {
                     estado = true;
                 }
             }
         }catch(SQLException e){
            throw new SQLException("Error en existe usuario en el proyecto [MenuDAO] " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (stmt  != null){ try{ stmt.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return estado;
    }
     
    
     
     
     
/**
 * 
 * @param usuario
 * @param con
 * @return
 * @throws SQLException
 */
      public Perfil getDatosCiaBase(String usuario, Connection con) throws SQLException {
          PreparedStatement st = null;
            ResultSet rs         = null;
            Perfil perfil        = new Perfil();
            String query = "SQL_DATOS_CIA_BASE";//JJCastro fase2
            try{
                st= con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, usuario);
                rs=st.executeQuery();
                while(rs.next()){
                   perfil.setDistrito( rs.getString(1) );
                   perfil.setBase    ( rs.getString(2) );                                          
                }
            }catch(Exception e){
                throw new SQLException( e.getMessage());
            }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
            return perfil;     
    }
     
     
      
      
/**
 * 
 * @param proyecto
 * @param usuario
 * @param conn
 * @param user
 * @throws Exception
 */
     public void Insert (String proyecto, String usuario, Connection conn, String user) throws Exception {
        PreparedStatement stmt = null;
         String query = "INSERT_PROYECTO_USUARIO";//JJCastro fase2
        try{
            Perfil perfil = this.getDatosCiaBase( usuario, conn); 
            
            String distrito = perfil.getDistrito();
            String base     = perfil.getBase();
            
            stmt = conn.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            stmt.setString(1, distrito);
            stmt.setString(2, proyecto);
            stmt.setString(3, usuario);
            stmt.setString(4, user);
            stmt.setString(5, base);
            
            stmt.executeUpdate();
        }
        catch(SQLException e){
            throw new SQLException("Error en Insert [MenuDAO] " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
           if (stmt  != null){ try{ stmt.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }        
    }
     
     
     
     
     
/**
 * 
 * @param PERFIL
 * @param USUARIOS
 * @param conn
 * @param user
 * @throws Exception
 */
    public void update (String PERFIL, String USUARIOS, Connection conn, String user) throws Exception {
        PreparedStatement stmt = null;
        String query = "UPDATE_PROYECTO_USUARIO";//JJCastro fase2
        try{
            stmt = conn.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            stmt.setString(1, user);
            stmt.setString(2, PERFIL);
            stmt.setString(3, USUARIOS);             
            stmt.executeUpdate();
        }
        catch(SQLException e){
            throw new SQLException("Error en Update [MenuDAO] " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
           if (stmt  != null){ try{ stmt.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
         }        
    }
    
    
    
    
     public void verificar(String PERFIL, String[] USUARIOS, Connection conn, String user) throws Exception {
        PreparedStatement st = null;  
        ResultSet         rs = null;
        String query = "SQL_VERIFICACION";//JJCastro fase2
        try{
            st= conn.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,  PERFIL);
            rs=st.executeQuery();        
            if(rs!=null){
               while(rs.next()){
                 String usuario = rs.getString(1);
                 int sw = 0;
                 for(int j=0;j<=USUARIOS.length-1;j++){
                      String usuArray =  USUARIOS[j]; 
                      if(usuario.equals(usuArray)){
                         sw=1;
                         break;
                      }
                 }
                 if (sw==0) anular(PERFIL, usuario, conn, user);
               }
            }
        }
        catch(SQLException e){
            throw new SQLException("Error en Update [MenuDAO] " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
    }
    
    
    
    
     
/**
 * 
 * @param PERFIL
 * @param USUARIOS
 * @param conn
 * @param user
 * @throws Exception
 */
     public void anular (String PERFIL, String USUARIOS, Connection conn, String user) throws Exception {
        PreparedStatement stmt = null;
         String query = "SQL_ANULAR";//JJCastro fase2
        try{
            stmt = conn.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            stmt.setString(1, user);
            stmt.setString(2, PERFIL);
            stmt.setString(3, USUARIOS);             
            stmt.executeUpdate();
        }
        catch(SQLException e){
            throw new SQLException("Error en Update [MenuDAO] " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
           if (stmt  != null){ try{ stmt.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
         }        
    }
     
     
     
     
}
