/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.DAOS;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
/**
 *
 * @author Rhonalf
 */
public class CotizacionDAO extends MainDAO {

    public CotizacionDAO() {
        super("CotizacionDAO.xml");
    }
    public CotizacionDAO(String dataBaseName) {
        super("CotizacionDAO.xml", dataBaseName);
    }

    private int contarDigitos(int numero){
        int contador=0;
        while(numero>0){
            numero /=10;
            contador++;
        }
        return contador;
    }

    public String getConsecutivo() throws Exception{
		PreparedStatement st = null;
		ResultSet rs = null;
		Connection con = null;
		String query="GET_COTSER";
		String cantidad = "";
		try{
		    con = this.conectarJNDI(query);
		    String sql = obtenerSQL(query);
		    st = con.prepareStatement(sql);
		    st.setString(1, "COTSER");
		    rs=st.executeQuery();
		    if(rs.next())
		    {   cantidad = rs.getString("codigo");
		    }
		}
		catch(Exception ec){
		    throw new Exception("Error en la consulta contar todas las cotizaciones: "+ec.toString());
		}
		finally{
		    if(st != null){
		        try{
		            st.close();
		        }
		        catch(SQLException e){
		            throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
		        }
		    }
		    this.desconectar(con);
		}

	       /* try{
		    //contar = contarTodas() + 1;
		    contar = contarTodas() + 1;
		    int ndig = this.contarDigitos(contar);
		    switch(ndig){
		        case 0:
		            r="CT0000001";
		        break;
		        case 1:
		            r +="000000"+contar;
		        break;
		        case 2:
		            r +="00000"+contar;
		        break;
		        case 3:
		            r +="0000"+contar;
		        break;
		        case 4:
		            r +="000"+contar;
		        break;
		        case 5:
		            r +="00"+contar;
		        break;
		        case 6:
		            r +="0"+contar;
		        break;
		        default:
		            r+=contar;
		        break;
		    }
		}
		catch(Exception e){
		    System.out.println("Error en generacion de consecutivo: "+e.toString());
		    e.printStackTrace();
		}*/
		return cantidad;
    }

    private int contarTodas() throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="CONTAR_TODAS_2";
        int cantidad = 0;
        try{
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                cantidad = rs.getInt("numero");
            }
            return cantidad;
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta contar todas las cotizaciones: "+ec.toString());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    public void aprobarOrden(String consecutivo) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="APROBAR_COT";
        int rowCount = 0;
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1,consecutivo);
            rowCount=st.executeUpdate();
            if(rowCount>0) System.out.println("Cotizacion aprobada");
            else System.out.println("Oops...");
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta aprobar orden: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de aprobar cotizacion: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    public ArrayList buscar(String criterio,String cadena) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="BUSCAR_HEAD";
        Cotizacion cot = null;
        ArrayList res = new ArrayList();
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            sql = sql.replaceAll("#PARAM", criterio);
            st = con.prepareStatement(sql);
            st.setString(1, "%"+cadena+"%");
            rs = st.executeQuery();
            while(rs.next()){
                cot = new Cotizacion();
                cot.setCodigo(rs.getString("consecutivo"));
                cot.setFecha(rs.getString("fecha"));
                cot.setAccion(rs.getString("id_accion"));
                res.add(cot);
            }
            return res;
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta buscar datos cotizacion: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de buscar datos cotizacion: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    public ArrayList buscarDets(String criterio,String cadena) throws Exception {
        System.out.println("criterio:"+criterio+"_cadena:"+cadena);
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="BUSCAR_COT";
        Cotizacion cot = null;
        ArrayList res = new ArrayList();
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            sql = sql.replaceAll("#PARAM", criterio);
            st = con.prepareStatement(sql);
            st.setString(1, "%"+cadena+"%");
            rs = st.executeQuery();
            while(rs.next()){
                System.out.println("filax");
                cot = new Cotizacion();//091222
                cot.setCodigo(rs.getString("cod_cotizacion"));
                cot.setMaterial(rs.getString("codigo_material"));
                cot.setFecha(rs.getString("fecha"));
                cot.setObservacion("observacion");
                cot.setAccion(rs.getString("id_accion"));
                cot.setCantidad(rs.getDouble("cantidad"));
                cot.setId(rs.getString("idcotizaciondets"));//091222
                res.add(cot);
            }
            return res;
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta buscar orden: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de buscar orden: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    private String getConsecutivo(String accion,String fecha) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="BUSCAR_CONSEC";
        String retorno="";
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1,accion);
            st.setString(2,fecha);
            rs = st.executeQuery();
            if(rs.next()) {
                retorno = rs.getString("consecutivo");
            }
            else retorno = this.getConsecutivo();

            return retorno;
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de generacion de consecutivo con params: "+e.toString());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    private boolean existeConsec(String consecutivo) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="ENCONTRAR_CONSEC";
        boolean retorno=false;
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1,consecutivo);
            rs = st.executeQuery();
            if(rs.next()) {
                if(Integer.parseInt(rs.getString("numero"))>0) retorno = true;
            }
            return retorno;
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de busqueda de consecutivo: "+e.toString());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    public void insertDetPlusOrden(String producto,String accion,int cantidad,String nota,String fecha,String usuario) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        String query="INSERTAR_ORDEN";
        String consec = getConsecutivo(accion,fecha);
        int rowCount=0;
        con = this.conectarJNDI(query);
        String sql = obtenerSQL(query);
        st = con.prepareStatement(sql);
        try{
            if(existeConsec(consec)==true){
                System.out.println("La fila ya esta en tabla cotizacion. Pasar a adicion de detalles");
            }
            else {
                st.setString(1,consec);
                st.setString(2, fecha);
                st.setString(3, accion);
                st.setString(4, usuario);
                st.setString(5, accion);//091027
                rowCount=st.executeUpdate();
                if(rowCount>0) System.out.println("Insertada fila en tabla cotizacion. Pasar a insercion de detalles");
                else System.out.println("Oops... por alguna razon no se inserto la fila en cotizacion...");
            }
            query="INSERTAR_ORDEN_DETS";
            sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1,producto);
            st.setInt(2, cantidad);
            st.setString(3, consec);
            st.setString(4, fecha);
            st.setString(5, nota);
            st.setString(6, accion);
            rowCount=st.executeUpdate();
            if(rowCount>0) {
                System.out.println("Insertada fila tabla detalles");
                this.valoresAccionP1(accion,usuario);
            }
            else System.out.println("Oops... por alguna razon no se inserto la fila en detalles...");
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de insercion de orden plus dets: "+e.toString());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

     public void anularCotizacion(String consecutivo,String user) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        String query="ANULAR_ORDEN";
        int rowCount = 0;
        String accion="";
        try {
            accion = getAccion(consecutivo);
            System.out.println("antes de anular cotizacion "+consecutivo+" accion "+accion);
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1,user);
            st.setString(2, consecutivo);

            st.setString(3, user);
            st.setString(4,accion);
            st.setString(5,consecutivo);

            rowCount=st.executeUpdate();
            if(rowCount>0) System.out.println("Cotizacion "+consecutivo+" anulada");
            else System.out.println("Oops... no se pudo anular...");

            this.desconectar(con);
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta anular cotizacion: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de anular cotizacion: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
             if (con != null){
                try{
                    this.desconectar(con);
                } catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }

        }
    }

     private void valoresAccionP1(String idaccion,String usuario) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="DATOS_PARA_ACCION";
        double mat=0.0;
        double man=0.0;
        double otr=0.0;
        boolean sip = false;
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, idaccion);
            rs = st.executeQuery();
            while(rs.next()){
                if(rs.getString(1).equals("M")) mat = rs.getDouble(3);
                if(rs.getString(1).equals("D")) man = rs.getDouble(3);
                if(rs.getString(1).equals("O")) otr = rs.getDouble(3);
                sip = true;
            }
            if(sip) this.valorizarAccionP1(idaccion, mat, man, otr,usuario);
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta buscar valores: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de buscar valores: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
     }

     private void valorizarAccionP1(String idaccion,double material,double mano,double otro,String user) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        String query="VALORIZAR_ACCION";
        int rowCount = 0;
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setDouble(1,material);
            st.setDouble(2,mano);
            st.setDouble(3,otro);
            st.setString(4, user);
            st.setString(5, idaccion);
            rowCount=st.executeUpdate();
            if(rowCount>0) System.out.println("Accion modificada");
            else System.out.println("Oops... no se pudo modificar la accion...");
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta valorizar Accion: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de valorizar Accion: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
     }

     private String getAccion(String consecutivo) throws Exception {
        String retorno="";
        try {
            ArrayList x = this.buscar("consecutivo", consecutivo);
            Cotizacion cot = (Cotizacion)x.get(0);
            retorno = cot.getAccion();
            return retorno;
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de buscar accion: "+e.toString());
        }
     }

     private void actualizarAccion(String accion,String user) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        String query="ACTUALIZAR_ACCION";
        int rowCount = 0;
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1,user);
            st.setString(2,accion);
            rowCount=st.executeUpdate();
            if(rowCount>0) System.out.println("Accion modificada");
            else System.out.println("Oops...");
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta modificar Accion: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de modificar Accion: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
     }

     public ArrayList detallesConsec(String consecutivo) throws NullPointerException,Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="BUSCAR_POR_CONSEC";
        ArrayList resultado = new ArrayList();
        Cotizacion cot = null;
        try{
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, consecutivo);
            rs=st.executeQuery();
            while(rs.next()){
               cot = new Cotizacion();
               cot.setAccion(rs.getString("id_accion"));
               cot.setCantidad(rs.getDouble("cantidad"));
               cot.setCodigo(rs.getString("cod_cotizacion"));
               cot.setFecha(rs.getString("fecha"));
               cot.setMaterial(rs.getString("codigo_material"));
               cot.setObservacion(rs.getString("observacion"));
               resultado.add(cot);
            }
            return resultado;
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta listar detalles: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de listado de detalles: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

     public ArrayList verPendientes() throws NullPointerException,Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="VER_PEND";
        ArrayList resultado = new ArrayList();
        Cotizacion cot = null;
        try{
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                cot = new Cotizacion();
                cot.setAccion(rs.getString("id_accion"));
                cot.setCodigo(rs.getString("consecutivo"));
                cot.setFecha(rs.getString("fecha"));
                resultado.add(cot);
            }
            return resultado;
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta listar todas las cotizaciones pendientes: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de listado de cotizaciones pendientes: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    public ArrayList ver_Aprobadas() throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="VER_APROBADAS";
        ArrayList resultado = new ArrayList();
        Cotizacion cot = null;
        try{
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                cot = new Cotizacion();
                cot.setAccion(rs.getString("id_accion"));
                cot.setCodigo(rs.getString("consecutivo"));
                cot.setFecha(rs.getString("fecha"));
                resultado.add(cot);
            }
            return resultado;
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta listar todas las cotizaciones aprobadas: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de listado de cotizaciones aprobadas: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    public ArrayList verTodas() throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="VER_TODAS";
        ArrayList resultado = new ArrayList();
        Cotizacion cot = null;
        try{
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                cot = new Cotizacion();
                cot.setAccion(rs.getString("id_accion"));
                cot.setCodigo(rs.getString("consecutivo"));
                cot.setFecha(rs.getString("fecha"));
                resultado.add(cot);
            }
            return resultado;
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta listar todas las cotizaciones aprobadas: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de listado de cotizaciones aprobadas: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    public TreeMap datosMs() throws Exception {
        TreeMap x = new TreeMap();
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="DATOS_MS";
        try{
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            rs=st.executeQuery();
            int i = 0;
            x.put("...","...");
            while(rs.next()) {
                x.put(rs.getString(1)+" "+rs.getString(2)+" "+rs.getString(3),rs.getString(1));
                i++;
            }
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta datos ms: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de datos ms: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
        return x;
    }

    public String datosAccion(String idaccion) throws Exception {
        String x = "";
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="DATOS_ACT";
        try{
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, idaccion);
            rs=st.executeQuery();
            int i = 0;
            while(rs.next()) {
                x = rs.getString(2)+";"+rs.getString(3);
                i++;
            }
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta datos accion: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de datos accion: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
        return x;
    }

    public boolean existeCotizacion(String accion) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="VERIFICAR_EXIST";
        boolean retorno=false;
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1,accion);
            rs = st.executeQuery();
            if(rs.next()) {
                if(Integer.parseInt(rs.getString("numero"))>0) retorno = true;
            }
            return retorno;
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de busqueda de cotizacion: "+e.toString());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    //added on 24-oct-2009
    /*public ArrayList getvaloresAccion(String idaccion) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="DATOS_PARA_ACCION";
        ArrayList resp = new ArrayList();
        double[] vals = new double[3];
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, idaccion);
            rs = st.executeQuery();
            while(rs.next()){
                if(rs.getString(1).equals("M")) vals[0] = rs.getDouble(3);
                if(rs.getString(1).equals("D")) vals[1] = rs.getDouble(3);
                if(rs.getString(1).equals("O")) vals[2] = rs.getDouble(3);
                
                System.out.println("vals[0]"+vals[0]+"vals[1]"+vals[1]+"vals[2]"+vals[2]);
                
                resp.add(vals);
            }
            return resp;
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta buscar valores accion: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de buscar valores accion: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }*/
    
    public ArrayList getvaloresAccion(String idaccion) throws Exception{//091029
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="DATOS_PARA_ACCION";
        ArrayList resp = new ArrayList();
        double[] vals = new double[3];
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, idaccion);
            rs = st.executeQuery();
            while(rs.next()){
                if(rs.getString(1).equals("M")) vals[0] = rs.getDouble(3);
                if(rs.getString(1).equals("D")) vals[1] = rs.getDouble(3);
                if(rs.getString(1).equals("O")) vals[2] = rs.getDouble(3);                
            }
            System.out.println("vals[0]"+vals[0]+"vals[1]"+vals[1]+"vals[2]"+vals[2]);
            resp.add(vals);//091029            
            return resp;
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta buscar valores accion: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de buscar valores accion: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    public String getConsecutivo(String accion) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="BUSCAR_CONSEC_2";
        String retorno="";
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1,accion);
            rs = st.executeQuery();
            if(rs.next()) {
                retorno = rs.getString("consecutivo");
            }
            else retorno = this.getConsecutivo();

            return retorno;
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de generacion de consecutivo con params: "+e.toString());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

public void valorizarAIU(double pa,double pi,double pu,double a,double i,double u,String user,String accion) throws Exception {
        StringStatement st = null;
        String query="VALOR_AIU";
        String sql="";
        PreparedStatement st2 = null;
        Connection con = null;
        try{
            //Actualiza los valores de AIU
            st = new StringStatement(obtenerSQL(query));
            st.setDouble(pa);
            st.setDouble(pi);
            st.setDouble(pu);
            st.setDouble(a);
            st.setDouble(i);
            st.setDouble(u);
            st.setString(user);
            st.setString(accion);

            //Actualiza precio de venta
            ElectricaribeOfertaDAO eod=new ElectricaribeOfertaDAO();
            DateFormat dateFormat = new SimpleDateFormat("yyyyMM");
            sql=st.getSql()+";"+eod.actPrecio_venta(this.getIdSolicitud(accion), dateFormat.format(new java.util.Date()));
            System.out.println(sql);
            //Ejecuta todo
            con = this.conectarJNDI(query);
            st2 = con.prepareStatement(sql);
            st2.execute();

        }
        catch(Exception e){
            e.printStackTrace();
            throw new Exception("Error en la consulta de generacion de consecutivo con params: "+e.toString());
        }
        finally{
            if(st2 != null){
                try{
                    st2.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }
    //added 27-oct-2009
    public boolean hayValoresAiu(String idaccion) throws Exception{
        boolean hay = true;
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="HAY_AIU";
        String retorno = "";
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, idaccion);
            rs = st.executeQuery();
            if(rs.next()) {
                retorno = rs.getString(1);
                hay = false;
            }
            return hay;
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de buscar valores existentes aiu: "+e.toString());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }
    
    
    public double[] getPorcentajesAIU(String idaccion) throws Exception{//091029
        double[] hay = new double[3];
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="GET_PORC_AIU";
        String retorno = "";
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, idaccion);
            rs = st.executeQuery();
            if(rs.next()) {
                hay[0]=Double.parseDouble(rs.getString("porc_administracion"));
                hay[1]=Double.parseDouble(rs.getString("porc_imprevisto"));
                hay[2]=Double.parseDouble(rs.getString("porc_utilidad"));
                //hay = false;
            }
            return hay;
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de buscar valores existentes aiu: "+e.toString());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    //091222
public void anularRegistro(String id_registro,String usuario) throws SQLException,Exception {
        PreparedStatement st = null;
        Connection con = null;
        String query = "ANULAR_DET";
        Cotizacion cds = null;
        int rowCount=0;
        try{
            //Se crea el objeto a partir de la consulta con el objetivo de actualizar la accion
	    cds = (Cotizacion)this.buscarDets("idcotizaciondets", id_registro).get(0);//100104
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, id_registro);
            rowCount=st.executeUpdate();
            if(rowCount>0){
                System.out.println("Registro identificado como "+id_registro+" ha sido anulado en la tabla cotizaciondets");

                System.out.println("Preparando modificacion de datos para la accion "+cds.getAccion());//100104
                this.valoresAccionP1(cds.getAccion(),usuario);//100104
            }
            else{
                System.out.println("Oops ... paso algo en la consulta de anular registros de cotizaciondets para el id "+id_registro);
                throw new Exception("Por alguna razon no se pudo anular el registro "+id_registro+" ...");
            }
        }
        catch(Exception e){
            throw new Exception("Error al anular el registro identificado como "+id_registro+"... : "+e.getMessage());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }
    public void guardarBorrador(String producto,String accion,int cantidad,String nota,String fecha,String usuario) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        String query="GUARDAR_COT";
        String consec = getConsecutivo(accion,fecha);
        int rowCount=0;
        con = this.conectarJNDI(query);
        String sql = obtenerSQL(query);
        st = con.prepareStatement(sql);
        try{
            if(existeConsec(consec)==true){
                System.out.println("La fila ya esta en tabla cotizacion. Pasar a adicion de detalles");
            }
            else {
                st.setString(1,consec);
                st.setString(2, fecha);
                st.setString(3, accion);
                st.setString(4, usuario);
                rowCount=st.executeUpdate();
                if(rowCount>0) System.out.println("Insertada fila borrador en tabla cotizacion. Pasar a insercion de detalles");
                else System.out.println("Oops... por alguna razon no se inserto la fila borrador en cotizacion...");
            }
            query="INSERTAR_ORDEN_DETS";
            sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1,producto);
            st.setInt(2, cantidad);
            st.setString(3, consec);
            st.setString(4, fecha);
            st.setString(5, nota);
            st.setString(6, accion);
            rowCount=st.executeUpdate();
            if(rowCount>0) {
                System.out.println("Insertada fila tabla detalles");
            }
            else System.out.println("Oops... por alguna razon no se inserto la fila en detalles...");
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de insercion de orden plus dets: "+e.toString());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    public void borrarDetalles(String accion) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        String query="DEL_COT";
        int rowCount=0;
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, accion);
            rowCount = st.executeUpdate();
            if(rowCount>0) System.out.println("Detalles de cotizacion de la accion "+accion+" han sido anulados");
            else System.out.println("Los detalles de cotizacion de la accion "+accion+" no pudieron ser anulados");
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de borrado de detalles: "+e.toString());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    public boolean buscarBorrador(String accion) throws Exception {
        boolean existe = false;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query="BUSCAR_GUARDADO";
        try{
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, accion);
            rs = st.executeQuery();
            if (rs.next()) existe = true;
            return existe;
        }
        catch(Exception e){
            throw new Exception("Error buscando existencia de borrador: "+e.toString());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                    if(rs!=null) rs.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    public void actualizarEstado(String accion,String usuario) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        String query="ACT_COT";
        int rowCount=0;
        try{
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            sql = sql.replaceAll("#user", usuario);
            st = con.prepareStatement(sql);
            st.setString(1, accion);
            st.setString(2, accion);
            rowCount = st.executeUpdate();
            if(rowCount>0) System.out.println("El estado de la cotizacion para la accion "+accion+" ha sido actualizado");
            else System.out.println("El estado de la cotizacion para la accion "+accion+" no pudo ser actualizado");
            this.desconectar(con);
        }
        catch(Exception e){
            throw new Exception("Error en la actualizacion de estado de cotizacion: "+e.toString());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            if (con != null){
                try{
                    this.desconectar(con);
                } catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }

        }
    }


    public String nombreContratista(String idaction) throws SQLException,NullPointerException,Exception {
        String nombre="";
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="CONT_ACCION";
        try{
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, idaction);
            rs = st.executeQuery();
            if(rs.next()) nombre = rs.getString(1);
            return nombre;
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta buscar nombre contratista de la accion: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de buscar nombre contratista de la accion: "+e.toString());
        }
        finally {
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    public String getIdSolicitud(String idaccion) throws Exception{
        Connection          con     = null;
        PreparedStatement   ps      = null;
        ResultSet           rs      = null;
        String              query   = "SQL_GET_IDSOLICITUD";
        String              sql     = "";
        String              id      = "";

        try{
            con         = this.conectarJNDI(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, idaccion);
            rs          = ps.executeQuery();

            if(rs.next()){
                id = rs.getString("id_solicitud");
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con);   } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return id;
    }
    public void insertDetPlusOrden(ArrayList lista,String usuario,String fecha) throws Exception{
        int tam = lista.size();
        String query="";
        String sql = "";
        String cons = "";
        String accion="";
        Cotizacion ct = null;
        PreparedStatement st = null;
        Connection con = null;
        boolean xst = false;
        Vector batch = new Vector();
        try{
            ct = (Cotizacion) lista.get(0);
            cons = this.getConsecutivo(ct.getAccion(), fecha);
            if(tam>0){
                for(int i=0;i<tam;i++){
                    ct = (Cotizacion) lista.get(i);
                    accion = ct.getAccion();
                    if(i==0) { System.out.println("Insertando datos de cotizacion para la accion "+ct.getAccion() +" ,cotizacion num. "+cons); }
                    if(this.existeConsec(cons)==false && xst==false) {
                        //Aca se inserta la cabecera y un detalle
                        query="INSERTAR_ORDEN";
                        sql = this.obtenerSQL(query);
                        con = this.conectar(query);
                        st = con.prepareStatement(sql);
                        st.setString(1,cons);
                        st.setString(2, ct.getFecha());
                        st.setString(3, ct.getAccion());
                        st.setString(4, usuario);
                        xst=true;
                        batch.add(st.toString());
                        this.desconectar(query);

                        System.out.println("Agregando cabecera...");

                        query="INSERTAR_ORDEN_DETS";
                        sql = this.obtenerSQL(query);
                        con = this.conectar(query);
                        st = con.prepareStatement(sql);
                        st.setString(1,ct.getMaterial());
                        st.setDouble(2, ct.getCantidad());
                        st.setString(3, cons);
                        st.setString(4, ct.getFecha());
                        st.setString(5, ct.getObservacion());
                        st.setString(6, ct.getAccion());
                        batch.add(st.toString());
                        this.desconectar(query);
                        System.out.println("Agregando detalle...");
                    }
                    else {
                        //Aca se inserta un detalle
                        query="INSERTAR_ORDEN_DETS";
                        sql = this.obtenerSQL(query);
                        con = this.conectar(query);
                        st = con.prepareStatement(sql);
                        st.setString(1,ct.getMaterial());
                        st.setDouble(2, ct.getCantidad());
                        st.setString(3, cons);
                        st.setString(4, ct.getFecha());
                        st.setString(5, ct.getObservacion());
                        st.setString(6, ct.getAccion());
                        batch.add(st.toString());
                        this.desconectar(query);
                        System.out.println("Agregando detalle...");
                    }

                }

                //se actualizan los valores de la accion
                query = "UPD_COT";
                sql = this.obtenerSQL(query);
                sql = sql.replaceAll("#accion", accion);
                ClientesVerDAO clvd = new ClientesVerDAO();
                String estado = clvd.estadoAccion(accion);
                //Si se esta creando una cotizacion nueva
                if(estado.equals("030")){
                    sql = sql + "UPDATE acciones SET estado='040' WHERE id_accion='#accion';";
                    sql = sql.replaceAll("#accion", accion);
                    System.out.println("Creando cotizacion y actualizando accion...");
                }

                con = this.conectar(query);
                st = con.prepareStatement(sql);
                st.setString(1, usuario);
                batch.add(st.toString());
                this.desconectar(query);

                if(estado.equals("030") || estado.equals("040")){
                     System.out.println("No se actualizaran precios venta ya que no se ha generado oferta para la accion "+accion);
                }
                else{
                    ElectricaribeOfertaDAO eod=new ElectricaribeOfertaDAO();
                    DateFormat dateFormat = new SimpleDateFormat("yyyyMM");
                    batch.add(eod.actPrecio_venta(this.getIdSolicitud(accion), dateFormat.format(new java.util.Date())));
                    System.out.println("Se actualizaran precios venta para la accion "+accion);


/*                    String idsolicitud = this.getIdSolicitud(accion);
                    query = "SQL_PRECVEN";
                    sql = this.obtenerSQL(query);
                    con = this.conectar(query);
                    st = con.prepareStatement(sql);
                    st.setString(1, idsolicitud);*/
                    //this.desconectar(query);

                }

                ApplusDAO apdao = new ApplusDAO();
                apdao.ejecutarSQL(batch);
                System.out.println("Proceso realizado...");
            }

        }
        catch(Exception e){
             throw new Exception("Error creando cotizacion: "+e.getMessage());
        }
        finally{
            if (st  != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                try{
                    this.desconectar(con);
                } catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    public void guardarBorrador(ArrayList lista,String usuario,String fecha) throws SQLException,Exception{
        int tam = lista.size();
        String query="";
        String sql = "";
        String cons = "";
        Cotizacion ct = null;
        PreparedStatement st = null;
        Connection con = null;
        boolean xst = false;
        Vector batch = new Vector();
        try{
            ct = (Cotizacion) lista.get(0);
            cons = this.getConsecutivo(ct.getAccion(), fecha);
            for(int i=0;i<tam;i++){
                ct = (Cotizacion) lista.get(i);
                if(i==0) { System.out.println("Insertando datos de cotizacion para la accion "+ct.getAccion() +" ,cotizacion num. "+cons); }
                if( this.existeConsec(cons)==false && xst==false) {
                    query="GUARDAR_COT";
                    sql = this.obtenerSQL(query);
                    con = this.conectar(query);
                    st = con.prepareStatement(sql);
                    st.setString(1,cons);
                    st.setString(2, ct.getFecha());
                    st.setString(3, ct.getAccion());
                    st.setString(4, usuario);
                    batch.add(st.toString());
                    xst = true;
                    System.out.println("Agregando cabecera borrador....");
                    this.desconectar(query);

                    query="INSERTAR_ORDEN_DETS";
                    sql = this.obtenerSQL(query);
                    con = this.conectar(query);
                    st = con.prepareStatement(sql);
                    st.setString(1,ct.getMaterial());
                    st.setDouble(2, ct.getCantidad());
                    st.setString(3, cons);
                    st.setString(4, ct.getFecha());
                    st.setString(5, ct.getObservacion());
                    st.setString(6, ct.getAccion());
                    batch.add(st.toString());
                    this.desconectar(query);
                    System.out.println("Agregando detalle...");
                }
                else {
                    query="INSERTAR_ORDEN_DETS";
                    sql = this.obtenerSQL(query);
                    con = this.conectar(query);
                    st = con.prepareStatement(sql);
                    st.setString(1,ct.getMaterial());
                    st.setDouble(2, ct.getCantidad());
                    st.setString(3, cons);
                    st.setString(4, ct.getFecha());
                    st.setString(5, ct.getObservacion());
                    st.setString(6, ct.getAccion());
                    batch.add(st.toString());
                    this.desconectar(query);
                    System.out.println("Agregando detalle...");
                }

            }

            this.desconectar(query);
            ApplusDAO apdao = new ApplusDAO();
            apdao.ejecutarSQL(batch);
            System.out.println("Proceso realizado...");
        }
        catch(Exception e){
             throw new Exception("Error creando cotizacion: "+e.getMessage());
        }
        finally{
            if (st  != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                try{
                    this.desconectar(con);
                } catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    public void anularRegistro(ArrayList listado,String usuario) throws Exception {
        Vector batch = new Vector();
        int dsize = listado.size();
        String id_registro="";
        String qstr="";
        Cotizacion cds = null;
        String query = "";
        String sql="";
        String accion="";
        PreparedStatement st = null;
        Connection con = null;
        try{
            if(dsize>0){
                for(int i=0;i<dsize;i++) {
                    id_registro = (String)listado.get(i);
                    qstr = qstr + "UPDATE cotizaciondets SET reg_status='A',precio_venta=0 WHERE idcotizaciondets = '"+id_registro+"' ;\n";
                    System.out.println("anular registro "+id_registro);

                }
                cds = (Cotizacion)this.buscarDets("idcotizaciondets", id_registro).get(0);
                accion=cds.getAccion();
                query = "UPD_COT";
                sql = this.obtenerSQL(query);
                sql = sql.replaceAll("#accion", accion);
                sql = qstr+sql;

                con = this.conectar(query);
                st = con.prepareStatement(sql);
                st.setString(1, usuario);
                batch.add(st.toString());

                st.close();
                this.desconectar(query);

                ApplusDAO apdao = new ApplusDAO();
                apdao.ejecutarSQL(batch);
            }
        }
        catch(Exception e){
            throw new Exception("Error anulando detalle: "+e.getMessage());
        }
        finally{
            if (st  != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                try{
                    this.desconectar(con);
                } catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }


}
