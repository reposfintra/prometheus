/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

/**
 *
 * @author aariza
 */

import com.tsp.operation.model.beans.Politicas;
import com.tsp.operation.model.beans.PreReporteCentralRiesgo;
import com.tsp.operation.model.beans.listaPoliticaBeans;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class ListaPoliticasReporteDAO extends MainDAO {
    
    public ListaPoliticasReporteDAO(String dataBaseName){
        super("ListaPoliticasReporteDAO.xml", dataBaseName);
    }
    
    
    
    public ArrayList<listaPoliticaBeans> getPoliticas() throws Exception {
        ArrayList politicas = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_OBTENER_POLITICAS");
            String sql = this.obtenerSQL("SQL_OBTENER_POLITICAS");
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            politicas = new ArrayList();
            while (rs.next()) {
                listaPoliticaBeans politica = new listaPoliticaBeans();
                politica.setIdPolitica(rs.getInt("id_politica"));
                politica.setPolitica(rs.getString("politica"));
                politica.setEdadIni(rs.getInt("edad_ini"));
                politica.setEdadFin(rs.getInt("edad_fin"));
                politica.setCentralRiesgo(rs.getString("central_riesgo"));
                politica.setIdConvenio(rs.getInt("id_convenio"));
                politica.setConvenio(rs.getString("convenio"));
                politica.setMontoIni(rs.getInt("monto_ini"));
                politica.setMontoFin(rs.getInt("monto_fin"));
                politica.setMonto(rs.getBoolean("monto"));
                politica.setEdad(rs.getBoolean("edad"));
              
             
                politicas.add(politica);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return politicas;
    }
 
    
     public ArrayList getNomConvenios(String entidad) throws Exception {
        ArrayList name = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_OBTENER_CONVENIOS");
            String sql = this.obtenerSQL("SQL_OBTENER_CONVENIOS");
            ps = con.prepareStatement(sql);
                ps.setString(1, entidad);
               name = new ArrayList();
            rs = ps.executeQuery();
           
            while (rs.next()) {
                name.add(rs.getString("idconvenio")+"-"+rs.getString("nombre"));                                
             }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return name;
    }
     
   
     
     
  public String insertPolitica(Politicas politica) throws SQLException{
  
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String resultado = "OK";
        try {
            con = conectarJNDI("SQL_INSERTAR_POLITICAS");
            con.setAutoCommit(false);
           
            String sql = this.obtenerSQL("SQL_INSERTAR_POLITICAS");
            ps = con.prepareStatement(sql);
            
            ps.setString(1, politica.getPolitica());
            ps.setString(2, politica.getCentralRiesgo());
            ps.setString(3, politica.getUnidadNegocio());
            ps.setInt(4, politica.getIdConvenio());
            ps.setInt(5, politica.getEdadIni());
            ps.setInt(6, politica.getEdadFin());
            ps.setDouble(7, politica.getMontoIni());
            ps.setDouble(8, politica.getMontoFin());
            ps.setString(9, politica.getAgenciaCobro());
            ps.setBoolean(10, politica.isMonto());//monto
            ps.setBoolean(11, politica.isAgencia());//agencia
            ps.setBoolean(12, politica.isEdad());//edad
            
            
            ps.execute();
            con.commit();
        } catch (Exception e) {
            con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
            resultado = "ER";
        } finally {
            
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
        return resultado;
    }
  public void updatePolitica(Politicas politica, int idPolitica) throws SQLException{
  
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_ACTUALIZAR_POLITICAS");
            con.setAutoCommit(false);
         
            String sql = this.obtenerSQL("SQL_ACTUALIZAR_POLITICAS");
            ps = con.prepareStatement(sql);
            
            ps.setString(1, politica.getPolitica());
            ps.setString(2, politica.getCentralRiesgo());
            ps.setString(3, politica.getUnidadNegocio());
            ps.setInt(4, politica.getIdConvenio());
            ps.setInt(5, politica.getEdadIni());
            ps.setInt(6, politica.getEdadFin());
            ps.setDouble(7, politica.getMontoIni());
            ps.setDouble(8, politica.getMontoFin());
            ps.setString(9, politica.getAgenciaCobro());
            ps.setBoolean(10, politica.isMonto());//monto
            ps.setBoolean(11, politica.isAgencia());//agencia
            ps.setBoolean(12, politica.isEdad());//edad
            ps.setInt(13, idPolitica);
            
            ps.execute();
            con.commit();
        } catch (Exception e) {
            con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
    }
  
public String deletePolitica(int idPolitica) throws SQLException{
  
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String resultado = "OK";
        try {
            con = conectarJNDI("SQL_BORRAR_POLITICAS");
            con.setAutoCommit(false);
         
            String sql = this.obtenerSQL("SQL_BORRAR_POLITICAS");
            ps = con.prepareStatement(sql);

            ps.setInt(1, idPolitica);
            
            ps.execute();
            con.commit();
        } catch (Exception e) {
            con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
            resultado = "ER";
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
        return resultado;
    }

public String deleteEntidadConvenio(int id) throws SQLException{
  
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String resultado = "OK";
        try {
            con = conectarJNDI("SQL_BORRAR_ENTIDAD_CONVENIO");
            con.setAutoCommit(false);
         
            String sql = this.obtenerSQL("SQL_BORRAR_ENTIDAD_CONVENIO");
            ps = con.prepareStatement(sql);

            ps.setInt(1, id);
            
            ps.execute();
            con.commit();
        } catch (Exception e) {
            con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
            resultado = "ER";
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
        return resultado;
    }


public ArrayList<PreReporteCentralRiesgo> getPreReporte(String rango1, String rango2, String convenio) throws Exception {
        ArrayList reporte = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_PRE_REPORTE");
            String sql = this.obtenerSQL("SQL_PRE_REPORTE");
            String query=sql.replace("#CONVENIOS", convenio);
            ps = con.prepareStatement(query);
            rango1=rango1+" days";
            rango2=rango2+" days";
            //ps.setString(1,convenio);
            ps.setString(1,rango1);
            ps.setString(2,rango2);
            rs = ps.executeQuery();
            reporte= new ArrayList();
            while (rs.next()) {
                PreReporteCentralRiesgo report = new PreReporteCentralRiesgo();
                report.setObligacionPendiente(rs.getDouble("obligacion_pendiente"));
                report.setNegocio(rs.getString("negocio"));
                report.setNomCliente(rs.getString("nombre"));
                report.setIdentificacion(rs.getString("identificacion"));
                report.setNumCuotas(rs.getString("numero_cuotas"));
                report.setObligacionTotal(rs.getDouble("obligacion_total"));
                report.setCuotaMinVencida(rs.getInt("cuota_minima_venc"));
                report.setCuotaMaxVencida(rs.getInt("cuota_max_venc"));
                report.setEstado(rs.getInt("dias_mora"));
                report.setCuotasEnMora(rs.getInt("cuotas_en_mora"));
                report.setFechaApertura(rs.getString("fecha_apertura"));
                report.setFechaVenci(rs.getString("fecha_vencimiento"));
                             
                reporte.add(report);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return reporte;
    }




  public ArrayList rstotbl (ResultSet rs) throws Exception {
        ArrayList tabla=new ArrayList();
        while(rs.next()){
           ArrayList tabla2=new ArrayList();
           for(int i=1;i<=rs.getMetaData().getColumnCount();i++)
           {    tabla2.add(rs.getString(i));
           }
           tabla.add(tabla2);
        }
        return tabla;
    }

public ArrayList getConsulta(String parametro, String rango1, String rango2, String convenio)throws Exception{

        Connection con=null;
        PreparedStatement st=null;
        String sql="";
        ArrayList resultado=new ArrayList();
        ResultSet rs=null;
        String sql2="";
        try{
            sql = this.obtenerSQL("SQL_GET_CONSULTA");
            con = this.conectarJNDI("SQL_GET_CONSULTA");
            st = con.prepareStatement(sql);
            st.setString(1, parametro);
            rs = st.executeQuery();
            if (rs.next()) {
                sql2 = rs.getString("sql_txt");
            }
            if (st != null) {
                st.close();
            }
            if (rs != null) {
                rs.close();
            }
            
            //ejecutamos el segundo query con los parametros 
            //de los rangos de la politica.
            
            if(!sql2.equals("")){
            
                rango1 = rango1 + " days";
                rango2 = rango2 + " days";
                sql2=sql2.replace("#CONVENIO",convenio);
                st = con.prepareStatement(sql2);
                //primer rango
                st.setString(1, rango1);
                st.setString(2, rango2);
                //segundo rango
                st.setString(3, rango1);
                st.setString(4, rango2);

                rs = st.executeQuery();
                resultado = this.rstotbl(rs);
                
                //guardar historico del reporte.
                //creamos un consecutivo para el historico.
                this.guardarHistoricoReporteData(rango1, rango2,convenio);
              
        }
            
                        
          
        }
        catch(Exception e){
            e.printStackTrace();
            e.toString();
        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(st!=null){st.close();}
            if(rs!=null){rs.close();}
        }
        return resultado;

    }

 public ArrayList getEntidades() throws Exception {
        ArrayList ent = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_OBTENER_ENTIDADES");
            String sql = this.obtenerSQL("SQL_OBTENER_ENTIDADES");
            ps = con.prepareStatement(sql);
             ent = new ArrayList();
            rs = ps.executeQuery();
           
            while (rs.next()) {
                ent.add(rs.getInt("id_entidad")+"-"+rs.getString("nombre"));                                
             }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return ent;
    }
 
 
  public ArrayList getEntidadConvenio() throws Exception {
        ArrayList ent = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_ENTIDADES_CONVENIOS");
            String sql = this.obtenerSQL("SQL_ENTIDADES_CONVENIOS");
            ps = con.prepareStatement(sql);
             ent = new ArrayList();
            rs = ps.executeQuery();
           
            while (rs.next()) {
                ent.add(rs.getString("entidad")+"-"+rs.getString("convenio")+"-"+rs.getInt("id"));                                
             }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return ent;
    }
 
 
 

   public ArrayList getConvenios() throws Exception {
        ArrayList conveni = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_CONVENIO");
            String sql = this.obtenerSQL("SQL_CONVENIO");
            ps = con.prepareStatement(sql);
             conveni = new ArrayList();
            rs = ps.executeQuery();
           
            while (rs.next()) {
                conveni.add(rs.getInt("id_convenio")+"-"+rs.getString("nombre"));                                
             }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return conveni;
    }
   
   
  public String insertEntidadConvenio(int entidad, int convenio) throws SQLException{
  
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String resultado = "OK";
        try {
            con = conectarJNDI("SQL_INSERTAR_ENTIDAD_CONVENIO");
            con.setAutoCommit(false);
           
            String sql = this.obtenerSQL("SQL_INSERTAR_ENTIDAD_CONVENIO");
            ps = con.prepareStatement(sql);
            
            ps.setInt(1, convenio);
            ps.setInt(2, entidad);
            ps.execute();
            con.commit();
        } catch (Exception e) {
            con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
            resultado = "ER";
        } finally {
            
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
        return resultado;
    } 
  
   public String insertEntidad(String entidad) throws SQLException{
  
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String resultado = "OK";
        try {
            con = conectarJNDI("SQL_INSERTAR_ENTIDAD");
            con.setAutoCommit(false);
           
            String sql = this.obtenerSQL("SQL_INSERTAR_ENTIDAD");
            ps = con.prepareStatement(sql);
            
            ps.setString(1, entidad);
            ps.execute();
            con.commit();
        } catch (Exception e) {
            con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
            resultado = "ER";
        } finally {
            
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
        return resultado;
    } 
   
   
    public String getRangosVencimientos(String politica, String central_riesgo)throws SQLException{
        String rangos ="";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_RANGOS_VENCIMIENTOS");
            String sql = this.obtenerSQL("SQL_RANGOS_VENCIMIENTOS");
            ps = con.prepareStatement(sql);
            ps.setString(1,politica);
            ps.setString(2, central_riesgo);
            rs = ps.executeQuery();
           
            while (rs.next()) {
                rangos = rs.getInt("edad_ini") + "," + rs.getInt("edad_fin")
                        + "," + rs.getInt("convenio") + "," + rs.getString("unidad_negocio")
                        + "," + rs.getString("tipo_reporte");                              
    }
        } catch (Exception e) {
             System.out.println(e.toString());
             throw new SQLException(e.getMessage());
     
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return rangos;
    }
     
    
    private void guardarHistoricoReporteData(String rango1, String rango2, String convenio) throws SQLException {

        Connection con = null;
        PreparedStatement st = null;
        String sql = "";
        String codigo = "";
        ResultSet rs = null;

        try {

            //obtengo el consecutivo para el historico.
            sql = this.obtenerSQL("SQL_SERIE_HIST");
            con = this.conectarJNDI("SQL_SERIE_HIST");
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            if (rs.next()) {
                codigo = rs.getString("codigo");
            }

            //desconecto todo...
            if (st != null) {
                st.close();
            }
            if (rs != null) {
                rs.close();
            }
            if (con != null) {
                this.desconectar(con);
            }

            //en este bloque insertamos el historico.
            if (!codigo.equals("")) {

                sql = this.obtenerSQL("SQL_INSERT_HIST");
                con = this.conectarJNDI("SQL_INSERT_HIST");
                sql=sql.replace("#CONVENIO",convenio);
                st = con.prepareStatement(sql);
                st.setString(1, rango1);
                st.setString(2, rango2);
                st.setString(3, codigo);
                st.setString(4, rango1);
                st.setString(5, rango2);
                st.setString(6, rango1);
                st.setString(7, rango2);
                st.executeUpdate();

                //desconecto todo...
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            e.toString();
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (st != null) {
                st.close();
            }
            if (rs != null) {
                rs.close();
            }
        }

    }
    
    
    
    public String getPrimerReporte(String parametro)throws SQLException{
        String resultado ="";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_PRIMER_REPORTE");
            String sql = this.obtenerSQL("SQL_PRIMER_REPORTE");
            ps = con.prepareStatement(sql);
            ps.setString(1,parametro);
        
            rs = ps.executeQuery();
           
            while (rs.next()) {
                resultado = rs.getString("dato");                              
             }
        } catch (Exception e) {
             System.out.println(e.toString());
             throw new SQLException(e.getMessage());
            
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return resultado;
    }
    
    
    
    public void actualizarReporte(String parametro) throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;

        try {
            con = conectarJNDI("SQL_ACTUALIZAR_REPORTE");
            String sql = this.obtenerSQL("SQL_ACTUALIZAR_REPORTE");
            ps = con.prepareStatement(sql);
            ps.setString(1, parametro);

            ps.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.toString());
            throw new SQLException(e.getMessage());

        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
    }
   
    }
 
