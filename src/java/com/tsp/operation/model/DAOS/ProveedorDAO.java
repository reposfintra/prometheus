package com.tsp.operation.model.DAOS;

import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;

/**
 *
 * @author  Tito Andres
 */
public class ProveedorDAO extends MainDAO {

    private Proveedor proveedor;
    private Vector proveedores;
    private List proveedores_list;

    public ProveedorDAO() {
        super("ProveedorDAO.xml");
    }
    public ProveedorDAO(String dataBaseName) {
        super("ProveedorDAO.xml", dataBaseName);
    }

    /**
     * Inserta un nuevo registro en el archivo proveedor.
     * @autor Ing. Tito Andr�s Maturana D.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void insertarProveedor() throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_INSERT";//JJCastro fase2


        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, this.proveedor.getC_nit());
                st.setString(2, this.proveedor.getC_idMims());
                st.setString(3, this.proveedor.getC_payment_name());
                st.setString(4, this.proveedor.getC_branch_code());
                st.setString(5, this.proveedor.getC_bank_account());
                st.setString(6, this.proveedor.getC_agency_id());
                st.setString(7, this.proveedor.getC_tipo_doc());
                st.setString(8, this.proveedor.getC_banco_transfer());
                st.setString(9, this.proveedor.getC_sucursal_transfer());
                st.setString(10, this.proveedor.getC_tipo_cuenta());
                st.setString(11, this.proveedor.getC_numero_cuenta());
                st.setString(12, this.proveedor.getC_codciudad_cuenta());
                st.setString(13, this.proveedor.getC_clasificacion());
                st.setString(14, this.proveedor.getC_gran_contribuyente());
                st.setString(15, this.proveedor.getC_agente_retenedor());
                st.setString(16, this.proveedor.getC_autoretenedor_rfte());
                st.setString(17, this.proveedor.getC_autoretenedor_iva());
                st.setString(18, this.proveedor.getC_autoretenedor_ica());
                st.setString(19, this.proveedor.getUsuario_modificacion());
                st.setString(20, this.proveedor.getUsuario_creacion());
                st.setString(21, this.proveedor.getDistrito());
                st.setString(22, this.proveedor.getBase());
                st.setString(23, this.proveedor.getHandle_code());
                st.setInt(24, this.proveedor.getPlazo());
                st.setString(25, this.proveedor.getCedula_cuenta());
                st.setString(26, this.proveedor.getNombre_cuenta());
                st.setString(27, this.proveedor.getTipo_pago());
                st.setString(28, this.proveedor.getNit_beneficiario());
                st.setString(29, this.proveedor.getConcept_code());
                st.setString(30, this.proveedor.getCmc());
                st.setString(31, this.proveedor.getAfil());
                st.setString(32, this.proveedor.getSede());
                st.setString(33, this.proveedor.getRegimen());
                st.setString(34, this.proveedor.getNit_afiliado());
                st.setString(35, this.proveedor.getCodfen());
                st.setString(36, this.proveedor.getTipoProveedor());
                //System.out.print("Esta es la custodia----------------"+Float.toString(this.proveedor.getCustch()));
                st.executeUpdate();
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL INSERTAR EL PROVEEDOR " + e.getMessage() + "" + e.getErrorCode());
        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Anula un registro en el archivo proveedor.
     * @autor Ing. Tito Andr�s Maturana D.
     * @param nit Identificaci�n del proveedor
     * @param cia C�digo del distrito
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void anularProveedor(String nit, String cia) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ANULAR";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, nit);
                st.executeUpdate();
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL ANULAR EL PROVEEDOR " + e.getMessage() + "" + e.getErrorCode());
        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Obtiene los registros del archivo proveedor.
     * @autor Ing. Tito Andr�s Maturana D.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void obtenerProveedores() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        this.proveedores = null;
        String query = "SQL_LISTAR";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();

                this.proveedores = new Vector();

                while (rs.next()) {
                    Proveedor prov = new Proveedor();
                    prov.setC_agency_id(rs.getString("agency_id"));//rs.getString("")
                    prov.setC_autoretenedor_iva(rs.getString("autoret_iva"));
                    prov.setC_agente_retenedor(rs.getString("agente_retenedor"));
                    prov.setC_autoretenedor_ica(rs.getString("autoret_ica"));
                    prov.setC_autoretenedor_rfte(rs.getString("autoret_rfte"));
                    prov.setC_banco_transfer(rs.getString("banco_transfer"));
                    prov.setC_branch_code(rs.getString("branch_code"));
                    prov.setC_bank_account(rs.getString("bank_account_no"));
                    prov.setC_clasificacion(rs.getString("clasificacion"));
                    prov.setC_codciudad_cuenta(rs.getString("codciu_cuenta"));
                    prov.setC_gran_contribuyente(rs.getString("gran_contribuyente"));
                    prov.setC_idMims(rs.getString("id_mims"));
                    prov.setC_nit(rs.getString("nit"));
                    prov.setC_payment_name(rs.getString("payment_name"));
                    prov.setC_numero_cuenta(rs.getString("no_cuenta"));
                    prov.setC_sucursal_transfer(rs.getString("suc_transfer"));
                    prov.setC_tipo_cuenta(rs.getString("tipo_cuenta"));
                    prov.setC_tipo_doc(rs.getString("tipo_doc"));
                    prov.setDistrito(rs.getString("dstrct"));
                    prov.setNombre(rs.getString("nombre"));
                    prov.setTipo_pago(rs.getString("tipo_pago"));
                    this.proveedores.add(prov);
                }

            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL LISTAR PROVEEDORES " + e.getMessage() + "" + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * ELimina un registro del archivo proveedor.
     * @autor Ing. Tito Andr�s Maturana D.
     * @param nit Identificaci�n del proveedor
     * @param cia C�digo del distrito
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void eliminarProveedor(String nit, String cia) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_DELETE";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, nit);
                st.executeUpdate();
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL ELIMINAR EL PROVEEDOR " + e.getMessage() + "" + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Getter for property proveedor.
     * @return Value of property proveedor.
     */
    public com.tsp.operation.model.beans.Proveedor getProveedor() {
        return proveedor;
    }

    /**
     * Setter for property proveedor.
     * @param proveedor New value of property proveedor.
     */
    public void setProveedor(com.tsp.operation.model.beans.Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    /**
     * Getter for property proveedores.
     * @return Value of property proveedores.
     */
    public java.util.Vector getProveedores() {
        return proveedores;
    }

    /**
     * Setter for property proveedores.
     * @param proveedores New value of property proveedores.
     */
    public void setProveedores(java.util.Vector proveedores) {
        this.proveedores = proveedores;
    }

    //Jose 23.01.2006
    /**
     * Metodo: searchProveedores, Este m�todo obtiene una lista de proveedores dado unos parametros
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @params String nit, String nombre
     * @autor : Ing. Jose de la rosa
     * @version : 1.0
     */
    public Vector searchProveedores(String nit, String nombre) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        this.proveedores = null;
        String query = "SQL_SEARCH";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, nit + "%");
                st.setString(2, nombre + "%");
                rs = st.executeQuery();
                this.proveedores = new Vector();
                while (rs.next()) {
                    Proveedor prov = new Proveedor();
                    prov.setC_nit(rs.getString("nit"));
                    prov.setNombre(rs.getString("nombre"));
                    prov.setDistrito(rs.getString("dstrct"));
                    this.proveedores.add(prov);
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL LISTAR PROVEEDORES " + e.getMessage() + "" + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return proveedores;
    }

    /**
     * Actualiza nuevo registro en el archivo proveedor.
     * @autor Ing. Tito Andr�s Maturana D.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void actualizarProveedor() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_UPDATE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, this.proveedor.getC_payment_name());
                st.setString(2, this.proveedor.getC_branch_code());
                st.setString(3, this.proveedor.getC_bank_account());
                st.setString(4, this.proveedor.getC_agency_id());
                st.setString(5, this.proveedor.getC_banco_transfer());
                st.setString(6, this.proveedor.getC_sucursal_transfer());
                st.setString(7, this.proveedor.getC_tipo_cuenta());
                st.setString(8, this.proveedor.getC_numero_cuenta());
                st.setString(9, this.proveedor.getC_codciudad_cuenta());
                st.setString(10, this.proveedor.getC_clasificacion());
                st.setString(11, this.proveedor.getC_gran_contribuyente());
                st.setString(12, this.proveedor.getC_autoretenedor_rfte());
                st.setString(13, this.proveedor.getC_autoretenedor_iva());
                st.setString(14, this.proveedor.getC_autoretenedor_ica());
                st.setString(15, this.proveedor.getUsuario_modificacion());
                st.setString(16, this.proveedor.getHandle_code());
                st.setInt(17, this.proveedor.getPlazo());
                st.setString(18, this.proveedor.getCedula_cuenta());
                st.setString(19, this.proveedor.getNombre_cuenta());
                st.setString(20, this.proveedor.getTipo_pago());
                st.setString(21, this.proveedor.getNit_beneficiario());
                st.setString(22, this.proveedor.getC_agente_retenedor());
                st.setString(23, this.proveedor.getConcept_code());
                st.setString(24, this.proveedor.getCmc());
                st.setString(25, this.proveedor.getRegimen());
                st.setString(26, this.proveedor.getAfil());
                st.setString(27, this.proveedor.getCodfen());
                st.setString(28, this.proveedor.getTipoProveedor());
                st.setString(29, this.proveedor.getC_nit());

                System.out.print(st.toString());
                st.executeUpdate();

            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL ACTUALIZAR EL PROVEEDOR " + e.getMessage() + "" + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Actualiza nuevo registro en el archivo proveedor y guarda registro en historial.
     * @autor Ing. Enrique De Lavalle Rizo. 2007/04/18
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void actualizarProveedor(String agency, String branch_code, String bank_account) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_UPDATE";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, this.proveedor.getC_payment_name());
                st.setString(2, this.proveedor.getC_branch_code());
                st.setString(3, this.proveedor.getC_bank_account());
                st.setString(4, this.proveedor.getC_agency_id());
                st.setString(5, this.proveedor.getC_banco_transfer());
                st.setString(6, this.proveedor.getC_sucursal_transfer());
                st.setString(7, this.proveedor.getC_tipo_cuenta());
                st.setString(8, this.proveedor.getC_numero_cuenta());
                st.setString(9, this.proveedor.getC_codciudad_cuenta());
                st.setString(10, this.proveedor.getC_clasificacion());
                st.setString(11, this.proveedor.getC_gran_contribuyente());
                st.setString(12, this.proveedor.getC_autoretenedor_rfte());
                st.setString(13, this.proveedor.getC_autoretenedor_iva());
                st.setString(14, this.proveedor.getC_autoretenedor_ica());
                st.setString(15, this.proveedor.getUsuario_modificacion());
                st.setString(16, this.proveedor.getHandle_code());
                st.setInt(17, this.proveedor.getPlazo());
                st.setString(18, this.proveedor.getCedula_cuenta());
                st.setString(19, this.proveedor.getNombre_cuenta());
                st.setString(20, this.proveedor.getTipo_pago());
                st.setString(21, this.proveedor.getNit_beneficiario());
                st.setString(22, this.proveedor.getC_agente_retenedor());
                st.setString(23, this.proveedor.getConcept_code());
                st.setString(24, this.proveedor.getCmc());
                st.setString(25, this.proveedor.getRegimen());
                st.setString(26, this.proveedor.getAfil());
                st.setString(27, this.proveedor.getCodfen());
                st.setString(28, this.proveedor.getTipoProveedor());
                st.setString(29, this.proveedor.getC_nit());


                st.executeUpdate();
                //System.out.println("distrito "+proveedor.getDistrito() +" nit "+proveedor.getC_nit()+" agencia "+agengy+" bcode "+ branch_code+" bc "+ bank_account+" user "+proveedor.getUsuario_creacion()+" base "+proveedor.getBase());
                this.historialProveedor(proveedor.getDistrito(), proveedor.getC_nit(), agency, branch_code, bank_account, proveedor.getUsuario_creacion(), proveedor.getBase());

            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL ACTUALIZAR EL PROVEEDOR " + e.getMessage() + "" + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Guarda registro en historial para las modificaciones de sede de pago del proveedor.
     * @autor Ing. Enrique De Lavalle Rizo. 2007/04/18
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void historialProveedor(String dstrct, String nit, String agency, String branch_code, String bank_account, String user, String base) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_HISTORIAL_MOD";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dstrct);
                st.setString(2, nit);
                st.setString(3, branch_code);
                st.setString(4, bank_account);
                st.setString(5, agency);
                st.setString(6, user);
                st.setString(7, user);
                st.setString(8, base);
                st.executeUpdate();


            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL GUARDAR HISTORIAL MOD PROVEEDOR " + e.getMessage() + "" + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * aprueba el  proveedor.
     * @autor Ing. Diogenes Bastidas
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void aprobarProveedor() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_APROBAR";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, this.proveedor.getC_gran_contribuyente());
                st.setString(2, this.proveedor.getC_agente_retenedor());
                st.setString(3, this.proveedor.getC_autoretenedor_rfte());
                st.setString(4, this.proveedor.getC_autoretenedor_iva());
                st.setString(5, this.proveedor.getC_autoretenedor_ica());
                st.setString(6, this.proveedor.getAprobado());
                st.setString(7, this.proveedor.getUsuario_modificacion());
                st.setString(8, this.proveedor.getC_nit());
                st.setString(9, this.proveedor.getDistrito());
                //System.out.println(st.toString());
                st.executeUpdate();

            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL APROBAR EL PROVEEDOR " + e.getMessage() + "" + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Este m�todo obtiene el E-Mail de um proveedro por medio del nit
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @params String nit
     * @autor : Ing. Julio Barros
     * @version : 1.0
     * @Fecha : 15-11-2006
     */
    public String obtenerEMailPorNit(String nit) throws SQLException {
        Connection con = null;
        String temp = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_EMAIL_NIT";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, nit);
                rs = st.executeQuery();
                if (rs.next()) {
                    temp = (rs.getString("e_mail"));
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL OBTENER EL PROVEEDOR " + e.getMessage() + "" + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return temp;
    }


    /**
     * Este m�todo obtiene el nit del afiliado de um proveedor por medio del nit
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @params String nit
     * @autor : Ing. Iris Vargas
     * @version : 1.0
     * @Fecha : 04-04-2011
     */
    public String obtenerDatAfi(String nit) throws SQLException {
        Connection con = null;
        String temp = " ;_; ;_; ";
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_DAT_AFI";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, nit);
                rs = st.executeQuery();
                if (rs.next()) {
                    temp = (rs.getString("nit_afiliado")+";_;"+rs.getString("payment_name")+";_;"+rs.getString("sede"));
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL OBTENER EL PROVEEDOR " + e.getMessage() + "" + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return temp;
    }

    /**
     * Obtiene un nuevo registro del archivo proveedor.
     * @autor Ing. Tito Andr�s Maturana D.
     * @param nit Identificaci�n del proveedor
     * @param cia C�digo del distrito
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void obtenerProveedor(String nit, String cia) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        this.proveedor = null;
        String query = "SQL_OBTENER";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, nit);
                rs = st.executeQuery();

                if (rs.next()) {
                    Proveedor prov = new Proveedor();
                    prov.setC_agency_id(rs.getString("agency_id"));//rs.getString("")
                    prov.setC_autoretenedor_iva(rs.getString("autoret_iva"));
                    prov.setC_agente_retenedor(rs.getString("agente_retenedor"));
                    prov.setC_autoretenedor_ica(rs.getString("autoret_ica"));
                    prov.setC_autoretenedor_rfte(rs.getString("autoret_rfte"));
                    prov.setC_banco_transfer(rs.getString("banco_transfer"));
                    prov.setC_branch_code(rs.getString("branch_code"));
                    prov.setC_bank_account(rs.getString("bank_account_no"));
                    prov.setC_clasificacion(rs.getString("clasificacion"));
                    prov.setC_codciudad_cuenta(rs.getString("codciu_cuenta"));
                    prov.setC_gran_contribuyente(rs.getString("gran_contribuyente"));
                    prov.setC_idMims(rs.getString("id_mims"));
                    prov.setC_nit(rs.getString("nit"));
                    prov.setC_payment_name(rs.getString("payment_name"));
                    prov.setC_numero_cuenta(rs.getString("no_cuenta"));
                    prov.setC_sucursal_transfer(rs.getString("suc_transfer"));
                    prov.setC_tipo_cuenta(rs.getString("tipo_cuenta"));
                    prov.setC_tipo_doc(rs.getString("tipo_doc"));
                    prov.setDistrito(rs.getString("dstrct"));
                    prov.setEstado(rs.getString("reg_status"));//Tito 10.11.2005
                    prov.setHandle_code(rs.getString("hc"));
                    prov.setPlazo(rs.getInt("plazo"));
                    prov.setCedula_cuenta(rs.getString("cedula_cuenta"));
                    prov.setNombre_cuenta(rs.getString("nombre_cuenta"));
                    prov.setTipo_pago(rs.getString("tipo_pago"));
                    prov.setNit_beneficiario(rs.getString("nit_beneficiario"));//jose 2006-09-28
                    prov.setCurrency(com.tsp.util.Util.coalesce(rs.getString("currency"), "")); // mfontalvo 20070123
                    prov.setCmc(rs.getString("cmc"));
                    prov.setConcept_code(rs.getString("concept_code"));
                    prov.setAprobado(rs.getString("aprobado"));
                    prov.setRegimen(rs.getString("regimen"));//20100827
                    prov.setAfil(rs.getString("afiliado"));//<!-- 20101111  -->
                    prov.setSede(rs.getString("sede"));//<!-- 20101111  -->
                    prov.setCodfen(rs.getString("cod_fenalco"));
                    prov.setTipoProveedor(rs.getString("tipo_proveedor"));
                    this.proveedor = prov;
                }

            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL OBTENER EL PROVEEDOR " + e.getMessage() + "" + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Este m�todo obtiene los Provedores por Nit dado
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @params String nit
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public void obtenerProveedoresPorNit(String nit) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        this.proveedor = null;
        Vector vP;
        this.proveedores = new Vector();
        String query = "SQL_PROVEEDORESXNIT";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, "%" + nit.toUpperCase() + "%");
                st.setString(2, "%" + nit.toUpperCase() + "%");
                rs = st.executeQuery();
                while (rs.next()) {
                    Proveedor prov = new Proveedor();
                    prov.setC_agency_id(rs.getString("agencia_banco"));//se cambio la agencia por la del banco para que funcione bien facturas por pagar
                    prov.setC_autoretenedor_iva(rs.getString("autoret_iva"));
                    prov.setC_agente_retenedor(rs.getString("agente_retenedor"));
                    prov.setC_autoretenedor_ica(rs.getString("autoret_ica"));
                    prov.setC_autoretenedor_rfte(rs.getString("autoret_rfte"));
                    prov.setC_banco_transfer(rs.getString("banco_transfer"));
                    prov.setC_branch_code(rs.getString("branch_code"));
                    prov.setC_bank_account(rs.getString("bank_account_no"));
                    prov.setC_clasificacion(rs.getString("clasificacion"));
                    prov.setC_codciudad_cuenta(rs.getString("codciu_cuenta"));
                    prov.setC_gran_contribuyente(rs.getString("gran_contribuyente"));
                    prov.setC_idMims(rs.getString("id_mims"));
                    prov.setC_nit(rs.getString("nit"));
                    prov.setC_payment_name(rs.getString("payment_name"));
                    prov.setC_numero_cuenta(rs.getString("no_cuenta"));
                    prov.setC_sucursal_transfer(rs.getString("suc_transfer"));
                    prov.setC_tipo_cuenta(rs.getString("tipo_cuenta"));
                    prov.setC_tipo_doc(rs.getString("tipo_doc"));
                    prov.setDistrito(rs.getString("dstrct"));
                    prov.setPlazo(rs.getInt("plazo"));
                    prov.setCurrency((rs.getString("currency") != null) ? rs.getString("currency") : "");
                    prov.setNom_beneficiario((rs.getString("beneficiario") != null) ? rs.getString("beneficiario") : "");
                    prov.setC_hc(rs.getString("hc"));
                    prov.setCodfen(rs.getString("cod_fenalco"));
                    this.proveedor = prov;
                    this.proveedores.add(proveedor);
                }

            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL OBTENER EL PROVEEDOR " + e.getMessage() + "" + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        // return vP;
    }

    /**
     * Este m�todo obtiene un Provedor por Nit dado
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @params String nit
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public Proveedor obtenerProveedorPorNit(String nit) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        this.proveedor = null;
        String query = "SQL_OBTENER_POR_NIT";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, nit);
                rs = st.executeQuery();

                if (rs.next()) {
                    Proveedor prov = new Proveedor();
                    prov.setC_agency_id(rs.getString("agencia_banco"));//se cambio la agencia por la del banco para que funcione bien facturas por pagar
                    prov.setC_autoretenedor_iva(rs.getString("autoret_iva"));
                    prov.setC_agente_retenedor(rs.getString("agente_retenedor"));
                    prov.setC_autoretenedor_ica(rs.getString("autoret_ica"));
                    prov.setC_autoretenedor_rfte(rs.getString("autoret_rfte"));
                    prov.setC_banco_transfer(rs.getString("banco_transfer"));
                    prov.setC_branch_code(rs.getString("branch_code"));
                    prov.setC_bank_account(rs.getString("bank_account_no"));
                    prov.setC_clasificacion(rs.getString("clasificacion"));
                    prov.setC_codciudad_cuenta(rs.getString("codciu_cuenta"));
                    prov.setC_gran_contribuyente(rs.getString("gran_contribuyente"));
                    prov.setC_idMims(rs.getString("id_mims"));
                    prov.setC_nit(rs.getString("nit"));
                    prov.setC_payment_name(rs.getString("payment_name"));
                    prov.setC_numero_cuenta(rs.getString("no_cuenta"));
                    prov.setC_sucursal_transfer(rs.getString("suc_transfer"));
                    prov.setC_tipo_cuenta(rs.getString("tipo_cuenta"));
                    prov.setC_tipo_doc(rs.getString("tipo_doc"));
                    prov.setDistrito(rs.getString("dstrct"));
                    prov.setPlazo(rs.getInt("plazo"));
                    //juan 02-05-2006
                    prov.setC_hc(rs.getString("hc"));
                    //Ivan
                    prov.setCurrency((rs.getString("currency") != null) ? rs.getString("currency") : "");
                    prov.setNom_beneficiario((rs.getString("beneficiario") != null) ? rs.getString("beneficiario") : "");
                    //AMATURANA 09.03.2007
                    prov.setRetencion_pago(rs.getString("ret_pago"));
                    prov.setNombre(rs.getString("nombre_nit"));
                    prov.setNit_beneficiario(rs.getString("nit_beneficiario"));
                    prov.setHandle_code(rs.getString("hc"));
                    prov.setCodfen(rs.getString("cod_fenalco"));
                    this.proveedor = prov;
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR AL OBTENER EL PROVEEDOR " + e.getMessage() + "" + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return proveedor;
    }

    /**
     * Actualiza el campo ret_pago para un proveedor
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct Distrito
     * @param nit Nit del proveedor
     * @param rp S o N si aplica retencion al pago
     * @version : 1.0
     */
    public void aplicaRetencionPago(String dstrct, String nit, String rp) throws SQLException {
        Connection con = null;
        String temp = null;
        PreparedStatement st = null;
        String query = "SQL_SET_RET_PAGO";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, rp);
                st.setString(2, dstrct);
                st.setString(3, nit);

                st.executeUpdate();

            }
        } catch (SQLException e) {
            throw new SQLException("ERROR aplicaRetencionPago [ProveedorDAO] " + e.getMessage() + "" + e.getErrorCode());
        } finally {//JJCastro fase2
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Obtiene los nitpro q no estan registrado en el archivo nit y proveedor
     * @autor Ing. Andr�s Maturana D.
     * @param fechai Fecha inicial del per�odo
     * @param fechaf Fecha final del per�odo
     * @param cia C�digo del distrito
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void reporteVerificacionNitpro(String fechai, String fechaf, String cia) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        this.proveedores_list = new LinkedList();
        org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
        String query = "SQL_REPNITPRO";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cia);
                st.setString(2, fechai);
                st.setString(3, fechaf);
                logger.info("?sql rep verif nit pro: " + st);
                rs = st.executeQuery();

                this.proveedores = new Vector();
                while (rs.next()) {
                    RepGral obj = new RepGral();
                    obj.LoadRepVerificacionNitpro(rs);

                    this.proveedores_list.add(obj);
                }

            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL GENERAR EL REPORTE DE VERIFICACION DEL NIT DEL PROPIETARIO " + e.getMessage() + "" + e.getErrorCode());
        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Getter for property proveedores_list.
     * @return Value of property proveedores_list.
     */
    public java.util.List getProveedores_list() {
        return proveedores_list;
    }

    /**
     * Setter for property proveedores_list.
     * @param proveedores_list New value of property proveedores_list.
     */
    public void setProveedores_list(java.util.List proveedores_list) {
        this.proveedores_list = proveedores_list;
    }

    public ArrayList<String> buscarConvenios(String nit) throws Exception {
        Connection con = null;
        ArrayList<String> list = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "BUSCAR_CONVENIO_NIT";

        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nit);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(rs.getString("id_prov_convenio"));
                }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en buscarConvenios[GestionConveniosDAO] " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return list;
    }

       public String delConRan(String nit){
        String cadena = "";
        String query = "DEL_PRO_CON_RAN";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, nit);
            cadena = st.getSql();
            st = null;
        }
        catch (Exception e) {
            System.out.println("Error al generar sql cuentas: "+e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

      public String delCon(String idprovconv){
        String cadena = "";
        String query = "DEL_PRO_CON";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, idprovconv);
            cadena = st.getSql();
            st = null;
        }
        catch (Exception e) {
            System.out.println("Error al generar sql cuentas: "+e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    //----------20100817-----------//
    public void insertConvs(String nitprov, ArrayList lista1) throws Exception {
        Connection con = null;
        String query = "INS_PRC";
        String query3 = "UPD_PRC";
        String query2 = "PRC_RAN";
        Statement stmt = null;
        con = conectarJNDI(query);
        con.setAutoCommit(false);
        stmt = con.createStatement();
        String sql3 = "";
        String sql2 = "";
        String sql = "";
        AfiliadoConvenio bean = null;
        AfiliadoConvenioRangos bean2 = null;
        StringStatement ps = null;
        ArrayList vtemp = null;
        ArrayList lista2 = null;
        ArrayList lista3 = new ArrayList();

        try {
            lista2 = this.buscarConvenios(nitprov);
            stmt.addBatch(this.delConRan(nitprov));
            if (lista1.size() > 0 && lista1 != null) {
                sql = this.obtenerSQL(query);
                sql2 = this.obtenerSQL(query2);
                sql3 = this.obtenerSQL(query3);
                String cod = "";
                if (lista2.size() > 0) {
                    for (int j = 0; j < lista1.size(); j++) {
                        bean = (AfiliadoConvenio) lista1.get(j);
                        if (!bean.getIdProvConvenio().equals("0")) {
                            lista3.add(bean.getIdProvConvenio());
                        }
                    }
                    for (int j = 0; j < lista2.size(); j++) {
                        if (!lista3.contains(lista2.get(j))) {
                            stmt.addBatch(this.delCon(lista2.get(j).toString()));
                        }
                    }

                }
                for (int i = 0; i < lista1.size(); i++) {
                    bean = (AfiliadoConvenio) lista1.get(i);
                    if (bean.getIdProvConvenio().equals("0")) {
                        cod = this.codigocons();
                        ps = new StringStatement(sql, true);
                    } else {
                        cod = bean.getIdProvConvenio();
                        ps = new StringStatement(sql3, true);
                    }
                    ps.setString(1, nitprov);
                    ps.setString(2, bean.getIdConvenio());
                    ps.setString(3, bean.getCodSector());
                    ps.setString(4, bean.getCodSubsector());
                    ps.setString(5, bean.getPorcentajeAfiliado());
                    ps.setString(6, bean.getCuentaComision());
                    ps.setString(7, bean.getValorCobertura());
                    ps.setString(8, bean.getPorcCoberturaFlotante());
                    ps.setString(9, bean.getTasaInteres());
                    ps.setString(10, bean.getValorCustodia());
                    ps.setString(11, bean.getUsuario());
                    ps.setString(12, bean.getComision());
                    ps.setString(13, cod);
                    stmt.addBatch(ps.getSql());
                    vtemp = bean.getRangos();
                    ps = null;
                    for (int j = 0; j < vtemp.size(); j++) {
                        bean2 = (AfiliadoConvenioRangos) vtemp.get(j);
                        ps = new StringStatement(sql2, true);
                        ps.setString(1, cod);
                        ps.setString(2, bean2.getCuotaIni());
                        ps.setString(3, bean2.getCuotaFin());
                        ps.setString(4, bean2.getPorcentajeComision());
                        ps.setString(5, bean.getUsuario());
                        stmt.addBatch(ps.getSql());
                        ps = null;
                        bean2 = null;
                    }
                    ps = null;
                    bean = null;
                }
            } else {
                for (int j = 0; j < lista2.size(); j++) {
                    stmt.addBatch(this.delCon(lista2.get(j).toString()));
                }
            }

         stmt.executeBatch();
            stmt.clearBatch();
            con.commit();
        }
        catch (Exception e) {
            System.out.println("Error en gestionprovconvrang: "+e.toString());
            con.rollback();
            if (e instanceof SQLException) {
                    throw new SQLException("ERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS:\n" + e.getMessage() + "La siguiente exception es : ----> " + ((SQLException) e).getNextException());
                } else {
                    e.printStackTrace();
                }
        }
        finally {
            if(stmt!=null){ try{stmt.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
    }

     //<!-- 20101111  -->
    public ArrayList searchConds(String nit) throws Exception{
        ArrayList lista = new ArrayList();
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CNV_PRV";
        String sql="";
        BeanGeneral rk = null;
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, nit);
            rs = st.executeQuery();
            while(rs.next()){
                rk = new BeanGeneral();
                rk.setValor_01(rs.getString("id_convenio"));
                rk.setValor_02(rs.getString("cod_sector"));
                rk.setValor_03(rs.getString("cod_subsector"));
                rk.setValor_04(rs.getString("porcentaje_afiliado"));
                rk.setValor_05(rs.getString("cuenta_comision"));
                rk.setValor_06(rs.getString("valor_cobertura"));
                rk.setValor_07(rs.getString("porc_cobertura_flotante"));
                rk.setValor_08(rs.getString("tasa_interes"));
                rk.setValor_09(rs.getString("valor_custodia"));
                rk.setValor_10(rs.getBoolean("comision")==true?"checked='checked'":"");
                rk.setValor_11(rs.getString("id_prov_convenio"));
                lista.add(rk);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("error: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    //<!-- 20101111  -->
    public ArrayList searchRanks(String nit) throws Exception{
        ArrayList lista = new ArrayList();
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "RN_CND";
        String sql="";
        BeanGeneral rk = null;
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, nit);
            rs = st.executeQuery();
            while(rs.next()){
                rk = new BeanGeneral();
                rk.setValor_01(rs.getString("id_prov_convenio"));
                rk.setValor_02(rs.getString("cuota_ini"));
                rk.setValor_03(rs.getString("cuota_fin"));
                rk.setValor_04(rs.getString("porcentaje_comision"));
                lista.add(rk);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("error: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    //<!-- 20101111  -->
    public ArrayList<BeanGeneral> buscarDeps(String codpais) throws Exception{
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SRC_DPT";
        String sql="";
        BeanGeneral rk = null;
        try{
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, codpais);
            rs = st.executeQuery();
            while(rs.next()){
                rk = new BeanGeneral();
                rk.setValor_01(rs.getString("departament_code"));
                rk.setValor_02(rs.getString("departament_name"));
                lista.add(rk);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("error en dao: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    //<!-- 20101111  -->
    public ArrayList<BeanGeneral> buscaCius(String codpais, String codpt) throws Exception{
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SRC_CIU";
        String sql="";
        BeanGeneral rk = null;
        try{
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, codpais);
            st.setString(2, codpt);
            rs = st.executeQuery();
            while(rs.next()){
                rk = new BeanGeneral();
                rk.setValor_01(rs.getString("codciu"));
                rk.setValor_02(rs.getString("nomciu"));
                lista.add(rk);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("error en dao: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    public String codigocons() throws Exception{
        String cod = "";
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "NXT_COD";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            if(rs.next()){
                cod = rs.getString("codigo");
            }
        }
        catch (Exception e) {
            throw new Exception("error: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return cod;
    }

    public ArrayList buscarConvs() throws Exception{
        ArrayList lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SQL_CONV";
        String sql="";
        try {
            lista = new ArrayList();
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while(rs.next()){
                lista.add(rs.getString("id_convenio")+";_;"+rs.getString("nombre"));
            }
        }
        catch (Exception e) {
            throw new Exception("error: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    public ArrayList buscarConvs(String nit) throws Exception {
        ArrayList lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONV_PROV";
        String sql = "";
        try {
            lista = new ArrayList();
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, nit);
            rs = st.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("id_convenio") + ";_;" + rs.getString("nombre"));
            }
        } catch (Exception e) {
            throw new Exception("error: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public ArrayList buscarSects() throws Exception {
        ArrayList lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SQL_SECT";
        String sql = "";
        try {
            lista = new ArrayList();
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("cod_sector") + ";_;" + rs.getString("nombre"));
            }
        } catch (Exception e) {
            throw new Exception("error: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public ArrayList buscarSects(String nit) throws Exception {
        ArrayList lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SECT_PROV";
        String sql = "";
        try {
            lista = new ArrayList();
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, nit);
            rs = st.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("cod_sector") + ";_;" + rs.getString("nombre"));
            }
        } catch (Exception e) {
            throw new Exception("error: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public ArrayList buscarSubSects(String idsect) throws Exception {
        ArrayList lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SQL_SUB";
        String sql = "";
        try {
            lista = new ArrayList();
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, idsect);
            rs = st.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("cod_subsector") + ";_;" + rs.getString("nombre"));
            }
        } catch (Exception e) {
            throw new Exception("error: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public String buscarDats(String idconv) throws Exception {
        String lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SQL_DAT";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, idconv);
            rs = st.executeQuery();
            if (rs.next()) {
                lista = (rs.getString("tasa_interes") + ";_;" + rs.getString("valor_custodia"));
            }
        } catch (Exception e) {
            throw new Exception("error: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public ArrayList listaAfils() throws Exception {
        ArrayList lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "LIST_AFIL";
        String sql = "";
        try {
            lista = new ArrayList();
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("nit") + ";_;" + rs.getString("nombre"));
            }
        } catch (Exception e) {
            throw new Exception("error: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

     public String datosAfilS(String idconv) throws Exception{
        String lista = "";
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "TDOC_AFIL";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, idconv);
            st.setString(2, idconv);
            rs = st.executeQuery();
            if(rs.next()){
                lista = (rs.getString("tipo_iden")+";_;"+rs.getString("sig"));
            }
        }
        catch (Exception e) {
            throw new Exception("error: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    public Proveedor buscarNitAfiliado(String nit) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_GET_NIT_AFILIADO";
        Proveedor p = null;

        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nit);
            rs = ps.executeQuery();
            while (rs.next()) {
                p = new Proveedor();
                p.setC_nit(rs.getString("nit"));
                p.setSede(rs.getString("sede"));
                p.setNit_afiliado(rs.getString("nit_afiliado"));
                p.setCiudad(rs.getString("ciudad"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en buscarNitAfiliado[ProveedorDAO] " + e.toString());
        } finally {
            if(ps!=null){ try{ps.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return p;
    }

    public Vector searchProveedores2(String nombre) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        this.proveedores = null;
        try {
            st = crearPreparedStatement("SQL_SEARCH2");
            st.setString(1, nombre + "%");
            rs = st.executeQuery();
            this.proveedores = new Vector();
            while (rs.next()) {
                Proveedor prov = new Proveedor();
                prov.setC_nit(rs.getString("nit"));
                prov.setNombre(rs.getString("nombre"));
                prov.setDistrito(rs.getString("dstrct"));
                this.proveedores.add(prov);
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR AL LISTAR PROVEEDORES " + e.getMessage() + "" + e.getErrorCode());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
            }
            desconectar("SQL_SEARCH2");
        }
        return proveedores;
    }

    public ArrayList cargarTipos_proveedor() throws Exception {
        ArrayList lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TIPOS_PROVEEDOR";
        String sql = "";
        try {
            lista = new ArrayList();
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("descripcion"));
            }
        } catch (Exception e) {
            throw new Exception("error: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }
}
