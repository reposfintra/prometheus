/****************************************************************************
 * Nombre clase: FormularioDAO.java                                      *
 * Descripci�n: Clase que maneja las consultas de los formatos de tablas.   *
 * Autor: Ing. Dioegenes Bastidas Morales                                   *
 * Fecha: 26 de noviembre de 2006, 04:47 PM                                 *
 * Versi�n: Java 1.0                                                        *
 * Copyright: Fintravalores S.A. S.A.                                  *
 ***************************************************************************/


package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.EncriptionException;
/**
 *
 * @author  dbastidas
 */
public class FormularioDAO extends MainDAO{
    
    /** Creates a new instance of FormularioDAO */
    public FormularioDAO() {
        super( "FormularioDAO.xml" );
    }
    
    
    /**
     * Metodo: insertar, permite ingresar un registro a la tabla 
     * @autor : Ing. Diogenes Bastidas  
     * @param :
     * @version : 1.0
     */
    public void insertar(String datos) throws SQLException{
        Connection con = null;
        String query = "SQL_INSERT",sql="";
        PreparedStatement st = null;
        ResultSet rs = null;
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
            st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#DATOS#", datos));
            st.executeUpdate();
            
        }}catch(SQLException ex){
            throw new SQLException("ERROR AL INGRESAR , "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }        
    }
    
    
    /**
     * Metodo: existeRegistro, verificaa si existe un registro en la base de datos
     * con esos parametros
     * @autor : Ing. Diogenes Bastidas Morales
     * @param :
     * @version : 1.0
     */
    public boolean existeRegistro(String campos, String tabla, String condicion) throws SQLException{
        Connection con = null;
        String query = "SQL_VALIDAR",sql="";
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean est = false;
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#DATOS#", campos).replaceAll("#TABLA#", tabla).replaceAll("#CONDICIONES#", condicion));
                rs = st.executeQuery();
                if (rs.next()) {
                    est = true;
                }
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL VERIFICAR EL REGISTRO , "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return est;
    }
    
    
   /**
     * Metodo: validarCampo, verificaa si existe un registro del campo a verificar
     * con los parametros definidos
     * @autor : Ing. Diogenes Bastidas Morales
     * @param :
     * @version : 1.0
     */
    public boolean validarCampo( String SQL ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        try{
            con = this.conectarBDJNDI("fintra");//JJCastro fase2
            if (con != null ){
                st = con.prepareStatement(SQL);
                rs = st.executeQuery();
               if(rs.next()){
                   sw = true;
               }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR VALIDAR CAMPO " + e.getMessage() + " " + e.getErrorCode() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
    
    /**
     * Metodo: actualizar, permite ingresar un registro a la tabla formato_tabla.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void actualizar(String tabla, String datos, String condiciones ) throws SQLException{
        Connection con = null;
        String query = "SQL_UPDATE", sql = "";
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#TABLA#", tabla).replaceAll("#DATOS#", datos).replaceAll("#CONDICIONES#", condiciones));//JJCastro fase2
                st.executeUpdate();
            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR AL ACTUALIZAR , " + ex.getMessage() + ", " + ex.getErrorCode());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }        
    }
    
    
    /**
     * Metodo: existeRegistro, verificaa si existe un registro en la base de datos
     * con esos parametros
     * @autor : Ing. Diogenes Bastidas Morales
     * @param :
     * @version : 1.0
     */
    public boolean buscarRegistro(String tabla, String condicion, Vector info ) throws SQLException{
        Connection con = null;
        String query = "SQL_VALIDAR",sql="";
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean est = false;
         
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#DATOS#", "*" ).replaceAll("#TABLA#", tabla ).replaceAll("#CONDICIONES#", condicion ));//JJCastro fase2
            rs = st.executeQuery();
            if (rs.next()){
               for (int i=0; i<info.size();i++){
                   Formato_tabla reg = (Formato_tabla) info.get(i);
                   reg.setValor_campo(rs.getString(reg.getCampo_tabla()));                    
               }
               est =  true;
            }
        }}catch(SQLException ex){
            throw new SQLException("ERROR AL VERIFICAR EL REGISTRO , "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return est;
    }
    
     /**
     * Metodo: eliminar, permite eliminar un registro de la base de datos.
     * @autor : Ing. Diogenes Bastidas
     * @param :
     * @version : 1.0
     */
    public void eliminar(String tabla, String condiciones ) throws SQLException{
        Connection con = null;
        String query = "SQL_DELETE",sql="";
        PreparedStatement st = null;
        ResultSet rs = null;
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#TABLA#", tabla ).replaceAll("#CONDICIONES#", condiciones ));//JJCastro fase2
            st.executeUpdate();            
        }}catch(SQLException ex){
            throw new SQLException("ERROR AL ACTUALIZAR , "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }        
    }
    
}
