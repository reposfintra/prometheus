/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.google.gson.JsonObject;
/**
 *
 * @author egonzalez
 */
public interface PerfilAdmonDAO {

    public abstract JsonObject modificar(JsonObject info);
    
    public abstract JsonObject buscarOpciones(String id);
    
}
