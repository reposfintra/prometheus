/*
 * ReportePPPDAO.java
 * @autor mfontalvo
 * Created on 9 de marzo de 2007, 10:35 AM
 */

package com.tsp.operation.model.DAOS;


import com.tsp.util.Util;
import com.tsp.operation.model.beans.Remesa;
import java.sql.*;
import java.util.*;


public class ReportePPPDAO extends MainDAO {
    
    /** Creates a new instance of ReportePPPDAO */
    public ReportePPPDAO() {
        super("ReportePPPDAO.xml");
    }
    
    
    /**
     * Metodo para extraer las remesas pendientes por facturar 
     * @autor mfontalvo
     * @param fi, fecha inicial de busqueda
     * @param ff, fecha final de busqueda
     * @throws Exception.
     * @return Vector, remesas encontradas
     */
    public Vector obtenerRNF(String fi, String ff) throws Exception {
        
        Connection con = null;
        PreparedStatement ps  = null;
        ResultSet         rs  = null;
        Vector            dt  = new Vector();
        String            query = "SQL_RNF";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, fi);
            ps.setString(2, ff);
            rs = ps.executeQuery();
            
            while( rs.next() ){
                Remesa r = new Remesa();
                r.setNombre_ciu ( Util.coalesce( rs.getString("nomage"), "") );
                r.setCodcli     ( Util.coalesce( rs.getString("codcli"), "") );
                r.setCliente    ( Util.coalesce( rs.getString("nomcli"), "") );
                r.setNumrem     ( Util.coalesce( rs.getString("numrem"), "") );
                r.setFecrem     ( rs.getDate  ("fecrem" ));
                r.setValorRemesa( rs.getDouble("vlrrem2"));
                r.setFecha_corte( rs.getDate  ("corte" ));
                r.setDias_facturacion( rs.getInt   ("dias_facturacion"));
                dt.add(r);
            }
            
        }} catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return dt;
    }
    
    
    
    
    /**
     * Metodo para extraer las remesas pendientes por facturar 
     * @autor mfontalvo
     * @param fi, fecha inicial de busqueda
     * @param ff, fecha final de busqueda
     * @throws Exception.
     * @return Vector, remesas encontradas
     */
    public Vector obtenerRFNP(String fi, String ff) throws Exception {
        Connection con = null;
        PreparedStatement ps  = null;
        ResultSet         rs  = null;
        Vector            dt  = new Vector();
        String            query = "SQL_RFNP";
        try{

            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, fi);
            ps.setString(2, ff);
            rs = ps.executeQuery();
            
            while( rs.next() ){
                Remesa r = new Remesa();
                r.setNombre_ciu      ( Util.coalesce( rs.getString("nomage"), "") );
                r.setCodcli          ( Util.coalesce( rs.getString("codcli"), "") );
                r.setCliente         ( Util.coalesce( rs.getString("nomcli"), "") );
                r.setFactura         ( Util.coalesce( rs.getString("documento"), "") );
                r.setFecha_factura   ( Util.coalesce( rs.getString("fecha_factura"), "0099-01-01") );
                r.setValor_itemf     ( rs.getDouble ( "valor_item" ));
                r.setNumrem          ( Util.coalesce( rs.getString("numero_remesa"), "") );
                r.setFecrem          ( rs.getDate   ("fecrem" ));
                r.setValorRemesa     ( rs.getDouble ("vlrrem2"));
                r.setFecha_corte     ( rs.getDate   ("corte" ));
                r.setDias_facturacion( rs.getInt    ("dias_facturacion"));
                r.setDias_pago       ( rs.getInt    ("dias_pago"));
                dt.add(r);
            }
            
        }} catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return dt;
    }
    
    
    /**
     * Metodo para extraer las remesas facturadas y pagadas
     * @autor mfontalvo
     * @param fi, fecha inicial de busqueda
     * @param ff, fecha final de busqueda
     * @throws Exception.
     * @return Vector, remesas encontradas
     */
    public Vector obtenerRFP(String fi, String ff) throws Exception {
        Connection con = null;
        PreparedStatement ps  = null;
        ResultSet         rs  = null;
        Vector            dt  = new Vector();
        String            query = "SQL_RFP";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, fi);
            ps.setString(2, ff);
            rs = ps.executeQuery();
            
            while( rs.next() ){
                Remesa r = new Remesa();
                r.setNombre_ciu      ( Util.coalesce( rs.getString("nomage"), "") );
                r.setCodcli          ( Util.coalesce( rs.getString("codcli"), "") );
                r.setCliente         ( Util.coalesce( rs.getString("nomcli"), "") );
                r.setFactura         ( Util.coalesce( rs.getString("documento"), "") );
                r.setFecha_factura   ( Util.coalesce( rs.getString("fecha_factura"), "0099-01-01") );
                r.setValor_itemf     ( rs.getDouble ( "valor_item" ));
                r.setNumrem          ( Util.coalesce( rs.getString("numero_remesa"), "") );
                r.setFecrem          ( rs.getDate   ("fecrem" ));
                r.setValorRemesa     ( rs.getDouble ("vlrrem2"));
                r.setFecha_corte     ( rs.getDate   ("fecha_ultimo_pago" ));
                r.setDias_facturacion( rs.getInt    ("dias_facturacion"));
                r.setDias_pago       ( rs.getInt    ("dias_pago"));
                dt.add(r);
            }
            
        }} catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return dt;
    }    
 
    
    
    
    /**
     * Metodo para extraer las remesas facturadas y pagadas
     * @autor mfontalvo
     * @param fi, fecha inicial de busqueda
     * @param ff, fecha final de busqueda
     * @throws Exception.
     * @return Vector, remesas encontradas
     */
    public String obtenerDatoTG(String table_code) throws Exception {
        Connection con = null;
        PreparedStatement ps   = null;
        ResultSet         rs   = null;
        String            query  = "SQL_OBTENER_TABLAGEN";
        String            dato = "";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, table_code);
            rs = ps.executeQuery();
            if( rs.next() ){
                dato = Util.coalesce(rs.getString("referencia"),"");
            }
           
        }}catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
         return dato;
    }      
}
