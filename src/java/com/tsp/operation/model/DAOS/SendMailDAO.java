package com.tsp.operation.model.DAOS;
/**
 *
 * @author  Armando
 */
import java.sql.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
import javax.servlet.http.*;
import java.text.SimpleDateFormat;
import org.apache.log4j.Logger;

/**
 *
 * @author  AMENDEZ
 */
public class SendMailDAO extends MainDAO {
    
    Logger logger = Logger.getLogger(SendMailDAO.class);
    
    public SendMailDAO(){
        super("SendMailDAO.xml");
    }
 
/**
 * 
 * @param sendMail
 * @throws SQLException
 */
    public void sendMail(SendMail sendMail) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        String query = "SQL_SEND_MAIL";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            psttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            psttm.setString(1, sendMail.getRecstatus());
            psttm.setString(2, sendMail.getEmailcode());
            psttm.setString(3, sendMail.getEmailfrom());
            psttm.setString(4, sendMail.getEmailto());
            psttm.setString(5, sendMail.getEmailcopyto());
            psttm.setString(6, sendMail.getEmailsubject());
            psttm.setString(7, sendMail.getEmailbody());
            psttm.setString(8, sendMail.getTipo());
            psttm.setString(9, sendMail.getSendername ());
            psttm.executeUpdate();
            
        }}
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL ENVIO DEL CORREO: " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (psttm  != null){ try{ psttm.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


/**
 * 
 * @param from
 * @param to
 * @param subject
 * @param body
 * @throws SQLException
 */
    public void sendMail(String from, String to, String subject, String body) throws SQLException{
        SendMail sm = null;
        sm = new SendMail();
        sm.setRecstatus("A");
        sm.setEmailcode("");
        sm.setEmailbody(body);
        sm.setEmailfrom(from);
        sm.setEmailto(to);
        sm.setEmailsubject(subject);
        sm.setEmailcopyto("");
        sendMail(sm);
    }
    
    /**
     * Retorna la traza de pila para el reporte de errores.
     * @param bug Excepci�n causante del error.
     * @return Traza de pila.
     */
    protected String getStackTrace(Throwable bug) {
        StringBuffer stackTrace = new StringBuffer(0);
        StackTraceElement[] trace = bug.getStackTrace();
        for (int i=0; i < trace.length; i++)
            stackTrace.append("\n\tat " + trace[i]);
        return stackTrace.toString();
    }
    
    /**
     * Metodo para obtener el listado de dirreciones al cual se debe enviar
     * la notificaion de un error de acuerdo al codigo de errores
     * @param codError Codigo del error.
     * @throws SQLException Si ocurre un error al acceder a la base de datos.
     */
    public String getEnviarA(String codError) throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String enviara = "";
        String query = "SQL_ENVIAR_A";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            stmt = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            stmt.setString(1, codError);
            rs = stmt.executeQuery();
            if (rs.next()){
                enviara = rs.getString(1);
            }
        }}catch(SQLException SQLe){
            throw new SQLException("ERROR BUSCANDO DIRECCIONES A LAS QUE DEBE SER ENVIADA LA NOTIFICACION DE ERROR: "+SQLe.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (stmt  != null){ try{ stmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return enviara;
    }
    
    /**
     * Env�a un e-mail con un informe detallado de un error que suceda en la aplicaci�n.
     * @param httpRequest Peticion HTTP.
     * @param bug Excepci�n causante del error.
     */
    public synchronized String bugReportSend( HttpServletRequest httpRequest, Throwable bug ) {
        // Leemos todos los atributos del objeto request.
        String item = null;
        String requestAttributes = "";
        String requestParameters = "";
        String sessionAttributes = "";
        Connection con = null;//JJCastro fase2
        
        //amendez
        String codError = ( bug.getMessage() != null && bug.getMessage().startsWith("[E") ?
        bug.getMessage().substring(0, 8) : "");
        String emailto = "";
        
        java.util.Enumeration enumData = null;
        enumData = httpRequest.getAttributeNames();
        while (enumData.hasMoreElements()) {
            item = (String) enumData.nextElement();
            requestAttributes += "\n  \t" + item + " = " + httpRequest.getAttribute(item);
        }
        
        // Leemos todos los par�metros del request
        enumData = httpRequest.getParameterNames();
        while (enumData.hasMoreElements()) {
            item = (String) enumData.nextElement();
            requestParameters += "\n  \t" + item + " = " + httpRequest.getParameter(item);
        }
        
        // Creamos la sesion, si no existe, y leemos sus atributos
        HttpSession session = httpRequest.getSession(true);
        
        enumData = session.getAttributeNames();
        while (enumData.hasMoreElements()) {
            item = (String) enumData.nextElement();
            sessionAttributes += "\n  \t" + item + " = " + session.getAttribute(item);
        }
        
        // Datos para el reporte de errores
        String bugRptData = "\n\n\tATRIBUTOS DEL OBJETO REQUEST " + requestAttributes +
        "\n\n\tPARAMETROS RECIBIDOS " + requestParameters +
        "\n\n\tDATOS DE SESION\n\tSesion activa desde: " +
        new java.util.Date(session.getCreationTime()) +
        "\n\tUltimo acceso: " +
        new java.util.Date(session.getLastAccessedTime()) +
        "\n\tAtributos: " + sessionAttributes;
        
        Usuario loggedUser = (Usuario) session.getAttribute("userLoggedIn");
        String remarks = ( loggedUser != null ?
        "\nNOTA: REPORTE DE ERROR ENVIADO POR: " + loggedUser.getNombre() + "\n\n" : "LOGOUT"
        );
        String bugReport = "\nLa excepcion causante del error ha sido:" +
        bug.getMessage() + getStackTrace(bug) + bugRptData;
        PreparedStatement pstmt = null;
        Statement stmt = null; ResultSet rs = null;
        if( !remarks.equals("LOGOUT") ) {
            try {
                //amendez
                emailto = getEnviarA(codError);
                if (emailto.equals("")){
                    emailto = "sot@mail.tsp.com";
                }

                String query = "SQL_BUG_REPORT_SEND";//JJCastro fase2
                con = this.conectarJNDI(query);
                if (con != null) {
                pstmt = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                pstmt.setString(1, "procesos@mail.tsp.com");
                pstmt.setString(2, emailto);
                pstmt.setString(3, ""/*getStackTrace(bug)*/);
                pstmt.setString(4, "SOT - Informe de Errores ("+loggedUser.getLogin()+")");
                pstmt.setString(5, bugReport );
                pstmt.setString(6, "Soporte DIT");
                pstmt.setString(7, remarks);
                pstmt.executeUpdate();
                ////System.out.println("mensaje de correo enviado: "+pstmt);
            }}catch (SQLException SQLE){
                logger.error(
                "\nEL SIGUIENTE REPORTE DE ERROR NO PUDO ENVIARSE. CAUSA:\n\t" +
                SQLE.getMessage() + "\n\nREPORTE COMPLETO:\n" + remarks + bugRptData
                );
            }finally{
                try {
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (pstmt  != null){ try{ pstmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
                catch( Exception ex ){
                    logger.error("NO SE PUDIERON CERRAR LOS ESTAMENTOS EN bugReportSend: "+ex.getMessage());
                }
            }
        }
        return bugReport;
    }
    
      /**
 * Envia un email con adjuntos
 * @param sendMail El objeto con los datos del mail
 * @param file El archivo que se va a adjuntar
 * @autor: ....... David Pi�a L�pez
 * @throws ....... SQLException
 * @version ...... 1.0
 */
    public void sendMail(SendMail sendMail, File file) throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement psttm = null;
        int longitud = 0;
        String query = "SQL_SEND_MAIL_ADJUNTO";
        try{            
            FileInputStream in  = new FileInputStream( file );
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ByteArrayInputStream  bfin;
            int input                 = in.read();
            byte[] savedFile          = null;
            while(input!=-1){             
              out.write(input);
              input  = in.read();
            }
            out.close();
            savedFile  = out.toByteArray();
            longitud =   savedFile.length ;
            bfin       = new ByteArrayInputStream( savedFile );
            
            con = this.conectarJNDI(query);
            if (con != null) {
            psttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            psttm.setString( 1, sendMail.getRecstatus() );
            psttm.setString( 2, sendMail.getEmailcode() );
            psttm.setString( 3, sendMail.getEmailfrom() );
            psttm.setString( 4, sendMail.getEmailto() );
            psttm.setString( 5, sendMail.getEmailcopyto() );
            psttm.setString( 6, sendMail.getEmailsubject() );
            psttm.setString( 7, sendMail.getEmailbody() );
            psttm.setString( 8, sendMail.getTipo() );
            psttm.setString( 9, sendMail.getSendername() );            
            psttm.setBinaryStream ( 10, bfin, longitud );
            psttm.setString( 11, sendMail.getNombrearchivo() );//Osvaldo            
            ////System.out.println( "QRY MAIL: " + psttm );
            psttm.executeUpdate();
            
            in.close(); 
            bfin.close();
            }}catch( SQLException e ){
            throw new SQLException("ERROR DURANTE EL ENVIO DEL CORREO sendMail(SendMail sendMail, File file): " + e.getMessage() + " " + e.getErrorCode());
        }catch( IOException ioe ){
            throw new SQLException("ERROR DURANTE EL ENVIO DEL CORREO sendMail(SendMail sendMail, File file): " + ioe.getMessage() );
        }finally{
                    if (psttm  != null){ try{ psttm.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    //ADDED Jun-19-2009
/**
 * 
 * @param sendMail
 * @return
 * @throws SQLException
 */
    public String sendMailString(SendMail sendMail) throws SQLException{
        Connection con = null;
        StringStatement psttm = null;
        String sql = "";
        String query = "SQL_SEND_MAIL";//JJCastro fase2
        try{

            psttm = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            psttm.setString(1, sendMail.getRecstatus());
            psttm.setString(2, sendMail.getEmailcode());
            psttm.setString(3, sendMail.getEmailfrom());
            psttm.setString(4, sendMail.getEmailto());
            psttm.setString(5, sendMail.getEmailcopyto());
            psttm.setString(6, sendMail.getEmailsubject());
            psttm.setString(7, sendMail.getEmailbody());
            psttm.setString(8, sendMail.getTipo());
            psttm.setString(9, sendMail.getSendername ());
            sql = psttm.getSql()+";";//JJCastro fase2
            
        }catch(Exception e){
            throw new SQLException("ERROR DURANTE EL ENVIO DEL CORREO: /n" + e.getMessage());
        }finally{//JJCastro fase2
            if (psttm  != null){ try{ psttm = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return sql;
    }
    
}
