/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.Usuario;

/**
 *
 * @author egonzalez
 */
public interface CorridasNominaDAO {
    
    /**
     *
     * @param filGerencia
     * @param periodo
     * @return
     */
    public abstract String getFacturas(String filGerencia, String periodo);
    public abstract String crearCorridas(JsonObject obj);
    public abstract String buscarBanco(String usuario, String banco);

    /**
     *
     * @param ojb
     * @param u
     * @return
     */
    public abstract String generarArchivo(JsonObject ojb,Usuario u);
    public abstract String generarPrevio(JsonObject ojb);
    
}
