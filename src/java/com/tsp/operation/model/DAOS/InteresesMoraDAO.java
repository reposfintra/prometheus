/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.DAOS;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author maltamiranda
 */
public class InteresesMoraDAO extends MainDAO {
    public InteresesMoraDAO(){
        super("InteresesMoraDAO.xml");
    }

    public ArrayList buscar_intereses(String idclie, String ms, String fechai, String fechaf, String factura,String nota) throws Exception{
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        ArrayList arl=null;
        String sql="";int i=1;
        try{
            sql=obtenerSQL("SQL_GET_INTMOR");
            con = conectarJNDI("SQL_GET_INTMOR");
            if(idclie.equals("")){
                sql=sql.replaceAll("#CLIENTE#", "--");
            }
            else{
                sql=sql.replaceAll("#CLIENTE#", "");
            }
            if(ms.equals("")){
                sql=sql.replaceAll("#MS#", "--");
            }
            else{
                sql=sql.replaceAll("#MS#", "");
            }
            if(fechai.equals("")){
                sql=sql.replaceAll("#FECHA#", "--");
            }
            else{
                sql=sql.replaceAll("#FECHA#", "");
            }
            if(factura.equals("")){
                sql=sql.replaceAll("#FACTURA#", "--");
            }
            else{
                sql=sql.replaceAll("#FACTURA#", "");
            }
            if(nota.equals("")){
                sql=sql.replaceAll("#NOTA#", "--");
            }
            else{
                sql=sql.replaceAll("#NOTA#", "");
            }
            ps  = con.prepareStatement(sql);
            ps.setString(i, idclie);i++;
            ps.setString(i, ms);i++;
            ps.setString(i, fechai);i++;
            ps.setString(i, fechaf);i++;
            ps.setString(i, factura);i++;
            ps.setString(i, nota);i++;
            rs=ps.executeQuery();
            arl=rstotbl(rs);
        }
        catch(Exception e){
            e.printStackTrace();
            e.toString();
            throw new Exception("Error al obtener los intereses"+e.toString());

        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(ps!=null){ps.close();}
            if(rs!=null){rs.close();}
        }

        return arl;
    }

    public ArrayList rstotbl (ResultSet rs) throws Exception {
        ArrayList tabla=new ArrayList();
        while(rs.next()){
           ArrayList tabla2=new ArrayList();
           for(int i=1;i<=rs.getMetaData().getColumnCount();i++)
           {    tabla2.add(rs.getString(i));
           }
           tabla.add(tabla2);
        }
        return tabla;
    }

    public void anular_intereses(String[] intereses, String login)throws Exception {
        Connection con=null;
        PreparedStatement st=null;
        String sql="";
        String interes="";
        for (int i=0;i<intereses.length;i++){
            interes=interes + "'" +intereses[i] +"',";
        }
        interes=interes.substring(0, interes.length()-1);
        try{
            sql=this.obtenerSQL("SQL_ANULAR_INTERESES");
            con=this.conectarJNDI("SQL_ANULAR_INTERESES");
            sql=sql.replaceAll("#INTERESES#", interes);
            sql=sql.replaceAll("#USUARIO#", "'"+login+"'");

            st=con.prepareStatement(sql);
            st.execute();
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println(e.getMessage());
            throw new Exception( "Error al Anular Intereses: " + e.toString());
        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(st!=null){st.close();}
        }
    }

    public void ignorar_intereses(String[] intereses, String login)throws Exception {
        Connection con=null;
        PreparedStatement st=null;
        String sql="";
        String interes="";
        for (int i=0;i<intereses.length;i++){
            interes=interes + "'" +intereses[i] +"',";
        }
        interes=interes.substring(0, interes.length()-1);
        try{
            sql=this.obtenerSQL("SQL_IGNORAR_INTERESES");
            con=this.conectarJNDI("SQL_IGNORAR_INTERESES");
            sql=sql.replaceAll("#INTERESES#", interes);
            sql=sql.replaceAll("#USUARIO#", "'"+login+"'");

            st=con.prepareStatement(sql);
            st.execute();
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println(e.getMessage());
            throw new Exception( "Error al Ignorar Intereses: " + e.toString());
        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(st!=null){st.close();}
        }
    }

    public String facturar_intereses(String[] intereses, String login)throws Exception{
        Connection con=null;
        PreparedStatement st=null;
        String sql="";
        String interes="' ";
        ResultSet rs=null;
        String respuesta="";
        for (int i=0;i<intereses.length;i++){
            interes=interes + "''" +intereses[i] +"'',";
        }
        interes=interes.substring(0, interes.length()-1);
        interes=interes + "'";
        try{
            sql=this.obtenerSQL("SQL_FACTURAR_INTERESES");
            con=this.conectarJNDI("SQL_FACTURAR_INTERESES");
            sql=sql.replaceAll("#INTERESES#", interes);
            sql=sql.replaceAll("#USUARIO#", "'"+login+"'");

            st=con.prepareStatement(sql);
            rs=st.executeQuery();
            if(rs.next()){
                respuesta=rs.getString("respuesta");
            }
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println(e.getMessage());
            throw new Exception( "Error al Facturar Intereses: " + e.toString());
        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(st!=null){st.close();}
            if(rs!=null){rs.close();}
        }
        return respuesta;
    }


}
