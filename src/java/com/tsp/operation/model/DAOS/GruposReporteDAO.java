/*
 * Nombre        GruposReporteDAO.java
 * Descripción   Clase para el acceso a los datos de los grupos de un reporte.
 * Autor         Alejandro Payares
 * Fecha         10 de mayo de 2006, 10:36 AM
 * Version       1.0
 * Coyright      Transportes Sanchez Polo SA.
 */

package com.tsp.operation.model.DAOS;

import java.sql.*;
import com.tsp.operation.model.beans.GruposReporte;
import java.util.Vector;
import java.util.Hashtable;

/**
 * Clase para el acceso a los datos de los grupos de un reporte.
 * @author Alejandro Payares
 */
public class GruposReporteDAO extends MainDAO {
    
    
    /**
     * Un vector para almacenar datos
     */    
    private Vector datos;
    
    
    /**
     * Crea una nueva instancia de GruposReporteDAO
     * @autor  Alejandro Payares
     */
    public GruposReporteDAO() {
        super("GruposReporteDAO.xml");
    }
    
    
    /**
     * Permite guardar los diferentes grupos de un reporte en la base de datos.
     * @param gr El objeto que contiene la información de los grupos del reporte
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     * @autor Alejandro Payares
     */
    public void guardarGrupos(GruposReporte gr)throws SQLException {
        PreparedStatement ps = null;
        try {
            Vector grupos = gr.getGrupos();
            Connection con = this.conectar("SQL_CREAR_GRUPO");
            for( int i=0; i< grupos.size(); i++ ){
                Hashtable grupo = (Hashtable) grupos.elementAt(i);
                ps = con.prepareStatement(obtenerSQL("SQL_CREAR_GRUPO"));
                ps.setString(1, grupo.get(gr.LLAVE_NOMBRE_GRUPO).toString());
                ps.setString(2, grupo.get(gr.LLAVE_COLOR_GRUPO).toString());
                ps.setString(3, gr.getCodigoReporte());
                String sql = ps.toString();
                StringBuffer strCampos = new StringBuffer();
                Vector campos = gr.obtenerCamposDeGrupo(grupo.get(gr.LLAVE_NOMBRE_GRUPO).toString());
                for( int j=0; j< campos.size(); j++ ){
                    Hashtable campo = (Hashtable) campos.elementAt(j);
                    strCampos.append("'"+campo.get(gr.LLAVE_NOMBRE_CAMPO)+"',");
                }
                if ( strCampos.length() > 0 ) {
                    strCampos.deleteCharAt(strCampos.length()-1);
                }
                sql = sql.replaceAll("'CAMPOS'", strCampos.toString());
                //////System.out.println("guardando en base de datos grupo "+(i+1)+" = "+sql);
                ps.executeUpdate(sql);
                ps.clearParameters();
                //////System.out.println("grupo guardado");
            }
        }
        finally {
            if(ps != null){ ps.close(); }
            this.desconectar("SQL_CREAR_GRUPO");
        }
    }
    
    /**
     * Carga en un objeto <CODE>com.tsp.operation.model.beans.GruposReporte</CODE> los grupos de un reporte.
     * @param codigoReporte El codigo del reporte del que serán cargados los datos
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     * @return El objeto con los datos de los grupos del reporte
     * @autor Alejandro Payares
     */
    public GruposReporte cargarGrupos(String codigoReporte)throws SQLException{
        GruposReporte gr = new GruposReporte();
        PreparedStatement ps = null;
        try {
            ps = this.crearPreparedStatement("SQL_OBTENER_DETALLE_GRUPO");
            ps.setString(1, codigoReporte);
            ResultSet rs = ps.executeQuery();
            gr.setCodigoReporte(codigoReporte);
            while( rs.next() ){
                String nombre = rs.getString("grupo");
                String color = rs.getString("colorgrupo");
                ////System.out.println("campos grupo "+nombre+" = "+rs.getString("campos"));
                String [] campos = rs.getString("campos").split(",");
                String [] titulos = rs.getString("titulos").split(",");
                gr.agregarGrupo(nombre, color);
                for( int i=0; i< campos.length; i++ ){
                    gr.agregarCampoAGrupo(nombre, campos[i], titulos[i]);
                }
            }
            rs.close();
        }
        finally {
            if(ps != null){ ps.close(); }
            this.desconectar("SQL_OBTENER_DETALLE_GRUPO");
            return gr;
        }
    }
    
    /**
     * Busca los reportes configurables y los almacena en un vector
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     * @autor Alejandro Payares
     */
    public void buscarReportes()throws SQLException{
        PreparedStatement ps = null;
        try {
            ps = this.crearPreparedStatement("SQL_OBTENER_REPORTES");
            ResultSet rs = ps.executeQuery();
            datos = new Vector();
            while( rs.next() ){
                Hashtable fila = new Hashtable();
                fila.put("codigo", rs.getString("codigorpt"));
                fila.put("nombre", rs.getString("nombrerpt"));
                datos.addElement(fila);
            }
            rs.close();
        }
        finally {
            if(ps != null){ ps.close(); }
            this.desconectar("SQL_OBTENER_REPORTES");
        }
    }
    
    /**
     * Devuelve los reportes encontrados por el metodo buscarReportes
     * @return Un vector con los reportes
     * @autor Alejandro Payares
     */
    public Vector obtenerReportes(){
        return datos;
    }
    
    /**
     * Busca los diferentes grupos del reporte dado.
     * @param codReporte El codigo del reporte al cual se le buscaran los grupos
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     * @autor Alejandro Payares
     */
    public void buscarGrupos(String codReporte)throws SQLException{
        PreparedStatement ps = null;
        try {
            ps = this.crearPreparedStatement("SQL_OBTENER_GRUPOS_REPORTE");
            ps.setString(1, codReporte);
            ResultSet rs = ps.executeQuery();
            datos = new Vector();
            while( rs.next() ){
                Hashtable fila = new Hashtable();
                fila.put("grupo",rs.getString("grupo"));
                fila.put("colorgrupo",rs.getString("colorgrupo"));
                datos.addElement(fila);
            }
            rs.close();
        }
        finally {
            if(ps != null){ ps.close(); }
            this.desconectar("SQL_OBTENER_GRUPOS_REPORTE");
        }
    }
    
    
    
    /**
     * Devuelve los grupos encontrados por el metodo buscarGrupos
     * @return Un vector con los grupos
     * @autor Alejandro Payares
     */
    public Vector obtenerGrupos(){
        return datos;
    }
    
    /**
     * Agrega un nuevo campo para el reporte específicado
     * @autor Alejandro Payares
     * @param colorGrupo El color del grupo al que pertenece el campo
     * @param dstrct El distrito
     * @param codigorpt El codigo del reporte
     * @param nomcampo El nombre del campo
     * @param titulocampo El titulo del campo
     * @param grupo EL nombre del grupo al que pertenece el campo
     * @param creation_user El usuario de creación del registro
     * @param base La base
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     */
    public void agregarCampo(   String dstrct,
    String codigorpt,
    String nomcampo,
    String titulocampo,
    String grupo,
    String colorGrupo,
    String creation_user,
    String base
    )throws SQLException{
        PreparedStatement ps = null;
        try {
            ps = this.crearPreparedStatement("SQL_OBTENER_ULTIMO_ORDEN");
            ps.setString(1, codigorpt);
            ResultSet rs = ps.executeQuery();
            if( rs.next() ){
                int orden = rs.getInt(1) + 1;
                rs.close();
                ps.close();
                ps = this.crearPreparedStatement("SQL_AGREGAR_CAMPO");
                ps.setString(1, dstrct);
                ps.setString(2, codigorpt);
                ps.setInt(3, orden);
                ps.setString(4, nomcampo);
                ps.setString(5, titulocampo);
                ps.setString(6, grupo);
                ps.setString(7, colorGrupo);
                ps.setString(8, creation_user);
                ps.setString(9, base);
                ////System.out.println("query agregar campo: "+ps);
                ps.executeUpdate();
            }
            rs.close();
        }
        finally {
            if(ps != null){ ps.close(); }
            this.desconectar("SQL_AGREGAR_CAMPO");
            this.desconectar("SQL_OBTENER_ULTIMO_ORDEN");
        }
    }
    
    /**
     * Modifica un campo del reporte especificado
     * @autor Alejandro Payares
     * @param nomcampoAntiguo El antiguo nombre del campo
     * @param colorgrupo El nuevo color del grupo
     * @param dstrct El distrito
     * @param antiguoOrden El antiguo orden del campo
     * @param orden El nuevo orden del campo
     * @param codigorpt El codigo del reporte al que pertenece el campo
     * @param nomcampo El nuevo nombre del campo
     * @param titulocampo EL titulo del campo
     * @param grupo El grupo del campo
     * @param user_update El usuario de actualización
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     */
    public void editarCampo(    String dstrct,
    int antiguoOrden,
    int orden,
    String codigorpt,
    String nomcampo,
    String nomcampoAntiguo,
    String titulocampo,
    String grupo,
    String colorgrupo,
    String user_update
    )throws SQLException{
        PreparedStatement ps = null;
        try {
            ps = this.crearPreparedStatement("SQL_EDITAR_CAMPO");
            ps.setInt(    1, orden);
            ps.setString( 2, nomcampo);
            ps.setString( 3, titulocampo);
            ps.setString( 4, grupo);
            ps.setString( 5, colorgrupo);
            ps.setString( 6, user_update);
            ps.setString( 7, codigorpt);
            ps.setString( 8, nomcampoAntiguo);
            ps.setInt(    9, antiguoOrden);
            ps.setString( 10, dstrct);
            ps.executeUpdate();
        }
        finally {
            if(ps != null){ ps.close(); }
            this.desconectar("SQL_EDITAR_CAMPO");
        }
    }
    
    
    /**
     * Elimina uno o más campos del reporte especificado
     * @autor Alejandro Payares
     * @param distrito EL distrito
     * @param user_update El usuario de actualización
     * @param codReporte EL codigo del reporte
     * @param nombres Los nombres de los campos a eliminar
     * @param ordenes Los ordenes de los campos
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     */
    public void eliminarCampos(String codReporte, String[] nombres, String [] ordenes, String distrito, String user_update)throws SQLException {
        PreparedStatement ps  = null;
        PreparedStatement ps2 = null;
        PreparedStatement ps3 = null;
        try {
            ps = this.crearPreparedStatement("SQL_ELIMINAR_CAMPO");
            for( int i=0; i< nombres.length; i++ ){
                String nombreCampo = nombres[i];
                String orden = ordenes[i];
                ps.setString(1, codReporte);
                ps.setString(2, nombreCampo);
                ps.setString(3, orden);
                ps.setString(4, distrito);
                ps.executeUpdate();
                ps.clearParameters();
            }
            
            ps2 = this.crearPreparedStatement("SQL_OBTENER_CAMPOS_MAYORES");
            ps2.setString(1, codReporte);
            ps2.setString(2, ordenes[0]);
            ps2.setString(3, distrito);
            ResultSet rs = ps2.executeQuery();
            ps3 = this.crearPreparedStatement("SQL_ORDENAR_CAMPO");
            // el orden del menor que fue eliminado
            int ordenNuevo = Integer.parseInt(ordenes[0]);
            // ordenamos solo los campos que fueron desordenados
            while( rs.next() ){
                String nombreCampo = rs.getString("nomcampo");
                String orden = rs.getString("orden");
                ps3.setInt(1,  ordenNuevo);
                ps3.setString(2, user_update);
                ps3.setString(3, nombreCampo);
                ps3.setString(4, codReporte);
                ps3.setString(5, orden);
                ps3.setString(6, distrito);
                ps3.executeUpdate();
                ps3.clearParameters();
                ordenNuevo++;
            }
            rs.close();
        }
        finally {
            if( ps != null ){ ps.close(); }
            if( ps2 != null ){ ps2.close(); }
            if( ps3 != null ){ ps3.close(); }
            this.desconectar("SQL_ELIMINAR_CAMPO");
            this.desconectar("SQL_OBTENER_CAMPOS_MAYORES");
            this.desconectar("SQL_ORDENAR_CAMPO");
        }
    }
    
    /**
     * Guarda los cambios realizados al orden de los campos
     * @autor Alejandro Payares
     * @param codReporte EL codigo del reporte
     * @param nombres Los nombres de los campos a eliminar
     * @param ordenes Los ordenes de los campos
     * @param ordenesAntiguos Los ordenes antiguos de los campos
     * @param distrito EL distrito
     * @param user_update El usuario de actualización
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     */    
    public void guardarCambiosOrdenCampos(String codReporte, String[] nombres, String [] ordenes, String [] ordenesAntiguos, String distrito, String user_update)throws SQLException {
        PreparedStatement ps  = null;
        PreparedStatement ps2 = null;
        try {
            
            // falta tener en cuenta los String [] ordenesAntiguos
            ps = this.crearPreparedStatement("SQL_ORDENAR_CAMPO");
            for( int i=0; i< nombres.length; i++ ){
                String nombre = nombres[i];
                int orden = Integer.parseInt(ordenes[i]);
                ps.setInt(1,  orden + 1000 );
                ps.setString(2,  user_update);
                ps.setString(3,  nombre);
                ps.setString(4,  codReporte);
                ps.setString(5,  ordenesAntiguos[i]);
                ps.setString(6,  distrito);
                ////System.out.println("query ordenar = "+ps);
                ps.executeUpdate();
                ps.clearParameters();
            }
            //ordenamos los campos
            ps2 = this.crearPreparedStatement("SQL_ORDENAR_CAMPO2");
            ps2.setString(1, user_update);
            ps2.setString(2, codReporte);
            ps2.setString(3, distrito);
            ps2.executeUpdate();
            
        }
        finally {
            if ( ps != null ) { ps.close(); }
            if ( ps2 != null ) { ps2.close(); }
            this.desconectar("SQL_ORDENAR_CAMPO");
            this.desconectar("SQL_ORDENAR_CAMPO2");
        }
    }
    
    
    /**
     * Metodo ejecutable de java
     */
    public static void main( String [] args ){
        try {
            GruposReporteDAO grd = new GruposReporteDAO();
            GruposReporte gr = grd.cargarGrupos("devoluciones");
            Vector v = gr.getGrupos();
            for( int i=0; i<v.size(); i++ ){
                Hashtable grupo = (Hashtable) v.elementAt(i);
                ////System.out.println("    grupo = gr.agregarGrupo('"+grupo.get("nombre")+"','"+grupo.get("color")+"');");
                Vector campos = gr.obtenerCamposDeGrupo(grupo.get("nombre").toString());
                ////System.out.println("campos "+grupo.get("nombre")+" = "+campos.size());
                for( int j=0; j<campos.size(); j++ ){
                    ////System.out.println("    grupo.agregarCampo('"+campos.elementAt(j)+"',grupo.campos.length);");
                }
            }
        }
        catch( Exception ex ){
            ex.printStackTrace();
        }
        
    }
}
