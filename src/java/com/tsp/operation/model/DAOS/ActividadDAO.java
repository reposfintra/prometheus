/***********************************************************************************
 * Nombre clase : ............... ActividadDAO.java                                *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.                                       *
 * Autor :....................... Ing. Diogenes Antonio Bastidas Morales           *
 * Fecha :........................ 25 de agosto de 2005, 07:20 AM                  *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;
//import com.tsp.util.connectionpool.PoolManager;

public class ActividadDAO extends MainDAO{
    
    /** Creates a new instance of ActividadDAO */
    public ActividadDAO() {
        super("ActividadDAO.xml");
    }
    private Actividad actividad;
    private Vector VecAct;
    
    private static final String insertar ="INSERT INTO actividad ( dstrct, cod_actividad, descorta, deslarga,fecinicio,m_fecini," +
                                                                  " corta_fecini, larga_fecini, fecfin, m_fecfin, corta_fecfin, larga_fecfin, " +
                                                                  " duracion, m_duracion, corta_duracion, larga_duracion, documento,  m_documento, " +
                                                                  " corta_documento, larga_documento, tiempodemora, m_tiempodemora, corta_tiempodemora, " +
                                                                  " larga_tiempodemora, causademora, m_causademora, corta_causademora, larga_causademora, " +
                                                                  " resdemora, m_resdemora, corta_resdemora, larga_resdemora, feccierre, m_feccierre, " +
                                                                  " corta_feccierre, larga_feccierre, observacion, m_observacion, corta_observacion, larga_observacion, " +
                                                                  " mane_evento, base, cantrealizada, m_cantrealizada, corta_cantrealizada, larga_cantrealizada, " +
                                                                  " cantplaneada, m_cantplaneada, corta_cantplaneada, larga_cantplaneada," +
                                                                  " referencia1, m_referencia1, corta_referencia1, larga_referencia1," +
                                                                  " referencia2, m_referencia2, corta_referencia2, larga_referencia2, " +
                                                                  " refnumerica1, m_refnumerica1, corta_refnumerica1, larga_refnumerica1," +
                                                                  " refnumerica2, m_refnumerica2, corta_refnumerica2, larga_refnumerica2," +
                                                                  " link, perfil, creation_user, creation_date, user_update, last_update ) "+                                         
                                          " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
                                          "         ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
                                          "         ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?  )";
    
    
    private static String modificar= "update               "+
                                     "       actividad     "+
                                     "    set              " +
                                     "        descorta = ?, " +
                                     "        deslarga = ?," +
                                     "        fecinicio = ?," +
                                     "        m_fecini = ?," +
                                     "        corta_fecini = ?," +
                                     "        larga_fecini = ?," +
                                     "        fecfin = ?," +
                                     "        m_fecfin = ?," +
                                     "        corta_fecfin = ?," +
                                     "        larga_fecfin = ?, " +
                                     "        duracion = ?, " +
                                     "        m_duracion = ?," +
                                     "        corta_duracion = ?, " +
                                     "        larga_duracion = ?," +
                                     "        documento = ?,  " +
                                     "        m_documento = ?, " +
                                     "        corta_documento = ?," +
                                     "        larga_documento = ?," +
                                     "        tiempodemora = ?, " +
                                     "        m_tiempodemora = ?, " +
                                     "        corta_tiempodemora = ?," +
                                     "        larga_tiempodemora = ?," +
                                     "        causademora = ?, " +
                                     "        m_causademora = ?, " +
                                     "        corta_causademora = ?, " +
                                     "        larga_causademora = ?," +
                                     "        resdemora = ?, " +
                                     "        m_resdemora = ?, " +
                                     "        corta_resdemora = ?, " +
                                     "        larga_resdemora = ?, " +
                                     "        feccierre = ?," +
                                     "        m_feccierre = ?, " +
                                     "        corta_feccierre = ?, " +
                                     "        larga_feccierre = ?, " +
                                     "        observacion = ?, " +
                                     "        m_observacion = ?, " +
                                     "        corta_observacion = ?," +
                                     "        larga_observacion = ?, " +
                                     "        mane_evento = ?," +
                                     "        estado = ?,     " +
                                     "        base = ?,        " +
                                     "        user_update = ?," +
                                     "        last_update = ?," +
                                     "        cantrealizada = ?," +
                                     "        m_cantrealizada = ?," +
                                     "        corta_cantrealizada = ?," +
                                     "        larga_cantrealizada= ?," +
                                     "        cantplaneada = ?," +
                                     "        m_cantplaneada = ?," +
                                     "        corta_cantplaneada = ?, " +
                                     "        larga_cantplaneada = ?," +
                                     "        perfil = ?, " +
                                     "        referencia1 = ?," +
                                     "        m_referencia1 = ?," +
                                     "        corta_referencia1 = ?," +
                                     "        larga_referencia1 = ?," +
                                     "        referencia2 = ?," +
                                     "        m_referencia2 = ?," +
                                     "        corta_referencia2 = ?," +
                                     "        larga_referencia2 = ?," +
                                     "        refnumerica1 = ?," +
                                     "        m_refnumerica1 = ?," +
                                     "        corta_refnumerica1 = ?," +
                                     "        larga_refnumerica1 = ?," +
                                     "        refnumerica2 = ?,   " +
                                     "        m_refnumerica2 = ?,   " +
                                     "        corta_refnumerica2 = ?, " +
                                     "        larga_refnumerica2 = ?," +
                                     "        link = ?             " +
                                     
                                     "  where                   " +
                                     "           dstrct = ?        " +
                                     "       and cod_actividad =? ";
    
    
    private static String listar = "SELECT * FROM actividad  WHERE estado = '' ";
    
    private static String buscar = " SELECT *              " +
                                   "    FROM actividad     " +
                                   " WHERE                 " +
                                   "         estado = ''   " +
                                   "     AND dstrct = ?       " +
                                   "     AND cod_actividad= ? ";

    private static String busqueda = " SELECT *              " +
                                     "    FROM actividad     " +
                                     " WHERE                 " +
                                     "         estado = ''   " +
                                     "     AND dstrct = ?       " +
                                     "     AND cod_actividad like ? " +
                                     "     AND deslarga like ?";
    
    private static String anular ="UPDATE               "+
                                  "       actividad     "+
                                  "    SET              " +
                                  "        estado = 'A', " +
                                  "        user_update = ?," +
                                  "        last_update = ?" +
                                  " WHERE                " +
                                  "       dstrct = ?         " +
                                  "   AND cod_actividad = ? ";
    
    private static String existeanu=" SELECT *              " +
                                   "    FROM actividad     " +
                                   " WHERE                 " +
                                   "         estado = 'A'   " +
                                   "     AND dstrct = ?       " +
                                   "     AND cod_actividad= ? ";
    
     /**
     * Metodo setActividad, setea el objeto de tipo actividad
     * @param: objeo actividad
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    
    public void  setActividad (Actividad act){
        
        this.actividad=act;
        
    }
    
    /**
     * Metodo obtActividad, obtiene  objeto de tipo actividad
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public Actividad obtActividad(){
        return actividad;
    }
    /**
     * Metodo obtVecActividad, retorna el vector de actividades
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public Vector obtVecActividad (){
        return VecAct;
    }
    /**
     * Metodo InsertarActividad, ingresa un registro en la tabla actividad
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void insertarActividad() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_INSERTAR";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, actividad.getDstrct());
                st.setString(2, actividad.getCodActividad());
                st.setString(3, actividad.getDescorta() );
                st.setString(4, actividad.getDesLarga());
                
                st.setString(5, actividad.getFecinicio());
                st.setString(6, actividad.getM_Fecinicio());
                st.setString(7, actividad.getCortaFecini() );
                st.setString(8, actividad.getLargaFecini() );
                
                st.setString(9, actividad.getFecfinal() );
                st.setString(10, actividad.getM_Fecfin() );
                st.setString(11, actividad.getCortaFecfin() );
                st.setString(12, actividad.getLargaFecfin() );
                
                st.setString(13, actividad.getDuracion() );
                st.setString(14, actividad.getM_Duracion() );
                st.setString(15, actividad.getCortaDuracion() );
                st.setString(16, actividad.getLargaDuracion() );
                
                st.setString(17, actividad.getDocumento() );
                st.setString(18, actividad.getM_Documento() );
                st.setString(19, actividad.getCortaDocumento() );                
                st.setString(20, actividad.getLargaDocumento() );
                
                st.setString(21, actividad.getTiempodemora() );
                st.setString(22, actividad.getM_Tiempodemora() );
                st.setString(23, actividad.getCortaTiempodemora() );
                st.setString(24, actividad.getLargaTiempodemora() );
                
                st.setString(25, actividad.getCausademora());
                st.setString(26, actividad.getM_Causademora());
                st.setString(27, actividad.getCortaCausademora());
                st.setString(28, actividad.getLargaCausademora());
                
                st.setString(29, actividad.getResdemora());
                st.setString(30, actividad.getM_Resdemora());
                st.setString(31, actividad.getCortaResdemora());
                st.setString(32, actividad.getLargaResdemora());
                
                st.setString(33, actividad.getFeccierre());
                st.setString(34, actividad.getM_Feccierre());
                st.setString(35, actividad.getCortaFeccierre());
                st.setString(36, actividad.getLargaFeccierre());
                
                st.setString(37, actividad.getObservacion());
                st.setString(38, actividad.getM_Observacion());
                st.setString(39, actividad.getCortaObservacion());
                st.setString(40, actividad.getLargaObservacion());
                st.setString(41, actividad.getMane_evento());
                st.setString(42, actividad.getBase());
                
                //NUEVO CAMPOS 05-12-2005
                st.setString(43, actividad.getCantrealizada());
                st.setString(44, actividad.getM_cantrealizada());
                st.setString(45, actividad.getCorta_cantrealizada());
                st.setString(46, actividad.getLarga_cantrealizada());
                
                st.setString(47, actividad.getCantplaneada());
                st.setString(48, actividad.getM_cantplaneada());
                st.setString(49, actividad.getCorta_cantplaneada());
                st.setString(50, actividad.getLarga_cantplaneada());
                
                //NUEVO CAMPOS 19-12-2005
                                
                st.setString(51, actividad.getReferencia1());
                st.setString(52, actividad.getM_referencia1());
                st.setString(53, actividad.getCorta_referencia1());
                st.setString(54, actividad.getLarga_referencia1());
                
                st.setString(55, actividad.getReferencia2());
                st.setString(56, actividad.getM_referencia2());
                st.setString(57, actividad.getCorta_referencia2());
                st.setString(58, actividad.getLarga_referencia2());
                
                st.setString(59, actividad.getRefnumerica1());
                st.setString(60, actividad.getM_refnumerica1());
                st.setString(61, actividad.getCorta_refnumerica1());
                st.setString(62, actividad.getLarga_refnumerica1());
                
                st.setString(63, actividad.getRefnumerica2());
                st.setString(64, actividad.getM_refnumerica2());
                st.setString(65, actividad.getCorta_refnumerica2());
                st.setString(66, actividad.getLarga_refnumerica2());
                
                st.setString(67, actividad.getLink());              
                //**************
                st.setString(68, actividad.getPerfil());
                
                st.setString(69, actividad.getCreation_user());
                st.setString(70, actividad.getCreation_date());
                st.setString(71, actividad.getUser_update());
                st.setString(72, actividad.getLast_update());
                
                 ////System.out.println(st);

                
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL INSERTAR LA ACTIVIDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
     /**
     * Metodo modificarActividad, modifica un registro en la tabla actividad
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void modificarActividad() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_UPDATE_ACTVIDAD";

        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2;
                st.setString(1, actividad.getDescorta() );
                st.setString(2, actividad.getDesLarga());
                st.setString(3, actividad.getFecinicio());
                st.setString(4, actividad.getM_Fecinicio());
                st.setString(5, actividad.getCortaFecini() );
                st.setString(6, actividad.getLargaFecini() );
                st.setString(7, actividad.getFecfinal() );
                st.setString(8, actividad.getM_Fecfin() );
                st.setString(9, actividad.getCortaFecfin() );
                st.setString(10, actividad.getLargaFecfin() );
                st.setString(11, actividad.getDuracion() );
                st.setString(12, actividad.getM_Duracion() );
                st.setString(13, actividad.getCortaDuracion() );
                st.setString(14, actividad.getLargaDuracion() );
                st.setString(15, actividad.getDocumento() );
                st.setString(16, actividad.getM_Documento() );
                st.setString(17, actividad.getCortaDocumento() );                
                st.setString(18, actividad.getLargaDocumento() );
                st.setString(19, actividad.getTiempodemora() );
                st.setString(20, actividad.getM_Tiempodemora() );
                st.setString(21, actividad.getCortaTiempodemora() );
                st.setString(22, actividad.getLargaTiempodemora() );
                st.setString(23, actividad.getCausademora());
                st.setString(24, actividad.getM_Causademora());
                st.setString(25, actividad.getCortaCausademora());
                st.setString(26, actividad.getLargaCausademora());
                st.setString(27, actividad.getResdemora());
                st.setString(28, actividad.getM_Resdemora());
                st.setString(29, actividad.getCortaResdemora());
                st.setString(30, actividad.getLargaResdemora());
                st.setString(31, actividad.getFeccierre());
                st.setString(32, actividad.getM_Feccierre());
                st.setString(33, actividad.getCortaFeccierre());
                st.setString(34, actividad.getLargaFeccierre());
                st.setString(35, actividad.getObservacion());
                st.setString(36, actividad.getM_Observacion());
                st.setString(37, actividad.getCortaObservacion());
                st.setString(38, actividad.getLargaObservacion());
                st.setString(39, actividad.getMane_evento());
                st.setString(40, actividad.getEstado());
                st.setString(41, actividad.getBase());
                st.setString(42, actividad.getUser_update());
                st.setString(43, actividad.getLast_update());
                //nuevo
                st.setString(44, actividad.getCantrealizada());
                st.setString(45, actividad.getM_cantrealizada());
                st.setString(46, actividad.getCorta_cantrealizada());
                st.setString(47, actividad.getLarga_cantrealizada());
                
                st.setString(48, actividad.getCantplaneada());
                st.setString(49, actividad.getM_cantplaneada());
                st.setString(50, actividad.getCorta_cantplaneada());
                st.setString(51, actividad.getLarga_cantplaneada());
                
                st.setString(52, actividad.getPerfil());
                
                //nuevo 19-12-2005
                st.setString(53, actividad.getReferencia1());
                st.setString(54, actividad.getM_referencia1());
                st.setString(55, actividad.getCorta_referencia1());
                st.setString(56, actividad.getLarga_referencia1());
                
                st.setString(57, actividad.getReferencia2());
                st.setString(58, actividad.getM_referencia2());
                st.setString(59, actividad.getCorta_referencia2());
                st.setString(60, actividad.getLarga_referencia2());
                
                st.setString(61, actividad.getRefnumerica1());
                st.setString(62, actividad.getM_referencia1());
                st.setString(63, actividad.getCorta_referencia1());
                st.setString(64, actividad.getLarga_referencia1());
                
                st.setString(65, actividad.getRefnumerica2());
                st.setString(66, actividad.getM_referencia2());
                st.setString(67, actividad.getCorta_referencia2());
                st.setString(68, actividad.getLarga_referencia2());
                
                st.setString(69, actividad.getLink());
                
                st.setString(70, actividad.getDstrct());
                st.setString(71, actividad.getCodActividad());

                //////System.out.println(st);
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL MODIFICAR ACTIVIDAD " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    /**
     * Metodo listarActividad, lista las actividades                  
     * @param:compa�ia, codigo actividad decripcion
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */ 
    public void listarActividad( ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;              
        boolean sw = false;
        VecAct = null;
        String query = "SQL_LISTAR_ACTIVIDAD";
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                VecAct = new Vector();
                
                rs = st.executeQuery();
                
                while (rs.next()){
                    VecAct.add(Actividad.load(rs)); 
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL LISTAR ACTIVIDAD " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    /**
     * Metodo listarActividadxBusq, lista las actividades dependiendo al codigo de la compa�ia,
     * codigo de actividad y/o  descripcion. nota: la busqueda es con like.                                
     * @param:compa�ia, codigo actividad decripcion
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void listarActividadxBusq(String cia, String cod, String des ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        boolean sw = false;
        VecAct = null;
        String query = "SQL_BUSQUEDA_ACTIVIDAD";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cia);
                st.setString(2, cod);
                st.setString(3, des);
                
                VecAct = new Vector();
                ////System.out.println(st);
                rs = st.executeQuery();
                
                while (rs.next()){
                    VecAct.add(Actividad.load(rs)); 
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL LISTAR ACTIVIDAD " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo  BuscarActividad, busca la activida dependiendo el codigo y la compa�ia.
     * @param:compa�ia, codigo actividad
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarActividad(String cod,String cia) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        actividad = null;
        String query = "SQL_BUSCAR_ACTIVIDAD";
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cia );
                st.setString(2, cod );
                rs = st.executeQuery();
                
                if (rs.next()){
                     actividad =  Actividad.load(rs);
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL BUSCAR ACTIVIDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo  existeActividadAnulada, retorna true o false si la actividad esta anulada.
     * @param:compa�ia, codigo actividad
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean existeActividadAnulada(String cod,String cia) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        actividad = null;
        boolean sw = false;
        String query = "SQL_EXISTE_ANULADO";
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cia );
                st.setString(2, cod );
                rs = st.executeQuery();
                
                if (rs.next()){
                     sw = true;
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL BUSCAR IDENTIDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
    
    /**
     * Metodo  AnularActividad, anula la actividad.
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void anularActividad() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ANULAR_ACTIVIDAD";
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, actividad.getUser_update());
                st.setString(2, actividad.getLast_update());               
                st.setString(3, actividad.getDstrct());
                st.setString(4, actividad.getCodActividad());

                
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL ANULAR ACTIVIDAD " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    } 
    
    
    /**
     * Metodo  existActividad, retorna true o false si existe la actividad.
     * @param:compa�ia, codigo actividad
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean existActividad(String cod) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "SQL_EXISTE_ACTIVIDAD";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,cod);
                rs = st.executeQuery();
                if (rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL CODIGO DE LA ACTIVIDAD" + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }

}
