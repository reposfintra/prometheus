/*
 * ImporteTextoDAO.java
 * Created on 24 de marzo de 2009, 16:03
 */
package com.tsp.operation.model.DAOS;
import java.sql.*;
import java.util.ArrayList;
import com.tsp.operation.model.beans.NegocioApplus;
import java.io.ByteArrayInputStream;
import java.util.Dictionary;
import com.tsp.util.Utility;
import java.util.List;
import java.util.LinkedList;
import com.tsp.operation.model.beans.Imagen;
import java.io.InputStream;
import java.io.File;
import java.io.FileOutputStream;

import java.io.BufferedReader;
import java.io.FileReader;

import java.util.ArrayList;

/**
 * @author  Fintra
 */
public class ImporteTextoDAO extends MainDAO{ 
    String resultado;
    public ImporteTextoDAO() {
        
        super("ImporteTextoDAO.xml");
        resultado="sin comentario..";
    } 
 
    public List searcArchivos(String documento  ,String ruta ,String loginx) throws SQLException {
                                   
        PreparedStatement  st    = null;
        ResultSet          rs    = null;
        List               lista = null;   
        String             query = "SQL_SEARCH_ARCHIVOS";
        Connection         con   = null;
        
        String             sql   = "";
        
        try{
            File carpeta =new File(ruta);
            //boolean borrar=carpeta.delete();
            //ystem.out.println("borrar::"+borrar);
           
            deleteDirectory(carpeta);
            this.createDir(ruta);
            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
            
            sql          =  this.obtenerSQL( query );
            
            String where = " WHERE  reg_status ='' ";
            String resto = " ";
            resto += " and id NOT IN (SELECT archivo FROM avales_fenalco) ";
            resto += " and  (document)               = ('"+ documento + "') "; 
            resto+=" ORDER BY creation_date ";        
            sql += where + resto;            
            
            //ystem.out.println("La consulta sql es: "+sql);
            st  =   con.prepareStatement( sql ); 
            rs  =   st.executeQuery();
            
            String filaProcesada="";
            
            while(rs.next()){
                if( lista == null) lista = new LinkedList();
                Imagen imagen = new Imagen();
                   imagen.setActividad     ( rs.getString("id")      );
                  
                   imagen.setDocumento     ( rs.getString("document")      );
                   imagen.setNameImagen    ( rs.getString("filename")      );
                   imagen.setBinary        ( rs.getBinaryStream("filebinary"));
                   imagen.setCreation_user ( rs.getString("creation_user"));
                   String fileName        = imagen.getNameImagen();                   
                   String[] name          = fileName.split(".");
                   if(name.length>0)                   
                          fileName        = name[0] + rs.getString("filename").replaceAll(" |.|-|:","") + name[1];
                   
                   imagen.setFileName      ( fileName);
                   imagen.setFecha_creacion (rs.getString("creation_date").substring(0,16));
                   try{
                       
                 //--- Eliminamos la Imagen
                 //      delete(ruta,fileName);
                       
                 //--- Escribimos el archivo :               
                       InputStream      in   = rs.getBinaryStream("filebinary");
                       int data;
                       
                       File             f    = new File( ruta + imagen.getActividad()+"__"+fileName );
                       FileOutputStream out  = new FileOutputStream(f);
                       while( (data = in.read()) != -1 )
                           out.write( data );

                       in.close();
                       out.close(); 
                       
                       
                       //File f = new File( �cont.txt� );
                       BufferedReader entrada =null;
                       try{
                            entrada = new BufferedReader( new FileReader( f ) );                       
                            while(true){ 
                            String texto = entrada.readLine();
                               //ystem.out.println("texto::"+texto);    
                               if(texto!=null) {

                                   filaProcesada=this.procesarFila(texto,imagen.getActividad(),loginx);

                               }else{
                                   break;
                               }
                           
                            }
                            entrada.close();                       
                       }catch(Exception ee){
                           entrada.close();   
                           System.out.println("error durante en parte de llamar a procesar fila en importe texto dao"+ee.toString());
                       }
                       
                   }catch(Exception k){ 
                       System.out.println(" error searching Archivos: " + k.toString()+"__"+k.getMessage());
                       throw new Exception(" error searching Archivos: " + k.toString()+"__"+k.getMessage());
                   } 
                   
                   
                lista.add( imagen );                
            }
            
            deleteDirectory(carpeta);
        }}catch(Exception e){
            System.out.println("errorrr:"+e.toString()+"__"+e.getMessage());
            throw new SQLException("Error searcArchivos() [DAO] : " + sql +" ->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
        return lista;
    } 




  /**
   *
   * @param dir
   * @throws Exception
   */
    public void createDir(String dir) throws Exception{
        try{
        File f = new File(dir); 
        if(! f.exists() ) f.mkdir();
     /*   else{
            File []arc =  f.listFiles(); 
            while( arc.length >0  ){
               File  imagen = arc[0];
               imagen.delete();
               arc =  f.listFiles();
            }
        }*/
        }catch(Exception e){ 
            System.out.println("error en createDir en imptextdao "+e.toString()+"__"+e.getMessage());
            throw new Exception ( e.getMessage());
        }
   }
    
    static public boolean deleteDirectory(File path) {
    if( path.exists() ) {
      File[] files = path.listFiles();
      for(int i=0; i<files.length; i++) {
         if(files[i].isDirectory()) {
           deleteDirectory(files[i]);
         }
         else {
           files[i].delete();
         }
      }
    }
    return( path.delete() );
  }
       
    
    public String procesarFila(String texto,String archivo,String loginx) throws SQLException {
        String respuesta="";
        PreparedStatement  st    = null;
        String             query ="";
        Connection         con   = null;
        try{ 
            
            ResultSet          rs    = null;
            
            int[] longitudes={2,6,3,11,2,8,10,8,8,11,2,11,30,11,2,4,13,9,8,15,2,3,8,8,8,8,1,8,2,1,2}; 
            String[] subcadenas=new  String[longitudes.length];
            String subcad="";
            int last_pos_ini=0;
            String subcadenitas="";
            String no_aval="err";
            //String archiv="err";
            for (int i=0;i<longitudes.length;i++){
                if (i==5){
                    no_aval=texto.substring(last_pos_ini,last_pos_ini+longitudes[i]);
                }
                /*if (i==36){
                    archiv=texto.substring(last_pos_ini,last_pos_ini+longitudes[i]);
                }*/

                subcad="'"+texto.substring(last_pos_ini,last_pos_ini+longitudes[i])+"'";
                last_pos_ini=last_pos_ini+longitudes[i];            
                //ystem.out.println("subcad"+subcad);
                subcadenas[i]=subcad;
                subcadenitas=subcadenitas+subcad+",";



            }

            query = "SQL_INSERT_AVAL";
            

            String             sql   = "";


            
            con = this.conectarJNDI(query);
            if(con!=null){
            sql          =  this.obtenerSQL( query );
            
            sql=sql.replaceAll("subcadenitas",subcadenitas);
            sql=sql.replaceAll("usuaritox","'"+loginx+"'");
            sql=sql.replaceAll("archivitox","'"+archivo+"'");
            //ystem.out.println("sql"+sql);
            st  =   con.prepareStatement( sql );
            //ystem.out.println("sql__"+sql+"no_aval"+no_aval+"archiv"+archivo);
            
            st.setString(1, no_aval);
            st.setString(2, archivo);
            
            st.execute();
            
            
            
            }}catch(Exception e)   {
            System.out.println("errorcito en dao de avales_"+e.toString()+"___"+e.getMessage()+"__archivo::"+archivo);
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;
    }
    
    public String getESTADO(){
        return resultado;
    }
    
}

