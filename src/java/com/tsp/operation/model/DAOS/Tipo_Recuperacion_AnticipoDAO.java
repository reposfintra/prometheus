/*
 * Codigo_discrepanciaDAO.java
 *
 * Created on 27 de junio de 2005, 11:59 PM
 */

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;


/**
 *
 * @author  Jose
 */
public class Tipo_Recuperacion_AnticipoDAO extends MainDAO{
    private Tipo_Recuperacion_Anticipo Tipo_Recuperacion;
    private Vector Tipo_Recuperacion_Anticipo;   
    private TreeMap treeTRA;
    /** Creates a new instance of Codigo_discrepanciaDAO */
    public Tipo_Recuperacion_AnticipoDAO() {
        super("Tipo_Recuperacion_AnticipoDAO.xml");
    }
    
     
    /**
     * Getter for property Tipo_Recuperacion.
     * @return Value of property Tipo_Recuperacion.
     */
    public com.tsp.operation.model.beans.Tipo_Recuperacion_Anticipo getTipo_Recuperacion() {
        return Tipo_Recuperacion;
    }
    
    /**
     * Setter for property Tipo_Recuperacion.
     * @param Tipo_Recuperacion New value of property Tipo_Recuperacion.
     */
    public void setTipo_Recuperacion(com.tsp.operation.model.beans.Tipo_Recuperacion_Anticipo Tipo_Recuperacion) {
        this.Tipo_Recuperacion = Tipo_Recuperacion;
    }
    
    /**
     * Getter for property Tipo_Recuperacion_Anticipoe.
     * @return Value of property Tipo_Recuperacion_Anticipoe.
     */
    public java.util.Vector getTipo_Recuperacion_Anticipo() {
        return Tipo_Recuperacion_Anticipo;
    }
    
    /**
     * Setter for property Tipo_Recuperacion_Anticipoe.
     * @param Tipo_Recuperacion_Anticipoe New value of property Tipo_Recuperacion_Anticipoe.
     */
    public void setTipo_Recuperacion_Anticipo(java.util.Vector Tipo_Recuperacion_Anticipoe) {
        this.Tipo_Recuperacion_Anticipo = Tipo_Recuperacion_Anticipo;
    }
    

/**
 * 
 * @throws SQLException
 */
    public void insert() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_INSERTAR";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, Tipo_Recuperacion.getCodigo());
                st.setString(2, Tipo_Recuperacion.getDescripcion());
                st.setString(3, Tipo_Recuperacion.getUsuario());
                st.setString(4, Tipo_Recuperacion.getDistrito());
                st.setString(5, Tipo_Recuperacion.getBase());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DE LA CAUSA DE ANULACION" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


/**
 * 
 * @param cod
 * @throws SQLException
 */
    public void search(String cod)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Tipo_Recuperacion=null;
        String query = "SQL_SEARCH";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,cod);
                rs= st.executeQuery();
                if(rs.next()){
                    Tipo_Recuperacion = new Tipo_Recuperacion_Anticipo();
                    Tipo_Recuperacion.setCodigo(rs.getString("codigo"));
                    Tipo_Recuperacion.setDescripcion(rs.getString("descripcion"));
                    Tipo_Recuperacion.setDistrito(rs.getString("dstrct"));
                }
             
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA TIPO DE RECUPERACION ANTICIPO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }        
    }


/**
 * 
 * @param cod
 * @return
 * @throws SQLException
 */
    public boolean exist(String cod) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "SQL_EXISTE";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,cod);
                rs = st.executeQuery();
                if (rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL CODIGO DE LAS TIPO DE RECUPERACION ANTICIPO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }



/**
 *
 * @throws SQLException
 */
    public void list()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_CONSULTA_LIST";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs= st.executeQuery();
                Tipo_Recuperacion_Anticipo = new Vector();
                while(rs.next()){
                    Tipo_Recuperacion = new Tipo_Recuperacion_Anticipo();
                    Tipo_Recuperacion.setCodigo(rs.getString("codigo"));
                    Tipo_Recuperacion.setDescripcion(rs.getString("descripcion"));
                    Tipo_Recuperacion.setDistrito(rs.getString("dstrct"));
                    Tipo_Recuperacion_Anticipo.add(Tipo_Recuperacion_Anticipo);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LAS TIPO DE RECUPERACION ANTICIPO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }


/**
 *
 * @throws SQLException
 */
    public void update() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_UPDATE";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,Tipo_Recuperacion.getDescripcion());
                st.setString(2,Tipo_Recuperacion.getUsuario());
                st.setString(3,Tipo_Recuperacion.getCodigo());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACI�N  DE LAS TIPO DE RECUPERACION ANTICIPO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


 /**
  *
  * @throws SQLException
  */
    public void anular() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ANULAR";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,Tipo_Recuperacion.getUsuario());
                st.setString(2,Tipo_Recuperacion.getCodigo());
                st.executeUpdate();
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE LAS TIPO DE RECUPERACION ANTICIPO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


 /**
  * 
  * @param codigo
  * @param desc
  * @throws SQLException
  */
    public void consultar(String codigo, String desc)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_CONSULTAR";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, codigo+"%");
                st.setString(2, desc+"%");
                rs= st.executeQuery();
                Tipo_Recuperacion_Anticipo = new Vector();
                while(rs.next()){
                    Tipo_Recuperacion = new Tipo_Recuperacion_Anticipo();
                    Tipo_Recuperacion.setCodigo(rs.getString("codigo"));
                    Tipo_Recuperacion.setDescripcion(rs.getString("descripcion"));
                    Tipo_Recuperacion.setDistrito(rs.getString("dstrct"));
                    Tipo_Recuperacion_Anticipo.add(Tipo_Recuperacion);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA DE LAS TIPO DE RECUPERACION ANTICIPO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    /**
     * Getter for property treeTRA.
     * @return Value of property treeTRA.
     */
    public java.util.TreeMap getTreeTRA() {
        return treeTRA;
    }    
  
    /**
     * Setter for property treeTRA.
     * @param treeTRA New value of property treeTRA.
     */
    public void setTreeTRA(java.util.TreeMap treeTRA) {
        this.treeTRA = treeTRA;
    }

/**
 * 
 * @throws SQLException
 */
    public void llenarTree()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_CONSULTAR_TREE";
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                treeTRA = new TreeMap();
                while(rs.next()){
                    treeTRA.put(rs.getString("descripcion"), rs.getString("codigo"));
                }
                
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL TREE DE TIPOS DE RECUPERACION" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
}
