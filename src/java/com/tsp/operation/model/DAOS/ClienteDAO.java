package com.tsp.operation.model.DAOS;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.sql.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;
import java.util.*;

public class ClienteDAO extends MainDAO {
    private TreeMap cbxCliente;
    private TreeMap AFCliente;
    private LinkedList clientes;
    private BeanGeneral BeanCliente;
    private Vector vector;
    private Vector vectorCod;
    private Vector vectorsSoporte;
    private Vector vectorsCargaSop;
    private String mensaje;
    
    /** Creates a new instance of ClienteDAO */
    public ClienteDAO() {
        super("ClienteDAO.xml");
    }
    public ClienteDAO(String dataBaseName) {
        super("ClienteDAO.xml", dataBaseName);
    }
    
    public TreeMap getCbxCliente(){
        return this.cbxCliente;
    }
    
    public void setCbxCliente(TreeMap clientes){
        this.cbxCliente = clientes;
    }
    
    public void listar() throws SQLException {
        listar("SQL_OBTENER_CLIENTES_ACTIVOS",null);
    }
    
    /** Lista los clientes de la tabla cliente */
    private void listar(String nombreConsulta, String [] parametros) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        cbxCliente = null;
        cbxCliente = new TreeMap();
        try{
            con = this.conectarJNDI(nombreConsulta);
            if (con != null) {
             ps = con.prepareStatement(this.obtenerSQL(nombreConsulta));//JJCastro fase2
            if ( parametros != null ){
                for(int i=0; i<parametros.length; i++ ){
                    ps.setString(i+1, parametros[i]);
                }
            }
            ////System.out.println("query clientes: "+ps);
            rs = ps.executeQuery();
            cbxCliente.put("", "");
            while (rs.next()) {
                cbxCliente.put(rs.getString(2), rs.getString(1));
            }
        }}
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR CLIENTES" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Busca los clientes que puede ver un usuario de acuerdo al tipo de usuario.
     * @param loggedUser el usuario en sesi�n
     * @throws SQLException si algun error ocurre en el acceso a la base de datos
     * @return Un TreeMap que contiene la lista de clientes visibles para el usuario en sesi�n
     * @autor Alejandro Payares
     * @ultima_modificaci�n Marzo 2 de 2006, 03:15 pm
     */
    public TreeMap listar(Usuario loggedUser) throws SQLException {
        ////System.out.println("tipo usuario = "+loggedUser.getTipo());
        if(loggedUser.getTipo().equals("ADMIN") || loggedUser.getTipo().equals("TSPUSER")){
            listar();
        }
        else if( loggedUser.getTipo().equals("DEST") ){
            listar("SQL_OBTENER_CLIENTE_DESTINATARIO",new String []{loggedUser.getClienteDestinat()});
        }
        else {
            buscarClientesDeUsuario(loggedUser);
        }
        return this.getCbxCliente();
    }
    
    
    /**
     * Busca los clientes asociados a un usuario y los carga en un TreeMap
     * @param loggedUser El usuario en sesi�n
     * @throws SQLException si algun error ocurre en el acceso a la base de datos
     * @autor Alejandro Payares
     * @ultima_modificaci�n Marzo 2 de 2006, 03:13 pm
     */
    public void buscarClientesDeUsuario(Usuario loggedUser) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs;
        cbxCliente = new TreeMap();
        String query = "SQL_OBTENER_CLIENTES_ACTIVOS_USUARIO";
        try {
            String sql = this.obtenerSQL(query);
            String [] vec = loggedUser.getClienteDestinat().split("#");
            StringBuffer sb = new StringBuffer();
            for( int i=0; i< vec.length; i++ ){
                sb.append("'"+vec[i]+"',");
            }
            if ( sb.length() > 0 ) {
                // eliminamos la ultima coma
                sb.deleteCharAt(sb.length()-1);
            }
            sql = sql.replaceFirst("'reemplazo'", sb.toString());
            con = this.conectarJNDI(query);//JJCastro fase2
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while( rs.next() ){
                cbxCliente.put(rs.getString("nomcli"), rs.getString("codcli"));
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA CLIENTES USUARIOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Crea una lista de objetos de clientes.
     * @throws SQLException si aparece un error en la base de datos
     */
    public void clienteSearch() throws SQLException {
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        clientes = null;
        String query = "SQL_CLIENTE_SEARCH";
        try {
            // Buscar los clientes y meterlos en una lista

            con = this.conectarJNDI(query);
            if (con != null) {
            pstmt = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = pstmt.executeQuery();
            clientes = new LinkedList();
            while (rs.next()) {
                clientes.add(Cliente.load(rs));
            }
            
            
        }} catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA CLIENTES USUARIOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (pstmt  != null){ try{ pstmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /*
     * Estos m�todos y atributos hacian parte de la clase com.tsp.operation.model.ClienteDao creada
     * por Henry Osorio. Fueron movidos a esta clase porque exista un solo DAO para los
     * clientes. Ademas los m�todos hechos por Henry Osorio fueron adaptados al sistema
     * de consultas por medio de archivos XML.
     * Cambio realizado por Alejandro Payares. nov 17 de 2005 4:21 pm
     */
    
    private Cliente cliente;
    private Vector clientesHenry;
    
    public Cliente getCliente(){
        return cliente;
    }
    
    public void setCliente(Cliente cli){
        this.cliente = cli;
    }
    public void updateCliente()throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_INSERT_CLIENTE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setFloat(1, cliente.getRentabilidad());
            st.setString(2, cliente.getTexto_oc());
            st.setString(3, cliente.getCodcli());
            ////System.out.println("Query: "+st);
            st.execute();
            }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE UN CLIENTE " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }




    public Vector listarClientes()throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Vector clientesHenry = new Vector();
        String query = "SQL_LISTAR_CLIENTES";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ////System.out.println(st);
            rs = st.executeQuery();
            while(rs.next()){
                Cliente c = new Cliente();
                c.setNomcli(rs.getString("nomcli"));
                c.setCodcli(rs.getString("codcli"));
                clientesHenry.addElement(c);
            }
           
            }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS CLIENTE " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
         return clientesHenry;
        
    }
    
     public Vector listarClientes2()throws SQLException{ 
         Connection con = null;
         PreparedStatement st = null;
        ResultSet rs = null;
        Vector clientesHenry = new Vector();
         String query = "SQL_LISTAR_CLIENTES_2";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ////System.out.println(st);
            rs = st.executeQuery();
            while(rs.next()){
                Cliente c = new Cliente();
                c.setNomcli(rs.getString("nomcli"));
                c.setCodcli(rs.getString("nit"));
                clientesHenry.addElement(c);
            }
           
            }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS CLIENTE " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
         return clientesHenry;
    }
    
    /**
     * Getter for property clientes.
     * @return Value of property clientes.
     */
    public java.util.Vector getClientes() {
        return clientesHenry;
    }
    
    /**
     * Getter for property clientes.
     * @return Value of property clientes.
     */
    public java.util.LinkedList getClientesSot() {
        return clientes;
    }
    
    /**
     * Setter for property clientes.
     * @param clientes New value of property clientes.
     */
    public void setClientes(java.util.Vector clientes) {
        this.clientesHenry = clientes;
    }
    
    /**
     * Retorna notas adicionales asociadas al cliente pasado como par�metro.
     * @param codCliente C�digo del cliente.
     * @throws SQLException Si un error de base de datos ocurre.
     * @return Notas del cliente.
     */
    public String notasClienteSearch(final String codCliente) throws SQLException { 
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        String query = "SQL_BUSCAR_NOTAS_CLIENTE";
        String notas = "";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
           pstmt = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            pstmt.setString(1, codCliente );
            rs = pstmt.executeQuery();
            if (rs.next()) {
                notas = rs.getString("notas");
            }
 
        }} finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (pstmt  != null){ try{ pstmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return notas;
    }
    
    /**
     * Metodo searchGeneralClientes, busca los clientes por nombre o codigo
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : String codigo o el nombre  buscar
     * @version : 1.0
     */
    public void searchGeneralClientes(String codnom)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        clientes = null;
        clientesHenry = new Vector();
        String query = "SQL_BUSCAR_CLIENTES";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, codnom+"%");
            st.setString(2, codnom+"%");
            ////System.out.println(st);
            rs = st.executeQuery();
            while(rs.next()){
                Cliente c = new Cliente();
                c.setNomcli(rs.getString("nomcli"));
                c.setCodcli(rs.getString("codcli"));
                clientesHenry.addElement(c);
            }
            
            }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS CLIENTE " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    /**
     * Metodo existeCliente, busca un cliente por el codigo
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : String codigo
     * @return : retorna true si el cliente existe en la Bd o false si no
     * @version : 1.0
     */
    public boolean existeCliente(String codcli)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        clientes = null;
        clientesHenry = new Vector();
       boolean  existe = false;
        String query = "SQL_BUSCAR_CLIENTE" ;

        try {
            con  = this.conectarJNDI(query);
            if(con!=null){
                st = con.prepareStatement("select codcli from cliente where codcli=? and reg_status = '' ");
                st.setString(1, codcli);
                ////System.out.println(st);
                rs = st.executeQuery();
                if(rs.next()){
                    existe =  true;
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS CLIENTE " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return existe;
    }
    
    ///by rarp
    public boolean existeNitCliente(String codcli,String nit)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        clientes = null;
        clientesHenry = new Vector();
        String query = "SQL_BUSCAR_CLIENTE";
        
        try {
            con = this.conectarJNDI(query);
            if(con!=null){
                st = con.prepareStatement("select codcli from cliente where codcli != ? AND reg_status != 'A' AND nit = ? ");
                st.setString(1, codcli);
                st.setString(2,nit);
                //System.out.println(st);
                rs = st.executeQuery();
                return rs.next();
            }
            return false;
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS CLIENTE " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    //end by rarp
    
    public boolean existeClienteIngresarEstadoLLeno(String codcli)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        clientes = null;
        clientesHenry = new Vector();
        String query = "SQL_BUSCAR_CLIENTE";
        try {
            con = this.conectarJNDI(query);
            if(con!=null){
                st = con.prepareStatement("select codcli from cliente where codcli=? and reg_status= 'I'");
                st.setString(1, codcli);
                ////System.out.println(st);
                rs = st.executeQuery();
                return rs.next();
            }
            return false;
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS CLIENTE " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }



    
     public boolean existeClienteIngresarEstadoVacio(String codcli)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        clientes = null;
        clientesHenry = new Vector();
         String query = "SQL_BUSCAR_CLIENTE";
        try {
            con = this.conectarJNDI(query);
            if(con!=null){
                st = con.prepareStatement("select codcli from cliente where codcli=? and reg_status= 'A'");
                st.setString(1, codcli);
                ////System.out.println(st);
                rs = st.executeQuery();
                return rs.next();
            }
            return false;
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS CLIENTE " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo vClientes , Metodo que retorna un Vector de Vectores con las placas dado Un Striing con el caracter inicial de cualquier placa
     * @autor : Ing. David Lamadrid
     * @param : String nombre
     * @version : 1.0
     */
    public static final String clientesPorLetra="select codcli,nomcli from cliente where nomcli like UPPER(?)";
    public void vClientes(String nombre) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        clientesHenry = new Vector();
        String query = "SQL_BUSCAR_CLIENTE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.clientesPorLetra);
                st.setString(1, nombre+"%");
                
                ////System.out.println("Query: "+st.toString());
                rs = st.executeQuery();
                while(rs.next()) {
                    Vector cliente = new Vector();
                    cliente.add(rs.getString("codcli"));
                    cliente.add(rs.getString("nomcli"));
                    clientesHenry.add(cliente);
                    cliente = null;//Liberar Espacio JJCastro
                }
            }
        }catch(SQLException e) {
            throw new SQLException("ERROR AL INSERTAR EL ESTADO" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    /**
     * Metodo datosCliente , Metodo que retorna el nombre y el codigo del cliente
     * @autor : Ing. jose de la rosa
     * @param : String codigo del cliente
     * @version : 1.0
     */
    public void datosCliente(String codcli)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        cliente=null;
        String query = "SQL_DATOS_CLIENTE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, codcli);
            rs = st.executeQuery();
            if(rs.next()){
                cliente = new Cliente();
                cliente.setCodcli(rs.getString("codcli"));
                cliente.setNomcli(rs.getString("nomcli"));
            } else {
                cliente = new Cliente(false);
            }
            }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE UN CLIENTE " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
 
    //Igomez 01-08/2006
   
     //Igomez 01-08/2006
   public void consultarCliente(String nombre) throws SQLException {
       Connection con = null;
       PreparedStatement st = null;
        ResultSet rs = null;
        clientesHenry=null;
        String query = "SQL_CONSULTAR_CLIENTE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, nombre.toUpperCase() );
            rs = st.executeQuery();
            clientesHenry = new Vector();
            Cliente cl = new Cliente();
            cl.setCodcli("SN");
            cl.setNomcli("SELECCIONE UN DEPARTAMENTO");
            cl.setUnidad("");
            cl.setNit("");
            clientesHenry.add(cl);
            ////System.out.println("CLIENTE: "+st);
            while(rs.next()){
                Cliente c = new Cliente();
                c.setCodcli((rs.getString("codcli")!=null)?rs.getString("codcli"):"");
                c.setNomcli((rs.getString("nomcli")!=null)?rs.getString("nomcli"):"");
                c.setUnidad((rs.getString("unidad")!=null)?rs.getString("unidad"):"");
                c.setNit   ((rs.getString("nit")!=null)?rs.getString("nit"):"");
                ////System.out.println("NIT-->"+rs.getString("nit"));
                clientesHenry.addElement(c);
            }
        }}
        catch(Exception e){
            throw new SQLException(
            "ERROR DURANTE EL PROCESO DE LISTAR LOS CLIENTES POR " +
            e.getMessage() + " " /*+ e.getErrorCode()*/ );
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
   //Ivan Gomez 27 julio 2006
    /**
     * Busca el cliente con el codigo digitado por el usuario
     * @param  String codigo del cliente
     * @throws SQLException si algun error ocurre en el acceso a la base de datos
     * @return beans de cliente
     * @autor Ivan Dario Gomez
     */  
    public Cliente BuscarCliente(String codCli) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Cliente cliente    = null;
        String query = "SQL_BUSCAR_CLIENTE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, codCli );
            rs = st.executeQuery();
            if(rs.next()){
                cliente = new Cliente();
                cliente.setCodcli((rs.getString("codcli")!=null)?rs.getString("codcli"):"");
                cliente.setNomcli((rs.getString("nomcli")!=null)?rs.getString("nomcli"):"");
                cliente.setUnidad((rs.getString("unidad")!=null)?rs.getString("unidad"):"");
                cliente.setNit((rs.getString("nit")!=null)?rs.getString("nit"):"");                
            }            
        }}
        catch(Exception e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL CLIENTE POR " + e.getMessage() + " " /*+ e.getErrorCode()*/ );
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return cliente;        
    }


    /**
     * 
     * @param nombre
     * @throws SQLException
     */
    public void consultarClienteFactura(String nombre) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        clientesHenry=null;
        String query = "SQL_CONSULTAR_CLIENTE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, nombre.toUpperCase() );
            rs = st.executeQuery();
            clientesHenry = new Vector();
            Cliente cl = new Cliente();
            cl.setCodcli("SN");
            cl.setNomcli("SIN SELECCION");
            clientesHenry.add(cl);
            ////System.out.println("CLIENTE: "+st);
            while(rs.next()){
                Cliente c = new Cliente();
                c.setCodcli(rs.getString("codcli"));
                c.setNomcli(rs.getString("nomcli"));
                c.setUnidad(rs.getString("unidad"));
                clientesHenry.addElement(c);
            }
        }}
        catch(Exception e){
            throw new SQLException(
            "ERROR DURANTE EL PROCESO DE LISTAR LOS CLIENTES POR " +
            e.getMessage() + " " /*+ e.getErrorCode()*/ );
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }





    /**
     * 
     * @param numpla
     * @throws SQLException
     */
    public void searchAccount_Code_C( String numpla )throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        cliente = null;
        String query = "SQL_SEARCH_CODIGO_CUENTA";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, numpla);
            ////System.out.println("Account_code : " +st );
            rs = st.executeQuery();
            if(rs.next()){
                cliente = new Cliente();
                cliente.setUnidad(rs.getString("account_code_c"));
            }
            }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE UN CLIENTE " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
      /**
    * Metodo searchNit , Metodo que retorna el el nit del cliente de una planilla
    * @autor : Ing. Ivan Gomez
    * @param : String numero de planilla
    * @version : 1.0
    */
     public void searchNit( String numpla )throws SQLException{
         Connection con = null;
         PreparedStatement st = null;
        ResultSet rs = null;
        cliente = null;
         String query = "SQL_BUSCAR_NIT";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, numpla);
            rs = st.executeQuery();
            if(rs.next()){
                cliente = new Cliente();
                cliente.setCodcli((rs.getString("nit")!=null)?rs.getString("nit"):"");
                cliente.setNomcli((rs.getString("propietario")!=null)?rs.getString("propietario"):"");
                ////System.out.println("PROPIRTARIO---> "+ rs.getString("propietario"));
            }
            }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL NIT DE UN CLIENTE " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
     
     /**
    * Metodo datosCliente , Metodo que retorna el nombre y el codigo del cliente
    * @autor : Ing. Diogenes Bastidas 
    * @param : String codigo del cliente
    * @version : 1.0
    */    
    public void informacionCliente(String codcli)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        cliente=null;
        String query ="SQL_INFO_CLIENTE" ;
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, codcli);
            ////System.out.println("");
            ////System.out.println(st.toString());
            rs = st.executeQuery();
            if(rs.next()){
                cliente = new Cliente();
                cliente.setCodcli(rs.getString("codcli")); 
                cliente.setNomcli(rs.getString("nomcli"));
                cliente.setNit(rs.getString("nit"));
                cliente.setBranch_code( rs.getString("branch_code") );
                cliente.setBank_account_no( rs.getString("bank_account_no") );
            } 
            }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE UN CLIENTE " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }






/**
 *
 * @param BeanCliente
 * @throws SQLException
 */
    //funcion ingresar cliente hecho por FFERNANDEZ
     public void agregarCliente(BeanGeneral BeanCliente)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
         String query = "SQL_INGRESAR_CLIENTE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, BeanCliente.getValor_01());
            st.setString(2, BeanCliente.getValor_02());
            st.setString(3, BeanCliente.getValor_03());
            st.setString(4, BeanCliente.getValor_04());
            st.setString(5, BeanCliente.getValor_05());
            st.setString(6, BeanCliente.getValor_06());
            st.setString(7, BeanCliente.getValor_07());
            st.setString(8, BeanCliente.getValor_08());
            st.setString(9, BeanCliente.getValor_09());
            st.setString(10, BeanCliente.getValor_10());
            st.setString(11, BeanCliente.getValor_11());
            st.setString(12, BeanCliente.getValor_12());
            st.setString(13, BeanCliente.getValor_13());
            st.setString(14, BeanCliente.getValor_14());
            st.setString(15, BeanCliente.getValor_15());
            st.setString(16, BeanCliente.getValor_16());
            st.setString(17, BeanCliente.getValor_17());
            st.setString(18, BeanCliente.getValor_18());
            st.setString(19, BeanCliente.getValor_19());
            st.setString(20, BeanCliente.getValor_20());
            st.setString(21, BeanCliente.getValor_21());
            st.setString(22, BeanCliente.getValor_22());
            st.setString(23, BeanCliente.getValor_23());
            st.setString(24, BeanCliente.getValor_24());
            st.setString(25, BeanCliente.getValor_25());
            
            //MODIFICACION CLIENTE
            
            st.setString(26, BeanCliente.getValor_26());
            st.setString(27, BeanCliente.getValor_27());
            st.setString(28, BeanCliente.getValor_28());
            st.setString(29, BeanCliente.getValor_29());
            st.setString(30, BeanCliente.getValor_30());
            st.setString(31, BeanCliente.getValor_31()); 
            st.setString(32, BeanCliente.getValor_32()); 
            st.setString(33, BeanCliente.getValor_33());  
            st.setString(34, BeanCliente.getValor_34()); 
            st.setString(35, BeanCliente.getValor_35()); 
            st.setString(36, BeanCliente.getValor_36()); 
            st.setString(37, BeanCliente.getValor_37()); 
            st.setString(38, BeanCliente.getValor_39());
            st.setString(39, BeanCliente.getValor_40());
            st.setString(40, BeanCliente.getValor_41());
            st.setString(41, BeanCliente.getValor_42());
            st.setString(42, BeanCliente.getValor_43()); 
            st.setString(43, BeanCliente.getValor_44());  
            st.setString(44, BeanCliente.getValor_45());
            st.setString(45, BeanCliente.getValor_46());
            /*st.setString(46, Float.toString(BeanCliente.getValor_47()));
            st.setString(47, Float.toString(BeanCliente.getValor_48()));
            st.setString(48, Float.toString(BeanCliente.getValor_49()));
            
            st.setString(49, BeanCliente.getValor_50());
            st.setString(50, BeanCliente.getValor_51());
            st.setString(51, BeanCliente.getValor_52());
            st.setString(52, BeanCliente.getValor_53());*/
            //st.setString(46, BeanCliente.getValor_54());
            //System.out.println("entro insersion  --<>>>"+st.toString());
            st.executeUpdate();
           
            }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DEL CLIENTE " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }








      public void buscarClienteModificar (String codCli) throws Exception {
          Connection con = null;
          PreparedStatement st = null;
        ResultSet rs = null;
        BeanGeneral BeanCliente    = null;
        vector = new Vector();
          String query = "SQL_BUSQUEDA_CLIENTE_MODIFICAR";
        try {
            ////System.out.println("codigoDao : "+codCli);
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, codCli );
            rs = st.executeQuery();
            System.out.println("entro  --<>>>"+st.toString());
            if(rs.next()){
                BeanCliente = new BeanGeneral();
                
                BeanCliente.setValor_01 ((rs.getString("estado")!=null)?rs.getString("estado"):"");
                BeanCliente.setValor_02 ((rs.getString("codcli")!=null)?rs.getString("codcli"):"");
                BeanCliente.setValor_03 ((rs.getString("nomcli")!=null)?rs.getString("nomcli"):"");
                BeanCliente.setValor_04 ((rs.getString("notas")!=null)?rs.getString("notas"):"");
                BeanCliente.setValor_05 ((rs.getString("agduenia")!=null)?rs.getString("agduenia"):"");
                BeanCliente.setValor_06 ((rs.getString("base")!=null)?rs.getString("base"):"");
                BeanCliente.setValor_07 ((rs.getString("texto_oc")!=null)?rs.getString("texto_oc"):"");
                BeanCliente.setValor_08 ((rs.getString("nit")!=null)?rs.getString("nit"):"");
                BeanCliente.setValor_09 ((rs.getString("cedagente")!=null)?rs.getString("cedagente"):"");
                BeanCliente.setValor_10 ((rs.getString("rentabilidad")!=null)?rs.getString("rentabilidad"):"");
                BeanCliente.setValor_11 (this.CaragarSoprteCliente( BeanCliente.getValor_02()));
                BeanCliente.setValor_12 ((rs.getString("fiduciaria")!=null)?rs.getString("fiduciaria"):"");
                BeanCliente.setValor_13 ((rs.getString("dstrct")!=null)?rs.getString("dstrct"):"");
                BeanCliente.setValor_14 ((rs.getString("moneda")!=null)?rs.getString("moneda"):"");
                BeanCliente.setValor_15 ((rs.getString("forma_pago")!=null)?rs.getString("forma_pago"):"");
                BeanCliente.setValor_16 ((rs.getString("plazo")!=null)?rs.getString("plazo"):"");
                BeanCliente.setValor_17 ((rs.getString("zona")!=null)?rs.getString("zona"):"");
                BeanCliente.setValor_18 ((rs.getString("branch_code")!=null)?rs.getString("branch_code"):"");
                BeanCliente.setValor_19 ((rs.getString("bank_account_no")!=null)?rs.getString("bank_account_no"):"");
                BeanCliente.setValor_20 ((rs.getString("cmc")!=null)?rs.getString("cmc"):"");
                BeanCliente.setValor_21 ((rs.getString("unidad")!=null)?rs.getString("unidad"):"");
                BeanCliente.setValor_22 ((rs.getString("codigo_impuesto")!=null)?rs.getString("codigo_impuesto"):"");
                BeanCliente.setValor_23 ((rs.getString("agfacturacion")!=null)?rs.getString("agfacturacion"):"");
                BeanCliente.setValor_24 ((rs.getString("reg_status")!=null)?rs.getString("reg_status"):"");
                BeanCliente.setValor_25 ((rs.getString("creation_date")!=null)?rs.getString("creation_date"):"");
                
                
                
                 //MODIFICACION ANEXOS DE PARAMETROS
                BeanCliente.setValor_26 ((rs.getString ("direccion")!=null)?rs.getString ("direccion"):"");
                BeanCliente.setValor_27 ((rs.getString ("telefono")!=null)?rs.getString ("telefono"):"");
                BeanCliente.setValor_28 ((rs.getString ("nomContacto")!=null)?rs.getString ("nomContacto"):"");
                BeanCliente.setValor_29 ((rs.getString ("telContacto")!=null)?rs.getString ("telContacto"):"");
                BeanCliente.setValor_30((rs.getString ("email_Contacto")!=null)?rs.getString ("email_Contacto"):"");
                BeanCliente.setValor_31((rs.getString ("dir_Factura")!=null)?rs.getString ("dir_Factura"):""); 
                BeanCliente.setValor_32 ((rs.getString ("ma_PreFactura")!=null)?rs.getString ("ma_PreFactura"):"");
                BeanCliente.setValor_33 ((rs.getString ("tiempoPreFac")!=null)?rs.getString ("tiempoPreFac"):"");
                BeanCliente.setValor_34((rs.getString ("tiempo_leg")!=null)?rs.getString ("tiempo_leg"):"");
                BeanCliente.setValor_35((rs.getString ("tiempo_Re_fact")!=null)?rs.getString ("tiempo_Re_fact"):""); 
                BeanCliente.setValor_36((rs.getString ("DiaPago")!=null)?rs.getString ("DiaPago"):""); 
                BeanCliente.setValor_37((rs.getString ("for_facturacion")!=null)?rs.getString ("for_facturacion"):""); 
                BeanCliente.setValor_39((rs.getString ("direccion_contacto")!=null)?rs.getString ("direccion_contacto"):""); 
                BeanCliente.setValor_40((rs.getString ("agencia_cobro")!=null)?rs.getString ("agencia_cobro"):""); 
                BeanCliente.setValor_41((rs.getString ("hc")!=null)?rs.getString ("hc"):""); 
                BeanCliente.setValor_42((rs.getString ("rif")!=null)?rs.getString ("rif"):""); 
                BeanCliente.setValor_43((rs.getString ("ciudad")!=null)?rs.getString ("ciudad"):""); 
                BeanCliente.setValor_44((rs.getString ("ciudad_factura")!=null)?rs.getString ("ciudad_factura"):""); 
                BeanCliente.setValor_45((rs.getString ("pais")!=null)?rs.getString ("pais"):""); 
                BeanCliente.setValor_46((rs.getString ("pais_envio")!=null)?rs.getString ("pais_envio"):""); 
                
                //by rarp
                
                /*BeanCliente.setValor_47(rs.getFloat ("treduc")); 
                BeanCliente.setValor_48(rs.getFloat ("cust")); 
                BeanCliente.setValor_49(rs.getFloat ("rem")); 
                
                BeanCliente.setValor_50((rs.getString ("titcta")!=null)?rs.getString ("titcta"):""); 
                BeanCliente.setValor_51((rs.getString ("cccta")!=null)?rs.getString ("cccta"):""); 
                BeanCliente.setValor_52((rs.getString ("tcta")!=null)?rs.getString ("tcta"):""); 
                BeanCliente.setValor_53((rs.getString ("ncta")!=null)?rs.getString ("ncta"):"");*/ 
                //BeanCliente.setValor_54((rs.getString ("nitas")!=null)?rs.getString ("nitas"):""); 
                
                //end by rarp
                this.vector.addElement( BeanCliente );
            }
            //return BeanCliente;
            }}
        catch(Exception e){
             e.printStackTrace();
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    /**
     * Metodo modificarIngreso, modifica un registro en la tabla ingreso
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void modificarCliente(BeanGeneral BeanCliente) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_MODIFICAR_CLIENTE";
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
           
            st.setString(1, BeanCliente.getValor_01());
            st.setString(2, BeanCliente.getValor_03());
            st.setString(3, BeanCliente.getValor_04());
            st.setString(4, BeanCliente.getValor_05());
            st.setString(5, BeanCliente.getValor_06());
            st.setString(6, BeanCliente.getValor_07());
            st.setString(7, BeanCliente.getValor_08());
            st.setString(8, BeanCliente.getValor_09());
            st.setString(9, BeanCliente.getValor_10());
            st.setString(10,BeanCliente.getValor_11());
            st.setString(11, BeanCliente.getValor_12());
            st.setString(12, BeanCliente.getValor_13());
            st.setString(13, BeanCliente.getValor_14());
            st.setString(14, BeanCliente.getValor_15());
            st.setString(15, BeanCliente.getValor_16());
            st.setString(16,BeanCliente.getValor_17());
            st.setString(17, BeanCliente.getValor_18());
            st.setString(18,BeanCliente.getValor_19());
            st.setString(19,BeanCliente.getValor_20());
            st.setString(20,BeanCliente.getValor_21());
            st.setString(21, BeanCliente.getValor_22());
            st.setString(22, BeanCliente.getValor_23());
            st.setString(23,BeanCliente.getValor_25());
             
            //MODIFICACION CLIENTE
            
            st.setString(24, BeanCliente.getValor_26());
            st.setString(25, BeanCliente.getValor_27());
            st.setString(26, BeanCliente.getValor_28());
            st.setString(27, BeanCliente.getValor_29());
            st.setString(28, BeanCliente.getValor_30());
            st.setString(29, BeanCliente.getValor_31());
            st.setString(30, BeanCliente.getValor_32());
            st.setString(31, BeanCliente.getValor_33());
            st.setString(32, BeanCliente.getValor_34());
            st.setString(33, BeanCliente.getValor_35());
            st.setString(34, BeanCliente.getValor_36());
            st.setString(35, BeanCliente.getValor_37());
            st.setString(36, BeanCliente.getValor_39());
            st.setString(37, BeanCliente.getValor_40());
            st.setString(38,BeanCliente.getValor_41());
            st.setString(39,BeanCliente.getValor_42());
            st.setString(40,BeanCliente.getValor_43());
            st.setString(41,BeanCliente.getValor_44());
            st.setString(42,BeanCliente.getValor_45());
            st.setString(43,BeanCliente.getValor_46());
            
            
            /*st.setString(44, Float.toString(BeanCliente.getValor_47()));
            st.setString(45, Float.toString(BeanCliente.getValor_48()));
            st.setString(46, Float.toString(BeanCliente.getValor_49()));
            
            st.setString(47,BeanCliente.getValor_50());
            st.setString(48,BeanCliente.getValor_51());
            st.setString(49,BeanCliente.getValor_52());
            st.setString(50,BeanCliente.getValor_53());*/
            //st.setString(44,BeanCliente.getValor_54());
            
            st.setString(44,BeanCliente.getValor_02());
            
            
            
            System.out.println("query modificacionessss"+st.toString());
             st.executeUpdate();
        }}catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error al MODIFICAR Cliente [ClienteDAO.xml]... \n"+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }




    /**
     *
     * @param codCli
     * @throws Exception
     */

    public void eliminarCliente(String codCli) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ELIMINAR_CLIENTE";
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, codCli);
            
            ////System.out.println(st.toString());
            st.executeUpdate();
            }}catch(Exception e) {
            throw new Exception("Error al ELIMINAR Cliente [ClienteDAO.xml]... \n"+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }



    
    public void BusquedaCLiente(String codCli,String nit,String nombre,String agencia,String pais ) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        BeanGeneral BeanCliente = null;
        vector = new Vector();
        String codigoEnvPAgina = codCli;
        String nitEnvPAgina = nit;
        String nombreEnvPAgina = nombre;
        String agenciaEnvPAgina = agencia;
        Connection con = null;
        String query = "SQL_BUSCA_CLIENTE";

        try{
            con = this.conectarJNDI(query);
            if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            int cont = 1;
            st.setString(1, "%"+codCli+"%");
            st.setString(2, "%"+nit+"%");
            st.setString(3, "%"+nombre+"%");
            st.setString(4, "%"+agencia+"%");
            st.setString(5, "%"+pais+"%");
            rs = st.executeQuery();
            System.out.println("query cliente "+st.toString());
            while(rs.next()){
                BeanCliente = new BeanGeneral();
                BeanCliente.setValor_01 ((rs.getString("estado")!=null)?rs.getString("estado"):"");
                BeanCliente.setValor_02 ((rs.getString("codcli")!=null)?rs.getString("codcli"):"");
                BeanCliente.setValor_03 ((rs.getString("nomcli")!=null)?rs.getString("nomcli"):"");
                BeanCliente.setValor_04 ((rs.getString("notas")!=null)?rs.getString("notas"):"");
                BeanCliente.setValor_05 ((rs.getString("agduenia")!=null)?rs.getString("agduenia"):"");
                BeanCliente.setValor_06 ((rs.getString("base")!=null)?rs.getString("base"):"");
                BeanCliente.setValor_07 ((rs.getString("texto_oc")!=null)?rs.getString("texto_oc"):"");
                BeanCliente.setValor_08 ((rs.getString("nit")!=null)?rs.getString("nit"):"");
                BeanCliente.setValor_09 ((rs.getString("cedagente")!=null)?rs.getString("cedagente"):"");
                BeanCliente.setValor_10 ((rs.getString("rentabilidad")!=null)?rs.getString("rentabilidad"):"");
                BeanCliente.setValor_11 ((rs.getString("soportes_fac")!=null)?rs.getString("soportes_fac"):"");
                BeanCliente.setValor_12 ((rs.getString("fiduciaria")!=null)?rs.getString("fiduciaria"):"");
                BeanCliente.setValor_13 ((rs.getString("dstrct")!=null)?rs.getString("dstrct"):"");
                BeanCliente.setValor_14 ((rs.getString("moneda")!=null)?rs.getString("moneda"):"");
                BeanCliente.setValor_15 ((rs.getString("forma_pago")!=null)?rs.getString("forma_pago"):"");
                BeanCliente.setValor_16 ((rs.getString("plazo")!=null)?rs.getString("plazo"):"");
                BeanCliente.setValor_17 ((rs.getString("zona")!=null)?rs.getString("zona"):"");
                BeanCliente.setValor_18 ((rs.getString("branch_code")!=null)?rs.getString("branch_code"):"");
                BeanCliente.setValor_19 ((rs.getString("bank_account_no")!=null)?rs.getString("bank_account_no"):"");
                BeanCliente.setValor_20 ((rs.getString("cmc")!=null)?rs.getString("cmc"):"");
                BeanCliente.setValor_21 ((rs.getString("unidad")!=null)?rs.getString("unidad"):"");
                BeanCliente.setValor_22 ((rs.getString("codigo_impuesto")!=null)?rs.getString("codigo_impuesto"):"");
                BeanCliente.setValor_23 ((rs.getString("agfacturacion")!=null)?rs.getString("agfacturacion"):"");
                BeanCliente.setValor_24 ((rs.getString("reg_status")!=null)?rs.getString("reg_status"):"");
                BeanCliente.setValor_25 ((rs.getString("creation_date")!=null)?rs.getString("creation_date"):"");
                
                BeanCliente.setValor_26 ((rs.getString ("direccion")!=null)?rs.getString ("direccion"):"");
                BeanCliente.setValor_27 ((rs.getString ("telefono")!=null)?rs.getString ("telefono"):"");
                BeanCliente.setValor_28 ((rs.getString ("nomContacto")!=null)?rs.getString ("nomContacto"):"");
                BeanCliente.setValor_29 ((rs.getString ("telContacto")!=null)?rs.getString ("telContacto"):"");
                BeanCliente.setValor_30((rs.getString ("email_Contacto")!=null)?rs.getString ("email_Contacto"):"");
                BeanCliente.setValor_31((rs.getString ("dir_Factura")!=null)?rs.getString ("dir_Factura"):""); 
                BeanCliente.setValor_32 ((rs.getString ("ma_PreFactura")!=null)?rs.getString ("ma_PreFactura"):"");
                BeanCliente.setValor_33 ((rs.getString ("tiempoPreFac")!=null)?rs.getString ("tiempoPreFac"):"");
                BeanCliente.setValor_34((rs.getString ("tiempo_leg")!=null)?rs.getString ("tiempo_leg"):"");
                BeanCliente.setValor_35((rs.getString ("tiempo_Re_fact")!=null)?rs.getString ("tiempo_Re_fact"):""); 
                BeanCliente.setValor_36((rs.getString ("DiaPago")!=null)?rs.getString ("DiaPago"):""); 
                BeanCliente.setValor_37((rs.getString ("for_facturacion")!=null)?rs.getString ("for_facturacion"):"");   
                BeanCliente.setValor_38(""+cont);   
                BeanCliente.setValor_39((rs.getString ("direccion_contacto")!=null)?rs.getString ("direccion_contacto"):"");   
                BeanCliente.setValor_40((rs.getString ("agcobro")!=null)?rs.getString ("agcobro"):"");    
                BeanCliente.setValor_41((rs.getString ("hc")!=null)?rs.getString ("hc"):"");    
                BeanCliente.setValor_42((rs.getString ("rif")!=null)?rs.getString ("rif"):"");    
                BeanCliente.setValor_43((rs.getString ("ciudadC")!=null)?rs.getString ("ciudadC"):"");    
                BeanCliente.setValor_44((rs.getString ("ciudad_facturaF")!=null)?rs.getString ("ciudad_facturaF"):"");    
                BeanCliente.setValor_45((rs.getString ("paisC")!=null)?rs.getString ("paisC"):"");    
                BeanCliente.setValor_46((rs.getString ("paisEnvio")!=null)?rs.getString ("paisEnvio"):"");    
                cont++;  
                 this.vector.addElement( BeanCliente );

		cont++;
            }
            
            }}catch(Exception e) {
            throw new Exception("Error al Busqueda del  Cliente [ClienteDAO.xml]... \n"+e.getMessage());
        }
finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }




    /**
     *
     * @param codCli
     * @throws Exception
     */
     public void eliminarClienteTemporalmente(String codCli) throws Exception {
         Connection con = null;
         PreparedStatement st = null;
        ResultSet rs = null;
         String query = "SQL_ELIMINAR_CLIENTE_TEMPORAL";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, codCli);
            
            ////System.out.println(st.toString());
            st.executeUpdate();
            }}catch(Exception e) {
            throw new Exception("Error al ELIMINAR Temporalmente Cliente [ClienteDAO.xml]... \n"+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
      /**
       * Getter for property vector.
       * @return Value of property vector.
       */
      public java.util.Vector getVector() {
          return vector;
      }
      
      /**
       * Setter for property vector.
       * @param vector New value of property vector.
       */
      public void setVector(java.util.Vector vector) {
          this.vector = vector;
      }
      
     public TreeMap getCbxCMC(){
        return this.cbxCliente;
    }
    
    public void setCbxCMC(TreeMap CMC){
        this.cbxCliente = CMC;
    }


    /**
     *
     *
     * @throws SQLException
     */
    public void listarCMC() throws SQLException{
        Connection con = null;
        PreparedStatement sttm = null;
        ResultSet rs = null;
        cbxCliente = null;
        cbxCliente = new TreeMap();
        String query = "SQL_LISTAR_CMC";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            sttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = sttm.executeQuery();
            cbxCliente.put("Seleccione Alguna", "NADA");
            while (rs.next()) {
                cbxCliente.put(rs.getString(1)+"-"+rs.getString(2), rs.getString(1));
            }
            }}
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR CMC" + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (sttm  != null){ try{ sttm.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }



    /**
     *
     * @throws SQLException
     */
    public void listarBase() throws SQLException{
        Connection con = null;
        PreparedStatement sttm = null;
        ResultSet rs = null;
        cbxCliente = null;
        cbxCliente = new TreeMap();
        String query = "SQL_LISTAR_BASE";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            sttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = sttm.executeQuery();
            cbxCliente.put("Seleccione Alguna", "NADA");
            while (rs.next()) {
                cbxCliente.put(rs.getString(1)+"-"+rs.getString(2), rs.getString(1));
            }
        }}
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR CMC" + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (sttm  != null){ try{ sttm.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


    /**
     *
     *
     * @throws SQLException
     */
     public void listarUca() throws SQLException{
         Connection con = null;
         PreparedStatement sttm = null;
        ResultSet rs = null;
        cbxCliente = null;
        cbxCliente = new TreeMap();
         String query = "SQL_LISTAR_UCA";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            sttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = sttm.executeQuery();
            cbxCliente.put("Seleccione Alguna", "NADA");
            while (rs.next()) {
                cbxCliente.put(rs.getString(1)+"-"+rs.getString(2), rs.getString(1));
            }
        }}
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR CMC" + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (sttm  != null){ try{ sttm.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
      
     /********************************************************
     Entregado a Fily 12 Febrero 2007
    *********************************************************/
   /**
    *
    * @return
    * @throws Exception
    */
    public String CodCliente() throws Exception { 
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String codigoBD= "";
        String tempCod = "";
        String valorCodigo = "";
        int contador = 0;
        int c = 0;
        int valorTemp = 0;
        String cadena = "";
        String concatenar = "";
        String prefijo = "";
        String query = "SQL_CODIGO_CLIENTE";
       try{
           con = this.conectarJNDI(query);
           if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            if(rs.next()){
               codigoBD=(rs.getString("last_number")!=null)?rs.getString("last_number"):"0";
               prefijo=(rs.getString("prefix")!=null)?rs.getString("prefix"):"0";
            }
            valorTemp = Integer.parseInt(codigoBD);
            if(valorTemp == 99999){
                codigoBD = "final";
                return codigoBD ;
            }
            else {
                cadena = codigoBD;
                valorTemp=codigoBD.length();
                switch(valorTemp)
                {
                    case 1:
                            cadena = "0000"+codigoBD;
                        break;
                    case 2:
                            cadena = "000"+codigoBD;
                       break;
                    case 3:
                            cadena = "00"+codigoBD;
                        break;
                    case 4:
                        cadena = "0"+codigoBD;
                        break;
                    case 5:
                        cadena = codigoBD;
                        break;
                }
                concatenar = prefijo+cadena;
                //System.out.println("concatenacion    "+concatenar);
                
            }
           }}catch(Exception e) {
            throw new Exception("Error al Busqueda del codigo del  Cliente [ClienteDAO.xml]... \n"+e.getMessage());
        }
       finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
       return concatenar;
       
    }

/**
 * Getter for property vectorCod.
 * @return Value of property vectorCod.
 */ 
    public java.util.Vector getVectorCod() {
        return vectorCod;
    }

/**
 * Setter for property vectorCod.
 * @param vectorCod New value of property vectorCod.
 */
    public void setVectorCod(java.util.Vector vectorCod) {
        this.vectorCod = vectorCod;
    }


/**
 *
 * @param codigo
 * @throws Exception
 */
     public void IncrementoDeCodigo(String codigo) throws Exception {
         Connection con = null;
         PreparedStatement st = null;
        ResultSet rs = null;
        String StrCodigo = "";
        String cadenaCod = "";
        cadenaCod = codigo.substring(4,6);
        int  valor = 0;
        valor = Integer.parseInt(cadenaCod);
        valor = valor + 1;
        StrCodigo = ""+valor;
         String query = "SQL_INCREMENTAR_CODIGO_CLIENTE";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.executeUpdate();
            }}catch(Exception e) {
            throw new Exception("Error al ELIMINAR Temporalmente Cliente [ClienteDAO.xml]... \n"+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
     
     
     
     public List listarAgecniaFacturacion() throws Exception{
         Connection con = null;
         PreparedStatement sttm = null;
         ResultSet rs = null;
         AFCliente = null;
         AFCliente = new TreeMap();
         List lista = new LinkedList();
         String query = "SQL_LISTAR_AGENCIA_FACTURACION";
         try {
             con = this.conectarJNDI(query);
             if (con != null) {
                 sttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                 rs = sttm.executeQuery();
                 AFCliente.put("Seleccione Alguna", "NADA");
                 while (rs.next()) {
                     Ciudad c = new Ciudad();
                     c.setCodCiu(rs.getString("table_code"));
                     c.setNomCiu(rs.getString("descripcion"));
                     lista.add(c);
                 }
             }
         }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR AGENCIA DE FACTURACION" + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (sttm  != null){ try{ sttm.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
     
     
     

      public TreeMap getAFcliente(){
        return this.AFCliente;
    }
    
    public void seAFcliente(TreeMap af){
        this.AFCliente = af;
    }
    
    
    
      public String BusquedaCLiente(String agencia ) throws Exception {
          Connection con = null;
          PreparedStatement st = null;
          ResultSet rs = null;
          String cedula = "";
          String query = "SQL_BUSCAR_CEDULA_AGENTE";
          try {
              con = this.conectarJNDI(query);
              if (con != null) {
                  st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                  st.setString(1, agencia);
                  //System.out.println("cedula    "+ st.toString());
                  rs = st.executeQuery();
                  if (rs.next()) {
                      cedula = (rs.getString("descripcion") != null) ? rs.getString("descripcion") : "";

                  }

              }
          }catch(Exception e) {
            throw new Exception("ERROR AL BUSCAR LA CEDULA DEL AGENTE O GERENTE [ClienteDAO.xml]... \n"+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return cedula;
    }


      /**
       *
       * @param var
       * @throws SQLException
       */
      public void searchCliente(String var)throws SQLException{
          Connection con = null;
          PreparedStatement st = null;
        ResultSet rs = null;
        cliente=null;
          String query = "SQL_SEARCH_CLIENTE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, var);
            st.setString(2, var);
            rs = st.executeQuery();
            if(rs.next()){
                cliente = new Cliente();
                cliente.setEstado(rs.getString("estado"));
                cliente.setCodcli(rs.getString("codcli"));
                cliente.setNomcli(rs.getString("nomcli"));
                cliente.setNotas(rs.getString("notas"));
                cliente.setAgduenia(rs.getString("agduenia"));
                cliente.setReg_status(rs.getString("reg_status"));
                cliente.setCreation_date(rs.getString("creation_date"));
                cliente.setLast_update(rs.getString("last_update"));
                cliente.setBase(rs.getString("base"));
                cliente.setTexto_oc(rs.getString("texto_oc"));
                cliente.setRentabilidad(rs.getFloat("rentabilidad"));
                cliente.setAgente(rs.getString("cedagente"));
                cliente.setCmc(rs.getString("cmc"));
                
                //////System.out.println("Agente due�o "+rs.getString("cedagente"));
                //+"-"+rs.getString("nombre")!=null?rs.getString("nombre"):"No se encontro el nombre"
                cliente.setEmail(rs.getString("email"));
            } else {
                cliente = new Cliente(false);
            }
            } }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE UN CLIENTE " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
      
      
      /**
       * 
       * @param codigo
       * @return
       * @throws SQLException
       */
       public String CaragarSoprteCliente(String codigo)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String soportes = "";
        String query = "SQL_SOPORTE_FACTURACION";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, codigo);
            rs = st.executeQuery();
            while (rs.next()) {
                soportes = soportes + "\n" + ((rs.getString("descripcion") != null) ? rs.getString("descripcion") : "");
            }

        }
    }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE SOPORTE DE CLIENTES" + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return soportes;
        
    }
       
 /**
  *
  * @throws Exception
  */
    public void BusquedaSoporteAD() throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        BeanGeneral Bean = null;
        vectorsSoporte = new Vector();
        String query = "SQL_SOPORTE_ADICIONAR_CLIENTE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                while (rs.next()) {
                    Bean = new BeanGeneral();
                    Bean.setValor_01((rs.getString("codigo") != null) ? rs.getString("codigo") : "");
                    Bean.setValor_02((rs.getString("descripcion") != null) ? rs.getString("descripcion") : "");
                    this.vectorsSoporte.addElement(Bean);
                }
            }
        }catch(Exception e) {
            throw new Exception("Error al Busqueda del  SQL_SOPORTE_ADICIONAR_CLIENTE [ClienteDAO.xml]... \n"+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }   
     
    /**
     * Getter for property vectorsSoporte.
     * @return Value of property vectorsSoporte.
     */
    public java.util.Vector getVectorsSoporte() {
        return vectorsSoporte;
    }
    
    /**
     * Setter for property vectorsSoporte.
     * @param vectorsSoporte New value of property vectorsSoporte.
     */
    public void setVectorsSoporte(java.util.Vector vectorsSoporte) {
        this.vectorsSoporte = vectorsSoporte;
    }
    
    
    
    /*funcion para agregar soporte de cliente*/
    
    public void agregarSoporteCliente(String llave,String codigo,String districto)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_INGRESAR_SOPORTE_CLIENTE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, llave);
            st.setString(2, codigo);
            st.setString(3, districto);
            System.out.println("query  agregar soporte  "+st.toString());
            st.executeUpdate();
           
            }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL SQL_INGRESAR_SOPORTE_CLIENTE " + e.getMessage() + " " + e.getErrorCode());
        }
finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    
    /*cargar el soporte de facturacion*/
    //Bellezas
    
 /*   public void cargarSoporte (String [] soportes) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        BeanGeneral Bean    = null;
        vectorsCargaSop = new Vector();
        for(int i = 0 ; i< soportes.length; i++){
              String llave = soportes[i];
               try {
                    ////System.out.println("codigoDao : "+codCli);
                    st = crearPreparedStatement("SQL_BUSQUEDA_SOPORTE");
                    st.setString( 1, llave );
                    rs = st.executeQuery();
                    if(rs.next()){
                        Bean = new BeanGeneral();

                        Bean.setValor_01 ((rs.getString("codigo")!=null)?rs.getString("codigo"):"");
                        Bean.setValor_02 ((rs.getString("descripcion")!=null)?rs.getString("descripcion"):"");

                    }
                    //return BeanCliente;
                }
                catch(Exception e){
                     e.printStackTrace();
                }
                finally {
                    if ( st != null ) { st.close(); }
                    this.desconectar("SQL_BUSQUEDA_SOPORTE");
                }
                this.vectorsCargaSop.addElement( Bean);
         }//cierra for
        
    }*/


            /**
     * Modificado: Ing. Jose Castro
     * @param soportes
     * @throws Exception
     */
    public void cargarSoporte (String [] soportes) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        BeanGeneral Bean    = null;
        vectorsCargaSop = new Vector();
        String query = "SQL_BUSQUEDA_SOPORTE";
        String sql = "";
        
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            sql = this.obtenerSQL(query);
            for (int i = 0; i < soportes.length; i++) {
                String llave = soportes[i];
                ////System.out.println("codigoDao : "+codCli);
                st = con.prepareStatement(sql);
                st.setString(1, llave);
                rs = st.executeQuery();
                if (rs.next()) {
                    Bean = new BeanGeneral();
                    Bean.setValor_01((rs.getString("codigo") != null) ? rs.getString("codigo") : "");
                    Bean.setValor_02((rs.getString("descripcion") != null) ? rs.getString("descripcion") : "");
                }
                //return BeanCliente;
                this.vectorsCargaSop.addElement(Bean);
            }//cierra for
        }catch(Exception e){
           e.printStackTrace();
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

    }


    /**
     * 
     * @param soportes
     * @return
     * @throws Exception
     */
/*  Belleza 2
 public String  cargarSoporteCampoCliente (String [] soportes) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        BeanGeneral Bean    = null;
        vectorsCargaSop = new Vector();
        String descripciones = "";
        String soporte = "";
        for(int i = 0 ; i< soportes.length; i++){
              String llave = soportes[i];
               try {
                    ////System.out.println("codigoDao : "+codCli);
                    st = crearPreparedStatement("SQL_BUSQUEDA_SOPORTE");
                    st.setString( 1, llave );
                    rs = st.executeQuery();
                    if(rs.next()){
                        descripciones = (rs.getString("descripcion")!=null)?rs.getString("descripcion"):"";

                    }
                    soporte = soporte +"\n - "+descripciones;
                }
                catch(Exception e){
                     e.printStackTrace();
                }
                finally {
                    if ( st != null ) { st.close(); }
                    this.desconectar("SQL_BUSQUEDA_SOPORTE");
                }
                this.vectorsCargaSop.addElement( Bean);
         }//cierra for
         return  soporte;
    }*/



         public String  cargarSoporteCampoCliente (String [] soportes) throws Exception {
             Connection con = null;
             PreparedStatement st = null;
             ResultSet rs = null;
             BeanGeneral Bean = null;
             vectorsCargaSop = new Vector();
             String descripciones = "";
             String soporte = "";
             String query = "SQL_BUSQUEDA_SOPORTE";
             String sql  = "";
             try {
                 con = this.conectarJNDI(query); //JJCastro fase2
                 if(con!=null){
                 sql = this.obtenerSQL(query);
                 for (int i = 0; i < soportes.length; i++) {
                     String llave = soportes[i];
                     ////System.out.println("codigoDao : "+codCli);
                     st = con.prepareStatement(sql);//JJCastro fase2
                     st.setString(1, llave);
                     rs = st.executeQuery();
                     if (rs.next()) {
                         descripciones = (rs.getString("descripcion") != null) ? rs.getString("descripcion") : "";

                     }
                     soporte = soporte + "\n - " + descripciones;

                     this.vectorsCargaSop.addElement(Bean);
                 }//cierra for
             }} catch (Exception e) {
                 e.printStackTrace();
             } finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
         return  soporte;
    }



    
    /**
     * Getter for property vectorsCargaSop.
     * @return Value of property vectorsCargaSop.
     */
    public java.util.Vector getVectorsCargaSop() {
        return vectorsCargaSop;
    }    
    
    /**
     * Setter for property vectorsCargaSop.
     * @param vectorsCargaSop New value of property vectorsCargaSop.
     */
    public void setVectorsCargaSop(java.util.Vector vectorsCargaSop) {
        this.vectorsCargaSop = vectorsCargaSop;
    }
    
    /**
     * Getter for property mensaje.
     * @return Value of property mensaje.
     */
    public java.lang.String getMensaje() {
        return mensaje;
    }
    
    /**
     * Setter for property mensaje.
     * @param mensaje New value of property mensaje.
     */
    public void setMensaje(java.lang.String mensaje) {
        this.mensaje = mensaje;
    }
    
        public String agregarClienteFen(BeanGeneral BeanCliente)throws SQLException{
        Connection con=null;
        StringStatement st = null;
        ResultSet rs = null;
        String sql="";
       String query = "SQL_INGRESAR_CLIENTE_FEN";
        try {
           st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, BeanCliente.getValor_01());//nombre cliente
            st.setString(2, BeanCliente.getValor_03());//nit del cliente
            st.setString(3, BeanCliente.getValor_04());//direccion
            st.setString(4, BeanCliente.getValor_05());//telefono
            st.setString(5, BeanCliente.getValor_06());//ciudad
            st.setString(6, BeanCliente.getValor_02());//tipo ident
            st.setString(7, BeanCliente.getValor_07());//celular
            st.setString(8, BeanCliente.getValor_08());//ciudad exp
            st.setString(9, BeanCliente.getValor_09());//rep legal
            st.setString(10, BeanCliente.getValor_10());//cc replegal 
            //st.executeUpdate();
            sql=st.getSql();
           
             }catch(Exception e){
            throw new SQLException("ERROR DURANTE LA INSERCION DEL CLIENTEFenalco " + e.getMessage() );
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sql;
    }

                 /**
       *
       * @param BeanCliente
       * @throws SQLException
       */
      public void UpdateClienteFen(BeanGeneral BeanCliente)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
          String query = "SQL_UPDATE_FEN";       
           
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, BeanCliente.getValor_01());//nombre cliente
            st.setString(2, BeanCliente.getValor_03());//direccion
            st.setString(3, BeanCliente.getValor_04());//telefono
            st.setString(4, BeanCliente.getValor_02());//tipo doc
            st.setString(5, BeanCliente.getValor_05());//ciudad
            st.setString(6, BeanCliente.getValor_07());//tipo ident
            st.setString(7, BeanCliente.getValor_09());//Rep Legal
            st.setString(8, BeanCliente.getValor_10());//cc replegal
            st.setString(9, BeanCliente.getValor_08());//exp cc
            st.setString(10, BeanCliente.getValor_06());//codcliente hidden
            //System.out.println("Query "+st.toString());
            st.executeUpdate();
           
            }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DEL CLIENTE" + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


   /**
    *
    * @param BeanCliente
    * @throws SQLException
    */
    public void DelClienteFen(BeanGeneral BeanCliente)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_DEL_FEN";
           
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, BeanCliente.getValor_03());//USER
            st.setString(2, BeanCliente.getValor_02());//AFILIADO
            st.setString(3, BeanCliente.getValor_01());//CLIENTE
            //System.out.println("Cos "+st.toString());
            st.executeUpdate();
           
            }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ELIMINACION DE CLIENTES " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


    /**
     *
     * @param tab
     * @param camp
     * @param valor
     * @return
     * @throws Exception
     */
     public String  Validar (String tab,String camp,String valor) throws Exception {
         Connection con = null;
         ResultSet rs = null;
         String res = "";
         PreparedStatement ps = null;
         String agregar = "";

         try {
             String sql = this.obtenerSQL("SQL_VALIDAR");
             agregar = sql.replaceAll("#TAB#", tab).replaceAll("#CAMPO#", camp).replaceAll("#VALOR#", valor);
             con = this.conectarJNDI("SQL_VALIDAR");
             if (con != null) {
                 ps = con.prepareStatement(agregar);

                 rs = ps.executeQuery();

                 if (rs.next()) {
                     res = "A";
                 }
             }
         } catch (Exception e) {
             e.printStackTrace();
         }
finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }                
         return  res;
    }



    /**
     * 
     * @param a
     * @param b
     * @return
     * @throws SQLException
     */
    public String Validar_2( String a,String b )throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        cliente = null;
        String res="";
        String query = "SQL_VALIDAR_2";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, a);
            st.setString(2, b);
            //System.out.println("Queryy--"+st.toString());
            rs = st.executeQuery();
            if(rs.next()){
               res="A";
            }
            }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL NIT CLIENTE AFILIADO " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return res;
    }

    /**
     * 
     * @param a
     * @param b
     * @return
     * @throws SQLException
     */
    public String Validar_3( String a,String b )throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        cliente = null;
        String res="";
        String query = "SQL_VALIDAR_3";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, a);
            st.setString(2, b);
            //System.out.println("Queryy-3 "+st.toString());
            rs = st.executeQuery();
            if(rs.next()){
               res="A";
            }
            }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA VALIDACION DEL NIT CLIENTE AFILIADO " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return res;
    }



    /**
     *
     * @param nombre
     * @param af
     * @throws SQLException
     */
     public void consultarClienteFen(String nombre,String af) throws SQLException {
        Connection con = null;
         PreparedStatement st = null;
        ResultSet rs = null;
        clientesHenry=null;
         String query = "SQL_CONSULTAR_CLIENTE_FEN";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, nombre.toUpperCase() );
            st.setString( 2, af );
            rs = st.executeQuery();
            clientesHenry = new Vector();
            Cliente cl = new Cliente();
            while(rs.next()){
                Cliente c = new Cliente();
                c.setCodcli((rs.getString("codcli")!=null)?rs.getString("codcli"):"");
                c.setNomcli((rs.getString("nomcli")!=null)?rs.getString("nomcli"):"");
                c.setNit   ((rs.getString("nit")!=null)?rs.getString("nit"):"");
                clientesHenry.addElement(c);
                c=null;
            }
            }}
        catch(Exception e){
            throw new SQLException(
            "ERROR DURANTE EL PROCESO DE LISTAR LOS CLIENTES POR " +
            e.getMessage() + " " /*+ e.getErrorCode()*/ );
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }


     /**
      *
      * @param BeanCliente
      * @throws SQLException
      */
    public void agregarNitFen(BeanGeneral BeanCliente)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_INSERTAR_NIT_FEN";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, BeanCliente.getValor_01());//nombre
            st.setString(2, BeanCliente.getValor_02());//tipoid
            st.setString(3, BeanCliente.getValor_03());//cedula
            st.setString(4, BeanCliente.getValor_04());//direccion
            st.setString(5, BeanCliente.getValor_05());//telefono
            st.setString(6, BeanCliente.getValor_13());//base
            st.setString(7, BeanCliente.getValor_11());//usario crea
            st.setString(8, BeanCliente.getValor_09());//expecedula
            st.setString(9, BeanCliente.getValor_10());//ciudad
            st.setString(10, BeanCliente.getValor_10());
            st.setString(11, BeanCliente.getValor_10());
            st.executeUpdate();
           
            }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DEL nit Fenalco " + e.getMessage() + " " + e.getErrorCode());
        }
finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    public void consultarClienteAll(String af) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        clientesHenry=null;
        String query = "SQL_CONSULTAR_CLIENTES_ALL";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, af );
            //System.out.println("Q-1 "+st.toString());
            rs = st.executeQuery();
            clientesHenry = new Vector();
            Cliente cl = new Cliente();
            while(rs.next()){
                Cliente c = new Cliente();
                c.setCodcli((rs.getString("codcli")!=null)?rs.getString("codcli"):"");
                c.setNomcli((rs.getString("nomcli")!=null)?rs.getString("nomcli"):"");
                c.setNit   ((rs.getString("nit")!=null)?rs.getString("nit"):"");
                clientesHenry.addElement(c);
                c=null;
            }
            } }
        catch(Exception e){
            throw new SQLException(
            "ERROR DURANTE EL PROCESO DE LISTAR LOS CLIENTES POR AFILIADO FEN " +
            e.getMessage() + " " /*+ e.getErrorCode()*/ );
        }
finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }        
    }
    
     public void ClientesList(String nombre) throws SQLException {
         Connection con = null;
         PreparedStatement st = null;
        ResultSet rs = null;
        clientesHenry=null;
         String query = "SQL_CONSULTAR_CLIENTES_ALL_2";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, nombre);
            rs = st.executeQuery();
            //System.out.println("Q-2 "+st.toString());
            clientesHenry = new Vector();
            BeanGeneral cl = new BeanGeneral();
            while(rs.next()){
                BeanGeneral c = new BeanGeneral();
                    c.setValor_01((rs.getString("codcli")!=null)?rs.getString("codcli"):"");
                    c.setValor_02((rs.getString("nomcli")!=null)?rs.getString("nomcli"):"");
                    c.setValor_03((rs.getString("nit")!=null)?rs.getString("nit"):"");
                    c.setValor_04((rs.getString("direccion")!=null)?rs.getString("direccion"):"");
                    c.setValor_05((rs.getString("telefono")!=null)?rs.getString("telefono"):"");
                    c.setValor_09((rs.getString(6)!=null)?rs.getString(6):"");
                    c.setValor_10((rs.getString("rif")!=null)?rs.getString("rif"):"");
                    c.setValor_06((rs.getString(8)!=null)?rs.getString(8):"");
                    c.setValor_07((rs.getString(9)!=null)?rs.getString(9):"");
                    
                    c.setValor_11((rs.getString(10)!=null)?rs.getString(10):"");
                    c.setValor_12((rs.getString(11)!=null)?rs.getString(11):"");
                    c.setValor_13((rs.getString(12)!=null)?rs.getString(12):"");
                    c.setValor_14((rs.getString(13)!=null)?rs.getString(13):"");
                    
                clientesHenry.addElement(c);
                c=null;
            }}
        }
        catch(Exception e){
            throw new SQLException(
            "ERROR DURANTE EL PROCESO DE LISTAR LOS CLIENTES POR " +
            e.getMessage() + " " );
        }
finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }



     /**
      *
      * @param bg
      * @throws Exception
      */
     
      public void UPrel(BeanGeneral bg)throws Exception{
          Connection con = null;
          PreparedStatement st = null;
          String query = "SQL_UP_AFIL";

            try
            {
                con = this.conectarJNDI(query);
                if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, bg.getValor_03());  
                st.setString(2, bg.getValor_01());
                st.setString(3, bg.getValor_02());
                st.executeUpdate();
             }}
            catch(Exception e)
            {
                 throw new Exception(e.getMessage()); 
            }      
            finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        }



     /**
      *
      * @param cd
      * @throws Exception
      */
    public void add_codeudor(Codeudor cd) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        String query = "INSERT_CODEUDOR";
            try
            {
                con = this.conectarJNDI(query);
                if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cd.getTipo_id());
                st.setString(2, cd.getId());
                st.setString(3, cd.getNombre());
                st.setString(4, cd.getExpedicion_id());
                st.setString(5, cd.getCel());
                st.setString(6, cd.getDireccion());
                st.setString(7, cd.getCiudad());
                st.setString(8, cd.getTelefono());
                st.setString(9, cd.getUniversidad());
                st.setString(10, cd.getCarrera());
                st.setString(11, cd.getSemestre());
                st.setString(12, cd.getCod());
                st.setString(13, cd.getCreation_user());
                //System.out.println(st.toString());
                st.execute();
             }}
            catch(Exception e)
            {   e.printStackTrace();
                System.out.println(e.getMessage());
                throw new Exception(e.getMessage());
            }
           finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Consulta si existe un cliente de acuerdo al nit
     * @param nit
     * @return true si existe o false si no
     * @throws Exception 
     */
    public boolean existeClienteXNit(String nit) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "BUSCAR_NIT";
        boolean existe = false;
        
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, nit);
            rs = st.executeQuery();
            if (rs.next()) {
                existe = true;
            }
            
        } catch (Exception e) {
            throw new Exception("Error en existeClienteXNit[ClienteDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return existe;
    }
    
    
     /**
    *
    * @param BeanCliente
    * @throws SQLException
    */
    public BeanGeneral buscarClienteXnit(String nit)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_CLIENTE_NIT";
        BeanGeneral listaCliente=null;

      
        try {
            con = this.conectarJNDI(query);
            
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, nit);
                
                rs=st.executeQuery();
           
                if (rs.next()) {
                    listaCliente=new BeanGeneral();
                    listaCliente.setValor_01(rs.getString("codcli"));
                    listaCliente.setValor_02(rs.getString("nomcli"));
                    listaCliente.setValor_03(rs.getString("nit"));
                    listaCliente.setValor_04(rs.getString("direccion"));
                    listaCliente.setValor_05(rs.getString("telefono"));  
                    listaCliente.setValor_06(rs.getString("celular"));
                    listaCliente.setValor_07(rs.getString("barrio"));
                    listaCliente.setValor_08(rs.getString("codciu"));
                    listaCliente.setValor_09(rs.getString("coddpto"));
                    listaCliente.setValor_55(rs.getString("observaciones"));
                    listaCliente.setValor_54(rs.getString("e_mail"));
                    
                }



            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE LA ELIMINACION DE CLIENTES " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return listaCliente;
    }
    
       public boolean actualizarClienteXnit(String nit, String codigo, String name, String direccion, String telefono, String celular,String barrio,String codciu,String cooddpto,String email, String observaciones, Usuario user) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        boolean status = false;
        String query = "ACTUALIZAR_CLIENTE_NIT";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, name);
                st.setString(2, direccion);
                st.setString(3, telefono);
                st.setString(4, celular);
                st.setString(5, codciu);
                st.setString(6, user.getLogin());
                st.setString(7, observaciones);
                st.setString(8, email);
                st.setString(9, nit);
                st.setString(10, codigo);
                
                
                //System.out.println(st.toString());
                st.executeUpdate();
                status = actualizarClienteXnit2(nit,codigo,name,direccion,telefono,celular,barrio, codciu, cooddpto, email,observaciones, user);                    
                
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return status;
    }
       
       
     public boolean actualizarClienteXnit2(String nit, String codigo, String name, String direccion, String telefono, String celular,String barrio,String codciu,String cooddpto,String email, String observaciones, Usuario user) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        boolean status = false;
        String query = "ACTUALIZAR_CLIENTE_NIT_2";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, name);
                st.setString(2, direccion);
                st.setString(3, telefono);
                st.setString(4, celular);
                st.setString(5, barrio);
                st.setString(6, codciu);
                st.setString(7, cooddpto);
                st.setString(8, user.getLogin());
                st.setString(9, observaciones);
                st.setString(10, email);
                st.setString(11, nit);
                

                //System.out.println(st.toString());
                int i = st.executeUpdate();

                if (i > 0) {
                    status = true;
                    
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return status;
    } 

    public String bucarNegocios(String nit) {
      JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
         String query = "CARGAR_NEGOCIOS_CLIENTE";
        try {
             con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nit);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {
                
                fila = new JsonObject();
                //sa.cod_neg,sp.numero_solicitud,sp.extracto_email,sp.extracto_correspondecia
                fila.addProperty("cod_neg", rs.getString("cod_neg"));              
                fila.addProperty("numero_solicitud", rs.getString("numero_solicitud"));
                fila.addProperty("extracto_email", rs.getString("extracto_email"));
                fila.addProperty("extracto_correspondecia", rs.getString("extracto_correspondecia"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
           e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj); 
    }

    public boolean actualizarSolicitud(String num_sol, String ext_email, String ext_corr,String nit) throws SQLException, Exception {
      Connection con = null;
        PreparedStatement st = null;
        boolean resp = true;
        String query = "ACTUALIZAR_SOLICITUD_PERSONA";
        
        boolean em = Boolean.parseBoolean(ext_email);
        boolean ec = Boolean.parseBoolean(ext_corr);
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setBoolean(1,em);
                st.setBoolean(2, ec);
                st.setString(3, num_sol);
                st.setString(4, nit);
                
                st.executeUpdate();                  
                
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return resp;
    }
    
    
    public JsonObject bucarEmail(String email) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;        
        String query = "BUSCAR_EMAIL";
        JsonObject data = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, email);
            rs = ps.executeQuery();
            data.addProperty("existe", false);
                     
            while (rs.next()) {
                data = new JsonObject();
                data.addProperty("existe", true);
                data.addProperty("nit", rs.getString("nit"));
                
            }
                          
            
        } catch (Exception e) {
           e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
       return data; 
    } 
     
    

}


