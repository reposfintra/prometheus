/*
 * ReportesPtoDAO.java
 *
 * Created on 31 de julio de 2005, 10:49
 */

package com.tsp.operation.model.DAOS;

import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.UtilFinanzas;
/**
 *
 * @author  mario
 */
public class ReportesDAO  extends MainDAO{
    
    private final String SQL_VENTAS_PT = 
          " select agencia, cliente, std_job_no, cdia01, cdia02, cdia03, cdia04, cdia05, cdia06, cdia07, cdia08, cdia09, cdia10, cdia11, cdia12, cdia13, cdia14, cdia15, cdia16, cdia17, cdia18, cdia19, cdia20, cdia21, cdia22, cdia23, cdia24, cdia25, cdia26, cdia27, cdia28, cdia29, cdia30, cdia31 from fin.pto_ventas where ano = ? and mes = ? and estado = '' and dstrct_code like ? and agencia like ? and cliente like ? and std_job_no like ? order by agencia, cliente, std_job_no ";
    
    private final String SQL_VENTAS_EJ = 
          " select b.agduenia, c.nomciu, a.cliente, b.nomcli, a.standar, a.dia, a.viajes" +
          " from                                              " +
          "     (select                                       " +
          "	   ('000'||substr(std_job_no,1,3)) as cliente," +
          "	   std_job_no as standar,                     " +
          "	   fecrem   as fecha,                         " +
          "        substr   (fecrem , 9, 2) as dia,           " +
          "	   count    (*)             as viajes         " +
          "	 from                                         " +
          "	   remesa                                     " +
          "	 where                                        " +
          "	   fecrem between ? and ?                     " +
          "	 group by                                     " +
          "	   std_job_no, fecrem                         " +
          "     ) a,                                          " +
          "     cliente b,                                    " +
          "     ciudad  c                                     " +
          " where                                             " +
          "     b.codcli = a.cliente                          " +
          " and c.codciu  = b.agduenia                        " +
          " and b.agduenia like ?                             " +
          " and a.cliente  like ?                             " +
          " and a.standar  like ?                             " +
          " order by a.dia, b.agduenia , a.cliente, a.standar ";
    
    private final String SQL_STANDAR_GRAL = " select ref_code from msf071 where entity_type = '+SJ' and entity_value = ? and ref_no = '010'";
    
    
    private final String SQL_CIUDAD  = " select nomciu       from ciudad  where codciu     = ? ";
    private final String SQL_CLIENTE = " select nomcli       from cliente where codcli     = ? ";
    private final String SQL_STANDAR = " select std_job_desc from stdjob  where std_job_no = ? ";
    
    
    /*Costos Operativos*/
    
    private final String SQL_COSTOS_OPERATIVOS =    "   SELECT std_job_no, ano, mes, elemento, valor    "+
                                                    "   FROM fin.pto_costos_operativos                      "+
                                                    "   WHERE std_job_no = ?  AND ano = ? AND mes =  ?  ";
    
    private final String SQL_TOTAL_COSTOS   =       "  SELECT SUM(valor)as total FROM fin.pto_costos_operativos "+
                                                    "  WHERE std_job_no = ? AND ano = ? AND mes =  ?        ";
    
    private final String SQL_TOTAL_CLIENTE =        "   SELECT SUM(ptc.valor*pt.cmensual)AS valor FROM fin.pto_ventas pt                        "+
                                                    "   LEFT OUTER JOIN fin.pto_costos_operativos ptc ON ( ptc.std_job_no = pt.std_job_no )     "+
                                                    "   WHERE pt.cliente = ? AND pt.ano = ? AND pt.mes = ?                                  ";    
    
    private final String SQL_TOTAL_AGENCIA =        "   SELECT SUM(ptc.valor*pt.cmensual)AS valor FROM fin.pto_ventas pt                        "+
                                                    "   LEFT OUTER JOIN fin.pto_costos_operativos ptc ON ( ptc.std_job_no = pt.std_job_no )     "+
                                                    "   WHERE pt.agencia LIKE ? AND pt.ano = ? AND pt.mes = ?                               ";
    
      private final String SQL_VENTAS = 
        " select                " +
        "   dstrct_code,        " +      
        "   std_job_no,         " +
        "   cmensual,           " +
        "   cdia01, cdia02, cdia03, cdia04 ,cdia05 ,cdia06 ,cdia07 ,cdia08 ,cdia09, cdia10," +
        "   cdia11, cdia12, cdia13, cdia14 ,cdia15 ,cdia16 ,cdia17 ,cdia18 ,cdia19, cdia20," +
        "   cdia21, cdia22, cdia23, cdia24 ,cdia25 ,cdia26 ,cdia27 ,cdia28 ,cdia29, cdia30," +
        "   cdia31              " +
        " from                  " +
        "   fin.pto_ventas          " +
        " where                 " +
        "       ano || mes = ?  " +
        // filtro
        //"   and agencia <> 'CG' and cliente not in ('000734','000369') "+
        "   and estado =  ''    " ; 
    
    private final String SQL_COMPLEMENTO_STD_JOB =
        "select                                                 " +
        "        a.std_job_desc,                                " +
        "        b.codciu,                                      " +
        "        b.nomciu,                                      " +
        "        c.codciu,                                      " +
        "        c.nomciu,                                      " +
        "        d.codcli,                                      " +
        "        d.nomcli,                                      " +
        "        e.codciu,                                      " +
        "        e.nomciu,                                      " +
        "        a.unit_of_work,                                " +
        "        a.maint_type,                                  " +
        "        a.recurso1,                                    " +
        "        a.recurso2,                                    " +
        "        a.recurso3,                                    " +
        "        a.recurso4,                                    " +
        "        a.recurso5,                                    " +
        "        a.vlr_freight,                                 " +
        "        a.currency                                     " +
        " from                                                  " +
        "      stdjob  a,                                       " +
        "      ciudad  b,                                       " +
        "      ciudad  c,                                       " +
        "      cliente d,                                       " +
        "      ciudad  e                                        " +
        " where                                                 " +
        "          a.dstrct_code = ?                            " +
        "      and a.std_job_no  = ?                            " +
        "      and b.codciu = a.origin_code                     " +
        "      and c.codciu = a.destination_code                " +
        "      and d.codcli = '000' || substr(a.std_job_no,1,3) " +
        "      and e.codciu = d.agduenia                        " ;    
    
    private final String SQL_TASA = " SELECT DOLAR, BOLIVAR, DTF FROM FIN.PTO_TASA WHERE ANO = ? AND MES = ? " ;
    
    private final String SQL_TAR_MON    = " SELECT SUBSTR(B.ASSOC_REC,1,11)/10000, SUBSTR(B.ASSOC_REC,16,3)  " +
                                          " FROM MSF010 B  WHERE B.TABLE_TYPE='UW' AND B.TABLE_CODE = ?      ";

    /** Creates a new instance of ReportesPtoDAO */
    public ReportesDAO() {
        super("ReportesDAO.xml");
    }
    

 /**
  * 
  * @param Distrito
  * @param Agencia
  * @param Cliente
  * @param Estandar
  * @param Ano
  * @param Mes
  * @return
  * @throws Exception
  */
    public List loadViajesPt (String Distrito, String Agencia, String Cliente, String Estandar, String Ano , String Mes) throws Exception{
        Connection con = null;
        PreparedStatement st  = null;                                
        ResultSet         rs  = null;        
        List viajes = new LinkedList();
        String query = "SQL_VENTAS_PT";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, Ano);
                st.setString(2, Mes);
                st.setString(3, (Distrito.equals("")?"%":Distrito));
                st.setString(4, (Agencia.equals("") ?"%":Agencia ));
                st.setString(5, (Cliente.equals("") ?"%":Cliente ));
                st.setString(6, (Estandar.equals("")?"%":Estandar));
                ////System.out.println("SQL: " + st.toString());
                rs = st.executeQuery();
                
                
                ViajesAgencia ag  = null;
                ViajesCliente cl  = null;
                ViajesStandar std = null;
                
                while(rs.next()){
                    
                    std = new ViajesStandar();
                    std.setStdJobNo   (rs.getString(3));
                    std.setStdJobDesc (getDescripcion(3, con, std.getStdJobNo()));
                    for (int i=1 ; i<=31 ; i++) std.setViajePtdo( i , rs.getInt(i+3));
                    
                    if (cl == null || !cl.getCliente().equals(rs.getString(2))){
                        if (cl!=null)  ag.addCliente(cl);
                        cl = new ViajesCliente();
                        cl.setCliente      (rs.getString(2));
                        cl.setClienteNombre(getDescripcion(2, con, cl.getCliente()));
                    }                    
                    cl.addStandar(std);
                    
                    if (ag == null || !ag.getAgencia().equals(rs.getString(1))) {
                        if (ag!=null) viajes.add(ag);
                        
                        ag = new ViajesAgencia();
                        ag.setAgencia      (rs.getString(1));
                        ag.setAgenciaNombre(getDescripcion(1, con, ag.getAgencia()));
                    }
                }
                if (ag != null) {
                    ViajesCliente ult = (ag.getListadoClientes().size()>0?
                                            (ViajesCliente) ag.getListadoClientes().get(ag.getListadoClientes().size()-1):
                                            null);
                    if (cl!=null && (ult==null || !ult.getCliente().equals(cl.getCliente())) ) ag.addCliente(cl);
                    viajes.add(ag);
                }
                
        }}
        catch(Exception e) {
            throw new SQLException("Error en rutina loadViajesPt [ReportesDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return viajes;
    }




 /**
  * 
  * @param Distrito
  * @param Agencia
  * @param Cliente
  * @param Estandar
  * @param Ano
  * @param Mes
  * @param lista
  * @return
  * @throws Exception
  */
    public List loadViajesEj (String Distrito, String Agencia, String Cliente, String Estandar, String Ano , String Mes, List lista) throws Exception{
        Connection con = null;
        PreparedStatement st  = null;                                
        ResultSet         rs  = null;
        
        Hashtable  listaTmp = new Hashtable();
        String query = "SQL_VENTAS_EJ";


        if (lista == null)
            lista = new LinkedList();
        
        Connection        conPostgres = this.conectarBDJNDI("fintra");
        //Connection        conOracle   = poolManager.getConnection("oracle");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        
        try {
                st = conPostgres.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, Ano + Mes + "01" );
                st.setString(2, Ano + Mes + UtilFinanzas.diaFinal(Ano + Mes));
                //st.setString(3, (Distrito.equals("")?"%":Distrito));
                st.setString(3, (Agencia.equals("") ?"%":Agencia ));
                st.setString(4, (Cliente.equals("") ?"%":Cliente ));
                st.setString(5, (Estandar.equals("")?"%":Estandar));
                rs = st.executeQuery();
                
                while(rs.next()){
                    String vAgencia       = rs.getString(1);
                    String vAgenciaNombre = rs.getString(2);
                    String vCliente       = rs.getString(3);
                    String vClienteNombre = rs.getString(4);
                    String vStandarEsp    = rs.getString(5);
                    int Dia    = rs.getInt(6);
                    int Viajes = rs.getInt(7);
                    
                    
                    String vStandarGral = "";
                    String std = (String) listaTmp.get(vStandarEsp);
                    
                    
                    if (std!=null && !std.equals("null")) vStandarGral = std;
                    else  {
                       // vStandarGral = getStandarGral(conOracle, vStandarEsp);
                        listaTmp.put(vStandarEsp, vStandarGral);
                    }
                    
                    if (!vStandarGral.equals("") && !vStandarGral.equals("CASUAL")){
                        if (! Actualizar(lista, vAgencia, vCliente, vStandarGral,  Dia, Viajes)){
                            String vDescripcion = this.getDescripcion(3, conPostgres, vStandarGral);
                            Insertar(lista, vAgencia, vAgenciaNombre, vCliente, vClienteNombre, vStandarGral , vDescripcion, Dia, Viajes);
                        }
                    }
                }
                
        }
        catch(Exception e) {
            throw new SQLException("Error en rutina loadViajesEj [ReportesDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
   
/**
 * 
 * @param lista
 * @param Agencia
 * @param Cliente
 * @param Standar
 * @param Dia
 * @param Viajes
 * @return
 */
    private boolean Actualizar (List lista, String Agencia, String Cliente, String Standar, int Dia , int Viajes){
        boolean sw= false;
        if (lista!=null){
            /////////////////////////////////////////////////////////////////////////////////
            for(int i = 0; i<lista.size() && !sw; i++){
                ViajesAgencia ag = (ViajesAgencia) lista.get(i);
                if (Agencia.equals(ag.getAgencia())){
                    //////////////////////////////////////////////////////////////////////////
                    for(int j = 0; j< ag.getListadoClientes().size() && !sw; j++){
                        ViajesCliente cl = (ViajesCliente) ag.getListadoClientes().get(j);
                        if (Cliente.equals(cl.getCliente())){
                        //////////////////////////////////////////////////////////////////////////
                        for(int k = 0; k< cl.getListaStandar().size() && !sw; k++){
                            ViajesStandar st = (ViajesStandar) cl.getListaStandar().get(k);
                            if (Standar.equals(st.getStdJobNo())){
                                sw = true;
                                st.setViajeEjdo(Dia, st.getViajeEjdo(Dia) + Viajes);
                            }
                        }
                        //////////////////////////////////////////////////////////////////////////
                        }
                    }
                    //////////////////////////////////////////////////////////////////////////
                }
            }
            /////////////////////////////////////////////////////////////////////////////////
        }
        return sw;
    }




  /**
   *
   * @param lista
   * @param Agencia
   * @param AgenciaNombre
   * @param Cliente
   * @param ClienteNombre
   * @param Standar
   * @param Descripcion
   * @param Dia
   * @param Viajes
   */
    private void Insertar (List lista, String Agencia, String AgenciaNombre, String Cliente, String ClienteNombre, String Standar, String Descripcion, int Dia , int Viajes){
        
        ViajesStandar std = new ViajesStandar();
        
        std.setStdJobNo   (Standar);
        std.setStdJobDesc (Descripcion);
        std.setViajeEjdo  (Dia, Viajes);
        
        boolean sw = false;
        
        if (lista!=null){
            /////////////////////////////////////////////////////////////////////////////////
            for(int i = 0; i<lista.size() && !sw ; i++){
                ViajesAgencia ag = (ViajesAgencia) lista.get(i);
                if (Agencia.equals(ag.getAgencia())){
                    //////////////////////////////////////////////////////////////////////////
                    for(int j = 0; j< ag.getListadoClientes().size() && !sw ; j++){
                        ViajesCliente cl = (ViajesCliente) ag.getListadoClientes().get(j);
                        if (Cliente.equals(cl.getCliente())){
                            cl.addStandar(std);
                            sw = true;
                        }
                    }
                    //////////////////////////////////////////////////////////////////////////
                    if (!sw){
                        ViajesCliente vc = new ViajesCliente();
                        vc.setCliente(Cliente);
                        vc.setClienteNombre(ClienteNombre);
                        vc.addStandar(std);
                        ag.addCliente(vc);
                        sw = true;
                    }
                }
            }
            /////////////////////////////////////////////////////////////////////////////////
            if (!sw){
                ViajesCliente vc = new ViajesCliente();
                vc.setCliente(Cliente);
                vc.setClienteNombre(ClienteNombre);
                vc.addStandar(std);
                ViajesAgencia va = new ViajesAgencia();
                va.setAgencia(Agencia);
                va.setAgenciaNombre(AgenciaNombre);
                va.addCliente(vc);
                sw = true;
                lista.add(va);
            }
            
        }
    }
    
/**
 * 
 * @param con
 * @param Standar
 * @return
 * @throws Exception
 */
    private String getStandarGral (Connection con, String Standar) throws Exception{
        ResultSet rs = null;
        PreparedStatement st = null;
        String StandarGral = "";
        String query = "SQL_STANDAR_GRAL";
        try{
        if(con != null ){
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, "2TSP " + Standar);
            rs = st.executeQuery();
            while(rs.next()){
                StandarGral = rs.getString(1);
                break;
            }
         }} catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_CLENTE \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
       return StandarGral;

}
    
    
 /**
  * 
  * @param tipo
  * @param con
  * @param Codigo
  * @return
  * @throws Exception
  */
    private String getDescripcion (int tipo, Connection con, String Codigo) throws Exception{
        String Descripcion = "";
        PreparedStatement st = null;
        ResultSet rs = null;
        String query1 = this.obtenerSQL("SQL_CIUDAD");
        String query2 = this.obtenerSQL("SQL_CLIENTE");
        String query3 = this.obtenerSQL("SQL_STANDAR");
        String consulta = "";
        try {
         consulta =   (tipo == 1 ? query1: tipo == 2 ? query2: tipo == 3 ? query3 : "");

         if (con != null) {
                st = con.prepareStatement(consulta);
                st.setString(1, Codigo);
                rs = st.executeQuery();
                while (rs.next()) {
                    Descripcion = rs.getString(1);
                    break;
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_CLENTE \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return Descripcion;
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //  DESCARGAR SOLO PRESUPUESTO A EXCEL
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *
 * @param Ano
 * @param Mes
 * @return
 * @throws Exception
 */
    public TreeMap BuscarVentas (String Ano , String Mes) throws Exception{
        
        PreparedStatement st  = null;                                
        ResultSet         rs  = null;
        TreeMap datos = new TreeMap();
        List    viajes = new LinkedList();
        TreeMap tasas  = new TreeMap();
        String query = "SQL_VENTAS";
   
        Connection        conPostgres = this.conectarJNDI(query);//JJCastro fase2
        //Connection        conOracle   = poolManager.getConnection("oracle");
  
        try {
                st = conPostgres.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, Ano + Mes);
                rs = st.executeQuery();
                
                tasas = getTasa(Ano, Mes, conPostgres);
                
                while(rs.next()){
                    DatosView dt = new DatosView();
                    dt.addValor( rs.getString(1), "DISTRITO"      );
                    dt.addValor( rs.getString(2), "STDJOBNO"      );
                    dt.addValor( rs.getString(3), "TOTAL"         );
                    for (int i=0;i<31;i++) dt.addValor(rs.getString(i+4), "V" + i );
                    
                    dt = getComplemento  (dt, conPostgres);
                    //dt = getTarifaMoneda (dt, conOracle  );
                    
                    dt.addValor( 
                        String.valueOf(
                           Double.parseDouble(dt.getValor("TOTAL"))  *  
                           Double.parseDouble(dt.getValor("TARIFA")) * 
                           Double.parseDouble(tasas.get(dt.getValor("MONEDA")).toString()) 
                        ), "TOTALPTO");
                    
                    viajes.add(dt);
                    dt = null;//Liberar Espacio JJCastro
                }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarVentas [ReportesDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conPostgres != null){ try{ this.desconectar(conPostgres); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        datos.put("viajes", viajes);
        datos.put("tasas" , tasas );
        
        return datos;
    }



/**
 * 
 * @param dt
 * @param con
 * @return
 * @throws Exception
 */
    private DatosView getComplemento (DatosView dt, Connection con) throws Exception{
        ResultSet rs = null;
        PreparedStatement st = null;
        try{

            String query = "SQL_COMPLEMENTO_STD_JOB";//JJCastro fase2
            dt.addValor( "" , "STDJOBDESC"    );
            dt.addValor( "" , "CODIGOORIGEN"  );
            dt.addValor( "" , "NOMBREORIGEN"  );
            dt.addValor( "" , "CODIGODESTINO" );
            dt.addValor( "" , "NOMBREDESTINO" );
            dt.addValor( "" , "CODIGOCLIENTE" );
            dt.addValor( "" , "NOMBRECLIENTE" );
            dt.addValor( "" , "CODIGOAGENCIA" );
            dt.addValor( "" , "NOMBREAGENCIA" );
            dt.addValor( "" , "UW"            );
            dt.addValor( "" , "TIPOVIAJE"     );  
            dt.addValor( "" ,  "RECURSO"  );
            dt.addValor( "0", "TARIFA"         );
            dt.addValor( "" , "MONEDA"         );            
            
            if(con != null ){
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dt.getValor("DISTRITO"));
                st.setString(2, dt.getValor("STDJOBNO"));
                rs = st.executeQuery();
                while(rs.next()){
                    dt.addValor( rs.getString(1) , "STDJOBDESC"    );
                    dt.addValor( rs.getString(2) , "CODIGOORIGEN"  );
                    dt.addValor( rs.getString(3) , "NOMBREORIGEN"  );
                    dt.addValor( rs.getString(4) , "CODIGODESTINO" );
                    dt.addValor( rs.getString(5) , "NOMBREDESTINO" );
                    dt.addValor( rs.getString(6) , "CODIGOCLIENTE" );
                    dt.addValor( rs.getString(7) , "NOMBRECLIENTE" );
                    dt.addValor( rs.getString(8) , "CODIGOAGENCIA" );
                    dt.addValor( rs.getString(9) , "NOMBREAGENCIA" );
                    dt.addValor( rs.getString(10), "UW"            );
                    dt.addValor( rs.getString(11), "TIPOVIAJE"     );
                    
                    String recurso = rs.getString(12);
                    for (int k=13;k<=16;k++) recurso += (!recurso.equals("") && !rs.getString(k).equals("") ? "; " + rs.getString(k): "");
                    dt.addValor(recurso,  "RECURSO"  );
                    dt.addValor( rs.getString(17), "TARIFA"         );
                    dt.addValor( rs.getString(18), "MONEDA"         );
                    
                    
                    
                    break;
                }
                
            }
        }catch (Exception ex){
            throw new Exception("Error en getComplemento [ReportesDAO] ...\n" + ex.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            
        }
        return dt;
    }



/**
 * 
 * @param Ano
 * @param Mes
 * @param con
 * @return
 * @throws Exception
 */
    private TreeMap getTasa (String Ano, String Mes, Connection con) throws Exception{
        TreeMap tasas = new TreeMap();
        tasas.put("DOL", "0");
        tasas.put("BOL", "0");
        tasas.put("DTF", "0");
        tasas.put("PES", "1");
        tasas.put("", "0");
        String query = "SQL_TASA";
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try{
            if(con != null ){
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, Ano);
                st.setString(2, Mes);
                rs = st.executeQuery();
                while(rs.next()){
                    tasas.put("DOL", rs.getString(1));
                    tasas.put("BOL", rs.getString(2));
                    tasas.put("DTF", rs.getString(3));
                    break;
                }
            }
        }catch (Exception ex){
            throw new Exception("Error en getComplemento [getTasa] ...\n" + ex.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return tasas;
    } 



/**
 *
 * @param datos
 * @param conOracle
 * @return
 * @throws Exception
 */
    public DatosView getTarifaMoneda(DatosView datos, Connection conOracle) throws Exception{        
        //PoolManager       poolManager = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        boolean           liberar     = false;
        if (conOracle==null){
            //poolManager = PoolManager.getInstance();
            //conOracle   = poolManager.getConnection("oracle");
            liberar     = true;
        }
        if (conOracle == null)
            throw new SQLException("Sin conexion");
        try{
            datos.addValor("0","TARIFA"); //tarifa
            datos.addValor("" ,"MONEDA"); // moneda
            st = conOracle.prepareStatement(this.SQL_TAR_MON);
            st.setString(1, datos.getValor("UW"));
            rs = st.executeQuery();
            while(rs.next()){
                datos.addValor(rs.getString(1),"TARIFA"); //tarifa
                datos.addValor(rs.getString(2),"MONEDA"); // moneda
                break;
            }
        }catch(Exception e){
            throw new Exception("Error en la rutina BuscarTarifaMoneda [ReportesDAO]...\n"+e.getMessage());
        }  
        finally{
           if(st!=null) st.close(); 
           //if (liberar) poolManager.freeConnection("oracle",conOracle);
        }
        return datos;
    }  
    
    
    /*13-12-05*/
/**
 *
 * @param std_job
 * @param ano
 * @param mes
 * @param con
 * @return
 * @throws Exception
 */
     private List getCostosOperativos(String std_job, String ano, String mes, Connection con) throws Exception{
        PreparedStatement st = null;
         ResultSet rs = null;
         List Lista = new LinkedList();
         try {
             if (con != null) {
                 st = con.prepareStatement(this.obtenerSQL("SQL_COSTOS_OPERATIVOS"));//JJCastro fase2
                 st.setString(1, std_job);
                 st.setString(2, ano);
                 st.setString(3, mes);
                 rs = st.executeQuery();
                 while (rs.next()) {
                     Lista.add(CostosOperativos.loadItem(rs));
                 }
             }
         } catch (Exception ex){
             throw new Exception("Error en getCostosOperativos [ReportesDAO] ...\n" + ex.getMessage());
         }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
         return Lista;
     }



/**
 * 
 * @param std_job
 * @param ano
 * @param mes
 * @param con
 * @return
 * @throws Exception
 */
      private double getValorTotalCostos(String std_job, String ano, String mes, Connection con) throws Exception{
         String query = "SQL_TOTAL_COSTOS";
         double valor = 0;
         PreparedStatement st = null;
         ResultSet rs = null;
         try{
             if(con != null ){
                 st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                 st.setString(1, std_job);
                 st.setString(2, ano);
                 st.setString(3, mes);
                 rs = st.executeQuery();
                 if(rs.next()){
                    valor = rs.getDouble("total");
                 }
             }
         }catch (Exception ex){
             throw new Exception("Error en getCostosOperativos [ReportesDAO] ...\n" + ex.getMessage());
         }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
           }
         return valor;
     }


 /**
  *
  * @param cliente
  * @param ano
  * @param mes
  * @param con
  * @return
  * @throws Exception
  */
      private double getValorTotalCliente(String cliente, String ano, String mes, Connection con) throws Exception{
          String query = "SQL_TOTAL_CLIENTE";
          double valor = 0;
          PreparedStatement st = null;
          ResultSet rs = null;
          try{
              if(con != null ){
                  st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                  st.setString(1, cliente);
                  st.setString(2, ano);
                  st.setString(3, mes);
                  rs = st.executeQuery();
                  if(rs.next()){
                      valor = rs.getDouble("valor");
                  }
                  if (rs!=null) rs.close();
                  if (st!=null) st.close();
              }
          }catch (Exception ex){
              throw new Exception("Error en getValorTotalCliente [ReportesDAO] ...\n" + ex.getMessage());
          }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
           }
          return valor;
      }
      


/**
 *
 * @param agencia
 * @param ano
 * @param mes
 * @param con
 * @return
 * @throws Exception
 */
      private double getValorTotalAgencia(String agencia, String ano, String mes, Connection con) throws Exception{
          String query = "SQL_TOTAL_AGENCIA";
          double valor = 0;
          PreparedStatement st = null;
          ResultSet rs = null;
          try{
              if(con != null ){
                  st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                  st.setString(1, agencia);
                  st.setString(2, ano);
                  st.setString(3, mes);
                  rs = st.executeQuery();
                  if(rs.next()){
                      valor = rs.getDouble("valor");
                  }
                  if (rs!=null) rs.close();
                  if (st!=null) st.close();
              }
          }catch (Exception ex){
              throw new Exception("Error en getValorTotalAgencia [ReportesDAO] ...\n" + ex.getMessage());
          }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
           }
          return valor;
      }


/**
 *
 * @param Distrito
 * @param Agencia
 * @param Cliente
 * @param Estandar
 * @param Ano
 * @param Mes
 * @return
 * @throws Exception
 */
     public List loadCostosPt(String Distrito, String Agencia, String Cliente, String Estandar, String Ano , String Mes) throws Exception{
         
         PreparedStatement st  = null;
         ResultSet         rs  = null;
         List viajes = new LinkedList();
         String query = "SQL_VENTAS_PT";
         
         Connection        conPostgres    = this.conectarJNDI(query);//JJCastro fase2
         if (conPostgres == null)
             throw new SQLException("Sin conexion");
         
         try {
             st = conPostgres.prepareStatement(this.obtenerSQL(query));
             st.setString(1, Ano);
             st.setString(2, Mes);
             st.setString(3, (Distrito.equals("")?"%":Distrito));
             st.setString(4, (Agencia.equals("") ?"%":Agencia ));
             st.setString(5, (Cliente.equals("") ?"%":Cliente ));
             st.setString(6, (Estandar.equals("")?"%":Estandar));
             rs = st.executeQuery();
             
             ViajesAgencia ag  = null;
             ViajesCliente cl  = null;
             ViajesStandar std = null;
             
             while(rs.next()){
                 
                 std = new ViajesStandar();
                 std.setStdJobNo   (rs.getString(3));
                 std.setStdJobDesc(getDescripcion(3, conPostgres, std.getStdJobNo()));
                 std.setListCostosOperativos(getCostosOperativos(std.getStdJobNo(),Ano, Mes, conPostgres));
                 std.setValor_total_costos(getValorTotalCostos(std.getStdJobNo(),Ano, Mes,conPostgres));
                     
                 for (int i=1 ; i<=31 ; i++){
                     if( std.getValor_total_costos()!=0 ){
                         std.setCostosPtdo( i , (rs.getInt(i+3)*std.getValor_total_costos()));
                     }
                     else
                         std.setCostosPtdo( i , 0);
                 }
                 if (cl == null || !cl.getCliente().equals(rs.getString(2))){
                     if (cl!=null)  ag.addCliente(cl);
                     cl = new ViajesCliente();
                     cl.setCliente      (rs.getString(2));
                     cl.setClienteNombre(getDescripcion(2, conPostgres, cl.getCliente()));
                     cl.setCoperativos_cliente(getValorTotalCliente(cl.getCliente(),Ano,Mes,conPostgres));
                 }
                 cl.addStandar(std);
                 
                 if (ag == null || !ag.getAgencia().equals(rs.getString(1))) {
                     if (ag!=null) viajes.add(ag);
                     
                     ag = new ViajesAgencia();
                     ag.setAgencia      (rs.getString(1));
                     ag.setAgenciaNombre(getDescripcion(1, conPostgres, ag.getAgencia()));
                     ag.setCoperativos_agencia(getValorTotalAgencia(ag.getAgencia(),Ano,Mes,conPostgres));
                 }
             }
             if (ag != null) {
                 ViajesCliente ult = (ag.getListadoClientes().size()>0?
                 (ViajesCliente) ag.getListadoClientes().get(ag.getListadoClientes().size()-1):
                     null);
                     if (cl!=null && (ult==null || !ult.getCliente().equals(cl.getCliente())) ) ag.addCliente(cl);
                     viajes.add(ag);
             }
             
         }
         catch(Exception e) {
             throw new SQLException("Error en rutina loadViajesPt [ReportesDAO]... \n"+e.getMessage());
         }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conPostgres != null){ try{ this.desconectar(conPostgres); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
         return viajes;
     }
     
   
    
}
