/******************************************************************
* Nombre                        EstCargaPlacaDAO.java
* Descripci�n                   Clase DAO para la tabla est_carga_placa
* Autor                         ricardo rosero
* Fecha                         13/01/2006
* Versi�n                       1.0
* Coyright                      Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.DAOS;
import java.util.*;
import java.text.*;
import java.sql.*;
import com.tsp.util.connectionpool.PoolManager;
import java.util.Arrays;
import com.tsp.operation.model.beans.*;

public class EstCargaPlacaDAO {
    // Atributos
    private Usuario usuario;

    private EstCargaPlaca ecp;

    private Vector vecEstadisticas;

    private String INSERT = "INSERT INTO est_carga_placa (plaveh, carga, num_viaje, periodo, fecha" +
                            "last_update,user_update,creation_date,creation_user,dstrct,base)" +
                            "VALUES(?,?,?,?,?,?,?,?,?,?,?)";

    private String listarCCP = "SELECT distinct pl.plaveh as plaveh, st.clas_carga as carga" +
                               " FROM planilla pl, plarem pr, remesa r, stdjob st" +
                               " WHERE pl.plaveh = '?' and pl.stapla='A' and pl.numpla = pr.numpla" +
                               " and pr.numrem = r.numrem and r.std_job_no = st.std_job_no";

    private String listar = "SELECT * FROM est_carga_placa ORDER BY plaveh";

    private Vector ecpVec;

    private static String SQL = "SELECT plaveh, carga, num_viaje, periodo, fecha, last_update, user_update," +
                                "creation_date, creation_user, reg_status, dstrct, base " +
                                "FROM est_carga_placa ORDER BY plaveh";

    private static String DELETE = "DELETE FROM est_carga_placa";

    private static String QUERY1 = "SELECT pl.plaveh as plaveh, pl.fecpla as fecpla, pl.numpla as numpla,"
                    +"pl.stapla as stapla, st.clas_carga as carga, r.std_job_no as stdjob, pr.numrem as plarem "
                    +"FROM planilla pl, plarem pr, remesa r, stdjob st "
                    +"WHERE pl.stapla='A' and (pl.fecpla BETWEEN ? and ?)"
                    +"and pl.numpla = pr.numpla and pr.numrem = r.numrem "
                    +"and r.std_job_no = st.std_job_no ORDER BY pl.fecpla";

    private static String QUERY2 = "SELECT COUNT(pl.plaveh) as num_viaje "+
                        "FROM planilla pl, plarem pr, remesa r, stdjob st "+
                        "WHERE pl.stapla='A' and pl.numpla = pr.numpla " +
                        "and pr.numrem = r.numrem and " +
                        "r.std_job_no = st.std_job_no and st.clas_carga = ? and " +
                        "pl.plaveh =?";

    private static String QUERY3 = "SELECT COUNT(pl.plaveh) as num_viaje "+
                "FROM planilla pl, plarem pr, remesa r, stdjob st "+
                "WHERE pl.stapla='A' and pl.numpla = pr.numpla " +
                "and pr.numrem = r.numrem and " +
                "r.std_job_no = st.std_job_no and st.clas_carga = ? and pl.plaveh =?";

    private static String QUERY4 = "SELECT pl.plaveh as plaveh, pl.fecpla as fecha, pl.numpla as numpla,"
                    +"pl.stapla as stapla, st.clas_carga as carga, r.std_job_no as stdjob, pr.numrem as numrem"
                    +"FROM planilla pl, plarem pr, remesa r, stdjob st "
                    +"WHERE pl.stapla='A' and (pl.fecpla BETWEEN ? and ?)"
                    +"and pl.numpla = pr.numpla and pr.numrem = r.numrem "
                    +"and r.std_job_no = st.std_job_no and st.clas_carga = ? ORDER BY pl.fecpla";

    private Connection con;

    private PoolManager pm;
    
    /**
     * Genera el reporte estadisticas
     * @autor                       ricardo rosero
     * @throws                      SQLException
     * @version                     1.0
     */
    public Vector obtenerInforme() throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        Vector informe = new Vector();
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL);
                ////System.out.println("consulta: " + st.toString());
                
                rs = st.executeQuery();
                ////System.out.println("ResultSet----> " + rs.toString());
                while(rs.next()){
                    ecp = ecp.load(rs);
                    informe.add(ecp);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA GENERACION DEL REPORTE DE ANTICIPOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        return informe;
    }
    
    /**
     * M�todo que elimina todos los registros de la tabla est_carga_placa
     * @autor                       ricardo rosero
     * @throws                      SQLException
     * @version                     1.0.     
     **/ 
    public void deleteTabla() throws SQLException{
        PreparedStatement ps = null;
        try{            
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(DELETE);
            ps.executeUpdate();
            ////System.out.println("DELETE ---->> "+DELETE);
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }        
    }
    
    /** Creates a new instance of EstCargaPlaca_DAO */
    public EstCargaPlacaDAO() {
    
    }
    
    /**
    * getEstCargaPlaca
    * @autor: ....... Ing. Ricardo Rosero
    * @version ...... 1.0
    */
    public EstCargaPlaca getEstCargaPlaca(){
        return this.ecp;
    }

    /**
    * setEstCargaPlaca
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ EstCargaPlaca escp
    * @version ...... 1.0
    */   
    public void setEstCargaPlaca(EstCargaPlaca escp){
        this.ecp = escp;
    }

    /**
    * insertar2
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public void insertar2() throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        PoolManager poolManager = null;
        int carga;
        String periodo;
        int viajes = 0;
        PreparedStatement st1 = null;
        PreparedStatement st2 = null;
        PreparedStatement st3 = null;
        int cont = 0;
        Calendar fechaXI = Calendar.getInstance();
        fechaXI.add(fechaXI.DATE, -0);
        java.util.Date fechXI = fechaXI.getTime();
        SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd");
        String fechaX1 = s1.format(fechXI);
            
        Calendar hoy = Calendar.getInstance();
        hoy.add(hoy.DATE, -0);
        java.util.Date hoy2 = hoy.getTime();
        SimpleDateFormat s10 = new SimpleDateFormat("yyyy-MM-dd");
        String hoy3 = s10.format(hoy2);
        
        Usuario usuario = new Usuario();
        String usu = usuario.getLogin();
        String estado = " ";
        String distrito = usuario.getDstrct();
        String base = usuario.getBase();
        
                
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                ////System.out.println("Entro a insertar los datos\n");
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/operation/model/periodos");
                
                int per1 = Integer.parseInt(rb.getString("periodo1"));
                ////System.out.println("Periodo1 ->"+per1+"\n");
                
                int carga1 = Integer.parseInt(rb.getString("clase1"));
                ////System.out.println("Carga1 ->"+carga1+"\n");
                
                int per2 = Integer.parseInt(rb.getString("periodo2"));
                ////System.out.println("Periodo2 ->"+per2+"\n");
                
                int carga2 = Integer.parseInt(rb.getString("clase2"));
                ////System.out.println("Carga2 ->"+carga2+"\n");
                
                int per3 = Integer.parseInt(rb.getString("periodo3"));
                ////System.out.println("Periodo3 ->"+per3+"\n");
                
                int carga3 = Integer.parseInt(rb.getString("clase3"));
                ////System.out.println("Carga3 ->"+carga3+"\n");
                
                int per4 = Integer.parseInt(rb.getString("periodo4"));
                ////System.out.println("Periodo4 ->"+per4+"\n");
                
                int carga4 = Integer.parseInt(rb.getString("clase4"));
                ////System.out.println("Carga4 ->"+carga4+"\n");
                
                int per5 = Integer.parseInt(rb.getString("periodo5"));
                ////System.out.println("Periodo5 ->"+per5+"\n");
                
                int carga5 = Integer.parseInt(rb.getString("clase5"));
                ////System.out.println("Carga5 ->"+carga5+"\n");
                
                if(carga1==1 && per1==6){
                    Calendar fechaI = Calendar.getInstance();
                    Calendar fechaF = Calendar.getInstance();
                    fechaI.add(fechaI.DATE, -182);
            	    java.util.Date fechI = fechaI.getTime();
            	    java.util.Date fechF = fechaF.getTime();
                    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
                    String fecha1 = s.format(fechI);
                    String fecha2 = s.format(fechF);
                    
                    st = con.prepareStatement(QUERY1);
                    st.setString(1, fecha2);
                    st.setString(2, fecha1);
                    rs2 = st.executeQuery();
                    
                    while(rs2.next()){
                        st3 = con.prepareStatement(QUERY2);
                        st3.setInt(1, carga1);
                        st3.setString(2, rs2.getString("plaveh"));
                        rs3 = st3.executeQuery();
                        if(rs3.next()){
                            viajes = rs3.getInt("num_viaje");
                            ////System.out.println("Viajes ->"+viajes+"\n");
                        }
                        cont++;   
                        
                        PreparedStatement st4 = con.prepareStatement(QUERY4);
                        ResultSet rs4 = st4.executeQuery();
                        if(rs4.next()){
                            String fechaR = rs4.getString("fecha");
                            ////System.out.println("Fecha--> "+fechaR);
                        }
                        
                        st1 = con.prepareStatement(INSERT);
                        st1.setString(1, rs2.getString("plaveh"));
                        st1.setInt(2, carga1);
                        st1.setInt(3, viajes);
                        st1.setInt(4, per1);
                        st1.setString(5, rs4.getString("fecha")); 
                        st1.setString(6, hoy3);
                        st1.setString(7, usu);
                        st1.setString(8, hoy3);
                        st1.setString(9, usu);
                        st1.setString(10, estado);
                        st1.setString(11, distrito);
                        st1.setString(12, base);
                        st1.executeUpdate();
                        ////System.out.println("ISNERT-->"+ st1.toString());
                    }
                }
                else if(carga1==1 && per1==12){
                        Calendar fechaI = Calendar.getInstance();
                        Calendar fechaF = Calendar.getInstance();
                        fechaI.add(fechaI.DATE, -365);
                        java.util.Date fechI = fechaI.getTime();
                        java.util.Date fechF = fechaF.getTime();
                        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
                        String fecha1 = s.format(fechI);
                        String fecha2 = s.format(fechF);
            
                        st = con.prepareStatement(QUERY1);
                        st.setString(1, fecha2);
                        st.setString(2, fecha1);
                        rs2 = st.executeQuery();

                        while(rs2.next()){
                            st3 = con.prepareStatement(QUERY2);
                            st3.setInt(1, carga1);
                            st3.setString(2, rs2.getString("plaveh"));
                            rs3 = st3.executeQuery();
                            if(rs3.next()){
                                viajes = rs3.getInt("num_viaje");
                                ////System.out.println("Viajes ->"+viajes+"\n");
                            }
                            PreparedStatement st4 = con.prepareStatement(QUERY4);
                            ResultSet rs4 = st4.executeQuery();
                            if(rs4.next()){
                                String fechaR = rs4.getString("fecha");
                                ////System.out.println("Fecha--> "+fechaR);
                            }

                            st1 = con.prepareStatement(INSERT);
                            st1.setString(1, rs2.getString("plaveh"));
                            st1.setInt(2, carga1);
                            st1.setInt(3, viajes);
                            st1.setInt(4, per1);
                            st1.setString(5, rs4.getString("fecha")); 
                            st1.setString(6, hoy3);
                            st1.setString(7, usu);
                            st1.setString(8, hoy3);
                            st1.setString(9, usu);
                            st1.setString(10, estado);
                            st1.setString(11, distrito);
                            st1.setString(12, base);
                            st1.executeUpdate();
                            ////System.out.println("ISNERT-->"+ st1.toString());
                        }
                }
                if(carga2==2 && per2==6){
                    Calendar fechaI = Calendar.getInstance();
                    Calendar fechaF = Calendar.getInstance();
                    fechaI.add(fechaI.DATE, -182);
            	    java.util.Date fechI = fechaI.getTime();
            	    java.util.Date fechF = fechaF.getTime();
                    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
                    String fecha1 = s.format(fechI);
                    String fecha2 = s.format(fechF);
            
                    st = con.prepareStatement(QUERY1);
                    st.setString(1, fecha2);
                    st.setString(2, fecha1);
                    rs2 = st.executeQuery();
                    
                    while(rs2.next()){
                        st3 = con.prepareStatement(QUERY2);
                        st3.setInt(1, carga2);
                        st3.setString(2, rs2.getString("plaveh"));
                        rs3 = st3.executeQuery();
                        
                        if(rs3.next()){
                            viajes = rs3.getInt("num_viaje");
                            ////System.out.println("Viajes ->"+viajes+"\n");
                        }
                            
                        PreparedStatement st4 = con.prepareStatement(QUERY4);
                        ResultSet rs4 = st4.executeQuery();
                        if(rs4.next()){
                            String fechaR = rs4.getString("fecha");
                            ////System.out.println("Fecha--> "+fechaR);
                        }
                        
                        st1 = con.prepareStatement(INSERT);
                        st1.setString(1, rs2.getString("plaveh"));
                        st1.setInt(2, carga2);
                        st1.setInt(3, viajes);
                        st1.setInt(4, per2);
                        st1.setString(5, rs4.getString("fecha")); 
                        st1.setString(6, hoy3);
                        st1.setString(7, usu);
                        st1.setString(8, hoy3);
                        st1.setString(9, usu);
                        st1.setString(10, estado);
                        st1.setString(11, distrito);
                        st1.setString(12, base);
                        st1.executeUpdate();
                        ////System.out.println("ISNERT-->"+ st1.toString());
                    }
                }
                else if(carga2==2 && per2==12){
                        Calendar fechaI = Calendar.getInstance();
                        Calendar fechaF = Calendar.getInstance();
                        fechaI.add(fechaI.DATE, -365);
                        java.util.Date fechI = fechaI.getTime();
                        java.util.Date fechF = fechaF.getTime();
                        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
                        String fecha1 = s.format(fechI);
                        String fecha2 = s.format(fechF);
            
                        st = con.prepareStatement(QUERY1);
                        st.setString(1, fecha2);
                        st.setString(2, fecha1);
                        rs2 = st.executeQuery();

                        while(rs2.next()){
                            st3 = con.prepareStatement(QUERY2);
                            st3.setInt(1, carga2);
                            st3.setString(2, rs2.getString("plaveh"));
                            rs3 = st3.executeQuery();
                        
                        if(rs3.next()){
                            viajes = rs3.getInt("num_viaje");
                            ////System.out.println("Viajes ->"+viajes+"\n");
                        }
                        PreparedStatement st4 = con.prepareStatement(QUERY4);
                        ResultSet rs4 = st4.executeQuery();
                        if(rs4.next()){
                            String fechaR = rs4.getString("fecha");
                            ////System.out.println("Fecha--> "+fechaR);
                        }
                        
                        st1 = con.prepareStatement(INSERT);
                        st1.setString(1, rs2.getString("plaveh"));
                        st1.setInt(2, carga2);
                        st1.setInt(3, viajes);
                        st1.setInt(4, per2);
                        st1.setString(5, rs4.getString("fecha")); 
                        st1.setString(6, hoy3);
                        st1.setString(7, usu);
                        st1.setString(8, hoy3);
                        st1.setString(9, usu);
                        st1.setString(10, estado);
                        st1.setString(11, distrito);
                        st1.setString(12, base);
                        st1.executeUpdate();
                        ////System.out.println("ISNERT-->"+ st1.toString());
                        }
                }
                if(carga3==3 && per3==6){
                    Calendar fechaI = Calendar.getInstance();
                    Calendar fechaF = Calendar.getInstance();
                    fechaI.add(fechaI.DATE, -182);
            	    java.util.Date fechI = fechaI.getTime();
            	    java.util.Date fechF = fechaF.getTime();
                    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
                    String fecha1 = s.format(fechI);
                    String fecha2 = s.format(fechF);
            
                    st = con.prepareStatement(QUERY1);
                    st.setString(1, fecha2);
                    st.setString(2, fecha1);
                    rs2 = st.executeQuery();
                    
                    while(rs2.next()){
                        st3 = con.prepareStatement(QUERY2);
                        st3.setInt(1, carga3);
                        st3.setString(2, rs2.getString("plaveh"));
                        rs3 = st3.executeQuery();
                        
                        if(rs3.next()){
                            viajes = rs3.getInt("num_viaje");
                            ////System.out.println("Viajes ->"+viajes+"\n");
                        }
                            
                        PreparedStatement st4 = con.prepareStatement(QUERY4);
                        ResultSet rs4 = st4.executeQuery();
                        if(rs4.next()){
                            String fechaR = rs4.getString("fecha");
                            ////System.out.println("Fecha--> "+fechaR);
                        }
                        
                        st1 = con.prepareStatement(INSERT);
                        st1.setString(1, rs2.getString("plaveh"));
                        st1.setInt(2, carga3);
                        st1.setInt(3, viajes);
                        st1.setInt(4, per3);
                        st1.setString(5, rs4.getString("fecha")); 
                        st1.setString(6, hoy3);
                        st1.setString(7, usu);
                        st1.setString(8, hoy3);
                        st1.setString(9, usu);
                        st1.setString(10, estado);
                        st1.setString(11, distrito);
                        st1.setString(12, base);
                        st1.executeUpdate();
                        ////System.out.println("ISNERT-->"+ st1.toString());
                    }
                }
                else if(carga3==3 && per3==12){
                        Calendar fechaI = Calendar.getInstance();
                        Calendar fechaF = Calendar.getInstance();
                        fechaI.add(fechaI.DATE, -365);
                        java.util.Date fechI = fechaI.getTime();
                        java.util.Date fechF = fechaF.getTime();
                        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
                        String fecha1 = s.format(fechI);
                        String fecha2 = s.format(fechF);
            
                        st = con.prepareStatement(QUERY1);
                        st.setString(1, fecha2);
                        st.setString(2, fecha1);
                        rs2 = st.executeQuery();

                        while(rs2.next()){
                            st3 = con.prepareStatement(QUERY2);
                            st3.setInt(1, carga3);
                            st3.setString(2, rs2.getString("plaveh"));
                            rs3 = st3.executeQuery();
                        
                        if(rs3.next()){
                            viajes = rs3.getInt("num_viaje");
                            ////System.out.println("Viajes ->"+viajes+"\n");
                        }
                            PreparedStatement st4 = con.prepareStatement(QUERY4);
                        ResultSet rs4 = st4.executeQuery();
                        if(rs4.next()){
                            String fechaR = rs4.getString("fecha");
                            ////System.out.println("Fecha--> "+fechaR);
                        }
                        
                        st1 = con.prepareStatement(INSERT);
                        st1.setString(1, rs2.getString("plaveh"));
                        st1.setInt(2, carga3);
                        st1.setInt(3, viajes);
                        st1.setInt(4, per3);
                        st1.setString(5, rs4.getString("fecha")); 
                        st1.setString(6, hoy3);
                        st1.setString(7, usu);
                        st1.setString(8, hoy3);
                        st1.setString(9, usu);
                        st1.setString(10, estado);
                        st1.setString(11, distrito);
                        st1.setString(12, base);
                        st1.executeUpdate();
                        ////System.out.println("ISNERT-->"+ st1.toString());
                        }
                }
                if(carga4==4 && per4==6){
                    Calendar fechaI = Calendar.getInstance();
                    Calendar fechaF = Calendar.getInstance();
                    fechaI.add(fechaI.DATE, -182);
            	    java.util.Date fechI = fechaI.getTime();
            	    java.util.Date fechF = fechaF.getTime();
                    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
                    String fecha1 = s.format(fechI);
                    String fecha2 = s.format(fechF);
            
                    st = con.prepareStatement(QUERY1);
                    st.setString(1, fecha2);
                    st.setString(2, fecha1);
                    rs2 = st.executeQuery();
                    
                    while(rs2.next()){
                        st3 = con.prepareStatement(QUERY2);
                        st3.setInt(1, carga4);
                        st3.setString(2, rs2.getString("plaveh"));
                        rs3 = st3.executeQuery();
                        
                        if(rs3.next()){
                            viajes = rs3.getInt("num_viaje");
                            ////System.out.println("Viajes ->"+viajes+"\n");
                        }
                            
                        PreparedStatement st4 = con.prepareStatement(QUERY4);
                        ResultSet rs4 = st4.executeQuery();
                        if(rs4.next()){
                            String fechaR = rs4.getString("fecha");
                            ////System.out.println("Fecha--> "+fechaR);
                        }
                        
                        st1 = con.prepareStatement(INSERT);
                        st1.setString(1, rs2.getString("plaveh"));
                        st1.setInt(2, carga4);
                        st1.setInt(3, viajes);
                        st1.setInt(4, per4);
                        st1.setString(5, rs4.getString("fecha")); 
                        st1.setString(6, hoy3);
                        st1.setString(7, usu);
                        st1.setString(8, hoy3);
                        st1.setString(9, usu);
                        st1.setString(10, estado);
                        st1.setString(11, distrito);
                        st1.setString(12, base);
                        st1.executeUpdate();
                        ////System.out.println("ISNERT-->"+ st1.toString());
                    }
                }
                else if(carga4==4 && per4==12){
                        Calendar fechaI = Calendar.getInstance();
                        Calendar fechaF = Calendar.getInstance();
                        fechaI.add(fechaI.DATE, -365);
                        java.util.Date fechI = fechaI.getTime();
                        java.util.Date fechF = fechaF.getTime();
                        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
                        String fecha1 = s.format(fechI);
                        String fecha2 = s.format(fechF);
            
                        st = con.prepareStatement(QUERY1);
                        st.setString(1, fecha2);
                        st.setString(2, fecha1);
                        rs2 = st.executeQuery();

                        while(rs2.next()){
                            st3 = con.prepareStatement(QUERY2);
                            st3.setInt(1, carga4);
                            st3.setString(2, rs2.getString("plaveh"));
                            rs3 = st3.executeQuery();

                        if(rs3.next()){
                            viajes = rs3.getInt("num_viaje");
                            ////System.out.println("Viajes ->"+viajes+"\n");
                        }
                        PreparedStatement st4 = con.prepareStatement(QUERY4);
                        ResultSet rs4 = st4.executeQuery();
                        if(rs4.next()){
                            String fechaR = rs4.getString("fecha");
                            ////System.out.println("Fecha--> "+fechaR);
                        }
                        
                        st1 = con.prepareStatement(INSERT);
                        st1.setString(1, rs2.getString("plaveh"));
                        st1.setInt(2, carga4);
                        st1.setInt(3, viajes);
                        st1.setInt(4, per4);
                        st1.setString(5, rs4.getString("fecha")); 
                        st1.setString(6, hoy3);
                        st1.setString(7, usu);
                        st1.setString(8, hoy3);
                        st1.setString(9, usu);
                        st1.setString(10, estado);
                        st1.setString(11, distrito);
                        st1.setString(12, base);
                        st1.executeUpdate();
                        ////System.out.println("ISNERT-->"+ st1.toString());
                        }
                }
                if(carga5==5 && per5==6){
                    Calendar fechaI = Calendar.getInstance();
                    Calendar fechaF = Calendar.getInstance();
                    fechaI.add(fechaI.DATE, -182);
            	    java.util.Date fechI = fechaI.getTime();
            	    java.util.Date fechF = fechaF.getTime();
                    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
                    String fecha1 = s.format(fechI);
                    String fecha2 = s.format(fechF);
            
                    st = con.prepareStatement(QUERY1);
                    st.setString(1, fecha2);
                    st.setString(2, fecha1);
                    rs2 = st.executeQuery();
                    
                    while(rs2.next()){
                        st3 = con.prepareStatement(QUERY2);
                        st3.setInt(1, carga5);
                        st3.setString(2, rs2.getString("plaveh"));
                        rs3 = st3.executeQuery();
                        
                        if(rs3.next()){
                            viajes = rs3.getInt("num_viaje");
                            ////System.out.println("Viajes ->"+viajes+"\n");
                        }
                            
                        PreparedStatement st4 = con.prepareStatement(QUERY4);
                        ResultSet rs4 = st4.executeQuery();
                        if(rs4.next()){
                            String fechaR = rs4.getString("fecha");
                            ////System.out.println("Fecha--> "+fechaR);
                        }
                        
                        st1 = con.prepareStatement(INSERT);
                        st1.setString(1, rs2.getString("plaveh"));
                        st1.setInt(2, carga5);
                        st1.setInt(3, viajes);
                        st1.setInt(4, per5);
                        st1.setString(5, rs4.getString("fecha")); 
                        st1.setString(6, hoy3);
                        st1.setString(7, usu);
                        st1.setString(8, hoy3);
                        st1.setString(9, usu);
                        st1.setString(10, estado);
                        st1.setString(11, distrito);
                        st1.setString(12, base);
                        st1.executeUpdate();
                        ////System.out.println("ISNERT-->"+ st1.toString());
                    }
                }
                else if(carga5==5 && per5==12){
                        Calendar fechaI = Calendar.getInstance();
                        Calendar fechaF = Calendar.getInstance();
                        fechaI.add(fechaI.DATE, -365);
                        java.util.Date fechI = fechaI.getTime();
                        java.util.Date fechF = fechaF.getTime();
                        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
                        String fecha1 = s.format(fechI);
                        String fecha2 = s.format(fechF);
            
                        st = con.prepareStatement(QUERY1);
                        st.setString(1, fecha2);
                        st.setString(2, fecha1);
                        rs2 = st.executeQuery();

                        while(rs2.next()){
                            viajes = numeroViajes(carga5, rs2.getString("plaveh"));
                            PreparedStatement st4 = con.prepareStatement(QUERY4);
                            ResultSet rs4 = st4.executeQuery();
                            if(rs4.next()){
                                String fechaR = rs4.getString("fecha");
                                ////System.out.println("Fecha--> "+fechaR);
                            }

                            st1 = con.prepareStatement(INSERT);
                            st1.setString(1, rs2.getString("plaveh"));
                            st1.setInt(2, carga5);
                            st1.setInt(3, viajes);
                            st1.setInt(4, per5);
                            st1.setString(5, rs4.getString("fecha")); 
                            st1.setString(6, hoy3);
                            st1.setString(7, usu);
                            st1.setString(8, hoy3);
                            st1.setString(9, usu);
                            st1.setString(10, estado);
                            st1.setString(11, distrito);
                            st1.setString(12, base);
                            st1.executeUpdate();
                            ////System.out.println("ISNERT-->"+ st1.toString());
                        }
                }
                
                
                ////System.out.println("Inserto los datos???\n");
            }
            
        }
        catch(SQLException e){
            throw new SQLException("ERROR AL INTENTAR INSERTAR LOS DATOS EN LA TABLA" + e.getMessage() + " " + e.getErrorCode());
            
        }
        finally{
            if (st != null){
                try{
                    st.close();
                    rs2.close(); 
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    // FUNCION QUE OBTIENE EL NUMERO DE VIAJES HECHOS POR UN VEHICULO CON UN TIPO DE CARGA
    /**
    * numeroViajes
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ int carga, String plaveh
    * @throws ....... SQLException
    * @version ...... 1.0
    */
    public int numeroViajes(int carga, String plaveh)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        int viajes = 0;
        try{
            poolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con != null){
                st = con.prepareStatement(QUERY3);
                st.setInt(1, carga);
                st.setString(2,plaveh);
                rs = st.executeQuery();
                if(rs.next()){
                    viajes = rs.getInt("num_viaje");
                    ////System.out.println("Viajes ->"+viajes+"\n");
                }
            }
        }
        catch(SQLException e){
                throw new SQLException("ERROR AL INTENTAR SACAR LOS DATOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }

            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return viajes;
    }
    
    // funcion que obtiene todos los vehiculos que transportaron un tipo de carga en un periodo de 1 a�o
    /**
    * obetenerDatosPeriodo1
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ int periodo, int clasificacionCarga
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public ResultSet obtenerDatosPeriodo1(int periodo, int cla)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Calendar fechaI = Calendar.getInstance();
        Calendar fechaF = Calendar.getInstance();
        if(periodo==12){
            fechaI.add(fechaI.DATE, -365);
            java.util.Date fechI = fechaI.getTime();
            java.util.Date fechF = fechaF.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
            String fecha1 = s.format(fechI);
            String fecha2 = s.format(fechF);
            try {
                poolManager = PoolManager.getInstance();
                con = poolManager.getConnection("fintra");
                if(con!=null){
                    ////System.out.println("Entro a sacar datos de 1 a�o\n");
                    st = con.prepareStatement(QUERY4);
                    st.setString(1, fecha2);
                    st.setString(2, fecha1);
                    st.setInt(3, cla);
                    rs = st.executeQuery();
                    
                }
            }
            catch(SQLException e){
                throw new SQLException("ERROR AL TRATAR DE OBTENER LOS DATOS" + e.getMessage() + " " + e.getErrorCode());
            }
            finally{
                if (st != null){
                    try{
                        st.close();
                    }
                    catch(SQLException e){
                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                    }
                }
            
                if (con != null){
                    poolManager.freeConnection("fintra", con);
                }
            }
        }
        return rs;
    }
    
    // funcion que obtiene todos los vehiculos que transportaron un tipo de carga en un periodo de 6 meses
    /**
    * obtenerDatosPeriodo6
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ int periodo, int clasificacionCarga
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public ResultSet obtenerDatosPeriodo6(int periodo, int cla)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Calendar fechaI = Calendar.getInstance();
        Calendar fechaF = Calendar.getInstance();
        if(periodo==6){
            fechaI.add(fechaI.DATE, -182);
            java.util.Date fechI = fechaI.getTime();
            java.util.Date fechF = fechaF.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
            String fecha1 = s.format(fechI);
            String fecha2 = s.format(fechF);
            try {
                poolManager = PoolManager.getInstance();
                con = poolManager.getConnection("fintra");
                if(con!=null){
                    ////System.out.println("Entro a sacar datos de 6 meses\n");
                    st = con.prepareStatement(QUERY4);
                    st.setString(1, fecha2);
                    st.setString(2, fecha1);
                    st.setInt(3, cla);
                    rs = st.executeQuery();
                }
            }
            catch(SQLException e){
                throw new SQLException("ERROR AL TRATAR DE OBTENER LOS DATOS" + e.getMessage() + " " + e.getErrorCode());
            }
            finally{
                if (st != null){
                    try{
                        st.close();
                    }
                    catch(SQLException e){
                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                    }
                }
            
                if (con != null){
                    poolManager.freeConnection("fintra", con);
                }
            }
        }
        return rs;
    }
    
    /**
     * Getter for property ecpVec.
     * @return Value of property ecpVec.
     */
    public java.util.Vector getEcpVec() {
        return ecpVec;
    }
    
    /**
     * Setter for property ecpVec.
     * @param ecpVec New value of property ecpVec.
     */
    public void setEcpVec(java.util.Vector ecpVec) {
        this.ecpVec = ecpVec;
    }
    
}
