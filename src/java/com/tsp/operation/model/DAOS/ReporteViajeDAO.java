/***********************************************************************************
 * Nombre clase : ............... ReporteViajeDAO.java                             *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD para generar los datos del reporte     *
 *                                de viaje.                                        *
 * Autor :....................... Ing. Juan Manuel Escandon Perez                  *
 * Fecha :........................ 16 de noviembre de 2005, 11:12 AM               *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;

public class ReporteViajeDAO extends MainDAO{


    private static String SQLCONSULTA       =       "   SELECT P.PLAVEH,P.FECCUM, P.NUMPLA, R.NUMREM, R.REMISION,           "+
                                                    "   P.PESOREAL,P.UNIT_VLR, C.NOMCIU AS ORIGEN, CI.NOMCIU AS DESTINO,    "+
                                                    "   R.DESCRIPCION FROM PLANILLA P                                       "+
                                                    "    LEFT OUTER JOIN CIUDAD C ON ( C.CODCIU = P.ORIPLA )                "+
                                                    "    LEFT OUTER JOIN CIUDAD CI ON ( CI.CODCIU = P.DESPLA )              "+
                                                    "    LEFT OUTER JOIN PLAREM PR ON ( PR.NUMPLA = P.NUMPLA )              "+
                                                    "    LEFT OUTER JOIN REMESA R ON ( R.NUMREM = PR.NUMREM )               "+
                                                    "    WHERE P.FECDSP BETWEEN ? AND ?                                     "+
                                                    "    AND P.PLAVEH = ?                                                   "+
                                                    "    ORDER BY P.NUMPLA                                                  ";
    
    /** Creates a new instance of ReporteViajeDAO */
    public ReporteViajeDAO() {
        super("ReporteViajeDAO.xml");
    }
    
    /**
     * Metodo listReporteViaje, lista todas las planillas que correspondan a una placa
     * determinanda entre rango un rango de fechas
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String placa , String fecha inicial , String fecha final
     * @version : 1.0
     */
    public List listReporteViaje( String placa, String fechaInicial, String fechaFinal ) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        String query = "SQLCONSULTA";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, fechaInicial);
            st.setString(2, fechaFinal);
            st.setString(3, placa);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(ReporteViaje.load(rs));
                }
            }
            }}catch(Exception e){
            throw new SQLException("Error en rutina LIST [ReporteViajeDAO].... \n"+ e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
}
