/***********************************************************************************
 * Nombre:                        Jerarquia_tiempoDAO.java                         *
 * Descripcion :                  Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.                                       *
 * Autor:                         Ing. Jose de la Rosa                             *
 * Fecha:                         24 de junio de 2005, 05:31 PM                    *
 * Versi�n: Java                  1.0                                              *
 * Copyright: Fintravalores S.A. S.A.                                         *
 ***********************************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;


public class Jerarquia_tiempoDAO {
    private Jerarquia_tiempo jerarquia_tiempo;
    private Vector jerarquia_tiempos;
    
    //Diogenes 16.12.05
    private Vector vecres;
    private Vector veccausa;
    
    private static String SQL_RESPONACT= "SELECT DISTINCT " +
    "                                          cod_ractividad," +
    "                                          COALESCE(get_destablagen(cod_ractividad,'RESACT'), '') AS responsable" +
    "                                          FROM jerarquia_tiempo                                                       " +
    "                                          WHERE cod_actividad = ?" +
    "					ORDER BY responsable";
    
    private static String SQL_CAUSAACT = "SELECT DISTINCT " +
    "                                        cod_cdemora," +
    "                                        COALESCE(get_destablagen(cod_cdemora,'CAUDEM'), '') AS CAUSA" +
    "                                     FROM jerarquia_tiempo   " +
    "                                     WHERE cod_actividad = ? " +
    "  					  ORDER BY CAUSA ";
    
    private static String SQL_LISTAR = "Select a.cod_actividad," +
    "                                     a.cod_ractividad, " +
    "                                     a.cod_cdemora, " +
    "                                     a.rec_status, " +
    "                                     a.user_update, " +
    "                                     a.creation_user, " +
    "                                     get_destablagen(a.cod_ractividad,'RESACT'  ) AS responsable, " +
    "                                     get_destablagen(a.cod_cdemora, 'CAUDEM' ) AS causa," +
    "                                     b.deslarga  " +
    "                                    from jerarquia_tiempo a," +
    "                                             actividad b          " +
    "                                        where a.cod_actividad like ?" +
    "                                          and a.cod_cdemora like ? " +
    "                                          and a.cod_ractividad like ?" +
    "                                          and a.rec_status != 'A'" +
    "                                          and a.cod_actividad = b.cod_actividad " +
    "                                          and b.estado = ''";
    
    private static String SQL_BUSCAR = "Select a.cod_actividad, " +
    "                                         a.cod_ractividad,  " +
    "                                         a.cod_cdemora,  " +
    "                                         a.rec_status,  " +
    "                                         a.user_update,  " +
    "                                         a.creation_user,  " +
    "                                         get_destablagen(a.cod_ractividad,'RESACT'  ) AS responsable,  " +
    "                                         get_destablagen(a.cod_cdemora, 'CAUDEM' ) AS causa, " +
    "                                         b.deslarga   " +
    "                                        from jerarquia_tiempo a, " +
    "                                             actividad b " +
    "                                            where a.cod_actividad = ? " +
    "                                              and a.cod_cdemora = ?  " +
    "                                              and a.cod_ractividad = ? " +
    "                                              and a.rec_status != 'A' " +
    "                                              and a.cod_actividad = b.cod_actividad  " +
    "                                              and b.estado = ''";
    
    /*fin*/
    /** Creates a new instance of Jerarquia_tiempoDAO */
    public Jerarquia_tiempoDAO() {
    }
    /**
     * Getter for property getJerarquia_tiempo
     * @return Value of property getJerarquia_tiempo.
     */
    public com.tsp.operation.model.beans.Jerarquia_tiempo getJerarquia_tiempo() {
        return jerarquia_tiempo;
    }
    /**
     * Setter for setJerarquia_tiempo.
     * @param desResponsable New value of setJerarquia_tiempo.
     */
    public void setJerarquia_tiempo(com.tsp.operation.model.beans.Jerarquia_tiempo jerarquia_tiempo) {
        this.jerarquia_tiempo = jerarquia_tiempo;
    }
    /**
     * Getter for property getJerarquia_tiempo
     * @return Value of property getJerarquia_tiempo.
     */
    public java.util.Vector getJerarquia_tiempos() {
        return jerarquia_tiempos;
    }
    /**
     * Setter for setJerarquia_tiempos.
     * @param desResponsable New value of setJerarquia_tiempos.
     */
    public void setJerarquia_tiempos(java.util.Vector jerarquia_tiempos) {
        this.jerarquia_tiempos = jerarquia_tiempos;
    }
    
    /**
     * Metodo insertJerarquia_tiempo, ingresa un registro en la tabla Jerarquia tiempo
     * @param:
     * @autor : Ing. Jose de la Rosa
     * @version : 1.0
     */
    public void insertJerarquia_tiempo() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("Insert into jerarquia_tiempo (cod_actividad,cod_ractividad,cod_cdemora,creation_user,creation_date,user_update,last_update,dstrct,rec_status, base) values(?,?,?,?,'now()',?,'now()',?,' ', ?)");
                st.setString(1,jerarquia_tiempo.getActividad());
                st.setString(2,jerarquia_tiempo.getResponsable());
                st.setString(3,jerarquia_tiempo.getDemora());
                st.setString(4,jerarquia_tiempo.getCreation_user());
                st.setString(5,jerarquia_tiempo.getUser_update());
                st.setString(6,jerarquia_tiempo.getCia());
                st.setString(7,jerarquia_tiempo.getBase());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DE LA JERARQUIA DE TIEMPO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    /**
     * Metodo searchJerarquia_tiempo, metodo que buscar el objeto jerarquia tiempo
     * @param: cod. actividad, cod. responsable, cod. demora
     * @autor : Ing. Jose de la Rosa
     * @version : 1.0
     */
    public void searchJerarquia_tiempo(String codact, String codres, String coddem)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_BUSCAR);
                st.setString(1,codact);
                st.setString(2,coddem);
                st.setString(3,codres);
                rs= st.executeQuery();
                jerarquia_tiempos = new Vector();
                while(rs.next()){
                    jerarquia_tiempo = new Jerarquia_tiempo();
                    jerarquia_tiempo.setActividad(rs.getString("cod_actividad"));
                    jerarquia_tiempo.setResponsable(rs.getString("cod_ractividad"));
                    jerarquia_tiempo.setDemora(rs.getString("cod_cdemora"));
                    jerarquia_tiempos.add(jerarquia_tiempo);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA JERARQUIA DE TIEMPO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    /**
     * Metodo existJerarquia_tiempo, metodo que retorna true si existe el la jerarquia 
     *        y false de lo contrario
     * @param: cod. actividad, cod. responsable, cod. demora
     * @autor : Ing. Jose de la Rosa
     * @version : 1.0
     */
    public boolean existJerarquia_tiempo(String codact, String codres, String coddem) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("select * from jerarquia_tiempo where cod_actividad=? and cod_ractividad=? and cod_cdemora=? and rec_status!='A'");
                st.setString(1,codact);
                st.setString(2,codres);
                st.setString(3,coddem);
                rs = st.executeQuery();
                if (rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS CODIGOS DE LA JERARQUIA DE TIEMPO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    /**
     * Metodo listJerarquia_tiempo, metodo que lista las jerarquias registradas 
     * @param: 
     * @autor : Ing. Jose de la Rosa
     * @version : 1.0
     */
    public void listJerarquia_tiempo()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from jerarquia_tiempo where rec_status != 'A' order by cod_actividad");
                rs= st.executeQuery();
                jerarquia_tiempos = new Vector();
                while(rs.next()){
                    jerarquia_tiempo = new Jerarquia_tiempo();
                    jerarquia_tiempo.setActividad(rs.getString("cod_actividad"));
                    jerarquia_tiempo.setResponsable(rs.getString("cod_ractividad"));
                    jerarquia_tiempo.setDemora(rs.getString("cod_cdemora"));
                    jerarquia_tiempos.add(jerarquia_tiempo);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA JERARQUIA DE TIEMPO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    /**
     * Metodo updateJerarquia_tiempo, metodo que actualiza el registro en jerarquia tiempo 
     * @param: nuevo cod. actividad, nuevo cod responsabla, nuevo cod demora, usuario, cod actividad, cod demora, base
     * @autor : Ing. Jose de la Rosa
     * @version : 1.0
     */
    public void updateJerarquia_tiempo(String ncodact, String ncodres, String ncoddem, String usu,String codact, String codres, String coddem, String base) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("update jerarquia_tiempo set cod_actividad=?,cod_ractividad=?,cod_cdemora=?, last_update='now()', user_update=?, rec_status=' ', base = ? where cod_actividad=? and cod_ractividad=? and cod_cdemora=?");
                st.setString(1,ncodact);
                st.setString(2,ncodres);
                st.setString(3,ncoddem);
                st.setString(4,usu);
                st.setString(5,base);
                st.setString(6,codact);
                st.setString(7,codres);
                st.setString(8,coddem);////System.out.println("UPDATE " + st);
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACI�N DE LA JERARQUIA DE TIEMPO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    /**
     * Metodo anularJerarquia_tiempo, metodo que anula un registro en jerarquia tiempo 
     * @param:
     * @autor : Ing. Jose de la Rosa
     * @version : 1.0
     */
    public void anularJerarquia_tiempo() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("update jerarquia_tiempo set rec_status='A',last_update='now()', user_update=?, base=? where cod_actividad = ? and cod_ractividad = ? and cod_cdemora = ?");
                st.setString(1,jerarquia_tiempo.getUser_update());
                st.setString(2,jerarquia_tiempo.getBase());
                st.setString(3,jerarquia_tiempo.getActividad());
                st.setString(4,jerarquia_tiempo.getResponsable());
                st.setString(5,jerarquia_tiempo.getDemora());
                st.executeUpdate();
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE LA JERARQUIA DEL TIEMPO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
   /*Modificacion **/ 
     /**
     * Metodo searchDetalleJerarquia_tiempos, metodo que buscar las jerarquias 
     * @param: cod actividad, cod responsable, cod demora
     * @autor : Ing. Jose de la Rosa
     * @version : 1.0
     */
    public Vector searchDetalleJerarquia_tiempos(String codact, String codres, String coddem) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        jerarquia_tiempos=null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            boolean sw = false;
            if(con!=null){
                
                st = con.prepareStatement(this.SQL_LISTAR);
                st.setString(1, codact+"%");
                st.setString(2, coddem+"%");
                st.setString(3, codres+"%");
                
                ////System.out.println(st.toString());
                
                rs = st.executeQuery();
                jerarquia_tiempos = new Vector();
                while(rs.next()){
                    jerarquia_tiempo = new Jerarquia_tiempo();
                    jerarquia_tiempo.setActividad(rs.getString("cod_actividad"));
                    jerarquia_tiempo.setResponsable(rs.getString("cod_ractividad"));
                    jerarquia_tiempo.setDemora(rs.getString("cod_cdemora"));
                    jerarquia_tiempo.setRec_status(rs.getString("rec_status"));
                    jerarquia_tiempo.setUser_update(rs.getString("user_update"));
                    jerarquia_tiempo.setCreation_user(rs.getString("creation_user"));
                    jerarquia_tiempo.setDesResponsable(rs.getString("responsable"));
                    jerarquia_tiempo.setDesCausa(rs.getString("causa"));
                    jerarquia_tiempo.setDesactividad(rs.getString("deslarga"));
                    jerarquia_tiempos.add(jerarquia_tiempo);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA JERARQUIA DE TIEMPO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return jerarquia_tiempos;
    }
    
    /*Nuevo Diogenes */
    /**
     * Metodo  buscarResponsablaAct, lista los responsables de la de una actividad
     * @param: codigo de la actividad
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
     
    
    public void buscarResponsablaAct(String codact)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_RESPONACT);
                st.setString(1,codact );
                rs= st.executeQuery();
                vecres = new Vector();
                
                while(rs.next()){
                    jerarquia_tiempo = new Jerarquia_tiempo();
                    jerarquia_tiempo.setResponsable(rs.getString("cod_ractividad"));
                    jerarquia_tiempo.setDesResponsable(rs.getString("responsable"));
                    vecres.add(jerarquia_tiempo);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA JERARQUIA DE TIEMPO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo  buscarCausaAct, lista la causas de la de una actividad
     * @param: codigo de la actividad
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarCausaAct(String codact)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_CAUSAACT);
                st.setString(1,codact );
                rs= st.executeQuery();
                veccausa = new Vector();
                
                while(rs.next()){
                    jerarquia_tiempo = new Jerarquia_tiempo();
                    jerarquia_tiempo.setDemora(rs.getString("cod_cdemora"));
                    jerarquia_tiempo.setDesCausa(rs.getString("causa"));
                    veccausa.add(jerarquia_tiempo);
                }
            }

        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA JERARQUIA DE TIEMPO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
     
    }
    
    /**
     * Getter for property vecres.
     * @return Value of property vecres.
     */
    public java.util.Vector getVecres() {
        return vecres;
    }
    
   
    /**
     * Getter for property veccausa.
     * @return Value of property veccausa.
     */
    public java.util.Vector getVeccausa() {
        return veccausa;
    }
    
   
    
}
