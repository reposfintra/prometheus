/************************************************************************
 * Nombre clase: CumplidoDAO.java                                       *
 * Descripci�n: Clase que maneja las consultas de los cumplidos y       *
 * Autor: Ing. Rodrigo Salazar                                          *
 * Fecha: Created on 20 de septiembre de 2005, 11:25 AM                 *
 * Versi�n: Java 1.0                                                    *
 * Copyright: Fintravalores S.A. S.A.                              *
 ***********************************************************************/

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.MainDAO;
/**
 *
 * @author  R.SALAZAR
 */
public class CumplidoDAO extends MainDAO{
    
    /** Creates a new instance of CumplidoDAO */
    public CumplidoDAO() {
        super("CumplidoDAO.xml");
    }
    
    private Cumplido cump;
    private Vector cumps;
    //Jose 25/10/2005
    private final String  SQL_UPDATE_CUMPLIDO = "update cumplido set fecmigracion = ?, usu_migracion = ?  where tipo_doc = ? and cod_doc = ?";
    
     private final String SQL_MIGRAR_CUMPLIDOS = "select c.tipo_doc, c.cod_doc, u.nit, c.cantidad from planilla as pl, cumplido as c,usuarios as u "+
    "where pl.numpla = c.cod_doc and pl.base IN ('COL') AND " +
    "upper(c.creation_user) = upper(idusuario) and "+
    "tipo_doc = '001' and "+
    "c.creation_date BETWEEN ? AND 'NOW()' AND "+
    "c.reg_status !='A'";
    
    //jose 13/01/2006
    private final String SQL_INSERT_CONTROL_SOPORTE = "INSERT INTO control_soporte (numrem, soporte, entregado_conductor, last_update, user_update, creation_date, creation_user, dstrct, base) VALUES (?,?,?,'now()',?,'now()',?,?,?)";
    
    private final String SQL_SERCH_CONTROL_PROCESO = "SELECT soporte, t.descripcion FROM remesa r, soporte_clientes s, tablagen t WHERE r.cliente=s.cliente AND numrem=? AND t.table_code = s.soporte";
    
    /*mfontalvo 20060113*/
    private final String SQL_BUSCAR =
    "SELECT                        " +
    "   c.numrem,                  " +
    "   c.soporte,                 " +
    "   t.descripcion,             " +
    "   c.agencia_envio,           " +
    "   to_char(c.fecha_envio          ,'YYYY-MM-DD HH24:MI') as fecha_envio,          " +
    "   to_char(c.fecha_recibido       ,'YYYY-MM-DD HH24:MI') as fecha_recibido,       " +
    "   to_char(c.fecha_envio_logico   ,'YYYY-MM-DD HH24:MI') as fecha_envio_logico,   " +
    "   to_char(c.fecha_recibido_logico,'YYYY-MM-DD HH24:MI') as fecha_recibido_logico " +
    " FROM                         " +
    "     control_soporte  c,      " +
    "     tablagen         t       " +
    " WHERE                        " +
    "     t.table_type = 'SOP'     " +
    " and t.table_code = c.soporte ";
    private final String SQL_ReporteCumplido =  "   SELECT c.cod_doc AS planilla,                                                                       "+
    "   p.fecdsp AS fecdesp,                                                                                "+
    "   get_nombreciudad(p.agcpla) AS agepla,                                                               "+
    "   get_nombreciudad(p.oripla) AS oripla,                                                               "+
    "   get_nombreciudad(p.despla) AS despla,                                                               "+
    "   c.creation_date::date AS feccpd,                                                                    "+
    "   up.nombre AS ucpla,                                                                                 "+
    "   get_nombreciudad(c.id_agencia) AS agecum,                                                           "+
    "   p.pesoreal AS cantori,                                                                              "+
    "   c.cantidad AS cantcump,                                                                             "+
    "   c.cantidad - p.pesoreal AS difcant,                                                                 "+
    "   mt.fecent AS fecentpla,                                                                             "+
    "   c.creation_date::date - mt.fecent::date AS difFechas,                                               "+
    "   f.cantidad AS cantremcum,                                                                           "+
    "   f.cantidad - r.pesoreal AS difcantidadrem,                                                          "+
    "   f.creation_date AS feccumrem,                                                                       "+
    "   get_nombreciudad(f.id_agencia) AS agecumprem,                                                       "+
    "   ur.nombre AS ucrem,                                                                                 "+
    "   p.numpla,                                                                                           "+
    "   get_nombreciudad(p.agcpla) AS agcpla,                                                               "+
    "   p.fecdsp,                                                                                           "+
    "   get_nombreciudad(p.oripla) AS oripla,                                                               "+
    "   get_nombreciudad(p.despla) AS despla,                                                               "+
    "   p.pesoreal,                                                                                         "+
    "   p.reg_status,                                                                                       "+
    "   r.numrem AS remesa,                                                                                 "+
    "   r.fecrem AS fecrem,                                                                                 "+
    "   get_nombreciudad(r.agcrem) AS agerem,                                                               "+
    "   get_nombreciudad(r.orirem) AS orirem,                                                               "+
    "   get_nombreciudad(r.desrem) AS desrem,                                                               "+
    "   get_nombrecliente(cliente) AS cliente,                                                              "+
    "   r.pesoreal AS cantrem,                                                                              "+
    "   get_documentos(r.numrem, '008') AS docint                                                           "+
    "   FROM                                                                                                "+
    "       cumplido c                                                                                      "+
    "       LEFT JOIN   planilla p ON ( p.numpla =  c.cod_doc  )                                            "+
    "       LEFT JOIN   plarem pr ON  ( pr.numpla = p.numpla )                                              "+
    "       LEFT JOIN   remesa r ON  ( r.numrem = pr.numrem )                                               "+
    "       LEFT JOIN   cumplido f ON ( r.numrem = f.cod_doc )                                              "+
    "       LEFT JOIN (                                                                                     "+
    "           SELECT  rmt.fechareporte AS fecent,                                                         "+
    "               rmt.numpla                                                                              "+
    "               FROM   rep_mov_trafico rmt                                                              "+
    "               WHERE  rmt.tipo_reporte IN ('ECL')                                                      "+
    "                   ) mt ON ( p.numpla = mt.numpla )                                                    "+
    "                   LEFT JOIN usuarios up ON ( c.creation_user = upper(up.idusuario) )                  "+
    "                   LEFT JOIN usuarios ur ON ( f.creation_user = upper(ur.idusuario) )                  "+
    "                   WHERE  p.reg_status = 'C'                                                           "+
    "                   AND c.creation_date BETWEEN ? AND ? AND c.id_agencia = ?                            "+
    "                   AND up.nombre LIKE ?                                                                "+
    "       ORDER BY c.creation_date                                                                        ";
    
    /*Documentos por cumplir*/
    private final String  SQL_DocumentosxCumplir =  "   SELECT a.documento                                                                      "+
    "   FROM                                                                                    "+
    "   ( SELECT rd.documento FROM planilla p                                                   "+
    "  JOIN plarem pr ON ( pr.numpla = p.numpla )                                               "+
    "  JOIN remesa r ON  ( r.numrem = pr.numrem )                                               "+
    "  JOIN remesa_docto rd ON ( rd.numrem = r.numrem AND rd.tipo_doc = '008'  )                                        "+
    "  WHERE p.numpla = ? ) a                                                                   "+
    "  WHERE                                                                                    "+
    "  a.documento NOT IN ( SELECT documento FROM cumplido_doc WHERE numpla = ? )               ";
    
    /*Discrepancias*/
    private final String SQL_Discrepancia       =   " SELECT num_discre FROM discrepancia WHERE numpla = ?";
    
    
    //Jose 18-02-2006
    private final String SQL_EXISTE_CONTROL_SOPORTE = "SELECT numrem FROM control_soporte WHERE dstrct = ? AND numrem = ? AND soporte = ? ";
    /**
     * Metodo: listarCumplidos, permite listar los registros cumplidos
     * @autor : Ing. Rodrigo Salazar
     * @param :
     * @version : 1.0
     */
    public Vector listarCumplidos()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        cumps = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("select * from cumplido where reg_status !='A' order by descripcion");
                rs = st.executeQuery();
                cumps = new Vector();
                
                while(rs.next()){
                    cump = new Cumplido();
                    cump.load(rs);
                    cumps.add(cump);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS CUMPLIDOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return cumps;
    }
    
    /**
     * Metodo: insertarCumplidos, permite insertar un cumplido
     * @autor : Ing. Rodrigo Salazar
     * @param : objeto Cumplido
     * @version : 1.0
     */
    public void insertarCumplidos(Cumplido var) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("insert into cumplido values (?, ?, ?, ?, '', ?, 'now()', ?, 'now()', ?,?,?,?)");
                st.setString(1, var.getTipo_doc());
                st.setString(2, var.getCod_doc());
                st.setDouble(3, var.getCantidad());
                st.setString(4, var.getUnidad());
                st.setString(5, var.getDistrict());
                st.setString(6, var.getMod_user());
                st.setString(7, var.getCrea_user());
                st.setString(8, var.getBase());
                st.setString(9, var.getComent());
                st.setString(10, var.getAgencia());
                st.executeUpdate();
                // Cambio el estado del documento a 'C'
                if(var.getTipo_doc().equals("001")){
                    st= con.prepareStatement("update planilla set reg_status = 'C', feccum = 'now()' where numpla = '"+var.getCod_doc()+"'");
                }
                else{
                    st= con.prepareStatement("update remesa set reg_status = 'C' where numrem = '"+var.getCod_doc()+"'");
                }
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL INSERTAR EL CUMPLIDO" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * Metodo: buscarrCumplido, permite buscar un cumplido por tipo y codigo
     * @autor : Ing. Rodrigo Salazar
     * @param : Tipo de documento, codigo documento
     * @version : 1.0
     */
    public Cumplido buscarCumplido(String tipo, String cod)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from cumplido where tipo_doc =? and cod_doc=? and reg_status !='A' ");
                st.setString(1, tipo  );
                st.setString(2, cod );
                rs = st.executeQuery();
                
                while(rs.next()){
                    cump = new Cumplido();
                    cump.load(rs);
                    cump.setCantidad( rs.getDouble("cantidad") );
                    cump.setUnidad( rs.getString("unidad") );
                    cump.setComent( rs.getString("coment"));
                    ////System.out.println("DAO = "+cump.getComent());
                    return cump;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS CUMPLIDOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return cump;
    }
    
    //Jose 25/10/2005
    /**
     * Metodo: listarCumplidosMigrados, lista los cumplidos migrados en la fecha dada
     * @autor : Ing. Jose de La Rosa
     * @param : fecha
     * @version : 1.0
     */
    public Vector listarCumplidosMigrados(String fecha)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        cumps = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(SQL_MIGRAR_CUMPLIDOS);
                st.setString(1, fecha+" 00:00" );
                rs = st.executeQuery();
                cumps = new Vector();
                
                while(rs.next()){
                    cump = new Cumplido();
                    cump.setCantidad( rs.getDouble("cantidad") );
                    cump.setTipo_doc( rs.getString("tipo_doc") );
                    cump.setPlanilla( rs.getString("cod_doc") );
                    cump.setCrea_user(rs.getString("nit") );
                    cumps.add(cump);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS CUMPLIDOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return cumps;
    }
    
    /**
     * Metodo: updateCumplido, permite actualizar un cumplido
     * @autor : Ing. Rodrigo Salazar
     * @param : fecha, usuario, tipo documento, codigo del documento
     * @version : 1.0
     */
    public void updateCumplido(String fecha, String usuario, String tipo_doc, String cod_doc) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_UPDATE_CUMPLIDO);
                st.setString(1, fecha);
                st.setString(2, usuario);
                st.setString(3, tipo_doc);
                st.setString(4, cod_doc);
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACI�N DE LOS CUMPLIDOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    public Cumplido getCumplido() {
        return cump;
    }
    
    public void setCumplido(Cumplido cump) {
        this.cump = cump;
    }
    
    //KAREN REALES
    /**
     * Metodo: estaCumplida, valida si una planilla esta cumplida o no
     * @autor : Ing. Karen Reales
     * @param : numero de la planilla
     * @version : 1.0
     */
    public boolean estaCumplida(String numpla)throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean esta = false;
        
        
        try {
            st = this.crearPreparedStatement("SQL_ESTACUMPLIDA");
            st.setString(1,numpla);
            rs = st.executeQuery();
            if(rs.next()){
                esta = true;
            }
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO LA PLANILLA CUMPLIDA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_ESTACUMPLIDA");
        }
        return esta;
    }
    
    //jose 2006-01-13
    /**
     * Metodo: insertarControl_proceso, permite insertar un cumplido
     * @autor : Ing. Jose de la rosa
     * @param : objeto Cumplido
     * @version : 1.0
     */
    public String insertarControl_proceso(Cumplido var) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        String sql = "";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_INSERT_CONTROL_SOPORTE);
                st.setString(1, var.getRemesa());
                st.setString(2, var.getSoporte());
                st.setString(3, var.getEntregado_conductor());
                st.setString(4, var.getUsuario_modificacion());
                st.setString(5, var.getUsuario_creacion());
                st.setString(6, var.getDistrict());
                st.setString(7, var.getBase());
                sql = st.toString ();
                //st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL INSERTAR EL CONTROL DE SOPORTE" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return sql;
    }
    
    /**
     * Metodo: listarControlSoporte, lista los soportes de los clientes dada una remesa
     * @autor : Ing. Jose de La Rosa
     * @param : remesa
     * @version : 1.0
     */
    public void listarControlSoporte(String numrem)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        cumps = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(SQL_SERCH_CONTROL_PROCESO);
                st.setString(1, numrem);
                rs = st.executeQuery();
                cumps = new Vector();
                while(rs.next()){
                    cump = new Cumplido();
                    cump.setSoporte(rs.getString("soporte") );
                    cump.setComent(rs.getString("descripcion") );
                    cumps.add(cump);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS CONTROLES DE SOPORTE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo: getCumplidos, permite retornar un vector de registros de cumplido.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Vector getCumplidos() {
        return cumps;
    }
    /**
     * Metodo: setCumplidos, permite obtener un vector de registros de cumplido.
     * @autor : Ing. Jose de la rosa
     * @param : vector
     * @version : 1.0
     */
    public void setCumplidos(Vector Veccump) {
        this.cumps = Veccump;
    }
    
    /**
     * Metodo,que obtiene el listado de documentos soporte de una remesa
     * @autor mfontalvo
     * @param 2006-01-17
     * @param Remesa, filtro a consultar
     * @param Estado, filtro a consultar
     * @param Tipo, fisicos o logicos
     * @throws Exception.
     * @return List, Listado tipo de documentos soportes
     * @version : 1.0
     */
    public List ListadoDocumentosSoporteRemesa(String Remesa, String Estado, String Tipo)throws Exception{
        
        PoolManager       poolManager = PoolManager.getInstance();
        
        Connection        con = poolManager.getConnection("fintra");
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        List lista        = new LinkedList();
        
        if (con==null)
            throw new Exception("sin Conexion");
        try {
            
            
            String SQL = this.SQL_BUSCAR;
            if (!Remesa.equals(""))
                SQL += " and c.numrem = '"+ Remesa +"' ";
            
            // tipo de envios
            if (Tipo.equals("FISICA")){
                if (Estado.equals("ENVIADOS"))
                    SQL += " and c.fecha_envio <> '0099-01-01 00:00:00' ";
                else if (Estado.equals("RECIBIDOS"))
                    SQL += " and c.fecha_recibido <> '0099-01-01 00:00:00' ";
            }else if(Tipo.equals("LOGICA")){
                if (Estado.equals("ENVIADOS"))
                    SQL += " and c.fecha_envio_logico    <> '0099-01-01 00:00:00' ";
                else if (Estado.equals("RECIBIDOS"))
                    SQL += " and c.fecha_recibido_logico <> '0099-01-01 00:00:00' ";
            }
            else if(Tipo.equals("TODAS")){
                if (Estado.equals("ENVIADOS"))
                    SQL += " and (c.fecha_envio <> '0099-01-01 00:00:00' or c.fecha_envio_logico  <> '0099-01-01 00:00:00') ";
                else if (Estado.equals("RECIBIDOS"))
                    SQL += " and (c.fecha_recibido <> '0099-01-01 00:00:00' or c.fecha_recibido_logico <> '0099-01-01 00:00:00') ";
            }
            
            SQL += " ORDER BY 1,2 ";
            //////System.out.println("SQL: " + SQL);
            
            st = con.prepareStatement(SQL);
            rs = st.executeQuery();
            while (rs.next()){
                Cumplido c = new Cumplido();
                c.setRemesa               ( rs.getString("numrem"));
                c.setSoporte              ( rs.getString("soporte"));
                c.setComent               ( rs.getString("descripcion"));
                c.setAgencia_envio        ( rs.getString("agencia_envio"));
                c.setFecha_envio          ( rs.getString("fecha_envio"));
                c.setFecha_recibido       ( rs.getString("fecha_recibido"));
                c.setFecha_envio_logico   ( rs.getString("fecha_envio_logico") );
                c.setFecha_recibido_logico( rs.getString("fecha_recibido_logico"));
                lista.add(c);
            }
            
        }catch(SQLException e){
            throw new SQLException("Error en ListadoDocumentosSoporteRemesa en CumplidoDAO ... " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (rs  != null) rs.close();
            if (st  != null) st.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
        return lista;
    }
    
    /**
     * Metodo, que modifica las fechas de envio y recibido tanto
     * fisico como logico y agencia de envio de los soportes de
     * una remesa
     * @fecha 2006-01-18
     * @autor mfontalvo
     * @param Remesa, que se va a actualizar
     * @param Soporte, que se va a actualizar
     * @param editAGE, indica si se puede modificar o no la agencia
     * @param Agencia, nueva agencia de envio
     * @param editFEF, indica si se puede modificar o no la fecha de envio fisico
     * @param fechaEnvioFisico, nueva fecha de envio fisico
     * @param editFRF, indica si se puede modificar o no la fecha de recibido fisico
     * @param fechaRecibidoFisico, nueva fecha de recibido fisico
     * @param editFEL, indica si se puede modificar o no la fecha de envio logico
     * @param fechaEnvioLogico, nueva fecha de envio logico
     * @param editFRL, indica si se puede modificar o no la fecha de recibido logico
     * @param fechaRecibidoLogico, nueva fecha de recibido logico
     * @param Usuario, que registra el envio
     * @throws Exception.
     * @version : 1.0
     */
    
    public void ActualizarFechas(
    String Remesa,
    String Soporte,
    boolean editAGE, String Agencia,
    boolean editFEF, String fechaEnvioFisico,
    boolean editFRF, String fechaRecibidoFisico,
    boolean editFEL, String fechaEnvioLogico,
    boolean editFRL, String fechaRecibidoLogico,
    String Usuario
    )throws Exception{
        
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        con = poolManager.getConnection("fintra");
        PreparedStatement st  = null;
        
        if (con==null)
            throw new Exception("sin Conexion");
        try {
            String fecha = Util.getFechaActual_String(6);
            String Query = " UPDATE control_soporte SET #CAMPOS# WHERE numrem = ? and soporte = ? " ;
            
            String campos = "";
            
            // agencia envio
            if (editAGE && !Agencia.equals("")){
                campos += " agencia_envio = '"+ Agencia +"' ,";
            }
            
            // fecha envio fisico
            if (editFEF && !fechaEnvioFisico.equals("")){
                campos +=
                " fecha_envio            = '"+ fechaEnvioFisico +"' ," +
                " usuario_registro_envio = '"+ Usuario          +"' ," +
                " fecha_registro_envio   = '"+ fecha            +"' ,";
            }
            
            // fecha recibido fisico
            if (editFRF && !fechaRecibidoFisico.equals("")){
                campos +=
                " fecha_recibido            = '"+ fechaRecibidoFisico +"' ," +
                " usuario_registro_recibido = '"+ Usuario             +"' ," +
                " fecha_registro_recibido   = '"+ fecha               +"' ," ;
            }
            
            // fecha envio logico
            if (editFEL && !fechaEnvioLogico.equals("")){
                campos +=
                " fecha_envio_logico            = '"+ fechaEnvioLogico +"' ," +
                " usuario_registro_envio_logico = '"+ Usuario          +"' ," +
                " fecha_registro_envio_logico   = '"+ fecha            +"' ," ;
            }
            
            // fecha recibido logico
            if (editFRL && !fechaRecibidoLogico.equals("")){
                campos +=
                " fecha_recibido_logico            = '"+ fechaRecibidoLogico +"' ," +
                " usuario_registro_recibido_logico = '"+ Usuario             +"' ," +
                " fecha_registro_recibido_logico   = '"+ fecha               +"' ," ;
            }
            
            
            
            if (!campos.equals("")){
                Query = Query.replaceAll("#CAMPOS#", campos.substring(0,campos.length()-1));
                st = con.prepareStatement(Query);
                st.setString(1, Remesa  );
                st.setString(2, Soporte );
                //////System.out.println("UPDATE : " + st.toString());
                st.executeUpdate();
            }
            
            
        }catch(SQLException e){
            throw new SQLException("Error en ActualizarFechas en CumplidoDAO ... " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st  != null) st.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    
    /**
     * Metodo DocumentosXCumplir, Busca los Documentos que faltan por cumplir relacionados a una planilla
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String planilla
     * @version : 1.0
     */
    public String DocumentosXCumplir( String planilla ) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        String documentos    = "";
        
        try {
            st = this.crearPreparedStatement("SQL_DocumentosxCumplir");
            
            st.setString(1, planilla);
            st.setString(2, planilla);
            ////System.out.println("DOCUMENTOS " + st);
            rs = st.executeQuery();
            while(rs.next()){
                documentos += rs.getString("documento") + ",";
            }
            if( documentos.length() > 0 )
                documentos = documentos.substring(0, documentos.length()-1);
        }
        catch(Exception e) {
            throw new SQLException("Error en rutina DocumentosXCumplir [CumplidoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            this.desconectar("SQL_DocumentosxCumplir");
        }
        return documentos;
    }
    
    /**
     * Metodo BuscarDiscrepancias, Busca las discrepancias relacionadas a una planilla
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String planilla
     * @version : 1.0
     */
    public String BuscarDiscrepancias(String planilla ) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        String discrepancias = "";
        
        try {
            st = this.crearPreparedStatement("SQL_Discrepancia");
            st.setString(1, planilla);
            ////System.out.println("DISCREPANCIA " + st);
            rs = st.executeQuery();
            while(rs.next()){
                discrepancias += rs.getString("num_discre") + ",";
            }
            if( discrepancias.length() > 0 )
                discrepancias = discrepancias.substring(0, discrepancias.length()-1);
        }
        catch(Exception e) {
            throw new SQLException("Error en rutina DocumentosXCumplir [CumplidoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            this.desconectar("SQL_Discrepancia");
        }
        return discrepancias;
    }
    
    /**
     * Metodo ReporteCumplido, Lista todos los registro del Reporte de Cumplido
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public List ReporteCumplido(String fechainicial, String fechafinal, String agencia, String usuario) throws Exception{
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        try{
            st = this.crearPreparedStatement("SQL_ReporteCumplido");
            fechainicial += " 00:00";
            fechafinal   += " 23:59";
            usuario      += "%";
            st.setString(1, fechainicial);
            st.setString(2, fechafinal);
            st.setString(3, agencia);
            st.setString(4, usuario.toUpperCase());
            ////System.out.println("REPORTE CUMPLIDO " + st);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    ReporteCumplido rc = ReporteCumplido.load(rs);
                    //rc.setDiscrepancias(BuscarDiscrepancias(rc.getPlanilla()));
                    rc.setDocumentosxcumplir(DocumentosXCumplir(rc.getPlanilla()));
                    lista.add(rc);
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina List [CumplidoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_ReporteCumplido");
        }
        return lista;
    }
    /**
     * Metodo: existeControlSoporte, valida si una remesa y un documento de soporte existen en la tabla
     * @autor : Ing. Jose de la rosa
     * @param : numero de la remesa y los documentos de soporte
     * @version : 1.0
     */
    public boolean existeControlSoporte(String distrito, String numrem, String soporte)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean esta = false;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(SQL_EXISTE_CONTROL_SOPORTE);
                st.setString(1,distrito);
                st.setString(2,numrem);
                st.setString(3,soporte);
                rs = st.executeQuery();
                if(rs.next()){
                    esta = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO El CONTROL DE SOPORTE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return esta;
    }
    public List ListaCumplidos( String fechainicial ) throws Exception{
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        Cumplido objeto;
        try{
            st = this.crearPreparedStatement("SQL_Reporte");          
            st.setString(1, fechainicial);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    objeto = new Cumplido();
                    objeto = Cumplido.load(rs);
                    lista.add(objeto);
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina List [CumplidoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_Reporte");
        }
        return lista;
    }
    
    public Cumplido SearchPlarem( String numrem ) throws Exception{
        PreparedStatement st = null;
        ResultSet rs         = null;
        String numpla;
        Cumplido objeto = new Cumplido();
        try{
            st = this.crearPreparedStatement("SQL_SEARCH");          
            st.setString(1, numrem);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                   objeto.setCod_doc(rs.getString("numpla"));
                   objeto.setEstado(rs.getString("reg_status"));
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina List [CumplidoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_SEARCH");
        }
        return objeto;
    }
    
    public String FechaProceso( String proceso ) throws Exception{
        PreparedStatement st = null;
        ResultSet rs         = null;
        String fecha         = "";
        try{
            st = this.crearPreparedStatement("SQL_FECHA_PROCESO");          
            st.setString(1, proceso);
            
            
            rs = st.executeQuery();
            
            if ( rs.next() ){
                    fecha = rs.getString(1);
            }
            
        }catch(Exception e){
            throw new SQLException("Error en rutina FechaProceso [CumplidoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_FECHA_PROCESO");
        }
        return fecha;
    }
    
     public void ActualizarFechaProceso( String proceso ) throws Exception{
        PreparedStatement st = null;
        ResultSet rs         = null;
        try{
            st = this.crearPreparedStatement("SQL_ACTUALIZAR_PROCESO");          
            st.setString(1, proceso);
            st.executeUpdate();
           
        }catch(Exception e){
            throw new SQLException("Error en rutina FechaProceso [CumplidoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_ACTUALIZAR_PROCESO");
        }
    }
    
    
    
    public void InsertCumplido(Cumplido objeto) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        String query          = "SQL_INSERT";
        String query2         = "SQL_UPDATE";
        try{
            
            query = this.obtenerSQL("SQL_INSERT");
            con = this.conectar("SQL_INSERT");
            st = con.prepareStatement(query);
            
            st.setString(1,  objeto.getTipo_doc());
            st.setString(2,  objeto.getCod_doc());
            st.setDouble(3,  objeto.getCantidad());
            st.setString(4,  objeto.getUnidad());
            st.setString(5,  objeto.getDistrict());
            st.setString(6,   ""+objeto.getFecha_mod());
            st.setString(7,  objeto.getMod_user());
            st.setString(8,  ""+objeto.getFecha_crea());
            st.setString(9,  objeto.getCrea_user());
            st.setString(10,  objeto.getBase());
            st.setString(11,  objeto.getComent());
            st.setString(12,  objeto.getAgencia());
            
            st.executeUpdate();
            
            
            query2 = this.obtenerSQL("SQL_UPDATE");
            
            st = con.prepareStatement(query2);
            st.setString(1, objeto.getCod_doc());
            
            st.executeUpdate();
            
            
        }catch(Exception e){
            throw new SQLException("Error en rutina Insert [CumplidoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            this.desconectar("SQL_INSERT");
        }
    }
    
    
    
    /**
     * Metodo,que obtiene el listado de documentos soporte de una remesa
     * @autor mfontalvo
     * @param 2006-01-17
     * @param Remesa, filtro a consultar
     * @param Estado, filtro a consultar
     * @param Tipo, fisicos o logicos
     * @throws Exception.
     * @return List, Listado tipo de documentos soportes
     * @version : 1.0
     */
    public List ListadoDocumentosSoporteRemesa(String Agencia, String fecIni, String fecFin, String Remesa, String Estado, String Tipo)throws Exception{
        
        Connection        con = null;
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        List lista        = new LinkedList();
        
        try {
            
            
            con = this.conectar("SQL_BUSCAR_SOPORTES_REMESA");
            if (con==null)
                throw new Exception("sin Conexion");
            
            
            
            // GENERACION DE FILTROS DE LA TABLA DE CONTROL SOPORTE
            String SQL = this.obtenerSQL("SQL_BUSCAR_SOPORTES_REMESA");
            String FiltroInsert = "";
            // numero de remesa
            if (!Remesa.equals("")){
                SQL          += " and c.numrem   = '"+ Remesa +"' ";
                FiltroInsert += " and rem.numrem = '"+ Remesa +"' ";
            }
            
            // Agencia Facturadora
            if (!Agencia.equals("") && !Agencia.equals("NADA")){
                SQL          += " and cli.agfacturacion = '"+ Agencia +"' ";
                FiltroInsert += " and cli.agfacturacion = '"+ Agencia +"' ";
            }
            
            // fecha cumplido
            if (!fecIni.equals("") && !fecFin.equals("")){
                SQL          += " and cum.creation_date between '"+ fecIni +"' and '"+ fecFin +"'  ";                        
                FiltroInsert += " and cum.creation_date between '"+ fecIni +"' and '"+ fecFin +"'  ";
            }

            
            
            
            
            // BLOQUE PARA TRAER LOS SOPORTES NO GUARDADOS EN CONTROL SOPORTE
            // DE REMESAS CUMPLIDAS, ESTO FUCIONA SI Y SOLO SI LA REMESA NO 
            // TIENE SOPORTES EN CONTROL_SOPORTE
            String SQLInsert = this.obtenerSQL("SQL_INSERT_CONTROL_SOPORTE");
            SQLInsert = SQLInsert.replaceAll("#FILTRO#", FiltroInsert );
            st = con.prepareStatement(SQLInsert);
            st.executeUpdate();
            
            
            
            
            // tipo de envios
            if (Tipo.equals("FISICA")){
                if (Estado.equals("ENVIADOS"))
                    SQL += " and c.fecha_envio <> '0099-01-01 00:00:00' ";
                else if (Estado.equals("RECIBIDOS"))
                    SQL += " and c.fecha_recibido <> '0099-01-01 00:00:00' ";
            }else if(Tipo.equals("LOGICA")){
                if (Estado.equals("ENVIADOS"))
                    SQL += " and c.fecha_envio_logico    <> '0099-01-01 00:00:00' ";
                else if (Estado.equals("RECIBIDOS"))
                    SQL += " and c.fecha_recibido_logico <> '0099-01-01 00:00:00' ";
            }
            else if(Tipo.equals("TODAS")){
                if (Estado.equals("ENVIADOS"))
                    SQL += " and (c.fecha_envio <> '0099-01-01 00:00:00' or c.fecha_envio_logico  <> '0099-01-01 00:00:00') ";
                else if (Estado.equals("RECIBIDOS"))
                    SQL += " and (c.fecha_recibido <> '0099-01-01 00:00:00' or c.fecha_recibido_logico <> '0099-01-01 00:00:00') ";
            }
            
            SQL += " ORDER BY 1,2 ";
            //System.out.println("SQL: " + SQL);
            
            st = con.prepareStatement(SQL);
            rs = st.executeQuery();
            while (rs.next()){
                Cumplido c = new Cumplido();
                c.setRemesa               ( rs.getString("numrem"));
                c.setSoporte              ( rs.getString("soporte"));
                c.setComent               ( rs.getString("descripcion"));
                c.setAgencia_envio        ( rs.getString("agencia_envio"));
                c.setFecha_envio          ( rs.getString("fecha_envio"));
                c.setFecha_recibido       ( rs.getString("fecha_recibido"));
                c.setFecha_envio_logico   ( rs.getString("fecha_envio_logico") );
                c.setFecha_recibido_logico( rs.getString("fecha_recibido_logico"));
                lista.add(c);
            }
            
        }catch(SQLException e){
            throw new SQLException("Error en ListadoDocumentosSoporteRemesa en CumplidoDAO ... " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (rs  != null) rs.close();
            if (st  != null) st.close();
            this.desconectar("SQL_BUSCAR_SOPORTES_REMESA");
        }
        return lista;
    }
    
    
public double maxCantidadPlanilla( String distrito, String numpla, String numrem ) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs         = null;
        double cant = 0;
        try{
            st = this.crearPreparedStatement("SQL_MAX_PLANILLA");          
            st.setString(1, distrito);
            st.setString(2, numpla);
            st.setString(3, numrem);
            rs=st.executeQuery();
            if( rs.next() )
                cant = rs.getDouble("cantidad");
        }catch(SQLException e){
            throw new SQLException("Error en rutina maxCantidadPlanilla( String numrem, String distrito ).... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_MAX_PLANILLA");
        }
        return cant;
    }
    
    public double maxCantidadRemesa( String planilla, String distrito ) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs         = null;
        String numpla;
        double cant = 0;
        try{
            st = this.crearPreparedStatement("SQL_MAX_REMESA");          
            st.setString(1, distrito);
            st.setString(2, planilla);
            rs=st.executeQuery();
            while( rs.next() ){
                cant  = cant+ rs.getDouble("cantidad");
            }
        }catch(SQLException e){
            throw new SQLException("Error en rutina maxCantidadRemesa( String planilla, String distrito ).... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_MAX_REMESA");
        }
        return cant;
    }
    
     /**
     * Metodo: insertarCumplidos, permite insertar un cumplido
     * @autor : Ing. Rodrigo Salazar
     * @param : objeto Cumplido
     * @version : 1.0
     */
    public String insertCumplidos(Cumplido var) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        String sql = "";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("insert into cumplido values (?, ?, ?, ?, '', ?, 'now()', ?, 'now()', ?,?,?,?)");
                st.setString(1, var.getTipo_doc());
                st.setString(2, var.getCod_doc());
                st.setDouble(3, var.getCantidad());
                st.setString(4, var.getUnidad());
                st.setString(5, var.getDistrict());
                st.setString(6, var.getMod_user());
                st.setString(7, var.getCrea_user());
                st.setString(8, var.getBase());
                st.setString(9, var.getComent());
                st.setString(10, var.getAgencia());
                sql = st.toString ();
                // Cambio el estado del documento a 'C'
                if(var.getTipo_doc().equals("001")){
                    st= con.prepareStatement("update planilla set reg_status = 'C', feccum = 'now()' where numpla = '"+var.getCod_doc()+"'");
                }
                else{
                    st= con.prepareStatement("update remesa set reg_status = 'C' where numrem = '"+var.getCod_doc()+"'");
                }
                sql = sql +";"+ st.toString ();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL INSERTAR EL CUMPLIDO" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return sql;
    }
    public double maxCantidadCumplida( String planilla, String distrito ) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs         = null;
        String numpla;
        double cant = 0;
        try{
            st = this.crearPreparedStatement("SQL_MAX_REMESA_CUMPL");          
            st.setString(1, distrito);
            st.setString(2, planilla);
            rs=st.executeQuery();
            if( rs.next() ){
                cant  = cant+ rs.getDouble("cantidad");
            }
        }catch(SQLException e){
            throw new SQLException("Error en rutina maxCantidadRemesa( String planilla, String distrito ).... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_MAX_REMESA_CUMPL");
        }
        return cant;
    }
    
      /**
     * Metodo: updateCumplidos, permite actualizar un cumplido
     * @autor : Ing. Rodrigo Salazar
     * @param : objeto Cumplido
     * @version : 1.0
     */
    public String updateCumplidos(Cumplido var) throws SQLException{
        PreparedStatement st = null;
        String sql = "";
        try{
            
           
                st = this.crearPreparedStatement("SQL_ACTUALIZA");
                st.setDouble(1, var.getCantidad());
                st.setString(2, var.getDistrict());
                st.setString(3, var.getTipo_doc());
                st.setString(4, var.getCod_doc());
                
                sql = st.toString();
                
            
        }catch(SQLException e){
            throw new SQLException("ERROR AL INSERTAR EL CUMPLIDO" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            this.desconectar("SQL_ACTUALIZA");
        }
        return sql;
    }


   /**
     * Metodo: Tiene Cumplido
     * @autor : Ing. Karen reales
     * @param : Placa
     * @version : 1.0
     */
    public boolean tieneCumplido(String placa) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean esta=false;
        try{
            
           
                st = this.crearPreparedStatement("SQL_CUMPLID_PLACA");
                st.setString(1, placa);
                rs = st.executeQuery();
                if(rs.next()){
                    esta = true;
                }
                
            
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO EL ULTIMO CUMPLIDO DE LA PLACA " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            this.desconectar("SQL_CUMPLID_PLACA");
        }
        return esta;
    }
}
 /*****************************************************
 Entregado a karen 13 Febrero 2007
  *****************************************************/
