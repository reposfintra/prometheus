    /*******************************************************************************
    * Nombre Clase ............. AdminSoftwareDAO.java
    * Descripci�n  .. . . . . .  Objeto de Acceso a datos para la administracion de software
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  10/01/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************************************************/



package com.tsp.operation.model.DAOS;


import java.io.*;
import java.util.*;
import java.sql.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;


public class AdminSoftwareDAO {
    
   
    
 /** ____________________________________________________________________________________________
                                              ATRIBUTOS
     ____________________________________________________________________________________________ */
    

    
    public static final String SEPARADOR                   =    "-";    
    
    private static String SQL_EQUIPO_INVENTARIO            =    " SELECT distinct(equipo)  FROM  inv.inventario_software ";
    
    private static String SQL_SITIO_EQUIPO_INVENTARIO      = 
                                                                " SELECT equipo, sitio, count(*)      "+
                                                                " FROM  inv.inventario_software       "+
                                                                " GROUP BY 1,2                        ";  
    
    
    private static String SQL_FILE_SITIO_EQUIPO_INVENTARIO =  
                                                                " SELECT                     "+                   
                                                                "       equipo,              "+
                                                                "       sitio,               "+
                                                                "       nombre_archivo,      "+
                                                                "       extension,           "+ 
                                                                "       ruta                 "+
                                                                " FROM                       "+ 
                                                                "    inv.inventario_software "+
                                                                " WHERE                      "+
                                                                "       equipo  = ?          "+ 
                                                                "  AND  sitio   = ?          "+
                                                                " ORDER BY 3,4               ";

    
    
    
    private static String SQL_SAVE_PROGRAM  =     
                                                    " INSERT INTO inv.programa  "+
                                                    " (                         "+
                                                    "   dstrct,                 "+
                                                    "   codigo_programa,        "+
                                                    "   nombre,                 "+
                                                    "   equipo,                 "+
                                                    "   sitio_revision,         "+
                                                    "   programador_asig,       "+
                                                    "   creation_user,          "+
                                                    "   base                    "+
                                                    " )                         "+
                                                    " VALUES                    "+
                                                    " (?,?,?,?,?,?,?,?)         ";

    
    
    private static String SQL_SAVE_RELACION = 
                                                " INSERT INTO inv.programa_inventario "+
                                                " (                                   "+
                                                "   dstrct,                           "+
                                                "   codigo_programa,                  "+
                                                "   equipo,                           "+
                                                "   sitio,                            "+ 
                                                "   ruta,                             "+
                                                "   nombre_archivo,                   "+
                                                "   extension,                        "+
                                                "   creation_user,                    "+
                                                "   base                              "+
                                                " )                                   "+
                                                " VALUES (?,?,?,?,?,?,?,?,?)          ";
    
    
    
    
    
    
     private static String SQL_DELETE_PROGRAM  = " DELETE  FROM  inv.programa             WHERE  codigo_programa = ? " ;
     
     private static String SQL_DELETE_RELACION = " DELETE  FROM  inv.programa_inventario  WHERE  codigo_programa = ? " ;
    
    
    
    
 /** ____________________________________________________________________________________________
                                              METODOS
    ____________________________________________________________________________________________ */
    

    
    
    
    public AdminSoftwareDAO() {}
    
    
    
    
    
    
     /**
     * Busca los diferentes equipos establecidos en la tabla inventario
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public List searchEquipos() throws Exception{
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("inventario");
          if (con == null)
                throw new SQLException("Sin conexion");
          PreparedStatement st        = null;
          ResultSet         rs        = null;
          List              lista     = new LinkedList();
          try {
              st= con.prepareStatement(this.SQL_EQUIPO_INVENTARIO);   
              rs=st.executeQuery();
              while(rs.next()){                  
                 String equipo = rs.getString(1);
                 lista.add(equipo);
              }
          }
          catch(SQLException e){
              throw new SQLException(" DAO:searchEquipos()  No se pudieron listar los Equipos -->"+ e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             if(rs!=null) rs.close();
             poolManager.freeConnection("inventario",con);
          } 
          return lista;
    }
     
     
     
     
     
     /**
     * Busca los diferentes sitios por equipos establecidos en la tabla inventario
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public List searchSitios() throws Exception{
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("inventario");
          if (con == null)
                throw new SQLException("Sin conexion");
          PreparedStatement st        = null;
          ResultSet         rs        = null;
          List              lista     = new LinkedList();
          try {
              st= con.prepareStatement(this.SQL_SITIO_EQUIPO_INVENTARIO);   
              rs=st.executeQuery();
              while(rs.next()){                  
                 String equipo = rs.getString(1);
                 String sitio  = rs.getString(2);                 
                 lista.add(equipo + SEPARADOR + sitio);
              }
          }
          catch(SQLException e){
              throw new SQLException(" DAO:searchSitios()  No se pudieron listar los Sitios -->"+ e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             if(rs!=null) rs.close();
             poolManager.freeConnection("inventario",con);
          } 
          return lista;
    }
     
     
     
     

     
      /**
     * Busca los diferentes archivos para un sitio y equipo
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public List searchFile(String equipo, String sitio) throws Exception{
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("inventario");
          if (con == null)
                throw new SQLException("Sin conexion");
          PreparedStatement st        = null;
          ResultSet         rs        = null;
          List              lista     = new LinkedList();
          try {
              st= con.prepareStatement(this.SQL_FILE_SITIO_EQUIPO_INVENTARIO);  
              st.setString(1, equipo);
              st.setString(2, sitio);
              rs=st.executeQuery();
              int cont=0;
              while(rs.next()){   
                  String nombre = rs.getString("nombre_archivo");
                  if(!nombre.trim().equals("")){
                     InventarioSoftware  file = new InventarioSoftware();
                        file.setId       ( cont);
                        file.setEquipo   ( rs.getString("equipo") );
                        file.setSitio    ( rs.getString("sitio") );
                        file.setNombre   ( rs.getString("nombre_archivo") );
                        file.setExtension( rs.getString("extension") );
                        file.setRuta     ( rs.getString("ruta") );
                    lista.add(file);
                    cont++;                      
                  }
              }
          }
          catch(SQLException e){
              throw new SQLException(" DAO:searchFile()  No se pudieron listar los file -->"+ e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             if(rs!=null) rs.close();
             poolManager.freeConnection("inventario",con);
          } 
          return lista;
    }
     
     
     
     
     
     
     
     /**
     * Guarda en la bd el programa
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public void save(Programa programa, String user) throws Exception{
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("inventario");
          if (con == null)
                throw new SQLException("Sin conexion");
          PreparedStatement st        = null;
          PreparedStatement st2       = null;
          try {
              
              this.delete(programa);
              
              // programa
              st= con.prepareStatement(this.SQL_SAVE_PROGRAM);
                st.setString(1, programa.getDistrito()   );
                st.setString(2, programa.getPrograma()   );
                st.setString(3, programa.getDescripcion());
                st.setString(4, programa.getEquipo()     );
                st.setString(5, programa.getSitio()      );
                st.setString(6, programa.getProgramador());
                st.setString(7, user);
                st.setString(8, "");
              st.execute();
              
              // Archivos del Programa
              st2= con.prepareStatement(this.SQL_SAVE_RELACION);
              List lista = programa.getListFile();
              for(int i=0; i<lista.size();i++){
                 InventarioSoftware  file  = (InventarioSoftware)lista.get(i);
                      st2.setString(1, programa.getDistrito());
                      st2.setString(2, programa.getPrograma());
                      st2.setString(3, file.getEquipo()      );
                      st2.setString(4, file.getSitio()       );
                      st2.setString(5, file.getRuta()        );
                      st2.setString(6, file.getNombre()      );
                      st2.setString(7, file.getExtension()   );
                      st2.setString(8, user                  );
                      st2.setString(9, "");
                  st2.execute();
                  st2.clearParameters();
              }
              
          }
          catch(SQLException e){
              this.delete(programa);
              throw new SQLException(" DAO:save() No se pudo guardar el programa -->"+ e.getMessage());
          }  
          finally{
             if(st!=null)  st.close();
             if(st2!=null) st2.close();
             poolManager.freeConnection("inventario",con);
          } 
    }
    
    
     
     
     
     /**
     * Elimina en la bd el programa
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public void delete(Programa programa) throws Exception{
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("inventario");
          if (con == null)
                throw new SQLException("Sin conexion");
          PreparedStatement st        = null;
          PreparedStatement st2       = null;
          try {
              st= con.prepareStatement(this.SQL_DELETE_PROGRAM);
              st.setString(1, programa.getPrograma());
              st.execute();
              
              st2= con.prepareStatement(this.SQL_DELETE_RELACION);
              st2.setString(1, programa.getPrograma());
              st2.execute();
          }
          catch(SQLException e){
              throw new SQLException(" DAO:delete() No se pudo eliminar el programa -->"+ e.getMessage());
          }  
          finally{
             if(st!=null)  st.close();
             if(st2!=null) st2.close();
             poolManager.freeConnection("inventario",con);
          } 
    }
    
    
    
}
