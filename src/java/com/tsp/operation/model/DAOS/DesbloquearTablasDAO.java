/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.DesbloquearTablasBeans;
import java.util.ArrayList;

/**
 *
 * @author mariana
 */
public interface DesbloquearTablasDAO {

    public ArrayList<DesbloquearTablasBeans> cargarTablas();
    
    public String updateMasivo();

}
