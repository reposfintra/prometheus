/*********************************************************************************
 * Nombre clase :      ConsultarModificarFacturaClienteDAO.java                  *
 * Descripcion :       DAO del ConsultarModificarFacturaClienteDAO.java          *
 *                     Clase que maneja los DAO (Data Access Object) los cuales  *
 *                     contienen los metodos que interactuan con la B.D.         *
 * Autor :             LREALES                                                   *
 * Fecha :             30 de noviembre de 2006, 10:00 AM                         *
 * Version :           1.0                                                       *
 * Copyright :         Fintravalores S.A.                                   *
 *********************************************************************************/

package com.tsp.operation.model.DAOS;

import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;
import java.util.Date;
import java.text.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.UtilFinanzas.*;

public class ConsultarModificarFacturaClienteDAO extends MainDAO {
    
    private BeanGeneral bean;
    
    private Vector vector;
          
    /** Creates a new instance of ConsultarModificarFacturaClienteDAO */
    public ConsultarModificarFacturaClienteDAO () {
        
        super ( "ConsultarModificarFacturaClienteDAO.xml" );
        
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector () {
        
        return vector;
        
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector ( java.util.Vector vector ) {
        
        this.vector = vector;
        
    }
        
    /** 
     * Funcion publica que obtiene la información que se va a mostrar
     * en la consulta de la factura de un cliente.
     */
    public void consultarFactura ( String distrito, String factura ) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_SELECT";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, distrito );
            st.setString( 2, factura );
            rs = st.executeQuery();
            
            this.vector = new Vector();
            
            while( rs.next() ){
                
                String estado = rs.getString( "reg_status" )!=null?rs.getString( "reg_status" ).toUpperCase():"";           
                String documento = rs.getString( "documento" )!=null?rs.getString( "documento" ).toUpperCase():"";
                String nit = rs.getString( "nit" )!=null?rs.getString( "nit" ).toUpperCase():"";
                String codcli = rs.getString( "codcli" )!=null?rs.getString( "codcli" ).toUpperCase():"";
                String nombre_cliente = rs.getString( "nombre_cliente" )!=null?rs.getString( "nombre_cliente" ).toUpperCase():"";
                String fecha_factura = rs.getString( "fecha_factura" )!=null?rs.getString( "fecha_factura" ):"0099-01-01";
                fecha_factura = fecha_factura.equals("0099-01-01")?"":fecha_factura;
                String fecha_vencimiento = rs.getString( "fecha_vencimiento" )!=null?rs.getString( "fecha_vencimiento" ):"0099-01-01";
                fecha_vencimiento = fecha_vencimiento.equals("0099-01-01")?"":fecha_vencimiento;
                String fecha_ultimo_pago = rs.getString( "fecha_ultimo_pago" )!=null?rs.getString( "fecha_ultimo_pago" ):"0099-01-01";
                fecha_ultimo_pago = fecha_ultimo_pago.equals("0099-01-01")?"":fecha_ultimo_pago;
                String descripcion = rs.getString( "descripcion" )!=null?rs.getString( "descripcion" ).toUpperCase():"";
                String observacion = rs.getString( "observacion" )!=null?rs.getString( "observacion" ).toUpperCase():"";
                String valor_factura = rs.getString( "valor_factura" )!=null?rs.getString( "valor_factura" ):"0.00";
                String valor_abono = rs.getString( "valor_abono" )!=null?rs.getString( "valor_abono" ):"0.00";
                String valor_saldo = rs.getString( "valor_saldo" )!=null?rs.getString( "valor_saldo" ):"0.00";
                String valor_tasa = rs.getString( "valor_tasa" )!=null?rs.getString( "valor_tasa" ):"0"; 
                String moneda = rs.getString( "moneda" )!=null?rs.getString( "moneda" ).toUpperCase():""; 
                String cantidad_items = rs.getString( "cantidad_items" )!=null?rs.getString( "cantidad_items" ):"0"; 
                String agencia_facturacion = rs.getString( "agencia_facturacion" )!=null?rs.getString( "agencia_facturacion" ).toUpperCase():""; 
                String agencia_cobro = rs.getString( "agencia_cobro" )!=null?rs.getString( "agencia_cobro" ).toUpperCase():""; 
                String fecha_probable_pago = rs.getString( "fecha_probable_pago" )!=null? rs.getString( "fecha_probable_pago" ) : "0099-01-01";
                fecha_probable_pago = fecha_probable_pago.equals("0099-01-01")?"":fecha_probable_pago;
                
                bean = new BeanGeneral ();
                
                if ( estado.equals("A") ) {
                    estado = "Anulada";
                } else {                    
                    estado = valor_saldo.equals("0.00")?"Pagada":"Pendiente";                    
                }
                
                bean.setValor_01( estado );
                bean.setValor_02( documento );
                bean.setValor_03( nit );
                bean.setValor_04( codcli );
                bean.setValor_05( nombre_cliente );
                bean.setValor_06( fecha_factura );
                bean.setValor_07( fecha_vencimiento );
                bean.setValor_08( fecha_ultimo_pago );
                bean.setValor_09( descripcion );
                bean.setValor_10( observacion );
                bean.setValor_11( valor_factura );
                bean.setValor_12( valor_abono );
                bean.setValor_13( valor_saldo );
                bean.setValor_14( valor_tasa );  
                bean.setValor_15( moneda );                
                bean.setValor_16( cantidad_items );
                bean.setValor_17( agencia_facturacion );                   
                bean.setValor_18( agencia_cobro );
                bean.setValor_19( fecha_probable_pago );
                bean.setValor_20(this.getRemesas(distrito,"FAC",factura));
                this.vector.add( bean );
                
            }
            
        }} catch( Exception e ){
            
            e.printStackTrace();
            
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    /** 
     * Funcion publica que actualiza la fecha de posible pago de una factura.
     */
    public void actualizarFactura ( String distrito, String factura, String nueva_fecha_probable_pago, String usuario ) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_UPDATE";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, nueva_fecha_probable_pago);
                st.setString(2, usuario);
                st.setString(3, distrito);
                st.setString(4, factura);
                st.executeUpdate();

            }
        }catch( Exception e ){
            
            e.printStackTrace();            
            
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    
     
     /**
     * Metodo getRemesas , Metodo para buscar las remesas de una factura
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @throws  : SQLException
     * @version : 1.0
     */
    public String getRemesas(String distrito,String tipo_doc,String factura) throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String remesas = "";
        String query = "SQL_BUSCAR_REMESAS";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito);
                st.setString(2, tipo_doc);
                st.setString(3, factura);
                rs = st.executeQuery();
                while (rs.next()) {
                    remesas += rs.getString("numero_remesa") + " - ";
                }
                if (remesas.length() > 0) {
                    remesas = remesas.substring(0, remesas.length() - 2);
                }

            }
        } catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA SQL_BUSCAR_REMESAS" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return remesas;
    }
    
    
}