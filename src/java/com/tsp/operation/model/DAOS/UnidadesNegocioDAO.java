/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.Convenio;
import com.tsp.operation.model.beans.ProcesoMeta;
import com.tsp.operation.model.beans.UnidadesNegocio;
import com.tsp.operation.model.beans.Usuario;
import java.util.ArrayList;

/**
 *
 * @author desarrollo
 */
public interface UnidadesNegocioDAO {

    public ArrayList<UnidadesNegocio> cargarUnidadesNegocio();

    public void guardarUnidadNegocio(String nomUnidad, String codUnidad, String codCentral);

    public ArrayList<Convenio> cargarConvenios();

    public int obtenerIdUnidadNegocio(String nomUnidad);

    public boolean existeUnidadNegocio(String nomUnidad, String codUnidad);

    public String insertarRelConvenio(int idUnidad, String string);

    public ArrayList<Convenio> cargarConveniosNegocio(String idUnidad);

    public UnidadesNegocio cargarDatosUnidadNegocio(String idUnidad);

    public void anularConvenioUnidadNegocio(String idUnidad, String idConvenio);

    public void actualizarUnidadNegocio(String idUnidad, String nomUnidad, String codCentralR, String codigoUnd);


}
