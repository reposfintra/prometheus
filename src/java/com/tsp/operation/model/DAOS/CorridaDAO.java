/**********************************************
 * Nombre clase :  CorridaDAO.java            *
 * Autor : Ing. Henry A.Osorio Gonz�lez       *
 * Modificado Ing Sandra Escalante
 * Fecha : 9 de diciembre de 2005, 09:00 AM   *
 * Version : 1.0                              *
 * Copyright : Fintravalores S.A.        *
 **********************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.Util;

public class CorridaDAO extends MainDAO {
    
    private Corrida corrida;
    private Vector vecCorridas;
    //sescalante
    private List listado;
    private CXP_Doc factura;
    private Vector vector;
    private Vector vectorValor;
    private Vector vectorGrupoValor;
    //sescalante
    private static final String SQL_BUSCAR_CORRIDA = "  SELECT  DISTINCT corrida, " +
    "                                                           get_nombreusuario(creation_user) AS generador" +
    "                                                   FROM    fin.corridas " +
    "                                                   WHERE   dstrct = ? " +
    "                                                           AND agencia_banco=?";
    //hosorio. modificado por sescalante
    private static final String BUSQ_CORRIDAS  ="select	distinct c.corrida,  	" +
    "                                                   n.nombre as proveedor," +
    "                                                   p.branch_code," +
    "                                                   p.bank_account_no," +
    "                                                   a.nombre as agencia," +
    "                                                   p.nit 		" +
    "                                           from    fin.corridas c 	JOIN ( 	select 	p.nit," +
    "                                                                                   p.branch_code," +
    "                                                                                   p.bank_account_no " +
    "                                                                           from 	proveedor p" +
    "                                                                           where  	p.dstrct = ?			" +
    "                                                                   ) p ON (  p.nit = c.beneficiario ),			" +
    "                                                                   agencia a," +
    "                                                                   nit n" +
    "                                           where  	c.dstrct = ? "+
    "                                                   and c.corrida = ?" +
    "                                                   and c.pago='0099-01-01 00:00:00'" +
    "                                                   and a.id_agencia = c.agencia_banco" +
    "                                                   and n.cedula = c.beneficiario" +
    "                                           order by proveedor"; 
    
    //sescalante
    private static String SQL_BUSCAR_FACTURAS = "   SELECT DISTINCT f.documento, " +
    "                                                               f.descripcion, " +
    "                                                               f.vlr_neto AS neto, " +
    "                                                               f.fecha_documento::date AS fchdoc, " +
    "                                                               f.fecha_vencimiento::date AS fchven, " +
    "                                                               f.tipo_documento AS tipodoc "+
    "                                               FROM            fin.corridas c " +
    "                                                                   JOIN fin.cxp_doc f " +
"                                                                   ON (        f.dstrct = ? " +
"                                                                               AND f.corrida = c.corrida " +
    "                                                                           AND f.tipo_documento = c.tipo_documento " +
    "                                                                           AND f.documento = c.documento " +
    "                                                                           AND f.proveedor = c.beneficiario )" +
    "                                               WHERE           c.dstrct = ? " +
    "                                                               AND f.corrida = ? " +
    "                                                               AND f.proveedor= ? " +
    "                                                               AND c.pago='0099-01-01 00:00:00'" +
    "                                               ORDER BY        f.documento";
    
    //hosorio modificado sescalante
    private static final String AUTORIZAR_CORRIDA = "   update  fin.corridas " +
    "                                                   set     pago='now()'," +
    "                                                           usuario_pago=? " +
    "                                                   where   corrida=?" +
    "                                                           AND tipo_documento = ?" +
    "                                                           AND documento = ?" +
    "                                                           AND beneficiario = ?";
    
    /** Creates a new instance of CorridaDAO */
    public CorridaDAO() {
        super("CorridaDAO.xml");
    }
    public CorridaDAO(String dataBaseName) {
        super("CorridaDAO.xml", dataBaseName);
    }
    
    
    
    /**
     * Metodo buscarCorridas, Busca todas las corridas del sistema segun distrito y agencia
     *                        si la agencia es OP muestra todas las corridas
     * @autor : Ing Ivan Dario Gomez 
     * @param : String dstrct, String agencia
     * @version : 2.0
     */
    public void buscarCorridas(String dstrct, String agencia) throws SQLException {
        PreparedStatement st = null;
        Connection con= null;
        ResultSet rs = null;
        listado = new LinkedList();
        corrida = new Corrida();
        String sql ="";
        try{    
                if(agencia.equals("OP")){
                sql = this.obtenerSQL("SQL_CORRIDAS").replaceAll("#AGENCIA#", "");
                }else{
                    sql = this.obtenerSQL("SQL_CORRIDAS").replaceAll("#AGENCIA#", "AND  agencia_banco = '"+agencia+"'");
                }
                con = this.conectarJNDI("SQL_CORRIDAS");//JJCastro fase2
            if (con != null) {
                st = con.prepareStatement(sql);
                st.setString(1, dstrct);
                st.setString(2, dstrct);
                rs = st.executeQuery();
                while (rs.next()) {
                    corrida = new Corrida();
                    corrida.setCorrida(rs.getString(1));
                    corrida.setFechaCreacion("");
                    corrida.setGenerador(rs.getString(2));
                    corrida.setValor(rs.getDouble(3));
                    corrida.setTotalFacturas(rs.getString(4));
                    corrida.setAutorizadas(rs.getString(5));
                    corrida.setFaltantes(rs.getString(6));
                    listado.add(corrida);
                }

            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo buscarCorridaBancos, Busca todas los bancos de una corrida segun distrito y agencia
     *                        si la agencia es OP muestra todos los bancos
     * @autor : Ing Ivan Dario Gomez 
     * @param : String dstrct, String agencia, String numero de corrida
     * @version : 2.0
     */
    public void buscarCorridaBancos(String dstrct, String agencia,String Numcorrida) throws SQLException {
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        listado = new LinkedList();
        String sql = "";
        try {
            if (agencia.equals("OP")) {
                sql = this.obtenerSQL("SQL_CORRIDA_BANCOS").replaceAll("#AGENCIA#", "");
            } else {
                sql = this.obtenerSQL("SQL_CORRIDA_BANCOS").replaceAll("#AGENCIA#", "AND  agencia_banco = '" + agencia + "'");
            }
            con = this.conectarJNDI("SQL_CORRIDA_BANCOS");//JJCastro fase2
            if (con != null) {
                st = con.prepareStatement(sql);
                st.setString(1, dstrct);
                st.setString(2, Numcorrida);
                rs = st.executeQuery();
                while (rs.next()) {
                    corrida = new Corrida();
                    corrida.setCorrida(Numcorrida);
                    corrida.setBanco(rs.getString("banco"));
                    corrida.setSucursal(rs.getString("sucursal"));
                    corrida.setAgencia(rs.getString("agencia"));
                    corrida.setValor(rs.getDouble("valor"));
                    corrida.setTotalFacturas(rs.getString("total"));
                    corrida.setAutorizadas(rs.getString("autorizadas"));
                    corrida.setFaltantes(rs.getString("faltantes"));
                    corrida.setTotal_cheques(rs.getString("cheques"));
                    listado.add(corrida);
                }

            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    
   /**
     * Metodo buscarBancoProveedor, Busca todos los Proveedores de un banco que tenga facturas en una corrida segun distrito y agencia
     *                        si la agencia es OP muestra todos los bancos
     * @autor : Ing Ivan Dario Gomez 
     * @param : String dstrct, String agencia, String numero de corrida, String Banco y String Sucursal
     * @version : 2.0
     */
    public void buscarBancoProveedor(String dstrct, String agencia,String Numcorrida,String banco, String sucursal) throws SQLException {
        PreparedStatement st = null;
        Connection con= null;
        ResultSet rs = null;
        listado = new LinkedList();
        String sql ="";
        try{    
//                if(agencia.equals("OP")){
//                sql = this.obtenerSQL("SQL_BANCO_PROVEEDOR").replaceAll("#AGENCIA#", "");
//                }else{
                    sql = this.obtenerSQL("SQL_BANCO_PROVEEDOR").replaceAll("#AGENCIA#", "AND  coalesce(get_nombreciudad(a.agencia_banco),'-') = '"+agencia+"'");
//                }
            con = this.conectarJNDI("SQL_BANCO_PROVEEDOR");//JJCastro fase2
            if (con != null) {
                st = con.prepareStatement(sql);
                st.setString(1, dstrct);
                st.setString(2, Numcorrida);
                st.setString(3, banco);
                st.setString(4, sucursal);
                //System.out.println("SQL_BANCO_PROVEEDOR-->"+ st.toString());
                rs = st.executeQuery();
                while (rs.next()) {
                    corrida = new Corrida();
                    corrida.setCorrida(Numcorrida);
                    corrida.setBanco(rs.getString("banco"));
                    corrida.setSucursal(rs.getString("sucursal"));
                    corrida.setNitProveedor(rs.getString("beneficiario"));
                    corrida.setNombre(rs.getString("nombre"));
                    corrida.setValor(rs.getDouble("valor"));
                    corrida.setTotalFacturas(rs.getString("total"));
                    corrida.setAutorizadas(rs.getString("autorizadas"));
                    corrida.setFaltantes(rs.getString("faltantes"));
                    corrida.setAgencia(agencia);
                    corrida.setNumerohc(rs.getString("hc"));
                    listado.add(corrida);
                }

            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    } 

    
    
    
    /**
     * Metodo buscarBancoProveedor, Busca todos los Proveedores de un banco que tenga facturas en una corrida segun distrito y agencia
     *                        si la agencia es OP muestra todos los bancos
     * @autor : Ing Ivan Dario Gomez 
     * @param : String dstrct, String agencia, String numero de corrida, String Banco y String Sucursal
     * @version : 2.0
     */
    public void buscarProveedorFactura(String dstrct, String agencia,String Numcorrida,String banco, String sucursal,String NitPro) throws SQLException {
        PreparedStatement st = null;
        Connection con= null;
        ResultSet rs = null;
        listado = new LinkedList();
        String sql ="";
        try{    

                sql = this.obtenerSQL("SQL_PROVEEDOR_FACTURAS").replaceAll("#AGENCIA#", "AND coalesce(get_nombreciudad(agencia_banco),'-') = '"+agencia+"'");
                con = this.conectarJNDI("SQL_PROVEEDOR_FACTURAS");//JJCastro fase2
            if (con != null) {
                st = con.prepareStatement(sql);
                st.setString(1, dstrct);
                st.setString(2, Numcorrida);
                st.setString(3, banco);
                st.setString(4, sucursal);
                st.setString(5, NitPro);
                rs = st.executeQuery();
                while (rs.next()) {
                    corrida = new Corrida();
                    corrida.setDocumento(rs.getString(1));
                    corrida.setPago(rs.getString(2));
                    corrida.setValor(rs.getDouble(3));
                    corrida.setSelected(rs.getString(4));
                    corrida.setTipoPago(rs.getString("tipo_pago"));
                    corrida.setBancoTransferencia(rs.getString("banco_transfer"));
                    corrida.setSucursalTransferencia(rs.getString("suc_transfer"));
                    corrida.setTipoCuentaTransferencia(rs.getString("tipo_cuenta"));
                    corrida.setNoCuentaTransferencia(rs.getString("no_cuenta"));
                    corrida.setCedulaCuentaTransferencia(rs.getString("cedula_cuenta"));
                    corrida.setNombreCuentaTransferencia(rs.getString("nombre_cuenta"));
                    corrida.setCorrida(Numcorrida);
                    corrida.setDistrito(dstrct);
                    corrida.setAgencia(agencia);
                    corrida.setBanco(banco);
                    corrida.setSucursal(sucursal);
                    corrida.setNitProveedor(NitPro);
                    listado.add(corrida);
                }

            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo autorizarCorrida, Autoriza las corridas marcadas y desmarca el resto
     * @autor  Ing. Ivan Dario Gomez
     * @param  String usuario, String[] corridas seleccionadas, Lista de corridas no seleccionadas
     * @version  1.0
     */
    public void autorizarCorrida(String user, String []corridasSelec,List CorridasNoSelec) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        PreparedStatement st2 = null;
        String query = "SQL_AUTORIZAR_CORRIDA";//JJCastro fase2
        String query2 = "SQL_DESMARCAR_CORRIDA";//JJCastro fase2

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                if (corridasSelec != null) {
                    for (int i = 0; i < corridasSelec.length; i++) {
                        st.setString(1, user);
                        st.setString(2, corridasSelec[i]);
                        st.executeUpdate();
                    }

                }
                st2 = con.prepareStatement(this.obtenerSQL(query2));//JJCastro fase2
                if (CorridasNoSelec != null) {
                    Iterator it = CorridasNoSelec.iterator();
                    while (it.hasNext()) {
                        Corrida corrida = (Corrida) it.next();
                        st2.setString(1, corrida.getCorrida());
                        st2.executeUpdate();
                    }
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL AUTORIZAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }    
    
    
    /**
     * Metodo autorizarBanco, Autoriza todas las facturas de un banco y desmarca las 
     *                        facturas de los bancos no seleccionados
     * @autor  Ing. Ivan Dario Gomez
     * @param  String dstrct ,String usuario, Lista de bancos seleccionados, lista de bancos no seleccionados
     * @version  1.0
     */
    public void autorizarBanco(String user, List BancosSelec,List BancosNoSelec) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        PreparedStatement st2 = null;
        String query = "SQL_AUTORIZAR_BANCOS";//JJCastro fase2
        String query2 = "SQL_DESMARCAR_BANCOS";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            if(BancosSelec !=null){
                Iterator ite = BancosSelec.iterator(); 
                 while(ite.hasNext()){
                    Corrida corr = (Corrida) ite.next(); 
                    st.setString(1, user);
                    st.setString(2, corr.getCorrida() );
                    st.setString(3, corr.getBanco()   );
                    st.setString(4, corr.getSucursal());
                    st.setString(5, corr.getAgencia() );
                    st.executeUpdate();
                }
                
            }
            st2 = con.prepareStatement(this.obtenerSQL(query2));//JJCastro fase2
            if(BancosNoSelec != null){
                Iterator it = BancosNoSelec.iterator(); 
                while(it.hasNext()){
    		    Corrida corrida = (Corrida) it.next();
                    st2.setString(1, corrida.getCorrida() );
                    st2.setString(2, corrida.getBanco()   );
                    st2.setString(3, corrida.getSucursal());
                    st2.setString(4, corrida.getAgencia() );
                    st2.executeUpdate();
                }
            }
        }}catch(SQLException e){
            throw new SQLException("ERROR AL AUTORIZAR BANCOS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    } 
    
    
    
    /**
     * Metodo autorizarProveedor, Autoriza todas las facturas de un proveedores y desmarca las 
     *                        facturas de los proveedores no seleccionados
     * @autor  Ing. Ivan Dario Gomez
     * @param  String usuario, Lista de bancos seleccionados, lista de bancos no seleccionados
     * @version  1.0
     */
    public void autorizarProveedor(String dstrct,String user, List ProveedorSelec,List ProveedorNoSelec) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        PreparedStatement st2 = null;
        String query = "SQL_AUTORIZAR_PROVEEDOR";//JJCastro fase2
        String query2 = "SQL_DESMARCAR_PROVEEDOR";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                if (ProveedorSelec != null) {
                    Iterator ite = ProveedorSelec.iterator();
                    while (ite.hasNext()) {
                        Corrida corr = (Corrida) ite.next();
                        st.setString(1, user);
                        st.setString(2, dstrct);
                        st.setString(3, corr.getCorrida());
                        st.setString(4, corr.getBanco());
                        st.setString(5, corr.getSucursal());
                        st.setString(6, corr.getAgencia());
                        st.setString(7, corr.getNitProveedor());
                        st.executeUpdate();
                    }

                }
                st2 = con.prepareStatement(this.obtenerSQL(query2));//JJCastro fase2
                if (ProveedorNoSelec != null) {
                    Iterator it = ProveedorNoSelec.iterator();
                    while (it.hasNext()) {
                        Corrida corrida = (Corrida) it.next();
                        st2.setString(1, dstrct);
                        st2.setString(2, corrida.getCorrida());
                        st2.setString(3, corrida.getBanco());
                        st2.setString(4, corrida.getSucursal());
                        st2.setString(5, corrida.getAgencia());
                        st2.setString(6, corrida.getNitProveedor());
                        st2.executeUpdate();
                    }
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL AUTORIZAR PROVEEDORES " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    } 
    
    
    
    /**
     * Metodo autorizarFactura, Autoriza todas las facturas de un proveedores y desmarca las 
     *                        facturas de los proveedores no seleccionados
     * @param dstrct
     * @param user
     * @param FacturaSelec
     * @param FacturaNoSelec
     * @throws java.sql.SQLException
     * @autor  Ing. Ivan Dario Gomez
     * @version  1.0
     */
    public void autorizarFactura(String dstrct,String user, List FacturaSelec,List FacturaNoSelec)throws SQLException {
        Connection con = null;
        StringStatement st = null;
        StringStatement st2 = null;
        String query = "SQL_AUTORIZAR_FACTURA";
        String query2 = "SQL_DESMARCAR_FACTURA";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                con.setAutoCommit(false);
                Statement stmt=con.createStatement();                
                if (FacturaSelec != null) {
                    Iterator ite = FacturaSelec.iterator();
                    while (ite.hasNext()) {
                        st = new StringStatement(this.obtenerSQL(query), true);
                        Corrida corr = (Corrida) ite.next();
                        st.setString(1, user);
                        st.setString(2, dstrct);
                        st.setString(3, corr.getCorrida());
                        st.setString(4, corr.getBanco());
                        st.setString(5, corr.getSucursal());
                        st.setString(6, corr.getAgencia());
                        st.setString(7, corr.getNitProveedor());
                        st.setString(8, corr.getDocumento());
                        stmt.addBatch(st.getSql());
                        
                    }

                }                
                
                if (FacturaNoSelec != null) {
                    Iterator it = FacturaNoSelec.iterator();
                    while (it.hasNext()) {
                        st2 = new StringStatement(this.obtenerSQL(query2), true);
                        Corrida corrida = (Corrida) it.next();
                        st2.setString(1, dstrct);
                        st2.setString(2, corrida.getCorrida());
                        st2.setString(3, corrida.getBanco());
                        st2.setString(4, corrida.getSucursal());
                        st2.setString(5, corrida.getAgencia());
                        st2.setString(6, corrida.getNitProveedor());
                        st2.setString(7, corrida.getDocumento());
                        stmt.addBatch(st2.getSql());                        
                    }
                }
                
                //ejecutamos todo en una solo peticion a la bd
                stmt.executeBatch();
                stmt.clearBatch();
                con.commit();
                stmt.close();
            }
        } catch (Exception e) {
            throw new SQLException("ERROR AL AUTORIZAR FACTURAS " + e.getMessage() + "" + e.getCause());
        } finally {//JJCastro fase2
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }
    
    
    /**
     * Metodo getVectorCorridas, obtiene el valor del vector vecCorridas
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @return Vector
     * @version : 1.0
     */
    public Vector getVectorCorridas() {
        return this.vecCorridas;
    }
    
    /**
     * Metodo buscarListaCorridas, busca las corridas segun parametros de busqueda
     * @autor : Ing. Sandra Escalante    
     * @param : String banco, String sucursal, String corrida, String distrito
     * @version : 1.0
     */
    public void buscarListaCorridas(String corr, String dstrct) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        listado = new LinkedList();
        corrida = null;
        String query = "BUSQ_CORRIDAS";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null){               
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dstrct);                            
                st.setString(2, dstrct);
                st.setString(3, corr);
                rs = st.executeQuery();
                while (rs.next()) {
                    corrida = new Corrida();
                    corrida.setCorrida(rs.getString("corrida"));                      
                    corrida.setAgencia(rs.getString("agencia"));
                    corrida.setBranch_code(rs.getString("branch_code"));
                    corrida.setBank_account_no(rs.getString("bank_account_no"));
                    corrida.setProveedor(rs.getString("proveedor"));
                    corrida.setNitProveedor(rs.getString("nit"));
                    listado.add(corrida);
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
     /**
     * Metodo getListado, obtiene la variable listado
     * @autor : Ing. Sandra Escalante
     * @return Vector
     * @version : 1.0
     */
    public List getListado() {
        return this.listado;
    }
    
    /**
     * Metodo buscarFacturasxCP, busca las facturas correspondientes a una corrida y proveedor dado
     * @autor : Ing. Sandra Escalante    
     * @param : String nro de la corrida, String codigo del proveedor
     * @version : 1.0
     */
    public void buscarFacturasxCP(String corrida, String prov, String dstrct) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        vecCorridas = new Vector();
        factura = new CXP_Doc();
        String query = "SQL_BUSCAR_FACTURAS";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dstrct);                            
                st.setString(2, dstrct);  
                st.setString(3, corrida);                            
                st.setString(4, prov);                
                rs = st.executeQuery();
                while (rs.next()) {
                    factura = new CXP_Doc();
                    factura.setDocumento(rs.getString("documento"));
                    factura.setDescripcion(rs.getString("descripcion"));
                    factura.setVlr_neto(rs.getFloat("neto"));
                    factura.setFecha_documento(rs.getString("fchdoc"));
                    factura.setFecha_vencimiento(rs.getString("fchven"));
                    factura.setTipo_documento(rs.getString("tipodoc"));
                    vecCorridas.add(factura);
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR FACTURAS (Corrida - Proveedor) " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    
    
    
    
    
    
    //funcion para el reporte Corridas trasferencias x propietario
/**
 * 
 * @param corrida
 * @throws Exception
 */
    public void busquedaXCorridas(String corrida ) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        BeanGeneral BeanGrupo = null;
        vector = new Vector();
        String query = "SQL_GRUPO_BENEFICIARIOS";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, corrida);
                String TempBeneficiario;
                rs = st.executeQuery();
                while (rs.next()) {
                    BeanGrupo = new BeanGeneral();
                    BeanGrupo.setValor_01((rs.getString("nombre") != null) ? rs.getString("nombre") : "");
                    BeanGrupo.setValor_02((rs.getString("NoVeces") != null) ? rs.getString("NoVeces") : "");
                    BeanGrupo.setValor_03((rs.getString("beneficiario") != null) ? rs.getString("beneficiario") : "");
                    BeanGrupo.setValor_04((rs.getString("corrida") != null) ? rs.getString("corrida") : "");
                    BeanGrupo.setValor_05((rs.getString("valortotal") != null) ? rs.getString("valortotal") : "");
                    BeanGrupo.setVec(this.cargarCorridas(BeanGrupo.getValor_03(), corrida));
                    this.vector.addElement(BeanGrupo);
                }
            }
        }catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error al busquedaXCorridas [CorridaDAO.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    
    
    
    
    
    
    
    //FUNCION PARA CARGAR EN EL VECTOR LAS CORRIDAS DEPENDIENDO EL BENEFICIARIO
/**
 * 
 * @param beneficiario
 * @param Corrida
 * @return
 * @throws Exception
 */
    public Vector cargarCorridas(String beneficiario, String Corrida) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        BeanGeneral BeanGrupoBeneficiarios = null;
        vectorValor = new Vector();
        int Corridas = 0;
        int cont = 1;
        String pago = "TRANSFERENCIA";
        String query = "SQL_CORRIDASXPROPIETARIO";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, beneficiario);
                st.setString(2, Corrida);
                String TempBeneficiario;
                rs = st.executeQuery();
                while (rs.next()) {
                    BeanGrupoBeneficiarios = new BeanGeneral();
                    BeanGrupoBeneficiarios.setValor_01((rs.getString("corrida") != null) ? rs.getString("corrida") : "");
                    BeanGrupoBeneficiarios.setValor_02((rs.getString("beneficiario") != null) ? rs.getString("beneficiario") : "");
                    BeanGrupoBeneficiarios.setValor_03((rs.getString("nombre") != null) ? rs.getString("nombre") : "");
                    BeanGrupoBeneficiarios.setValor_04((rs.getString("banco_transfer") != null) ? rs.getString("banco_transfer") : "");
                    BeanGrupoBeneficiarios.setValor_05((rs.getString("documento") != null) ? rs.getString("documento") : "");
                    if (rs.getString("tipo_pago").equals("T")) {
                        BeanGrupoBeneficiarios.setValor_06(pago);
                    }
                    if (rs.getString("tipo_pago").equals("")) {
                        BeanGrupoBeneficiarios.setValor_06("");
                    }
                    BeanGrupoBeneficiarios.setValor_07((rs.getString("no_cuenta") != null) ? rs.getString("no_cuenta") : "");
                    BeanGrupoBeneficiarios.setValor_08((rs.getString("cedula_cuenta") != null) ? rs.getString("cedula_cuenta") : "");
                    BeanGrupoBeneficiarios.setValor_09((rs.getString("tipo_cuenta") != null) ? rs.getString("tipo_cuenta") : "");
                    BeanGrupoBeneficiarios.setValor_10((rs.getString("nombre_cuenta") != null) ? rs.getString("nombre_cuenta") : "");
                    BeanGrupoBeneficiarios.setValor_11((rs.getString("valor") != null) ? rs.getString("valor") : "");
                    BeanGrupoBeneficiarios.setValor_13("" + cont);
                    BeanGrupoBeneficiarios.setValor_14((rs.getString("impresion") != null) ? rs.getString("impresion") : "");
                    BeanGrupoBeneficiarios.setValor_15((rs.getString("tipo_documento") != null) ? rs.getString("tipo_documento") : "");
                    this.vectorValor.add(BeanGrupoBeneficiarios);
                    cont++;
                }

            }
        }catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error al Busqueda cargarCorridas[CorridaDAO.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return vectorValor;
    }
    
    
    
    
    
    
    
    
    
    /**********************************************************************************/
/**
 * 
 * @param Propietario
 * @throws Exception
 */
    public void busquedaXPropietario(String Propietario ) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        BeanGeneral BeanCorrida = null;
        vector = new Vector();
        String PropietarioEnvPAgina = Propietario;
        String query = "SQL_TRANSFERENCIASXPROPIETARIO";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                int cont = 1;
                st.setString(1, Propietario);
                String TempBeneficiario;
                String pago = "Tranferencia";

                rs = st.executeQuery();
                while (rs.next()) {
                    BeanCorrida = new BeanGeneral();
                    BeanCorrida.setValor_01((rs.getString("corrida") != null) ? rs.getString("corrida") : "");
                    BeanCorrida.setValor_02((rs.getString("beneficiario") != null) ? rs.getString("beneficiario") : "");
                    BeanCorrida.setValor_03((rs.getString("nombre") != null) ? rs.getString("nombre") : "");
                    BeanCorrida.setValor_04((rs.getString("banco_transfer") != null) ? rs.getString("banco_transfer") : "");
                    BeanCorrida.setValor_05((rs.getString("documento") != null) ? rs.getString("documento") : "");
                    if (rs.getString("tipo_pago").equals("T")) {
                        BeanCorrida.setValor_06(pago);
                    }
                    if (rs.getString("tipo_pago").equals("")) {
                        BeanCorrida.setValor_06("");
                    }
                    BeanCorrida.setValor_07((rs.getString("no_cuenta") != null) ? rs.getString("no_cuenta") : "");
                    BeanCorrida.setValor_08((rs.getString("cedula_cuenta") != null) ? rs.getString("cedula_cuenta") : "");
                    BeanCorrida.setValor_09((rs.getString("tipo_cuenta") != null) ? rs.getString("tipo_cuenta") : "");
                    BeanCorrida.setValor_10((rs.getString("nombre_cuenta") != null) ? rs.getString("nombre_cuenta") : "");
                    BeanCorrida.setValor_11((rs.getString("valor") != null) ? rs.getString("valor") : "");
                    BeanCorrida.setValor_13("" + cont);
                    BeanCorrida.setValor_14((rs.getString("impresion") != null) ? rs.getString("impresion") : "");
                    BeanCorrida.setValor_15((rs.getString("tipo_documento") != null) ? rs.getString("tipo_documento") : "");


                    this.vector.addElement(BeanCorrida);
                    cont++;
                }

            }
        }catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error al Busqueda de la Corrida [CorridaDAO.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }



    /******************************************/
/**
 * 
 * @param Corrida
 * @throws Exception
 */
    public void busquedaXHiloCorridas(String Corrida ) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        BeanGeneral BeanCorrida = null;
        vector = new Vector();
        String PropietarioEnvPAgina = Corrida;
        String query = "SQL_HILO_CORRIDA";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            int cont = 1;
            st.setString(1, Corrida );
            String TempBeneficiario;
            String pago = "Tranferencia";
            rs = st.executeQuery();
                while(rs.next()){
                    BeanCorrida = new BeanGeneral();
                    BeanCorrida.setValor_01 ((rs.getString("corrida")!=null)?rs.getString("corrida"):"");
                    BeanCorrida.setValor_02 ((rs.getString("beneficiario")!=null)?rs.getString("beneficiario"):"");
                    BeanCorrida.setValor_03 ((rs.getString("nombre")!=null)?rs.getString("nombre"):"");
                    BeanCorrida.setValor_04 ((rs.getString("banco_transfer")!=null)?rs.getString("banco_transfer"):"");
                    BeanCorrida.setValor_05 ((rs.getString("documento")!=null)?rs.getString("documento"):"");
                    if (rs.getString("tipo_pago").equals("T")){
                        BeanCorrida.setValor_06 (pago);
                    }if(rs.getString("tipo_pago").equals("")){
                        BeanCorrida.setValor_06 ("");
                    }
                    BeanCorrida.setValor_07 ((rs.getString("no_cuenta")!=null)?rs.getString("no_cuenta"):"");
                    BeanCorrida.setValor_08 ((rs.getString("cedula_cuenta")!=null)?rs.getString("cedula_cuenta"):"");
                    BeanCorrida.setValor_09 ((rs.getString("tipo_cuenta")!=null)?rs.getString("tipo_cuenta"):"");
                    BeanCorrida.setValor_10 ((rs.getString("nombre_cuenta")!=null)?rs.getString("nombre_cuenta"):"");
                    BeanCorrida.setValor_11 ((rs.getString("valor")!=null)?rs.getString("valor"):"");
                    BeanCorrida.setValor_13 (""+cont);
                    BeanCorrida.setValor_14 ((rs.getString("impresion")!=null)?rs.getString("impresion"):"");
                    BeanCorrida.setValor_15 ((rs.getString("tipo_documento")!=null)?rs.getString("tipo_documento"):"");
                    this.vector.addElement( BeanCorrida );
                    
                     cont++;
                     BeanCorrida = null;//Liberar Espacio JJCastro
            }
         
        }}catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error al Busqueda de la Corrida [CorridaDAO.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    
/**
 *
 * @param Propietario
 * @param Corrida
 * @throws Exception
 */
    public void busquedaXTransferencias(String Propietario,String Corrida ) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        int Corridas = 0;
        BeanGeneral BeanCorrida = null;
        vector = new Vector();
        String PropietarioEnvPAgina = Propietario;
        Corridas = Integer.parseInt(Corrida);
        String query = "SQL_TRANSFERENCIAS";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            int cont = 1;
            st.setString(1, Propietario );
            st.setInt(2, Corridas );
            String TempBeneficiario;
            String pago = "Tranferencia";
            rs = st.executeQuery();
                while(rs.next()){
                    BeanCorrida = new BeanGeneral();
                    BeanCorrida.setValor_01 ((rs.getString("corrida")!=null)?rs.getString("corrida"):"");
                    BeanCorrida.setValor_02 ((rs.getString("beneficiario")!=null)?rs.getString("beneficiario"):"");
                    BeanCorrida.setValor_03 ((rs.getString("nombre")!=null)?rs.getString("nombre"):"");
                    BeanCorrida.setValor_04 ((rs.getString("banco_transfer")!=null)?rs.getString("banco_transfer"):"");
                    BeanCorrida.setValor_05 ((rs.getString("documento")!=null)?rs.getString("documento"):"");
                    if (rs.getString("tipo_pago").equals("T")){
                        BeanCorrida.setValor_06 (pago);
                    }if(rs.getString("tipo_pago").equals("")){
                        BeanCorrida.setValor_06 ("");
                    }
                    BeanCorrida.setValor_07 ((rs.getString("no_cuenta")!=null)?rs.getString("no_cuenta"):"");
                    BeanCorrida.setValor_08 ((rs.getString("cedula_cuenta")!=null)?rs.getString("cedula_cuenta"):"");
                    BeanCorrida.setValor_09 ((rs.getString("tipo_cuenta")!=null)?rs.getString("tipo_cuenta"):"");
                    BeanCorrida.setValor_10 ((rs.getString("nombre_cuenta")!=null)?rs.getString("nombre_cuenta"):"");
                    BeanCorrida.setValor_11 ((rs.getString("valor")!=null)?rs.getString("valor"):"");
                    BeanCorrida.setValor_13 (""+cont);
                    BeanCorrida.setValor_14 ((rs.getString("impresion")!=null)?rs.getString("impresion"):"");
                    BeanCorrida.setValor_15 ((rs.getString("tipo_documento")!=null)?rs.getString("tipo_documento"):"");
                    
                    this.vector.addElement( BeanCorrida );
                     cont++;
            }
         
        }}catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error al Busqueda de la Corrida [CorridaDAO.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    
    
    /********************************************************************/
    //funcion que realiza la suma total del valor propietario ingresos y egresos
    public void SumaValorCorridas(String Propietario ) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        BeanGeneral BeanCorrida = null;
        vectorValor = new Vector();
        String PropietarioEnvPAgina = Propietario;
        String query = "SQL_SUMAVALOR_CORRIDAS";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, Propietario);
                rs = st.executeQuery();
                if (rs.next()) {
                    BeanCorrida = new BeanGeneral();
                    BeanCorrida.setValor_01((rs.getString("valortotal") != null) ? rs.getString("valortotal") : "");
                    this.vectorValor.addElement(BeanCorrida);
                }

            }
        }catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error al Busqueda de la Corrida [CorridaDAO.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    } 
    /**
       * Getter for property vector.
       * @return Value of property vector.
       */
      public java.util.Vector getVector() {
          return vector;
      }
      
      /**
       * Getter for property vectorValor.
       * @return Value of property vectorValor.
       */
      public java.util.Vector getVectorValor() {
          return vectorValor;
      }      
      
      /**
       * Setter for property vectorValor.
       * @param vectorValor New value of property vectorValor.
       */
      public void setVectorValor(java.util.Vector vectorValor) {
          this.vectorValor = vectorValor;
      }
      
      
      
      public void cxpro(String Propietario, String Corridas ) throws Exception {
        Connection con = null;//JJCastro fase2
          PreparedStatement st = null;
          ResultSet rs = null;
          BeanGeneral BeanCorrida = null;
          vector = new Vector();
          String PropietarioEnvPAgina = Propietario;
          String query = "SQL_CORRIDASXPROPIETARIO";//JJCastro fase2
          try {
              con = this.conectarJNDI(query);
              if (con != null) {
                  st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                  int cont = 1;
                  st.setString(1, Propietario);
                  st.setString(2, Corridas);
                  String TempBeneficiario;
                  String pago = "Tranferencia";
                  rs = st.executeQuery();
                  while (rs.next()) {
                      BeanCorrida = new BeanGeneral();
                      BeanCorrida.setValor_01((rs.getString("corrida") != null) ? rs.getString("corrida") : "");
                      BeanCorrida.setValor_02((rs.getString("beneficiario") != null) ? rs.getString("beneficiario") : "");
                      BeanCorrida.setValor_03((rs.getString("nombre") != null) ? rs.getString("nombre") : "");
                      BeanCorrida.setValor_04((rs.getString("banco_transfer") != null) ? rs.getString("banco_transfer") : "");
                      BeanCorrida.setValor_05((rs.getString("documento") != null) ? rs.getString("documento") : "");
                      if (rs.getString("tipo_pago").equals("T")) {
                          BeanCorrida.setValor_06(pago);
                      }
                      if (rs.getString("tipo_pago").equals("")) {
                          BeanCorrida.setValor_06("");
                      }
                      BeanCorrida.setValor_07((rs.getString("no_cuenta") != null) ? rs.getString("no_cuenta") : "");
                      BeanCorrida.setValor_08((rs.getString("cedula_cuenta") != null) ? rs.getString("cedula_cuenta") : "");
                      BeanCorrida.setValor_09((rs.getString("tipo_cuenta") != null) ? rs.getString("tipo_cuenta") : "");
                      BeanCorrida.setValor_10((rs.getString("nombre_cuenta") != null) ? rs.getString("nombre_cuenta") : "");
                      BeanCorrida.setValor_11((rs.getString("valor") != null) ? rs.getString("valor") : "");
                      BeanCorrida.setValor_13("" + cont);
                      BeanCorrida.setValor_14((rs.getString("impresion") != null) ? rs.getString("impresion") : "");
                      BeanCorrida.setValor_15((rs.getString("tipo_documento") != null) ? rs.getString("tipo_documento") : "");


                      this.vector.addElement(BeanCorrida);
                      cont++;
                  }

              }
          }catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error al Busqueda de la Corrida [CorridaDAO.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
      
      /**
     * Metodo buscarCorridasUsuarios, Busca todas las corridas del sistema segun distrito y agencia
     *                        si la agencia es OP muestra todas las corridas
     * @autor : Ing Jose de la rosa
     * @param : String dstrct, String agencia
     * @version : 2.0
     */
    public void buscarCorridasUsuarios(String dstrct, String agencia, String usuario) throws SQLException {
        PreparedStatement st = null;
        Connection con= null;
        ResultSet rs = null;
        listado = new LinkedList();
        corrida = new Corrida();
        String sql ="";

        try{
                if(agencia.equals("OP")){
                sql = this.obtenerSQL("SQL_CORRIDAS_USUARIO").replaceAll("#AGENCIA#", "");
                }else{
                    sql = this.obtenerSQL("SQL_CORRIDAS_USUARIO").replaceAll("#AGENCIA#", "AND  agencia_banco = '"+agencia+"'");
                }
                con = this.conectarJNDI("SQL_CORRIDAS_USUARIO");//JJCastro fase2
                st = con.prepareStatement(sql);
                st.setString(1, dstrct);
                st.setString(2, usuario);
                st.setString(3, dstrct);
                st.setString(4, usuario);
                st.setString(5, dstrct);
                st.setString(6, usuario);

                rs = st.executeQuery();
                while (rs.next()) {
                    corrida = new Corrida();
                    corrida.setCorrida      (rs.getString("corrida"));
                    corrida.setFechaCreacion(""); 
                    corrida.setGenerador    (rs.getString("generador"));
                    corrida.setValor        (rs.getDouble("valor"));
                    corrida.setTotalFacturas(rs.getString("total"));
                    corrida.setAutorizadas  (rs.getString("autorizadas"));
                    corrida.setFaltantes    (rs.getString("faltantes"));
                    corrida.setTotal_cheques(rs.getString("cheques"));
                    listado.add(corrida);                    
                }
                
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    /**
     * Metodo excluirValoresMenores, excluye los las facturas que tengan el valor menor al digitado
     * @autor  Ing. Ivan Dario Gomez
     * @param String dstrct, String corrida, double valor
     * @version  1.0
     */
    public String excluirValoresMenores(String dstrct, String corrida, double valor) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        StringStatement st2 = null;
        ResultSet rs = null;
        listado = new LinkedList();
        String sql = "";
        String query = "SQL_LISTA_FACTURAS";//JJCastro fase2
        String query2 = "SQL_DELETE_CORRIDAS";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct );
            st.setString(2, corrida);
            st.setDouble(3, valor);
            rs = st.executeQuery();
            st2 = new StringStatement(this.obtenerSQL(query2), true);//JJCastro fase2
            while (rs.next()) {
                st2.setString(1, rs.getString("dstrct"));
                st2.setString(2, rs.getString("corrida"));
                st2.setString(3, rs.getString("tipo_documento"));
                st2.setString(4, rs.getString("documento"));
                st2.setString(5, rs.getString("beneficiario"));
                
                st2.setString(6, rs.getString("dstrct"));
                st2.setString(7, rs.getString("beneficiario"));
                st2.setString(8, rs.getString("tipo_documento"));
                st2.setString(9, rs.getString("documento"));
                sql+= st2.getSql();//JJCastro fase2
               
                
            }
           
        }}catch(Exception e){
            throw new SQLException("ERROR AL SQL_LISTA_FACTURAS " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (st2  != null){ try{ st2 = null;            } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sql;
    } 

}

