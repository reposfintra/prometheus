/*
* Nombre        NegocioTrazabilidadDAO.java
* Descripci�n   Clase para el acceso a los datos de negocios_trazabilidad
* Autor         Iris vargas
* Fecha         25 de noviembre de 2011, 11:57 AM
* Versi�n       1.0
* Coyright      Geotech S.A.
*/

package com.tsp.operation.model.DAOS;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.CmbGeneralScBeans;
import com.tsp.operation.model.beans.NegocioTrazabilidad;
import com.tsp.operation.model.beans.NegocioAsociacion;
import com.tsp.operation.model.beans.StringStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import com.tsp.operation.model.beans.NegocioAnalista;
import com.tsp.operation.model.beans.Usuario;
import java.sql.SQLException;

/**
 * Clase para el acceso a los datos de negocios_trazabilidad
 * @author Iris Vargas
 */
public class NegocioTrazabilidadDAO extends MainDAO {
    

    public NegocioTrazabilidadDAO() {
        super("NegocioTrazabilidadDAO.xml");
    }

    public NegocioTrazabilidadDAO(String dataBaseName) {
        super("NegocioTrazabilidadDAO.xml", dataBaseName);
    }

    /**
     * Sql para insertar registro en la tabla negocios_trazabilidad
     * @return sql de insersion de negocios_trazabilidad
     * @param negtraza objeto NegocioTrazabilidad 
     * @throws Exception cuando hay error
     */
    public String insertNegocioTrazabilidad(NegocioTrazabilidad  negtraza){
        String cadena = "";
        String query = "INS_NEG_TRAZABILIDAD";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1,  negtraza.getDstrct());
            st.setInt(2, Integer.parseInt(negtraza.getNumeroSolicitud()));
            st.setString(3,  negtraza.getActividad());
            st.setString(4,  negtraza.getUsuario());
            st.setString(5,  negtraza.getComentarios());
            st.setString(6,  negtraza.getConcepto());
            st.setString(7,  negtraza.getCausal());
            st.setString(8, negtraza.getComentario_standby());
            cadena = st.getSql();
            cadena=cadena.replaceAll("#NEGOCIO#", negtraza.getCodNeg()!=null?"'"+negtraza.getCodNeg()+"'":"null");
            st = null;
        }
        catch (Exception e) {
            System.out.println("Error en insertNegocioTrazabilidad (NegocioTrazabilidadDAO)"+e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    /**
     * Sql para actualizar la actividad de un negocio
     * @return sql de insersion de negocios_trazabilidad
     * @param negocio   codigo de negocio
     * @param actividad  actividad a actualizar
     * @throws Exception cuando hay error
     */
    public String updateActividad(String negocio, String actividad){
        String cadena = "";
        String query = "UPD_ACTIVIDAD";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, actividad);
            st.setString(2,  negocio);            
            cadena = st.getSql();
            st = null;
        }
        catch (Exception e) {
            System.out.println("Error en updateActividad (NegocioTrazabilidadDAO)"+e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

     /**
     * Sql para actualizar el estado de un negocio
     * @return sql que actualiza el estado de un negocio
     * @param negocio   codigo de negocio
     * @param estado  estado a actualizar
     * @throws Exception cuando hay error
     */
    public String updateEstadoNeg(String negocio, String estado){
        String cadena = "";
        String query = "UPD_ESTADO_NEG";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, estado);
            st.setString(2,  negocio);
            cadena = st.getSql();
            st = null;
        }
        catch (Exception e) {
            System.out.println("Error en updateEstadoNeg (NegocioTrazabilidadDAO)"+e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    /**
     * Sql para actualizar el estado de un formulario
     * @return sql que actualiza el estado de un formulario
     * @param form   numero de formulario
     * @param estado  estado a actualizar
     * @throws Exception cuando hay error
     */
    public String updateEstadoForm(String form, String estado){
        String cadena = "";
        String query = "UPD_ESTADO_FORM";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, estado);
            st.setString(2,  form);
            cadena = st.getSql();
            st = null;
        }
        catch (Exception e) {
            System.out.println("Error en updateEstadoForm (NegocioTrazabilidadDAO)"+e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    /**
     * Sql para buscar la trazabilidad de una solicitud
     * @return lista de las trazas de la solicitud
     * @param num_solicitud 
     * @throws Exception cuando hay error
     */
     public ArrayList< NegocioTrazabilidad> buscarNegocioTrazabilidad(int num_solicitud) throws Exception{
        NegocioTrazabilidad bean = null;
        ArrayList list = new ArrayList();
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        String query="SRC_NEGOCIO_TRAZABILIDAD";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con=conectarJNDI(query);
            ps=con.prepareStatement(sql);
            ps.setInt(1, num_solicitud);
            rs=ps.executeQuery();
            while(rs.next()){
                bean = new NegocioTrazabilidad();
               bean=bean.load(rs);
               list.add(bean);
            }
        }
        catch (Exception e) {
            System.out.println("Error en buscarNegocioTrazabilidad: "+e.toString());
            e.printStackTrace();
            throw new Exception("Error en buscarNegocioTrazabilidad: "+e.toString());
        }
        finally {
            if(ps!=null){ try{ps.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return list;
    }
     
      /**
     * Sql para buscar la trazabilidad de una solicitud y actividad
     * @return la traza de la actividad
     * @param num_solicitud
     * @throws Exception cuando hay error
     */
     public  NegocioTrazabilidad buscarTraza(int num_solicitud, String actividad) throws Exception{
        NegocioTrazabilidad bean = null;
        ArrayList list = new ArrayList();
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        String query="SRC_TRAZA";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con=conectarJNDI(query);
            ps=con.prepareStatement(sql);
            ps.setInt(1, num_solicitud);
            ps.setString(2, actividad);
            rs=ps.executeQuery();
            if(rs.next()){
                bean = new NegocioTrazabilidad();
               bean=bean.load(rs);
              
            }
        }
        catch (Exception e) {
            System.out.println("Error en buscarTraza: "+e.toString());
            e.printStackTrace();
            throw new Exception("Error en buscarTraza: "+e.toString());
        }
        finally {
            if(ps!=null){ try{ps.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return bean;
    }
     
        /**
     * Sql para buscar la ultima traza de una solicitud 
     * @return la traza de la actividad
     * @param num_solicitud
     * @throws Exception cuando hay error
     */
     public  NegocioTrazabilidad buscarUltimaTraza(int num_solicitud) throws Exception{
        NegocioTrazabilidad bean = null;
       // ArrayList list = new ArrayList();
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        String query="SRC_ULTIMA_TRAZA";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con=conectarJNDI(query);
            ps=con.prepareStatement(sql);
            ps.setInt(1, num_solicitud);
            //ps.setString(2, actividad);
            rs=ps.executeQuery();
            if(rs.next()){
               bean = new NegocioTrazabilidad();
               bean=bean.load(rs);
              
            }
        }
        catch (Exception e) {
            System.out.println("Error en buscarTraza: "+e.toString());
            e.printStackTrace();
            throw new Exception("Error en buscarTraza: "+e.toString());
        }
        finally {
            if(ps!=null){ try{ps.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return bean;
    }
     
            /**
     * Sql para buscar la actividad traza de una solicitud 
     * @return la traza de la actividad
     * @param num_solicitud
     * @throws Exception cuando hay error
     */
     public  NegocioTrazabilidad buscarDevolverSolTraza(int num_solicitud) throws Exception{
        NegocioTrazabilidad bean = null;
       // ArrayList list = new ArrayList();
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        String query="SRC_DEVOLVERSOL_TRAZA";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con=conectarJNDI(query);
            ps=con.prepareStatement(sql);
            ps.setInt(1, num_solicitud);
            //ps.setString(2, actividad);
            rs=ps.executeQuery();
            if(rs.next()){
               bean = new NegocioTrazabilidad();
               bean=bean.load(rs);
              
            }
        }
        catch (Exception e) {
            System.out.println("Error en buscarTraza: "+e.toString());
            e.printStackTrace();
            throw new Exception("Error en buscarTraza: "+e.toString());
        }
        finally {
            if(ps!=null){ try{ps.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return bean;
    }

     /**
     * Genera una lista de ocupaciones
     * @return lista con los datos encontrados
     * @throws Exception cuando hay error
     */
    public ArrayList listadotablagen(String tabla, String ref) throws Exception{
        ArrayList cadena = new ArrayList();
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        String query="SRC_TBGEN";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con=conectarJNDI(query);
            ps=con.prepareStatement(sql);
            ps.setString(1, tabla);
            ps.setString(2, ref);
            rs=ps.executeQuery();
            while(rs.next()){
                cadena.add(rs.getString("table_code")+";_;"+rs.getString("dato"));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en listadotablagen: "+e.toString());
        }
        finally {
            if(ps!=null){ try{ps.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return cadena;
    }
    
        /**
     * Sql para buscar la trazabilidad de una solicitud
     * @return lista de las trazas de la solicitud
     * @param num_solicitud
     * @throws Exception cuando hay error
     */
     public ArrayList< NegocioTrazabilidad> buscarNegocioTrazabilidadRechazado(int num_solicitud) throws Exception{
        NegocioTrazabilidad bean = null;
        ArrayList list = new ArrayList();
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        String query="SRC_NEGOCIO_TRAZABILIDAD_RECHAZADO";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con=conectarJNDI(query);
            ps=con.prepareStatement(sql);
            ps.setInt(1, num_solicitud);
            rs=ps.executeQuery();
            while(rs.next()){
                bean = new NegocioTrazabilidad();
               bean=bean.load(rs);
               list.add(bean);
            }
        }
        catch (Exception e) {
            System.out.println("Error en buscarNegocioTrazabilidad: "+e.toString());
            e.printStackTrace();
            throw new Exception("Error en buscarNegocioTrazabilidad: "+e.toString());
        }
        finally {
            if(ps!=null){ try{ps.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return list;
    }





      public ArrayList< NegocioTrazabilidad> buscarNegocioTrazabilidadDatacredito(int num_solicitud) throws Exception{
        NegocioTrazabilidad bean = null;
        ArrayList list = new ArrayList();
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        String query="CONSULTAR_RESPUESTA_PERSONALIZADA";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con=conectarJNDI(query);
            ps=con.prepareStatement(sql);
            ps.setInt(1, num_solicitud);
            rs=ps.executeQuery();
            while(rs.next()){
                bean = new NegocioTrazabilidad();
               bean=bean.load(rs);
               list.add(bean);
            }
        }
        catch (Exception e) {
            System.out.println("Error en buscarNegocioTrazabilidadDatacredito: "+e.toString());
            e.printStackTrace();
            throw new Exception("Error en buscarNegocioTrazabilidadDatacredito: "+e.toString());
        }
        finally {
            if(ps!=null){ try{ps.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return list;
    }

      
       /**
     * Sql para buscar si un negocio tiene analista asignado
     * @return devuelve true o false segun el caso
     * @param cod_neg codigo del negocio
     * @throws Exception cuando hay error
     */
    public boolean tieneAnalista(String cod_neg) throws Exception {
        boolean sw = false;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "TIENE_ANALISTA";
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, cod_neg);
            rs = ps.executeQuery();
            if (rs.next()) {
                sw = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en tieneAnalista [NegocioTrazabilidadDAO] " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }
        return sw;
    }

    /**
     * Sql para buscar los usarios con el perfil analista
     * @return devuelve listado de analistas
     * @throws Exception cuando hay error
     */
     public ArrayList< String> buscarAnalistas() throws Exception{
       
        ArrayList <String> list = new ArrayList();
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        String query="BUSCAR_ANALISTAS";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con=conectarJNDI(query);
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while(rs.next()){               
               list.add(rs.getString("usuarios"));
            }
        }
        catch (Exception e) {
            System.out.println("Error en buscarNegocioTrazabilidadDatacredito: "+e.toString());
            e.printStackTrace();
            throw new Exception("Error en buscarNegocioTrazabilidadDatacredito: "+e.toString());
        }
        finally {
            if(ps!=null){ try{ps.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return list;
    }

     /**
     * Sql para buscar la ultima secuencia de la asignacion a analista
     * @return secuencia
     * @param numero_solicitud ultiliza la solicitud para obtener el producto de la misma
     * @throws Exception cuando hay error
     */
    public int secUltimoAnalistaProducto(String numero_solicitud) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "ULTIMO_ANALISTA_NEGOCIO";
        int sec=0;
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, numero_solicitud);
            rs = ps.executeQuery();
            if (rs.next()) {
                sec=rs.getInt("secuencia");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en secUltimoAnalistaProduct [NegocioTrazabilidadDAO] " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }
        return sec;
    }

    /**
     * Sql para insertar registro en la tabla negocios_analista
     * @return sql de insersion de negocios_analista
     * @param neg_analista objeto NegocioTrazabilidad
     * @throws Exception cuando hay error
     */
    public String insertNegocioAnalista(NegocioAnalista  neg_analista ){
        String cadena = "";
        String query = "INSERT_NEGOCIO_ANALISTA";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1,  neg_analista.getDstrct());
            st.setString(2,  neg_analista.getCodNeg());
            st.setInt(3, neg_analista.getSecuencia());
            st.setString(4,  neg_analista.getAnalista());
            cadena = st.getSql();
            st = null;
        }
        catch (Exception e) {
            System.out.println("Error en insertNegocioAnalista (NegocioTrazabilidadDAO)"+e.toString());
            e.printStackTrace();
        }
        return cadena;
    }
    
      public ArrayList<NegocioAsociacion>  buscarNegocioRelAut(String cod_cliente, String negocioPadre) throws Exception{
        NegocioAsociacion bean = null;
        ArrayList  list = new ArrayList();
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        String query="BUSCAR_NEGOCIOS_REL_AUTOMOTOR";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con=conectarJNDI();
            ps=con.prepareStatement(sql);
            ps.setString(1, cod_cliente);
            ps.setString(2, negocioPadre);
            rs=ps.executeQuery();
            while(rs.next()){
                bean = new NegocioAsociacion();
                bean.setNegasoc(rs.getString("cod_neg"));
                bean.setCedula(rs.getString("cedula"));
                bean.setFecha_negocio(rs.getString("fecha_negocio"));
                bean.setTipo_neg(rs.getString("tipo_neg"));
                bean.setVr_negocio(rs.getDouble("vr_negocio"));
                bean.setValor_saldo(rs.getDouble("valor_saldo"));   
                bean.setEstado(rs.getString("estado"));
                list.add(bean);
            }
        }
        catch (Exception e) {
            System.out.println("Error en buscarNegocioRelAut: "+e.toString());
            e.printStackTrace();
            throw new Exception("Error en buscarNegocioRelAut: "+e.toString());
        }
        finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return list;
    }
      
      public String actualizarNegocioRel(String negocio, String neg_rel,String neg_type){
        String cadena = "";
        String query = "UPD_NEGOCIO_REL";     
        String sql = "";
        String neg_rel_seg="",neg_rel_gps="";
          if (neg_type.equals("seguro")) {
              neg_rel_seg = neg_rel;
          } else {
              neg_rel_gps = neg_rel;
          }
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, neg_rel_seg);
            st.setString(2, neg_rel_gps);
            st.setString(3,  negocio);
            cadena = st.getSql();
            st = null;
        }
        catch (Exception e) {
            System.out.println("Error en updateNegocioRel (NegocioTrazabilidadDAO)"+e.toString());
            e.printStackTrace();
        }
        return cadena;
    }
      
    public String actualizarCicloNegocioRel(String negocio, String neg_rel){
        String cadena = "";
        String query = "UPD_CICLO_NEGOCIO_REL";     
        String sql = "";       
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, neg_rel);
            st.setString(2, negocio);         
            cadena = st.getSql();
            st = null;
        }
        catch (Exception e) {
            System.out.println("Error en actualizarCicloNegocioRel (NegocioTrazabilidadDAO)"+e.toString());
            e.printStackTrace();
        }
        return cadena;
    }   

    public String  buscarNegociosStandBy(String unidad_negocio){
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();
        JsonArray datos = new JsonArray();
        
         
        Gson gson = new Gson();
        String query = "BUSCAR_NEGOCIOS_STANDBY"; 
        String filtro = "";
        
        if(unidad_negocio != ""){
            filtro = "and un.id = "+ unidad_negocio;
        }                    
                
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            ps = con.prepareStatement(query);           
            rs = ps.executeQuery();
          
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();
                fila.addProperty("afiliado", rs.getString("afiliado"));
                fila.addProperty("id_cliente", rs.getString("id_cliente"));
                fila.addProperty("cliente", rs.getString("cliente"));
                fila.addProperty("negocio", rs.getString("negocio"));
                fila.addProperty("numero_solicitud", rs.getString("numero_solicitud"));
                fila.addProperty("vr_negocio", rs.getString("vr_negocio"));
                fila.addProperty("vr_desembolso", rs.getString("vr_desembolso"));
                fila.addProperty("fecha_negocio", rs.getString("fecha_negocio"));
                fila.addProperty("fecha_desembolso", rs.getString("fecha_desembolso"));
                fila.addProperty("cod_actividad_anterior", rs.getString("cod_actividad_anterior"));
                fila.addProperty("actividad", rs.getString("actividad"));
                fila.addProperty("usuario_standby", rs.getString("usuario_standby"));
                fila.addProperty("fecha_standby", rs.getString("fecha_standby"));
                fila.addProperty("causal_standby", rs.getString("causal_standby"));
                fila.addProperty("dias_exp", rs.getString("dias_exp"));
                fila.addProperty("dias_restantes", rs.getString("dias_restantes"));
                fila.addProperty("comentarios", rs.getString("comentarios"));              
                fila.addProperty("asesor", rs.getString("asesor"));     
                fila.addProperty("agencia", rs.getString("departamento"));
                datos.add(fila);
            }
           obj.add("rows", datos);
        }catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return  gson.toJson(obj);
        }
         
    }
    
    public String  buscarConfigCausalesStandBy(){
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();
        JsonArray datos = new JsonArray();
             
        Gson gson = new Gson();
        String query = "BUSCAR_CAUSALES";     
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);  
            ps.setString(1, "CAUSACONCP");
            ps.setString(2, "STANDBY");
            rs = ps.executeQuery();
          
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();
                fila.addProperty("table_code", rs.getString("table_code"));
                fila.addProperty("dato", rs.getString("dato"));
                fila.addProperty("descripcion", rs.getString("descripcion"));        
                fila.addProperty("reg_status", rs.getString("reg_status"));           
                datos.add(fila);
            }
           obj.add("rows", datos);
        }catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return  gson.toJson(obj);
        }
         
    }
    
    public String insertCausalStandBy(String nombre, String plazo, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;      
        String query = "INSERT_CAUSAL_STANDBY";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);        
            ps.setString(1, plazo);
            ps.setString(2, usuario.getLogin());
            ps.setString(3, nombre);
                      
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";             
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;  
       }
    }

    public String actualizarCausalStandBy(String codigo, String plazo, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;    
        String query = "UPDATE_CAUSAL_STANDBY";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);       
            ps.setString(1, plazo);
            ps.setString(2, usuario.getLogin());
            ps.setString(3, codigo);
         
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";                
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson; 
       }
    }

    public String activaInactivaCausalStandBy(String codigo, String estado, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;    
        String query = "UPD_ESTADO_CAUSAL_STANDBY";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);  
            ps.setString(1, estado);
            ps.setString(2, usuario.getLogin());
            ps.setInt(3, Integer.parseInt(codigo));       
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";           
            } catch (Exception ex) {
               System.out.println(ex.getMessage());
               ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson; 
       }
    }
    
    public String getCausalStandBy(String codigo) {
        Connection con = null;
        PreparedStatement ps = null;    
        ResultSet rs = null;
        String query = "GET_CAUSAL_STANDBY";
        String  causal = "";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);  
            ps.setString(1, codigo);                      
            rs = ps.executeQuery();          
            while(rs.next()){
               causal = rs.getString("dato") + " (Tiempo Estipulado: " + rs.getString("descripcion") +" d�as)";
            }
                 
        }catch (Exception e) {         
                System.out.println("Error en getCausalStandBy: " + e.toString());
                e.printStackTrace();
                throw new Exception("Error en getCausalStandBy: " + e.toString());         
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }                
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return causal; 
       }
    }
    
    public String getMailUser(String usuario) {
        Connection con = null;
        PreparedStatement ps = null;    
        ResultSet rs = null;
        String query = "GET_MAIL_USER";
        String  email = "";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);  
            ps.setString(1, usuario);                      
            rs = ps.executeQuery();          
            while(rs.next()){
               email = rs.getString("email");
            }
                 
        }catch (Exception e) {         
                System.out.println("Error en getMailUser: " + e.toString());
                e.printStackTrace();
                throw new Exception("Error en getMailUser: " + e.toString());         
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }                
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return email; 
       }
    }
    
    public String getCorreosToSendStandBy(String type) {
        Connection con = null;
        PreparedStatement ps = null;    
        ResultSet rs = null;
        String query = "GET_CORREOS_STANDBY";
        String  correos = "";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);  
            ps.setString(1, type);                      
            rs = ps.executeQuery();          
            while(rs.next()){
               correos = rs.getString("dato");
            }
                 
        }catch (Exception e) {         
                System.out.println("Error en getCorreosToSendStandBy: " + e.toString());
                e.printStackTrace();
                throw new Exception("Error en getCorreosToSendStandBy: " + e.toString());         
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }                
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return correos; 
       }
    }
    
    public JsonObject getInfoCuentaEnvioCorreo() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "GET_INFO_CORREO_STANDBY";
        JsonObject obj = null;

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, "STANDBY");
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("servidor", rs.getString("servidor"));
                fila.addProperty("puerto", rs.getString("puerto"));
                fila.addProperty("usuario", rs.getString("usuario"));
                fila.addProperty("clave", rs.getString("clave"));
                fila.addProperty("bodyMessage", rs.getString("bodyMessage"));
            }
            obj = fila;

        } catch (Exception e) {
            System.out.println("Error en getInfoCuentaEnvioCorreo: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en getInfoCuentaEnvioCorreo: " + e.toString());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return obj;
        }
    }
    
    public String insertErrorCorreoStandBy(String proceso, String descripcion, String detalle_error, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;      
        String query = "INSERT_ERROR_ENVIO_CORREO_STANDBY";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);    
            ps.setString(1, proceso);
            ps.setString(2, descripcion);
            ps.setString(3, detalle_error);
            ps.setString(4, usuario);
                      
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";             
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;  
       }
    }
    
    public String getUnidadConvenio(int id_convenio) {
        Connection con = null;
        PreparedStatement ps = null;    
        ResultSet rs = null;
        String query = "GET_UNIDAD_CONVENIO";
        String  idUnidad = "";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);  
            ps.setInt(1, id_convenio);                      
            rs = ps.executeQuery();          
            while(rs.next()){
               idUnidad = rs.getString("id_unidad");
            }
                 
        }catch (Exception e) {         
                System.out.println("Error en getUnidadConvenio: " + e.toString());
                e.printStackTrace();
                throw new Exception("Error en getUnidadConvenio: " + e.toString());         
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }                
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return idUnidad; 
       }
    }
    
    public String getNombreCliente(String negocio) {
        Connection con = null;
        PreparedStatement ps = null;    
        ResultSet rs = null;
        String query = "GET_NOMBRE_CLIENTE";
        String  nombre = "";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);  
            ps.setString(1, negocio);                      
            rs = ps.executeQuery();          
            while(rs.next()){
               nombre = rs.getString("nombre");
            }
                 
        }catch (Exception e) {         
                System.out.println("Error en getNombreCliente: " + e.toString());
                e.printStackTrace();
                throw new Exception("Error en getNombreCliente: " + e.toString());         
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }                
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return nombre; 
       }
    }

    public String buscarNegociosStandByxUnidadNegocio(String unidad_negocio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();
        JsonArray datos = new JsonArray();
             
        Gson gson = new Gson();
        String query = "BUSCAR_NEGOCIOS_STANDBY_UNIDAD_NEGOCIO";     
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);  
            ps.setString(1, unidad_negocio);  
            rs = ps.executeQuery();
          
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();
                fila.addProperty("afiliado", rs.getString("afiliado"));
                fila.addProperty("id_cliente", rs.getString("id_cliente"));
                fila.addProperty("cliente", rs.getString("cliente"));
                fila.addProperty("negocio", rs.getString("negocio"));
                fila.addProperty("numero_solicitud", rs.getString("numero_solicitud"));
                fila.addProperty("vr_negocio", rs.getString("vr_negocio"));
                fila.addProperty("vr_desembolso", rs.getString("vr_desembolso"));
                fila.addProperty("fecha_negocio", rs.getString("fecha_negocio"));
                fila.addProperty("fecha_desembolso", rs.getString("fecha_desembolso"));
                fila.addProperty("cod_actividad_anterior", rs.getString("cod_actividad_anterior"));
                fila.addProperty("actividad", rs.getString("actividad"));
                fila.addProperty("usuario_standby", rs.getString("usuario_standby"));
                fila.addProperty("fecha_standby", rs.getString("fecha_standby"));
                fila.addProperty("causal_standby", rs.getString("causal_standby"));
                fila.addProperty("dias_exp", rs.getString("dias_exp"));
                fila.addProperty("dias_restantes", rs.getString("dias_restantes"));
                fila.addProperty("comentarios", rs.getString("comentarios"));              
                datos.add(fila);
            }
           obj.add("rows", datos);
        }catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return  gson.toJson(obj);
        }
        
    }
    
    public ArrayList<CmbGeneralScBeans> GetComboGenerico(String Query, String IdCmb, String DescripcionCmb, String wuereCmb) throws Exception {
        
        ArrayList combo = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            con = conectarJNDI(Query);
            String sql = this.obtenerSQL(Query);
            
            ps = con.prepareStatement(sql);
            if ( wuereCmb != "") {
                ps.setString(1, wuereCmb);
            }
            
            rs = ps.executeQuery();
            combo = new ArrayList();
            while (rs.next()) {
                CmbGeneralScBeans cmb = new CmbGeneralScBeans();
                cmb.setIdCmb(rs.getInt(IdCmb));
                cmb.setDescripcionCmb(rs.getString(DescripcionCmb));
                combo.add(cmb);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return combo;
    }

    public String getUnidadNegocio(int id_convenio) {
        Connection con = null;
        PreparedStatement ps = null;    
        ResultSet rs = null;
        String query = "GET_UNIDAD_CONVENIO";
        String  unidad_negocio = "";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);  
            ps.setInt(1, id_convenio);                      
            rs = ps.executeQuery();          
            while(rs.next()){
               unidad_negocio = rs.getString("descripcion");
            }
                 
        }catch (Exception e) {         
                System.out.println("Error en getUnidadConvenio: " + e.toString());
                e.printStackTrace();
                throw new Exception("Error en getUnidadConvenio: " + e.toString());         
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }                
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return unidad_negocio; 
       }
    }
    
    
          /**
     * Sql para buscar la trazabilidad de una solicitud y actividad
     * @return la traza de la actividad
     * @param num_solicitud
     * @throws Exception cuando hay error
     */
     public  NegocioTrazabilidad buscarTrazaNegocio(int num_solicitud) throws Exception{
        NegocioTrazabilidad bean = null;
        ArrayList list = new ArrayList();
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        String query="SQL_ACTIVIDAD_TRAZA";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con=conectarJNDI(query);
            ps=con.prepareStatement(sql);
            ps.setInt(1, num_solicitud);
            rs=ps.executeQuery();
            if(rs.next()){
                bean = new NegocioTrazabilidad();
               bean=bean.load(rs);
              
            }
        }
        catch (Exception e) {
            System.out.println("Error en buscarTraza: "+e.toString());
            e.printStackTrace();
            throw new Exception("Error en buscarTraza: "+e.toString());
        }
        finally {
            if(ps!=null){ try{ps.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return bean;
    }
     
     /**
     * Sql para actualizar la actividad de un negocio
     * @return sql de insersion de negocios_trazabilidad
     * @param negocio   codigo de negocio
     * @param estado_neg  actividad a actualizar
    
     */
    public String updateActividadnegocio(String negocio, String estado_neg,String actividad){
        String cadena = "";
        String query = "UPD_ACTIVIDAD_NEGOCIO";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1,  estado_neg);            
            st.setString(2,  actividad);            
            st.setString(3,  negocio);            
            cadena = st.getSql();
            st = null;
        }
        catch (Exception e) {
            System.out.println("Error en updateActividad (NegocioTrazabilidadDAO)"+e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    public String obtenerCausalUltimoStandBy(String numeroSolicitud, String codigoNegocio, String actividad) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareCall(this.obtenerSQL("GET_LAST_CAUSAL_STBY"));
            ps.setString(1, numeroSolicitud);
            ps.setString(2, codigoNegocio);
            ps.setString(3, actividad);
            
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString(1);
            }
        } finally {
            try {               
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
        return null;
    }
}
