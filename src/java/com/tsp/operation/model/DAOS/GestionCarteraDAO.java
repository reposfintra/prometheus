/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * GestionCarteraDAO.java :
 * Copyright 2010 Rhonalf Martinez Villa <rhonaldomaster@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.tsp.operation.model.DAOS;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;
import javax.swing.text.DefaultEditorKit;
/**
 *
 * @author rhonalf
 */
public class GestionCarteraDAO extends MainDAO {

    public GestionCarteraDAO() {
        super("GestionCarteraDAO.xml");
    }
    public GestionCarteraDAO(String dataBaseName) {
        super("GestionCarteraDAO.xml", dataBaseName);
    }

    /**
     * Busca datos de clientes con negocios en estado aprobado y/o transferido dado un filtro
     * @param filtro Columna de la tabla cliente aplicada como filtro
     * @param cadena Dato que se quiere buscaar
     * @param tipodoc Titulo valor del negocio (CH,PG,LT)
     * @return Listado con los registros que coincidieron
     * @throws Exception Cuando ocurre un error
     */
    public ArrayList<ClienteNeg> buscarDatos(String filtro,String cadena,String tipodoc,String conv_sect_subs,String sald,String fecha) throws Exception{
         ArrayList<ClienteNeg> lista = null;
         ClienteNeg cliente = null;
         PreparedStatement st = null;
         Connection con = null;
         ResultSet rs = null;
         String query = "SQL_SRCH_CL";
         String sql="";
         try {
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#filtro", filtro);
            sql = sql.replaceAll("#cadena", cadena);
            sql = sql.replaceAll("#convs", conv_sect_subs);
            sql = sql.replaceAll("#saldo", sald);
            if(tipodoc.equals("")){
                sql = sql.replaceAll("#concepto", "");
            }
            else{
                //sql = sql.replaceAll("#concepto", "AND tipo_titulo_valor=\'" + tipodoc + "\' ");
                //descomentar la anterior y comentar la siguiente cuando se tenga la columna tipo_titulo_valor en la tabla negocios
                sql = sql.replaceAll("#concepto", "AND f.concepto=\'" + tipodoc + "\' ");
            }

            if(fecha.equals("")){
            sql = sql.replaceAll("#fecha", "");
            }
            else{
                //sql = sql.replaceAll("#concepto", "AND tipo_titulo_valor=\'" + tipodoc + "\' ");
                //descomentar la anterior y comentar la siguiente cuando se tenga la columna tipo_titulo_valor en la tabla negocios
                sql = sql.replaceAll("#fecha", "AND f.valor_saldo>0 AND f.fecha_vencimiento=\'" + fecha + "\' ");
            }




            lista = new ArrayList<ClienteNeg>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while(rs.next()){
                cliente = new ClienteNeg();
                cliente.setCodcli(rs.getString("codcli"));
                cliente.setEmail(rs.getString("email"));
                cliente.setNit(rs.getString("nit"));
                cliente.setNomcli(rs.getString("nomcli"));
                cliente.setDireccion(rs.getString("direccion"));
                cliente.setTelefono(rs.getString("telefono"));
                cliente.setDvenc(rs.getInt("maxdiasvenc"));
                cliente.setFecha_prox_gestion(rs.getString("f_prx_ges"));
                cliente.setFecha_ult_gestion(rs.getString("f_ult_ges"));
                cliente.setFecha_next_venc(rs.getString("maxvenc"));
                cliente.setSaldo_vencido(rs.getFloat("saldo_vencido"));
                lista.add(cliente);
                cliente = null;
            }
        }
        catch (Exception e) {
            throw new Exception("Error en buscarDatos en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    /**
     * Busca los negocios de un cliente que tengan un tipo de titulo valor
     * @param nit Nit del cliente
     * @param tipodoc Tipo del titulo valor (CH,PG,LT)
     * @return Listado con los datos encontrados
     * @throws Exception Cuando hay un error
     */
    public ArrayList<Negocios> listaNegocios(String nit,String tipodoc,String fecha) throws Exception{
        ArrayList<Negocios> lista = null;
        Negocios negocio = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "NEG_CLIE";
        String sql="";
        try {
            lista = new ArrayList<Negocios>();
            sql = this.obtenerSQL(query);
            if(tipodoc.equals("")){
                sql = sql.replaceAll("#concepto", "");
            }
            else{
                sql = sql.replaceAll("#concepto", "AND concepto=\'" + tipodoc + "\' ");
            }

            if(fecha.equals("")){
            sql = sql.replaceAll("#fecha", "");
            sql = sql.replaceAll("#fg", "");
            }
            else{
                //sql = sql.replaceAll("#concepto", "AND tipo_titulo_valor=\'" + tipodoc + "\' ");
                //descomentar la anterior y comentar la siguiente cuando se tenga la columna tipo_titulo_valor en la tabla negocios
                sql = sql.replaceAll("#fecha", "AND  fact.fecha_vencimiento=\'" + fecha + "\' ");
                 sql = sql.replaceAll("#fg", "AND  f.fecha_vencimiento=\'" + fecha + "\' ");
            }




            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, nit);
            rs = st.executeQuery();
            ArrayList<String> codsreg = new ArrayList<String>();
            while(rs.next()){
                String codnego = rs.getString("cod_neg");
                boolean esta = false;
                for(int i=0;i<codsreg.size();i++){
                    if(codsreg.get(i).equals(codnego)){
                        esta = true;
                        break;
                    }
                }
                if(esta==false){
                    codsreg.add(rs.getString("cod_neg"));
                    negocio = new Negocios();
                    negocio.setCod_negocio(rs.getString("cod_neg"));
                    negocio.setConcepto(rs.getString("concepto"));
                    negocio.setCuotas(rs.getInt("cuotas"));
                    negocio.setFactura_vencer(rs.getString("factven"));
                    negocio.setFecha_vencimiento(rs.getString("fven"));
                    negocio.setNombreafil(rs.getString("afiliado"));
                    negocio.setSaldoneg(rs.getDouble("saldo"));
                    negocio.setValor_cuota(rs.getDouble("valor_cuota"));
                    negocio.setFecha_neg(rs.getString("fecha_factura"));//fecha_factura 2010-10-04
                    negocio.setTotpagado(rs.getDouble("cartera"));
                    negocio.setVr_desem(rs.getDouble("desembolso"));
                    negocio.setNodocs(rs.getInt("diasv"));//usado aca para guardar dias vencidos(para no tener que modificar clase Negocios)
                    negocio.setCuotas_vig(rs.getInt("vigentes"));
                    negocio.setCuotas_ven(rs.getInt("vencidas"));
                    negocio.setCuotas_pagas(rs.getInt("pagas"));
                    negocio.setSaldo_ven(rs.getDouble("vencido"));
                    negocio.setSaldo_pagar(rs.getDouble("a_pagar"));


                    lista.add(negocio);
                    negocio = null;
                }
            }
        }
        catch (Exception e) {
            throw new Exception("Error en listaNegocios en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    /**
     * Datos del encabezado de la pagina /jsp/fenalco/clientes/facturas_cartera.jsp
     * @param cod_neg Codigo del negocio
     * @return Listado con los datos
     * @throws Exception Cuando hay error
     */
    public ArrayList<String> cabeceraTabla(String cod_neg) throws Exception{
        ArrayList<String> lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "DATA1";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<String>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, cod_neg);
            rs = st.executeQuery();
            if(rs.next()){
                lista.add(rs.getString("nombre"));
                lista.add(rs.getString("identificacion"));
                lista.add(rs.getString("telefono"));
                lista.add(rs.getString("celular"));
                lista.add(rs.getString("email"));
                lista.add(rs.getString("direccion"));
                lista.add(rs.getString("nomciu"));
                lista.add(rs.getString("cod_neg"));
                lista.add(rs.getString("dato"));
                lista.add(rs.getString("afiliado"));
                lista.add(rs.getString("sector_afil"));
                lista.add(rs.getString("barrio"));
            }
        }
        catch (Exception e) {
            throw new Exception("Error en cabeceraTabla en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    /**
     * Datos de las facturas de un negocio
     * @param cod_neg Codigo del negocio
     * @return Listado con los datos
     * @throws Exception Cuando hay error
     */
    public ArrayList<factura> facturasNegocio(String cod_neg) throws Exception{
        ArrayList<factura> lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "FACT_NEG";
        String sql="";
        factura f = null;
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<factura>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, cod_neg);
            rs = st.executeQuery();
            while(rs.next()){
                f = new factura();
                f.setDocumento(rs.getString("documento"));
                f.setDias_mora(rs.getInt("dias_mora"));
                f.setEstado(rs.getString("estadofact"));
                f.setFactura(rs.getString("fact_original"));//la factura original
                f.setFecha_ultimo_pago(rs.getString("fecha_ultimo_pago"));
                f.setFecha_vencimiento(rs.getString("fecha_vencimiento"));
                f.setFecha_ven_prejurid(rs.getString("ndiasf"));
                f.setFisico(rs.getString("fisico"));
                f.setValor_factura(rs.getDouble("valor_cuota"));
                f.setValor_saldo(rs.getDouble("valor_saldo"));
                f.setFecha_factura(rs.getString("fecha_factura"));
                f.setDescripcion(rs.getString("descripcion"));
                f.setValor_abono(rs.getDouble("valor_abono"));
                f.setValor_saldome(rs.getDouble("valor_mora"));//valor mora
                lista.add(f);
                f = null;
            }
        }
        catch (Exception e) {
            throw new Exception("Error en facturasNegocio en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    /**
     * Datos de las facturas de un negocio
     * @param cod_neg Codigo del negocio
     * @param filtro Estado en el que estan las facturas
     * @return Listado con los datos
     * @throws Exception Cuando hay error
     */
    public ArrayList<factura> facturasNegocio(String cod_neg,String filtro) throws Exception{
        ArrayList<factura> lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "FACT_NEG_ST";
        String sql="";
        factura f = null;
        try {
            sql = this.obtenerSQL(query);
            if(filtro.equals("TODAS")){
                sql = sql.replaceAll("#filtro", "");
            }
            else if(filtro.equals("VIGENTE")){
                sql = sql.replaceAll("#filtro", "WHERE t.valor_saldo!=0");
            }
            else if(filtro.equals("CANCELADA")){
                sql = sql.replaceAll("#filtro", "WHERE t.valor_saldo=0");
            }
            else if(filtro.equals("SINIESTRADA")){
                sql = sql.replaceAll("#filtro", "WHERE t.estadofact='Siniestrada'");
            }
            else if(filtro.equals("NEGOCIADA")){
                sql = sql.replaceAll("#filtro", "WHERE t.estadofact='Negociada'");
            }
            else{
                sql = sql.replaceAll("#filtro", "");
            }
            lista = new ArrayList<factura>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, cod_neg);
            rs = st.executeQuery();
            while(rs.next()){
                f = new factura();
                f.setDocumento(rs.getString("documento"));
                f.setDias_mora(rs.getInt("dias_mora"));
                f.setEstado(rs.getString("estadofact"));
                f.setFactura(rs.getString("fact_original"));//la factura original
                f.setFecha_ultimo_pago(rs.getString("fecha_ultimo_pago"));
                f.setFecha_vencimiento(rs.getString("fecha_vencimiento"));
                f.setFecha_ven_prejurid(rs.getString("ndiasf"));
                f.setFisico(rs.getString("fisico"));
                f.setValor_factura(rs.getDouble("valor_cuota"));
                f.setValor_saldo(rs.getDouble("valor_saldo"));
                f.setValor_abono(rs.getDouble("valor_abono"));
                f.setValor_saldome(rs.getDouble("valor_mora"));//valor mora
                lista.add(f);
                f = null;
            }
        }
        catch (Exception e) {
            throw new Exception("Error en facturasNegocio en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    /**
     * Datos de los ingresos de una factura
     * @param documento Numero de la factura
     * @return Listado con los datos
     * @throws Exception Cuando hay un error
     */
    public ArrayList<Ingreso> ingresosFactura(String cod_neg) throws Exception{
        ArrayList<Ingreso> lista = null;
        Ingreso ingreso = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "ING_FACT";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<Ingreso>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, cod_neg);
            rs = st.executeQuery();
            while(rs.next()){
                ingreso = new Ingreso();
                ingreso.setBranch_code(rs.getString("branch_code"));
                ingreso.setNum_ingreso(rs.getString("num_ingreso"));
                ingreso.setFecha_consignacion(rs.getString("fecha_consignacion"));
                ingreso.setVlr_ingreso(rs.getDouble("valor_ingreso"));
                lista.add(ingreso);
                ingreso = null;
            }
        }
        catch (Exception e) {
            throw new Exception("Error en ingresosFactura en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    /**
     * Datos de los ingresos de un negocio. Se incluyen los anulados.
     * @param cod_neg Codigo del negocio
     * @return Listado con los datos
     * @throws Exception Cuando hay un error
     */
    public ArrayList<Ingreso> ingresosNegocio(String cod_neg) throws Exception{
        ArrayList<Ingreso> lista = null;
        Ingreso ingreso = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "ING_NEG";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#columna", "fecha_consignacion");
            sql = sql.replaceAll("#orden", "DESC");
            lista = new ArrayList<Ingreso>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, cod_neg);
            st.setString(2, cod_neg);
            st.setString(3, cod_neg);
            rs = st.executeQuery();
            while(rs.next()){
                ingreso = new Ingreso();
                ingreso.setConcepto(rs.getString("tipo_documento"));
                ingreso.setCreation_date(rs.getString("fecha_creacion"));
                ingreso.setNum_ingreso(rs.getString("num_ingreso"));
                ingreso.setFecha_consignacion(rs.getString("fecha_consignacion"));
                ingreso.setVlr_ingreso(rs.getDouble("vlr_ingreso"));
                ingreso.setDescripcion_ingreso(rs.getString("descripcion"));
                ingreso.setBranch_code(rs.getString("branch_code"));
                ingreso.setReg_status(rs.getString("estado"));
                lista.add(ingreso);
                ingreso = null;
            }
        }
        catch (Exception e) {
            throw new Exception("Error en ingresosNegocio en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    /**
     * Encuentra datos del cliente asociado a un negocio. Se usa en la pagina del estado de cuenta
     * @param cod_neg El codigo del negocio
     * @return String con el dato encontrado
     * @throws Exception Cuando hay error
     */
    public String nitNegocio(String cod_neg) throws Exception{
        String nit="";
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "NIT_NEG";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, cod_neg);
            rs = st.executeQuery();
            if(rs.next()){
                nit = rs.getString("nit")+";_;"+rs.getString("direccion")+";_;"+rs.getString("ciudad");
            }
        }
        catch (Exception e) {
            throw new Exception("Error en nitNegocio en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return nit;
    }

    /**
     * Encuentra el saldo de un negocio con la diferencia entre sus facturas y los ingresos de las facturas
     * @param cod_neg El codigo del negocio
     * @return String con el dato buscado
     * @throws Exception Cuando hay error
     */
    public String saldoNegocio(String cod_neg) throws Exception{
        String sal = "";
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SAL_NEG";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#codigoneg", cod_neg);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            if(rs.next()){
                sal = ""+rs.getString("vigente")+";"+rs.getString("vencido")+";"+rs.getString("intereses")+";";
            }
        }
        catch (Exception e) {
            throw new Exception("Error en saldoNegocio en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return sal;
    }

    /**
     * Encuentra las facturas afectadas por un ingreso
     * @param coding El numero del ingreso
     * @return Listado con las facturas afectadas
     * @throws Exception Cuando hay error
     */
    public ArrayList<factura> facturasIngreso(String coding) throws Exception{
        ArrayList<factura> lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "FACT_ING";
        String sql="";
        factura fact = null;
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            lista = new ArrayList<factura>();
            st = con.prepareStatement(sql);
            st.setString(1, coding);
            st.setString(2, coding);
            rs = st.executeQuery();
            while(rs.next()){
                fact = new factura();
                fact.setDocumento(rs.getString("documento"));
                fact.setFactura(rs.getString("fact_original"));//la factura original
                fact.setFisico(rs.getString("fisico"));
                fact.setValor_abono(rs.getDouble("valor_ingreso"));
                fact.setValor_factura(rs.getDouble("valor_cuota"));
                fact.setValor_saldo(rs.getDouble("valor_saldo"));
                lista.add(fact);
                fact = null;
            }
        }
        catch (Exception e) {
            throw new Exception("Error en facturasIngreso en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    /**
     * Lista la gestion hecha sobre una factura
     * @param documento La factura a buscar
     * @return Listado con los datos encontrados
     * @throws Exception Cuando hay un error
     */
    public ArrayList<BeanGeneral> observacionesFact(String documento) throws Exception{
        ArrayList<BeanGeneral> lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "OBS_FACTS";
        BeanGeneral bean = null;
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<BeanGeneral>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, documento);
            rs = st.executeQuery();
            while(rs.next()){
                bean = new BeanGeneral();
                bean.setValor_01(rs.getString("observacion"));
                bean.setValor_02((rs.getString("creation_date")).substring(0, 19));
                bean.setValor_03(rs.getString("tipo_gestion"));
                bean.setValor_04(rs.getString("fecha_prox_gestion"));
                bean.setValor_05(rs.getString("prox_accion"));
                bean.setValor_06(rs.getString("documento"));
                bean.setValor_07(rs.getString("creation_user"));
                bean.setValor_08(rs.getString("id"));
                bean.setValor_09(rs.getString("tipo"));
                bean.setValor_10(rs.getString("dato"));
                lista.add(bean);
                bean = null;
            }
        }
        catch (Exception e) {
            throw new Exception("Error en observacionesFact en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    /**
     * Busca el texto de una observacion de una factura
     * @param codigo El codigo de la observacion a ver
     * @return Cadena con el texto de la observacion
     * @throws Exception Cuando hay error
     */
    public String verObs(String codigo) throws Exception {
        String obs = "";
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "OBS_COD";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, codigo);
            rs = st.executeQuery();
            if(rs.next()){
                obs = rs.getString("observacion");
            }
        }
        catch (Exception e) {
            throw new Exception("Error en verObs en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return obs;
    }



        /**
     * Busca el texto de una observacion de una factura
     * @param codigo El codigo de la observacion a ver
     * @return Cadena con el texto de la observacion
     * @throws Exception Cuando hay error
     */
    public ArrayList<String> ver_dato(String tipo,String codigo) throws Exception {
        ArrayList<String> lista = new ArrayList<String>();
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SQL_VER_DATO";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, tipo);
            st.setString(2, codigo);
            rs = st.executeQuery();
            while(rs.next())
            {
                  lista.add(rs.getString("codigo") + ";_;" + rs.getString("dato"));
            }
        }
        catch (Exception e) {
            throw new Exception("Error en verObs en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }



  /*/ Busca el texto de una observacion de una factura
     * @param codigo El codigo de la observacion a ver
     * @return Cadena con el texto de la observacion
     * @throws Exception Cuando hay error
     */
    public String actualiza_dato(String tipo,String codigo,String dato) throws Exception {
        String msg="";
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        int k=0;
        String query = "SQL_APDATE_DATO";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, dato);
            st.setString(2, tipo);
             st.setString(3, codigo);
            k = st.executeUpdate();
            if(k>0)
            {
                msg="Actualizado";
            }
        }
        catch (Exception e) {
            throw new Exception("Error en actualizar dato en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return msg;
    }







  /*/ Busca el texto de una observacion de una factura
     * @param codigo El codigo de la observacion a ver
     * @return Cadena con el texto de la observacion
     * @throws Exception Cuando hay error
     */
    public String inserta_dato(String tipo,String dato,String user) throws Exception {
        String msg="";
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String desc="Codigo Para "+dato;
        int k=0;
        String query = "SQL_INSERT_DATO";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, tipo);
            st.setString(2, tipo);
            st.setString(3, desc);
             st.setString(4,user);
             st.setString(5,dato);
            k = st.executeUpdate();
            if(k>0)
            {
                msg="Insertado";
            }
        }
        catch (Exception e) {
            throw new Exception("Error en actualizar dato en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return msg;
    }









    /**
     * Lista la gestion hecha sobre las facturas de un negocio
     * @param documento El negocio a buscar
     * @return Listado con los datos encontrados
     * @throws Exception Cuando hay un error
     */
    public ArrayList<BeanGeneral> observacionesNeg(String documento) throws Exception{
        ArrayList<BeanGeneral> lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "OBS_NEG";
        BeanGeneral bean = null;
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<BeanGeneral>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, documento);
            rs = st.executeQuery();
            while(rs.next()){
                bean = new BeanGeneral();
                bean.setValor_01(rs.getString("observacion"));
                bean.setValor_02((rs.getString("creation_date")).substring(0, 19));
                bean.setValor_03(rs.getString("tipo_gestion"));
                bean.setValor_04(rs.getString("fecha_prox_gestion"));
                bean.setValor_05(rs.getString("prox_accion"));
                bean.setValor_06(rs.getString("documento"));
                bean.setValor_07(rs.getString("creation_user"));
                bean.setValor_08(rs.getString("id"));
                bean.setValor_09(rs.getString("tipo"));
                bean.setValor_10(rs.getString("dato"));
                lista.add(bean);
                bean = null;
            }
        }
        catch (Exception e) {
            throw new Exception("Error en observacionesNeg en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }




    /**
     * Lista la gestion hecha sobre las facturas de un negocio
     * @param documento El negocio a buscar
     * @return Listado con los datos encontrados
     * @throws Exception Cuando hay un error
     */
    public ArrayList<BeanGeneral> observacionesClinte(String nit,String codcli) throws Exception{
        ArrayList<BeanGeneral> lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "OBS_CLIE";
        BeanGeneral bean = null;
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<BeanGeneral>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, nit);
            st.setString(2, codcli);
            rs = st.executeQuery();
            while(rs.next()){
                bean = new BeanGeneral();
                bean.setValor_01(rs.getString("observacion"));
                bean.setValor_02((rs.getString("creation_date")).substring(0, 19));
                bean.setValor_03(rs.getString("tipo_gestion"));
                bean.setValor_04(rs.getString("fecha_prox_gestion"));
                bean.setValor_05(rs.getString("prox_accion"));
                bean.setValor_06(rs.getString("documento"));
                bean.setValor_07(rs.getString("creation_user"));
                bean.setValor_08(rs.getString("id"));
                bean.setValor_09(rs.getString("tipo"));
                bean.setValor_10(rs.getString("dato"));
                lista.add(bean);
                bean = null;
            }
        }
        catch (Exception e) {
            throw new Exception("Error en observacionesNeg en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }



    /**
     * Datos para la pagina de gestion de cartera
     * @param documento Numero de la factura
     * @return Datos requeridos
     * @throws Exception Cuando hay error
     */
    public BeanGeneral datosHeader(String documento) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "HEAD_GEST";
        BeanGeneral bean = null;
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            bean = new BeanGeneral();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, documento);
            rs = st.executeQuery();
            if(rs.next()){
                bean.setValor_01(rs.getString("negasoc"));
                bean.setValor_02(rs.getString("nomcli"));
                bean.setValor_03(rs.getString("valor_saldo"));
                bean.setValor_04(rs.getString("nit"));
                bean.setValor_05(rs.getString("documento"));
                bean.setValor_06(rs.getString("fecha_vencimiento"));
                bean.setValor_07(rs.getString("dias_mora"));
                bean.setValor_08(rs.getString("estado_gestion_cartera"));
                bean.setValor_09(rs.getString("codcli"));
                bean.setValor_10(rs.getString("valor_mora"));
                bean.setValor_11(rs.getString("telefono"));
                bean.setValor_12(rs.getString("celular"));
                bean.setValor_13(rs.getString("email"));
            }
        }
        catch (Exception e) {
            throw new Exception("Error en datosHeader en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return bean;
    }

    /**
     * Nombre del tipo de gestion
     * @param codigo Codigo del tipo de gestion
     * @param table_type El tipo de tablagen. Es para saber si es listado de gestion actual o proxima gestion
     * @return String con el nombre del tipo de gestion
     * @throws Exception Cuando hay un error
     */
    public String nombreGestion(String codigo, String table_type) throws Exception{
        String nombre = "";
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "NOM_GEST";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, codigo);
            st.setString(2, table_type);
            rs = st.executeQuery();
            if(rs.next()){
                nombre = rs.getString("dato");
            }
        }
        catch (Exception e) {
            throw new Exception("Error en nombreGestion en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return nombre;
    }

    /**
     * Inserta observacion de una factura
     * @param documento Numero de la factura
     * @param observacion Texto de la observacion
     * @param user Usuario que crea la observacion
     * @param tipo_gestion Codigo del tipo de gestion
     * @param fecha_prox_gestion Fecha de la proxima gestion
     * @param prox_accion Codigo de la proxima accion
     * @throws Exception Cuando hay error
     */
     public void insObservacion(String documento,String observacion,String user,String tipo_gestion,String fecha_prox_gestion,String prox_accion, String tipo, String dato) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        String query = "INSERT_OBS";
        String sql="";
        int k = 0;
        try {
            if(fecha_prox_gestion.equals("")){ fecha_prox_gestion="0099-01-01 00:00:00"; }
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, documento);
            st.setString(2, observacion);
            st.setString(3, user);
            st.setString(4, tipo_gestion);
            st.setString(5, fecha_prox_gestion);
            st.setString(6, prox_accion);
            st.setString(7, tipo);
            st.setString(8, dato);
            st.setString(9, documento);
            k = st.executeUpdate();
            
        }
        catch (Exception e) {
            throw new Exception("Error en insObservacion en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
    }

    /**
     * Lista los diferentes tipos de gestion
     * @param table_type El tipo de tablagen. Es para saber si es listado de gestion actual o proxima gestion
     * @return Listado con los datos obtenidos
     * @throws Exception Cuando hay error
     */
    public ArrayList<String> listaGestion(String table_type,String estado) throws Exception{
        ArrayList<String> lista = null;
        String nombre = "";
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "LIST_GEST";
        if(estado.equals("A"))
        query = "LIST_GEST_A";

        String sql="";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<String>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, table_type);
            rs = st.executeQuery();
            while(rs.next()){
                lista.add(rs.getString("codigo") + ";_;" + rs.getString("dato")+ ";_;" + rs.getString("estado"));
            }
        }
        catch (Exception e) {
            throw new Exception("Error en listaGestion en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    /**
     * Datos de los ingresos de un negocio
     * @param cod_neg Codigo del negocio
     * @return Listado con los datos
     * @throws Exception Cuando hay un error
     */
    public ArrayList<Ingreso> ingresosNegocio2(String cod_neg) throws Exception{
        ArrayList<Ingreso> lista = null;
        Ingreso ingreso = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "ING_NEG2";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<Ingreso>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, cod_neg);
            st.setString(2, cod_neg);
            rs = st.executeQuery();
            while(rs.next()){
                ingreso = new Ingreso();
                ingreso.setConcepto(rs.getString("tipo_documento"));
                ingreso.setCreation_date(rs.getString("fecha_creacion"));
                ingreso.setNum_ingreso(rs.getString("num_ingreso"));
                ingreso.setFecha_consignacion(rs.getString("fecha_consignacion"));
                ingreso.setVlr_ingreso(rs.getDouble("vlr_ingreso"));
                ingreso.setDescripcion_ingreso(rs.getString("descripcion_ingreso"));
                ingreso.setBranch_code(rs.getString("branch_code"));
                //ingreso.setReg_status(rs.getString("estado"));
                lista.add(ingreso);
                ingreso = null;
            }
        }
        catch (Exception e) {
            throw new Exception("Error en ingresosNegocio2 en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    /**
     * Datos de las facturas de un negocio
     * @param cod_neg Codigo del negocio
     * @filtro Estado de la factura
     * @param columna Columna por la que se quiere ordenar
     * @param orden Si es ascendente o descendente
     * @return Listado con los datos
     * @throws Exception Cuando hay error
     */
    public ArrayList<factura> facturasNegocioSort(String cod_neg, String filtro, String columna, String orden) throws Exception{
        ArrayList<factura> lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "FACT_NEG_SORT";
        String sql="";
        factura f = null;
        try {
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#order", columna+" "+orden);
            if(filtro.equals("TODAS")){
                sql = sql.replaceAll("#filtro", "");
            }
            else if(filtro.equals("VIGENTE")){
                sql = sql.replaceAll("#filtro", "WHERE t.valor_saldo!=0");
            }
            else if(filtro.equals("CANCELADA")){
                sql = sql.replaceAll("#filtro", "WHERE t.valor_saldo=0");
            }
            else if(filtro.equals("SINIESTRADA")){
                sql = sql.replaceAll("#filtro", "WHERE t.estadofact='Siniestrada'");
            }
            else if(filtro.equals("NEGOCIADA")){
                sql = sql.replaceAll("#filtro", "WHERE t.estadofact='Negociada'");
            }
            else{
                sql = sql.replaceAll("#filtro", "");
            }
            lista = new ArrayList<factura>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, cod_neg);
            rs = st.executeQuery();
            while(rs.next()){
                f = new factura();
                f.setDocumento(rs.getString("documento"));
                f.setDias_mora(rs.getInt("dias_mora"));
                f.setEstado(rs.getString("estadofact"));
                f.setFactura(rs.getString("fact_original"));//la factura original
                f.setFecha_ultimo_pago(rs.getString("fecha_ultimo_pago"));
                f.setFecha_vencimiento(rs.getString("fecha_vencimiento"));
                f.setFecha_ven_prejurid(rs.getString("ndiasf"));
                f.setFisico(rs.getString("fisico"));
                f.setValor_factura(rs.getDouble("valor_cuota"));
                f.setValor_saldo(rs.getDouble("valor_saldo"));
                //f.setFecha_factura(rs.getString("fecha_factura"));
                //f.setDescripcion(rs.getString("descripcion"));
                f.setValor_abono(rs.getDouble("valor_abono"));
                f.setValor_saldome(rs.getDouble("valor_mora"));//valor mora
                lista.add(f);
                f = null;
            }
        }
        catch (Exception e) {
            throw new Exception("Error en facturasNegocio en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

	/**
     * Verifica si para el nit hay que mostrar las pf y ff
     * @param nit Dato a verificar
     * @return Si hay que mostrar o no
     * @throws Exception Cuando ocurre un error
     */
    public boolean clienteEspecial(String nit) throws Exception{
        boolean esp = false;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SQL_SP";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, nit);
            rs = st.executeQuery();
            if(rs.next()){
                esp = true;
            }
        }
        catch (Exception e) {
            System.out.println("Error en clienteEspecial en GestionCarteraDAO.java: "+e.toString());
            e.printStackTrace();
            throw new Exception("Error en clienteEspecial en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return esp;
    }

    /**
     * Busca todas las pf y ff no anuladas
     * @return Listado con datos
     * @throws Exception Cuando hay error
     */
    public ArrayList<factura> facturasPf() throws Exception{
        ArrayList<factura> lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "FAC_PF";
        String sql="";
        factura f = null;
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<factura>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while(rs.next()){
                f = new factura();
                f.setDocumento(rs.getString("documento"));
                f.setDias_mora(rs.getInt("dias_mora"));
                f.setEstado(rs.getString("estadofact"));
                f.setFactura(rs.getString("fact_original"));//la factura original
                f.setFecha_ultimo_pago(rs.getString("fecha_ultimo_pago"));
                f.setFecha_vencimiento(rs.getString("fecha_vencimiento"));
                f.setFecha_ven_prejurid(rs.getString("ndiasf"));
                f.setFisico(rs.getString("fisico"));
                f.setValor_factura(rs.getDouble("valor_cuota"));
                f.setValor_saldo(rs.getDouble("valor_saldo"));
                f.setFecha_factura(rs.getString("fecha_factura"));
                f.setDescripcion(rs.getString("descripcion"));
                f.setValor_abono(rs.getDouble("valor_abono"));
                f.setValor_saldome(rs.getDouble("valor_mora"));//valor mora
                lista.add(f);
                f = null;
            }
        }
        catch (Exception e) {
            System.out.println("Error en facturasPf en GestionCarteraDAO.java: "+e.toString());
            e.printStackTrace();
            throw new Exception("Error en facturasPf en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    /**
     * Busca la lista de pf segun un filtro
     * @param filtro El filtro
     * @param orden Orden
     * @param columna columna por la que se ordena
     * @return Listado con los coincidentes
     * @throws Exception Cuando hay error
     */
    public ArrayList<factura> facturasPf2(String filtro, String orden,String columna) throws Exception{
        ArrayList<factura> lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "FAC_PF2";
        String sql="";
        factura f = null;
        try {
            sql = this.obtenerSQL(query);
            //if(orden.equals("")) orden = "fecha_vencimiento ASC";
            sql = sql.replaceAll("#orden", columna+" "+orden);
            if(filtro.equals("TODAS")){
                sql = sql.replaceAll("#filtro", "");
            }
            else if(filtro.equals("VIGENTE")){
                sql = sql.replaceAll("#filtro", "AND f.valor_saldo>0");
            }
            else if(filtro.equals("CANCELADA")){
                sql = sql.replaceAll("#filtro", "AND f.valor_saldo=0");
            }
            else{
                sql = sql.replaceAll("#filtro", "");
            }
            lista = new ArrayList<factura>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while(rs.next()){
                f = new factura();
                f.setDocumento(rs.getString("documento"));
                f.setDias_mora(rs.getInt("dias_mora"));
                f.setEstado(rs.getString("estadofact"));
                f.setFactura(rs.getString("fact_original"));//la factura original
                f.setFecha_ultimo_pago(rs.getString("fecha_ultimo_pago"));
                f.setFecha_vencimiento(rs.getString("fecha_vencimiento"));
                f.setFecha_ven_prejurid(rs.getString("ndiasf"));
                f.setFisico(rs.getString("fisico"));
                f.setValor_factura(rs.getDouble("valor_cuota"));
                f.setValor_saldo(rs.getDouble("valor_saldo"));
                f.setFecha_factura(rs.getString("fecha_factura"));
                f.setDescripcion(rs.getString("descripcion"));
                f.setValor_abono(rs.getDouble("valor_abono"));
                f.setValor_saldome(rs.getDouble("valor_mora"));//valor mora
                lista.add(f);
                f = null;
            }
        }
        catch (Exception e) {
            System.out.println("Error en facturasPf2 en GestionCarteraDAO.java: "+e.toString());
            e.printStackTrace();
            throw new Exception("Error en facturasPf2 en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    /**
     * Busca todos los ingresos asociados a pf y ff
     * @return Listado con datos
     * @throws Exception Cuando hay error
     */
    public ArrayList<Ingreso> ingresosPf() throws Exception{
        ArrayList<Ingreso> lista = null;
        Ingreso ingreso = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "ING_PF";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<Ingreso>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while(rs.next()){
                ingreso = new Ingreso();
                ingreso.setConcepto(rs.getString("tipo_documento"));
                ingreso.setCreation_date(rs.getString("fecha_creacion"));
                ingreso.setNum_ingreso(rs.getString("num_ingreso"));
                ingreso.setFecha_consignacion(rs.getString("fecha_consignacion"));
                ingreso.setVlr_ingreso(rs.getDouble("vlr_ingreso"));
                //ingreso.setDescripcion_ingreso(rs.getString("descripcion_ingreso"));
                ingreso.setBranch_code(rs.getString("branch_code"));
                ingreso.setReg_status(rs.getString("estado"));
                lista.add(ingreso);
                ingreso = null;
            }
        }
        catch (Exception e) {
            System.out.println("Error en ingresosPf en GestionCarteraDAO.java: "+e.toString());
            e.printStackTrace();
            throw new Exception("Error en ingresosPf en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    /**
     * Busca todos los ingresos asociados a pf y ff. No eascoge anulados
     * @return Listado con datos
     * @throws Exception Cuando hay error
     */
    public ArrayList<Ingreso> ingresosPf2() throws Exception{
        ArrayList<Ingreso> lista = null;
        Ingreso ingreso = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "ING_PF2";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<Ingreso>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while(rs.next()){
                ingreso = new Ingreso();
                ingreso.setConcepto(rs.getString("tipo_documento"));
                ingreso.setCreation_date(rs.getString("fecha_creacion"));
                ingreso.setNum_ingreso(rs.getString("num_ingreso"));
                ingreso.setFecha_consignacion(rs.getString("fecha_consignacion"));
                ingreso.setVlr_ingreso(rs.getDouble("vlr_ingreso"));
                ingreso.setDescripcion_ingreso(rs.getString("descripcion_ingreso"));
                ingreso.setBranch_code(rs.getString("branch_code"));
                //ingreso.setReg_status(rs.getString("estado"));
                lista.add(ingreso);
                ingreso = null;
            }
        }
        catch (Exception e) {
            System.out.println("Error en ingresosPf2 en GestionCarteraDAO.java: "+e.toString());
            e.printStackTrace();
            throw new Exception("Error en ingresosPf2 en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    //20101230
    /**
     * Busca los tipos de titulo valor
     * @return Listado con los resultados encontrados
     * @throws Exception Cuando hay error
     */
    public ArrayList<String> titulosValor() throws Exception{
        ArrayList<String> lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "LIST_TITV";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<String>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while(rs.next()){
                lista.add(rs.getString("descripcion")+";_;"+rs.getString("referencia"));
            }
        }
        catch (Exception e) {
            throw new Exception("Error en titulosValor en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    public BeanGeneral datosFacturasNegocio(String codneg) throws Exception{
        BeanGeneral bean = new BeanGeneral();
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SRC_NEG_PLUS";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, codneg);
            st.setString(2, codneg);
            st.setString(3, codneg);
            rs = st.executeQuery();
            bean.setValor_01("0;_;Pagadas");
            bean.setValor_02("0;_;Vencidas;_;0");
            bean.setValor_03("0;_;Vigentes");
            while(rs.next()){
                if(rs.getString("estado").equals("Pagadas")){
                    bean.setValor_01(rs.getString("cant")+";_;"+rs.getString("estado"));
                }
                if(rs.getString("estado").equals("Vencidas")){
                    bean.setValor_02(rs.getString("cant")+";_;"+rs.getString("estado")+";_;"+rs.getString("saldo"));
                }
                if(rs.getString("estado").equals("Vigentes")){
                    bean.setValor_03(rs.getString("cant")+";_;"+rs.getString("estado"));
                }
                /*if(ind==1){ bean.setValor_01(rs.getString("cant")+";_;"+rs.getString("estado")); ind++;}
                if(ind==2){ bean.setValor_02(rs.getString("cant")+";_;"+rs.getString("estado")+";_;"+rs.getString("saldo")); ind++;}
                if(ind==3){ bean.setValor_03(rs.getString("cant")+";_;"+rs.getString("estado")); ind++;}*/
            }
        }
        catch (Exception e) {
            throw new Exception("Error en titulosValor en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return bean;
    }

    public ArrayList<Ingreso> ingresosNegocioSort(String cod_neg,String columna,String orden) throws Exception{
        ArrayList<Ingreso> lista = null;
        Ingreso ingreso = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "ING_NEG";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#columna", columna);
            sql = sql.replaceAll("#orden", orden);
            lista = new ArrayList<Ingreso>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, cod_neg);
            st.setString(2, cod_neg);
            st.setString(3, cod_neg);
            rs = st.executeQuery();
            while(rs.next()){
                ingreso = new Ingreso();
                ingreso.setConcepto(rs.getString("tipo_documento"));
                ingreso.setCreation_date(rs.getString("fecha_creacion"));
                ingreso.setNum_ingreso(rs.getString("num_ingreso"));
                ingreso.setFecha_consignacion(rs.getString("fecha_consignacion"));
                ingreso.setVlr_ingreso(rs.getDouble("vlr_ingreso"));
                ingreso.setDescripcion_ingreso(rs.getString("descripcion"));
                ingreso.setBranch_code(rs.getString("branch_code"));
                ingreso.setReg_status(rs.getString("estado"));
                lista.add(ingreso);
                ingreso = null;
            }
        }
        catch (Exception e) {
            throw new Exception("Error en ingresosNegocio en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    public ArrayList<BeanGeneral> estadoCuenta(String codneg,String columna, String orden) throws Exception{
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "EST_CTA";
        String sql="";
        BeanGeneral beang = null;
        try {
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#columna", columna);
            sql = sql.replaceAll("#orden", orden);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, codneg);
            st.setString(2, codneg);
            rs = st.executeQuery();
            while(rs.next()){
                beang = new BeanGeneral();
                beang.setValor_01(rs.getString("fecha_documento"));
                beang.setValor_02(rs.getString("documento"));
                beang.setValor_03(rs.getString("tipo"));
                beang.setValor_04(Util.customFormat(rs.getDouble("valor_factura")));
                beang.setValor_05(Util.customFormat(rs.getDouble("valor_ingreso")));
                beang.setValor_06(Util.customFormat(rs.getDouble("vr_ajuste_ingreso")));
                beang.setValor_07(Util.customFormat(rs.getDouble("valor_aplicado")));
                beang.setValor_08(rs.getString("cuenta_ajuste"));
                beang.setValor_09(rs.getString("cuenta_cabecera"));
                if(beang.getValor_09().equals("")==false){
                    try {
                        beang.setValor_08(this.cuentasAjusteIngreso(rs.getString("documento")));
                    }
                    catch (Exception e) {
                        System.out.println("error: "+e.toString());
                        e.printStackTrace();
                    }
                }
                lista.add(beang);
                beang = null;
            }
        }
        catch (Exception e) {
            throw new Exception("Error en estadoCuenta en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    public String cuentasAjusteIngreso(String num_ingreso) throws Exception{
        String cuentas = "";
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CT_AJ";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, num_ingreso);
            rs = st.executeQuery();
            int ct = 0;
            while(rs.next()){
                cuentas += (ct!=0 ? "," : "") + rs.getString("cuentas");
                ct = 1;
            }
        }
        catch (Exception e) {
            throw new Exception("Error en cuentasAjusteIngreso en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return cuentas;
    }

    public ArrayList<BeanGeneral> estadoCuentaPf(String columna, String orden) throws Exception{
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "EST_CTP";
        String sql="";
        BeanGeneral beang = null;
        try {
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#columna", columna);
            sql = sql.replaceAll("#orden", orden);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while(rs.next()){
                beang = new BeanGeneral();
                beang.setValor_01(rs.getString("fecha_documento"));
                beang.setValor_02(rs.getString("documento"));
                beang.setValor_03(rs.getString("tipo"));
                beang.setValor_04(Util.customFormat(rs.getDouble("valor_factura")));
                beang.setValor_05(Util.customFormat(rs.getDouble("valor_ingreso")));
                beang.setValor_06(Util.customFormat(rs.getDouble("vr_ajuste_ingreso")));
                beang.setValor_07(Util.customFormat(rs.getDouble("valor_aplicado")));
                beang.setValor_08(rs.getString("cuenta_ajuste"));
                beang.setValor_09(rs.getString("cuenta_cabecera"));
                if(beang.getValor_09().equals("")==false){
                    try {
                        beang.setValor_08(this.cuentasAjusteIngreso(rs.getString("documento")));
                    }
                    catch (Exception e) {
                        System.out.println("error: "+e.toString());
                        e.printStackTrace();
                    }
                }
                lista.add(beang);
                beang = null;
            }
        }
        catch (Exception e) {
            throw new Exception("Error en estadoCuentaPf en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    public double sumatoria_ic_ia(String cod_neg) throws Exception{
        double suma = 0;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SUM_ICA";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, cod_neg);
            rs = st.executeQuery();
            if(rs.next()){
                suma = rs.getDouble("sum_ingresos");
            }
        }
        catch (Exception e) {
            throw new Exception("Error en sumatoria_ic_ia en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return suma;
    }

    public double sumatoria_ajustes(String cod_neg) throws Exception{
        double suma = 0;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SUM_AJ";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, cod_neg);
            rs = st.executeQuery();
            if(rs.next()){
                suma = rs.getDouble("sum_ajustes");
            }
        }
        catch (Exception e) {
            throw new Exception("Error en sumatoria_ajustes en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return suma;
    }

    public double sumatoria_nc(String cod_neg) throws Exception{
        double suma = 0;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SUM_NC";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, cod_neg);
            rs = st.executeQuery();
            if(rs.next()){
                suma = rs.getDouble("sum_nc");
            }
        }
        catch (Exception e) {
            throw new Exception("Error en sumatoria_nc en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return suma;
    }

    public ArrayList<Ingreso> ingresosPf(String col,String orden) throws Exception{
        ArrayList<Ingreso> lista = null;
        Ingreso ingreso = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "ING_PFS";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#orden", orden);
            sql = sql.replaceAll("#col", col);
            lista = new ArrayList<Ingreso>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while(rs.next()){
                ingreso = new Ingreso();
                ingreso.setConcepto(rs.getString("tipo_documento"));
                ingreso.setCreation_date(rs.getString("fecha_creacion"));
                ingreso.setNum_ingreso(rs.getString("num_ingreso"));
                ingreso.setFecha_consignacion(rs.getString("fecha_consignacion"));
                ingreso.setVlr_ingreso(rs.getDouble("vlr_ingreso"));
                //ingreso.setDescripcion_ingreso(rs.getString("descripcion_ingreso"));
                ingreso.setBranch_code(rs.getString("branch_code"));
                ingreso.setReg_status(rs.getString("estado"));
                lista.add(ingreso);
                ingreso = null;
            }
        }
        catch (Exception e) {
            System.out.println("Error en ingresosPf en GestionCarteraDAO.java: "+e.toString());
            e.printStackTrace();
            throw new Exception("Error en ingresosPf en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    public void actualizarEstadoCartera(String codcli, String estado) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        String query = "UPD_SCLI";
        String sql="";
        try{
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, estado);
            st.setString(2, codcli);
            int k = st.executeUpdate();
        }
        catch (Exception e) {
            System.out.println("Error en actualizarEstadoCartera en GestionCarteraDAO.java: "+e.toString());
            e.printStackTrace();
            throw new Exception("Error en actualizarEstadoCartera en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
    }

    public int conteoFacturasNegocio(String cod_neg) throws Exception{
        int num=0;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "COUNT_FAC";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, cod_neg);
            rs = st.executeQuery();
            if(rs.next()){
                num = rs.getInt("cantidad");
            }
        }
        catch (Exception e) {
            System.out.println("Error en conteoFacturasNegocio en GestionCarteraDAO.java: "+e.toString());
            e.printStackTrace();
            throw new Exception("Error en conteoFacturasNegocio en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return num;
    }

    public String UpCP(String a) throws Exception{
      Connection con = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      String query = "UP_SERIES";
      String ret = "";
      try {
          con = this.conectarJNDI(query);
          if (con != null) {
              ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
              ps.setString(1, a);
              rs = ps.executeQuery();
              if (rs.next()) {
                  ret = rs.getString(1);
              }
          }
      }catch (Exception ex)
            {
                ex.printStackTrace();
                throw new Exception(ex.getMessage());
            }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
            return ret;
      }

     /**
     * Trae los textos de la carta referenciada
     * @param referencia codigo de la carta
     * @return Listado de textos
     * @throws Exception Cuando hay error
     */
    public ArrayList<String> textoCarta(String referencia) throws Exception{
        ArrayList<String> lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CARTA_TEXT";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<String>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, referencia);
            rs = st.executeQuery();
            while(rs.next()){
                lista.add(rs.getString("table_code")+";_;"+rs.getString("dato"));
            }
        }
        catch (Exception e) {
            throw new Exception("Error en facturasNegocio en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    /**
     * Lista de las gestiones pendientes
     * @param documento El negocio a buscar
     * @return Listado con los datos encontrados
     * @throws Exception Cuando hay un error
     */
    public ArrayList<BeanGeneral> gestionesPendientes(String login) throws Exception{
        ArrayList<BeanGeneral> lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "GESTION_ALERT";
        BeanGeneral bean = null;
        String sql="";
        try {
            con = this.conectarJNDI(query);
            //Agragamos la condicion para el fintro de usuarios
            String sql1 = "SELECT dato FROM tablagen "
                    + "WHERE table_type like 'SECURE_ECA%' "
                    + "AND referencia='GestionesPend' and table_code='" + login + "'";
            
            st = con.prepareStatement(sql1);
            rs = st.executeQuery();
            sql = this.obtenerSQL(query);
           
            if (rs.next()) {
                sql = sql.replaceAll("#FILTROUSER", "");
            } else {
                sql = sql.replaceAll("#FILTROUSER", "AND x.creation_user='" + login + "'");
            }

            lista = new ArrayList<BeanGeneral>();
            
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while(rs.next()){
                bean = new BeanGeneral();
                bean.setValor_01(rs.getString("observacion"));
                bean.setValor_02((rs.getString("creation_date")).substring(0, 19));
                bean.setValor_03(rs.getString("tipo_gestion"));
                bean.setValor_04(rs.getString("fecha_prox_gestion"));
                bean.setValor_05(rs.getString("prox_accion"));
                bean.setValor_06(rs.getString("documento"));
                bean.setValor_07(rs.getString("creation_user"));
                bean.setValor_08(rs.getString("id"));
                bean.setValor_09(rs.getString("tipo"));
                bean.setValor_10(rs.getString("dato"));
                bean.setValor_11(rs.getString("nomcli"));
                lista.add(bean);
                bean = null;
            }
        }
        catch (Exception e) {
            throw new Exception("Error en gestionesPendientes  en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    /**
     * Lista los fomularios mas recientes del cliente
     * @param negocio del cliente
     * @return Listado con los datos encontrados
     * @throws Exception Cuando hay un error
     */
    public ArrayList<BeanGeneral> buscarForm(String negocio) throws Exception{
        ArrayList<BeanGeneral> lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "BUSCAR_FORM";
        BeanGeneral bean = null;
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<BeanGeneral>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1,negocio);
            rs = st.executeQuery();
            while(rs.next()){
                bean = new BeanGeneral();
                bean.setValor_01(rs.getString("fecha"));
                bean.setValor_03(rs.getString("negocio"));
                bean.setValor_02(rs.getString("formulario"));
                lista.add(bean);
                bean = null;
            }
        }
        catch (Exception e) {
            throw new Exception("Error en buscarForm en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }


    /**
     * Datos de las facturas de un negocio
     * @param cod_neg Codigo del negocio
     * @return Listado con los datos
     * @throws Exception Cuando hay error
     */
    public ArrayList<factura> facturasNegocioClie(String cod_neg) throws Exception{
        ArrayList<factura> lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "ESTADO_CUENTA_CLIENTE";
        String sql="";
        factura f = null;
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<factura>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, cod_neg);
            rs = st.executeQuery();
            while(rs.next()){
                f = new factura();
                f.setFecha_vencimiento(rs.getString("fecha_vencimiento"));
                f.setTipo_documento(rs.getString("tipo"));
                f.setEstado(rs.getString("estado"));
                f.setDias_mora(rs.getInt("dias"));
                f.setValor_factura(rs.getDouble("valor_factura"));
                f.setValor_saldo(rs.getDouble("valor_saldo"));
                f.setValor_facturame(rs.getDouble("apagar"));
                f.setFecha_ultimo_pago(rs.getString("pago"));
                f.setDiferencia(rs.getString("diferencia"));

                lista.add(f);
                f = null;
            }
        }
        catch (Exception e) {
            throw new Exception("Error en facturasNegocio en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }


     /**
     * Trae los negocios que tengan una factura que vence en n dias
     * @param vence numero dias en que vence la factura
     * @param dias numero dias antes de vencimiento de la factura para gestion preventiva
     * @return Listado de negocios
     * @throws Exception Cuando hay error
     */
    public ArrayList<String> facturasPorVencer(int vence,int dias) throws Exception{
        ArrayList<String> lista = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "FACTURA_POR_VENCER";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<String>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setInt(1, dias);
            st.setInt(2, vence);
            rs = st.executeQuery();
            while(rs.next()){
                lista.add(rs.getString("negasoc")+";"+rs.getString("email")+";"+rs.getString("documento")+";"+rs.getString("fecha"));
            }
        }
        catch (Exception e) {
            throw new Exception("Error en facturasNegocio en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }
    
    
    public Cliente estadoClienteCartera(String nit) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "INFORMCACION_CLIENTES";
        Cliente cliente = null;
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            cliente = new Cliente();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, nit);
            rs = st.executeQuery();
            if (rs.next()) {

                cliente.setNomcli(rs.getString("nomcli"));
                cliente.setNit(rs.getString("nit"));
                cliente.setEstado(rs.getString("estado_gestion_cartera"));
                cliente.setCodcli(rs.getString("codcli"));

            }
        } catch (Exception e) {
            throw new Exception("Error en datosHeader en GestionCarteraDAO.java: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return cliente;
    }

    public String[] getFacturasAGestionar(String negocio) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SQL_FACTURAS_GESTION";
        String sql = "";
        String[] resultado = new String[3];
        String documentos = "";
        String facts = "";
        String valor = "";

        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, negocio);
            
            System.out.println(st);
            
            rs = st.executeQuery();
            while (rs.next()) {

                documentos += rs.getString("documento") + ",";
                facts += rs.getString("documento") + ";_;";
                valor += rs.getString("valor_saldo") + ";_;";
            }
            
            //verificamos que no esten vacias los resultados.
            if (!documentos.equals("") && !facts.equals("") && !valor.equals("")) {
                resultado[0] = documentos;
                resultado[1] = facts;
                resultado[2] = valor;
            } else {
                resultado[0] = "";
                resultado[1] = "";
                resultado[2] = "";
            }


        } catch (Exception e) {
            throw new Exception("Error en datosHeader en GestionCarteraDAO.java: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return resultado;
    }
    
    public ArrayList<BeanGeneral> getGestionesPorNegocio(String negocio) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SQL_FACTURAS_GESTION_V1";
        String sql = "";
        BeanGeneral beanGeneral=null;
        ArrayList<BeanGeneral> resultado = null;
        
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, negocio);
            rs = st.executeQuery();
            resultado =new ArrayList<BeanGeneral>();
            while (rs.next()) {
                beanGeneral=new BeanGeneral();
                beanGeneral.setValor_01(rs.getString("observacion"));
                beanGeneral.setValor_02(rs.getString("tipo_gestion"));
                beanGeneral.setValor_03(rs.getString("pro_accion"));
                beanGeneral.setValor_04(rs.getString("fecha_prox_gestion"));
                beanGeneral.setValor_05(rs.getString("creation_date"));
                beanGeneral.setValor_06(rs.getString("creation_user"));
                resultado.add(beanGeneral);
               
            }
            
        } catch (Exception e) {
            throw new Exception("Error en datosHeader en GestionCarteraDAO.java: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return resultado;
    }
    
    public void insCompromisoPago(String documento,String observacion,double valorPagar, String fechaPagar, String ciudad, String barrio, String direccion, String usuario) throws Exception{
        PreparedStatement ps = null;
        Connection con = null;
        String query = "INSERT_COMPROMISO_PAGO";
        String sql="";
        int k = 0;
        try {          
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, documento);
            ps.setString(2, observacion);
            ps.setDouble(3, valorPagar);
            ps.setString(4, fechaPagar);
            ps.setString(5, ciudad);
            ps.setString(6, barrio);
            ps.setString(7, direccion);
            ps.setString(8, usuario);
            k = ps.executeUpdate();
            
        }
        catch (Exception e) {
            throw new Exception("Error en insCompromisoPago en GestionCarteraDAO.java: "+e.toString());
        }
        finally {
            if(ps!=null){ try{ps.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
    }
    
    public ArrayList<BeanGeneral> getCompromisosPorNegocio(String negocio) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SQL_BUSCA_COMPROMISOS_PAGO";
        String sql = "";
        BeanGeneral beanGeneral=null;
        ArrayList<BeanGeneral> resultado = null;
        
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, negocio);
            rs = st.executeQuery();
            resultado =new ArrayList<BeanGeneral>();
            while (rs.next()) {
                beanGeneral=new BeanGeneral();
                beanGeneral.setValor_01(rs.getString("observacion"));
                beanGeneral.setValor_02(rs.getString("valor_a_pagar"));
                beanGeneral.setValor_03(rs.getString("fecha_a_pagar"));     
                beanGeneral.setValor_04(rs.getString("direccion"));   
                beanGeneral.setValor_05(rs.getString("barrio"));   
                beanGeneral.setValor_06(rs.getString("ciudad"));   
                beanGeneral.setValor_07(rs.getString("creation_date"));
                beanGeneral.setValor_08(rs.getString("creation_user"));
                resultado.add(beanGeneral);               
            }
            
        } catch (Exception e) {
            throw new Exception("Error en datosHeader en GestionCarteraDAO.java: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return resultado;
    }
    

    
   

}