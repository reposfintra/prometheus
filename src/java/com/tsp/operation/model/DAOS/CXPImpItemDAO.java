/***********************************************************************************
 * Nombre clase : ...............  CXPImpItemDAO.java                                   *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.                                       *
 * Autor :....................... Ing. David Lamadrid                               *
 * Fecha :....................... 15 de noviembre de 2005, 11:02 AM                   *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  dlamadrid
 */
public class CXPImpItemDAO
{
        
        private CXPImpItem impuestoItem;
        private Vector vImpuestosItem;
        /** Creates a new instance of CXPImpItemDAO */
        public CXPImpItemDAO ()
        {
        }
        
        /**
         * Getter for property impuestoItem.
         * @return Value of property impuestoItem.
         */
        public com.tsp.operation.model.beans.CXPImpItem getImpuestoItem ()
        {
                return impuestoItem;
        }
        
        /**
         * Setter for property impuestoItem.
         * @param impuestoItem New value of property impuestoItem.
         */
        public void setImpuestoItem (com.tsp.operation.model.beans.CXPImpItem impuestoItem)
        {
                this.impuestoItem = impuestoItem;
        }
        
        /**
         * Getter for property vImpuestosItem.
         * @return Value of property vImpuestosItem.
         */
        public java.util.Vector getVImpuestosItem ()
        {
                return vImpuestosItem;
        }
        
        /**
         * Setter for property vImpuestosItem.
         * @param vImpuestosItem New value of property vImpuestosItem.
         */
        public void setVImpuestosItem (java.util.Vector vImpuestosItem)
        {
                this.vImpuestosItem = vImpuestosItem;
        }
        
}
