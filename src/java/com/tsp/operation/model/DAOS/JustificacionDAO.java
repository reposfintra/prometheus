/************************************************************************
 * Nombre clase: JustificacionDAO.java                                  *
 * Descripción: Clase que maneja las consultas de las Justificaciones.  *
 * Autor: Ing. Jose de la rosa                                          *
 * Fecha: 13 de marzo de 2006, 03:43 PM                                 *
 * Versión: Java 1.0                                                    *
 * Copyright: Fintravalores S.A. S.A.                              *
 ***********************************************************************/

package com.tsp.operation.model.DAOS;

import java.sql.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;
import java.util.*;

public class JustificacionDAO  extends MainDAO{
    private Justificacion justificacion;
    private Vector VecJustificacion;
    /** Creates a new instance of JustificacionDAO */
    public JustificacionDAO () {
        super ("JustificacionDAO.xml");
    }
    
    /**
     * Getter for property justificacion.
     * @return Value of property justificacion.
     */
    public com.tsp.operation.model.beans.Justificacion getJustificacion () {
        return justificacion;
    }
    
    /**
     * Setter for property justificacion.
     * @param justificacion New value of property justificacion.
     */
    public void setJustificacion (com.tsp.operation.model.beans.Justificacion justificacion) {
        this.justificacion = justificacion;
    }
    
    /**
     * Getter for property VecJustificacion.
     * @return Value of property VecJustificacion.
     */
    public java.util.Vector getVecJustificacion () {
        return VecJustificacion;
    }
    
    /**
     * Setter for property VecJustificacion.
     * @param VecJustificacion New value of property VecJustificacion.
     */
    public void setVecJustificacion (java.util.Vector VecJustificacion) {
        this.VecJustificacion = VecJustificacion;
    }
    
    /**
     * Metodo: ingresarJustificacion, permite ingresar un registro a la tabla justificacion.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String ingresarJustificacion ()throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "";
        try {
            st = this.crearPreparedStatement ("SQL_INSERT");
            st.setString (1, justificacion.getPrograma ());
            st.setString (2, justificacion.getTipo ());
            st.setString (3, justificacion.getDocumento ());
            st.setString (4, justificacion.getDescripcion ());
            st.setString (5, justificacion.getUsuario_modificacion ());
            st.setString (6, justificacion.getUsuario_creacion ());
            st.setString (7, justificacion.getBase ());
            st.setString (8, justificacion.getDistrito ());
            ////System.out.println ("Query: "+st);
            sql = st.toString();
            //st.execute ();
        }catch(SQLException e){
            throw new SQLException ("ERROR DURANTE EL INGRESO DE UNA JUSTIFICACIÓN " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally{
            if (st != null){ st.close (); }
            this.desconectar ("SQL_INSERT");
        }
        return sql;
    }
    
    /**
     * Metodo: ingresarJustificacion, permite ingresar un registro a la tabla justificacion.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */    
    public void listarJustificaciones()throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = this.crearPreparedStatement("SQL_LISTAR");
            st.setString (1, justificacion.getPrograma ()+"%");
            st.setString (2, justificacion.getTipo ()+"%");
            st.setString (3, justificacion.getDocumento ()+"%");
            ////System.out.println(st);
            rs = st.executeQuery();
            while(rs.next()){
                justificacion.setTipo (rs.getString("tipo"));
                justificacion.setDocumento (rs.getString("documento"));
                justificacion.setDescripcion (rs.getString("descripcion"));
                justificacion.setPrograma (rs.getString("modulo"));
                VecJustificacion.addElement(justificacion);
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS JUSTIFICACIONES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){ st.close(); }
            this.desconectar("SQL_LISTAR");
        }
        
    }
    
}
