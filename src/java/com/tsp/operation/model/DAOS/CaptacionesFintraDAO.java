/**********************************************
 * Nombre       Clase  CaptacionesFintraDAO.java 
 * Autor        ING JULIO BARROS RUEDA
 * Fecha        06/06/2007
 * Copyright    Fintravalores S.A.
 **********************************************/


package com.tsp.operation.model.DAOS;


import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;


public class CaptacionesFintraDAO extends MainDAO{
    
    private Planilla pla;
    
       
    public CaptacionesFintraDAO() {
        super("CaptacionesFintraDAO.xml");  
    }
    public CaptacionesFintraDAO(String dataBaseName) {
        super("CaptacionesFintraDAO.xml", dataBaseName);  
    }
    
    
    public String ConsecutivoCaptaciones(String operacion)throws Exception {
        Connection con = null;
        String  consecutivo  = "" ;
        boolean            Query1 = false;
        PreparedStatement  st     = null;        
        ResultSet          rs     = null;        
        String             query  = "SQL_RETORNAR_SERIE_CAPTACION";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, operacion);
            rs = st.executeQuery();
            while (rs.next()){
                consecutivo = rs.getString("prefix")+(rs.getInt("last_number"));
            }
        }}catch(Exception e){
            throw new SQLException("DAO:  ConsecutivoCaptaciones() : "+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return consecutivo;
    } 
    
    
    
    public String Registrar_Operacion(Captacion captacion) throws Exception {
        String SQL="";
        StringStatement ps = null;;
        String SQL_INSERTAR_OPERACION_CAPTACIONES      = "";
        try {
            SQL_INSERTAR_OPERACION_CAPTACIONES         = this.obtenerSQL("SQL_INSERTAR_OPERACION_CAPTACIONES");
            ps = new StringStatement(SQL_INSERTAR_OPERACION_CAPTACIONES,true);
            ps.setString(1 , "");
            ps.setString(2 , "FINV");
            ps.setString(3 , captacion.getProveedor());
            ps.setString(4 , captacion.getTipo_operacion());
            ps.setString(5 , captacion.getTipo_documento());
            ps.setString(6 , captacion.getDocumento());
            ps.setString(7 , captacion.getFecha_documento());
            ps.setString(8 , captacion.getFecha_inicio());
            ps.setDouble(9 , captacion.getVlr_capital());
            ps.setString(10, captacion.getMoneda());
            ps.setDouble(11, captacion.getInteres());
            ps.setString(12, captacion.getFrecuencia());
            ps.setString(13, captacion.getClase_ingreso());
            ps.setString(14, captacion.getRef1());
            ps.setString(15, captacion.getRef2());
            ps.setString(16, captacion.getRef3());
            ps.setString(17, captacion.getFecha_liquidacion());
            ps.setString(18, captacion.getFecha_corte());
            ps.setDouble(19, captacion.getVlr_intereses());
            ps.setDouble(20, captacion.getVlr_retefuente());
            ps.setDouble(21, captacion.getVlr_total_capital());
            ps.setDouble(22, captacion.getVlr_nuevo_capital());
            ps.setString(23, captacion.getTipo_documento_previo());
            ps.setString(24, captacion.getDocumento_previo());
            ps.setString(25, captacion.getBase());
            ps.setString(26, captacion.getCreation_user());
            ps.setString(27, captacion.getCreation_user());
            ps.setDouble(28, captacion.getVlr_reteica());
            SQL +=ps.getSql()+";";
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new SQLException("Error en insertar Captacion....... "+e.toString()+ e.getMessage());
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
           }
        return SQL;
    }
    
    
    /**
     * M�todo buscar ExtractosPP realizados
     * @autor   JULIO BARROS RUEDA
     * @param   parametro de busqueda
     * @param   tipo, tipo de filtro
     * @throws  Exception
     * @version 1.0.
     **/
    public Propietario buscarPropietario(String propietario) throws Exception {
        Connection con = null;
        Propietario  Propietario  = new Propietario(); 
        PreparedStatement  st     = null;        
        ResultSet          rs     = null;        
        String             query  = "SQL_RETORNAR_PROPIETARIO_CAPTACION";
        String             query1 = "SQL_RETORNAR_PROPIETARIO_NIT";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, propietario);
            //System.out.println("ejecutando query Hpta"+propietario+"  "+st.toString());
            rs = st.executeQuery();
            if(rs.next()){
                Propietario.setCedula            ( rs.getString("nit")          );
                Propietario.setS_nombre          ( rs.getString("payment_name") );
                Propietario.setFrecuencia        ( rs.getString("frecuencia_captacion"));
                Propietario.setTasa_captacion    ( rs.getDouble("tasa_captacion"));
                Propietario.setCapital           ( rs.getDouble("vlr_capital"));
                Propietario.setInicio            ( rs.getString("fecha_inicio"));
                Propietario.setTasa_capital      ( rs.getDouble("interes"));
                Propietario.setFrecuencia_capital( rs.getString("frecuencia"));
                Propietario.setTipo_doc          ( rs.getString("tipo_documento"));
                Propietario.setDocumento         ( rs.getString("documento"));
                Propietario.setPerfil            (rs.getString("perfil"));
            }else{
                st = con.prepareStatement(this.obtenerSQL(query1));//JJCastro fase2
                st.setString(1, propietario);
                rs = st.executeQuery();
                while (rs.next()){
                    Propietario.setCedula        ( rs.getString("nit")          );
                    Propietario.setS_nombre      ( rs.getString("payment_name") );
                    Propietario.setFrecuencia    ( rs.getString("frecuencia_captacion"));
                    Propietario.setTasa_captacion( rs.getDouble("tasa_captacion"));
                }
            }
        }}catch(Exception e){
            throw new SQLException("DAO:  buscarPropietario() : "+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }        
         return Propietario;
    } 
    
    
    /**
     * M�todo que inserta registros a la tabla de anticipos para terceros
     * @autor....... jabarros
     * @throws...... Exception
     * @version..... 1.0.
     **/
    public synchronized String  InsertAnticipo(Captacion captacion)throws Exception {
        String SQL_INSERT_ANTICIPO_PP = this.obtenerSQL("SQL_INSERT_ANTICIPO_PP");
        StringStatement st = new StringStatement(SQL_INSERT_ANTICIPO_PP,true);
        
        try{
            String reanticipo = "N";
            double porcentaje      = this.descuento(captacion.getProveedor()); 
            
            double descuento       = ( captacion.getVlr_capital() * porcentaje )/100;
            double neto            =  captacion.getVlr_capital() - descuento ;
            double comision        =  2000 ;
            double consignar       = (int) Math.round( neto - comision );        
            
            st.setString( 1,  captacion.getDstrct() );
            st.setString( 2,  ""  );//agencia de Despacho
            st.setString( 3,  captacion.getProveedor()  );
            st.setString( 4,  ""         );
            st.setString( 5,  ""        );
            st.setString( 6,  "802022016" );
            st.setString( 7,  "01"     );
            st.setDouble( 8, captacion.getVlr_capital()  );
            st.setDouble( 9, captacion.getVlr_capital()  );
            st.setString(10, captacion.getMoneda()       );
            st.setString(11, "now()"                     );
            st.setString(12, captacion.getCreation_user());
            st.setString(13, captacion.getBase()         );
            st.setString(14, reanticipo                  );
            
            st.setDouble(15 , porcentaje   );
            st.setDouble(16 , descuento    );
            st.setDouble(17 , neto         );
            st.setDouble(18 , comision     );
            st.setDouble(19 , consignar    );
            st.setString(20 , ""  ); //cedula conductor  
            st.setString(21 , ""           );
            st.setString(22 , "S" );
            st.setString(23 , captacion.getCreation_user() );
            st.setString(24 , "now()" );
            st.setString(25 , "");//beneficiarioPP.getC_nit()           );
            st.setString(26 , "");//beneficiarioPP.getC_payment_name()  );
            st.setString(27 , "");//beneficiarioPP.getC_tipo_cuenta()   );
            st.setString(28 , "");//beneficiarioPP.getC_branch_code()   );
            st.setString(29 , "");//beneficiarioPP.getC_bank_account()  );
            st.setString(30 , "");//beneficiarioPP.getC_numero_cuenta() );
            st.setInt   (31 , 0 );//secuencia );
            
            st.setString(32, "");//banco);
            st.setString(33, "");//cta);
            st.setString(34, "");//tipoCta);
            return st.getSql()+";"; 
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception(" InsertAnticipo"+ e.getMessage());
        }
finally{//JJCastro fase2
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
           }
    }
    
       /**
     * Metodo que busca el descuento para el nit
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
    public double descuento(String nit)throws Exception{
        Connection con = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_DESCUENTO_NIT";
        double            valor       = 0;
         try{ 
             con = this.conectarJNDI(query);
             if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, nit       );
            rs = st.executeQuery();
            if ( rs.next() )
                valor = rs.getDouble(1);
            
             }}catch(Exception e) {
             throw new SQLException(" descuento  -> " +e.getMessage());
         }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        } 
         return valor;
    }
    
       /**
     * Metodo que busca las captaciones d eun propietario
     * @autor: ....... Julio Barros
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
    public Vector MovimientosProveedorCaptaciones(String nit,String fechai,String fechaf)throws Exception{
        Connection con = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_CAPTACIONES_PROPIETARIO"; 
        Vector       captaciones      = new Vector();
         try{ 
             con = this.conectarJNDI(query);
             if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, nit       );
            st.setString(2, fechai+" 00:00:00");
            st.setString(3, fechaf+" 23:59:59");
            //System.out.println("query "+st.toString());
            rs = st.executeQuery();
            while ( rs.next() ){
                Captacion  captacion   = new Captacion();
                captacion =  LoadCaptacion(rs);  
                captaciones.add(captacion);
            }
            
         }}catch(Exception e) {
             throw new SQLException(" MovimientosProveedorCaptaciones  -> " +e.getMessage());
         }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        } 
         return captaciones;
    }
    
    
      /**
     * Metodo que busca las captaciones d eun propietario
     * @autor: ....... Julio Barros
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
    public Vector ProveedoresCaptacionesMesAMes(String fechai,String fechaf)throws Exception{
        PreparedStatement  st      = null;
        ResultSet          rs      = null; 
        Connection         con     = null;
        String            query       = "SQL_PROPIETARIOS_MES_A_MES"; 
        Vector       captaciones      = new Vector();
        String           nits         = "";
        
         try{
            nits =  ReornarProveedores();
            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
            String sql   =   this.obtenerSQL( query ).replaceAll("#PROVEEDORES#", nits );
            st = con.prepareStatement(sql);
            st.setString(1, fechai+" 00:00:00");
            st.setString(2, fechaf+" 23:59:59");
            rs = st.executeQuery();
            while ( rs.next() ){
                Captacion  captacion   = new Captacion();
                captacion =  LoadCaptacion(rs);  
                captaciones.add(captacion);
            }
            
         }}catch(Exception e) {
             throw new SQLException(" ProveedoresCaptacionesMesAMes  -> " +e.getMessage());
         }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        } 
         return captaciones;
    }
    
        /**
     * 
     * @autor: ....... Julio Barros
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
    public String ReornarProveedores()throws Exception{
        Connection con = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_PROVEEDORES_CAPTACIONES";
        String            proveedores = "";
        int               cen         = 0;
         try{ 
             con = this.conectarJNDI(query);
             if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            while ( rs.next() ){
                if(cen != 0)
                    proveedores +=",";
                proveedores +="'"+rs.getString("proveedor")+"'";
                cen++;
            }
         }}catch(Exception e) {
             throw new SQLException(" ReornarProveedores  -> " +e.getMessage());
         }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        } 
         return proveedores;
    }


/**
 *
 * @param rs
 * @return
 * @throws Exception
 */
    public Captacion LoadCaptacion(ResultSet rs) throws Exception{
        try{
            Captacion  captacion   = new Captacion();
            captacion.setReg_status             (rs.getString("reg_status")); 
            captacion.setDstrct                 (rs.getString("dstrct")); 
            captacion.setProveedor              (rs.getString("proveedor")); 
            captacion.setTipo_operacion         (rs.getString("tipo_operacion")); 
            captacion.setTipo_documento         (rs.getString("tipo_documento")); 
            captacion.setDocumento              (rs.getString("documento")); 
            captacion.setFecha_documento        (rs.getString("fecha_documento")); 
            captacion.setFecha_inicio           (rs.getString("fecha_inicio")); 
            captacion.setVlr_capital            (Double.parseDouble(rs.getString("vlr_capital"))); 
            captacion.setMoneda                 (rs.getString("moneda")); 
            captacion.setInteres                (Double.parseDouble(rs.getString("interes")));
            captacion.setFrecuencia             (rs.getString("frecuencia"));
            captacion.setClase_ingreso          (rs.getString("clase_ingreso"));
            captacion.setRef1                   (rs.getString("ref1"));
            captacion.setRef2                   (rs.getString("ref2"));
            captacion.setRef3                   (rs.getString("ref3"));
            captacion.setFecha_liquidacion      (rs.getString("fecha_liquidacion"));
            captacion.setFecha_corte            (rs.getString("fecha_corte"));
            captacion.setVlr_intereses          (Double.parseDouble(rs.getString("vlr_intereses")));
            captacion.setVlr_retefuente         (Double.parseDouble(rs.getString("vlr_retefuente")));
            captacion.setVlr_reteica         (Double.parseDouble(rs.getString("vlr_reteica")));
            captacion.setVlr_total_capital      (Double.parseDouble(rs.getString("vlr_total_capital")));
            captacion.setVlr_nuevo_capital      (Double.parseDouble(rs.getString("vlr_nuevo_capital")));
            captacion.setTipo_documento_previo  (rs.getString("tipo_documento_previo"));
            captacion.setDocumento_previo       (rs.getString("documento_previo"));
            captacion.setBase                   (rs.getString("base"));
            captacion.setCreation_date          (rs.getString("creation_date"));
            captacion.setCreation_user          (rs.getString("creation_user"));
            captacion.setLast_update            (rs.getString("last_update"));
            captacion.setUser_update            (rs.getString("user_update"));
            captacion.setCod_cuenta_contable    (rs.getString("cod_cuenta_contable"));
            captacion.setTransaccion_id         (rs.getString("transaccion_id"));
            captacion.setDoc_contabilidad       (rs.getString("doc_contabilidad"));
            captacion.setFecha_contabilazo      (rs.getString("fecha_contabilazo"));
            captacion.setUsuario_contabilizo    (rs.getString("usuario_contabilizo"));
            captacion.setFecha_anulacion_con    (rs.getString("fecha_anulacion_con"));
            captacion.setUsuario_anulacion_con  (rs.getString("usuario_anulacion_con"));
            captacion.setFecha_recibo_con       (rs.getString("fecha_recibo_con"));
            captacion.setUsuario_recibio_con    (rs.getString("usuario_recibio_con")); 
            return captacion;
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception(" LoadCaptacion  -> " +e.getMessage());
        }
    }
    
}
