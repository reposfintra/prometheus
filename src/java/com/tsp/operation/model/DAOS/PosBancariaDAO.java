/****************************************************************************
 *      Nombre Clase.................   PosBancaria.java    
 *      Descripci�n..................   DAO del archivo posicion_bancaria
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   18.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 ****************************************************************************/
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  Andres
 */
public class PosBancariaDAO {
    
    private PosBancaria posbanc;
    private Vector vector;
    private Vector corridas;
    
    private static final String SQL_OBTENER = 
            "SELECT * " +
            "FROM   fin.posicion_bancaria " +
            "WHERE  dstrct = ? AND agency_id=? AND branch_code=? " +
            "       AND bank_account_no=? AND fecha=? " +
            "       AND reg_status<>'A'";
    
    private static final String SQL_INGRESAR = 
            "INSERT INTO fin.posicion_bancaria(" +
            "   dstrct, " +
            "   agency_id, " +
            "   branch_code, " +
            "   bank_account_no, " +
            "   fecha, " +
            "   saldo_inicial, " +
            "   saldo_anterior, " +
            "   anticipos, " +
            "   proveedores, " +
            "   nuevo_saldo, " +
            "   cupo, " +
            "   creation_user, " +
            "   base) " +
            "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    
    private static final String SQL_EXISTE = 
            "SELECT reg_status " +
            "FROM   fin.posicion_bancaria " +
            "WHERE  dstrct = ? AND agency_id=? AND branch_code=? " +
            "       AND bank_account_no=? AND fecha=? ";
    
    private static final String SQL_UPDATE = 
            "UPDATE fin.posicion_bancaria SET " +
            "   reg_status=?, " +
            "   saldo_inicial=?, " +
            "   saldo_anterior=?, " +
            "   anticipos=?, " +
            "   proveedores=?, " +
            "   nuevo_saldo=?, " +
            "   cupo=?, " +
            "   user_update=?, " +
            "   last_update='now()' " +
            "WHERE " + 
            "   dstrct = ? AND agency_id=? AND branch_code=? AND " +
            "   bank_account_no=? AND fecha=?";
    
    private static final String SQL_LISTAR =
            "SELECT * " +
            "FROM fin.posicion_bancaria " +
            "WHERE " + 
            "   agency_id=? AND branch_code=? AND " +
            "   fecha=? AND reg_status<>'A'";
    
    private static final String SQL_LISTAR_AGC =
            "SELECT * " +
            "FROM fin.posicion_bancaria " +
            "WHERE " + 
            "   dstrct = ? AND agency_id=? AND fecha=? AND reg_status<>'A'";
    
    private static final String SQL_LISTAR_BAN =
            "SELECT * " +
            "FROM fin.posicion_bancaria " +
            "WHERE " + 
            "   dstrct = ? AND branch_code=? AND fecha=? AND reg_status<>'A'";
    
    private static final String SQL_LISTAR_FEC =
            "SELECT * " +
            "FROM fin.posicion_bancaria " +
            "WHERE " + 
            "  dstrct = ? AND fecha=? AND reg_status<>'A'";
    
    private static final String SQL_CHEQUE_RUN_1 = 
            "SELECT " +
            "       inv_type, " +
            "       (loc_inv_orig - pp_amt_loc - pp_offset_amt + loc_atax_t_a ) AS total " +
            "FROM (" +
            "   SELECT  " +
            "           cheque_run_no, " +
            "           branch_code, " +
            "           bank_acct_no, " +
            "           run_start_date " +
            "   FROM    msf280_run_ctl " +
            "   WHERE " +
            "           acct_dstrct = ? " +
            "           AND branch_code = ? " +
            "           AND bank_acct_no = ? " +
            "           AND run_start_date = ? " +
            ") c1, msf260 c2  " +
            "WHERE  " +
            "       c2.branch_code = c1.branch_code  " +
            "       AND c2.bank_acct_no = c1.bank_acct_no  " +
            "       AND c2.cheque_run_no = c1.cheque_run_no ";
    
    private static final String SQL_CHEQUE_RUN_2 = 
            "SELECT " +
            "       inv_type, " +
            "       (loc_inv_orig - pp_amt_loc - pp_offset_amt + loc_atax_t_a ) AS total " +
            "FROM (" +
            "   SELECT  " +
            "           cheque_run_no, " +
            "           branch_code, " +
            "           bank_acct_no, " +
            "           run_start_date " +
            "   FROM    msf280_run_ctl " +
            "   WHERE " +
            "           acct_dstrct = ? " +
            "           AND branch_code = ? " +
            "           AND bank_acct_no = ? " +
            "           AND run_start_date BETWEEN ? AND ? " +
            ") c1, msf260 c2  " +
            "WHERE  " +
            "       c2.branch_code = c1.branch_code  " +
            "       AND c2.bank_acct_no = c1.bank_acct_no  " +
            "       AND c2.cheque_run_no = c1.cheque_run_no ";
    
    private static final String SQL_REGANTERIOR = 
            "SELECT a.* " +
            "FROM   fin.posicion_bancaria  a, (" +
            "   SELECT  dstrct, " +
            "           agency_id, " +
            "           branch_code, " +
            "           bank_account_no, " +
            "           MAX(fecha) as fecha " +
            "   FROM    fin.posicion_bancaria " +
            "   WHERE   dstrct = ? " +
            "           AND agency_id = ? " +
            "           AND branch_code = ? " +
            "           AND bank_account_no = ? " +
            "   GROUP BY 1,2,3,4  " +
            ") b " +
            "WHERE  b.dstrct = a.dstrct " +
            "       AND b.agency_id = a.agency_id  " +
            "       AND b.branch_code = a.branch_code  " +
            "       AND b.bank_account_no = a.bank_account_no " +
            "       AND b.fecha = a.fecha ";
    
    /** Creates a new instance of PosBancariaDAO */
    public PosBancariaDAO() {
    }
    
    /**
     * Obtiene un registro del archivo posicion_bancaria.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cia C�digo del Distrito
     * @param agencia C�digo de la agencia
     * @param banco Nombre del banco
     * @param sucursal Nombre de la sucursal del banco
     * fecha Fecha
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void obtener(String cia, String agencia, String banco, String sucursal, String fecha) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        this.posbanc = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(this.SQL_OBTENER);
                st.setString(1, cia);
                st.setString(2, agencia);
                st.setString(3, banco);
                st.setString(4, sucursal);
                st.setString(5, fecha);
                
                //////System.out.println("....................... obtener de posicion_bancaria : " + st.toString());
                
                rs = st.executeQuery();
                
                if (rs.next()){
                    this.posbanc = new PosBancaria();
                    this.posbanc.Load(rs);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR EN OBTENCION DE LA POSICION BANCARIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Verifica la existence de un registro en el archivo posicion_bancaria.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cia C�digo del Distrito
     * @param agencia C�digo de la agencia
     * @param banco Nombre del banco
     * @param sucursal Nombre de la sucursal del banco
     * @fecha Fecha Fecha de la posici�n bancaria.
     * @return El reg_status del registro si existe, de lo contrario null
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String existe(String cia, String agencia, String banco, String sucursal, String fecha) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String reg_status = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(this.SQL_EXISTE);
                st.setString(1, cia);
                st.setString(2, agencia);
                st.setString(3, banco);
                st.setString(4, sucursal);
                st.setString(5, fecha);
                
                //////System.out.println("....................... existe en posicion_bancaria : " + st.toString());
                
                rs = st.executeQuery();
                
                if (rs.next()){
                    reg_status =  rs.getString("reg_status");
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR EN EXISTE LA POSICION BANCARIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        return reg_status;
    }
    
    /**
     * Ingresa un registro en el archivo posicion_bancaria.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void ingresar() throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(this.SQL_INGRESAR);
                
                st.setString(1, this.posbanc.getDstrct());
                st.setString(2, this.posbanc.getAgency_id());
                st.setString(3, this.posbanc.getBranch_code());
                st.setString(4, this.posbanc.getBank_account_no());
                st.setString(5, this.posbanc.getFecha());
                st.setDouble(6, this.posbanc.getSaldo_inicial());
                st.setDouble(7, this.posbanc.getSaldo_anterior());
                st.setDouble(8, this.posbanc.getAnticipos());
                st.setDouble(9, this.posbanc.getProveedores());
                st.setDouble(10, this.posbanc.getNuevo_saldo());
                st.setDouble(11, this.posbanc.getCupo());
                st.setString(12, this.posbanc.getCreation_user());
                st.setString(13, this.posbanc.getBase());
                
                //////System.out.println("....................... ingreso en posicion_bancaria : " + st.toString());
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR EN EL INGRESO DE LA POSICION BANCARIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }        
    }
    
    /**
     * Actualiza un registro en el archivo posicion_bancaria.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void actualizar() throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(this.SQL_UPDATE);
                
                st.setString(1, this.posbanc.getReg_status());
                st.setDouble(2, this.posbanc.getSaldo_inicial());
                st.setDouble(3, this.posbanc.getSaldo_anterior());
                st.setDouble(4, this.posbanc.getAnticipos());
                st.setDouble(5, this.posbanc.getProveedores());
                st.setDouble(6, this.posbanc.getNuevo_saldo());
                st.setDouble(7, this.posbanc.getCupo());
                st.setString(8, this.posbanc.getCreation_user());
                st.setString(9, this.posbanc.getDstrct());
                st.setString(10, this.posbanc.getAgency_id());
                st.setString(11, this.posbanc.getBranch_code());
                st.setString(12, this.posbanc.getBank_account_no());
                st.setString(13, this.posbanc.getFecha());
                
                //////System.out.println("....................... actualizacion en posicion_bancaria : " + st.toString());
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR EN LA ACTUALIZACION DE LA POSICION BANCARIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }        
    }
    
    /**
     * Obtiene una lista de registros del archivo posicion_bancaria.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cia C�digo del Distrito
     * @param agencia C�digo de la agencia
     * @param banco Nombre del banco
     * @param fecha Fecha
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void listar(String cia, String agencia, String banco, String fecha) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        this.posbanc = null;
        this.vector = new Vector();
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                
                if( agencia.length()==0 && banco.length()==0){
                    st = con.prepareStatement(this.SQL_LISTAR_FEC);
                    st.setString(1, cia);
                    st.setString(2, fecha);
                } else if ( agencia.length()!=0 && banco.length()==0 ) {
                    st = con.prepareStatement(this.SQL_LISTAR_AGC);
                    st.setString(1, cia);
                    st.setString(2, agencia);
                    st.setString(3, fecha);
                } else if ( agencia.length()==0 && banco.length()!=0) {
                    st = con.prepareStatement(this.SQL_LISTAR_BAN);
                    st.setString(1, cia);
                    st.setString(2, banco);
                    st.setString(3, fecha);
                } else {
                    st = con.prepareStatement(this.SQL_LISTAR);
                    st.setString(1, cia);
                    st.setString(2, agencia);
                    st.setString(3, banco);
                    st.setString(4, fecha);
                }
                
                
                //////System.out.println("....................... listar de posicion_bancaria : " + st.toString());
                
                rs = st.executeQuery();
                
                while (rs.next()){
                    this.posbanc = new PosBancaria();
                    this.posbanc.Load(rs);
                    
                    this.vector.add(posbanc);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR EN LISTAR DE LA POSICION BANCARIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Getter for property posbanc.
     * @return Value of property posbanc.
     */
    public com.tsp.operation.model.beans.PosBancaria getPosbanc() {
        return posbanc;
    }
    
    /**
     * Setter for property posbanc.
     * @param posbanc New value of property posbanc.
     */
    public void setPosbanc(com.tsp.operation.model.beans.PosBancaria posbanc) {
        this.posbanc = posbanc;
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return vector;
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        this.vector = vector;
    }    
    
    /**
     * Obtiene un arreglo de las ultimas corridas generadas para un banco y una sucursal
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cia C�digo del Distrito
     * @param banco Nombre del banco
     * @param sucursal Nombre de la sucursal del banco
     * @param fecha Fecha Actual
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void corridaAnterior(String cia, String banco, String sucursal, String fecha) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("oracle");
            
            if(con!=null){
                st = con.prepareStatement(this.SQL_CHEQUE_RUN_1);
                st.setString(1, cia);
                st.setString(2, banco);
                st.setString(3, sucursal);
                st.setString(4, fecha);
                
                rs = st.executeQuery();
                
                this.corridas = new Vector();
                while (rs.next()){
                    String tipo =  rs.getString(1);
                    double total = rs.getDouble(2);
                    Hashtable ht = new Hashtable();
                    ht.put("tipo", tipo);
                    ht.put("total", String.valueOf(total));
                    //////System.out.println("............. TOTAL: " + total);
                    this.corridas.add(ht);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR EN BUSQUEDA DE CORRIDAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("oracle", con);
            }
        }
    }
    
    /**
     * Obtiene un arreglo de las corridas generadas en un per�odo para un banco y una sucursal
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cia C�digo del Distrito
     * @param banco Nombre del banco
     * @param sucursal Nombre de la sucursal del banco
     * @param fechai Fecha inicial del periodo
     * @param fechaf Fecha final del periodo
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void corridaPeriodo(String cia, String banco, String sucursal, String fechai, String fechaf) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("oracle");
            
            if(con!=null){
                st = con.prepareStatement(this.SQL_CHEQUE_RUN_2);
                st.setString(1, cia);
                st.setString(2, banco);
                st.setString(3, sucursal);
                st.setString(4, fechai);
                st.setString(5, fechaf);
                
                rs = st.executeQuery();
                
                this.corridas = new Vector();
                while (rs.next()){
                    String tipo =  rs.getString(1);
                    double total = rs.getDouble(2);
                    Hashtable ht = new Hashtable();
                    ht.put("tipo", tipo);
                    ht.put("total", String.valueOf(total));
                    //////System.out.println("............. TOTAL: " + total);
                    this.corridas.add(ht);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR EN BUSQUEDA DE CORRIDAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("oracle", con);
            }
        }
    }
    
    /**
     * Getter for property corridas.
     * @return Value of property corridas.
     */
    public java.util.Vector getCorridas() {
        return corridas;
    }
    
    /**
     * Setter for property corridas.
     * @param corridas New value of property corridas.
     */
    public void setCorridas(java.util.Vector corridas) {
        this.corridas = corridas;
    }
    
    /**
     * Obtiene ultimo registro del archivo posicion_bancaria para un banco espec�fico.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cia C�digo del Distrito
     * @param agencia C�digo de la agencia
     * @param banco Nombre del banco
     * @param sucursal Nombre de la sucursal del banco
     * fecha Fecha
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void posicionAnterior(String cia, String agencia, String banco, String sucursal) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        this.posbanc = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(this.SQL_REGANTERIOR);
                st.setString(1, cia);
                st.setString(2, agencia);
                st.setString(3, banco);
                st.setString(4, sucursal);
                
                //////System.out.println("....................... posicion_bancaria anterior: " + st.toString());
                
                rs = st.executeQuery();
                
                if (rs.next()){
                    this.posbanc = new PosBancaria();
                    this.posbanc.Load(rs);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR EN OBTENCION DE LA POSICION BANCARIA ANTERIOR " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
}
