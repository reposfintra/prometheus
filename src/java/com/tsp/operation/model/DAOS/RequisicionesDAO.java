/*
    Document   : Modulo de Requisiciones Fintra
    Created on : 10/07/2013, 11:00:00 AM
    Author     : hcuello
*/

package com.tsp.operation.model.DAOS;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.BeansGenericoRta;
import com.tsp.operation.model.beans.CmbGeneralScBeans;

import com.tsp.operation.model.beans.CmbGenericoBeans;
import com.tsp.operation.model.beans.Politicas;
import com.tsp.operation.model.beans.RequisicionBeans;
import com.tsp.operation.model.beans.RequisicionesListadoBeans;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Statement;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.CallableStatement;
import javax.swing.JOptionPane;

public class RequisicionesDAO extends MainDAO {

    public RequisicionesDAO(String dataBaseName) {
       super("RequisicionesDAO.xml",dataBaseName);
    }
    
    //------------------------------------------------------------    
    
    public ArrayList<CmbGenericoBeans> GetComboGenerico(String Query, String IdCmb, String DescripcionCmb, String wuereCmb) throws Exception {
        
        ArrayList combo = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            con = conectarJNDI(Query);
            String sql = this.obtenerSQL(Query);
            
            ps = con.prepareStatement(sql);
            if ( wuereCmb != "") {
                ps.setString(1, wuereCmb);
            }
            
            rs = ps.executeQuery();
            combo = new ArrayList();
            while (rs.next()) {
                CmbGenericoBeans cmb = new CmbGenericoBeans();
                cmb.setIdCmb(rs.getInt(IdCmb));
                cmb.setDescripcionCmb(rs.getString(DescripcionCmb));
             
                combo.add(cmb);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return combo;
    }
    
    //------------------------------------------------------------

    public ArrayList<CmbGeneralScBeans> GetComboGenericoStr(String Query, String IdCmb, String DescripcionCmb, String wuereCmb) throws Exception {
        
        ArrayList combo = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            con = conectarJNDI(Query);
            String sql = this.obtenerSQL(Query);
            
            ps = con.prepareStatement(sql);
            if ( wuereCmb != "") {
                ps.setString(1, wuereCmb);
            }
            
            rs = ps.executeQuery();
            combo = new ArrayList();
            while (rs.next()) {
                CmbGeneralScBeans cmb = new CmbGeneralScBeans();
                cmb.setIdCmbStr(rs.getString(IdCmb)); //cmb.setIdCmb(rs.getInt(IdCmb)); getIdCmbStr-idCombostr
                cmb.setDescripcionCmb(rs.getString(DescripcionCmb));
                
                combo.add(cmb);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return combo;
    }    
    
    //------------------------------------------------------------
    
    public ArrayList<BeansGenericoRta> GetRespuestaUnica(String Query, String Var1, String Var2, String Var3, String Var4) throws Exception {
        
        ArrayList combo = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            con = conectarJNDI(Query);
            String sql = this.obtenerSQL(Query);
            
            ps = con.prepareStatement(sql);
            
            if ( Var1 != "") {
                ps.setString(1, Var1);
            }
            
            if ( Var2 != "") {
                ps.setString(2, Var2);
            }            

            if ( !Var1.equals("") ) {
                sql = sql.replaceAll("#VAR3#", Var3);
            }else{
                sql = sql.replaceAll("#VAR3#", "");
            }   

            if ( !Var1.equals("") ) {
                sql = sql.replaceAll("#VAR4#", Var4);
            }else{
                sql = sql.replaceAll("#VAR4#", "");
            }              
            
            
            rs = ps.executeQuery();
            combo = new ArrayList();
            while (rs.next()) {
                BeansGenericoRta cmb = new BeansGenericoRta();
                cmb.setId(rs.getInt("soy_del_proceso"));
             
                combo.add(cmb);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return combo;
    }
    
    //------------------------------------------------------------
    
    public ArrayList<RequisicionesListadoBeans> GetRequisicionesListado(String Query, String Estado, String Mes, String Ano, String Proceso, String Asignada, String TipoReq, String Prioridad, String LoginUser, String wHrItem, String wFiltro) throws Exception {
        
        ArrayList ListadoReqs = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            String sql = this.obtenerSQL(Query);
            
            if ( wFiltro.equals("FromFilter") ) {
            
                if ( !Mes.equals("") && !Ano.equals("") ) {
                    sql = sql.replaceAll("#CADWHERE#", " WHERE EXTRACT(YEAR FROM fch_radicacion) = '"+Ano+"' AND EXTRACT(MONTH FROM fch_radicacion) = '"+Mes+"' AND usuario_generador = '"+LoginUser+"'");
                }else{
                    sql = sql.replaceAll("#CADWHERE#", "");
                }    

                if ( !Estado.equals("") ) {
                    sql = sql.replaceAll("#ESTADO#", " AND id_estado_requisicion = " + Estado);
                }else{
                    sql = sql.replaceAll("#ESTADO#", "");
                }    

                if ( !Proceso.equals("") ) {
                    sql = sql.replaceAll("#PROCESO#", " AND id_proceso_interno = " + Proceso);
                }else{
                    sql = sql.replaceAll("#PROCESO#", "");
                }
                
                if ( Asignada.equals("") ) {
                    sql = sql.replaceAll("#ASIGNADA#", "");
                }else if ( Asignada.equals("1") ) {
                    sql = sql.replaceAll("#ASIGNADA#", " AND solucionador_responsable != ''");
                }else if ( Asignada.equals("0") ) {
                    sql = sql.replaceAll("#ASIGNADA#", " AND solucionador_responsable = ''");
                }
                
                sql = sql.replaceAll("#TIPORQU#", "");
                sql = sql.replaceAll("#PRIORIDAD#", "");

                if ( !wHrItem.equals("") ) {
                    sql = sql.replaceAll("#ITEM#", " AND id = " + wHrItem);
                }else{
                    sql = sql.replaceAll("#ITEM#", "");
                }
                
            }else if ( wFiltro.equals("FilterBandejaEntrada") ) {                
                
                if ( !Mes.equals("") && !Ano.equals("") ) {
                    //sql = sql.replaceAll("#CADWHERE#", " WHERE EXTRACT(YEAR FROM fch_radicacion) = '"+Ano+"' AND EXTRACT(MONTH FROM fch_radicacion) = '"+Mes+"' and id_proceso_interno = (SELECT id_proceso_interno FROM rel_proceso_interno_usuario where login = '"+LoginUser+"')");
                    sql = sql.replaceAll("#CADWHERE#", " WHERE EXTRACT(YEAR FROM fch_radicacion) = '"+Ano+"' AND EXTRACT(MONTH FROM fch_radicacion) = '"+Mes+"'");
                }else{
                    sql = sql.replaceAll("#CADWHERE#", "");
                }    

                if ( !Estado.equals("") ) {
                    sql = sql.replaceAll("#ESTADO#", " AND id_estado_requisicion = " + Estado);
                }else{
                    sql = sql.replaceAll("#ESTADO#", "");
                }    

                if ( !Proceso.equals("") ) {
                    sql = sql.replaceAll("#PROCESO#", " AND id_proceso_interno in (SELECT id_proceso_interno FROM rel_proceso_interno_usuario where login = '"+LoginUser+"') and usuario_generador in (SELECT login FROM rel_proceso_interno_usuario where id_proceso_interno = "+Proceso+")");
                }else{
                    sql = sql.replaceAll("#PROCESO#", " AND id_proceso_interno in (SELECT id_proceso_interno FROM rel_proceso_interno_usuario where login = '"+LoginUser+"')");
                }
                
                if ( Asignada.equals("") ) {
                    sql = sql.replaceAll("#ASIGNADA#", "");
                }else if ( Asignada.equals("1") ) {
                    sql = sql.replaceAll("#ASIGNADA#", " AND solucionador_responsable != ''");
                }else if ( Asignada.equals("0") ) {
                    sql = sql.replaceAll("#ASIGNADA#", " AND solucionador_responsable = ''");
                }
                
                sql = sql.replaceAll("#TIPORQU#", "");
                sql = sql.replaceAll("#PRIORIDAD#", "");

                if ( !wHrItem.equals("") ) {
                    sql = sql.replaceAll("#ITEM#", " AND id = " + wHrItem);
                }else{
                    sql = sql.replaceAll("#ITEM#", "");
                }
                
            }else if ( wFiltro.equals("FilterOwn1") ) {
                
                sql = sql.replaceAll("#CADWHERE#", " WHERE solucionador_responsable = '"+LoginUser+"'");

                if ( !Estado.equals("") ) {
                    sql = sql.replaceAll("#ESTADO#", " AND id_estado_requisicion = " + Estado);
                }else{
                    sql = sql.replaceAll("#ESTADO#", "");
                }                 
                
                sql = sql.replaceAll("#PROCESO#", "");
                sql = sql.replaceAll("#ASIGNADA#", "");
                
                if ( !TipoReq.equals("") ) {
                    sql = sql.replaceAll("#TIPORQU#", " AND id_tipo_requisicion = " + TipoReq);
                }else{
                    sql = sql.replaceAll("#TIPORQU#", "");
                }  
                
                if ( !Prioridad.equals("") ) {
                    sql = sql.replaceAll("#PRIORIDAD#", " AND id_prioridad = " + Prioridad);
                }else{
                    sql = sql.replaceAll("#PRIORIDAD#", "");
                }  
                
                sql = sql.replaceAll("#ITEM#", "");                
                
            }else if ( wFiltro.equals("Editar") ) {
                
                sql = sql.replaceAll("#CADWHERE#", " WHERE id = "+wHrItem);
                sql = sql.replaceAll("#ESTADO#", "");
                sql = sql.replaceAll("#PROCESO#", "");
                sql = sql.replaceAll("#ASIGNADA#", "");
                sql = sql.replaceAll("#TIPORQU#", "");
                sql = sql.replaceAll("#PRIORIDAD#", "");
                sql = sql.replaceAll("#ITEM#", "");
            
            }  
            
            con = conectarJNDI(Query);
            ps = con.prepareStatement(sql);
            
            System.out.println(ps);
                
            ListadoReqs = new ArrayList();
            
            rs = ps.executeQuery();
            while (rs.next()) {
                RequisicionesListadoBeans LstReq = new RequisicionesListadoBeans();
                
                LstReq.setId(rs.getInt("id"));
                LstReq.setIdEmpresa(rs.getString("id_empresa"));
                LstReq.setDscEmpresa(rs.getString("empresa"));
                LstReq.setIdTipoRequisicion(rs.getInt("id_tipo_requisicion"));
                LstReq.setDscTipoRequisicion(rs.getString("tipo_requisicion"));
                LstReq.setIdProcesoSgc(rs.getInt("id_proceso_interno"));
                LstReq.setDscProcesoSgc(rs.getString("proceso_interno"));
                LstReq.setRadicado(rs.getString("radicado"));
                LstReq.setFchRadicacion(rs.getString("fch_radicacion"));
                LstReq.setUsuario(rs.getString("usuario_generador"));
                LstReq.setAsunto(rs.getString("asunto"));
                LstReq.setDescripcion(rs.getString("descripcion"));
                LstReq.setIdEstado(rs.getInt("id_estado_requisicion"));
                LstReq.setEstado(rs.getString("estado"));
                LstReq.setCodTaskRq(rs.getInt("cod_tipo_tarea"));
                LstReq.setDscTipoTarea(rs.getString("tipo_tarea"));
                LstReq.setFchCierre(rs.getString("fch_cierre"));
                LstReq.setAtiende(rs.getString("solucionador_responsable"));
                LstReq.setIdPrioridad(rs.getInt("id_prioridad"));
                LstReq.setDscPrioridad(rs.getString("prioridad"));
                
                LstReq.setRqFchInicio(rs.getString("fecha_inicio_estimado"));
                LstReq.setRqFchFin(rs.getString("fecha_fin_estimado"));
                LstReq.setHorasTrabajo(rs.getFloat("horas_trabajo"));
                
                LstReq.setIdPriorizacion(rs.getInt("orden_priorizacion"));
                LstReq.setModeradoPor(rs.getString("moderado_por"));
                
                LstReq.setAutorizado(rs.getInt("autorizado"));
                LstReq.setAutoriza(rs.getString("autorizado_por"));
                
                ListadoReqs.add(LstReq);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        
        return ListadoReqs;
    }

    //------------------------------------------------------------

    public ArrayList<RequisicionesListadoBeans> GetRequisicionesTareas(String Query, String ReqId, String EstadoTarea, String LoginUser, String wHrItem) throws Exception {
        
        ArrayList ListadoReqs = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            String sql = this.obtenerSQL(Query);
            
            if ( !ReqId.equals("") ) {
                sql = sql.replaceAll("#CADWHERE#", " WHERE id_requisicion = "+ReqId);
            }else{
                sql = sql.replaceAll("#CADWHERE#", "");
            }    

            if ( !EstadoTarea.equals("") ) {
                sql = sql.replaceAll("#ESTADOTSK#", " AND id_estado_requisicion = " + EstadoTarea);
            }else{
                sql = sql.replaceAll("#ESTADOTSK#", "");
            }    

            if ( !wHrItem.equals("") ) {
                sql = sql.replaceAll("#ITEM#", " AND id = " + wHrItem);
            }else{
                sql = sql.replaceAll("#ITEM#", "");
            }
 
            
            con = conectarJNDI(Query);
            ps = con.prepareStatement(sql);
            
            System.out.println(ps);
                
            ListadoReqs = new ArrayList();
            
            rs = ps.executeQuery();
            while (rs.next()) {
                
                RequisicionesListadoBeans LstReq = new RequisicionesListadoBeans();
                
                LstReq.setIdTask(rs.getInt("id"));
                LstReq.setIdEmpresaTask(rs.getString("id_empresa"));
                LstReq.setIdUsuarioTask(rs.getInt("id_usuario_atiende"));
                LstReq.setUsuarioTask(rs.getString("usuario_responsable"));
                LstReq.setIdTipoTask(rs.getInt("id_tipo_tarea"));
                LstReq.setTipoTask(rs.getString("tipo_tarea"));
                LstReq.setDescripcionTask(rs.getString("descripcion"));
                LstReq.setFchInicioTask(rs.getString("fecha_inicio_estimada"));
                LstReq.setFchFinTask(rs.getString("fecha_fin_estimada"));
                LstReq.setHourTask(rs.getString("horas_estimadas"));
                LstReq.setFchTerminacion(rs.getString("fecha_culminacion"));
                LstReq.setHoraReprocesoTask(rs.getString("horas_reproceso"));
                LstReq.setIdEstadoTask(rs.getInt("id_estado_tarea"));
                LstReq.setDescEstadoTask(rs.getString("estado"));
                
                ListadoReqs.add(LstReq);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        
        return ListadoReqs;
    }
    

    public ArrayList<RequisicionesListadoBeans> GetRqIndicadores(String Query, String Ano, String Mes, String Proceso) throws Exception {
        
        ArrayList ListadoReqs = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            String sql = this.obtenerSQL(Query);
            
            con = conectarJNDI(Query);
            ps = con.prepareStatement(sql);
            
            ps.setString(1, Ano);
            ps.setString(2, Mes);
            ps.setString(3, Proceso);            
            
            System.out.println(ps);
                
            ListadoReqs = new ArrayList();
            
            rs = ps.executeQuery();
            while (rs.next()) {
                
                RequisicionesListadoBeans LstReq = new RequisicionesListadoBeans();
                
                LstReq.setIndiceiEstadistica(rs.getInt("secuencia_i"));
                LstReq.setDescYearEne(rs.getString("enero"));
                LstReq.setDescYearFeb(rs.getString("febrero"));
                LstReq.setDescYearMar(rs.getString("marzo"));
                LstReq.setDescYearAbr(rs.getString("abril"));
                LstReq.setDescYearMay(rs.getString("mayo"));
                LstReq.setDescYearJun(rs.getString("junio"));
                LstReq.setDescYearJul(rs.getString("julio"));
                LstReq.setDescYearAgo(rs.getString("agosto"));
                LstReq.setDescYearSep(rs.getString("septiembre"));
                LstReq.setDescYearOct(rs.getString("octubre"));
                LstReq.setDescYearNov(rs.getString("noviembre"));
                LstReq.setDescYearDic(rs.getString("diciembre"));
                LstReq.setDescAcumulado(rs.getString("acumulado"));
                
                ListadoReqs.add(LstReq);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        
        return ListadoReqs;
    }
    
    
    
//------------------------------------------------------------
    
  public String InsertRequisiciones(String Dstrct, int TipoRequisicion, int ProcesoSgc, String Iduser, String Asunto, String Descripcion, int Prioridad, String CmbPartners) throws SQLException{
  
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String resultado = "OK";
        try {
            
            con = conectarJNDI("SQL_INSERTAR_REQ");
            con.setAutoCommit(false);
           
            String sql = this.obtenerSQL("SQL_INSERTAR_REQ");
            ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            
            ps.setString(1, Dstrct);
            ps.setInt(2, TipoRequisicion);
            ps.setInt(3, ProcesoSgc);
            ps.setInt(4, TipoRequisicion);
            ps.setString(5, Iduser);
            ps.setString(6, Asunto);
            ps.setString(7, Descripcion);
            ps.setInt(8, Prioridad);
            ps.setString(9, CmbPartners);
            
            System.out.println(ps);
            
            int resp = ps.executeUpdate();
            
            if (resp > 0) {
                
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    resultado = rs.getString(1);                   
                }
                
            }
            
            con.commit();
            
        } catch (Exception e) {
            con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
            resultado = "ER";
        } finally {
            
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
        return resultado;
    }
    
  public String ActualizaRequisicion(String Dstrct, int TipoRequisicion, int ProcesoSgc, String Iduser, String Asunto, String Descripcion, int Prioridad, int ReqId) throws SQLException{
  
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String resultado = "OK";
        try {
            
            con = conectarJNDI("SQL_ACTUALIZAR_REQ");
            con.setAutoCommit(false);
           
            String sql = this.obtenerSQL("SQL_ACTUALIZAR_REQ");
            ps = con.prepareStatement(sql);
            
            ps.setInt(1, TipoRequisicion);
            ps.setInt(2, ProcesoSgc);
            ps.setString(3, Asunto);
            ps.setString(4, Descripcion);
            ps.setInt(5, Prioridad);
            ps.setInt(6, ReqId);
            
            System.out.println(ps);
            
            ps.execute();
            con.commit();
        } catch (Exception e) {
            con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
            resultado = "ER";
        } finally {
            
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
        return resultado;
    }
  

    public String ActualizaRqResponsable(String Responsable, int ReqId) throws SQLException{
  
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String resultado = "OK";
        try {
            
            con = conectarJNDI("SQL_ACTUALIZAR_REQ_RESPONSABLE");
            con.setAutoCommit(false);
           
            String sql = this.obtenerSQL("SQL_ACTUALIZAR_REQ_RESPONSABLE");
            ps = con.prepareStatement(sql);
            
            ps.setString(1, Responsable);
            ps.setInt(2, ReqId);
            
            System.out.println(ps);
            
            ps.execute();
            con.commit();
        } catch (Exception e) {
            con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
            resultado = "ER";
        } finally {
            
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
        return resultado;
    }

  
    public String listarTareas(String usuario) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            
            String query = "SQL_BUSCAR_TAREAS"; 
            String filtro = " AND solucionador_responsable ='"+usuario+"'";
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));  
            rs = ps.executeQuery();
            Gson gson = new Gson();
            JsonArray arr = new JsonArray();
            JsonObject fila;
              
            while (rs.next()) {
            
                fila = new JsonObject();
                fila.addProperty("id",rs.getString("id"));
                fila.addProperty("title",rs.getString("descripcion"));
                fila.addProperty("start",rs.getString("fecha_inicio_estimada"));
                fila.addProperty("end",rs.getString("fecha_fin_estimada"));
                fila.addProperty("color",rs.getString("color"));
                arr.add(fila);
                
            }
          
            return gson.toJson(arr);
       
        } catch (Exception ex) {  
              throw new SQLException("ERROR OBTENIENDO listarTareas: listarTareas(String usuario)  " + ex.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }          
        }

    }
    
    public String listarTareas(String idProceso,int anio, int mes) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            
            String query = "SQL_BUSCAR_TAREAS"; 
            String filtro = " AND req.id_proceso_interno ="+idProceso+ " AND EXTRACT(YEAR FROM fecha_inicio_estimada::timestamp)="+anio;
            filtro += (mes>0) ? " AND EXTRACT(MONTH FROM fecha_inicio_estimada::timestamp)= "+mes : " " ;
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro)); 
            rs = ps.executeQuery();
            Gson gson = new Gson();
            JsonArray arr = new JsonArray();
            JsonObject fila;
              
            while (rs.next()) {
            
                fila = new JsonObject();
                fila.addProperty("id",rs.getString("id"));
                fila.addProperty("title",rs.getString("solucionador_responsable")+"-"+rs.getString("descripcion"));
                fila.addProperty("start",rs.getString("fecha_inicio_estimada"));
                fila.addProperty("end",rs.getString("fecha_fin_estimada"));
                fila.addProperty("color",rs.getString("color"));
                arr.add(fila);
                
            }
          
            return gson.toJson(arr);
       
        } catch (Exception ex) {  
              throw new SQLException("ERROR OBTENIENDO listarTareas: listarTareas(String usuario)  " + ex.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }          
        }

    }

    public ArrayList<RequisicionBeans> listarPrioridadRequisicion() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_GRILLA_PRIORIDAD_REQUISICION";

        ArrayList<RequisicionBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();

            while (rs.next()) {
                RequisicionBeans beanReq = new RequisicionBeans();
                beanReq.setPrireid(rs.getString("id"));
                beanReq.setPriredecripcion(rs.getString("descripcion"));
                beanReq.setPrirecolor(rs.getString("color"));
                beanReq.setPrireestado(rs.getString("estado"));

                lista.add(beanReq);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    public String guardarPrioridadRequisicion(String descripcion, String color, String usuario) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String resultado = "OK";
        try {

            con = conectarJNDI("SQL_GUARDAR_PRIORIDAD_REQUISICION");

            String sql = this.obtenerSQL("SQL_GUARDAR_PRIORIDAD_REQUISICION");
            ps = con.prepareStatement(sql);

            ps.setString(1, "FINV");
            ps.setString(2, descripcion);
            ps.setString(3, color);
            ps.setString(4, usuario);
            System.out.println(ps);

            ps.execute();

        } catch (Exception e) {
            //con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
            resultado = "ER";
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
        return resultado;
    }

    public String editarPrioridadRequisicion(String descripcion, String color, String usuario, String id) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String resultado = "OK";
        try {
            con = conectarJNDI("SQL_EDITAR_PRIORIDAD_REQUISICION");
            String sql = this.obtenerSQL("SQL_EDITAR_PRIORIDAD_REQUISICION");
            ps = con.prepareStatement(sql);
            ps.setString(1, descripcion);
            ps.setString(2, color);
            ps.setString(3, usuario);
            ps.setString(4, id);
            System.out.println(ps);

            ps.execute();

        } catch (Exception e) {
            //con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
            resultado = "ER";
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
        return resultado;
    }

    public String cambiarestadoPriRequisicion(String usuario, String id) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String resultado = "OK";
        try {
            con = conectarJNDI("SQL_CAMBIAR_ESTADO_PRIREQ");
            String sql = this.obtenerSQL("SQL_CAMBIAR_ESTADO_PRIREQ");
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario);
            ps.setString(2, id);
            System.out.println(ps);

            ps.execute();

        } catch (Exception e) {
            //con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
            resultado = "ER";
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
        return resultado;
    }

    public ArrayList<RequisicionBeans> listarEstadoRequisicion() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_GRILLA_ESTADO_REQUISICION";

        ArrayList<RequisicionBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();

            while (rs.next()) {
                RequisicionBeans beanReq = new RequisicionBeans();
                beanReq.setEstreqid(rs.getString("id"));
                beanReq.setEstreqdescripcion(rs.getString("descripcion"));
                beanReq.setEstreqestado(rs.getString("estado"));

                lista.add(beanReq);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    public String guardarEstadoRequisicion(String descripcion, String usuario) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String resultado = "OK";
        try {

            con = conectarJNDI("SQL_GUARDAR_ESTADO_REQUISICION");

            String sql = this.obtenerSQL("SQL_GUARDAR_ESTADO_REQUISICION");
            ps = con.prepareStatement(sql);

            ps.setString(1, "FINV");
            ps.setString(2, descripcion);
            ps.setString(3, usuario);
            System.out.println(ps);

            ps.execute();

        } catch (Exception e) {
            //con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
            resultado = "ER";
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
        return resultado;
    }

    public String editarEstadoRequisicion(String descripcion, String usuario, String id) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String resultado = "OK";
        try {
            con = conectarJNDI("SQL_EDITAR_ESTADO_REQUISICION");
            String sql = this.obtenerSQL("SQL_EDITAR_ESTADO_REQUISICION");
            ps = con.prepareStatement(sql);
            ps.setString(1, descripcion);
            ps.setString(2, usuario);
            ps.setString(3, id);
            System.out.println(ps);

            ps.execute();

        } catch (Exception e) {
            //con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
            resultado = "ER";
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
        return resultado;
    }

    public String cambiarestadoRequisicion(String usuario, String id) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String resultado = "OK";
        try {
            con = conectarJNDI("SQL_CAMBIAR_ESTADO_REQ");
            String sql = this.obtenerSQL("SQL_CAMBIAR_ESTADO_REQ");
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario);
            ps.setString(2, id);
            System.out.println(ps);

            ps.execute();

        } catch (Exception e) {
            //con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
            resultado = "ER";
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
        return resultado;
    }

    public ArrayList<RequisicionBeans> listarTipoTarea() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_GRILLA_TIPO_TAREA";

        ArrayList<RequisicionBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();

            while (rs.next()) {
                RequisicionBeans beanReq = new RequisicionBeans();
                beanReq.setTiptareaid(rs.getString("id"));
                beanReq.setTiptareadesc(rs.getString("descripcion"));
                beanReq.setTiptareaestado(rs.getString("estado"));

                lista.add(beanReq);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    public String guardarTipoTarea(String descripcion, String usuario) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String resultado = "OK";
        try {
            con = conectarJNDI("SQL_GUARDAR_TIPO_TAREA");
            String sql = this.obtenerSQL("SQL_GUARDAR_TIPO_TAREA");
            ps = con.prepareStatement(sql);
            ps.setString(1, "FINV");
            ps.setString(2, descripcion);
            ps.setString(3, usuario);
            System.out.println(ps);

            ps.execute();

        } catch (Exception e) {
            //con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
            resultado = "ER";
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
        return resultado;
    }

    public String editarTipoTarea(String descripcion, String usuario, String id) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String resultado = "OK";
        try {
            con = conectarJNDI("SQL_EDITAR_TIPO_TAREA");
            String sql = this.obtenerSQL("SQL_EDITAR_TIPO_TAREA");
            ps = con.prepareStatement(sql);
            ps.setString(1, descripcion);
            ps.setString(2, usuario);
            ps.setString(3, id);
            System.out.println(ps);

            ps.execute();

        } catch (Exception e) {
            //con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
            resultado = "ER";
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
        return resultado;
    }

    public String cambiarestadoTipoTarea(String usuario, String id) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String resultado = "OK";
        try {
            con = conectarJNDI("SQL_CAMBIAR_ESTADO_TIPO_TAREA");
            String sql = this.obtenerSQL("SQL_CAMBIAR_ESTADO_TIPO_TAREA");
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario);
            ps.setString(2, id);
            System.out.println(ps);

            ps.execute();

        } catch (Exception e) {
            //con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
            resultado = "ER";
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
        return resultado;
    }

    public ArrayList<RequisicionBeans> listarEstadoTipoTarea() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_GRILLA_ESTADO_TIPO_TAREA";

        ArrayList<RequisicionBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();

            while (rs.next()) {
                RequisicionBeans beanReq = new RequisicionBeans();
                beanReq.setEsttiptareaid(rs.getString("id"));
                beanReq.setEsttiptareadesc(rs.getString("descripcion"));
                beanReq.setEsttiptareaestado(rs.getString("estado"));

                lista.add(beanReq);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    public String guardarEstadoTipoTarea(String descripcion, String usuario) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String resultado = "OK";
        try {
            con = conectarJNDI("SQL_GUARDAR_ESTADO_TIPO_TAREA");
            String sql = this.obtenerSQL("SQL_GUARDAR_ESTADO_TIPO_TAREA");
            ps = con.prepareStatement(sql);
            ps.setString(1, "FINV");
            ps.setString(2, descripcion);
            ps.setString(3, usuario);
            System.out.println(ps);

            ps.execute();

        } catch (Exception e) {
            //con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
            resultado = "ER";
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
        return resultado;
    }

    public String editarEstadoTipoTarea(String descripcion, String usuario, String id) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String resultado = "OK";
        try {
            con = conectarJNDI("SQL_EDITAR_ESTADO_TIPO_TAREA");
            String sql = this.obtenerSQL("SQL_EDITAR_ESTADO_TIPO_TAREA");
            ps = con.prepareStatement(sql);
            ps.setString(1, descripcion);
            ps.setString(2, usuario);
            ps.setString(3, id);
            System.out.println(ps);

            ps.execute();

        } catch (Exception e) {
            //con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
            resultado = "ER";
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
        return resultado;
    }

    public String cambiarestTipoTarea(String usuario, String id) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String resultado = "OK";
        try {
            con = conectarJNDI("SQL_CAMBIAR_EST_TAREA");
            String sql = this.obtenerSQL("SQL_CAMBIAR_EST_TAREA");
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario);
            ps.setString(2, id);
            System.out.println(ps);

            ps.execute();

        } catch (Exception e) {
            //con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
            resultado = "ER";
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
        return resultado;
    }

    public ArrayList<RequisicionBeans> listarTipoRequisicion() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_GRILLA_TIPO_REQUISISCION";

        ArrayList<RequisicionBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();

            while (rs.next()) {
                RequisicionBeans beanReq = new RequisicionBeans();
                beanReq.setTiporeqid(rs.getString("id"));
                beanReq.setTiporeqdescripcion(rs.getString("descripcion"));
                beanReq.setTiporeqeficacia(rs.getString("meta_eficacia"));
                beanReq.setTiporeqeficiencia(rs.getString("meta_eficiencia"));
                beanReq.setTiporeqestado(rs.getString("estado"));

                lista.add(beanReq);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    public String guardarTipoRequisicion(String descripcion, String usuario, String eficacia,String eficiencia) throws SQLException {
         Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String resultado = "OK";
        try {
            con = conectarJNDI("SQL_GUARDAR_TIPO_REQUISiCION");
            String sql = this.obtenerSQL("SQL_GUARDAR_TIPO_REQUISiCION");
            ps = con.prepareStatement(sql);
            ps.setString(1, "FINV");
            ps.setString(2, descripcion);
            ps.setString(3, usuario);
            ps.setInt(4, Integer.parseInt(eficacia));
            ps.setInt(5,Integer.parseInt(eficiencia));
            
            System.out.println(ps);

            ps.execute();

        } catch (Exception e) {
            //con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
            resultado = "ER";
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
        return resultado;
    }

     public String editarTipoRequisicion(String descripcion, String usuario, String id, String eficacia, String eficiencia) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String resultado = "OK";
        try {
            con = conectarJNDI("SQL_EDITAR_TIPO_REQUISICION");
            String sql = this.obtenerSQL("SQL_EDITAR_TIPO_REQUISICION");
            ps = con.prepareStatement(sql);
            ps.setString(1, descripcion);
            ps.setString(2, usuario);
            ps.setInt(3, Integer.parseInt(eficacia));
            ps.setInt(4,Integer.parseInt(eficiencia));
            ps.setString(5, id);
            System.out.println(ps);

            ps.execute();

        } catch (Exception e) {
            //con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
            resultado = "ER";
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
        return resultado;
    }

    public String cambiarEstadoTipoRequisicion(String usuario, String id) throws SQLException {
       Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String resultado = "OK";
        try {
            con = conectarJNDI("SQL_CAMBIAR_ESTADO_TIPO_REQUISICION");
            String sql = this.obtenerSQL("SQL_CAMBIAR_ESTADO_TIPO_REQUISICION");
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario);
            ps.setString(2, id);
            System.out.println(ps);

            ps.execute();

        } catch (Exception e) {
            //con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
            resultado = "ER";
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
        return resultado;
    }
    
    //public ArrayList<String> GuardarTareas(ArrayList<RequisicionesListadoBeans> requisicionesListadoBeanses, Usuario usuario, int TipoTarea, int IdReqTask) throws Exception {
    public void GuardarTareasANTERIOR(ArrayList<RequisicionesListadoBeans> requisicionesListadoBeanses, Usuario usuario, int TipoTarea, int IdReqTask) throws Exception {

        String cadena = "";
        String query = "SQL_INSERTAR_TAREAS";
        String sql = "";
        StringStatement st = null;
        ArrayList<String> listQuerys = new ArrayList<>();
        
        //int HoraReprocesoTask = 0;
        float HoraReprocesoTask = 0;
        String FchTerminacion = null;
        TransaccionService tService = null;
        
        try {
            
            tService = new TransaccionService();
            tService.crearStatement();

            for (RequisicionesListadoBeans listadoBeans : requisicionesListadoBeanses) {

                System.out.println(listadoBeans.toString()); 

                sql = this.obtenerSQL(query);
                st = new StringStatement(sql, true);

                HoraReprocesoTask = Float.parseFloat(!listadoBeans.getHoraReprocesoTask().equals("") ? listadoBeans.getHoraReprocesoTask() : "0"); //Integer.parseInt(!listadoBeans.getHoraReprocesoTask().equals("") ? listadoBeans.getHoraReprocesoTask() : "0");
                FchTerminacion = !listadoBeans.getFchTerminacion().equals("") ? listadoBeans.getFchTerminacion() : "0099-01-01";

                st.setInt(1, IdReqTask);
                st.setString(2, usuario.getLogin());
                st.setInt(3, TipoTarea);
                st.setInt(4, listadoBeans.getIdTask());
                st.setString(5, listadoBeans.getDescripcionTask());
                st.setString(6, listadoBeans.getFchInicioTask());
                st.setString(7, listadoBeans.getFchFinTask());
                st.setString(8, listadoBeans.getHourTask());
                st.setString(9, FchTerminacion);
                //st.setFloat  (10, HoraReprocesoTask); //setInt(10, HoraReprocesoTask);
                st.setString(11, usuario.getLogin());

                cadena = st.getSql();
                System.out.println(cadena);

                tService.getSt().addBatch(cadena);

                st = null;
                listQuerys.add(cadena);
            }
            
            tService.execute();
            
        }catch (Exception ex) {
            
            ex.printStackTrace();
            System.out.println(ex.toString());
            
        } finally {
            try {
                if (tService != null) { tService.closeAll(); }
            } catch (Exception e) { }            
        }        
        
        //return listQuerys;
        
    }

    public void GuardarTareas(ArrayList<RequisicionesListadoBeans> requisicionesListadoBeanses, Usuario usuario, int TipoTarea, int IdReqTask) throws Exception {

        String cadena = "";
        String query = "SQL_INSERTAR_TAREAS";
        String sql = "";
        StringStatement st = null;
        ArrayList<String> listQuerys = new ArrayList<>();
        
        //int HoraReprocesoTask = 0;
        //float HoraReprocesoTask = 0;
        String HoraReprocesoTask = "0.00";
        String FchTerminacion = null;
        TransaccionService tService = null;

        try {
            
            tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            Connection con = conectarJNDI(query);

            CallableStatement callableStatement = con.prepareCall("{" + this.obtenerSQL(query) + "}");

            for (RequisicionesListadoBeans listadoBeans : requisicionesListadoBeanses) {
                
                //HoraReprocesoTask = Integer.parseInt(!listadoBeans.getHoraReprocesoTask().equals("") ? listadoBeans.getHoraReprocesoTask() : "0");
                /*
                if ( listadoBeans.getHoraReprocesoTask().equals("") || listadoBeans.getHoraReprocesoTask().equals("0.00") ) {
                    HoraReprocesoTask = 0;
                }else{
                    HoraReprocesoTask = Float.parseFloat(listadoBeans.getHoraReprocesoTask()); //Integer.parseInt(listadoBeans.getHoraReprocesoTask());
                }*/
                
                HoraReprocesoTask = !listadoBeans.getHoraReprocesoTask().equals("") ? listadoBeans.getHoraReprocesoTask() : "0.00";
                    
                FchTerminacion = !listadoBeans.getFchTerminacion().equals("") ? listadoBeans.getFchTerminacion() : "0099-01-01";
                
                callableStatement.setInt(1, IdReqTask);
                callableStatement.setString(2, usuario.getLogin());
                callableStatement.setInt(3, TipoTarea);
                callableStatement.setInt(4, listadoBeans.getIdTask());
                callableStatement.setString(5, listadoBeans.getDescripcionTask());
                callableStatement.setString(6, listadoBeans.getFchInicioTask());
                callableStatement.setString(7, listadoBeans.getFchFinTask());
                callableStatement.setString(8, listadoBeans.getHourTask());
                callableStatement.setString(9, FchTerminacion);
                callableStatement.setString(10, HoraReprocesoTask);  // setInt(10, HoraReprocesoTask);
                callableStatement.setString(11, usuario.getLogin());
                callableStatement.addBatch();
                
            }
            /*
            int[] updateCounts = callableStatement.executeBatch();
            for (int i = 0; i < updateCounts.length; i++) {
                int updateCount = updateCounts[i];
                //JOptionPane.showMessageDialog(null,"indice :"+ i);                
            }*/
            
            int[] updateCounts = callableStatement.executeBatch();
            
            
        }catch (Exception ex) {
            
            ex.printStackTrace();
            System.out.println(ex.toString());
              System.out.println(ex.getMessage());
            
        } finally {
            try {
                if (tService != null) { tService.closeAll(); }
            } catch (Exception e) { }            
        }        
        
    }    
    
    public void GuardarAccionRq(Usuario usuario, int IdReqTask, int IdActionRq, String DscAction, int IdEstadoRq, int TipoTarea) throws Exception {

        String cadena = "";
        String query = "SQL_INSERTAR_ACCIONRQ";
        String sql = "";
        StringStatement st = null;
        ArrayList<String> listQuerys = new ArrayList<>();
        
        //int HoraReprocesoTask = 0;
        String FchTerminacion = null;
        TransaccionService tService = null;

        try {
            
            tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            Connection con = conectarJNDI(query);

            CallableStatement callableStatement = con.prepareCall("{" + this.obtenerSQL(query) + "}");

            callableStatement.setInt(1, IdReqTask);
            callableStatement.setInt(2, IdActionRq);
            callableStatement.setString(3, DscAction);
            callableStatement.setString(4, usuario.getLogin());
            callableStatement.setInt(5, TipoTarea);
            callableStatement.setInt(6, IdEstadoRq);
            
            //callableStatement.addBatch();
            callableStatement.executeQuery(); //execute();
            
            /*    
            int[] updateCounts = callableStatement.executeBatch();
            for (int i = 0; i < updateCounts.length; i++) {
                int updateCount = updateCounts[i];
                //JOptionPane.showMessageDialog(null,"indice :"+ i);                
            }*/
            
        }catch (Exception ex) {
            
            ex.printStackTrace();
            System.out.println(ex.toString());
            
        } finally {
            try {
                if (tService != null) { tService.closeAll(); }
            } catch (Exception e) { }            
        }        
        
    }    
    
    public ArrayList searchNombresArchivos(String directorioArchivos, String numreq) throws Exception{
        
        ArrayList nombresArchivos=new ArrayList();
        File dir = new File(directorioArchivos + numreq);
        try{
            if (dir.exists()){
                String[] ficheros = dir.list();
                for (int x=0;x<ficheros.length;x++) {
                      nombresArchivos.add(ficheros[x]);
                }
            }
      
           
        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.getEstadosApplus.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
     
        return nombresArchivos;
    }
    
     public boolean copiarArchivo(
                                 String negocio
                                ,String rutaOrigen, String rutaDestino
                                ,String filename
                               ) throws SQLException {

        boolean swFileCopied = false;
        try{            
          
            File carpetaDestino =new File(rutaDestino);
          
            deleteDirectory(carpetaDestino);
            this.createDir(rutaDestino);
                          
            try{
                    //Copiamos el archivo de la carpeta origen a la carpeta destino
                    InputStream in = new FileInputStream(rutaOrigen + negocio + "/" +filename);
                    OutputStream out = new FileOutputStream(rutaDestino + filename);

                    // Copy the bits from instream to outstream
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                    in.close();
                    out.close();
                    swFileCopied = true;
               
                   }catch(Exception k){ throw new Exception(" FILE: " + k.getMessage());}

            }catch(Exception e){
                System.out.println("errorrr:"+e.toString()+"__"+e.getMessage());
                throw new SQLException("Error searchImagenes() [DAO] : ->"+ e.getMessage());
            }
       
        return swFileCopied;
    }
     
    public boolean eliminarArchivo(String directorioArchivos, String numreq, String nomarchivo) throws Exception{
        
        File dir = new File(directorioArchivos + numreq);
        File archivo = new File(directorioArchivos + numreq + '/' + nomarchivo);
        try{
            if (dir.exists()){               
                if (archivo.exists()) archivo.delete();
                if (dir.list().length == 0){
                    deleteDirectory(dir);
                }               
            }
        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.getEstadosApplus.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
     
        return  archivo.delete();
    }
    
    public void createDir(String dir) throws Exception{
        try{
        File f = new File(dir);
        if(! f.exists() ) f.mkdir();    
        }catch(Exception e){ throw new Exception ( e.getMessage());}
   }

    static public boolean deleteDirectory(File path) {
        if( path.exists() ) {
          File[] files = path.listFiles();
          for(int i=0; i<files.length; i++) {
             if(files[i].isDirectory()) {
               deleteDirectory(files[i]);
             }
             else {
               files[i].delete();
             }
          }
        }
        return( path.delete() );
  }
    
    public String listarActividadesUsuario(String idProceso,int idUsuario,int anio, int mes) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            
            String query = "SQL_BUSCAR_TAREAS_POR_USUARIO"; 
            String filtro = " AND req.id_proceso_interno ="+idProceso+ " AND EXTRACT(YEAR FROM fecha_inicio_estimada::timestamp)="+anio;
            filtro += (mes>0) ? " AND EXTRACT(MONTH FROM fecha_inicio_estimada::timestamp)= "+mes : " " ;
            filtro += " AND treq.id_usuario_atiende = "+idUsuario ;
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro)); 
            rs = ps.executeQuery();
            Gson gson = new Gson();
            JsonArray arr = new JsonArray();
            JsonObject fila;
              
            while (rs.next()) {
            
                fila = new JsonObject();
                fila.addProperty("id",rs.getString("id"));
                fila.addProperty("title",rs.getString("solucionador_responsable")+"-"+rs.getString("descripcion"));
                fila.addProperty("start",rs.getString("fecha_inicio_estimada"));
                fila.addProperty("end",rs.getString("fecha_fin_estimada"));
                fila.addProperty("color",rs.getString("color"));
                arr.add(fila);
                
            }
          
            return gson.toJson(arr);
       
        } catch (Exception ex) {  
              throw new SQLException("ERROR OBTENIENDO listarTareas: listarTareas(String usuario)  " + ex.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }          
        }

    }


   
}

