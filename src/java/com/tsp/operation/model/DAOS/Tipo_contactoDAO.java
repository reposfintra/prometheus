/*
 * Nombre        Tipo_contactoDAO.java
 * Autor         Ing. Rodrigo Salazar
 * Modificado    Ing. Sandra Escalante
 * Fecha         17 Junio 2005
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;


public class Tipo_contactoDAO extends MainDAO{
    private TipoContacto tipo_contacto;
    private Vector tipo_contactos;
        
    private static String SQL_VERTODOS = "  SELECT  table_code as cod_tcontacto," +
    "                                               descripcion " +
    "                                       FROM    tablagen" +
    "                                       WHERE   table_type = 'TCONTACTO'" +
    "                                               AND reg_status != 'A' " +
    "                                       ORDER BY descripcion";
    

    /** Creates a new instance of Tipo_contactoDAO */
    public Tipo_contactoDAO() {
        super("Tipo_contactoDAO.xml");
    }
    
    /**
     * Metodo <tt>getTipo_contactos</tt>, obtiene el vector tipo_contactos
     * @autor : Ing. Rodrigo Salazar  
     * @return Vector
     * @version : 1.0
     */
    public Vector getTipo_contactos() {
        return tipo_contactos;
    }
    
    /**
     * Metodo <tt>setTipo_contactos</tt>, instancia el vector tipo_contactos
     * @autor : Ing. Rodrigo Salazar  
     * @param Vetor
     * @version : 1.0
     */
    public void setTipo_contactos(Vector tipo_contactoes) {
        this.tipo_contactos = tipo_contactos;
    }
    
    /**
     * Metodo <tt>listarTcontactos</tt>, obtiene los tipos de contactos del sistema
     * @autor : Ing. Rodrigo Salazar  
     * @return Vector
     * @version : 1.0
     */
    public Vector listarTContactos ()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        tipo_contactos = null;
        String query = "SQL_VERTODOS";
        
        try {            
            con = this.conectarJNDI(query);
            if (con != null) {
               st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                                
                rs = st.executeQuery();
                tipo_contactos = new Vector();
                
                while(rs.next()){
                    tipo_contacto = new TipoContacto();
                    tipo_contacto.setCodigo(rs.getString("cod_tcontacto"));
                    tipo_contacto.setDescripcion(rs.getString("descripcion"));
                    tipo_contactos.add(tipo_contacto);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS TIPOS DE CONTACTOS " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return tipo_contactos;
    }
}
