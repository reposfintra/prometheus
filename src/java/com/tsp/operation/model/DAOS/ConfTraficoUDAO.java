/********************************************************************
 *      Nombre Clase.................   ConfTraficoUDAO.java
 *      Descripci�n..................   lase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  dlamadrid
 */
public class ConfTraficoUDAO {
    
    private Vector vConf;
    private TreeMap tConf;
    /** Creates a new instance of ConfTraficoUDAO */
    public ConfTraficoUDAO () {
    }
    
    private static final String INSERTAR = "insert into conf_trafico_usuario(configuracion,linea,clas,filtro,var1,var2,var3,last_update,user_update,creation_date,creation_user,base, dstrct) values(?,?,?,?,?,?,?,'0099-01-01 00:00:00','',now(),?,'',?)";
    private static final String CONFPORID="select configuracion,linea,clas,filtro,var1,var2,var3 from conf_trafico_usuario where creation_user=?";
    private static final String EXISTE="select id from conf_trafico_usuario where creation_user=? ";
    private static final String ACTUALIZAR = "update conf_trafico_usuario set configuracion=?,linea=?,clas=?,filtro=?,var1=?,var2=?,var3=?,last_update=now(),user_update=? where creation_user=? ";
    private final static String DELETE_CONFIG = " DELETE FROM conf_trafico_usuario WHERE creation_user = ?";
    
    /**
     * Getter for property vConf.
     * @return Value of property vConf.
     */
    public java.util.Vector getVConf () {
        return vConf;
    }
    
    /**
     * Setter for property vConf.
     * @param vConf New value of property vConf.
     */
    public void setVConf (java.util.Vector vConf) {
        this.vConf = vConf;
    }
    
    /**
     * Getter for property tConf.
     * @return Value of property tConf.
     */
    public java.util.TreeMap getTConf () {
        return tConf;
    }
    
    /**
     * Setter for property tConf.
     * @param tConf New value of property tConf.
     */
    public void setTConf (java.util.TreeMap tConf) {
        this.tConf = tConf;
    }
    
   
    
    
     /**
     * Este m�todo se encarga de registrar un regitro en la tabla conf_trafico_u
      *cargando un objeto tipo ConfTraficoU
     * @author  dlamadrid
     * @version 1.0
     * @param ConfTraficoU c
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void insertarConfiguracion (ConfTraficoU c) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (INSERTAR);
                st.setString (1, c.getConfiguracion ());
                st.setString (2, c.getLinea ());
                st.setString (3, c.getClas ());
                st.setString (4, c.getFiltro ());
                st.setString (5, c.getVar1 ());
                st.setString (6, c.getVar2 ());
                st.setString (7, c.getVar3 ());
                st.setString (8, c.getCreation_user ());
                st.setString (9, c.getDstrct ());
                st.executeUpdate ();
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL LISTAR IDENTIDAD" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
    
    /**
     * Este m�todo se encarga de retornar tru si existe un registro en la tabla conf_trafico_u dado el usuario y false si no existe
     * @author  dlamadrid
     * @version 1.0
     * @param String usuario
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public boolean existeConfiguracion (String usuario) throws SQLException {
        ////System.out.println ("entro a generar filtros Usuarios");
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        this.vConf = new Vector ();
        boolean sw=false;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (this.EXISTE);
                st.setString (1, usuario);
                rs = st.executeQuery ();
                
                while (rs.next ()){
                    sw=true;
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL LISTAR IDENTIDAD" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
        return sw;
    }
    
    /**
     * Este m�todo se encarga de actualizar un registro en la tabla conf_trafico_u seteado un objeto ConfTraficoU
     * @author  dlamadrid
     * @version 1.0
     * @param ConfTraficoU c
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void actualizarConfiguracion (ConfTraficoU c) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (ACTUALIZAR);
                st.setString (1, c.getConfiguracion ());
                st.setString (2, c.getLinea ());
                st.setString (3, c.getClas ());
                st.setString (4, c.getFiltro ());
                st.setString (5, c.getVar1 ());
                st.setString (6, c.getVar2 ());
                st.setString (7, c.getVar3 ());
                st.setString (8, c.getCreation_user ());
                st.setString (9, c.getCreation_user ());
                st.executeUpdate ();
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL LISTAR IDENTIDAD" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
    
    
     /**
     * Este m�todo se encarga de listar los  registros en la tabla conf_trafico_u dado un usuario
     * @author  dlamadrid
     * @version 1.0
     * @param String usuario
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public ConfTraficoU listarConfiguracionPorUsuario (String usuario) throws SQLException {
        ////System.out.println ("entro a generar filtros Usuarios");
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        ConfTraficoU conf= new ConfTraficoU ();
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (this.CONFPORID);
                st.setString  (1,usuario);
                rs = st.executeQuery ();
                while (rs.next ()){
                    conf= conf.load (rs);
                }
                //////System.out.println ("CONFIGURACION  "+conf.getConfiguracion () );
                //////System.out.println ("LINEA " +conf.getLinea ());
                //////System.out.println ("VAR1 "+conf.getVar1 ());
                //////System.out.println ("VAR2"+conf.getVar2 ());
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL LISTAR IDENTIDAD" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
        return conf;
    }
    
    /**
     * Elimina todas las configuraciones de un determinado usuario
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param login Login del usuario
     * @throws SQLException
     * @version 1.0
     */
    public void eliminarConfiguraciones(String login) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.DELETE_CONFIG);
                st.setString(1, login);
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL ELIMINAR CONFIGURACIONES DEL USUARIO" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
}
