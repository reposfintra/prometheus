/***********************************************************************************
 * Nombre clase :  ClienteActividadDAO.java
 * Descripcion :                   Clase que maneja los DAO ( Data Access Object )
 *                                los cuales contienen los metodos que interactuan
 *                                con la BD.
 * Autor :                         Ing. Diogenes Antonio Bastidas Morales
 * Fecha :                          29 de agosto de 2005, 09:40 AM
 * Version :  1.0
 * Copyright : Fintravalores S.A.
 ***********************************************************************************/


package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;


public class ClienteActividadDAO extends MainDAO {
    
    /** Creates a new instance of ClienteActividadDAO */
    public ClienteActividadDAO() {
        super("ClienteActividadDAO.xml");
    }
    private ClienteActividad clientact;
    private Vector VecClientact;
    
    private static String SQL_CONSULTAR_ACTIVIDADES =
    "SELECT " +
    "rem.numrem, " +
    "cli.nomcli, " +
    "cli.codcli, " +
    "rem.tipoviaje, " +
    "c_a.cod_actividad, " +
    "act.deslarga, " +
    "p_r.numpla " +
    "FROM " +
    "remesa rem " +
    "LEFT JOIN clienteactividad c_a ON ( c_a.dstrct = 'FINV' AND c_a.codcli = rem.cliente AND c_a.tipoviaje = rem.tipoviaje ) " +
    "LEFT JOIN actividad act ON ( act.dstrct = 'FINV' AND act.cod_actividad = c_a.cod_actividad ) " +
    "LEFT JOIN cliente cli ON ( cli.codcli = rem.cliente ), " +
    "plarem p_r " +
    "WHERE " +
    "rem.numrem = ? " +
    "AND rem.numrem = p_r.numrem " +
    "AND rem.aduana = 'S' " +
    "AND rem.reg_status != 'A' ";
    
    private static final String insertar = "insert into clienteactividad (dstrct, codcli, cod_actividad, tipoviaje, secuencia,  base, creation_user, creation_date, user_update, last_update)" +
    " values (?,?,?,?,?,?,?,?,?,?)";
    
    private static final String modificar = "update clienteactividad "+
    "  set secuencia = ?," +
    "    estado = ?,   "+
    "    base = ?,     "+
    "    user_update = ?, "+
    "    last_update = ?  "+
    "   where             "+
    "        dstrct = ?      "+
    "    and codcli = ?   "+
    "    and cod_actividad = ? "+
    "    and tipoviaje = ? ";
    
    private static String listar = "select * from clienteactividad  where estado = '' ";
    
    private static String buscar = " select ca.codcli,  " +
    "  ca.tipoviaje," +
    "  ca.cod_Actividad," +
    "  ca.secuencia," +
    "  ca.dstrct,  " +
    "  a.descorta, " +
    "  a.deslarga, " +
    "  c.nomcli," +
    "  t.descripcion" +
    " from actividad a,  " +
    "   cliente c,   " +
    "   clienteactividad ca  " +
    "  left join tablagen t on (t.table_type = 'TBLFRON' and t.table_code = ca.tipoviaje )" +
    "  where    " +
    "     c.estado = 'A'  " +
    "     and a.estado = ''            " +
    "     and ca.cod_actividad = a.cod_actividad " +
    "     and ca.codcli = c.codcli      " +
    "     and ca.estado = ''             " +
    "     and ca.dstrct = ?" +
    "     and ca.cod_actividad = ?" +
    "     and ca.codcli = ?" +
    "     and ca.tipoviaje = ?" +
    "    order by c.nomcli, a.descorta  ";
    
    private static String busqueda = "select  ca.codcli,  " +
    "         ca.tipoviaje," +
    "         ca.cod_Actividad," +
    "         ca.secuencia," +
    "         ca.dstrct,  " +
    "         a.descorta, " +
    "         a.deslarga, " +
    "         c.nomcli," +
    "         t.descripcion  "+
    "   from actividad a,               "+
    "        cliente c,                 "+
    "        clienteactividad ca " +
    "        left join tablagen t on (t.table_type = 'TBLFRON' and t.table_code = ca.tipoviaje )       "+
    "   where                           "+
    "            c.estado = 'A'          "+
    "        and a.estado = ''          "+
    "        and ca.cod_actividad = a.cod_actividad " +
    "        and ca.estado = ''          "+
    "        and ca.codcli = c.codcli    " +
    "        and ca.dstrct = ?              "+
    "        and a.descorta like ?       "+
    "        and c.nomcli like ?         "+
    "        and ca.tipoviaje like ?    "+
    "        order by ca.codcli, ca.tipoviaje, ca.secuencia  ";
    
    private static String anular = "update clienteactividad " +
    "    set  estado = 'A',  "+
    "    user_update = ?, "+
    "    last_update = ?  "+
    "   where             "+
    "        dstrct = ?      "+
    "    and codcli = ?   "+
    "    and cod_actividad = ? "+
    "    and tipoviaje = ? ";
    
    private static String existeanu = " select *                  " +
    "    from clienteactividad  " +
    " where                     " +
    "         estado = 'A'      " +
    "    and dstrct = ?           "+
    "    and codcli = ?        "+
    "    and cod_actividad = ? "+
    "    and tipoviaje = ? ";
    
    private static String listarxClient = "select  ca.codcli," +
    "	ca.cod_actividad,  " +
    "   ca.tipoviaje,     " +
    "   ca.secuencia,     " +
    "   a.descorta,       " +
    "   a.deslarga,       " +
    "   get_nombrecliente(ca.codcli) as nomcli,  " +
    "   get_destablagen(tipoviaje, 'TBLFRON') as destipoviaje  " +
    " FROM clienteactividad ca," +
    "      actividad a    " +
    " WHERE               " +
    "     ca.estado = ''   " +
    " and ca.dstrct = ?             " +
    " and ca.codcli = ?             " +
    " and ca.cod_actividad = a.cod_actividad " +
    " and a.dstrct = ? " +
    " and a.estado =''               " +
    " order by ca.tipoviaje, ca.secuencia ";
    
    private static String ActClientTipo = "select ca.*,a.descorta, a.deslarga "+
    "  from clienteactividad ca,  "+
    "	 actividad a            "+
    "  where                      "+
    "         ca.estado = ''      "+
    "     and ca.dstrct = ?          "+
    "     and ca.codcli = ?       "+
    "     and ca.tipoviaje = ?    "+
    "     and ca.cod_actividad = a.cod_actividad "+
    "     and a.estado = ''       "+
    "    order by ca.secuencia";
    
    private static String SQL_EXISTE_SECUENCIA = "SELECT secuencia " +
    "FROM clienteactividad " +
    "WHERE dstrct = ? " +
    "     and codcli = ? " +
    "     and secuencia = ?" +
    "     and tipoviaje = ? " +
    "     and estado = ''   ";
    
    private static String SQL_BUSCAR_ACTCLIENTE = "select ca.secuencia, " +
    "						ca.codcli, " +
    "                                           ca.tipoviaje," +
    "                                           ca.secuencia, " +
    "                                           a.cod_actividad," +
    "                                           a.dstrct," +
    "                                           a.descorta," +
    "                                           a.deslarga" +
    "                                      from clienteactividad ca,  " +
    "					  	 actividad a" +
    "                                      where  " +
    "                                            ca.dstrct = ? " +
    "                                        and ca.codcli = ? " +
    "                                        and ca.tipoviaje = ?" +
    "                                        and ca.estado = '' " +
    "                                        and a.dstrct = ? " +
    "                                        and a.cod_actividad = ca.cod_actividad " +
    "                                        and a.estado = ''" +
    "                                       order by ca.secuencia";
    
    private static String SQL_ACTIVIDADSINFECCIERRE = "SELECT c.secuencia," +
    "       ia.feccierre, " +
    "       ia.cod_actividad " +
    "   FROM  clienteactividad c," +
    "       infoactividad ia " +
    "   WHERE c.dstrct = ? " +
    "         and c.codcli = ? " +
    "         and c.tipoviaje = ? " +
    "         and ia.dstrct = ? " +
    "         and ia.codcli = ?" +
    "         and ia.cod_actividad = c.cod_actividad " +
    "         and ia.numrem = ?" +
    "         and ia.feccierre <> '0099-01-01 00:00:00'" +
    "         and ia.estado = ''" +
    "   order by ia.feccierre DESC  limit 1";
    
    /**
     * Metodo  consultarActividades, busca las actividades x remesa
     * @param: el numero de la remesa
     * @autor : LREALES
     * @version : 1.0
     */
    public void consultarActividades( String remesa ) throws SQLException {
        
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if ( con != null ){
                
                st = con.prepareStatement( this.SQL_CONSULTAR_ACTIVIDADES );
                
                st.setString( 1, remesa );
                
                rs = st.executeQuery();
                
                this.VecClientact = new Vector();
                
                while ( rs.next() ){
                    
                    ClienteActividad ca = new ClienteActividad();
                    
                    ca.setRemesa( rs.getString("numrem") );
                    ca.setPlanilla( rs.getString("numpla") );
                    ca.setCodCliente( rs.getString("codcli") );
                    ca.setNomCliente( rs.getString("nomcli") );
                    ca.setTipoViaje( rs.getString("tipoviaje") );
                    ca.setCodActividad( rs.getString("cod_actividad") );
                    ca.setLargaAct( rs.getString("deslarga") );
                    
                    VecClientact.add( ca );
                }
                
            }
            
        } catch( SQLException e ){
            
            throw new SQLException( "ERROR EN [consultarActividades] " + e.getMessage() + " - " + e.getErrorCode() );
            
        } finally{
            
            if( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO " + e.getMessage() );
                }
            }
            
            if ( con != null ){
                
                poolManager.freeConnection( "fintra", con );
                
            }
            
        }
        
    }
    /**
     * Setter for property  setClienteActividad.
     * @param destipo_viaje New value of property  setClienteActividad.
     */
    public void  setClienteActividad(ClienteActividad ca){
        
        this.clientact=ca;
        
    }
    public ClienteActividad obtClienteActividad(){
        return clientact;
    }
    public Vector obtVecClientAct(){
        return VecClientact;
    }
    /**
     * Metodo insertarClientAct, ingresa un registro en la tabla clienteactividad
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void insertarClientAct() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(insertar);
                st.setString(1, clientact.getDstrct());
                st.setString(2, clientact.getCodCliente());
                st.setString(3, clientact.getCodActividad());
                st.setString(4, clientact.getTipoViaje());
                st.setInt(5, clientact.getSecuencia());
                st.setString(6, clientact.getBase());
                st.setString(7, clientact.getCreation_user());
                st.setString(8, clientact.getCreation_date());
                st.setString(9, clientact.getUser_update());
                st.setString(10, clientact.getLast_update());
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL INSERTAR LA ACTIVIDAD AL CLIENTE " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    /**
     * Metodo modificarClientAct, modifica un registro en la tabla clienteactividad
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void modificarClientAct() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(modificar);
                st.setInt(1, clientact.getSecuencia());
                st.setString(2, clientact.getEstado());
                st.setString(3, clientact.getBase());
                st.setString(4, clientact.getUser_update());
                st.setString(5, clientact.getLast_update());
                st.setString(6, clientact.getDstrct());
                st.setString(7, clientact.getCodCliente());
                st.setString(8, clientact.getCodActividad());
                st.setString(9, clientact.getTipoViaje());
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL MODIFICAR CLIENTEACTIVIDAD " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    /**
     * Metodo listarClientAct, lista las actividades de los clientes
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public Vector listarClientAct( ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        VecClientact = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(listar);
                
                VecClientact = new Vector();
                
                rs = st.executeQuery();
                
                while (rs.next()){
                    VecClientact.add(ClienteActividad.load(rs));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL LISTAR CLIENTE ACTIVIDAD " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return VecClientact;
    }
    /**
     * Metodo listarClientActxBusq, lista las actividades del cliente dependiendo al codigo del cliente,
     * codigo de actividad, tipo viaje. nota: la busqueda es con like.
     * @param:compa�ia, codigo actividad, cliente, tipo
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void listarClientActxBusq(String cia, String act, String clien, String tipo ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        VecClientact = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(busqueda);
                st.setString(1, cia);
                st.setString(2, act);
                st.setString(3, clien);
                st.setString(4, tipo);
                
                VecClientact = new Vector();
                ////System.out.println(st);
                
                rs = st.executeQuery();
                
                while (rs.next()){
                    ClienteActividad clientact = new ClienteActividad();
                    clientact.setCodCliente(rs.getString("codcli"));
                    clientact.setCodActividad(rs.getString("cod_Actividad"));
                    clientact.setTipoViaje(rs.getString("tipoviaje"));
                    clientact.setSecuencia(rs.getInt("secuencia"));
                    clientact.setDescortaAct(rs.getString("descorta"));
                    clientact.setLargaAct(rs.getString("deslarga"));
                    clientact.setNomCliente(rs.getString("nomcli"));
                    clientact.setDstrct(rs.getString("dstrct"));
                    clientact.setDestipo_viaje(rs.getString("descripcion"));
                    VecClientact.add(clientact);
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL LISTAR CLIENTEACTIVIDAD " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * Metodo  buscarClientAct, busca la activida dependiendo al codigo del cliente,
     * codigo de actividad, tipo viaje
     * @param: cliente, actividad, tipo, compa�ia
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarClientAct(String cli,String act, String tipo,String cia) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        clientact = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(buscar);
                st.setString(1, cia);
                st.setString(2, act);
                st.setString(3, cli);
                st.setString(4, tipo);
                
                ////System.out.println(st);
                rs = st.executeQuery();
                
                if (rs.next()){
                    ClienteActividad ca = new ClienteActividad();
                    ca.setCodCliente(rs.getString("codcli"));
                    ca.setNomCliente(rs.getString("nomcli"));
                    ca.setTipoViaje(rs.getString("tipoviaje"));
                    ca.setCodActividad(rs.getString("cod_Actividad"));
                    ca.setDescortaAct(rs.getString("descorta"));
                    ca.setLargaAct(rs.getString("deslarga"));
                    ca.setSecuencia(rs.getInt("secuencia"));
                    ca.setDstrct(rs.getString("dstrct"));
                    ca.setDestipo_viaje(rs.getString("descripcion"));
                    clientact = ca;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR CLIENTEACTIVIDAD " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    /**
     * Metodo  ExisteClientActAnulada, retorna true o false si la relacion clienteactividad esta anulada.
     * @param:cliente,actividad,tipo,distrito
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean existeClientActAnulada(String cli,String act, String tipo,String cia) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        clientact = null;
        boolean sw = false;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(existeanu);
                st.setString(1, cia);
                st.setString(2, cli);
                st.setString(3, act);
                st.setString(4, tipo);
                
                
                rs = st.executeQuery();
                
                if (rs.next()){
                    sw = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR CLIENTEACTIVIDAD ANULADO " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return sw;
    }
    /**
     * Metodo  AnularClientAct, anula de la tabla  clienteactividad.
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void anularClientAct() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(anular);
                st.setString(1, clientact.getUser_update());
                st.setString(2, clientact.getLast_update());
                st.setString(3, clientact.getDstrct());
                st.setString(4, clientact.getCodCliente());
                st.setString(5, clientact.getCodActividad());
                st.setString(6, clientact.getTipoViaje());
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL ANULAR CLIENTEACTIVIDAD " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    /**
     * Metodo ListarActiClient, lista las actividades asignadas un cliente
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void listarActiClient(String cia,  String clien ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        VecClientact = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(listarxClient);
                st.setString(1, cia);
                st.setString(2, clien);
                st.setString(3, cia);
                
                
                VecClientact = new Vector();
                ////System.out.println(st);
                
                rs = st.executeQuery();
                
                while (rs.next()){
                    ClienteActividad clientact = new ClienteActividad();
                    clientact.setCodCliente(rs.getString("codcli"));
                    clientact.setNomCliente(rs.getString("nomcli"));
                    clientact.setCodActividad(rs.getString("cod_Actividad"));
                    clientact.setTipoViaje(rs.getString("tipoviaje"));
                    clientact.setSecuencia(rs.getInt("secuencia"));
                    clientact.setDescortaAct(rs.getString("descorta"));
                    clientact.setLargaAct(rs.getString("deslarga"));
                    //clientact.setDstrct(rs.getString("dstrct"));
                    clientact.setDestipo_viaje(rs.getString("destipoviaje"));
                    VecClientact.add(clientact);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL LISTAR CLIENTEACTIVIDAD " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    /**
     * Metodo  ActiClientTipo, retorna true si encuentra la activida dependiendo al codigo del cliente,
     * cliente, tipo
     * @param: distrito, cliente, tipo viaje
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean actiClientTipo(String cia,  String clien, String tipo ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        VecClientact = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(ActClientTipo);
                st.setString(1, cia);
                st.setString(2, clien);
                st.setString(3, tipo);
                
                
                VecClientact = new Vector();
                ////System.out.println(st);
                
                rs = st.executeQuery();
                
                while (rs.next()){
                    sw = true;
                    ClienteActividad clientact = new ClienteActividad();
                    clientact.setCodCliente(rs.getString("codcli"));
                    clientact.setCodActividad(rs.getString("cod_Actividad"));
                    clientact.setTipoViaje(rs.getString("tipoviaje"));
                    clientact.setSecuencia(rs.getInt("secuencia"));
                    clientact.setDescortaAct(rs.getString("descorta"));
                    clientact.setLargaAct(rs.getString("deslarga"));
                    clientact.setDstrct(rs.getString("dstrct"));
                    VecClientact.add(clientact);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL LISTAR CLIENTEACTIVIDAD " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return sw;
    }
    
    /**
     * Metodo  existeSecuencia, retorna true existe la secuencia dependiendo distrito, cliente, nro sec tipo
     * @param: distrito, cliente, tipo viaje
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    
    public boolean existeSecuencia(String cia,  String clien,  int sec, String tipo ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        VecClientact = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_EXISTE_SECUENCIA);
                st.setString(1, cia);
                st.setString(2, clien);
                st.setInt(3, sec);
                st.setString(4, tipo);
                
                
                VecClientact = new Vector();
                ////System.out.println(st);
                
                rs = st.executeQuery();
                
                if (rs.next()){
                    sw = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL VERIFICAR SI EXISTE SEC " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return sw;
    }
    /**
     * Metodo  buscarActCliente, busca las actividades asignadas a un cliente dependiendo al codigo del cliente,
     * codigo de actividad, tipo viaje
     * @param: cliente, actividad, tipo, compa�ia
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean buscarActCliente(String cia,  String clien, String tipo ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        VecClientact = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_BUSCAR_ACTCLIENTE);
                st.setString(1, cia);
                st.setString(2, clien);
                st.setString(3, tipo);
                st.setString(4, cia);
                rs = st.executeQuery();
                VecClientact = new Vector();
                while (rs.next()){
                    sw = true;
                    ClienteActividad clact = new ClienteActividad();
                    clact.setCodCliente(rs.getString("codcli"));
                    clact.setCodActividad(rs.getString("cod_Actividad"));
                    clact.setTipoViaje(rs.getString("tipoviaje"));
                    clact.setSecuencia(rs.getInt("secuencia"));
                    clact.setDescortaAct(rs.getString("descorta"));
                    clact.setLargaAct(rs.getString("deslarga"));
                    clact.setDstrct(rs.getString("dstrct"));
                    VecClientact.add(clact);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL OBTENER LA ACTIVIDAD -> " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return sw;
    }
    /**
     * Metodo  buscarUltimaActividadCerrada, busca la ultima acitvidad cerrada del cliente,
     * depenediendo del distrito, cliente, tipo, numrem
     * @param: distrito, cliente, tipo, numrem
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public ClienteActividad buscarUltimaActividadCerrada(String cia,  String clien, String tipo, String numrem  ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        ClienteActividad clientact = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_ACTIVIDADSINFECCIERRE);
                st.setString(1, cia);
                st.setString(2, clien);
                st.setString(3, tipo);
                st.setString(4, cia);
                st.setString(5, clien);
                st.setString(6, numrem);
                
                rs = st.executeQuery();
                
                if (rs.next()) {
                    clientact = new ClienteActividad();
                    clientact.setCodActividad(rs.getString("cod_Actividad"));
                    clientact.setSecuencia(rs.getInt("secuencia"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR LA ULTIMA ACTIVIDAD CERRADA " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return clientact;
    }
    
    
    /**
     * M�todo buscarCliente  metodo para buscar los datos de un cliente
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String nombre de la tabla
     * @version.....1.0.
     **/
    public Cliente buscarCliente(String codcli)throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List lista = null;
        Cliente cli  = null;
        try {
            ps = this.crearPreparedStatement("SQL_BUSCAR_CLIENTE");
            ps.setString(1, codcli);
            rs = ps.executeQuery();
            if(rs.next()){
                cli = new Cliente();
                cli.setCodcli(rs.getString("codcli"));
                cli.setNomcli(rs.getString("nomcli"));
                
            }
        }
        catch( Exception ex ){
            throw new SQLException("ERROR OBTENIENDO REGISTROS SQL_BUSCAR_CLIENTE: "+ex.getMessage());
        }
        finally{
            if ( rs != null ) {rs.close();}
            if ( ps != null ) {ps.close();}
            this.desconectar("SQL_BUSCAR_CLIENTE");
        }
        return cli;
    }
    
    
    /**
     * M�todo buscarCliente  metodo para buscar los datos de un cliente
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String nombre de la tabla
     * @version.....1.0.
     **/
    public List buscarClientePorNombre(String nomcli)throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List lista = null;
        Connection con =null;
        try {
            String sql = this.obtenerSQL("SQL_BUSCAR_CLIENTE_POR_NOMBRE").replaceAll("#CLIENTE#",nomcli.toUpperCase());
            con = this.conectar("SQL_BUSCAR_CLIENTE_POR_NOMBRE");
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            lista = new LinkedList();
            while(rs.next()){
                Cliente cli = new Cliente();
                cli.setCodcli(rs.getString("codcli"));
                cli.setNomcli(rs.getString("nomcli"));
                lista.add(cli);
               
            }
      
    }
    catch( Exception ex ){ 
        ex.printStackTrace();
        throw new SQLException("ERROR OBTENIENDO REGISTROS SQL_BUSCAR_CLIENTE: "+ex.getMessage());
    }
    finally{
        if ( rs != null ) {rs.close();}
        if ( ps != null ) {ps.close();}
        this.desconectar("SQL_BUSCAR_CLIENTE_POR_NOMBRE");
    }
    return lista;
}


}
