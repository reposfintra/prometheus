/****************************************************************************
 * Nombre clase: Acuerdo_especialDAO.java                                   *
 * Descripci�n: Clase que maneja las consultas de los acuerdos especiales.  *
 * Autor: Ing. Jose de la rosa                                              *
 * Fecha: 6 de diciembre de 2005, 09:00 AM                                  *
 * Versi�n: Java 1.0                                                        *
 * Copyright: Fintravalores S.A. S.A.                                  *
 ***************************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

public class Acuerdo_especialDAO extends MainDAO{
    private Acuerdo_especial acuerdo_especial;
    private Vector vecacuerdo_especial;
    
    /** Creates a new instance of Acuerdo_especialDAO */
    public Acuerdo_especialDAO () {
        super ( "Acuerdo_especialDAO.xml" );
        
    }
    /**
     * Metodo: getAcuerdo_especial, permite retornar un objeto de registros de Acuerdo especial.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Acuerdo_especial getAcuerdo_especial () {
        return acuerdo_especial;
    }
    
    /**
     * Metodo: setAcuerdo_especial, permite obtener un objeto de registros de Acuerdo especial.
     * @autor : Ing. Jose de la rosa
     * @param : objeto
     * @version : 1.0
     */
    public void setAcuerdo_especial (Acuerdo_especial acuerdo_especial) {
        this.acuerdo_especial = acuerdo_especial;
    }
    
    /**
     * Metodo: getAcuerdo_especiales, permite retornar un vector de registros de Acuerdo especial.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Vector getAcuerdo_especiales () {
        return vecacuerdo_especial;
    }
    
    /**
     * Metodo: setAcuerdo_especiales, permite obtener un vector de registros de Acuerdo especial.
     * @autor : Ing. Jose de la rosa
     * @param : vector
     * @version : 1.0
     */
    public void setAcuerdo_especiales (Vector vecacuerdo_especial) {
        this.vecacuerdo_especial = vecacuerdo_especial;
    }
    
    /**
     * Metodo: insertAcuerdo_especial, permite ingresar un registro a la tabla Acuerdo_especial.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void insertAcuerdo_especial () throws SQLException{
        PreparedStatement st = null;
        try{
            st = crearPreparedStatement ("SQL_INSERT");
            st.setString (1,acuerdo_especial.getTipo_acuerdo ());
            st.setString (2,acuerdo_especial.getStandar ());
            st.setString (3,acuerdo_especial.getCodigo_concepto ());
            st.setFloat  (4,acuerdo_especial.getPorcentaje_descuento ());
            st.setString (5,acuerdo_especial.getUsuario_creacion ());
            st.setString (6,acuerdo_especial.getUsuario_modificacion ());
            st.setString (7,acuerdo_especial.getBase ());
            st.setString (8,acuerdo_especial.getDistrito ());
            st.setString (9,acuerdo_especial.getTipo());
            st.executeUpdate ();
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL INGRESAR UN ACUERDO ESPECIAL, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_INSERT");
        }
    }
    
    /**
     * Metodo: searchAcuerdo_especial, permite buscar un Acuerdo especial dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : numero de nit, nombre del propietario y la placa.
     * @version : 1.0
     */
    public void searchAcuerdo_especial (String standar, String distrito, String concepto, String tipo)throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = crearPreparedStatement ("SQL_SERCH");
            st.setString (1, distrito);
            st.setString (2, tipo);
            st.setString (3, standar);
            st.setString (4, concepto);
            
            
            rs= st.executeQuery ();
            while(rs.next ()){
                acuerdo_especial = new Acuerdo_especial ();
                acuerdo_especial.setTipo_acuerdo (rs.getString ("tipo_acuerdo"));
                acuerdo_especial.setStandar (rs.getString ("std_job_no"));
                acuerdo_especial.setCodigo_concepto (rs.getString ("codigo_concepto"));
                acuerdo_especial.setPorcentaje_descuento (rs.getFloat ("porcentaje_descuento"));
                acuerdo_especial.setTipo(rs.getString ("tipo"));
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR UN ACUERDO ESPECIAL, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_SERCH");
        }
    }
    
    /**
     * Metodo: existAcuerdo_especial, permite buscar un Acuerdo especial dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : numero de nit, nombre del propietario y la placa.
     * @version : 1.0
     */
    public boolean existAcuerdo_especial (String standar, String distrito, String concepto, String tipo) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        try {
            st = crearPreparedStatement ("SQL_SERCH");
            st.setString (1, distrito);
            st.setString (2, tipo);
            st.setString (3, standar);            
            st.setString (4, concepto);
            
            rs= st.executeQuery ();
            if (rs.next ()){
                sw = true;
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR SI EXISTEN LOS ACUERDOS ESPECIALES, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_SERCH");
        }
        return sw;
    }
    
    /**
     * Metodo: existStandarTipo, permite buscar un Acuerdo especial dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : numero del estandar, el tipo de acuerdo.
     * @version : 1.0
     */
    public void existStandarTipo (String standar, String tipo, String distrito, String tipoP) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = crearPreparedStatement ("SQL_EXISTE_ESTANDAR");
            st.setString (1, standar);
            st.setString (2, tipo);
            st.setString (3, distrito);
            st.setString (4, tipoP);
            rs= st.executeQuery ();
            vecacuerdo_especial = new Vector ();
            while(rs.next ()){
                acuerdo_especial = new Acuerdo_especial ();
                acuerdo_especial.setTipo_acuerdo (rs.getString ("tipo_acuerdo"));
                acuerdo_especial.setStandar (rs.getString ("std_job_no"));
                acuerdo_especial.setCodigo_concepto (rs.getString ("codigo_concepto"));
                acuerdo_especial.setPorcentaje_descuento (rs.getFloat ("porcentaje_descuento"));
                acuerdo_especial.setTipo(rs.getString ("tipo"));
                vecacuerdo_especial.add (acuerdo_especial);
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR LOS ESTANDARES DE LOS ACUERDOS ESPECIALES, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_EXISTE_ESTANDAR");
        }
    }
    
    /**
     * Metodo: listAcuerdo_especial, permite listar todas los Acuerdo especiales
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void listAcuerdo_especial ( String distrito )throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = crearPreparedStatement ("SQL_LIST");
            st.setString (1, distrito);
            rs= st.executeQuery ();
            vecacuerdo_especial = new Vector ();
            while(rs.next ()){
                acuerdo_especial = new Acuerdo_especial ();
                acuerdo_especial.setTipo_acuerdo (rs.getString ("tipo_acuerdo"));
                acuerdo_especial.setStandar (rs.getString ("std_job_no"));
                acuerdo_especial.setCodigo_concepto (rs.getString ("codigo_concepto"));
                acuerdo_especial.setPorcentaje_descuento (rs.getFloat ("porcentaje_descuento"));
                vecacuerdo_especial.add (acuerdo_especial);
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL LISTAR LOS ACUERDOS ESPECIALES, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_LIST");
        }
    }
    
    /**
     * Metodo: updateAcuerdo_especial, permite actualizar un Acuerdo especial dados unos parametros.
     * @autor : Ing. Jose de la rosa
     * @param : numero de nit, nombre del propietario, la placa y usuario.
     * @version : 1.0
     */
    public void updateAcuerdo_especial () throws SQLException{
        PreparedStatement st = null;
        try{
            st = crearPreparedStatement ("SQL_UPDATE");
            st.setString (1,acuerdo_especial.getTipo_acuerdo ());
            st.setFloat  (2,acuerdo_especial.getPorcentaje_descuento ());
            st.setString (3,acuerdo_especial.getUsuario_modificacion ());
            st.setString (4,acuerdo_especial.getDistrito ());
            st.setString(5, acuerdo_especial.getTipo());
            st.setString (6,acuerdo_especial.getStandar ());
            st.setString (7,acuerdo_especial.getCodigo_concepto ());
            //System.out.println("sql->"+st);
            st.executeUpdate ();
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL ACTUALIZAR LOS ACUERDOS ESPECIALES, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_UPDATE");
        }
    }
    
    /**
     * Metodo: anularacuerdo_especial, permite anular una Acuerdo especial.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void anularAcuerdo_especial () throws SQLException{
        PreparedStatement st = null;
        try{
            st = crearPreparedStatement ("SQL_ANULAR");
            st.setString (1,acuerdo_especial.getUsuario_modificacion ());
            st.setString (2,acuerdo_especial.getDistrito ());
            st.setString (3,acuerdo_especial.getTipo());
            st.setString (4,acuerdo_especial.getStandar ());
            st.setString (5,acuerdo_especial.getCodigo_concepto ());
            st.executeUpdate ();
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL ANULAR LOS ACUERDOS ESPECIALES, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_ANULAR");
        }
    }
    
    /**
     * Metodo: searchDetalleAcuerdo_especial, permite listar Acuerdo especiales por detalles dados unos parametros.
     * @autor : Ing. Jose de la rosa
     * @param : numero de nit, nombre del propietario y la placa.
     * @version : 1.0
     */
    public void searchDetalleAcuerdo_especial (String tipo, String standar, String codigo, String distrito, String tipoP) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = crearPreparedStatement ("SQL_SEARCH_DETALLES");
            st.setString (1, distrito);
            st.setString (2, tipoP+"%");
            st.setString (3, standar+"%");
            st.setString (4, tipo+"%");
            st.setString (5, codigo+"%");
            
            rs = st.executeQuery ();
            vecacuerdo_especial = new Vector ();
            while(rs.next ()){
                acuerdo_especial = new Acuerdo_especial ();
                acuerdo_especial.setTipo_acuerdo (rs.getString ("tipo_acuerdo"));
                acuerdo_especial.setStandar (rs.getString ("std_job_no"));
                acuerdo_especial.setCodigo_concepto (rs.getString ("codigo_concepto"));
                acuerdo_especial.setPorcentaje_descuento (rs.getFloat ("porcentaje_descuento"));
                acuerdo_especial.setTipo(rs.getString("tipo"));
                vecacuerdo_especial.add (acuerdo_especial);
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR LOS ACUERDOS ESPECIALES, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_SEARCH_DETALLES");
        }
    }
    
    
}
