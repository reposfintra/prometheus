/***********************************************************************************
 * Nombre clase : ............... ExtractoDAO.java                                 *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la tablas necesarias para la generaci�n de   *
 *                                extractos.                                       *
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                     *
 * Fecha :....................... 03 de enero de 2006, 11:02 AM                    *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/
package com.tsp.operation.model.DAOS;
import java.sql.*;
import com.tsp.util.connectionpool.*;
import java.util.*;
import java.text.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.TasaService;
import com.tsp.exceptions.*;

public class ExtractoDAO extends MainDAO{
    
    
    private Vector extracto;
    private String mensaje;    
    private String banco     = "";
    private String cuenta    = "";
    private String aprobadas = "";
    private String pagadas   = "";
    private double toralFactProveedor = 0;
    private String propietarios = "";    
    public  String filtroProveedor = "";
    private Vector beneficiarios = null;
    TasaService tasaService = new TasaService();
    /** Creates a new instance of ExtractoCarbonDAO  */
    public ExtractoDAO() {
        super( "ExtractoDAO.xml" );
    }
    public ExtractoDAO(String dataBaseName) {
        super( "ExtractoDAO.xml", dataBaseName );
    }
    
    /**
     *Metodo para buscar la cantidad y valor total de los cheques de una corrida
     */
    private double [] searchQtyValCheques(String distrito, String banco, String cuenta, String corrida) throws SQLException{
        double[] chq = null;
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "QRY_CHEQUES";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, corrida);
                st.setString(2, distrito);
                st.setString(3, banco);
                st.setString(4, cuenta);
                rs = st.executeQuery();
                if (rs.next()) {
                    chq = new double[2];
                    chq[0] = rs.getDouble(1);
                    chq[1] = rs.getDouble(2);
                }
            }
        }catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR LA CANTIDAD Y VALOR TOTAL DE LOS CHEQUES DE CORRIDA, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return chq;
    }
    
    /**
     *Metodo para obtener la informacion de la
     *corrida.
     */
    private Run getInfoRun(String distrito, String banco, String cuenta, String corrida, String tipo) throws SQLException, InformationException{
        Connection con = null;//JJCastro fase2
        Run run = null;
        double[] cheq = null;
        DecimalFormat form = null;
        Calendar fechaCump = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        PreparedStatement psttm = null;
        ResultSet rs = null;
        String query = "SQL_QRY_RUN";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                psttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                psttm.setString(1, distrito);
                psttm.setString(2, corrida);
                psttm.setString(3, banco);
                psttm.setString(4, cuenta);
                rs = psttm.executeQuery();
                if (rs.next()) {
                    run = new Run();
                    run.setAcct_dstrct(rs.getString(1));
                    run.setBanco(rs.getString(2));
                    run.setSucursal(rs.getString(3));
                    run.setCheque_run_no(rs.getString(4));
                    run.setRc_due_date(rs.getString(5));
                    run.setTpago(rs.getString("tpago"));
                }
                if (run != null) {
                    run.setMsg("");
                    run.setTipo(tipo);
                }
            }
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL BUSCAR LA INFORMACION DE LA CORRIDA, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (psttm  != null){ try{ psttm.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return run;
    }


/**
 * 
 * @param distrito
 * @param banco
 * @param cuenta
 * @param corrida
 * @param aprobadas
 * @param pagadas
 * @param tipo
 * @throws SQLException
 * @throws InformationException
 */
    public void extracto(String distrito, String banco, String cuenta, String corrida, String aprobadas,String pagadas, String tipo) throws SQLException, InformationException {
        String propietario = "", tipo_pago = "", banco_transfer = "", suc_transfer = "";
        String placa = "", tipo_cuenta = "", no_cuenta = "", cedula_cuenta = "", nombre_cuenta = "";
        String supp_to_pay = null;
        String supp_name = null;
        String supplier_no = null;
        String inv_no = null;
        String po_no = null;
        String ext_inv_no = null;
        String moneda = null;
        double totalFactura = 0;
        double totalPlaca = 0;
        double totalPropietario = 0;
        EncPropietario encProp = null;
        EncFactura encFact = null;
        this.banco = banco;
        this.cuenta = cuenta;
        this.aprobadas = aprobadas;
        this.pagadas   = pagadas;
        java.util.Date hoy = new java.util.Date();
        SimpleDateFormat formato = new SimpleDateFormat("yyyyMMdd");
        String fecha = formato.format(hoy);
        Run run = null;
        Descuento desc = null;
        DetalleFactura detalleFact = null;
        PreparedStatement psttm = null;
        ResultSet rs = null;
        Connection  con = null;
        String      query = "SQL_QRY_DETALLE_RUN";
        String sql = "";
        String varT = "";
        if (tipo.equals("A"))
            varT = "";
        else
            varT = "PLACA";
        try {
            run = getInfoRun(distrito, banco, cuenta, corrida, tipo); /*Informacion de la Corrida OK*/
            if (run!=null){
                moneda = getMonedaBanco(banco, cuenta);/*Obteniendo la moneda del banco OK*/                
                con = this.conectarJNDI(query);//JJCastro fase2
                //System.out.println("PRIPIETARIOS DAO: "+this.getPropietarios());                
                if (!this.getPropietarios().equals("") && this.getPropietarios().length()>1){                    
                    filtroProveedor = "AND a.beneficiario IN ("+this.getPropietarios()+")";
                }
                //cuando esten pagados no sirve el filtro de jose
                sql = this.obtenerSQL(query);
                if(pagadas.equals("S")){
                    sql = sql.replaceAll("#CHEQUE#","AND a.cheque !='' AND a.pago !='0099-01-01 00:00:00'").replaceAll("#PAGO#","");
                    sql  = sql.replaceAll("#BENEFICIARIO#", filtroProveedor);
                }else{
                    sql = sql.replaceAll("#CHEQUE#","AND a.cheque =''");
                    sql  = sql.replaceAll("#BENEFICIARIO#", filtroProveedor);                    
                    if(aprobadas.equals("ok"))
                        sql = sql.replaceAll("#PAGO#","AND a.pago !='0099-01-01 00:00:00'");
                    else
                        sql = sql.replaceAll("#PAGO#","");
                }
                
                
                psttm = con.prepareStatement( sql );//OK QRY
                psttm.setString(1, distrito);
                psttm.setString(2, corrida);
                psttm.setString(3, banco);
                psttm.setString(4, cuenta);
                //exclusi�n docuemntos RA y valor 0
                psttm.setString(5, distrito);
                psttm.setString(6, corrida);
                psttm.setString(7, banco);
                psttm.setString(8, cuenta);
                //System.out.println("QUERY DETALLE RUN: "+psttm);
                rs = psttm.executeQuery();
                extracto = new Vector();
                extracto.add(run);
                boolean swi = false;
                while (rs.next()) {
                    swi = true;
                    supp_to_pay = rs.getString(1);
                    supp_name = rs.getString(2);
                    supplier_no = rs.getString(3);
                    inv_no = rs.getString(4);
                    po_no = rs.getString(5);
                    ext_inv_no = rs.getString(4);
                    tipo_pago = rs.getString(7);
                    banco_transfer = rs.getString(8);
                    suc_transfer = rs.getString(9);
                    tipo_cuenta = rs.getString(10);
                    no_cuenta = rs.getString(11);
                    cedula_cuenta = rs.getString(12);
                    nombre_cuenta = rs.getString(13);                   
                    if (!propietario.equals(supp_to_pay)){
                        if (encProp != null){
                            //ADICION DE TOTAL POR PLACA CUANDO ES LA ULTIMA O LA UNICA PLACA DE UN PROPIETARIO
                            detalleFact = new DetalleFactura();
                            detalleFact.setFacturaNo("<b>TOTAL "+varT+" "+placa+":</b>");
                            //   detalleFact = totalFactura(distrito, corrida, banco, cuenta, inv_no,supplier_no, moneda, supp_to_pay,tipo);
                            // totalPlaca  = detalleFact.getItemValor();
                            detalleFact.setItemValor(totalPlaca);
                            detalleFact.setMoneda(moneda);
                            encProp.getFacturas().add(detalleFact);
                            totalPlaca = 0;
                            //ADICIONA EL TOTAL POR PROPIETARIO
                            detalleFact = new DetalleFactura();
                            detalleFact.setFacturaNo("<b>TOTAL PROPIETARIO "+propietario+":</b>");
                            placa = supplier_no;
                            totalPropietario = getTotalProp(distrito, banco, cuenta, corrida, propietario,moneda, aprobadas, pagadas);
                            if (totalPropietario==0)
                                detalleFact.setFacturaNo("<b>NO HAY TASA DE CONVERSI�N PARA LA FECHA DE CREACI�N DE ESTA FACTURA");
                            detalleFact.setItemValor(totalPropietario);
                            detalleFact.setTotalFacturaProveedor(toralFactProveedor);
                            detalleFact.setMoneda(moneda);
                            encProp.getFacturas().add(detalleFact);
                            toralFactProveedor = 0;
                            //  encProp.getFacturas().add(new Comentario());
                        }
                        propietario = supp_to_pay;
                        
                        encProp = new EncPropietario();
                        encProp.setPropietario(supp_to_pay);//id Propietario
                        encProp.setNombre(supp_name);//Nombre Propietario
                        encProp.setFecha(fecha);
                        encProp.setCorrida(run.getCheque_run_no()+" &nbsp;de&nbsp; "+run.getBanco() + " " + run.getSucursal());
                        encProp.setFechaCumplido(run.getRc_due_date());
                        encProp.setFacturas(new Vector());                                                                                                                                                                                                                                                                                                                                               encProp.setTipo_pago(tipo_pago);
                        encProp.setBanco_transfer(banco_transfer);
                        encProp.setSuc_transfer(suc_transfer);
                        encProp.setTipo_cuenta(tipo_cuenta);
                        encProp.setNo_cuenta(no_cuenta);
                        encProp.setCedula_cuenta(cedula_cuenta);
                        encProp.setNombre_cuenta(nombre_cuenta);
                        encProp.getFacturas().add(placa.equals("")?supplier_no:placa);
                        encFact = searchEncFact(po_no);//Busqueda de encabezado de la Factura OK
                        encProp.getFacturas().add(encFact);
                        encProp.getFacturas().addAll(getDetalleFact(distrito, corrida, inv_no, supp_to_pay, supplier_no, moneda));
                        //ADICION DE EL TOTAL DE LA FACTURA QRY OK
                        detalleFact = totalFactura(distrito, corrida, banco, cuenta, inv_no,supplier_no, moneda, supp_to_pay,tipo);
                        totalFactura = detalleFact.getItemValor();
                        totalPlaca = totalPlaca + totalFactura;
                        if (inv_no.startsWith("OP"))
                            encProp.getFacturas().add(detalleFact);
                        extracto.add(encProp);
                    }
                    else{
                        if (!placa.equals(supplier_no)){
                            placa = placa.equals("")?supplier_no:placa;
                            //ADICION DE TOTAL POR PLACA
                            detalleFact = new DetalleFactura();
                            detalleFact.setFacturaNo("<b>TOTAL "+varT+" "+placa+":</b>");
                            detalleFact.setItemValor(totalPlaca);
                            detalleFact.setMoneda(moneda);
                            encProp.getFacturas().add(detalleFact);
                            totalPlaca = 0;
                            placa = supplier_no;
                            encProp.getFacturas().add(placa);
                            encFact = searchEncFact(po_no);//OK
                            encProp.getFacturas().add(encFact);
                            encProp.getFacturas().addAll(getDetalleFact(distrito, corrida, inv_no, supp_to_pay, supplier_no,moneda));
                            //ADICION DE EL TOTAL DE LA FACTURA OK
                            detalleFact = totalFactura(distrito, corrida, banco, cuenta,inv_no,supplier_no, moneda,supp_to_pay,tipo);
                            totalFactura = detalleFact.getItemValor();
                            totalPlaca = totalPlaca + totalFactura;
                            if (inv_no.startsWith("OP"))
                                encProp.getFacturas().add(detalleFact);
                        }
                        else{
                            
                            encFact = searchEncFact(po_no);//Enc Factura OK

                            encProp.getFacturas().add(encFact);
                            encProp.getFacturas().addAll(getDetalleFact(distrito, corrida, inv_no, supp_to_pay, supplier_no,moneda));
                            //ADICION DE EL TOTAL DE LA FACTURA OK
                            detalleFact = totalFactura(distrito, corrida, banco, cuenta, inv_no,supplier_no, moneda,supp_to_pay,tipo);
                            totalFactura = detalleFact.getItemValor();
                            totalPlaca = totalPlaca + totalFactura;
                            if (inv_no.startsWith("OP"))
                                encProp.getFacturas().add(detalleFact);
                        }
                    }
                }
                if (swi==true) {
                    //TOTALIZACION DE LA ULTIMA PLACA
                    detalleFact = new DetalleFactura();
                    detalleFact.setFacturaNo("<b>TOTAL "+varT+" "+placa+":</b>");
                    //totalPlaca = redondear(totalPlaca, 3);
                    detalleFact.setItemValor(totalPlaca);
                    detalleFact.setMoneda(moneda);
                    encProp.getFacturas().add(detalleFact);
                    //TOTALIZACION DEL ULTIMO PROPIETARIO
                    detalleFact = new DetalleFactura();//QRY CHK OK
                    totalPropietario = getTotalProp(distrito, banco, cuenta, corrida, propietario,moneda, aprobadas, pagadas);
                    if (totalPropietario==0)
                        detalleFact.setFacturaNo("<b>NO HAY TASA DE CONVERSI�N PARA LA FECHA DE CREACI�N DE ESTA FACTURA");
                    else
                        detalleFact.setFacturaNo("<b>TOTAL PROPIETARIO "+propietario+":</b>");
                    detalleFact.setItemValor(totalPropietario);
                    detalleFact.setTotalFacturaProveedor(toralFactProveedor);
                    detalleFact.setMoneda(moneda);
                    encProp.getFacturas().add(detalleFact);
                    toralFactProveedor = 0;
                    //encProp.getFacturas().add(new Comentario());
                } else {
                    this.setMensaje("No se pudo generar el reporte con los parametros de busqueda");
                }
                /*Agregando el Total Banco
                if (swi) {
                    double total = 0;
                    detalleFact = new DetalleFactura();
                    detalleFact.setMoneda(moneda);
                    total = this.getTotalBanco(distrito, this.banco, this.cuenta, corrida,moneda,this.aprobadas,this.pagadas);
                    detalleFact.setFacturaNo("<b>TOTAL BANCO :</b>");
                    detalleFact.setItemValor(total);
                    encProp.getFacturas().add(detalleFact);
                    encProp.getFacturas().add(new Comentario());
                    //detalle.add(detalleFact);
                }*/
            } else {
                this.setMensaje("Algunas de las facturas en esta corrida no tienen datos en la cabecera. No se pudo generar el reporte");
            }
        }catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL OBTENER EL EXTRACTO, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (psttm  != null){ try{ psttm.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    
    /**
     *
     *Metodo para obtener el detalle de una factura
     */
    private Vector getDetalleFact(String dstrct, String corrida, String inv_no, String supp_to_pay, String supplier, String monedaBanco) throws SQLException {
        Vector detalle = null;
        Vector anticipo = null;
        DetalleFactura detalleFact = null;
        String po_no = null;
        String tmp = null;
        String moneda = null;
        PreparedStatement psttm = null;
        ResultSet rs = null;
        Connection con = null;
        String factura = "";
        double ajustePlanilla = 0;
        double valor_tasa = 0;
        String fecha_documento = "";
        try {
            con = this.conectarJNDI("QRY_DETALLE_FACT");//JJCastro fase2
            String sql    =   this.obtenerSQL("QRY_DETALLE_FACT");
            psttm = con.prepareStatement(sql);
            psttm.setString(1, dstrct);
            psttm.setString(2, corrida);
            //psttm.setString (2, supplier);
            psttm.setString(3, inv_no);
            psttm.setString(4, supp_to_pay);
            //System.out.println("QRY DETALLE:"+psttm);
            rs = psttm.executeQuery();
            if (rs.next()){
                detalle = new Vector();
                rs.beforeFirst();
                while (rs.next()) {
                    moneda  = rs.getString(6);
                    fecha_documento = rs.getString(8);
                    factura = rs.getString(1);
                    if (moneda.equals(monedaBanco)){
                        detalleFact = new DetalleFactura();
                        detalleFact.setFacturaNo(factura);
                        detalleFact.setItemNo(rs.getString(2));
                        detalleFact.setItemDesc(rs.getString(3));
                        detalleFact.setItemValor(rs.getDouble(5));
                        detalleFact.setMoneda(monedaBanco);
                        detalleFact.setIva(rs.getDouble("IVA"));
                        detalleFact.setRiva(rs.getDouble("RIVA"));
                        detalleFact.setRica(rs.getDouble("RICA"));
                        detalleFact.setRfte(rs.getDouble("RFTE"));
                        detalleFact.setNeto(detalleFact.getItemValor()+detalleFact.getIva()+detalleFact.getRiva()+detalleFact.getRica()+detalleFact.getRfte());
                        detalleFact.setConcepto(rs.getString("concepto"));
                        if (rs.getString("concepto").equals("09"))
                            ajustePlanilla = rs.getDouble(5);
                        
                    }
                    else{
                        Tasa tasa = tasaService.buscarValorTasa(monedaBanco, moneda, monedaBanco, fecha_documento);
                        if (tasa != null)
                            valor_tasa = tasa.getValor_tasa();
                        detalleFact = new DetalleFactura();
                        detalleFact.setFacturaNo(factura);
                        detalleFact.setItemNo(rs.getString(2));
                        detalleFact.setItemDesc(rs.getString(3));
                        detalleFact.setItemValor(com.tsp.util.Util.redondearByMoneda(rs.getDouble(4)*valor_tasa,monedaBanco));
                        detalleFact.setMoneda(monedaBanco);
                        detalleFact.setIva(com.tsp.util.Util.redondearByMoneda(rs.getDouble("IVA")*valor_tasa,monedaBanco));
                        detalleFact.setRiva(com.tsp.util.Util.redondearByMoneda(rs.getDouble("RIVA")*valor_tasa,monedaBanco));
                        detalleFact.setRica(com.tsp.util.Util.redondearByMoneda(rs.getDouble("RICA")*valor_tasa,monedaBanco));
                        detalleFact.setRfte(com.tsp.util.Util.redondearByMoneda(rs.getDouble("RFTE")*valor_tasa,monedaBanco));
                        detalleFact.setNeto((detalleFact.getItemValor()+detalleFact.getIva()+detalleFact.getRiva()+detalleFact.getRica()+detalleFact.getRfte()));
                        detalleFact.setConcepto(rs.getString("concepto"));
                        if (rs.getString("concepto").equals("09"))
                            ajustePlanilla = com.tsp.util.Util.redondearByMoneda(rs.getDouble(4)*valor_tasa,monedaBanco);
                    }
                    
                    tmp = rs.getString(7);
                    tmp = tmp.trim();
                    if ((!"".equals(tmp)) && (tmp != null))
                        po_no = tmp;
                    detalle.add(detalleFact);
                    //System.out.println("Tamano detalle ANTES "+detalle.size());
                    if (rs.isLast()) {
                        
                        //Informaci�n de Notas creditos y notas debitos de la factura.
                        this.getNotasCreditosDebitosFactura(detalle,dstrct, factura, supp_to_pay, corrida ,monedaBanco);
                        
                        //Totalizacion de impuestos
                        this.getTotalImpuestos(detalle,dstrct,supp_to_pay,factura,corrida,monedaBanco);
                        
                        double total = 0;
                        //Agregando el Saldo Factura
                        detalleFact = new DetalleFactura();
                        detalleFact.setMoneda(monedaBanco);
                        total = this.getSaldoFactura(dstrct, corrida, supp_to_pay, factura,monedaBanco);
                        detalleFact.setFacturaNo("<b>SALDO FACTURA :</b>");
                        detalleFact.setItemValor(total);
                        detalle.add(detalleFact);
                        
                        //Agregando el Bruto de la factura
                        detalleFact = new DetalleFactura();
                        detalleFact.setMoneda(monedaBanco);
                        total = this.getTotalFactura(dstrct,supp_to_pay,factura,monedaBanco);
                        detalleFact.setFacturaNo("<b>TOTAL FACTURA :</b>");
                        detalleFact.setItemValor(total);
                        detalle.add(detalleFact);
                        
                        
                    }
                }
                //Recorriendo el vector para eliminar ajuste planilla y agregarle el valor al Anticipo
                int tam = detalle.size();
                DetalleFactura detalleF = null;
                double val              = 0;
                int i = 0;
                while (i<tam-1) {                
                    detalleF =  (DetalleFactura)detalle.get(i);
                    if (detalleF.getConcepto()!=null && detalleF.getConcepto().equals("01")) {
                        
                        val = ajustePlanilla+detalleF.getItemValor();
                        
                        detalleF.setItemValor(val);
                        detalleF.setNeto(val);
                        detalle.removeElementAt(i);
                        detalle.add(i, detalleF);
                    }else if (detalleF.getConcepto()!=null  && detalleF.getConcepto().equals("09"))
                        detalle.removeElementAt(i);
                    i++;
                }                
            }
        }catch(Exception ex){
            ex.printStackTrace();

        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (psttm  != null){ try{ psttm.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return detalle;
    }
    
    /**
     *Metodo para calcular el total de una factura
     */
/**
 * 
 * @param dstrct
 * @param corrida
 * @param banco
 * @param sucursal
 * @param factura
 * @param placa
 * @param monedaBanco
 * @param beneficiario
 * @param tipo
 * @return
 * @throws SQLException
 */
    public DetalleFactura totalFactura(String dstrct, String corrida, String banco, String sucursal, String factura, String placa, String monedaBanco,String beneficiario, String tipo) throws SQLException{
        Connection con = null;//JJCastro fase2
        String inv_date = null;
        String currency_type = null;
        DetalleFactura detalleFact = null;
        double total = 0;
        double tasaCambio = 0;
        PreparedStatement psttm = null;
        ResultSet rs = null;
        String query = "QRY_TOTAL_FACT";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            psttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            psttm.setString(1, dstrct);
            psttm.setString(2, corrida);
            psttm.setString(3, factura);
            psttm.setString(4, beneficiario);
            psttm.setString(5, banco);
            psttm.setString(6, sucursal);
            rs = psttm.executeQuery();
            if (rs.next()){
                detalleFact = new DetalleFactura();
            }
            if (detalleFact!=null){
                total = this.getTotalPlanilla(dstrct, banco, sucursal, corrida, factura ,beneficiario,monedaBanco);
                detalleFact.setFacturaNo(tipo.equals("P")?"<b>TOTAL PLANILLA:</b>":"<b>TOTAL FACTURA:</b>");
                detalleFact.setItemValor(total);
                detalleFact.setMoneda(monedaBanco);
            } else
                detalleFact = new DetalleFactura();
            
            
        }}catch(SQLException ex){
            throw new SQLException("ERROR AL CALCULAR EL TOTAL DE LA FACTURA, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (psttm  != null){ try{ psttm.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return detalleFact;
    }
    
    /**
     *Metodo que busca el encabezado de la de la factura de la placa a partir de
     *la oc.
     */
/**
 * 
 * @param oc
 * @return
 * @throws SQLException
 */
    private EncFactura searchEncFact(String oc) throws SQLException{
        Connection con = null;//JJCastro fase2
        String ot = null;
        EncFactura encFact = null;
        PreparedStatement psttm = null;
        ResultSet rs = null;
        String query = "QRY_OT_OC";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                psttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                psttm.setString(1, oc);
                rs = psttm.executeQuery();
                if (rs.next()) {
                    encFact = new EncFactura();
                    encFact.setPo_no(oc);
                    encFact.setRemision(rs.getString(1));
                    encFact.setNc(rs.getString(2).length() >= 6 ? rs.getString(2).substring(3, 6) : "");
                    encFact.setFecha(rs.getString(3));
                    encFact.setConductor(rs.getString(4));
                    encFact.setCarga(rs.getString(5) + " " + rs.getString(6));
                    encFact.setOrigen(rs.getString("origen"));
                    encFact.setDestino(rs.getString("destino"));
                }
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL BUSCAR EL ENCABEZADO DE LA FACTURA DE LA PLACA, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (psttm  != null){ try{ psttm.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return encFact;
    }
    /**
     *Metodo para obtener la tasa de conversion entre mon
     *
     */
    private double getTasaConversion(String monFor, String fecha) throws SQLException{
        double tasa = 0;
        Connection con = null;//JJCastro fase2
        PreparedStatement psttm = null;
        ResultSet rs = null;
        String query = "QRY_CONV_MONEDAS";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                psttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                psttm.setString(1, monFor);
                psttm.setString(2, fecha.substring(0, 10));
                rs = psttm.executeQuery();
                if (rs.next()) {
                    tasa = rs.getDouble(1);
                }
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL OBTENER LA TASA DE CONVERSION, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (psttm  != null){ try{ psttm.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return tasa;
    }


/**
 * 
 * @param po_no
 * @return
 * @throws SQLException
 */
    private Vector getAnticipo(String po_no) throws SQLException{
        Connection con = null;//JJCastro fase2
        Vector anticipo = null;
        DetalleFactura detalleFact = null;
        String moneda = null;
        PreparedStatement psttm = null;
        ResultSet rs = null;
        String query = "QRY_ANTICIPO";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                psttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                psttm.setString(1, po_no);
                rs = psttm.executeQuery();
                anticipo = new Vector();
                while (rs.next()) {
                    detalleFact = new DetalleFactura();
                    detalleFact.setFacturaNo(rs.getString(1));
                    detalleFact.setItemDesc(rs.getString(2));
                    moneda = rs.getString(5);
                    detalleFact.setMoneda(moneda);
                    if (moneda.equals("PES")) {
                        detalleFact.setItemValor(-rs.getDouble(3));
                    } else {
                        detalleFact.setItemValor(-rs.getDouble(4));
                    }
                    anticipo.add(detalleFact);
                }
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL OBTENER EL ANTICIPO, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (psttm  != null){ try{ psttm.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return anticipo;
    }


 /**
  *
  * @param banco
  * @param cuenta
  * @return
  * @throws SQLException
  */
    private String getMonedaBanco(String banco, String cuenta)throws SQLException{
        Connection con = null;//JJCastro fase2
        String moneda = null;
        PreparedStatement psttm = null;
        ResultSet rs = null;
        String query = "QRY_MONEDA_BANCO";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                psttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                psttm.setString(1, banco);
                psttm.setString(2, cuenta);
                rs = psttm.executeQuery();
                if (rs.next()) {
                    moneda = rs.getString(1);
                }
                return moneda;
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL OBTENER LA MONEDA DEL BANCO, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (psttm  != null){ try{ psttm.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return moneda;
    }
    
    /**
     *Metodo para obtener el total del cheque de un propietario
     */
    public double getTotalProp(String distrito, String banco, String cuenta, String corrida, String propietario,String monedaBanco,String aprobadas,String pagadas) throws SQLException{
        PreparedStatement psttm = null;
        ResultSet rs = null;
        String moneda = "";
        String fecha_documento = "";
        double total = 0;
        double valor_tasa = 0;
        Connection con = null;
        try {
            String sql = this.obtenerSQL("QRY_CHQ_PROP");
            con = this.conectarJNDI("QRY_CHQ_PROP");//JJCastro fase2
            
            if(pagadas.equals("S")){
                sql = sql.replaceAll("#CHEQUE#","AND c.cheque !='' AND c.pago !='0099-01-01 00:00:00'").replaceAll("#PAGO#","");
                sql  = sql.replaceAll("#BENEFICIARIO#", filtroProveedor);
            }else{
                sql = sql.replaceAll("#CHEQUE#","AND c.cheque =''");
                sql  = sql.replaceAll("#BENEFICIARIO#", filtroProveedor);
                if(aprobadas.equals("ok"))
                    sql = sql.replaceAll("#PAGO#","AND c.pago !='0099-01-01 00:00:00'");
                else
                    sql = sql.replaceAll("#PAGO#","");
            }
            
            psttm = con.prepareStatement(sql);
            psttm.setString(1, distrito);
            psttm.setString(2, corrida);
            psttm.setString(3, propietario);
            psttm.setString(4, banco);
            psttm.setString(5, cuenta);
            rs = psttm.executeQuery();
            while (rs.next()){
                moneda = rs.getString(3);
                fecha_documento = rs.getString(4);
                if (moneda.equals(monedaBanco))
                    total += rs.getDouble(1);
                else {
                    Tasa tasa = tasaService.buscarValorTasa(monedaBanco, moneda, monedaBanco, fecha_documento);
                    if (tasa != null)
                        valor_tasa = tasa.getValor_tasa();
                    total += com.tsp.util.Util.redondearByMoneda(rs.getDouble(2)*valor_tasa,monedaBanco);
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (psttm  != null){ try{ psttm.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return total;
    }
    
    
    /**
     * Getter for property extracto.
     * @return Value of property extracto.
     */
    public java.util.Vector getExtracto() {
        return extracto;
    }
    
    /**
     * Setter for property extracto.
     * @param extracto New value of property extracto.
     */
    public void setExtracto(java.util.Vector extracto) {
        this.extracto = extracto;
    }
    
    /**
     * Getter for property mensaje.
     * @return Value of property mensaje.
     */
    public java.lang.String getMensaje() {
        return mensaje;
    }
    
    
    
    /**
     * Setter for property mensaje.
     * @param mensaje New value of property mensaje.
     */
    public void setMensaje(java.lang.String mensaje) {
        this.mensaje = mensaje;
    }
    
    /**
     *Metodo para obtener el total del cheque de un propietario
     */
    public double getTotalPlanilla(String distrito, String banco, String cuenta, String corrida,String factura, String beneficiario, String monedaBanco) throws SQLException{
        PreparedStatement psttm = null;
        ResultSet rs = null;
        String moneda = "";
        String fecha_documento = "";
        double total = 0;
        double valor_tasa = 0;
        Connection con = null;
        String query = "SQL_TOTAL_PLANILLA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            psttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            psttm.setString(1, distrito);
            psttm.setString(2, corrida);
            psttm.setString(3, factura);
            psttm.setString(4, beneficiario);
            psttm.setString(5, banco);
            psttm.setString(6, cuenta);
            rs = psttm.executeQuery();
            if (rs.next()){
                moneda = rs.getString(3);
                fecha_documento =  rs.getString(4);
                if (moneda.equals(monedaBanco))
                    total = rs.getDouble(1);
                else {
                    Tasa tasa = tasaService.buscarValorTasa(monedaBanco, moneda, monedaBanco, fecha_documento);
                    if (tasa != null)
                        valor_tasa = tasa.getValor_tasa();
                    total = com.tsp.util.Util.redondearByMoneda(rs.getDouble(2)*valor_tasa,monedaBanco);
                }
            }
        }}catch(Exception ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (psttm  != null){ try{ psttm.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return total;
    }
    
    /**
     *Metodo para obtener el total del cheque de un propietario
     */
    public double getNetoFactura(String distrito, String factura, String beneficiario, String monedaBanco) throws SQLException{
        Connection con = null;//JJCastro fase2
        double netoFact = 0;
        PreparedStatement psttm = null;
        ResultSet rs = null;
        String moneda = "";
        String fecha_documento = "";
        double valor_tasa = 0;
        String query = "SQL_NETO_FACTURA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            psttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            psttm.setString(1, distrito);
            psttm.setString(2, beneficiario);
            psttm.setString(3, factura);
            rs = psttm.executeQuery();
            if (rs.next()){
                moneda          = rs.getString(3);
                fecha_documento = rs.getString(4);
                if(moneda.equals(monedaBanco))
                    netoFact = rs.getDouble(1);
                else {
                    Tasa tasa = tasaService.buscarValorTasa(monedaBanco, moneda, monedaBanco, fecha_documento);
                    if (tasa != null)
                        valor_tasa = tasa.getValor_tasa();
                    netoFact = com.tsp.util.Util.redondearByMoneda(rs.getDouble(2)*valor_tasa,monedaBanco);
                }
            }
        }}catch(Exception ex) {
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (psttm  != null){ try{ psttm.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return netoFact;
    }
    /**
     *Metodo para obtener el total del cheque de un propietario
     */
    public double getSaldoFactura(String distrito, String corrida, String beneficiario, String factura,String monedaBanco) throws SQLException{
        Connection con = null;//JJCastro fase2
        double saldoFact = 0;
        PreparedStatement psttm = null;
        ResultSet rs = null;
        String moneda = "";
        String fecha_documento = "";
        double valor_tasa = 0;
        String query = "SQL_SALDO_FACTURA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            psttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            psttm.setString(1, distrito);
            psttm.setString(2, corrida);
            psttm.setString(3, factura);
            psttm.setString(4, beneficiario);
            rs = psttm.executeQuery();
            if (rs.next()){
                moneda          =  rs.getString(3);
                fecha_documento =  rs.getString(4);
                if(moneda.equals(monedaBanco))
                    saldoFact = rs.getDouble(1);
                else {
                    Tasa tasa = tasaService.buscarValorTasa(monedaBanco, moneda, monedaBanco, fecha_documento);
                    if (tasa != null)
                        valor_tasa = tasa.getValor_tasa();
                    saldoFact = com.tsp.util.Util.redondearByMoneda(rs.getDouble(2)*valor_tasa,monedaBanco);
                }
                
            }
        }}catch(Exception ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (psttm  != null){ try{ psttm.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return saldoFact;
    }


    /**
     *Metodo para obtener el bruto de la factura
     */
/**
 * 
 * @param distrito
 * @param beneficiario
 * @param factura
 * @param monedaBanco
 * @return
 * @throws SQLException
 */
    public double getTotalFactura(String distrito, String beneficiario, String factura, String monedaBanco) throws SQLException{
        Connection con = null;//JJCastro fase2
        double totalFact = 0;
        PreparedStatement psttm = null;
        ResultSet rs = null;
        String moneda = "";
        String fecha_documento = "";
        double valor_tasa = 0;
        String query = "SQL_TOTAL_FACTURA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            psttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            psttm.setString(1, distrito);
            psttm.setString(2, beneficiario);
            psttm.setString(3, factura);

            rs = psttm.executeQuery();
            if (rs.next()){
                fecha_documento =  rs.getString("fecha_documento");
                moneda          =  rs.getString("moneda");
                if(moneda.equals(monedaBanco)) {
                    toralFactProveedor+= rs.getDouble(3);
                    totalFact = rs.getDouble(3);
                } else {
                    Tasa tasa = tasaService.buscarValorTasa(monedaBanco, moneda, monedaBanco, fecha_documento);
                    if (tasa != null)
                        valor_tasa = tasa.getValor_tasa();
                    totalFact = com.tsp.util.Util.redondearByMoneda(rs.getDouble(3)*valor_tasa,monedaBanco);
                    toralFactProveedor+=totalFact;
                }
                
            }
        }}catch(Exception ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (psttm  != null){ try{ psttm.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return totalFact;
    }
    
    
    
    /**
     * Metodo listaCorridaBanco , Metodo para buscar las corridas de un banco sucursal
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @throws  : SQLException
     * @version : 1.0
     */
    public List listaCorridaBanco(String Dstrct, String banco, String sucursal,String aprobadas,String pagadas) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        List lista = new LinkedList();
        String sql = "";
        try{
            sql = this.obtenerSQL("SQL_BACO_CORRIDA");
            
            if(pagadas.equals("S")){
                sql = sql.replaceAll("#CHEQUE#","AND cheque !='' AND pago !='0099-01-01 00:00:00'").replaceAll("#PAGO#","");
                sql  = sql.replaceAll("#BENEFICIARIO#", filtroProveedor);
            }else{
                sql = sql.replaceAll("#CHEQUE#","AND cheque =''");
                sql  = sql.replaceAll("#BENEFICIARIO#", filtroProveedor);
                if(aprobadas.equals("ok"))
                    sql = sql.replaceAll("#PAGO#","AND pago !='0099-01-01 00:00:00'");
                else
                    sql = sql.replaceAll("#PAGO#","");
            }
            
            con = this.conectarJNDI("SQL_BACO_CORRIDA");//JJCastro fase2
            st = con.prepareStatement(sql);
            
            st.setString(1, Dstrct);
            st.setString(2, banco);
            st.setString(3, sucursal);

            rs = st.executeQuery();
            while (rs.next()) {
                Corrida corrida = new Corrida();
                corrida.setBanco   (rs.getString("banco")   );
                corrida.setSucursal(rs.getString("sucursal"));
                corrida.setCorrida(rs.getString("corrida") );
                lista.add(corrida);
                corrida = null;//Liberar Espacio JJCastro
            }
            
            
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA SQL_BACO_CORRIDA" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
/**
 * 
 * @param dstrct
 * @param banco
 * @param sucursal
 * @param corrida
 * @param aprobados
 * @param pagados
 * @return
 * @throws SQLException
 */
    public Vector obtenerCorridasXLS(String dstrct, String banco, String sucursal, String corrida, String aprobados, String pagados) throws SQLException{
        Vector vec = new Vector();
        PreparedStatement  psttm   = null;
        Connection         con     = null;
        ResultSet          rs      = null;
        String             query   = "SQL_CORRIDA_XLS";
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            String sql    =   this.obtenerSQL( query ).replaceAll("#PAGADO#", pagados.equals("S")?"!=":"=" );
            if( pagados.equals("S") )
                sql    =   sql.replaceAll("#APROBADO#", "!=" );
            else
                sql    =   sql.replaceAll("#APROBADO#", aprobados.equals("")?"=":"!=" );
                psttm =    con.prepareStatement( sql );
                psttm.setString(1, dstrct);
                psttm.setString(2, corrida);
                psttm.setString(3, banco);
                psttm.setString(4, sucursal);
                rs = psttm.executeQuery();
                while (rs.next()) {
                    Vector corridas = new Vector();
                    corridas.add( rs.getString(1) );//nit
                    corridas.add( rs.getString(2) );//nombre suplier
                    corridas.add( rs.getString(3) );//placa
                    corridas.add( rs.getString(4) );//factura
                    corridas.add( rs.getString(5) );//fecha factura
                    corridas.add( rs.getString(6) );//fecha vencimiento
                    corridas.add( rs.getString(7) );//descripcion factura
                    corridas.add( rs.getString(13));//valor factura bruto
                    corridas.add( rs.getString(14) );//iva
                    corridas.add( rs.getString(17) );//riva
                    corridas.add( rs.getString(16) );//rica
                    corridas.add( rs.getString(15) );//rfte
                    corridas.add( rs.getString(8) );//valor neto
                    corridas.add( rs.getString(9)  );//ot
                    corridas.add( rs.getString(10) );//oc
                    corridas.add( rs.getString(11) );//cheque
                    corridas.add( rs.getString(12) );//cuenta
                    corridas.add( rs.getString(18) );//moneda
                    corridas.add( corrida );
                    vec.add( corridas );
                }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL OBTENER LOS DATOS DE LA CORRIDA EN XLS, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (psttm  != null){ try{ psttm.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return vec;
    }
    
    /**
     *Metodo para obtener el bruto de la factura
     *getNotasCreditosDebitosFactura(detalle,dstrct, factura, supp_to_pay, corrida ,monedaBanco);*/
    public void  getNotasCreditosDebitosFactura(Vector detalle, String distrito, String factura, String beneficiario, String corrida, String monedaBanco) throws SQLException{
        Connection con = null;//JJCastro fase2
        DetalleFactura detalleFact = null;
        double totalFact = 0;
        PreparedStatement psttm = null;
        ResultSet rs = null;
        String moneda = "";
        String fecha_documento = "";
        double valor_tasa = 0;
        String query = "SQL_NC_ND";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            psttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            psttm.setString(1, distrito);
            psttm.setString(2, factura);
            psttm.setString(3, beneficiario);
            rs = psttm.executeQuery();
            //System.out.println("SQL_NC_ND"+psttm);
            while (rs.next()){
                detalleFact = new DetalleFactura();
                fecha_documento =  rs.getString("fecha_documento");
                moneda          =  rs.getString("moneda");
                if (moneda.equals(monedaBanco)){
                    detalleFact = new DetalleFactura();
                    detalleFact.setFacturaNo(rs.getString("tipo"));
                    detalleFact.setItemNo("");
                    detalleFact.setItemDesc(rs.getString("descripcion"));
                    detalleFact.setFacturaNo(rs.getString("tipo"));
                    if (rs.getString("tipo").startsWith("NC")) {
                        detalleFact.setItemValor(rs.getDouble("BRUTO")*-1);
                        detalleFact.setIva(rs.getDouble("IVA")*-1);
                        detalleFact.setRiva(rs.getDouble("RIVA")*-1);
                        detalleFact.setRica(rs.getDouble("RICA")*-1);
                        detalleFact.setRfte(rs.getDouble("RFTE")*-1);
                    }else {
                        detalleFact.setItemValor(rs.getDouble("BRUTO"));
                        detalleFact.setIva(rs.getDouble("IVA"));
                        detalleFact.setRiva(rs.getDouble("RIVA"));
                        detalleFact.setRica(rs.getDouble("RICA"));
                        detalleFact.setRfte(rs.getDouble("RFTE"));
                    }
                    detalleFact.setMoneda(monedaBanco);
                    
                    detalleFact.setNeto(detalleFact.getItemValor()+detalleFact.getIva()+detalleFact.getRiva()+detalleFact.getRica()+detalleFact.getRfte());
                    detalle.add(detalleFact);
                }
                else{
                    Tasa tasa = tasaService.buscarValorTasa(monedaBanco, moneda, monedaBanco, fecha_documento);
                    if (tasa != null)
                        valor_tasa = tasa.getValor_tasa();
                    detalleFact = new DetalleFactura();
                    detalleFact.setFacturaNo(rs.getString("tipo"));
                    detalleFact.setItemNo("");
                    detalleFact.setItemDesc(rs.getString("descripcion"));
                    if (rs.getString("tipo").startsWith("NC")) {
                        detalleFact.setItemValor(com.tsp.util.Util.redondearByMoneda(rs.getDouble("BRUTO")*valor_tasa,monedaBanco)*-1);
                        detalleFact.setIva(com.tsp.util.Util.redondearByMoneda(rs.getDouble("IVA")*valor_tasa,monedaBanco)*-1);
                        detalleFact.setRiva(com.tsp.util.Util.redondearByMoneda(rs.getDouble("RIVA")*valor_tasa,monedaBanco)*-1);
                        detalleFact.setRica(com.tsp.util.Util.redondearByMoneda(rs.getDouble("RICA")*valor_tasa,monedaBanco)*-1);
                        detalleFact.setRfte(com.tsp.util.Util.redondearByMoneda(rs.getDouble("RFTE")*valor_tasa,monedaBanco)*-1);
                    } else {
                        detalleFact.setItemValor(com.tsp.util.Util.redondearByMoneda(rs.getDouble("BRUTO")*valor_tasa,monedaBanco));
                        detalleFact.setIva(com.tsp.util.Util.redondearByMoneda(rs.getDouble("IVA")*valor_tasa,monedaBanco));
                        detalleFact.setRiva(com.tsp.util.Util.redondearByMoneda(rs.getDouble("RIVA")*valor_tasa,monedaBanco));
                        detalleFact.setRica(com.tsp.util.Util.redondearByMoneda(rs.getDouble("RICA")*valor_tasa,monedaBanco));
                        detalleFact.setRfte(com.tsp.util.Util.redondearByMoneda(rs.getDouble("RFTE")*valor_tasa,monedaBanco));
                    }
                    detalleFact.setMoneda(monedaBanco);                    
                    detalleFact.setNeto((detalleFact.getItemValor()+detalleFact.getIva()+detalleFact.getRiva()+detalleFact.getRica()+detalleFact.getRfte()));
                    detalle.add(detalleFact);
                }
            }
        }}catch(Exception ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (psttm  != null){ try{ psttm.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     *Metodo para obtener el bruto de la factura
     */
    public void  getTotalImpuestos(Vector detalle, String distrito, String beneficiario, String factura, String corrida, String monedaBanco) throws SQLException{
        Connection con = null;//JJCastro fase2
        DetalleFactura detalleFact = null;
        double totalFact = 0;
        PreparedStatement psttm = null;
        ResultSet rs = null;
        String moneda = "";
        String fecha_documento = "";
        double valor_tasa = 0;
        String query = "SQL_TOTAL_IMP_FACTURA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            psttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            psttm.setString(1, distrito);
            psttm.setString(2, beneficiario);
            psttm.setString(3, factura);
            psttm.setString(4, distrito);
            psttm.setString(5, factura);
            psttm.setString(6, beneficiario);
            rs = psttm.executeQuery();
            while (rs.next()){
                detalleFact = new DetalleFactura();
                fecha_documento =  rs.getString("fecha_documento");
                moneda          =  rs.getString("moneda");
                if (moneda.equals(monedaBanco)){
                    detalleFact = new DetalleFactura();
                    detalleFact.setFacturaNo("TOTALES FAC");
                    detalleFact.setItemNo("");
                    detalleFact.setItemDesc("");
                    detalleFact.setItemValor(rs.getDouble("total"));
                    detalleFact.setMoneda(monedaBanco);                    ;
                    detalleFact.setIva(rs.getDouble("IVA"));
                    detalleFact.setRiva(rs.getDouble("RIVA"));
                    detalleFact.setRica(rs.getDouble("RICA"));
                    detalleFact.setRfte(rs.getDouble("RFTE"));
                    detalle.add(detalleFact);
                }
                else{
                    Tasa tasa = tasaService.buscarValorTasa(monedaBanco, moneda, monedaBanco, fecha_documento);
                    if (tasa != null)
                        valor_tasa = tasa.getValor_tasa();
                    detalleFact = new DetalleFactura();
                    detalleFact.setFacturaNo("TOTALES FAC");
                    detalleFact.setItemNo("");
                    detalleFact.setItemDesc("");
                    detalleFact.setItemValor(com.tsp.util.Util.redondearByMoneda(rs.getDouble("total")*valor_tasa,monedaBanco));
                    detalleFact.setMoneda(monedaBanco);
                    detalleFact.setIva(com.tsp.util.Util.redondearByMoneda(rs.getDouble("IVA")*valor_tasa,monedaBanco));
                    detalleFact.setRiva(com.tsp.util.Util.redondearByMoneda(rs.getDouble("RIVA")*valor_tasa,monedaBanco));
                    detalleFact.setRica(com.tsp.util.Util.redondearByMoneda(rs.getDouble("RICA")*valor_tasa,monedaBanco));
                    detalleFact.setRfte(com.tsp.util.Util.redondearByMoneda(rs.getDouble("RFTE")*valor_tasa,monedaBanco));
                    //detalleFact.setNeto((detalleFact.getItemValor()+detalleFact.getIva()+detalleFact.getRiva()+detalleFact.getRica()+detalleFact.getRfte()));
                    detalle.add(detalleFact);
                }
            }
        }}catch(Exception ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (psttm  != null){ try{ psttm.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     *Metodo para obtener el total del cheque de un propietario
     */
    public double getTotalBanco(String distrito, String banco, String cuenta, String corrida, String monedaBanco,String aprobadas,String pagadas) throws SQLException{
        PreparedStatement psttm = null;
        ResultSet rs = null;
        String moneda = "";
        String fecha_documento = "";
        double total = 0;
        double valor_tasa = 0;
        Connection con = null;
        String query = "SQL_TOTAL_BANCO";//JJCastro fase2
        try {
            String sql = this.obtenerSQL("SQL_TOTAL_BANCO");
            con = this.conectarJNDI(query);//JJCastro fase2
            
            if(pagadas.equals("S")){
                sql = sql.replaceAll("#CHEQUE#","AND c.cheque !='' AND c.pago !='0099-01-01 00:00:00'").replaceAll("#PAGO#","");
                sql  = sql.replaceAll("#BENEFICIARIO#", filtroProveedor);
            }else{
                sql = sql.replaceAll("#CHEQUE#","AND c.cheque =''");
                sql  = sql.replaceAll("#BENEFICIARIO#", filtroProveedor);
                if(aprobadas.equals("ok"))
                    sql = sql.replaceAll("#PAGO#","AND c.pago !='0099-01-01 00:00:00'");
                else
                    sql = sql.replaceAll("#PAGO#","");
            }
            
            psttm = con.prepareStatement(sql);
            psttm.setString(1, distrito);
            psttm.setString(2, banco);
            psttm.setString(3, cuenta);
            psttm.setString(4, corrida);
            //System.out.println("TOTAL B: "+psttm);
            rs = psttm.executeQuery();
            while (rs.next()){
                moneda = rs.getString(2);
                fecha_documento = rs.getString(3);
                if (moneda.equals(monedaBanco))
                    total += rs.getDouble(1);
                else {
                    Tasa tasa = tasaService.buscarValorTasa(monedaBanco, moneda, monedaBanco, fecha_documento);
                    if (tasa != null)
                        valor_tasa = tasa.getValor_tasa();
                    total += com.tsp.util.Util.redondearByMoneda(rs.getDouble(1)*valor_tasa,monedaBanco);
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (psttm  != null){ try{ psttm.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return total;
    }
    
    /**
     *Metodo para obtener todos los propietarios
     */
    public void searchBeneficiariosCorrida(String distrito, String banco, String cuenta, String corrida, String aprobadas,String pagadas) throws SQLException{
        PreparedStatement psttm = null;
        ResultSet rs = null;
        String moneda = "";
        String fecha_documento = "";
        double total = 0;
        double valor_tasa = 0;
        Connection con = null;
        beneficiarios = new Vector();;
        Propietario p = null;        
        try {
            String sql = this.obtenerSQL("SQL_BENEFICIARIOS");
            con = this.conectarJNDI("SQL_BENEFICIARIOS");//JJCastro fase2
            
            if(pagadas.equals("S")){
                sql = sql.replaceAll("#CHEQUE#","AND cheque !='' AND pago !='0099-01-01 00:00:00'").replaceAll("#PAGO#","");               
            }else{
                sql = sql.replaceAll("#CHEQUE#","AND cheque =''");                
                if(aprobadas.equals("ok"))
                    sql = sql.replaceAll("#PAGO#","AND pago !='0099-01-01 00:00:00'");
                else
                    sql = sql.replaceAll("#PAGO#","");
            }            
            psttm = con.prepareStatement(sql);
            psttm.setString(1, distrito);                      
            psttm.setString(2, corrida);
            psttm.setString(3, banco);  
            psttm.setString(4, cuenta);
            System.out.println("BENEFICIARIOS"+psttm);
            rs = psttm.executeQuery();
            while (rs.next()){
                p = new Propietario();
                p.setCedula(rs.getString("beneficiario"));
                p.setP_nombre(rs.getString("nombre"));
                beneficiarios.addElement(p);
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (psttm  != null){ try{ psttm.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    /**
     * Getter for property propietarios.
     * @return Value of property propietarios.
     */
    public java.lang.String getPropietarios() {
        return propietarios;
    }
    
    /**
     * Setter for property propietarios.
     * @param propietarios New value of property propietarios.
     */
    public void setPropietarios(java.lang.String propietarios) {
        this.propietarios = propietarios;
    }
    
    /**
     * Getter for property beneficiarios.
     * @return Value of property beneficiarios.
     */
    public java.util.Vector getBeneficiarios() {
        return beneficiarios;
    }
    
    /**
     * Setter for property beneficiarios.
     * @param beneficiarios New value of property beneficiarios.
     */
    public void setBeneficiarios(java.util.Vector beneficiarios) {
        this.beneficiarios = beneficiarios;
    }
    
     /**
     * M�todo detalle de un extrato prontopago.
     * @autor   Jpinedo
     * @throws  Exception
     * @version 09-Jun-2011
     **/

      /************************listar  detalle de un extrato prontopago*********************************/
    public List<ExtractoDetalle> Lista_extracto_det(int secuencia,String nit,String documento) throws SQLException {
        List<ExtractoDetalle> lis = new LinkedList<ExtractoDetalle>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_EXTRACTO_DETALLE";
        try {
        con = this.conectarJNDI(query);
        if(con!=null){
         ps = con.prepareStatement(this.obtenerSQL(query));
         ps.setInt(1, secuencia);
         ps.setString(2, nit);
         ps.setString(3, documento);
         rs = ps.executeQuery();
            while (rs.next()) {
                ExtractoDetalle etd = new ExtractoDetalle();
                etd.setDescripcion(rs.getString("descripcion"));
                etd.setConcepto(rs.getString("concepto"));
                etd.setDocumento(rs.getString("documento"));
                etd.setNit(rs.getString("nit"));
                etd.setDescripcion(rs.getString("descripcion"));
                etd.setVlr(rs.getFloat("vlr"));
                etd.setRetefuente(rs.getFloat("retefuente"));
                etd.setReteica(rs.getFloat("reteica"));
                etd.setImpuestos(rs.getFloat("impuestos"));
                etd.setVlr_pp_item(rs.getFloat("vlr_pp_item"));
                etd.setVlr_ppa_item(rs.getFloat("vlr_ppa_item"));
                etd.setPlaca(rs.getString("placa"));


                lis.add(etd);
            }


        }
        }
        catch(SQLException e)
        {
             System.out.print("<---->"+e.getMessage());
            throw new SQLException("ERROR DURANTE Listar " + e.getMessage() + " " + e.getErrorCode());
        }
        finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lis;
    }





            /**
     * M�todo lista una lista de documentos de un detalle de extrato prontopago.
     * @autor   Jpinedo
     * @throws  Exception
     * @version 09-Jun-2011
     **/
      /************************listar  detalle de un extrato prontopago*********************************/
    public List<ExtractoDetalle> Lista_doc_extracto_det(int secuencia,String nit) throws SQLException {
        List<ExtractoDetalle> lis = new LinkedList<ExtractoDetalle>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_DOC_EXTRACTO_DETALLE";
        try {
        con = this.conectarJNDI(query);
        if(con!=null){
         ps = con.prepareStatement(this.obtenerSQL(query));
         ps.setInt(1, secuencia);
         ps.setString(2, nit);
         rs = ps.executeQuery();
            while (rs.next()) {
                ExtractoDetalle etd = new ExtractoDetalle();
                etd.setDocumento(rs.getString("documento"));
                etd.setPlaca(rs.getString("placa"));
                lis.add(etd);
            }


        }
        }
        catch(SQLException e)
        {
             System.out.print("<---->"+e.getMessage());
            throw new SQLException("ERROR DURANTE Listar " + e.getMessage() + " " + e.getErrorCode());
        }
        finally
        {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lis;
    }


            /**
     * M�todo listar factura propietario de un extrato prontopago.
     * @autor   Jpinedo
     * @throws  Exception
     * @version 09-Jun-2011
     **/
        /************************listar  detalle de un extrato prontopago*********************************/
    public List<ExtractoDetalle> Lista_facturas_propietario(int secuencia,String nit) throws SQLException {
        List<ExtractoDetalle> lis = new LinkedList<ExtractoDetalle>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_EXTRACTO_DETALLE_FACTURA_PROPIETARIO";
        try {
        con = this.conectarJNDI(query);
        if(con!=null){
         ps = con.prepareStatement(this.obtenerSQL(query));
         ps.setInt(1, secuencia);
         ps.setString(2, nit);
         rs = ps.executeQuery();
            while (rs.next()) {
                ExtractoDetalle etd = new ExtractoDetalle();
                etd.setDescripcion(rs.getString("descripcion"));
                etd.setConcepto(rs.getString("concepto"));
                etd.setDocumento(rs.getString("documento"));
                etd.setNit(rs.getString("nit"));
                etd.setDescripcion(rs.getString("descripcion"));
                etd.setVlr(rs.getFloat("vlr"));
                etd.setRetefuente(rs.getFloat("retefuente"));
                etd.setReteica(rs.getFloat("reteica"));
                etd.setImpuestos(rs.getFloat("impuestos"));
                etd.setVlr_pp_item(rs.getFloat("vlr_pp_item"));
                etd.setVlr_ppa_item(rs.getFloat("vlr_ppa_item"));
                etd.setPlaca(rs.getString("placa"));


                lis.add(etd);
            }


        }
        }
        catch(SQLException e)
        {
             System.out.print("<---->"+e.getMessage());
            throw new SQLException("ERROR DURANTE Listar " + e.getMessage() + " " + e.getErrorCode());
        }
        finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lis;
    }


    
}

