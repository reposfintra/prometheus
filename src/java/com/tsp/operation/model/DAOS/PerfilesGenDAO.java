
package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
//import com.tsp.util.connectionpool.PoolManager;


public class PerfilesGenDAO extends MainDAO {
    
    public PerfilesGenDAO() {
        super("PerfilesGenDAO.xml");
    }

/**
 * 
 * @param Perfiles
 * @throws Exception
 */
    public void EliminarRelaciones(String [] Perfiles) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query = "SQL_ELIMINAR_RELACIONES";
        
        //eliminacion de las relaciones existentes de los perfiles seleccionados
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            for (int i=0; i<Perfiles.length;i++){
                st.clearParameters();
                st.setString(1, Perfiles[i]);
                st.executeUpdate();
            }
            }}
        catch(Exception e) {
            e.printStackTrace();
            throw new SQLException("Error en rutina EXECUTE_QUERY [PERFILESGEN DAO]... \n"+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

/**
 * 
 * @param Perfiles
 * @param Tablas
 * @throws Exception
 */
    public void addRelacionPerfilesGen ( String [] Perfiles, String [] Tablas) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query = "SQL_ELIMINAR_RELACIONES";
        //eliminacion de las relaciones existentes de los perfiles seleccionados
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            for (int i=0; i<Perfiles.length;i++){
                st.clearParameters();
                st.setString(1, Perfiles[i]);
                st.executeUpdate();
            }
            
            String tab = "";
            // adicion de los clientes a los soportes
            st = con.prepareStatement(this.obtenerSQL("SQL_INSERTAR_RELACIONES"));
            for (int i=0; i<Perfiles.length;i++){
                tab = "";
                for (int j=0 ; j < Tablas.length ; j++){
                    if (j == 0){
                        tab = tab + Tablas[j];  
                    }else{    
                        tab = tab + "#" + Tablas[j];
                    }
                }
                st.clearParameters();
                st.setString(1, tab);
                st.setString(2, Perfiles[i]);
                st.executeUpdate();   
                
            }
            
        }}
        catch(Exception e) {
            e.printStackTrace();
            throw new SQLException("Error en rutina EXECUTE_QUERY [PERFILESGEN DAO]... \n"+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }



 /**
  * 
  * @return
  * @throws SQLException
  */
    public List listarPerfiles()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_PERFILES";
        List Resultado = new LinkedList();
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                while (rs.next()){
                    Object datos = null;
                    datos = General.load(rs);
                    Resultado.add(datos);
                }
            }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS PERFILES " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    return Resultado;
}


 /**
  * 
  * @return
  * @throws SQLException
  */
    public List listarTablas()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_TABLAS";
        List Resultado = new LinkedList();
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                while (rs.next()){
                    Object datos = null;
                    datos = General.load(rs);
                    Resultado.add(datos);
                }
            }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS TABLAS " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    return Resultado;
}
        
//lista las tablas de un perfil determinado.
/**
 *
 * @return
 * @throws SQLException
 */
public List listarTP()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
       String query = "SQL_LISTAR_TABLASPORPERFIL";
        List Resultado = new LinkedList();
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                while (rs.next()){
                    Object datos = null;
                    datos = General.load(rs);
                    Resultado.add(datos);
                }
            }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS TABLAS ASIGANDAS A UN PERFIL " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    return Resultado;
}    
    
}