/***********************************************
 * Nombre       LiquidacionDAO.java 
 * Autor        ING FERNEL VILLACOB DIAZ
 * Fecha        12/04/206
 * Copyright    Transportes Sanchez Polo S.A.
 **********************************************/



package com.tsp.operation.model.DAOS;



import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;



public class LiquidacionDAO extends MainDAO{
    
    
    
    public LiquidacionDAO() {
        super("LiquidacionDAO.xml");
    }
    
    
    
    
    
    
    
    /**
     * Metodo que busca la equivalencia del impuesto en tabla general
     * @autor fvillacob
     * @throws Exception.
     */
    public String  codigoImp_tablaGen(String impuesto) throws Exception{
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        String            query  = "SQL_CAMBIO_IMPUESTO";
        String            code   = impuesto;
        
        try{
            st = this.crearPreparedStatement(query);
            st.setString(1, impuesto);
            rs = st.executeQuery();
            if (rs.next())
                code = rs.getString("referencia");
                
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close(); 
            this.desconectar(query);
        }
        return code;
    } 
    
    
    
    /**
     * M�todo que obtiene los  movimientos de la planilla en la tabla  movpla
     * @autor   fvillacob
     * @param   numero de la planilla (String)
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    public List getMovimientos(String distrito, String oc) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            query     = "SQL_MOVIMIENTOS_PLANILLA";
        List              lista     = new LinkedList();
        try {    
            
             st= crearPreparedStatement(query);
             st.setString(1, oc  );
             st.setString(2, distrito );
             rs=st.executeQuery();
             while(rs.next()){
                 ItemLiquidacion  item =  loadMovimiento(rs); 
                 lista.add(item);
             }
             
        }
        catch(SQLException e){
            throw new SQLException(" DAO: getMovimientos.-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            desconectar(query);
        }
        return lista;
    }
    
    
    
    
    
    
    /**
     * M�todo que agrega descuentos de la tabla  tbldes  asociados al std
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public List agregarDescuentos_Carbon(String std, String distrito)throws Exception{
          PreparedStatement st          = null;
          ResultSet         rs          = null;
          List              descuentos  = new LinkedList();
          String            query       = "SQL_MOVIMIENTOS_PLANILLA_CARBON";
          
          try { 
              
               if (  existeStd_stdjobsel( distrito, std )  ){
                   
                   st= crearPreparedStatement(query);
                   st.setString(1, std      );
                   st.setString(2, distrito );
                   rs=st.executeQuery();
                   while(rs.next()){
                        ItemLiquidacion  item =  loadMovimiento(rs); 
                        descuentos.add(item);
                   }
              }
               
         } catch(SQLException e){
              throw new SQLException(" DAO: agregarDescuentos_Carbon-->"+ e.getMessage());
         }  
         finally{
             if(st!=null) st.close();
             if(rs!=null) rs.close();
             desconectar(query);
         }   
         return descuentos;
    }
    
    
    
    
    
    
     /**
     * M�todo que carga el objeto  ItemLiquidacion de los datos obtenidos de la consulta
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public ItemLiquidacion  loadMovimiento(ResultSet  rs)throws Exception{
        ItemLiquidacion  item = new ItemLiquidacion();
        try{
            
             item.setConcepto    ( resetNull( rs.getString("concepto") )   );
             item.setDescripcion ( resetNull( rs.getString("descripcion")) );
             item.setValor       (            rs.getDouble("valor")        );
             item.setMoneda      ( resetNull( rs.getString("moneda")    )  );
             item.setAsignador   ( resetNull( rs.getString("asignador") )  );
             item.setIndicador   ( resetNull( rs.getString("indicador") )  );
            
        } catch(SQLException e){
              throw new SQLException(" DAO: loadMovimiento -->"+ e.getMessage());
        } 
        return item;
    }
    
    
    
    
    
    /**
     * M�todo que verifica que exista el std en la tabla stdjobsel
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public boolean existeStd_stdjobsel(String distrito, String std)throws Exception{
          PreparedStatement st        = null;
          ResultSet         rs        = null;
          boolean           estado    = false;
          String            query     = "SQL_BUSCAR_STD_STDJOBSEL";          
          try {            
             
              st= crearPreparedStatement(query);
              st.setString(1, distrito);
              st.setString(2, std);
              rs=st.executeQuery();
              if ( rs.next() )
                  estado = true;
              
          } catch(SQLException e){
              throw new SQLException(" DAO: existeStd_stdjobsel.-->"+ e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             if(rs!=null) rs.close();
             desconectar(query);
          }      
          return estado;
    }
    
    
    
    
    
    /**
     * M�todo que obtiene datos del proveedor para calcular los impuestos
     * @autor   fvillacob
     * @param   nit del propietario (String)
     * @throws  Exception
     * @version 1.0.
     **/
    public Hashtable getDatosPlanilla(String distrito, String oc) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            query     = "SQL_DATOS_PLANILLA";
        Hashtable         datos     = null;
        try {            
            st= crearPreparedStatement(query);
            st.setString(1, distrito);
            st.setString(2, oc      );
            rs=st.executeQuery();
            if ( rs.next() ){                
                datos  = new Hashtable();                
                datos.put("valor",        resetValorNull( rs.getString("valoroc")       )  );
                datos.put("moneda",       resetNull     ( rs.getString("monedaoc")      )  );
                datos.put("propietario",  resetNull     ( rs.getString("propietario")   )  );
                datos.put("valorUnitario",resetValorNull( rs.getString("unidad_costo")  )  );   // Para calcular el valor si la planilla esta cumplida 
                datos.put("estado",       resetNull     ( rs.getString("estado")        )  );   // Para determinar si esta cumplida
                datos.put("origen",       resetNull     ( rs.getString("oripla")        )  );   // Para el retefuente
                datos.put("base",         resetNull     ( rs.getString("base")          )  );   // Para lo de carbon
                datos.put("placa",        resetNull     ( rs.getString("placa")         )  );   
                datos.put("pais_placa",   resetNull     ( rs.getString("pais_placa")    )  );   
                
                
                
            }
        }
        catch(SQLException e){
            throw new SQLException(" DAO: getDatosPlanilla.-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            desconectar(query);
        }
        return datos;
    }
    
    
    
    
    
    /**
     * M�todo que obtiene datos del proveedor para calcular los impuestos
     * @autor   fvillacob
     * @param   nit del propietario (String)
     * @throws  Exception
     * @version 1.0.
     **/
    public Hashtable getDatosPropietario( String distrito, String nit) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            query     = "SQL_DATOS_PROPIETARIO";
        Hashtable         proveedor = new  Hashtable();
        try {            
            st= crearPreparedStatement(query);
            st.setString(1, distrito );
            st.setString(2, nit      );
            rs=st.executeQuery();
            if ( rs.next() ){             
                proveedor.put("distrito",            resetNull( rs.getString("distrito")           )  );
                proveedor.put("retefuente",          resetNull( rs.getString("retefuente")         )  );
                proveedor.put("reteica",             resetNull( rs.getString("reteica")            )  );
                proveedor.put("agente_retenedor",    resetNull( rs.getString("agente_retenedor")   )  );
                proveedor.put("banco",               resetNull( rs.getString("banco")              )  );
                proveedor.put("sucursal",            resetNull( rs.getString("sucursal")           )  );
            }
        }
        catch(SQLException e){
            throw new SQLException(" DAO: getDatosPropietario.-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            desconectar(query);
        }
        return proveedor;
    }
    
    
    
    
    
    /**
     * M�todo que obtiene la agencia  relacionada de un codigo de ciudad, en este caso la del origen de la planilla
     * @autor   fvillacob
     * @param   String codigo
     * @throws  Exception
     * @version 1.0.
     **/
    public Hashtable getAgenciaRelacionada(String codigo) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            query     = "SQL_AGENCIA_RELACIONADA";
        Hashtable         agencia   = new  Hashtable();
        try {            
            st= crearPreparedStatement(query);
            st.setString(1, codigo );
            rs=st.executeQuery();
            if ( rs.next() ){
                agencia.put("agencia",   resetNull( rs.getString("asociada")  )  );
                agencia.put("rica",      resetNull( rs.getString("codica")    )  );
            }
        }
        catch(SQLException e){
            throw new SQLException(" DAO: getAgenciaRelacionada.-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            desconectar(query);
        }
        return agencia;
    }
    
    
    
    
    
    /**
     * M�todo que obtiene la monada del banco y sucursal
     * @autor   fvillacob
     * @param   String banco, String sucursal
     * @throws  Exception
     * @version 1.0.
     **/
    public String  getMonedaBanco(String distrito, String banco, String sucursal) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            query     = "SQL_MONEDA_BANCO";
        String            moneda    = "";
        try {            
            st= crearPreparedStatement(query);
            st.setString(1, distrito );
            st.setString(2, banco    );
            st.setString(3, sucursal );
            rs=st.executeQuery();
            if ( rs.next() )
                 moneda  =  resetNull( rs.getString("monedabanco")  );
        }
        catch(SQLException e){
            throw new SQLException(" DAO: getMonedaBanco.-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            desconectar(query);
        }
        return moneda;
    }
    
    
    
    
    
    
    
    /**
     * M�todo que obtiene la cantidad cumplida de una oc de carbon
     * @autor   fvillacob
     * @param   String distrito, String oc
     * @throws  Exception
     * @version 1.0.
     **/
    public double  getCantidadCumplidaCarbon(String distrito, String oc) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            query     = "SQL_CANTIDAD_CUMPLIDA_CARBON";
        double            cantidad  = 1;
        try {            
            st= crearPreparedStatement(query);
            st.setString(1, distrito    );
            st.setString(2, oc          );
            rs=st.executeQuery();
            if ( rs.next() )
                 cantidad  =  rs.getDouble("cantidad");
        }
        catch(SQLException e){
            throw new SQLException(" DAO: getCantidadCumplidaCarbon.-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            desconectar(query);
        }
        return cantidad;
    }
    
    
    
    
    
    /**
     * M�todo que obtiene la cantidad cumplida de una oc en la tabla cumplido
     * @autor   fvillacob
     * @param   String distrito, String oc
     * @throws  Exception
     * @version 1.0.
     **/
    public double  getCantidadCumplida(String distrito, String oc) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            query     = "SQL_CANTIDAD_CUMPLIDA";
        double            cantidad  = 1;
        try {            
            st= crearPreparedStatement(query);
            st.setString(1, distrito    );
            st.setString(2, oc          );
            rs=st.executeQuery();
            if ( rs.next() )
                 cantidad  =  rs.getDouble("cantidad");
        }
        catch(SQLException e){
            throw new SQLException(" DAO: getCantidadCumplida.-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            desconectar(query);
        }
        return cantidad;
    }
    
    
    
    
    
    
    
    /**
     * M�todo que Devuelve el valor del impuesto
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public Hashtable getImpuesto(String distrito, String codeImpuesto, String tipo) throws Exception{
        
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            query     = "SQL_IMPUESTO";
        Hashtable         impuesto  = new  Hashtable(); 
        try{
            
            st= crearPreparedStatement(query);
            st.setString(1, distrito       );
            st.setString(2, codeImpuesto   );
            st.setString(3, tipo           );
            rs=st.executeQuery();
            if ( rs.next() ){             
                 impuesto.put("codigo",      rs.getString("codigo") );
                 impuesto.put("tipo",        rs.getString("tipo")   );
                 impuesto.put("porcentaje",  rs.getString("porcentaje") );
            }
            
        }catch(SQLException e){
            throw new SQLException(" DAO: No se pudo obtener el impuesto-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            desconectar(query);
        }
        return impuesto;
    }
    
    
    
    
    
    /**
     * M�todo obtiene el porcentaje  de anticipo  para la planilla medisnte el stj
     * @autor   fvillacob  12/04/2006 
     * @param   String distrito, String std
     * @throws  Exception
     * @version 1.0.
     **/
    public double getPorcentajeAnticipo(String distrito, String std) throws Exception{
        PreparedStatement st         = null;
        ResultSet         rs         = null;
        String            query      = "SQL_PORCENTAJE_ANTICIPO_PLANILLA";
        double            porcentaje = 0;        
        try {
            
            st= crearPreparedStatement(query);
            st.setString(1, distrito);
            st.setString(2, std     );
            rs=st.executeQuery();
            if ( rs.next() )
                 porcentaje = rs.getDouble("porcentaje_maximo_anticipo");
            
        }
        catch(SQLException e){
            throw new SQLException(" DAO: getPorcentajeAnticipo .-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            desconectar(query);
        }
        return porcentaje;
    }
    
    
    
    
    
    /**
     * M�todo obtiene el estandar de la planilla
     * @autor   fvillacob  12/04/2006 
     * @throws  Exception
     * @version 1.0.
     **/
    public String getStandar(String distrito, String planilla) throws Exception{
        PreparedStatement st         = null;
        ResultSet         rs         = null;
        String            query      = "SQL_STANDAR";
        String            std        = "";       
        try {
            
            st= crearPreparedStatement(query);
            st.setString(1, distrito );
            st.setString(2, planilla );
            rs=st.executeQuery();
            if( rs.next() ){
                std = resetNull ( rs.getString("std_job_no") );
            }
            
        }
        catch(SQLException e){
            throw new SQLException(" DAO: getStandar .-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            desconectar(query);
        }
        return std;
    }
    
    
    
    
    
    /**
     * M�todo obtiene la tasa de conversi�n de una moneda a otra
     * @autor   fvillacob  17/04/2006 
     * @throws  Exception
     * @version 1.0.
     **/
    public double getTasa(String distrito, String moneda1, String moneda2) throws Exception{
        PreparedStatement st         = null;
        ResultSet         rs         = null;
        String            query      = "SQL_TASA";
        double            tasa       = 1;      
        try {
            
            st= crearPreparedStatement(query);
            st.setString(1, distrito );
            st.setString(2, moneda1  );
            st.setString(3, moneda2  );
            rs=st.executeQuery();
            if( rs.next() ){
                tasa = rs.getDouble("tasa");
            }
            
        }
        catch(SQLException e){
            throw new SQLException(" DAO: getTasa .-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            desconectar(query);
        }
        return tasa;
    }
    
    
    
    
    
    
    public String resetNull(String val){
        if(val==null)
            val="";
        return val;
    }
    
    
    public String resetValorNull(String val){
        if(val==null)
            val="0";
        return val;
    }
    
    
    
    
    
    
}
