    /**
    * Autor : Ing. Roberto Rocha P..
    * Date  : 31Enero
    * Copyrigth Notice : Fintravalores S.A. S.A
    * Version 1.0
    -->
    <%--
    -@(#)
    --Descripcion : DAO que maneja los avales de Fenalco
    **/

    package com.tsp.operation.model.DAOS;

    import java.io.*;
    import java.sql.*;
    import java.util.*;
    import com.tsp.util.*;
    import com.tsp.operation.model.beans.*;
    import java.sql.Connection;
    import java.sql.Statement;
    import java.sql.ResultSet;
    import java.sql.DriverManager;
    import java.lang.*;
    import com.tsp.operation.model.TransaccionService;


    public class FacturaObservacionDAO extends MainDAO  {

        private BeanGeneral bg=new BeanGeneral();
        private Vector Datos;
        
        private static final String SQL_INSERT="SQL_INSERT";//Nombre asociado en el XML a la insercion del las observaciones /*javier en 20080610*/
        private static final String SQL_UPDATE_FACT="SQL_UPDATE_FACT";//Nombre asociado en el XML a la actualizacion de la factura /*javier en 20080610*/
        private static final String SQL_SELECT_FACTURAS_CLIENTES = "SQL_SELECT_FACTURAS_CLIENTES";//Nombre asociado en el XML que consulta las facturas de un cliente /*javier en 20080610*/
        
        private String[] noFacturas=null;//Array que contendra el numero de facturas de un cliente /*javier en 20080610*/

        
        public FacturaObservacionDAO() {
                super("FacturaObservacionDAO.xml");
        }
        public FacturaObservacionDAO(String dataBaseName) {
                super("FacturaObservacionDAO.xml", dataBaseName);
        }
        
        /*modificado por javier en 20080610*/
        /**
         * 
         * @param bg
         * @return
         * @throws SQLException
         */
        public String InsertObs(BeanGeneral bg)throws SQLException{
            Connection con = null;
            PreparedStatement st = null;
            PreparedStatement ps = null;
            String men="";
            String query = "SQL_INSERT";
            try{
                con = this.conectarJNDI(query);
                if (con != null) {
               st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,  bg.getValor_01());
                st.setString(2,  bg.getValor_02());
                st.setString(3,  bg.getValor_03());
                st.setString(4,  bg.getValor_04());
                //System.out.println("Query "+st.toString());
                st.executeUpdate();
                st.clearParameters();
                ////////
                ps = con.prepareStatement(this.obtenerSQL("SQL_UPDATE_FACT"));//JJCastro fase2
                 ps.setString(1,  bg.getValor_01());
                ps.executeUpdate();
                ps.clearParameters();
                
                bg=null;
                men = "EXITO INGRESANDO OBSERVACION";
             
            }}catch(SQLException e){
                throw new SQLException("ERROR INGRESANDO OBSERVACION " + e.getMessage()) ;        
            }finally{
            if (con != null){con.setAutoCommit(true);}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
             if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
           }
            return men;
        }



/**
 *
 * @param a
 * @return
 * @throws Exception
 */
       public Vector ObsxFact(String a ) throws Exception{
           Connection con = null;
           PreparedStatement ps = null;
            ResultSet rs = null;
            String  query = "SQL_SELECT";
            Datos = new Vector();
            BeanGeneral bgen;
            try{
                con = this.conectarJNDI(query);
                if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, a);
                rs = ps.executeQuery();
                while (rs.next())
                {
                    bgen=new BeanGeneral();
                    bgen.setValor_01(rs.getString(1));
                    bgen.setValor_02(rs.getString(2));
                    bgen.setValor_03(rs.getString(3));
                    bgen.setValor_04(rs.getString(4));
                    Datos.add(bgen);
                }
            }}
            catch (Exception ex)
            {
                ex.printStackTrace();
                throw new Exception(ex.getMessage());
            }finally{
            if (con != null){con.setAutoCommit(true);}
            if (ps  != null){ try{ ps.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
             if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
           }
            return Datos;
     }
       
    /*
    *Consulta los nros de factura de un cliente
    *@author Javier Martinez jemartinez en 20080610
    */       
    private String[] getNoFacturaCliente(String codigo_cliente) throws Exception{
       Connection con = null;
       PreparedStatement ps = null;
       ResultSet result=null;//Obtendra el resultado de la consulta
       String query = "SQL_SELECT_FACTURAS_CLIENTES";//JJCastro fase2
       try{
           //Prepara la consulta que obtiene los codigos de factura de un determinado cliente
            con = this.conectarJNDI(query);
           if (con != null) {
           ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
           ps.setString(1,codigo_cliente);
           result = ps.executeQuery();//Ejecuta la consulta de los nros de factura de los clientes

            result.last();
            int numMax = result.getRow();//Nro de filas en la consulta
            result.beforeFirst();
            noFacturas = new String[numMax];//Inicializa el array contendra los nros de facturas
            int i=0;
           while(result.next()){
                noFacturas[i]=result.getString(1);
                i++;
            }
            //Hasta este punto se tienen los numeros de facturas de un cliente
       }}catch(Exception e){
           throw new Exception(e.toString());
       }finally{
            if (con != null){con.setAutoCommit(true);}
            if (ps  != null){ try{ ps.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
             if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
           }
       return noFacturas;//Retorna el array con los numeros de factura de un cliente
    }

       /**
    * Inserta observaciones generales en las facturas de un cliente
    * @return el mensaje de exito o fracaso del proceso 
    */
    /*javier en 20080610*/
    public String insertObsAll(BeanGeneral bg) throws Exception{
        Connection con = null;
       PreparedStatement insercion = null;
       PreparedStatement actualizacion = null;
       String mensajeOperacion="";
       String query = "SQL_INSERT" ;//JJCastro fase2
       String query2 = "SQL_UPDATE_FACT";//JJCastro fase2

       try{
           con = this.conectarJNDI(query);
           if (con != null) {
            insercion  = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            actualizacion  = con.prepareStatement(this.obtenerSQL(query2));//JJCastro fase2

           String[] noFactura = getNoFacturaCliente(bg.getValor_05());//noFactura contiene la lista de nros de factura de los clientes

           int tam = noFactura.length;//Cantidad de facturas


           //Para cada numero de factura inserta la observacion
           for(int i=0;i<tam;i++){
               //System.out.println("\n InserObsAllFactura: "+noFactura[i]);
                insercion.setString(1,  noFactura[i]);//No factura
                insercion.setString(2,  bg.getValor_02());//Observacion
                insercion.setString(3,  bg.getValor_03());//Usuario
                insercion.setString(4,  bg.getValor_04());//Usuario
                insercion.executeUpdate();//Inserta en la base de datos
                insercion.clearParameters();//Limpia el PreparedStatement

                actualizacion.setString(1, noFactura[i]);//No factura
                actualizacion.executeUpdate();//actualiza la tabla en la base de datos
                actualizacion.clearParameters();//Limpia el PreparedStatement

           }

           bg=null;
           mensajeOperacion="EXITO DURANTE EL INGRESO DE OBSERVACIONES";

     }}catch(Exception e){
           throw new Exception("ERROR DURANTE EL INGRESO DE OBSERVACIONES " + e.getMessage()); 
     }finally{
            if (insercion  != null){ try{ insercion.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (actualizacion  != null){ try{ actualizacion.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
             if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
           }
     return mensajeOperacion;
    }

       
}