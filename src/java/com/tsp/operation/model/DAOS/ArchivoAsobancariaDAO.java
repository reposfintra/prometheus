/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.google.gson.JsonObject;
import com.tsp.operation.model.MenuOpcionesModulos;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.RecaudoAsobancaria;
import com.tsp.operation.model.beans.RecaudoAsobancariaDetalle;
import com.tsp.operation.model.beans.RecaudoAsobancariaDiccionario;
import com.tsp.operation.model.beans.Usuario;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author egonzalez
 */
public interface ArchivoAsobancariaDAO {
       
     /***   
     * @return 
     * @throws java.sql.SQLException 
     */
    public ArrayList<RecaudoAsobancariaDiccionario> cargarDiccionario() throws SQLException;
    
    /***     
     * @param listaCabecera
     * @param listDetalle
     * @param usuario
     * @param empresa
     * @return 
     * @throws java.sql.SQLException 
     */
    public int insertarRecaudos(RecaudoAsobancaria listaCabecera, ArrayList listDetalle, String usuario, String empresa) throws SQLException;   
    
     /*** 
     * @param idRecaudo
     * @param usuario
     * @return 
     * @throws java.sql.SQLException 
     */
    public String procesarRecaudosDetalle(int idRecaudo, String usuario) throws SQLException; 
    
      /**
     * 
     * @param idRecaudo   
     * @return
     * @throws SQLException
     */
    public ArrayList<RecaudoAsobancaria> buscarCabeceraRecaudo(int idRecaudo) throws SQLException;
    
     /**
     * 
     * @param fechaini
     * @param fechafin
     * @param entidadRecaudadora
     * @param referencia
     * @param empresa
     * @return
     * @throws SQLException
     */
    public ArrayList<RecaudoAsobancaria> buscarCabeceraRecaudo(String fechaini, String fechafin, String entidadRecaudadora, String referencia, String empresa) throws SQLException;

     /**
     * 
     * @param idRecaudo
     * @param tipoAsobancaria
     * @return
     * @throws SQLException
     */
    public ArrayList<RecaudoAsobancariaDetalle> buscarDetalleRecaudo(int idRecaudo, String tipoAsobancaria) throws SQLException;
    
     /***
     * @param usuario
     * @param distrito
     * @return 
     * @throws java.sql.SQLException 
     */
    public ArrayList<MenuOpcionesModulos> cargarMenuRecaudo(String usuario, String distrito) throws SQLException;
    
      /***
     * @param distrito
     * @return 
     * @throws java.sql.SQLException 
     */
    public String cargarCboEntidadesRecaudo(String distrito) throws SQLException;
    
     /**
     * 
     * @param texto
     * @return
     * @throws SQLException
     */
    public String obtenerEstadoRecaudo(String texto) throws SQLException;
    
     /**
     * 
     * @param total_registros
     * @param valor_total
     * @param cod_entidad
     * @param fecha_archivo
     * @param distrito
     * @return
     * @throws SQLException
     */
    public boolean ingresadoArchivoRecaudo(int total_registros,double valor_total, int cod_entidad, String fecha_archivo, String distrito) throws SQLException;
    
     /**
     * 
     * @param cod_entidad
     * @return
     * @throws SQLException
     */
    public boolean existeCodEntidadRecaudo(int cod_entidad) throws SQLException;
    
      /**
     * 
     * @param idDetRecaudo   
     * @return
     * @throws SQLException
     */
    public ArrayList<BeanGeneral> buscarInfoPagoExtracto(int idDetRecaudo) throws SQLException;
    
     /**
     * 
     * @param numIngreso 
     * @return
     * @throws SQLException
     */
    public ArrayList<BeanGeneral> buscarDetalleIngreso(String numIngreso) throws SQLException;
    
    public String obtenerCausalDevolucion(String idCausal) throws SQLException;
      
    public ArrayList<String> cargarSucursales(int cod_entidad);
    
    /**
     * *
     * @param idEmpresa
     * @return @throws java.sql.SQLException
     */
    public ArrayList<BeanGeneral> cargarEntidadesRecaudo(String idEmpresa) throws SQLException;
    
    public boolean existeEntidadRecaudo(String empresa, int codigo) throws SQLException;
    
    public String guardarEntidadRecaudadora(String empresa,int codigo,String descripcion,String nit,String direccion,String telefono,String email,String ciudad,String cuenta,String is_bank,String pago_automatico,String usuario, String reg_status);
    
    public String actualizarEntidadRecaudadora(String empresa,int codigo,String descripcion,String nit,String direccion,String telefono,String email,String ciudad,String cuenta,String is_bank,String pago_automatico,String usuario, String reg_status);
    
    public String cambiarEstadoEntidadRecaudadora(int codEntidad, String estado);
    
     /**
     *
     * @return
     */
    public String cargarPais();
    
     /**
     *
     * @param codpais
     * @return
     */
    public String cargarDepartamento(String codpais);

    /**
     *
     * @param coddpt
     * @return
     */
    public String cargarCiudad(String coddpt);

    /**
     *
     * @return
     */
    public String cargarComisionEntidadesRecaudo();

    /**
     *
     * @param cod_entidad
     * @param valor
     * @param usuario
     * @return
     */
    public String actualizarComisionEntidadRecaudo(int cod_entidad,String codigo_canal, double valor_comision, double porcentaje_iva, double valor_total_comision, Usuario usuario);
    
    
}
