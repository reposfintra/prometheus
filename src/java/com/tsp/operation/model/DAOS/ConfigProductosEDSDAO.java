/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.ConfigProductosEDSBeans;
import java.util.ArrayList;

/**
 *
 * @author egonzalez
 */
public interface ConfigProductosEDSDAO {

    public abstract JsonObject consultar_productos(JsonObject info);

    public abstract JsonObject modificar_productos(JsonObject info);

    public abstract JsonObject cargar_combo(JsonObject info);

    public String guardarRangos(JsonObject info);

    public ArrayList<ConfigProductosEDSBeans> listarRangos(String id_config);

    public String guardarHistoricoRangos(JsonObject info);

    public ArrayList<ConfigProductosEDSBeans> listarHistrico(String id_config);
}
