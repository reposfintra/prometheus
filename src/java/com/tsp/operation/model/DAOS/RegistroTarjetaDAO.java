/*
 * RegistroTarjetaDAO.java
 *
 * Created on 4 de septiembre de 2005, 10:06 AM
 */

package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.*;
/**
 *
 * @author  Jm
 */
public class RegistroTarjetaDAO extends MainDAO{
    
    
    /** Creates a new instance of RegistroTarjetaDAO */
    public RegistroTarjetaDAO() {
        super( "RegistroTarjetaDAO.xml" );
    }
    
    /*INSERT */
    public void INSERT(String dstrct, String num_tarjeta, String placa, String cedula, String creation_user, String user_update) throws SQLException {
        Connection con = null;
        PreparedStatement st          = null;
        String query = "SQL_INSERT";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dstrct);
                st.setString(2, num_tarjeta);
                st.setString(3, placa);
                st.setString(4, cedula);
                st.setString(5, creation_user);
                st.setString(6, user_update);
                st.executeUpdate();
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina INSERT [RegistroTarjetaDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    
    /* ANULAR / ACTIVAR */
    /**
     * 
     * @param reg_status
     * @param numtarjeta
     * @param cedula
     * @param fecha_creacion
     * @param usuario
     * @throws SQLException
     */
    public void CESTADO( String reg_status, String numtarjeta, String cedula, String fecha_creacion, String usuario ) throws SQLException {
        Connection con = null;
        PreparedStatement st          = null;
        String query = "SQL_UPDATEEST";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, reg_status);
            st.setString(2, usuario);
            st.setString(3, numtarjeta);
            st.setString(4, cedula);
            st.setString(5, fecha_creacion);
            st.executeUpdate();
            }}
        catch(SQLException e) {
            throw new SQLException("Error en rutina CESTADO [RegistroTarjetaDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    /*BUSCAR*/
    /**
     * 
     * @param num_tarjeta
     * @param cedula
     * @param fecha_creacion
     * @return
     * @throws SQLException
     */
    public RegistroTarjeta SEARCH( String num_tarjeta, String cedula, String fecha_creacion ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        RegistroTarjeta datos = null;
        String query = "SQL_SEARCH";
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, num_tarjeta);
                st.setString(2, cedula);
                st.setString(3, fecha_creacion);
                rs = st.executeQuery();
                while (rs.next()) {
                    datos = RegistroTarjeta.load(rs);
                    break;
                }
            }
        }
        catch(Exception e) {
            throw new SQLException("Error en rutina SEARCH[RegistroTarjetaDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return datos;
    }
    
    
/**
 * 
 * @param num_tarjeta
 * @param cedula
 * @param fecha_creacion
 * @return
 * @throws SQLException
 */
    public boolean BUSCAR( String num_tarjeta, String cedula, String fecha_creacion ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        boolean flag         = false;
        String query = "SQL_SEARCH";
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, num_tarjeta);
            st.setString(2, cedula);
            st.setString(3, fecha_creacion);
            
            rs = st.executeQuery();
            while(rs.next()){
                flag = true;
                break;
            }
            }}
        catch(Exception e) {
            throw new SQLException("Error en rutina BUSCAR[RegistroTarjetaDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return flag;
    }
    
    
/**
 * 
 * @param num_tarjeta
 * @param cedula
 * @param fecha_creacion
 * @param placa
 * @return
 * @throws SQLException
 */
    public boolean buscarAll( String num_tarjeta, String cedula, String fecha_creacion, String placa ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        boolean flag         = false;
        String query = "SQL_SEARCHALL";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, num_tarjeta);
            st.setString(2, cedula);
            st.setString(3, fecha_creacion);
            st.setString(4, placa);
            rs = st.executeQuery();
            while(rs.next()){
                flag = true;
                break;
            }
            }}
        catch(Exception e) {
            throw new SQLException("Error en rutina BUSCAR[RegistroTarjetaDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return flag;
    }
    
/**
 * 
 * @param num_tarjeta
 * @param cedula
 * @param fecha_creacion
 * @throws SQLException
 */
    public void DELETE( String num_tarjeta, String cedula, String fecha_creacion  ) throws SQLException {
        Connection con = null;
        PreparedStatement st          = null;
        String query = "SQL_DELETE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, num_tarjeta);
            st.setString(2, cedula);
            st.setString(3, fecha_creacion);
            st.executeUpdate();
        }}
        catch(SQLException e) {
            throw new SQLException("Error en rutina DELETE[RegistroTarjetaDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
  /**
   * 
   * @return
   * @throws Exception
   */
    public List LIST() throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_LIST";
        List lista           = new LinkedList();
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(RegistroTarjeta.load(rs));
                }
            }
            }}catch(Exception e){
            throw new SQLException("Error en rutina LIST [RegistroTarjetaDAO].... \n"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    /* MODIFICAR */
  /**
   *
   * @param nnum_tarjeta
   * @param nplaca
   * @param ncedula
   * @param ncreation_user
   * @param nuser_update
   * @param num_tarjeta
   * @param cedula
   * @param fecha_creacion
   * @param nfecha
   * @throws SQLException
   */
    public void UPDATE( String nnum_tarjeta, String nplaca, String ncedula, String ncreation_user, String nuser_update, String num_tarjeta, String cedula, String fecha_creacion, String nfecha ) throws SQLException {
        Connection con = null;
        PreparedStatement st          = null;
        String query                  = "SQL_UPDATE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, nnum_tarjeta);
            st.setString(2, nplaca);
            st.setString(3, ncedula);
            st.setString(4, ncreation_user);
            st.setString(5, nuser_update);
            st.setString(6, nfecha);
            st.setString(7, num_tarjeta);
            st.setString(8, cedula);
            st.setString(9, fecha_creacion);
            st.executeUpdate();
        }}
        catch(SQLException e) {
            throw new SQLException("Error en rutina UPDATE[RegistroTarjetaDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


 /**
  * 
  * @param num_tarjeta
  * @return
  * @throws SQLException
  */
    public boolean BUSCARTARJETA( String num_tarjeta ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        boolean flag         = false;
        String query = "SQL_SEARCHTARJETA";
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, num_tarjeta);
            rs = st.executeQuery();
            while(rs.next()){
                flag = true;
                break;
            }
            }}
        catch(Exception e) {
            throw new SQLException("Error en rutina BUSCARTARJETA[RegistroTarjetaDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return flag;
    }
    
  /**
   *
   * @param numtarjeta
   * @param cedula
   * @param placa
   * @param fechacreacion
   * @return
   * @throws Exception
   */
    public List LISTREG( String numtarjeta, String cedula, String placa, String fechacreacion ) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_SEARCH2";
        List lista           = new LinkedList();
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, numtarjeta);
            st.setString(2, cedula);
            st.setString(3, placa);
            st.setString(4, fechacreacion);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(RegistroTarjeta.load(rs));
                }
            }
            }}catch(Exception e){
            throw new SQLException("Error en rutina LIST [RegistroTarjetaDAO].... \n"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
}
