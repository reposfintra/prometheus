package com.tsp.operation.model.DAOS;

import java.sql.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;
import java.util.*;

public class TransitoDAO extends MainDAO {
    private TreeMap cbxCliente;
    private TreeMap AFCliente;
    private LinkedList clientes;
    private BeanGeneral Bean;
    private Vector vector;
    private Vector vectorCod;
    /** Creates a new instance of ClienteDAO */
    public TransitoDAO() {
        super("TransitoDAO.xml");
    }
    
      
    /** Lista los clientes de la tabla cliente */
    public void BusquedaMinifiesto(String FechaI,String FechaF ) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        BeanGeneral Bean = null;
        vector = new Vector();
        Vector vectorDefecha = new Vector();
        BeanGeneral beanG = new BeanGeneral();
        String agRetenedor = "";
        String AutoRetefuente="";
        String Retefuente = "";
        String Estado = "";
        String FechaPago= "";
        String TipoId = "";
        String Dia= "";
        String Mes = "";
        String A�o = "";
        String Fecha = "";
        
        String Dia2= "";
        String Mes2 = "";
        String A�o2 = "";
        String Fecha2 = "";
        
        String Dia3= "";
        String Mes3 = "";
        String A�o3 = "";
        String Fecha3 = "";
        
        String FechaExp= "";
        String FechaVen = "";
        String validafecha = "0099/01/01";
        String cargueDescargue = "";
       try{
            st = this.crearPreparedStatement("SQL_BUSCA_MANIFIESTO");
            int cont = 1;
            st.setString(1, FechaI);
            st.setString(2, FechaF);
            rs = st.executeQuery(); 
             while(rs.next()){
                Bean = new BeanGeneral();
                Bean.setValor_30 ((rs.getString("distrito")!=null)?rs.getString("distrito"):"");
                Bean.setValor_31 ((rs.getString("propietario")!=null)?rs.getString("propietario"):"");
                Bean.setValor_32 ((rs.getString("facturaop")!=null)?rs.getString("facturaop"):"");
                Bean.setValor_33 ((rs.getString("planilla")!=null)?rs.getString("planilla"):"");
                cargueDescargue = this.nombre_cliente(Bean.getValor_33());
                
                
                Bean.setValor_01 ((rs.getString("Nmanifiesto")!=null)?rs.getString("Nmanifiesto"):"");
                FechaExp  = (rs.getString("FecExpedicion")!=null)?rs.getString("FecExpedicion"):"0099/01/01";
                if(FechaExp.equals(validafecha)){
                    FechaExp = "";
                    Bean.setValor_02 (FechaExp);
                }else{
                    A�o = FechaExp.substring(0,4);
                    Mes = FechaExp.substring(5,7);
                    Dia = FechaExp.substring(8,10);
                    Fecha = Dia + "/"+ Mes + "/"+ A�o; 
                    Bean.setValor_02 (Fecha);
                }   
                  
                Bean.setValor_03 ((rs.getString("CiuOrigen")!=null)?rs.getString("CiuOrigen"):"");
                Bean.setValor_04 ((rs.getString("CiuDestino")!=null)?rs.getString("CiuDestino"):"");
                Bean.setValor_05 ((rs.getString("PlacaV")!=null)?rs.getString("PlacaV"):"");
                Bean.setValor_08 ((rs.getString("EstadoMani")!=null)?rs.getString("EstadoMani"):"");
               
                Bean.setValor_07 ((rs.getString("FechaPago")!=null)?rs.getString("FechaPago"):"0099/01/01");
                FechaPago = Bean.getValor_07();
                if(FechaPago.equals(validafecha)){
                    FechaPago = "";
                    Bean.setValor_07("");
                    Bean.setValor_27 ("");
                }else{
                    String fechaven= "";
                    Bean.setValor_07(FechaPago);
                    Estado = Bean.getValor_08();
                    if(Estado.equals("C")){
                      Bean.setValor_08("R");  
                      //FechaPago = Util.fechaFinal(Bean.getValor_07(), 7);
                      this.SQL_RETEFUENTE_FECHA(Bean.getValor_30(), Bean.getValor_31(), Bean.getValor_32());  
                      vectorDefecha = this.getVectorCod();
                      if(vectorDefecha.size()<= 0){
                          fechaven = "0099/01/01";
                          beanG.setValor_02("0099/01/01");
                          beanG.setValor_01("1");
                      }else{
                          beanG= (BeanGeneral) vectorDefecha.elementAt(0);
                          if( beanG.getValor_01().equals("")){
                              beanG.setValor_01("1");
                          }
                          fechaven = beanG.getValor_02();
                        
                      }
                      if (fechaven.equals(validafecha)){
                          fechaven = "";
                          Bean.setValor_27 (fechaven);    
                      }else{
                          
                          A�o2 = fechaven.substring(0,4);
                          Mes2 = fechaven.substring(5,7);
                          Dia2 = fechaven.substring(8,10);
                          Fecha2 = Dia2 + "/"+ Mes2 + "/"+ A�o2; 
                          Bean.setValor_27 (Fecha2);
                     }
                     
                    }
                    else {
                      FechaPago = Util.fechaFinal(Bean.getValor_07(), 15);
                      A�o2 = FechaPago.substring(0,4);
                      Mes2 = FechaPago.substring(5,7);
                      Dia2 = FechaPago.substring(8,10);
                      Fecha2 = Dia2 + "/"+ Mes2 + "/"+ A�o2; 
                      Bean.setValor_27 (Fecha2);
                    }
                }
                    
                 
                    
                Bean.setValor_09 ((rs.getString("TipoIdentificacion")!=null)?rs.getString("TipoIdentificacion"):"");
                Bean.setValor_10 ((rs.getString("Nconductor")!=null)?rs.getString("Nconductor"):"");
                Bean.setValor_11 ((rs.getString("configuracion")!=null)?rs.getString("configuracion"):"");
                Bean.setValor_12 ((rs.getString("pesoTotal")!=null)?rs.getString("pesoTotal"):"");
                Bean.setValor_14 ((rs.getString("PrealizaPagoCargue")!=null)?rs.getString("PrealizaPagoCargue"):"");
                Bean.setValor_15 ((rs.getString("PrealizaPagoDescargue")!=null)?rs.getString("PrealizaPagoDescargue"):"");
                Bean.setValor_16 ((rs.getString("CciudadDane")!=null)?rs.getString("CciudadDane"):"");
                Bean.setValor_17 ((rs.getString("PolizaAseguradora")!=null)?rs.getString("PolizaAseguradora"):"");
                Bean.setValor_18 ((rs.getString("Nidentificacion")!=null)?rs.getString("Nidentificacion"):"");
                FechaVen = (rs.getString("fechaVenpoliza")!=null)?rs.getString("fechaVenpoliza"):"0099/01/01";
                if(FechaVen.equals(validafecha)){
                    FechaVen = "";
                   Bean.setValor_19 (FechaVen);
                }else{
                    A�o3 = FechaVen.substring(0,4);
                    Mes3 = FechaVen.substring(5,7);
                    Dia3 = FechaVen.substring(8,10);
                    Fecha3 = Dia3 + "/"+ Mes3 + "/"+ A�o3; 
                    Bean.setValor_19 (Fecha3);
                }   
                  
                Bean.setValor_20 ((rs.getString("TidentificacionCarga")!=null)?rs.getString("TidentificacionCarga"):"");
                Bean.setValor_21 ((rs.getString("placaremolque")!=null)?rs.getString("placaremolque"):"");
                Bean.setValor_22 ((rs.getString("tipoCarroceria")!=null)?rs.getString("tipoCarroceria"):"");
                Bean.setValor_06 ((rs.getString("ValorViaje")!=null)?rs.getString("ValorViaje"):"");
                /*datos de cargue y descargue*/
                Bean.setValor_34 (cargueDescargue);
                String valorAnticipo = this.anticipo(Bean.getValor_33());
                Bean.setValor_23 ((valorAnticipo!=null)?valorAnticipo:"");
                  
                   
               
                //cuando esta anulado todos los montos son cero
                if(!Estado.equals("A") ){
                    Retefuente = beanG.getValor_01();
                    double retencion = Double.parseDouble(Retefuente);
                    double dividir = retencion / 100;
                    double valorplanilla = Double.parseDouble(Bean.getValor_06());
                    double totalRetencion = valorplanilla * dividir ;
                    String valorTruncar = ""+Util.redondear( totalRetencion,0);
                    int ini = valorTruncar.length() - 2;
                    String valorFinal = valorTruncar.substring(0,ini);
                    Bean.setValor_25 (""+valorFinal);
                    Bean.setValor_26 (""+valorFinal);
                   

                
                }else{
                    Bean.setValor_06 ("0");
                    Bean.setValor_13 ("0");
                    Bean.setValor_25 ("0"); 
                    Bean.setValor_26("0");
                }
           
                this.vector.addElement( Bean );
               cont++;  
             
            }
            //return BeanCliente;
        }
        catch(Exception e){
             e.printStackTrace();
        }
        finally {
            if ( st != null ) { st.close(); }
            this.desconectar("SQL_BUSCA_MANIFIESTO");
        }
        
    }

     public void SQL_RETEFUENTE_FECHA(String distrito, String proveedor, String op ) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        String retefuente = "";
        String fechaven = "";
         BeanGeneral Bean2 = null;
        vectorCod = new Vector();
        try{
            st = this.crearPreparedStatement("SQL_RETEFUENTE_FECHA");
            st.setString(1, distrito);
            st.setString(2, proveedor);
            st.setString(3, op);
             rs = st.executeQuery();
             if(rs.next()){
              Bean2 = new BeanGeneral();
              Bean2.setValor_01((rs.getString("retefuente")!=null)?rs.getString("retefuente"):"");
              Bean2.setValor_02((rs.getString("vencimiento")!=null)?rs.getString("vencimiento"):"0099/01/01");
             
              this.vectorCod.addElement( Bean2 );
            }
        }catch(Exception e) {
            throw new Exception("ERROR AL BUSCAR LA SQL_RETEFUENTE_FECHA [TransitoDAO.xml]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_RETEFUENTE_FECHA");
            
        }
       
    }
      
  
      
      public String nombre_cliente (String planilla ) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        String nombre = "";
           try{
            st = this.crearPreparedStatement("SQL_NOMBRE_CLIENTE");
             st.setString(1, planilla);
               rs = st.executeQuery();
            if(rs.next()){
                nombre = (rs.getString ("nombre")!=null)?rs.getString ("nombre"):"";    
            }
            
        }catch(Exception e) {
            throw new Exception("ERROR AL BUSCAR LA SQL_NOMBRE_CLIENTE [TransitoDAO.xml]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_NOMBRE_CLIENTE");
        }
        return nombre;
    }
      
    public String anticipo (String planilla ) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        String anticipo = "";
           try{
            st = this.crearPreparedStatement("SQL__ANTICIPO");
             st.setString(1, planilla);
               rs = st.executeQuery();
            if(rs.next()){
                anticipo = (rs.getString ("valorAnticipo")!=null)?rs.getString ("valorAnticipo"):"";    
            }
            
        }catch(Exception e) {
            throw new Exception("ERROR AL BUSCAR LA SQL__ANTICIPO [TransitoDAO.xml]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL__ANTICIPO");
        }
        return anticipo;
    }  
      
   
      
      /**
       * Getter for property vector.
       * @return Value of property vector.
       */
      public java.util.Vector getVector() {
          return vector;
      }      
     
      /**
       * Setter for property vector.
       * @param vector New value of property vector.
       */
      public void setVector(java.util.Vector vector) {
          this.vector = vector;
      }
      
      
      
      /*funcion para sacar los datos de vehiculo del transito **/
       public void BusquedaVehiculos(String FechaI,String FechaF ) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        BeanGeneral Bean = null;
        vector = new Vector();
        String agRetenedor = "";
        String AutoRetefuente="";
        String Retefuente = "";
        String Estado = "";
        String FechaPago= "";
        String TipoId = "";
       try{
            st = this.crearPreparedStatement("SQL_TRANSITO_VEHICULO");
            int cont = 1;
            st.setString(1, FechaI);
            st.setString(2, FechaF);
            System.out.println("query vehiculos ->>>>"+st.toString());
            rs = st.executeQuery(); 
            String val = "";
            String Tipo = "";
            while(rs.next()){
                Bean = new BeanGeneral();
                
                            
                Bean.setValor_01 ((rs.getString("PlacaVehiculo")!=null)?rs.getString("PlacaVehiculo"):"");
                Bean.setValor_02 ((rs.getString("MarcaVehiculo")!=null)?rs.getString("MarcaVehiculo"):"");
                Bean.setValor_03 ((rs.getString("modelo")!=null)?rs.getString("modelo"):"");
                Bean.setValor_04 ((rs.getString("LineaVehiculo")!=null)?rs.getString("LineaVehiculo"):"");
                Bean.setValor_05 ((rs.getString("NumeroSerie")!=null)?rs.getString("NumeroSerie"):"");
                Bean.setValor_06 ((rs.getString("color")!=null)?rs.getString("color"):"");
                Bean.setValor_07 ((rs.getString("carroceria")!=null)?rs.getString("carroceria"):"");
                Bean.setValor_08 ((rs.getString("pesoVehiculo")!=null)?rs.getString("pesoVehiculo"):"");
                Bean.setValor_09 ((rs.getString("NregistroNacional")!=null)?rs.getString("NregistroNacional"):"");
                Bean.setValor_10 ((rs.getString("poliza")!=null)?rs.getString("poliza"):"");
                Bean.setValor_11 ((rs.getString("TidentAseguradora")!=null)?rs.getString("TidentAseguradora"):"");
                
                Bean.setValor_12 ((rs.getString("NPropietario")!=null)?rs.getString("NPropietario"):"");
                Bean.setValor_13 ((rs.getString("tenedor")!=null)?rs.getString("tenedor"):"");
                Bean.setValor_14 ((rs.getString("polizaAseguradora")!=null)?rs.getString("polizaAseguradora"):"");
                Bean.setValor_15 ((rs.getString("fechaVencPoliza")!=null)?rs.getString("fechaVencPoliza"):"0099/01/01");
                Tipo= (rs.getString("tipoIdentConductor")!=null)?rs.getString("tipoIdentConductor"):"";
                if(Tipo.equals("CED")){
                     Bean.setValor_16  ("C");
                }else{
                     Bean.setValor_16 ("");
                }
                Bean.setValor_17 ((rs.getString("tipoIdentenedor")!=null)?rs.getString("tipoIdentenedor"):"");
                Bean.setValor_18 (val);
                
                    
               
                this.vector.addElement( Bean );
            }
            //return BeanCliente;
        }
        catch(Exception e){
             e.printStackTrace();
        }
        finally {
            if ( st != null ) { st.close(); }
            this.desconectar("SQL_TRANSITO_VEHICULO");
        }
        
    }
       
       
    /*funcion para sacar los datos de vehiculo del transito **/
       public void BusquedaConductores(String FechaI,String FechaF ) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        BeanGeneral Bean = null;
        vector = new Vector();
        String val = "";
        String TipoI = "";
         
        try{
            st = this.crearPreparedStatement("SQL_TRANSITO_CONDUCTORES");
            int cont = 1;
            st.setString(1, FechaI);
            st.setString(2, FechaF);
            rs = st.executeQuery(); 
            while(rs.next()){
                Bean = new BeanGeneral();
                 TipoI = (rs.getString("tipoIdentificacion")!=null)?rs.getString("tipoIdentificacion"):"";
                 if(TipoI.equals("CED")){
                     Bean.setValor_01  ("C");
                }else{
                     Bean.setValor_01 ("");
                } 
                Bean.setValor_02 ((rs.getString("cedualConductor")!=null)?rs.getString("cedualConductor"):"");
                Bean.setValor_03 ((rs.getString("Papellido")!=null)?rs.getString("Papellido"):"");
                Bean.setValor_04 ((rs.getString("Sapellido")!=null)?rs.getString("Sapellido"):"");
                Bean.setValor_05 ((rs.getString("Pnombre")!=null)?rs.getString("Pnombre"):"");
                Bean.setValor_06 ((rs.getString("Snombre")!=null)?rs.getString("Snombre"):"");
                Bean.setValor_07 ((rs.getString("telefono")!=null)?rs.getString("telefono"):"");
                Bean.setValor_08 ((rs.getString("direccion")!=null)?rs.getString("direccion"):"");
                Bean.setValor_09 ((rs.getString("codigoCiudad")!=null)?rs.getString("codigoCiudad"):"");
                Bean.setValor_10 ((rs.getString("categoria")!=null)?rs.getString("categoria"):"");
                this.vector.addElement( Bean );
            }
            //return BeanCliente;
        }
        catch(Exception e){
             e.printStackTrace();
        }
        finally {
            if ( st != null ) { st.close(); }
            this.desconectar("SQL_TRANSITO_CONDUCTORES");
        }
        
    }   
       
    
    /*FUNCION PARA SACAR TODAS LAS EMPRESAS PARA EL TRASITO*/   
    public void BusquedaEmpresas(String FechaI,String FechaF ) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        BeanGeneral Bean = null;
        vector = new Vector();
        String val = "";
        String TipoI = "";
         
        try{
            st = this.crearPreparedStatement("SQL_TRANSITO_EMPRESAS");
            int cont = 1;
            st.setString(1, FechaI);
            st.setString(2, FechaF);
            rs = st.executeQuery(); 
            while(rs.next()){
                Bean = new BeanGeneral();
                 TipoI = (rs.getString("TipoIdentificacion")!=null)?rs.getString("TipoIdentificacion"):"";
                 if(TipoI.equals("NIT")){
                     Bean.setValor_01  ("N");
                }else{
                     Bean.setValor_01 ("");
                } 
                Bean.setValor_02 ((rs.getString("numEmpresa")!=null)?rs.getString("numEmpresa"):"");
                Bean.setValor_03 ((rs.getString("nombreEmpresa")!=null)?rs.getString("nombreEmpresa"):"");
                Bean.setValor_04 ((rs.getString("telefono")!=null)?rs.getString("telefono"):"");
                Bean.setValor_05 ((rs.getString("direccion")!=null)?rs.getString("direccion"):"");
                Bean.setValor_06 ((rs.getString("codigo_dane")!=null)?rs.getString("codigo_dane"):"");
                Bean.setValor_07 ((rs.getString("CodigoMinisterio")!=null)?rs.getString("CodigoMinisterio"):"");
                this.vector.addElement( Bean );
            }
           
        }
        catch(Exception e){
             e.printStackTrace();
        }
        finally {
            if ( st != null ) { st.close(); }
            this.desconectar("SQL_TRANSITO_EMPRESAS");
        }
        
    }
    
    
     /*FUNCION PARA SACAR TODAS LAS EMPRESAS PARA EL TRASITO*/   
    public void BusquedaMercancias(String FechaI,String FechaF ) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        BeanGeneral Bean = null;
        vector = new Vector();
        String val = "";
        String TipoI = "";
        String remesa = "";
        String Nempaque ="";
        String Nremesa ="";
        String Cproducto ="";
        String Nproducto ="";
        String tipoNombre ="";
        String nombreRemiDes ="";
        String codigoRemiDes ="";
        String empaque = "";
        String valorEmpaque= "";
        String Umedida = "";
        String vacio = "";
         
    try{
            st = this.crearPreparedStatement("SQL_TRANSITO_MERCANCIAS");
            int cont = 1;
            st.setString(1, FechaI);
            st.setString(2, FechaF);
            rs = st.executeQuery();
            System.out.println("QUERY MERCANCIA->>>>>  "+st.toString());
            while(rs.next()){
                Bean = new BeanGeneral();
                Bean.setValor_01 ((rs.getString("Nmanifiesto")!=null)?rs.getString("Nmanifiesto"):"");
                remesa = this.BuscarRemesas(Bean.getValor_01());
                this.BusacarDatosRemesa(remesa);
                Vector datosC = this.getVectorCod();
                Nempaque = "";
                Nremesa = "";
                Cproducto = "";
                Nproducto = "";
                tipoNombre = "";
                nombreRemiDes = "";
                codigoRemiDes = "";
                Bean.setValor_10 ("");
                Bean.setValor_11 ("");
                 
                for (int i = 0; i < datosC.size(); i++){
                    BeanGeneral Bean2 = (BeanGeneral) datosC.elementAt(i);
                    Nempaque = Bean2.getValor_01();
                    Nremesa = Bean2.getValor_02();
                    Cproducto = Bean2.getValor_03();
                    Nproducto = Bean2.getValor_04();
                    tipoNombre = Bean2.getValor_05();
                    nombreRemiDes = Bean2.getValor_06();
                    codigoRemiDes = Bean2.getValor_07();
                     if(tipoNombre.equals("RE")){
                       Bean.setValor_10 (nombreRemiDes);//nombre del remitente
                    }
                    if(tipoNombre.equals("DE")){
                       Bean.setValor_11 (nombreRemiDes);//nombre del destinatario
                    }
                } 
                Bean.setValor_02 (Nremesa);
                Umedida = (rs.getString("CunidadM")!=null)?rs.getString("CunidadM"):"";
                if(Umedida.equals("KGR")){
                    Bean.setValor_03 ("1");//unidad de medida
                    Bean.setValor_04 ("0");//en Galones
                    Bean.setValor_05 ((rs.getString("pesoProducto")!=null)?rs.getString("pesoProducto"):"");//en Kilogramos
                }
                else if(Umedida.equals("GAL")){
                    Bean.setValor_03 ("2");//unidad de medida
                    Bean.setValor_04 ((rs.getString("pesoProducto")!=null)?rs.getString("pesoProducto"):"");//en Galones
                    Bean.setValor_05 ("0");//en Kilogramos
                    
                }else {
                    Bean.setValor_03 ("");//unidad de medida
                    Bean.setValor_04 ("0");//en Galones
                    Bean.setValor_05 ("0");//en Kilogramos
                    
                }
                
                if(Nempaque.equals("PAQ")){
                    valorEmpaque = "0";
                }else if(Nempaque.equals("CAJ")){
                    valorEmpaque = "1";
                }else if(Nempaque.equals("SAC")){
                    valorEmpaque = "3";
                }else if(Nempaque.equals("BUL")){
                    valorEmpaque = "4";
                }else if(Nempaque.equals("TN")){
                    valorEmpaque = "5";
                }else {
                    valorEmpaque = "";
                }
                Bean.setValor_06 (valorEmpaque);
                Bean.setValor_07 ((rs.getString("CNaturaleza")!=null)?rs.getString("CNaturaleza"):"");
                Bean.setValor_08 (Cproducto);
                Bean.setValor_09 (Nproducto);
               
                 Bean.setValor_12 ((rs.getString("CodigoDane")!=null)?rs.getString("CodigoDane"):"");
               
                this.vector.addElement( Bean );
            }
           
        }
        catch(Exception e){
             e.printStackTrace();
        }
        finally {
            if ( st != null ) { st.close(); }
            this.desconectar("SQL_TRANSITO_MERCANCIAS");
        }
        
    }    
    
    
    public String BuscarRemesas(String planilla ) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        String val = "";
        String TipoI = "";
        String remesa = "";
          
        try{
            st = this.crearPreparedStatement("SQL_REMESA");
            st.setString(1, planilla);
            rs = st.executeQuery(); 
             if(rs.next()){
                remesa =(rs.getString("Nremesa")!=null)?rs.getString("Nremesa"):"";
                }
           
        }
        catch(Exception e){
             e.printStackTrace();
        }
        finally {
            if ( st != null ) { st.close(); }
            this.desconectar("SQL_REMESA");
        }
        return remesa;
    }    
    
    
      public void BusacarDatosRemesa(String remesa ) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        BeanGeneral Bean2 = null;
        vectorCod = new Vector();
         
        try{
            st = this.crearPreparedStatement("SQL_DATOS_REMESA");
            int cont = 1;
            st.setString(1, remesa);
            System.out.println("carroceria  --->    "+st.toString());
            rs = st.executeQuery(); 
            while(rs.next()){
                Bean2 = new BeanGeneral();
              
                Bean2.setValor_01 ((rs.getString("Nempaque")!=null)?rs.getString("Nempaque"):"");
                Bean2.setValor_02 ((rs.getString("Nremesa")!=null)?rs.getString("Nremesa"):"");
                Bean2.setValor_03 ((rs.getString("Cproducto")!=null)?rs.getString("Cproducto"):"");
                Bean2.setValor_04 ((rs.getString("Nproducto")!=null)?rs.getString("Nproducto"):"");
                Bean2.setValor_05 ((rs.getString("tipoNombre")!=null)?rs.getString("tipoNombre"):"");
                Bean2.setValor_06 ((rs.getString("nombreRemiDes")!=null)?rs.getString("nombreRemiDes"):"");
                Bean2.setValor_07 ((rs.getString("codigoRemiDes")!=null)?rs.getString("codigoRemiDes"):"");
                this.vectorCod.addElement( Bean2 );
            }
           
        }
        catch(Exception e){
             e.printStackTrace();
        }
        finally {
            if ( st != null ) { st.close(); }
            this.desconectar("SQL_DATOS_REMESA");
        }
        
    }
    
    /**
     * Getter for property vectorCod.
     * @return Value of property vectorCod.
     */
    public java.util.Vector getVectorCod() {
        return vectorCod;
    }    

    /**
     * Setter for property vectorCod.
     * @param vectorCod New value of property vectorCod.
     */
    public void setVectorCod(java.util.Vector vectorCod) {
        this.vectorCod = vectorCod;
    }    
      
}


