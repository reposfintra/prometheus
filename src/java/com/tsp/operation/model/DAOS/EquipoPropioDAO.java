/******************************************************************
* Nombre ......................EquipoPropioDAO.java
* cescripci�n..................Clase DAO para equipo propio
* Autor........................Armando Oviedo
* Fecha........................06/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.DAOS; 

import com.tsp.operation.model.beans.*; 
import com.tsp.util.connectionpool.PoolManager;
import java.sql.*;
import java.util.*;

public class EquipoPropioDAO {
    
    private EquipoPropio ep;
    private Connection con;
    private PoolManager pm; 
    private Vector epros;
    
    /****  SQL QUERIES *****/
    
    private static String SQL_INSERT = "INSERT INTO equipopropio VALUES(?,?,?,?,?,?,?,?,?)";
    
    private static String SQL_UPDATE = "UPDATE " +
                                       "    EquipoPropio " +
                                       "SET " +
                                       "    last_update='now()', user_update=?, trailer=?, claseequipo=? " +                                       
                                       "WHERE " +
                                       "    trailer=? AND claseequipo=?";
    
    private static String SQL_BUSCAR = "SELECT * FROM equipopropio WHERE trailer=? AND claseequipo=?";
    
    private static String SQL_UPDEXIST = "UPDATE " +
                                         "  EquipoPropio " +
                                         "SET " +
                                         "  REG_STATUS='', claseequipo=?," +
                                         "  CREATION_DATE='now()', CREATION_USER=?, LAST_UPDATE=?, USER_UPDATE=?, BASE=?, DSTRCT=? " +
                                         "WHERE " +
                                         "  trailer = ? AND claseequipo = ?";
    
    private static String SQL_EXISTEDE = "SELECT * FROM equipopropio WHERE trailer=? AND REG_STATUS!='A'";
    
    private static String SQL_EXISTEDEANULADO = "SELECT trailer FROM equipopropio WHERE trailer=? AND claseequipo=? AND REG_STATUS='A'";
    
    private static String SQL_DELETEEP = "UPDATE EquipoPropio SET REG_STATUS = ? WHERE trailer=?";
    
    private static String SQL_BUSCARTODOSDESCUENTOS = "SELECT trailer,claseequipo FROM equipopropio WHERE REG_STATUS!='A'";
    
    private static String SQL_EXISTEPLACA = "SELECT platlr FROM PLANILLA WHERE platlr=?";
    
    private static String SQL_LIST_EQUIPO = "SELECT * FROM equipopropio WHERE trailer=?";
    
    private static String SQL_EXISTECLASEEQUIPO = "SELECT * FROM equipopropio WHERE claseequipo=?";
    
    /** Creates a new instance of EquipoPropioDAO */
    public EquipoPropioDAO() {
    }
    
    /**
     * M�todo que setea un objeto EquipoPropio
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......EquipoPropio ce
     **/     
    public void setEP(EquipoPropio ep){
        this.ep = ep;
    }
    
    /**
     * M�todo que retorna un objeto EquipoPropio
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @return.......EquipoPropio ce
     **/     
    public EquipoPropio getEquipoPropio(){
        return ep;
    }
    
    /**
     * M�todo que retorna si existe o no una placa
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean existe
     **/
    public boolean existePlaca() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{            
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(SQL_EXISTEPLACA);            
            ps.setString(1, ep.getTrailer());
            rs = ps.executeQuery();            
            return rs.next();                            
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(rs != null){
                try{
                    rs.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
        return false; 
    }
    
    /**
     * M�todo que retorna si existe o no una clase de equipo
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean existe
     **/
    public boolean existeClaseEquipo() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{            
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(SQL_EXISTECLASEEQUIPO);                        
            ps.setString(1, ep.getClaseEquipo());
            rs = ps.executeQuery();            
            return rs.next();                            
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(rs != null){
                try{
                    rs.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
        return false; 
    }                          
    
    /**
     * M�todo que busca y setea un objeto EquipoPropio
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......EquipoPropio ce cargado con el set
     **/ 
    public void BuscarEquipoPropio() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        EquipoPropio tmp = null;
        try{            
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(SQL_BUSCAR);
            ps.setString(1, ep.getTrailer());
            ps.setString(2, ep.getClaseEquipo());
            rs = ps.executeQuery();
            while(rs.next()){
                tmp = new EquipoPropio();              
                tmp = tmp.loadResultSet(rs);                                                            
            }                        
            setEP(tmp);            
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(rs != null){
                try{
                    rs.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }            
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * M�todo que retorna un boolean si existe el EquipoPropio o no
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......c�digo cel cescuento cargado con el set
     **/     
    public boolean existeEP() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{            
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(SQL_EXISTEDE);            
            ps.setString(1, ep.getTrailer());
            rs = ps.executeQuery();            
            return rs.next();                            
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(rs != null){
                try{
                    rs.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
        return false; 
    }
    
    /**
     * M�todo que retorna un boolean si existe el EquipoPropio o no, anulado
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......c�digo cel cescuento cargado con el set
     **/   
    public boolean existeCEAnulado() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{            
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(SQL_EXISTEDEANULADO);            
            ps.setString(1, ep.getTrailer());
            ps.setString(2, ep.getClaseEquipo());
            rs = ps.executeQuery();            
            return rs.next();                            
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(rs != null){
                try{
                    rs.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
        return false;        
    }
    
    /**
     * M�todo que actualiza el reg_status ce un EquipoPropio anulado
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void addCEUPD() throws SQLException{
        PreparedStatement ps = null;
        try{            
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(SQL_UPDEXIST);            
            ps.setString(1,ep.getClaseEquipo());            
            ps.setString(2,ep.getCreationUser());
            ps.setString(3,ep.getLastUpdate());
            ps.setString(4,ep.getUserUpdate());
            ps.setString(5,ep.getBase());
            ps.setString(6,ep.getDstrct());
            ps.setString(7,ep.getTrailer());   
            ps.setString(8, ep.getClaseEquipo());
            ps.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * M�todo que agrega un EquipoPropio
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void addCE() throws SQLException{
        PreparedStatement ps = null;
        try{            
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(SQL_INSERT);
            ps.setString(1,ep.getRegStatus());
            ps.setString(2,ep.getDstrct());
            ps.setString(3,ep.getLastUpdate());
            ps.setString(4,ep.getUserUpdate());
            ps.setString(5,ep.getCreationDate());
            ps.setString(6,ep.getCreationUser());
            ps.setString(7,ep.getBase());
            ps.setString(8,ep.getTrailer());
            ps.setString(9,ep.getClaseEquipo());            
            ps.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }        
    }
    
    /**
     * M�todo que modifica un EquipoPropio
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void updateCE() throws SQLException{
        PreparedStatement ps = null;
        try{
            pm =  PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setString(1, ep.getUserUpdate());
            ps.setString(2, ep.getTrailer());
            ps.setString(3, ep.getClaseEquipo());            
            ps.setString(4, ep.getTrailer());
            ps.setString(5, ep.getClaseEquipo());            
            ps.executeUpdate();           
        }catch(SQLException ex){       
            ex.printStackTrace();
        }
        finally{
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }        
    }
    
    /**
     * M�todo que elimina un EquipoPropio
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void DeleteCE() throws SQLException{
        PreparedStatement ps = null;
        try{
            pm =  PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(SQL_DELETEEP);
            ps.setString(1, "A");
            ps.setString(2, ep.getTrailer());
            ps.setString(3, ep.getClaseEquipo());
            ps.executeUpdate();            
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * M�todo que setea un vector que contiene todos los objetos ce la tabla
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void BuscarTodosEquiposPropios() throws SQLException{        
        PreparedStatement ps = null;
        ResultSet rs = null;
        epros = new Vector();
        try{
            pm =  PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(SQL_BUSCARTODOSDESCUENTOS);            
            rs = ps.executeQuery();            
            while(rs.next()){
                EquipoPropio tmp = new EquipoPropio();
                tmp.setTrailer(rs.getString(1));
                tmp.setClaseEquipo(rs.getString(2));                
                epros.add(tmp);
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(rs != null){
                try{
                    rs.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * M�todo que guetea todos los cescuentosequipos
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     * @return......Vector ce objetos EquipoPropio
     **/     
    public Vector getTodosEquiposPropios(){
        return this.epros;
    }       
    
    /**
     * Metodo: searchEquipo, permite buscar los equipos propios
     * @autor : Ing. Jose de la rosa
     * @param : una placa.
     * @version : 1.0
     */
    public void searchEquipo (String placa)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if(con!=null){
                st = con.prepareStatement (SQL_LIST_EQUIPO); 
                st.setString (1,placa);
                rs= st.executeQuery ();
                epros = new Vector ();
                while(rs.next ()){
                    EquipoPropio tmp = new EquipoPropio();
                    tmp.setTrailer (rs.getString ("trailer"));
                    tmp.setClaseEquipo (rs.getString ("claseequipo"));
                    epros.add (tmp);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA LISTA DEL EQUIPO PROPIO " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally{
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
    }
    
}