/*
 * Codigo_discrepanciaDAO.java
 *
 * Created on 27 de junio de 2005, 11:59 PM
 */

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  Jose
 */
public class Causas_AnulacionDAO {
    private Causas_Anulacion causa_anulacion;
    private Vector causas_anulacion;   
    private TreeMap tcausasa;
    /** Creates a new instance of Codigo_discrepanciaDAO */
    public Causas_AnulacionDAO() {
    }
    
     
    /**
     * Getter for property causa_anulacion.
     * @return Value of property causa_anulacion.
     */
    public com.tsp.operation.model.beans.Causas_Anulacion getCausa_anulacion() {
        return causa_anulacion;
    }
    
    /**
     * Setter for property causa_anulacion.
     * @param causa_anulacion New value of property causa_anulacion.
     */
    public void setCausa_anulacion(com.tsp.operation.model.beans.Causas_Anulacion causa_anulacion) {
        this.causa_anulacion = causa_anulacion;
    }
    
    /**
     * Getter for property causas_anulacione.
     * @return Value of property causas_anulacione.
     */
    public java.util.Vector getCausas_anulacion() {
        return causas_anulacion;
    }
    
    /**
     * Setter for property causas_anulacione.
     * @param causas_anulacione New value of property causas_anulacione.
     */
    public void setCausas_anulacion(java.util.Vector causas_anulacione) {
        this.causas_anulacion = causas_anulacion;
    }
    
    
    public void insert() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("Insert into causa_anulacion (codigo,descripcion,creation_user,dstrct,base) values(?,?,?,?,?)");
                st.setString(1, causa_anulacion.getCodigo());
                st.setString(2, causa_anulacion.getDescripcion());
                st.setString(3, causa_anulacion.getUsuario());
                st.setString(4, causa_anulacion.getDistrito());
                st.setString(5, causa_anulacion.getBase());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DE LA CAUSA DE ANULACION" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void search(String cod)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        causa_anulacion=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from causa_anulacion where codigo=? and reg_status <> 'A'");
                st.setString(1,cod);
                rs= st.executeQuery();
                if(rs.next()){
                    causa_anulacion = new Causas_Anulacion();
                    causa_anulacion.setCodigo(rs.getString("codigo"));
                    causa_anulacion.setDescripcion(rs.getString("descripcion"));
                    causa_anulacion.setDistrito(rs.getString("dstrct"));
                }
             
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA CAUSAS DE ANULACION " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public boolean exist(String cod) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("select CODIGO from causa_anulacion where codigo=? and reg_status <> 'A'");
                st.setString(1,cod);
                rs = st.executeQuery();
                if (rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL CODIGO DE LAS CAUSAS DE ANULACION " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
    public void list()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from causa_anulacion where reg_status <> 'A' order by descripcion");
                rs= st.executeQuery();
                causas_anulacion = new Vector();
                while(rs.next()){
                    causa_anulacion = new Causas_Anulacion();
                    causa_anulacion.setCodigo(rs.getString("codigo"));
                    causa_anulacion.setDescripcion(rs.getString("descripcion"));
                    causa_anulacion.setDistrito(rs.getString("dstrct"));
                    causas_anulacion.add(causas_anulacion);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LAS CAUSAS DE ANULACION " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public void update() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("update causa_anulacion set descripcion=?, last_update='now()', user_update=?, reg_status='' where codigo= ?");
                st.setString(1,causa_anulacion.getDescripcion());
                st.setString(2,causa_anulacion.getUsuario());
                st.setString(3,causa_anulacion.getCodigo());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACI�N  DE LAS CAUSAS DE ANULACION " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
   
    public void anular() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("update causa_anulacion set reg_status='A',last_update='now()', user_update=? where codigo = ?");
                st.setString(1,causa_anulacion.getUsuario());
                st.setString(2,causa_anulacion.getCodigo());
                st.executeUpdate();
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE LAS CAUSAS DE ANULACION " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void consultar(String codigo, String desc)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from causa_anulacion where reg_status <> 'A' and codigo like ? and descripcion like ? order by descripcion");
                st.setString(1, codigo+"%");
                st.setString(2, desc+"%");
                rs= st.executeQuery();
                causas_anulacion = new Vector();
                while(rs.next()){
                    causa_anulacion = new Causas_Anulacion();
                    causa_anulacion.setCodigo(rs.getString("codigo"));
                    causa_anulacion.setDescripcion(rs.getString("descripcion"));
                    causa_anulacion.setDistrito(rs.getString("dstrct"));
                    causas_anulacion.add(causa_anulacion);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA DE LAS CAUSAS DE ANULACION " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Getter for property tcausasa.
     * @return Value of property tcausasa.
     */
    public java.util.TreeMap getTcausasa() {
        return tcausasa;
    }    
  
    /**
     * Setter for property tcausasa.
     * @param tcausasa New value of property tcausasa.
     */
    public void setTcausasa(java.util.TreeMap tcausasa) {
        this.tcausasa = tcausasa;
    }
    
    public void llenarTree()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                
                st = con.prepareStatement("select * from causa_anulacion where reg_status <> 'A' order by descripcion");
                rs = st.executeQuery();
                tcausasa = new TreeMap();
                while(rs.next()){
                    tcausasa.put(rs.getString("descripcion"), rs.getString("codigo"));
                }
                
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL TREE DE CAUSAS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
}
