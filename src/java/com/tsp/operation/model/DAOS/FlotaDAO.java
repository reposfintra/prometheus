 /***********************************************************************************
 * Nombre clase : ............... FlotaDAO.java                                    *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la tabla Flota.                              *
 * Autor :....................... Ing. Juan Manuel Escandon Perez                  *
 * Fecha :........................ 5 de diciembre de 2005, 05:33 PM                *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/
package com.tsp.operation.model.DAOS; 

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.*;

public class FlotaDAO extends MainDAO{
   
    /*
    *DECLARACION DE LAS CONSULTAS SQL
    */
    
    /*Insert */
    private static String SQLInsert     =  "   insert into flota ( std_job_no, placa, dstrct, base, creation_user ) values ( ? , ?, ?, ?, ? )  ";
    
    /*Update*/
    private static String SQLUpdate     =  "   update flota set std_job_no = ?, placa = ? where std_job_no = ? and placa = ? ";
    
    /*Delete*/
    private static String SQLDelete     =  "   delete from flota where std_job_no = ? and placa = ?  ";
    
    /*Update status*/
    private static String SQLUpdateR    =  "   update flota set reg_status = ? where std_job_no = ? and placa = ?   ";
    
    /*Filtro * std_job*/
    private static String SQLFILTROS    =   "   select f.std_job_no, f.placa, st.std_job_desc, f.reg_status from flota f          "+
                                            "   left outer join stdjob st on (st.std_job_no = f.std_job_no)                       "+
                                            "   where f.std_job_no =  ?                                                            ";
    
    /*Filtro * placa*/
    private static String SQLFILTROP    =   "   select f.std_job_no, f.placa, st.std_job_desc, f.reg_status from flota f          "+
                                            "   left outer join stdjob st on (st.std_job_no = f.std_job_no)                       "+
                                            "   where f.placa = ?                                                                 ";
    
    /*Filtro*/
    private static String SQLFILTRO     =   "   select f.std_job_no, f.placa, st.std_job_desc, f.reg_status from flota f          "+
                                            "   left outer join stdjob st on (st.std_job_no = f.std_job_no)                       "+
                                            "   where f.placa like ? and f.std_job_no like ?                                      ";

    /*Existe*/
    private static String SQLSearch     =   "   select placa from flota where std_job_no = ? and placa = ?                         ";
    /** Creates a new instance of FlotaDAO */
    public FlotaDAO() {
         super("FlotaDAO.xml");
    }
    
     /**
     * Metodo Insert, a�ade un nuevo registro a la tabla Flota
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String distrito, String base, String placa , String std_job , String usuario
     * @version : 1.0
     */
    public void Insert(String dstrct, String base , String std_job, String placa, String usuario) throws SQLException {
        PreparedStatement st          = null;              
        try {
            st = this.crearPreparedStatement("SQL_INSERT");
            st.setString(1, std_job);
            st.setString(2, placa);
            st.setString(3, dstrct);
            st.setString(4, base);
            st.setString(5, usuario);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina INSERT [FlotaDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            desconectar("SQL_INSERT");
        }
    }
    
    /**
     * Metodo UpdateEstado, modifica el campo reg_status en la tabla flota
     * para un std_job y una placa correspondiente
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String reg_status, String placa , String std_job
     * @version : 1.0
     */
    public void UpdateEstado(String reg_status, String std_job,  String placa, String usuario ) throws SQLException {
        PreparedStatement st          = null; 
        try {
            st = this.crearPreparedStatement("SQL_UPDATER");
            st.setString(1, usuario);
            st.setString(2, reg_status);
            st.setString(3, std_job);
            st.setString(4, placa);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina UPDATEESTADO [FlotaDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            desconectar("SQL_UPDATER");
        }
    }
    
     /**
     * Metodo Delete, elimina un registro de la tabla flota 
     * para un std_job y una placa correspondiente
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String placa , String std_job
     * @version : 1.0
     */
    public void Delete(String std_job,  String placa ) throws SQLException {
        PreparedStatement st          = null; 
        try {
            st = this.crearPreparedStatement("SQL_DELETE");
            st.setString(1, std_job);
            st.setString(2, placa);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina Delete [FlotaDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            desconectar("SQL_DELETE");
        }
    }
    
    /**
     * Metodo ListxPlaca, lista toda la flota correspondiente para una 
     * placa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String placa
     * @version : 1.0
     */
    public List ListxPlaca(String placa) throws Exception{      
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        try{
            st = this.crearPreparedStatement("SQL_FILTROP");
            st.setString( 1, placa);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(Flota.load(rs));
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina ListxPlaca [FlotaDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            desconectar("SQL_FILTROP");
        }
        return lista;
    }
    
    
    /**
     * Metodo ListxStd, lista toda la flota correspondiente para un
     * std_job
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String placa
     * @version : 1.0
     */
    public List ListxStd(String std_job) throws Exception{    
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        try{
            st = this.crearPreparedStatement("SQL_FILTROS");
            st.setString( 1, std_job);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(Flota.load(rs));
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina ListxStd [FlotaDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            desconectar("SQL_FILTROS");
        }
        return lista;
    }
    
     /**
     * Metodo Buscar, retorna un true si encuentra un registro dentro de la 
     * tabla flota, para un std_job y una placa correspondiente
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String placa, String std_job
     * @version : 1.0
     */
    public boolean Buscar( String std_job_no, String placa ) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        boolean flag         = false;
        
   
        try {
            st = this.crearPreparedStatement("SQL_SEARCH");
            st.setString(1, std_job_no);
            st.setString(2, placa);
            rs = st.executeQuery();
            while(rs.next()){
                flag = true;
                break;
            }
        }
        catch(Exception e) {
            throw new SQLException("Error en rutina Buscar [FlotaDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            desconectar("SQL_SEARCH");
        }
        return flag;
    }
    
    
    /**
     * Metodo List, lista toda la flota correspondiente para esta std_job 
     * y / o placa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String placa , String std_job 
     * @version : 1.0
     */
    public List List(String placa, String std_job) throws Exception{   
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        try{
            
            st = this.crearPreparedStatement("SQL_FILTRO");
            st.setString( 1, placa+"%");
            st.setString( 2, std_job);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(Flota.load(rs));
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina List [FlotaDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            desconectar("SQL_FILTRO");
        }
        return lista;
    }
    


    /******* jose 2007-02-01 *********/
    public boolean existStandar (String standar, String distrito, String flota)throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        try {
            st = crearPreparedStatement ("SQL_EXISTE_STANDAR");
            st.setString (1, standar);
            st.setString (2, distrito);
            st.setString (3, flota);
            rs= st.executeQuery ();
            sw = rs.next ();
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR UN STANDAR EN FLOTA, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_EXISTE_STANDAR");
        }
        return sw;
    }    
    
     /**
     * Metodo: existPlaca, permite buscar una flota que se encuentre dentro de un rango de fecha
     * @autor : Ing. Jose de la rosa
     * @see existPlaca - FlotalDAO     
     * @param : la placa, el distrito, la fecha actual
     * @version : 1.0
     */    
    public Flota existPlaca (String placa, String distrito, String fecha )throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Flota f = null;
        try {
            st = crearPreparedStatement ("SQL_EXISTE_PLACA_FECHAS");
            st.setString (1, placa);
            st.setString (2, distrito);
            st.setString (3, fecha);
            st.setString (4, fecha);
            st.setString (5, fecha);
            rs= st.executeQuery ();
            while(rs.next ()){
                f = new Flota();
                f.setPlaca ( rs.getString ("placa") );
                f.setStd_job_no ( rs.getString ("std_job_no") );
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR UNA PLACA EN FLOTA, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_EXISTE_PLACA_FECHAS");
        }
        return f;
    }
}
