/*
 * ActividadTraficoDAO.java
 *
 * Created on 9 de septiembre de 2005, 05:50 PM
 */

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  INTEL
 */
public class ActividadTraficoDAO {
    
     private ActividadTrafico act_trf;
    private Vector vector;
    
    /** Creates a new instance of ActividadTraficoDAO */
    public ActividadTraficoDAO() {
    }
    
    public ActividadTrafico getActividadTrafico() {
        return act_trf;
    }

    public void setActividadTrafico(ActividadTrafico act_trf) {
        this.act_trf = act_trf;
    }

    public Vector getActividadTraficos() {
        return vector;
    }

    public void setActividadTraficos(Vector vector) {
        this.vector = vector;
    }
    
    public void insertActividadTrafico() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("Insert into actividad_trf (cod_act,desc_act,creation_user,creation_date,user_update,last_update,cia,rec_status,base) values(?,?,?,'now()',?,'now()',?,' ',?)");
                st.setString(1,act_trf.getCodigo());
                st.setString(2,act_trf.getDescripcion());
                st.setString(3,act_trf.getCreation_user());
                st.setString(4,act_trf.getUser_update());
                st.setString(5,act_trf.getCia());
                st.setString(6,act_trf.getBase()); 
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DE LA ACTIVIDAD TRAFICO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void searchActividadTrafico(String cod)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from actividad_trf where cod_act=? and rec_status != 'A'");
                st.setString(1,cod);
                rs= st.executeQuery();
                vector = new Vector();
                while(rs.next()){
                    act_trf = new ActividadTrafico();
                    act_trf.setCodigo(rs.getString("cod_act"));
                    act_trf.setDescripcion(rs.getString("desc_act"));
                    vector.add(act_trf);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA ACTIVIDAD TRAFICO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public boolean existActividadTrafico(String cod) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("select * from actividad_trf where cod_act=? and rec_status != 'A'");
                st.setString(1,cod);
                rs = st.executeQuery();
                if (rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA ACTIVIDAD TRAFICO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
    public void listActividadTrafico()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from actividad_trf where rec_status != 'A' order by cod_act");
                rs= st.executeQuery();
                vector = new Vector();
                while(rs.next()){
                    act_trf = new ActividadTrafico();
                    act_trf.setCodigo(rs.getString("cod_act"));
                    act_trf.setDescripcion(rs.getString("desc_act"));
                    act_trf.setBase(rs.getString("base"));
                    vector.add(act_trf);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA ACTIVIDAD TRAFICO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public void updateActividadTrafico(String cod, String desc, String usu, String base) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("update actividad_trf set desc_act=?, last_update='now()', user_update=?, rec_status=' ', base = ? where cod_act = ? ");
                st.setString(1,desc);
                st.setString(2,usu);
                st.setString(3,base);
                st.setString(4,cod);
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACI�N DE LA ACTIVIDAD TRAFICO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void anularActividadTrafico() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("update actividad_trf set rec_status='A',last_update='now()', user_update=? where cod_act = ?");
                st.setString(1,act_trf.getUser_update());
                st.setString(2,act_trf.getCodigo());
                st.executeUpdate();
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE LA ACTIVIDAD TRAFICO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public Vector searchDetalleActividadTraficos(String cod, String desc, String base) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        vector=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            boolean sw = false;
            if(con!=null){
                st = con.prepareStatement("Select * from actividad_trf where cod_act like ? and desc_act like ? and base like ? and rec_status != 'A'");
                st.setString(1, cod+"%");
                st.setString(2, desc+"%");
                st.setString(3, base+"%");
                rs = st.executeQuery();
                vector = new Vector();
                while(rs.next()){
                    act_trf = new ActividadTrafico();
                    act_trf.setCodigo(rs.getString("cod_act"));
                    act_trf.setDescripcion(rs.getString("desc_act"));
                    act_trf.setRec_status(rs.getString("rec_status"));
                    act_trf.setUser_update(rs.getString("user_update"));
                    act_trf.setCreation_user(rs.getString("creation_user"));
                    act_trf.setBase(rs.getString("base"));
                    vector.add(act_trf);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS ACTIVIDADES" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return vector;
    }
    public Vector listarActividadTrafico()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from actividad_trf where rec_status != 'A' order by cod_act");
                rs= st.executeQuery();
                vector = new Vector();
                while(rs.next()){
                    act_trf = new ActividadTrafico();
                    act_trf.setCodigo(rs.getString("cod_act"));
                    act_trf.setDescripcion(rs.getString("desc_act"));
                    act_trf.setBase(rs.getString("base"));
                    vector.add(act_trf);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA ACTIVIDAD TRAFICO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return vector;
    }
    
    public String obtenerActividadTrafico(String cod)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String desc = "";
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from actividad_trf where cod_act=?");
                st.setString(1,cod);
                rs= st.executeQuery();
                vector = new Vector();
                if(rs.next()){
                    desc = rs.getString("desc_act");
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA ACTIVIDAD TRAFICO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        return desc;
        
    }
    
}
