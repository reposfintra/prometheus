/*
 * ImpoExpoDAO.java
 *
 * Created on 1 de agosto de 2006, 03:18 PM
 */
/********************************************************************
 *      Nombre Clase.................   ImpoExpoDAO.java
 *      Descripci�n..................   DAO que maneja impo_expo
 *      Autor........................   David Pi�a Lopez
 *      Fecha........................   1 de agosto de 2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.ImpoExpo;

/**
 *
 * @author  David
 */
public class ImpoExpoDAO extends MainDAO{
    
    Vector vector;
    ImpoExpo imp;
    
    /** Creates a new instance of ImpoExpoDAO */
    public ImpoExpoDAO() {
        super("ImpoExpoDAO.xml");
    }
    
    
    
    /**
     *M�todo que obtiene el valor total de la remesa en la factura detallada
     *@param dstrct Distrito
     *@param cliente El codigo del cliente
     *@param tipo el tipo documento
     *@return el valor de la sumatoria
     *@autor: David Pi�a     
     *@throws: En caso de que un error de base de datos ocurra.     
     */
    public double obtenerSumatoriaFacturasRemesa( String dstrct, String tipo, String documento ) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        double total = 0;
        try{
            st = this.crearPreparedStatement("SQL_SUMATORIA_FACTURAS_REMESA");
            st.setString( 1, dstrct );
            st.setString( 2, tipo );
            st.setString( 3, documento );
            rs = st.executeQuery();
            while( rs.next() ){
                total = rs.getDouble( "total" );
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE obtenerSumatoriaFacturasRemesa( String dstrct, String tipo, String documento ) " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){ st.close(); }
            if (rs != null){ rs.close(); }
            this.desconectar("SQL_SUMATORIA_FACTURAS_REMESA");
        }
        return total;
    }
    
    /**
     *M�todo que actualiza el usuario y fecha de generacion de impo_expo
     *@param dstrct Distrito
     *@param cliente El codigo del cliente
     *@param tipo el tipo documento     
     *@param usuario El usuario de generacion
     *@autor: David Pi�a     
     *@throws: En caso de que un error de base de datos ocurra.     
     */
    public void actualizarGeneracion( String dstrct, String tipo, String documento, String usuario ) throws SQLException{
        PreparedStatement st = null;        
        try{
            st = this.crearPreparedStatement("SQL_ACTUALIZAR_GENERACION");
            st.setString( 1, usuario );
            st.setString( 2, dstrct );
            st.setString( 3, tipo );
            st.setString( 4, documento );
            st.executeUpdate();            
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE obtenerActualizarGeneracion( String dstrct, String tipo, String documento ) " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){ st.close(); }            
            this.desconectar("SQL_ACTUALIZAR_GENERACION");
        }        
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return vector;
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        this.vector = vector;
    }    
    
     /**
     *M�todo que obtiene las importaciones o exportaciones de un cliente dado
     *@param cliente El codigo del cliente
     *@param tipo el tipo documento
     *@autor Ing. Andr�s Maturana     
     *@throws En caso de que un error de base de datos ocurra.     
     */
    public void obtenerRegistroCliente( String cliente, String dstrct ) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;        
        try{
            st = this.crearPreparedStatement("SQL_GET_CLIENTE");
            st.setString( 1, dstrct );
            st.setString( 2, cliente );
            rs = st.executeQuery();            
            vector = new Vector();            
            while( rs.next() ){
                Hashtable ht = new Hashtable();
                ht.put( "dstrct", rs.getString( "dstrct" ) );
                ht.put( "tipo_doc", rs.getString( "tipo_doc" ) );
                ht.put( "tipo_doc_desc", rs.getString( "tipo_doc_desc" ) );
                ht.put( "documento", rs.getString( "documento" ) );
                ht.put( "descripcion", rs.getString( "descripcion" ) );
                ht.put( "codcli", rs.getString( "codcli" ) );
                ht.put( "nombrecliente", rs.getString( "nombrecliente" ) );
                ht.put( "fecha_sia", rs.getString( "fecha_sia" ) );
                ht.put( "fecha_eta", rs.getString( "fecha_eta" ) );
                vector.add( ht );
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE obtenerRegistroCliente " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){ st.close(); }
            if (rs != null){ rs.close(); }
            this.desconectar("SQL_GET_CLIENTE");
        }
    }
    
  
      /**********************************************************
     * Metodo para obtener las importaciones que no se les ha
     *        grabado fecha de llegada o salida CDR
     *@autor: Ing. Osvaldo P�rez
     *@throws: En caso de que un error de base de datos ocurra.
     **********************************************************/
    public void obtenerImpoSinFecha( String dstrct ) throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        try{
            st = this.crearPreparedStatement("SQL_OBTENER_IMPORTACIONES_SIN_FECHAS");
            
            if( dstrct != null && !dstrct.equals("") ){
                
                st.setString( 1, dstrct );
                
                rs = st.executeQuery();
                
                this.vector = new Vector();
                
                
                while( rs.next() ){
                    
                    imp = new ImpoExpo();
                    
                    imp.setDocumento( nvl(rs.getString( "documento" ) ) );
                    imp.setFecha_eta( nvl(rs.getString( "fecha_eta" ) ) );
                    imp.setFecha_sia( nvl(rs.getString( "fecha_sia" ) ) );
                    imp.setFecha_llegada_cdr( nvl( rs.getString( "fecha_llegada_cdr" ) ) );
                    imp.setFecha_salida_cdr( nvl(rs.getString( "fecha_salida_cdr" ) ) );
                    //Nombre del cliente
                    imp.setCodcli( nvl(rs.getString( "nomcli" ) ) );
                    imp.setUsuario_genero( nvl( rs.getString( "usuario_genero" ) ) );
                    imp.setFecha_genero( nvl( rs.getString( "fecha_reporte" ) ) );
                    
                    this.vector.add( imp );
                }
            }
        }catch (SQLException e){
            throw new SQLException("ERROR ImpoExpoDAO.obtenerImpoSinFecha() " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (st != null){ st.close(); }
            this.desconectar("SQL_OBTENER_IMPORTACIONES_SIN_FECHAS");
        }
    }
    
    /**********************************************************
     * Metodo para obtener todas las importaciones o exportaciones
     *@param: String dstrct distrito del usuario
     *@param: String tipo 'IMP' � 'EXP'
     *@autor: Ing. Osvaldo P�rez
     *@throws: En caso de que un error de base de datos ocurra.
     **********************************************************/
    public void obtenerTodasImpoExpo( String dstrct, String tipo ) throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        try{
            st = this.crearPreparedStatement("SQL_OBTENER_TODAS_IMPO_EXPO");
            
            if( dstrct != null && !dstrct.equals("") ){
                
                st.setString( 1, tipo );
                st.setString( 2, dstrct );
                
                rs = st.executeQuery();
                
                this.vector = new Vector();
                
                
                while( rs.next() ){
                    
                    imp = new ImpoExpo();
                    
                    imp.setDocumento( nvl( rs.getString( "documento" ) ) );
                    imp.setFecha_eta( nvl( rs.getString( "fecha_eta" ) ) );
                    imp.setFecha_sia( nvl( rs.getString( "fecha_sia" ) ) );
                    imp.setFecha_llegada_cdr( nvl( rs.getString( "fecha_llegada_cdr" ) ) );
                    imp.setFecha_salida_cdr( nvl(rs.getString( "fecha_salida_cdr" ) ) );
                    //Nombre del cliente
                    imp.setCodcli( nvl(rs.getString( "nomcli" ) ) );
                    imp.setUsuario_genero( nvl( rs.getString( "usuario_genero" ) ) );
                    imp.setFecha_genero( nvl( rs.getString( "fecha_reporte" ) ) );
                    
                    this.vector.add( imp );
                }
            }
        }catch (SQLException e){
            throw new SQLException("ERROR ImpoExpoDAO.obtenerTodasImpoExpo() " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (st != null){ st.close(); }
            this.desconectar("SQL_OBTENER_TODAS_IMPO_EXPO");
        }
    }
    
    /**********************************************************
     * Metodo para obtener la importacion con el codigo dado
     *@param: String dstrct distrito del usuario
     *@param: String codigo codigo de la importacion
     *@autor: Ing. Osvaldo P�rez
     *@throws: En caso de que un error de base de datos ocurra.
     **********************************************************/
    public void obtenerImportacion( String dstrct, String codigo ) throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        try{
            st = this.crearPreparedStatement("SQL_OBTENER_IMPORTACION");
            
            if( dstrct != null && !dstrct.equals("") ){
                
                st.setString( 1, codigo );
                st.setString( 2, dstrct );
                
                rs = st.executeQuery();
                
                vector = new Vector();
                if( rs.next() ){
                    
                    vector = new Vector();
                    
                    imp = new ImpoExpo();
                    
                    imp.setDocumento( nvl(rs.getString( "documento" ) ) );
                    imp.setFecha_eta( nvl(rs.getString( "fecha_eta" ) ) );
                    imp.setFecha_sia( nvl(rs.getString( "fecha_sia" ) ) );
                    imp.setFecha_llegada_cdr( nvl(rs.getString( "fecha_llegada_cdr" ) ) );
                    imp.setFecha_salida_cdr( nvl(rs.getString( "fecha_salida_cdr" ) ) );
                    imp.setUsuario_genero( nvl( rs.getString( "usuario_genero" ) ) );
                    imp.setFecha_genero( nvl( rs.getString( "fecha_reporte" ) ) );
                    //Nombre del cliente
                    imp.setCodcli( ( rs.getString( "nomcli" ) != null )? rs.getString( "nomcli" ) : "" );
                    
                    vector.add(imp);
                }
            }
        }catch (SQLException e){
            throw new SQLException("ERROR ImpoExpoDAO.obtenerImportacion() " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (st != null){ st.close(); }
            this.desconectar("SQL_OBTENER_IMPORTACION");
        }
    }
    
    
    /**********************************************************
     * Metodo para grabar las fechas CDR de la importacion
     *@param: String dstrct distrito del usuario
     *@param: String codigo codigo de la importacion
     *@param: String llegada fecha de llegada al centro de distribucion
     *@param: String salida fecha de salida del centro de distribucion
     *@autor: Ing. Osvaldo P�rez
     *@throws: En caso de que un error de base de datos ocurra.
     **********************************************************/
    public void grabarFechasCDR( String dstrct, String codigo, String llegada, String salida ) throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        try{
            st = this.crearPreparedStatement("SQL_GRABAR_FECHAS_CDR");
            
            if( dstrct != null && !dstrct.equals("") ){
                
                st.setString( 1, llegada );
                st.setString( 2, salida );
                st.setString( 3, codigo );
                st.setString( 4, dstrct );
                
                st.executeUpdate();
            }
        }catch (SQLException e){
            throw new SQLException("ERROR ImpoExpoDAO.grabarFechasCDR() " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (st != null){ st.close(); }
            this.desconectar("SQL_GRABAR_FECHAS_CDR");
        }
    }
    /**
     * Getter for property imp.
     * @return Value of property imp.
     */
    public com.tsp.operation.model.beans.ImpoExpo getImp() {
        return imp;
    }
    
    /**
     * Setter for property imp.
     * @param imp New value of property imp.
     */
    public void setImp(com.tsp.operation.model.beans.ImpoExpo imp) {
        this.imp = imp;
    }
    
 public Vector reporteImportacion( String impo, String usuario, String dstrct ) throws Exception{
        
        PreparedStatement st  = null;
        PreparedStatement st2 = null;
        ResultSet rs          = null;
        Vector v = new Vector();
        
        try{
            
            try{
                
                st = this.crearPreparedStatement("SQL_REPORTE_IMPORTACION");
                
                st.setString( 1, impo );
                rs = st.executeQuery();
                
            }catch (Exception ex){
                //System.out.println("Error en consulta ImpoExpoDAO.reporteImportacion "+ex.getMessage());
            }finally{
                this.desconectar("SQL_REPORTE_IMPORTACION");
            }
            
            while( rs.next() ){

                ImpoExpo i = new ImpoExpo();
                
                i.setDocumento(         impo);
                i.setContenedor(        nvl(       rs.getString( "contenedores" ) ) );
                i.setConductor(         nvl(       rs.getString( "nombre" ) ) );
                i.setPlaca(             nvl(       rs.getString( "plaveh" ) ) );
                i.setHora_salida(       nvl(       rs.getString( "hora_salida") ) +" "+ nvl( rs.getString("time") ) );
                i.setFecha_cargue(      nvl(       rs.getString( "fecdsp") ) );
                
                String lle = nvl( rs.getString( "fecha_llegada_cdr") );
                String sal = nvl( rs.getString( "fecha_salida_cdr") );
                
                lle = lle.equals("0099-01-01")? "" : lle;
                sal = sal.equals("0099-01-01")? "" : sal;
                
                i.setFecha_llegada_cdr( lle );
                i.setFecha_salida_cdr( sal );                
                i.setPatio(             nvl( rs.getString( "patio") ) );                
                i.setOt(                nvl( rs.getString( "numrem") ) );                
                i.setFactura(           nvl( rs.getString( "factura") ) );
                i.setFecha_fac(         nvl( rs.getString( "fecha_factura") ) );                
                i.setVlr_transporte(    nvl( rs.getString( "vlr_transporte") ) );
                i.setVlr_factura(       nvl( rs.getString( "valor_factura") ) );                
                //Observacion de la planilla
                i.setDescripcion(       nvl( rs.getString( "observacion") ) );
                //fecha estimada arribo
                i.setFecha_eta(         nvl( rs.getString( "arribo") ) );
                
                i.setTipo_doc(          nvl( rs.getString("descripcion") ) );//Descripcion de la Importacion
                i.setFecha_sia(         nvl( rs.getString("fecha_sia" ) ) );
                
                v.add( i );
                
                //Actualizando la importacion por el reporte
                try{
                    
                    st2 = this.crearPreparedStatement("SQL_ACTUALIZAR_IMPO_REPORTE");
                    
                    st2.setString( 1, usuario );
                    st2.setString( 2, impo );
                    st2.setString( 3, dstrct );
                    
                    st2.executeUpdate();
                    
                }catch (Exception ex){
                    //System.out.println("Error en consulta ImpoExpoDAO.reporteImportacion 2 "+ex.getMessage());
                }finally{
                    this.desconectar("SQL_ACTUALIZAR_IMPO_REPORTE");
                }
                
            }
            
        }catch (Exception ex){
            //System.out.println("Error en ImpoExpoDAO.reporteImportacion "+ex.getMessage() );
        }finally{
            if( st!=null ) st.close();
            if( st2!=null ) st.close();
            if( rs!=null ) rs.close();
        }
        return v;
    }
    
    private String nvl( String value ){
        return (value != null)? value : "";
    }
    
    /*
    public static void main(String[]aaaaaa) throws Exception{

        ImpoExpoDAO i = new ImpoExpoDAO();
        Vector v = i.reporteImportacion( "impor1", "OPEREZ", "FINV" );

        //System.out.println( v.size() );
    }
    */
    
     
      /**
     *M�todo que obtiene las importaciones o exportaciones de un cliente dado
     *@param cliente El codigo del cliente
     *@param tipo el tipo documento
     *@autor: David Pi�a     
     *@throws: En caso de que un error de base de datos ocurra.     
     */
    public void obtenerImpoExpoCliente( String distrito, String cliente, String tipo, String codigo, String pendiente, String sin_cdr, String inicio, String fin ) throws SQLException{
        PreparedStatement   st      = null;
        ResultSet           rs      = null;   
        Connection          con     = null;
        String              query   = "SQL_GET_IMPOEXPO_CLIENTE";
        String              sql     = "";
        vector                      = null;
        try{
            con = this.conectar(query);
            sql = this.obtenerSQL(query).replaceAll("#CDR#", sin_cdr.equals ("")?"":" AND (fecha_llegada_cdr = '0099-01-01' OR fecha_salida_cdr = '0099-01-01')");
            sql = sql.replaceAll("#PENDIENTE#", pendiente.equals ("")?"":" AND (usuario_genero = '' OR fecha_genero = '0099-01-01 00:00:00') AND creation_date BETWEEN '"+inicio+" 00:00' AND '"+fin+" 23:59' " );
            st = con.prepareStatement( sql );
            st.setString( 1, distrito );
            st.setString( 2, tipo );
            st.setString( 3, codigo+"%" );
            st.setString( 4, cliente+"%" );
            rs = st.executeQuery();            
            vector = new Vector();            
            while( rs.next() ){
                Hashtable ht = new Hashtable();
                ht.put( "dstrct",           rs.getString( "dstrct" ) );
                ht.put( "tipo_doc",         rs.getString( "tipo_doc" ) );
                ht.put( "documento",        rs.getString( "documento" ) );
                ht.put( "descripcion",      rs.getString( "descripcion" ) );
                ht.put( "codcli",           rs.getString( "codcli" ) );
                ht.put( "nombrecliente",    rs.getString( "nombrecliente" ) );
                ht.put( "fecha_sia",        rs.getString( "fecha_sia" ) );
                ht.put( "fecha_eta",        rs.getString( "fecha_eta" ) );
                ht.put( "fecha_llegada_cdr",rs.getString( "fecha_llegada_cdr" ) );
                ht.put( "fecha_salida_cdr", rs.getString( "fecha_salida_cdr" ) );
                ht.put( "usuario_genero",   rs.getString( "usuario_genero" ) );
                ht.put( "fecha_genero",     rs.getString( "fecha_genero" ) );
                vector.add( ht );
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE obtenerImpoExpoCliente( String cliente, String tipo ) " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){ st.close(); }
            if (rs != null){ rs.close(); }
            this.desconectar(query);
        }
    }
    
   
    
    /**
     *M�todo verifica si existe o no un documento de exportaci�n.
     *@param dstrct Distrito
     *@param doc N�mero del documento de exportaci�n.
     *@autor Ing. Andr�s Maturana
     *@throws En caso de que un error de base de datos ocurra.     
     */
    public boolean existeDocExpo( String dstrct, String doc ) throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        try{
            st = this.crearPreparedStatement("SQL_EXISTE_DOCEXPO");
            
            if( dstrct != null && !dstrct.equals("") ){
                
                st.setString( 1, dstrct );
                st.setString( 2, doc );
                
                rs = st.executeQuery();
                
                if( rs.next() ){
                    return true;
                }
            }
        }catch (SQLException e){
            throw new SQLException("ERROR existeDocExpo( String dstrct, String doc ) " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (st != null){ st.close(); }
            this.desconectar("SQL_EXISTE_DOCEXPO");
        }
        
        return false;
    }
    public String fechaSalidaTrafico ( String dstrct, String numpla ) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;        
        String name = "0099-01-01 00:00:00";
        try{
            st = this.crearPreparedStatement("SQL_FECHA_SALIDA_TRAFICO");
            st.setString( 1, dstrct );
            st.setString( 2, numpla );
            rs = st.executeQuery();            
            if( rs.next() ){
                name = rs.getString( "fechareporte" );
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE fechaSalidaTrafico ( String dstrct, String numpla ) " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){ st.close(); }
            if (rs != null){ rs.close(); }
            this.desconectar("SQL_FECHA_SALIDA_TRAFICO");
        }
        return name;
    }
    
     public String fechaEntregaTrafico ( String dstrct, String numpla ) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;        
        String name = "0099-01-01 00:00:00";
        try{
            st = this.crearPreparedStatement("SQL_FECHA_ENTREGA_TRAFICO");
            st.setString( 1, dstrct );
            st.setString( 2, numpla );
            rs = st.executeQuery();            
            if( rs.next() ){
                name = rs.getString( "fechareporte" );
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE fechaEntregaTrafico ( String dstrct, String numpla ) " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){ st.close(); }
            if (rs != null){ rs.close(); }
            this.desconectar("SQL_FECHA_ENTREGA_TRAFICO");
        }
        return name;
    }

    
     /**
     *  M�todo que obtiene las informacion para el reporte exportaciones
     *  de materia prima.
     *@param dstrct Distrito
     *@param cliente El codigo del cliente
     *@param tipo el tipo documento
     *@autor Ing. Andr�s Maturana
     *@throws En caso de que un error de base de datos ocurra.     
     */
    public void obtenerExpoCliente( String dstrct, String tipo, String documento ) throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;   
        PreparedStatement st2 = null;
        ResultSet rs2 = null;   
        PreparedStatement st3 = null;
        ResultSet rs3 = null;   
        Connection con = null;
        
        
        org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
        
        try{
            con = this.conectar("SQL_IMPOEXPO_REMESAS");
            
            st = con.prepareStatement(this.obtenerSQL("SQL_IMPOEXPO_REMESAS"));
            st.setString( 1, dstrct );
            st.setString( 2, tipo );
            st.setString( 3, documento );
            
            logger.info("REPORTE MABE: " + st);
            
            rs = st.executeQuery();            
            vector = new Vector();            
            while( rs.next() ){
                Hashtable ht = new Hashtable();
                ht.put( "documento_rel", rs.getString( "documento_rel" ) );
                ht.put( "planilla", rs.getString( "planilla" ) );
                ht.put( "contenedores", rs.getString( "contenedores" ) );
                ht.put( "origenviaje", rs.getString( "origenviaje" ) );
                ht.put( "conductor", rs.getString( "conductor" ) );
                ht.put( "placa", rs.getString( "placa" ) );
                ht.put( "cedcon", rs.getString( "cedcon" ) );
                ht.put( "escolta", rs.getString( "escolta" ) );
                ht.put( "hora_salida", rs.getString( "hora_salida" ) );
                
                /* Info de la 2da OC */
                
                st2 = con.prepareStatement(this.obtenerSQL("SQL_EXPO_2DA_OC"));
                st2.setString( 1, "" + ht.get("documento_rel") );
                logger.info("QUERY 2DA OC: " + st2);
                rs2 = st2.executeQuery();
                
                ht.put( "fec_2daoc", "" );
                ht.put( "fecEcl_2doc", "" );                
                while ( rs2.next() ){                    
                    //ht.put( "fec_2daoc", rs2.getString( "fecpla" ) );
                    ht.put( "fecEcl_2doc", rs2.getString( "fechareporte" ) );
                    ht.put( "fec_2daoc", rs2.getString( "fechareporte_sal" ) );//AMATURANA 26.02.2007
                } 
                
                /* Info de la Factura */
                
                st3 = con.prepareStatement(this.obtenerSQL("SQL_VR_FRA_EXPO_REPORTE"));                
                st3.setString( 1, dstrct );
                st3.setString( 2, "" + ht.get("documento_rel") );
                //logger.info("FRA EXPO: " + st3);
                rs3 = st3.executeQuery();
                
                ht.put( "fechafactura", "" );   
                ht.put( "nfactura", "" );       
                ht.put( "fra_moneda", "" );        
                while ( rs3.next() ){                    
                    ht.put( "fechafactura", rs3.getString( "fecha_factura" ) );
                    ht.put( "nfactura", rs3.getString( "documento" ) );
                    ht.put( "fra_moneda", rs3.getString( "moneda" ) );
                    ht.put( "vr_transp", rs3.getString( "valor_itemme" ) );
                }              
                                
                vector.add( ht );
            }
        }
        catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE obtenerExpoCliente " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){ st.close(); }
            if (rs != null){ rs.close(); }
            this.desconectar("SQL_IMPOEXPO_REMESAS");
        }
    }
    public void obtenerImpoExpoRemesas( String dstrct, String tipo, String documento ) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;        
        try{
            st = this.crearPreparedStatement("SQL_GET_IMPOEXPO_REMESAS");
            st.setString( 1, dstrct );
            st.setString( 2, tipo );
            st.setString( 3, documento );
            rs = st.executeQuery();            
            vector = new Vector();            
            while( rs.next() ){
                Hashtable ht = new Hashtable();
                ht.put( "documento_rel", rs.getString( "documento_rel" ) );
                ht.put( "planilla", rs.getString( "planilla" ) );
                ht.put( "contenedores", rs.getString( "contenedores" ) );
                ht.put( "pesoreal", rs.getString( "pesoreal" ) );
                ht.put( "origenviaje", rs.getString( "origenviaje" ) );
                ht.put( "conductor", rs.getString( "conductor" ) );
                ht.put( "placa", rs.getString( "placa" ) );
                ht.put( "horasalida", fechaSalidaTrafico ( dstrct, rs.getString( "planilla" ) ) );
                ht.put( "fechacargue",  rs.getString( "horasalida" ) );
                ht.put( "escolta", rs.getString( "escolta" ) );
                ht.put( "fechadescargue", fechaEntregaTrafico( dstrct, rs.getString( "planilla" ) ) );
                ht.put( "patiocontenedores", rs.getString( "patiocontenedores" ) );
                ht.put( "valoritem", rs.getString( "valoritem" ) );
                ht.put( "doctransporte", rs.getString( "doctransporte" ) );
                ht.put( "valortransporte", rs.getString( "valortransporte" ) );
                ht.put( "fechafactura", rs.getString( "fechafactura" ) );
                vector.add( ht );
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE obtenerImpoExpoRemesas( String dstrct, String tipo, String documento ) " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){ st.close(); }
            if (rs != null){ rs.close(); }
            this.desconectar("SQL_GET_IMPOEXPO_REMESAS");
        }
    }
}
