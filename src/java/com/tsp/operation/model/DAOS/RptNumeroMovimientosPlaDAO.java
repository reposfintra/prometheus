/*
 * RptNumeroMovimientosPla.java
 *
 * Created on 12 de septiembre de 2005, 9:44
 */

package com.tsp.operation.model.DAOS;
import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  Henry
 */
public class RptNumeroMovimientosPlaDAO {
    
    Vector vecRpts;
    NumeroRptControlOracle numRpt;
    
    private static final String SQL_NO_RPTS_PLA = "" +
    "SELECT " +    
        "a.BRANCH_CODE," +
        "a.BANK_ACCT_NO," +
        "a.CHEQUE_NO," +
        "a.NUMMOV," +
        "b.SUPP_TO_PAY," +
        "b.EXT_INV_NO," +
        "b.CHEQUE_RUN_NO," +
        "b.LOC_INV_ORIG," +
        "b.SUPPLIER_NO," +
        "b.SUPPLIER_NAME," +
        "b.INV_NO " +
    "FROM" +
    "    (SELECT" +
    "        DISTINCT" +
    "        A.INV_NO," +
    "        A.EXT_INV_NO," +
    "        A.SUPP_TO_PAY," +
    "        B.SUPPLIER_NO," +
    "        B.SUPPLIER_NAME," +
    "        A.CHEQUE_RUN_NO," +
    "        A.CHEQUE_NO," +
    "        A.BANK_ACCT_NO," +
    "        A.BRANCH_CODE," +
    "        A.LOC_INV_ORIG," +
    "        A.DSTRCT_CODE" +
    "    FROM" +
    "        MSF260 A," +
    "        MSF200 B" +
    "    WHERE" +
    "        A.LOADED_DATE BETWEEN ? AND ?" +
    "        AND A.INV_TYPE = 'A'" +
    "        AND B.SUPPLIER_NO = A.SUPPLIER_NO" +
    "        ) B," +
    "    (" +
    "    SELECT" +
    "         a.branch_code," +
    "         a.bank_acct_no," +
    "         a.cheque_no," +
    "         a.dstrct_code," +
    "         count(c.planilla) nummov" +
    "    FROM" +
    "         msf260   a," +
    "         msf26a   b," +
    "         trafimo  c" +
    "    WHERE" +    
    "         a.loaded_date between ? and ?" +
    "         and  a.inv_type = 'A'" +
    "         and  substr(a.dstrct_code,1,3) = ?" +
    "         and  b.dstrct_code  = a.dstrct_code" +
    "         and  b.supplier_no  = a.supplier_no" +
    "         and  b.inv_no       = a.inv_no" +
    "         and  c.planilla     = b.po_no" +
    "     GROUP BY branch_code,bank_acct_no,cheque_no,a.dstrct_code" +
    "     ) A " +
    "WHERE" +
    "     b.BRANCH_CODE  = a.BRANCH_CODE" +
    "     AND  b.BANK_ACCT_NO = a.BANK_ACCT_NO" +
    "     AND  b.CHEQUE_NO    = a.CHEQUE_NO" +
    "     AND  b.dstrct_code  = a.dstrct_code";    
    /** Creates a new instance of RptNumeroMovimientosPla */
    public RptNumeroMovimientosPlaDAO() {
    }
    
    public Vector getVectorRpts(){
        return vecRpts;
    }
    public void generarRptNumeroMovimienots (String dstrct_code, String fecini, String fecfin)throws SQLException{            
        vecRpts = new Vector();
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("oracle");
            if(con!=null){        
                st = con.prepareStatement(this.SQL_NO_RPTS_PLA);
                st.setString(1, fecini.replaceAll("-",""));
                st.setString(2, fecfin.replaceAll("-",""));
                st.setString(3, fecini.replaceAll("-",""));
                st.setString(4, fecfin.replaceAll("-",""));
                st.setString(5, dstrct_code);
                ////System.out.println(fecini.replaceAll("-",""));
                rs = st.executeQuery();
                while(rs.next()){
                    numRpt = new NumeroRptControlOracle();
                    numRpt.setBRANCH_CODE(rs.getString(1));
                    numRpt.setBANCK_ACCT_NO(rs.getString(2));
                    numRpt.setCHEQUE_NO(rs.getString(3));
                    numRpt.setNumMov(rs.getString(4));
                    numRpt.setSUPP_TO_PAY(rs.getString(5));
                    numRpt.setEXT_INV_NO(rs.getString(6));
                    numRpt.setCHEQUE_RUN_NO(rs.getString(7));                   
                    numRpt.setLOC_INV_ORIG(rs.getString(8));
                    numRpt.setSUPPLIER_NO(rs.getString(9));
                    numRpt.setSUPPLIER_NAME(rs.getString(10));
                    numRpt.setINV_NO(rs.getString(11));
                    
                    //numRpt.setPO_NO(rs.getString("PO_NO"));
                    
                    vecRpts.addElement(numRpt);
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA  BUSQUEDA DE DATOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("oracle", con);
            }
        }
        
    }
}
