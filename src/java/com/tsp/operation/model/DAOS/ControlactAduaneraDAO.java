
/***********************************************************************************
 * Nombre clase : ............... ControlactAcuaneraDAO.java                       *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.                                       *
 * Autor :....................... Ing. Diogenes Antonio Bastidas Morales           *
 * Fecha :........................ 30 de noviembre de 2005, 08:45 AM               *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  dbastidas
 */
public class ControlactAduaneraDAO {
    
    /** Creates a new instance of ControlactAcuaneraDAO */
    public ControlactAduaneraDAO() {
    }
    
    private ControlactAduanera controlact;
    private Planilla planilla;
    private List reporte;
    private List planillas;
    
    
    private static String CONSULTAREPORTE ="select A.*,                           "+
                                            "           b.nomcli,                  "+
                                            "           c.nomciu as origen,        "+
                                            "           d.nomciu as destino        "+
                                            "    from                              "+
                                            "    (select r.numrem,                 "+
                                            "           r.cliente,                 "+ 
                                            "           r.fecrem,                  "+ 
                                            "           r.orirem,                  "+ 
                                            "           r.desrem,                  "+
                                            "           p.numpla,                  "+ 
                                            "           rd.documento,              "+
                                            "           s.std_job_desc,            "+
                                            "           s.wo_type                  "+
                                            "    from                              "+
                                            "          remesa r,                   "+  
                                            "          plarem pl,                  "+ 
                                            "          planilla p,                 "+
                                            "          stdjob s,                   "+
                                            "          remesa_docto rd             "+
                                            "    where                             "+ 
                                            "         r.fecrem between ? and ?     "+
                                            "    and  r.reg_status <> 'C'          "+ 
                                            "    and  r.numrem = pl.numrem         "+
                                            "    and  pl.numpla = p.numpla         "+
                                            "    and  r.std_job_no = s.std_job_no  "+
                                            "    and  s.wo_type in ('RM','RC','RE') "+
                                            "    and  r.numrem = rd.numrem          "+
                                            "    and  rd.tipo_doc = '009')A         "+
                                            "    LEFT JOIN cliente B  ON (B.codcli = A.cliente) "+
                                            "    LEFT JOIN ciudad C  ON (C.codciu = A.orirem)   "+
                                            "    LEFT JOIN ciudad D  ON (D.codciu = A.desrem)   " +
                                            "    order by a.wo_type, a.fecrem                   "; 
    
      private static String BUSCARPLANILLA = "Select A.*,                   "+
                                             "           b.nomciu as origen, "+
                                             "           c.nomciu as destino  "+
                                             "   from                          "+
                                             "   (SELECT p.numpla,             "+
                                             "           p.plaveh,             "+ 
                                             "           p.fecdsp,             "+
                                             "           p.oripla,             "+
                                             "           p.despla              "+
                                             "   from                          "+
                                             "         plarem pl,              "+
                                             "         planilla p              "+
                                             "   where                         "+
                                             "        pl.numrem = ?            "+ 
                                             "   and  pl.numpla = p.numpla)A   "+ 
                                             "   LEFT JOIN ciudad B  ON (B.codciu = A.oripla)  "+
                                             "   LEFT JOIN ciudad C  ON (C.codciu = A.despla) ";
    
    /**
     * Metodo reporteActividadAduanera, lista todas la remesa dadas entra un rango de fechas
     *  que el wo_type sea RM,RC,RE, ademas que el reg_status <> C (las que no estan cumplidas)
     * @param: fecha inicio y fecha fin del rango
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    
    
    public void reporteActividadAduanera (String fecini, String fecfin ) throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(CONSULTAREPORTE);
                st.setString(1, fecini);
                st.setString(2, fecfin);
                
                reporte = new LinkedList ();
                
                rs = st.executeQuery();
                
                while (rs.next()){
                    controlact = new ControlactAduanera();
                    controlact.setCliente(rs.getString("nomcli"));
                    controlact.setFecrem(rs.getString("fecrem"));
                    controlact.setFacComercial(rs.getString("documento"));
                    controlact.setOrirem(rs.getString("origen"));
                    controlact.setDesrem(rs.getString("destino"));
                    controlact.setDesstdjob(rs.getString("std_job_desc"));
                    controlact.setRemesa(rs.getString("numrem"));
  
                    reporte.add(controlact);
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL LISTAR EL REPORTE DE ACTIVIDADES ADUANERAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        } 
       
    }
    
    /**
     * Metodo getActividadAduanera, retrorna la lista de actividades aduaneras
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public List getActividadAduanera (){
        return reporte;        
    }
    
    /**
     * Metodo buscarPlanillaRemesa, busca las planillas relacionadas a una  remesa 
     * @param: numero de la remesa
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    
    public void buscarPlanillaRemesa ( String numrem ) throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(BUSCARPLANILLA);
                st.setString(1, numrem);

                
                planillas = new LinkedList ();
                
                rs = st.executeQuery();
                
                while (rs.next()){
                    planilla = new Planilla();
                    planilla.setNumpla(rs.getString("numpla"));
                    planilla.setPlaveh(rs.getString("plaveh"));
                    planilla.setFecdsp(rs.getString("fecdsp"));
                    planilla.setOrigenpla(rs.getString("origen"));
                    planilla.setDestinopla(rs.getString("destino"));
  
                    planillas.add(planilla);
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL LISTAR LAS PLANILLAS DE LA REMESA DE ACTIVIDADES ADUANERAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        } 
       
    }
    
    /**
     * Metodo getPlanillasRem, retrorna la lista de las planillas realacionadas a una remesa
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public List getPlanillasRem (){
        return planillas;        
    }
    
}
