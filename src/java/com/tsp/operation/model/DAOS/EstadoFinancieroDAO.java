/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.DAOS;

import java.sql.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
import com.tsp.util.Util;


/**
 *
 * @author Alvaro
 */
public class EstadoFinancieroDAO extends MainDAO {


    public EstadoFinancieroDAO() {
        super("EstadoFinancieroDAO.xml");
    }
    public EstadoFinancieroDAO(String dataBaseName) {
        super("EstadoFinancieroDAO.xml",dataBaseName);
    }


    /**
     * Lista las acciones de un contratista sin prefacturar
     * @id_contratista Identificacion del contratista, codigo interno

     */


    public List getEstadoFinanciero1(String distrito, String anio, String informe,double secuencia1,double secuencia2)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_ESTADO_FINANCIERO_1";
        List listaEstadoFinanciero1 = null;
        String filtro="";
        //validamos el filtro de secuencia..
        if(secuencia1 >0 && secuencia2 >0){
             filtro="where a.secuencia between "+secuencia1+" and "+secuencia2;
        }

        try {


            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query ).replaceAll("#filtro", filtro);
            st           = con.prepareStatement( sql );
            st.setString( 1, anio );
            st.setString( 2, distrito );
            st.setString( 3, informe );
            st.setString( 4, distrito );
            st.setString( 5, informe );
            st.setString( 6, distrito );
            st.setString( 7, informe );
            st.setString( 8, distrito );
            st.setString( 9, informe );
            st.setString( 10, distrito );
            st.setString( 11, informe );



            rs = st.executeQuery();

            listaEstadoFinanciero1 =  new LinkedList();

            while (rs.next()){
                listaEstadoFinanciero1.add(EstadoFinanciero1.load(rs));
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS PARA GENERACION ESTADOS FINANCIEROS 1. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return listaEstadoFinanciero1;
    }



    public void creaMayorTercero(String distrito, String anio, String informe, String unidad  ) throws SQLException {

        PreparedStatement st       = null;
        Connection        con      = null;


        String            query    = "SQL_CREA_MAYOR_TERCERO";
        String periodo = anio + "%";

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            if (informe.equalsIgnoreCase("EF_1")) {
                sql = sql.replaceAll("#UNIDAD#", "");
                sql = sql.replaceAll("#UNIDAD_1#", "");
                sql = sql.replaceAll("#UNIDAD_2#", "");
            }
            else {
                sql = sql.replaceAll("#UNIDAD#", "  and substr(cuenta,1,1) in ('I','C','G') and substr(cuenta, 7,3) = '" + unidad + "' " );
                sql = sql.replaceAll("#UNIDAD_1#", " and substr(cuenta, 7,3) = '" + unidad + "' " );
                sql = sql.replaceAll("#UNIDAD_2#", "   substr(cuenta, 7,3) = '" + unidad + "' and " );
            }

            st           = con.prepareStatement( sql );
            st.setString( 1, informe );
            st.setString( 2, distrito );
            st.setString( 3, periodo );
            st.setString( 4, distrito );
            st.setString( 5, anio );
            st.setString( 6, informe);
            st.setString( 7, anio );
            st.setString( 8, informe);
            st.setString( 9, informe);



            st.executeUpdate();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CREACION DEL LA TABLA TEMPORAL MAYOR TERCERO \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
    }




    public void creaMovimientoTercero(String distrito, String anio, String formato, String unidad ) throws SQLException {

        PreparedStatement st       = null;
        Connection        con      = null;


        String            query    = "SQL_CREA_MOVIMIENTO_TERCERO";
        String periodo = anio + "%";

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, formato );
            st.setString( 2, distrito );
            st.setString( 3, periodo );




            st.executeUpdate();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CREACION DEL LA TABLA TEMPORAL MOVIMIENTO TERCERO \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

    }

    public void setMayorTercero(String ano, String mes) throws SQLException {

        PreparedStatement st       = null;
        Connection        con      = null;


        String            query    = "SQL_SET_MAYOR_TERCERO";


        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            
            sql = sql.replaceAll("#CAMPO#", "mov"+mes);

            st  = con.prepareStatement( sql );
            st.setString( 1, ano + mes );

            st.executeUpdate();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION  DEL LA TABLA TEMPORAL MAYOR TERCERO \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
    }




    public void creaMayorBalance(String distrito,String anio,String valorAcumulado, String valorAcumuladoAnioAnterior)throws SQLException         {

        PreparedStatement st       = null;
        Connection        con      = null;

        String            query    = "SQL_CREA_MAYOR_BALANCE";

        try {

            String anioAnterior = Integer.toString( Integer.valueOf(anio).intValue() - 1 );

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );

            sql = sql.replaceAll("#PERIODOS_ACUMULADOS#", valorAcumulado);
            sql = sql.replaceAll("#PERIODOS_ACUMULADOS_ANIO_ANTERIOR#", valorAcumuladoAnioAnterior);

            st           = con.prepareStatement( sql );
            st.setString( 1, distrito );
            st.setString( 2, anio );
            st.setString( 3, distrito );
            st.setString( 4, anioAnterior );
            st.setString( 5, distrito );
            st.setString( 6, anioAnterior );

            st.executeUpdate();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CREACION DEL LA TABLA TEMPORAL MAYOR TERCERO \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }


    }



    /**
     * Lista las acciones de un contratista sin prefacturar
     * @id_contratista Identificacion del contratista, codigo interno

     */


    public List getEstadoFinanciero2(String distrito, String informe)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_ESTADO_FINANCIERO_2";
        List listaEstadoFinanciero2 = null;

        try {


            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, distrito );
            st.setString( 2, distrito );
            st.setString( 3, informe );

            rs = st.executeQuery();

            listaEstadoFinanciero2 =  new LinkedList();

            while (rs.next()){
                listaEstadoFinanciero2.add(EstadoFinanciero2.load(rs));
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS PARA GENERACION ESTADOS FINANCIEROS 1. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return listaEstadoFinanciero2;
    }

    public List validaCuadreContable(String distrito,String anio,String mes)throws SQLException {
        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_VERIFICA_CUADRE_CONTABLE";
        List listaTotales = null;
        String periodo = anio + mes;

        try {


            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            sql = sql.replaceAll("#MES#", mes);
            st           = con.prepareStatement( sql );
            st.setString( 1, distrito );
            st.setString( 2, periodo );
            st.setString( 3, distrito );
            st.setString( 4, periodo );
            st.setString( 5, distrito );
            st.setString( 6, anio );

            rs = st.executeQuery();

            listaTotales =  new LinkedList();

            while (rs.next()){
                listaTotales.add(TotalesContables.load(rs));
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS PARA GENERACION ESTADOS FINANCIEROS 1. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }


        return listaTotales;
    }

public boolean validaExistenciaComprobante(String distrito,String tipo_documento, String documento)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet         rs       = null;

        String            query    = "SQL_EXISTE_COMPROBANTE";
        boolean existe = false;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );

            st           = con.prepareStatement( sql );
            st.setString( 1, distrito );
            st.setString( 2, tipo_documento );
            st.setString( 3, documento );
            rs = st.executeQuery();
            if (rs.next()){
                existe = true;
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE VERIFICACION DE LA EXISTENCIA DE UN COMPROBANTE \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return existe;
}



    public void creaResumenPuc(String distrito,String anio,String mes)throws SQLException {
        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet         rs       = null;

        String            query    = "SQL_CREA_RESUMEN_PUC";

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            sql = sql.replaceAll("#MES#", mes);
            st           = con.prepareStatement( sql );
            st.setString( 1, distrito );
            st.setString( 2, distrito );
            st.setString( 3, anio );

            st.executeUpdate();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CREACION DE UN RESUMEN PUC. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

    }



    public int getSecuencia(String nombreSecuencia)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet         rs       = null;

        String            query    = "SQL_GET_SECUENCIA";
        int secuencia = 0;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            sql = sql.replaceAll("#NOMBRE_SECUENCIA#", nombreSecuencia);
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();
            if (rs.next()){
                secuencia = rs.getInt("secuencia");
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE UNA SECUENCIA \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return secuencia;

    }

    public String setCabecera(String distrito, String usuario, String anio, String mesCaracter, int grupoTransaccion, String detalle, String numero) throws SQLException{


        PreparedStatement st       = null;
        Connection        con      = null;

        String      comando_sql  =  "";

        String            query    = "SQL_CABECERA_COMPROBANTE";

        String            numdoc   = "CD"+ anio.substring(2) + mesCaracter + numero;


        int diaFinalMes = Util.diasDelMes( Integer.parseInt(mesCaracter) , Integer.parseInt( anio )) ;
        String fechaDocumento = anio + "-" + mesCaracter + "-" + Util.ceroPad(diaFinalMes, 2);



        try {
            //con   = this.conectarJNDI( query );
            con   = this.conectar( query );
            String  sql  = obtenerSQL( query );
            sql = sql.replaceAll("#DISTRITO#", distrito);
            sql = sql.replaceAll("#NUMDOC#", numdoc);
            sql = sql.replaceAll("#GRUPO_TRANSACCION#", Integer.toString(grupoTransaccion) );
            sql = sql.replaceAll("#PERIODO#", anio+mesCaracter);
            sql = sql.replaceAll("#FECHA_DOCUMENTO#", fechaDocumento);
            sql = sql.replaceAll("#USUARIO#", usuario);
            sql = sql.replaceAll("#DETALLE#", detalle);
            st           = con.prepareStatement( sql );

            comando_sql = st.toString();



        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CREACION DE LA CABECERA DEL COMPROBANTE DE CIERRE \n " + e.getMessage());
        }
        finally{
            st.close();
            //this.desconectar(con);
            this.desconectar(query);
        }
        return comando_sql;

    }


    public String setDetalle(String distrito, String usuario, String anio, String mesCaracter, int grupoTransaccion, String detalle, String numero) throws SQLException{


        PreparedStatement st       = null;
        Connection        con      = null;

        String      comando_sql  =  "";

        String            query    = "SQL_DETALLE_COMPROBANTE";

        String            numdoc   = "CD"+ anio.substring(2) + mesCaracter + numero;

        try {
            //con   = this.conectarJNDI( query );
            con   = this.conectar( query );
            String  sql  = obtenerSQL( query );
            sql = sql.replaceAll("#DISTRITO#", distrito);
            sql = sql.replaceAll("#NUMDOC#", numdoc);
            sql = sql.replaceAll("#GRUPO_TRANSACCION#", Integer.toString(grupoTransaccion));
            sql = sql.replaceAll("#PERIODO#", anio+mesCaracter);
            sql = sql.replaceAll("#USUARIO#", usuario);
            sql = sql.replaceAll("#DETALLE#", detalle);

            st           = con.prepareStatement( sql );

            comando_sql = st.toString();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CREACION DEL DETALLE DEL COMPROBANTE DE CIERRE \n " + e.getMessage());
        }
        finally{
            st.close();
            //this.desconectar(con);
            this.desconectar(query);
        }
        return comando_sql;

    }



    public String eliminaComprobante(String tipoDocumento, String documento)throws SQLException{

        StringStatement st       = null;
        Connection        con      = null;
        String      comando_sql    =  "";

        String            query    = "SQL_ELIMINA_COMPROBANTE";

        try {
            //con   = this.conectarJNDI( query );
            con   = this.conectar( query );
            String  sql  = obtenerSQL( query );
            st = new StringStatement(sql,true);
            st.setString( 1, tipoDocumento );
            st.setString( 2, documento );
            st.setString( 3, tipoDocumento );
            st.setString( 4, documento );

            comando_sql = st.getSql();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ELIMINACION DE UN COMPROBANTE DE CIERRE \n " + e.getMessage());
        }
        finally{
            st = null;
            //this.desconectar(con);
            this.desconectar(query);
        }
        return comando_sql;


    }




    public String setMayor(String distrito, String usuario, String anio, String mes, int grupoTransaccion, final String NUMERO_CD) throws SQLException {

        StringStatement st       = null;
        String  comando_sql    =  "";
        String  query    = "SQL_SET_MAYOR";
        String  documento   = "CD"+ anio.substring(2) + mes + NUMERO_CD;
        String  tipoDocumento = "CDIAR";

        try {
            String  sql  = obtenerSQL( query );
            sql = sql.replaceAll("#MES#", mes);
            sql = sql.replaceAll("#DISTRITO#", distrito);
            sql = sql.replaceAll("#ANIO#", anio);
            sql = sql.replaceAll("#BASE#", "COL");
            sql = sql.replaceAll("#USUARIO#", usuario);

            st = new StringStatement( sql,true );

            st.setString( 1, tipoDocumento );
            st.setString( 2, documento );
            st.setInt( 3, grupoTransaccion );
            st.setString( 4, anio );
            st.setString( 5, tipoDocumento );
            st.setString( 6, documento );
            st.setInt( 7, grupoTransaccion );
            st.setString( 8, distrito );
            st.setString( 9, anio );
            st.setString( 10, usuario );
            st.setString( 11, tipoDocumento );
            st.setString( 12, documento );
            st.setInt( 13, grupoTransaccion );

            comando_sql = st.getSql();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DEL MAYOR CON EL COMPROBANTE DE CIERRE \n " + e.getMessage());
        }
        finally{
            st = null;
            //this.desconectar(con);
            this.desconectar(query);
        }
        return comando_sql;

    }

    public String setReversaMayor(String distrito, String usuario, String anio, String mes, final String NUMERO_CD) throws SQLException {

        StringStatement st       = null;
        String      comando_sql    =  "";
        String            query    = "SQL_SET_REVERSA_MAYOR";
        String            documento   = "CD"+ anio.substring(2) + mes + NUMERO_CD;
        String            tipoDocumento = "CDIAR";

        try {
            String  sql  = obtenerSQL( query );
            sql = sql.replaceAll("#MES#", mes);
            st = new StringStatement(sql,true);

            st.setString( 1, tipoDocumento );
            st.setString( 2, documento );
            st.setString( 3, distrito );
            st.setString( 4, anio );
            st.setString( 5, tipoDocumento );
            st.setString( 6, documento );

            comando_sql = st.getSql();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA REVERSION EN EL MAYOR CON EL COMPROBANTE DE CIERRE \n " + e.getMessage());
        }
        finally{
            st = null;
            //this.desconectar(con);
            this.desconectar(query);
        }
        return comando_sql;

    }



    public void creaResumenPucAnual(String distrito,String anio,String mes, final String CUENTA_UTILIDAD)throws SQLException {
        PreparedStatement st       = null;
        Connection        con      = null;

        String            query    = "SQL_CREA_RESUMEN_PUC_ANUAL";

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            sql = sql.replaceAll("#CUENTA_UTILIDAD#", CUENTA_UTILIDAD);
            st           = con.prepareStatement( sql );
            st.setString( 1, distrito );
            st.setString( 2, anio );

            st.executeUpdate();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CREACION DE UN RESUMEN PUC. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

    }


     public String trasladaSaldos(String distrito, String usuario, String anio, String base) throws SQLException {

        StringStatement st       = null;
        Connection        con      = null;

        String      comando_sql    =  "";
        String            query    = "SQL_CREA_CUENTAS_MAYOR";


        String anioSiguiente = Integer.toString(Integer.parseInt(anio) + 1);

        try {
            //con   = this.conectarJNDI( query );
            con   = this.conectar( query );
            String  sql  = obtenerSQL( query );
            sql = sql.replaceAll("#DISTRITO#", distrito);
            sql = sql.replaceAll("#ANIO_SIGUIENTE#", anioSiguiente);
            sql = sql.replaceAll("#USUARIO#", usuario);
            sql = sql.replaceAll("#BASE#", base);

            st = new StringStatement( sql,true );

            st.setString( 1, distrito );
            st.setString( 2, anio );
            st.setString( 3, distrito );
            st.setString( 4, anioSiguiente );
            st.setString( 5, distrito );
            st.setString( 6, anioSiguiente );

            st.setString( 7, distrito );
            st.setString( 8, anio );
            st.setString( 9, anioSiguiente );

            comando_sql = st.getSql();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE EL TRASLADO DE CUENTAS AL SIGUIENTE ANO \n " + e.getMessage());
        }
        finally{
            st = null;
            //this.desconectar(con);
            this.desconectar(query);
        }
        return comando_sql;



     }






}
