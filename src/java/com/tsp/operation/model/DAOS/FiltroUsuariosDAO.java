/********************************************************************
 *      Nombre Clase.................    FiltroUsuariosDAO.java
 *      Descripci�n..................   lase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  David A
 */
public class FiltroUsuariosDAO {
    
    private Vector vecFiltros;
    
    /** Creates a new instance of FiltroUsuarios */
    public FiltroUsuariosDAO () {
    }
    
    public Vector obtVecFiltrosU () {
        return this.vecFiltros;
    }
    
    private static final String ELIMINAR="delete from filtrousuarios where oid=?";
    private static final String CONS="select oid,idusuario,consulta,linea,nombre, filtro from filtrousuarios  where idusuario=?";
    private static final String FILTROS_ID="select oid,idusuario,consulta,linea,nombre,filtro from filtrousuarios  where oid=?";
    private static final String INSERTAR= "insert into filtrousuarios(idusuario,consulta,linea, nombre,creation_user,dstrct,filtro ) values(?,?,?,?,?,?,?)";
    
    
    /**
     * Este m�todo se encarga de listar los filtros creados por el usuario en el programa de control trafico
     * @author  dlamadrid
     * @version 1.0
     * @param ConfTraficoU c
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void generarFiltrosU (String campo) throws SQLException {
        ////System.out.println ("entro a generar filtros Usuarios");
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        vecFiltros = null;
        try {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null) {
                st = con.prepareStatement (CONS);
                st.setString (1, campo);
                vecFiltros = new Vector ();
                rs = st.executeQuery ();
                
                while (rs.next ()) {
                    Vector fila = new Vector ();
                    fila.add (rs.getString ("oid"));
                    fila.add (rs.getString ("idusuario"));
                    fila.add (rs.getString ("consulta"));
                    fila.add (rs.getString ("nombre"));
                    fila.add (rs.getString ("linea"));
                    fila.add (rs.getString ("filtro"));
                    
                    vecFiltros.add (fila);
                }
            }
        }catch(SQLException e) {
            throw new SQLException ("ERROR AL LISTAR IDENTIDAD" + e.getMessage ()+"" + e.getErrorCode ());
        }finally {
            if(st != null) {
                try {
                    st.close ();
                }
                catch(SQLException e) {
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null) {
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
    
    /**
     * Este m�todo se encarga de encontrar un filtros creados dado el codigo del filtro
     * @author  ing David lamadrid
     * @version 1.0
     * @param String id
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void generarFiltrosC (String id) throws SQLException {
        ////System.out.println ("entro a generar filtros Usuarios");
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        vecFiltros = null;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (FILTROS_ID);
                st.setString (1, id);
                
                vecFiltros = new Vector ();
                rs = st.executeQuery ();
                
                while (rs.next ()){
                    Vector fila = new Vector ();
                    fila.add (rs.getString ("oid"));
                    fila.add (rs.getString ("idusuario"));
                    fila.add (rs.getString ("consulta"));
                    fila.add (rs.getString ("nombre"));
                    fila.add (rs.getString ("linea"));
                    fila.add (rs.getString ("filtro"));
                    vecFiltros.add (fila);
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL LISTAR IDENTIDAD" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
    
    /**
     * Este m�todo se encarga de insertar un filtro con objeto tipo FiltrosUsuarios 
     * @author  ing David lamadrid
     * @version 1.0
     * @param String id
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void insertarFiltro (FiltrosUsuarios u) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        try {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null) {
                st = con.prepareStatement (INSERTAR);
                st.setString (1, u.getIdUsuario ());
                st.setString (2, u.getConsulta ());
                st.setString (3, u.getLinea ());
                st.setString (4, u.getNombre ());
                st.setString (5, u.getCreation_user ());
                st.setString (6,u.getDstrct ());
                st.setString (7,u.getFiltro());
                st.executeUpdate ();
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL LISTAR IDENTIDAD" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null) {
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
        
    }

    /**
     * Este m�todo se encarga de elminar un filtro dado el codigo del mismo 
     * @author  ing David lamadrid
     * @version 1.0
     * @param String id
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void eliminarFiltro (String id) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null) {
                st = con.prepareStatement (ELIMINAR);
                st.setString (1,id);
                st.executeUpdate ();
            }
        }catch(SQLException e) {
            throw new SQLException ("ERROR AL LISTAR IDENTIDAD" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null) {
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
    
    
    
}
