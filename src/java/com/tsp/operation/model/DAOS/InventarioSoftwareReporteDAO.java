/******************************************************************
* Nombre ......................ReporteInventarioSoftwareDAO.java
* Descripci�n..................Clase DAO para realizar el 
                               inventario de archivos slt
* Autor........................Ing. Armando Oviedo
* Fecha........................30/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  Armando Oviedo
 */
public class InventarioSoftwareReporteDAO {
    
    private Connection con;
    private PoolManager pm;
    private InventarioSoftware is;
    private Vector VectorProgramas;   
    
    
    // Fernel 07/01/2006
    public final String TABLA_COMPUTO  = "COMPUTO";
    public final String TABLA_SITIOS   = "SITIOS";
    public final String SEPARADOR      = "�-�";
    
    
    
    
    
   /* ________________________________________________________________________________________
     *                                  ATRIBUTOS
     * ________________________________________________________________________________________ */
    
   
    
    private static String SQL_GRABAR_INVENTARIO     =   "INSERT INTO  inv.inventario_software         " +
                                                        "(dstrct, equipo, sitio, fecha, nombre_archivo, ruta, tamano, extension, base, creation_user, user_update) " +
                                                        " VALUES(?,?,?,?,?,?,?,?,?,?,?)               ";
    
    private static String SQL_EXISTE_REPORTE        =   "SELECT nombre_archivo                                                FROM inv.inventario_software   WHERE equipo= ?  AND sitio = ?   limit 1 ";
    private static String SQL_BUSCARINVENTARIO      =   "SELECT equipo, sitio, fecha, nombre_archivo, ruta, tamano, extension FROM inv.inventario_software   WHERE equipo= ?  AND sitio = ?";
    private static String SQL_DELETE                =   "DELETE                                                               FROM inv.inventario_software   WHERE equipo= ?  AND sitio = ?";
    
    
    
    
    
    
    
    // Fernel 07/01/2006
    private static String SQL_SEARCH_TABLA_GEN  =    
                                                   " SELECT  table_code  , descripcion,  referencia  "+
                                                   " FROM    tablagen  WHERE  table_type = ?         "+
                                                   " ORDER BY table_code                             ";  

    
    
    
    /* ________________________________________________________________________________________
     *                                  METODOS
     * ________________________________________________________________________________________ */
   
    
    
                                                   
                                                   
                                                        
     /**
     * M�todo que captura los diferentes tipo de tabla de una tabla especifica en tabla general
     * @autor.......Fernel 07/01/2006           
     * @throws......Exception
     * @version.....1.0.
     * @param.......String tipo
     **/
      public  List  getTables (String tipo) throws Exception {
           
            PoolManager poolManager  = PoolManager.getInstance();
            Connection  conn         = poolManager.getConnection("fintra");
            if (conn == null)
                throw new SQLException("Sin conexion");
            PreparedStatement  st     = null;
            ResultSet          rs     = null;
            List               lista  = new LinkedList();        
            try{
                    String  tabla = (tipo.equals(this.TABLA_COMPUTO))?this.TABLA_COMPUTO : this.TABLA_SITIOS;
                    st= conn.prepareStatement(this.SQL_SEARCH_TABLA_GEN);
                    st.setString(1, tabla);
                    rs=st.executeQuery();
                    while(rs.next()){
                       String campo = rs.getString(1) + SEPARADOR + rs.getString(2);
                       lista.add(campo);
                    }

            }catch(Exception e){
                throw new SQLException("Error getTables() : "+ e.getMessage());
            }
            finally{
                if(st!=null) st.close();
                if(rs!=null) rs.close();
                poolManager.freeConnection("fintra",conn);
            }
            return lista;
    }                                             
                                                   
                                                   
    
    
    
    
    
    
    
   
    
    /**
     * M�todo que setea un objeto de tipo InventarioSoftware
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......InventarioSoftware is
     **/
    public void setInventarioSoftware(InventarioSoftware is){
        this.is = is;        
    }
    
    /** Creates a new instance of InventarioSoftwareReporteDAO */
    public InventarioSoftwareReporteDAO() {
    }
    
    /**
     * M�todo que borra todos los datos contenidos en la tabla de inventario software
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/
    public void deleteTablaInventarioSoftware(String equipo, String sitio) throws SQLException{
        PreparedStatement ps = null;        
        try{            
            pm = PoolManager.getInstance();
            con = pm.getConnection("inventario");            
            ps = con.prepareStatement(SQL_DELETE);
            ps.setString(1, equipo);
            ps.setString(2, sitio);          
            ps.executeUpdate();
        }catch(Exception ex){
            ex.printStackTrace();
        }finally{
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("inventario", con );
            }
        }
    }
    
    /**
     * M�todo que devuelve un vector de tipo inventario software
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/
    public Vector getVectorProgramas(){
        return this.VectorProgramas;
    }
    
    /**
     * M�todo que graba un vector de tipo inventario software en la base de datos
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......Vector listaProgramas
     **/
    public void grabarInventario(Vector listaProgramas) throws SQLException{
        PreparedStatement ps = null;        
        try{
            con = pm.getConnection("inventario"); 
            pm  = PoolManager.getInstance();
            ps = con.prepareStatement(SQL_GRABAR_INVENTARIO);
            
            for(int i=0;i<listaProgramas.size();i++){                
                InventarioSoftware is = (InventarioSoftware)(listaProgramas.elementAt(i));
                
                ps.setString    (1,  is.getDstrct() );
                ps.setString    (2,  is.getEquipo() );
                ps.setString    (3,  is.getSitio()  );                
                ps.setTimestamp (4,  new java.sql.Timestamp( is.getFecha().getTime()) );
                ps.setString    (5,  is.getNombre());
                ps.setString    (6,  is.getRuta());
                ps.setLong      (7,  is.getTamano());
                ps.setString    (8,  is.getExtension());
                ps.setString    (9,  is.getBase());
                ps.setString    (10, is.getCreation_user() );
                ps.setString    (11, is.getUserUpdate());                
                ps.execute();
                
                ps.clearParameters();
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }finally{
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("inventario", con );
            }
        }
    }
    
    /**
     * M�todo que retorna un boolean si existen o no items en la base de datos
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/
    public boolean existeReporte(String equipo, String sitio) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean existe = false;
        try{
            pm = PoolManager.getInstance();
            con = pm.getConnection("inventario");
            ps = con.prepareStatement(SQL_EXISTE_REPORTE);  
            ps.setString(1, equipo);
            ps.setString(2, sitio);
            rs = ps.executeQuery();
            if(rs.next()){
                existe = true;
            }            
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
           if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("inventario", con );
            } 
        }
        return existe;     
    }
    
    /**
     * M�todo que forma un vector de objetos tipo: InventarioSoftware
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......VehNoRetornado de
     **/
    public void obtenerReporteBaseDatos(String equipo, String sitio) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        VectorProgramas = new Vector();
        try{
            pm = PoolManager.getInstance();
            con = pm.getConnection("inventario");
            ps = con.prepareStatement(SQL_BUSCARINVENTARIO);
            ps.setString(1, equipo);
            ps.setString(2, sitio);
            rs = ps.executeQuery();
            while(rs.next()){                
                InventarioSoftware tmp = new InventarioSoftware();                                                                
                tmp.setEquipo     (equipo);
                tmp.setSitio      (sitio);
                tmp.setNombre     (rs.getString("nombre"));                
                tmp.setFecha      (new java.util.Date(rs.getTimestamp("fecha").getTime()));
                tmp.setTamano     (rs.getLong("tamano"));
                tmp.setRuta       (rs.getString("ruta"));
                tmp.setExtension  (rs.getString("extension"));
                tmp.setProfundidad(profundidad(tmp.getRuta()));                
                VectorProgramas.add(tmp);                                                
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                pm.freeConnection("inventario", con );
            } 
        }
    }   
    
    /**
     * M�todo que retorna el nombre de un archivo sin su extensi�n
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......String nombre
     **/
    public String getNombre(String nombre){
        String name[] = nombre.split("\\.");
        return name[0];
    }
    
    /**
     * M�todo que retorna un vector de Strings que contiene
     * la cadena de la ruta del archivo, dividida por el caracter / 
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......String path
     **/
    public String[] profundidad(String path){
        String cadena[] = path.split("\\\\");         
        return (cadena);        
    }
    
    /**
     * M�todo que obtiene la extensi�n de un archivo
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......String nombre completo del archivo sin la ruta (String filename)
     **/
    public String getExtension(String filename){        
        String[] cadena = filename.split("\\.");
        String extension = "";
        if(cadena.length>=2){
            extension = cadena[cadena.length-1];
        }
        return extension;
    }
}
