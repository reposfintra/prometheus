/***********************************************************************************
 * Nombre clase : ............... RetrasoSalidaDAO.java                                   *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.                                       *
 * Autor :....................... Ing. David Lamadrid                               *
 * Fecha :....................... 15 de diciembre de 2005, 11:02 AM                   *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  dlamadrid
 */
public class RetrasoSalidaDAO extends MainDAO
{
    
    /** Creates a new instance of RetrasoSalidaDAO */
    public RetrasoSalidaDAO ()
    {super("RetrasoSalidaDAO.xml");
    }
     private Vector vRetrasos;
     private RetrasoSalida retraso;
    /**
     * Getter for property vRetrasos.
     * @return Value of property vRetrasos.
     */
    public java.util.Vector getVRetrasos ()
    {
        return vRetrasos;
    }
    
    /**
     * Setter for property vRetrasos.
     * @param vRetrasos New value of property vRetrasos.
     */
    public void setVRetrasos (java.util.Vector vRetrasos)
    {
        this.vRetrasos = vRetrasos;
    }
    
    /**
     * Getter for property retraso.
     * @return Value of property retraso.
     */
    public com.tsp.operation.model.beans.RetrasoSalida getRetraso ()
    {
        return retraso;
    }
    
    /**
     * Setter for property retraso.
     * @param retraso New value of property retraso.
     */
    public void setRetraso (com.tsp.operation.model.beans.RetrasoSalida retraso)
    {
        this.retraso = retraso;
    }
    
  /*  private static String INSERTAR="insert into  retraso_salida (dstrct,tipo,planilla,causa,descripcion,c_actual,c_nuevo,last_update,creation_date,creation_user) values(?,?,?,?,?,?,?,'0099-01-01 00:00:00',now(),?)";
    private static String EXISTE="";
    private static String ACTUALIZARTRAFICO="update trafico set fecha_salida=? where planilla=?";
   
    private static String ACTUALIZARNIT="update nit set celular=? where cedula=?";
    private static String ACTUALIZAR_INGRESO_C="update ingreso_trafico set cel_cond=? where planilla=?";
    //Diogenes  
    private static String BUSCAR_TIEMPO_TRAMO="   SELECT COALESCE ( tiempo , 0 ) as tiempo" +
                                                "  FROM ingreso_trafico a," +
                                                "          tramo b            " +
                                                "     where     " +
                                                "	      a.planilla = ?" +
                                                "        AND   b.dstrct = ? " +
                                                "        AND   b.origin = a.pto_control_ultreporte" +
                                                "        AND   b.destination = a.pto_control_proxreporte";
    
     private static String ACTUALIZARINGRESOT="UPDATE ingreso_trafico " +
                                                "set fecha_salida = ? ," +
                                                "   fecha_ult_reporte = ?," +
                                                "   fecha_prox_reporte = ? ::  TIMESTAMP + CAST( ? AS INTERVAL) " +
                                                "    where planilla = ? ";*/
     
    /**
     * Metodo insertarRetrazo, Metodo que permite la insercion de un registro en la tabla retraso_salida,previamente aya sido seteado un objeto tipo RetrasoSalida.
     * @autor : Ing. David Lamadrid
     * @param : 
     * @version : 1.0
     */
    public void insertarRetrazo () throws Exception 
    {
        Connection con = null;
        Statement st = null;
        PreparedStatement ps = null;
        ResultSet rs         = null;
        try
        {
            
            con = conectar("SQL_INSERTAR");
            con.setAutoCommit (false);
            st = con.createStatement ();
            
            ps = con.prepareStatement (this.obtenerSQL("SQL_INSERTAR"));
            ps.setString (1, this.retraso.getDstrct ());
            ps.setString (2, this.retraso.getTipo ());
            ps.setString (3, this.retraso.getPlanilla ());
            ps.setString (4, this.retraso.getCausa ());
            ps.setString (5, this.retraso.getDescripcion ());
            ps.setString (6, this.retraso.getC_actual ());
            ps.setString (7, this.retraso.getC_nuevo ());
            ps.setString (8, this.retraso.getCreation_user ());
            st.addBatch (ps.toString ());
            ////System.out.println (""+ps.toString ());
            
            

           
            if(this.retraso.getTipo ().equals ("salida")){
                double tiempo = buscarTiempoTramo(this.retraso.getDstrct (),this.retraso.getPlanilla());
                ps = con.prepareStatement (this.obtenerSQL("SQL_ACTUALIZARINGRESOT"));
                ps.setString (1,this.retraso.getC_nuevo ());
                ps.setString (2,this.retraso.getC_nuevo ());
                ps.setString (3,this.retraso.getC_nuevo ());
                ps.setString (4, tiempo+" HOUR" );
                ps.setString (5,this.retraso.getPlanilla ());
                ////System.out.println("Actualiza Ingreso Trafico "+ps.toString());
                st.addBatch (ps.toString ());
                
                ////System.out.println("entro en actualizar trafico "+this.retraso.getPlanilla ());
                ps = con.prepareStatement (this.obtenerSQL("SQL_ACTUALIZARTRAFICO"));
                ps.setString (1,this.retraso.getC_nuevo ());
                ps.setString (2,this.retraso.getPlanilla ());
                st.addBatch (ps.toString ());
                ////System.out.println (""+ps.toString ());
            }
            else{
                ps = con.prepareStatement (this.obtenerSQL("SQL_ACTUALIZAR_INGRESO_C"));
                ps.setString (1,this.retraso.getC_nuevo ());
                ps.setString (2,this.retraso.getPlanilla ());
                st.addBatch (ps.toString ());
                
                ps = con.prepareStatement (this.obtenerSQL("SQL_ACTUALIZARNIT"));
                ps.setString (1,this.retraso.getC_nuevo ());
                ps.setString (2,this.retraso.getCedulaC ());
                st.addBatch (ps.toString ());
                
            }
            ////System.out.println("Ejecuto Batch");
            
            st.executeBatch ();
            
            con.commit ();
            con.setAutoCommit (true);
            
        }
        catch(Exception e)
        {
            
            if(con!=null)
            {
                con.rollback ();
            }
            throw new SQLException ("Error en rutina UPDATE  [RetrasoDAO].... \n"+ e.getMessage ());
            
        }
        finally
        {
            if(st!=null) st.close ();
            if(rs!=null) rs.close ();
           desconectar("SQL_INSERTAR");
        } 
    }
    
    /**
     * Metodo buscarTiempoTramo, busca el tiempo en horas que existe entre el origen 
     * y el destino de la planilla
     * Autor Diogenes Bastidas morales
     * @param String frase
     */
    public double buscarTiempoTramo(String dstrct, String planilla) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        double tiempo = 0.0;
        
        try{
                st = this.crearPreparedStatement("SQL_BUSCAR_TIEMPO_TRAMO");
                st.setString(1, planilla);
                st.setString(2, dstrct);
                ////System.out.println(st);
                rs = st.executeQuery();

                if (rs.next()){
                    tiempo = rs.getDouble("tiempo");
                }
                
           
        }catch(Exception e){
            throw new Exception("ERROR AL BUSCAR TIEMPO DE UNA PLANILLA" + e.getMessage());
        }finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_BUSCAR_TIEMPO_TRAMO");
        }
        return tiempo;
    }
    
    
    
    
}
