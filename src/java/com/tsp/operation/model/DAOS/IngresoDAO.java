/****
 * Nombre clase :              IngresoDAO.java
 * Descripcion :              Clase que maneja los DAO ( Data Access Object )
 *                            los cuales contienen los metodos que interactuan
 *                            con la BD.
 * Autor :                    Ing. Diogenes Antonio Bastidas Morales
 * Fecha :                    9 de mayo de 2006, 09:19 AM
 * Version :  1.0
 * Copyright : Fintravalores S.A.
 ****/


package com.tsp.operation.model.DAOS; 
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.EncriptionException;

public class IngresoDAO extends MainDAO {
    private Ingreso ingreso;
    private LinkedList listadoingreso;
    /** Creates a new instance of IngresoDAO */
    public IngresoDAO() {
        super("IngresoDAO.xml");
    }
    public IngresoDAO(String dataBaseName) {
        super("IngresoDAO.xml", dataBaseName);
    }
    
    /**
     * Metodo insertarIngreso, ingresa un registro en la tabla ingreso
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    
    public String insertarIngreso() throws Exception {
        StringStatement st = null;
        ResultSet rs = null;
        String sql = "";
        String query = "SQL_INGRESAR";
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, ingreso.getReg_status());
            st.setString(2, ingreso.getDstrct());
            st.setString(3, ingreso.getCodcli());
            st.setString(4, ingreso.getNitcli());
            st.setString(5, ingreso.getNum_ingreso());
            st.setString(6, ingreso.getConcepto());
            st.setString(7, ingreso.getTipo_ingreso());
            st.setString(8, ingreso.getFecha_consignacion());
            st.setString(9, ingreso.getFecha_ingreso());
            st.setString(10, ingreso.getBranch_code());
            st.setString(11, ingreso.getBank_account_no());
            st.setString(12, ingreso.getCodmoneda());
            st.setString(13, ingreso.getAgencia_ingreso());
            st.setString(14, ingreso.getPeriodo());
            st.setDouble(15, ingreso.getVlr_ingreso());
            st.setDouble(16, ingreso.getVlr_ingreso_me());
            st.setInt(17, ingreso.getTransaccion());
            st.setDouble(18, ingreso.getVlr_tasa());
            st.setString(19, ingreso.getFecha_tasa());
            st.setInt(20, ingreso.getCant_item());
            st.setString(21, ingreso.getCreation_user());
            st.setString(22, ingreso.getCreation_date());
            st.setString(23, ingreso.getUser_update());
            st.setString(24, ingreso.getLast_update());
            st.setString(25, ingreso.getBase());
            st.setInt(26, ingreso.getTransaccion_anulacion());
            st.setString(27, ingreso.getTipo_documento());
            st.setString(28, ingreso.getDescripcion_ingreso());
            st.setString(29, ingreso. getNro_consignacion());
            st.setString(30, ingreso.getCuenta());
            st.setString(31, ingreso.getAuxiliar());
            st.setDouble(32, ingreso.getTasaDolBol());
            st.setString(33, ingreso.getAbc());
            st.setDouble(34, ingreso.getVlr_saldo_ing());
            st.setString(35, ingreso.getCmc());
            st.setInt(36,ingreso.getNro_extracto());
            sql = st.getSql();
        }catch(Exception e) {
            throw new Exception("Error al Insertar Ingreso [IngresoDAO.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return sql;
    }
    
    /**
     * Metodo modificarIngreso, modifica un registro en la tabla ingreso
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void modificarIngreso() throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_MODIFICAR";
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, ingreso.getFecha_consignacion());
                st.setString(2, ingreso.getBranch_code());
                st.setString(3, ingreso.getBank_account_no());
                st.setString(4, ingreso.getCodmoneda());
                st.setDouble(5, ingreso.getVlr_ingreso());
                st.setDouble(6, ingreso.getVlr_ingreso_me());
                st.setDouble(7, ingreso.getVlr_tasa());
                st.setString(8, ingreso.getFecha_tasa());
                st.setString(9, ingreso.getConcepto());
                st.setString(10, ingreso.getDescripcion_ingreso());
                st.setString(11, ingreso.getNro_consignacion());
                st.setString(12, ingreso.getUser_update());
                st.setString(13, ingreso.getLast_update());
                st.setString(14, ingreso.getCuenta());
                st.setString(15, ingreso.getAuxiliar());
                st.setDouble(16, ingreso.getTasaDolBol());
                st.setString(17, ingreso.getAbc());
                st.setDouble(18, ingreso.getVlr_saldo_ing());
                st.setString(19, ingreso.getDstrct());
                st.setString(20, ingreso.getNum_ingreso());
                st.setString(21, ingreso.getTipo_documento());
                st.executeUpdate();
            }
        }catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error al MODIFICAR Ingreso [IngresoDAO.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo buscarSerie, busca el numero del ingreso
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public String  buscarSerie(String tipo) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        PreparedStatement st2 = null;
        ResultSet rs = null;
        int num = 0;
        String serie = "";
        String query = "SQL_BUSCAR_SERIE";

        try{
            con = this.conectarJNDI(query);
           if(con!=null){
                        st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                        st.setString(1, tipo);
                        rs = st.executeQuery();
                       if(rs.next()){
                                    num = rs.getInt("last_number");
                                    serie = rs.getString("prefix")+Util.llenarConCerosALaIzquierda(num,6);
                        }else{
                            serie="";
                        }

                                
              if ( num > 0 ){
                    st2 = con.prepareStatement(this.obtenerSQL("SQL_INC_SERIE"));//JJCastro fase2
                    st2.setString(1, tipo);
                    st2.executeUpdate();
                }
            
            
        }}catch(Exception e) {
            throw new Exception("Error al buscar la serie Ingreso [IngresoDAO.xml]... \n"+e.getMessage());
        }
finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (st2  != null){ try{ st2.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return serie;
    }
    
    /**
     * Getter for property ingreso.
     * @return Value of property ingreso.
     */
    public com.tsp.operation.model.beans.Ingreso getIngreso() {
        return ingreso;
    }
    
    /**
     * Setter for property ingreso.
     * @param ingreso New value of property ingreso.
     */
    public void setIngreso(com.tsp.operation.model.beans.Ingreso ingreso) {
        this.ingreso = ingreso;
    }
    
  
    
    /**
     * Metodo tieneItemsIngreso, verifica si tiene items un ingreso
     * @param: distrito, tipodocumento,nroingreso
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean tieneItemsIngreso(String dstrct, String tipodocumento, String nroingreso ) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean valor = false;
        String query = "SQL_TIENE_ITEMS";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, tipodocumento );
            st.setString(3, nroingreso);
            rs = st.executeQuery();
            
            if(rs.next()){
                valor = true;
            }
            
            }}catch(Exception e) {
            throw new Exception("Error al Buscar Items Ingreso [IngresoDAO.xml]... \n"+e.getMessage());
        }
finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return valor;
    }
    
    /**
     * M�todo que busca las facturas asociadas a un ingreso
     * @autor.......Jose de la rosa
     * @throws......Exception
     * @version.....1.0.
     **/
    public void consultaFacturasIngresos(String distrito, String factura )throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        listadoingreso             = null;
        String             query   = "SQL_CONSULTA_FACTURA_OTRO_INGRESOS";
        try{

            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito);
            st.setString(2, factura);
            System.out.println("Q1 "+st.toString());
            rs = st.executeQuery();
            listadoingreso = new LinkedList();
            while(rs.next()){
                ingreso = new Ingreso();
                ingreso.setNum_ingreso(rs.getString("num_ingreso"));
                ingreso.setNomCliente(rs.getString("cliente")!=null?rs.getString("cliente"):"");
                ingreso.setFecha_consignacion(rs.getString("creation_date"));
                ingreso.setVlr_ingreso_me(rs.getDouble("valor_ingreso_me"));
                ingreso.setBranch_code(rs.getString("branch_code"));
                ingreso.setBank_account_no( rs.getString("bank_account_no") );
                ingreso.setCodmoneda( rs.getString("codmoneda") );
                ingreso.setConcepto ( rs.getString("factura")!=null?rs.getString("factura"):"" );
                listadoingreso.add(ingreso);
            }
            }}catch(Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }             
        
    /*Modificacion dbastidas 19 dic*/
    
    /**
     * Metodo buscarIngreso, modifica un registro en la tabla ingreso
     * @param: distrito, numero ingreso
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarIngreso(String dstrct, String tipodoc, String num_ingreso ) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ingreso = null;
        String query = "SQL_BUSCAR";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, tipodoc);
            st.setString(3, num_ingreso );
            st.setString(4, dstrct);
            st.setString(5, tipodoc);
            st.setString(6, num_ingreso );
            rs = st.executeQuery();
            if(rs.next()){
                ingreso = new Ingreso();
                ingreso.setDstrct(rs.getString("dstrct"));
                ingreso.setCodcli(rs.getString("codcli"));
                ingreso.setNomCliente(rs.getString("nomcli"));
                ingreso.setNitcli(rs.getString("nitcli"));
                ingreso.setNum_ingreso(rs.getString("num_ingreso"));
                ingreso.setConcepto(rs.getString("concepto"));
                ingreso.setTipo_ingreso(rs.getString("tipo_ingreso"));
                ingreso.setFecha_consignacion(rs.getString("fecha_consignacion"));
                ingreso.setFecha_ingreso(rs.getString("fecha_ingreso"));
                ingreso.setBranch_code(rs.getString("branch_code"));
                ingreso.setBank_account_no(rs.getString("bank_account_nos")!=null?rs.getString("bank_account_nos"):"/");
                ingreso.setCodmoneda(rs.getString("codmoneda"));
                ingreso.setAgencia_ingreso(rs.getString("agencia_ingreso"));
                ingreso.setVlr_ingreso(rs.getDouble("vlr_ingreso"));
                ingreso.setVlr_ingreso_me(rs.getDouble("vlr_ingreso_me"));
                ingreso.setTransaccion(rs.getInt("transaccion"));
                ingreso.setFecha_tasa(rs.getString("fecha_tasa"));
                ingreso.setDescripcion_ingreso(rs.getString("descripcion_ingreso"));
                ingreso.setCant_item(rs.getInt("cant_item"));
                ingreso.setTipo_documento(rs.getString("tipo_documento"));
                ingreso.setFecha_anulacion(rs.getString("fecha_anulacion"));
                ingreso.setFecha_contabilizacion(rs.getString("fecha_contabilizacion"));
                ingreso.setDestipo(rs.getString("destipo"));
                ingreso.setVlr_saldo_ing(rs.getDouble("saldo_ing"));
                ingreso.setReg_status(rs.getString("reg_status"));
                ingreso.setTransaccion(rs.getInt("transaccion"));   
                ingreso.setCuenta(rs.getString("cuenta"));
                ingreso.setAbc(rs.getString("abc"));
                ingreso.setTasaDolBol(rs.getDouble("tasa_dol_bol"));
                ingreso.setCreation_user(rs.getString("creation_user"));
                ingreso.setNro_extracto(rs.getInt("nro_extracto"));
                if(rs.getString("auxiliar").length() > 0 ){
                    String[] aux = rs.getString("auxiliar").split("-");
                    if(aux.length>1){
                        ingreso.setTipo_aux(aux[0]);
                        ingreso.setAuxiliar(aux[1]);
                    }else{
                        ingreso.setTipo_aux(aux[0]);
                        ingreso.setAuxiliar("");                    
                    }
                }else{
                    ingreso.setTipo_aux("");
                    ingreso.setAuxiliar("");
                }
                
            }
            
            
            }}catch(Exception e) {
            throw new Exception("Error al Buscar Ingreso [IngresoDAO.xml]... \n"+e.getMessage());
        }
finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
   
    
    /**
     * Metodo buscarIngresosCliente, busca los ingresos del cliente
     * @param: codigo del cliente, fecha inicial, fecha final
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarIngresosCliente(String dstrct, String tipo, String codcli,String fecini, String fecfin,String estado ) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        listadoingreso = null;
        String query = "SQL_BUSCAR_INGRESOS";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, tipo);
            st.setString(3, codcli+"%");
            st.setString(4, fecini+" 00:00");
            st.setString(5, fecfin+" 23:59");
            st.setString(6, estado);
            
            listadoingreso = new LinkedList();
            rs = st.executeQuery();
            
            while(rs.next()){
                ingreso = new Ingreso();
                ingreso.setDstrct(dstrct);
                ingreso.setNum_ingreso(rs.getString("num_ingreso"));
                ingreso.setFecha_consignacion(rs.getString("fecha_consignacion"));
                ingreso.setFecha_ingreso(rs.getString("fecha_ingreso"));
                ingreso.setCodmoneda(rs.getString("codmoneda"));
                ingreso.setVlr_ingreso(rs.getDouble("vlr_ingreso"));
                ingreso.setVlr_ingreso_me(rs.getDouble("vlr_ingreso_me"));
                ingreso.setNomCliente(rs.getString("nomcli"));
                ingreso.setDestipo(rs.getString("destipo"));
                ingreso.setTipo_documento(rs.getString("tipo_documento"));
                ingreso.setVlr_saldo_ing(rs.getDouble("saldo_ing"));
                ingreso.setReg_status(rs.getString("reg_status"));
                ingreso.setBranch_code(rs.getString("branch_code")!=null?rs.getString("branch_code"):"");
                ingreso.setBank_account_no(rs.getString("bank_account_nos")!=null?rs.getString("bank_account_nos"):"");
                listadoingreso.add(ingreso);
                
            }
            
            
            }}catch(Exception e) {
            throw new Exception("Error al Buscar Ingreso [IngresoDAO.xml]... \n"+e.getMessage());
        }
finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Getter for property listadoingreso.
     * @return Value of property listadoingreso.
     */
    public java.util.LinkedList getListadoingreso() {
        return listadoingreso;
    }
    
    /**
     * Setter for property listadoingreso.
     * @param listadoingreso New value of property listadoingreso.
     */
    public void setListadoingreso(java.util.LinkedList listadoingreso) {
        this.listadoingreso = listadoingreso;
    }
    
    
    /**
     * Metodo buscarIngresos_NroFactura, busca los ingresos del cliente, deacuerdo a un numero de factura
     * @param: distito, numero de factura
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarIngresos_NroFactura(String dstrct, String factura ) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        listadoingreso = null;
        String query = "SQL_BUSCAR_INGRESO_FACTURA";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, factura);
            
            listadoingreso = new LinkedList();
            rs = st.executeQuery();
            
            while(rs.next()){
                ingreso = new Ingreso();
                ingreso.setDstrct(dstrct);
                ingreso.setNum_ingreso(rs.getString("num_ingreso"));
                ingreso.setFecha_consignacion(rs.getString("fecha_consignacion"));
                ingreso.setFecha_ingreso(rs.getString("fecha_ingreso"));
                ingreso.setCodmoneda(rs.getString("codmoneda"));
                ingreso.setVlr_ingreso(rs.getDouble("vlr_ingreso"));
                ingreso.setVlr_ingreso_me(rs.getDouble("vlr_ingreso_me"));
                ingreso.setNomCliente(rs.getString("nomcli"));
                ingreso.setReg_status(rs.getString("reg_status"));
                ingreso.setDestipo(rs.getString("destipo"));
                ingreso.setTipo_documento(rs.getString("tipo_documento"));
                ingreso.setVlr_saldo_ing(rs.getDouble("saldo_ing"));
                ingreso.setBranch_code(rs.getString("branch_code")!=null?rs.getString("branch_code"):"");
                ingreso.setBank_account_no(rs.getString("bank_account_nos")!=null?rs.getString("bank_account_nos"):"");
                listadoingreso.add(ingreso);
            }
            
            
            }}catch(Exception e) {
            throw new Exception("Error al Buscar Ingreso [IngresoDAO.xml]... \n"+e.getMessage());
        }
finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
     /**
     * M�todo que busca los ingresos de acuuerdo a unos filtros
     * @autor.......Jose de la rosa
     * @throws......Exception
     * @version.....1.0.
     **/
    
    public void consultaIngresos(String distrito, String cliente, String periodo, String tipo_ingreso, String fec_ini, String fec_fin, String tipo_doc, String doc, String estado, String no_ingreso)throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        listadoingreso             = null;
        String             filtro  = "", filtro2  = "";
        String             query   = "SQL_SEARCH_INGRESOS_CONSULTA";
        try{

            con = this.conectarJNDI(query);//JJCastro fase2
            if(estado.equals("T"))
                filtro =   " WHERE vlrIngreso = vlrItem ";
            else if(estado.equals("P"))
                filtro =   " WHERE vlrIngreso > vlrItem   AND  vlrItem != 0 ";
            else if(estado.equals("N"))
                filtro =   " WHERE vlrItem = 0 ";
            else
                filtro =   "";

            if( tipo_doc.equals("") && doc.equals ("") )
                filtro2 =   " LEFT ";
            else
                filtro2 = " INNER ";
            
            String fech = !fec_ini.equals ("") && !fec_fin.equals ("")?"a.creation_date  BETWEEN '"+fec_ini+" 00:00' AND '"+fec_fin+" 23:59' AND ":"";

            String sql    =   this.obtenerSQL( query ).replaceAll("#CONDICIONES#", filtro );
            sql           =   sql.replaceAll("#PARAM#", filtro2 );
            sql           =   sql.replaceAll ("#FECHA#", fech);

            st            =   con.prepareStatement( sql );
            no_ingreso = no_ingreso.equals ("")?no_ingreso+"%":no_ingreso;
            st.setString(1, no_ingreso);
            st.setString(2, cliente+"%");
            st.setString(3, periodo+"%");
            st.setString(4, tipo_ingreso+"%");
            st.setString(5, distrito);
            st.setString(6, tipo_doc+"%");
            st.setString(7, doc+"%");
            System.out.println(st.toString());
            rs = st.executeQuery();
            listadoingreso = new LinkedList();
            while(rs.next()){
                ingreso = new Ingreso();
                ingreso.setNomCliente(rs.getString("nomcli")!=null?rs.getString("nomcli"):"");
                ingreso.setCodcli(rs.getString("codcli"));
                ingreso.setNum_ingreso(rs.getString("num_ingreso"));
                ingreso.setFecha_consignacion(rs.getString("fecha_consignacion"));
                ingreso.setVlr_ingreso_me(rs.getDouble("vlrIngreso"));
                ingreso.setVlr_ingreso(rs.getDouble("vlr_ingreso"));
                ingreso.setTipo_documento(rs.getString("tipo_documento"));
                ingreso.setBranch_code(rs.getString("branch_code"));
                ingreso.setBank_account_no( rs.getString("bank_account_nos")!=null?rs.getString("bank_account_nos"):"/" );
                ingreso.setCodmoneda( rs.getString("codmoneda") );
                ingreso.setTipo_ingreso( rs.getString("tipo_ingreso")!=null? rs.getString("tipo_ingreso"):"" );
                ingreso.setPeriodo( rs.getString("periodo")!=null?rs.getString("periodo"):"" );
                ingreso.setReg_status(rs.getString("estado")!=null? rs.getString("estado") :"" );
                ingreso.setFecha_contabilizacion( rs.getString("fecha_contabilizacion")!=null?rs.getString("fecha_contabilizacion"):"0099-01-01 00:00:00" );
                if( rs.getString("total").equals("S") )
                    ingreso.setConcepto("T");
                if ( rs.getString("parcial").equals("S") && rs.getString("no_tiene").equals("") )
                    ingreso.setConcepto("P");
                if ( rs.getString("parcial").equals("S") && rs.getString("no_tiene").equals("S") )
                    ingreso.setConcepto("N");

                listadoingreso.add(ingreso);
            }
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }



 /**
  *
  * @param tipodocumento
  * @param nroingreso
  * @throws Exception
  */
    public void update_Fecimp_Ingreso( String tipodocumento, String nroingreso ) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_UPDATE_FECIMP_INGRESO";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, tipodocumento );
            st.setString(2, nroingreso);
            st.executeUpdate();
            
            }}catch(Exception e) {
            throw new Exception("Error enr update_fecimp_ingreso [IngresoDAO.xml]... \n"+e.getMessage());
        }
finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    /**
     * Metodo anularIngresoContabilizado, modifica un registro en la tabla ingreso
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public String anularIngresoContabilizado( ) throws Exception {
        StringStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ANULAR_DESCON";
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, ingreso.getFecha_anulacion());
            st.setInt(2, ingreso.getTransaccion_anulacion());
            st.setString(3, ingreso.getPeriodo_anulacion() );
            st.setString(4, ingreso.getFecha_anulacion());
            st.setString(5, ingreso.getUser_update() ) ;
            st.setString(6, ingreso.getLast_update());
            st.setString(7, ingreso.getDstrct());
            st.setString(8, ingreso.getTipo_documento());
            st.setString(9, ingreso.getNum_ingreso());

            return st.getSql()+";";
        }catch(Exception e) {
            throw new Exception("Error al ANULAR Ingreso [IngresoDAO.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
    }



    /**
     *
     * @return
     * @throws Exception
     */
    public String anularIngreso( ) throws Exception {
        StringStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ANULAR";
        try{
           st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, ingreso.getUser_update());
            st.setString(2, ingreso.getDstrct());
            st.setString(3, ingreso.getTipo_documento());
            st.setString(4, ingreso.getNum_ingreso());
            st.setString(5, ingreso.getUser_update());
            st.setString(6, ingreso.getDstrct());
            st.setString(7, ingreso.getTipo_documento());
            st.setString(8, ingreso.getNum_ingreso());
            return st.getSql()+";";
        }catch(Exception e) {
            throw new Exception("Error al ANULAR Ingreso [IngresoDAO.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
    }


/**
 *
 * @param dstrct
 * @param tipodoc
 * @param num_ingreso
 * @throws Exception
 */
    public void buscarIngresoMiscelaneo(String dstrct, String tipodoc, String num_ingreso ) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ingreso = null;
        String query = "SQL_BUSCAR_INGMISCELANEO";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2,  tipodoc);
            st.setString(3, num_ingreso );
            rs = st.executeQuery();
            if(rs.next()){
                ingreso = new Ingreso();
                ingreso.setDstrct(rs.getString("dstrct"));
                ingreso.setCodcli(rs.getString("codcli"));
                ingreso.setNomCliente(rs.getString("nomId"));
                ingreso.setNitcli(rs.getString("nitcli"));
                ingreso.setNum_ingreso(rs.getString("num_ingreso"));
                ingreso.setConcepto(rs.getString("concepto"));
                ingreso.setTipo_ingreso(rs.getString("tipo_ingreso"));
                ingreso.setFecha_consignacion(rs.getString("fecha_consignacion"));
                ingreso.setFecha_ingreso(rs.getString("fecha_ingreso"));
                ingreso.setBranch_code(rs.getString("branch_code"));
                ingreso.setBank_account_no(rs.getString("bank_account_nos")!=null?rs.getString("bank_account_nos"):"");
                ingreso.setCodmoneda(rs.getString("codmoneda"));
                ingreso.setAgencia_ingreso(rs.getString("agencia_ingreso"));
                ingreso.setVlr_ingreso(rs.getDouble("vlr_ingreso"));
                ingreso.setVlr_ingreso_me(rs.getDouble("vlr_ingreso_me"));
                ingreso.setTransaccion(rs.getInt("transaccion"));
                ingreso.setFecha_tasa(rs.getString("fecha_tasa"));
                ingreso.setDescripcion_ingreso(rs.getString("descripcion_ingreso"));
                ingreso.setCant_item(rs.getInt("cant_item"));
                ingreso.setNro_consignacion(rs.getString("nro_consignacion"));
                ingreso.setFecha_contabilizacion(rs.getString("fecha_contabilizacion"));
                ingreso.setDestipo(rs.getString("destipo"));
                ingreso.setTipo_documento(rs.getString("tipo_documento"));
                ingreso.setTransaccion(rs.getInt("transaccion"));  
                ingreso.setCreation_user(rs.getString("creation_user")!=null?rs.getString("creation_user"):"");
                ingreso.setReg_status(rs.getString("reg_status")!=null?rs.getString("reg_status"):"");
            }
            
            
            }}catch(Exception e) {
            throw new Exception("Error al Buscar Ingreso [IngresoDAO.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


/**
 * 
 * @param dstrct
 * @param tipo
 * @param identificacion
 * @param fecha1
 * @param fecha2
 * @throws Exception
 */
    public void busquedaIngresoMiscelaneo(String dstrct,String tipo, String identificacion, String fecha1, String fecha2 ) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ingreso = null;
        listadoingreso = null;
        String query = "SQL_BUSQUEDA_ING_MISCELANEOS";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, tipo);
            st.setString(3, identificacion+"%" );
            st.setString(4, fecha1+" 00:00");
            st.setString(5, fecha2+" 23:59");
            listadoingreso = new LinkedList();
            rs = st.executeQuery();
            
            while(rs.next()){
                ingreso = new Ingreso();
                ingreso.setDstrct(rs.getString("dstrct"));
                ingreso.setNomCliente(rs.getString("nombre"));
                ingreso.setNitcli(rs.getString("nitcli"));
                ingreso.setNum_ingreso(rs.getString("num_ingreso"));
                ingreso.setFecha_consignacion(rs.getString("fecha_consignacion"));
                ingreso.setFecha_ingreso(rs.getString("fecha_ingreso"));
                ingreso.setVlr_ingreso(rs.getDouble("vlr_ingreso"));
                ingreso.setVlr_ingreso_me(rs.getDouble("vlr_ingreso_me"));
                ingreso.setCodmoneda(rs.getString("codmoneda"));
                ingreso.setDestipo(rs.getString("destipo"));
                ingreso.setTipo_documento(rs.getString("tipo_documento"));
                ingreso.setBranch_code(rs.getString("branch_code"));
                ingreso.setBank_account_no(rs.getString("bank_account_nos")!=null?rs.getString("bank_account_nos"):"");
                ingreso.setReg_status(rs.getString("reg_status"));
                ingreso.setCreation_user( rs.getString("creation_user"));
                listadoingreso.add(ingreso);
            }
            
            
            }}catch(Exception e) {
            throw new Exception("Error al Buscar Ingreso [IngresoDAO.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

     /**
     * Metodo tieneItemsIngreso, verifica si tiene items un ingreso
     * @param: distrito, tipodocumento,nroingreso
     * @autor : Ing Jose de la rosa
     * @version : 1.0
     */
    public double saldoIngresoRI(String dstrct, String tipodocumento, String nroingreso ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        double valor = 0;
        String query = "SQL_SALDOS_RI";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString (1, dstrct);
            st.setString (2, tipodocumento );
            st.setString (3, nroingreso);
            st.setString (4, tipodocumento.equals("ING")?"RI":"RC");
            st.setString (5, tipodocumento.equals("ING")?"IC":"NC");
            rs = st.executeQuery();
            if(rs.next()){
                valor = rs.getDouble("saldo_ingreso");
            }
            }}catch(SQLException e) {
            throw new SQLException("Error al Buscar Items Ingreso [IngresoDAO.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return valor;
    }
    
    /**
     * Metodo tieneSubIngreso, verifica si un ingreso tiene sub ingresos asociados
     * @param: distrito, tipodocumento,nroingreso
     * @autor : Ing. Jdelarosa
     * @version : 1.0
     */
    public boolean tieneSubIngreso(String dstrct, String tipodocumento, String nroingreso ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean valor = false;
        String query = "SQL_TIENE_SUBINGRESOS";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString (1, dstrct);
            st.setString (2, tipodocumento );
            st.setString (3, nroingreso);
            st.setString (4, tipodocumento.equals("ING")?"IC":"NC");
            st.setString (5, tipodocumento.equals("ING")?"RI":"RC");
            rs = st.executeQuery();
            if(rs.next()){
                if( rs.getInt("ingre") == 0 ) 
                    valor = false;
                else
                    valor = true;
            }
            
            }}catch(SQLException e) {
            throw new SQLException("Error al Buscar Items Ingreso [IngresoDAO.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return valor;
    }    



    public String insertarNewIngreso() throws Exception {
        StringStatement st = null;
        ResultSet rs = null;
        String sql = "";
        String query = "SQL_NEW_INGRESAR";
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, ingreso.getReg_status());
            st.setString(2, ingreso.getDstrct());
            st.setString(3, ingreso.getCodcli());
            st.setString(4, ingreso.getNitcli());
            //st.setString(5, this.encontrarSerie("ICAC"));
            st.setString(5, ingreso.getConcepto());
            st.setString(6, ingreso.getTipo_ingreso());
            st.setString(7, ingreso.getFecha_consignacion());
            st.setString(8, ingreso.getFecha_ingreso());
            st.setString(9, ingreso.getBranch_code());
            st.setString(10, ingreso.getBank_account_no());
            st.setString(11, ingreso.getCodmoneda());
            st.setString(12, ingreso.getAgencia_ingreso());
            st.setString(13, ingreso.getPeriodo());
            st.setDouble(14, ingreso.getVlr_ingreso());
            st.setDouble(15, ingreso.getVlr_ingreso_me());
            st.setInt(16, ingreso.getTransaccion());
            st.setDouble(17, ingreso.getVlr_tasa());
            st.setString(18, ingreso.getFecha_tasa());
            st.setInt(19, ingreso.getCant_item());
            st.setString(20, ingreso.getCreation_user());
            st.setString(21, ingreso.getCreation_date());
            st.setString(22, ingreso.getUser_update());
            st.setString(23, ingreso.getLast_update());
            st.setString(24, ingreso.getBase());
            st.setInt(25, ingreso.getTransaccion_anulacion());
            st.setString(26, ingreso.getTipo_documento());
            st.setString(27, ingreso.getDescripcion_ingreso());
            st.setString(28, ingreso. getNro_consignacion());
            st.setString(29, ingreso.getCuenta());
            st.setString(30, ingreso.getAuxiliar());
            st.setDouble(31, ingreso.getTasaDolBol());
            st.setString(32, ingreso.getAbc());
            st.setDouble(33, ingreso.getVlr_saldo_ing());
            st.setString(34, ingreso.getCmc());
            sql = st.getSql();
        }catch(Exception e) {
            throw new Exception("Error al Insertar Ingreso [IngresoDAO.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return sql;
    }



    /**
     * 
     * @param tipo
     * @return
     * @throws Exception
     */
    public String  incrementarSerie(String tipo) throws Exception {
        StringStatement st = null;
        String sql = "";
        String query = "SQL_INC_SERIE";
        try {

            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, tipo);
            sql = st.getSql();

        } catch (Exception e) {
            throw new Exception("Error al buscar la serie Ingreso [IngresoDAO.xml]... \n" + e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return sql;
    }

    /**
     * Metodo buscarIngresos, busca los ingresos
     * @param:  fecha inicial, fecha final
     * @autor :  Ing. Iris Vargas
     * @version : 1.0
     */
    public void buscarIngresos(String dstrct, String tipo, String banco, String fecini, String fecfin, String num_ingreso, String factura) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        listadoingreso = null;
        String query = "SQL_GET_INGRESOS";
        String sql="";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {

                sql = this.obtenerSQL(query);
                sql = !dstrct.equals("") ? sql.replaceAll("#DSTRCT#"," AND a.dstrct = '"+ dstrct+"' ") : sql.replaceAll("#DSTRCT#", "");
                sql = !tipo.equals("") ? sql.replaceAll("#TIPO#","AND a.tipo_documento ='"+ tipo+"' ") : sql.replaceAll("#TIPO#", "");
                sql = !fecini.equals("")&&!fecfin.equals("") ? sql.replaceAll("#FECHAS#","AND a.creation_date BETWEEN '"+fecini+"' AND  '"+ fecfin+"'") : sql.replaceAll("#FECHAS#", "");      
                sql = !banco.equals("") ? sql.replaceAll("#BANCO#", "AND c.branch_code=  '"+banco+"' ") : sql.replaceAll("#BANCO#", "");
                sql = !num_ingreso.equals("") ? sql.replaceAll("#INGRESO#","AND a.num_ingreso='"+ num_ingreso+"'") : sql.replaceAll("#INGRESO#", "");
                sql = !factura.equals("") ? sql.replaceAll("#FACTURA#","AND b.factura='"+ factura+"'") : sql.replaceAll("#FACTURA#", "");
                st = con.prepareStatement(sql);//JJCastro fase2

                listadoingreso = new LinkedList();
                rs = st.executeQuery();

                while (rs.next()) {
                    ingreso = new Ingreso();
                    ingreso.setDstrct(dstrct);
                    ingreso.setNum_ingreso(rs.getString("num_ingreso"));
                    ingreso.setFecha_consignacion(rs.getString("fecha_consignacion"));
                    ingreso.setFecha_ingreso(rs.getString("fecha_ingreso"));
                    ingreso.setCodmoneda(rs.getString("codmoneda"));
                    ingreso.setVlr_ingreso(rs.getDouble("vlr_ingreso"));
                    ingreso.setVlr_ingreso_me(rs.getDouble("vlr_ingreso_me"));
                    ingreso.setNomCliente(rs.getString("nomcli"));
                    ingreso.setDestipo(rs.getString("destipo"));
                    ingreso.setTipo_documento(rs.getString("tipo_documento"));
                    ingreso.setVlr_saldo_ing(rs.getDouble("saldo_ing"));
                    ingreso.setReg_status(rs.getString("reg_status"));
                    ingreso.setBranch_code(rs.getString("branch_code") != null ? rs.getString("branch_code") : "");
                    ingreso.setBank_account_no(rs.getString("bank_account_nos") != null ? rs.getString("bank_account_nos") : "");
                    listadoingreso.add(ingreso);

                }


            }
        } catch (Exception e) {
            throw new Exception("Error al Buscar Ingreso [IngresoDAO.xml]... \n" + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }
}