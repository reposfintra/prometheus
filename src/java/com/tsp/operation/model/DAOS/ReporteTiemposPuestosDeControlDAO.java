/*
 * Nombre        ReporteTiemposPuestosDeControlDAO.java
 * Descripci�n   Clase que permite el acceso a los datos del reporte de timpos entre puestos de control.
 * Autor         Alejandro Payares
 * Fecha         1 de febrero de 2006, 10:47 AM
 * Version       1.0
 * Coyright      Transportes Sanchez Polo SA.
 */

package com.tsp.operation.model.DAOS;

import com.tsp.exceptions.InformationException;

import java.util.LinkedList;
import java.util.Hashtable;

import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Connection;

import java.io.*;


/**
 * Clase que permite el acceso a los datos del reporte de timpos entre puestos de control.
 * @author Alejandro Payares
 */
public class ReporteTiemposPuestosDeControlDAO extends MainDAO {
    
    private LinkedList datos;
    private String promedio;
    
    /**
     * Crea una nueva instancia de ReporteTiemposPuestosDeControlDAO
     * @autor  Alejandro Payares
     */
    public ReporteTiemposPuestosDeControlDAO() {
        super("ReporteTiemposPuestosDeControlDAO.xml");
    }
    
    /**
     * Busca los datos para el reporte. Los datos encontrados son almacenados en una lista de tipo
     * <CODE>java.util.LinkedList</CODE> donde cada elemento en un <CODE>java.util.Hashtable</CODE>
     * que contiene los datos correspondiente a una fila del reporte.
     * @param fechaInicio La fecha de inicio de busqueda del reporte.
     * @param fechaFin La fecha de fin de busqueda del reporte.
     * @throws SQLException Si algun error con la base de datos ocurre.
     */
    public void buscarDatos(String fechaInicio, String fechaFin ) throws SQLException {
        PreparedStatement st = null;
        PreparedStatement stDif = null;
        PreparedStatement stDemora = null;
        PreparedStatement stResta = null;
        try {
            st = this.crearPreparedStatement("SQL_TIEMPOS_PUESTOS_DE_CONTROL");
            stDif = this.crearPreparedStatement("SQL_OBTENER_DIFERENCIA");
            stDemora = this.crearPreparedStatement("SQL_OBTENER_DEMORA");
            stResta = this.crearPreparedStatement("SQL_RESTAR_TIEMPOS");
            st.setString(1,fechaInicio + " 00:00");
            st.setString(2, fechaFin + " 23:59");
            ResultSet rs = st.executeQuery();
            datos = new LinkedList();
            Hashtable filaAnterior = null;
            boolean huboFinDeViajeAntes = false;
            String planillaDeFinDeViaje = null;
            while ( rs.next() ){
                Hashtable fila = new Hashtable();
                String planilla = rs.getString("numpla");
                String ruta = rs.getString("ruta");
                String puesto_de_control = rs.getString("puesto_de_control");
                String hora_pc = rs.getString("hora_pc");
                String codmims = rs.getString("codmims");
                String tipo = rs.getString("tipomtr");
                boolean esFinDeViaje = "EIF".indexOf(tipo) >= 0 || rs.getString("destino").equals(puesto_de_control);
                //con esta bandera eliminamos errores de trafico
                boolean debeIgnoraseEstaFila = (filaAnterior != null && filaAnterior.get("codmims").equals(codmims)) || (huboFinDeViajeAntes && planillaDeFinDeViaje.equals(planilla));
                // eliminamos los tramos cuyo tramo sea el mismo puesto de control
                if ( debeIgnoraseEstaFila && filaAnterior != null) {
                    datos.removeLast();
                    if ( !datos.isEmpty() ) {
                        filaAnterior = (Hashtable)datos.getLast();
                    }
                }
                else if ( debeIgnoraseEstaFila && filaAnterior == null ) {
                    //este caso ocurre cuando hay un reporte de trafico despues de la entrega, lo cual es un error de trafico
                    continue;
                }
                // establecemos los valores de la fila anterior
                if ( filaAnterior != null && (!debeIgnoraseEstaFila || esFinDeViaje) ) {
                    String horaAnterior = filaAnterior.get("hora_pc").toString();
                    filaAnterior.put("puesto_de_control2", puesto_de_control);
                    filaAnterior.put("hora_pc2", hora_pc);
                    filaAnterior.put("codmims2", codmims);
                    String diferencia = "";
                    // calculando duraci�n del tramo
                    stDif.setString(1, hora_pc);
                    stDif.setString(2, horaAnterior);
                    ResultSet rsDif = stDif.executeQuery();
                    rsDif.next();
                    String tiempoTramo = rsDif.getString(1);
                    stDif.clearParameters();
                    // calculando demora del tramo
                    stDemora.setString(1, planilla);
                    stDemora.setString(2, horaAnterior);
                    stDemora.setString(3, hora_pc);
                    stDemora.setString(4, horaAnterior);
                    stDemora.setString(5, hora_pc);
                    ResultSet rsDemora = stDemora.executeQuery();
                    if( rsDemora.next() ){
                        stResta.setString(1, tiempoTramo);
                        stResta.setString(2, rsDemora.getString("demora"));
                        ResultSet rsResta = stResta.executeQuery();
                        rsResta.next();
                        tiempoTramo = rsResta.getString("dif");
                    }
                    filaAnterior.put("diferencia", tiempoTramo.replaceFirst("day","d�a"));
                }
                huboFinDeViajeAntes = esFinDeViaje;
                if ( esFinDeViaje ) {
                    planillaDeFinDeViaje = planilla;
                    filaAnterior = null;
                    continue;
                }
                fila.put("planilla",planilla);
                fila.put("ruta", ruta);
                fila.put("puesto_de_control", puesto_de_control);
                fila.put("hora_pc",hora_pc);
                fila.put("codmims",codmims);
                filaAnterior = fila;
                datos.add(fila);
            }
        }
        catch( Exception ex ){
            throw new SQLException("ERROR AL BUSCAR DATOS DEL REPORTE: "+ex.getMessage()+"\n"+this.getStackTrace(ex));
        }
        finally {
            if(st != null){st.close();}
            if(stDif != null){stDif.close();}
            this.desconectar("SQL_TIEMPOS_PUESTOS_DE_CONTROL");
            this.desconectar("SQL_OBTENER_DIFERENCIA");
            this.desconectar("SQL_OBTENER_DEMORA");
            this.desconectar("SQL_RESTAR_TIEMPOS");
        }
    }
    
    /**
     * Devuelve los datos encontrados por el m�todo buscarDatos().
     * @return La lista con los datos del reporte.
     */
    public LinkedList obtenerDatos(){
        return datos;
    }
    
    /**
     * Elimina los datos arrojados por el reporte, es util para liberar memoria en el servidor
     */
    public void borrarDatos(){
        if ( datos != null ) {
            datos.clear();
            datos = null;
            System.gc();
        }
    }
    
    
    /**
     * Calcula el tiempo promedio entre los puestos de control dados
     * @param PCOrigen El puesto de control de origen
     * @param PCDestino El puesto de control de destino
     * @throws SQLException Si algun error ocurre en la base de datos
     * @throws InformationException Si los puestos de control no son adyacentes.
     */
    public void calcularTiempoPromedioEntrePuestosDeControl(String PCOrigen, String PCDestino )throws SQLException, InformationException {
        PreparedStatement st = null;;
        ResultSet rs = null;
        datos = new LinkedList();
        try {
            st = this.crearPreparedStatement("SQL_VALIDAR_PC_ADYACENTES");
            st.setString(1, PCOrigen + PCDestino);
            //////System.out.println("query adyacentes: "+st);
            rs = st.executeQuery();
            if ( !rs.next() ) {
                throw new InformationException("LOS PUESTOS DE CONTROL INGRESADOS NO SON ADYACENTES");
            }
            rs.close();
            st.close();
            st = this.crearPreparedStatement("SQL_OBTENER_DETALLE_PROMEDIO");
            st.setString(1, PCOrigen );
            st.setString(2, PCDestino );
            //////System.out.println("query promedio: "+st);
            rs = st.executeQuery();
            String [] campos = {"numpla","duracion","demora","total"};
            StringBuffer fechas = new StringBuffer();
            while ( rs.next() ) {
                Hashtable fila = new Hashtable();
                for( int i=0; i< campos.length; i++ ){
                    fila.put(campos[i],rs.getString(campos[i]));
                }
                fechas.append("'" + rs.getString("total") + "'::interval + " );
                datos.add(fila);
            }
            if ( datos.isEmpty() ) {
                this.promedio = "";
            }
            else{
                // eliminamos el ultimo signo +
                fechas.delete(fechas.length() - 3, fechas.length() );
                rs.close();
                st.close();
                String sql = this.obtenerSQL("SQL_CALCULAR_PROMEDIO");
                sql = sql.replaceFirst("'FECHAS'", fechas.toString() );
                sql = sql.replaceFirst("'CANTIDAD'", "" + datos.size() );
                Connection c = this.conectar("SQL_CALCULAR_PROMEDIO");
                //////System.out.println("query calculo: "+sql);
                st = c.prepareStatement(sql);
                rs = st.executeQuery();
                promedio = rs.next() ? rs.getString("promedio").substring(1) : "";
                c = null;
            }
        }
        catch( SQLException ex ){
            ex.printStackTrace();
        }
        finally {
            if ( st != null ) { st.close(); }
            this.desconectar("SQL_VALIDAR_PC_ADYACENTES");
            this.desconectar("SQL_OBTENER_DETALLE_PROMEDIO");
            this.desconectar("SQL_CALCULAR_PROMEDIO");
        }
    }
    
    public String obtenerPromedio(){
        return promedio;
    }
    
    /**
     * Metodo ejecutable de java
     */
    public static void main( String [] args ){
        try {
            ReporteTiemposPuestosDeControlDAO dao = new ReporteTiemposPuestosDeControlDAO();
            dao.buscarDatos("2006-01-02","2006-01-02");
            LinkedList lista = dao.obtenerDatos();
            ////System.out.println(lista.size()+" registros encontrados");
            java.util.Iterator ite = lista.iterator();
            PrintWriter log = null;
            log = new PrintWriter(
            new BufferedWriter(
            new FileWriter( "c:/logs/excell.txt", true)
            ),
            true );
            log.println("PLANILLA,RUTA,PUESTO DE CONTROL 1,CODIGO MIMS PC 1,FECHA Y HORA PC 1,PUESTO DE CONTROL 2,CODIGO MIMS PC 2,FECHA Y HORA PC 2,DIFERENCIA");
            while( ite.hasNext() ){
                Hashtable fila = (Hashtable) ite.next();
                log.print(fila.get("planilla")+",");
                log.print(fila.get("ruta")+",");
                log.print(fila.get("puesto_de_control")+",");
                log.print(fila.get("codmims")+",");
                log.print(fila.get("hora_pc")+",");
                log.print(fila.get("puesto_de_control2")+",");
                log.print(fila.get("hora_pc2")+",");
                log.print(fila.get("codmims2")+",");
                log.print(fila.get("diferencia"));
                log.println();
            }
            log.close();
            ////System.out.println("LISTO!!!");
        }
        catch( Exception ex ){
            ex.printStackTrace();
        }
        
    }
    
}
