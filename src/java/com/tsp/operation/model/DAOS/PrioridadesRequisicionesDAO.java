/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.google.gson.JsonObject;

/**
 *
 * @author egonzalez
 */
public interface PrioridadesRequisicionesDAO {
    
    public abstract JsonObject buscar_requisiciones(JsonObject info);
    
    public abstract JsonObject modificar_prioridades(JsonObject info);
    
    public abstract JsonObject getFiltro(String consulta);
    
    public abstract JsonObject getUsuariosProceso(String idProceso);
}
