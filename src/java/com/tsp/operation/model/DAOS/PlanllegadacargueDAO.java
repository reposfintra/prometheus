/*
 * PlanllegadacargueDAO.java
 *
 * Created on 13 de septiembre de 2006, 02:54 PM
 */

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.exceptions.EncriptionException;

public class PlanllegadacargueDAO extends MainDAO {
     private BeanGeneral bean;
    
    private Vector vectorValores;

    public PlanllegadacargueDAO() {
         super("PlanllegadacargueDAO.xml");
    }
    
    
    /**
     * Metodo insertPlanllegadaCargue, ingresa un registro en la tabla actividad
     * @param: objeto plan_llegada_cargue 
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void insertarPlanllegadaCargue ( Plan_llegada_cargue llegada_cargue ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_INSERTAR";
        
        try{
            
            st = this.crearPreparedStatement(query);
            st.setString(1, llegada_cargue.getDstrct() );
            st.setString(2, llegada_cargue.getNro_trasn() );
            st.setString(3, llegada_cargue.getCodcli() );
            st.setString(4, llegada_cargue.getNumrem() );
            st.setString(5, llegada_cargue.getLlegada_cargue() );
            st.setString(6, llegada_cargue.getCreation_user() );
            st.setString(7, llegada_cargue.getCreation_date() );
            st.setString(8, llegada_cargue.getUser_update() );
            st.setString(9, llegada_cargue.getLast_update() );
            st.setString(10, llegada_cargue.getBase() );
            
            st.executeUpdate();
            
        } catch( Exception e ){
            
            throw new Exception ( "ERROR DURANTE LA INSERCION DE LA FECHA PLANEACION DE CARGUE " + e.getMessage () );
            
        }
        finally{
            
            if(st!=null) st.close ();
            this.desconectar (query);
            
        }
        
    }
    
    
    /**
     * Metodo modificarPlanllegadaCargue, ingresa un registro en la tabla actividad
     * @param: objeto tipo 
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void modificarPlanllegadaCargue ( Plan_llegada_cargue llegada_cargue ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_MODIFICAR";
        
        try{
            
            st = this.crearPreparedStatement(query);
            st.setString(1, llegada_cargue.getLlegada_cargue() );
            st.setString(2, llegada_cargue.getUser_update() );
            st.setString(3, llegada_cargue.getLast_update() );
            st.setString(4, llegada_cargue.getDstrct() );
            st.setString(5, llegada_cargue.getNro_trasn() );
            st.setString(6, llegada_cargue.getCodcli() );
            st.setString(7, llegada_cargue.getNumrem() );
            st.executeUpdate();
            
        } catch( Exception e ){
            
            throw new Exception ( "ERROR DURANTE LA MODIFICACION DE LA FECHA PLANEACION DE CARGUE " + e.getMessage () );
            
        }
        finally{
            
            if(st!=null) st.close ();
            this.desconectar (query);
            
        }
        
    }
    
    /**
     * Metodo buscarInfoTransporte, busca la informacion del transporte
     * @param: objeto tipo Plan_llegada_cargue, cod cliente y nro transporte
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public String buscarInfoTransporte ( String cliente, String nrotrans ) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_BUSCAR_INF_TRANS";
        ResultSet rs         = null;
        String numrem = "";
        try{
            
            st = this.crearPreparedStatement(query);
            st.setString(1, cliente );
            st.setString(2, nrotrans );
            rs = st.executeQuery();
            if(rs.next()){
                numrem = rs.getString("numrem");
            }
            
        } catch( Exception e ){
            
            throw new Exception ( "ERROR BUSCAR INFO TRANSPORTE " + e.getMessage () );
            
        }
        finally{
            
            if(st!=null) st.close ();
            this.desconectar (query);
            
        }
        return numrem;
    }    
    
    
      public java.util.Vector getvectorValores() {
          return vectorValores;
      }
      
      /**
       * Setter for property vector.
       * @param vector New value of property vector.
       */
      public void setvectorValores(java.util.Vector vectorValores) {
          this.vectorValores = vectorValores;
      }
      
      
    /* funcion que retorna los descargues depemndiedo de la fecha
     * creado por FILY STEVEN FERNANDEZ V
     * 2006-12-11 */
     public void ReporteViajesDescargues (String fecini, String fecfin ) throws SQLException {
        vectorValores = new Vector();
        PreparedStatement st = null;
        ResultSet rs = null;
        int cont = 0;
        double pesol =0;
        double pesov = 0;
        double pesot = 0;
        try {
            
            st = crearPreparedStatement( "SQL_VIAJESXDESCARGUES" );
            
           st.setString( 1, fecini );
           st.setString( 2, fecfin );
          
           
            rs = st.executeQuery();
            
                
            while( rs.next() ){
                pesot = 0;
                cont++;
                String remision = rs.getString( "remisiones" )!=null?rs.getString( "remisiones" ).toUpperCase():"";
                String placa = rs.getString( "placa" )!=null?rs.getString( "placa" ).toUpperCase():"";
                String fechaCump = rs.getString( "fecha" )!=null?rs.getString( "fecha" ).toUpperCase():""; 
                String pesoReal = rs.getString( "pesoreales" )!=null?rs.getString( "pesoreales" ).toUpperCase():"";
                String nitpro = rs.getString( "nitpropitario" )!=null?rs.getString( "nitpropitario" ).toUpperCase():"";
                String nompro = rs.getString( "nomPropietario" )!=null?rs.getString( "nomPropietario" ).toUpperCase():"";
                String cedcon = rs.getString( "cedconductor" )!=null?rs.getString( "cedconductor" ).toUpperCase():"";
                String nomcon = rs.getString( "nomConductor" )!=null?rs.getString( "nomConductor" ).toUpperCase():"";
                String pesominlleno = rs.getString( "pesolleno" )!=null?rs.getString( "pesolleno" ).toUpperCase():"";
                String pesominvacio = rs.getString( "pesovacio" )!=null?rs.getString( "pesovacio" ).toUpperCase():"";
                String pesoNeto = "";
                String origen = rs.getString( "origen" )!=null?rs.getString( "origen" ).toUpperCase():"";
                String destino = rs.getString( "destino" )!=null?rs.getString( "destino" ).toUpperCase():"";
                String puertoDescargue = rs.getString( "descargue" )!=null?rs.getString( "descargue" ).toUpperCase():"";           
                String item = ""+cont;                
                
               
                bean = new BeanGeneral ();
                
                bean.setValor_01 ( item );
                bean.setValor_02 ( remision );
                bean.setValor_03( fechaCump );
                bean.setValor_04( pesoReal );
                bean.setValor_05( nitpro );
                bean.setValor_06( nompro);
                bean.setValor_07( cedcon );
                bean.setValor_08( nomcon );
                bean.setValor_09( pesominlleno );
                bean.setValor_10( pesominvacio );
                bean.setValor_11( pesoNeto );
                bean.setValor_12( origen);
		bean.setValor_13( destino );
                bean.setValor_14( placa );
                bean.setValor_15( puertoDescargue );
               
               this.vectorValores.addElement(bean);
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'ReporteViajesDescargues()' - [PlanllegadacargueDAO].. " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_VIAJESXDESCARGUES" );
            
        }
        
    }
}
