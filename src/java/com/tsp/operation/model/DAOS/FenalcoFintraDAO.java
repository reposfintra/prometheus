/**********************************************
 * Nombre       Clase  FenalcoFintraDAO.java 
 * Autor        ING DERLYS si Apellido
 * Fecha        25/06/2007
 * Copyright    Transportes Sanchez Polo S.A.
 **********************************************/

package com.tsp.operation.model.DAOS;



import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;



public class FenalcoFintraDAO extends MainDAO{
    
     private Planilla pla;
     private String codt="0";
     private static final  String Select_Consulta = "SELECT NIT,NOMCLI FROM CLIENTE C INNER JOIN CLIENTE_AFIL CA ON C.NIT=CA.NIT_CLI WHERE CA.NIT_AFIL=? AND CA.REG_STATUS='' AND C.ESTADO='A'";
     //hay que cambiar esa consultasss
     //para traer los clientes debo preguntar en provvedor y luego sacar el nitdelterceroo
     private TreeMap clientes;
    /** Creates a new instance of FenalcoFintraDAO */
    public FenalcoFintraDAO() {
        super("FenalcoFintraDAO.xml");  
    }
    public FenalcoFintraDAO(String dataBaseName) {
        super("FenalcoFintraDAO.xml", dataBaseName);  
    }
    
    
    /**
     * M�todo buscar ExtractosPP realizados
     * @autor   Derlys sin apellido
     * @param   parametro de busqueda
     * @param   tipo, tipo de filtro
     * @throws  Exception
     * @version 1.0.
     **/
    public Propietario buscarPropietarioF(String propietario) throws Exception {
        //System.out.println("a dao");
        Connection con = null;
        Propietario  Propietario  = new Propietario();
        boolean            Query1 = false;
        PreparedStatement  st     = null;        
        ResultSet          rs     = null;        
        String             query  = "SQL_RETORNAR_PROPIETARIO_FENALCO";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, propietario);
            //System.out.println("ejecutando query "+propietario+"  "+st.toString());
            rs = st.executeQuery();
            if(rs.next()){
                Propietario.setCedula            ( rs.getString("nit")          );
                Propietario.setS_nombre          ( rs.getString("payment_name") );
                Propietario.setTasa_fenalco      ( Double.parseDouble(rs.getString("tasa_fenalco")));
                Propietario.setCustodiacheque    ( rs.getDouble("custodiacheque"));
                Propietario.setRem               (Double.parseDouble(rs.getString("remesa")));
                //System.out.println("termino");
                //System.out.println("custodiacheque"+rs.getDouble("cust"));
                //System.out.println("TASA"+Double.parseDouble(rs.getString("treduc")));
            }
        }}catch(Exception e){
            throw new SQLException("DAO:  buscarPropietarioF() : "+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        //System.out.println("retorna prop");
        return Propietario;
    } 
    
    /**
     * M�todo buscar ExtractosPP realizados
     * @autor   Derlys sin apellido
     * @param   parametro de busqueda
     * @param   tipo, tipo de filtro
     * @throws  Exception
     * @version 1.0.
     * fpag 30 0 45
     * 01 che 02 letra
     **/
    public Vector buscarPorcentajeAval(String fomapag,String tiponeg,String nit) throws Exception {
        Connection con = null;
        Propietario  Propietario  = new Propietario();
        boolean            Query1 = false;
        PreparedStatement  st     = null;        
        ResultSet          rs     = null;  
        String             Query   = "";
        String             query1  = "SQL_PORCENTAJE_FENALCO_C30";
        String             query2  = "SQL_PORCENTAJE_FENALCO_C45";
        String             query3  = "SQL_PORCENTAJE_FENALCO_L30";
        String             query4  = "SQL_PORCENTAJE_FENALCO_L45";
        Vector             Avales  = new Vector();
        try{
            if (fomapag.equals("30")) {
                if (tiponeg.equals("01")) {
                    Query = query1;
                } else {
                    Query = query3;
                }
            } else if (fomapag.equals("45")) {
                if (tiponeg.equals("01")) {
                    Query = query2;
                } else {
                    Query = query4;
                }
            }

            con = this.conectarJNDI(Query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(Query));//JJCastro fase2
                st.setString(1, nit);
                st.setString(2, nit);
                rs = st.executeQuery();
                while (rs.next()) {

                    Avales.add(rs.getString("tasa"));
                    codt = rs.getString("tbcd");
                }
            }
        }catch(Exception e){
            throw new SQLException("DAO:  buscarPorcentajeAval() : "+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return Avales;
    } 
    


/**
 *
 * @param fecha
 * @return
 * @throws Exception
 */
    public boolean buscarFestivo(String fecha) throws Exception {
        Connection con = null;
        Propietario  Propietario  = new Propietario(); 
        boolean            Query1 = false;
        PreparedStatement  st     = null;        
        ResultSet          rs     = null;  
        String             query   = "SQL_FESTIVO";
        boolean            festivo = false;
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, fecha);
            rs = st.executeQuery();
            if(rs.next()){
                festivo =  ( rs.getBoolean("festivos"));
            }
            }}catch(Exception e){
            throw new SQLException("DAO:  buscarFestivo() : "+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return festivo;
    } 
    
/**
 *
 * @param usid
 * @return
 * @throws Exception
 */
      public String busconitas(String usid) throws Exception {
        Connection con = null;
        PreparedStatement  st     = null;
        ResultSet          rs     = null;  
        String             query   = "SQL_NIT_ASIGNADO";
        String              ret="";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, usid);
            rs = st.executeQuery();
            if(rs.next()){
                ret = ( rs.getString("nitas"));
            }
            }}catch(Exception e){
            throw new SQLException("DAO:  buscanitAsig() : "+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return ret;
    }
        
 /**
  *
  * @param usid
  * @return
  * @throws Exception
  */
      public String busconitas2(String usid) throws Exception {
        Connection con = null;
        PreparedStatement  st     = null;
        ResultSet          rs     = null;  
        String             query   = "SQL_NIT_ASIGNADO2";
        String              ret="";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, usid);
            //System.out.println("qery  "+st.toString());
            rs = st.executeQuery();
            if(rs.next()){
                ret = ( rs.getString("nitas"));
                //System.out.println("terminooooooooo8989898989898989");
            }
            }}catch(Exception e){
            throw new SQLException("DAO:  buscanitAsig() : "+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return ret;
    }
     

/**
 * 
 * @param propietario
 * @return
 * @throws Exception
 */
      public Propietario buscarPropietarioF2(String propietario) throws Exception {
        Connection con = null;
        Propietario  Propietario  = new Propietario();
        boolean            Query1 = false;
        PreparedStatement  st     = null;        
        ResultSet          rs     = null;        
        String             query  = "SQL_RETORNAR_PROPIETARIO_FENALCO2";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, propietario);
            //System.out.println("ejecutando query "+propietario+"  "+st.toString());
            rs = st.executeQuery();
            if(rs.next()){
                Propietario.setTasa_fenalco      ( Double.parseDouble(rs.getString("vr_aval")));
                Propietario.setCustodiacheque    ( rs.getDouble("vr_custodia"));
                Propietario.setRem               (Double.parseDouble(rs.getString("porc_remesa")));
                //System.out.println("termino");
                //System.out.println("custodiacheque"+rs.getDouble("cust"));
                //System.out.println("TASA"+Double.parseDouble(rs.getString("treduc")));
            }
            }}catch(Exception e){
            throw new SQLException("DAO:  buscarPropietarioF() : "+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return Propietario;
    }  
     


/**
 * 
 * @param fomapag
 * @param tiponeg
 * @param codneg
 * @param codtb
 * @return
 * @throws Exception
 */
      public Vector buscarPorcentajeAval2(String fomapag,String tiponeg,String codneg,String codtb) throws Exception {
        Connection con = null;
        Propietario  Propietario  = new Propietario(); 
        boolean            Query1 = false;
        PreparedStatement  st     = null;        
        ResultSet          rs     = null;  
        String             Query   = "";
        String             query1  = "SQL_PORCENTAJE_FENALCO_C30_2";
        String             query2  = "SQL_PORCENTAJE_FENALCO_C45_2";
        String             query3  = "SQL_PORCENTAJE_FENALCO_L30_2";
        String             query4  = "SQL_PORCENTAJE_FENALCO_L45_2";
        Vector             Avales  = new Vector(); 
        try{
            
            if(fomapag.equals("30")){
                if(tiponeg.equals("01"))
                    Query = query1;
                else
                    Query = query3;
            }else if(fomapag.equals("45")){
                if(tiponeg.equals("01"))
                    Query = query2;
                else
                    Query = query4;
            }
            con = this.conectarJNDI(Query);
            if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(Query));//JJCastro fase2
            st.setString(1, codtb);
            st.setString(2, codneg); 
            //System.out.println("retorna prop"+st.toString());
            rs = st.executeQuery();
            while(rs.next()){
                Avales.add( rs.getString("tasa"));
            }
        }}catch(Exception e){
            throw new SQLException("DAO:  buscarPorcentajeAval() : "+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return Avales;
    } 
     
     
     public String getcodt()
    {
        return codt;
    }




/**
 * 
 * @param a
 * @return
 * @throws SQLException
 */
     public Vector obtenerpaises(String a ) throws SQLException {
       Vector Clientes = null;
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Clientes = new Vector();
         String query = "SQL_RETORNAR_PROPIETARIO_FENALCO2";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement("SELECT NIT,NOMCLI FROM CLIENTE C INNER JOIN CLIENTE_AFIL CA ON C.NIT=CA.NIT_CLI WHERE CA.NIT_AFIL=? AND CA.REG_STATUS='' AND C.ESTADO='A'");//JJCastro fase2
                st.setString(1, a);
                rs = st.executeQuery();
                //Clientes = new String();
                int j=0;
                //System.out.println("Consulta--------"+st.toString());
                while (rs.next()){
                    Clientes.add(j, rs.getString("nit"));
                    Clientes.add(j+1, rs.getString("nomcli"));
                    j=j+2;
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
       return Clientes;
   }



 /**
  * 
  * @param ciclo
  * @param fechai
  * @return
  * @throws Exception
  */
   public String obtenerPrimeraFechaPago(String ciclo,String fechai)throws Exception {
        Connection con = null;
        PreparedStatement  st     = null;
        ResultSet          rs     = null;
        String             query   = "SQL_PLAZO_PP";
        String              ret="";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, fechai);
            st.setString(2, fechai);
            st.setString(3, fechai);
            st.setString(4, ciclo);
            st.setString(5, fechai);
            st.setString(6, ciclo);
            st.setString(7, fechai);
            //System.out.println("qery  "+st.toString());
            rs = st.executeQuery();
            if(rs.next()){
                ret = ( rs.getString("nodias"));
                //System.out.println("terminooooooooo");
            }
        }}catch(Exception e){
            throw new SQLException("DAO:  obtenerPrimeraFechaPago() : "+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return ret;
    }

}
