package com.tsp.operation.model.DAOS;

import java.sql.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;
import java.util.*;

public class Control_ExcepcionDAO extends MainDAO {
    private TreeMap cbxCliente;
    private LinkedList clientes;
    private BeanGeneral BeanCE;
    private Vector vector;
    /** Creates a new instance of ClienteDAO */
    public Control_ExcepcionDAO() {
        super("Control_ExcepcionDAO.xml");
    }
    public Control_ExcepcionDAO(String dataBaseName) {
        super("Control_ExcepcionDAO.xml", dataBaseName);
    }
    
    public boolean existeControl(String codcli,String Origen, String Destino, String agencia)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        clientes = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from control_excepcion where  codcli = ? and  origen = ? and destino = ? and agencia = ?");
                st.setString(1, codcli);
                st.setString(2, Origen);
                st.setString(3, Destino);
                st.setString(4, agencia);
                rs = st.executeQuery();
                return rs.next();
            }
            return false;
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS control_excepcion (existeControl) " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }  
    
    public void agregarControlE(BeanGeneral BeanControl)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            st = this.crearPreparedStatement("SQL_INGRESAR_CONTROL_EXCEP");
            st.setString(1, BeanControl.getValor_01());
            st.setString(2, BeanControl.getValor_02());
            st.setString(3, BeanControl.getValor_03());
            st.setString(4, BeanControl.getValor_05());
            st.setString(5, BeanControl.getValor_06());
            st.setString(6, BeanControl.getValor_07());
            st.setString(7, BeanControl.getValor_04());
           
           
            
            
            st.executeUpdate();
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DEL CONTROL EXCEPCION " + e.getMessage() + " " + e.getErrorCode());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_INGRESAR_CONTROL_EXCEP");
        }
    }
    public void bucarControlExcepcion (String codigo,String origen, String Destino, String agencia) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        BeanGeneral Bean    = null;
        vector = new Vector();
        int cont = 1;
        try {
            ////System.out.println("codigoDao : "+codCli);
            st = crearPreparedStatement("SQL_BUSCA_CONTROL_EXCEPCION");
            st.setString( 1,"%"+codigo+"%");
            st.setString( 2,"%"+origen+"%" );
            st.setString( 3,"%"+Destino+"%" );
            st.setString( 4,"%"+agencia+"%" );
      
            rs = st.executeQuery();
            while(rs.next()){
                Bean = new BeanGeneral();
                
                Bean.setValor_01 ((rs.getString("codcli")!=null)?rs.getString("codcli"):"");
                Bean.setValor_02 ((rs.getString("origen")!=null)?rs.getString("origen"):"");
                Bean.setValor_03 ((rs.getString("destino")!=null)?rs.getString("destino"):"");
                String pv= ((rs.getString("plan_viaje")!=null)?rs.getString("plan_viaje"):"");
                if(pv.equals("N")){
                    Bean.setValor_04("");
                }
                if(pv.equals("S")){
                    Bean.setValor_04("NO APLICA");
                }
                String hr= ((rs.getString("hoja_reporte")!=null)?rs.getString("hoja_reporte"):"");
                if(hr.equals("N")){
                    Bean.setValor_05("");
                }
                if(hr.equals("S")){
                    Bean.setValor_05("NO APLICA");
                }
                String per= ((rs.getString("pernoctacion")!=null)?rs.getString("pernoctacion"):"");
                if(per.equals("N")){
                    Bean.setValor_06("");
                }
                if(per.equals("S")){
                    Bean.setValor_06("NO APLICA");
                }
                Bean.setValor_07 ((rs.getString("agencia")!=null)?rs.getString("agencia"):"");
                Bean.setValor_08 ((rs.getString("norigen")!=null)?rs.getString("norigen"):"");
                Bean.setValor_09((rs.getString("ndestino")!=null)?rs.getString("ndestino"):"");
                Bean.setValor_10 ((rs.getString("nagencia")!=null)?rs.getString("nagencia"):"");
               
                Bean.setValor_11 (""+cont);
                
                this.vector.addElement( Bean );
                cont= cont + 1;
            }
            //return BeanCliente;
        }
        catch(Exception e){
             e.printStackTrace();
        }
        finally {
            if ( st != null ) { st.close(); }
            this.desconectar("SQL_BUSCA_CONTROL_EXCEPCION");
        }
        
    }
      
      public void modificarCotrolExcepcion(BeanGeneral BeanCE) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try{
            st = this.crearPreparedStatement("SQL_MODIFICAR_CONTROL_EXCEPCION");
           
            st.setString(1, BeanCE.getValor_05());
            st.setString(2, BeanCE.getValor_06());
            st.setString(3, BeanCE.getValor_07());
            st.setString(4, BeanCE.getValor_01());
            st.setString(5, BeanCE.getValor_02());
            st.setString(6, BeanCE.getValor_03());
            st.setString(7, BeanCE.getValor_04());
            
            
            
            
            //System.out.println(""+st.toString());
            st.executeUpdate();
        }catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error al MODIFICAR controlExcepcion [ControlExcepcionDAO.xml]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_MODIFICAR_CONTROL_EXCEPCION");
        }
    }
      
      
      
      
     public void buscarControl (String codigo,String origen,String destino, String agencia) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        BeanGeneral Bean   = null;
        vector = new Vector();
        try {
            ////System.out.println("codigoDao : "+codCli);
            st = crearPreparedStatement("BUSCAR_CONTROL");
            st.setString( 1, codigo );
            st.setString( 2, origen );
            st.setString( 3, destino );
            st.setString( 4, agencia );
            
            //System.out.println(""+ st.toString());
            rs = st.executeQuery();
            if(rs.next()){
                Bean = new BeanGeneral();
                
                Bean.setValor_01 ((rs.getString("codcli")!=null)?rs.getString("codcli"):"");
                Bean.setValor_02 ((rs.getString("origen")!=null)?rs.getString("origen"):"");
                Bean.setValor_03 ((rs.getString("destino")!=null)?rs.getString("destino"):"");
                Bean.setValor_04 ((rs.getString("plan_viaje")!=null)?rs.getString("plan_viaje"):"");
                Bean.setValor_05 ((rs.getString("hoja_reporte")!=null)?rs.getString("hoja_reporte"):"");
                Bean.setValor_06 ((rs.getString("pernoctacion")!=null)?rs.getString("pernoctacion"):"");
                Bean.setValor_07 ((rs.getString("agencia")!=null)?rs.getString("agencia"):"");
                        
                this.vector.addElement( Bean );
            }
            //return BeanCliente;
        }
        catch(Exception e){
             e.printStackTrace();
        }
        finally {
            if ( st != null ) { st.close(); }
            this.desconectar("BUSCAR_CONTROL");
        }
        
    }  
     
     public void eliminarControl (String codigo,String origen,String destino, String agencia) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try{
            st = this.crearPreparedStatement("SQL_ELIMINAR_CONTROL");
            
            st.setString(1, codigo);
            st.setString(2, origen);
            st.setString(3, destino);
            st.setString(4, agencia);
            //System.out.println(""+st.toString());
            ////System.out.println(st.toString());
            st.executeUpdate();
        }catch(Exception e) {
            throw new Exception("Error al ELIMINAR CONTROL EXCEPCION [CONTRL_EXCEPCIONDAO.xml]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_ELIMINAR_CONTROL");
        }
    }
      
    public java.util.Vector getVector() {
        return vector;
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        this.vector = vector;
    }
    
}


