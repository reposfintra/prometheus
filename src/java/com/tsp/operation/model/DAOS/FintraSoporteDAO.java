/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.FintraSoporteBeans;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.reporteOfertasBeans;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author mariana
 */
public interface FintraSoporteDAO {

    /**
     *
     * @param comprobante
     * @return
     */
    public ArrayList<FintraSoporteBeans> cargarComprobante(String comprobante);

    /**
     *
     * @param comprobante
     * @return
     */
    public String actualizarComprobante(String comprobante);

    /**
     *
     * @param comprobante
     * @return
     */
    public String eliminarComprobanteCabecera(String comprobante);

    /**
     *
     * @param comprobante
     * @return
     */
    public String eliminarComprobanteDetalle(String comprobante);

    /**
     *
     * @param info
     * @param usuario
     * @return
     */
    public String guardarExcelTemp(JsonObject info, Usuario usuario);

    /**
     *
     * @param usuario
     * @return
     */
    public String actualizarDocumentos(Usuario usuario);

    public String limpiar();

    public ArrayList<FintraSoporteBeans> cargarDocumentosPendientes();

    public ArrayList<FintraSoporteBeans> cargarTiposDocumentos();

    /**
     *
     * @param fechaInicio
     * @param fechaFin
     * @param user
     * @param tipoDocumento
     * @return
     */
    public ArrayList<FintraSoporteBeans> cargarDocumentos(String fechaInicio, String fechaFin, String user, String tipoDocumento);

    public String eliminarPreAprobadosUniversidad();

    /**
     *
     * @param negocio
     * @param nit
     * @param valor_ult_credito
     * @param valor_aprobado
     * @param usuario
     * @return
     */
    public String insertarPreAprobadosUniversidad(String negocio, String nit, Double valor_ult_credito, Double valor_aprobado, Usuario usuario);

    public String cargarEstadoOfertas();

    public String CustodiadaPor();

    public String Custodiador(String nit);

    /**
     *
     * @param estado_oferta
     * @param fechaInicio
     * @param fechaFin
     * @param multiservicio
     * @param cliente
     * @param lineanegocio
     * @return
     */
    public String cargarOfertas(String estado_oferta, String fechaInicio, String fechaFin, String multiservicio, String cliente, String lineanegocio);

    /**
     *
     * @param id_solicitud
     * @return
     */
    public ArrayList<reporteOfertasBeans> cargarAcciones(String id_solicitud);

    public ArrayList<reporteOfertasBeans> cargarDistribuciones();

    public ArrayList<reporteOfertasBeans> cargarDistribucionesLibres();

    public ArrayList<reporteOfertasBeans> cargarDistribucionesASoc();

    public ArrayList<reporteOfertasBeans> cargarSolicitudesLibres();

    public ArrayList<reporteOfertasBeans> cargarTipoProyectASoc(String cod_proyecto);

    public ArrayList<reporteOfertasBeans> cargarDistribucionASoc(String cod_distribucion);

    public ArrayList<reporteOfertasBeans> cargarTipoProyecto();

    public ArrayList<reporteOfertasBeans> cargarTipoCliente();

    /**
     *
     * @param usuario
     * @param tipo_proyecto
     * @return
     */
    public String guardarTipoProyecto(Usuario usuario, String tipo_proyecto);

    /**
     *
     * @param usuario
     * @param tipocliente
     * @return
     */
    public String guardarTipoCliente(Usuario usuario, String tipocliente);

    /**
     *
     * @param usuario
     * @param tipo_proyecto
     * @param cod_proyecto
     * @return
     */
    public String actualizarTipoProyecto(Usuario usuario, String tipo_proyecto, String cod_proyecto);

    /**
     *
     * @param usuario
     * @param tipoCliente
     * @param cod_tipoCliente
     * @return
     */
    public String actualizarTipoCliente(Usuario usuario, String tipoCliente, String cod_tipoCliente);

    /**
     *
     * @param usuario
     * @param cod_proyecto
     * @return
     */
    public String anularTipoProyecto(Usuario usuario, String cod_proyecto);

    /**
     *
     * @param usuario
     * @param cod_tipoCliente
     * @return
     */
    public String anularTipoCliente(Usuario usuario, String cod_tipoCliente);

    /**
     *
     * @param usuario
     * @param distribucion
     * @param opav
     * @param fintra
     * @param interv
     * @param provin
     * @param eca
     * @param iva
     * @param vlr_agregado
     * @return
     */
    public String guardarDistribuciones(Usuario usuario, String distribucion, String opav, String fintra, String interv, String provin, String eca, String iva, String vlr_agregado);

    /**
     *
     * @param usuario
     * @param oferta
     * @param tipo_numos
     * @return
     */
    public String guardarDistribuciones(Usuario usuario, String oferta, String tipo_numos);

    /**
     *
     * @param usuario
     * @param distribucion
     * @return
     */
    public String guardarDistribuciones(Usuario usuario, String distribucion);

    public String cargarTipoMultiservicio();

    /**
     *
     * @param distribucion
     * @return
     */
    public String verificarTipoSolicitud(String distribucion);

    /**
     *
     * @param usuario
     * @param informacion
     * @param cod_proyecto
     * @return
     */
    public String guardarRelacionTipoProyectSoli(Usuario usuario, JsonArray informacion, String cod_proyecto);

    /**
     *
     * @param informacion
     * @param cod_proyecto
     * @return
     */
    public String quitarRelacionTipoProyectSoli(JsonArray informacion, String cod_proyecto);

    /**
     *
     * @param informacion
     * @param cod_distribucion
     * @return
     */
    public String guardarRelacionDistrSoli(JsonArray informacion, String cod_distribucion);

    /**
     *
     * @param informacion
     * @param cod_distribucion
     * @return
     */
    public String quitarRelacionDistrSoli(JsonArray informacion, String cod_distribucion);

    public String cargarLineaNegocio();

    /**
     *
     * @param multiservicio
     * @param cliente
     * @return
     */
    public ArrayList<reporteOfertasBeans> buscarMultiservicio(String multiservicio, String cliente);

    public String buscarMultiservicio(Usuario usuario);

    /**
     *
     * @param mltiservicio
     * @return
     */
    public ArrayList<reporteOfertasBeans> buscarClienteMultiservicio(String mltiservicio);

    /**
     *
     * @param usuario
     * @param informacion
     * @return
     */
    public String guardarExcluidos(Usuario usuario, JsonArray informacion);

    /**
     *
     * @param usuario
     * @param informacion
     * @return
     */
    public String guardarIncluidos(Usuario usuario, JsonArray informacion);

    /**
     *
     * @param multiservicio
     * @param id_cliente
     * @return
     */
    public ArrayList<reporteOfertasBeans> cargarExcluidos(String multiservicio, String id_cliente);

    /**
     *
     * @param usuario
     * @return
     */
    public String anularHistPreAprobUnivActual(Usuario usuario);

    /**
     *
     * @param unidad_negocio
     * @param usuario
     * @return
     */
    public String insertarHistPreAprobadosUniversidad(String unidad_negocio, Usuario usuario);

    public String cargarUnidadesNegocio(String filter);

    /**
     *
     * @param id_und_negocio
     * @return
     */
    public String eliminarPreAprobados(String id_und_negocio);

    /**
     *
     * @param id_und_negocio
     * @param negocio
     * @param nombre
     * @param nit
     * @param valor_ult_credito
     * @param incremento
     * @param und_negocio
     * @param valor_aprobado
     * @param afiliado
     * @param fecha_desembolso
     * @param usuario
     * @param fecha_vencimiento
     * @param departamento
     * @param ciudad
     * @param direccion
     * @param periodo_desembolso
     * @param barrio
     * @param telefono
     * @param valor_saldo
     * @return
     */
    public String insertarPreAprobados(String id_und_negocio, String nit, String nombre, Double valor_ult_credito, Double valor_aprobado, Double incremento, String und_negocio, String negocio, String afiliado, String fecha_desembolso, String periodo_desembolso, String fecha_vencimiento, String departamento, String ciudad, String direccion, String barrio, String telefono, Double valor_saldo, Usuario usuario);

    /**
     *
     * @param id_und_negocio
     * @param usuario
     * @return
     */
    public String anularHistPreAprobadosActual(String id_und_negocio, Usuario usuario);

    /**
     *
     * @param id_und_negocio
     * @param usuario
     * @return
     */
    public String insertarHistPreAprobados(String id_und_negocio, Usuario usuario);

    public String cargarEndosoFacturas();

    /**
     *
     * @param nomproces
     * @param nit
     * @param custodiador
     * @param cuenta
     * @param cmc
     * @param usuario
     * @return
     */
    public String guardarFacturaEndoso(String nomproces, String nit, String custodiador, String cuenta, String cmc, Usuario usuario);

    /**
     *
     * @param nomproces
     * @param nit
     * @param custodiador
     * @param cuenta
     * @param cmc
     * @param usuario
     * @param id
     * @return
     */
    public String actualizarFacturaEndoso(String nomproces, String nit, String custodiador, String cuenta, String cmc, Usuario usuario, String id);

    /**
     *
     * @param negocio
     * @param cedula
     * @return
     */
    public String cargarNegociosPoliza(String polizas, String tipo_poliza, String aseguradora, String convenio, String cliente, String estado, String nombre_query);

    public String cargarDocumentosAjustepeso();

    /**
     *
     * @param info
     * @param usuario
     * @return
     */
    public String guardarExcelAjustepeso(JsonObject info, Usuario usuario);

    public String limpiarAjustePeso();

    public String AjustarPeso();
    
    public String generarPreAprobados(String query, String id_unidad_negocio, String periodo);
    
    public String insertarPreAprobadosFintraCredit(String id_und_negocio,  String periodo, String negocio, String cedula_deudor, String nombre_deudor, String telefono, String celular, String direccion, String barrio, String ciudad, String email, String cedula_codeudor,  String nombre_codeudor, String telefono_codeudor, String celular_codeudor, String cuotas, String id_convenio, String afiliado, String tipo, String fecha_desembolso, String fecha_ult_pago, String dias_pagos, double vr_negocio, double valor_factura, double valor_saldo, double porcentaje, String altura_mora, double valor_preaprobado);

    public boolean isAllowToGeneratePreAprobados(String usuario);

    public String CargarControlCuentas();

    /**
     *
     * @param id
     * @return
     */
    public String cambiarEstadoControlCuentas(String id);

    public String cargarUnidadNegocio();

    /**
     *
     * @param usuario
     * @param tiponegocio
     * @param ixm
     * @param gac
     * @param cabing
     * @param deting
     * @return
     */
    public String guardarControlCuentas(Usuario usuario, String tiponegocio, String ixm, String gac, String cabing, String deting);

    /**
     *
     * @param usuario
     * @param tiponegocio
     * @param ixm
     * @param gac
     * @param cabing
     * @param deting
     * @param id
     * @param cod_controlcuentas
     * @return
     */
    public String updateControlCuentas(Usuario usuario, String tiponegocio, String ixm, String gac, String cabing, String deting, String id, String cod_controlcuentas);
    
    public String cargarComboTipoPolizas();

    /**
     *
     * @param poliza
     * @return
     */
    public String cargarComboAseguradoras(String poliza);

    public String cargarComboTodasAseguradoras();

    public String cargarComboConvenios();

    /**
     *
     * @param codigo_fasecolda
     * @return
     */
    public String verificarCodigoFasecolda(String codigo_fasecolda);

    /**
     *
     * @param poliza
     * @param aseguradora
     * @return
     */
    public String cargarConfigPoliza(String poliza, String aseguradora);

    /**
     *
     * @param usuario
     * @param id_config_poliza
     * @param neg_vehiculo
     * @param nomcliente
     * @param nit
     * @param afiliado
     * @param placa
     * @param servicio
     * @param Vlrpoliza
     * @param fecha_inicio
     * @param fecha_fin
     * @param codigo_fasecolda
     * @param tipo
     * @return
     */
    public String ActualizarPGV(Usuario usuario, String id_config_poliza, String neg_vehiculo, String nomcliente, String nit, String afiliado, String placa, String servicio, String Vlrpoliza, String fecha_inicio, String fecha_fin, String codigo_fasecolda, String tipo);

    /**
     *
     * @param usuario
     * @param info
     * @return
     */
    public String datosPdf(Usuario usuario, JsonObject info);

    /**
     *
     * @param usuario
     * @param neg_vehiculo
     * @param fecha_fin
     * @return
     */
    public String marcarPGV(Usuario usuario, String neg_vehiculo ,String fecha_fin);
    
    public String cargarObservaciones(String id_solicitud);
    
    public String CargarCuotaManejo();
    
    public String cambiarEstadoCuotaManejo(String id);
    
    public String actualizarCuotaManejo(Usuario usuario, String id, String valor);

    public String CargarPolizas();

    /**
     *
     * @param usuario
     * @param nombre_poliza
     * @param descripcion
     * @return
     */
    public String guardarPoliza(Usuario usuario, String nombre_poliza, String descripcion);

    
    /**
     *
     * @param usuario
     * @param nombre_poliza
     * @param descripcion
     * @param id
     * @return
     */
    public String actualizarPoliza(Usuario usuario, String nombre_poliza, String descripcion, String id);

   /**
     *
     * @param unidad_negocio
     * @return
     */
    public Object cargarConvenios(String unidad_negocio);

    public String actualizarConvenio(Usuario usuario, String convenio, String tasa_interes, String tasa_usura, String tasa_compra_cartera, String id_convenio,String tasa_aval,String tasa_max_fintra,String tasa_sic_ea);

    /**
     *
     * @param usuario
     * @param unidad_negocio
     * @return
     */
    public String actualizarConveniosxUnidadNegocio(Usuario usuario, String tasa_interes, String tasa_usura, String tasa_compra_cartera, String unidad_negocio,String tasa_avalc);

    public String cambiarEstadoPoliza(Usuario usuario,String id);

    public String reportecostos();

    public String detallecostos(String multiservicio, String id_solicitud);

    public String listarInsumos();

    public String ActualizarInsumo(Usuario usuario, String id, String codigo_material, String descripcion);

    public String cargarValores();

    public String actualizarValores(Usuario usuario, String id, String valor_xdefecto);

    public String cargarSubCategorias();

    public String actualizarSubcategoria(Usuario usuario, String id, String nombre);
    
    public String cargarCategorias();

    public String actualizarCategoria(Usuario usuario, String id, String nombre);

    public String cargarApu();

    public String actualizarApu(Usuario usuario, String id, String nombre);

    public String cargarEspecificaciones();

    public String actualizarEspecificacion(Usuario usuario, String id, String nombre);
    
    public String consultarClasificacionClientes(String id_unidad_negocio, String periodo);
    
    public String cargarAnioImpuesto(String anio);

    public String cargarAnticipos(String conductor, String placa, String planilla);

    public Object autorizarAnticipo(String id);

    public String mostrarPresolicitud(String numero_solicitud);

    public String marcarRecoleccionFirmas(String numero_solicitud, Usuario usuario);

    public String buscarNegocioAreactivar(String cod_neg, String fechaInicio, String fechaFin);

    public String reactivarNegocio(Usuario usuario, String cod_neg);

    public String reactivarSolicitud(Usuario usuario, String cod_neg);

    public String insertarTrazabilidad(Usuario usuario, String cod_neg, String comentario);

    public String historicoConvenios(Usuario usuario, String convenio, String tasa_interes, String tasa_usura, String tasa_compra_cartera, String id_convenio,String tasa_aval,String tasa_max_fintra,String tasa_sic_ea);

    public String historicoConveniosMasivo(Usuario usuario, String unidad_negocio, String tasa_usura, String tasa_compra_cartera, String tasa_interes,String tasa_avalc);

    public String cargarNuevasPolizas();
            
    public boolean insertarNuevasPolizas(Usuario usuario, String descripcion) throws SQLException;
    
    public boolean actualizarNuevasPolizas(Usuario usuario, String descripcion, int id) throws SQLException;
        
    public boolean cambiarEstadoNuevasPolizas(String usuario, int id, String estado);
    
    public String cargarConfiguracionPoliza(int sucursal, int unidadNegocio);
    
    public boolean insertarConfiguracionPoliza(Usuario usuario, int poliza, int aseguradora, int tipoCobro, int valorPoliza, int unidadNegocio, int sucursal) throws SQLException ;
    
    public boolean actualizarConfiguracionPoliza(Usuario usuario, int poliza, int aseguradora, int tipoCobro, int valorPoliza, int unidadNegocio, int sucursal, int id) throws SQLException;
    
    public boolean cambiarEstadoConfiguracionPoliza(String usuario, int id, String estado);
    
    public String cargarAseguradoras();
    
    public boolean insertarAseguradoras(Usuario usuario, String descripcion, long nit, float retorno, int plazoPago, int prr) throws SQLException;
        
    public boolean actualizarAseguradoras(Usuario usuario, String descripcion, float retorno, int plazoPago, int prr, int id) throws SQLException;
    
    public boolean cambiarEstadoAseguradora(String usuario, int id, String estado);
    
    public String cargarTipoCobro();
    
    public boolean insertarTipoCobro(Usuario usuario, String descripcion, String tipo, String financiacion) throws SQLException;
    
    public boolean actualizarTipoCobro(Usuario usuario, String descripcion, String tipo, String financiacion, int id) throws SQLException;
    
    public boolean cambiarEstadoTipoCobro(String usuario, int id, String estado);
    
    public String cargarValorPoliza();
    
    public boolean insertarValorPoliza(Usuario usuario, String descripcion, String tipo, String calcularSobre, float valorPorcentaje, float valorAbsoluto, boolean iva) throws SQLException;
    
    public boolean actualizarValorPoliza(Usuario usuario, String descripcion, String tipo, String calcularSobre, float valorPorcentaje, float valorAbsoluto, boolean iva, int id) throws SQLException ;
    
    public boolean cambiarEstadoValorPoliza(String usuario, int id, String estado);
    
    public String cargarSucursalesUnidad(int unidadNegocio, String ciudad, String departamento);
    
    public Map cargarDepartamentos() throws SQLException;
    
    public Map cargarCiudades(String dpto) throws SQLException;
    
    public int aplicarConfiguracionesPolizas(String[] ids, String usuario) throws SQLException ;
    
    public String guardarExcelFacturacion(JsonObject info, Usuario usuario);
    
    public String cargarExtractosPorGenerar(String lote_generado);

    public String cargarComboExtractosPorGenerar();

    public String generarExtractosDigitales(String lote_generado, Usuario usuario);

    public String cargarExtractosPorEnviarSms(String lote_generado);

    public String cargarComboExtractosPorEnviarSms();
}
