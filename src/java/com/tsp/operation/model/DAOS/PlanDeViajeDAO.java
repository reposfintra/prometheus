 /***************************************
    * Nombre Clase ............. PlanDeViajeDAO.java
    * Descripci�n  .. . . . . .  Generamos Los Query para manejar los Planes de  Viajes.
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  10/10/2005
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/



package com.tsp.operation.model.DAOS;



import java.io.*;
import java.util.*;
import java.sql.*;
import com.tsp.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;




public class PlanDeViajeDAO {
    
    /** ____________________________________________________________________________________________
                                              ATRIBUTOS
        ____________________________________________________________________________________________ */
    
    
    
    
    private static String SEPERADOR  = ":";
    
    
    
    private static String TIPO_PARQUEADERO = "PAR";
    private static String TIPO_PERNOTADERO = "PER";
    private static String TIPO_TANQUEO     = "GAS";
    private static String TIPO_RESTAURANTE = "RES";
    
    
    
    private static String FORMAT_PLV = 
     
    
    " SELECT                                              "+                            
    "         a.*,                                        "+
    "         b.nombre      as nameCond,                  "+
    "         b.direccion   as dirCond,                   "+
    "         b.codciu      as ciudadCond,                "+
    "         b.telefono    as telCond,                   "+
    "         b.celular     as celCond,                   "+
    "         c.nombre      as nameProp,                  "+
    "         c.direccion   as dirProp,                   "+
    "         c.codciu      as ciudadProp,                "+
    "         c.telefono    as telProp                    "+ 
    "    FROM                                             "+
    "    (                                                "+
    "          SELECT                                     "+
    "                a.*,                                 "+
    "                b.desctipocarga,                     "+
    "                c.numcaravana                        "+
    "          FROM                                       "+ 
    "             (                                       "+
    "                SELECT                               "+        
    "                    a.numpla       as oc,            "+        
    "                    a.cia          as distrito,      "+        
    "                    a.fecdsp       as fechaDespacho, "+        
    "                    a.oripla       as origen,        "+        
    "                    a.despla       as destino,       "+        
    "                    a.plaveh       as placa,         "+        
    "                    a.ruta_pla     as ruta,          "+        
    "                    a.contenedores as contenedores,  "+        
    "                    a.platlr       as placaUnidad,   "+        
    "                    a.cedcon       as cedulaCond,    "+        
    "                    a.nitpro       as nitProp,       "+        
    "                    c.destinatario as destinatario,  "+        
    "                    d.propietario  as propietario,   "+        
    "                    c.codtipocarga as tipocarga,     "+        
    "                    a.platlr       as trailer,       "+
    "                    a.reg_status   as estado         "+
    "               FROM                                  "+       
    "                   PLANILLA a,                       "+      
    "                   PLAREM   b,                       "+      
    "                   REMESA   c,                       "+      
    "                   PLACA    d                        "+
     "               WHERE                                "+       
    "                       a.numpla  = ?                 "+
    "                  and  a.cia     = ?                 "+          
    "                  and  b.numpla  = a.numpla          "+          
    "                  and  c.numrem  = b.numrem          "+          
    "                  and  d.placa   = a.plaveh          "+
    "              ) a                                    "+
    "             LEFT JOIN                               "+
    "             tipocarga  b                            "+
    "             ON                                      "+
    "                 b.codtipocarga = a.tipocarga        "+    
    "             LEFT JOIN                               "+
    "             caravana  c                             "+
    "             ON                                      "+
    "                 c.numpla       = a.oc               "+  
    
    "    ) a                                              "+
    "    LEFT JOIN                                        "+
    "    NIT b                                            "+
    "      ON  b.cedula  =  LTRIM( a.cedulaCond, '0')     "+          
    "    LEFT JOIN                                        "+ 
    "    NIT c                                            "+
    "      ON  c.cedula  =   a.propietario                ";

    
    
    
    
    
     private static String SAVE_PLV   =
    "    INSERT INTO  plan_viaje  "+
    "    (                        "+
    "       dstrct          ,     "+
    "       planilla        ,     "+
    "       fecha           ,     "+
    "       cedcon          ,     "+
    "       radio           ,     "+
    "       celular         ,     "+
    "       avantel         ,     "+
    "       telefono        ,     "+
    "       cazador         ,     "+
    "       movil           ,     "+
    "       otro            ,     "+
    "       nomfam          ,     "+
    "       telfam          ,     "+
    "       nitpro          ,     "+
    "       comentario      ,     "+
    "       retorno         ,     "+
    "       productos       ,     "+
    "       agencia         ,     "+ 
    "       usuario         ,     "+
    "       creation_user         "+
    "    )                        "+
    "    VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)  ";

     
     
     
    private static String SQL_SAVE_UBICACIONES   =
    " INSERT INTO plan_viaje_ubicacion  "+
    " (                                 "+
    "   dstrct,                         "+
    "   planilla,                       "+
    "   cod_ubicacion,                  "+
    "   creation_user                   "+
    " )                                 "+
    " VALUES ( ?,?,?,? )                ";

    
    
    
    
    private static String SEARCH_PLV = 

    "     SELECT                  "+
    "       dstrct          ,     "+
    "       planilla        ,     "+
    "       fecha           ,     "+
    "       cedcon          ,     "+
    "       radio           ,     "+
    "       celular         ,     "+
    "       avantel         ,     "+
    "       telefono        ,     "+
    "       cazador         ,     "+
    "       movil           ,     "+
    "       otro            ,     "+
    "       nomfam          ,     "+
    "       telfam          ,     "+
    "       nitpro          ,     "+
    "       comentario      ,     "+
    "       retorno         ,     "+
    "       leido           ,     "+
    "       productos       ,     "+
    "       agencia         ,     "+ 
    "       usuario         ,     "+
    "       creation_user   ,     "+
    "  substr(creation_date,1,19) as creation_date "+
   " FROM   plan_viaje            "+
   " WHERE   dstrct      = ?      "+
   "     and planilla    = ?      ";
    
    
    
    
    
     private static String SQL_SEARCH_UBICACIONES   =
        "  SELECT                             "+
        "	   A.dstrct,                  "+
        "	   A.planilla,                "+
        "	   A.cod_ubicacion,           "+
        "	   B.descripcion,             "+
        "	   B.cia,                     "+
        "	   B.tipo,                    "+
        "	   B.pais,                    "+
        "	   B.estado,                  "+ 
        "	   B.ciudad,                  "+
        "	   B.direccion,               "+
        "	   B.torigen,                 "+
        "	   B.tdestino,                "+
        "	   B.relacion,                "+
        "	   B.tiempo,                  "+
        "	   B.distancia,               "+
        "	   B.tel1,                    "+
        "	   B.tel2,                    "+
        "	   B.tel3,                    "+
        "	   B.dstrct,                  "+
        "	   B.modalidad                "+
        " FROM  plan_viaje_ubicacion  A,      "+
        "      ubicacion              B       "+
        " WHERE                               "+
        "      A.dstrct        = ?            "+ 
        " and  A.planilla      = ?            "+ 
        " and  A.cod_ubicacion = B.cod_ubicacion "+
        " and  B.tipo          = ?               "; 

     
     
     
        
     private static String DELETE_PLV             =   " DELETE FROM   plan_viaje           WHERE   dstrct = ?  and  planilla = ? ";
     
          
     private static String DELETE_UBICACIONES_PLV =   " DELETE FROM   plan_viaje_ubicacion  WHERE  dstrct = ?  AND planilla = ?  ";
     
     
     
     
     private static String MODIFY_PLV =
        "UPDATE  plan_viaje               "+
        "SET                              "+
        "       dstrct          = ? ,     "+
        "       planilla        = ? ,     "+
        "       fecha           = ? ,     "+
        "       cedcon          = ? ,     "+
        "       radio           = ? ,     "+
        "       celular         = ? ,     "+
        "       avantel         = ? ,     "+
        "       telefono        = ? ,     "+
        "       cazador         = ? ,     "+
        "       movil           = ? ,     "+
        "       otro            = ? ,     "+
        "       nomfam          = ? ,     "+
        "       telfam          = ? ,     "+
        "       nitpro          = ? ,     "+
        "       comentario      = ? ,     "+
        "       retorno         = ? ,     "+
        "       productos       = ? ,     "+
        "       agencia         = ? ,     "+ 
        "       usuario         = ? ,     "+
        "       creation_user   = ?       "+
        " WHERE                           "+
        "      dstrct           =?        "+
        " AND  planilla         =?        ";
    
    
     
     
     
     
     
     
     
     
     private static String SEARCH_RUTA =
     
       " SELECT                                            "+
       "       origen||destino||secuencia as via,          "+
       "       descripcion                as descripcion,  "+
       "       via                        as ruta          "+
       " FROM                                              "+
       "       via                                         "+
       " WHERE origen||destino=?                           "+
       " ORDER BY secuencia                                ";
     
    
     
     private static String UPDATE_RUTA_PLANILLA =
     " UPDATE PLANILLA SET  ruta_pla = ?   WHERE  numpla = ?  and  cia=? ";
     
     
     
     
     private static String PLACAS_ESCOLTA =    " SELECT  placa             "+
                                               " FROM    escolta_vehiculo  "+
                                               " WHERE   numpla =  ?       ";

     
     private static String PLACAS_CARAVANAS =      
                                               " SELECT distinct (placa )   "+
                                               " FROM   escolta_caravana    "+ 
                                               " WHERE  numcaravana  = ?    ";
     
                                             /*  " SELECT                           "+
                                               "        a.numpla as   planilla,   "+
                                               "        b.plaveh as   placa       "+
                                               " FROM                             "+
                                               "        caravana  a,              "+
                                               "        planilla  b               "+
                                               " WHERE                            "+
                                               "        a.numcaravana = ?         "+
                                               "  and   b.numpla      = a.numpla  ";*/


     
     private static String NOMBRE_CIUDAD = " SELECT nomciu  FROM  ciudad  WHERE  codciu = ? ";
     
     
     
     
     private static String SEARCH_ZONA = 
                                          "  SELECT                  "+   
                                          "     a.*,                 "+
                                          "     b.desczona           "+
                                          "  FROM                    "+
                                          "   (                      "+
                                          "    SELECT  nomciu  ,     "+
                                          "            zona          "+
                                          "    FROM    ciudad        "+
                                          "    WHERE   codciu  = ?   "+
                                          "   )    a                 "+
                                          "  LEFT JOIN               "+
                                          "  zona  b                 "+
                                          "  ON                      "+
                                          "    b.codzona =  a.zona   ";
     
     
     
     private static String DATOS_TRAFICO =
                                        " SELECT                                        "+
                                        "       a.zona,                                 "+
                                        "       c.desczona,                             "+
                                        "       a.pto_control_proxreporte,              "+
                                        "       b.nomciu                                "+
                                        "FROM                                           "+
                                        "       trafico  a,                             "+
                                        "       ciudad   b,                             "+
                                        "       zona     c                              "+
                                        "WHERE                                          "+
                                        "       a.planilla = ?                          "+ 
                                        " and   b.codciu   = a.pto_control_proxreporte  "+
                                        " and   c.codzona  = a.zona                     ";
     
     
     
     
     
     private static String SQL_DATOS_INGRESO_TRAFICO =
                                               " SELECT                              "+
                                               "         zona ,                      "+
                                               "         nomzona ,                   "+
                                               "         pto_control_proxreporte ,   "+
                                               "         nompto_control_proxreporte, "+
                                               "         fecha_salida                "+
                                               "  FROM                               "+
                                               "        ingreso_trafico              "+
                                               "  WHERE                              "+
                                               "         planilla = ?                ";

          
     
     
     private static String UPDATE_INGRESO_TRAFICO =      
                                               " UPDATE  ingreso_trafico      "+
                                               " SET                          "+
                                               "     fecha_salida       = ?,  "+
                                               "     fecha_prox_reporte = ?,  "+
                                               "     cel_cond           = ?,  "+
                                               "     fecha_ult_reporte  = ?   "+
                                               " WHERE                        "+
                                               "     planilla           = ?   ";

     
     
     private static String UPDATE_TRAFICO = 
                                               " UPDATE  trafico               "+
                                               " SET                           "+                                               
                                               "     fecha_salida        = ?,  "+
                                               "     fecha_prox_reporte  = ?,  "+   
                                               "     cel_cond            = ?,  "+
                                               "     fecha_ult_reporte   = ?   "+
                                               " WHERE                         "+
                                               "     planilla            = ?   ";

     
     
     private static String UPDATE_INGRESO_TRAFICO_CAMBIO_VIA = 
      "  UPDATE  ingreso_trafico                  "+
      "  SET                                      "+
      "         pto_control_proxreporte    = ?,   "+
      "         nompto_control_proxreporte = ?,   "+
      "         zona                       = ?,   "+ 
      "         nomzona                    = ?,   "+
      "         via                        = ?    "+
      "  WHERE                                    "+ 
      "         planilla  = ?                     ";
     
     
            
     
     private static String UPDATE_TRAFICO_CAMBIO_VIA = 
     "   UPDATE trafico                  "+
     "   SET                             "+
     "     zona                    = ?,  "+
     "     pto_control_proxreporte = ?   "+
     "  WHERE                            "+
     "     planilla     = ?              ";
     
     
     
     
     
    private static final String SQL_DURACION_TRAMO       = 
    
   " SELECT                                                                                     "+
   "     ( CAST( ? AS TIMESTAMP) +  CAST( TIEMPO ||' HOUR' AS INTERVAL))::varchar  as proxima   "+ 
   " FROM    TRAMO                                                                              "+
   " WHERE   ORIGIN = ? AND DESTINATION = ?                                                     ";
    
    
    
    
    
    
    
    private static final String SQL_UBICACION  = 
    
   " SELECT                    "+
   "        cod_ubicacion,     "+
   "        descripcion,       "+
   "        cia,               "+ 
   "        tipo,              "+ 
   "        pais,              "+
   "        estado,            "+
   "        ciudad,            "+
   "        direccion,         "+
   "        torigen,           "+
   "        tdestino,          "+ 
   "        relacion,          "+
   "        tiempo,            "+
   "        distancia,         "+
   "        tel1,              "+
   "        tel2,              "+
   "        tel3,              "+
   "        dstrct,            "+
   "        modalidad          "+ 
   " FROM   ubicacion          ";
    
    
        

   private static final String SQL_UBICACION_TRAMOS       =     
   
   " WHERE  modalidad = 'T'    "+
   "    and tipo      = ?      "+
   "    and torigen   = ?      "+
   "    and tdestino  = ?      ";
    
    
   
    private static final String SQL_UBICACION_CIUDAD       = 
   " WHERE                     "+
   "       modalidad = 'C'     "+
   "   and tipo      = ?       "+
   "   and ciudad    = ?       ";
    
    
    
    
    
    
     
    
    /** ____________________________________________________________________________________________
                                              METODOS
        ____________________________________________________________________________________________ */
    
    
    
    public PlanDeViajeDAO() {
    }
     
     /**
     * Carga los datos obtenidos de la tabla de Ubicacion
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/   
    public Ubicacion loadUbicacion(ResultSet rs)throws Exception{
        Ubicacion ub = new Ubicacion();
        try{
            ub.setCod_ubicacion( rs.getString("cod_ubicacion")  );            
            ub.setDescripcion  ( rs.getString("descripcion")    );
            ub.setDireccion    ( rs.getString("direccion")      );
            ub.setTel1         ( rs.getString("tel1")           );
            ub.setTel2         ( rs.getString("tel2")           );
            ub.setTel3         ( rs.getString("tel3")           );
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return ub;
    }
    
    /**
     * Plan de Viajes Creados
     * @autor.......fvillacob 
     * @parameter... String oc, String distrito         
     * @throws......Exception
     * @version.....1.0.     
     **/ 
    
    public PlanDeViaje search(String oc , String distrito)throws Exception{
      PoolManager poolManager = PoolManager.getInstance();
      Connection con          = poolManager.getConnection("fintra");
      if (con == null)
            throw new SQLException("Sin conexion");
      PreparedStatement st        = null;
      ResultSet         rs        = null;
      PlanDeViaje       planViaje = null;
      PlanDeViaje       complemet = null;
      try {            
          st= con.prepareStatement(this.SEARCH_PLV);
          st.setString(1, distrito);
          st.setString(2, oc);          
          rs=st.executeQuery();
          while(rs.next()){
            planViaje = new   PlanDeViaje();
              planViaje.loadSearch(rs);  
              complemet = this.formarPlanViaje(oc, distrito);
              planViaje.complemet( complemet ); 
              
          // Ubicacion plan:
              planViaje.setListAlimentacionPlan( this.searchUbicacionPlan( planViaje.getDistrito(),  planViaje.getPlanilla(), this.TIPO_RESTAURANTE  ) );
              planViaje.setListParqueaderoPlan ( this.searchUbicacionPlan( planViaje.getDistrito(),  planViaje.getPlanilla(), this.TIPO_PARQUEADERO  )  );
              planViaje.setListPernotacionPlan ( this.searchUbicacionPlan( planViaje.getDistrito(),  planViaje.getPlanilla(), this.TIPO_PERNOTADERO  )  );
              planViaje.setListTanqueoPlan     ( this.searchUbicacionPlan( planViaje.getDistrito(),  planViaje.getPlanilla(), this.TIPO_TANQUEO      )  );
              
            break;   
          }
      }
      catch(SQLException e){
          throw new SQLException(" DAO: No se pudo buscar el Plan de Viaje.-->"+ e.getMessage());
      }  
      finally{
         if(st!=null) st.close();
         if(rs!=null) rs.close();
         poolManager.freeConnection("fintra",con);
      } 
      return planViaje;
    }
    
    
    
    
    
    
    
    
    
    /**
     * Listado de Ubicaciones para el Plan de Viaje
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/  
     public List searchUbicacionPlan( String distrito, String planilla,  String tipo)throws Exception{
         
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("fintra");
          if (con == null)
                throw new SQLException("Sin conexion");        
          PreparedStatement st        = null;
          ResultSet         rs        = null;
          List              lista     = new LinkedList();          
          try {
                st= con.prepareStatement(this.SQL_SEARCH_UBICACIONES);
                st.setString(1, distrito );
                st.setString(2, planilla );
                st.setString(3, tipo );
                rs=st.executeQuery();
                while(rs.next()){
                       Ubicacion ubicacion = this.loadUbicacion(rs);
                       lista.add(ubicacion);
                }
          }
          catch(Exception e){
              throw new SQLException(" DAO: searchUbicacion -->"+ e.getMessage());
          }  
          finally{
             if(st!=null)  st.close();
             if(rs!=null)  rs.close();
             poolManager.freeConnection("fintra",con);
          } 
          return lista;
    }
     
    
    
    
    
    
    
    
    
    /**
     * Placas que acompa�as a la planilla en la caravana
     * @autor.......fvillacob 
     * @parameter... String oc, String distrito         
     * @throws......Exception
     * @version.....1.0.     
     **/ 
     public String placasCaravanas( PlanDeViaje plan)throws Exception{
         
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("fintra");
          if (con == null)
                throw new SQLException("Sin conexion");
        
          PreparedStatement st        = null;
          ResultSet         rs        = null;
          String            escoltas  = "";
          try {
              int caravana  =  Integer.parseInt( (plan.getCaravana().equals(""))?"0":plan.getCaravana());
              
              // Escolta a la Caravana:
              st= con.prepareStatement(this.PLACAS_CARAVANAS);
              st.setInt(1, caravana);
              rs=st.executeQuery();
              while(rs.next()){
                 String placa = rs.getString("placa");
                 if( ! placa.equals(plan.getPlaca() ))
                      escoltas += placa + " - ";
              }

             // Escolta a la planilla
              st= con.prepareStatement(this.PLACAS_ESCOLTA);
              st.setString(1, plan.getPlanilla());
              rs=st.executeQuery();
              while(rs.next()){
                 String placa = rs.getString("placa");
                 if( ! placa.equals(plan.getPlaca() ))
                      escoltas += placa + " - ";
              }
              
              
          }
          catch(SQLException e){
              throw new SQLException(" DAO: No se updateTrafico-->"+ e.getMessage());
          }  
          finally{
             if(st!=null)  st.close();
             if(rs!=null)  rs.close();
             poolManager.freeConnection("fintra",con);
          } 
          return escoltas;
    }
     
     
     
     
     
     
    
      
      
      /**
     * Nombre y codigo de la zona para el PC
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/ 
      public String getZona( String codigo )throws Exception{
          
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("fintra");
          if (con == null)
                throw new SQLException("Sin conexion");
        
          PreparedStatement st        = null;
          ResultSet         rs        = null;
          String            zona      = codigo;
          try {
              st= con.prepareStatement(this.SEARCH_ZONA);
              st.setString(1, codigo);
              rs=st.executeQuery();
              while(rs.next())
                  zona = rs.getString("zona") +  SEPERADOR + rs.getString("desczona");
          }
          catch(SQLException e){
              throw new SQLException(" DAO: No se pudo buscar nombre de zona -->"+ e.getMessage());
          }  
          finally{
             if(st!=null)  st.close();
             if(rs!=null)  rs.close();
             poolManager.freeConnection("fintra",con);
          } 
          return zona;
    }
      
    
      
      
      
     /**
     * Datos de la tabla trafico relacionados a la planilla
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/ 
      public PlanDeViaje getDatosTrafico( PlanDeViaje plan)throws Exception{
          
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("fintra");
          if (con == null)
                throw new SQLException("Sin conexion");
        
          PreparedStatement st        = null;
          ResultSet         rs        = null;
          try {
                            
                        
                  st= con.prepareStatement(this.SQL_DATOS_INGRESO_TRAFICO);
                  st.setString(1, plan.getPlanilla());
                  rs=st.executeQuery();
                  if(rs.next()){
                      plan.setZona            ( rs.getString("zona"));   
                      plan.setNombreZona      ( rs.getString("nomzona"));   
                      plan.setPCTrafico       ( rs.getString("pto_control_proxreporte")); 
                      plan.setNombrePCTrafico ( rs.getString("nompto_control_proxreporte"));  
                      plan.setFechaPlanViaje  ( rs.getString("fecha_salida")); 
                  }
           
              
          }
          catch(SQLException e){
              throw new SQLException(" DAO: getDatosTrafico -->"+ e.getMessage());
          }  
          finally{
             if(st!=null)  st.close();
             if(rs!=null)  rs.close();
             poolManager.freeConnection("fintra",con);
          } 
          return plan;
    }
    
    
    
    
    
     /**
     * Guardar el Plan de Viaje
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/ 
     public void save(PlanDeViaje plan)throws Exception{
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("fintra");
          if (con == null)
                throw new SQLException("Sin conexion");
          PreparedStatement st    = null;
          try {
              st= con.prepareStatement(this.SAVE_PLV);
                st.setString(1,   plan.getDistrito()         );
                st.setString(2,   plan.getPlanilla()         );
                st.setString(3,   plan.getFechaPlanViaje()   );
                st.setString(4,   plan.getCedulaConductor()  );                
                st.setString(5,   plan.getRadio()            );
                st.setString(6,   plan.getCelular()          );
                st.setString(7,   plan.getAvantel()          );
                st.setString(8,   plan.getTelefono()         );
                st.setString(9,   plan.getCazador()          );
                st.setString(10,  plan.getMovil()            );
                st.setString(11,  plan.getOtros()            );
                st.setString(12,  plan.getNameFamiliar()     );
                st.setString(13,  plan.getTelFamiliar()      );
                st.setString(14,  plan.getCedulaPropietario());
                st.setString(15,  plan.getComentario1()      );
                st.setString(16,  plan.getRetorno()          );                
                st.setString(17,  plan.getProducto()         );                
                st.setString(18,  plan.getAgencia()          );
                st.setString(19,  plan.getUsuario()          );
                st.setString(20,  plan.getUsuario()          );
                
              st.execute();
              
              // Ubicaciones
              savePlanUbicacion(plan, plan.getListAlimentacionPlan());
              savePlanUbicacion(plan, plan.getListParqueaderoPlan() );
              savePlanUbicacion(plan, plan.getListPernotacionPlan() );
              savePlanUbicacion(plan, plan.getListTanqueoPlan()     );
              
              // Actualizacion Trafico y Planilla
              this.updateRutaPlanilla(plan.getPlanilla(), plan.getDistrito(), plan.getProximoRuta());
              this.updateTrafico( plan);
          }
          
          catch(SQLException e){
              throw new SQLException(" DAO: No se pudo grabar el Plan de Viaje.-->"+ st.toString() + e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             poolManager.freeConnection("fintra",con);
          } 
    }
     
     
     
     
      /**
     * Guardar Ubicaciones del Plan de Viaje
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/ 
     public void savePlanUbicacion(PlanDeViaje plan, List listaUbicacion)throws Exception{
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("fintra");
          if (con == null)
                throw new SQLException("Sin conexion");
          PreparedStatement st    = null;
          try {
              st= con.prepareStatement(this.SQL_SAVE_UBICACIONES);
              for(int i=0; i<listaUbicacion.size();i++ ){
                  Ubicacion ub = (Ubicacion)listaUbicacion.get(i);                  
                      st.setString(1,   plan.getDistrito()         );
                      st.setString(2,   plan.getPlanilla()         );
                      st.setString(3,   ub.getCod_ubicacion()      );
                      st.setString(4,   plan.getUsuario()          );
                  st.execute();
                  st.clearParameters();
              }              
          }
          catch(SQLException e){
              throw new SQLException(" DAO: savePlanUbicacion .-->"+ st.toString() + e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             poolManager.freeConnection("fintra",con);
          } 
    }
     
     
     
     
     
     
     
     /**
     * Modificar el Plan de Viaje
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/ 
      public void modify(PlanDeViaje plan)throws Exception{
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("fintra");
          if (con == null)
                throw new SQLException("Sin conexion");
          PreparedStatement st    = null;
          try {
              st= con.prepareStatement(this.MODIFY_PLV);
             // set
                   st.setString(1,   plan.getDistrito()         );
                    st.setString(2,   plan.getPlanilla()         );
                    st.setString(3,   plan.getFechaPlanViaje()   );
                    st.setString(4,   plan.getCedulaConductor()  );                
                    st.setString(5,   plan.getRadio()            );
                    st.setString(6,   plan.getCelular()          );
                    st.setString(7,   plan.getAvantel()          );
                    st.setString(8,   plan.getTelefono()         );
                    st.setString(9,   plan.getCazador()          );
                    st.setString(10,  plan.getMovil()            );
                    st.setString(11,  plan.getOtros()            );
                    st.setString(12,  plan.getNameFamiliar()     );
                    st.setString(13,  plan.getTelFamiliar()      );
                    st.setString(14,  plan.getCedulaPropietario());
                    st.setString(15,  plan.getComentario1()      );
                    st.setString(16,  plan.getRetorno()          );                
                    st.setString(17,  plan.getProducto()         );                
                    st.setString(18,  plan.getAgencia()          );
                    st.setString(19,  plan.getUsuario()          );
                    st.setString(20,  plan.getUsuario()          );
                //where                
                st.setString(21,  plan.getDistrito()        );
                st.setString(22,  plan.getPlanilla()        );
             
              
              st.execute();
              
           // Ubicaciones del Plan:
              
               deleteUbicacionesPlan(plan);
               savePlanUbicacion(plan, plan.getListAlimentacionPlan());
               savePlanUbicacion(plan, plan.getListParqueaderoPlan() );
               savePlanUbicacion(plan, plan.getListPernotacionPlan() );
               savePlanUbicacion(plan, plan.getListTanqueoPlan()     );
                          
           // Actualizar trafico y planilla
              this.updateRutaPlanilla(plan.getPlanilla(),  plan.getDistrito(), plan.getProximoRuta());
              this.updateTrafico( plan);
              
          }
          catch(Exception e){
              throw new SQLException(" DAO: No se pudo modificar el Plan de Viaje.-->"+ e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             poolManager.freeConnection("fintra",con);
          } 
    }
     
      
      
      
      
     /**
     * Eliminar el plan de Viaje
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/ 
     public void delete(PlanDeViaje plan)throws Exception{
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("fintra");
          if (con == null)
                throw new SQLException("Sin conexion");
          PreparedStatement st    = null;
          try {
              
                st= con.prepareStatement( this.DELETE_PLV );
                st.setString(1,  plan.getDistrito()   );
                st.setString(2,  plan.getPlanilla()   );
                st.execute();
                
                deleteUbicacionesPlan(plan);
                                
                this.updateRutaPlanilla(plan.getPlanilla(),   plan.getDistrito(), plan.getProximoRuta());
                
          }
          catch(Exception e){
              throw new SQLException(" DAO: No se pudo eliminar el Plan de Viaje.-->"+ e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             poolManager.freeConnection("fintra",con);
          } 
    }
     
     
     
     /**
     * Eliminar las Ubicaciones del plan de Viaje
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/ 
     public void deleteUbicacionesPlan(PlanDeViaje plan)throws Exception{
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("fintra");
          if (con == null)
                throw new SQLException("Sin conexion");
          PreparedStatement st    = null;
          try {
                st= con.prepareStatement( this.DELETE_UBICACIONES_PLV );
                st.setString(1,  plan.getDistrito()   );
                st.setString(2,  plan.getPlanilla()   );
                st.execute();
          }
          catch(Exception e){
              throw new SQLException(" DAO: deleteUbicacionesPlan.-->"+ e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             poolManager.freeConnection("fintra",con);
          } 
    }
     
     
    
     
     
     /**
     * Nombre del codigo de las ciudades
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/ 
      public String nombreCiudad( String codigo)throws Exception{
          
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("fintra");
          if (con == null)
                throw new SQLException("Sin conexion");
        
          PreparedStatement st        = null;
          ResultSet         rs        = null;
          String            ciudad    = codigo;
          try {
              st= con.prepareStatement(this.NOMBRE_CIUDAD);
              st.setString(1, codigo);
              rs=st.executeQuery();
              while(rs.next())
                  ciudad = rs.getString(1);
          }
          catch(SQLException e){
              throw new SQLException(" DAO: No se nombreCiudad-->"+ e.getMessage());
          }  
          finally{
             if(st!=null)  st.close();
             if(rs!=null)  rs.close();
             poolManager.freeConnection("fintra",con);
          } 
          return ciudad;
    }
     
     
     
     
     /**
     * Actualizar las tablas de Trafico
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/ 
     
     public void updateTrafico( PlanDeViaje plan )throws Exception{
         
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("fintra");
          if (con == null)
              throw new SQLException("Sin conexion");
      
          PreparedStatement st         = null;
          PreparedStatement st2        = null;
          PreparedStatement st3        = null;
          PreparedStatement st4        = null;
          try {
              
               String   celular     =  plan.getTelConductor();
               String   fechaSalida =  plan.getFechaPlanViaje();
               String   proximaVia  =  plan.getProximoRuta();
               
               String   codePC      = "";
               String   descPC      = "";
               String   codeZona    = "";
               String   descZona    = "";
               
               String   pc          =  plan.getProximoPC();
               if(!pc.equals(":")){
                  String[] vecPC =  pc.split(this.SEPERADOR );
                  codePC         =  vecPC[0];
                  descPC         =  vecPC[1];  
               }
               
               String   zona        =  plan.getProximoZona();
               if(!zona.equals(":")){
                 String[] vec  =  zona.split(this.SEPERADOR);
                 codeZona      =  vec[0];
                 descZona      =  vec[1];     
               }
              
               String   fechaProxPC = this.getDuracion(fechaSalida,  plan.getOrigenCode(), codePC );               
                              
               st= con.prepareStatement( this.UPDATE_INGRESO_TRAFICO );
                 st.setString(1, fechaSalida  );
                 st.setString(2, fechaProxPC  );
                 st.setString(3, celular      );
                 st.setString(4, fechaSalida  );
                 st.setString(5, plan.getPlanilla()  );
               st.execute();               
               
               st2= con.prepareStatement( this.UPDATE_TRAFICO  );
                 st2.setString(1, fechaSalida  );
                 st2.setString(2, fechaProxPC  );
                 st2.setString(3, celular      );
                 st2.setString(4, fechaSalida  );
                 st2.setString(5, plan.getPlanilla()  );
               st2.execute();               
               
               
        //--- Cambio de Via y/o Puesto Control
               if( !proximaVia.equals( plan.getRuta() ) ||  !codePC.equals( plan.getPCTrafico() )  ){
                   
                  //-- Si es Origen
                       if( codePC.equals(plan.getOrigenCode()) ){
                            fechaProxPC  = fechaSalida;
                            codeZona     = plan.getZona();
                            descZona     = plan.getNombreZona();                       
                       }

                       st3= con.prepareStatement( this.UPDATE_INGRESO_TRAFICO_CAMBIO_VIA  );                   
                         st3.setString(1, codePC     );
                         st3.setString(2, descPC     );
                         st3.setString(3, codeZona   );
                         st3.setString(4, descZona   );
                         st3.setString(5, proximaVia );
                         st3.setString(6, plan.getPlanilla()  );
                       st3.execute();  

                       st4= con.prepareStatement( this.UPDATE_TRAFICO_CAMBIO_VIA   );                   
                         st4.setString(1, codeZona   );
                         st4.setString(2, codePC     );
                         st4.setString(3, plan.getPlanilla()   );
                       st4.execute();  
              }
              
          }
          catch(Exception e){
              throw new SQLException(" DAO: No se updateTrafico-->"+ e.getMessage());
          }  
          finally{
             if(st!=null)  st.close();
             if(st2!=null) st2.close();
             if(st3!=null) st3.close();
             if(st4!=null) st4.close();
             poolManager.freeConnection("fintra",con);     
          } 
    }
     
     
     
     
     
     
     
   /**
     * Duracion de Un Tramo
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/      
 public static String getDuracion( String fecha, String Origen, String Destino) throws SQLException{
         PoolManager poolManager = PoolManager.getInstance();
         Connection con          = poolManager.getConnection("fintra");
         if (con == null)
              throw new SQLException("Sin conexion");
       
        PreparedStatement st          =  null;
        ResultSet         rs          =  null;
        String            proxima     =  fecha;
        try {
            st = con.prepareStatement(SQL_DURACION_TRAMO);
            st.setString(1,  fecha);
            st.setString(2,  Origen);
            st.setString(3,  Destino);
            rs = st.executeQuery();
            while (rs.next()){
                proxima = rs.getString(1);
                break;
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina getDuracion [xxxxxDAO]... "+e.getMessage());
        }
        finally {
            if (rs!=null) rs.close();
            if (st!=null) st.close();
            poolManager.freeConnection("fintra",con);
        }
        return proxima;
    }





     
     /**
     * Listado de Vias para esa ruta
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/  
     public List searchVias( String ruta, Connection con)throws Exception{
         
          /*PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("fintra");
          if (con == null)
                throw new SQLException("Sin conexion");*/
        
          PreparedStatement st        = null;
          ResultSet         rs        = null;
          List              lista     = null;
          try {

              st= con.prepareStatement(this.SEARCH_RUTA);
              st.setString(1, ruta);
              rs=st.executeQuery();
              while(rs.next()){
                 if( lista==null) lista = new LinkedList();
                 Via  via = new Via();
                   via.setCodigo      ( rs.getString("via"));
                   via.setDescripcion ( rs.getString("descripcion"));
                   via.setVia         ( rs.getString("ruta"));
                   
                   String cadena = via.getVia();
                   List listaPC = new LinkedList();
                   for (int i=0; i <cadena.length(); i += 2 ){
                       String puesto  = cadena.substring( i  , i+2 );
                       String nombre  = this.nombreCiudad(puesto);
                       String zona    = this.getZona(puesto);                       
                       
                       listaPC.add( puesto + SEPERADOR + nombre + this.SEPERADOR + zona);  
                   }                   
                   via.setPuestos(listaPC);
                   
                 lista.add(via);
              }
          }
          catch(SQLException e){
              throw new SQLException(" DAO: No se pudo buscar las vias para la ruta.-->"+ e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             if(rs!=null) rs.close();
           //  poolManager.freeConnection("fintra",con);
          } 
          return lista;
    }
     
     
     
     
     
     
     
    /**
     * Actualizamos el campo ruta de la planilla
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/      
     
    public void updateRutaPlanilla( String oc, String distrito, String ruta )throws Exception{
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("fintra");
          if (con == null)
                throw new SQLException("Sin conexion");
          PreparedStatement st        = null;
          try {

              st= con.prepareStatement(this.UPDATE_RUTA_PLANILLA);
               st.setString(1, ruta);
               st.setString(2, oc);
               st.setString(3, distrito);
              st.execute();
              
          }
          catch(SQLException e){
              throw new SQLException(" DAO: No se actualizar la ruta de planilla.-->"+ e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             poolManager.freeConnection("fintra",con);
          } 
    }
    /**
     * Plan de Viajes NO CREADOS, se forman a partir de la OC 
     * @autor.......fvillacob 
     * @parameter... String oc, String distrito        
     * @throws......Exception
     * @version.....1.0.     
     **/ 

    
    public PlanDeViaje formarPlanViaje(String oc, String distrito)throws Exception{
        
        //org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
        
      PoolManager poolManager = PoolManager.getInstance();
      Connection con          = poolManager.getConnection("fintra");
      if (con == null)
            throw new SQLException("Sin conexion");
      PreparedStatement st        = null;
      ResultSet         rs        = null;
      PlanDeViaje       planViaje = null;  
      try {            
          st= con.prepareStatement(this.FORMAT_PLV);
          st.setString(1, oc);
          st.setString(2, distrito);
          rs=st.executeQuery();
          while(rs.next()){
            planViaje = new   PlanDeViaje();
                planViaje.loadFormat(rs); 
                                
                planViaje.setVias              ( this.searchVias( planViaje.getOrigen() + planViaje.getDestino(), con) );  
                planViaje.setEscolta           ( this.placasCaravanas(planViaje ));                
                planViaje.setOrigen            ( this.nombreCiudad   ( planViaje.getOrigen()   ));
                planViaje.setDestino           ( this.nombreCiudad   ( planViaje.getDestino()  ));
                planViaje.setCiudadConductor   ( this.nombreCiudad   ( planViaje.getCiudadConductor()   ));
                planViaje.setCiudadPropietario ( this.nombreCiudad   ( planViaje.getCiudadPropietario() ));                
           
                planViaje = this.getDatosTrafico(planViaje);                
                //logger.info("? ruta plan de viaje: " + planViaje.getRuta());
                List listAlimentacion = new LinkedList();
                List listPernotacion  = new LinkedList();
                List listParqueadero  = new LinkedList();
                List listTanqueo      = new LinkedList();                
                List listaVia = planViaje.getVias();
                if(listaVia!=null){
                    Iterator it = listaVia.iterator();
                    String ruta = "";
                    while( it.hasNext() ){//AMATURANA 29.03.2007
                        Via  via = (Via) it.next();
                        if ( via.getCodigo().equals(planViaje.getRuta()) ){
                            ruta = via.getVia(); 
                        }
                    }
                    //Via    via    = (Via)listaVia.get(0);
                    //String ruta   = via.getVia(); 
                    //logger.info("? ruta formar plan viaje: " + ruta);
                    listAlimentacion = this.searchUbicacion( ruta, this.TIPO_RESTAURANTE );
                    listPernotacion  = this.searchUbicacion( ruta, this.TIPO_PERNOTADERO );
                    listParqueadero  = this.searchUbicacion( ruta, this.TIPO_PARQUEADERO );
                    listTanqueo      = this.searchUbicacion( ruta, this.TIPO_TANQUEO     );
                }                
                planViaje.setListAlimentacion( listAlimentacion );
                planViaje.setListParqueadero ( listParqueadero  );
                planViaje.setListPernotacion ( listPernotacion  );
                planViaje.setListTanqueo     ( listTanqueo      );
                
            break;   
          }
      }
      catch(SQLException e){
          throw new SQLException(" DAO: No se pudo formar el Plan de Viaje.-->"+ e.getMessage());
      }  
      finally{
         if(st!=null) st.close();
         if(rs!=null) rs.close();
         poolManager.freeConnection("fintra",con);
      } 
      return planViaje;
    }
    
    /**
     * Listado de Vias para esa ruta
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/  
     public List searchUbicacion( String via, String tipo)throws Exception{
         
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("fintra");
          if (con == null)
                throw new SQLException("Sin conexion");        
          PreparedStatement st        = null;
          ResultSet         rs        = null;
          PreparedStatement st2       = null;
          ResultSet         rs2       = null;
          List              lista     = new LinkedList();
          
          try {

              String[] tramos  = new String[via.length()];
              String[] puestos = new String[via.length()];               
          // Puestos de la Via
               int contPuesto = 0;
               for (int i=0; i <via.length(); i += 2 ){
                   puestos[contPuesto] = via.substring( i  , i+2 );
                   contPuesto++;
               }              
          // Tramos de la Via
               int contTramo = 0;
               for (int i=0; i <contPuesto-1; i++ ){
                   String pc1 = puestos[i];
                   String pc2 = puestos[i+1];
                   tramos[contTramo] = pc1 + pc2;
                   contTramo++;
               }
               
               //logger.info("? Ruta Via: " + via + " - Tipo Ubicacion: " + tipo);
               // Busqueda ubicacion entre Tramos:
               st= con.prepareStatement(this.SQL_UBICACION + this.SQL_UBICACION_TRAMOS);
               for (int i=0; i <contTramo; i++){
                   String origen  =    tramos[i].substring(0,2);
                   String destino =    tramos[i].substring(2,4);
                   st.setString(1, tipo);
                   st.setString(2, origen);
                   st.setString(3, destino);
                   //logger.info("?ubicacion entre tramos : " + st);
                   rs = st.executeQuery();
                   while(rs.next()){
                       Ubicacion ubicacion = this.loadUbicacion(rs);
                       lista.add(ubicacion);
                   }
                   st.clearParameters();
               }
               
               
               // Busqueda ubicacion entre Tramos:
               st2= con.prepareStatement(this.SQL_UBICACION  + this.SQL_UBICACION_CIUDAD);
               for (int i=0; i <contPuesto; i++){
                   String  pc     =   puestos[i];
                   st2.setString(1, tipo);
                   st2.setString(2, pc);
                   //logger.info("?ubicacion en ciudad : " + st2);
                   rs2 = st2.executeQuery();
                   while(rs2.next()){
                       Ubicacion ubicacion = this.loadUbicacion(rs2);
                       lista.add(ubicacion);
                   }
                   st2.clearParameters();
               }
               //logger.info("? ubicaciones: " + lista.size());
               //logger.info("? ubicaciones: " + lista);
              
          }
          catch(Exception e){
              throw new SQLException(" DAO: searchUbicacion -->"+ e.getMessage());
          }  
          finally{
             if(st!=null)  st.close();
             if(rs!=null)  rs.close();
             if(st2!=null) st.close();
             if(rs2!=null) rs.close();
             poolManager.freeConnection("fintra",con);
          } 
          return lista;
    }
     
     
     
    
}

