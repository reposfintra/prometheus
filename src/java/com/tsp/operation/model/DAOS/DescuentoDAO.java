/************************************************************************************
 * Nombre clase: Descuento.java                                                     *
 * Descripci�n: Clase que maneja las consultas de los descuentos.                   *
 * Autor: Ing. Jose de la rosa                                                      *
 * Fecha: 12 de diciembre de 2005, 08:52 AM                                         *
 * Versi�n: Java 1.0                                                                *
 * Copyright: Fintravalores S.A. S.A.                                          *
 ***********************************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

public class DescuentoDAO extends MainDAO{
    private Descuento descuento;
    private Vector vecdescuento;
    /** Creates a new instance of Descuento */
    public DescuentoDAO () {
        super ( "DescuentoMantenimientoDAO.xml" );
    }
    /**
     * Metodo: getDescuento, permite retornar un objeto de registros de descuento.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Descuento getDescuento () {
        return descuento;
    }
    
    /**
     * Metodo: setDescuento, permite obtener un objeto de registros de descuento.
     * @autor : Ing. Jose de la rosa
     * @param : objeto
     * @version : 1.0
     */
    public void setDescuento (Descuento descuento) {
        this.descuento = descuento;
    }
    
    /**
     * Metodo: getDescuentos, permite retornar un vector de registros de descuento.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Vector getDescuentos () {
        return vecdescuento;
    }
    
    /**
     * Metodo: setDescuentos, permite obtener un vector de registros de descuento.
     * @autor : Ing. Jose de la rosa
     * @param : vector
     * @version : 1.0
     */ 
    public void setDescuentos (Vector vecdescuento) {
        this.vecdescuento = vecdescuento;
    }
    
    /**
     * Metodo: insertDescuento, permite ingresar un registro a la tabla Descuento.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String insertDescuento () throws SQLException{
        PreparedStatement st = null;
        String sql="";
        try{
            st = crearPreparedStatement ("SQL_INSERT");
            st.setString (1,descuento.getPlaca_trailer ());
            st.setString (2,descuento.getPlaca_cabezote ());
            st.setString (3,descuento.getNum_planilla ());
            st.setString (4,descuento.getStd_job_no ());
            st.setDouble (5,descuento.getValor ());
            st.setString (6,descuento.getClase_equipo ());
            st.setString (7,descuento.getTipo_acuerdo ());
            st.setString (8,descuento.getCodigo_concepto ());
            st.setString (9, descuento.getProveedor ());
            st.setString (10, descuento.getFecha_proceso ());
            st.setString (11,descuento.getUsuario_creacion ());
            st.setString (12,descuento.getUsuario_modificacion ());
            st.setString (13,descuento.getBase ());
            st.setString (14,descuento.getDistrito ());
            st.setString (15,descuento.getNum_equipo ());
            sql = st.toString ()+";";                        
        }catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException ("ERROR AL INGRESAR UN DESCUENTO A UN EQUIPO, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_INSERT");
        }
        return sql;
    }
    
    /**
     * Metodo: existDescuento, permite buscar un descuento clase dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : numero de planilla.
     * @version : 1.0
     */
    public boolean existDescuentoPlanillaFecha (String distrito, String numpla, String concepto, String fecha) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        try {
            st = crearPreparedStatement ("SQL_SERCH");
            st.setString (1, distrito);
            st.setString (2, numpla);
            st.setString (3, concepto);
            st.setString (4, fecha);
            rs = st.executeQuery ();
            sw = rs.next ();
        }
        catch(SQLException ex){
            throw new SQLException ("ERROR DURANTE LA BUSQUEDA DEL DESCUNETO DE LA PLANILLA, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_SERCH");
        }
        return sw;
    }
    
    public String obtenerCodigoCuenta (String distrito, String proveedor, String concepto, String numpla) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        String cod = "";
        try {
            st = crearPreparedStatement ("SQL_CODIGO_CUENTA");
            st.setString (1, distrito);
            st.setString (2, proveedor);
            st.setString (3, concepto);
            st.setString (4, numpla);
            rs = st.executeQuery ();
            if( rs.next () )
                cod = rs.getString ("codigo")!=null?rs.getString ("codigo"):"";
        }
        catch(SQLException ex){
            throw new SQLException ("ERROR DURANTE LA BUSQUEDA DEL CODIGO DE CUENTA, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_CODIGO_CUENTA");
        }
        return cod;
    }    
    
    /**
     * Metodo: existDescuentoMovPlanilla, permite verificar si existe el registro en MOVPLA
     * @autor : Ing. Jose de la rosa
     * @param : numero de planilla.
     * @version : 1.0
     */
    public boolean existDescuentoMovPlanilla ( String numpla, String concepto, String proveedor ) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        try {
            st = crearPreparedStatement ("SQL_SERCH_MOVPLA");
            st.setString (1, concepto);
            st.setString (2, numpla);
            st.setString (3, proveedor);
            rs = st.executeQuery ();
            sw = rs.next ();
        }
        catch(SQLException ex){
            throw new SQLException ("ERROR existDescuentoMovPlanilla ( String numpla, String concepto, String proveedor ), "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_SERCH_MOVPLA");
        }
        return sw;
    }    
    
    /**
     * Metodo: existDescuentoMantenimento,  permite verificar si existe el registro en DESCUENTO_MANTENIMIENTO
     * @autor : Ing. Jose de la rosa
     * @param : numero de planilla.
     * @version : 1.0
     */
    public boolean existDescuentoMantenimento ( String distrito, String numpla, String concepto ) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        try {
            st = crearPreparedStatement ("SQL_SERCH_MANTENIMIENTO");
            st.setString (1, distrito);
            st.setString (2, numpla);
            st.setString (3, concepto);
            rs = st.executeQuery ();
            sw = rs.next ();
        }
        catch(SQLException ex){
            throw new SQLException ("ERROR existDescuentoMantenimento ( String numpla, String concepto, String proveedor ), "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_SERCH_MANTENIMIENTO");
        }
        return sw;
    }  
    
}
