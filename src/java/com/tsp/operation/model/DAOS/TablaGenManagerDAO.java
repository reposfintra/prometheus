/*
* Nombre        TablaGenManagerDAO.java
* Descripci�n   Clase DAO para tabla general
* Autor         Alejandro Payares
* Fecha         14 de diciembre de 2005, 05:25 PM
* Versi�n       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/
package com.tsp.operation.model.DAOS;

import java.sql.*;
import java.util.LinkedList;
import org.apache.log4j.Logger;
import com.tsp.operation.model.beans.TablaGen;
import java.util.*;
import com.tsp.operation.model.beans.*;
/**
 * Objeto para el acceso a los datos de la tabla tablagen.
 *
 * @author  Alejandro Payares
 */
public class TablaGenManagerDAO extends MainDAO {

    /**
     * Lista donde se cargan las tablas registradas en tablagen.
     */
    private TreeMap hcs;
    private TreeMap fpago;
    private LinkedList tablas;
    private Vector infoTabla = null;
    private TreeMap info = null;
    private TablaGen tblgen;
    private LinkedList general;
    //ffernandez 18/04/2007
    private TreeMap Thcs;

    /**
     * Objeto para registrar entradas en el log del sistema.
     */
    private Logger logger = Logger.getLogger(TablaGenManagerDAO.class);

    /** Creates a new instance of TablaGenManagerDAO */
    public TablaGenManagerDAO() {
        super("TablaGenManagerDAO.xml");
    }

    /** Creates a new instance of TablaGenManagerDAO */
    public TablaGenManagerDAO(String dataBaseName) {
        super("TablaGenManagerDAO.xml", dataBaseName);
    }


    /**
     * Busca las tablas registradas en tabla gen y las carga en la lista de tablas
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void buscarTablas() throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_TABLAS";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = ps.executeQuery();
            tablas = new LinkedList();
            while(rs.next()){
                TablaGen t = new TablaGen();
                t.setTable_type(rs.getString("table_code"));
                t.setDescripcion(rs.getString("descripcion"));
                tablas.add(t);
                t = null;//Liberar Espacio JJCastro
            }
        }}
        catch( Exception ex ){
            logger.error("ERROR OBTENIENDO TABLAS: "+this.getStackTrace(ex));
            throw new SQLException("ERROR OBTENIENDO TABLAS: "+ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    /**
     * Busca las tablas registradas en tabla gen para un perfil determinado y las carga en la lista de tablas
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void buscarTablasPerfiles(String perfil) throws SQLException{

        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;

        try {
            con = this.conectarJNDI("SQL_OBTENER_TABLAS_PERFILES");//JJCastro fase2
            st = con.prepareStatement(this.obtenerSQL("SQL_OBTENER_TABLAS_PERFILES"));
            st.setString(1, perfil);
            rs = st.executeQuery();
            tablas = new LinkedList();
            if(rs.next()){
                String tablasp = "";
                tablasp = rs.getString("dato");
                String vtablas[] = tablasp.split("#");
                for(int i = 0 ; i < vtablas.length ; i++){
                    st.clearParameters();
                    st = con.prepareStatement(this.obtenerSQL("SQL_BUSCAR_TABLA"));
                    st.setString(1, vtablas[i]);
                    rs = st.executeQuery();
                    if(rs.next()){
                        TablaGen t = new TablaGen();
                        t.setTable_type(vtablas[i]);
                        t.setDescripcion(rs.getString("descripcion"));
                        tablas.add(t);
                         t = null;//Liberar Espacio JJCastro
                    }
                }
            }
        }
        catch( Exception ex ){
            logger.error("ERROR OBTENIENDO TABLAS: "+this.getStackTrace(ex));
            throw new SQLException("ERROR OBTENIENDO TABLAS: "+ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    /**
     * Devuelve la lista de las tablas cargadas por el m�todo obtenerTablas()
     * @return La lista de tablas
     */
    public LinkedList obtenerTablas(){
        return tablas;
    }

    /**
     * Devuelve la lista de las tablas cargadas por el m�todo obtenerTablasPerfiles()
     * @return La lista de tablas
     */
    public LinkedList obtenerTablasPerfiles(){
        return tablas;
    }

    /**
     * Devuelve una lista con los registros cargados por el m�todo buscarRegistros
     * @return La lista de registros.
     */
    public LinkedList obtenerRegistrosDeTabla(){
        return tablas;
    }

    /**
     * busca los registros de tablagen cuyo table_type sea igual al dado
     * @param table_type el tipo de la tabla o el nombre de la sub-tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void buscarRegistros(String table_type)throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_REGISTROS";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, table_type);
            rs = ps.executeQuery();
            tablas = new LinkedList();
            while(rs.next()){
                tablas.add(TablaGen.load(rs));
            }
        }
        }catch( Exception ex ){
            logger.error("ERROR OBTENIENDO REGISTROS DE TABLAGEN: "+this.getStackTrace(ex));
            throw new SQLException("ERROR OBTENIENDO REGISTROS DE TABLAGEN: "+ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    /**
     * Agrega una tabla nueva en tablagen
     * @param tipo el tipo o nombre de la tabla
     * @param descripcion una corta descripci�n del uso de la tabla
     * @param user el usuario que crea la tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @throws InformationException si la tabla a agregar ya existe
     */
    public void agregarTabla(String tipo, String descripcion, String user, String obs)throws SQLException, com.tsp.exceptions.InformationException {
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        String query = "SQL_AGREGAR_TABLA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, tipo);
            ps.setString(2, descripcion);
            ps.setString(3, user);
            ps.setString(4, obs);
            ps.executeUpdate();
        }
        }catch( SQLException ex ){
            if ( ex.getSQLState().equals("23505") ) {
                throw new com.tsp.exceptions.InformationException("La tabla "+tipo+" ya existe!");
            }
            logger.error("ERROR AGREGANDO TABLA EN TABLAGEN: "+this.getStackTrace(ex));
            throw new SQLException("ERROR AGREGANDO TABLA EN TABLAGEN: "+ex.getMessage());
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    /**
     * Agregae un nuevo registro a una subtabla de tablagen
     * @param tipo el tipo o nombre de la tabla
     * @param codigo el codigo del registro
     * @param referencia la referencia del registro
     * @param descripcion la descripci�n del registro
     * @param user el usuario que crea la tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @throws InformationException si el registro a agregar corresponde a uno ya existente
     */
public void agregarRegistro(String tipo, String codigo, String referencia, String descripcion, String user, String dato)throws SQLException, com.tsp.exceptions.InformationException {
    Connection con = null;//JJCastro fase2
    PreparedStatement ps = null;
    String query = "SQL_AGREGAR_REGISTRO";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, tipo);
            ps.setString(2, codigo);
            ps.setString(3, referencia);
            ps.setString(4, descripcion);
            ps.setString(5, user);
            ps.setString(6, dato);
            ps.executeUpdate();
        }
        }catch( SQLException ex ){
            if ( ex.getSQLState().equals("23505") ) {
                throw new com.tsp.exceptions.InformationException("El registro ingresado para la tabla "+tipo+" ya existe!");
            }
            logger.error("ERROR AGREGANDO REGISTRO EN TABLAGEN: "+this.getStackTrace(ex));
            throw new SQLException("ERROR AGREGANDO REGISTRO EN TABLAGEN: "+ex.getMessage());
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    /**
     * perimte obtener un registro completo que coincida con el oid dado
     * @param oid el identificador �nico del registro
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @return un bean tipo <CODE>TablaGen</CODE> correspondiente al registro
     */
    public TablaGen obtenerRegistro(String oid)throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_REGISTRO";//JJCastro fase2
        TablaGen  tblgen = null;//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, oid);
            rs = ps.executeQuery();
            tablas = new LinkedList();
            if (rs.next()){
             tblgen = TablaGen.load(rs);
            }

        }
        }catch( Exception ex ){
            logger.error("ERROR BUSCANDO REGISTRO DE TABLAGEN: "+this.getStackTrace(ex));
            throw new SQLException("ERROR BUSCANDO REGISTRO DE TABLAGEN: "+ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return tblgen;
    }

    /**
     * Obtiene el registro correspondiente a una tabla sin sus registros, es decir
     * el nombre de la tabla y su descripci�n
     * @param type el nombre de la tabla, o el tipo
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @return Un bean <CODE>TablaGen</CODE> con los datos de la tabla
     */
    public TablaGen obtenerTabla(String type)throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_TABLA";//JJCastro fase2
        TablaGen t =null;//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, type);
            rs = ps.executeQuery();
            tablas = new LinkedList();
            if (rs.next()){
                t = new TablaGen();
                t.setTable_type(rs.getString("table_code"));
                t.setDescripcion(rs.getString("descripcion"));
                t.setDato(rs.getString("dato"));

            }
        }
        }catch( Exception ex ){
            logger.error("ERROR BUSCANDO TABLA DE TABLAGEN: "+this.getStackTrace(ex));
            throw new SQLException("ERROR BUSCANDO TABLA DE TABLAGEN: "+ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return t;
    }

    /**
     * Permite modificar una tabla dentro de tablgen, es decir su nombre y su descripci�n.
     * Cuando se modifica el nombre, tambi�n son modificados los registros que pertencen a
     * esa tabla.
     * @param tipo el tipo o nombre de la tabla
     * @param descripcion la descripci�n de la tabla
     * @param tipoViejo el nombre antiguo de la tabla
     * @param user el usuario que modifica la tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @throws InformationException Si el nuevo nombre de la tabla corresponde a una ya existente.
     */
    public void editarTabla(String tipo, String descripcion, String tipoViejo, String user, String obs)throws SQLException, com.tsp.exceptions.InformationException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        String query = "SQL_EDITAR_TABLA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, tipo);
            ps.setString(2, descripcion);
            ps.setString(3, user);
            ps.setString(4, obs);
            ps.setString(5, tipoViejo);
            ps.setString(6, tipo);
            ps.setString(7, user);
            ps.setString(8, tipoViejo);
            ps.executeUpdate();
        }
        }catch( SQLException ex ){
            if ( ex.getSQLState().equals("23505") ) {
                throw new com.tsp.exceptions.InformationException("La tabla "+tipo+" ya existe!");
            }
            logger.error("ERROR EDITANDO TABLA EN TABLAGEN: "+this.getStackTrace(ex));
            throw new SQLException("ERROR EDITANDO TABLA EN TABLAGEN: "+ex.getMessage());
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    /**
     * Permite modificar un registro de una subtabla de tablagen.
     * @param oid el identificador �nico del registro
     * @param tipo el tipo o nombre de la tabla
     * @param codigo el codigo del registro
     * @param referencia la referencia del registro
     * @param descripcion la descripci�n del registro
     * @param user el usuario que edita el registro
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @throws InformationException Si el nuevo nombre del registro, la referencia y la descripci�n
     * corresponden a un registro ya existente.
     */
    public void editarRegistro (String oid,String tipo, String codigo, String referencia, String descripcion, String user, String dato)throws SQLException, com.tsp.exceptions.InformationException {
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        String query = "SQL_EDITAR_REGISTRO";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString (1, tipo);
            ps.setString (2, codigo);
            ps.setString (3, referencia);
            ps.setString (4, descripcion);
            ps.setString (5, user);
            ps.setString (6, dato);
            ps.setString (7, oid);
            ps.executeUpdate ();
        }
        }catch( SQLException ex ){
            if ( ex.getSQLState ().equals ("23505") ) {
                throw new com.tsp.exceptions.InformationException ("El registro editado para la tabla "+tipo+" ya existe!");
            }
            logger.error ("ERROR EDITANDO REGISTRO EN TABLAGEN: "+this.getStackTrace (ex));
            throw new SQLException ("ERROR EDITANDO REGISTRO EN TABLAGEN: "+ex.getMessage ());
        }finally{//JJCastro fase2
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    /**
     * Permite eliminar uno o m�s registros
     * @param oids Los identificadores �nicos de los registros a eliminar.
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void eliminarRegistros(String [] oids)throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        try {
            StringBuffer sb = new StringBuffer();
            if ( oids.length > 1 ) {
                sb.append(oids[0]+"',");
                for( int i=1; i< oids.length-1; i++ ){
                    sb.append("'"+oids[i]+"',");
                }
                sb.append("'"+oids[oids.length-1]);
            }
            else {
                sb.append(oids[0]);
            }
            con = this.conectarJNDI("SQL_ELIMINAR_REGISTRO");//JJCastro fase2
            String sql = this.obtenerSQL("SQL_ELIMINAR_REGISTRO");
            sql = sql.replaceAll("'oids'","'"+sb.toString()+"'");
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        }
        catch( SQLException ex ){
            logger.error("ERROR ELIMINANDO REGISTRO EN TABLAGEN: "+this.getStackTrace(ex));
            throw new SQLException("ERROR ELIMINANDO REGISTRO EN TABLAGEN: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }

    /**
     * Permite eliminar una o m�s tablas
     * @param tablas los nombre o tipos de las tablas
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void eliminarTablas(String [] tablas)throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        try {
            StringBuffer sb = new StringBuffer();
            if ( tablas.length > 1 ) {
                sb.append(tablas[0]+"',");
                for( int i=1; i< tablas.length-1; i++ ){
                    sb.append("'"+tablas[i]+"',");
                }
                sb.append("'"+tablas[tablas.length-1]);
            }
            else {
                sb.append(tablas[0]);
            }
            con = this.conectarJNDI("SQL_ELIMINAR_TABLAS");//JJCastro fase2
            String sql = this.obtenerSQL("SQL_ELIMINAR_TABLAS");
            sql = sql.replaceAll("'tablas'","'"+sb.toString()+"'");
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        }
        catch( SQLException ex ){
            logger.error("ERROR ELIMINANDO TABLAS EN TABLAGEN: "+this.getStackTrace(ex));
            throw new SQLException("ERROR ELIMINANDO TABLAS EN TABLAGEN: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }

     /**
     * M�todo que busca en la tabla general dato y busca la informacion relacionada con un
     * codigo de tabla
     * @param.......String codigo de una tabla
     * @autor.......Henry A. Osorio Gonzalez
     * @version.....1.0.
     **/
    public Vector buscarInfoTablaGeneralDato(String codTabla)throws SQLException{
        Connection con = null;//JJCastro fase2
        Vector datos = new Vector();
        TblGeneralDato tblDato;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_TBGEN_DATO";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1,codTabla);
            rs = ps.executeQuery();
            while(rs.next()){
                tblDato = new TblGeneralDato();
                tblDato.setCodTabla(rs.getString("codtabla"));
                tblDato.setSecuencia(rs.getString("secuencia"));
                tblDato.setTipo(rs.getString("tipo"));
                tblDato.setLeyenda(rs.getString("leyenda"));
                tblDato.setLongitud(rs.getString("longitud"));
                datos.addElement(tblDato);
                tblDato = null;//Liberar Espacio JJCastro
            }

        }
        }catch( SQLException ex ){
            logger.error("ERROR BUSCANDO DATOS EN TABLAGEN: "+this.getStackTrace(ex));
            throw new SQLException("ERROR ELIMINANDO TABLAS EN TABLAGEN: "+ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return datos;
    }
    /**
     * M�todo que busca en la tabla general dato y busca la informacion relacionada con un
     * codigo de tabla
     * @param.......String codigo de una tabla
     * @autor.......Henry A. Osorio Gonzalez
     * @version.....1.0.
     **/
    public void buscarDatosTablasGenerales(String codTabla,String codigo, String programa)throws SQLException{
        Connection con = null;//JJCastro fase2
        infoTabla = new Vector();
        String dato = "", codtabla="";
        TblGeneralDato tblDato;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_DATOS_TBLS_GENERALES";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1,codTabla);
            ps.setString(2,codigo+"%");
            ps.setString(3,programa+"%");
            rs = ps.executeQuery();
            while(rs.next()){
                dato = rs.getString("dato");
                codtabla = rs.getString("codtabla");
                info = new TreeMap();
                info.put("codtabla", codtabla);
                info.put("codigo", rs.getString("codigo"));
                info.put("descripcion", rs.getString("descripcion"));
                this.agregarDatoXSecuencia(dato, this.buscarInfoTablaGeneralDato(codTabla));
                infoTabla.addElement(info);
            }
        }
        }catch( SQLException ex ){
            logger.error("ERROR BUSCANDO DATOS EN TABLAGEN: "+this.getStackTrace(ex));
            throw new SQLException("ERROR ELIMINANDO TABLAS EN TABLAGEN: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }
    /**
     * M�todo que busca guarda en un tremap la informacion almacenada en el campo dato.
     * la informacion de este campo es obtenida de acuerdo a los parametros guardados en
     * la tabla general dato.
     * @param.......String dato Vector secuencias
     * @autor.......Henry A. Osorio Gonzalez
     * @version.....1.0.
     **/
    public void agregarDatoXSecuencia(String dato, Vector secs) {
        String secuencia = "";
        int longitud  = 0;
        String valor     = "";
        String leyenda   = "";
        int tam = secs.size();
        int pos = 0;
        int posF = 0;
        for (int i=0; i<secs.size(); i++) {
            TblGeneralDato tblDato = (TblGeneralDato) secs.elementAt(i);
            secuencia = tblDato.getSecuencia();
            longitud  = Integer.parseInt(tblDato.getLongitud());
            leyenda   = tblDato.getLeyenda();
            posF = pos+longitud;
            info.put(leyenda,dato.substring(pos,posF));
            pos = longitud;
        }
    }

    /**
     * Getter for property infoTabla.
     * @return Value of property infoTabla.
     */
    public java.util.Vector getVectorTablasGenerales() {
        return infoTabla;
    }

    /**
     * M�todo que busca en la tabla general dato y busca la informacion relacionada con un
     * codigo de tabla
     * @param.......String codigo de una tabla
     * @autor.......Ing. Juan Manuel Escandon Perez
     * @version.....2.0.
     **/
    public void DatosTablasGenerales(String codTabla,String codigo, String programa)throws SQLException{
        Connection con = null;//JJCastro fase2
        infoTabla = new Vector();
        String dato = "", codtabla="";
        TblGeneralDato tblDato;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_DATOS_TBLS_GENERALES";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1,codTabla);
            ps.setString(2,codigo);
            ps.setString(3,programa+"");
            rs = ps.executeQuery();
            while(rs.next()){
                dato = rs.getString("dato");
                codtabla = rs.getString("codtabla");
                info = new TreeMap();
                info.put("codtabla", codtabla);
                info.put("codigo", rs.getString("codigo"));
                info.put("descripcion", rs.getString("descripcion"));
                this.agregarDatoXSecuencia(dato, this.buscarInfoTablaGeneralDato(codTabla));
                infoTabla.addElement(info);
            }
        }
        }catch( SQLException ex ){
            logger.error("ERROR BUSCANDO DATOS EN TABLAGEN: "+this.getStackTrace(ex));
            throw new SQLException("ERROR ELIMINANDO TABLAS EN TABLAGEN: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
      /**
     * Obtiene el registro correspondiente a una tabla sin sus registros, es decir
     * el nombre de la tabla y su descripci�n
     * @param type el nombre de la tabla, o el tipo
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @return Un bean <CODE>TablaGen</CODE> con los datos de la tabla
     */
    public LinkedList obtenerInfoTablaGen(String type, String programa)throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_INFO_TABLAGEN";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, type);
            ps.setString(2, programa);
            rs = ps.executeQuery();
            tablas = new LinkedList();
            while (rs.next()){
                TablaGen t = new TablaGen();
                t.setTable_code(rs.getString("table_code"));
                t.setDescripcion(rs.getString("descripcion"));
                t.setReferencia (rs.getString ("referencia"));
                tablas.add (t);
                t = null;//Liberar Espacio JJCastro
            }
        }
        }catch( Exception ex ){
            logger.error("ERROR BUSCANDO TABLA DE TABLAGEN: "+this.getStackTrace(ex));
            throw new SQLException("ERROR BUSCANDO TABLA DE TABLAGEN: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return tablas;
    }
    /**jose
     * busca los registros de tablagen cuyo table_type sea igual al dado
     * @param table_type el tipo de la tabla o el nombre de la sub-tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void buscarRegistrosConReferencia(String table_type,String programa, String referencia)throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_REGISTROS_REFERENCIA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, table_type);
            ps.setString(2, programa);
            ps.setString(3, referencia);
            rs = ps.executeQuery();
            tablas = new LinkedList();
            while(rs.next()){
                tablas.add(TablaGen.load(rs));
            }
        }
        }catch( Exception ex ){
            logger.error("ERROR OBTENIENDO REGISTROS DE TABLAGEN: "+this.getStackTrace(ex));
            throw new SQLException("ERROR OBTENIENDO REGISTROS DE TABLAGEN: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    /**
      * Verifica si una fecha esta registrada como d�a feriado.
      * @autor Ing. Tito Andr�s Maturana De La Cruz
      * @param fecha Fecha a verificar
      * @throws SQLException si algun error ocurre con el acceso a la base de datos.
      * @return <code>true</code> si es feriado, <code>false</code> si no lo es
      */
    public boolean isFeriado(String fecha)throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean isFeriado = false;
        String query = "SQL_DIA_FERIADO";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, fecha);
            rs = ps.executeQuery();

            if (rs.next()){
                isFeriado = true;
            }
        }
        }catch( Exception ex ){
            logger.error("ERROR VERIFICANDO SI EL DIA ES FERIADO: "+this.getStackTrace(ex));
            throw new SQLException("ERROR VERIFICANDO SI EL DIA ES FERIADO: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }

        return isFeriado;
    }


    /**
     * Getter for property tblgen.
     * @return Value of property tblgen.
     */
    public TablaGen getTblgen () {
        return tblgen;
    }
     /**
     * Setter for property tblgen.
     */
    public void setTblgen (TablaGen tblgen) {
        this.tblgen = tblgen;
    }
    /**
     * Getter for property general.
     * @return Value of property general.
     */
    public java.util.LinkedList getReferencias() {
        return general;
    }
/**
     * M�todo que busca en la tabla general y busca la informacion relacionada con una
     * referencia
     * @param.......String codigo de la tabla y String referencia
     * @autor.......Ing. Juan Manuel Escandon Perez
     * @version.....2.0.
     **/
    public void buscarReferencia( String table_type , String referencia)throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_REFERENCIA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, table_type);
            ps.setString(2, "%"+referencia+"%");
            rs = ps.executeQuery();
            tablas = new LinkedList();
            while(rs.next()){
                tablas.add(TablaGen.load(rs));
            }
        }
        }catch( Exception ex ){
            throw new SQLException("ERROR OBTENIENDO REGISTROS DE TABLAGEN: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }



 /**
  *
  * @param table_type
  * @param code
  * @return
  * @throws SQLException
  */
    public TablaGen obtenerInformacionDato(String table_type, String code)throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        TablaGen tblgen = null;
        String query = "SQL_OBNETER_REGISTRO_CODE";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, table_type);
            ps.setString(2, code);
            rs = ps.executeQuery();
            while(rs.next()){
                tblgen = new TablaGen();
                tblgen.setDescripcion (rs.getString ("descripcion"));
                tblgen.setReferencia (rs.getString ("referencia"));
                tblgen.setTable_code (rs.getString ("table_code"));
                tblgen.setTable_type (rs.getString ("table_type"));
                tblgen.setDato (rs.getString ("dato"));
            }
        }
        }catch( Exception ex ){
            logger.error("ERROR OBTENIENDO REGISTROS DE TABLAGEN: "+this.getStackTrace(ex));
            throw new SQLException("ERROR OBTENIENDO REGISTROS DE TABLAGEN: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
         }
        return tblgen;
    }
     /**
     * busca los registros de tablagen cuyo table_type sea igual al dado y
     * reg_status != 'A'
     *@author Osvaldo P�rez Ferrer
     * @param table_type el tipo de la tabla o el nombre de la sub-tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void buscarRegistrosNoAnulados(String table_type)throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_REGISTROS_NO_ANULADOS";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, table_type);
            rs = ps.executeQuery();
            tablas = new LinkedList();
            while(rs.next()){
                tablas.add(TablaGen.load(rs));
            }
        }
        }catch( Exception ex ){
            logger.error("ERROR en buscarRegistrosNoAnulados: "+this.getStackTrace(ex));
            throw new SQLException("ERROR en buscarRegistrosNoAnulados: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }

    /**
     * verifica si existe un registro con tabla_type = type y table_code
     * @author Osvaldo P�rez Ferrer
     * @param table_type el tipo de la tabla o el nombre de la sub-tabla
     * @param code codigo o table_code
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @return 0 si el registro no existe
     *         1 si existe
     *         -1 si existe anulado
     */
    public int existeRegistroCode(String type, String code)throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_OBNETER_REGISTRO_CODE";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, type);
            ps.setString(2, code);
            rs = ps.executeQuery();

            if(rs.next()){
                return (rs.getString("reg_status").equals("A"))? -1:1;
            }
        }
        }catch( Exception ex ){
            logger.error("ERROR en existeRegistroCode: "+this.getStackTrace(ex));
            throw new SQLException("ERROR en existeRegistroCode: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return 0;
    }

    /**
     * verifica si existe un registro con tabla_type = type y dato
     * @author Osvaldo P�rez Ferrer
     * @param table_type el tipo de la tabla o el nombre de la sub-tabla
     * @param code codigo o table_code
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @return 0 si el registro no existe
     *         1 si existe
     *         -1 si existe anulado
     */
    public int existeRegistroDato(String type, String dato)throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_OBNETER_REGISTRO_DATO";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, type);
            ps.setString(2, dato);
            rs = ps.executeQuery();

            if(rs.next()){
                return (rs.getString("reg_status").equals("A"))? -1:1;
            }
        }
        }catch( Exception ex ){
            logger.error("ERROR en existeRegistroDato: "+this.getStackTrace(ex));
            throw new SQLException("ERROR en existeRegistroDato: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return 0;
    }

    /**
     * obtiene el registro, con tabla_code dado, o con dato dado
     * @author Osvaldo P�rez Ferrer
     * @param table_type el tipo de la tabla o el nombre de la sub-tabla
     * @param code codigo o table_code
     * @param dato dato
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void obtenerRegistro(String type, String code, String dato)throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        tblgen = null;
        String query = "SQL_OBNETER_REGISTRO_CODE";//JJCastro fase2
        String query2 = "SQL_OBNETER_REGISTRO_DATO";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
            if(code.length()>0){
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, type);
                ps.setString(2, code);
            }
            if(dato.length()>0){
                ps = con.prepareStatement(this.obtenerSQL(query2));//JJCastro fase2
                ps.setString(1, type);
                ps.setString(2, dato);
            }

            rs = ps.executeQuery();

            if(rs.next()){
                tblgen = TablaGen.load(rs);
            }
        }
        }catch( Exception ex ){
            logger.error("ERROR en obtenerRegistro: "+this.getStackTrace(ex));
            throw new SQLException("ERROR en obtenerRegistro: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }

    }

    /**
     * Permite modificar un registro de una subtabla de tablagen, incluido el reg_status.
     * @param oid el identificador �nico del registro
     * @param tipo el tipo o nombre de la tabla
     * @param codigo el codigo del registro
     * @param referencia la referencia del registro
     * @param descripcion la descripci�n del registro
     * @param user el usuario que edita el registro
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @throws InformationException Si el nuevo nombre del registro, la referencia y la descripci�n
     * corresponden a un registro ya existente.
     */
    public void actualizarRegistro(String oid,String tipo, String codigo, String referencia, String descripcion, String user, String dato)throws SQLException, com.tsp.exceptions.InformationException {
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        String query = "SQL_ACTUALIZAR";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, tipo);
            ps.setString(2, codigo);
            ps.setString(3, referencia);
            ps.setString(4, descripcion);
            ps.setString(5, user);
            ps.setString(6, dato);
            ps.setString(7, oid);
            ps.executeUpdate();
        }
        }catch( SQLException ex ){
            if ( ex.getSQLState().equals("23505") ) {
                throw new com.tsp.exceptions.InformationException("El registro editado para la tabla "+tipo+" ya existe!");
            }
            logger.error("ERROR en actualizarRegistro: "+this.getStackTrace(ex));
            throw new SQLException("ERROR en actualizarRegistro: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    /**
     * busca los registros de tablagen cuyo table_type sea igual al dado
     * @param table_type el tipo de la tabla o el nombre de la sub-tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void buscarRegistrosOrdenados(String table_type)throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_REGISTROS_ORDERBY_CODE";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, table_type);
            rs = ps.executeQuery();
            tablas = new LinkedList();
            while(rs.next()){
                tablas.add(TablaGen.load(rs));
            }
        }
    }catch( Exception ex ){
            logger.error("ERROR OBTENIENDO REGISTROS DE TABLAGEN: "+this.getStackTrace(ex));
            throw new SQLException("ERROR OBTENIENDO REGISTROS DE TABLAGEN: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    /**
     * @author : Osvaldo P�rez
     * busca los registros de tablagen que sean equipos propios, con la
     * descripcion de la clase de equipo, proveniente de la tablagen CLAEQUI
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void obtenerEquipos() throws SQLException{

        this.buscarRegistrosOrdenados("EQPROPIO");

        LinkedList equipos = this.obtenerTablas();
        LinkedList equipos2 = new LinkedList();
        for(int i=0; i<equipos.size(); i++){
            TablaGen eq = new TablaGen();

            TablaGen t = (TablaGen) equipos.get(i);

            this.buscarDatos("CLAEQUI",t.getReferencia());

            eq.setTable_code( t.getTable_code());
            eq.setDescripcion( this.getTblgen().getDescripcion());
            eq.setDato( t.getDato());
            //////System.out.println("Descripcion: "+this.getTblgen().getDescripcion());
            ////System.out.println("Descripcion: "+eq.getDescripcion());
            equipos2.add(eq);

        }
        this.tablas = equipos2;
    }
    public void buscarDatos (String table_type, String table_code )throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_DATOS";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString (1, table_type);
            ps.setString (2, table_code);
            rs = ps.executeQuery();
            if(rs.next()){
                tblgen = new TablaGen ();
                tblgen.setTable_code (rs.getString ("table_code"));
                tblgen.setReferencia (rs.getString ("referencia"));
                tblgen.setDato(rs.getString ("dato"));
                tblgen.setDescripcion (rs.getString ("descripcion"));
            }
        }
        } catch( Exception ex ){
            logger.error ("ERROR OBTENIENDO REGISTROS DE TABLAGEN: "+this.getStackTrace (ex));
            throw new SQLException ("ERROR OBTENIENDO REGISTROS DE TABLAGEN: "+ex.getMessage ());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
     /**
     * @author : Osvaldo P�rez
     * busca los registros de tablagen que sean equipos propios, con la
     * descripcion de la clase de equipo, proveniente de la tablagen CLAEQUI
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void obtenerEquipos(String clase) throws SQLException{

        this.buscarRegistrosOrdenados("EQPROPIO");

        LinkedList equipos = this.obtenerTablas();
        LinkedList equipos2 = new LinkedList();
        for(int i=0; i<equipos.size(); i++){
            TablaGen eq = new TablaGen();

            TablaGen t = (TablaGen) equipos.get(i);

            this.buscarDatos("CLAEQUI",t.getReferencia());

            eq.setTable_code( t.getTable_code());
            eq.setDescripcion( this.getTblgen().getDescripcion());
            eq.setDato( t.getDato());
            if( t.getReferencia().equals(clase) ){
                equipos2.add(eq);
            }

        }
        this.tablas = equipos2;
    }
    
       /**
     * busca los registros de tablagen cuyo table_type sea igual al dado
     * @author : Osvaldo P�rez Ferrer
     * @param table_type el tipo de la tabla o el nombre de la sub-tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void buscarRegistroByDesc(String table_type, String table_code)throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_REGISTRO_BY_DESCRIPCION";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, table_type);
            ps.setString(2, table_code);
            rs = ps.executeQuery();
            tablas = new LinkedList();
            while(rs.next()){
                tablas.add(TablaGen.load(rs));
            }
        }
        }catch( Exception ex ){
            ex.printStackTrace();
            logger.error("ERROR OBTENIENDO REGISTROS DE TABLAGEN buscarRegistrosOrdenadosByDesc: "+this.getStackTrace(ex));
            throw new SQLException("ERROR OBTENIENDO REGISTROS DE TABLAGEN buscarRegistrosOrdenadosByDesc: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
       /**
     * busca los registros de tablagen cuyo table_type y table_code sean iguales a los dados
     * @author : Osvaldo P�rez Ferrer
     * @param table_type el tipo de la tabla o el nombre de la sub-tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void buscarRegistrosOrdenadosByDesc(String table_type)throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_REGISTROS_ORDERBY_DESCRIPCION";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, table_type);
            rs = ps.executeQuery();
            tablas = new LinkedList();
            while(rs.next()){
                tablas.add(TablaGen.load(rs));
            }
        }
        }catch( Exception ex ){
            ex.printStackTrace();
            logger.error("ERROR OBTENIENDO REGISTROS DE TABLAGEN buscarRegistrosOrdenadosByDesc: "+this.getStackTrace(ex));
            throw new SQLException("ERROR OBTENIENDO REGISTROS DE TABLAGEN buscarRegistrosOrdenadosByDesc: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    /**
     * Getter for property hcs.
     * @return Value of property hcs.
     */
    public java.util.TreeMap getHcs() {
        return hcs;
    }

    /**
     * Setter for property hcs.
     * @param hcs New value of property hcs.
     */
    public void setHcs(java.util.TreeMap hcs) {
        this.hcs = hcs;
    }

    /** Lista los THCODE de la tabla tablagen */
    public void listarHc() throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        hcs = new TreeMap();
        String query = "SQL_OBTENER_REGISTROS_HC";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = ps.executeQuery();
            hcs.put("", "");
            while (rs.next()) {
                hcs.put(rs.getString(1), rs.getString(2));
            }
        }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR HC" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    /**
     * Getter for property fpago.
     * @return Value of property fpago.
     */
    public java.util.TreeMap getFpago() {
        return fpago;
    }

    /**
     * Setter for property fpago.
     * @param fpago New value of property fpago.
     */
    public void setFpago(java.util.TreeMap fpago) {
        this.fpago = fpago;
    }

     public void listarFpago() throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        fpago = new TreeMap();
        String query = "SQL_OBTENER_REGISTROS_FPAGO";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = ps.executeQuery();
            fpago.put("", "");
            while (rs.next()) {
                fpago.put(rs.getString(2), rs.getString(1));
            }
        }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR FPAGO" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

      /**
     * busca los registros de tablagen cuyo table_type sea igual al dado y la secuencia del dato
     * @author : Jose de la rosa
     * @param table_type el tipo de la tabla o el nombre de la sub-tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public String buscarCampoTablagenDato( String table_type, String table_code, String secuencia )throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "";
        String query = "SQL_OBTENER_CAMPO_DATO";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, secuencia);
            ps.setString(2, table_type);
            ps.setString(3, table_code);
            ps.setString(4, secuencia);
            ps.setString(5, table_type);
            ps.setString(6, secuencia);
            rs = ps.executeQuery();
            if(rs.next()){
                sql = rs.getString ("campo");
            }
        }
        }catch( Exception ex ){
            ex.printStackTrace();
            logger.error("ERROR OBTENIENDO REGISTROS DE TABLAGEN buscarCampoTablagenDato: "+this.getStackTrace(ex));
            throw new SQLException("ERROR OBTENIENDO REGISTROS DE TABLAGEN buscarCampoTablagenDato: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sql;
    }

    /**
      * Obtiene el valor de la leyenda en el campo dato
      * @author Ing. Andr�s Maturana De La Cruz
      * @param tabla Table Type
      * @param codigo Table Code
      * @param leyenda Leyenda
      * @throws SQLException si algun error ocurre con el acceso a la base de datos.
      */
     public String obtenerLeyendaDato(String tabla, String codigo, String leyenda)throws SQLException {
         Connection con = null;//JJCastro fase2
         PreparedStatement ps = null;
         ResultSet rs = null;
         String query = "SQL_OBTENER_LEYENDA_DATO";//JJCastro fase2

         try {
             con = this.conectarJNDI(query);
             if (con != null) {
             ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
             ps.setString(1, tabla);
             ps.setString(2, tabla);
             ps.setString(3, leyenda);
             ps.setString(4, tabla);
             ps.setString(5, leyenda);
             ps.setString(6, tabla);
             ps.setString(7, codigo);

             rs = ps.executeQuery();

             if(rs.next()){
                 return rs.getString("data").trim();
             }
         }
         }catch( Exception ex ){
             ex.printStackTrace();
             throw new SQLException("ERROR EN obtenerLeyendaDato: "+ex.getMessage());
         }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

         return null;
     }
      /**
      * Obtienene al aprobador de las facturas para la facturaci�n autom�tica
      * @autor Ing. Andr�s Maturana De La Cruz
      * @param id Almacenadora
      * @throws SQLException si algun error ocurre con el acceso a la base de datos.
      * @return aprobador de la factura
      */
    public String aprobadorCXPAuto(String id)throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String aprob = null;
        String query = "SQL_APROBADOR_CXP";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, id);
            rs = ps.executeQuery();
            if (rs.next()){
                aprob = rs.getString("referencia");
            }
        }
        }catch( Exception ex ){
            logger.error("ERROR OBTENIENDO EL APROBADOR DE LAS FACTURACION AUTOMATICA: "+this.getStackTrace(ex));
            throw new SQLException("ERROR OBTENIENDO EL APROBADOR DE LAS FACTURACION AUTOMATICA: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        logger.info("?aprobador: " + aprob);
        return aprob;
    }

    /**
     * @author : Ing. Enrique De Lavalle Rizo
     * busca el aprobador para las facturas de anticipos al proveedors
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public String buscarAprobadorFactura ()throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String aprobador="";
        String query = "SQL_OBTENER_DATOS";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString (1, "AUTFACT");
            ps.setString (2, "TERCEROS");
            rs = ps.executeQuery ();
            if(rs.next ()){
                aprobador = rs.getString ("referencia");
            }
        }
        }catch( Exception ex ){
            logger.error ("ERROR OBTENIENDO REGISTROS APROBADOR DE TABLAGEN: "+this.getStackTrace (ex));
            throw new SQLException ("ERROR OBTENIENDO APROBADOR DE TABLAGEN: "+ex.getMessage ());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return aprobador;
    }

    /**
     * Getter for property tablas.
     * @return Value of property tablas.
     */
    public java.util.LinkedList getTablas() {
        return tablas;
    }

    /**
     * Setter for property tablas.
     * @param tablas New value of property tablas.
     */
    public void setTablas(java.util.LinkedList tablas) {
        this.tablas = tablas;
    }

      /**
     * Obtiene el registro correspondiente a una tabla.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param tbltype el nombre de la tabla, o el tipo
     * @param tblcode C�digo de la tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @return Un bean <CODE>TablaGen</CODE> con los datos de la tabla
     */
    public TablaGen obtenerTablaGen(String tbltype, String tblcode)throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_GET_TBLGEN";
        TablaGen t = null;
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, tbltype);
            ps.setString(2, tblcode);
            logger.info("? obtenerTablaGen: " + ps);
            rs = ps.executeQuery();
            if (rs.next()){
                t = new TablaGen();
                t.setTable_type(rs.getString("table_type"));
                t.setTable_code(rs.getString("table_code"));
                t.setDescripcion(rs.getString("descripcion"));
                t.setReferencia(rs.getString("referencia"));
                t.setDato(rs.getString("dato"));
                t.setUsuario(rs.getString("referencia"));
            }
        }
        }catch( Exception ex ){
            logger.error("ERROR obtenerTablaGen [TablaGenManagerDAO]: "+this.getStackTrace(ex));
            throw new SQLException("ERROR obtenerTablaGen [TablaGenManagerDAO]: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }

                return t;
    }

    /**
     * Obtiene el registro correspondiente a una tabla a partor de una descripci�n.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param tbltype el nombre de la tabla, o el tipo
     * @param desc Descripci�n de la tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @return Un bean <CODE>TablaGen</CODE> con los datos de la tabla
     */
    public TablaGen obtenerTablaGenByDesc(String tbltype, String desc)throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_TBLGEN_BY_DESC";
        TablaGen t = null;
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, tbltype);
            ps.setString(2, desc);
            logger.info("?obtenerTablaGenByDesc: " + ps);
            rs = ps.executeQuery();
            if (rs.next()){
                t = new TablaGen();
                t.setTable_type(rs.getString("table_type"));
                t.setTable_code(rs.getString("table_code"));
                t.setDescripcion(rs.getString("descripcion"));
                t.setReferencia(rs.getString("referencia"));
            }
        }
        }catch( Exception ex ){
            logger.error("ERROR obtenerTablaGenByDesc [TablaGenManagerDAO]: "+this.getStackTrace(ex));
            throw new SQLException("ERROR obtenerTablaGenByDesc [TablaGenManagerDAO]: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return t;
    }

      /**
     * @author : Ing. Luis Eduardo Frieri
     * busca los registros de una tabla en tablagen dado el table_type y los ubica en un treemap
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */

    public TreeMap BuscarDatosTreemap(String type) throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        info = new TreeMap();
        String query = "SQL_OBTENER_REGISTROS_ORDERBY_DESCRIPCION";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, type);
            rs = ps.executeQuery();
            info.put("", "");
            while (rs.next()) {
                info.put(rs.getString(5), rs.getString(3));
            }
        }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA DE DATOS TREEMAP EN TABLAGEN" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return info;
    }

    //ffernandez 18/04/2007
    public void listarTODOHc() throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        Thcs = new TreeMap();
        String query = "SQL_OBTENER_HC";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = ps.executeQuery();
            Thcs.put("", "");
            while (rs.next()) {
                Thcs.put(rs.getString(1), rs.getString(2));
            }
        }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR listarTODOHc" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }

    /**
     * Getter for property Thcs.
     * @return Value of property Thcs.
     */
    public java.util.TreeMap getThcs() {
        return Thcs;
    }

    /**
     * Setter for property Thcs.
     * @param Thcs New value of property Thcs.
     */
    public void setThcs(java.util.TreeMap Thcs) {
        this.Thcs = Thcs;
    }

    public ArrayList obtenerRegistroDato(String type, String dato) throws SQLException,Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_OBNETER_REGISTRO_DATO";
        ArrayList lista = new ArrayList();
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, type);
            st.setString(2, dato);
            rs = st.executeQuery();
            while (rs.next()) {
                lista.add(TablaGen.load(rs));
            }
        } catch (Exception ex) {
            throw new SQLException("ERROR en obtenerRegistro: " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }
    
     public String getDato(String table_type, String table_code) throws Exception{
        String dato = "";
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        String query = "DATO_TBG";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, table_type);
            ps.setString(2, table_code);
            rs = ps.executeQuery();
            if(rs.next()){
                dato  = rs.getString("dato");
            }
        }
        catch (Exception e) {
            throw new Exception("Error al buscar datos: "+e.toString());
        }
        finally {
            if(ps!=null){ try{ps.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return dato;
    }

}
