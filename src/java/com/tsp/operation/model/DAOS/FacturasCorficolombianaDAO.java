/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.DAOS;

import java.sql.*;
import java.util.*;

/**
 *
 * @author maltamiranda
 */
public class FacturasCorficolombianaDAO extends MainDAO{

    public FacturasCorficolombianaDAO(){
        super("FacturasCorficolombianaDAO.xml");
    }

    public ArrayList getReportes() throws Exception {
        Connection con=null;
        PreparedStatement st=null;
        String sql="";
        ResultSet rs=null;
        ArrayList arl=null;
        try{
            sql=this.obtenerSQL("SQL_GET_REPORTES");
            con=this.conectarJNDI("SQL_GET_REPORTES");
            st=con.prepareStatement(sql);
            rs=st.executeQuery();
            arl=new ArrayList();

            while(rs.next())
            {   arl.add(new String[]{rs.getString("id_reporte"),rs.getString("reporte")});

            }
        }
        catch(Exception e){
            System.out.println("error en fac corfi dao"+e.toString()+"_"+e.getMessage());
            e.printStackTrace();
            throw new Exception( "errore n getReportes: " + e.toString());
        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(st!=null){st.close();}
            if(rs!=null){rs.close();}
        }
        return arl;
    }

    /*public String rstotbl (ResultSet rs) throws Exception {
        String tabla="<table border=2>";
        while(rs.next()){
           tabla+="<tr>";
           for(int i=1;i<=rs.getMetaData().getColumnCount();i++)
           {    tabla+="<td>"+rs.getString(i)+"</td>";
           }
           tabla+="</tr>";
        }
        tabla+="</tabla>";
        return tabla;
    }*/

    public ArrayList rstotbl (ResultSet rs) throws Exception {
        ArrayList tabla=new ArrayList();
        while(rs.next()){
           ArrayList tabla2=new ArrayList();
           for(int i=1;i<=rs.getMetaData().getColumnCount();i++)
           {    tabla2.add(rs.getString(i));
           }
           tabla.add(tabla2);
        }
        return tabla;
    }

    public ArrayList getConsultaNms() throws Exception{

        Connection con=null;
        PreparedStatement st=null;
        String sql="";
        ArrayList resultado=new ArrayList();
        ResultSet rs=null;
        String sql2="";
        try{
            sql=this.obtenerSQL("SQL_GET_FACTURAS");
            con=this.conectarJNDI("SQL_GET_FACTURAS");
            st=con.prepareStatement(sql);
            //st.setString(1, id_reporte);
            rs=st.executeQuery();
            /*if(rs.next())
            {
                sql2=rs.getString(tipo);
            }*/
            //if(st!=null){st.close();}
            //if(rs!=null){rs.close();}
            //st=con.prepareStatement(sql2);
            //st.setString(1, periodo);
            //if(parametro!=null && !parametro.equals("") && !parametro.equals("GENERAR_FACTURAS")){
                //st.setString(2, parametro);
            //}
            //rs=st.executeQuery();
            resultado=this.rstotbl(rs);
        }
        catch(Exception e){
            e.printStackTrace();
            throw new Exception( "errore n getConsultaNms: " + e.toString());
        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(st!=null){st.close();}
            if(rs!=null){rs.close();}
        }
        return resultado;

    }
    public String generarFacturas(String[] facturas,String usuario) throws Exception{
        Connection con=null;
        PreparedStatement st=null;
        String sql="";
        ResultSet rs=null;
        String respuesta="nada",facturitas="' ";
        for (int i=0;i<facturas.length;i++){
            facturitas=facturitas + "''" +facturas[i] +"'',";
        }
        facturitas=facturitas.substring(0, facturitas.length()-1);
        facturitas=facturitas+"'";
        try{
            sql=this.obtenerSQL("SQL_GENERAR_PMS_IAS");
            con=this.conectarJNDI("SQL_GENERAR_PMS_IAS");
            sql=sql.replaceAll("facturitasnms", facturitas);
            sql=sql.replaceAll("usrx", "'"+usuario+"'");

            //System.out.println("sql_generarFacturas:"+sql);
            st=con.prepareStatement(sql);
            rs=st.executeQuery();
            if(rs.next())            {
                respuesta=rs.getString("generacion");
            }
        }
        catch(Exception e){
            e.printStackTrace();
            throw new Exception( "errore n generarFacturas: " + e.toString());
        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(st!=null){st.close();}
            if(rs!=null){rs.close();}
        }
        System.out.println("respuesta d generarFacturas:"+respuesta);
        return respuesta;
    }
    public String generarReporteRc(String[] facturas,String usuario,String fecha) throws Exception{
        Connection con=null;
        PreparedStatement st=null;
        String sql="";
        ResultSet rs=null;
        String respuesta="nada",facturitas="' ";
        for (int i=0;i<facturas.length;i++){
            facturitas=facturitas + "''" +facturas[i] +"'',";
        }
        facturitas=facturitas.substring(0, facturitas.length()-1);
        facturitas=facturitas+"'";
        try{
            sql=this.obtenerSQL("SQL_GENERAR_REPORTE_RES");
            con=this.conectarJNDI("SQL_GENERAR_REPORTE_RES");
            sql=sql.replaceAll("facturitasnms", facturitas);
            sql=sql.replaceAll("usrx", "'"+usuario+"'");
            sql=sql.replaceAll("fecha", "'"+fecha+"'");

            //System.out.println("sql_generarFacturas:"+sql);
            st=con.prepareStatement(sql);
            rs=st.executeQuery();
            if(rs.next())            {
                respuesta=rs.getString("generacion");
            }
        }
        catch(Exception e){
            e.printStackTrace();
            throw new Exception( "errore n generarReporte: " + e.toString());
        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(st!=null){st.close();}
            if(rs!=null){rs.close();}
        }
        return respuesta;
    }

    public String generarReporte(String tipo) throws Exception{
        Connection con=null;
        PreparedStatement st=null;
        ResultSet rs=null;
        String respuesta="";
        try{
            con=this.conectarJNDI("SQL_GENERAR_REPORTE_FID");

            //System.out.println("sql_generarFacturas:"+sql);
            st=con.prepareStatement(this.obtenerSQL("SQL_GENERAR_REPORTE_FID"));
            st.setString(1, tipo);
            rs=st.executeQuery();
            if(rs.next()){
                respuesta=rs.getString("descripcion");
            }
        }
        catch(Exception e){
            e.printStackTrace();
            throw new Exception( "errore n generarReporte: " + e.toString());
        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(st!=null){st.close();}
            if(rs!=null){rs.close();}
        }
        return respuesta;
    }

    public ArrayList getConsultaRes() throws Exception{

        Connection con=null;
        PreparedStatement st=null;
        String sql="";
        ArrayList resultado=new ArrayList();
        ResultSet rs=null;
        String sql2="";
        try{
            sql=this.obtenerSQL("SQL_GET_FACTURAS_RES");
            con=this.conectarJNDI("SQL_GET_FACTURAS_RES");
            st=con.prepareStatement(sql);
            rs=st.executeQuery();
            resultado=this.rstotbl(rs);
        }
        catch(Exception e){
            e.printStackTrace();
            throw new Exception( "errore n getConsultaNms: " + e.toString());
        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(st!=null){st.close();}
            if(rs!=null){rs.close();}
        }
        return resultado;

    }


}