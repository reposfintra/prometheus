/**************************************************************************
 * Nombre clase: UbicacionVehicularClientesDAO.java                               
 * Descripci�n:  Clase que maneja las consultas para el reporte Ubicacion
 *               Vehicular Clientes     
 * Autor:        Ing. Ivan DArio Gomez Vanegas                                    
 * Fecha:        Created on 4 de enero de 2006, 10:35                       
 * Versi�n:      Java 1.0                                                     
 * Copyright:    Fintravalores S.A. S.A.                                 
 ***************************************************************************/

package com.tsp.operation.model.DAOS;

import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import org.apache.log4j.*;

public class UbicacionVehicularClientesDAO extends MainDAO {
    static transient Logger logger = Logger.getLogger(UbicacionVehicularClientesDAO.class);
    
    /** Creates a new instance of UbicacionVehicularClientesDAO */
    public UbicacionVehicularClientesDAO() {
        super("UbicacionVehicularClientesDAO.xml");
        
    }
    // LOS SIGUIENTES M�TODOS Y/O ATRIBUTOS FUERON COPIADOS DE LA APLICACI�N SOT
    // PARA IMPLEMENTAR EL MODULO DE UBICACION VEHICULAR EXOM.
    // Ivan Dario Gomez - Enero 3 de 2006 - 8:30 am
   
   
    private String []  camposReporte;
    private LinkedList datosReporte;
    private LinkedList devoluciones;
    /**
   * Este m�todo genera los registros que hacen parte del reporte de ubicaci�n
   * vehicular y los guarda en una lista.
   * @param ubicVehArgs Criterios de b�squeda. Son los siguientes:
   *        ubicVehArgs[0] = C�digo del cliente.
   *        ubicVehArgs[1] = Fecha Inicial
   *        ubicVehArgs[2] = Fecha Final
   *        ubicVehArgs[3] = Tipos de Viaje (NA, RM, RC, RE, DM, DC)
   * @param user Objeto con los datos de un usuario destinatario
   *             Para cualquier otro usuario, este atributo no tiene efecto alguno.
   * @throws SQLException si un error de acceso a la base de datos ocurre.
   */
    public void UbicacionVehicularSearch(String [] ubicVehArgs, Usuario user)  throws SQLException {
        Connection con = null;
        ResultSet rs   = null;
        Statement stmt = null;
      
      try {
              String query =""; 
              String consulta  = super.obtenerSQL("SQL_UBICACION_VEHICULAR");
              String User   = user.getIdusuario();
              String cliente        = ubicVehArgs[0]; //request.getParameter("cliente");
              String fechaini       = ubicVehArgs[1]; //request.getParameter("fechaini");
              String fechafin       = ubicVehArgs[2]; //request.getParameter("fechafin");
              String listaTipoViaje = ubicVehArgs[3]; //request.getParameter("listaTipoViaje");
              String estadoViajes   = ubicVehArgs[4];
              String condirem       = "";
              Vector cond = new Vector(4, 1);
              cond.clear();
              if( !fechaini.equals("") ) {
                condirem = " AND fecrem BETWEEN '" + fechaini + "' AND '" + fechafin + "' ";
              }

              query = consulta.replaceAll("#CLIENTE#","'" + cliente +"'"+condirem );

              if (!estadoViajes.equals("TODOS")) {
                if (estadoViajes.equals("RUTA")) {
                  cond.add(" position('EN VIA' IN pl.observacion) > 0 ");
                } else if (estadoViajes.equals("CONENTREGA")) {
                  cond.add(" position('FIN DE VIAJE' IN pl.observacion) > 0 ");
                } else if (estadoViajes.equals("PORCONF")) {
                  cond.add(" position('PROGRAMADO PARA SALIR' IN pl.observacion) > 0 ");
                }
              }
              //******* FIN BLOQUE TEMPORAL *********************************************
              // Se excluyen los viajes de SAFERBO (paqueteo) porque no tienen seguimiento
              // en trafico
              cond.add(" pl.plaveh NOT IN ('AIH388','TIZ628','TKD233','TNE245','SAFERB')");
              // Excluimos las planillas que no tengan Trafico
              //cond.add(" LTRIM(pl.ultimoreporte) <> '' ");
              //******* FIN BLOQUE TEMPORAL ********************************************

              String [] tiposDeViaje = listaTipoViaje.split(",");
              String condition = ((tiposDeViaje.length > 1)? "(" : "");
              for( int tv = 0; tv < tiposDeViaje.length; tv++ ) {
                if( tiposDeViaje.length > 1 ) {
                  condition += "rm.tipoviaje = '" + tiposDeViaje[tv] + "'";
                  condition += ((tv + 1 < tiposDeViaje.length)? " OR " : "");
                } else {
                  condition += "rm.tipoviaje = '" + tiposDeViaje[tv] + "'";
                }
              }
              condition += ((tiposDeViaje.length > 1)? ")" : ""); 
              cond.add( condition );
              for( int cnd = 0; cnd < cond.size(); cnd++ ) {
                if( cnd == 0 ) {
                  query += (String)cond.get(cnd);
                } else {
                  query += " AND " + ((String)cond.get(cnd));
                }
              }
              // OJO - desactivado temporalmente  se debe mejorar este codigo - NP - 2004-11-02
              //         if ( user.getTipo().equals("DEST") )
              //           query += " AND rm.destinatario = '" + user.getNombreClienteDestinat() + "' ";

              query += " ORDER BY pl.fecdsp";

              logger.info("QUERY: " + query ); 

              con = this.conectarJNDI("SQL_UBICACION_VEHICULAR");//JJCastro fase2
              if(con!=null){
              stmt = con.prepareStatement(query);
              rs =  stmt.executeQuery( query );
              ResultSetMetaData rsmd = rs.getMetaData();
              int columnas = rsmd.getColumnCount();
              for( int i=0; i<columnas; i++ ){
                  ////System.out.print(rsmd.getColumnName(i+1)+",");
              }
              buscarCamposDeReporte(cliente,User);
              String camposReporte [] = obtenerCamposDeReporte();
              datosReporte = new LinkedList();
              while(rs.next()){
                   datosReporte.add(ReporteUbicacionVehicular.load(rs, camposReporte));
              }
       }}catch(SQLException e) {
            throw new SQLException("Error en rutina UbicacionVehicularSearch  [DAO]... \n"+e.getMessage());        
    }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (stmt  != null){ try{ stmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    
  }
    
   
  
 
 

  
  // metodos de alejandro*********************
  
  /**
     * Este m�todo permite obtener los titulos del encabezado del reporte de
     * informaci�n al cliente. El Hashtable que es devuelto contiene los titulos de todos
     * los campos del reporte y la clave de cada titulo es su respectivo nombre
     * de campo en la Base de datos, para obtener los nombre de los campos del
     * reporte utilize el metodo <CODE>String [] obtenerCamposDeReporte()</CODE>.
     * @throws SQLException Si algun problema ocurre en el acceso a la Base de Datos.
     * @return Una tabla con los titulos de los campos del reporte de informaci�n al cliente.
     */
    public Hashtable obtenerTitulosDeReporte() throws SQLException{
        ConsultasGeneralesDeReportesDAO cgrd = new ConsultasGeneralesDeReportesDAO();
        return cgrd.obtenerTitulosDeReporte("ubicacionveh");
    }
 
    /**
     * Este m�todo solo puede ser accesado dentro del DAO, y permite buscar los
     * nombres de los campos del reporte ubicacionVehicular en la base de datos. Es
     * invocado por el m�todo <code>void buscarDatosDeReporte(String [])</code> y el resultado
     * de la busqueda el devuelto por el m�todo
     * <CODE>String [] obtenerCamposReporte()</CODE>.
     * @param codigoCliente El codigo del cliente que ver� el reporte, este codigo es obtenido del usuario
     * que est� actualmente activo en la sesi�n.
     * @param codigoReporte El codigo del reporte de devoluciones.
     * @throws SQLException Si algun problema ocurre con el acceso a la Base de datos.
     */
    private void buscarCamposDeReporte(String codigoCliente, String loggedUser) throws SQLException{
        ConsultasGeneralesDeReportesDAO cgr = new ConsultasGeneralesDeReportesDAO();
        camposReporte = cgr.buscarCamposDeReporte(codigoCliente, "ubicacionveh", "C", loggedUser );
       
    }
    
    /**
     * Devuelve un array de tipo String [] donde se encuentran almacenados los nombres
     * de los campos del reporte de informaci�n al cliente.
     * @return El array con los nombres de los campos del reporte de informaci�n al cliente.
     */
    public String [] obtenerCamposDeReporte(){
        return camposReporte;
    }
    
        /**
     * Devuelve una lista [] donde se encuentran almacenados los nombres
     * de los campos del reporte de informaci�n al cliente.
     * @return la lista con los nombres de los campos del reporte de informaci�n al cliente.
     */
    public LinkedList obtenerDatosReporte(){
        return datosReporte;
    }
    
    /**
   * Retorna las devoluciones asociadas a una planilla en el reporte
   * de ubicaci�n vehicular.
   * @param numPlanilla N�mero de la planilla.
   * @throws SQLException Si un error de acceso a la base de datos ocurre.
   */
  public void UbicacionVehicularReturn(String numPlanilla) throws SQLException {
    Connection con = null;
    ResultSet         rs    = null;
    PreparedStatement pstmt = null;
    devoluciones            = null;
    String query = "SQL_DEVOLUCIONES";
    try {
      // Seleccionar opciones activas del perfil.
     con = this.conectarJNDI(query);
     if (con != null) {
      pstmt = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
      pstmt.setString(1, numPlanilla );
      rs = pstmt.executeQuery();
      
      while(rs.next()) {
        if( devoluciones == null )
          devoluciones = new LinkedList();
           Hashtable fila = new Hashtable();
           for(int i=1;i<=19;i++){
               String valor = rs.getString(i);
             
               fila.put(String.valueOf(i),valor);
             
           }
        devoluciones.add(fila);
      }      
    }}
    catch( Exception ex){
        ex.printStackTrace();
    }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (pstmt  != null){ try{ pstmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    
  }
  
   /**
   * Getter for property devoluciones.
   * @return Value of property devoluciones.
   */
  public LinkedList getDevoluciones() {
    return this.devoluciones;
  }
  
  
}
