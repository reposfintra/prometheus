/********************************************************************
 *      Nombre Clase.................   AdminHistoricoDAO.java
 *      Descripci�n..................   Bean de la tabla admin_historicos
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.DAOS;

import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import java.text.*;
/**
 *
 * @author  dlamadrid
 */
public class AdminHistoricoDAO
{
        
        
        private static final String insertar        = "insert into admin_historicos(fecha_inicio,fecha_final,tabla_o,tabla_d,rutina,duracion,estado,usuario) values (?,?,?,?,?,?,?,?) ";
        private static final String eliminar        = "delete from admin_historicos where oid=?";
        private static final String listar          = "select oid,* from admin_historicos ";
        private static final String listarPorId     = "select oid,* from admin_historicos where oid=?";
        private static final String actualizar      = "update admin_historicos set fecha_inicio=?,fecha_final=?,tabla_o=?,tabla_d=?,rutina=?,duracion=?,estado=?,usuario=? where oid=?  ";
        private static final String existeHistorico ="select * from admin_historicos where tabla_o = ?";
        
        private static AdminHistorico admin;
        
        private static Vector historico;
        /** Creates a new instance of AdminHistoricoDAO */
        public AdminHistoricoDAO ()
        {
        }
        
        
        /**
         * Metodo que setea el vector de objetos tipo Adminhistorico
         * @autor.......David Lamadrid
         * @param.......Vector historico
         * @see.........
         * @version.....1.0.
         * @return.......
         */
        public void setHistorico (Vector historico)
        {
                this.historico=historico;
        }
        
        
        /**
         * Metodo que obtiene el vector de objetos tipo Adminhistorico
         * @autor.......David Lamadrid
         * @param.......
         * @see.........
         * @version.....1.0.
         * @return.......Vector historico
         */
        public Vector getHistorico ()
        {
                return this.historico;
        }
        
        /**
         * Metodo que obtiene un objeto tipo Adminhistorico
         * @autor.......David Lamadrid
         * @param.......
         * @see.........
         * @version.....1.0.
         * @return.......AdminHistorico admin
         */
        public AdminHistorico getAdmin ()
        {
                return admin;
        }
        
        /**
         * Metodo que setea un objeto tipo Adminhistorico
         * @autor.......David Lamadrid
         * @param.......AdminHistorico admin
         * @see.........
         * @version.....1.0.
         * @return.......
         */
        public void setAdmin (AdminHistorico admin)
        {
                this.admin = admin;
        }
        
        /**
         * Metodo que inserta un registro en la tabla admin_historicos
         * @autor.......David Lamadrid
         * @param.......
         * @see.........
         * @throws......SQLException
         * @version.....1.0.
         * @return.......
         */
        public void insertar () throws SQLException
        {
                Connection con = null;
                PreparedStatement st = null;
                ResultSet rs = null;
                PoolManager poolManager = null;
                try
                {
                        poolManager = PoolManager.getInstance ();
                        con = poolManager.getConnection ("fintra");
                        if (con != null)
                        {
                                st = con.prepareStatement (insertar);
                                st.setString (1,admin.getFechaInicio ());
                                st.setString (2,admin.getFechaFinal ());
                                st.setString (3,admin.getTablaO ());
                                st.setString (4,admin.getTablaD ());
                                st.setString (5,admin.getRutina ());
                                st.setInt (6,admin.getDuracion ());
                                st.setString (7,admin.getEstado ());
                                st.setString (8,admin.getUsuario ());
                                st.executeUpdate ();
                        }
                        ////System.out.println ("consulta"+st);
                }catch(SQLException e)
                {
                        throw new SQLException ("ERROR DURANTE LA INSERCION DE ADMIN HISTORICO " + e.getMessage () + " " + e.getErrorCode ());
                }
                finally
                {
                        if (st != null)
                        {
                                try
                                {
                                        st.close ();
                                }
                                catch(SQLException e)
                                {
                                        throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                                }
                        }
                        if (con != null)
                        {
                                poolManager.freeConnection ("fintra", con);
                        }
                }
        }
        
        /**
         * Metodo que sete el Vector historico con una lista de registros de  admin_historicos
         * @autor.......David Lamadrid
         * @param.......
         * @see.........
         * @throws......
         * @version.....1.0.
         * @return.......
         */
        public void listarHistorico ()throws SQLException
        {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;
                PoolManager poolManager = null;
                historico = null;
                
                try
                {
                        poolManager = PoolManager.getInstance ();
                        con = poolManager.getConnection ("fintra");
                        if (con != null)
                        {
                                st = con.prepareStatement (listar);
                                rs = st.executeQuery ();
                                
                                historico =  new Vector ();
                                
                                while (rs.next ())
                                {
                                        historico.add (admin.load (rs));
                                        // ////System.out.println("");
                                }
                        }
                }catch(SQLException e)
                {
                        throw new SQLException ("ERROR DURANTE LA BUSQUEDA " + e.getMessage ()+" " + e.getErrorCode ());
                }finally
                {
                        if(st != null)
                        {
                                try
                                {
                                        st.close ();
                                }
                                catch(SQLException e)
                                {
                                        throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                                }
                        }
                        if ( con != null)
                        {
                                poolManager.freeConnection ("fintra", con );
                        }
                }
        }
        
        /**
         * Metodo que sete el Vector historico con una lista de registros de  admin_historicos dado el codigo del registro
         * @autor.......David Lamadrid
         * @param.......String id(Codigo del registro)
         * @see.........
         * @throws...... SQLException
         * @version.....1.0.
         * @return.......
         */
        public void listarHistoricoPorId (String id)throws SQLException
        {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;
                PoolManager poolManager = null;
                admin = null;
                ////System.out.println ("id"+id);
                try
                {
                        poolManager = PoolManager.getInstance ();
                        con = poolManager.getConnection ("fintra");
                        if (con != null)
                        {
                                st = con.prepareStatement (listarPorId);
                                st.setString (1, id);
                                //////System.out.println("consulta1 "+st);
                                rs = st.executeQuery ();
                                
                                while (rs.next ())
                                {
                                        this.setAdmin (admin.load (rs));
                                }
                                
                        }
                }catch(SQLException e)
                {
                        throw new SQLException ("ERROR DURANTE LA BUSQUEDA " + e.getMessage ()+" " + e.getErrorCode ());
                }finally
                {
                        if(st != null)
                        {
                                try
                                {
                                        st.close ();
                                }
                                catch(SQLException e)
                                {
                                        throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                                }
                        }
                        if ( con != null)
                        {
                                poolManager.freeConnection ("fintra", con );
                        }
                }
                // return admin
        }
        
        /**
         * Metodo que verifica si existe un registro con un nombre de tabla
         * @autor.......David Lamadrid
         * @param.......
         * @see.........
         * @throws......SQLException
         * @version.....1.0.
         * @return.......boolean existe(true si existe ,false si no existe)
         */
        public boolean  existeHistorico (String nombreTabla)throws SQLException
        {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;
                PoolManager poolManager = null;
                //historico = null;
                boolean existe=false;
                
                try
                {
                        poolManager = PoolManager.getInstance ();
                        con = poolManager.getConnection ("fintra");
                        if (con != null)
                        {
                                st = con.prepareStatement (existeHistorico);
                                st.setString (1, nombreTabla);
                                rs = st.executeQuery ();
                                
                                if(!nombreTabla.equals (""))
                                {
                                        // historico =  new Vector();
                                        
                                        while (rs.next ())
                                        {
                                                existe = true;
                                        }
                                }
                        }
                }catch(SQLException e)
                {
                        throw new SQLException ("ERROR DURANTE LA BUSQUEDA " + e.getMessage ()+" " + e.getErrorCode ());
                }finally
                {
                        if(st != null)
                        {
                                try
                                {
                                        st.close ();
                                }
                                catch(SQLException e)
                                {
                                        throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                                }
                        }
                        if ( con != null)
                        {
                                poolManager.freeConnection ("fintra", con );
                        }
                }
                return existe;
        }
        
        /**
         * Metodo que actualiza un registro de admin_historicos
         * @autor.......David Lamadrid
         * @param.......String nombreTabla
         * @see.........
         * @throws......SQLException registro ya existe en el sitema
         * @version.....1.0.
         * @return.......
         */
        public void actualizarHistorico (String nombreTabla) throws SQLException
        {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;
                PoolManager poolManager = null;
                
                try
                {
                        poolManager = PoolManager.getInstance ();
                        con = poolManager.getConnection ("fintra");
                        if (con != null)
                        {
                                st = con.prepareStatement (actualizar);
                                st.setString (1, admin.getFechaInicio ());
                                st.setString (2, admin.getFechaFinal ());
                                st.setString (3, admin.getTablaO ());
                                st.setString (4, admin.getTablaD ());
                                st.setString (5, admin.getRutina ());
                                st.setInt (6, admin.getDuracion ());
                                st.setString (7, admin.getEstado ());
                                st.setString (8,admin.getUsuario ());
                                st.setString (9,nombreTabla);
                                ////System.out.println ("consulta"+ st);
                                st.executeUpdate ();
                        }
                }catch(SQLException e)
                {
                        throw new SQLException ("ERROR AL ACTUALIZAR HISTORICO" + e.getMessage ()+"" + e.getErrorCode ());
                }finally
                {
                        if(st != null)
                        {
                                try
                                {
                                        st.close ();
                                }
                                catch(SQLException e)
                                {
                                        throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                                }
                        }
                        if ( con != null)
                        {
                                poolManager.freeConnection ("fintra", con );
                        }
                }
        }
        
        /**
         * Metodo que elimina un registro de admin_historico dado el nombre de la tabla
         * @autor.......David Lamadrid
         * @param.......String nombreTabla
         * @see.........
         * @throws......registro no existe en el sitema
         * @version.....1.0.
         * @return.......
         */
        public void eliminarHistorico (String nombreTabla) throws SQLException
        {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;
                PoolManager poolManager = null;
                
                try
                {
                        poolManager = PoolManager.getInstance ();
                        con = poolManager.getConnection ("fintra");
                        if (con != null)
                        {
                                st = con.prepareStatement (eliminar);
                                st.setString (1, nombreTabla);
                                st.executeUpdate ();
                        }
                }catch(SQLException e)
                {
                        throw new SQLException ("ERROR AL ELIMINAR HISTORIAL" + e.getMessage ()+"" + e.getErrorCode ());
                }finally
                {
                        if(st != null)
                        {
                                try
                                {
                                        st.close ();
                                }
                                catch(SQLException e)
                                {
                                        throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                                }
                        }
                        if ( con != null)
                        {
                                poolManager.freeConnection ("fintra", con );
                        }
                }
        }
        
        
        public  boolean existeTabla (String nombre ) throws SQLException
        {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;
                PoolManager poolManager = null;
                boolean x = false;
                try
                {
                        poolManager = PoolManager.getInstance ();
                        con = poolManager.getConnection ("fintra");
                        if (con != null)
                        {
                                st = con.prepareStatement ("select * from "+ nombre);
                                ////System.out.println ("query: "+st);
                                rs = st.executeQuery ();
                                x=true;
                        }
                }
                catch(org.postgresql.util.PSQLException e)
                {
                        poolManager.freeConnection ("fintra", con );
                }
                finally
                {
                        if(st != null)
                        {
                                try
                                {
                                        st.close ();
                                }
                                catch(SQLException e)
                                {
                                        throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                                }
                        }
                        if ( con != null)
                        {
                                poolManager.freeConnection ("fintra", con );
                        }
                }
                return x;
        }//Fin del Metodo
        
        
        /**
         * Metodo que retorna un String con una fecha final dada una fecha inicial y un nuemro de dias
         * @autor.......David Lamadrid
         * @param.......String fechai,int n(numero de dias)
         * @see.........
         * @throws......registro no existe en el sitema
         * @version.....1.0.
         * @return.......String fechaf(fecha Final)
         */
        public  String fechaFinal (String fechai,int n)
        {
                String timeStamp = fechai;
                SimpleDateFormat fmt = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
                Calendar c = Util.crearCalendar (timeStamp);
                c.add (c.DATE, n);
                java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp (c.getTime ().getTime ());
                String fechaf=""+sqlTimestamp;
                return fechaf;
        }
}



