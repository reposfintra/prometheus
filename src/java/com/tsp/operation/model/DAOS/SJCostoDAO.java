/**
 * Nombre        SJCostoDAO.java
 * Descripci�n
 * Autor         Mario Fontalvo Solano
 * Fecha         3 de mayo de 2006, 02:41 PM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.SJCosto;
import java.util.*;
import java.sql.*;

public class SJCostoDAO extends MainDAO {
    
    /** Crea una nueva instancia de  SJCostoDAO */
    public SJCostoDAO() {
        super ("SJCostoDAO.xml");
    }
    

    
     /**
      * M�todo para extraer los costos de un estandar
      * @autor mfontalvo
      * @throws Exception.
      * @param distrito, distrito del estandar.
      * @param stdjob, estanadar a consultar.
      * @return List, lista de costos.
      * @version..... 1.0.
      **/
     public Vector getCostos (String distrito, String stdjob)throws Exception{
        Connection con = null;
        PreparedStatement st          = null; 
        ResultSet         rs          = null;
        Vector            lista       = new Vector();
         String query = "SQL_OBTENER_COSTOS";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,  distrito       );
            st.setString(2,  stdjob         );
            rs = st.executeQuery();
            while(rs.next()){
                SJCosto sj = new SJCosto();  
                sj.setCodOrigen       ( rs.getString("codOrigen" ) );
                sj.setNomOrigen       ( rs.getString("nomOrigen" ) );
                sj.setCodDestino      ( rs.getString("codDestino") );
                sj.setNomDestino      ( rs.getString("nomDestino") );
                sj.setCodAgenciaRelDes( rs.getString("codAgencia") );
                sj.setNomAgenciaRelDes( rs.getString("nomAgencia") );
                sj.setTipoCosto       ( rs.getString("tipoCosto" ) );
                sj.setValorCosto      ( rs.getDouble("cost"      ) );
                sj.setMoneda          ( rs.getString("currency"  ) );
                sj.setFechaCreacion   ( new java.util.Date(rs.getTimestamp("fechaCreacion").getTime()) );
                sj.setCodMasCF        ( rs.getString("ft_code"   ) );
                sj.setCodMasFT        ( rs.getString("cf_code"   ) );
                sj.setTasa            ( rs.getDouble("tasa"      ) );
                lista.add(sj);
            }
            }}catch(Exception e) {
             throw new SQLException(" SJCostoDAO.getCostos()... \n " +e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }     
    
    
}
