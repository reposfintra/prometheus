/*************************************************************
 * Nombre      ............... ImportarDAO.java
 * Descripcion ............... Clase general de importaciones
 * Autor       ............... mfontalvo
 * Fecha       ............... Octubre - 01 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/


package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.Util;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;


/**
 * Clase de consulta a la base de datos
 */
public class ImportacionDAO extends MainDAO {
    
    /**
     * Tipo que indica la creacion de la tabla de parametros
     */
    
    private static final int CREATE_PAR  = 0;
    /**
     * Tipo que indica la insercion de datos de la tabla de parametros
     */
    private static final int INSERT_PAR  = 1;
    /**
     * Tipo que indica la segunda manera creacion de la tabla de parametros
     */
    private static final int INSERT_PAR2 = 2;
    /**
     * Tipo que indica la eliminacion de un registro de la tabla de parametros
     */
    private static final int DELETE_PAR  = 3;
    /**
     * Tipo que indica la actualizacion de un registro de la tabla de parametros
     */
    private static final int UPDATE_PAR  = 4;
    /**
     * Tipo que indica la segunda manera de eliminacion de un registro de la tabla de parametros
     */
    private static final int UPDATE_PAR2 = 5;
    
    /**
     * Tipo que indica la creacion de la tabla de estrutura de importacion
     */
    private static final int CREATE_EST  = 6;
    /**
     * Tipo que indica la insercion de un registro de la tabla de estructuras
     */
    private static final int INSERT_EST  = 7;
    /**
     * Tipo que indica la eliminacion  de un registro de la tabla de estructuras
     */
    private static final int DELETE_EST  = 8;
    
    /**
     * Tipo que indica la consulta general de parametros de importacion
     */
    private static final int SELECT_PAR  = 9;   // busca parametros de una tabla
    /**
     * Tipo que indica la consulta general de estructura de importacion
     */
    private static final int SELECT_EST  = 10;  // busca la estrucura de una tabla
    /**
     * Tipo que indica la consulta de las tablas del sistema
     */
    private static final int SELECT_TAB  = 11;  // busca campos de la tabla
    /**
     * Tipo que indica la busqueda de la tabla mas adecuada
     */
    private static final int SELECT_TMA  = 12;  // tabla mas adeucada
    
    
    
    /***************************************************************************/
    
    /**
     * query para insertar datos basicos de parametros de importacion
     */
    
    private final String SQL_INSERT_PAR =
    "  insert into importacion_parametros   " +
    "  (formato, tabla, descripcion, usuario_creacion, fecha_creacion, usuario_actualizacion, fecha_actualizacion) " +
    "  values ( ? , ? , ? , ? , ? , ? , ?)     " ;
    
    
    /**
     * query para insertar todos los campos dentro de parametros de importacion
     */
    private final String SQL_INSERT_PAR2 =
    "  insert into importacion_parametros   " +
    "  (formato, tabla, descripcion, ctab, fcon, adat, bpna, insert, update,  usuario_creacion, fecha_creacion, usuario_actualizacion, fecha_actualizacion) " +
    "  values ( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?, ? , ? )  " ;
    
    
    /**
     * query para eliminar registros dentro de parametros de importacion
     */
    
    private final String SQL_DELETE_PAR =
    " delete from importacion_parametros where UPPER(formato) = UPPER(?) ";
    
    
    /**
     * query para modificar todos los campos de parametros de importacion
     */
    
    private final String SQL_UPDATE_PAR =
    "  update importacion_parametros  " +
    "  set                            " +
    "      descripcion = ?,           " +
    "      ctab        = ?,           " +
    "      fcon        = ?,           " +
    "      adat        = ?,           " +
    "      bpna        = ?,           " +
    "      titulo      = ?,           " +
    "      evento      = ?,           " +
    "      insert      = ?,           " +
    "      update      = ?,           " +
    "      usuario_actualizacion = ?, " +
    "      fecha_actualizacion   = ?  " +
    "  where UPPER(formato)  = UPPER(?) " ;
    
    /**
     * query para modificar datos basicos de parametros de importacion
     */
    
    private final String SQL_UPDATE_PAR2 =
    "  update importacion_parametros  " +
    "  set                            " +
    "      descripcion           = ?, " +
    "      usuario_actualizacion = ?, " +
    "      fecha_actualizacion   = ?  " +
    "  where UPPER(formato)  = UPPER(?)  " ;
    
    
    /**
     * query para buscar los parametros de importacion de una tabla
     */
    
    private final String SQL_BUSCAR_PAR =
    " select formato, tabla, descripcion, ctab, fcon, adat, bpna, titulo, evento, insert, update" +
    " from   importacion_parametros              " +
    " where  upper(formato) like ( upper(?) )    " +
    " order by formato                           " ;
    
    /***************************************************************************/
    
    
    /**
     * query para insertar datos de estructura de tablas de importacion
     */
    
    private final String SQL_INSERT_EST =
    "  insert into importacion_estructura   " +
    "  (formato, tabla, sec, campo, tipo, tiene_default, tipo_default, valor_default , alias, validacion, insercion, aplica_update, obs_validacion, obs_insercion, usuario_creacion, fecha_creacion, usuario_actualizacion, fecha_actualizacion)" +
    "  values ( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?, ? , ? , ? , ? , ?, ?) " ;
    
    /*
     * query para eliminar datos de estructura de tablas de importacion
     */
    
    /**
     * Tipo que indica la eliminacion de un registro de la tabla de estructura
     */
    private final String SQL_DELETE_EST =
    " delete from importacion_estructura where upper(formato) = upper(?) " ;
    
    
    /**
     * query para buscar datos de estructura de tablas de importacion
     */
    
    private final String SQL_BUSCAR_EST =
    " select tabla, sec, campo, tipo, extra " +
    "        ,tiene_default, tipo_default, valor_default, alias, validacion , insercion, aplica_update, obs_validacion, obs_insercion" +
    " from   importacion_estructura         " +
    " where UPPER(formato)  = UPPER(?)      " +
    " order by sec                          ";
    
    
    /***************************************************************************/
    
    
    /**
     * query para buscar los campos de una tabla en la base de datos
     */
    
    private final String SQL_EXISTS_TAB =
    " SELECT                        " +
    "      B.ATTNAME                " +
    " FROM                          " +
    "      PG_TYPE      A,          " +
    "      PG_ATTRIBUTE B           " +
    " WHERE                         " +
    "       A.TYPNAME  = ?          " +
    "   AND A.TYPRELID = B.ATTRELID " +
    "   AND B.ATTNUM   > 0          " +
    "   AND B.ATTTYPID > 0          " +
    " ORDER BY B.ATTNUM             " ;
    
    
    /**
     * query para seleccionar la tabla mas adecuada segun el nombre del archivo
     */
    
    private final String SQL_BUSCAR_TMA =
    " SELECT formato                             " +
    " FROM                                       " +
    "    importacion_parametros                  " +
    " WHERE                                      " +
    "    ( bpna = 'C' AND UPPER(formato) = UPPER(?)) " +
    " OR ( bpna = 'I' AND UPPER(   ?   ) LIKE UPPER(formato || '%'))   " +
    " ORDER BY length(formato) DESC              " +
    " LIMIT 1                                    " ;
    
    
    /**
     * query para extraer el listado general de tablas de la base de datos
     */
    
    private final String SQL_TABLAS =
    " SELECT distinct schemaname, tablename   " +
    " FROM   pg_type, pg_tables               " +
    " WHERE  typname = tablename              " +
    "    and schemaname not in ('pg_catalog', 'information_schema')  " +
    " ORDER BY schemaname, tablename          ";
    
    
    /**
     * query para verificar si un usuario puede o no migrar con un formato
     */
    private final String SQL_USUARIO_VALIDO = 
    " SELECT (','||trim(dato)||',') as dato FROM tablagen WHERE table_type = 'TFORMATOS' AND UPPER(table_code) = UPPER(?) ";
    
    /**
     * Crea una nueva Instancia de ImportacionesDAO
     */
    public ImportacionDAO() {
        super("ImportacionDAO.xml");//JJCastro fase2
    }
    public ImportacionDAO(String dataBaseName) {
        super("ImportacionDAO.xml", dataBaseName);//JJCastro fase2
    }
    
    
    /**
     * Procedimiento que ejcuta sentencias de creacion, modificacion insert
     * dependiendo del tipo de consulta que se mande a ejecutar
     * parametros:
     *  Tipo   -> tipo de consulta
     *  args[] -> parametros del query
     * @autor : Ing. Mario Fontalvo
     * @param Tipo Idica el tipo de consulta a realizar
     * @param args Arreglo de String que contiene los parametros de la consulta
     * @throws Exception .
     */
    
    private void EXECUTE_UPDATE(int Tipo, String args[]) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st          = null;
        String query = "SQL_INSERT_PAR";//JJCastro fase2
        String sql1 = "";

        con = this.conectarJNDI(query);//JJCastro fase2
        if (con == null)
            throw new SQLException("Sin conexion");
        try {
            String SQL = (Tipo == CREATE_PAR        )? "":
                (Tipo == INSERT_PAR        )? "SQL_INSERT_PAR":
                    (Tipo == INSERT_PAR2       )? "SQL_INSERT_PAR2":
                        (Tipo == DELETE_PAR        )? "SQL_DELETE_PAR":
                            (Tipo == UPDATE_PAR        )? "SQL_UPDATE_PAR":
                                (Tipo == UPDATE_PAR2       )? "SQL_UPDATE_PAR2":
                                    (Tipo == CREATE_EST        )? "":
                                        (Tipo == INSERT_EST        )? "SQL_INSERT_EST":
                                            (Tipo == DELETE_EST        )? "SQL_DELETE_EST": "";
                                            
                                            if (!SQL.equals("")){
                                                st = con.prepareStatement(this.obtenerSQL(SQL));//JJCastro fase2
                                                if (args!=null){
                                                    for (int i=0; i<args.length;i++){
                                                        st.setString((i+1),  (!args[i].equals("#FECHA#")? args[i]: Util.getFechaActual_String(6) ) );
                                                    }
                                                }
                                                sql1 = st.toString();
                                                st.executeUpdate();
                                            }
                                            else throw new SQLException("Tipo de Operacion no definida . ["+Tipo+"]");
        }
        catch(Exception e) {
            System.out.println("SQL: " + sql1);
            e.printStackTrace();
            if (!(Tipo == CREATE_PAR || Tipo == CREATE_EST )) throw new SQLException("Error en rutina EXECUTE_UPDATE [ImportacionesDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Procedimiento que ejecuta sentencias de consulta
     * dependiendo del tipo de consulta que se mande a ejecutar
     * parametros:
     *  Tipo   -> tipo de consulta
     *  args[] -> parametros del query
     * @autor : Ing. Mario Fontalvo
     * @param Tipo tipo de consulta a ejecutar
     * @param args Arreglo de String que contiene los parametros de la consulta
     * @throws SQLException .
     * @return Lista de datos a retornar
     */
    
    private List EXECUTE_QUERY(int Tipo, String []args) throws SQLException {
        
        Connection        con        = null;//JJCastro fase2
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        List              Resultado   = new LinkedList();
        String query = "SQL_BUSCAR_PAR";//JJCastro fase2


        con = this.conectarJNDI(query);//JJCastro fase2
        if (con == null)
            throw new SQLException("Sin conexion");
        try {
            
            // seleccion del query dependiewndo del tipo
            
            String SQL = (Tipo == SELECT_PAR  ? "SQL_BUSCAR_PAR"  ://JJCastro fase2
                Tipo == SELECT_EST  ? "SQL_BUSCAR_EST"  ://JJCastro fase2
                    Tipo == SELECT_TMA  ? "SQL_BUSCAR_TMA"  ://JJCastro fase2
                        Tipo == SELECT_TAB  ? "SQL_EXISTS_TAB"  : "");//JJCastro fase2
                        
                        if (!SQL.equals("")){
                            st = con.prepareStatement(this.obtenerSQL(SQL));//JJCastro fase2
                            
                            // seteo de parametros de la consulta segun args[]
                            
                            if (args!=null){
                                for (int i=0;i<args.length;i++)
                                    st.setString((i+1),  (args[i].equals("ALL")?"%":args[i]));
                            }
                            
                            rs = st.executeQuery();
                            while (rs.next()){
                                Object datos = null;
                                
                                // seteo del bean de salida segun sea el tipo de query
                                
                                /////////////////////////////////////////////
                                if ( Tipo == SELECT_PAR )
                                    datos = ImportacionParametros.load(rs);
                                
                                else if ( Tipo == SELECT_EST )
                                    datos = ImportacionEstructura.load(rs);
                                
                                else if ( Tipo == SELECT_TAB)
                                    datos = rs.getString(1);
                                
                                else if ( Tipo == SELECT_TMA)
                                    datos = rs.getString(1);
                                ////////////////////////////////////////////
                                
                                Resultado.add(datos);
                                
                            }
                        }
        }
        catch(SQLException e) {
            e.printStackTrace();
            throw new SQLException("Error en rutina EXECUTE_QUERY [ImportacionesDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return Resultado;
    }
    
    
    
    /**
     * procedimiento para agragar una tabla de importacion en las siguientes
     * tablas
     *  - importacion_parametros
     *  - importacion_estrucutura
     * @autor : Ing. Mario Fontalvo
     * @param campos Array de campos a ingresar
     * @param estructura Array de definiciones de Importacion
     * @param def Arreglo de valores que indica si el campo posee un valor default o no de migracion
     * @param tdef Arreglo de valores que indica si el tipo de valores default que posee un campo.
     * @param vdef valor default de la variable, este se implementará si en el momento de la
     * importacion este campo viene vacio
     * @param tabla nombre de la tabla de importacion
     * @param desc descripcion de la tabla de importacion
     * @param usuario usuario que registra la tabla de importacion
     * @throws Exception .
     * @see EXECUTE_UPDATE (int, String [])
     */
    
    public void agregarEstructura   (String []campos, String [] estructura, String [] def, String []tdef, String []vdef, String []alias, String []validaciones, String []insercion, String update[], String []obs_validaciones, String []obs_insercion, String formato, String tabla, String desc, String usuario) throws Exception{
        try {
            if (campos!=null){
                
                // agrega parametros de importacion datos basicos
                String [] paramsPar = { formato, tabla,  desc, usuario, "#FECHA#", usuario, "#FECHA#"  } ;
                this.EXECUTE_UPDATE( INSERT_PAR , paramsPar );
                
                // agrega la estrucutura campos y tipos de datos
                for (int i = 0 ; i < campos.length ; i++ ){
                    String [] paramsEst = { formato, tabla,  String.valueOf(i),  campos[i],  estructura[i],  def[i],  tdef[i], vdef[i] , alias[i], validaciones[i],  insercion[i], update[i], obs_validaciones[i],  obs_insercion[i], usuario, "#FECHA#", usuario, "#FECHA#" } ;
                    this.EXECUTE_UPDATE( INSERT_EST , paramsEst);
                }
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en rutina agregarEstructura [ImportacionesDAO]... \n"+e.getMessage());
        }
    }
    
    
    /**
     * procedimiento para modifila una tabla de importacion en las siguientes
     * tablas
     *  - importacion_parametros
     *  - importacion_estrucutura
     * @autor : Ing. Mario Fontalvo
     * @param campos Array de campos a ingresar
     * @param estructura Array de definiciones de Importacion
     * @param def Arreglo de valores que indica si el campo posee un valor default o no de migracion
     * @param tdef Arreglo de valores que indica si el tipo de valores default que posee un campo.
     * @param vdef valor default de la variable, este se implementará si en el momento de la
     * importacion este campo viene vacio
     * @param tabla nombre de la tabla de importacion
     * @param desc descripcion de la tabla de importacion
     * @param usuario usuario que registra la tabla de importacion
     * @throws Exception .
     * @see EXECUTE_UPDATE (int, String [])
     */
    public void modificarEstructura(String [] campos, String [] estructura, String [] def, String tdef[], String vdef[], String []alias, String []validaciones, String []insercion,  String []update, String []obs_validaciones, String []obs_insercion,  String formato, String tabla, String desc, String usuario) throws Exception{
        try {
            if (campos!=null){
                
                // modificacion en la tabla de parametros
                String [] paramsPar = { desc , usuario, "#FECHA#", formato  } ;
                this.EXECUTE_UPDATE( UPDATE_PAR2 , paramsPar  );
                
                
                // elimina toda la estructura de una tabla
                String [] paramsDel = { formato } ;
                this.EXECUTE_UPDATE( DELETE_EST , paramsDel  );
                
                
                // agrega la nueva estrucutura
                for (int i = 0 ; i < campos.length ; i++ ){
                    String [] paramsEst = { formato, tabla,  String.valueOf(i),  campos[i],  estructura[i],  def[i],  tdef[i], vdef[i], alias[i], validaciones[i], insercion[i], update[i], obs_validaciones[i],  obs_insercion[i], usuario, "#FECHA#", usuario, "#FECHA#" } ;
                    this.EXECUTE_UPDATE( INSERT_EST , paramsEst );
                }
                
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en rutina modificarEstructura [ImportacionesDAO]... \n"+e.getMessage());
        }
    }
    
    
    /**
     * agregar en importacion_parametros datos completos
     * @autor : Ing. Mario Fontalvo
     * @param tabla nombre de la tabla
     * @param descripcion descripcion de la tabla de importacion
     * @param ctab parametro de construccion del archivo y toma los siguientes valores
     *  -   A: nombre del archivo
     *  -   T: nombre de la tabla
     * @param fcon parametro de fecha de construccion del archivo y toma los siguientes valores
     *  - A: fecha del Archivo
     *  - S: fecha actual
     *  - N: no aplicar fecha
     * @param adat parametro que indica la actualizacion de datos del archivo de migracion
     *    -   A: adicionar datos al momento de subirlos
     *    -   E: eliminar los datos actuales antes de subir el archivo
     * @param bpna parametro de busqueda por nombre de archivo
     *    - I: inicia por el nombre del archivo
     *    - C: nombre completo
     * @param usuario usuario que registra el parametro de importacion
     * @throws Exception .
     * @see EXECUTE_UPDATE (int, String []).
     */
    
    
    public void agregarParametros   (String formato, String tabla, String descripcion, String ctab, String fcon, String adat, String bpna, String ins, String upd, String usuario) throws Exception{
        try {
            
            // llama a EXECUTE_UPDATE para que inserte los paramertros
            String [] paramsPar = { formato, tabla, descripcion, ctab, fcon, adat, bpna, ins, upd, usuario, "#FECHA#", usuario, "#FECHA#"  } ;
            this.EXECUTE_UPDATE( INSERT_PAR2 , paramsPar  );
            
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en rutina agregarParametros [ImportacionesDAO]... \n"+e.getMessage());
        }
    }
    
    
    
    /**
     * modifica los parametros de importacion
     * @autor : Ing. Mario Fontalvo
     * @see EXECUTE_UPDATE (int, String []).
     * @param titulo indica si el archivo de importacion pose o no titulo (S/N)
     *
     * @param evento Nombre del evento que manipulará la importacion, en caso de no definirlo
     * se encargara de procesarlo por default el mismo programa en general
     * @param tabla nombre de la tabla
     * @param descripcion descripcion de la tabla de importacion
     * @param ctab parametro de construccion del archivo y toma los siguientes valores
     *  -   A: nombre del archivo
     *  -   T: nombre de la tabla
     * @param fcon parametro de fecha de construccion del archivo y toma los siguientes valores
     *  - A: fecha del Archivo
     *  - S: fecha actual
     *  - N: no aplicar fecha
     * @param adat parametro que indica la actualizacion de datos del archivo de migracion
     *    -   A: adicionar datos al momento de subirlos
     *    -   E: eliminar los datos actuales antes de subir el archivo
     * @param bpna parametro de busqueda por nombre de archivo
     *    - I: inicia por el nombre del archivo
     *    - C: nombre completo
     * @param usuario usuario que registra el parametro de importacion
     * @throws Exception .
     */
    
    public void modificarParametros(String formato, String tabla, String descripcion, String ctab, String fcon, String adat, String bpna, String titulo, String evento, String ins, String upd, String usuario) throws Exception{
        try {
            
            // llamado del proc. general para que actulice los parametros
            String [] paramsPar = { descripcion , ctab, fcon, adat, bpna, titulo, evento, ins, upd, usuario, "#FECHA#", formato  } ;
            this.EXECUTE_UPDATE( UPDATE_PAR , paramsPar );
            
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en rutina modificarParametros [ImportacionesDAO]... \n"+e.getMessage());
        }
    }
    
    
    /**
     * rutina que elimina una tabla de
     * - importacion_parametros
     * - importacion_estructura
     * parametros de entrada un array de string donde
     * vienen todas los formatos que se deben borrar
     * @autor : Ing. Mario Fontalvo
     * @param tabla nombre de las tablas a eliminar
     * @throws Exception .
     */
    
    public void eliminarEstructura  (String [] formatos) throws Exception{
        
        try {
            if (formatos!=null){
                // recorrido de todas las tablas
                for (int i = 0; i < formatos.length ; i++ ){
                    String [] paramsDel = { formatos[i] } ;
                    // eliminacion de la estructura
                    this.EXECUTE_UPDATE( DELETE_EST , paramsDel  );
                    // eliminacion de los parametros
                    this.EXECUTE_UPDATE( DELETE_PAR , paramsDel  );
                }
            }
            
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en rutina eliminarEstructura [ImportacionesDAO]... \n"+e.getMessage());
        }
    }
    
    /**
     * funcion que devuelve un listado de todas la tablas existentes dentro de
     * - importacion_parametros
     * @autor : Ing. Mario Fontalvo
     * @throws Exception .
     * @return Listado general de tablas
     * @see EXECUTE_UPDATE (int, String [])
     */
    public List Listado( ) throws Exception{
        List lista = null;
        try {
            
            // llama a la funcion que ejecuta solo consultas,
            String [] paramsQuery = { "ALL" } ;
            lista =  this.EXECUTE_QUERY( SELECT_PAR , paramsQuery  );
            
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en rutina Listado [ImportacionesDAO]... \n"+e.getMessage());
        }
        return lista;
    }
    
    /**
     * funcion que busca una tabla especifica y devuelve los parametros
     * de imporacion mas su estructura
     * @autor : Ing. Mario Fontalvo
     * @param tabla nombre de la tabla a consultar
     * @throws Exception .
     * @return Objeto ParametrosImportacion que contiene la definicion de la tabla y sus campos
     * mas sus parametros de importacion en general
     * @see EXECUTE_UPDATE (int, String [])
     */
    
    public ImportacionParametros Buscar  ( String formato ) throws Exception{
        ImportacionParametros   datos = null;
        
        try {
            
            // primero busca los parametros
            String [] paramsQuery = { formato } ;
            List lista =  this.EXECUTE_QUERY( SELECT_PAR , paramsQuery  );
            
            // si lo encontro busca la estructura de la tabla
            if ( lista!=null && !lista.isEmpty() ){
                datos =  (ImportacionParametros) lista.get(0);
                datos.setListaCampos( this.EXECUTE_QUERY( SELECT_EST , paramsQuery  ) );
            }
            
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en rutina Buscar [ImportacionesDAO]... \n"+e.getMessage());
        }
        return datos;
    }
    
    
    
    /**
     * procediento que importa a la base de datos
     * parametros:
     *  listado con los datos generales de imporaction
     *  archivo para poder obtener las propiedades del mismo
     * @autor : Ing. Mario Fontalvo
     * @param listado listado de datos a importar
     * @param tabParams Parametros de Importacion de la tabla
     * @throws Exception .
     * @return Errores generados por la importacion
     * @see EXECUTE_UPDATE (int, String [])
     */
    
    
    public TreeMap ImportarInsert(List listado, ImportacionParametros tabParams ) throws Exception {
        
        // inicializacion de los variables de retorno
        TreeMap result = new TreeMap();
        
        // inicializacion de los variables de control de
        // errores
        List err = new LinkedList();
        List rnm = new LinkedList();
        
        
        // despues de obtener la definicion de la estructura se procesa la
        // lista contra la estructura de insert obtenido
        Connection        conexion    = this.conectarBDJNDI(this.getDatabaseName());//JJCastro fase2
        PreparedStatement st          = null;
        
        
        if (conexion == null)
            throw new SQLException("Sin conexion");
        try {
            
            // recorre el listado para al final procesarlo
            // setea el insert general
            st = conexion.prepareStatement(tabParams.getConsulta());
            
            List lista = new LinkedList( listado );
            Iterator it = lista.iterator();
            int linea = (tabParams.getTitulo().equals("S")?1:0);
            while (it.hasNext()) {
                linea++;
                Object [] datos = (Object []) it.next();
                String status  = (String   ) datos[0];
                String [] dato = (String []) datos[1];
                
                try{                    
                    if (!status.equalsIgnoreCase("ok"))
                        throw new Exception (status); 
                    
                    // inicializacion del estamento
                    st.clearParameters();
                    
                    // seteo de los parametros
                    for ( int i = 0 ; i < dato.length ; st.setString( i+1 ,  dato[i++] ));
                    
                    //ejecucion de la consulta
                    st.executeUpdate();
                    
                }catch (Exception ex){
                    err.add("LINEA ["+ linea +"] " + ex.getMessage().replaceAll("\n",""));
                    rnm.add(dato);
                }
            }
            
            if (!err.isEmpty()){
                result.put("err" , err);
                result.put("rnm" , rnm);
            }
        }
        catch (Exception e){
            e.printStackTrace();
            throw new Exception("Error en rutina ImportarInsert [ImportarDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conexion != null){ try{ this.desconectar(conexion); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return result;
        
    }
    
    

    /**
     * procediento que importa a la base de datos
     * parametros:
     *  listado con los datos generales de imporaction
     *  archivo para poder obtener las propiedades del mismo
     * @autor : Ing. Mario Fontalvo
     * @param listado listado de datos a importar
     * @param tabParams Parametros de Importacion de la tabla
     * @throws Exception .
     * @return Errores generados por la importacion
     * @see EXECUTE_UPDATE (int, String [])
     */
    
    
    public TreeMap ImportarUpdate(List listado, ImportacionParametros tabParams ) throws Exception {
        
        // inicializacion de los variables de retorno
        TreeMap result = new TreeMap();
        
        // inicializacion de los variables de control de
        // informacion de ejecucion de las actualizaciones
        List info = new LinkedList();
        List reg  = new LinkedList();
        
        
        // despues de obtener la definicion de la estructura se procesa la
        // lista contra la estructura de insert obtenido

        Connection        conexion    = this.conectarBDJNDI(this.getDatabaseName());//JJCastro fase2
        PreparedStatement st          = null;
        
        
        if (conexion == null)
            throw new SQLException("Sin conexion");
        try {
            
            // recorre el listado para al final procesarlo
            // setea el insert general
            st = conexion.prepareStatement(tabParams.getConsulta());
            
            List lista = new LinkedList( listado );
            Iterator it = lista.iterator();
            int linea = 1;
            while (it.hasNext()) {
                linea++;
                Object [] datos = (Object []) it.next();
                String status   = (String   ) datos[0];
                String [] dato  = (String []) datos[1];
                int numFilasAfectadas = 0;
                try{
                    
                    if (!status.equals("ok"))
                        throw new Exception( status );
                    
                    // inicializacion del estamento
                    st.clearParameters();
                    
                    // seteo de los parametros
                    for ( int i = 0 ; i < dato.length ; st.setString( i+1 ,  dato[i++] ));
                    
                    //ejecucion de la consulta
                    numFilasAfectadas = st.executeUpdate();
                    info.add("LINEA ["+ linea +"] La ejecucion de esta actualizacion, afecto " +  numFilasAfectadas + " registro(s) en la base de datos");
                    
                } catch (Exception ex) {
                    info.add("LINEA ["+ linea +"] " + ex.getMessage().replaceAll("\n",""));
                    
                } finally {
                    reg.add(dato);
                }
                
                if (!reg.isEmpty()){
                    result.put("err" , info);
                    result.put("rnm" , reg);
                }
            }

        }
        catch (Exception e){
            e.printStackTrace();
            throw new Exception("Error en rutina ImportarInsert [ImportarDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conexion != null){ try{ this.desconectar(conexion); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return result;
        
    }
    
    
    /**
     * Procedimento que genera la estructura del insert de la tabla
     * en caso de no encontrar ninguan estructura  este procedimiento
     * creara la tabla, y en ultimo lugar si no tiene registro de estos
     * arroja un error al no econtrar la estructura
     * @autor : Ing. Mario Fontalvo
     * @param archivo Nombre del archivo de Importacion
     * @throws Exception .
     * @return Parametros de Importacion de una tabla
     * @see EXECUTE_UPDATE (int, String [])
     */
    
    public ImportacionParametros  prepararInsert( File archivo, String usuario  ) throws Exception {
        Connection  conexion    =null;
        Statement   st          = null;
        
        ImportacionParametros tabParams = null;
        
        
        // estrucutra que sera devuelta al fian
        String   insert = " insert into  #tabla#  ( #campos# ) values ( #values# ) ";
        
        conexion = this.conectarBDJNDI(this.getDatabaseName());//JJCastro fase2
        if (conexion == null)
            throw new SQLException("Sin conexion");
        
        try {
            
            st = conexion.createStatement();
            
            // datos del archivo
            SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
            
            int pos = archivo.getName().lastIndexOf(".");
            String narc = (pos!=-1? archivo.getName().substring(0,pos) : "");
            java.util.Date   darc = new java.util.Date( archivo.lastModified() );
            
            
            //////////////////////////////////////////////////////////////////////
            // busqueda de los campos en la base de datos
            String ntab   = "";
            
            
            // seleccion tabla mas adecuada
            List ltma    = EXECUTE_QUERY( SELECT_TMA, new String [] { narc , narc } );
            if ( ltma != null && ltma.size() > 0 ) ntab = ltma.get(0).toString();
            
            
            // si no hay ninguna tabla una excepcion y rompe la importacion
            if ( ntab.equals("") )
                throw new Exception("Error, no encontro definido parametros de Importacion para el archivo " + archivo);
            
            
            
            
            // busqueda de parametros de importacion
            tabParams = Buscar( ntab );
            
            
            // si tiene parametros de importacion
            if ( tabParams != null ){
                
                
                if ( !verificarUsuario(tabParams.getFormato(), usuario) ) 
                    throw new Exception("Permiso denegado para el usuario "+ usuario.toUpperCase() +". no puede trabajar con el formato " + tabParams.getFormato().toUpperCase());
                
                
                //construccion del nombre de la tabla
                String ctab = ( tabParams.getCTAB().equals("A") ? narc : tabParams.getTabla() );
                
                ctab += ( tabParams.getFCON().equals("A")
                ? fmt.format(darc)                    // fecha del archivo
                : tabParams.getFCON().equals("S")
                ? fmt.format(new java.util.Date())    // fecha del sistema
                : "" );
                
                
                // crea la estructura en base a la tabla de importacion
                List lcampos = tabParams.getListaCampos();                
                if (lcampos!=null && lcampos.size()>0){
                    
                    // verificamos si se pueden eliminar los datos de la tabla
                    String delete = "";
                    if ( tabParams.getADAT().equals("E") )
                        delete = " delete from "+ ctab;
                    else if (tabParams.getADAT().equals("U"))
                        delete = " delete from "+ ctab + " where creation_user = '" + usuario + "'";
                        
                    if (!delete.equals(""))
                        st.execute( delete );
                }
                
                String campos = "  ";
                String values = "  ";
                
                for (int i = 0; i < lcampos.size(); i++) {
                    ImportacionEstructura cmp = (ImportacionEstructura) lcampos.get(i);
                    campos += cmp.getCampo() + ", ";
                    switch (cmp.getCampo()) {
                        case "fecha_documento":
                            values += "?::date, ";
                            break;
                        case "total_debito":
                            values += "?::numeric, ";
                            break;
                        case "total_credito":
                            values += "?::numeric, ";
                            break;
                        case "debito":
                            values += "?::numeric, ";
                            break;
                        case "credito":
                            values += "?::numeric, ";
                            break;
                        case "creation_date":
                            values += "?::date, ";
                            break;
                        case "total_foraneo":
                            values += "?::numeric, ";
                            break;
                        case "valor_foraneo":
                            values += "?::numeric, ";
                            break;
                        case "fecha_vencimiento":
                            values += "?::timestamp without time zone, ";
                            break;  
                        case "valor_factura":
                            values += "?::numeric, ";
                            break; 
                        case "saldo_fac_excel":
                            values += "?::numeric, ";
                            break;  
                        case "valor_recaudo":
                            values += "?::numeric, ";
                            break;   
                        case "saldo_recaudo":
                            values += "?::numeric, ";
                            break; 
                        case "valor":
                            values += "?::numeric, ";
                            break;
                        case "fecha_consignacion":
                            values += "?::date, ";
                            break;
                        case "fecha":
                            values += "?::date, ";
                            break;
                        case "intereses ":
                            values += "?::numeric, ";
                            break;
                        default:
                            values += "?, ";
                    }
                    
                    //values     += "? , ";
                    //estructura += cmp.getCampo() + " " + cmp.getTipo() + " " + cmp.getExtra() + ", ";
                }
                
                
                // se termina el proceso de construccion del insert
                
                campos = campos.substring(0, campos.length() - 2 );
                values = values.substring(0, values.length() - 2 );
                
                insert = insert.replaceAll("#tabla#" , ctab  )
                .replaceAll("#campos#", campos)
                .replaceAll("#values#", values);
                
                tabParams.setConsulta(insert);
                
            }
            
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new SQLException("Error en rutina  prepararInsert  [ImportarDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conexion != null){ try{ this.desconectar(conexion); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return tabParams;
        
    }
    
    
    
    /**
     * Procedimento que genera la estructura del insert de la tabla
     * en caso de no encontrar ninguan estructura  este procedimiento
     * creara la tabla, y en ultimo lugar si no tiene registro de estos
     * arroja un error al no econtrar la estructura
     * @autor : Ing. Mario Fontalvo
     * @param archivo Nombre del archivo de Importacion
     * @throws Exception .
     * @return Parametros de Importacion de una tabla
     * @see EXECUTE_UPDATE (int, String [])
     */
    
    public ImportacionParametros  prepararUpdate( String archivo, String []titulos, String usuario  ) throws Exception {

        ImportacionParametros tabParams = null;
        
        // estrucutra que sera devuelta al fian
        String update = "update #tabla# set #campos# #restricciones# ";      
        
        try {
            
            // nombre del archivo
            int pos = archivo.lastIndexOf(".");
            String narc = (pos!=-1? archivo.substring(0,pos) : "");
           
            
            // seleccion tabla mas adecuada
            String ntab = "";
            List ltma    = EXECUTE_QUERY( SELECT_TMA, new String [] { narc , narc } );
            if ( ltma != null && ltma.size() > 0 ) ntab = ltma.get(0).toString();
            
            
            // si no haya ninguna tabla una excepcion y rompe la importacion
            if ( ntab.equals("") )
                throw new Exception("Error, no encontro definido parametros de Importacion para el archivo " + archivo);
            

            // busqueda de parametros de importacion
            tabParams = Buscar( ntab );
            
            // si tiene parametros de importacion
            if ( tabParams != null ){
                
                
                if ( !verificarUsuario(tabParams.getFormato(), usuario) ) 
                    throw new Exception("Permiso denegado para el usuario "+ usuario.toUpperCase() +". no puede trabajar con el formato " + tabParams.getFormato().toUpperCase());
                
   
                // busqueda de llaves primarias
                List llaves = this.obtenerLLavesPrimarias( tabParams.getTabla() );
                if (llaves == null || llaves.isEmpty())
                    throw new Exception ("Para poder realizar las modificaciones se quiere que la tabla contenga una llave primaria.");
                            
                
                // marcacion de los campos que son llaves primarias
                List lcampos = tabParams.getListaCampos();
                int numllaves = 0;
                for ( int i = 0; i< lcampos.size() ; i++){
                    ImportacionEstructura cmp = (ImportacionEstructura) lcampos.get(i);
                    for (int j=0; j< llaves.size(); j++)
                        if (  cmp.getCampo().trim().equalsIgnoreCase(((String) llaves.get(j)).trim())  ){
                            cmp.setPrimaryKey(true);
                            numllaves++;
                            break;
                        }
                }           
                
                // marcacion de los campos que vienen en el archivo
                for ( int i = 0; i< lcampos.size() ; i++){
                    ImportacionEstructura cmp = (ImportacionEstructura) lcampos.get(i);
                    cmp.setSecuencia(String.valueOf(-1));
                    
                    // se agrego solo el if para que tome solo los campos de actualizacion
                    if ( cmp.isAplicaUpdate() ){
                        for (int j=0; j<titulos.length; j++){
                            if ( titulos[j] != null && cmp.getAlias().trim().equalsIgnoreCase(titulos[j].trim())  ){
                                cmp.setSeleccionado(true);
                                cmp.setSecuencia( String.valueOf( j ) );
                                break;
                            }
                        }
                    }
                }
                
                
                // campos a actualizar
                StringBuffer campos      = new StringBuffer();
                StringBuffer primaryKeys = new StringBuffer();
                for ( int i = 0; i< lcampos.size() ; i++){
                    ImportacionEstructura cmp = (ImportacionEstructura) lcampos.get(i);
                    if ( cmp.isSeleccionado() ){
                        if ( !cmp.isPrimaryKey() )
                            campos.append(  cmp.getCampo() + " = ? , "  );
                        else
                            primaryKeys.append(  (primaryKeys.toString().equals("")?"" :" and ") + cmp.getCampo() + " = ? "  );
                    }
                    else if (cmp.isPrimaryKey()){
                        throw new Exception ("Faltan campos de llaves primarias en el archivo de importacion. las llaves son : " + llaves.toString().replaceAll("[|]","") );
                    }
                }
                
                if (!campos.toString().equals("")){
                    campos.replace( campos.lastIndexOf(", "), campos.lastIndexOf(", ") + 1 , "");
                } else
                    throw new Exception ("no existen campos en el archivo para actualizar");
                
                update = update.replaceAll("#tabla#"        , tabParams.getTabla())
                               .replaceAll("#campos#"       , campos.toString())
                               .replaceAll("#restricciones#", (primaryKeys.toString().trim().equals("")?"":" where " +  primaryKeys.toString() ) );
                tabParams.setConsulta( update );
                
                ////System.out.println("update : " + update);
            }
            
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en rutina  prepararUpdate  [ImportarDAO]... \n"+e.getMessage());
        }
        return tabParams;
        
    }
    
    
    /**
     * Procedimiento que incluye una tabla de la base de datos como una tabla de
     * importacion
     * @autor : Ing. Mario Fontalvo
     * @param ntabla nombre de la tabla
     * @param usuario usuario que realiza el proceso
     * @throws Exception .
     * @return boolean que indica si se pudo o no incluir la tabla como una tabla de importacion
     * @see EXECUTE_UPDATE (int, String [])
     */
    public boolean Incluir(String formato, String ntabla, String usuario) throws Exception{
        Connection        Conexion    = null;//JJCastro fase2
        boolean           result      = false;

        Conexion = this.conectarBDJNDI(this.getDatabaseName()); //JJCastro fase2
        if (Conexion == null)
            throw new SQLException("Sin conexion");
        try {
            
            String schema, table;
            String []tab = ntabla.split("\\.");
            
            if (tab.length==2){
                schema = tab[0];
                table  = tab[1];
            }
            else{
                schema = "public";
                table  = ntabla;
            }
            
            
            // Listamos sólo tablas
            String tipos[] = new String [] { "TABLE" };
            DatabaseMetaData dbmd   = Conexion.getMetaData();
            ResultSet        tablas = dbmd.getTables( null, schema, table , tipos );
            
            if ( tablas.next() ) {
                
                String    tabla    = tablas.getString( tablas.findColumn( "TABLE_NAME" ) );
                ResultSet columnas = dbmd.getColumns(null, schema, tabla, null);
                
                
                
                columnas.last();
                int total = columnas.getRow();
                columnas.beforeFirst();
                int i;
                
                String [] campo = new String [total];
                String [] estru = new String [total];
                String [] def   = new String [total];
                String [] tdef  = new String [total];
                String [] vdef  = new String [total];
                String [] alias = new String [total];
                String [] validaciones = new String [total];
                String [] inserciones = new String [total];
                String [] update      = new String [total];
                
                for (i=0;i<total;i++){
                    campo [i] = "";
                    estru [i] = "";
                    def   [i] = "N";
                    tdef  [i] = "";
                    vdef  [i] = "";
                    alias [i] = "";
                    validaciones [i] = "";
                    inserciones  [i] = "";
                    update       [i] = "N";
                }
                
                i=0;
                while(columnas.next()){
                    
                    campo [i] = columnas.getString("COLUMN_NAME");
                    alias [i] = columnas.getString("COLUMN_NAME");
                    estru [i] =
                    
                    com.tsp.util.UtilFinanzas.Trunc(
                        Util.descripcionTipoCampo(Integer.parseInt(columnas.getString("DATA_TYPE"))) + " "+
                        (columnas.getString("IS_NULLABLE").equals("NO")?"not null":"null") + " " +
                        (columnas.getString("COLUMN_DEF")==null?"":"default "+columnas.getString("COLUMN_DEF") )
                    , 200);
                    i++;
                    
                }
                if (total>0){
                    agregarEstructura(campo, estru, def, tdef, vdef, alias, validaciones, inserciones, update, validaciones, inserciones ,formato, ntabla, "" , usuario);
                    result = true;
                }
                
            };
            
        }
        catch(Exception e) {
            throw new Exception("Error en rutina Incluir [ImportacionDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (Conexion != null){ try{ this.desconectar(Conexion); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return result;
    }
    
    
    /**
     * Procedimiento que incluye una tabla de la base de datos como una tabla de
     * importacion
     * @autor : Ing. Mario Fontalvo
     * @param ntabla nombre de la tabla
     * @throws Exception .
     * @return boolean que indica si se pudo o no incluir la tabla como una tabla de importacion
     * @see EXECUTE_UPDATE (int, String [])
     */
    public List obtenerLLavesPrimarias( String ntabla ) throws Exception{
        Connection        Conexion    = null;
        List              result      = new LinkedList();


        Conexion = this.conectarBDJNDI(this.getDatabaseName());//JJCastro fase2
        if (Conexion == null)
            throw new SQLException("Sin conexion");
        try {
            
            String schema, table;
            String []tab = ntabla.split("\\.");
            
            if (tab.length==2){
                schema = tab[0];
                table  = tab[1];
            }
            else{
                schema = null;
                table  = ntabla;
            }
            
            // Listamos sólo tablas
            String tipos[] = new String [] { "TABLE" };
            DatabaseMetaData dbmd   = Conexion.getMetaData();
            ResultSet        tablas = dbmd.getPrimaryKeys( null, schema, table );
            
            while ( tablas.next() ) {
                result.add( tablas.getString( "COLUMN_NAME" ) );
            }
            
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en rutina Incluir [ImportacionDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (Conexion != null){ try{ this.desconectar(Conexion); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return result;
    }    
    
    /**
     * Metodo para extraer el listado general de tablas de la base de datos
     * @autor mfontalvo
     * @fecha 2006-01-28
     * @return TreeMap seteado con el listado de tablas general de la base de
     * @throws Exception .
     */
    
    public TreeMap ListarTablas() throws Exception {
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        TreeMap           result = new TreeMap();
        

        Connection  conexion  = this.conectarBDJNDI(this.getDatabaseName());//JJCastro fase2
        if (conexion == null)
            throw new SQLException("Sin conexion");
        try {
            st = conexion.prepareStatement(SQL_TABLAS);
            rs = st.executeQuery();
            while(rs.next()){
                result.put(
                rs.getString("schemaname") + "." + rs.getString("tablename"),
                rs.getString("schemaname") + "." + rs.getString("tablename")
                );
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en rutina ListarTablas [ImportacionDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conexion != null){ try{ this.desconectar(conexion); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return result;
    }
    
    
    /**
     * Metodo para verificar si un usuario puede trabajar con un formato.
     * @autor mfontalvo
     * @fecha 2007-01-29
     * @throws Exception .
     */
    public boolean verificarUsuario(String formato, String usuario) throws Exception {
        
        if (usuario==null || usuario.trim().equals(""))
            return false;

        
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        boolean           usuarioValido = false;
        String query = "SQL_USUARIO_VALIDO";//JJCastro fase2
        Connection  conexion  = this.conectarJNDI(query);//JJCastro fase2
        if (conexion == null)
            throw new SQLException("Sin conexion");
        try {
            st = conexion.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, formato );
            rs = st.executeQuery();
            if (rs.next()){
                String usuarios = Util.coalesce(rs.getString("dato"),"").replaceAll(" ","");
                if (usuarios.toUpperCase().indexOf(","+usuario.toUpperCase()+",")!=-1){
                    usuarioValido = true;
                }
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en rutina verificarUsuario [ImportacionDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conexion != null){ try{ this.desconectar(conexion); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return usuarioValido;
    }
    
    
    /**
     * Metodo de verifiacion de permiso del usuario sobre un formato
     * @utor mfontalvo
     * @throws Exception.
     * @return msg, mensaje que indica el estado inicial del proceso.
     */
    public String usuarioValido(String archivo, String usuario, String tipo) throws Exception{
        String msg = "OK";
        try{
            
            // nombre del archivo
            int pos = archivo.lastIndexOf(".");
            String narc = (pos!=-1? archivo.substring(0,pos) : "");
           
            
            // seleccion tabla mas adecuada
            String ntab = "";
            List ltma    = EXECUTE_QUERY( SELECT_TMA, new String [] { narc , narc } );
            if ( ltma != null && ltma.size() > 0 ) ntab = ltma.get(0).toString();
            
            
            // si no haya ninguna tabla una excepcion y rompe la importacion
            if ( ntab.equals("") )
                msg = "Error, no encontro definido parametros de Importacion para el archivo " + archivo;
                     
            else {
                if (!verificarUsuario(ntab, usuario))
                    msg = "Error, permiso denegado, el usuario no tiene acceso al formato requerido.";
            }
            
            
            ImportacionParametros formato = this.Buscar(ntab) ;
            if ( formato!=null ){
                if (tipo.equalsIgnoreCase("Insercion") && !formato.getInsert().equals("S") ){
                    msg = "Error, este formato no permite INSERCIONES en la base de datos.";
                } else if (tipo.equalsIgnoreCase("Actualizacion") && !formato.getUpdate().equals("S") ){
                    msg = "Error, este formato no permite ACTUALIZACIONES en la base de datos.";
                }
            }
            
            
        } catch (Exception ex){
            throw new Exception (ex.getMessage());
        }
        return msg;
    }
    
    
    public String obtenerDato (String sql) throws Exception { 
        Connection        Conexion    = null;
        PreparedStatement ps          = null;   
        ResultSet         rs          = null;
        String            dato        = null;
        Conexion = this.conectarBDJNDI(this.getDatabaseName());//JJCastro fase2
        if (Conexion == null)
            throw new SQLException("Sin conexion");
        try {
            ps = Conexion.prepareStatement(sql);
            rs = ps.executeQuery();
            while ( rs.next() ) {
                dato = rs.getString( 1 );
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en obtenerDato [ImportacionDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (Conexion != null){ try{ this.desconectar(Conexion); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return dato;        
    }
        
}
