/*
 * ProductosDAO.java
 *
 * Created on 27 de julio de 2009, 8:42
 */

package com.tsp.operation.model.DAOS;


import java.sql.*;
import java.util.*;



/**
 *
 * @author  Rhonalf
 */
public class ProductosDAO extends MainDAO {
    /** Creates a new instance of ProductosDAO */
    public ProductosDAO() {
        super("ProductosDAO.xml");
    }


/**
 *
 * @return
 * @throws Exception
 */
    public ArrayList listarTodos() throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="LISTAR_TODOS";
        ArrayList resultado = new ArrayList();
        String cod="";
        String dsc="";
        String cad="";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs=st.executeQuery();
            while(rs.next()){
                cod=rs.getString("cod_producto");
                dsc=rs.getString("descripcion");
                cad=cod+";"+dsc;
                resultado.add(cad);
            }

            }}
        catch(Exception ec){
            throw new Exception("Error en la consulta listar todos los productos: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
           return resultado;
    }





/**
 *
 * @return
 * @throws Exception
 */
    private int getMaxProd() throws Exception {
        int max = 0;
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="GENERAR_CONS";
        String cod="";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs=st.executeQuery();
            while(rs.next()){
                cod=rs.getString("cod_producto");
            }
            max = Integer.parseInt(cod.substring(2));
            
            }}
        catch(Exception ec){
            throw new Exception("Error en la consulta listar todos los productos: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return max;

    }


/**
 * 
 * @return
 * @throws Exception
 */
    public ArrayList verTodos() throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="VER_TODOS";
        ArrayList resultado = new ArrayList();
        String cod="";
        String dsc="";
        String cad="";
        String pr="";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs=st.executeQuery();
            while(rs.next()){
                cod=rs.getString("cod_producto");
                dsc=rs.getString("descripcion");
                pr=rs.getString("precio");
                cad=cod+";"+dsc+";"+pr;
                resultado.add(cad);
            }
            
        }}
        catch(Exception ec){
            throw new Exception("Error en la consulta listar todos los productos: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resultado;
    }


/**
 * 
 * @param codigo
 * @return
 * @throws Exception
 */
    public ArrayList listarProovedoresPara(int codigo) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="LISTAR_PROVEEDORES_PRECIO";
        ArrayList resultado = new ArrayList();
        String sd = "";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setInt(1,codigo);
            rs=st.executeQuery();
            while(rs.next()){
                sd=rs.getString(2)+";"+rs.getString(4)+";"+rs.getDouble(3);
                resultado.add(sd);
            }
            
            }}
        catch(Exception ec){
            throw new Exception("Error en la consulta listar proveedores para: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resultado;
    }


/**
 * 
 * @param descripcion
 * @param precio
 * @throws Exception
 */
    public void insertarProducto(String descripcion,double precio) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="INSERTAR_PROD";
        int rowCount=0;
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,descripcion);
            st.setDouble(2, precio);
            st.setString(3, this.contarProductos());
            rowCount=st.executeUpdate();
            if(rowCount>0) System.out.println("Todo ok...");
            else System.out.println("Oops...");
            }}
        catch(Exception ec){
            throw new Exception("Error en la consulta insertar producto: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    public int contarDigitos(int numero){
        int contador=0;
        while(numero>0){
            numero /=10;
            contador++;
        }
        return contador;
    }
    
    public String contarProductos_ex(){
        int contar=0;
        ArrayList rx = new ArrayList();
        String r="PR";
        try{
            rx = listarTodos();
            contar=rx.size()+1;
            int ndig = this.contarDigitos(contar);
            switch(ndig){
                case 0:
                    r="PR00001";
                break;
                case 1:
                    r +="0000"+contar;
                break;
                case 2:
                    r +="000"+contar;
                break;
                case 3:
                    r +="00"+contar;
                break;
                case 4:
                    r +="0"+contar;
                break;
                default:
                    r+=contar;
                break;
            }
        }
        catch(Exception e){
            System.out.println("Error en generacion de consecutivo: "+e.toString());
            e.printStackTrace();
        }
        return r;
    }

    public String contarProductos(){
        int contar=0;
        String r="PR";
        try{
            contar=this.getMaxProd()+1;
            int ndig = this.contarDigitos(contar);
            switch(ndig){
                case 0:
                    r="PR00001";
                break;
                case 1:
                    r +="0000"+contar;
                break;
                case 2:
                    r +="000"+contar;
                break;
                case 3:
                    r +="00"+contar;
                break;
                case 4:
                    r +="0"+contar;
                break;
                default:
                    r+=contar;
                break;
            }
        }
        catch(Exception e){
            System.out.println("Error en generacion de consecutivo: "+e.toString());
            e.printStackTrace();
        }
        return r;
    }


/**
 * 
 * @param codigo
 * @throws Exception
 */
    public void anularProducto(String codigo) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="ANULAR_PROD";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,codigo);
            st.executeUpdate();
            } }
        catch(Exception ec){
            throw new Exception("Error en la consulta anular producto: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    private String opcion(int tipo){
        String txt="";
        switch(tipo){
            case 1:
              txt="cod_producto";
            break;
            case 2:
              txt="descripcion";
            break;
            case 3:
              txt="precio";
            break;
            default:
              txt="cod_producto";
            break;
        }
        return txt;
    }



    public ArrayList buscarPor(int filtro,String cadena) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="BUSCAR_FILTRO";
        ArrayList resultado = new ArrayList();
        String cod="";
        String dsc="";
        String cad="";
        String pr="";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                String sql = obtenerSQL(query);
                st = con.prepareStatement(sql.replaceAll("#PARAM", this.opcion(filtro)));
                st.setString(1, "%" + cadena + "%");
                rs = st.executeQuery();
                while (rs.next()) {
                    cod = rs.getString("cod_producto");
                    dsc = rs.getString("descripcion");
                    pr = rs.getString("precio");
                    cad = cod + ";" + dsc + ";" + pr;
                    resultado.add(cad);
                }

            }
        }
        catch(NullPointerException ec){
            throw new NullPointerException("No hay resultado ..." + ec.toString());
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta buscar por: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
            return resultado;
    }

}
