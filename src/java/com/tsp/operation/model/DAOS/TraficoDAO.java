/********************************************************************
 *      Nombre Clase.................   TraficoDAO.java
 *      Descripci�n..................   lase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.LogWriter;

/**
 *
 * @author  David
 */
public class TraficoDAO extends MainDAO {
    
    private PrintWriter pw;
    private LogWriter logWriter;
    
    private Ingreso_Trafico trafic;
    private Ingreso_Trafico ingreso;
    private Vector vecTrafico;
    private Vector vecCampo;
    private Vector vecZonas;
    private Vector registros;
    private List traficoDetallado;
    
    private static final String SQL_OBTENER_TRAFICO =
            "SELECT " +
            "       COALESCE(z.desczona, 'NR') AS nomzona, " +
            "       c1.* " +
            "FROM( " +
            "   SELECT " +
            "           COALESCE(get_nombrenit(cedcon),'NR') AS nomcond, " +
            "           COALESCE(get_nombrenit(cedprop),'NR') AS nomprop, " +
            "           COALESCE(get_nombreciudad(origen),'NR') AS nomorigen, " +
            "           COALESCE(get_nombreciudad(destino),'NR') AS nomdestino, " +
            "           COALESCE(get_nombreciudad(pto_control_ultreporte),'NINGUNO') AS nompto_control_ultreporte, " +
            "           COALESCE(get_nombreciudad(pto_control_proxreporte),'NINGUNO') AS nompto_control_proxreporte, " +
            "           * " +
            "   FROM " +
            "           trafico " +
            "   WHERE " +
            "           planilla=?) AS c1 " +
            "LEFT JOIN zona z ON ( c1.zona = z.codzona ) ";
    private static final String ZONAS_POR_U="select zona,desczona from zonausuario as zu,zona as z where upper(zu.nomusuario)=? and zu.zona=z.codzona ";
    private static final String VZONAS="select zona from zonausuario where upper(nomusuario)=?";
    private static final String SQL_OBTENER_SECUENCIA="select via from via where origen=? and destino=? and secuencia=?";
    private static final String LISTAR_TRAFICO    = "SELECT * FROM trafico";
    private static final String ZONA_EN_I_TRAFICO="select nomzona  from ingreso_trafico where planilla=?";
    private static final String PLANILLA_ID="select planilla from ingreso_trafico where oid=?";
    private static final String ACTUALIZAR_I_TRAFICO_ZONA="update ingreso_trafico set nomzona='SEGURIDAD',zona='005' where oid=?";
    private static final String ACTUALIZAR_MOV_T="update ingreso_trafico set nomzona='SEGURIDAD',zona='005' where planilla=? and tipo_despacho <> 'v'";
    private static final String ACTUALIZAR_T="update trafico set nomzona='SEGURIDAD',zona='005' where planilla=? and tipo_despacho <> 'v'";
    private static final String OB_N_CARAVANA="select numcaravana from caravana where numpla=?";
    private static final String TURNOS_POR_USUARIO="select zona from turnos where (usuario_turno=? and fecha_turno=? and h_entrada <=? and h_salida >=?) or (usuario_turno=? and fecha_turno='0099-01-01') ";
    private static final String ZONAS_POR_U_POR_TURNO="select zona,desczona from zonausuario as zu,zona as z where upper(zu.nomusuario)=? and zu.zona=z.codzona and z.codzona=?";
    //Tito Andr�s 03.10.2005
    //Tito Andr�s 03.10.2005
    private static final String SQL_OBTENER = "select * from ingreso_trafico where planilla=?";
   
    
    
    private static final String SQL_ESCOLTAS_VEH = 
            "SELECT array_to_string(array_accum(placa || ' (' || COALESCE(get_nombreciudad(codorigen),'NR') || ' - ' || COALESCE(get_nombreciudad(coddestino), 'NR') || ')') , ', ') AS esc  " +
            "FROM escolta_vehiculo  " +
            "WHERE numpla = ? AND estado!='L' AND reg_status!='A' ";
    
    private static final String SQL_ESCOLTAS_CAR = 
            "SELECT array_to_string(array_accum(placa || ' (' || COALESCE(get_nombreciudad(codorigen),'NR') || ' - ' || COALESCE(get_nombreciudad(coddestino), 'NR') || ')') , ', ') " +
            "FROM caravana car, escolta_caravana ecar " +
            "WHERE car.numcaravana = ecar.numcaravana AND ecar.estado!='L' AND car.reg_status!='A' AND car.numpla = ?";
    
    private static final String SQL_CARAVANA = 
            "SELECT array_to_string(array_accum(numcaravana) , ', ') " +
            "FROM caravana car " +
            "WHERE numpla = ? AND reg_status!='A'";
        //////////////// AMATURANA
    private static final String SQL_PLACA_ITRAFICO = "SELECT * FROM ingreso_trafico WHERE placa = ? ORDER BY fecha_salida DESC";
    
    /** Creates a new instance of TraficoDAO */
    public TraficoDAO() {
        super("TraficoDAO.xml");
        try{
            /**************************************************************************/
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String ruta = rb.getString("ruta")+ "/exportar/migracion/KREALES";
            
            File file = new File(ruta);
            file.mkdirs();
            
            pw = new PrintWriter(System.err, true);
            
            String logFile = ruta + "/ACTUALIZAR_ZONA.log";
            
            pw = new PrintWriter(new FileWriter(logFile, true), true);
            logWriter = new LogWriter("ACTUALIZAR_ZONA", LogWriter.INFO, pw);
            logWriter.setPrintWriter(pw);
            /****************************************************************************/
        }catch (IOException e){
        }
    }
    
    public Vector obtVecTrafico() {
        return vecTrafico;
    }
    
    public Vector obtVectorCampo() {
        return vecCampo;
    }
    
    public Vector obtVectorZonas() {
        return vecZonas;
    }
    
    /**
     * Este m�todo retorna un String con los codigos de zonas a los que puede acceder un usuario en una fecha y hora determindas del registro de turnos
     * @param String user
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public String zonasDeTurnos(String user) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String zonas="";
        java.util.Date utilDate = new java.util.Date(); //fecha actual
        long lnMilisegundos = utilDate.getTime();
        java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(lnMilisegundos);
        String fecha=sqlTimestamp+"";
        fecha=fecha.substring(0,11);
        String hora=sqlTimestamp+"";
        hora=hora.substring(11,19);
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.TURNOS_POR_USUARIO);
                st.setString(1, user);
                st.setString(2,fecha);
                st.setString(3,hora);
                st.setString(4, hora);
                st.setString(5, user);
                rs = st.executeQuery();
                //////System.out.println("CONSULTA "+st);
                while (rs.next()) {
                    zonas=rs.getString("zona");
                }
            }
        }catch(SQLException e) {
            throw new SQLException("zon" + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        return zonas;
    }
    
    
    /**
     * metodo que retorna un Vector con las zonas asignadas a un usuario en la tabla zonausuarios
     * @param String idUsuario
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void gennerarVZonas(String idUsuario) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        vecTrafico = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(VZONAS);
                st.setString(1, idUsuario);
                vecZonas = new Vector();
                rs = st.executeQuery();
                
                while (rs.next()) {
                    vecZonas.add(rs.getString("zona"));
                }
            }
        }catch(SQLException e) {
            throw new SQLException("generar zona" + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        
    }

    /**
     * Este m�todo que retorna un Vector con la informacion completa de ingreso_trafico
     *
     * @param numpla Numero de planilla/OC a la que se quiere consultar trafico
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void listarTrafico() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        vecTrafico = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(LISTAR_TRAFICO);
                
                vecTrafico = new Vector();
                rs = st.executeQuery();
                
                while (rs.next()) {
                    vecTrafico.add(Trafico.load(rs));
                }
            }
        }catch(SQLException e) {
            throw new SQLException("listar tr" + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        
    }
    
    
    /**
     * Este m�todo que retorna un Vector con la informacion de ingreso trafico dadas las zonas
     * que tiene asignadas un usuario
     * @param String campo,String validacion
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public Vector generarCampos(String campo,String validacion) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        vecCampo = null;
        String cons="select distinct("+campo+") from ingreso_trafico where "+validacion+"order by "+campo;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(cons);
                
                vecCampo = new Vector();
                rs = st.executeQuery();
                
                while (rs.next()) {
                    Vector fila = new Vector();
                    fila.add(campo);
                    fila.add(rs.getString(campo));
                    vecCampo.add(fila);
                }
            }
        }catch(SQLException e) {
            throw new SQLException("generar campos" + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        return this.vecCampo;
    }
    
    /**
     * Este m�todo que retorna  el nombre de la zona de un registro en ingrso trafico dado la planilla
     * que tiene asignadas un usuario
     * @param String campo(Planilla),Connection con
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public String  encontrarZona(String campo,Connection con) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        vecCampo = null;
        String zona="";
        try {
            if (con != null) {
                st = con.prepareStatement(ZONA_EN_I_TRAFICO);
                st.setString(1, campo);
                vecCampo = new Vector();
                rs = st.executeQuery();
                
                while (rs.next()) {
                    zona =(rs.getString("nomzona"));
                }
            }
        }catch(SQLException e) {
            throw new SQLException("encontar zona" + e.getMessage()+"" + e.getErrorCode());
        }
        return zona;
    }
    
    
    /**
     * Este m�todo que retorna  el numero de la planilla dado el oid del registro
     *
     * @param String id
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public String  getNumeroPlanilla(String id) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        vecCampo = null;
        String planilla="";
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(PLANILLA_ID);
                st.setString(1, id);
                vecCampo = new Vector();
                rs = st.executeQuery();
                
                while (rs.next()) {
                    planilla =(rs.getString("planilla"));
                }
            }
        }catch(SQLException e) {
            throw new SQLException("get numpla" + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        return planilla;
    }
    
    /**
     * Este m�todo que actualiza las zonas en ingreso trafico a zonas de
     * de seguridad dado el oid del registro
     * @param String campo(oid en el registro)
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void actualizarZona(String campo) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        vecCampo = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(ACTUALIZAR_I_TRAFICO_ZONA);
                st.setString(1,campo);
                st.executeUpdate();
            }
        }catch(SQLException e) {
            throw new SQLException("actualizar zona" + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        
    }
    /**
     * Este m�todo que actualiza los registros de ingreso_trafico cambiando a zona de seguridad
     * aquellos que esten retrasados mas de una hora
     * @param String filtro,java.sql.Timestamp fechaActual
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void actualizarZonas(String filtro,java.sql.Timestamp fechaActual) throws SQLException, IOException {
        Connection con= null;
        PreparedStatement st = null;
        PreparedStatement st1 = null;
        //PreparedStatement st2 = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        vecCampo = null;
        
        
        
        String CONSULTA = "select planilla,fecha_prox_reporte,tipo_despacho,nomzona,demora from ingreso_trafico where "+ filtro;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(CONSULTA);
                rs = st.executeQuery();
                while (rs.next()) {
                    //////System.out.println("entro en el next......................................................................");
                    java.sql.Timestamp fecha2 = java.sql.Timestamp.valueOf(""+rs.getString("fecha_prox_reporte"));
                    String f2=""+fecha2;
                    double demora= rs.getDouble("demora");
                    if(f2.indexOf("0099-")<0){
                        
                        long atrazo=getDiferencia(fecha2,fechaActual,demora);
                        //////System.out.println("Realiso la diferecncia......................................................................");
                        String idPlanilla=rs.getString("planilla");
                        String nomZona=rs.getString("nomzona");
                        String tipo_despacho=rs.getString("tipo_despacho");
                        //////System.out.println("Obtengo el id Planilla ......................................................................");
                        if(atrazo <= -300000) {
                            logWriter.log("PLANILLA "+idPlanilla,LogWriter.INFO);
                            logWriter.log("ATRASO "+atrazo,LogWriter.INFO);
                            logWriter.log("FECHA DE PROX REPORTE "+fecha2,LogWriter.INFO);
                            logWriter.log("FECHA DE ACTUALIZACION "+fechaActual,LogWriter.INFO);
                            logWriter.log("DEMORA "+demora,LogWriter.INFO);
                            if ((!nomZona.equalsIgnoreCase("SEGURIDAD"))&&(!tipo_despacho.equalsIgnoreCase("V"))&&(!tipo_despacho.equalsIgnoreCase("D"))) {
                                try {
                                    //////System.out.println("Entra en el ultimo try  ................................................)");
                                    st1 = con.prepareStatement(ACTUALIZAR_MOV_T);
                                    st1.setString(1, idPlanilla);
                                    st1.executeUpdate();
                                    st1 = con.prepareStatement(ACTUALIZAR_T);
                                    st1.setString(2, idPlanilla);
                                    st1.executeUpdate();
                                }
                                catch(SQLException e) {
                                    //   throw new SQLException("No existe palnilla " + e.getMessage()+"  " + e.getErrorCode());
                                }
                            }
                        }
                    }
                    //////System.out.println("antes de salir del next ...........................................................");
                    
                }
                // st.executeUpdate();
            }
        }
        catch(SQLException e) {
            throw new SQLException("act zona" + e.getMessage()+"" + e.getErrorCode());
        }
        finally {
            if(st != null)
            {st.close();}
            if ( con != null)
            { poolManager.freeConnection("fintra", con );}
        }
        
    }
    
    /**
     * Este m�todo retorna el select de la consulta de ingreso_trafico dado un Vector de String de campos
     *que van a ser seleccionados
     * @param String String []campo
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public String getConfiguracion(String []campo) {
        String configuracion="";
        for (int i=0;i<campo.length;i++) {
            if (i ==( campo.length-1)){
                configuracion=configuracion+campo[i];
            }
            else{
                configuracion=configuracion+campo[i]+",";
            }
        }
        configuracion ="select tipo_despacho,"+ configuracion;
        return configuracion;
    }
    
   /**
     * Este m�todo retorna el oreder by de la consulta de ingreso_trafico dado un Vector de String de campos y el orden
     *que van a ser seleccionados
     * @param String []campo,String orden
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public String getClasificacion(String []campo,String orden) {
        String configuracion="";
        for (int i=0;i<campo.length;i++) {
            //////System.out.println("entra al DAO getClasificacion");
            if (i ==( campo.length-1)){
                configuracion=configuracion+campo[i]+" "+ orden;
            }
            else{
                configuracion=configuracion+campo[i]+ " "+orden +",";
            }
        }
        configuracion =" order by "+configuracion;
        ////System.out.println("CONFIGURACION "+configuracion);
        return configuracion;
    } 
    
    /**
     * Este m�todo retorna  la consulta de ingreso_trafico dado la configuracion ,filtro y clasificacion
     *que van a ser seleccionados
     * @param String configuracion,String filtro,String clasificacion
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public String getConsulta(String configuracion,String filtro,String clasificacion) {
        String consulta="";
        
        consulta = configuracion + ",reg_status from ingreso_trafico " + filtro+ clasificacion;
        
        return consulta;
    }
    
    /**
     * Este m�todo retorna  un Vector con los campos dada un String con el nombre de los campos
     * separados por -
     * @param String linea
     */
    public  Vector obtenerCampos(String linea) {
        Vector campos = new Vector();
        StringTokenizer separados = new StringTokenizer(linea, "-");
        // Agrego cada uno de los campos al vector y luego lo retorno
        while (separados.hasMoreTokens()) {
            campos.addElement(separados.nextToken());
        }
        return campos;
    }
    
    /**
     * Este m�todo retorna  un long con la difirencia entre dos fechas en formato SQL
     *
     * @param java.sql.Timestamp fecha1(fecha inicial), java.sql.Timestamp fecha2(fechaFinal)
     */
    public long getDiferencia(java.sql.Timestamp fecha1, java.sql.Timestamp fecha2,double demora) {
        //  java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp.parse(;
        
        //los milisegundos
        
        long diferenciaMils = (fecha1.getTime()  +  (long)((demora)/1000))  - fecha2.getTime();
        
        
        //obtenemos los segundos
        long segundos = diferenciaMils / 1000;
        
        //obtenemos las horas
        long horas = segundos / 3600;
        
        //restamos las horas para continuar con minutos
        //segundos -= horas*3600;
        
        //igual que el paso anterior
        long minutos = segundos /60;
        segundos -= minutos*60;
        
        return minutos;
        
    }
    
    public String formatoFecha(String campo){
        String fecha="";
        fecha = campo.substring(11,16)+" "+campo.substring(8,10)+"/"+campo.substring(5,7)+"/"+campo.substring(2,4);
        //fecha= fecha +
        return fecha;
    }
    
 
  
    
    
    /**
     * Este m�todo que retorna un String con el numero de la caravana dada la planilla
     *
     * @param String numpla
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public String obtenerNumeroCaravana(String numpla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String caravana="";
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(OB_N_CARAVANA);
                st.setString(1, numpla);
                
                rs = st.executeQuery();
                //////System.out.println("--> obtener fila: " + st.toString());
                while(rs.next()) {
                    caravana = rs.getString("numcaravana");
                    //////System.out.println("Caravana en consulta "+caravana);
                }
                
                rs.close();
            }
        }catch(SQLException e) {
            throw new SQLException("ERROR AL OBTENER EL REGISTRO DE INGRESO TRAFICO " + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        //////System.out.println("Caravana al final "+caravana);
        return caravana;
    }
     /**
     * Obtiene el un registro del archivo ingreso trafico
     * @autor Andr�s Maturana De La Cruz
     * @param numpla Numero de la planilla
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @version 1.0
     */
    public void obtenerIngreso_Trafico(String numpla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null, rs2=null;
        PoolManager poolManager = null;
        ingreso = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.SQL_OBTENER);
                st.setString(1, numpla);
                
                rs = st.executeQuery();
                //////System.out.println("--> obtener fila: " + st.toString());
                
                
                while(rs.next()) {
                    ingreso = new Ingreso_Trafico();
                    
                    ingreso.setPlanilla(rs.getString("planilla"));
                    ingreso.setPlaca(rs.getString("placa"));
                    ingreso.setCedcon(rs.getString("cedcon"));
                    ingreso.setNomcond(rs.getString("nomcond"));
                    ingreso.setCedprop(rs.getString("cedprop"));
                    ingreso.setNomprop(rs.getString("nomprop"));
                    ingreso.setNomorigen(rs.getString("nomorigen"));
                    ingreso.setNomdestino(rs.getString("nomdestino"));
                    ingreso.setNomzona(rs.getString("nomzona"));
                    ingreso.setEscolta(rs.getString("escolta"));
                    ingreso.setCaravana(rs.getString("caravana"));
                    ingreso.setFecha_despacho(rs.getString("fecha_despacho"));
                    ingreso.setPto_control_ultreporte(rs.getString("pto_control_ultreporte"));
                    ingreso.setNompto_control_ultreporte(rs.getString("nompto_control_ultreporte"));
                    ingreso.setFecha_ult_reporte(rs.getString("fecha_ult_reporte"));
                    ingreso.setPto_control_proxreporte(rs.getString("pto_control_proxreporte"));
                    ingreso.setNompto_control_proxreporte(rs.getString("nompto_control_proxreporte"));
                    ingreso.setFecha_prox_reporte(rs.getString("fecha_prox_reporte"));
                    ingreso.setUlt_observacion(rs.getString("ult_observacion"));
                    ingreso.setOrigen(rs.getString("origen"));
                    ingreso.setDestino(rs.getString("destino"));
                    ingreso.setBase(rs.getString("base"));
                    ingreso.setZona(rs.getString("zona"));
                    ingreso.setDstrct(rs.getString("dstrct"));
                    ingreso.setFecha_salida(rs.getString("fecha_salida"));
                }
                
                rs.close();
            }
        }catch(SQLException e) {
            throw new SQLException("ERROR AL OBTENER EL REGISTRO DE INGRESO TRAFICO " + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    
    /*
     *  Obtiene un registro de ingreso_trafico teniendo el n�mero de la planilla
     *  @autor Tito Andr�s 03.10.2005
     *
     */
    String SQL_OBTENER_VIA="select via from ingreso_trafico where planilla=?";
    public String  obtenerVia(String numpla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Ingreso_Trafico ingreso = null;
        String via="";
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.SQL_OBTENER_VIA);
                st.setString(1, numpla);
                
                rs = st.executeQuery();
                //////System.out.println("--> obtener fila: " + st.toString());
                
                
                if(rs.next()) {
                    via=rs.getString("via");
                }
                
                rs.close();
            }
        }catch(SQLException e) {
            throw new SQLException("ERROR AL OBTENER EL REGISTRO DE INGRESO TRAFICO " + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        return via;
    }
    
    /*
     *  Obtiene un registro de ingreso_trafico teniendo el n�mero de la planilla
     *  @autor Tito Andr�s 03.10.2005
     *
     */
    String SQL_OBTENER_PROX_PC="select pto_control_proxreporte from ingreso_trafico where planilla=?";
    public String  obtenerPPC(String numpla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Ingreso_Trafico ingreso = null;
        String via="";
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.SQL_OBTENER_PROX_PC);
                st.setString(1, numpla);
                
                rs = st.executeQuery();
                //////System.out.println("--> obtener fila: " + st.toString());
                
                
                if(rs.next()) {
                    via=rs.getString("pto_control_proxreporte");
                }
                
                rs.close();
            }
        }catch(SQLException e) {
            throw new SQLException("ERROR AL OBTENER EL REGISTRO DE INGRESO TRAFICO " + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        return via;
    }
    
    
    public boolean  validarVia(String ppc,String secuencia) throws SQLException {
        boolean sw=false;
        String campo1=secuencia.substring(0,2);
        String campo2=secuencia.substring(2,4);
        //////System.out.println("campo 1"+campo1+ " campo2 "+campo2);
        if(ppc.equals(campo1) || ppc.equals(campo2)){
            sw= true;
        }
        else{
            sw= false;
        }
        return sw;
    }
    
    
    public String  obtenerSecuencia(String via) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Ingreso_Trafico ingreso = null;
        String origen="";
        String destino="";
        String codsecuencia="";
        origen=via.substring(0,2);
        //////System.out.println("origen "+origen);
        destino = via.substring(2,4);
        //////System.out.println("destino "+destino);
        codsecuencia=via.substring(4,via.length());
        //////System.out.println("secuencia "+codsecuencia);
        String secuencia="";
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.SQL_OBTENER_SECUENCIA);
                st.setString(1,origen);
                st.setString(2,destino);
                st.setString(3,codsecuencia);
                rs = st.executeQuery();
                //////System.out.println("--> obtener fila: " + st.toString());
                while(rs.next()) {
                    secuencia=rs.getString("via");
                }
                
                rs.close();
            }
        }catch(SQLException e) {
            throw new SQLException("ERROR AL OBTENER EL REGISTRO DE INGRESO TRAFICO " + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        return secuencia;
    }
    
    
    private TreeMap zonas ;
    public void obtenerZonasPorUsuario(String id) throws SQLException {
        this.zonas = new TreeMap();
        
        // Vector vzu=this.gennerarVZonasPorUsuario(id);
        Vector vzu =vDeTurnos(id);
        zonas.put("Seleccione", "");
        for(int i=0; i< vzu.size(); i++) {
            
            Vector fila = (Vector)vzu.elementAt(i);
            zonas.put(fila.elementAt(1), fila.elementAt(0));
        }
    }
    
    /**
     * Getter for property zonas.
     * @return Value of property zonas.
     */
    public java.util.TreeMap getZonas() {
        return zonas;
    }
    
    /**
     * Setter for property zonas.
     * @param zonas New value of property zonas.
     */
    public void setZonas(java.util.TreeMap zonas) {
        this.zonas = zonas;
    }
    
    /**
     * Este m�todo que retorna un Vector con las zonas que tiene asignadas un usuario
     *
     * @param String idUsuario
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public Vector gennerarVZonasPorUsuario(String idUsuario) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        
        Vector vZonasPorU = new Vector();
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.ZONAS_POR_U);
                st.setString(1,idUsuario);
                
                rs = st.executeQuery();
                
                while (rs.next()) {
                    Vector fila = new Vector();
                    fila.add(rs.getString("zona"));
                    fila.add(rs.getString("desczona"));
                    vZonasPorU.add(fila);
                }
            }
        }catch(SQLException e) {
            throw new SQLException("generar v zona" + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        return vZonasPorU;
    }
    
    /**
     * metodo que retorna un Vector con la descripcion de la zonas y el codigo de zonas de un usuario de trafico en una hora determinada
     * @param String user
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public Vector vDeTurnos(String user) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String zona=this.zonasDeTurnos(user);
        String[] zonas=zona.split(",");
        Vector vZonasPorU = new Vector();
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                for(int i=0;i<zonas.length;i++){
                    st = con.prepareStatement(this.ZONAS_POR_U_POR_TURNO);
                    st.setString(1,user);
                    st.setString(2, ""+zonas[i]);
                    rs = st.executeQuery();
                    while (rs.next()) {
                        Vector fila = new Vector();
                        fila.add(rs.getString("zona"));
                        fila.add(rs.getString("desczona"));
                        vZonasPorU.add(fila);
                    }
                }
            }
        }catch(SQLException e) {
            throw new SQLException("v de Turnos" + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        return vZonasPorU;
    }
    
    /**
     * metodo que retorna un Vector con la descripcion de la zonas y el codigo de zonas de un usuario de trafico en una hora determinada
     * @param String user
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void gennerarVZonasTurno(String user) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String zona=this.zonasDeTurnos(user);
        String[] zonas=zona.split(",");
        vecTrafico = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                vecZonas = new Vector();
                for(int i=0;i<zonas.length;i++){
                    st = con.prepareStatement(this.ZONAS_POR_U_POR_TURNO);
                    st.setString(1,user);
                    st.setString(2, ""+zonas[i]);
                    rs = st.executeQuery();
                    while (rs.next()) {
                        vecZonas.add(rs.getString("zona"));
                    }
                }
            }
        }catch(SQLException e) {
            throw new SQLException("generar v zona" + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    /***************************************************************************
     ***************************************************************************
     **                                                                       **
     **    Los siguientes metodos y atributos fueron agregados de la          **
     **    aplicaci�n SOT. Alejandro Payares, nov 24 de 2005.                 **
     **                                                                       **
     ***************************************************************************
     ***************************************************************************/
    
    
    private transient List traficoList;
    private transient List traficoObList;
    
    /**
     * Este m�todo genera los registros que hacen parte de los movimientos de
     * trafico de una OC/Planilla asi como las observaciones
     * @param numpla Numero de planilla/OC a la que se quiere consultar trafico
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void traficoSearch(String numpla )throws SQLException {
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = this.crearPreparedStatement("SQL_TRAFICO_SEARCH");
            pstmt.setString(1, numpla );
            rs = pstmt.executeQuery();
            traficoList = new LinkedList();
            while (rs.next()) {
                traficoList.add( ReporteTrafico.load(rs) );
            }
        } finally {
            if (rs != null) rs.close();
            if ( pstmt != null ) pstmt.close();
            this.desconectar("SQL_TRAFICO_SEARCH");
        }
    }
    
    public List getTraficoList() {
        return traficoList;
    }
    
    /**
     * Este m�todo genera los registros que hacen parte de los movimientos de
     * las observaciones de trafico de una OC/Planilla.
     * @param numpla Numero de planilla/OC a la que se quiere consultar trafico
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void traficoObSearch(String numpla )throws SQLException {
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = this.crearPreparedStatement("SQL_TRAFICO_OB_SEARCH");
            pstmt.setString(1, numpla );
            rs = pstmt.executeQuery();
            traficoObList = new LinkedList();
            while (rs.next()) {
                traficoObList.add( ReporteTraficoOb.load(rs) );
            }
        } finally {
            if (rs != null) rs.close();
            if ( pstmt != null ) pstmt.close();
            this.desconectar("SQL_TRAFICO_OB_SEARCH");
        }
    }
    
    /**
     * Getter for getTraficoObList.
     * @return List
     */
    public List getTraficoObList() {
        return traficoObList;
    }
      /**
     * Getter for property ingreso.
     * @return Value of property ingreso.
     */
    public com.tsp.operation.model.beans.Ingreso_Trafico getIngreso() {
        return ingreso;
    }
    
    /**
     * Setter for property ingreso.
     * @param ingreso New value of property ingreso.
     */
    public void setIngreso(com.tsp.operation.model.beans.Ingreso_Trafico ingreso) {
        this.ingreso = ingreso;
    }
    /**
     * Obtiene el un registro del archivo trafico
     * @autor Andr�s Maturana De La Cruz
     * @param numpla Numero de la planilla
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @version 1.0
     */
    public void obtenerTrafico(String numpla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null, rs2=null;
        PoolManager poolManager = null;
        ingreso = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.SQL_OBTENER_TRAFICO);
                st.setString(1, numpla);
                
                rs = st.executeQuery();
                //////System.out.println("--> obtener fila: " + st.toString());
                
                
                while(rs.next()) {
                    ingreso = new Ingreso_Trafico();
                    
                    ingreso.setPlanilla(rs.getString("planilla"));
                    ingreso.setPlaca(rs.getString("placa"));
                    ingreso.setCedcon(rs.getString("cedcon"));
                    ingreso.setNomcond(rs.getString("nomcond"));
                    ingreso.setCedprop(rs.getString("cedprop"));
                    ingreso.setNomprop(rs.getString("nomprop"));
                    ingreso.setNomorigen(rs.getString("nomorigen"));
                    ingreso.setNomdestino(rs.getString("nomdestino"));
                    ingreso.setNomzona(rs.getString("nomzona"));
                    ingreso.setEscolta(rs.getString("escolta"));
                    ingreso.setCaravana(rs.getString("caravana"));
                    ingreso.setFecha_despacho(rs.getString("fecha_despacho"));
                    ingreso.setPto_control_ultreporte(rs.getString("pto_control_ultreporte"));
                    ingreso.setNompto_control_ultreporte(rs.getString("nompto_control_ultreporte"));
                    ingreso.setFecha_ult_reporte(rs.getString("fecha_ult_reporte"));
                    ingreso.setPto_control_proxreporte(rs.getString("pto_control_proxreporte"));
                    ingreso.setNompto_control_proxreporte(rs.getString("nompto_control_proxreporte"));
                    ingreso.setFecha_prox_reporte(rs.getString("fecha_prox_reporte"));
                    ingreso.setUlt_observacion(rs.getString("ult_observacion"));
                    ingreso.setOrigen(rs.getString("origen"));
                    ingreso.setDestino(rs.getString("destino"));
                                        ingreso.setFecha_salida(rs.getString("fecha_salida"));
                  
                }
                
                rs.close();
            }
        }catch(SQLException e) {
            throw new SQLException("ERROR AL OBTENER EL REGISTRO DE INGRESO TRAFICO " + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    
    /**
     * Obtiene los escoltas asociados a una planilla
     * @autor Andr�s Maturana De La Cruz
     * @param numpla Numero de la planilla
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @version 1.0
     */
    public String obtenerEscoltasVehiculo(String numpla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null, rs2=null;
        PoolManager poolManager = null;
        String esc = "";
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.SQL_ESCOLTAS_VEH);
                st.setString(1, numpla);
                
                rs = st.executeQuery();
                //////System.out.println("--> obtener fila: " + st.toString());
                
                
                while(rs.next()) {
                    esc = rs.getString(1);
                  
                }
                
                rs.close();
            }
        }catch(SQLException e) {
            throw new SQLException("ERROR AL OBTENER LOS ESCOLTAS VEHICULO " + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        
        return esc;
    }
    
    /**
     * Obtiene los escoltas asociados a una planilla
     * @autor Andr�s Maturana De La Cruz
     * @param numpla Numero de la planilla
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @version 1.0
     */
    public String obtenerEscoltasCaravana(String numpla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null, rs2=null;
        PoolManager poolManager = null;
        String esc = "";
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.SQL_ESCOLTAS_CAR);
                st.setString(1, numpla);
                
                rs = st.executeQuery();
                //////System.out.println("--> obtener fila: " + st.toString());
                
                
                if(rs.next()) {
                    esc = rs.getString(1);
                  
                }
                
                rs.close();
            }
        }catch(SQLException e) {
            throw new SQLException("ERROR AL OBTENER LOS ESCOLTAS VEHICULO CARAVANA " + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        
        return esc;
    }
    
    /**
     * Obtiene las caravanas asociadas a una planilla
     * @autor Andr�s Maturana De La Cruz
     * @param numpla Numero de la planilla
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @version 1.0
     */
    public String obtenerCaravanas(String numpla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null, rs2=null;
        PoolManager poolManager = null;
        String car = "";
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.SQL_CARAVANA);
                st.setString(1, numpla);
                
                rs = st.executeQuery();
                //////System.out.println("--> obtener fila: " + st.toString());
                
                
                if(rs.next()) {
                    car = rs.getString(1);
                  
                }
                
                rs.close();
            }
        }catch(SQLException e) {
            throw new SQLException("ERROR AL OBTENER LAS CARAVANAS " + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        
        return car;
    }
    ////////////////////////////////// AMATURANA 11.08.2006
    
    /**
     * Getter for property registros.
     * @return Value of property registros.
     */
    public java.util.Vector getRegistros() {
        return registros;
    }
    
    /**
     * Setter for property registros.
     * @param registros New value of property registros.
     */
    public void setRegistros(java.util.Vector registros) {
        this.registros = registros;
    }
    
    /**
     * Obtiene el un registro del archivo ingreso trafico pertenecientes a una placa.
     * @autor Andr�s Maturana De La Cruz
     * @param numpla Numero de la planilla
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @version 1.0
     */
    public void obtenerIngreso_TraficoPlaca(String placa) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null, rs2=null;
        PoolManager poolManager = null;
        ingreso = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.SQL_PLACA_ITRAFICO);
                st.setString(1, placa);
                
                rs = st.executeQuery();
                ////System.out.println("--> obtener fila placa: " + st.toString());
                
                this.registros = new Vector();
                while(rs.next()) {
                    ingreso = new Ingreso_Trafico();
                    
                    ingreso.setPlanilla(rs.getString("planilla"));
                    ingreso.setPlaca(rs.getString("placa"));
                    ingreso.setCedcon(rs.getString("cedcon"));
                    ingreso.setNomcond(rs.getString("nomcond"));
                    ingreso.setCedprop(rs.getString("cedprop"));
                    ingreso.setNomprop(rs.getString("nomprop"));
                    ingreso.setNomorigen(rs.getString("nomorigen"));
                    ingreso.setNomdestino(rs.getString("nomdestino"));
                    ingreso.setNomzona(rs.getString("nomzona"));
                    ingreso.setEscolta(rs.getString("escolta"));
                    ingreso.setCaravana(rs.getString("caravana"));
                    ingreso.setFecha_despacho(rs.getString("fecha_despacho"));
                    ingreso.setPto_control_ultreporte(rs.getString("pto_control_ultreporte"));
                    ingreso.setNompto_control_ultreporte(rs.getString("nompto_control_ultreporte"));
                    ingreso.setFecha_ult_reporte(rs.getString("fecha_ult_reporte"));
                    ingreso.setPto_control_proxreporte(rs.getString("pto_control_proxreporte"));
                    ingreso.setNompto_control_proxreporte(rs.getString("nompto_control_proxreporte"));
                    ingreso.setFecha_prox_reporte(rs.getString("fecha_prox_reporte"));
                    ingreso.setUlt_observacion(rs.getString("ult_observacion"));
                    ingreso.setOrigen(rs.getString("origen"));
                    ingreso.setDestino(rs.getString("destino"));
                    ingreso.setBase(rs.getString("base"));
                    ingreso.setZona(rs.getString("zona"));
                    ingreso.setDstrct(rs.getString("dstrct"));
                    ingreso.setFecha_salida(rs.getString("fecha_salida"));
                    
                    this.registros.add(ingreso);
                }
                
                rs.close();
            }
        }catch(SQLException e) {
            throw new SQLException("ERROR AL OBTENER EL REGISTRO DE INGRESO TRAFICO " + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
    }
     /**
     * Obtiene el un registro del archivo ingreso trafico pertenecientes a un despacho manual.
     * @autor Andr�s Maturana De La Cruz
     * @param dstrct Distrito
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @version 1.0
     */
    public void reporteDespachoManual(String dstrct )throws SQLException {
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = this.crearPreparedStatement("SQL_REP_DESMANUAL");
            pstmt.setString(1, dstrct );
            rs = pstmt.executeQuery();
            this.registros = new Vector();
            
            while (rs.next()) {
                Hashtable ht = new Hashtable();
                ht.put("oc", rs.getString("numpla"));
                ht.put("placa", rs.getString("placa"));
                ht.put("cond", rs.getString("conductor"));
                ht.put("nomcond", rs.getString("nomcond"));
                ht.put("fecdespacho", rs.getString("fecha_despacho"));
                ht.put("via", rs.getString("via"));
                ht.put("cliente", rs.getString("nomcli"));
                ht.put("carga", rs.getString("carga"));
                ht.put("descripcion", rs.getString("descripcion"));
                ht.put("justificacion", rs.getString("justificacion"));
                ht.put("fecsalida", rs.getString("fecha_salida"));
                ht.put("nompto_control_ultreporte", rs.getString("nompto_control_ultreporte"));
                ht.put("nompto_control_proxreporte", rs.getString("nompto_control_proxreporte"));
                ht.put("fecha_ult_reporte", rs.getString("fecha_ult_reporte"));
                ht.put("fecha_prox_reporte", rs.getString("fecha_prox_reporte"));
                ht.put("ult_observacion", rs.getString("ult_observacion"));
                ht.put("cedprop", rs.getString("cedprop"));
                ht.put("nomprop", rs.getString("nomprop"));
                ht.put("origen", rs.getString("origen"));
                ht.put("destino", rs.getString("destino"));
                ht.put("agencia", rs.getString("agencia"));
                ht.put("descripcion", rs.getString("descripcion"));
                ht.put("justific", rs.getString("justificacion"));
                ht.put("legal", rs.getString("legal"));
                
                this.registros.add(ht);
            }
        } finally {
            if (rs != null) rs.close();
            if ( pstmt != null ) pstmt.close();
            this.desconectar("SQL_REP_DESMANUAL");
        }
    }
     /**
     *Metodo para obtener reporte detallado de trafico
     *@param planillas numeros de planillas separadas por comas.
     */
    public void getReporteDetalladoTraf(String planillas) throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        TraficoDetallado trafDet = null;
        String planillasList = "";
        String [] planillasArray = planillas.split(",");
        
        if (!planillas.equals("") ) {
            for(int idx = 0; idx < planillasArray.length; idx++ ) {
                String oc = planillasArray[idx].trim();
                if( !oc.equals("") )                    
                planillasList += "'" + oc + "',";
            }
        }
        planillasList = planillasList.substring(0,planillasList.length()-1);                
        
        try{
            st = this.crearPreparedStatement( "SQL_TRAFICO_DETALLADO" );
            String sql = st.toString();
            sql = sql.replace( "#PLANILLAS#" ,  planillasList);
            
            rs = st.executeQuery(sql);
            
            traficoDetallado = new LinkedList();
            
            while (rs.next()){
                trafDet = TraficoDetallado.load(rs);
                traficoSearch(trafDet.getNumpla());
                traficoObSearch(trafDet.getNumpla());
                trafDet.setTraficoList(traficoList);
                trafDet.setTraficoObList(traficoObList);
                traficoDetallado.add(trafDet);
            }
            
        }
        catch(SQLException e){
            throw new SQLException("ERROR EJECUTANDO REPORTE DE TRAFICO DETALLADO "+ e.getMessage());
        }
        finally{
            if (st != null) st.close();
            this.desconectar("SQL_TRAFICO_DETALLADO");
        }
    }
    
      /**
     * Getter for property traficoDetallado.
     * @return Value of property traficoDetallado.
     */
    public java.util.List getTraficoDetallado() {
        return traficoDetallado;
    }
    
    /**
     * Setter for property traficoDetallado.
     * @param traficoDetallado New value of property traficoDetallado.
     */
    public void setTraficoDetallado(java.util.List traficoDetallado) {
        this.traficoDetallado = traficoDetallado;
    }
    
    public static void main(String[]kljf){
        try{
            TraficoDAO t = new TraficoDAO();
            t.getReporteDetalladoTraf("354447,278260,459345");
        }catch (Exception ex){
            ////System.out.println(ex.getMessage());
        }
        
    }
    
       /**
     * Este m�todo que genera el Vector de ingreso trafico dado la consulta y el Vector de campos que van a ser mostrados
     *
     * @param String consulta,Vector campos
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void generarTabla(String consulta,Vector campos) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager
        poolManager = null;
        boolean sw = false;
        vecTrafico = null;
        String valor="";
        String nombreCampo="";
        consulta = consulta.replaceAll("-_-", "'");
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(consulta);
                vecTrafico = new Vector();
                rs = st.executeQuery();
                while (rs.next()) {
                    Vector fila = new Vector();
                    for (int i=0; i<campos.size(); i++) {
                        valor=""+campos.elementAt(i);
                        //////System.out.println("valor en dao"+valor);
                        nombreCampo=rs.getString(valor);
                        if(valor.equals("fecha_ult_reporte") || valor.equals("fecha_prox_reporte") || valor.equals("fecha_despacho")|| valor.equals("fecha_salida")){
                            nombreCampo=nombreCampo.substring(0,16);
                            if(nombreCampo.equals("0099-01-01 00:00:00")||nombreCampo.equals("0099-01-01 00:00")){
                                nombreCampo="";
                            }
                            else{
                                nombreCampo=this.formatoFecha(nombreCampo);
                            }
                        }
                        
                        fila.add(nombreCampo);
                    }
                    fila.add(rs.getString("fecha_ult_reporte"));
                    fila.add(rs.getString("fecha_prox_reporte"));
                    fila.add(rs.getString("planilla"));
                    fila.add(rs.getString("demora"));
                    fila.add(rs.getString("placa"));
                    fila.add(rs.getString("cedcon"));
                    fila.add(rs.getString("fecha_salida"));
                    fila.add(rs.getString("cel_cond"));
                    fila.add(rs.getString("tipo_despacho"));
                    fila.add(rs.getString("nomcond"));
                    fila.add(rs.getString("reg_status"));
                    fila.add(rs.getString("ult_observacion"));
                    fila.add(rs.getString("color_letra"));
                    fila.add(rs.getString("obs"));
                    fila.add(rs.getString("reg_status"));
                    fila.add(rs.getString("neintermedias"));
                    vecTrafico.add(fila);
                }
            }
        }catch(SQLException e) {
            throw new SQLException("ERROR generar tabla " + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
    }

      /**
     * Actualiza ingreso trafico y cambia el color de la fila.
     * @autor Karen Reales
     * @param Numpla, Color de la letra
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @version 1.0
     */
    public void cambiarColor(String numpla, String color )throws SQLException {
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = this.crearPreparedStatement("SQL_CAMBIAR_COLOR");
            pstmt.setString(1, color );
            pstmt.setString(2, numpla );
            pstmt.executeUpdate();

        } finally {
            if (rs != null) rs.close();
            if ( pstmt != null ) pstmt.close();
            this.desconectar("SQL_CAMBIAR_COLOR");
        }
    }
    
    /**
     * Obtiene el un registro del archivo ingreso trafico pertenecientes a un despacho manual.
     * @autor Andr�s Maturana De La Cruz
     * @param dstrct Distrito
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @version 1.0
     */
    public void reporteDespachoManual( String dstrct, String fechai, String fechaf )throws SQLException {
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = this.crearPreparedStatement("SQL_REP_DESMANUAL");
            pstmt.setString(1, dstrct );
            pstmt.setString(2, fechai );
            pstmt.setString(3, fechaf );
            rs = pstmt.executeQuery();
            this.registros = new Vector();
            
            while (rs.next()) {
                Hashtable ht = new Hashtable();
                ht.put("oc", rs.getString("numpla"));
                ht.put("placa", rs.getString("placa"));
                ht.put("cond", rs.getString("conductor"));
                ht.put("nomcond", rs.getString("nomcond"));
                ht.put("fecdespacho", rs.getString("fecha_despacho"));
                ht.put("via", rs.getString("via"));
                ht.put("cliente", rs.getString("nomcli"));
                ht.put("carga", rs.getString("carga"));
                ht.put("descripcion", rs.getString("descripcion"));
                ht.put("justificacion", rs.getString("justificacion"));
                ht.put("fecsalida", rs.getString("fecha_salida"));
                ht.put("nompto_control_ultreporte", rs.getString("nompto_control_ultreporte"));
                ht.put("nompto_control_proxreporte", rs.getString("nompto_control_proxreporte"));
                ht.put("fecha_ult_reporte", rs.getString("fecha_ult_reporte"));
                ht.put("fecha_prox_reporte", rs.getString("fecha_prox_reporte"));
                ht.put("ult_observacion", rs.getString("ult_observacion"));
                ht.put("cedprop", rs.getString("cedprop"));
                ht.put("nomprop", rs.getString("nomprop"));
                ht.put("origen", rs.getString("origen"));
                ht.put("destino", rs.getString("destino"));
                ht.put("agencia", rs.getString("agencia"));
                ht.put("descripcion", rs.getString("descripcion"));
                ht.put("justific", rs.getString("justificacion"));
                ht.put("legal", rs.getString("legal"));
                
                this.registros.add(ht);
            }
        } finally {
            if (rs != null) rs.close();
            if ( pstmt != null ) pstmt.close();
            this.desconectar("SQL_REP_DESMANUAL");
        }
    }
    
}
