/*
 * AnticipoGasolinaDAO.java
 *  autor: imorales
 * Created on 29 de enero de 2008, 10:22 PM
 */



package com.tsp.operation.model.DAOS;

import java.io.*;

import java.sql.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.TasaService;
import com.tsp.util.*;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.AnticipoGasolina;
//import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import java.util.regex.*;
//import org.apache.log4j.Logger;
import com.tsp.finanzas.contab.model.*;
import com.tsp.finanzas.contab.*;
/**
 *
 * @author iamorales
 */
public class AnticipoGasolinaDAO extends MainDAO {

    private  List listAnticiposGas;

    private  ArrayList listAnticiposAprobadosGas;
    private  ArrayList listAnticiposTransferidosGas;

    private Connection con2 = null;
    private ArrayList anticiposGasolina=null;
    private String idAnticipoGasolinaAceptado="";
    String valEfectivoAceptado="",valGasolinaAceptado="",valNeto="",val="";
    private String COLUMNAS  =
    " a.id,                           "+
    " a.dstrct    ,                   "+
    " a.agency_id ,                   "+
    " a.pla_owner ,                   "+
    " a.planilla  ,                   "+
    " a.supplier  ,                   "+
    " a.proveedor_anticipo ,          "+
    " a.concept_code ,                "+
    " a.vlr      ,                    "+
    " a.vlr_for  ,                    "+
    " a.currency ,                    "+
    " a.fecha_anticipo,               "+

    " a.aprobado,                     "+
    " substr(a.fecha_autorizacion,1,16)   as fecha_autorizacion ,   "+
    " a.user_autorizacion  ,          "+

    " a.transferido,                  "+
    " substr(a.fecha_transferencia,1,16)  as fecha_transferencia ,  "+
    " a.banco_transferencia ,         "+
    " a.cuenta_transferencia ,        "+
    " a.tcta_transferencia ,          "+
    " a.user_transferencia ,          "+

    " a.banco,                        "+
    " a.sucursal,                     "+
    " a.nombre_cuenta,                "+
    " a.cuenta,                       "+
    " a.tipo_cuenta,                  "+
    " a.nit_cuenta,                   "+

    " substr(a.fecha_migracion,1,16)  as fecha_migracion ,  "+
    " a.user_migracion ,              "+

    " a.factura_mims ,                "+
    " a.vlr_mims_tercero ,            "+
    " a.vlr_mims_propietario,         "+
    " a.estado_pago_tercero,          "+
    " a.estado_desc_propietario,      "+
    " substr(a.fecha_pago_tercero,1,16)      as fecha_pago_tercero ,      "+
    " substr(a.fecha_desc_propietario,1,16)  as fecha_desc_propietario ,  "+

    " a.cheque_pago_tercero ,         "+
    " a.cheque_desc_propietario,      "+
    " a.corrida_pago_tercero,         "+
    " a.corrida_desc_propietario,     "+

    " a.nombre_agencia,    "+

    " 'nada'                   as nombre_proveedor,  "+
    " a.nombre_propietario                         as nombre_prpietario, "+

    " a.porcentaje,                  "+
    " a.vlr_descuento,               "+
    " a.vlr_neto,                    "+
    " a.vlr_combancaria,             "+
    " a.vlr_consignacion,            "+
    " a.reanticipo,                  "+
    " a.cedcon,                      "+
    " a.transferencia,               "+
    " a.liquidacion,                 "+
    " a.secuencia                    "+
    " , a.creation_user              "+
    " , a.vlr_gasolina               "+
    " , a.vlr_efectivo               ";

    public AnticipoGasolinaDAO() {
        super("AnticipoGasolinaDAO.xml");
        anticiposGasolina=new ArrayList();
    }
    public AnticipoGasolinaDAO(String dataBaseName) {
        super("AnticipoGasolinaDAO.xml", dataBaseName);
        anticiposGasolina=new ArrayList();
    }

    public List getCuentasProveedor() {
        return listAnticiposGas;
    }

    public void setListAnticiposGas(List listAnticiposGas1) {
        this.listAnticiposGas = listAnticiposGas1;
    }

    /*  Autor IMorales.
     *  Fecha  Ene - 25 -08
     *  Metodo:  aprobar
     *  Descripcion: Aprueba una lista de Cuentas
     */

    private String reset(String val){
        if(val==null)
            val = "";
        return val;
    }

    private String resetNumero(String val){
        if(val==null)
            val = "0";
        return val;
    }

    /*  Autor IMorales.
     *  Fecha  Ene - 25 -08
     *  Metodo:  isCuentaValida
     *  Descripcion: Verifica si una cuenta es valida.
     */

    public boolean isAnticipoGasolina(String conductor1,String placa1,String login1) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_ANTGAS";
        anticiposGasolina=new ArrayList();
        boolean respuesta=false;
        try{

            con = this.conectarJNDI(query);//JJCastro fase2

            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );

            st.setString(1, login1);
            st.setString(2, conductor1);
            st.setString(3, placa1);

            rs = st.executeQuery();

            while (rs.next()){
                //ystem.out.println("un dato leido::"+rs.getString("id"));

                AnticipoGasolina anticipoGasolina=new AnticipoGasolina();
                //anticipoGasolina.setDstrct(rs.getString("dstrct"));
                anticipoGasolina.setId(Integer.parseInt(rs.getString("id")));
                anticipoGasolina.setVlr(Double.parseDouble(rs.getString("vlr")));
                anticipoGasolina.setFecha_anticipo(rs.getString("fecha_anticipo"));
                anticipoGasolina.setPlanilla(rs.getString("planilla"));
                anticipoGasolina.setNombreConductor(rs.getString("nombre"));
                anticipoGasolina.setConductor(rs.getString("cedcon"));
                anticipoGasolina.setSupplier(rs.getString("supplier"));

                anticipoGasolina.setVlrNeto(Double.parseDouble(rs.getString("vlr_neto_real")));
                anticipoGasolina.setNombrePropietario(rs.getString("nombre_propietario"));
                anticipoGasolina.setPla_owner(rs.getString("pla_owner"));
                anticiposGasolina.add(anticipoGasolina);
                respuesta= true;
            }
            return respuesta;
        }catch(Exception e){
            System.out.println("error en anticipoGasolinaDAO en isAnti..."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

    }

    public ArrayList getAnticiposGasolina(){
        return anticiposGasolina;
    }


    /**
     *
     * @param idAntGas
     * @param loginx
     * @param valEfectiv
     * @param valGasolin
     * @param vlrnetoreal
     * @return
     * @throws Exception
     */
    public String  aceptarAnticipoGasolina(String idAntGas,String loginx,String valEfectiv,String valGasolin,String vlrnetoreal)  throws Exception{
        String respuesta="mal";
        Connection         con     = null;
        PreparedStatement  st      = null;

        String             query   = "SQL_ACEPTAR_ANTGAS";

        try{
            //ystem.out.println("_aceptarAnticipoGasolina");
            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
            //ystem.out.println("_aceptarAnticipoGasolina2");
            String sql    =   this.obtenerSQL( query );
            //ystem.out.println("sql"+sql+"_"+idAntGas+"loginx"+loginx);
            st = con.prepareStatement( sql );
            st.setString(1, loginx);
            st.setDouble(2, Double.parseDouble(valEfectiv));
            st.setDouble(3, Double.parseDouble(valGasolin));
            st.setString(4, loginx);
            st.setDouble(5,  Double.parseDouble(vlrnetoreal));
            st.setString(6, idAntGas);

            int affected =st.executeUpdate(); 
            if (affected>0){
                respuesta="Anticipo de Estaci�n de Gasolina Aceptado.";
                idAnticipoGasolinaAceptado=idAntGas;
                valEfectivoAceptado=valEfectiv;
                valGasolinaAceptado=valGasolin;


            }else{
                respuesta="Anticipo de Estaci�n de Gasolina INV�LIDO.";
                idAnticipoGasolinaAceptado="";
                valEfectivoAceptado="";
                valGasolinaAceptado="";
            }
            //inicio de actualizar preanticipo
            if (affected>0){
                query   = "SQL_ACTUALIZAR_PREANTICIPO";
                sql    =   this.obtenerSQL( query );
                st = con.prepareStatement( sql );
                st.setInt(1, Integer.parseInt(idAntGas));
                st.setString(2, idAntGas);

                affected =st.executeUpdate();

            }
            //fin de actualizar preanticipo

        }}catch(Exception e){
            System.out.println("error en anticipoGasolinaDAO en aceptar..."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;
    }



/**
 *
 * @param estaci
 * @return
 * @throws Exception
 */
    public ArrayList getAnticiposGasolinaAprobados(String estaci) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_ANTGAS_APROB";
        listAnticiposAprobadosGas=new ArrayList();
        boolean respuesta=false;
        try{

            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
            String sql    =   this.obtenerSQL( query );

            if (estaci!=null && !(estaci.equals(""))){
                sql=sql.replaceAll("WHERE","WHERE (user_autorizacion='"+estaci+"' ) AND ");
            }

            st            =   con.prepareStatement( sql );

            rs = st.executeQuery();
            String estacionx="";
            String autorizadorx="";
            while (rs.next()){
                //ystem.out.println("un dato leido::"+rs.getString("id"));

                AnticipoGasolina anticipoGasolina=new AnticipoGasolina();
                //anticipoGasolina.setDstrct(rs.getString("dstrct"));
                anticipoGasolina.setId(Integer.parseInt(rs.getString("id")));
                anticipoGasolina.setVlr(Double.parseDouble(rs.getString("vlr")));
                anticipoGasolina.setFecha_anticipo(rs.getString("fecha_anticipo"));
                anticipoGasolina.setPlanilla(rs.getString("planilla"));
                anticipoGasolina.setNombreConductor(rs.getString("nombre"));
                anticipoGasolina.setConductor(rs.getString("cedcon"));
                anticipoGasolina.setSupplier(rs.getString("supplier"));
                anticipoGasolina.setVlrNeto(Double.parseDouble(rs.getString("vlr_neto")));
                anticipoGasolina.setNombrePropietario(rs.getString("nombre_propietario"));
                anticipoGasolina.setPla_owner(rs.getString("pla_owner"));
                autorizadorx=rs.getString("user_autorizacion");
                anticipoGasolina.setUser_autorizacion(autorizadorx);
                anticipoGasolina.setFecha_autorizacion(rs.getString("fecha_autorizacion"));
                estacionx=rs.getString("estacion");

                anticipoGasolina.setVlr_preanticipos(rs.getString("sum_preants_id"));

                if (estacionx==null){
                    estacionx=autorizadorx;
                }
                anticipoGasolina.setAsesor(estacionx);

                anticipoGasolina.setSumaAnticiposDplanilla(resetNumero(rs.getString("plata_anticipos_de_planilla")));
                anticipoGasolina.setSumaCxpsTspDplanilla(resetNumero(rs.getString("plata_cxp_de_planilla")));
                anticipoGasolina.setCxpTspPosible_corrida_cheque(reset(rs.getString("cxp_posible_corrida_cheque")));
                anticipoGasolina.setCxpTspPosible2_corrida_cheque(reset(rs.getString("cxp_posible2_corrida_cheque")));

                listAnticiposAprobadosGas.add(anticipoGasolina);

            }

        }}catch(Exception e){
            System.out.println("error en anticipoGasolinaDAO en getAnticiposGasolinaAprobados..."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return listAnticiposAprobadosGas;

    }

    public AnticipoGasolina getAnticipoGasolina(String id1,String login) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_GET_ANTGAS";
        AnticipoGasolina respuesta=null;
        try{

            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
            String sql    =   this.obtenerSQL( query );
            //ystem.out.println("sql"+sql);
            st            =   con.prepareStatement( sql );
            //ystem.out.println("id1"+id1);
            st.setString(1, login);
            st.setString(2, id1);

            rs = st.executeQuery();

            if (rs.next()){
                //ystem.out.println("un dato leido"+rs.getString("id"));

                AnticipoGasolina anticipoGasolina=new AnticipoGasolina();
                //anticipoGasolina.setDstrct(rs.getString("dstrct"));
                anticipoGasolina.setId(Integer.parseInt(rs.getString("id")));
                anticipoGasolina.setVlr(Double.parseDouble(rs.getString("vlr")));
                anticipoGasolina.setFecha_anticipo(rs.getString("fecha_anticipo"));
                anticipoGasolina.setPlanilla(rs.getString("planilla"));
                anticipoGasolina.setNombreConductor(rs.getString("nombre_conductor"));
                anticipoGasolina.setConductor(rs.getString("cedcon"));
                anticipoGasolina.setSupplier(rs.getString("supplier"));

                anticipoGasolina.setNombreAgencia(rs.getString("nombre_agencia"));
                anticipoGasolina.setPla_owner(rs.getString("pla_owner"));
                anticipoGasolina.setNombrePropietario(rs.getString("nombre_propietario"));
                anticipoGasolina.setVlrNeto(Double.parseDouble(rs.getString("vlr_neto_real")));
                anticipoGasolina.setAsesor(rs.getString("user_update"));
                anticipoGasolina.setVlr_efectivo(rs.getString("vlr_efectivo"));
                anticipoGasolina.setVlr_gasolina(rs.getString("vlr_gasolina"));

                respuesta= anticipoGasolina;
            }

        }}catch(Exception e){
            System.out.println("error en anticipoGasolinaDAO en getAnticipoGasolina..."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
         return respuesta;
    }

    public String getIdAnticipoGasolinaAceptado(){
        return idAnticipoGasolinaAceptado;
    }

    public String getValGasolinaAceptado(){
        return valGasolinaAceptado;
    }

    public String getValEfectivoAceptado(){
        return valEfectivoAceptado;
    }


/**
 *
 * @param seleccion
 * @param loginx
 * @param documento
 * @return
 * @throws Exception
 */
    public String facturarAnticipos(List seleccion,String loginx,String documento) throws Exception{
        String respuesta="mal";
        Connection         con     = null;
        PreparedStatement  st      = null;

        String             query   = "SQL_FACTURAR_ANTGAS";
        String idAntGas="";
        String consultaSQL ="";
        int affected=0 ;
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
            String sql    =   this.obtenerSQL( query );

            for (int i=0;i<seleccion.size();i++){
                idAntGas=""+((AnticipoGasolina)seleccion.get(i)).getId();
                //ystem.out.println("sql"+sql+"_"+idAntGas+"loginx"+loginx);
                st = con.prepareStatement( sql );
                st.setString(1, loginx);
                st.setString(2, loginx);
                st.setString(3,documento);
                st.setString(4, idAntGas);
                consultaSQL = consultaSQL + st.toString()+";";//Obtengo la query
                //affected =st.executeUpdate();
                //ystem.out.println("affected"+affected);
            }

           /* if (affected>0){
                respuesta=""+""+" Anticipo(s) de Estaci�n de Gasolina Facturado(s).";

            }else{
                respuesta="No hubo facturaci�n.";

            }*/

            }}catch(Exception e){
            System.out.println("error en anticipoGasolinaDAO en facturar..."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return consultaSQL;

    }



/**
 *
 * @param seleccion
 * @param loginx
 * @param comisionador
 * @return
 * @throws Exception
 */
    public String transferirAnticipos(List seleccion,String loginx,String comisionador) throws Exception{
        String respuesta="mal";
        Connection         con     = null;
        PreparedStatement  st      = null;

        String             query   = "SQL_TRANSFERIR_ANTGAS";
        String idAntGas="";
        int affected=0 ;
        String estado_comision="";
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            String sql    =   this.obtenerSQL( query );

            for (int i=0;i<seleccion.size();i++){
                idAntGas=""+((AnticipoGasolina)seleccion.get(i)).getId();
                //ystem.out.println("sql"+sql+"_"+idAntGas+"loginx"+loginx);
                st = con.prepareStatement( sql );
                st.setString(1, loginx);
                st.setString(2, loginx);

                if (comisionador.equals("Fintra")){
                    estado_comision="no asumio comision bancaria";
                }else{
                    estado_comision="asumio comision bancaria";
                }
                st.setString(3, estado_comision);

                st.setString(4, idAntGas);

                affected =st.executeUpdate();
                //ystem.out.println("affected"+affected);
            }

            if (affected>0){
                respuesta=""+""+" Anticipo(s) de Estaci�n de Gasolina Transferido(s).";

            }else{
                respuesta="No hubo transferencia.";

            }

        }catch(Exception e){
            System.out.println("error en anticipoGasolinaDAO en transferir..."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;

    }


    public String getVal(){
        return val;
    }

    public String getValNeto(){
        return valNeto;
    }


 /**
  *
  * @return
  * @throws Exception
  */
    public ArrayList getAnticiposGasolinaFacturados() throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_ANTGAS_FACT";
        ArrayList listAnticiposFacturadosGas=new ArrayList();
        boolean respuesta=false;
        try{

            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );

            rs = st.executeQuery();

            while (rs.next()){
                //ystem.out.println("un dato leido::"+rs.getString("id"));

                AnticipoGasolina anticipoGasolina=new AnticipoGasolina();
                //anticipoGasolina.setDstrct(rs.getString("dstrct"));
                anticipoGasolina.setId(Integer.parseInt(rs.getString("id")));
                anticipoGasolina.setVlr(Double.parseDouble(rs.getString("vlr")));
                anticipoGasolina.setFecha_anticipo(rs.getString("fecha_anticipo"));
                anticipoGasolina.setPlanilla(rs.getString("planilla"));
                anticipoGasolina.setNombreConductor(rs.getString("nombre"));
                anticipoGasolina.setConductor(rs.getString("cedcon"));
                anticipoGasolina.setSupplier(rs.getString("supplier"));
                anticipoGasolina.setVlrNeto(Double.parseDouble(rs.getString("vlr_neto")));
                anticipoGasolina.setNombrePropietario(rs.getString("nombre_propietario"));
                anticipoGasolina.setPla_owner(rs.getString("pla_owner"));
                anticipoGasolina.setUser_transferencia(rs.getString("user_transferencia"));
                anticipoGasolina.setFecha_transferencia(rs.getString("fecha_transferencia"));
                anticipoGasolina.setUser_autorizacion(rs.getString("user_autorizacion"));
                anticipoGasolina.setFecha_autorizacion(rs.getString("fecha_autorizacion"));
                listAnticiposFacturadosGas.add(anticipoGasolina);

            }

            }}catch(Exception e){
            System.out.println("error en anticipoGasolinaDAO en getAnticiposGasolinaFacturados..."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return listAnticiposFacturadosGas;

    }





    /**
     *
     * @param distrito
     * @param proveedor
     * @param ckAgencia
     * @param Agencia
     * @param ckPropietario
     * @param Propietario
     * @param ckPlanilla
     * @param Planilla
     * @param ckPlaca
     * @param Placa
     * @param ckConductor
     * @param Conductor
     * @param ckReanticipo
     * @param reanticipo
     * @param ckFechas
     * @param fechaIni
     * @param fechaFin
     * @param ckLiquidacion
     * @param Liquidacion
     * @param ckTransferencia
     * @param Transferencia
     * @param ckFactura
     * @param Factura
     * @return
     * @throws Exception
     */
    public  ArrayList consultarAnticipos( String distrito,        String proveedor,
    String ckAgencia,       String Agencia ,
    String ckPropietario,   String Propietario,
    String ckPlanilla,      String Planilla,
    String ckPlaca ,        String Placa,
    String ckConductor,     String Conductor,
    String ckReanticipo,    String reanticipo,
    String ckFechas,        String fechaIni,  String fechaFin,

    String ckLiquidacion,   String Liquidacion,
    String ckTransferencia, String Transferencia,
    String ckFactura,       String Factura

    ) throws Exception{

        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_CONSULTA_ANTICIPOS_GASOLINA";
        //List               lista   = new LinkedList();
        ArrayList               lista   = new ArrayList();
        try{

            String PAGADA = "50";

            String filtro = "";
            if( ckAgencia        !=null )  filtro += " AND a.agency_id  ='"+ Agencia     +"'";
            if( ckPropietario    !=null )  filtro += " AND a.pla_owner  ='"+ Propietario +"'";
            if( ckConductor      !=null )  filtro += " AND a.cedcon     ='"+ Conductor   +"'";
            if( ckPlaca          !=null )  filtro += " AND a.supplier   ='"+ Placa       +"'";
            if( ckPlanilla       !=null )  filtro += " AND a.planilla   ='"+ Planilla    +"'";
            if( ckReanticipo     !=null )  filtro += " AND a.reanticipo ='"+ reanticipo  +"'";
            if( ckFechas         !=null )  filtro += " AND a.fecha_autorizacion  BETWEEN     '"+  fechaIni  +" 00:00:00' and '"+  fechaFin +" 23:59:59'";
            if( ckLiquidacion    !=null )  filtro += " AND a.liquidacion   ='"+ Liquidacion    +"'";

            if (Placa!=null && Placa.equals("marca de estacion como agencia")){
                filtro += " AND a.user_autorizacion ='"+ Agencia    +"'";
            }

            //if( ckTransferencia  !=null )  filtro += " AND a.transferencia ='"+ Transferencia  +"'";
            if( ckTransferencia  !=null && (ckFactura==null || (Factura!=null && Factura.equals("N")))) {
                if (Transferencia.equals("N")){
                    filtro += " AND a.estado_pago_tercero !='A'";
                }else{
                    filtro += " AND a.estado_pago_tercero ='A'";
                }
            }
            //if( ckFactura        !=null )  filtro += " AND a.factura_mims  ='"+ Factura        +"'";
            if(ckFactura!=null ){
                if (Factura.equals("N")){
                    filtro += " AND a.estado_pago_tercero !='P'";
                }else{
                    filtro += " AND a.estado_pago_tercero ='P'";
                }
            }
            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
            String sql   =   this.obtenerSQL( query ).replaceAll("#COLUMNAS#", this.COLUMNAS).replaceAll("#FILTRO#",filtro);
            st= con.prepareStatement(sql);
            st.setString(1, distrito    );
            st.setString(2, proveedor   );

            rs=st.executeQuery();
            while(rs.next()){
                AnticipoGasolina anticipo = load(rs);
                anticipo.setCxpPosible(reset(rs.getString("posible_factura_tercero")));
                anticipo.setCxpPosible2(reset(rs.getString("posible_factura_tercero2")));
                anticipo.setCxp(reset(rs.getString("factura_tercero")));
                anticipo.setSumaAnticiposDplanilla(resetNumero(rs.getString("plata_anticipos_de_planilla")));
                anticipo.setSumaCxpsTspDplanilla(resetNumero(rs.getString("plata_cxp_de_planilla")));
                anticipo.setCxpTspPosible_corrida_cheque(reset(rs.getString("cxp_posible_corrida_cheque")));
                anticipo.setCxpTspPosible2_corrida_cheque(reset(rs.getString("cxp_posible2_corrida_cheque")));

                lista.add(  anticipo );

            }

        }}catch(Exception e){
            throw new Exception( "consultarAnticipos " + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }



    /**
     *
     * @param rs
     * @return
     * @throws Exception
     */
     public    AnticipoGasolina load(ResultSet  rs)throws Exception{
        AnticipoGasolina anticipo =  new  AnticipoGasolina();
        try{

            anticipo.setId                       (          rs.getInt   ("id")                          );
            anticipo.setDstrct                   (   reset( rs.getString("dstrct" )                   ) );
            anticipo.setAgency_id                (   reset( rs.getString("agency_id" )                ) );
            anticipo.setPla_owner                (   reset( rs.getString("pla_owner" )                ) );
            anticipo.setPlanilla                 (   reset( rs.getString("planilla" )                 ) );
            anticipo.setSupplier                 (   reset( rs.getString("supplier" )                 ) );
            anticipo.setProveedor_anticipo       (   reset( rs.getString("proveedor_anticipo" )       ) );
            anticipo.setConcept_code             (   reset( rs.getString("concept_code" )             ) );
            anticipo.setVlr                      (          rs.getDouble("vlr" )                        );
            anticipo.setVlr_for                  (          rs.getDouble("vlr_for" )                    );
            anticipo.setCurrency                 (   reset( rs.getString("currency" )                 ) );
            anticipo.setFecha_anticipo           (   resetFechas( rs.getString("fecha_anticipo" )           ) );

            anticipo.setAprobado                 (   reset( rs.getString("aprobado" )                 ) );
            anticipo.setFecha_autorizacion       (   resetFechas( rs.getString("fecha_autorizacion" )       ) );
            anticipo.setUser_autorizacion        (   reset( rs.getString("user_autorizacion" )        ) );

            anticipo.setTransferido              (   reset( rs.getString("transferido" )              ) );
            anticipo.setFecha_transferencia      (   resetFechas( rs.getString("fecha_transferencia" )      ) );
            anticipo.setBanco_transferencia      (   reset( rs.getString("banco_transferencia" )      ) );
            anticipo.setCuenta_transferencia     (   reset( rs.getString("cuenta_transferencia" )     ) );
            anticipo.setTcta_transferencia       (   reset( rs.getString("tcta_transferencia" )       ) );

            anticipo.setUser_transferencia       (   reset( rs.getString("user_transferencia" )       ) );

            anticipo.setBanco                    (   reset( rs.getString("banco" )                    ) );
            anticipo.setSucursal                 (   reset( rs.getString("sucursal" )                 ) );
            anticipo.setNombre_cuenta            (   reset( rs.getString("nombre_cuenta" )            ) );
            anticipo.setCuenta                   (   reset( rs.getString("cuenta" )                   ) );
            anticipo.setTipo_cuenta              (   reset( rs.getString("tipo_cuenta" )              ) );
            anticipo.setNit_cuenta               (   reset( rs.getString("nit_cuenta" )               ) );

            anticipo.setFecha_migracion          (   resetFechas( rs.getString("fecha_migracion" )          ) );
            anticipo.setUser_migracion           (   reset( rs.getString("user_migracion" )           ) );
            anticipo.setFactura_mims             (   reset( rs.getString("factura_mims" )             ) );

            anticipo.setEstado_pago_tercero      (   reset( rs.getString("estado_pago_tercero" )      ) );
            anticipo.setEstado_desc_propietario  (   reset( rs.getString("estado_desc_propietario" )  ) );
            anticipo.setFecha_pago_tercero       (   resetFechas( rs.getString("fecha_pago_tercero" )       ) );
            anticipo.setFecha_desc_propietario   (   resetFechas( rs.getString("fecha_desc_propietario" )   ) );
            anticipo.setCheque_pago_tercero      (   reset( rs.getString("cheque_pago_tercero" )      ) );
            anticipo.setCheque_desc_propietario  (   reset( rs.getString("cheque_desc_propietario" )  ) );
            anticipo.setCorrida_pago_tercero     (   reset( rs.getString("corrida_pago_tercero" )     ) );
            anticipo.setCorrida_desc_propietario(   reset( rs.getString("corrida_desc_propietario" ) ) );

            anticipo.setVlr_mims_tercero         (          rs.getDouble("vlr_mims_tercero" )           );
            anticipo.setVlr_mims_propietario     (          rs.getDouble("vlr_mims_propietario" )       );

            anticipo.setNombreAgencia            (   reset( rs.getString("nombre_agencia" )           ) );
            anticipo.setNombreProveedor          (   reset( rs.getString("nombre_proveedor" )         ) );
            anticipo.setNombrePropietario        (   reset( rs.getString("nombre_prpietario" )        ) );

            anticipo.setPorcentaje               (          rs.getDouble("porcentaje" )                 );
            anticipo.setVlrDescuento             (          rs.getDouble("vlr_descuento")               );
            anticipo.setVlrNeto                  (          rs.getDouble("vlr_neto")                    );
            anticipo.setVlrComision              (          rs.getDouble("vlr_combancaria")             );
            anticipo.setVlrConsignar             (          rs.getDouble("vlr_consignacion")            );

            anticipo.setReanticipo               (  reset( rs.getString("reanticipo" )                ) );

            anticipo.setConductor                (  reset( rs.getString("cedcon" )                    ) );
            anticipo.setNombreConductor          (  getNameNit(  anticipo.getConductor()              ) );


            anticipo.setTransferencia            (  reset( rs.getString("transferencia" )             ) );
            anticipo.setLiquidacion              (  reset( rs.getString("liquidacion" )               ) );
            anticipo.setSecuencia                (         rs.getInt   ("secuencia")                    );
            anticipo.setUsuario_creacion         (  reset( rs.getString("creation_user" )               ) );

            anticipo.setVlr_efectivo(rs.getString("vlr_efectivo"));
            anticipo.setVlr_gasolina(rs.getString("vlr_gasolina"));

        }catch(Exception e){
            throw new Exception( " load "+ e.getMessage());
        }
        return anticipo;
    }

    public String resetFechas(String val) throws Exception{
        if( val==null ||  val.equals("0099-01-01 00:00") )
            val = "";
        return val;
    }




/**
 *
 * @param nit
 * @return
 * @throws Exception
 */
    public String  getNameNit(String nit)throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_NAME_NIT";
        String            name        = "";
        try{
            con = this.conectarJNDI( query );//JJCastro fase2
            if(con!=null){
            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, nit  );
            st.setString(2, nit  );
            rs = st.executeQuery();
            if(rs.next())
                name = reset( rs.getString(1) );

        }}catch(Exception e) {
            throw new SQLException(" getNameNit  -> " +e.getMessage());
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return name;
    }


 /**
  *
  * @param login
  * @return
  * @throws Exception
  */
    public boolean existeEstacion(String login) throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_EXISTE_NIT_ESTACION";
        String            name        = "";
        boolean respuesta=false;
        try{
            con = this.conectarJNDI( query );//JJCastro fase2
            if(con!=null){
            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, login  );
            rs = st.executeQuery();
            if(rs.next()){
                respuesta=true;
            }

        }}catch(Exception e) {
            throw new SQLException(" existeEstacion -> "+login+"___" +e.getMessage());
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;

    }



 /**
  *
  * @param estacion
  * @return
  * @throws Exception
  */
    public String saveEstacion(Hashtable  estacion) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st          = null;
        String            query       = "SQL_SAVE_ESTACION_GAS";
        String            msj         = "Registro guardado...";
        try{
            String nombre = (String) estacion.get("nombre");
            //String desc      = (String) cuenta.get("descuento");
            //String defaultCTA =  (String) cuenta.get("principal");

            nombre =  nombre.toUpperCase().replaceAll("�","N");

            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,  (String) estacion.get("nit")   );
            st.setString(2,  (String) estacion.get("tel")     );
            //st.setString(3,  (String) estacion.get("lugar")        );
            st.setString(3,  (String) estacion.get("dir")      );
            st.setString(4,  (String) estacion.get("id")   );
            st.setString(5,  (String) estacion.get("tipo_id")     );
            st.setString(6,  nombre      );
            st.setString(7,  (String) estacion.get("e_mail") );
            //st.setString(8, (String) estacion.get("descuento")       );
            st.setString(8, (String) estacion.get("c_origen")       );
            st.setString(9, (String) estacion.get("c_paiso")       );
            st.setString(10, (String) estacion.get("nombre_estacion")       );
            st.setString(11, (String) estacion.get("login_estacion")       );
            st.execute();

            }}catch(Exception e) {
             msj = e.getMessage();
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return msj;
    }



/**
 *
 * @param estacion
 * @return
 * @throws Exception
 */
    public String updateEstacion(Hashtable  estacion) throws Exception{
        Connection con = null;
        PreparedStatement st          = null;
        String            query       = "SQL_UPDATE_ESTACION_GAS";
        String            msj         = "Registro actualizado...";
        try{
            String nombre = (String) estacion.get("nombre");
            //String desc      = (String) cuenta.get("descuento");
            //String defaultCTA =  (String) cuenta.get("principal");

            nombre =  nombre.toUpperCase().replaceAll("�","N");

            con = this.conectarJNDI(query);
            if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2

            st.setString(1,  (String) estacion.get("tel")     );
            //st.setString(3,  (String) estacion.get("lugar")        );
            st.setString(2,  (String) estacion.get("dir")      );
            st.setString(3,  (String) estacion.get("id")   );
            st.setString(4,  (String) estacion.get("tipo_id")     );
            st.setString(5,  nombre      );
            st.setString(6,  (String) estacion.get("e_mail") );
            //st.setString(7, (String) estacion.get("descuento")       );
            st.setString(7, (String) estacion.get("c_origen")       );
            st.setString(8, (String) estacion.get("c_paiso")       );
            st.setString(9, (String) estacion.get("nombre_estacion")       );
            st.setString(10, (String) estacion.get("nit")       );
            st.setString(11,  (String) estacion.get("login_estacion")   );

            st.execute();

            }}catch(Exception e) {
             msj = e.getMessage();
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return msj;
    }



/**
 *
 * @return
 * @throws Exception
 */
    public ArrayList listarEstaciones() throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_ESTACIONES";
        ArrayList listEstaciones=new ArrayList();
        String respuesta="";

        try{

            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){

            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );

            rs = st.executeQuery();

            while (rs.next()){
                //ystem.out.println("un dato leido::"+rs.getString("id_responsable"));

                Hashtable ht = new Hashtable();

                ht.put("nit",      rs.getString("nit")  );
                ht.put("tel",      rs.getString("tel")  );
                ht.put("dir",      rs.getString("dir")  );
                ht.put("id",      rs.getString("id_responsable")  );
                ht.put("nombre",      rs.getString("nombre_responsable")  );
                ht.put("tipo_id",      rs.getString("tipo_id_responsable")  );
                ht.put("e_mail",      rs.getString("mail")  );
                ht.put("descuento",   resetNumero(   rs.getString("descuento") ) );

                //ht.put("reg_status",      rs.getString("reg_status")  );
                ht.put("ciudad",      rs.getString("codciu")  );
                ht.put("pais",      rs.getString("country_code")  );
                ht.put("nombre_estacion",      rs.getString("nombre_estacion")  );
                ht.put("login_estacion",      rs.getString("login")  );
                listEstaciones.add(ht);
                 ht = null;//Liberar Espacio JJCastro
            }

        }}catch(Exception e){
            System.out.println("error en anticipoGasolinaDAO en listarEstaciones..."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        respuesta="Consulta terminada";
        return listEstaciones;

    }


    /**
     *
     * @param login_usuario
     * @return
     * @throws Exception
     */
    public boolean isLogin(String login_usuario) throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_SEARCH_LOGIN";
        String            name        = "";
        boolean respuesta=false;
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, login_usuario  );
            rs = st.executeQuery();
            if(rs.next())
                respuesta=true;

        }}catch(Exception e) {
            throw new SQLException(" isLogin  -> " +e.getMessage());
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;
    }




/**
 *
 * @param login_usuario
 * @param descuento_login
 * @param usuario
 * @return
 * @throws Exception
 */
    public String asignarLoginDescuento(String login_usuario,String descuento_login,String usuario) throws Exception{
        Connection con = null;
        String respuesta="error";
        PreparedStatement st          = null;
        String            query       = "SQL_UPDATE_LOGINDESCUENTO";

        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                                if (existeLoginDescuento(login_usuario)){
                                                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                                                st.setString(1, descuento_login );
                                                st.setString(2,  usuario    );
                                                st.setString(3,  login_usuario);

                                                int actualizados=st.executeUpdate();
                                                if (actualizados==1){
                                                    respuesta="actualizacion exitosa.";
                                                }

                                }else{
                                                query       = "SQL_INSERT_DESCUENTOLOGIN";
                                                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2

                                                st.setString(1,  login_usuario );
                                                st.setString(2, descuento_login);
                                                st.setString(3  ,  usuario    );
                                                st.setString(4  ,  usuario    );

                                                int actualizados=st.executeUpdate();
                                                if (actualizados==1){
                                                    respuesta="insercion exitosa.";
                                                }
                                }

            }}catch(Exception e) {
            System.out.println("error en asignarLoginDescuento dao__"+e.toString()+"__"+e.getMessage());
            respuesta = e.getMessage();
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return respuesta        ;
    }


/**
 *
 * @param login_usuario
 * @return
 * @throws Exception
 */
    public boolean existeLoginDescuento(String login_usuario) throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_SEARCH_DESCUENTOLOGIN";

        boolean respuesta=false;
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, login_usuario  );
            rs = st.executeQuery();
            if(rs.next())
                respuesta=true;

            }}catch(Exception e) {
            throw new SQLException(" existeLoginDescuento  -> " +e.getMessage());
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;
    }


/**
 *
 * @param login_usuario
 * @return
 * @throws Exception
 */
    public boolean existeParLoginDescuento(String login_usuario)  throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_SEARCH_DESCUENTOLOGIN";
        String            name        = "";
        boolean respuesta=false;
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, login_usuario  );
            rs = st.executeQuery();
            if(rs.next())
                respuesta=true;

            }}catch(Exception e) {
            throw new SQLException(" existeParLoginDescuento en dao  -> " +e.getMessage());
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;
    }



 /**
  *
  * @param preanticipos1
  * @param loginx
  * @return
  * @throws Exception
  */
    public boolean aceptarPreanticipos(ArrayList preanticipos1,String loginx)  throws Exception{
        Connection con = null;
        PreparedStatement st          = null;
        String            query       = "SQL_INSERT_PREANTICIPO";
        int respuesta=0;
        boolean ansuer=false;
        try {
            con = this.conectarJNDI(query);
            if (con != null) {

                String[] preant = new String[2];
                for (int i = 0; i < preanticipos1.size(); i++) {
                    preant = (String[]) preanticipos1.get(i);
                    st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                    st.setString(1, preant[0]);

                    st.setString(2, preant[1]);
                    st.setString(3, loginx);
                    st.setString(4, loginx);
                    //ystem.out.println("antes de execute");
                    respuesta = st.executeUpdate();
                    //ystem.out.println("despues de execute"+respuesta);
                }
                if (respuesta >= 1) {
                    ansuer = true;
                }
            }
        }catch(Exception e) {
             System.out.println("error aceptarPreanticipos dao:"+e.toString());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return ansuer;

    }





    public  ArrayList consultarPreanticipos( String fechaIni,  String fechaFin,String loginx) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_CONSULTA_PREANTICIPOS";

        ArrayList               lista   = new ArrayList();
        fechaFin=fechaFin+" 23:59:59";
        try{

            //String filtro = "";
            //filtro += " WHERE a.fecha_autorizacion  BETWEEN     '"+  fechaIni  +" 00:00:00' and '"+  fechaFin +" 23:59:59'";

            con = this.conectarJNDI(query);
            if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, fechaIni  );
            st.setString(2, fechaFin  );
            //st.setString(3, loginx );

            rs=st.executeQuery();
            while(rs.next()){
                String[] preanticipo=new String[8];
                //ystem.out.println("rs.getString(cedcon)"+rs.getString("cedcon"));
                preanticipo[0]=rs.getString("cedcon");
                preanticipo[1]=rs.getString("val");
                preanticipo[2]=rs.getString("creation_date");
                preanticipo[3]=rs.getString("creation_user");
                preanticipo[4]=rs.getString("estado_pago");
                preanticipo[5]=rs.getString("id");
                preanticipo[6]=rs.getString("planilla");
                if (preanticipo[6]==null ){
                    preanticipo[6]="";
                }
                preanticipo[7]=rs.getString("supplier");
                if (preanticipo[7]==null ){
                    preanticipo[7]="";
                }
                //preanticipo[4]=rs.getString("anticipo_gas");
                lista.add(preanticipo);

            }

            }}catch(Exception e){
            System.out.println("en consultarpreAnticipos"+e.toString()+"_"+e.getMessage());
            throw new Exception( "consultarpreAnticipos " + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }


/**
 *
 * @param fechaIni
 * @param fechaFin
 * @param loginx
 * @return
 * @throws Exception
 */
    public  ArrayList consultarPreanticiposAnulables( String fechaIni,  String fechaFin,String loginx) throws Exception{

        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_CONSULTA_PREANTICIPOS_ANULABLES";

        ArrayList               lista   = new ArrayList();
        fechaFin=fechaFin+" 23:59:59";
        try{

            //String filtro = "";
            //filtro += " WHERE a.fecha_autorizacion  BETWEEN     '"+  fechaIni  +" 00:00:00' and '"+  fechaFin +" 23:59:59'";

            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, fechaIni  );
            st.setString(2, fechaFin  );
            st.setString(3, loginx );

            rs=st.executeQuery();
            while(rs.next()){
                String[] preanticipo=new String[6];
                //ystem.out.println("rs.getString(cedcon)"+rs.getString("cedcon"));
                preanticipo[0]=rs.getString("cedcon");
                preanticipo[1]=rs.getString("val");
                preanticipo[2]=rs.getString("creation_date");
                preanticipo[3]=rs.getString("creation_user");
                preanticipo[4]=rs.getString("estado_pago");
                preanticipo[5]=rs.getString("oid");
                //preanticipo[4]=rs.getString("anticipo_gas");
                lista.add(preanticipo);

            }

            }}catch(Exception e){
            System.out.println("en consultarpreAnticiposAnulables"+e.toString()+"_"+e.getMessage());
            throw new Exception( "consultarpreAnticiposAnulables " + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }




 /**
  *
  * @param conduc
  * @return
  * @throws Exception
  */
    public  ArrayList consultarPreAnticiposConductor( String conduc) throws Exception{

        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_CONSULTA_PREANTICIPOS_CONDUC";

        ArrayList               lista   = new ArrayList();

        try{

            //String filtro = "";
            //filtro += " WHERE a.fecha_autorizacion  BETWEEN     '"+  fechaIni  +" 00:00:00' and '"+  fechaFin +" 23:59:59'";

            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, conduc );
            st.setString(2, conduc );

            rs=st.executeQuery();
            while(rs.next()){
                String[] preanticipo=new String[5];
                //ystem.out.println("rs.getString(cedcon)"+rs.getString("cedcon"));
                preanticipo[0]=rs.getString("cedcon");
                preanticipo[1]=rs.getString("val");
                preanticipo[2]=rs.getString("creation_date");
                preanticipo[3]=rs.getString("creation_user");
                preanticipo[4]=rs.getString("totpre");

                lista.add(preanticipo);

            }

            }}catch(Exception e){
            System.out.println("en consultarpreAnticiposCond"+e.toString()+"_"+e.getMessage());
            throw new Exception( "consultarpreAnticiposCond " + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }



 /**
  *
  * @return
  * @throws Exception
  */
    public java.util.TreeMap getTreeMapEstaciones() throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_ESTACIONES";
        TreeMap Cust = new TreeMap();
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = ps.executeQuery();
                while (rs.next()) {
                    Cust.put(rs.getString(1), rs.getString(2));
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
       return Cust;
    }



 /**
  *
  * @param estaci
  * @return
  * @throws Exception
  */
    public ArrayList getAnticiposGasolinaTransferibles(String estaci) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_ANTGAS_TRANSF";
        listAnticiposTransferidosGas=new ArrayList();
        boolean respuesta=false;
        try{

            con = this.conectarJNDI(query);//JJCastro fase2

            String sql    =   this.obtenerSQL( query );

            if (estaci!=null && !(estaci.equals(""))){
                sql=sql.replaceAll("WHERE","WHERE (user_autorizacion='"+estaci+
                    "' OR user_autorizacion IN (SELECT despachador FROM fin.despachador_estacion WHERE estacion='"+estaci+"')) AND ");
            }

            st            =   con.prepareStatement( sql );

            rs = st.executeQuery();
            String estacionx="";
            String autorizadorx="";
            while (rs.next()){
                //ystem.out.println("un dato leido::"+rs.getString("id"));

                AnticipoGasolina anticipoGasolina=new AnticipoGasolina();
                //anticipoGasolina.setDstrct(rs.getString("dstrct"));
                anticipoGasolina.setId(Integer.parseInt(rs.getString("id")));
                anticipoGasolina.setVlr(Double.parseDouble(rs.getString("vlr")));
                anticipoGasolina.setFecha_anticipo(rs.getString("fecha_anticipo"));
                anticipoGasolina.setPlanilla(rs.getString("planilla"));
                anticipoGasolina.setNombreConductor(rs.getString("nombre"));
                anticipoGasolina.setConductor(rs.getString("cedcon"));
                anticipoGasolina.setSupplier(rs.getString("supplier"));
                anticipoGasolina.setVlrNeto(Double.parseDouble(rs.getString("vlr_neto")));
                anticipoGasolina.setNombrePropietario(rs.getString("nombre_propietario"));
                anticipoGasolina.setPla_owner(rs.getString("pla_owner"));
                autorizadorx=rs.getString("user_autorizacion");
                anticipoGasolina.setUser_autorizacion(autorizadorx);
                anticipoGasolina.setFecha_autorizacion(rs.getString("fecha_autorizacion"));
                estacionx=rs.getString("estacion");
                if (estacionx==null || estacionx.equals("")){
                    estacionx=autorizadorx;
                }
                anticipoGasolina.setAsesor(estacionx);
                listAnticiposTransferidosGas.add(anticipoGasolina);

            }

        }catch(Exception e){
            System.out.println("error en anticipoGasolinaDAO en getAnticiposGasolinaTransf..."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return listAnticiposTransferidosGas;

    }



    public String AnularPreAnticipos(String[] preanticiposaanular) throws Exception{
        String respuesta="error";
        PreparedStatement st          = null;
        String            query       = "SQL_ANULAR_PREANTICIPOS";
        Connection         con     = null;
        String conjunto_preanticipos="0'";
        if (preanticiposaanular.length>0){
            conjunto_preanticipos="'"+preanticiposaanular[0]+"'";
        }

        for (int i=1;i<preanticiposaanular.length;i++){
            conjunto_preanticipos=conjunto_preanticipos+",'"+preanticiposaanular[i]+"'";

        }

        try{
                con = this.conectarJNDI(query);//JJCastro fase2
                if(con!=null){

                String sql    =   this.obtenerSQL( query );
                sql=sql.replaceAll("anulables", conjunto_preanticipos);

                //st = this.crearPreparedStatement(query);
                st            =   con.prepareStatement( sql );

                int actualizados=st.executeUpdate();
                if (actualizados>=1){
                    respuesta="Anulaci�n exitosa.";
                }else{
                    respuesta="Anulaci�n errada.";
                }

        }}catch(Exception e) {
            System.out.println("error en AnularPreAnticipos dao__"+e.toString()+"__"+e.getMessage());
            respuesta = e.getMessage();
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return respuesta        ;
    }

     //jemartinez
     /**
     *@param id identificador del anticipo
     */
    private final static String SQL_CALCULAR_VALORES = "SQL_CALCULAR_VALORES";
    public String obtenerValoresAnticipos(String id) throws Exception{
        Connection con = null;
        PreparedStatement state = null;
        ResultSet result = null;
        String valores = "";
        String query = "SQL_CALCULAR_VALORES";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            state = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            state.setString(1,id);
            result = state.executeQuery();
            if(result.next()){
                valores = result.getString(1)+";_"+result.getString(2)+";_"+result.getString(3);
            }
            //ystem.out.println("Exito!! "+valores);
        }}catch(Exception e){
            throw new Exception("DAO: "+e.getMessage());
        }finally{//JJCastro fase2
            if (result  != null){ try{result.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (state  != null){ try{ state.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return valores;
    }

    private static final String INSERTAR_CXP_ITEM = "INSERTAR_CXP_ITEM";
    private static final String INSERTAR_CXP_DOC = "INSERTAR_CXP_DOC";
    private static final String VALOR_PROVISION_CXP_DOC = "VALOR_PROVISION_CXP_DOC";
    private static final String ACTUALIZAR_CXP_DOC = "ACTUALIZAR_CXP_DOC";


    /**
     *
     *@param anticipos lista con todos los anticipos a los que se generara una CXP
     *@param documento Numero de la factura
     *@return la consulta sql
     *@throws Exception si ocurre un error en la base de datos
     * belleza6
     */
   /* public String setCxpItems(String[] anticipos, String documento, String login)throws java.lang.Exception{
        PreparedStatement st = null;
        String sql = "";
        for(int i=0;i<anticipos.length;i++) {
            String[] temp = anticipos[i].split(";_");
            //item;_descripcion;_estacion;_valor
            //ystem.out.println("Desde items doc: "+anticipos[i]);
            try{
                st = this.crearPreparedStatement(INSERTAR_CXP_ITEM);
                st.setString(1,temp[2]);//Proveedor
                st.setString(2,documento );//Documento
                if(i==0){
                     st.setString(3, "001");//Item
                }else{
                     st.setString(3,temp[0]);
                }
                st.setString(4, temp[1]);//Descripcion
                st.setDouble(5, Double.parseDouble(temp[3]));//Valor
                st.setDouble(6, Double.parseDouble(temp[3]));//Valor moneda Extranjera
                st.setString(7,login); //User Update
                st.setString(8,login);// Creation User
                sql = sql + st.toString()+";";
                //executeUpdate();
                //ystem.out.println("Exito insertando items");
            }catch(java.lang.Exception e){
                throw new java.lang.Exception("DAO: "+e.toString());
            }finally{
                if(st!=null) st.close();
                this.desconectar(INSERTAR_CXP_ITEM);
            }
        }
        return sql;
    }*/


    /**
     *
     *@param anticipos lista con todos los anticipos a los que se generara una CXP
     *@param documento Numero de la factura
     *@return la consulta sql
     *@throws Exception si ocurre un error en la base de datos
  */
        public String setCxpItems(String[] anticipos, String documento, String login)throws java.lang.Exception{
        StringStatement st = null;
        String sql = "";
        String query = "INSERTAR_CXP_ITEM";
        String consulta = this.obtenerSQL(query);
        try{

        for(int i=0;i<anticipos.length;i++) {

                                    String[] temp = anticipos[i].split(";_");
                                    st = new StringStatement (consulta, true);//JJCastro fase2
                                    st.setString(1,temp[2]);//Proveedor
                                    st.setString(2,documento );//Documento
                                    if(i==0){
                                         st.setString(3, "001");//Item
                                    }else{
                                         st.setString(3,temp[0]);
                                    }
                                    st.setString(4, temp[1]);//Descripcion
                                    st.setDouble(5, Double.parseDouble(temp[3]));//Valor
                                    st.setDouble(6, Double.parseDouble(temp[3]));//Valor moneda Extranjera
                                    st.setString(7,login); //User Update
                                    st.setString(8,login);// Creation User

                                    st.setString(9,temp[0]);// id d anticipo 20101021

                                    sql = sql + st.getSql()+";";//JJCastro fase2
                                    if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}

        }}catch(java.lang.Exception e){
                                    throw new java.lang.Exception("DAO: "+e.toString());
            }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return sql;
    }


    //item;_descripcion;_estacion;_valor

/**
 *
 * @param informacion
 * @param documento
 * @param login
 * @param sumaAnticipos
 * @return
 * @throws java.lang.Exception
 */
    public String setCxpDoc(String informacion, String documento, String login, double sumaAnticipos) throws java.lang.Exception{
        StringStatement state = null;
        String[] temp = informacion.split(";_");
        String sql = "";
        String query = "INSERTAR_CXP_DOC";
        //ystem.out.println("desde cxp doc: "+informacion);
        try{
            state  = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            state.setString(1,temp[2]);//NIT PROVEEDOR
            state.setString(2,documento);//FACTURA FENALCO
            state.setString(3,"Anticipo Gasolina ( estaci�n "+temp[2]+" )");//DESCRIPCION
            state.setString(4,login);//USUARIO
            state.setDouble(5, sumaAnticipos);//SUMA PROVISION
            state.setDouble(6,sumaAnticipos);//SUMA PROVISION
            state.setDouble(7,sumaAnticipos);//SUMA PROVISION
            state.setDouble(8,sumaAnticipos);//SUMA PROVISION
            state.setDouble(9,sumaAnticipos);//SUMA PROVISION
            state.setDouble(10,sumaAnticipos);//SUMA PROVISION
            state.setString(11,login);//USUARIO
            state.setString(12,login);//USUARIO
            sql = sql + state.getSql()+";";
        }catch(java.lang.Exception e){
            throw new java.lang.Exception("DAO: "+e.toString());
        }finally{//JJCastro fase2
            if (state != null){ try{ state = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return sql;
    }

    /**
     * Obtiene el valor hasta el momento de una CXP_DOC
     * @param noFactura numero de la factura
     * @return informacion con el valor
     * @throws Exception si ocurre un error en la base de datos
     */

    public String obtenerValorCxpDoc(String noFactura) throws java.lang.Exception{
        Connection con = null;
        PreparedStatement state = null;
        ResultSet result = null;
        String informacion = null;
        String query = "VALOR_PROVISION_CXP_DOC";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            state = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            state.setString(1, noFactura);
            result = state.executeQuery();
            if(result.next()){
                informacion = result.getString(1);
            }

        }}catch(java.lang.Exception e){
            throw new java.lang.Exception("DAO: "+e.toString());
        }finally{//JJCastro fase2
            if (result  != null){ try{ result.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (state != null){ try{ state.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return informacion;
    }

    ///Miguel Altamiranda///
/**
 * Creado por: Miguel Altamiranda
 * @param conductor1
 * @param placa1
 * @param login1
 * @param planilla
 * @return
 * @throws Exception
 */
    public boolean isAnticipoGasolina2(String conductor1,String placa1,String login1, String planilla) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_ANTGAS2";
        anticiposGasolina=new ArrayList();
        boolean respuesta=false;
        try{

            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, login1);
            st.setString(2, conductor1);
            st.setString(3, placa1);
            st.setString(4, planilla);

            rs = st.executeQuery();

            while (rs.next()){
                //ystem.out.println("un dato leido::"+rs.getString("id"));

                AnticipoGasolina anticipoGasolina=new AnticipoGasolina();
                //anticipoGasolina.setDstrct(rs.getString("dstrct"));
                anticipoGasolina.setId(Integer.parseInt(rs.getString("id")));
                anticipoGasolina.setVlr(Double.parseDouble(rs.getString("vlr")));
                anticipoGasolina.setFecha_anticipo(rs.getString("fecha_anticipo"));
                anticipoGasolina.setPlanilla(rs.getString("planilla"));
                anticipoGasolina.setNombreConductor(rs.getString("nombre"));
                anticipoGasolina.setConductor(rs.getString("cedcon"));
                anticipoGasolina.setSupplier(rs.getString("supplier"));

                anticipoGasolina.setVlrNeto(Double.parseDouble(rs.getString("vlr_neto_real")));
                anticipoGasolina.setNombrePropietario(rs.getString("nombre_propietario"));
                anticipoGasolina.setPla_owner(rs.getString("pla_owner"));
                anticiposGasolina.add(anticipoGasolina);
                respuesta= true;
            }

            }}catch(Exception e){
            System.out.println("error en anticipoGasolinaDAO en isAnti..."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;

    }


 /**
  *
  * @param conductor1
  * @param placa1
  * @param login1
  * @param planilla
  * @return
  * @throws Exception
  */
    public boolean isAnticipoGasolina3(String conductor1,String placa1,String login1, String planilla) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_SEARCH_ANTGAS3";
        anticiposGasolina = new ArrayList();
        boolean respuesta = false;
        try {

            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, conductor1);
                st.setString(2, placa1);
                st.setString(3, planilla);

                rs = st.executeQuery();

                while (rs.next()) {
                    //ystem.out.println("un dato leido::"+rs.getString("id"));

                    AnticipoGasolina anticipoGasolina = new AnticipoGasolina();
                    anticipoGasolina.setUser_autorizacion(rs.getString("user_autorizacion"));
                    anticipoGasolina.setFecha_autorizacion(rs.getString("fecha"));
                    anticipoGasolina.setVlrNeto(rs.getDouble("vlr_neto"));
                    anticipoGasolina.setNombreProveedor(rs.getString("nombre_estacion"));
                    anticiposGasolina.add(anticipoGasolina);
                    respuesta = true;
                }

            }
        }catch(Exception e){
            System.out.println("error en anticipoGasolinaDAO en isAnti..."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
         return respuesta;
    }
    ///Fin Miguel Altamiranda///


        /**
     * Consulta con el login del usuario los datos de estacion
     * @param usuario Login del usuario
     * @return TreeMap con los datos
     * @throws Exception Cuando hay error
     */
    public TreeMap treeMapEstacionesUsuario(String usuario) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String  Query = "SQL_ESTACION_US";
        TreeMap Cust = new TreeMap();
        Connection con = null;
        String sql = "";
        try{
            sql = this.obtenerSQL(Query);
            con = this.conectarJNDI(Query);
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario);
            rs = ps.executeQuery();
            while (rs.next()){
                Cust.put(rs.getString(1), rs.getString(2));
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        finally{
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
            this.desconectar(con);
        }
       return Cust;
    }

    /**
     * Consulta si el login del usuario es de una estacion
     * @param usuario Login del usuario
     * @return Si es o no usuario de estacion
     * @throws Exception Cuando hay error
     */
    public boolean esEstacionUsuario(String usuario) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String Query = "SQL_ESTACION_US";
        boolean es = false;
        Connection con = null;
        String sql = "";
        try{
            con = this.conectarJNDI(Query);
            sql = this.obtenerSQL(Query);
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario);
            rs = ps.executeQuery();
            while (rs.next()){
                es = true;
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        finally{
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
            this.desconectar(con);
        }
       return es;
    }

    /**
     * Genera el consecutivo para la cxp automatica
     * @param seriecode Nombre del document_type en la tabla de series
     * @return Cadena con el codigo generado
     * @throws Exception Cuando hay error
     */
    public String codigoCXPAuto(String seriecode) throws Exception{
        String cod = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String Query = "SQL_COD_CXP";
        Connection con = null;
        String sql = "";
        try{
            con = this.conectarJNDI(Query);
            sql = this.obtenerSQL(Query);
            ps = con.prepareStatement(sql);
            ps.setString(1, seriecode);
            rs = ps.executeQuery();
            while (rs.next()){
                cod = rs.getString(1);
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        finally{
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
            this.desconectar(con);
        }
        return cod;
    }

    /**
     * Busca datos de una estacion
     * @param login_estacion login de la estacion
     * @return Objeto BeanGeneral con los datos encontrados
     * @throws Exception Cuando hay error
     */
    public BeanGeneral datosEstacion(String login_estacion) throws Exception{
        BeanGeneral bean = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_DAT_EST";
        Connection con = null;
        String sql = "";
        try{
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, login_estacion);
            rs = ps.executeQuery();
            if (rs.next()){
                bean = new BeanGeneral();
                bean.setValor_01(rs.getString("nit"));
                bean.setValor_02(rs.getString("tel"));
                bean.setValor_03(rs.getString("dir"));
                bean.setValor_04(rs.getString("id_responsable"));
                bean.setValor_05(rs.getString("nombre_responsable"));
                bean.setValor_06(rs.getString("codciu"));
                bean.setValor_07(rs.getString("nombre_estacion"));
                bean.setValor_08(rs.getString("payment_name"));
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        finally{
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
            this.desconectar(con);
        }
        return bean;
    }


}
