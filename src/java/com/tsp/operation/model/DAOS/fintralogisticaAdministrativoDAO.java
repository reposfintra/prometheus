/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.fintralogisticaAdministrativoBeans;
import java.util.ArrayList;

/**
 *
 * @author mariana
 */
public interface fintralogisticaAdministrativoDAO {
    
    /**
     *
     * @param ventas
     * @param planilla
     * @param corrida
     * @param fecha_corrida
     * @param empresa
     * @param ingreso
     * @return
     */
    public ArrayList<fintralogisticaAdministrativoBeans>cargarReporte(String ventas,String planilla,String corrida,String fecha_corrida,String empresa,String ingreso);
    
    /**
     *
     * @param empresa
     * @param usuario
     * @return
     */
    public ArrayList<fintralogisticaAdministrativoBeans>generarIAPendientes(String empresa,Usuario usuario);
    
   
    
}
