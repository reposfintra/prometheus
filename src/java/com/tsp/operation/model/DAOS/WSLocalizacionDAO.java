package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.Direccion;
import com.tsp.operation.model.beans.EmailLocalizacion;
import com.tsp.operation.model.beans.Entidad;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Telefono;
import com.tsp.util.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * DAO para las tablas del web service de localizacion RECONOCER+ de datacredito<br/>
 * 1/09/2011<br/>
 * @author darrieta - GEOTECH SOLUTIONS S.A.
 */
public class WSLocalizacionDAO extends MainDAO {
    
    public WSLocalizacionDAO() {
        super("WSLocalizacionDAO.xml");
    }
    public WSLocalizacionDAO(String dataBaseName) {
        super("WSLocalizacionDAO.xml", dataBaseName);
    }

    /**
     * Elimina los registros de una persona
     * @param identificacion identificacion de la persona
     * @param tipoIdentificacion tipo de identificacion
     * @return query generado
     * @throws Exception
     */
    public String eliminarDirecciones(String identificacion, String tipoIdentificacion) throws Exception{
        StringStatement st = null;
        String query = "ELIMINAR_DIRECCIONES";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, identificacion);
            st.setInt(2, Integer.parseInt(tipoIdentificacion));
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en eliminarDirecciones() -->> [WSLocalizacionDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new Exception("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }

    /**
     * Elimina los registros de una persona
     * @param identificacion identificacion de la persona
     * @param tipoIdentificacion tipo de identificacion
     * @return query generado
     * @throws Exception
     */
    public String eliminarTelefonos(String identificacion, String tipoIdentificacion) throws Exception{
        StringStatement st = null;
        String query = "ELIMINAR_TELEFONOS";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, identificacion);
            st.setInt(2, Integer.parseInt(tipoIdentificacion));
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en eliminarTelefonos() -->> [WSLocalizacionDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new Exception("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }

    /**
     * Elimina los registros de una persona
     * @param identificacion identificacion de la persona
     * @param tipoIdentificacion tipo de identificacion
     * @return query generado
     * @throws Exception
     */
    public String eliminarEmails(String identificacion, String tipoIdentificacion) throws Exception{
        StringStatement st = null;
        String query = "ELIMINAR_EMAILS";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, identificacion);
            st.setInt(2, Integer.parseInt(tipoIdentificacion));
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en eliminarEmails() -->> [WSLocalizacionDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new Exception("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }

    /**
     * Vefifica la existencia de una persona en la bd
     * @param entidad bean con la entidad a buscar
     * @return true si existe la entidad o false si no existe
     * @throws Exception
     */
    public boolean existeEntidad(Entidad entidad) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "BUSCAR_ENTIDAD";
        boolean existe = false;
        try{
            con = this.conectarJNDI(query);
            if ( con != null ){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, entidad.getCodSuscriptor());
                rs = st.executeQuery();
                if(rs.next()){
                    existe = true;
                }
            }
        }catch(Exception e){
            throw new Exception("ERROR en existeEntidad() -->> [WSLocalizacionDAO] " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new Exception("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new Exception("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(Exception e){ throw new Exception("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return existe;
    }

    /**
     * Inserta una entidad en la tabla
     * @param entidad informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarEntidad(Entidad entidad) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("codigo_suscriptor");
            valores.add("'"+Util.escapeSQL(entidad.getCodSuscriptor())+"'");
            if(entidad.getNombreSuscriptor()!=null){
                campos.add("nombre_suscriptor");
                valores.add("'"+Util.escapeSQL(entidad.getNombreSuscriptor())+"'");
            }
            if(entidad.getNit()!=null){
                campos.add("nit");
                valores.add("'"+Util.escapeSQL(entidad.getNit())+"'");
            }
            if(entidad.getContrato()!=null){
                campos.add("contrato");
                valores.add("'"+Util.escapeSQL(entidad.getContrato())+"'");
            }
            if(entidad.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(entidad.getCreationUser())+"'");
            }
            if(entidad.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(entidad.getUserUpdate())+"'");
            }

            sql = "insert into wsdc.entidad(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";

        }catch(Exception ex){
            throw new Exception("ERROR en insertarEntidad() -->> [WSLocalizacionDAO] "+ex.getMessage());
        }
        return sql;
    }


    /**
     * Edita la informacion de una entidad
     * @param entidad
     * @return
     * @throws Exception
     */
    public String editarEntidad(Entidad entidad) throws Exception{
        StringStatement st = null;
        String query = "UPDATE_ENTIDAD";
        String sql;
        ArrayList campos = new ArrayList();

        try{

            if(entidad.getNombreSuscriptor()!=null){
                campos.add("nombre_suscriptor='"+Util.escapeSQL(entidad.getNombreSuscriptor())+"'");
            }
            if(entidad.getNit()!=null){
                campos.add("nit='"+Util.escapeSQL(entidad.getNit().toString())+"'");
            }
            if(entidad.getContrato()!=null){
                campos.add("contrato='"+Util.escapeSQL(entidad.getContrato())+"'");
            }

            campos.add("user_update='"+entidad.getUserUpdate()+"'");
            campos.add("last_update=now()");

            st = new StringStatement(this.obtenerSQL(query).replaceAll("#CAMPOS#", Util.join(campos, ", ")), true);
            st.setString(1, entidad.getCodSuscriptor());
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en editarEntidad() -->> [WSLocalizacionDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new Exception("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }

    /**
     * Inserta un direccion en la tabla
     * @param direccion informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarDireccion(Direccion direccion) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(direccion.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(direccion.getIdentificacion())+"'");
            if(direccion.getTipo()!=null){
                campos.add("tipo");
                valores.add("'"+Util.escapeSQL(direccion.getTipo())+"'");
            }
            if(direccion.getDireccion()!=null){
                campos.add("direccion");
                valores.add("'"+Util.escapeSQL(direccion.getDireccion())+"'");
            }
            if(direccion.getFuente()!=null){
                campos.add("fuente");
                valores.add("'"+Util.escapeSQL(direccion.getFuente())+"'");
            }
            if(direccion.getCreacion()!=null){
                campos.add("creacion");
                valores.add("'"+direccion.getCreacion()+"'");
            }
            if(direccion.getActualizacion()!=null){
                campos.add("actualizacion");
                valores.add("'"+direccion.getActualizacion()+"'");
            }
            if(direccion.getNumReportes()>0){
                campos.add("num_reportes");
                valores.add(direccion.getNumReportes());
            }
            if(direccion.getEstrato()!=null){
                campos.add("estrato");
                valores.add("'"+Util.escapeSQL(direccion.getEstrato())+"'");
            }
            if(direccion.getProbabilidadEntrega()!=null){
                campos.add("probabilidad_entrega");
                valores.add("'"+Util.escapeSQL(direccion.getProbabilidadEntrega())+"'");
            }
            if(direccion.getCiudad()!=null){
                campos.add("ciudad");
                valores.add("'"+Util.escapeSQL(direccion.getCiudad())+"'");
            }
            if(direccion.getPais()!=null){
                campos.add("pais");
                valores.add("'"+Util.escapeSQL(direccion.getPais())+"'");
            }
            if(direccion.getNuevaNomenclatura()!=null){
                campos.add("nueva_nomenclatura");
                valores.add("'"+Util.escapeSQL(direccion.getNuevaNomenclatura())+"'");
            }
            if(direccion.getEntidad()!=null){
                campos.add("entidad");
                valores.add("'"+Util.escapeSQL(direccion.getEntidad())+"'");
            }
            if(direccion.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(direccion.getCreationUser())+"'");
            }
            if(direccion.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(direccion.getUserUpdate())+"'");
            }

            sql = "insert into wsdc.direccion(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";

        }catch(Exception ex){
            throw new Exception("ERROR en insertarDireccion() -->> [WSLocalizacionDAO] "+ex.getMessage());
        }
        return sql;
    }

    /**
     * Inserta un telefono en la tabla
     * @param telefono informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarTelefono(Telefono telefono) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(telefono.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(telefono.getIdentificacion())+"'");
            if(telefono.getTipo()!=null){
                campos.add("tipo");
                valores.add("'"+Util.escapeSQL(telefono.getTipo())+"'");
            }
            if(telefono.getNumero()!=0){
                campos.add("numero");
                valores.add(telefono.getNumero());
            }
            if(telefono.getNombreCiudad()!=null){
                campos.add("nombre_ciudad");
                valores.add("'"+Util.escapeSQL(telefono.getNombreCiudad())+"'");
            }
            if(telefono.getNombreDepartamento()!=null){
                campos.add("nombre_departamento");
                valores.add("'"+Util.escapeSQL(telefono.getNombreDepartamento())+"'");
            }
            if(telefono.getCodigoPais()!=0){
                campos.add("codigo_pais");
                valores.add(telefono.getCodigoPais());
            }
            if(telefono.getCodigoArea()!=0){
                campos.add("codigo_area");
                valores.add(telefono.getCodigoArea());
            }
            if(telefono.getFuente()!=null){
                campos.add("fuente");
                valores.add("'"+Util.escapeSQL(telefono.getFuente())+"'");
            }
            if(telefono.getEntidad()!=null){
                campos.add("entidad");
                valores.add("'"+Util.escapeSQL(telefono.getEntidad())+"'");
            }
            if(telefono.getReportado()!=null){
                campos.add("reportado");
                valores.add("'"+Util.escapeSQL(telefono.getReportado())+"'");
            }
            if(telefono.getNumReportes()>0){
                campos.add("num_reportes");
                valores.add(telefono.getNumReportes());
            }
            if(telefono.getCreacion()!=null){
                campos.add("creacion");
                valores.add("'"+telefono.getCreacion()+"'");
            }
            if(telefono.getActualizacion()!=null){
                campos.add("actualizacion");
                valores.add("'"+telefono.getActualizacion()+"'");
            }
            if(telefono.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+telefono.getCreationUser()+"'");
            }
            if(telefono.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(telefono.getUserUpdate())+"'");
            }

            sql = "insert into wsdc.telefono(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";

        }catch(Exception ex){
            throw new Exception("ERROR en insertarTelefono() -->> [WSLocalizacionDAO] "+ex.getMessage());
        }
        return sql;
    }

    /**
     * Inserta un email en la tabla
     * @param email informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarEmail(EmailLocalizacion email) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(email.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(email.getIdentificacion())+"'");
            if(email.getDireccion()!=null){
                campos.add("direccion");
                valores.add("'"+Util.escapeSQL(email.getDireccion())+"'");
            }
            if(email.getTipo()!=null){
                campos.add("tipo");
                valores.add("'"+Util.escapeSQL(email.getTipo())+"'");
            }
            if(email.getFuente()!=null){
                campos.add("fuente");
                valores.add("'"+Util.escapeSQL(email.getFuente())+"'");
            }
            if(email.getReportado()!=null){
                campos.add("reportado");
                valores.add("'"+Util.escapeSQL(email.getReportado())+"'");
            }
            if(email.getEntidad()!=null){
                campos.add("entidad");
                valores.add("'"+Util.escapeSQL(email.getEntidad())+"'");
            }
            if(email.getNumReportes()>0){
                campos.add("num_reportes");
                valores.add(email.getNumReportes());
            }
            if(email.getCreacion()!=null){
                campos.add("creacion");
                valores.add("'"+email.getCreacion()+"'");
            }
            if(email.getActualizacion()!=null){
                campos.add("actualizacion");
                valores.add("'"+email.getActualizacion()+"'");
            }
            if(email.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(email.getCreationUser())+"'");
            }
            if(email.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(email.getUserUpdate())+"'");
            }

            sql = "insert into wsdc.email(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";

        }catch(Exception ex){
            throw new Exception("ERROR en insertarEmail() -->> [WSLocalizacionDAO] "+ex.getMessage());
        }
        return sql;
    }
    
}
