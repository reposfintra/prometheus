/***************************************
 * Nombre Clase ............. StatusBarraDAO.java
 * Descripci�n  .. . . . . .  Ejecutamos los SQL para los PENDIENTES
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  25/08/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/




package com.tsp.operation.model.DAOS;



import java.io.*;
import java.util.*;
import java.sql.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;


public class StatusBarraDAO extends MainDAO{
    
    /** Creates a new instance of StatusBarraDAO */
    public StatusBarraDAO() {
        super("StatusBarraDAO.xml");
    }
    
    
    
    
    
    // ANTICIPOS:
     
    /**
     * Metodo que busca la cantodad de ANTICIPOS por aprobar
     * @autor     fvillacob
     * @throws    Exception.
     */
    public int  ANTICIPOS_Aprobar(String distrito, String proveedor)throws Exception{
        PreparedStatement st          = null; 
        ResultSet         rs          = null;
        String            query       = "SQL_ANTICIPOS_APROBAR";
        int               total       = 0;
        try{
            
            st = this.crearPreparedStatement(query);
            st.setString(1, distrito  );
            st.setString(2, proveedor );
            rs = st.executeQuery();
            if( rs.next() )
                total =  rs.getInt(1);
            
        }catch(Exception e){
            throw new Exception( " DAO : ANTICIPOS_Aprobar " + e.getMessage());
        }finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            this.desconectar(query);
        } 
        return total;
    }
    
    
    /**
     * Metodo que busca la cantodad de ANTICIPOS por transferir
     * @autor     fvillacob
     * @throws    Exception.
     */
    public int  ANTICIPOS_Transferir(String distrito, String proveedor)throws Exception{
        PreparedStatement st          = null; 
        ResultSet         rs          = null;
        String            query       = "SQL_ANTICIPOS_TRANSFERIR";
        int               total       = 0;
        try{
            
            st = this.crearPreparedStatement(query);
            st.setString(1, distrito  );
            st.setString(2, proveedor );
            rs = st.executeQuery();
            if( rs.next() )
                total =  rs.getInt(1);
            
        }catch(Exception e){
            throw new Exception( " DAO : ANTICIPOS_Transferir " + e.getMessage());
        }finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            this.desconectar(query);
        } 
        return total;
    }
    
    
    
    
    
    
    
    
    
    // LIQUIDACIONES :
    
    
     /**
     * Metodo que busca la cantodad de liquidaciones para aprobar
     * @autor     fvillacob
     * @throws    Exception.
     */
    public int  getLIQUIDACION_Aprobar()throws Exception{
        PreparedStatement st          = null; 
        ResultSet         rs          = null;
        String            query       = "SQL_CANTIDAD_LIQ_POR_APROBAR";
        int               total       = 0;
        try{
            
            st = this.crearPreparedStatement(query);
            rs = st.executeQuery();
            if( rs.next() )
                total =  rs.getInt(1);
            
        }catch(Exception e){
            throw new Exception( " DAO : getLiquidacionesPorAprobar " + e.getMessage());
        }finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            this.desconectar(query);
        } 
        return total;
    }
    
    
    
    /**
     * Metodo que busca la cantodad de liquidaciones para Migrar
     * @autor     fvillacob
     * @throws    Exception.
     */
    public int  getLIQUIDACION_Migrar()throws Exception{
        PreparedStatement st          = null; 
        ResultSet         rs          = null;
        String            query       = "SQL_CANTIDAD_LIQ_POR_MIGAR";
        int               total       = 0;
        try{
            
            st = this.crearPreparedStatement(query);
            rs = st.executeQuery();
            while( rs.next() )
                total++;
            
        }catch(Exception e){
            throw new Exception( " DAO : getLiquidacionesPorMigrar " + e.getMessage());
        }finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            this.desconectar(query);
        } 
        return total;
    }

    
    
}
