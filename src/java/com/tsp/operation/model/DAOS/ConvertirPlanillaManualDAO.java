/*********************************************************************************
 * Nombre clase :      ConvertirPlanillaManualDAO.java                           *
 * Descripcion :       DAO del ConvertirPlanillaManualDAO.java                   *
 *                     Clase que maneja los DAO (Data Access Object) los cuales  *       *
 *                     contienen los metodos que interactuan con la B.D.         *
 * Autor :             LREALES                                                   *
 * Fecha :             29 de marzo de 2006, 08:12 AM                             *
 * Version :           1.0                                                       *
 * Copyright :         Fintravalores S.A.                                   *
 *********************************************************************************/

package com.tsp.operation.model.DAOS;

import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;
import java.util.Date;
import java.text.*;
import com.tsp.util.connectionpool.PoolManager;

public class ConvertirPlanillaManualDAO extends MainDAO {
    
    private Vector plaman;  
    private Vector cod_clientes; 
    private Ingreso_Trafico it = null;
    private String pto_control_ultreporte = "";
    private String nompto_control_ultreporte = "";
    private String fecha_ult_reporte = "";
    private String pto_control_proxreporte = "";
    private String nompto_control_proxreporte = "";
    private String fecha_prox_reporte = "";
    private String creation_date = "";
    private String creation_user = "";
    
    
    /** Creates a new instance of ConvertirPlanillaDAO */
    public ConvertirPlanillaManualDAO () {
        
        super( "ConvertirPlanillaManualDAO.xml" );
        
    }
       
     /**
     * Setter for property plaman.
     * @param plaman New value of property plaman.
     */
    public void setPlaManual (java.util.Vector plaman) {
        
        this.plaman = plaman;
        
    }
    
    /**
     * Getter for property plaman.
     * @return Value of property plaman.
     */
    public java.util.Vector getPlaManual () {
        
        return plaman;
        
    }
    
    /**
     * Getter for property it.
     * @return Value of property it.
     */
    public Ingreso_Trafico getIngTra () {
        
        return this.it;
        
    }     
    
    
    /** Funcion publica que obtiene algunos datos de la planilla manual (ingreso_trafico) */
    public void listaPlanillasManuales () throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_PLANILLA_MANUAL" );
            
            rs = st.executeQuery();
            
            this.plaman = new Vector();
            
            while( rs.next() ){
                
                it = new Ingreso_Trafico();
                
                it.setPlanilla( rs.getString("planilla") == null?"":rs.getString("planilla") );
                it.setPlaca( rs.getString("placa") == null?"":rs.getString("placa") );                
                String nomcond = rs.getString("nomcond") == null?"":rs.getString("nomcond");
                String nomcond2 = rs.getString("nomcond2") == null?"":rs.getString("nomcond2");
                nomcond = nomcond.equals("")?nomcond2:nomcond;                
                it.setNomcond( nomcond );
                it.setNomorigen( rs.getString("nomorigen") == null?"":rs.getString("nomorigen") );
                it.setNomdestino( rs.getString("nomdestino") == null?"":rs.getString("nomdestino") );
                it.setCreation_user( rs.getString("creation_user") == null?"":rs.getString("creation_user") );
                
                plaman.add( it );
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE listaPlanillasManuales.. " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{   
            
            if ( st != null ){                
                try{                    
                    st.close();                    
                } catch( SQLException e ){                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );                    
                }                
            }
            
            this.desconectar( "SQL_PLANILLA_MANUAL" );
            
        }
        
    }
    
    /** Funcion publica que verifica si existe la planilla manual (ingreso_trafico) */
    public boolean existePlanilla ( String numplaman ) throws SQLException {
        
        boolean boo = false;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_EXISTE_PLANILLA_MANUAL" );
            st.setString( 1, numplaman );
            
            rs = st.executeQuery();
            
            it = new Ingreso_Trafico();
            
            this.plaman = new Vector();
            
            while( rs.next() ){
                
                boo = true;
                
                it.setPlanilla( rs.getString( "planilla" ) );
                it.setPlaca( rs.getString( "placa" ) );
                it.setPto_control_ultreporte( rs.getString( "pto_control_ultreporte" ) );                
                it.setNompto_control_ultreporte( rs.getString( "nompto_control_ultreporte" ) );
                it.setFecha_ult_reporte( rs.getString( "fecha_ult_reporte" ) );
                it.setPto_control_proxreporte( rs.getString( "pto_control_proxreporte" ) );
                it.setNompto_control_proxreporte( rs.getString( "nompto_control_proxreporte" ) );
                it.setFecha_prox_reporte( rs.getString( "fecha_prox_reporte" ) );
                it.setCreation_date( rs.getString( "creation_date" ) );
                it.setCreation_user( rs.getString( "creation_user" ) );
                
                plaman.add( it );
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS MANUALES POR NUMERO DE PLANILLA" + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){                
                try{                    
                    st.close();                    
                } catch( SQLException e ){                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );                    
                }                
            }
            
            this.desconectar( "SQL_EXISTE_PLANILLA_MANUAL" );
            
        }
        
        return boo;
        
    }
    
    /** Funcion publica que verifica si existe la planilla en rep_mov_trafico */
    public boolean existeRepMovTra ( String numplaori ) throws SQLException {
        
        boolean boo = false;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_SELECT_REP_MOV_TRA" );
            
            st.setString( 1, numplaori );
            
            rs = st.executeQuery();
            
            if ( rs.next() ) {
                
                boo = true;
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE existeRepMovTra.. " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){                
                try{                    
                    st.close();                    
                } catch( SQLException e ){                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );                    
                }                
            }
            
            this.desconectar( "SQL_SELECT_REP_MOV_TRA" );
            
        }
        
        return boo;
        
    }
    
    /** Funcion publica que devuelve la placa de la planilla manual */
    public String PlacaPlanillaManual ( String numplaman ) throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        String placa_manual = "";
        
        try {
            
            st = crearPreparedStatement( "SQL_EXISTE_PLANILLA_MANUAL" );
            
            st.setString( 1, numplaman );
            
            rs = st.executeQuery();
                        
            while( rs.next() ){
                
                placa_manual = rs.getString( "placa" );
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'PlacaPlanillaManual'.. " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){                
                try{                    
                    st.close();                    
                } catch( SQLException e ){                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );                    
                }                
            }
            
            this.desconectar( "SQL_EXISTE_PLANILLA_MANUAL" );
            
        }
        
        return placa_manual;
        
    }
    
    /** Funcion publica que devuelve la ruta de la planilla manual */
    public String RutaPlanillaManual ( String numplaman ) throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        String ruta_manual = "";
        
        try {
            
            st = crearPreparedStatement( "SQL_RUTA_MANUAL" );
            
            st.setString( 1, numplaman );
            
            rs = st.executeQuery();
            
            while( rs.next() ){
                
                ruta_manual = rs.getString( "ruta" );
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE LA BUSQUEDA DE LA RUTA DE LA PLANILLA MANUAL " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){                
                try{                    
                    st.close();                    
                } catch( SQLException e ){                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );                    
                }                
            }
            
            this.desconectar( "SQL_RUTA_MANUAL" );
            
        }
        
        return ruta_manual;
        
    }
    
    /** Funcion publica que devuelve el cliente de la planilla */
    public String ClientePlanilla ( String numpla ) throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        String cliente = "";
        
        try {
            
            st = crearPreparedStatement( "SQL_CLIENTE" );
            
            st.setString( 1, numpla );
            
            rs = st.executeQuery();
            
            while( rs.next() ){
                
                cliente = rs.getString( "cliente" );
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE LA BUSQUEDA DEL CLIENTE DE LA PLANILLA " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){                
                try{                    
                    st.close();                    
                } catch( SQLException e ){                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );                    
                }                
            }
            
            this.desconectar( "SQL_CLIENTE" );
            
        }
        
        return cliente;
        
    }
    
    /**
     * Getter for property cod_clientes.
     * @return Value of property cod_clientes.
     */
    public java.util.Vector getClientes () {
        
        return cod_clientes;
        
    }
    
    /** Funcion publica que verifica si existe la planilla original (ingreso_trafico) */
    public boolean existePlanillaOriginal ( String numplaori ) throws SQLException {
        
        boolean boo = false;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_EXISTE_PLANILLA_ORIGINAL" );
            
            st.setString( 1, numplaori );
            
            rs = st.executeQuery();
            
            while( rs.next() ){
                
                boo = true;
                
                it = new Ingreso_Trafico();
                
                it.setPlaca( rs.getString( "placa" ) );                
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS ORIGINALES POR NUMERO DE PLANILLA" + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){                
                try{                    
                    st.close();                    
                } catch( SQLException e ){                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );                    
                }                
            }
            
            this.desconectar( "SQL_EXISTE_PLANILLA_ORIGINAL" );
            
        }
        
        return boo;
        
    }
    
    /** Funcion publica que devuelve la placa de la planilla original */
    public String PlacaPlanillaOriginal ( String numplaori ) throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        String placa_original = "";
        
        try {
            
            st = crearPreparedStatement( "SQL_EXISTE_PLANILLA_ORIGINAL" );
            st.setString( 1, numplaori );
            
            rs = st.executeQuery();
            
            while( rs.next() ){
                
                it = new Ingreso_Trafico();
                
                it.setPlaca( rs.getString( "placa" ) );
                it.setPto_control_ultreporte( rs.getString( "pto_control_ultreporte" ) );
                it.setLast_update( rs.getString( "last_update" ) );
                placa_original = rs.getString( "placa" );
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE LA BUSQUEDA DE LA PLACA DE LA PLANILLA ORIGINAL " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){                
                try{                    
                    st.close();                    
                } catch( SQLException e ){                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );                    
                }                
            }
            
            this.desconectar( "SQL_EXISTE_PLANILLA_ORIGINAL" );
            
        }
        
        return placa_original;
        
    }
    
    /** Funcion publica que devuelve la ruta de la planilla original */
    public String RutaPlanillaOriginal ( String numplaori ) throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        String ruta_original = "";
        
        try {
            
            st = crearPreparedStatement( "SQL_RUTA_ORIGINAL" );
            
            st.setString( 1, numplaori );
            
            rs = st.executeQuery();
            
            while( rs.next() ){
                
                ruta_original = rs.getString( "ruta_pla" );
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE LA BUSQUEDA DE LA RUTA DE LA PLANILLA ORIGINAL " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){                
                try{                    
                    st.close();                    
                } catch( SQLException e ){                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );                    
                }                
            }
            
            this.desconectar( "SQL_RUTA_ORIGINAL" );
            
        }
        
        return ruta_original;
        
    }
    
    /** Funcion publica que actualiza algunos datos de la planilla manual (ingreso_trafico) */
    public String Update_Ingreso_Trafico ( String numplaori, String pto_control_ultreporte, String nompto_control_ultreporte, String fecha_ult_reporte, String pto_control_proxreporte, String nompto_control_proxreporte, String fecha_prox_reporte, String creation_date, String creation_user, String zona, String nomzona, String ult_observacion ) throws SQLException {
        
        PreparedStatement st = null;
        
        try {
            
            st = this.crearPreparedStatement( "SQL_UPDATE_INGRESO_TRAFICO" );
            
            st.setString( 1, pto_control_ultreporte );
            st.setString( 2, nompto_control_ultreporte );
            st.setString( 3, fecha_ult_reporte );
            st.setString( 4, pto_control_proxreporte );
            st.setString( 5, nompto_control_proxreporte );
            st.setString( 6, fecha_prox_reporte );
            st.setString( 7, creation_date );
            st.setString( 8, creation_user );
            st.setString( 9, zona );
            st.setString( 10, nomzona );
            st.setString( 11, ult_observacion );
            st.setString( 12, numplaori );
            
            return st.toString()+";";
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "Error en Update_Ingreso_Trafico [ConvertirPlanillaDAO]..." + e.getMessage() + " - " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st  != null ) st.close();
            this.desconectar( "SQL_UPDATE_INGRESO_TRAFICO" );
            
        }
        
    }
     
    /** Funcion publica que actualiza algunos datos de la planilla manual (trafico) */
    public String Update_Trafico ( String numplaori, String pto_control_ultreporte, String fecha_ult_reporte, String pto_control_proxreporte, String fecha_prox_reporte, String creation_date, String creation_user, String zona, String ult_observacion ) throws SQLException {
        
        PreparedStatement st = null;
        
        try {
            
            st = this.crearPreparedStatement( "SQL_UPDATE_TRAFICO" );
            
            st.setString( 1, pto_control_ultreporte );
            st.setString( 2, fecha_ult_reporte );
            st.setString( 3, pto_control_proxreporte );
            st.setString( 4, fecha_prox_reporte );
            st.setString( 5, creation_date );            
            st.setString( 6, creation_user );
            st.setString( 7, zona );            
            st.setString( 8, ult_observacion );
            st.setString( 9, numplaori );
            
            return st.toString()+";";
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "Error en Update_Trafico [ConvertirPlanillaDAO]..." + e.getMessage() + " - " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st  != null ) st.close();
            this.desconectar( "SQL_UPDATE_TRAFICO" );
            
        }
        
    }
    
    /** Funcion publica que inserta algunos datos en la planilla original (rep_mov_trafico) */
    public String Update_Rep_Mov_Trafico ( String fecrep, String numplaori, String numplaman ) throws SQLException {
                
        PreparedStatement st = null;   
        
        try {
            
            st = this.crearPreparedStatement( "SQL_UPDATE_REP_MOV_TRAFICO" );  
            
            st.setString( 1, "" );
            st.setString( 2, numplaori );
            st.setString( 3, numplaman );
            st.setString( 4, fecrep );
            
            return st.toString()+";";
               
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "Error en Update_Rep_Mov_Trafico [ConvertirPlanillaDAO]..." + e.getMessage() + " - " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st  != null ) st.close();
            this.desconectar( "SQL_UPDATE_REP_MOV_TRAFICO" );
            
        }
        
    }
    
    /** Funcion publica que elimina la planilla manual ingresada de la tabla ingreso_trafico */
    public String Delete_Planilla_Manual_Ingreso_Trafico ( String numplaman ) throws SQLException {
        
        PreparedStatement st = null;
        
        try {
            
            st = this.crearPreparedStatement( "SQL_DELETE_PLANILLA_MANUAL_INGRESO_TRAFICO" );
            
            st.setString( 1, numplaman );
            
            return st.toString()+";";
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "Error en Delete_Planilla_Manual_Ingreso_Trafico [ ConvertirPlanillaManualDAO ]..." + e.getMessage() + " - " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st  != null ) st.close();
                this.desconectar( "SQL_DELETE_PLANILLA_MANUAL_INGRESO_TRAFICO" );
            
        }
        
    }
    
    /** Funcion publica que elimina la planilla manual ingresada de la tabla trafico */
    public String Delete_Planilla_Manual_Trafico ( String numplaman ) throws SQLException {
        
        PreparedStatement st = null;
        
        try {
            
            st = this.crearPreparedStatement( "SQL_DELETE_PLANILLA_MANUAL_TRAFICO" );
            
            st.setString( 1, numplaman );
            
            return st.toString()+";";
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "Error en Delete_Planilla_Manual_Trafico [ ConvertirPlanillaManualDAO ]..." + e.getMessage() + " - " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st  != null ) st.close();
                this.desconectar( "SQL_DELETE_PLANILLA_MANUAL_TRAFICO" );
            
        }
        
    }
    
    /** Funcion publica que elimina la planilla manual ingresada de la tabla rep_mov_trafico */
    public String Delete_Planilla_Manual_Rep_Mov_Trafico ( String numplaman ) throws SQLException {
        
        PreparedStatement st = null;
        
        try {
            
            st = this.crearPreparedStatement( "SQL_DELETE_PLANILLA_MANUAL_REP_MOV_TRAFICO" );
            
            st.setString( 1, numplaman );
            
            return st.toString()+";";
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "Error en Delete_Planilla_Manual_Rep_Mov_Trafico [ ConvertirPlanillaManualDAO ]..." + e.getMessage() + " - " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st  != null ) st.close();
                this.desconectar( "SQL_DELETE_PLANILLA_MANUAL_REP_MOV_TRAFICO" );
            
        }
        
    }
    
    /** Funcion publica que anula la planilla manual ingresada de la tabla despacho_manual */
    public String Anular_Planilla_Manual_Despacho_Manual ( String numplaman, String usuario ) throws SQLException {
        
        PreparedStatement st = null;
        
        try {
            
            st = this.crearPreparedStatement( "SQL_ANULAR_DESPACHO_MANUAL" );
            
            st.setString( 1, usuario );
            st.setString( 2, usuario );
            st.setString( 3, numplaman );
            
            return st.toString()+";";
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "Error en Anular_Planilla_Manual_Despacho_Manual [ ConvertirPlanillaManualDAO ]..." + e.getMessage() + " - " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st  != null ) st.close();
                this.desconectar( "SQL_ANULAR_DESPACHO_MANUAL" );
            
        }
        
    }
        
    /*******************************************/
    /** Funcion publica que devuelve la ruta de la planilla en ingreso trafico */
    public String RutaPlanillaIngresoTrafico ( String numplaori ) throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        String ruta_ingreso_trafico = "";
        
        try {
            
            st = crearPreparedStatement( "SQL_RUTA_INGRESO_TRAFICO" );
            
            st.setString( 1, numplaori );
            
            rs = st.executeQuery();
                        
            if ( rs.next() ){
                
                ruta_ingreso_trafico = rs.getString( "via" );
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE LA BUSQUEDA DE LA RUTA DE LA PLANILLA EN INGRESO TRAFICO " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){                
                try{                    
                    st.close();                    
                } catch( SQLException e ){                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );                    
                }                
            }
            
            this.desconectar( "SQL_RUTA_INGRESO_TRAFICO" );
            
        }
        
        return ruta_ingreso_trafico;
        
    }
    /*******************************************/
    
    /** Funcion publica que extrae info (ingreso_trafico) */
    public void buscarPlanilla ( String numpla ) throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_EXISTE_PLANILLA_MANUAL" );
            st.setString( 1, numpla );
            
            rs = st.executeQuery();
            
            this.plaman = new Vector();
            
            if ( rs.next() ){
                
                it = new Ingreso_Trafico();
                
                it.setPlanilla( rs.getString( "planilla" ) );
                it.setPlaca( rs.getString( "placa" ) );
                it.setPto_control_ultreporte( rs.getString( "pto_control_ultreporte" ) );                
                it.setNompto_control_ultreporte( rs.getString( "nompto_control_ultreporte" ) );
                it.setFecha_ult_reporte( rs.getString( "fecha_ult_reporte" ) );
                it.setPto_control_proxreporte( rs.getString( "pto_control_proxreporte" ) );
                it.setNompto_control_proxreporte( rs.getString( "nompto_control_proxreporte" ) );
                it.setFecha_prox_reporte( rs.getString( "fecha_prox_reporte" ) );
                it.setCreation_date( rs.getString( "creation_date" ) );
                it.setCreation_user( rs.getString( "creation_user" ) );
                it.setZona( rs.getString( "zona" ) );
                it.setNomzona( rs.getString( "nomzona" ) );
                it.setUlt_observacion( rs.getString( "ult_observacion" ) );
                
                plaman.add( it );
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE LA BUSQUEDA DE LA PLANILLA " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){                
                try{                    
                    st.close();                    
                } catch( SQLException e ){                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );                    
                }                
            }
            
            this.desconectar( "SQL_EXISTE_PLANILLA_MANUAL" );
            
        }
        
    }
    
    /** Funcion publica que extrae info (ingreso_trafico) */
    public void buscarRepMovTra ( String numpla ) throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_SELECT_REP_MOV_TRA" );
            
            st.setString( 1, numpla );
           
            rs = st.executeQuery();
            
            this.plaman = new Vector();
            
            while ( rs.next() ){
                
                Ingreso_Trafico it = new Ingreso_Trafico();
                
                it.setReg_status( rs.getString( "reg_status" ) );
                it.setDstrct( rs.getString( "dstrct" ) );
                it.setPlanilla( rs.getString( "numpla" ) ); 
                it.setObservacion( rs.getString( "observacion" ) );                
                it.setTipo_procedencia( rs.getString( "tipo_procedencia" ) );
                it.setUbicacion_procedencia( rs.getString( "ubicacion_procedencia" ) );
                it.setTipo_reporte( rs.getString( "tipo_reporte" ) );
                it.setLast_update( rs.getString( "last_update" ) );
                it.setUser_update( rs.getString( "user_update" ) );
                it.setCreation_date( rs.getString( "creation_date" ) );
                it.setCreation_user( rs.getString( "creation_user" ) );
                it.setBase( rs.getString( "base" ) );
                it.setFechareporte( rs.getString( "fechareporte" ) );
                it.setZona( rs.getString( "zona" ) );
                it.setFechareporte_planeado( rs.getString( "fechareporte_planeado" ) );
                it.setCausa( rs.getString( "causa" ) );
                it.setClasificacion( rs.getString( "clasificacion" ) );
                
                plaman.add( it );
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE LA BUSQUEDA DEL REPORTE DE MOVIMIENTO DE TRAFICO " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){                
                try{                    
                    st.close();                    
                } catch( SQLException e ){                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );                    
                }                
            }
            
            this.desconectar( "SQL_SELECT_REP_MOV_TRA" );
            
        }
        
    }
    /** Funcion publica que extrae info (ingreso_trafico) */
    public void buscarRepMovTra2 ( String numpla, String numplaori ) throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_SELECT_REP_MOV_TRA_2" );
            st.setString( 1, numpla );
            st.setString( 2, numplaori );
            rs = st.executeQuery();
            
            this.plaman = new Vector();
            
            while ( rs.next() ){
                
                Ingreso_Trafico it = new Ingreso_Trafico();
                
                it.setReg_status( rs.getString( "reg_status" ) );
                it.setDstrct( rs.getString( "dstrct" ) );
                it.setPlanilla( rs.getString( "numpla" ) ); 
                it.setObservacion( rs.getString( "observacion" ) );                
                it.setTipo_procedencia( rs.getString( "tipo_procedencia" ) );
                it.setUbicacion_procedencia( rs.getString( "ubicacion_procedencia" ) );
                it.setTipo_reporte( rs.getString( "tipo_reporte" ) );
                it.setLast_update( rs.getString( "last_update" ) );
                it.setUser_update( rs.getString( "user_update" ) );
                it.setCreation_date( rs.getString( "creation_date" ) );
                it.setCreation_user( rs.getString( "creation_user" ) );
                it.setBase( rs.getString( "base" ) );
                it.setFechareporte( rs.getString( "fechareporte" ) );
                it.setZona( rs.getString( "zona" ) );
                it.setFechareporte_planeado( rs.getString( "fechareporte_planeado" ) );
                it.setCausa( rs.getString( "causa" ) );
                it.setClasificacion( rs.getString( "clasificacion" ) );
                
                plaman.add( it );
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE LA BUSQUEDA DEL REPORTE DE MOVIMIENTO DE TRAFICO " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){                
                try{                    
                    st.close();                    
                } catch( SQLException e ){                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );                    
                }                
            }
            
            this.desconectar( "SQL_SELECT_REP_MOV_TRA_2" );
            
        }
        
    }
    /** Funcion publica que inserta algunos datos en la planilla original (rep_mov_trafico) */
    public String Update_Rep_Mov_Trafico_2 ( String numplaori, String numplaman, String reg_status, String last_update, String user_update ) throws SQLException {
                
        PreparedStatement st = null;   
        
        try {
            
            st = this.crearPreparedStatement( "SQL_UPDATE_REP_MOV_TRAFICO_2" );  
            
            st.setString( 1, reg_status );
            st.setString( 2, numplaori );
            st.setString( 3, last_update );
            st.setString( 4, user_update );
            st.setString( 5, numplaman );
            
            return st.toString()+";";
               
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "Error en Update_Rep_Mov_Trafico_2 [ConvertirPlanillaDAO]..." + e.getMessage() + " - " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st  != null ) st.close();
            this.desconectar( "SQL_UPDATE_REP_MOV_TRAFICO_2" );
            
        }
        
    }
    
    /** Funcion publica que actualiza rep_mov_tra */
    public String Update_Rep_Mov_Trafico_Minuto ( String numpla, String fecrep, int minuto ) throws SQLException {
        
        PreparedStatement st = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_UPDATE_REP_MOV_TRAFICO_MINUTO" );
            
            st.setString( 1, fecrep );
            st.setString( 2, minuto+" MINUTE" );
            st.setString( 3, numpla );
            st.setString( 4, fecrep );
            
            return st.toString()+";";
                        
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE LA BUSQUEDA DE Update_Rep_Mov_Trafico_Minuto " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){                
                try{                    
                    st.close();                    
                } catch( SQLException e ){                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );                    
                }                
            }
            
            this.desconectar( "SQL_UPDATE_REP_MOV_TRAFICO_MINUTO" );
            
        }
        
    }
    
    /** Funcion publica que verifica si existe la planilla (trafico) */
    public boolean existePlanillaEnTrafico ( String numplaori ) throws SQLException {
        
        boolean boo = false;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_EXISTE_PLANILLA_EN_TRAFICO" );
            
            st.setString( 1, numplaori );
            
            rs = st.executeQuery();
            
            while( rs.next() ){
                
                boo = true;              
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'existePlanillaEnTrafico'.. " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){                
                try{                    
                    st.close();                    
                } catch( SQLException e ){                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );                    
                }                
            }
            
            this.desconectar( "SQL_EXISTE_PLANILLA_EN_TRAFICO" );
            
        }
        
        return boo;
        
    }
    
    /** Funcion publica que devuelve el cliente de la planilla de trafico*/
    public String ClientePlanilla_Caso_2 ( String numpla ) throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        String cliente = "";
        
        try {
            
            st = crearPreparedStatement( "SQL_CLIENTE_CASO_2" );
            
            st.setString( 1, numpla );
            
            rs = st.executeQuery();
            
            while( rs.next() ){
                
                cliente = rs.getString( "cliente" );
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'ClientePlanilla_Caso_2'.. " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){                
                try{                    
                    st.close();                    
                } catch( SQLException e ){                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );                    
                }                
            }
            
            this.desconectar( "SQL_CLIENTE_CASO_2" );
            
        }
        
        return cliente;
        
    }
    
    /** Funcion publica que devuelve la placa de la planilla en trafico */
    public String PlacaPlanilla_Caso_2 ( String numpla ) throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        String placa = "";
        
        try {
            
            st = crearPreparedStatement( "SQL_EXISTE_PLANILLA_EN_TRAFICO" );
            
            st.setString( 1, numpla );
            
            rs = st.executeQuery();
                        
            while( rs.next() ){
                
                placa = rs.getString( "placa" );
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'PlacaPlanilla_Caso_2'.. " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){                
                try{                    
                    st.close();                    
                } catch( SQLException e ){                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );                    
                }                
            }
            
            this.desconectar( "SQL_EXISTE_PLANILLA_EN_TRAFICO" );
            
        }
        
        return placa;
        
    }
    
}