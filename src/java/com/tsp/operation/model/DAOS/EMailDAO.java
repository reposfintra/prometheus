/***************************************
* Nombre Clase ............. EMailDAO.java
* Descripci�n  .. . . . . .  Permite  Guardar Mail, para ser enviados
* Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
* Fecha . . . . . . . . . .  08/11/2005
* versi�n . . . . . . . . .  1.0
* Copyright ...............  Transportes Sanchez Polo S.A.
*******************************************/



package com.tsp.operation.model.DAOS;



import java.io.*;
import java.util.*;
import java.sql.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;

import com.tsp.operation.model.TransaccionService;//091206
import java.sql.*;//091206

public class EMailDAO extends MainDAO{//091206

    
    /** ____________________________________________________________________________________________
                                              ATRIBUTOS
        ____________________________________________________________________________________________ */
    
    
    
    
    private static  String INSERT =
                                    " INSERT INTO sendmail     "+
                                    " (                        "+
                                    "   emailcode ,            "+
                                    "   emailfrom ,            "+
                                    "   emailto,               "+
                                    "   emailcopyto ,          "+
                                    "   emailsubject ,         "+
                                    "   emailbody ,            "+
                                    "   sendername ,           "+
                                    "   remarks                "+
                                    " )                        "+
                                    " VALUES (?,?,?,?,?,?,?,?) ";
    
    
    
    
    
    /** ____________________________________________________________________________________________
                                             METODOS
        ____________________________________________________________________________________________ */
    
    
    
    
    public EMailDAO() {
        super("EMailDAO.xml");//091206
    }
    public EMailDAO(String dataBaseName) {
        super("EMailDAO.xml", dataBaseName);//091206
    }
    
    
    /* Guarda el msj del proceso en la tabla sendmail */
    public static void  saveMail(Email  mail)throws Exception{
          PoolManager poolManager = PoolManager.getInstance();
          Connection con          = poolManager.getConnection("fintra");
          if (con == null)
                throw new SQLException("Sin conexion");          
          PreparedStatement st        = null;
          ResultSet         rs        = null;   
          try {
              
               st= con.prepareStatement(INSERT);   
                 st.setString(1, mail.getEmailcode()    );
                 st.setString(2, mail.getEmailfrom()    );
                 st.setString(3, mail.getEmailto()      );
                 st.setString(4, mail.getEmailcopyto()  );
                 st.setString(5, mail.getEmailsubject() );
                 st.setString(6, mail.getEmailbody()    );
                 st.setString(7, mail.getSenderName()   );
                 st.setString(8, mail.getRemarks()      );               
               st.execute();
               
          }catch(Exception e){
              throw new Exception(" DAO[saveMail] : No se pudo guardar el Mail -->"+ e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             if(rs!=null) rs.close();
             poolManager.freeConnection("fintra",con);
          } 
    }
    
    public String saveMailError(Email2  mail2)  throws Exception{//091206
            String respuesta="ok";
            PreparedStatement  st    = null;
            String             query1 = "SQL_INSERT_MAIL_ERROR";

            Connection con=null;
            String ejecutable="";
            try{
                String sql    =   this.obtenerSQL( query1 );
                con= this.conectar(query1);
                st            =   con.prepareStatement( sql );

                st.setString(1, mail2.getEmailcode()    );
                st.setString(2, mail2.getEmailfrom()    );
                //st.setString(3, mail2.getEmailto()      );
                st.setString(3, "imorales@fintravalores.com"      );
                //st.setString(4, mail2.getEmailcopyto()  );
                st.setString(4, ""  );
                st.setString(5, "error:" +mail2.getEmailsubject() );
                st.setString(6, mail2.getEmailto()+"__"+mail2.getEmailbody()    );
                st.setString(7, mail2.getSenderName()   );
                st.setString(8, mail2.getRemarks()      );

                ejecutable=ejecutable+st.toString();

                TransaccionService  scv = new TransaccionService();
                if(!ejecutable.equals("")){
                    try{
                        scv.crearStatement();
                        scv.getSt().addBatch(ejecutable);
                        scv.execute();
                    }catch(SQLException e){
                        throw new SQLException("ERROR DURANTE SQL_INSERT_MAIL error "+e.getMessage());
                    }
                }
                ejecutable=null;

            }catch(Exception e){
                System.out.println("error insercion mail:"+e.toString()+"__"+e.getMessage());
                throw new SQLException("Error SQL_INSERT_MAIL : "+ e.getMessage());
            }
            finally{
                if(st!=null) st.close();
                this.desconectar(query1);
            }
            return respuesta;
        }//091206

    
}
