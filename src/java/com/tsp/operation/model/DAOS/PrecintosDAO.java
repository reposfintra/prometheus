/********************************************************************
 *  Nombre Clase.................   PrecintosDAO.java
 *  Descripci�n..................   DAO de la tabla precintos
 *  Autor........................   Ing. Tito Andr�s Maturana
 *                                  Ing. Leonardo Parodi
 *  Fecha........................   22.12.2005
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.DAOS;


import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import javax.swing.*;
import java.text.*;

public class PrecintosDAO extends MainDAO {
    private Vector vector; //Tito
    private Precinto precinto = new Precinto(); //Leonardo
    
    /** Creates a new instance of PrecintosDAO */
    public PrecintosDAO() {   
        super("PrecintosDAO.xml");
    }
    
    private final String SQL_STICKERS_UTILIZADOS =
    "SELECT COALESCE(nomciu, '') AS agencia, " +
    "       COALESCE(usuarios.nombre, '') AS usuario," +
    "       to_char(fec_utilizacion, 'YYYY-mm-dd') AS fec_utilizacion, " +
    "       precintos.* " +
    "FROM   precintos " +
    "LEFT JOIN ciudad ON (codciu=agencia) " +
    "LEFT JOIN usuarios ON (idusuario=precintos.user_update) " +
    "WHERE  numpla<>'' AND precintos.reg_status<>'A' " +
    "       AND fec_utilizacion BETWEEN ? AND ? " +
    "       AND tipo_documento = 'Sticker' " +
    "ORDER BY precinto";





private final String SQL_UTILIZADOS =
    "SELECT COALESCE(nomciu, '') AS agencia, " +
    "       COALESCE(usuarios.nombre, '') AS usuario," +
    "       to_char(fec_utilizacion, 'YYYY-mm-dd') AS fec_utilizacion, " +
    "       precintos.* " +
    "FROM   precintos " +
    "LEFT JOIN ciudad ON (codciu=agencia) " +
    "LEFT JOIN usuarios ON (upper(idusuario)=precintos.user_update) " +
    "WHERE  numpla<>'' AND precintos.reg_status<>'A' " +
    "       AND fec_utilizacion BETWEEN ? AND ? " +
    "       AND tipo_documento = 'Precinto' " +
    "ORDER BY precintos.agencia, precinto";
    
     private final String SQL_UTILIZADOS_AGENCIA =
    "SELECT COALESCE(nomciu, '') AS agencia, " +
    "       COALESCE(usuarios.nombre, '') AS usuario," +
    "       to_char(fec_utilizacion, 'YYYY-mm-dd') AS fec_utilizacion, " +
    "       precintos.* " +
    "FROM   precintos " +
    "LEFT JOIN ciudad ON (codciu=agencia) " +
    "LEFT JOIN usuarios ON (idusuario=precintos.user_update) " +
    "WHERE  numpla<>'' AND precintos.reg_status<>'A' " +
    "       AND agencia=? AND fec_utilizacion BETWEEN ? AND ?" +
    "       AND tipo_documento = 'Precinto' " +
    "ORDER BY precinto";
    
    
    
    private final String SQL_INFOPLANILLA =
    "SELECT " +
    "    pl.numpla, " +
    "    rem.numrem, " +
    "    pl.fecpla, " +
    "    COALESCE(nomcli, '') AS cliente, " +
    "    COALESCE(ciu1.nomciu, '') AS origen, " +
    "    COALESCE(ciu2.nomciu, '') AS destino " +
    "FROM " +
    "    planilla pl " +
    "LEFT JOIN ciudad ciu1 ON (ciu1.codciu=pl.oripla) " +
    "LEFT JOIN ciudad ciu2 ON (ciu2.codciu=pl.despla) " +
    "JOIN plarem plr ON (pl.numpla = plr.numpla) " +
    "JOIN remesa rem ON (rem.numrem=plr.numrem) " +
    "LEFT JOIN cliente ON (codcli=rem.cliente)" +
    "WHERE pl.numpla = ? ";   
    
    
    private final String SQL_NO_UTILIZADOS =
    "SELECT COALESCE(nomciu, '') AS agencia, " +
    "       precintos.* " +
    "FROM precintos " +
    "LEFT JOIN ciudad ON (codciu=agencia) " +
    "WHERE  numpla='' AND precintos.creation_date BETWEEN ? AND ?" +
    "       AND tipo_documento = 'Precinto' " +
    "       AND precintos.reg_status<>'A' ";
    
    private final String SQL_NO_UTILIZADOS_AGENCIA =
    "SELECT COALESCE(nomciu, '') AS agencia, " +
    "       precintos.* " +
    "FROM precintos " +
    "LEFT JOIN ciudad ON (codciu=agencia) " +
    "WHERE  numpla='' AND agencia=? " +
    "       AND precintos.creation_date BETWEEN ? AND ?" +
    "       AND tipo_documento = 'Precinto' " +
    "       AND precintos.reg_status<>'A'";
    
    /* TITO */
    private final String SQL_STICKERS_UTILIZADOS_AGENCIA =
    "SELECT COALESCE(nomciu, '') AS agencia, " +
    "       COALESCE(usuarios.nombre, '') AS usuario," +
    "       to_char(fec_utilizacion, 'YYYY-mm-dd') AS fec_utilizacion, " +
    "       precintos.* " +
    "FROM   precintos " +
    "LEFT JOIN ciudad ON (codciu=agencia) " +
    "LEFT JOIN usuarios ON (idusuario=precintos.user_update) " +
    "WHERE  numpla<>'' AND precintos.reg_status<>'A' " +
    "       AND agencia=? AND fec_utilizacion BETWEEN ? AND ?" +
    "       AND tipo_documento = 'Sticker' " +
    "ORDER BY precinto";
    
  
    
    private final String SQL_STICKERS_NO_UTILIZADOS =
    "SELECT COALESCE(nomciu, '') AS agencia, " +
    "       precintos.* " +
    "FROM precintos " +
    "LEFT JOIN ciudad ON (codciu=agencia) " +
    "WHERE  numpla='' AND precintos.creation_date BETWEEN ? AND ?" +
    "       AND tipo_documento = 'Sticker' " +
    "       AND precintos.reg_status<>'A'";
    
    private final String SQL_STICKERS_NO_UTILIZADOS_AGENCIA =
    "SELECT COALESCE(nomciu, '') AS agencia, " +
    "       precintos.* " +
    "FROM precintos " +
    "LEFT JOIN ciudad ON (codciu=agencia) " +
    "WHERE  numpla='' AND agencia=? " +
    "       AND precintos.creation_date BETWEEN ? AND ?" +
    "       AND tipo_documento = 'Sticker' AND " +
    "       precintos.reg_status<>'A'";
    
       
    /* LEONARDO */
    private static final String INSERT_PRECINTO=
    "INSERT INTO precintos (agencia, precinto, numpla, "+
    "last_update, user_update, creation_date, creation_user, reg_status, dstrct, "+
    "base, tipo_documento ) "+
    "VALUES (?,?,'',now(),?,now(),?,'',?,?,?)";
    
    private static final String ANULAR_PRECINTO=
    "DELETE FROM precintos WHERE precinto = ? AND tipo_documento = ?";
    
    private static final String CONSULT_PRECINTOS=
    "SELECT * FROM precintos WHERE precinto = ? AND tipo_documento = ?";
    
   
    
    
    private static final String CONSULTA_SERIE = 
            "SELECT " +
            "       get_nombreciudad(agencia) AS agencia, " +
            "       tipo_documento, " +
            "       get_nombreusuario(creation_user) AS creation_user, " +
            "       creation_date, " +
            "       array_to_string(array_accum(precinto) , ', ') AS precintos " +
            "FROM (" +
            "   SELECT " +
            "           agencia, " +
            "           tipo_documento, " +
            "           creation_user, " +
            "           to_char(creation_date, 'YYYY-MM-DD HH:MI') AS creation_date, " +
            "           precinto " +
            "   FROM " +
            "           precintos " +
            "   WHERE " +
            "           precinto::numeric BETWEEN ? AND ? " +
            "           AND agencia LIKE ? " +
            "           AND tipo_documento LIKE ? " +
            ") AS c1 " +
            "GROUP BY agencia, tipo_documento, creation_date, creation_user " +
            "ORDER BY tipo_documento, agencia, creation_user, creation_date";
    
     private static final String EXISTE_PRECINTO_PLANILLA =  "Select precinto from precintos where  tipo_documento=? and precinto=? and ((numpla =''AND agencia=?)  or (numpla = ? ))";
    
    /**
     * Metodo InsertarPrecinto , Metodo que Ingresa una serie de registros
     * @autor : Ing. Leonardo Parody
     * @throws : SQLException
     * @param : String ruta
     * @param : String base
     * @param : String dato
     * @version : 1.0
     */
    public void InsertarPrecinto() throws SQLException {
        ////System.out.println("ESTOY EN EL DAO INSERTAR PRECINTOS");
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                ////System.out.println("VOY A EJECUTAR EL QUERY");
                st = con.prepareStatement(INSERT_PRECINTO);
                st.setString(1, precinto.getAgencia());
                st.setString(2, precinto.getPrecinto());
                st.setString(3, precinto.getUser_update());
                st.setString(4, precinto.getCreation_user());
                st.setString(5, precinto.getDstrct());
                st.setString(6, precinto.getBase());
                st.setString(7, precinto.getTipoDocumento());
                ////System.out.println("Query  "+st);
                st.executeUpdate();
                ////System.out.println("YA EJECUTE EL QUERY");
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR AL REALIZAR LA CONSULTA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * Metodo ExistePrecinto , Metodo que verifica si una serie de precintos existe
     * @autor : Ing. Leonardo Parody
     * @throws : SQLException
     * @param : long inicio
     * @param : long fin
     * @retorna una lista de precintos existentes
     * @version : 1.0
     */
    public List ExisteSerie(long inicio, long fin, String tipo_documento) throws SQLException {
        ////System.out.println("ESTOY EN EL DAO EXISTE SERIE");
        LinkedList precintos = new LinkedList();
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String serini = ""+inicio;
        String serfin = ""+fin;
        int cont=0;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                ////System.out.println("VOY A EJECUTAR EL QUERY");
                for (long i=inicio; i<=fin; i++){
                    st = con.prepareStatement(CONSULT_PRECINTOS);
                    st.setString(1, ""+i);
                    st.setString(2, tipo_documento);
                    ////System.out.println("SQL Existe precinto = "+st);
                    rs = st.executeQuery();
                    while (rs.next()) {
                        String precint = rs.getString("precinto");
                        precintos.add(precint);
                        ////System.out.println("precinto agregado = "+precint);
                    }
                }
                ////System.out.println("Query  "+st);
                ////System.out.println("YA EJECUTE EL QUERY");
            }
            return precintos;
            
        }catch(SQLException e){
            throw new SQLException("ERROR AL REALIZAR LA CONSULTA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    /**
     * Metodo EliminarPrecinto , Metodo que elimina precintos
     * @autor : Ing. Leonardo Parody
     * @throws : SQLException
     * @param : String precinto
     * @version : 1.0
     */
    public void EliminarPrecinto(String precinto, String tipo_documento) throws SQLException {
        ////System.out.println("ESTOY EN EL DAO ELIMINAR PRECINTO");
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        int cont=0;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                ////System.out.println("VOY A EJECUTAR EL UPDATE");
                st = con.prepareStatement(ANULAR_PRECINTO);
                st.setString(1, precinto);
                st.setString(2, tipo_documento);
                ////System.out.println("Query  "+st);
                st.executeUpdate();
                ////System.out.println("YA EJECUTE EL UPDATE");
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL REALIZAR LA CONSULTA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    /**
     * Metodo SerieAsignada , Metodo que verifica si una serie de precintos se encuentra asignada
     * @autor : Ing. Leonardo Parody
     * @throws : SQLException
     * @param : long inicio
     * @param : long fin
     * @version : 1.0
     */
    public List SerieAsignada(long inicio, long fin, String tipo_documento) throws SQLException {
        ////System.out.println("ESTOY EN EL DAO SERIE OCUPADA");
        LinkedList precintos = new LinkedList();
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String serini = ""+inicio;
        String serfin = ""+fin;
        int cont=0;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                ////System.out.println("VOY A EJECUTAR EL QUERY");
                for (long i=inicio; i<=fin; i++){
                    st = con.prepareStatement(CONSULT_PRECINTOS);
                    st.setString(1, ""+i);
                    st.setString(2, tipo_documento);
                    ////System.out.println("SQL Existe precinto = "+st);
                    rs = st.executeQuery();
                    while (rs.next()) {
                        String numpla = rs.getString("numpla");
                        if (!numpla.equals("")){
                            String precint = rs.getString("precinto");
                            precintos.add(precint);
                            ////System.out.println("precinto agregado = "+precint);
                        }
                        
                    }
                }
                ////System.out.println("Query  "+st);
                ////System.out.println("YA EJECUTE EL QUERY");
            }
            return precintos;
            
        }catch(SQLException e){
            throw new SQLException("ERROR AL REALIZAR LA CONSULTA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * Obtiene los precintos utilizados por una o todas las agencias
     * en un per�odo determinado
     * @autor Ing. Tito Andr�s Maturana
     * @param agencia C�digo de la agencia
     * @param fechaI Fecha de inicio del reporte
     * @param fechaF Fecha final del reporte
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @version 1.0
     */
    public void precintosUtilizados(String agencia, String fechaI, String fechaF)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        this.vector = new Vector();
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                if( agencia.length()==0 ){
                    st = con.prepareStatement(this.SQL_UTILIZADOS);
                    st.setString(1, fechaI);
                    st.setString(2, fechaF);
                } else {
                    st = con.prepareStatement(this.SQL_UTILIZADOS_AGENCIA);
                    st.setString(1, agencia);
                    st.setString(2, fechaI);
                    st.setString(3, fechaF);
                }
                
                ////System.out.println("........ consulta: " + st.toString());
                
                rs = st.executeQuery();
                
                while( rs.next() ){
                    Precinto pr = new Precinto();
                    pr.Load(rs);
                    pr.setUser_update(rs.getString("usuario"));
                    
                    this.vector.add(pr);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA DE PRECINTOS UTILIZADOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Obtiene informaci�n de un n�mero de planilla, tales como: remes, fecha,
     * ruta (origen-destino) y cliente
     * en un per�odo determinado
     * @autor Ing. Tito Andr�s Maturana
     * @param numpla N�mero de la planilla
     * @returns <code>InfoPlanilla</code> con informaci�n de la planilla
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @version 1.0
     */
    public InfoPlanilla informacionPlanilla(String numpla) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        InfoPlanilla info = new InfoPlanilla();
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(this.SQL_INFOPLANILLA);
                st.setString(1, numpla);
                
                ////System.out.println("................ consulta info: " + st.toString());
                rs = st.executeQuery();
                
                if( rs.next() ){
                    info.setPlanilla(rs.getString("numpla"));
                    info.setRemesa(rs.getString("numrem"));
                    info.setFecha(rs.getString("fecpla"));
                    info.setOrigen(rs.getString("origen"));
                    info.setDestino(rs.getString("destino"));
                    info.setCliente(rs.getString("cliente"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA DE INFORMACION DE LA PLANILLA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        return info;
    }
    
    
    /**
     * Obtiene los precintos no utilizados por una o todas las agencias
     * en un per�odo determinado
     * @autor Ing. Tito Andr�s Maturana
     * @param agencia C�digo de la agencia
     * @param fechaI Fecha de inicio del reporte
     * @param fechaF Fecha final del reporte
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @version 1.0
     */
    public void precintosNoUtilizados(String agencia, String fechaI, String fechaF)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        this.vector = new Vector();
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                if( agencia.length()==0 ){
                    st = con.prepareStatement(this.SQL_NO_UTILIZADOS);
                    st.setString(1, fechaI);
                    st.setString(2, fechaF);
                } else {
                    st = con.prepareStatement(this.SQL_NO_UTILIZADOS_AGENCIA);
                    st.setString(1, agencia);
                    st.setString(2, fechaI);
                    st.setString(3, fechaF);
                }
                
                ////System.out.println("........ consulta: " + st.toString());
                
                rs = st.executeQuery();
                
                while( rs.next() ){
                    Precinto pr = new Precinto();
                    pr.Load(rs);
                    
                    this.vector.add(pr);
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CONSULTA DE PRECINTOS NO UTILIZADOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Busca un numero de precinto o sticker para ver si existe
     * @autor Ing. Karen Reales
     * @param agencia C�digo de la agencia
     * @param numero
     * @returns true o false
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @version 1.0
     */
    public boolean existe(String agencia, String numero, String tipo)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean esta = false;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select precinto from precintos where agencia =? and tipo_documento=? and precinto=? and numpla = ''");
                st.setString(1, agencia);
                st.setString(2, tipo);
                st.setString(3, numero);
                rs = st.executeQuery();
                
                if( rs.next() ){
                    esta= true;
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR VERIFICANDO SI EL PRECINTO EXISTE O NO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        return esta;
    }
    /**
     * Actualiza el precinto con el numero de planilla que lo utilizo y la fecha de utilizacion
     * @autor Ing. Karen Reales
     * @returns String con el comando para inserccion
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @version 1.0
     */
    public String utilizar()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql="";
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update precintos set numpla=?, fec_utilizacion='now()', user_update = ?  where agencia =? and tipo_documento=? and precinto=?");
                st.setString(1, precinto.getNumpla());
                st.setString(2, precinto.getUser_update());
                st.setString(3, precinto.getAgencia());
                st.setString(4, precinto.getTipoDocumento());
                st.setString(5, precinto.getPrecinto());
                
                sql = st.toString();
            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR ACTUALIZANDO PRECINTOS UTILIZADOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
    }
    /**
     * Obtiene los stickers utilizados por una o todas las agencias
     * en un per�odo determinado
     * @autor Ing. Tito Andr�s Maturana
     * @param agencia C�digo de la agencia
     * @param fechaI Fecha de inicio del reporte
     * @param fechaF Fecha final del reporte
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @version 1.0
     */
    public void stickersUtilizados(String agencia, String fechaI, String fechaF)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        this.vector = new Vector();
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                if( agencia.length()==0 ){
                    st = con.prepareStatement(this.SQL_STICKERS_UTILIZADOS);
                    st.setString(1, fechaI);
                    st.setString(2, fechaF);
                } else {
                    st = con.prepareStatement(this.SQL_STICKERS_UTILIZADOS_AGENCIA);
                    st.setString(1, agencia);
                    st.setString(2, fechaI);
                    st.setString(3, fechaF);
                }
                
                ////System.out.println("........ consulta: " + st.toString());
                
                rs = st.executeQuery();
                
                while( rs.next() ){
                    Precinto pr = new Precinto();
                    pr.Load(rs);
                    pr.setUser_update(rs.getString("usuario"));
                    
                    this.vector.add(pr);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA DE STICKERS UTILIZADOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
     /**
     * Obtiene los stickers no utilizados por una o todas las agencias
     * en un per�odo determinado
     * @autor Ing. Tito Andr�s Maturana
     * @param agencia C�digo de la agencia
     * @param fechaI Fecha de inicio del reporte
     * @param fechaF Fecha final del reporte
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @version 1.0
     */
    public void stickersNoUtilizados(String agencia, String fechaI, String fechaF)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        this.vector = new Vector();
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                if( agencia.length()==0 ){
                    st = con.prepareStatement(this.SQL_STICKERS_NO_UTILIZADOS);
                    st.setString(1, fechaI);
                    st.setString(2, fechaF);
                } else {
                    st = con.prepareStatement(this.SQL_STICKERS_NO_UTILIZADOS_AGENCIA);
                    st.setString(1, agencia);
                    st.setString(2, fechaI);
                    st.setString(3, fechaF);
                }
                
                ////System.out.println("........ consulta: " + st.toString());
                
                rs = st.executeQuery();
                
                while( rs.next() ){
                    Precinto pr = new Precinto();
                    pr.Load(rs);
                    
                    this.vector.add(pr);
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CONSULTA DE STICKERS NO UTILIZADOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return vector;
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        this.vector = vector;
    }
    /**
     * Setter for Object precinto.
     * @param precinto New value of Object precinto.
     */
    public void setPrecinto(Precinto precintos) {
        this.precinto = precintos;
    }
    
    /**
     * Actualiza el precinto dejando vacio el campo de planilla de todos los precintos
     * marcados con ese numero de planilla.
     * @autor Ing. Karen Reales
     * @params  Numero de la planilla
     * @returns String con el comando para inserccion
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @version 1.0
     */
    public String desmarcar()throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql="";
        try {
            st = this.crearPreparedStatement("DESMARCAR_PRECINTOS");
            st.setString(1, precinto.getUser_update());
            //st.setString(2, precinto.getAgencia());
            st.setString(2, precinto.getTipoDocumento());
            st.setString(3, precinto.getNumpla());
            sql = st.toString();
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DESMARCANDO PRECINTOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("DESMARCAR_PRECINTOS");
        }
        return sql;
    } 
    
    
    
   /**
     * Busca un numero de precinto o sticker para ver si ha sido utilizado
     * por otra planilla que no sea la que se esta modificando
     * @autor Ing. Karen Reales
     * @param agencia C�digo de la agencia
     * @param numero
     * @param numero de la planilla
     * @returns true o false
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @version 1.0
     */
    public boolean existe(String agencia, String numero, String tipo,String numpla)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean esta = false;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(EXISTE_PRECINTO_PLANILLA);
                st.setString(1, tipo);
                st.setString(2, numero);
                st.setString(3, agencia);
                st.setString(4, numpla);
                
                rs = st.executeQuery();
                
                if( rs.next() ){
                    esta= true;
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR VERIFICANDO SI EL PRECINTO EXISTE O NO CON UNA PLANILLA DISTINTA A LA DEL PARAMETRO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        return esta;
    }    
       /**
     * Consulta la agencia, el usuario que creo la serie determinada
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param agencia C�digo de la agencia
     * @param tipo_doc Precinto - Sticker
     * @param inicio Inicio de la serie
     * @param fin Fin de la serie
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @version 1.0
     */
    public void consultar(String agencia, String tipo_doc, long inicio, long fin)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        this.vector = new Vector();
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                
                st = con.prepareStatement(this.CONSULTA_SERIE);
                st.setLong(1, inicio);
                st.setLong(2, fin);
                st.setString(3, agencia + "%");
                st.setString(4, tipo_doc + "%");
                
                //////System.out.println("........ consulta: " + st.toString());
                
                rs = st.executeQuery();
                
                while( rs.next() ){
                    Precinto pr = new Precinto();
                    pr.setAgencia(rs.getString("agencia"));
                    pr.setTipo_Documento(rs.getString("tipo_documento"));
                    pr.setSerie(rs.getString("precintos"));
                    pr.setCreation_user(rs.getString("creation_user"));
                    pr.setCreation_date(rs.getString("creation_date"));
                    
                    this.vector.add(pr);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA DE LA SERIE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    /**
     * Actualiza el precinto con el numero de planilla que lo utilizo y la fecha de utilizacion
     * @autor Ing. Karen Reales
     * @returns String con el comando para inserccion
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @version 1.0
     */
    public String reutilizar()throws SQLException{
        
        String sql="";
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = this.crearPreparedStatement("REUTILIZAR");
            st.setString(1, precinto.getNumpla());
            st.setString(2, precinto.getUser_update());
            st.setString(3, precinto.getTipoDocumento());
            st.setString(4, precinto.getPrecinto());
            
            sql = st.toString();
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR ACTUALIZANDO PRECINTOS UTILIZADOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            this.desconectar("REUTILIZAR");
        }
        return sql;
    }
    
     /**
     * Busca un numero de precinto entre las ordenes de carga no anuladas realizadas en una 
     *agencia para verificar si existe o no
     * @autor Ing. Karen Reales
     * @param agencia C�digo de la agencia
     * @param numero de precinto
     * @returns true o false
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @version 1.0
     */
    public boolean existeOCarga(String agencia, String numero, String dstrct, String ocarga)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        boolean esta = false;
        try {
           
            con = this.conectar("BUSCAR_PRECINTO_OCARGA");
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL("BUSCAR_PRECINTO_OCARGA"));
                st.setString(1, dstrct);
                st.setString(2, ocarga);
                st.setString(3, agencia);
                st.setString(4, numero+",");
                
                rs = st.executeQuery();
                
                if( rs.next() ){
                    esta= true;
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR VERIFICANDO SI EL PRECINTO EXISTE O NO EN LA ORDEN DE CARGUE " + e.getMessage());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
               this.desconectar("BUSCAR_PRECINTO_OCARGA");
            }
        }
        
        return esta;
    }
    
    /**
     * Consulta la agencia, el usuario que creo la serie determinada
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param agencia C�digo de la agencia
     * @param tipo_doc Precinto - Sticker
     * @param inicio Inicio de la serie
     * @param fin Fin de la serie
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @version 1.0
     */
    public void consultar(String agencia, String tipo_doc)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        this.vector = new Vector();
        
        org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
        
        boolean esta = false;
        try {
           
            con = this.conectar("SQL_CONSULTA_SERIE");
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL("SQL_CONSULTA_SERIE"));
                st.setString(1, agencia );
                st.setString(2, tipo_doc );
                
                logger.info("Consulta: " + st.toString());
                
                rs = st.executeQuery();
                
                while( rs.next() ){
                    Precinto pr = new Precinto();
                    pr.setAgencia(rs.getString("agencia"));
                    pr.setTipo_Documento(rs.getString("tipo_documento"));
                    pr.setSerie(rs.getString("precintos"));
                    pr.setCreation_user(rs.getString("creation_user"));
                    pr.setCreation_date(rs.getString("creation_date"));
                    
                    this.vector.add(pr);
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CONSULTA DE LA SERIE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
               this.desconectar("SQL_CONSULTA_SERIE");
            }
        }
    }

}
