/***************************************
    * Nombre Clase ............. MigrarOP266DAO.java
    * Descripci�n  .. . . . . .  Permite generar los SQL para buscar las OP y sus Item, de cada OC
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  12/10/2005
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/


package com.tsp.operation.model.DAOS;



import java.io.*;
import java.util.*;
import java.sql.*;
import com.tsp.util.Utility;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;



public class MigrarOP266DAO {
    
    
    /** ____________________________________________________________________________________________
                                              ATRIBUTOS
        ____________________________________________________________________________________________ */
    

    
     private double  VLR_ICA;
    
     
     
    
    // POSTGRES:
        private static String SERACH_OC_ANULADAS =    
                                                " SELECT dstrct, documento FROM documento_anulado  "+
                                                " WHERE                                            "+
                                                "      tipodoc         = '001'                     "+ 
                                                "  and fecha_migracion = '0099-01-01'              "; 

    
        
    
        private static String UPDATE_DOCUMENTO =    
                                                " UPDATE   documento_anulado             "+
                                                " SET fecha_migracion    = ?             "+
                                                " WHERE  tipodoc         = '001'         "+
                                                "    AND fecha_migracion = '0099-01-01'  "+
                                                "    AND documento       = ?             ";
                                              

    
        
        
    
    // ORACLE:
    
        private static final String OP_OC = 
    
                                              "SELECT                                     "+
                                              "      a.dstrct_code         distrito,      "+                         
                                              "      a.BRANCH_CODE         banco,         "+
                                              "      a.BANK_ACCT_NO        sucursal,      "+
                                              "      a.accountant          grabador,      "+
                                              "      a.supplier_no         placa,         "+
                                              "      a.INV_NO              factura,       "+      
                                              "      (a.for_inv_orig * -1) valor,         "+
                                              "      a.currency_type       moneda         "+    
                                              " FROM                                      "+  
                                              "       msf260   a,                         "+  
                                              "       msf26a   b                          "+
                                              " WHERE                                     "+  
                                              "       b.PO_NO      = ?                    "+       
                                              "   and a.INV_TYPE   IN ('0','1')           "+  
                                              "   and b.dstrct_code     =  a.dstrct_code  "+                 
                                              "   and b.supplier_no     =  a.supplier_no  "+               
                                              "   and b.inv_no          =  a.inv_no       ";

    
    
    
        
    private static final String ITEM_OP =
    
                                            " SELECT                                       "+
                                            "   b.DSTRCT_CODE         DISTRITO,            "+     
                                            "   b.INV_NO              FACTURA,             "+     
                                            "   b.inv_item_no         ITEM,                "+     
                                            "   b.INV_ITEM_DESC       DESCRIPCION,         "+     
                                            "   (b.for_val_invd*-1)   ITEM_VAL,            "+ 
                                            "   b.whold_tax_code      RETENCION_ITEM,      "+
                                            "   b.authsd_by           AUTORIZADO,          "+
                                            "   b.po_no               OC,                  "+
                                            "   rica.atax_code        RICA_CODE,           "+
                                            "   rica.atax_amount_l    RICA_VLR,            "+
                                            "   b.ACCOUNT_CODE        ACCOUNT_CODE,        "+
                                            "   b.work_order          OT,                  "+
                                            "   b.pp_amt_loc          IRENTA_ITEM          "+
                                            " FROM                                         "+      
                                            "     msf26a b,                                "+  
                                            "     msf263 rica                              "+    
                                            " WHERE                                        "+
                                            "      b.dstrct_code =  ?                      "+     
                                            "  and b.supplier_no =  ?                      "+  
                                            "  and b.inv_no      =  ?                      "+
                                            "  and b.inv_item_no like '%'                  "+
                                            "  AND b.dstrct_code = rica.inv_dstrct_code(+) "+  
                                            "  AND b.supplier_no = rica.inv_supplier_no(+) "+ 
                                            "  AND b.inv_no      = rica.inv_no(+)          "+ 
                                            "  AND b.inv_item_no = rica.inv_item_no(+)     "+       
                                            "  AND 'RICA'        = rica.tax_ref(+)         ";

        
        
    
    
    /** ____________________________________________________________________________________________
                                              METODOS
        ____________________________________________________________________________________________ */
    
    
    public MigrarOP266DAO() { }
    
    
    
    
    
    public List searchOCAnuladas()throws Exception{
          PoolManager poolManager     = PoolManager.getInstance();
          Connection con              = poolManager.getConnection("fintra");
          if (con == null)
                throw new SQLException("Sin conexion");
          PreparedStatement st        = null;
          ResultSet         rs        = null;
          List              lista     = null;
          try {

              st= con.prepareStatement( this.SERACH_OC_ANULADAS );
              rs=st.executeQuery();
              while(rs.next()){
                 if( lista==null) lista = new LinkedList();
                 OP266  ocAnulada = new OP266();
                    ocAnulada.setDistrito ( rs.getString(1));
                    ocAnulada.setOc       ( rs.getString(2));                 
                 lista.add(ocAnulada);
              }
              
              lista = this.searchOP(lista);
              
          }
          catch(SQLException e){
              throw new SQLException(" DAO: No se pudo buscar las OC Anuladas.-->"+ e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             if(rs!=null) rs.close();
             poolManager.freeConnection("fintra",con);
          } 
          return lista;
    }
    
    
    
    
    
    
    public List searchOP(List lista)throws Exception{
          PoolManager poolManager     = PoolManager.getInstance();
          Connection con              = poolManager.getConnection("oracle");
          if (con == null)
                throw new SQLException("Sin conexion");
          PreparedStatement st        = null;
          ResultSet         rs        = null;
          try {
              if(lista!=null){
                 String hoy = Utility.getHoy("/");  
                 
                 st= con.prepareStatement( this.OP_OC );
                 Iterator it = lista.iterator();
                 while(it.hasNext()){
                     OP266  ocAnulada = (OP266) it.next();
                     st.setString(1, ocAnulada.getOc() );
                     rs=st.executeQuery();
                     while(rs.next()){
                          ocAnulada.setBanco    ( rs.getString(2) );
                          ocAnulada.setSucursal ( rs.getString(3) );
                          ocAnulada.setGrabador ( rs.getString(4) );
                          ocAnulada.setPlaca    ( rs.getString(5) );
                          ocAnulada.setOP       ( rs.getString(6) );
                          ocAnulada.setValor    ( rs.getString(7) );
                          ocAnulada.setMoneda   ( rs.getString(8) );
                          
                          ocAnulada.setFechaCumplido (hoy);
                          ocAnulada.setFechaPago     (hoy);
                          
                          ocAnulada.setItems    ( this.searchItemOP(ocAnulada, con)  );
                          ocAnulada.setVlrIca   ( String.valueOf( this.VLR_ICA) );
                          
                     }
                     st.clearParameters();
                 }
              }
          }
          catch(SQLException e){
              throw new SQLException(" DAO: No se pudo buscar las OP de OC Anuladas.-->"+ st.toString() + e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             if(rs!=null) rs.close();
             poolManager.freeConnection("oracle",con);
          } 
          return lista;
    }
    
    
    
    
    
    
    
    public List searchItemOP(OP266  ocAnulada, Connection con)throws Exception{       
          PreparedStatement st        = null;
          ResultSet         rs        = null;
          List              lista     = null;
          this.VLR_ICA                = 0;
          
          try {
              st= con.prepareStatement( this.ITEM_OP );
                st.setString(1,  ocAnulada.getDistrito() );
                st.setString(2,  ocAnulada.getPlaca()    );
                st.setString(3,  ocAnulada.getOP()       );                
              rs=st.executeQuery();
              while(rs.next()){
                 if( lista==null) lista = new LinkedList();
                 Items  item  = new Items();
                    item.setDistrito   ( ( rs.getString(1) ==null) ?"": rs.getString(1)  );
                    item.setFactura    ( ( rs.getString(2) ==null) ?"": rs.getString(2)  );
                    item.setItem       ( ( rs.getString(3) ==null) ?"": rs.getString(3)  );                    
                    item.setItemDesc   ( ( rs.getString(4) ==null) ?"": rs.getString(4)  );
                    item.setValor      ( ( rs.getString(5) ==null) ?"": rs.getString(5)  );                    
                    item.setWhTax      ( ( rs.getString(6) ==null) ?"": rs.getString(6)  );
                    item.setAutorizado ( ( rs.getString(7) ==null) ?"": rs.getString(7)  );                    
                    item.setOC         ( ( rs.getString(8) ==null) ?"": rs.getString(8)  );                    
                    item.setReteIca    ( ( rs.getString(9) ==null) ?"": rs.getString(9)  );                    
                    item.setAccountCode( ( rs.getString(11)==null) ?"": rs.getString(11) );
                    item.setOT         ( ( rs.getString(12)==null) ?"": rs.getString(12) );
                    
                    this.VLR_ICA += rs.getDouble(10);
                    
                 lista.add(item);
              }       
              
          }
          catch(SQLException e){
              throw new SQLException(" DAO: No se pudo buscar los Item de la OP.-->"+ e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             if(rs!=null) rs.close();
          } 
          return lista;
    }
    
    
    
    
    
    
    
    
     public void UpdateDocument(List lista)throws Exception{
          PoolManager poolManager     = PoolManager.getInstance();
          Connection con              = poolManager.getConnection("fintra");
          if (con == null)
                throw new SQLException("Sin conexion");          
          PreparedStatement st        = null;          
          try {
              if(lista!=null){
                 String hoy = Utility.getHoy("-");                   
                 st= con.prepareStatement( this.UPDATE_DOCUMENTO );
                 Iterator it = lista.iterator();
                 while(it.hasNext()){
                     OP266  ocAnulada = (OP266) it.next();
                     st.setString(1, hoy);
                     st.setString(2, ocAnulada.getOc() );
                     st.execute(); 
                     st.clearParameters();
                 }
              }
          }
          catch(SQLException e){
              throw new SQLException(" DAO: No se pudo Actualizar Documentos-->"+ st.toString() + e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             poolManager.freeConnection("fintra",con);
          } 
    }
    
    
    
    
    
    
}
