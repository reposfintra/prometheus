/*
 * StandarDAO.java
 *
 * Created on 9 de enero de 2007, 04:16 PM
 */

package com.tsp.operation.model.DAOS;
import java.sql.*;
import java.util.LinkedList;
import org.apache.log4j.Logger;
import java.util.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  Ivan Dario Gomez
 */
public class StandarDAO extends MainDAO{
    
    /** Creates a new instance of StandarDAO */
    public StandarDAO() {
        super("StandarDAO.xml");
    }
    public StandarDAO(String dataBaseName) {
        super("StandarDAO.xml", dataBaseName);
    }
    
    /**
     * M�todo buscarTableCode  metodo para buscar datos en tablagen
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String nombre de la tabla
     * @version.....1.0.
     **/
    public List buscarTableCode(String table_type)throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List lista = null;
        try {
            ps = this.crearPreparedStatement("SQL_TBLGEN");
            ps.setString(1, table_type);
            rs = ps.executeQuery();
            lista = new LinkedList();
            while(rs.next()){
                TablaGen t = new TablaGen();
                t.setTable_code(rs.getString("table_code"));
                t.setReferencia(rs.getString("referencia"));
                t.setDescripcion(rs.getString("descripcion"));
                lista.add(t);
                
            }
        }
        catch( Exception ex ){
            throw new SQLException("ERROR OBTENIENDO REGISTROS DE TABLAGEN en buscarTableCode: "+ex.getMessage());
        }
        finally{
            if ( rs != null ) {rs.close();}
            if ( ps != null ) {ps.close();}
            this.desconectar("SQL_TBLGEN");
        }
        return lista;
    }
    
    
    /**
     * M�todo buscarCliente  metodo para buscar los datos de un cliente
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String nombre de la tabla
     * @version.....1.0.
     **/
    public Cliente buscarCliente(String codcli)throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List lista = null;
        Cliente cli  = null;
        try {
            ps = this.crearPreparedStatement("SQL_BUSCAR_CLIENTE");
            ps.setString(1, codcli.toUpperCase());
            rs = ps.executeQuery();
            if(rs.next()){
                cli = new Cliente();
                cli.setCodcli(rs.getString("codcli"));
                cli.setAgduenia(rs.getString("agduenia"));
                cli.setNomcli(rs.getString("nomcli"));
                String a = rs.getString("sec_standard");
                if(a.length()<3){
                    String b="";
                    for(int i=0; i<3-a.length();i++ ){
                        b +="0";
                    }
                    a = b+a;
                }
                cli.setSec_standard(a);
                
            }
        }
        catch( Exception ex ){
            throw new SQLException("ERROR OBTENIENDO REGISTROS SQL_BUSCAR_CLIENTE: "+ex.getMessage());
        }
        finally{
            if ( rs != null ) {rs.close();}
            if ( ps != null ) {ps.close();}
            this.desconectar("SQL_BUSCAR_CLIENTE");
        }
        return cli;
    }
    
    
    /**
     * M�todo buscarCliente  metodo para buscar los datos de un cliente
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String nombre de la tabla
     * @version.....1.0.
     **/
    public List buscarClientePorNombre(String nomcli)throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List lista = null;
        Connection con =null;
        try {
            String sql = this.obtenerSQL("SQL_BUSCAR_CLIENTE_POR_NOMBRE").replaceAll("#CLIENTE#",nomcli.toUpperCase());
            con = this.conectar("SQL_BUSCAR_CLIENTE_POR_NOMBRE");
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            lista = new LinkedList();
            while(rs.next()){
                Cliente cli = new Cliente();
                cli.setCodcli(rs.getString("codcli"));
                cli.setAgduenia(rs.getString("agduenia"));
                cli.setNomcli(rs.getString("nomcli"));
                String a = rs.getString("sec_standard");
                if(a.length()<3){
                    String b="";
                    for(int i=0; i<3-a.length();i++ ){
                        b +="0";
                    }
                    a = b+a;
                }
                cli.setSec_standard(a);
                lista.add(cli);
                
            }
        }
        catch( Exception ex ){
            ex.printStackTrace();
            throw new SQLException("ERROR OBTENIENDO REGISTROS SQL_BUSCAR_CLIENTE: "+ex.getMessage());
        }
        finally{
            if ( rs != null ) {rs.close();}
            if ( ps != null ) {ps.close();}
            this.desconectar("SQL_BUSCAR_CLIENTE_POR_NOMBRE");
        }
        return lista;
    }
    
    
    /**
     * M�todo buscarCiudad  metodo para buscar la ciudad
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String nombre de la tabla
     * @version.....1.0.
     **/
    public List buscarCiudad(String nomciu)throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        Connection con = null;
        List listaCiudad =null;
        try {
            String sql = this.obtenerSQL("SQL_BUSCAR_CIUDAD").replaceAll("#CIUDAD#",nomciu.toUpperCase());;
            con = this.conectar("SQL_BUSCAR_CIUDAD");
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            listaCiudad = new LinkedList();
            while(rs.next()){
                Ciudad ciudad = new Ciudad();
                ciudad.setCodCiu(rs.getString("codciu"));
                ciudad.setNomCiu(rs.getString("nomciu"));
                ciudad.setPais(rs.getString("pais"));
                listaCiudad.add(ciudad);
                
            }
        }
        catch( Exception ex ){
            throw new SQLException("ERROR OBTENIENDO REGISTROS SQL_BUSCAR_CIUDAD: "+ex.getMessage());
        }
        finally{
            if ( rs != null ) {rs.close();}
            if ( ps != null ) {ps.close();}
            this.desconectar("SQL_BUSCAR_CIUDAD");
        }
        return listaCiudad;
    }
    
    
    
    /**
     * M�todo buscarCiudadPorCodigo  metodo para buscar la ciudad
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String nombre de la tabla
     * @version.....1.0.
     **/
    public Ciudad buscarCiudadPorCodigo(String codciu)throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs  = null;
        Ciudad ciudad = null;
        try {
            ps = this.crearPreparedStatement("SQL_BUSCAR_CIUDAD_POR_CODIGO");
            ps.setString(1, codciu.toUpperCase());
            rs = ps.executeQuery();
            if(rs.next()){
                ciudad = new Ciudad();
                ciudad.setCodCiu(rs.getString("codciu"));
                ciudad.setNomCiu(rs.getString("nomciu"));
                ciudad.setPais(rs.getString("pais"));
                
                
            }
        }
        catch( Exception ex ){
            throw new SQLException("ERROR OBTENIENDO REGISTROS SQL_BUSCAR_CIUDAD_POR_CODIGO: "+ex.getMessage());
        }
        finally{
            if ( rs != null ) {rs.close();}
            if ( ps != null ) {ps.close();}
            this.desconectar("SQL_BUSCAR_CIUDAD_POR_CODIGO");
        }
        return ciudad;
    }
    
    
    /**
     * M�todo buscarMonedas  metodo para buscar las monedas
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......
     * @version.....1.0.
     **/
    public List buscarMonedas()throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List lista = null;
        try {
            ps = this.crearPreparedStatement("SQL_BUSCAR_MONEDAS");
            rs = ps.executeQuery();
            lista = new LinkedList();
            while(rs.next()){
                Moneda m = new Moneda();
                m.setCodMoneda(rs.getString("codmoneda"));
                m.setNomMoneda(rs.getString("nommoneda"));
                lista.add(m);
                
            }
        }
        catch( Exception ex ){
            throw new SQLException("ERROR OBTENIENDO REGISTROS DE SQL_BUSCAR_MONEDAS: "+ex.getMessage());
        }
        finally{
            if ( rs != null ) {rs.close();}
            if ( ps != null ) {ps.close();}
            this.desconectar("SQL_BUSCAR_MONEDAS");
        }
        return lista;
    }
    
    /**
     * M�todo buscarCliente  metodo para buscar los datos de un cliente
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String nombre de la tabla
     * @version.....1.0.
     **/
    public EquivalenciaCarga buscarCarga(String codcarga)throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List lista = null;
        EquivalenciaCarga eq  = null;
        try {
            ps = this.crearPreparedStatement("SQL_BUSCAR_CARGA_POR_CODIGO");
            ps.setString(1, codcarga.toUpperCase());
            rs = ps.executeQuery();
            if(rs.next()){
                eq = new EquivalenciaCarga();
                eq.setCodigo_carga(rs.getString("codigo_carga"));
                eq.setDescripcion_carga(rs.getString("descripcion_carga"));
                eq.setDescripcion_corta(rs.getString("descripcion_corta"));
            }
        }
        catch( Exception ex ){
            throw new SQLException("ERROR OBTENIENDO REGISTROS SQL_BUSCAR_CARGA_POR_CODIGO: "+ex.getMessage());
        }
        finally{
            if ( rs != null ) {rs.close();}
            if ( ps != null ) {ps.close();}
            this.desconectar("SQL_BUSCAR_CARGA_POR_CODIGO");
        }
        return eq;
    }
    
    
    
    /**
     * M�todo buscarCargaPorDescripcion  metodo para buscar los datos de la carga
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String descripcion de la carga
     * @version.....1.0.
     **/
    public List buscarCargaPorDescripcion(String desc)throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List lista = null;
        Connection con =null;
        try {
            String sql = this.obtenerSQL("SQL_BUSCAR_CARGA_DESCRIPCION").replaceAll("#DESC#",desc.toUpperCase());
            con = this.conectar("SQL_BUSCAR_CARGA_DESCRIPCION");
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            lista = new LinkedList();
            while(rs.next()){
                EquivalenciaCarga eq = new EquivalenciaCarga();
                eq.setCodigo_carga(rs.getString("codigo_carga"));
                eq.setDescripcion_carga(rs.getString("descripcion_carga"));
                eq.setDescripcion_corta(rs.getString("descripcion_corta"));
                lista.add(eq);
                
            }
        }
        catch( Exception ex ){
            ex.printStackTrace();
            throw new SQLException("ERROR OBTENIENDO REGISTROS SQL_BUSCAR_CARGA_DESCRIPCION: "+ex.getMessage());
        }
        finally{
            if ( rs != null ) {rs.close();}
            if ( ps != null ) {ps.close();}
            this.desconectar("SQL_BUSCAR_CARGA_DESCRIPCION");
        }
        return lista;
    }
    
    
    
    /**
     * M�todo grabarEstandar  metodo para guardar el estandar
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......objeto Standar
     * @version.....1.0.
     **/
    public String grabarEstandar(Standar sj)throws SQLException {
        Connection con = null;
        Statement st = null;
        PreparedStatement ps = null;
        String sql="";
        String SQL_INSERT_SJ      = this.obtenerSQL("SQL_INSERT_SJ");
        String SQL_INSERT_TARIFA  = this.obtenerSQL("SQL_INSERT_TARIFA");
        String SQL_INSERT_TRAMO   = this.obtenerSQL("SQL_INSERT_TRAMO");
        String SQL_INSERT_RECURSO = this.obtenerSQL("SQL_INSERT_RECURSO");
        String SQL_INSERT_FLETE   = this.obtenerSQL("SQL_INSERT_FLETE");
        String SQL_AUMENTAR_SECUENCIA = this.obtenerSQL("SQL_AUMENTAR_SECUENCIA");
        try {
            con = this.conectar("SQL_INSERT_SJ");
            ps = con.prepareStatement(SQL_INSERT_SJ);
            ps.setString(1,  sj.getDstrct_code());
            ps.setString(2,  sj.getCodigo_sj());     //std_job_no
            ps.setString(3,  sj.getDescripcion_sj());//std_job_desc
            ps.setString(4,  sj.getCod_cli());
            ps.setString(5,  sj.getTipo_ruta());     //wo_type
            ps.setString(6,  sj.getAg_responsable());//originator_id
            ps.setString(7,  sj.getOrigen());        //origin_code
            ps.setString(8,  sj.getDestino());       //destination_code
            ps.setString(9,  sj.getNom_origen());
            ps.setString(10, sj.getNom_destino());
            ps.setString(11, sj.getStandar_general());//stdjobgen
            ps.setString(12, (sj.isBloq_despacho())?"S":"N");
            ps.setString(13, sj.getFrontera_asoc());
            ps.setString(14,(sj.isPlaneable())?"S":"N");
            ps.setString(15, sj.getTipo());
            ps.setString(16, sj.getPagador());
            ps.setString(17, sj.getAccount_code_c());
            ps.setString(18, sj.getAccount_code_i());
            ps.setString(19, sj.getCodigo_carga());
            ps.setString(20, sj.getAg_duenia() );
            ps.setInt   (21, sj.getListaTarifa().size());
            ps.setString(22, (sj.isRemesaFacturable())?"S":"N");
            ps.setString(23, sj.getE_mail());
            ps.setString(24, (sj.isReq_remesa_padre())?"S":"N");
            ps.setDouble(25, sj.getVlr_tope_carga());
            ps.setString(26, sj.getTipo_facturacion());
            ps.setInt   (27, sj.getPeriodoFac_inicial());
            ps.setInt   (28, sj.getPeriodoFac_final());
            ps.setString(29, (sj.isInd_vacio())?"S":"N");
            ps.setString(30, sj.getCreation_user());
            ps.setString(31, sj.getBase());
            
            sql+= ps.toString()+";";
            
            
            ps = con.prepareStatement(SQL_AUMENTAR_SECUENCIA);
            ps.setString(1, sj.getCod_cli());
            
            sql += ps.toString()+";";
            
            //bloque para insertar los datos de las tarifas
            for(int i =0; i< sj.getListaTarifa().size();i++ ){
                Standar tarifa = (Standar) sj.getListaTarifa().get(i);;
                ps = con.prepareStatement(SQL_INSERT_TARIFA);
                ps.setString(1, sj.getDstrct_code());
                ps.setString(2, sj.getCodigo_sj());
                ps.setInt   (3, i+1);//tarifa.getSeq_tarifa());
                ps.setDouble(4, tarifa.getVlr_tarifa());
                ps.setString(5, tarifa.getMoneda_tarifa());
                ps.setString(6, tarifa.getUnidad_tarifa());
                ps.setString(7, (tarifa.isVigente_tarifa())?"S":"N");
                ps.setString(8, sj.getCreation_user());
                ps.setString(9, sj.getBase());
                
                sql+= ps.toString()+";";
            }
            //fin bloque de tarifas
            
            //inicio del bloque para llenar los tramos
            for(int t =0; t< sj.getListaTramos().size();t++ ){
                Standar tramo = (Standar) sj.getListaTramos().get(t);
                ps = con.prepareStatement(SQL_INSERT_TRAMO);
                ps.setString(1,  sj.getDstrct_code());
                ps.setString(2,  sj.getCodigo_sj());
                ps.setString(3,  sj.getCod_cli());
                ps.setString(4,  tramo.getCod_origen_tramo());
                ps.setString(5,  tramo.getCodigo_destino_tramo());
                ps.setString(6,  tramo.getPorcentaje_tramo());
                ps.setString(7,  (tramo.isInd_vacio_tramo())?"S":"N");
                ps.setInt   (8,  tramo.getNumero_escoltas());
                ps.setString(9, tramo.getTipo_trafico());
                ps.setString(10, (tramo.isControl_cargue())?"S":"N");
                ps.setString(11, (tramo.isControl_descargue())?"S":"N");
                ps.setString(12, (tramo.isPreferencia())?"S":"N");
                ps.setString(13, (tramo.isContingente())?"S":"N");
                ps.setString(14, (tramo.isCaravana())?"S":"N");
                ps.setString(15, (tramo.isRequiere_planviaje())?"S":"N");
                ps.setString(16, (tramo.isRequiere_hojareporte())?"S":"N");
                ps.setString(17, tramo.getPeso_lleno_tramo());
                ps.setString(18, sj.getCreation_user());
                ps.setString(19, sj.getBase());
                
                sql+= ps.toString()+";";
                int size_recurso = ((Standar) sj.getListaTramos().get(t)).getListaRecursos().size(); //Saco el size de la lista de recursos
                for(int r =0; r< size_recurso;r++ ){
                    Standar recurso = (Standar) tramo.getListaRecursos().get(r);
                    int size_flete = ((Standar)((Standar) sj.getListaTramos().get(t)).getListaRecursos().get(r)).getListaFletes().size(); //Saco el size de la lista de recursos
                   
                    ps = con.prepareStatement(SQL_INSERT_RECURSO);
                    ps.setString(1, sj.getDstrct_code());
                    ps.setString(2, sj.getCodigo_sj());
                    ps.setString(3, tramo.getCod_origen_tramo());
                    ps.setString(4, tramo.getCodigo_destino_tramo());
                    ps.setString(5, recurso.getTipo_recurso());
                    ps.setString(6, recurso.getCodigo_recurso());
                    ps.setInt   (7, recurso.getPrioridad() );  //me falta definir la prioridad
                    ps.setString(8, sj.getCreation_user());
                    ps.setString(9, sj.getBase());
                    ps.setInt   (10, size_flete );
                    
                    sql+= ps.toString()+";";
                    
                   
                    for(int f =0; f< size_flete;f++ ){
                        Standar flete = (Standar) recurso.getListaFletes().get(f);
                        ps = con.prepareStatement(SQL_INSERT_FLETE);
                        ps.setString(1, sj.getDstrct_code());
                        ps.setString(2, sj.getCodigo_sj());
                        ps.setString(3, tramo.getCod_origen_tramo());
                        ps.setString(4, tramo.getCodigo_destino_tramo());
                        ps.setInt   (5, f+1);
                        ps.setDouble(6, flete.getVlr_flete());
                        ps.setString(7, flete.getMoneda_flete());
                        ps.setString(8, flete.getUnidad_flete());
                        ps.setString(9, (flete.isVigente_flete())?"S":"N");
                        ps.setString(10, sj.getCreation_user());
                        ps.setString(11, sj.getBase());
                        ps.setString(12, recurso.getCodigo_recurso());
                        
                        sql+= ps.toString()+";";
                    }
                    
                }
                
            }
            //fin del bloque de tramos
            
            
            
           
            
        }
        catch( Exception ex ){
            ex.printStackTrace();
           
            throw new SQLException("ERROR OBTENIENDO REGISTROS SQL_BUSCAR_CIUDAD: "+ex.getMessage());
        }
        finally{
            if ( st != null )  {st.close();}
            if ( ps != null )  {ps.close();}
            this.desconectar("SQL_INSERT_SJ");
        }
        
        return sql;
        
    }
    
    
    /**
     * M�todo buscarRecurso  metodo para buscar los datos de un recurso
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String codigo del recurso
     * @version.....1.0.
     **/
    public Recursosdisp buscarRecurso(String codrecurso)throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List lista = null;
        Recursosdisp recurso  = null;
        try {
            ps = this.crearPreparedStatement("SQL_BUSCAR_RECURSO");
            ps.setString(1, codrecurso.toUpperCase());
            rs = ps.executeQuery();
            if(rs.next()){
                recurso = new Recursosdisp();
                recurso.setRecurso(rs.getString("recurso"));
                recurso.setTipo(rs.getString("tipo_recurso"));
                recurso.setDestino(rs.getString("descri"));//se guarda la descripcion
                
                
            }
        }
        catch( Exception ex ){
            throw new SQLException("ERROR OBTENIENDO REGISTROS SQL_BUSCAR_RECURSO: "+ex.getMessage());
        }
        finally{
            if ( rs != null ) {rs.close();}
            if ( ps != null ) {ps.close();}
            this.desconectar("SQL_BUSCAR_RECURSO");
        }
        return recurso;
    }
    
    
    
    
    /**
     * M�todo buscarRecursoPorDescripcion  metodo para buscar los datos de un recurso
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String codigo del recurso
     * @version.....1.0.
     **/
    public List buscarRecursoPorDescripcion(String codrecurso)throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List lista = null;
        Connection con = null;
        try {
            con = this.conectar("SQL_BUSCAR_RECURSO_POR_DESC");
            String sql = this.obtenerSQL("SQL_BUSCAR_RECURSO_POR_DESC").replaceAll("#DESC#",codrecurso.toUpperCase());
            ps = con.prepareStatement(sql);
            
            rs = ps.executeQuery();
            lista = new LinkedList();
            while(rs.next()){
                Recursosdisp recurso = new Recursosdisp();
                recurso.setRecurso(rs.getString("recurso"));
                recurso.setTipo(rs.getString("tipo_recurso"));
                recurso.setDestino(rs.getString("descri"));//se guarda la descripcion
                lista.add(recurso);
                
            }
        }
        catch( Exception ex ){
            throw new SQLException("ERROR OBTENIENDO REGISTROS SQL_BUSCAR_RECURSO_POR_DESC: "+ex.getMessage());
        }
        finally{
            if ( rs != null ) {rs.close();}
            if ( ps != null ) {ps.close();}
            this.desconectar("SQL_BUSCAR_RECURSO_POR_DESC");
        }
        return lista;
    }
    
    
    /**
     * M�todo buscarRecurso  metodo para buscar los datos de un recurso
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String codigo del recurso
     * @version.....1.0.
     **/
    public boolean existeSj(String dstrct, String cod_sj)throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean existe  = false;
        try {
            ps = this.crearPreparedStatement("SQL_EXISTE_ESTANDAR");
            ps.setString(1, dstrct);
            ps.setString(2, cod_sj);
            
            rs = ps.executeQuery();
            if(rs.next()){
               existe = true; 
            }
        }
        catch( Exception ex ){
            throw new SQLException("ERROR OBTENIENDO REGISTROS SQL_EXISTE_ESTANDAR: "+ex.getMessage());
        }
        finally{
            if ( rs != null ) {rs.close();}
            if ( ps != null ) {ps.close();}
            this.desconectar("SQL_EXISTE_ESTANDAR");
        }
        return existe;
    }
    
    
     /**
     * M�todo existeTramo  metodo para buscar el tramo
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String origen String destino
     * @version.....1.0.
     **/
    public boolean existeTramo(String origen, String destino)throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean existe  = false;
        try {
            ps = this.crearPreparedStatement("SQL_EXISTE_TRAMO");
            ps.setString(1, origen);
            ps.setString(2, destino);
            //System.out.println("TRAMO--->"+ps.toString());
            rs = ps.executeQuery();
            if(rs.next()){
               existe = true; 
            }
        }
        catch( Exception ex ){
            throw new SQLException("ERROR OBTENIENDO REGISTROS SQL_EXISTE_TRAMO: "+ex.getMessage());
        }
        finally{
            if ( rs != null ) {rs.close();}
            if ( ps != null ) {ps.close();}
            this.desconectar("SQL_EXISTE_TRAMO");
        }
        return existe;
    }
    
    
     /**
     * M�todo ListarEstandares  metodo para buscar estandares
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String codigo del recurso
     * @version.....1.0.
     **/
    public List ListarEstandares(String dstrct, String codsj, String codcli,String fechaini,String fechafin)throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List lista = null;
        Connection con = null;
        try {
            con = this.conectar("SQL_BUSCAR_ESTANDARES");
            String sql = this.obtenerSQL("SQL_BUSCAR_ESTANDARES");
            sql = (!codsj.equals(""))?sql.replaceAll("#1#","AND std_job_no  = '"+codsj.toUpperCase()+"'"):sql.replaceAll("#1#",""); 
            sql = (!codcli.equals(""))?sql.replaceAll("#2#","AND codcli  = '"+codcli.toUpperCase()+"'"):sql.replaceAll("#2#",""); 
            sql = (!fechaini.equals(""))?sql.replaceAll("#3#","AND substr(creation_date,1,10) between '"+fechaini+"'  AND  '"+fechafin+"'"):sql.replaceAll("#3#",""); 
            
            ps = con.prepareStatement(sql);
            ps.setString(1, dstrct);
            rs = ps.executeQuery();
            lista = new LinkedList();
            while(rs.next()){
                Standar standar = new Standar();
                standar.setDstrct_code(rs.getString("dstrct_code"));
                standar.setOrigen(rs.getString("origin_code"));
                standar.setCod_cli(rs.getString("codcli"));
                standar.setNomcli(rs.getString("nombre_cliente"));
                standar.setAg_duenia(rs.getString("ageduenia"));
                standar.setPagador(rs.getString("cod_pagador"));
                
                standar.setCodigo_sj(rs.getString("std_job_no"));
                standar.setDescripcion_sj(rs.getString("std_job_desc"));
                standar.setTipo(rs.getString("tipo"));
                
                standar.setStandar_general(rs.getString("stdjobgen"));
                standar.setBloq_despacho((rs.getString("bloquea_despacho").equals("S")?true:false));
                
                
                standar.setE_mail(rs.getString("email_aut_bloqueo"));
                standar.setAg_responsable(rs.getString("originator_id"));
                
                standar.setAprobado((rs.getString("aprobacion").equals("S")?true:false));
                standar.setPlaneable((rs.getString("planeable").equals("S")?true:false));
                standar.setNom_origen(rs.getString("origin_name"));
                standar.setDestino(rs.getString("destination_code"));
                
                standar.setNom_destino(rs.getString("destination_name"));
                
                standar.setInd_vacio((rs.getString("ind_vacio").equals("S")?true:false));
                standar.setReq_remesa_padre((rs.getString("req_remesa_padre").equals("S")?true:false));
                standar.setRemesaFacturable((rs.getString("remesa_facturable").equals("S")?true:false));
                
                
                standar.setTipo_ruta(rs.getString("wo_type"));
                standar.setCodigo_carga(rs.getString("cod_carga"));
                standar.setTipo_facturacion(rs.getString("tipo_facturacion"));
                standar.setVlr_tope_carga(rs.getDouble("vlr_tope_carga"));
                
                standar.setPeriodoFac_inicial(rs.getInt("per_fac_inicial"));
                standar.setPeriodoFac_final(rs.getInt("per_fac_final"));
                
                //standar.setDesc_carga(rs.getString("ageduenia"));
                //standar.setDesc_carga_corta(rs.getString("ageduenia"));
                lista.add(standar);
                
            }
        }
        catch( Exception ex ){
            throw new SQLException("ERROR OBTENIENDO REGISTROS SQL_BUSCAR_ESTANDARES: "+ex.getMessage());
        }
        finally{
            if ( rs != null ) {rs.close();}
            if ( ps != null ) {ps.close();}
            this.desconectar("SQL_BUSCAR_ESTANDARES");
        }
        return lista;
    }
    
    
     /**
     * M�todo ListarEstandares  metodo para buscar estandares
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String codigo del recurso
     * @version.....1.0.
     **/
    public Standar buscarEstandar(String dstrct, String codsj, String codcli)throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        PreparedStatement ps2 = null;
        ResultSet rs2 = null;
        PreparedStatement ps3 = null;
        ResultSet rs3 = null;
        PreparedStatement ps4 = null;
        ResultSet rs4 = null;
        PreparedStatement ps5 = null;
        ResultSet rs5 = null;
        List lista = null;
        Connection con = null;
        Standar standar = new Standar();
        try {
            con = this.conectar("SQL_BUSCAR_SJ");
            String sql = this.obtenerSQL("SQL_BUSCAR_SJ");
            ps = con.prepareStatement(sql);
            ps.setString(1, dstrct);
            ps.setString(2, codsj);
            ps.setString(3, codcli);
            rs = ps.executeQuery();
            
            if(rs.next()){
                
                standar.setUnidad_negocio(rs.getString("account_code_c").substring(3,6));
                
                standar.setDstrct_code(rs.getString("dstrct_code"));
                standar.setOrigen(rs.getString("origin_code"));
                standar.setCod_cli(rs.getString("codcli"));
                standar.setNomcli(rs.getString("nombre_cliente"));
                standar.setAg_duenia(rs.getString("ageduenia"));
                standar.setPagador(rs.getString("cod_pagador"));
                
                standar.setCodigo_sj(rs.getString("std_job_no"));
                standar.setDescripcion_sj(rs.getString("std_job_desc"));
                standar.setTipo(rs.getString("tipo"));
                
                standar.setStandar_general(rs.getString("stdjobgen"));
                standar.setBloq_despacho((rs.getString("bloquea_despacho").equals("S")?true:false));
                
                
                standar.setE_mail(rs.getString("email_aut_bloqueo"));
                standar.setAg_responsable(rs.getString("originator_id"));
                
                standar.setAprobado((rs.getString("aprobacion").equals("S")?true:false));
                standar.setPlaneable((rs.getString("planeable").equals("S")?true:false));
                standar.setNom_origen(rs.getString("origin_name"));
                standar.setDestino(rs.getString("destination_code"));
                
                standar.setNom_destino(rs.getString("destination_name"));
                
                standar.setInd_vacio((rs.getString("ind_vacio").equals("S")?true:false));
                standar.setReq_remesa_padre((rs.getString("req_remesa_padre").equals("S")?true:false));
                standar.setRemesaFacturable((rs.getString("remesa_facturable").equals("S")?true:false));
                
                
                standar.setTipo_ruta(rs.getString("wo_type"));
                standar.setCodigo_carga(rs.getString("cod_carga"));
                standar.setDesc_carga(rs.getString("desc_carga"));
                standar.setDesc_carga_corta(rs.getString("descripcion_corta"));
                standar.setTipo_facturacion(rs.getString("tipo_facturacion"));
                standar.setVlr_tope_carga(rs.getDouble("vlr_tope_carga"));
                
                standar.setPeriodoFac_inicial(rs.getInt("per_fac_inicial"));
                standar.setPeriodoFac_final(rs.getInt("per_fac_final"));
                standar.setAccount_code_i(rs.getString("account_code_i"));
                standar.setAccount_code_c(rs.getString("account_code_c"));
                standar.setFrontera_asoc(rs.getString("frontera_asoc"));
                
                sql = this.obtenerSQL("SQL_BUSCAR_TARIFA");
                ps2 = con.prepareStatement(sql);
                ps2.setString(1,standar.getDstrct_code() );
                ps2.setString(2,standar.getCodigo_sj()   );
                rs2 = ps2.executeQuery();
                List listaTarifa = new LinkedList();
                while(rs2.next()){
                    Standar tarifa = (Standar) new Standar();
                    tarifa.setVlr_tarifa(rs2.getDouble("vlr_freight"));
                    tarifa.setMoneda_tarifa(rs2.getString("currency"));
                    tarifa.setUnidad_tarifa(rs2.getString("unidad"));
                    tarifa.setVigente_tarifa((rs2.getString("vigente").equals("S")?true:false));
                    
                    listaTarifa.add(tarifa);
                }
                standar.setListaTarifa(listaTarifa);
                
                
                sql = this.obtenerSQL("SQL_BUSCAR_TRAMOS");
                ps3 = con.prepareStatement(sql);
                ps3.setString(1,standar.getDstrct_code() );
                ps3.setString(2,standar.getCodigo_sj()   );
                rs3 = ps3.executeQuery();
                List listaTramos = new LinkedList();
                while(rs3.next()){
                    Standar tramos = new Standar();
                    
                    tramos.setCod_origen_tramo    (rs3.getString("origin_code"));
                    tramos.setCodigo_destino_tramo(rs3.getString("destination_code"));
                    tramos.setNombre_origen_tramo (rs3.getString("nombre_origen"));
                    tramos.setNombre_destino_tramo(rs3.getString("nombre_destino"));
                    tramos.setPorcentaje_tramo    (rs3.getString("porcentaje_maximo_anticipo"));
                    tramos.setPeso_lleno_tramo    (rs3.getString("peso_lleno_maximo"));
                    String numero_escolta =       (rs3.getString("numescoltas")!=null)?rs3.getString("numescoltas"):"0";
                    numero_escolta = (numero_escolta.equals(""))?"0":numero_escolta;
                    tramos.setNumero_escoltas     (Integer.parseInt(numero_escolta));
                    tramos.setTipo_trafico        (rs3.getString("tipo_trafico"));
                    tramos.setInd_vacio_tramo     ((rs3.getString("ind_vacio").equals("S")?true:false));
                    tramos.setControl_cargue      ((rs3.getString("control_cargue").equals("S")?true:false));
                    tramos.setControl_descargue   ((rs3.getString("control_descargue").equals("S")?true:false));
                    tramos.setCaravana            ((rs3.getString("caravana").equals("S")?true:false));
                    tramos.setRequiere_planviaje  ((rs3.getString("req_plan_viaje").equals("S")?true:false));
                    tramos.setPreferencia         ((rs3.getString("preferencia").equals("S")?true:false));
                    tramos.setRequiere_hojareporte((rs3.getString("req_hoja_reporte").equals("S")?true:false));
                    tramos.setContingente         ((rs3.getString("contingente").equals("S")?true:false));
                    
                    sql = this.obtenerSQL("SQL_BUSCAR_RECURSOS");
                    ps4 = con.prepareStatement(sql);
                    ps4.setString(1,standar.getDstrct_code() );
                    ps4.setString(2,standar.getCodigo_sj()   );
                    ps4.setString(3,tramos.getCod_origen_tramo());
                    ps4.setString(4,tramos.getCodigo_destino_tramo());
                    
                    rs4 = ps4.executeQuery();
                    List listaRecursos = new LinkedList();
                    while(rs4.next()){
                        Standar recursos = new Standar();
                        
                        recursos.setCodigo_recurso(rs4.getString("cod_recurso"));
                        recursos.setTipo_recurso(rs4.getString("tipo_recurso"));
                        Recursosdisp rec = this.buscarRecurso(recursos.getCodigo_recurso());
                        recursos.setDescripcion_recurso(rec.getDestino()); 
                        recursos.setPrioridad(rs4.getInt("prioridad"));
                        
                        sql = this.obtenerSQL("SQL_BUSCAR_FLETES");
                        ps5 = con.prepareStatement(sql);
                        ps5.setString(1,standar.getDstrct_code() );
                        ps5.setString(2,standar.getCodigo_sj()   );
                        ps5.setString(3,tramos.getCod_origen_tramo());
                        ps5.setString(4,tramos.getCodigo_destino_tramo());
                        ps5.setString(5,recursos.getCodigo_recurso());
                        rs5 = ps5.executeQuery();
                        
                        List listaFlete = new LinkedList();
                        while(rs5.next()){
                            Standar fletes = new Standar();
                            
                            fletes.setVlr_flete(rs5.getDouble("cost"));
                            fletes.setMoneda_flete(rs5.getString("currency"));
                            fletes.setUnidad_flete(rs5.getString("unidad"));
                            fletes.setVigente_flete((rs5.getString("vigente").equals("S")?true:false));
                            listaFlete.add(fletes);
                        }
                        
                        recursos.setListaFletes(listaFlete);
                        listaRecursos.add(recursos);
                    
                    }
                
                    tramos.setListaRecursos(listaRecursos);
                    listaTramos.add(tramos);
                }
                
                standar.setListaTramos(listaTramos);
            }
                
                //standar.setDesc_carga(rs.getString("ageduenia"));
                //standar.setDesc_carga_corta(rs.getString("ageduenia"));
                
                
           
        }
        catch( Exception ex ){
            ex.printStackTrace();
            throw new SQLException("ERROR OBTENIENDO REGISTROS SQL_BUSCAR_SJ: "+ex.getMessage());
        }
        finally{
            if ( rs != null ) {rs.close();}
            if ( ps != null ) {ps.close();}
            if ( rs2 != null ) {rs2.close();}
            if ( ps2 != null ) {ps2.close();}
            if ( rs3 != null ) {rs3.close();}
            if ( ps3 != null ) {ps3.close();}
            if ( rs4 != null ) {rs4.close();}
            if ( ps4 != null ) {ps4.close();}
            if ( rs5 != null ) {rs5.close();}
            if ( ps5 != null ) {ps5.close();}
            this.desconectar("SQL_BUSCAR_SJ");
        }
        return standar;
    }
    
    
     /**
     * M�todo updateEstandar  metodo para guardar el estandar
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......objeto Standar
     * @version.....1.0.
     **/
    public String updateEstandar(Standar sj)throws SQLException {
        Connection con = null;
        Statement st = null;
        PreparedStatement ps = null;
        String sql="";
        String SQL_INSERT_SJ      = this.obtenerSQL("SQL_INSERT_SJ");
        String SQL_INSERT_TARIFA  = this.obtenerSQL("SQL_INSERT_TARIFA");
        String SQL_INSERT_TRAMO   = this.obtenerSQL("SQL_INSERT_TRAMO");
        String SQL_INSERT_RECURSO = this.obtenerSQL("SQL_INSERT_RECURSO");
        String SQL_INSERT_FLETE   = this.obtenerSQL("SQL_INSERT_FLETE");
        String SQL_AUMENTAR_SECUENCIA = this.obtenerSQL("SQL_AUMENTAR_SECUENCIA");
        String SQL_DELETE_SJ      = this.obtenerSQL("SQL_DELETE_SJ").replaceAll("#DISTRITO#",sj.getDstrct_code()).replaceAll("#CODSJ#",sj.getCodigo_sj()); 
        try {
            con = this.conectar("SQL_DELETE_SJ");
            ps = con.prepareStatement(SQL_DELETE_SJ);
            sql+= ps.toString(); 
            
            con = this.conectar("SQL_INSERT_SJ");
            ps = con.prepareStatement(SQL_INSERT_SJ);
            ps.setString(1,  sj.getDstrct_code());
            ps.setString(2,  sj.getCodigo_sj());     //std_job_no
            ps.setString(3,  sj.getDescripcion_sj());//std_job_desc
            ps.setString(4,  sj.getCod_cli());
            ps.setString(5,  sj.getTipo_ruta());     //wo_type
            ps.setString(6,  sj.getAg_responsable());//originator_id
            ps.setString(7,  sj.getOrigen());        //origin_code
            ps.setString(8,  sj.getDestino());       //destination_code
            ps.setString(9,  sj.getNom_origen());
            ps.setString(10, sj.getNom_destino());
            ps.setString(11, sj.getStandar_general());//stdjobgen
            ps.setString(12, (sj.isBloq_despacho())?"S":"N");
            ps.setString(13, sj.getFrontera_asoc());
            ps.setString(14,(sj.isPlaneable())?"S":"N");
            ps.setString(15, sj.getTipo());
            ps.setString(16, sj.getPagador());
            ps.setString(17, sj.getAccount_code_c());
            ps.setString(18, sj.getAccount_code_i());
            ps.setString(19, sj.getCodigo_carga());
            ps.setString(20, sj.getAg_duenia() );
            ps.setInt   (21, sj.getListaTarifa().size());
            ps.setString(22, (sj.isRemesaFacturable())?"S":"N");
            ps.setString(23, sj.getE_mail());
            ps.setString(24, (sj.isReq_remesa_padre())?"S":"N");
            ps.setDouble(25, sj.getVlr_tope_carga());
            ps.setString(26, sj.getTipo_facturacion());
            ps.setInt   (27, sj.getPeriodoFac_inicial());
            ps.setInt   (28, sj.getPeriodoFac_final());
            ps.setString(29, (sj.isInd_vacio())?"S":"N");
            ps.setString(30, sj.getCreation_user());
            ps.setString(31, sj.getBase());
            
            sql+= ps.toString()+";";
            
            
                       
            //bloque para insertar los datos de las tarifas
            for(int i =0; i< sj.getListaTarifa().size();i++ ){
                Standar tarifa = (Standar) sj.getListaTarifa().get(i);;
                ps = con.prepareStatement(SQL_INSERT_TARIFA);
                ps.setString(1, sj.getDstrct_code());
                ps.setString(2, sj.getCodigo_sj());
                ps.setInt   (3, i+1);//tarifa.getSeq_tarifa());
                ps.setDouble(4, tarifa.getVlr_tarifa());
                ps.setString(5, tarifa.getMoneda_tarifa());
                ps.setString(6, tarifa.getUnidad_tarifa());
                ps.setString(7, (tarifa.isVigente_tarifa())?"S":"N");
                ps.setString(8, sj.getCreation_user());
                ps.setString(9, sj.getBase());
                
                sql+= ps.toString()+";";
            }
            //fin bloque de tarifas
            
            //inicio del bloque para llenar los tramos
            for(int t =0; t< sj.getListaTramos().size();t++ ){
                Standar tramo = (Standar) sj.getListaTramos().get(t);
                ps = con.prepareStatement(SQL_INSERT_TRAMO);
                ps.setString(1,  sj.getDstrct_code());
                ps.setString(2,  sj.getCodigo_sj());
                ps.setString(3,  sj.getCod_cli());
                ps.setString(4,  tramo.getCod_origen_tramo());
                ps.setString(5,  tramo.getCodigo_destino_tramo());
                ps.setString(6,  tramo.getPorcentaje_tramo());
                ps.setString(7,  (tramo.isInd_vacio_tramo())?"S":"N");
                ps.setInt   (8,  tramo.getNumero_escoltas());
                ps.setString(9, tramo.getTipo_trafico());
                ps.setString(10, (tramo.isControl_cargue())?"S":"N");
                ps.setString(11, (tramo.isControl_descargue())?"S":"N");
                ps.setString(12, (tramo.isPreferencia())?"S":"N");
                ps.setString(13, (tramo.isContingente())?"S":"N");
                ps.setString(14, (tramo.isCaravana())?"S":"N");
                ps.setString(15, (tramo.isRequiere_planviaje())?"S":"N");
                ps.setString(16, (tramo.isRequiere_hojareporte())?"S":"N");
                ps.setString(17, tramo.getPeso_lleno_tramo());
                ps.setString(18, sj.getCreation_user());
                ps.setString(19, sj.getBase());
                
                sql+= ps.toString()+";";
                int size_recurso = ((Standar) sj.getListaTramos().get(t)).getListaRecursos().size(); //Saco el size de la lista de recursos
                for(int r =0; r< size_recurso;r++ ){
                    Standar recurso = (Standar) tramo.getListaRecursos().get(r);
                    int size_flete = ((Standar)((Standar) sj.getListaTramos().get(t)).getListaRecursos().get(r)).getListaFletes().size(); //Saco el size de la lista de recursos
                    ps = con.prepareStatement(SQL_INSERT_RECURSO);
                    ps.setString(1, sj.getDstrct_code());
                    ps.setString(2, sj.getCodigo_sj());
                    ps.setString(3, tramo.getCod_origen_tramo());
                    ps.setString(4, tramo.getCodigo_destino_tramo());
                    ps.setString(5, recurso.getTipo_recurso());
                    ps.setString(6, recurso.getCodigo_recurso());
                    ps.setInt   (7, recurso.getPrioridad() );  //me falta definir la prioridad
                    ps.setString(8, sj.getCreation_user());
                    ps.setString(9, sj.getBase());
                    ps.setInt   (10, size_flete );
                    
                    sql+= ps.toString()+";";
                    
                    
                    for(int f =0; f< size_flete;f++ ){
                        Standar flete = (Standar) recurso.getListaFletes().get(f);
                        ps = con.prepareStatement(SQL_INSERT_FLETE);
                        ps.setString(1, sj.getDstrct_code());
                        ps.setString(2, sj.getCodigo_sj());
                        ps.setString(3, tramo.getCod_origen_tramo());
                        ps.setString(4, tramo.getCodigo_destino_tramo());
                        ps.setInt   (5, f+1);
                        ps.setDouble(6, flete.getVlr_flete());
                        ps.setString(7, flete.getMoneda_flete());
                        ps.setString(8, flete.getUnidad_flete());
                        ps.setString(9, (flete.isVigente_flete())?"S":"N");
                        ps.setString(10, sj.getCreation_user());
                        ps.setString(11, sj.getBase());
                        ps.setString(12, recurso.getCodigo_recurso());
                        sql+= ps.toString()+";";
                    }
                    
                }
                
            }
            //fin del bloque de tramos
            
            
            
           
            
        }
        catch( Exception ex ){
            ex.printStackTrace();
           
            throw new SQLException("ERROR OBTENIENDO REGISTROS SQL_BUSCAR_CIUDAD: "+ex.getMessage());
        }
        finally{
            if ( st != null )  {st.close();}
            if ( ps != null )  {ps.close();}
            this.desconectar("SQL_DELETE_SJ");
        }
        
        return sql;
        
    }
    
    
    
}
