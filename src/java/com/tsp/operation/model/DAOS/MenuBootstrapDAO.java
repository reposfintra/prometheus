/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.MenuOpcionesModulos;
import com.tsp.operation.model.beans.Usuario;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author user
 */
public interface MenuBootstrapDAO {
    
     /***
     * Este metodo carga el menu dinamicamente segun el modulo que se solicite.
     * @param usuario 
     * @param modulo
     * @return 
     * @throws java.sql.SQLException 
     */
     public ArrayList<MenuOpcionesModulos> cargarMenuOpciones(Usuario usuario,String modulo) throws SQLException;
}
