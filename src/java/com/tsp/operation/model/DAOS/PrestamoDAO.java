/***************************************
* Nombre Clase ............. PrestamoDAO.java
* Descripción  .. . . . . .  Ofrece los sql para los prestamos
* Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
* Fecha . . . . . . . . . .  09/02/2006
* versión . . . . . . . . .  1.0
* Copyright ...Transportes Sanchez Polo S.A.
*******************************************/




package com.tsp.operation.model.DAOS;


import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;




public class PrestamoDAO extends MainDAO{
    
    
    
    
    
    /* _______________________________________________________________________________________
    *                                 ATRIBUTOS
    *________________________________________________________________________________________ */
   
   
    
    
    public static String TABLA_TERCEROS                = "TERCEROS";
    public static String TABLA_TIPO_PRESTAMOS          = "TPRESTAMO";
    public static String TABLA_PERIODOS_PAGO           = "PPAGO";
    public static String TABLA_CONCEPTO_PRESTAMO       = "CPRESTAMO";
    public static String TABLA_CLASIFICACION_PRESTAMO  = "CLPRESTAMO";
    public static String TABLA_EQUIPOS_PRESTAMO        = "EQPRESTAMO"; 
    private Vector datosPrestamo;  //Tmolina 2008-09-13
    private BeanGeneral bg;        //Tmolina 2008-09-13
    ArrayList nitsx;               //Tmolina 2008-09-13
    boolean swNit;                 //Tmolina 2008-09-13

    
    /* _______________________________________________________________________________________
    *                                 METODOS
    *________________________________________________________________________________________ */
   
    
    
    
    public PrestamoDAO() {
        super("PrestamoDAO.xml");
    }
    
    
    
    
    /**
     * Metodo obtiene datos de tabla general
     * @autor fvillacob
     * @throws Exception.
     */
    public List getTablas(String tipo)throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null; 
        ResultSet         rs          = null;
        List              lista       = new LinkedList();
        String query = "SQL_TABLA_GEN";
        try{
            con = this.conectarJNDI( "SQL_TABLA_GEN" );//JJCastro fase2
            if(con !=null){
            st = con.prepareStatement( this.obtenerSQL("SQL_TABLA_GEN" ));
            st.setString(1, tipo);
            rs = st.executeQuery();
            while(rs.next()){
                Hashtable tabla =  new Hashtable();
                    tabla.put("codigo",      rs.getString("table_code")  );
                    tabla.put("descripcion", rs.getString("descripcion") );
                    tabla.put("referencia",  rs.getString("referencia")  );                    
                    tabla.put("dato",        rs.getString("dato")        );
                    //tabla.put("datko",        null        );
                lista.add(tabla);
            }
            
        }}catch(Exception e) {
            System.out.println("oops en dao "+ e.toString()+"__"+e.getMessage());
             throw new SQLException(" DAO: getTablas  -> "+ e.toString()+"__"+e.getMessage());
        } 
        finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return lista;
    }
    
    
    
    
    
    
    
    /**
     * Metodo que devuelve la lista de proveedores
     * @autor fvillacob
     * @param agencia, agencia del usuario
     * @throws Exception.
     */
    public List getProveedores( String agencia )throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null; 
        ResultSet         rs          = null;
        List              lista       = new LinkedList(); 
        String            query       = "SQL_PROVEEDORES";
        try{
            con = this.conectarJNDI(query); //JJCastro fase2
            if(con!=null){
            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, agencia );
            rs = st.executeQuery();
            while(rs.next()){
                Hashtable tabla =  new Hashtable();
                    tabla.put("nit",       rs.getString("nit")              );
                    tabla.put("nombre",    rs.getString("payment_name")     );
                    tabla.put("banco",     rs.getString("branch_code")      );                    
                    tabla.put("sucursal",  rs.getString("bank_account_no")  );                    
                lista.add(tabla);
             tabla = null;//Liberar Espacio JJCastro
            }
            
        }}catch(Exception e) {
             System.out.println("error en prestamoDAO "+e.toString()+"__"+e.getMessage());
             throw new SQLException(" DAO: getProveedores  -> " +e.getMessage());
        } 
        finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
           if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
           }  
        return lista;
    }
    
    
    
    
    
    
    /**
     * Metodo para guardar los prestamos
     * @autor fvillacob
     * @param pr, Objeto prestamo que alamacena en una lista las amortizaciones 
     * @param user, usuario que graba las amortizaciones
     * @throws Exception.
     */
     public void save(Prestamo pr, String user)throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null; 
        String            query       = "SQL_SAVE_PRESTAMO";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
              st.setString(1,  pr.getDistrito()           );
              st.setString(2,  pr.getBeneficiario()       );
              st.setString(3,  pr.getTercero()            );
              st.setDouble(4,  pr.getMonto()              );
              st.setDouble(5,  pr.getIntereses()          );
              st.setInt   (6,  pr.getCuotas()             );              
              st.setInt   (7,  pr.getFrecuencias()        );              
              st.setDouble(8,  pr.getTasa()               );              
              st.setString(9,  pr.getTipoPrestamo()       );              
              st.setDouble(10, pr.getInteresDemora()     );              
              st.setString(11, pr.getFechaEntregaDinero() );
              st.setString(12, pr.getFechaInicialCobro()  );              
              st.setString(13, pr.getAprobado()           );
              st.setString(14, pr.getConcepto()           );
              st.setString(15, pr.getObservacion()        );              
              st.setString(16, user                       );
              st.setString(17, pr.getClasificación()      );
              st.setString(18, pr.getEquipo()             );
              st.setDouble(19, pr.getCuotaInicial()       );
              st.setDouble(20, pr.getCuotaFinanciacion()  );
              st.setDouble(21, pr.getCuotaMonitoreo()     );
              st.setString(22, pr.getPlaca()              );
              st.setInt   (23, pr.getInicio_financiacion());
              st.setInt   (24, pr.getInicio_monitoreo()   );
              st.executeUpdate();
         // Obtenemos el serial:
            pr.setId( this.getSerial() );
            
            
            if( pr.getAprobado().toUpperCase().equals("S"))
               this.grabarAmortizaciones(pr, user );            
            
            }}catch(Exception e) {
             throw new SQLException(" Save(Prestamo)  -> " +e.getMessage());
        } 
        finally {
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
           }  
    }
     
     
     
     
     /**
     * Metodo para obtener el valor actual de la serie de la tabla
     * @autor  fvillacob
     * @throws Exception.
     */

     public int getSerial() throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null; 
        ResultSet         rs          = null;
        int               serial      = 0;
        String query = "SQL_VAL_SECUENCIA_PRESTAMO";
        try{            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            if (rs.next()){
                serial = rs.getInt(1);
            }            
            }}catch(Exception e) {
             throw new SQLException(" DAO: getSerial() -> " +e.getMessage());
        } 
        finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            } 
        return serial;
     }
     
     
     
     
     
     

     
    /**
     * Metodo para guardar las amortizaciones de un prestamo en la base de datos
     * @autor mfontalvo
     * @param pt, Objeto prestamo que alamacena en una lista las amortizaciones 
     * @param usuario, usuario que graba las amortizaciones
     * @throws Exception.
     */
    public void grabarAmortizaciones ( Prestamo pt , String usuario) throws Exception{

        if (pt==null) return;
        if (pt.getAmortizacion()==null || pt.getAmortizacion().isEmpty()) return;
        
        Connection        con = null;
        PreparedStatement st  = null; 
        try{
            con = this.conectarJNDI("SQL_INSERT_AMORTIZACIONES");
            if(con!=null){
            con.setAutoCommit(false);   
            
            st = con.prepareStatement(this.obtenerSQL("SQL_INSERT_AMORTIZACIONES"));
            for (int i = 0; i<pt.getAmortizacion().size(); i++){
                Amortizacion am = (Amortizacion) pt.getAmortizacion().get(i);                
                st.clearParameters();
                st.setString (1 ,  pt.getDistrito());
                st.setInt    (2 ,  pt.getId());
                st.setInt    (3 ,  i + 1 ) ;
                st.setString (4 ,  pt.getBeneficiario());
                st.setString (5 ,  pt.getTercero());
                st.setDate   (6 ,  new java.sql.Date(am.getFecha().getTime()));
                st.setDouble (7 ,  am.getTotalAPagar());
                st.setDouble (8 ,  am.getCapital    ());
                st.setDouble (9 ,  am.getInteres    ());
                st.setDouble (10,  am.getMonto      ());
                st.setDouble (11,  am.getSaldo      ());
                st.setString (12,  usuario);
                st.setString (13,  Util.getFechaActual_String(6));
                st.setString (14,  usuario);
                st.setString (15,  Util.getFechaActual_String(6));
                st.addBatch();
            }
            st.executeBatch();            
            con.commit();            
            con.setAutoCommit(true);
        }}catch(Exception e) {
            con.rollback();
            throw new SQLException(" PrestamoDAO: grabarAmortizaciones  -> \n" +e.getMessage());
        } 
        finally {
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }  
    }
    

    
    /**
     * Metodo para obtener el listado de prestamos
     * @autor mfontalvo
     * @param where resticciones para el listado de prestamos
     * @return Lista de prestamos.
     * @throws Exception.
     */
    public List obtenerPrestamos ( String where ) throws Exception{
        Connection        con   = null;
        PreparedStatement st    = null; 
        ResultSet         rs    = null;
        List              lista = new LinkedList();
        try{
            
            con = this.conectarJNDI("SQL_LISTA_PRESTAMOS");
            if(con!=null){
            
            st = con.prepareStatement(this.obtenerSQL("SQL_LISTA_PRESTAMOS") + where + " ORDER BY id ");
            rs = st.executeQuery();
            while (rs.next()){
                Prestamo pt = new Prestamo();
                pt.setId                 ( rs.getInt   ("id"));
                pt.setDistrito           ( rs.getString("dstrct"));
                pt.setBeneficiario       ( rs.getString("beneficiario"));
                pt.setBeneficiarioName   ( rs.getString("nbeneficiario"));
                pt.setTercero            ( rs.getString("tercero"));
                pt.setTerceroName        ( rs.getString("ntercero"));
                pt.setMonto              ( rs.getDouble("monto"));
                pt.setIntereses          ( rs.getDouble("interes"));
                pt.setCuotas             ( rs.getInt   ("cuotas"));
                pt.setTasa               ( rs.getDouble("tasa"));
                pt.setTipoPrestamo       ( rs.getString("tipoprestamo"));
                pt.setTipoPrestamoName   ( rs.getString("ntipoprestamo"));
                pt.setFechaEntregaDinero ( rs.getString("entregadinero") );
                pt.setFechaInicialCobro  ( rs.getString("primercobro") );
                pt.setAprobado           ( rs.getString("aprobado"));
                pt.setFrecuencias        ( rs.getInt   ("periodos"));
                pt.setClasificación      ( rs.getString("clasificacion"));
                pt.setClasificacionName  ( rs.getString("nclasificacion"));
                pt.setValorPagado        ( rs.getDouble("vPagado") );
                pt.setValorDescontado    ( rs.getDouble("vDescontado") );
                pt.setValorMigrado       ( rs.getDouble("vMigrado") );
                pt.setValorRegistrado    ( rs.getDouble("vRegistrado") );
                pt.setCapitalDescontado  ( rs.getDouble("vCapitalDescontado") );
                pt.setInteresDescontado  ( rs.getDouble("vInteresDescontado") );
                
                pt.setPlaca                      ( rs.getString("placa"));
                pt.setEquipo                     ( rs.getString("equipo"));
                pt.setDescripcion_equipo         ( rs.getString("equipo_desc"));
                pt.setFecha_migracion            ( rs.getString("fecha_migracion"));
                pt.setFactura_inicial            ( rs.getString("factura_cuota_inicial"));
                pt.setFactura_financiacion       ( rs.getString("factura_financiacion"));
                pt.setFactura_monitoreo_inicial  ( rs.getString("factura_monitoreo_inicial"));
                pt.setFactura_monitoreo          ( rs.getString("factura_monitoreo"));
                pt.setCuotaInicial               ( rs.getDouble("cuota_inicial") );
                pt.setCuotaFinanciacion          ( rs.getDouble("cuota_financiacion") );
                pt.setCuotaMonitoreo             ( rs.getDouble("cuota_monitoreo") );                
                pt.setInicio_financiacion        ( rs.getInt   ("inicio_financiacion") );
                pt.setInicio_monitoreo           ( rs.getInt   ("inicio_monitoreo") );                
                pt.setNit_tercero                ( rs.getString("nit_tercero"));                               
                pt.setBanco_tercero              ( rs.getString("banco_ter"));     
                pt.setSucursal_tercero           ( rs.getString("sucursal_ter"));     
                pt.setBanco_beneficiario         ( rs.getString("banco_ben"));     
                pt.setSucursal_beneficiario      ( rs.getString("sucursal_ben"));     
                pt.setCreation_date              ( rs.getString("creation_date"));    
                pt.setCreation_user              ( rs.getString("creation_user"));    
                
                
                pt.setSaldo_prestamo( (pt.getTipoPrestamo().equals("AP") && pt.getCuotas()!=0)?(pt.getCapitalDescontado()+pt.getInteresDescontado()-pt.getValorPagado()):(pt.getMonto() - pt.getCapitalDescontado())  );                 
                lista.add(pt);
            }
            
        }}catch(Exception e) {
            throw new SQLException(" PrestamoDAO: obtenerPrestamos  -> \n" +e.getMessage());
        } 
        finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
             }  
        return lista;
    }
        
    
    
    /**
     * Metodo que actualiza el estado de aprobado de un prestamo
     * @autor mfontalvo
     * @param usuario, usuario que aprueba el prestamo
     * @param distrito, distrito del prestamo
     * @param id, id  del prestamo
     * @throws Exception.
     */
    public void aprobarPrestamo (String usuario, String distrito, int id)throws Exception{
        Connection        con = null;
        PreparedStatement st  = null; 
        try{
            con = this.conectarJNDI( "SQL_UPDATE_PRESTAMO" );
            if(con!=null){
            st = con.prepareStatement( this.obtenerSQL("SQL_UPDATE_PRESTAMO") );
            st.setString(1,  "S"     );
            st.setString(2,  Util.getFechaActual_String(6) );
            st.setString(3,  usuario );
            st.setString(4,  distrito);
            st.setInt   (5,  id      );
            st.executeUpdate();
            
        }}catch(Exception e) {
             throw new SQLException(" aprobarPrestamo (Prestamo)  -> " +e.getMessage());
        } 
        finally {
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }  
    }     
    
  
    
    
    /**
     * Metodo que actualiza el interes de un prestamo
     * @autor mfontalvo
     * @param interes, nuevo valor del interes
     * @param usuario, usuario que aprueba el prestamo
     * @param distrito, distrito del prestamo
     * @param id, id  del prestamo
     * @throws Exception.
     */
    public void actualizarInteresPrestamo (double interes, String usuario, String distrito, int id)throws Exception{
        Connection        con      = null;
        PreparedStatement st       = null; 
        try{
            con = this.conectarJNDI( "SQL_UPDATE_PRESTAMO_INTERES" );//JJCastro fase2
            if(con!=null){
            st = con.prepareStatement( this.obtenerSQL("SQL_UPDATE_PRESTAMO_INTERES") );
            st.setDouble(1,  interes );
            st.setString(2,  Util.getFechaActual_String(6) );
            st.setString(3,  usuario );
            st.setString(4,  distrito);
            st.setInt   (5,  id      );
            st.executeUpdate();
            
        }}catch(Exception e) {
             throw new SQLException(" actualizarInteresPrestamo (Prestamo)  -> " +e.getMessage());
        } 
        finally {
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }  
    }   
    
    /**
     * Metodo que actualiza el interes de un prestamo
     * @autor mfontalvo
     * @param cuotas, nuevo cuotas del prestamo
     * @param usuario, usuario que aprueba el prestamo
     * @param distrito, distrito del prestamo
     * @param id, id  del prestamo
     * @throws Exception.
     */
    public void actualizarCuotasPrestamo (int cuotas, String usuario, String distrito, int id)throws Exception{
        Connection        con      = null;
        PreparedStatement st       = null; 
        try{
            con = this.conectarJNDI( "SQL_UPDATE_PRESTAMO_CUOTAS" );//JJCastro fase2
            if(con!=null){
            st = con.prepareStatement( this.obtenerSQL("SQL_UPDATE_PRESTAMO_CUOTAS") );
            st.setInt   (1,  cuotas  );
            st.setString(2,  Util.getFechaActual_String(6) );
            st.setString(3,  usuario );
            st.setString(4,  distrito);
            st.setInt   (5,  id      );
            st.executeUpdate();
            
            }}catch(Exception e) {
             throw new SQLException(" actualizarCuotasPrestamo (Prestamo)  -> " +e.getMessage());
        } 
        finally {
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }  
    }    
    

    /**
     * Metodo que elimina prestamos
     * @autor mfontalvo
     * @param distrito, distrito del prestamo
     * @param id, id  del prestamo
     * @throws Exception.
     */
    public void eliminarPrestamo (String distrito, int id)throws Exception{
        Connection con = null;
        PreparedStatement st          = null; 
        try{
            con = this.conectarJNDI( "SQL_DELETE_PRESTAMO" );//JJCastro fase2
            if(con!=null){
            st = con.prepareStatement( this.obtenerSQL("SQL_DELETE_PRESTAMO") );
            st.setString(1,  distrito);
            st.setInt   (2,  id      );
            st.executeUpdate();
            
        }}catch(Exception e) {
             throw new SQLException(" eliminarPrestamo (Prestamo)  -> " +e.getMessage());
        } 
        finally {
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
    }     
         
    /**
     * Metodo para obtener el listado de amortizaciones
     * @autor mfontalvo
     * @param where resticciones para el listado de amortizaiones
     * @throws Exception.
     */ 
    public List obtenerAmortizaciones (String where) throws Exception{
        Connection        con   = null;
        PreparedStatement st    = null; 
        ResultSet         rs    = null;
        List              lista = new LinkedList();
        try{
            
            con = this.conectarJNDI("SQL_LISTA_AMORTIZACIONES");//JJCastro fase2
            if(con!=null){

            st = con.prepareStatement(this.obtenerSQL("SQL_LISTA_AMORTIZACIONES") + where + " ORDER BY a.dstrct, a.prestamo, a.item ");
            rs = st.executeQuery();
            while (rs.next()){
                
                Amortizacion am = new Amortizacion();
                
                am.setReg_status      ( rs.getString("reg_status"));
                am.setDstrct          ( rs.getString("dstrct")         );
                am.setBeneficiario    ( rs.getString("beneficiario")   );
                am.setNameBeneficiario( rs.getString("nbeneficiario")  );
                am.setTercero         ( rs.getString("tercero")        );
                am.setNameTercero     ( rs.getString("ntercero")       );
                am.setPrestamo        ( rs.getString("prestamo")       );
                am.setItem            ( rs.getString("item")           );
                am.setMonto           ( rs.getDouble("valor_monto")    );
                am.setTotalAPagar     ( rs.getDouble("valor_a_pagar")  );
                am.setCapital         ( rs.getDouble("valor_capital")  );
                am.setInteres         ( rs.getDouble("valor_interes")  );
                am.setSaldo           ( rs.getDouble("valor_saldo")    );
                am.setFecha           ( new java.util.Date (rs.getString("fecha_pago").replaceAll("-","/")) );
                am.setFechaPago       ( rs.getString("fecha_pago")     );
                
                am.setFechaMigracion    ( rs.getString("fecha_transferencia"));
                am.setUserMigracion     ( rs.getString("usuario_tranferencia"));
                
                am.setBancoDescuento    ( rs.getString("banco_descuento"));
                am.setSucursalDescuento ( rs.getString("sucursal_descuento"));
                am.setChequeDescuento   ( rs.getString("cheque_descuento"));
                am.setCorridaDescuento  ( rs.getString("corrida_descuento"));
                am.setValorDescuento    ( rs.getDouble("valor_descuento"));
                am.setFechaDescuento    ( rs.getString("fecha_descuento"));
                am.setEstadoDescuento   ( rs.getString("estado_descuento"));
                
                am.setBancoPagoTercero    ( rs.getString("banco_pago_ter"));
                am.setSucursalPagoTercero ( rs.getString("sucursal_pago_ter"));
                am.setChequePagoTercero   ( rs.getString("cheque_pago_ter"));
                am.setCorridaPagoTercero  ( rs.getString("corrida_pago_ter"));
                am.setValorPagoTercero    ( rs.getDouble("valor_pago_ter"));
                am.setFechaPagoTercero    ( rs.getString("fecha_pago_ter"));
                am.setEstadoPagoTercero   ( rs.getString("estado_pago_ter"));
                lista.add(am);
                
            }
            
        }}catch(Exception e) {
            throw new SQLException(" PrestamoDAO: obtenerAmortizaciones  -> \n" +e.getMessage());
        } 
        finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return lista;        
    }
    
    
    
    /**
     * Busca los datos de la amortizacion en la base de datos mims para 
     * saber si la factura migrada ya esta cancelada
     * @autor mfontalvo
     * @param lista, lista de amortizaciones a verificar
     * @return Listado de amortizaciones seteadas mon los datos de mims.
     * @throws Exception.
     */    
    public List verificacionMims (List lista) throws Exception{ 
        Connection        con      = null;
        PreparedStatement st       = null; 
        ResultSet         rs       = null;
        try{
            con = this.conectarJNDI( "SQL_VERIFICACION_MIMS" );//JJCastro fase2
            if(con!=null){
            
            st = con.prepareStatement( this.obtenerSQL("SQL_VERIFICACION_MIMS") );
            
            for (int i=0;i<lista.size(); i++){
                Amortizacion am = (Amortizacion) lista.get(i);
                
                // verificacion descuento del beneficiario
                if (!am.getEstadoDescuento().equals("50")){
                    
                    String id_mims = obtenerID_MIMS(am.getBeneficiario(),"P");
                    st.clearParameters();
                    st.setString(1, am.getDstrct() );
                    st.setString(2, id_mims        );
                    st.setString(3, "PR" + am.getPrestamo() + "C" + am.getItem() );
                    rs = st.executeQuery();
                    if (rs.next()){
                        am.setBancoDescuento    ( rs.getString("banco"     ) );
                        am.setSucursalDescuento ( rs.getString("sucursal"  ) );
                        am.setChequeDescuento   ( rs.getString("cheque"    ) );
                        am.setCorridaDescuento  ( rs.getString("corrida"   ) );
                        am.setValorDescuento    ( rs.getDouble("valor_fact") );
                        am.setFechaDescuento    ( Util.fechaPostgres(rs.getString("fecpago"   ) ));
                        am.setEstadoDescuento   ( rs.getString("estado"    ) );
                        am.setModificarDescuento( true );
                        lista.set(i, am);
                    }                    
                }
                
                // verificacion pago al tercero
                if (!am.getEstadoPagoTercero().equals("50")){
                    
                    String id_mims = obtenerID_MIMS(am.getTercero(),"T");
                    st.clearParameters();
                    st.setString(1, am.getDstrct() );
                    st.setString(2, id_mims        );
                    st.setString(3, "PR" + am.getPrestamo() + "C" + am.getItem() );
                    rs = st.executeQuery();

                    if (rs.next()){
                        am.setBancoPagoTercero    ( rs.getString("banco"     ) );
                        am.setSucursalPagoTercero ( rs.getString("sucursal"  ) );
                        am.setChequePagoTercero   ( rs.getString("cheque"    ) );
                        am.setCorridaPagoTercero  ( rs.getString("corrida"   ) );
                        am.setValorPagoTercero    ( rs.getDouble("valor_fact") );
                        am.setFechaPagoTercero    ( Util.fechaPostgres(rs.getString("fecpago"   ) ));
                        am.setEstadoPagoTercero   ( rs.getString("estado"    ) );
                        am.setModificarPagoTercero(true);
                        lista.set(i, am);
                    }                    
                }                
            }// end for
            
        }}catch(Exception e) {
            throw new SQLException(" PrestamoDAO: verificacionMims  -> \n" +e.getMessage());
        } 
        finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return lista;
    }
    
    

    /**
     * Actualiza los datos de amortizacion que estan en mism en sot  
     * @autor mfontalvo
     * @param lista, lista de amortizaciones a actualizar
     * @throws Exception.
     */    
    public void actualizarAmortizaciones (List lista, String usuario) throws Exception{        
        Connection con = null;
        PreparedStatement st= null, st2=null; 
        try{
            con = this.conectarJNDI( "SQL_ACTUALIZACION_DESCUENTOS" );//JJCastro fase2
            if(con!=null){
            
            st  = con.prepareStatement( this.obtenerSQL("SQL_ACTUALIZACION_DESCUENTOS") );
            st2 = con.prepareStatement( this.obtenerSQL("SQL_ACTUALIZACION_PAGO_TERCEROS") );

            for (int i=0;i<lista.size(); i++){
                Amortizacion am = (Amortizacion) lista.get(i);
                
                // actualizar datos del descuento
                if (am.isModificarDescuento()){
                    st.clearParameters();
                    st.setString(1 , am.getBancoDescuento());
                    st.setString(2 , am.getSucursalDescuento());
                    st.setString(3 , am.getChequeDescuento());
                    st.setString(4 , am.getCorridaDescuento());
                    st.setString(5 , am.getFechaDescuento());
                    st.setDouble(6 , am.getValorDescuento());
                    st.setString(7 , am.getEstadoDescuento());
                    st.setString(8 , usuario);
                    st.setString(9 , Util.getFechaActual_String(6));
                    st.setString(10, am.getDstrct() );
                    st.setString(11, am.getPrestamo() );
                    st.setString(12, am.getItem() );                    
                    st.executeUpdate();
                }
                
                // actualizar datos del pago al tercero
                if (am.isModificarPagoTercero()){
                    st2.clearParameters();
                    st2.setString(1 , am.getBancoPagoTercero());
                    st2.setString(2 , am.getSucursalPagoTercero());
                    st2.setString(3 , am.getChequePagoTercero());
                    st2.setString(4 , am.getCorridaPagoTercero());
                    st2.setString(5 , am.getFechaPagoTercero());
                    st2.setDouble(6 , am.getValorPagoTercero());
                    st2.setString(7 , am.getEstadoPagoTercero());
                    st2.setString(8 , usuario);
                    st2.setString(9 , Util.getFechaActual_String(6));
                    st2.setString(10, am.getDstrct() );
                    st2.setString(11, am.getPrestamo() );
                    st2.setString(12, am.getItem() );                    
                    st2.executeUpdate();
                }
                
            }
        }}catch(Exception e) {
            throw new SQLException(" PrestamoDAO: actualizarAmortizaciones  -> \n" +e.getMessage());
        } 
        finally {
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (st2 != null){ try{ st2.close();           } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO 2 " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
    }  
    
    
    


    /**
     * Verifica si un usuario es aprobador 
     * @autor mfontalvo
     * @param distrito, distrito del usuario
     * @param usuario, usuario a verificar
     * @return boolean, que indica si el usuario es o no aprobador
     * @throws Exception.
     */    
    public boolean isUsuarioAprobadorPrestamo (String distrito, String usuario) throws Exception{       
        Connection        con         = null;
        PreparedStatement st          = null; 
        ResultSet         rs          = null;
        boolean           isAprobador = false;
        try{
            con = this.conectarJNDI( "SQL_USUARIO_APROBADOR" );//JJCastro fase2
            if(con!=null){
            st = con.prepareStatement( this.obtenerSQL("SQL_USUARIO_APROBADOR") );
            st.setString(1, distrito);
            st.setString(2, usuario );
            rs = st.executeQuery();
            if (rs.next()){
                isAprobador = true;
            }
        }}catch(Exception e) {
            throw new SQLException(" PrestamoDAO: isUsuarioAprobadorPrestamo  -> \n" +e.getMessage());
        } 
        finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return isAprobador;
    }    
    
    
    /**
     * Reliquidacion de Amortizaciones Modificadas
     * @param pt, Objeto prestamo que almacena en una lista las amortizaciones 
     * @param usuario, usuario que graba las amortizaciones
     * @throws Exception.
     */
    public void reliquidacionAmortizaciones ( Prestamo pt , String usuario ) throws Exception{

        if (pt==null) return;
        if (pt.getAmortizacion()==null || pt.getAmortizacion().isEmpty()) return;
        
        
        Connection        con = null;
        StringStatement2 stInsert = null, stDelete = null; 
        PreparedStatement st = null; 
        try{
            con = this.conectarJNDI("SQL_INSERT_AMORTIZACIONES");//JJCastro fase2
            if(con!=null){
            con.setAutoCommit(false);   
            
            st  = con.prepareStatement(""); 
            
            StringBuffer pagos = new StringBuffer();
            
            for (int i = 0; i<pt.getAmortizacion().size(); i++){
                Amortizacion am = (Amortizacion) pt.getAmortizacion().get(i); 
                if (!am.isSeleccionada() )  {
                    if (!pagos.toString().equals("")) pagos.append(",");
                    pagos.append("'"+ am.getItem() +"'");
                }                
            }
            if (!pagos.toString().equals("")){
                // DELETE DE AMORTIZACION
                stDelete = new StringStatement2(this.obtenerSQL("SQL_DELETE_AMORTIZACIONES").replaceAll("#LISTADO#",pagos.toString()), true);
                stDelete.setString (1 ,  pt.getDistrito());
                stDelete.setInt    (2 ,  pt.getId());
                st.addBatch(stDelete.getSql());               
            }     
            else
                st.addBatch("delete from fin.amortizaciones where dstrct = '"+ pt.getDistrito() +"' and prestamo = '"+ pt.getId() +"'");
            
            for (int i = 0; i<pt.getAmortizacion().size(); i++){
                Amortizacion am = (Amortizacion) pt.getAmortizacion().get(i); 
                
                if ( am.isSeleccionada() ){
                    // INSERT NUEVA AMORTIZACION
                    stInsert = new StringStatement2(this.obtenerSQL("SQL_INSERT_AMORTIZACIONES"), true); 
                    stInsert.clearParameters();
                    stInsert.setString (1 ,  pt.getDistrito());
                    stInsert.setInt    (2 ,  pt.getId());
                    stInsert.setInt    (3 ,  i + 1 ) ;
                    stInsert.setString (4 ,  pt.getBeneficiario());
                    stInsert.setString (5 ,  pt.getTercero());
                    stInsert.setString (6 ,  com.tsp.util.UtilFinanzas.customFormatDate(am.getFecha() , "yyyy-MM-dd" ));
                    stInsert.setDouble (7 ,  am.getTotalAPagar());
                    stInsert.setDouble (8 ,  am.getCapital    ());
                    stInsert.setDouble (9 ,  am.getInteres    ());
                    stInsert.setDouble (10,  am.getMonto      ());
                    stInsert.setDouble (11,  am.getSaldo      ());
                    stInsert.setString (12,  usuario);
                    stInsert.setString (13,  Util.getFechaActual_String(6));
                    stInsert.setString (14,  usuario);
                    stInsert.setString (15,  Util.getFechaActual_String(6));
                    st.addBatch(stInsert.getSql());
                }
            }
            st.executeBatch();            
            con.commit();            
            con.setAutoCommit(true);
        }}catch(Exception e) {
            con.rollback();
            throw new SQLException(" PrestamoDAO: grabarAmortizaciones  -> \n" +e.getMessage());
        } 
        finally {
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
    }
    
    

    /**
     * COnsulta del ID MIMS del tercero o beneficiario
     * @autor mfontalvo
     * @param nit, Nit a consultar
     * @param tipo, (P, T) buscar para proveedor o tercero
     * @return String, id mims
     * @throws Exception.
     */    
    public String obtenerID_MIMS (String nit, String tipo) throws Exception{     
        Connection        con         = null;
        PreparedStatement st          = null; 
        ResultSet         rs          = null;
        String            id_mims     = "";
        String query = (tipo.equals("P")?"SQL_SEARCH_ID_MIMS_PROVEEDOR":
                        tipo.equals("T")?"SQL_SEARCH_ID_MIMS_TERCERO"  :"ERROR");
        
        try{
            if (query.equals("ERROR")){
                throw new Exception ("Consulta no valida para buscar el id mims.");
            }
            
            con = this.conectarJNDI( query );//JJCastro fase2
            if(con!=null){
            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, nit);
            rs = st.executeQuery();
            if (rs.next()){
                id_mims = rs.getString("id_mims");
            }
        }}catch(Exception e) {
            throw new SQLException(" PrestamoDAO: obtenerID_MIMS  -> \n" +e.getMessage());
        } 
        finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return id_mims;
    }    
    
    
    
    /**
     * Actualizacion datos de migracion de prestamos
     * @autor mfontalvo.
     * @throws Exception.
     */
    public String updateFechaMigracionPrestamo( String dstrct, int id, String factura_inicial, String factura_financiacion, String factura_monitoreo, String factura_inicial_monitoreo ) throws Exception{
        StringStatement2 st = null;
        String sql = "";
        
        try{
            st = new StringStatement2( this.obtenerSQL("SQL_UPDATE_MIGRACION_PRESTAMO") ); 
            st.setString( factura_inicial              );
            st.setString( factura_financiacion         );
            st.setString( factura_inicial_monitoreo    );
            st.setString( factura_monitoreo            );
            st.setString( dstrct                       );
            st.setInt   ( id                           );
            sql = st.getSql();
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ( ex.getMessage() );
        }
        return sql;
    }
    
    
    
    
    /**
     * Metodo para obtener el listado de prestamos
     * @autor mfontalvo
     * @param where resticciones para el listado de prestamos
     * @return Lista de prestamos.
     * @throws Exception.
     */
    public Vector obtenerPrestamosLiquidacion ( String beneficiario ) throws Exception{
        Connection        con   = null;
        PreparedStatement st    = null; 
        ResultSet         rs    = null;
        Vector            lista = new Vector();
        try{
            
            con = this.conectarJNDI("SQL_SALDO_PRESTAMOS_POR_LIQUIDACION");//JJCastro fase2
            if(con!=null){
            st = con.prepareStatement(this.obtenerSQL("SQL_SALDO_PRESTAMOS_POR_LIQUIDACION") );
            st.setString(1, beneficiario );
            rs = st.executeQuery();
            
            while (rs.next()){
                Prestamo pt = new Prestamo();
                pt.setId           ( rs.getInt    ("id"));
                pt.setDistrito     ( rs.getString ("dstrct"));
                pt.setBeneficiario ( rs.getString ("beneficiario"));
                pt.setTercero      ( rs.getString ("tercero"));
                pt.setMonto        ( rs.getDouble ("saldo"));                
                pt.setIntereses    ( rs.getDouble ("intereses"));                
                pt.setCuotas       ( rs.getInt    ("cuotas"));
                lista.add(pt);
            }
            
        }}catch(Exception e) {
            e.printStackTrace();
            throw new SQLException(" PrestamoDAO: obtenerPrestamos  -> \n" +e.getMessage());
        } 
        finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return lista;
    }
    
    
    
    
    /**
     * Metodo para guardar las amortizaciones de un prestamo en la base de datos
     * @autor mfontalvo
     * @param pt, Objeto prestamo que alamacena en una lista las amortizaciones 
     * @param usuario, usuario que graba las amortizaciones
     * @throws Exception.
     */
    public String grabarAmortizacionesSQL ( Prestamo pt , String usuario) throws Exception{

        if (pt==null) return "";
        if (pt.getAmortizacion()==null || pt.getAmortizacion().isEmpty()) return "";
        
        StringStatement2 st  = null; 
        String sql = "";
        try{
            
            st = new StringStatement2(this.obtenerSQL("SQL_INSERT_AMORTIZACION_ABONO"), true);  
            for (int i = 0; i<pt.getAmortizacion().size(); i++){
                Amortizacion am = (Amortizacion) pt.getAmortizacion().get(i);                
                st.clearParameters();
                st.setString (1 ,  pt.getDistrito());
                st.setInt    (2 ,  pt.getId());
                st.setInt    (3 ,  am.getCuota()) ;
                st.setString (4 ,  pt.getBeneficiario());
                st.setString (5 ,  pt.getTercero());
                st.setString (6 ,  "now()"  );
                st.setDouble (7 ,  am.getTotalAPagar());
                st.setDouble (8 ,  am.getCapital    ());
                st.setDouble (9 ,  am.getInteres    ());
                st.setDouble (10,  am.getMonto      ());
                st.setDouble (11,  am.getSaldo      ());
                st.setString (12,  usuario);
                st.setString (13,  "now()" );
                st.setString (14,  usuario);
                st.setString (15,  "now()" );
                st.setString (16,  am.getReferencia()        );
                st.setString (17,  am.getEstadoDescuento()   );
                st.setString (18,  am.getEstadoPagoTercero() );
                sql += st.getSql();
            }
        }catch(Exception e) {
            e.printStackTrace();
            throw new SQLException(" PrestamoDAO: grabarAmortizacionesSQL  -> \n" +e.getMessage());
        } 
        finally {
            if(st!=null)  st = null;
        }  
        return sql;
    }
    
    
    /**
     * Metodo para guardar las amortizaciones de un prestamo en la base de datos
     * @autor mfontalvo
     * @param pt, Objeto prestamo que alamacena en una lista las amortizaciones 
     * @param usuario, usuario que graba las amortizaciones
     * @throws Exception.
     */
    public String updatePrestamoSQL ( Prestamo pt , String usuario) throws Exception{

        if (pt==null) return "";
        
        StringStatement2 st  = null; 
        String sql = "";
        try{
            Amortizacion am = (Amortizacion) pt.getAmortizacion().get(0); 
            
            
            st = new StringStatement2(this.obtenerSQL("SQL_UPDATE_PRESTAMO_ABONOS"));  
            st.setInt    ( pt.getCuotas() );
            st.setInt    ( pt.getCuotas() );
            st.setDouble ( am.getTotalAPagar() );
            st.setDouble ( am.getInteres()     );
            st.setDouble ( am.getCapital()     );
            st.setDouble ( am.getTotalAPagar() );
            st.setDouble ( am.getTotalAPagar() );
            st.setInt    ( pt.getId()     );
            sql += st.getSql();
        }catch(Exception e) {
            throw new SQLException(" PrestamoDAO: grabarAmortizaciones  -> \n" +e.getMessage());
        } 
        finally {
            if(st!=null)  st = null;
        }  
        return sql;
    }
    
    
    /**
     * Metodo para verificar si un prestamo tiene prestamos tipo
     * abonos prestamos.
     * @autor mfontalvo
     * @param where resticciones para el listado de prestamos
     * @return Lista de prestamos.
     * @throws Exception.
     */
    public boolean existePrestamosTipoAbonos ( String beneficiario ) throws Exception{
        Connection        con    = null;
        PreparedStatement st     = null; 
        ResultSet         rs     = null;
        boolean           existe = false;
        String query = "SQL_EXISTE_PRESTAMO";
        try{
            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, beneficiario );
            rs = st.executeQuery();
            
            if (rs.next()){
              existe = true;
            }
            
            }}catch(Exception e) {
            e.printStackTrace();
            throw new SQLException(" PrestamoDAO: obtenerPrestamos  -> \n" +e.getMessage());
        } 
        finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return existe;
    }
    
    
    
    
    /**
     * Metodo para extraer los pagos hechos en un rango de fechas relacionados 
     * a los prestamos de abonos tranferencias
     * @autor mfontalvo
     * @param fecha inicial de pago
     * @param fecha final de pago
     * @return Lista de cuotas.
     * @throws Exception.
     */
    public Vector getPagosPrestamosAbonosTransferencias ( String fecha_inicial, String fecha_final) throws Exception{
        Connection        con    = null;
        PreparedStatement st     = null; 
        ResultSet         rs     = null;
        Vector            datos  = new Vector();
        String query = "SQL_CONSULTA_ABONOS_TRANSFERENCIAS";
        try{
            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, fecha_inicial );
            st.setString(2, fecha_final   );
            rs = st.executeQuery();
            
            while (rs.next()){
              Amortizacion am = new Amortizacion();
              am.setDstrct  ( rs.getString( "dstrct" ) ); 
              am.setPrestamo( rs.getString( "id" ) ); 
              am.setBeneficiario    ( rs.getString( "beneficiario" ) ); 
              am.setNameBeneficiario( rs.getString( "nombre" ) ); 
              am.setPlaca           ( rs.getString( "placa" ) ); 
              am.setCuota           ( rs.getInt   ( "item" ) ); 
              am.setMonto           ( rs.getDouble( "valor_monto" ) ); 
              am.setCapital         ( rs.getDouble( "valor_capital" ) ); 
              am.setInteres         ( rs.getDouble( "valor_interes" ) ); 
              am.setTotalAPagar     ( rs.getDouble( "valor_a_pagar" ) ); 
              am.setSaldo           ( rs.getDouble( "valor_saldo" ) ); 
              am.setReferencia      ( rs.getString( "referencia" ) );               
              datos.add(am);
            }            
        }}catch(Exception e) {
            e.printStackTrace();
            throw new SQLException(" PrestamoDAO: obtenerPrestamos  -> \n" +e.getMessage());
        } 
        finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return datos;
    }
    
     /**
     * Metodo que busca los prestamos qe han sido aprobados.
     * @autor tmolina 2008-09-13
     * @throws Exception.
     */
    public void ListadoPrestamoAprobados() throws Exception{
           Connection con = null;
            PreparedStatement ps = null;
            ResultSet rs = null;
            String  query = "SQL_TRANFERENCIAS_LIST";
            datosPrestamo = new Vector();
            try{
                con = this.conectarJNDI(query);
                if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = ps.executeQuery();
                while (rs.next()){
                    bg = new BeanGeneral();
                    bg.setValor_01(rs.getString(1));  //num_prestamo
                    System.out.println(rs.getString(1));
                    bg.setValor_02(rs.getString(2));  //beneficiario
                    bg.setValor_03(rs.getString(3));  //nombre
                    bg.setValor_04(rs.getString(4));  //monto
                    bg.setValor_05(rs.getString(5));  //fecha
                    bg.setValor_06(rs.getString(6));  //banco_transfer
                    bg.setValor_07(rs.getString(7));  //tipo_cuenta
                    bg.setValor_08(rs.getString(8));  //nombre_cuenta
                    bg.setValor_09(rs.getString(9));  //cedula_cuenta
                    bg.setValor_10(rs.getString(10)); //no_cuenta
                    bg.setValor_11(rs.getString(11)); //observacion
                    datosPrestamo.add(bg);
                    bg = null;
                }
            }}
            catch (Exception ex){
                ex.printStackTrace();
                throw new Exception("ERROR en Listado Prestamos Aprobados: "+ex.getMessage());
            }finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
     }
    
    /**
     * Metodo retorna una lista de los prestamos aprobados
     * @autor tmolina 2008-09-13
     */
    public java.util.Vector getDatosPrestamo() {
          return datosPrestamo;
    }
    
     /**
     * Actualiza los valores de comision y 4 x mil de un determinado prestamo
     * @param codP Codigo del Prestamo
     * @param com comision
     * @param nomB Banco
     * @param codB sucursal
     * @param cuatroxmil Valor cuatro x mil
     * @autor tmolina
     * @throws Exception
     */
    public void updateValoresPrestamo(String codP,String com,String nomB,String codB, String cuatroxmil)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        String query = "ACTUALIZAR_COMISION";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, com);
            st.setString(2, cuatroxmil);
            st.setString(3, com);
            st.setString(4, cuatroxmil);
            st.setString(5, nomB);
            st.setString(6, codB);
            st.setString(7, codP);
            System.out.println(st.toString());
            st.executeUpdate();
            }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE PRESTAMOS " + e.getMessage() + " " + e.getErrorCode());        
        }finally {
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
   }
    
    /**
    * Atualiza el prestamo para indicar que fue transferido al beneficiario.
    * @param cod Codigo del Prestamo
    * @autor tmolina 2008-09-13
    * @throws SQLException
    */
   public void upPrestamo(String cod)throws SQLException{
       Connection con = null;
       PreparedStatement st = null;
       String query = "SQL_ACTUALIZAR_PREST";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, cod);
            st.executeUpdate();
            }}catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA ACTUALIZACION DEL PRESTSAMO " + e.getMessage() + " " + e.getErrorCode());        
        }finally {
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
   }
   
    /**
    * Incrementa la serie para un determinado tipo de documento
    * @param tipo tipo de documento
    * @autor tmolina 2008-09-13
    * @throws Exception
    */
   public String  incrementarSerie(String tipo) throws Exception {
        StringStatement st = null;
        String sql = "";
       String query = "SQL_INC_SERIE";
        try{
             st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
             st.setString(1, tipo);
             sql = st.getSql();//JJCastro fase2
        }catch(Exception e){
            throw new Exception("Error al buscar la serie Prestamo [PrestamoDAO.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return sql;
   }
   
    /**
    * Inicializa la lista de los nits.
    * @autor tmolina 2008-09-13
    */
   public void inicializarNits(){
        nitsx=new ArrayList();
   }
   
    /**
    * Ingresa los subledgers que no esten en la tabla.
    * @param nit nit del beneficiario
    * @param prefijo tipo subledger
    * @param cuenta cuenta contable
    * @param user usuario en sesion
    * @autor tmolina 2008-09-13
    * @throws Exception
    */
   public String subled(String nit, String prefijo, String cuenta, String user)throws Exception{
       StringStatement st = null;
       String sql = "";
       String val = "";
       String query = "SQL_SUBLED_CLI";
       try{
           st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
           val = ValSub(cuenta, prefijo, nit );
           if(val.equals("A")){
               st.setString(1, cuenta );
               st.setString(2, nit );
               st.setString(3, prefijo ); //tipo_subledger
               st.setString(4, nit );
               st.setString(5, user );
               sql=sql+st.getSql();//JJCastro fase2
           }   
       }catch(SQLException e){
           throw new SQLException("ERROR DURANTE LAS INSERCION DE SUBLEDGERS " + e.getMessage() + " " + e.getErrorCode());        
       }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }

       return sql;
   }
   
    /**
    * Hace una validacion para verificar que un subledger no este.
    * @param a cuenta contable
    * @param b tipo subledger
    * @param c nit del beneficiario
    * @autor tmolina 2008-09-13
    * @throws Exception
    */
   public String ValSub(String a,String b,String c ) throws Exception{
       Connection con = null;
       PreparedStatement ps = null;
       ResultSet rs = null;
       String query = "SQL_VALIDAR_SUBLED";
       String res    = "A";
       try{
           con = this.conectarJNDI(query);
           if (con != null) {
           ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
           ps.setString(1, a);
           ps.setString(2, b);
           ps.setString(3, c);
           rs = ps.executeQuery();
           if (rs.next()){
               res = rs.getString(1);
           }
           }}catch (Exception ex){
           ex.printStackTrace();
           throw new Exception(ex.getMessage());
       }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
       return res;
   }
   
      /** 
      * Metodo: save, permite crear el query para insertar el egreso, la cxp y la cxc correspondiente.
      * @param cod: codigo del prestamo
      * @param user: usuario en sesion.
      * @autor : Tmolina 2008-09-13
      * @version : 1.0
      * @throws Exception
      */
   public String save(String cod , String user)throws Exception{

            Connection con = null;
            PreparedStatement st      = null; 
            StringStatement fac     = null;
            StringStatement detfac  = null;
            StringStatement eg      = null;
            StringStatement egd     = null;
            StringStatement cxp     = null;
            StringStatement cxp_items = null;
            ResultSet         rs  = null;
            double tot   = 0;
            double monto = 0;
            double tasa  = 0;
            String[] datos = new String[10];
            String sql       = "";
            String hc        = "";
            String cuenta    = "";
            String cuenta1   = "";
            String cuentaCXC = "";
            String query = "SQL_PRESTAMO_DATOS";//JJCastro fase2
            try{
                con = this.conectarJNDI(query);//JJCastro fase2
                if(con!=null){
                    st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                    fac = new StringStatement(this.obtenerSQL("SQL_INSERTAR_FACTURA"), true);//JJCastro fase2
                    detfac = new StringStatement(this.obtenerSQL("SQL_INSERTAR_DET_FACTURA"), true);//JJCastro fase2
                    cxp = new StringStatement(this.obtenerSQL("INSERT_CXP_DOC"), true); //JJCastro fase2
                    cxp_items = new StringStatement(this.obtenerSQL("INSERT_CXP_DOC_ITEMS"), true); //JJCastro fase2
                    eg = new StringStatement(this.obtenerSQL("SQL_INSERT_EGRESO"), true); //JJCastro fase2
                    egd = new StringStatement(this.obtenerSQL("SQL_INSERT_EGRESODET"), true);//JJCastro fase2
                 
                st.setString(1, cod);
                 
                 rs = st.executeQuery();
                 if(rs.next()){
                   datos[0] = rs.getString("BENEFICIARIO");
                   datos[1] = rs.getString("TIPOPRESTAMO"); //PARA SABER EL HANDLE CODE Y LA CUENTA QUE LLEVARA
                   datos[2] = rs.getString("BANCOTR"); 
                   datos[3] = rs.getString("SUCURSALTR");
                   datos[4] = rs.getString("VLR_GIRADO");
                   datos[5] = rs.getString("PAYMENT_NAME");
                   datos[6] = rs.getString("COMISION");
                   datos[7] = rs.getString("MONTO");
                   datos[8] = rs.getString("TASA");
                   datos[9] = rs.getString("CUATROXMIL");
                 }
                 
                 tot   = Double.parseDouble(datos[4]);
                 monto = Double.parseDouble(datos[7]);
                 tasa  = Double.parseDouble(datos[8]);
                 if(datos[1].equals("AP")){ //AP = Si el prestamo es de abono planillas
                     hc        = "AB";
                     cuenta    = "23050502";
                     cuentaCXC = "13050511";
                     cuenta1   = "23050501";
                 }
                 
                 //INICIO CXC
                 fac.setString(1,  datos[0]);
                 fac.setString(2,  datos[0]);
                 fac.setString(3,  datos[1]);
                 fac.setDouble(4,  monto);
                 fac.setDouble(5,  tasa);
                 fac.setString(6,  user);
                 fac.setDouble(7,  monto);
                 fac.setDouble(8,  0);
                 fac.setDouble(9,  monto);
                 fac.setDouble(10, monto);
                 fac.setDouble(11, 0);
                 fac.setString(12, cod);
                 fac.setString(13, hc);
                 sql+= fac.getSql()+";";//JJCastro fase2
                 //FIN CXC
                 
                 //INICIO detalle CXC
                 detfac.setString(1,  datos[0]);
                 detfac.setDouble(2,  monto);
                 detfac.setDouble(3,  monto);
                 detfac.setDouble(4,  tasa);
                 detfac.setString(5,  user);
                 detfac.setDouble(6,  monto);
                 detfac.setDouble(7,  monto);
                 detfac.setString(8,  cod);
                 detfac.setString(9,  "RD-"+datos[0]);
                 detfac.setString(10, cuentaCXC);
                 sql+= detfac.getSql()+";";//JJCastro fase2
                 //FIN detalle CXC
                 
                 //INICIO CXP_DOC
                 cxp.setString(1, datos[0]);
                 cxp.setString(2, datos[0]);
                 cxp.setString(3,  hc);       //HANDLE CODE
                 cxp.setString(4,  datos[2]);
                 cxp.setString(5,  datos[3]);
                 cxp.setDouble(6,  monto);
                 cxp.setDouble(7,  monto);
                 cxp.setDouble(8,  0);
                 cxp.setDouble(9,  monto);
                 cxp.setDouble(10, monto);
                 cxp.setDouble(11, 0);
                 cxp.setString(12, user);
                 cxp.setString(13, cod);
                 sql+= cxp.getSql()+";";//JJCastro fase2
                 //FIN CXP_DOC
                 
                 //INICIO CXP_ITEMS_DOC
                 cxp_items.setString(1, datos[0]);
                 cxp_items.setString(2, datos[0]);
                 cxp_items.setString(3, datos[7]);
                 cxp_items.setString(4, datos[7]);
                 cxp_items.setString(5, cuenta);
                 cxp_items.setString(6, hc+cod);//cod
                 cxp_items.setString(7, user);
                 cxp_items.setString(8, "AR-"+datos[0]);
                 sql+= cxp_items.getSql()+";";//JJCastro fase2
                 //FIN CXP_ITEMS_DOC
                 
                 //INICIO Egreso
                 eg.setString(1,  datos[2]);
                 eg.setString(2,  datos[3]);
                 eg.setString(3,  datos[0]);
                 eg.setString(4,  datos[5]);
                 eg.setDouble(5,  tot);
                 eg.setDouble(6,  tot);
                 eg.setString(7,  user);
                 eg.setString(8,  datos[0]);
                 eg.setString(9, datos[0]);
                 eg.setString(10, datos[6]);
                 eg.setString(11, datos[9]);
                 sql+=eg.getSql()+";";//JJCastro fase2
                 //FIN Egreso
                 
                 //INICIO Egresodet
                 egd.setString(1, datos[2]);
                 egd.setString(2, datos[3]);
                 egd.setString(3, ""+monto);
                 egd.setString(4, ""+monto);
                 egd.setString(5, user);
                 egd.setString(6, datos[0]);
                 sql+=egd.getSql()+";";//JJCastro fase2
                 //FIN Egresodet  
                 
                 //INICIO Aumentar Serie de CXC
                 sql+= this.incrementarSerie("PRESTAMO")+";";
                 //FIN Aumentar Serie de CXC
                 
                 //INICIO Aumentar Serie de CXP
                 sql+= this.incrementarSerie("FACTPROV")+";";
                 //FIN Aumentar Serie de CXP
                 
                 //INICIO Aumentar Serie de Egreso
                 sql+= this.incrementarSerie("EGRESO")+";";
                 //FIN Aumentar Serie de Egreso
                 
                 //INICIO Creacion de subledgers
                 swNit=true;
                 for (int i=0 ; i<nitsx.size();i++){
                     if (((String)(nitsx.get(i))).equals(datos[0])){
                         swNit=false;
                         break;
                     }
                 }
                 if (swNit){
                    sql +=  subled(datos[0],"RD", cuentaCXC,user)+";";
                    sql +=  subled(datos[0],"RD", cuenta1,user)+";";
                    sql +=  subled(datos[0],"AR", cuenta1,user)+";";
                    sql +=  subled(datos[0],"AR", cuenta,user)+";";
                    nitsx.add(datos[0]);
                 }
                 //FIN Creacion de subledgers
                  
           }}catch(SQLException e){
                 throw new SQLException("ERROR DURANTE LA GENERACION DE DOCS PRESTAMO " + e.getMessage() + " " + e.getErrorCode());        
           }finally{//JJCastro fase2
            if (st  != null){ try{st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (fac  != null){ try{ fac = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (detfac  != null){ try{ detfac = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (eg  != null){ try{ eg = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (egd  != null){ try{ egd = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (cxp  != null){ try{ cxp = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (cxp_items  != null){ try{ cxp_items = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
           }
            
           System.out.println(sql.toString());
            return sql;
   }
   
   /**
     * Metodo para obtener el listado de prestamos ordenado
     * @autor tmolina
     * @param beneficiario beneficiario del prestamo
     * @return Lista de prestamos.
     * @throws Exception.
     */
    public Vector obtenerPrestamosLiquidacionOrderBy ( String beneficiario, String fecha_ultimo_anticipo ) throws Exception{
        Connection        con   = null;
        PreparedStatement st    = null; 
        ResultSet         rs    = null;
        Vector            lista = new Vector();
        try{
            
            con = this.conectarJNDI("SQL_SALDO_PRESTAMOS_POR_LIQUIDACION_ORDERBY");//JJCastro fase2
            if(con!=null){
            st = con.prepareStatement(this.obtenerSQL("SQL_SALDO_PRESTAMOS_POR_LIQUIDACION_ORDERBY") );
            st.setString(1, fecha_ultimo_anticipo );
            st.setString(2, fecha_ultimo_anticipo );
            st.setString(3, beneficiario );
            rs = st.executeQuery();
            
            while (rs.next()){
                Prestamo pt = new Prestamo();
                pt.setId           ( rs.getInt    ("id"));
                pt.setDistrito     ( rs.getString ("dstrct"));
                pt.setBeneficiario ( rs.getString ("beneficiario"));
                pt.setTercero      ( rs.getString ("tercero"));
                pt.setMonto        ( rs.getDouble ("saldo"));                
                pt.setIntereses    ( rs.getDouble ("intereses"));                
                pt.setCuotas       ( rs.getInt    ("cuotas"));
                lista.add(pt);
            }
            
        }}catch(Exception e) {
            e.printStackTrace();
            throw new SQLException(" PrestamoDAO: obtenerPrestamos  -> \n" +e.getMessage());
        } 
        finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return lista;
    }
    
    /**
     * Metodo para guardar las amortizaciones de un prestamo en la base de datos
     * @autor tmolina
     * @param pt, Objeto prestamo que alamacena en una lista las amortizaciones 
     * @param usuario, usuario que graba las amortizaciones
     * @param fecha_ultima_liquidacion, fecha de la ultima liquidacion
     * @throws Exception.
     */
    public String grabarAmortizacionesII ( Prestamo pt , String usuario, String fecha_ultima_liquidacion) throws Exception{

        if (pt==null) return "";
        if (pt.getAmortizacion()==null || pt.getAmortizacion().isEmpty()) return "";
        
        StringStatement2 st  = null; 
        String sql = "";
        try{
            
            st = new StringStatement2(this.obtenerSQL("SQL_INSERT_AMORTIZACION_ABONO"), true);  
            for (int i = 0; i<pt.getAmortizacion().size(); i++){
                Amortizacion am = (Amortizacion) pt.getAmortizacion().get(i);                
                st.clearParameters();
                st.setString (1 ,  pt.getDistrito());
                st.setInt    (2 ,  pt.getId());
                st.setInt    (3 ,  am.getCuota()) ;
                st.setString (4 ,  pt.getBeneficiario());
                st.setString (5 ,  pt.getTercero());
                st.setString (6 ,  fecha_ultima_liquidacion  );
                st.setDouble (7 ,  am.getTotalAPagar());
                st.setDouble (8 ,  am.getCapital    ());
                st.setDouble (9 ,  am.getInteres    ());
                st.setDouble (10,  am.getMonto      ());
                st.setDouble (11,  am.getSaldo      ());
                st.setString (12,  usuario);
                st.setString (13,  "now()" );
                st.setString (14,  usuario);
                st.setString (15,  "now()" );
                st.setString (16,  am.getReferencia()        );
                st.setString (17,  am.getEstadoDescuento()   );
                st.setString (18,  am.getEstadoPagoTercero() );
                sql += st.getSql();
            }
        }catch(Exception e) {
            e.printStackTrace();
            throw new SQLException(" PrestamoDAO: grabarAmortizacionesSQL  -> \n" +e.getMessage());
        } 
        finally {
            if(st!=null)  st = null;
        }  
        return sql;
    }
}//EndClas

