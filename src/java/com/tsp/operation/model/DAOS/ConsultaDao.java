/*
 * ConsultaDao.java
 *
 * Created on 28 de julio de 2005, 06:49 PM
 *
 * Clase para el manejo de la opcion de gestion de consultas.
 */

package com.tsp.operation.model.DAOS;

import java.util.*;
import com.tsp.util.*;
import java.sql.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;
import com.tsp.operation.model.DAOS.MainDAO;
/**
 *
 * @author amendez
 */
public class ConsultaDao extends MainDAO{
  /**Lista para almacenar los resultados de los querys*/
  private List<ConsultaSQL> actionResultsList;
  
  private static Logger logger = Logger.getLogger(ConsultaDao.class);
  
  /** Creates a new instance of ConsultaDao */
  public ConsultaDao() {
      super("ConsultaDAO.xml");
  }
  public ConsultaDao(String dataBaseName) {
      super("ConsultaDAO.xml", dataBaseName);
  }


  /* Original
    public List<ConsultaSQL> getConsultasSQL( Usuario user ) throws SQLException {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    PoolManager poolManager = PoolManager.getInstance();
    List queriesList = new LinkedList<ConsultaSQL>();
    String SQL = "SELECT * FROM consultas " +
    (user != null && !user.getTipo().equals("ADMIN") ?
    "WHERE POSITION('" + user.getLogin() + "' IN usuario) > 0 AND estado = 'A'" : "WHERE estado = 'A'");
    SQL = SQL + " ORDER BY id";

    try {
      conn = poolManager.getConnection("fintra");
      stmt = conn.createStatement();
      rs = stmt.executeQuery( SQL );
      while( rs.next() )
        queriesList.add( ConsultaSQL.load(rs) );
    }finally{
      try {
        if( rs != null ) rs.close();
        rs = null;
      }catch (SQLException ignore){
        rs = null;
      }
      try {
        if( stmt != null ) stmt.close();
        stmt = null;
      }catch (SQLException ignore){
        stmt = null;
      }
      poolManager.freeConnection("fintra", conn);
    }
    return queriesList;
  }
  */

  /**
   * Retorna una lista con los queries guardados en la tabla "consultas"
   * cargados en memoria.
   * @param user Usuario SOT al que se le buscar�n consultas asociadas.
   *             Si es <code>null</code>, las carga TODAS.
   * @throws SQLException Si un error de acceso a la base de datos ocurre.
   */
  public List<ConsultaSQL> getConsultasSQL( Usuario user ) throws SQLException {
      Connection con = null;
      PreparedStatement st = null;
      ResultSet rs = null;
      List queriesList = new LinkedList<ConsultaSQL>();
      String query = "SQL_GETCONSULTA";
      String condicion  = "";
      condicion = (user != null && !user.getTipo().equals("ADMIN") ? "POSITION('" + user.getLogin() + "' IN usuario) > 0 AND " : " ");

      try {
          con = this.conectarJNDI(query);
          if (con != null) {
              st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#CONDICION#", condicion));//JJCastro fase2
              rs = st.executeQuery();
              while (rs.next()) {
                  queriesList.add(ConsultaSQL.load(rs));
              }
          }
      }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA CONSULTA " + e.getMessage()+" " + e.getErrorCode());
      }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    return queriesList;
  }
  
  
 
  public ArrayList<ComponentesCosulta> getValidarComponente(String id) throws SQLException {
      Connection con = null;
      PreparedStatement st = null;
      ResultSet rs = null;
      ArrayList datosComponentes = new ArrayList<ComponentesCosulta>();
      String query = "SQL_VALIDA_COMPONENTE";
    

      try {
          con = this.conectarJNDI(query);
          if (con != null) {
              st = con.prepareStatement(this.obtenerSQL(query));
              st.setString(1, id);
              rs = st.executeQuery();
              while (rs.next()) {
                  ComponentesCosulta componenetes=new ComponentesCosulta();
                  componenetes.setId_consulta(rs.getInt("id_consulta"));
                  componenetes.setTipo_componente(rs.getString("tipo_componente"));
                  componenetes.setParametro(rs.getString("parametro"));
                  datosComponentes.add(componenetes);
              }
          }
      }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA CONSULTA " + e.getMessage()+" " + e.getErrorCode());
      }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    return datosComponentes;
  }
  
  
  
  public String getObtenerCuentas(Usuario user) throws SQLException {
      Connection con = null;
      PreparedStatement st = null;
      ResultSet rs = null;
      String cuentas = "";
      String query = "SQL_CUENTA_USER";
    

      try {
          con = this.conectarJNDI(query);
          if (con != null) {
              st = con.prepareStatement(this.obtenerSQL(query));
              st.setString(1, user.getLogin());
              rs = st.executeQuery();
              while (rs.next()) {
                cuentas=rs.getString("referencia");
              }
          }
      }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA CONSULTA " + e.getMessage()+" " + e.getErrorCode());
      }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    return cuentas;
  }



/**
 * 
 * @param cadenaConsulta
 * @throws SQLException
 */
  private void validarCadenaConsulta(String cadenaConsulta)   throws SQLException  {
      Connection con = null;
      PreparedStatement st = null;
      ResultSet rs = null;
      Properties palabrasReservadas = new Properties();
      String query = "SQL_VALIDADAR_CADENA";

      try {
          // Hallar el listado de palabras reservadas invalidas.
          con = this.conectarJNDI(query);
          if (con != null) {
              st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
              rs = st.executeQuery();
              while (rs.next()) {
                  String palabraReservada = rs.getString("table_code").toString();
                  palabrasReservadas.setProperty(palabraReservada, palabraReservada);
              }
          }
      }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    
    // Validar query utilizando el separador de salto de linea (<br>)
    String subquery = null;
    Collection conjPalabrasInvalidas = palabrasReservadas.values();
    String [] queries = cadenaConsulta.toLowerCase().split("<br>");
    for( int idx = 0; idx < queries.length; idx++ )
    {
      subquery = queries[idx].trim();
      for (Iterator conjPalabrasInvalidasIt = conjPalabrasInvalidas.iterator(); conjPalabrasInvalidasIt.hasNext();) {
        String palabraInvalida = (String) conjPalabrasInvalidasIt.next();
        if( subquery.startsWith(palabraInvalida.toLowerCase()) )
          throw new SQLException("��SOLO SE ADMITEN INSTRUCCIONES \"CREATE\" (PARA CREACION DE TABLAS TEMPORALES) Y \"SELECT\" (CONSULTAS)!!");
      }
    }
    
    // Validar con el separador de consultas en PSQL (;).
    queries = cadenaConsulta.toLowerCase().split(";");
    for( int idx = 0; idx < queries.length; idx++ )
    {
      subquery = queries[idx].trim();
      for (Iterator conjPalabrasInvalidasIt = conjPalabrasInvalidas.iterator(); conjPalabrasInvalidasIt.hasNext();) {
        String palabraInvalida = (String) conjPalabrasInvalidasIt.next();
        if( subquery.startsWith(palabraInvalida.toLowerCase()) )
          throw new SQLException("��SOLO SE ADMITEN INSTRUCCIONES \"CREATE\" (PARA CREACION DE TABLAS TEMPORALES) Y \"SELECT\" (CONSULTAS)!!");
      }
    }
  }




  
  /**
   * Guarda en la base de datos un comando SQL para su posterior ejecuci�n.
   * @param httpRequest Peticion HTTP.
   * @throws SQLException Si un error de acceso a la base de datos ocurre.
   * @throws InformationException Si hay un error de sintaxis en el query que se va a ejecutar.
   */
  public void consultasManageSqlCreate(Hashtable<String, String[]> parameters) throws SQLException, InformationException {
    Connection con = null;
    Connection conn1 = null;
    PreparedStatement pstmt1 = null;
    PreparedStatement pstmt2 = null;
    String query[]         = parameters.get("query");
    this.validarCadenaConsulta(query[0]);
    String [] queries = query[0].split(";");
    String creador[]       = parameters.get("creador");
    String usuario[]     = parameters.get("userAdd");
    String descripcion[]   = parameters.get("descripcion");
    String nombresParams[] = parameters.get("nombresParams");
    String baseDeDatos[] = parameters.get("baseDeDatos");
    String usuarios = Arrays.deepToString(usuario);
    usuarios = usuarios.replace("[", "").replace("]", "");
    String qry =  "SQL_INSERT_CONSULTA";

    try {
      con = this.conectarJNDI(qry);//JJCastro fase2
      for( int idx = 0; idx < queries.length; idx++ )
      {
        try {
          String queryString = queries[idx].trim().replaceAll("<param-in>", "''");
          pstmt1 = con.prepareStatement( queryString );
        }catch (SQLException SQLE){
          throw new InformationException(
          "Hay un error de sintaxis en el query digitado." +
          "\nDetalles: " + SQLE.getSQLState() + " - " + SQLE.getMessage()
          );
        }finally{
            if (pstmt1  != null){ try{ pstmt1.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
      }

      pstmt2 = con.prepareStatement(this.obtenerSQL(qry));//JJCastro fase2
      pstmt2.setString(1, usuarios);
      pstmt2.setString(2, query[0]);
      pstmt2.setString(3, descripcion[0]);
      pstmt2.setString(4, nombresParams != null && !nombresParams[0].trim().equals("") ? nombresParams[0] : "");
      pstmt2.setString(5, creador[0]);
      pstmt2.setString(6, baseDeDatos[0]);
      pstmt2.executeUpdate();
    } catch(SQLException e){
        throw new InformationException("Error consultasManageSqlCreate: "+e.getMessage());
    }finally{
      if (pstmt2  != null){ try{ pstmt2.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
      if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
    }
  }
  
  /**
   * Ejecuta un comando SQL guardado en la base de datos.
   * @param httpRequest Peticion HTTP.
   * @throws SQLException Si un error de acceso a la base de datos ocurre.
   * @throws InformationException Si hay un error de sintaxis en el query que se va a ejecutar.
   */
  public synchronized void consultasManageSqlExecute(Hashtable<String, String[]> parameters)   throws SQLException, InformationException {

      int idx = 0;
      Connection con = null;
      Connection oraconn = null;
      PreparedStatement stmt = null;
      PreparedStatement pstmt = null;
      ResultSet rs = null;
      String recordId[] = parameters.get("recordId");
      String qry = "SQL_CONSULTA_ESPECIFICA";


    try {
//Parte1
              con = this.conectarJNDI(qry);//JJCastro fase2
              String query[] = new String[1];
              String [] paramValues = null;
              if( recordId != null ) {
                                // Buscar el registro con ese ID
                                stmt = con.prepareStatement(this.obtenerSQL(qry));//JJCastro fase2
                                stmt.setString(1, recordId[0]);
                                rs = stmt.executeQuery();

                                if(rs.next()){
                                query[0] = rs.getString("query");
                                }

                                String [] param = null;
                                Vector<String> paramSet = new Vector<String>();
                                for( idx = 0; parameters.containsKey("param" + recordId[0] + idx); idx++ )
                                 paramSet.add(parameters.get("param" + recordId[0] + idx)[0].replace("<percentage>", "%"));
                                if( paramSet.size() > 0 )
                                {
                                  paramValues = new String[paramSet.size()];
                                  for( idx = 0; idx < paramSet.size(); idx++ )
                                    paramValues[idx] = paramSet.get(idx);
                                }

              }else{

                              query = parameters.get("query");
                              String[] paramValues2 = parameters.get("valoresParams");
                              if (paramValues2 != null && !paramValues2[0].trim().equals("")) {
                                  paramValues = paramValues2[0].split(",");
                              }

              }
//Fin Parte1

      this.validarCadenaConsulta(query[0]);
      
      try{
        // Si el query tiene par�metros, a��dalos.
        // IMPORTANTE: SOLO ACEPTA CADENAS!!
        if( paramValues != null ) {

                for( idx = 0; idx < paramValues.length; idx++ ){

                    int idxParam = query[0].indexOf('?');
                    int idxParamSet = query[0].indexOf("<param-in>");
                    if( idxParamSet >= 0 && ((idxParam >= 0 && idxParamSet < idxParam) || idxParam < 0) ){

                              String csvValues = "";
                              String [] csvSplittedValues = paramValues[idx].split(",");
                              for( int jdx = 0; jdx < csvSplittedValues.length; jdx++ )
                              csvValues += (csvValues.equals("") ? "'" : ",'") + csvSplittedValues[jdx].trim() + "'";
                              query[0] = query[0].replaceFirst("<param-in>", csvValues);
                    
                    }else{
                      query[0] = query[0].replaceFirst("\\?", "'" + paramValues[idx].trim() + "'");
                    }

                }

        }
        
        // Ejecutar el query
        idx = 0;
        String [] queries = query[0].split("<(b|B)(r|R)>");
        String baseDeDatos[] = parameters.get("baseDeDatos");
        if( "PSQL_SOT".equals(baseDeDatos[0]) ){

            // Ejecutar los queries para crear las tablas temporales
                              while( idx < queries.length && queries[idx].trim().toLowerCase().startsWith("create") ){
                                   try {
                                         pstmt = con.prepareStatement(queries[idx]);
                                          pstmt.executeUpdate();
                                    }catch (Exception E){
                                         throw new InformationException("Error : "+E.getMessage());
                                    }finally{
                                      if (pstmt  != null){ try{ pstmt.close(); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                                    }
                                    idx++;
                              }
                              pstmt = con.prepareStatement(queries[idx]);
        
        }else{

                          oraconn = this.conectarBDJNDI("mim");
                          // Ejecutar los queries para crear las tablas temporales
                          while( idx < queries.length && queries[idx].trim().toLowerCase().startsWith("create") )
                          {
                            try {
                              pstmt = oraconn.prepareStatement(queries[idx]);
                              pstmt.executeUpdate();
                            }catch (Exception E){
                            }finally{
                             if (pstmt  != null){ try{ pstmt.close(); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                            }
                            idx++;
                          }
                          pstmt = oraconn.prepareStatement(queries[idx]);
        }
        
              // Ejecutar el query y extraer los resultados.
              rs = pstmt.executeQuery();
              this.setActionResultsList(null);
              this.setActionResultsList(new LinkedList<ConsultaSQL>());
              while (rs.next()) {
                  this.getActionResultsList().add(ConsultaSQL.loadExecutionResults(rs));
              }
        
      }catch (SQLException SQLE){
        System.out.println("error en consultadao"+SQLE.toString()+"__"+SQLE.getMessage());
        throw new InformationException("Hay un error de sintaxis en el query digitado." +  "\nDetalles: " + SQLE.getSQLState() + " - " + SQLE.getMessage());
      }
    }
      finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (stmt  != null){ try{ stmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (pstmt  != null){ try{ pstmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                if (oraconn != null){ try{ this.desconectar(oraconn); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}

      }
  }





  
  /**
   *BUSCA UN QUERY GUARDADO EN LA TABLA CONSULTAS POR EL ID
   *@param id El id de la consulta a buscar.
   *@throws SQLException Si un error de acceso a la base de datos ocurre.
   */
  public ConsultaSQL getQry(String id) throws SQLException{

      Connection con = null;
      PreparedStatement psttm = null;
      ResultSet rs = null;
      ConsultaSQL consultaSQL = null;
      String query = "SQL_ALL_CONSULTAS";
      try {
          con = this.conectarJNDI(query);//JJCastro fase2
          if (con != null) {
              psttm = con.prepareStatement(query);
              psttm.setString(1, id);
              rs = psttm.executeQuery();
              if (rs.next()) {
                  consultaSQL = ConsultaSQL.load(rs);
              }
          }
      } catch(SQLException e){
      throw new SQLException("ERROR OBTENIENDO DATOS DEL QUERY: " + e.getMessage());
    } finally{
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (psttm  != null){ try{ psttm.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
    }
    return consultaSQL;
  }
  
  /**
   * Modifica un query que ha sido guardado con anterioridad en la BD.
   * @param httpRequest Peticion HTTP.
   * @throws SQLException Si un error de acceso a la base de datos ocurre.
   * @throws InformationException Si hay un error de sintaxis en el query que se va a ejecutar.
   */
  public void consultasManageSqlUpdate(Hashtable<String, String[]> parameters) throws SQLException, InformationException {
    Connection con = null;
    PreparedStatement pstmt1 = null;
    PreparedStatement pstmt2 = null;
    String query[] = parameters.get("query");
    this.validarCadenaConsulta(query[0]);
    String [] queries = query[0].split(";");
    String id[]            = parameters.get("id");
    String usuario[]     = parameters.get("userAdd");
    String descripcion[]   = parameters.get("descripcion");
    String nombresParams[] = parameters.get("nombresParams");
    String baseDeDatos[] = parameters.get("baseDeDatos");
    String usuarios = Arrays.deepToString(usuario);
    usuarios = usuarios.replace("[", "").replace("]", "");
    String qry = "SQL_UPDATE_CONSULTA";

    try {
      con = this.conectarJNDI(qry);//JJCastro fase2
      if (con != null) {
      for( int idx = 0; idx < queries.length; idx++ ){
                        try {
                          String queryString = queries[idx].trim().replaceAll("<param-in>", "''");
                          pstmt1 = con.prepareStatement( queryString );
                        }catch (SQLException SQLE){
                          throw new InformationException(
                          "Hay un error de sintaxis en el query digitado." +
                          "\nDetalles: " + SQLE.getSQLState() + " - " + SQLE.getMessage()
                          );
                        }finally{
                        if (pstmt1  != null){ try{ pstmt1.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                        }
         }
       
      
      pstmt2 = con.prepareStatement(this.obtenerSQL(qry));//JJCastro fase2
      pstmt2.setString(1, usuarios);
      pstmt2.setString(2, query[0]);
      pstmt2.setString(3, descripcion[0]);
      pstmt2.setString(4, nombresParams != null && !nombresParams[0].trim().equals("") ? nombresParams[0] : "");
      pstmt2.setString(5, baseDeDatos[0]);
      pstmt2.setString(6, id[0]);
      pstmt2.executeUpdate();
      }
    }catch(SQLException e){
      throw new SQLException("ERROR OBTENIENDO DATOS DEL QUERY: " + e.getMessage());
    } finally{ // pstmt1   pstmt2   con
                if (pstmt1  != null){ try{ pstmt1.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (pstmt2  != null){ try{ pstmt2.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
    }
  }
  
  /**
   * Cambia el estado de un query a 'I' inactivo para que no sea utilizado.
   * @param httpRequest Peticion HTTP.
   * @throws SQLException Si un error de acceso a la base de datos ocurre.
   * @throws InformationException Si hay un error de sintaxis en el query que se va a ejecutar.
   */
  public void consultasManageSqlDelete( String id ) throws SQLException{
    Connection con = null;
    PreparedStatement pstmt1 = null;
    String query = "SQL_UPDATECONSULTA_ESTADO" ;
    try {

     con = this.conectarJNDI(query);
          if (con != null) {
              pstmt1 = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
              pstmt1.setString(1, id);
              pstmt1.executeUpdate();
          }
      } catch(SQLException e){
      throw new SQLException("� ERROR ELIMINANDO CONSULTA !" + e.getMessage());
    } finally{
                if (pstmt1  != null){ try{ pstmt1.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
    }
  }
  
  /**
   * Busca un usuario a partir de su ID.
   * @param id ID del usuario a buscar.
   * @throws SQLException Si un error de acceso a la base de datos ocurre.
   * @return Usuario encontrado.
   */
  public Usuario UsuarioSearch(final String id) throws Exception {
    Connection con = null;
      ResultSet rs = null;
      PreparedStatement pstmt = null;
      Usuario user = null;
      String query = "SQL_CONSULTA_USUARIO";
      try {
          con = this.conectarJNDIFintra();//JJCastro fase2
          // Chequear que el usuario exista.
          if (con != null) {
              pstmt = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
              pstmt.setString(1, id.trim());
              rs = pstmt.executeQuery();
              if (rs.next()) {
                  user = Usuario.load(rs, true);
              }
          }
      }catch(Exception ec){
            throw new Exception("Error en la consulta buscar por: "+ec.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (pstmt  != null){ try{ pstmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
     return user;
  }
  
  /**
   * Busca un usuario a partir de su ID.
   * @param id ID del usuario a buscar.
   * @throws SQLException Si un error de acceso a la base de datos ocurre.
   * @return Usuario encontrado.
   */
  public Usuario UsuarioSearch(final String id, String distrito) throws Exception {
    Connection con = null;
      ResultSet rs = null;
      PreparedStatement pstmt = null;
      Usuario user = null;
      String query = "SQL_CONSULTA_USUARIO_2";
      try {
          con = this.conectarJNDIFintra();//JJCastro fase2
          // Chequear que el usuario exista.
          if (con != null) {
              pstmt = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
              pstmt.setString(1, id.trim());
              pstmt.setString(2, distrito.trim());
              rs = pstmt.executeQuery();
              if (rs.next()) {
                  user = Usuario.load(rs, true);
              }
          }
      }catch(Exception ec){
            throw new Exception("Error en la consulta buscar por: "+ec.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (pstmt  != null){ try{ pstmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
     return user;
  }
  
  /**
   * Retorna una lista con los usuarios de la aplicaci�n que pertenezcan
   * a un determinado tipo.
   * @param tipo Tipo de usuario. Si es <code>null</code>, los carga TODOS.
   * @return Lista con los datos de los usuarios cargados en memoria.
   * @throws SQLException Si un error de acceso a la base de datos ocurre.
   */
  public List getUsuariosSot(String tipo) throws Exception {
    Connection con = null;
      Statement stmt = null;
      ResultSet rs = null;
      List usersList = new LinkedList();
      String reemplazar = "";
      String query = "SQL_GET_USUARIO";//JJCastro fase2
      String sql = "";
      try {
          con = this.conectarJNDIFintra();
          if (con != null) {
              reemplazar = (tipo != null ? "  AND tipo = '" + tipo + "' " : "");
              sql = this.obtenerSQL(query).replaceAll("#reemplazar#", reemplazar);
              stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
              rs = stmt.executeQuery(sql);

              while (rs.next()) {
                  usersList.add(Usuario.load(rs, false));
              }
          }
      }catch(Exception ec){
            throw new Exception("Error en la consulta buscar por: "+ec.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (stmt  != null){ try{ stmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    return usersList;
  }
  //----------------------------------------------------------------------------
  // Fin de la implementaci�n del m�dulo administrativo de consultas.
  //----------------------------------------------------------------------------
  
  public synchronized List<ConsultaSQL> getActionResultsList() {
    return actionResultsList;
  }
  
  public synchronized void setActionResultsList(List<ConsultaSQL> actionResultsList) {
    this.actionResultsList = actionResultsList;
  }
}
