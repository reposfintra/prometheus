/*
 * VerificacionDocDAO.java
 *
 * Created on 30 de septiembre de 2005, 05:08 PM
 */

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;



/**
 *
 * @author  dbastidas
 */
public class VerificacionDocDAO  extends MainDAO{
    
    /** Creates a new instance of VerificacionDocDAO */
    public VerificacionDocDAO() {
        super("VerificacionDocDAO.xml");
    }
    private VerificacionDoc veridoc;
    private Vector Vecverfdoc;
    
    private static String  actualizar =   "     update nit set    "+
                                          "     t_libmilitar = ?, "+
                                          "     t_cedula = ?      "+
                                          "  where                "+
                                          "       cedula = ?;     "+ 
                                          "  update conductor set "+
                                          "       t_judicial = ?, "+
                                          "       t_eps = ?,      "+
                                          "       t_arp = ?,      "+
                                          "       t_categoriapase = ?, "+
                                          "       t_libtripulante = ?, "+
                                          "       t_pasaporte = ?,     "+
                                          "       t_visa = ?,          "+ 
                                          "       usuarioverifica = ?, "+
                                          "       fecverificacion = ?  "+
                                          "  where                     "+
                                          "        cedula = ?;         "+
                                          "  update placa set          "+
                                          "       t_tarpropcab = ?,    "+
                                          "       t_copiacontcomventa = ?, "+
                                          "       t_copiacontarr = ?,     "+
                                          "       t_tarpropsemi = ?,      "+
                                          "       t_certemgases = ?,      "+
                                          "       t_soat = ?,             "+
                                          "       t_regnalcarga = ?,      "+
                                          "       t_regnalsemire = ?,     "+
                                          "       t_tarempresarial = ?,   "+
                                          "       t_fotocceducond = ?,    "+ 
                                          "       t_tarhabil = ?,         "+
                                          "       usrqueverifica = ?,     "+
                                          "       fecverificacion = ?     "+
                                          "   where                       "+
                                          "        conductor = ?;         ";
    
   private static String  buscardocConductor = "select  n.nombre,          "+          
                                               "    n.libmilitar,          "+         
                                               "    n.cedula,              "+         
                                               "    n.t_libmilitar,        "+         
					       "    n.t_cedula,            "+         
                                               "    c.nrojudicial,         "+         
                                               "    c.vencejudicial,       "+         
                                               "    c.nomeps,              "+         
                                               "    c.nroeps,              "+         
                                               "    c.fecafieps,           "+         
                                               "    c.nomarp,              "+         
                                               "    c.fecafiarp,           "+         
                                               "    c.nopase,              "+         
                                               "    c.categoriapase,       "+         
                                               "    c.vigenciapase,        "+         
                                               "    c.nrolibtripulante,    "+         
                                               "    c.vencelibtripulante,  "+         
                                               "    c.nopasaporte,         "+         
                                               "    c.pasaportevence,      "+        
                                               "    c.nrovisa,             "+        
                                               "    c.vencevisa,           "+        
                                               "    c.t_judicial,          "+        
					       "    c.t_eps,               "+        
					       "    c.t_arp,               "+        
				               "    c.t_categoriapase,     "+        
					       "   c.t_libtripulante,      "+       
					       "   c.t_pasaporte,          "+       
                                               "    c.t_visa		   "+ 	   
                                               "    from                   "+        
                                               "     conductor c,          "+         
                                               "     nit n                 "+        
                                               "     where                 "+         
                                               "         c.cedula = ?      "+                
                                               "    and  n.cedula = c.cedula  ";
   
   private static String buscardocPlaca ="select a.*,                             "+
                                         "         b.tarprop as tarpropsemi,  "+
                                         "         b.fecvenprop as ventarpropsemi, "+
                                         "         b.reg_nal_carga as regnalsemi,  "+
                                         "         b.fecvenreg   as venregnalsemi  "+ 
                                         "         from                            "+
                                         "    (select  		                   "+ 
                                         "          p.placa,                       "+															
                                         "          p.tarprop,                     "+
                                         "          p.fecvenprop,                  "+
                                         "          p.certemgases,                 "+
                                         "          p.fecvegases,                  "+
                                         "          p.ciasoat,                     "+
                                         "          p.venseguroobliga,             "+
                                         "          p.reg_nal_carga,               "+
                                         "          p.fecvenreg,                   "+
                                         "          p.tarempresa,                  "+
                                         "          p.fecvenempresa,               "+
                                         "    	    p.tarhabil,                    "+
                                         "          p.fecvenhabil,                 "+
                                         "          p.poliza_andina,               "+
                                         "          p.fecvenandina,                "+
                                         "          p.placa_trailer,               "+ 
                                         "          p.trailer,                     "+
					 "          p.t_tarpropcab,                "+
					 "          p.t_copiacontcomventa,         "+
					 " 	    p.t_copiacontarr,              "+
					 " 	    p.t_tarpropsemi,               "+ 
 				         "          p.t_certemgases,               "+
					 "          p.t_soat,                      "+
					 " 	    p.t_regnalcarga,               "+
                                         "  	    p.t_regnalsemire,              "+
					 "          p.t_tarempresarial,            "+
					 "          p.t_fotocceducond,             "+
					 " 	    p.t_tarhabil,                  "+ 
                                         " 	    p.t_polizaandina      	   "+ 				    
                                         "          from                           "+
                                         "           placa p                       "+   
                                         "           where                         "+
				   	 "		placa = ? )A               "+ 
                                         "          LEFT JOIN placa b  ON (b.placa = a.trailer) ";
   
   private static String  act_cedula  =   "     update nit set    "+
                                          "     t_cedula = ?      "+
                                          "  where                "+
                                          "       cedula = ?;     ";
   
   private static String  act_libmilitar  =   "     update nit set    "+
                                              "     t_libmilitar = ?    "+
                                              "  where                "+
                                              "       cedula = ?;     ";
  
   private static String  act_judicial  =   "     update conductor set    "+
                                              "     t_judicial = ?    "+
                                              "  where                "+
                                              "       cedula = ?;     ";
   
   private static String  act_eps  =   "     update conductor set    "+
                                       "     t_eps = ?    "+
                                       "  where                "+
                                       "       cedula = ?;     ";
   
   private static String  act_arp  =   "     update conductor set    "+
                                       "     t_arp = ?    "+
                                       "  where                "+
                                       "       cedula = ?;     ";
  
   private static String  act_categoriapase  =   "     update conductor set    "+
                                                 "     t_categoriapase = ?    "+
                                                 "  where                "+
                                                 "       cedula = ?;     ";
   
   private static String  act_libtripulante  =   "     update conductor set    "+
                                                 "     t_libtripulante = ?    "+
                                                 "  where                "+
                                                 "       cedula = ?;     ";
   
   private static String  act_pasaporte  =   "     update conductor set    "+
                                                 "     t_pasaporte = ?    "+
                                                 "  where                "+
                                                 "       cedula = ?;     ";
   
   private static String  act_visa  =   "     update conductor set    "+
                                                 "     t_visa = ?    "+
                                                 "  where                "+
                                                 "       cedula = ?;     ";
   
   private static String  act_tarpropcab  =   "  update placa set          "+
                                              "       t_tarpropcab = ?    "+
                                              "   where                       "+
                                              "        placa = ?;         ";
   
   private static String  act_copiacontcomventa  =   "  update placa set          "+
                                              "       t_copiacontcomventa = ?    "+
                                              "   where                       "+
                                              "        placa = ?;         ";
   
   private static String  act_copiacontarr  =   "  update placa set          "+
                                                "       t_copiacontarr = ?    "+
                                                "   where                       "+
                                                "        placa = ?;         ";
   
   private static String  act_tarpropsemi  =   "  update placa set          "+
                                                "       t_tarpropsemi = ?   "+
                                                "   where                   "+
                                                "        placa = ?;     ";
   
   private static String  act_certemgases  =   "  update placa set          "+
                                                "       t_certemgases = ?   "+
                                                "   where                   "+
                                                "        placa = ?;     ";
   
   private static String  act_soat  =   "  update placa set          "+
                                        "       t_soat = ?   "+
                                        "   where                   "+
                                        "        placa = ?;     ";
   
   private static String  act_regnalcarga  =   "  update placa set          "+
                                               "       t_regnalcarga = ?   "+
                                               "   where                   "+
                                               "        placa = ?;     ";
   
   private static String  act_regnalsemire  =   "  update placa set          "+
                                               "       t_regnalsemire = ?   "+
                                               "   where                   "+
                                               "        placa = ?;     ";
    
   private static String  act_tarempresarial  = "  update placa set          "+
                                                "       t_tarempresarial = ?   "+
                                                "   where                   "+
                                                "        placa = ?;     ";
   
   private static String  act_tarhabil  = "  update placa set          "+
                                          "       t_tarhabil = ?   "+
                                          "   where                   "+
                                          "        placa = ?;     ";
   
   private static String  act_polizaandina  = "  update placa set          "+
                                          "       t_polizaandina = ?   "+
                                          "   where                   "+
                                          "        placa = ?;     ";
   
   private static String  verificarplaca ="  update placa set          "+
                                          "       usrqueverifica = ?,     "+
                                          "       fecverificacion = ?     "+
                                          "   where                       "+
                                          "        placa = ?;         ";
   
   private static String  verificarconductor = "  update conductor set          "+
                                          "       usuarioverifica = ?,     "+
                                          "       fecverificacion = ?     "+
                                          "   where                       "+
                                          "        cedula = ?;         ";
   
   private static String huella_der = "  update conductor set          "+
                                          "       huella_der = ?     "+
                                          "   where                       "+
                                          "        cedula = ?;         ";
   
   private static String huella_izq = "  update conductor set          "+
                                          "       huella_izq = ?     "+
                                          "   where                       "+
                                          "        cedula = ?;         ";
   
   private static String consultasPlaca = "";
   private static String consultasConductor = "";
   
   public VerificacionDoc ObtDocumentos(){
       return veridoc;
   }
   
   public void setDocumentos(VerificacionDoc documen){
       this.veridoc = documen;
   }

 /**
  * 
  * @throws SQLException
  */
   public void VerificarDocumento() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
       String query = "SQL_ACTUALIZAR";
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, veridoc.getT_libmilitar() );
                st.setString(2, veridoc.getT_cedula() );
                st.setString(3, veridoc.getCedula());
                
                st.setString(4, veridoc.getT_judicial());
                st.setString(5, veridoc.getT_eps());
                st.setString(6, veridoc.getT_arp());
                st.setString(7, veridoc.getT_categoriapase());
                st.setString(8, veridoc.getT_libtripulante());
                st.setString(9, veridoc.getT_pasaporte());
                st.setString(10, veridoc.getT_visa());
                st.setString(11, veridoc.getUsuarioverica());
                st.setString(12, veridoc.getFechavericacion());
                st.setString(13, veridoc.getCedula());
                
                st.setString(14, veridoc.getT_tarpropcab());
                st.setString(15, veridoc.getT_copiacontcomventa());
                st.setString(16, veridoc.getT_copiacontarr());
                st.setString(17, veridoc.getT_tarpropsemi());
                st.setString(18, veridoc.getT_certemgases());
                st.setString(19, veridoc.getT_soat());
                st.setString(20, veridoc.getT_regnalcarga());
                st.setString(21, veridoc.getT_regnalsemire());
                st.setString(22, veridoc.getT_tarempresarial());
                st.setString(23, veridoc.getT_cedula());
                st.setString(24, veridoc.getT_tarhabil());
                st.setString(25, veridoc.getUsuarioverica());
                st.setString(26, veridoc.getFechavericacion());
                st.setString(27, veridoc.getCedula());

                ////System.out.println("Modificar "+st);
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL VERIFICAR LOS DOCUMENTOS" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


/**
 * 
 * @param ced
 * @throws SQLException
 */
   public void cargarDocConductor(String ced)throws SQLException {
       Connection con= null;
       PreparedStatement st = null;
       ResultSet rs = null;       
       veridoc = null;
       String query = "SQL_BUSCARDOC_CONDUCTOR";
       try{
           con = this.conectarJNDI(query);
           if (con != null) {
               st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, ced );

                ////System.out.println(st);
                
                rs = st.executeQuery();
                
                if (rs.next()){
                     VerificacionDoc doctos = new VerificacionDoc();
                     doctos.setNombre(rs.getString("nombre"));
                     doctos.setLibmilitar(rs.getString("libmilitar")); 
                     doctos.setT_libmilitar(rs.getString("t_libmilitar"));
                     
                     doctos.setCedula(rs.getString("cedula"));                    
                     doctos.setT_cedula(rs.getString("t_cedula"));
                                          
                     doctos.setNrojudicial(rs.getString("nrojudicial"));
                     doctos.setVencejudicial(rs.getString("vencejudicial"));
                     doctos.setT_judicial(rs.getString("t_judicial"));
                     
                     doctos.setNomeps(rs.getString("nomeps"));
                     doctos.setNroeps(rs.getString("nroeps"));
                     doctos.setFecafieps(rs.getString("fecafieps"));
                     doctos.setT_eps(rs.getString("t_eps"));
                     
                     doctos.setNomarp(rs.getString("nomarp"));
                     doctos.setFecafiarp(rs.getString("fecafiarp"));
                     doctos.setT_arp(rs.getString("t_arp"));
                     
                     doctos.setNopase(rs.getString("nopase"));
                     doctos.setCategoriapase(rs.getString("categoriapase"));
                     doctos.setVigenciapase(rs.getString("vigenciapase"));
                     doctos.setT_categoriapase(rs.getString("t_categoriapase"));
                     
                     doctos.setNrolibtripulante(rs.getString("nrolibtripulante"));
                     doctos.setVencelibtripulante(rs.getString("vencelibtripulante"));
                     doctos.setT_libtripulante(rs.getString("t_libtripulante"));
                     
                     doctos.setNopasaporte(rs.getString("nopasaporte"));
                     doctos.setVencepasaporte(rs.getString("pasaportevence"));
                     doctos.setT_pasaporte(rs.getString("t_pasaporte"));
                     
                     doctos.setNrovisa(rs.getString("nrovisa"));
                     doctos.setVencevisa(rs.getString("vencevisa"));
                     doctos.setT_visa(rs.getString("t_visa"));
                     veridoc = doctos;
                     
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL BUSCAR DOCUMENTOS " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
       
   }
   
    public void cargarDocPlaca(String pla)throws SQLException {
       Connection con= null;
       PreparedStatement st = null;
       ResultSet rs = null;       
        veridoc = null;
        String query = "SQL_BUSCARDOC_PLACA";
       try{
           con = this.conectarJNDI(query);
           if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, pla );
               
                rs = st.executeQuery();
                
                if (rs.next()){
                     VerificacionDoc doctos = new VerificacionDoc();
                     doctos.setPlaca(rs.getString("placa"));
                     doctos.setTarprop(rs.getString("tarprop"));
                     doctos.setFecvenprop(rs.getString("fecvenprop"));
                     doctos.setT_tarpropcab(rs.getString("t_tarpropcab"));
                     
                     doctos.setCertemgases(rs.getString("certemgases"));
                     doctos.setFecvegases(rs.getString("fecvegases"));
                     doctos.setT_certemgases(rs.getString("t_certemgases"));
                     
                     doctos.setCiasoat(rs.getString("ciasoat"));
                     doctos.setVenseguroobliga(rs.getString("venseguroobliga"));
                     doctos.setT_soat(rs.getString("t_soat"));
                     
                     doctos.setReg_nal_carga(rs.getString("reg_nal_carga"));
                     doctos.setFecvenreg(rs.getString("fecvenreg"));
                     doctos.setT_regnalcarga(rs.getString("t_regnalcarga"));
                     
                     
                     doctos.setTarempresa(rs.getString("tarempresa"));
                     doctos.setFecvenempresa(rs.getString("fecvenempresa"));
                     doctos.setT_tarempresarial(rs.getString("t_tarempresarial"));
                     
                     doctos.setTarhabil(rs.getString("tarhabil"));
                     doctos.setFecvenhabil(rs.getString("fecvenhabil"));
                     doctos.setT_tarhabil(rs.getString("t_tarhabil"));
                     
                     doctos.setTarpropsemi(rs.getString("tarpropsemi"));
                     doctos.setVentarpropsemi(rs.getString("ventarpropsemi"));
                     doctos.setT_tarpropsemi(rs.getString("t_tarpropsemi"));
                     
                     doctos.setRegnalsemi(rs.getString("regnalsemi"));
                     doctos.setVenregnalsemi(rs.getString("venregnalsemi"));
                     doctos.setT_regnalsemire(rs.getString("t_regnalsemire"));
                     
                     doctos.setPoliza_andina(rs.getString("poliza_andina"));
                     doctos.setFecvenandina(rs.getString("fecvenandina"));
                     doctos.setT_polizaandina(rs.getString("t_polizaandina"));
                     
                     doctos.setT_copiacontarr(rs.getString("t_copiacontarr"));
                     doctos.setT_copiacontcomventa(rs.getString("t_copiacontcomventa"));
                     
                     veridoc = doctos;
                     
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL BUSCAR DOCUMENTOS " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
       
   }


/**
 * 
 * @param cedula
 * @param estado
 * @throws SQLException
 */
   public void tieneDocumentosConductor(String cedula, String estado  ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        int sw =0, sw1=0;
        
        try{
            con = this.conectarJNDI("SQL_CONSULTA_CEDULA");//JJCastro fase2
            if (con != null){
                st = con.prepareStatement(this.obtenerSQL("SQL_CONSULTA_CEDULA"));
                st.setString(1,cedula);
                rs = st.executeQuery();
                if (rs.next()){
                    sw=1;
                }

                if (estado.equals("011")){
                    st = con.prepareStatement(this.obtenerSQL("SQL_ACT_CEDULA"));
                    st.setString(1, "S");
                    st.setString(2, cedula);
                }
                else if (estado.equals("024")){
                    st = con.prepareStatement(this.obtenerSQL("SQL_ACT_LIBMILITAR"));
                    st.setString(1, "S");
                    st.setString(2, cedula);
                }
                else if (estado.equals("025")){
                    st = con.prepareStatement(this.obtenerSQL("SQL_ACT_JUDICIAL"));
                    st.setString(1, "S");
                    st.setString(2, cedula);
                }
                else if (estado.equals("027")){
                    st = con.prepareStatement(this.obtenerSQL("SQL_ACT_EPS"));
                    st.setString(1, "S");
                    st.setString(2, cedula);
                }
                else if (estado.equals("028")){
                    st = con.prepareStatement(this.obtenerSQL("SQL_ACT_ARP"));
                    st.setString(1, "S");
                    st.setString(2, cedula);
                }
                else if (estado.equals("023")){
                    st = con.prepareStatement(this.obtenerSQL("SQL_ACT_CATEGORIAPASE"));
                    st.setString(1, "S");
                    st.setString(2, cedula);
                }
                else if (estado.equals("029")){
                    st = con.prepareStatement(this.obtenerSQL("SQL_ACT_LIBTRIPULANTE"));
                    st.setString(1, "S");
                    st.setString(2, cedula);
                }
                else if (estado.equals("030")){
                    st = con.prepareStatement(this.obtenerSQL("SQL_ACT_PASAPORTE"));
                    st.setString(1, "S");
                    st.setString(2, cedula);
                }
                else if (estado.equals("026")){
                    st = con.prepareStatement(this.obtenerSQL("SQL_ACT_VISA"));
                    st.setString(1, "S");
                    st.setString(2, cedula);
                }
                else if ( estado.substring(0,1).equals("D") ){
                    st = con.prepareStatement(this.obtenerSQL("SQL_HUELLA_DER"));
                    st.setString(1, estado);
                    st.setString(2, cedula);
                }
                else if ( estado.substring(0,1).equals("I") ){
                    st = con.prepareStatement(this.obtenerSQL("SQL_HUELLA_IZQ"));
                    st.setString(1, estado);
                    st.setString(2, cedula);
                }
                else{
                    sw1=1;
                }
                
                if ( (sw == 1) && (sw1 == 0) ){
                    st.executeUpdate();
                }
                else{
                    consultasConductor="  "+consultasConductor+st;
                }
                ////System.out.println("Modificar "+st);
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL VERIFICAR ACTUALIZAR LOS DOCUMENTOS" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }                                           


 /**
  * 
  * @param placa
  * @param estado
  * @throws SQLException
  */
    public void tieneDocumentosPlaca(String placa, String estado  ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        int sw = 0, sw1=0;
        
        try{
            con = this.conectarJNDI("SQL_CONSULTA_PLACA");
            if (con != null){
                st = con.prepareStatement(this.obtenerSQL("SQL_CONSULTA_PLACA"));
                st.setString(1,placa);
                rs = st.executeQuery();
                if (rs.next()){
                    sw=1;
                }

                if (estado.equals("014")){
                    st = con.prepareStatement(this.obtenerSQL("SQL_ACT_TARPROBCAB"));
                    st.setString(1, "S");
                    st.setString(2, placa);
                }
                else if (estado.equals("021")){
                    st = con.prepareStatement(this.obtenerSQL("SQL_ACT_COPIACONTCOMVENTA"));
                    st.setString(1, "S");
                    st.setString(2, placa);
                }
                else if (estado.equals("022")){
                    st = con.prepareStatement(this.obtenerSQL("SQL_ACT_COPIACONTARR"));
                    st.setString(1, "S");
                    st.setString(2, placa);
                }
                else if (estado.equals("033")){
                    st = con.prepareStatement(this.obtenerSQL("SQL_ACT_TARPROPSEMI"));
                    st.setString(1, "S");
                    st.setString(2, placa);
                }
                else if (estado.equals("017")){
                    st = con.prepareStatement(this.obtenerSQL("SQL_ACT_CERTEMGASES"));
                    st.setString(1, "S");
                    st.setString(2, placa);
                }
                else if (estado.equals("019")){
                    st = con.prepareStatement(this.obtenerSQL("SQL_ACT_SOAT"));
                    st.setString(1, "S");
                    st.setString(2, placa);
                }
                else if (estado.equals("020")){
                    st = con.prepareStatement(this.obtenerSQL("SQL_ACT_REGNALCARGA"));
                    st.setString(1, "S");
                    st.setString(2, placa);
                }
                else if (estado.equals("034")){
                    st = con.prepareStatement(this.obtenerSQL("SQL_ACT_REGNALSEMIRE"));
                    st.setString(1, "S");
                    st.setString(2, placa);
                }
                else if (estado.equals("018")){
                    st = con.prepareStatement(this.obtenerSQL("SQL_ACT_TAREMPRESARIAL"));
                    st.setString(1, "S");
                    st.setString(2, placa);
                }
                else if (estado.equals("015")){
                    st = con.prepareStatement(this.obtenerSQL("SQL_ACT_TARHABIL"));
                    st.setString(1, "S");
                    st.setString(2, placa);
                }
                else if (estado.equals("016")){
                    st = con.prepareStatement(this.obtenerSQL("SQL_ACT_POLIZAANDINA"));
                    st.setString(1, "S");
                    st.setString(2, placa);
                }
                else{
                    sw1=1;
                }
                if ( (sw == 1) && (sw1 == 0) ){
                    ////System.out.println("Modificar "+st);
                    st.executeUpdate();
                }
                else{
                    consultasPlaca="  "+consultasPlaca+st;
                }
                
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL ACTUALIZAR LA PLACA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


 /**
  * 
  * @param estado
  * @throws SQLException
  */
    public void Verificar(String estado) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        
        try{
            con = this.conectarJNDI("SQL_VERIFICARPLACA");
            if (con != null){
                if (estado.equals("Placa")){
                    st = con.prepareStatement(this.obtenerSQL("SQL_VERIFICARPLACA"));
                    st.setString(1, veridoc.getUsuarioverica());
                    st.setString(2, veridoc.getFechavericacion());
                   st.setString(3, veridoc.getPlaca());
                }
                else{
                    st = con.prepareStatement(this.obtenerSQL("SQL_VERIFICARCONDUCTOR"));
                    st.setString(1, veridoc.getUsuarioverica());
                    st.setString(2, veridoc.getFechavericacion());
                    st.setString(3, veridoc.getCedula());
                }

                ////System.out.println("Verificar "+st);
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL VERIFICAR LOS DOCUMENTOS" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


 /**
  * 
  * @param act
  * @throws SQLException
  */
      public void AgregarDocumentos(String act ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        int sw =0;

        try{
              con = this.conectarBDJNDI("fintra");//JJCastro fase2
            if (con != null){
                if (act.equalsIgnoreCase("Conductor")){
                   
                    if(!consultasConductor.trim().equals("")){
                        rs = st.executeQuery(consultasConductor);
                        
                    }
                    consultasConductor="";
                }
                else{                    
                    if(!consultasPlaca.trim().equals("")){
                        rs = st.executeQuery(consultasPlaca);
                                              
                    }

                    consultasPlaca="";
                }


                ////System.out.println("Actualizando "+st);
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL ACTUALIZAR LOS CAMPOS DE DOCUMENTOS " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
}
