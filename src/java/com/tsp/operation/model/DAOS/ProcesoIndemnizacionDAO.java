/*
 * Nombre        ProcesoIndemnizacionDAO.java
 * Descripci�n   Clase para el acceso a los datos del programa generar facturas de aval
 * Autor         Iris vargas
 * Fecha         28 de abril de 2012, 12:08 PM
 * Versi�n       1.0
 * Coyright      Geotech S.A.
 */
package com.tsp.operation.model.DAOS;

import com.tsp.finanzas.contab.model.beans.Comprobantes;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.StringStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase para el acceso a los datos de negocios_trazabilidad
 * @author Iris Vargas
 */
public class ProcesoIndemnizacionDAO extends MainDAO {

    public ProcesoIndemnizacionDAO() {
        super("ProcesoIndemnizacionDAO.xml");
    }   
    public ProcesoIndemnizacionDAO(String dataBaseName) {
        super("ProcesoIndemnizacionDAO.xml", dataBaseName);
    }   
    

    /**
     * M�todos que setea valores nulos
     * @autor.......ivargas
     * @version.....1.0.
     **/
    private String reset(String val) {
        if (val == null) {
            val = "";
        }
        return val;
    }

     public Comprobantes buscarNegocio(String negocio, String num_titulo) throws java.lang.Exception {
        Connection con = null;
        Comprobantes comprobante = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_COMPROBANTE_TIT";

        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, negocio);
            st.setString(2, num_titulo);
            rs = st.executeQuery();
            if (rs.next()) {
                comprobante = loadComprobantes(rs);
            }

        } catch (java.lang.Exception e) {
            throw new java.lang.Exception("[ProcesoIndemnizacionDAO.buscarnegocio]: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return comprobante;
    }

      public  String getAccountDES(String oc) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_DESC_CTA";
        String             cta     = "";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, oc  );
            st.setString(2, "FINV");
            rs = st.executeQuery();
            if( rs.next() )
                cta  =  reset( rs.getString("nombre_largo")  );

        }catch(Exception e){
            throw new Exception( "getAccountBCO_Mal " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return cta;
    }    

    public String ingresarCXC(BeanGeneral bg) throws SQLException {
        String query = "SQL_INSERTAR_CXC";
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, "FAC"); //tipo documento
            st.setString(2,bg.getValor_07()); //numero de documento
            st.setString(3, bg.getValor_02()); //nit
            st.setString(4, bg.getValor_02()); //nit para obtener el codcli
            st.setString(5, bg.getValor_13()); //concepto
            st.setString(6, bg.getValor_03()); //Fecha factura
            st.setString(7, bg.getValor_14()); //fecha_vencimiento
            st.setString(8, bg.getValor_01()); //descripcion document_type
            st.setString(9, bg.getValor_06()); //valor_factura
            st.setInt(10, 1); //valor_tasa
            st.setString(11, "PES"); //moneda
            st.setInt(12, 1); //cantidad_items
            st.setString(13, "CREDITO"); //forma_pago
            st.setString(14, "OP"); //agencia_facturacion
            st.setString(15, "BQ"); //agencia_cobro
            st.setString(16, "ADMIN"); //creation_user
            st.setString(17, bg.getValor_06()); //valor_facturame
            st.setString(18, bg.getValor_06()); //valor_saldo
            st.setString(19, bg.getValor_06()); //valor_saldome
            st.setString(20, bg.getValor_11()); //negasoc
            st.setString(21, "COL"); //base
            st.setString(22, ""); //num_doc_fen
            st.setString(23, ""); //tipo_ref1
            st.setString(24, ""); //ref1
            st.setString(25, bg.getValor_04()); //cmc
            st.setString(26, bg.getValor_05()); //dstrct
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarCXC[ProcesoIndemnizacionDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String ingresarDetalleCXC(BeanGeneral bg, String item) throws SQLException {
        String query = "SQL_INSERTAR_DET_CXC";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, "FAC"); //tipo_documento
            st.setString(2, bg.getValor_07()); //numero de documento
            st.setString(3, item); //item
            st.setString(4, bg.getValor_02()); //nit
            st.setString(5, bg.getValor_13()); //concepto
            st.setString(6, bg.getValor_10()); //descripcion
            st.setInt(7, 1); //cantidad
            st.setString(8, bg.getValor_06() ); //valor_unitario
            st.setString(9, bg.getValor_06()); //valor_item
            st.setInt(10, 1); //valor_tasa
            st.setString(11, "PES"); //moneda
            st.setString(12, bg.getValor_08()); //creation_user
            st.setString(13, bg.getValor_06()); //valor_unitariome
            st.setString(14, bg.getValor_06()); //valor_itemme
            st.setString(15,  bg.getValor_11()); //numero_remesa
            st.setString(16, "COL"); //base
            st.setString(17, "RD-" + bg.getValor_02()); //auxiliar
            st.setString(18, bg.getValor_09()); //Cuenta (codigo_cuenta_contable)
            st.setString(19, bg.getValor_05()); //Distrito
            st.setString(20, bg.getValor_07()); //documento relacionado
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarDetalleCXC[ProcesoIndemnizacionDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String ingresarCXP(BeanGeneral bg) throws SQLException {
        String query = "SQL_INSERTAR_CXP";
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            NegociosDAO dao= new NegociosDAO(this.getDatabaseName());
            st.setString(1, bg.getValor_02()); //proveedor
            st.setString(2, "FAP");//tipo_documento
            st.setString(3, bg.getValor_07()); //documento
            st.setString(4, bg.getValor_02()); //descripcion (Se envia el nit del afiliado para colocarlo en la descripcion)
            st.setString(5, "OP"); //agencia
            st.setString(6, bg.getValor_02()); //banco (Se envia el nit del afiliado)
            st.setString(7, bg.getValor_02()); //sucursal (Se envia el nit del afiliado)
            st.setDouble(8, Double.parseDouble(bg.getValor_06()));//vlr_neto
            st.setDouble(9, Double.parseDouble(bg.getValor_06()));//vlr_saldo
            st.setDouble(10, Double.parseDouble(bg.getValor_06()));//vlr_neto_me
            st.setDouble(11, Double.parseDouble(bg.getValor_06()));//vlr_saldo_me
            st.setInt(12, 1);//tasa
            st.setString(13, bg.getValor_03());//usuario creacion
            st.setString(14, bg.getValor_12());//base
            st.setString(15, "PES");//moneda_banco
            st.setString(16, "");//clase_documento_rel
            st.setString(17, "PES");//moneda
            st.setString(18, "NEG");//tipo_documento_rel
            st.setString(19,  bg.getValor_11());//documento_relacionado
            st.setString(20, bg.getValor_04()); //handle_code
            st.setString(21, bg.getValor_05());//Distrito
            st.setString(22, dao.aprobadorCxPNeg());//aprobador
            st.setString(23, bg.getValor_08());//usuario aprobacion
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarCXP[ProcesoIndemnizacionDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String ingresarDetalleCXP(BeanGeneral bg) throws SQLException {
        String query = "INSERTAR_DETALLE_CXP";
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, bg.getValor_02()); //PROVEEDOR
            st.setString(2, "FAP"); //tipo_documento
            st.setString(3, bg.getValor_07()); //documento
            st.setString(4, "1"); //item
            st.setString(5, "CXP A " + bg.getValor_02()); //descripcion
            st.setDouble(6, Double.parseDouble(bg.getValor_06()));//vlr
            st.setDouble(7, Double.parseDouble(bg.getValor_06()));//vlr_me
            st.setString(8, bg.getValor_09());//codigo_cuenta
            st.setString(9, bg.getValor_11());//planilla
            st.setString(10, bg.getValor_08());//creation_user
            st.setString(11, bg.getValor_12());//base
            st.setString(12, "AR-" + bg.getValor_02());//AUXILIAR
            st.setString(13, bg.getValor_05());//Distrito
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarDetalleCXP[ProcesoIndemnizacionDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

     /**
     * M�todo que los negocios que ya tienen vencido el plazo para indemnizar
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List getNegociosVencidaIndemnizacion(String fecha) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        List               lista   = new ArrayList();
        String             query   = "SRC_VENCIDO_INDEMNIZACION";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, fecha);
            st.setString(2, fecha);
            rs = st.executeQuery();
            while (rs.next()){
                Comprobantes  comprobante = loadComprobantes(rs);
                lista.add( comprobante );
                comprobante = null;
            }

        }catch(Exception e){
            throw new Exception( "getNegociosVencidaIndemnizacion " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lista;
    }

     /**
     * M�todo que carga los datos de los negocios
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  Comprobantes loadComprobantes(ResultSet  rs)throws Exception{
        Comprobantes  comp  = new  Comprobantes();
        try{
           comp.setAbc      ("N/A");
           comp.setBase     ("COL");
           comp.setDstrct   (rs.getString("dist"));
           comp.setTipodoc  ("NEG");
           comp.setNumdoc   (rs.getString("cod_neg"));
           comp.setTercero  (rs.getString("cod_cli"));
           comp.setTotal_debito(Double.valueOf(rs.getString("tot_pagado")).doubleValue());
           comp.setTotal_credito(Double.valueOf(rs.getString("vr_desembolso")).doubleValue());
           comp.setMoneda   ("PES");
           comp.setRef_1    (reset(rs.getString("nit_tercero")));//nit del proveedor
           comp.setAuxiliar ("RD-"+rs.getString("cod_cli"));
           comp.setFecha_creacion(reset(rs.getString("fecha_ap")));
           comp.setDetalle  ("NEGOCIO No "+rs.getString("cod_neg"));
           comp.setRef_2(rs.getString("NOMB"));
           comp.setRef_4    (rs.getInt("numero_solicitud"));
           comp.setComentario    (rs.getString("num_titulo"));
           comp.setRef_5    (rs.getInt("indice"));
           comp.setSucursal ("BQ");
           comp.setCustodia (rs.getDouble("vr_custodia"));
           comp.setRem      (rs.getDouble("valor_remesa"));
           comp.setModrem   (rs.getString("mod_remesa"));
           comp.setCmc      (rs.getString("cmc"));
           comp.setTpr      (rs.getDouble("porterem"));
           comp.setTdesc    (rs.getDouble("tdescuento"));
           comp.setRef_3(reset(rs.getString("fecha_ap")));//Mod TMOLINA 03-Enero-2008
           comp.setVlr_aval(rs.getDouble("valor_aval"));
           comp.setId_convenio(rs.getInt("id_convenio"));
           comp.setId_remesa(rs.getInt("id_remesa"));
           comp.setTipoNegocio(rs.getString("tneg"));
           comp.setValor(rs.getDouble("valor"));
           comp.setFecha_aplicacion(reset(rs.getString("fecha")));
        }catch(Exception e){
            throw new Exception( "loadComprobantes " + e.getMessage());
        }
        return comp;
    }

}
