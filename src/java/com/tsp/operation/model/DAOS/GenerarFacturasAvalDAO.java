/*
 * Nombre        GenerarFacturasAvalDAO.java
 * Descripci�n   Clase para el acceso a los datos del programa generar facturas de aval
 * Autor         Iris vargas
 * Fecha         28 de abril de 2012, 12:08 PM
 * Versi�n       1.0
 * Coyright      Geotech S.A.
 */
package com.tsp.operation.model.DAOS;

import com.tsp.finanzas.contab.model.beans.Comprobantes;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.StringStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Clase para el acceso a los datos de negocios_trazabilidad
 * @author Iris Vargas
 */
public class GenerarFacturasAvalDAO extends MainDAO {

    public GenerarFacturasAvalDAO() {
        super("GenerarFacturasAvalDAO.xml");
    }
    public GenerarFacturasAvalDAO(String dataBaseName) {
        super("GenerarFacturasAvalDAO.xml", dataBaseName);
    }

    /**
     * Sql con el listado de convenios segun campo anombre
     * @return sql con el listado de convenios segun campo anombre
     * @param anombre  indica si el convenio esta o no anombre de un tercero
     * @throws Exception cuando hay error
     */
    public ArrayList<String> buscarConveniosAnombre(boolean anombre) throws Exception {
        ArrayList<String> lista = new ArrayList<String>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_CONVENIOS_ANOMBRE";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setBoolean(1, anombre);
            rs = ps.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("id_convenio") + ";_;" + rs.getString("nombre"));
            }
        } catch (Exception e) {
            System.out.println("Error en buscarConveniosAnombre: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error buscarConveniosAnombre: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

     /**
     * Sql con el listado de convenios segun el avalista
     * @return sql con el listado de convenios segun el avalista
     * @param avalista  indica el nit del avalista
     * @throws Exception cuando hay error
     */
    public ArrayList<String> buscarConveniosAvalista(String avalista) throws Exception {
        ArrayList<String> lista = new ArrayList<String>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_CONVENIOS_AVALISTA";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, avalista);
            rs = ps.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("id_convenio") + ";_;" + rs.getString("nombre"));
            }
        } catch (Exception e) {
            System.out.println("Error en buscarConveniosAvalista: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error buscarConveniosAvalista: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

     /**
     * Sql con el listado de afiliados segun convenio
     * @return sql con el listado de afiliados segun convenio
     * @param convenio 
     * @throws Exception cuando hay error
     */
    public ArrayList<String> buscarAfiliadosConvenio(int convenio) throws Exception {
        ArrayList<String> lista = new ArrayList<String>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_AFILIADOS_CONVENIO";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, convenio);
            rs = ps.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("nit") + ";_;" + rs.getString("payment_name"));
            }
        } catch (Exception e) {
            System.out.println("Error en buscarAfiliadosConvenio: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error buscarAfiliadosConvenio: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

     /**
     * Sql con el listado de Avalistas
     * @return sql con el listado de Avalistas
     * @throws Exception cuando hay error
     */
    public ArrayList<String> buscarAvalistas() throws Exception {
        ArrayList<String> lista = new ArrayList<String>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_AVALISTAS";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("nit") + ";_;" + rs.getString("avalista"));
            }
        } catch (Exception e) {
            System.out.println("Error en buscarAvalistas: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error buscarAvalistas: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

     /**
     * Sql con el listado de negocios a generar cxc segun filtros
     * @return sql con el listado de negocios a generar cxc segun filtros
     * @param fechaini
     * @param fechafin
     * @param convenio
     * @param anombre
     * @param afiliado
     * @throws Exception cuando hay error
     */
    public ArrayList<BeanGeneral> buscarNegocios(String fechaini,String fechafin, int convenio,boolean anombre, String afiliado ) throws Exception {
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_NEGOCIOS_CXC";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            String where="";
            if(!afiliado.equals("")){
                where+=" AND n.nit_tercero='"+afiliado+"'";
            }
           
            sql = sql.replaceAll("#WHERE#", where);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1,fechaini);
            ps.setString(2,fechafin);
            ps.setBoolean(3, anombre);
            ps.setInt(4, convenio);
            rs = ps.executeQuery();
            while (rs.next()) {
                BeanGeneral bean = new BeanGeneral();
                bean.setValor_01(rs.getString("convenio"));
                bean.setValor_02(rs.getString("nit_tercero"));
                bean.setValor_03(rs.getString("payment_name"));
                bean.setValor_04(rs.getString("cod_neg"));
                bean.setValor_05(rs.getString("fecha_negocio"));
                bean.setValor_06(rs.getString("vr_negocio"));
                bean.setValor_07(rs.getString("valor_aval"));
                bean.setValor_08(rs.getString("cod_cli"));
                bean.setValor_09(rs.getString("cliente"));
                lista.add(bean);
            }
        } catch (Exception e) {
            System.out.println("Error en buscarNegocios: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error buscarNegocios: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    /**
     * Sql con el listado de negocios a generar cxp segun filtros
     * @return sql con el listado de negocios a generar cxp segun filtros
     * @param fechaini
     * @param fechafin
     * @param convenio
     * @throws Exception cuando hay error
     */
    public ArrayList<BeanGeneral> buscarNegocios(String fechaini,String fechafin, int convenio ) throws Exception {
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_NEGOCIOS_CXP";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);

            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1,fechaini);
            ps.setString(2,fechafin);
            ps.setInt(3, convenio);
            rs = ps.executeQuery();
            while (rs.next()) {
                BeanGeneral bean = new BeanGeneral();
                bean.setValor_01(rs.getString("convenio"));
                bean.setValor_02(rs.getString("nit"));
                bean.setValor_03(rs.getString("avalista"));
                bean.setValor_04(rs.getString("cod_neg"));
                bean.setValor_05(rs.getString("fecha_negocio"));
                bean.setValor_06(rs.getString("vr_negocio"));
                bean.setValor_07(rs.getString("valor_aval"));
                bean.setValor_08(rs.getString("cod_cli"));
                bean.setValor_09(rs.getString("cliente"));
                lista.add(bean);
            }
        } catch (Exception e) {
            System.out.println("Error en buscarNegocios: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error buscarNegocios: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

     /**
     * Sql con el listado de afiliados
     * @return sql con el listado de negocios a generar cxc segun filtros
     * @param fechaini
     * @param fechafin
     * @param convenio
     * @throws Exception cuando hay error
     */
    public ArrayList<String> buscarAfiliados(String fechaini,String fechafin, int convenio) throws Exception {
        ArrayList<String> lista = new ArrayList<String>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_AFILIADOS";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);

            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1,fechaini);
            ps.setString(2,fechafin);
            ps.setInt(3, convenio);
            rs = ps.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("nit_tercero"));
            }
        } catch (Exception e) {
            System.out.println("Error en buscarAfiliados: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error buscarAfiliados: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    /**
     * M�todo que carga los datos de los negocios
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public Comprobantes loadComprobantes(ResultSet rs) throws Exception {

        Comprobantes comp = new Comprobantes();
        try {
            comp.setDstrct(rs.getString("dist"));
            comp.setNumdoc(rs.getString("cod_neg"));
            comp.setTercero(rs.getString("cod_cli"));
            comp.setRef_1(reset(rs.getString("nit_tercero")));//nit del proveedor
            comp.setAuxiliar("RD-" + rs.getString("cod_cli"));
            comp.setFecha_creacion(reset(rs.getString("fecha_negocio")));
            comp.setDetalle("Descargue comision Aval No " + rs.getString("cod_neg"));
            comp.setRef_2(rs.getString("NOMB"));
            comp.setRef_5(rs.getInt("nro_docs"));
            comp.setCustodia(rs.getDouble("vr_custodia"));
            comp.setRem(rs.getDouble("valor_remesa"));
            comp.setModrem(rs.getString("mod_remesa"));
            comp.setCmc(rs.getString("cmc"));
            comp.setTpr(rs.getDouble("porterem"));
            comp.setTdesc(rs.getDouble("tdescuento"));
            comp.setRef_3(reset(rs.getString("fecha_ap")));
            comp.setVlr_aval(rs.getDouble("valor_aval"));
            comp.setId_convenio(rs.getInt("id_convenio"));
            comp.setId_remesa(rs.getInt("id_remesa"));
            comp.setTipoNegocio(rs.getString("tneg"));
        } catch (Exception e) {
            throw new Exception("loadComprobantes" + e.getMessage());
        }
        return comp;
    }

    /**
     * M�todos que setea valores nulos
     * @autor.......ivargas
     * @version.....1.0.
     **/
    private String reset(String val) {
        if (val == null) {
            val = "";
        }
        return val;
    }

     public Comprobantes buscarNegocio(String negocio, String dstrct) throws java.lang.Exception {
        Connection con = null;
        Comprobantes comprobante = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_COMPROBANTE_AVAL";

        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, negocio);
            st.setString(2, dstrct);
            rs = st.executeQuery();
            if (rs.next()) {
                comprobante = loadComprobantes(rs);
            }

        } catch (java.lang.Exception e) {
            throw new java.lang.Exception("[GenerarFacturasAvalDAO.buscarnegocio]: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return comprobante;
    }

      public  String getAccountDES(String oc) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_DESC_CTA";
        String             cta     = "";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, oc  );
            st.setString(2, "FINV");
            rs = st.executeQuery();
            if( rs.next() )
                cta  =  reset( rs.getString("nombre_largo")  );

        }catch(Exception e){
            throw new Exception( "getAccountBCO_Mal " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return cta;
    }    

    public String ingresarCXCComisionAval(BeanGeneral bg) throws SQLException {
        String query = "SQL_INSERTAR_CXC_COMISION_AVAL";
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, "FAC"); //tipo documento
            st.setString(2,bg.getValor_07()); //numero de documento
            st.setString(3, bg.getValor_02()); //nit
            st.setString(4, bg.getValor_02()); //nit para obtener el codcli
            st.setString(5, ""); //concepto
            st.setString(6, bg.getValor_03()); //Fecha factura
            st.setString(7, bg.getValor_03()); //fecha_vencimiento
            st.setString(8, bg.getValor_01()); //descripcion document_type
            st.setString(9, bg.getValor_06()); //valor_factura
            st.setInt(10, 1); //valor_tasa
            st.setString(11, "PES"); //moneda
            st.setInt(12, 1); //cantidad_items
            st.setString(13, "CREDITO"); //forma_pago
            st.setString(14, "OP"); //agencia_facturacion
            st.setString(15, "BQ"); //agencia_cobro
            st.setString(16, "ADMIN"); //creation_user
            st.setString(17, bg.getValor_06()); //valor_facturame
            st.setString(18, bg.getValor_06()); //valor_saldo
            st.setString(19, bg.getValor_06()); //valor_saldome
            st.setString(20, ""); //negasoc
            st.setString(21, "COL"); //base
            st.setString(22, ""); //num_doc_fen
            st.setString(23, ""); //tipo_ref1
            st.setString(24, ""); //ref1
            st.setString(25, bg.getValor_04()); //cmc
            st.setString(26, bg.getValor_05()); //dstrct
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarCXCComisionAval[GenerarFacturasAvalDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String ingresarDetalleCXCComisionAval(BeanGeneral bg, String item) throws SQLException {
        String query = "SQL_INSERTAR_DET_CXC_COMISION_AVAL";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, "FAC"); //tipo_documento
            st.setString(2, bg.getValor_07()); //numero de documento
            st.setString(3, item); //item
            st.setString(4, bg.getValor_02()); //nit
            st.setString(5, ""); //concepto
            st.setString(6, bg.getValor_10()); //descripcion
            st.setInt(7, 1); //cantidad
            st.setString(8, bg.getValor_06() ); //valor_unitario
            st.setString(9, bg.getValor_06()); //valor_item
            st.setInt(10, 1); //valor_tasa
            st.setString(11, "PES"); //moneda
            st.setString(12, bg.getValor_08()); //creation_user
            st.setString(13, bg.getValor_06()); //valor_unitariome
            st.setString(14, bg.getValor_06()); //valor_itemme
            st.setString(15,  bg.getValor_11()); //numero_remesa
            st.setString(16, "COL"); //base
            st.setString(17, "RD-" + bg.getValor_02()); //auxiliar
            st.setString(18, bg.getValor_09()); //Cuenta (codigo_cuenta_contable)
            st.setString(19, bg.getValor_05()); //Distrito
            st.setString(20, bg.getValor_11()); //documento relacionado
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarDetalleCXCComisionAval[GenerarFacturasAvalDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String ingresarCXPAvalista(BeanGeneral bg) throws SQLException {
        String query = "SQL_INSERTAR_CXP";
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            NegociosDAO dao= new NegociosDAO(this.getDatabaseName());
            st.setString(1, bg.getValor_02()); //proveedor
            st.setString(2, "FAP");//tipo_documento
            st.setString(3, bg.getValor_04()); //documento
            st.setString(4, bg.getValor_02()); //descripcion (Se envia el nit del afiliado para colocarlo en la descripcion)
            st.setString(5, "OP"); //agencia
            st.setString(6, bg.getValor_02()); //banco (Se envia el nit del afiliado)
            st.setString(7, bg.getValor_02()); //sucursal (Se envia el nit del afiliado)
            st.setDouble(8, Double.parseDouble(bg.getValor_09()));//vlr_neto
            st.setDouble(9, Double.parseDouble(bg.getValor_09()));//vlr_saldo
            st.setDouble(10, Double.parseDouble(bg.getValor_09()));//vlr_neto_me
            st.setDouble(11, Double.parseDouble(bg.getValor_09()));//vlr_saldo_me
            st.setInt(12, 1);//tasa
            st.setString(13, bg.getValor_03());//usuario creacion
            st.setString(14, bg.getValor_05());//base
            st.setString(15, "PES");//moneda_banco
            st.setString(16, "");//clase_documento_rel
            st.setString(17, "PES");//moneda
            st.setString(18, "");//tipo_documento_rel
            st.setString(19, "");//documento_relacionado
            st.setString(20, bg.getValor_06()); //handle_code
            st.setString(21, bg.getValor_08());//Distrito
            st.setString(22, dao.aprobadorCxPNeg());//aprobador
            st.setString(23, bg.getValor_03());//usuario aprobacion
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarCXPAvalista[GenerarFacturasAvalDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String ingresarDetalleCXPAvalista(BeanGeneral bg) throws SQLException {
        String query = "INSERTAR_DETALLE_CXP";
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, bg.getValor_02()); //PROVEEDOR
            st.setString(2, "FAP"); //tipo_documento
            st.setString(3, bg.getValor_04()); //documento
            st.setString(4, "1"); //item
            st.setString(5, "CXP COMISION AVAL " + bg.getValor_02()); //descripcion
            st.setDouble(6, Double.parseDouble(bg.getValor_09()));//vlr
            st.setDouble(7, Double.parseDouble(bg.getValor_09()));//vlr_me
            st.setString(8, bg.getValor_07());//codigo_cuenta
            st.setString(9, bg.getValor_10());//planilla
            st.setString(10, bg.getValor_03());//creation_user
            st.setString(11, bg.getValor_05());//base
            st.setString(12, "AR-" + bg.getValor_02());//AUXILIAR
            st.setString(13, bg.getValor_08());//Distrito
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarDetalleCXPAvalista[GenerarFacturasAvalDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

}
