/****************************************************************************
 * Nombre Clase ................... TblEstadoDAO.java
 * Descripci�n .................... Data Acces Object para la clase TblEstado
 * Autor .......................... Tito Andr�s Maturana
 * Versi�n ........................ 1.0
 * Fecha .......................... 4 de octubre de 2005, 05:44 PM
 * Copyright....................... Transportes S�nchez Polo S.A.
 ***************************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  Andres
 */
public class TblEstadoDAO {
        
        private static String SQL_INSERT = "insert into tbl_estado(tipo, codestado, descripcion, color, fondo," +
                "color_letra, imagen, creation_date, creation_user, reg_status, dstrct, base, filename) " +
                "values(?,?,?,?,?,?,?,'now()',?,'',?,?,?)";
        
        private static String SQL_OBTENER = "select * from tbl_estado where tipo=? and codestado=? and reg_status<>'A'";
        
        private static String SQL_EXISTE = "select * from tbl_estado where tipo=? and codestado=?";
        
        private static String SQL_LISTAR = "select * from tbl_estado where reg_status<>'A'";
        
        private static String SQL_UPDATE = "update tbl_estado set imagen=?, last_update = 'now()', user_update=?, filename=? " +
                " where tipo=? and codestado=?";
        
        private static String SQL_UPDATE2 = "update tbl_estado set tipo=?, codestado=?, descripcion=?, color=?, fondo=?," +
                "color_letra=?, last_update='now', user_update=? where tipo=? and codestado=? ";
        
        private static String SQL_ANULAR = "update tbl_estado set reg_status='A', last_update='now', user_update=?" +
                " where tipo=? and codestado=?"; 
        
        
        /** Creates a new instance of TblEstadoDAO */
        
        private TblEstado estado;
        private Vector estados;
        
        public TblEstadoDAO() {
        }
        
        public void insertEstado(String filename, ByteArrayInputStream bfin, int longitud,  Dictionary fields, 
                String contentType, String usuario, String agencia, String base) throws SQLException {
                        
                PoolManager poolManager = PoolManager.getInstance();
                Connection conPostgres  = poolManager.getConnection("fintra");
                if (conPostgres == null)
                        throw new SQLException("Sin conexion");
                PreparedStatement  st    = null;   
                try{
                        st= conPostgres.prepareStatement(this.SQL_INSERT);

                        String tipo = (String)fields.get("c_tipo");  
                        String codestado = (String)fields.get("c_codestado");  
                        String color = (String)fields.get("c_color");  
                        String fondo =  (String)fields.get("c_fondo"); 
                        String color_letra = (String)fields.get("c_color_letra"); 
                        String descripcion = (String)fields.get("c_descripcion");  

                        st.setString(1, tipo);        
                        st.setString(2, codestado.toUpperCase());
                        st.setString(3, descripcion);
                        st.setString(4, color);
                        st.setString(5, fondo);
                        st.setString(6, color_letra);
                        st.setBinaryStream(7, bfin, longitud);
                        st.setString(8, usuario);
                        st.setString(9, agencia);
                        st.setString(10, base);
                        st.setString(11, filename);


                        st.execute();
                }catch(Exception e){
                    throw new SQLException("Error insertImagen() : "+ e.getMessage());
                }
                finally{
                    if(st!=null) st.close();
                    poolManager.freeConnection("fintra",conPostgres);
                }
        }
        
        
        public void updateEstado(String filename, ByteArrayInputStream bfin, int longitud,  Dictionary fields, 
                String contentType, String usuario) throws SQLException {
                        
                PoolManager poolManager = PoolManager.getInstance();
                Connection conPostgres  = poolManager.getConnection("fintra");
                if (conPostgres == null)
                        throw new SQLException("Sin conexion");
                PreparedStatement  st    = null;   
                try{
                        st= conPostgres.prepareStatement(this.SQL_UPDATE);
                        
                        String tipo = (String)fields.get("c_tipo");  
                        String codestado = (String)fields.get("c_codestado");  
                        
                        st.setBinaryStream(1, bfin, longitud);
                        st.setString(2, usuario);
                        st.setString(3, filename);
                        st.setString(4, tipo);        
                        st.setString(5, codestado.toUpperCase());


                        st.execute();
                        
                        ////System.out.println("se actulizo el registro : tipo= "+ tipo + " , codestado= " + codestado);
                }catch(Exception e){
                    throw new SQLException("Error insertImagen() : "+ e.getMessage());
                }
                finally{
                    if(st!=null) st.close();
                    poolManager.freeConnection("fintra",conPostgres);
                }
        }
        
        public void updateEstado(String tipo, String codestado) throws SQLException{
                
                PoolManager poolManager = PoolManager.getInstance();
                Connection conPostgres  = poolManager.getConnection("fintra");
                if (conPostgres == null)
                        throw new SQLException("Sin conexion");
                PreparedStatement  st    = null;   
                try{
                        st= conPostgres.prepareStatement(this.SQL_UPDATE2);
                        
                        st.setString(1, this.estado.getTipo());
                        st.setString(2, this.estado.getCodestado().toUpperCase());
                        st.setString(3, this.estado.getDescripcion());
                        st.setString(4, this.estado.getColor());
                        st.setString(5, this.estado.getFondo());
                        st.setString(6, this.estado.getColor_letra());     
                        st.setString(7, this.estado.getUsuario_modificacion());
                        st.setString(8, tipo);
                        st.setString(9, codestado.toUpperCase());


                        st.execute();
                }catch(Exception e){
                    throw new SQLException("ERROR AL ACTUALIZAR EL CODIGO DE ESTADO. "+ e.getMessage());
                }
                finally{
                    if(st!=null) st.close();
                    poolManager.freeConnection("fintra",conPostgres);
                }
        
        }
        
        public void anularEstado(String tipo, String codestado, String user) throws SQLException{
                
                PoolManager poolManager = PoolManager.getInstance();
                Connection conPostgres  = poolManager.getConnection("fintra");
                if (conPostgres == null)
                        throw new SQLException("Sin conexion");
                PreparedStatement  st    = null;   
                try{
                        st= conPostgres.prepareStatement(this.SQL_ANULAR);                      
                        
                        st.setString(1, user);
                        st.setString(2, tipo);
                        st.setString(3, codestado.toUpperCase());


                        st.execute();
                }catch(Exception e){
                    throw new SQLException("ERROR AL ANULAR EL CODIGO DE ESTADO. "+ e.getMessage());
                }
                finally{
                    if(st!=null) st.close();
                    poolManager.freeConnection("fintra",conPostgres);
                }
        
        }
        
        
        
        public void obtenerEstado(String tipo, String codestado, String ruta) throws SQLException {
                        
                PoolManager poolManager = PoolManager.getInstance();
                Connection conPostgres  = poolManager.getConnection("fintra");
                ResultSet rs = null;
                if (conPostgres == null)
                        throw new SQLException("Sin conexion");
                PreparedStatement  st    = null;   
                this.estado = null;
                
                try{
                        st= conPostgres.prepareStatement(this.SQL_OBTENER);
                        
                        st.setString(1, tipo);        
                        st.setString(2, codestado.toUpperCase());
                        
                        rs = st.executeQuery();
                        
                        if( rs.next() ){
                                this.estado = new TblEstado();
                                this.estado.load(rs);
                                
                                try{
                                        //--- Escribimos el archivo :               
                                       InputStream in = this.estado.getImagen();
                                       int data;
                                       File path = new File(ruta);
                                       path.mkdirs();
                                       File f = new File( ruta + this.estado.getFilename() );
                                       //f.mkdir();
                                       FileOutputStream out  = new FileOutputStream(f);
                                       while( (data = in.read()) != -1 )
                                           out.write( data );

                                       in.close();
                                       out.close();
                       
                                }catch(Exception k){ 
                                        throw new IOException(" ERROR AL ESCRIBIR EL ARCHIVO: " + k.getMessage());} 
                                }
                        
                }catch(Exception e){
                    throw new SQLException("ERROR AL OBTENER EL ESTADO : "+ e.getMessage());
                }
                finally{
                    if(st!=null) st.close();
                    poolManager.freeConnection("fintra",conPostgres);
                }
        }
        
        public boolean existeEstado(String tipo, String codestado) throws SQLException {
                        
                PoolManager poolManager = PoolManager.getInstance();
                Connection conPostgres  = poolManager.getConnection("fintra");
                ResultSet rs = null;
                if (conPostgres == null)
                        throw new SQLException("Sin conexion");
                PreparedStatement  st    = null;   
                this.estado = null;
                boolean r = false;
                
                try{
                        st= conPostgres.prepareStatement(this.SQL_EXISTE);
                        
                        st.setString(1, tipo);        
                        st.setString(2, codestado.toUpperCase());
                        
                        rs = st.executeQuery();
                        
                        if( rs.next() ){
                                r = true;
                        }
                        
                }catch(Exception e){
                    throw new SQLException("ERROR AL VERIFICAR LA EXISTENCIA EL ESTADO : "+ e.getMessage());
                }
                finally{
                    if(st!=null) st.close();
                    poolManager.freeConnection("fintra",conPostgres);
                }
                
                return r;
        }
        
        public void obtenerEstados() throws SQLException {
                        
                PoolManager poolManager = PoolManager.getInstance();
                Connection conPostgres  = poolManager.getConnection("fintra");
                ResultSet rs = null;
                if (conPostgres == null)
                        throw new SQLException("Sin conexion");
                PreparedStatement  st    = null;   
                this.estado = null;
                
                try{
                        st= conPostgres.prepareStatement(this.SQL_LISTAR);
                        
                        rs = st.executeQuery();
                        
                        this.estados = new Vector();
                        
                        while( rs.next() ){
                                this.estado = new TblEstado();
                                this.estado.load(rs);
                                this.estados.add(this.estado);
                        }
                        
                }catch(Exception e){
                    throw new SQLException("ERROR AL LISTAR LSO ESTADOS : "+ e.getMessage());
                }
                finally{
                    if(st!=null) st.close();
                    poolManager.freeConnection("fintra",conPostgres);
                }
        }
        
       
        /**
         * Getter for property estados.
         * @return Value of property estados.
         */
        public java.util.Vector getEstados() {
                return estados;
        }
        
        /**
         * Setter for property estados.
         * @param estados New value of property estados.
         */
        public void setEstados(java.util.Vector estados) {
                this.estados = estados;
        }
        
        /**
         * Getter for property estado.
         * @return Value of property estado.
         */
        public com.tsp.operation.model.beans.TblEstado getEstado() {
                return estado;
        }
        
        /**
         * Setter for property estado.
         * @param estado New value of property estado.
         */
        public void setEstado(com.tsp.operation.model.beans.TblEstado estado) {////System.out.println("estado setted!!");
                this.estado = estado;
        }
        
}
