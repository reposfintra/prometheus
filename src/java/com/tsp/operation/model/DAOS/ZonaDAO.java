/*
 * ZonaDAO.java
 *
 * Created on 13 de julio de 2005, 09:00 AM
 */
package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.*;


/**
 *
 * @author  Henry
 */
public class ZonaDAO extends MainDAO {
    
     //20-11-2006
    private static final String SQL_LISTAR_ZONASXUSUARIO    = "SELECT * FROM ZONA z, zonausuario zu WHERE z.codzona = zu.zona and upper(zu.nomusuario) = ? and z.REC_STATUS!='A' AND zu.rec_status != 'A' ORDER BY z.desczona";
    private static final String SQL_LISTAR_ZONASFRON = "SELECT * FROM ZONA WHERE REC_STATUS!='A' AND FRONTERA = 'S' ORDER BY desczona";
    private static final String SQL_LISTAR_ZONASNOFRON = "SELECT * FROM ZONA WHERE REC_STATUS!='A' AND FRONTERA = 'N' ORDER BY desczona";
   
    private List zonasFronterisas;
    private List zonasNoFronterisas;
    
    private Zona zona;
    private List zonas;
    private Vector VecZona; 
    private static final String SQL_LISTAR_ZONAS    = "SELECT * FROM ZONA WHERE REC_STATUS!='A' ORDER BY desczona";
    
    private static final String SQL_INSERTAR_ZONA   = "INSERT INTO ZONA (codzona, desczona, base, creation_user) VALUES(?,?,?,?)";
    private static final String SQL_EXISTE_ZONA     = "SELECT * FROM ZONA WHERE CODZONA=? AND REC_STATUS!='A'";
    private static final String SQL_EXISTE_ZONA_ANULADA = "SELECT * FROM ZONA WHERE CODZONA=? AND REC_STATUS='A'";
    private static final String SQL_BUSCAR_ZONA     = "SELECT * FROM ZONA where CODZONA= ?";
    private static final String SQL_ACTUALIZAR_ZONA = "UPDATE ZONA SET desczona=?, user_update = ?, last_update = 'now()' where CODZONA=?";
    private static final String SQL_ELIMINAR_ZONA   = "UPDATE ZONA SET rec_status = 'A', user_update = ?, last_update = 'now()' where CODZONA=?";
    private static final String SQL_ACTUALIZAR_ANULADA = "UPDATE ZONA SET REC_STATUS='' WHERE CODZONA=?";

    
    public ZonaDAO() {
        super("ZonaDAO.xml");//JJCastro fase2
    }
    
    public void setZona(Zona zon){        
        this.zona = zon;        
    }
    
    // insertar zona en BD
    public void insertarZona() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        String query = "SQL_ACTUALIZAR_ANULADA";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                if(existeZonaAnulada(zona.getCodZona())){
                    st = con.prepareStatement(this.obtenerSQL("SQL_ACTUALIZAR_ANULADA"));//JJCastro fase2
                    st.setString(1, zona.getCodZona());
                }
                else{
                    st = con.prepareStatement(this.obtenerSQL("SQL_INSERTAR_ZONA"));//JJCastro fase2
                    st.setString(1, zona.getCodZona());
                    st.setString(2, zona.getNomZona());
                    st.setString(3, zona.getBase());
                    st.setString(4, zona.getCreation_user());
                }                
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL INSERTAR ZONA" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }    

/**
 * 
 * @param cod
 * @return
 * @throws SQLException
 */
    public boolean existeZonaAnulada(String cod) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        String query = "SQL_EXISTE_ZONA_ANULADA";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cod);
                rs = st.executeQuery();
                return rs.next();
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR ZONA" + e.getMessage() + "" + e.getErrorCode() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return false;
    } 
    
/**
 *
 * @param cod
 * @return
 * @throws SQLException
 */
    public boolean existeZona (String cod) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        String query = "SQL_EXISTE_ZONA";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cod);
                rs = st.executeQuery();
                return rs.next();
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR ZONA" + e.getMessage() + "" + e.getErrorCode() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return false;
    }    
    //obtiene la lista de ZONAS
    public List obtenerZonas (){
        return zonas;
    }
    public Vector obtZonas (){
        return VecZona;
    }

 /**
  *
  * @throws SQLException
  */
    public void listarZonas ()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        zonas = null;
        String query = "SQL_LISTAR_ZONAS";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                zonas =  new LinkedList();
                
                while (rs.next()){
                    zonas.add(Zona.load(rs));
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }


 /**
  *
  * @param usuario
  * @throws SQLException
  */
    public void listarZonasxUsuario ( String usuario)throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        zonas = null;
        String query = "SQL_LISTAR_ZONASXUSUARIO";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, usuario);
                rs = st.executeQuery();
                zonas =  new LinkedList();
                while (rs.next()){
                    zonas.add(Zona.load(rs));
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    public Zona obtenerZona()throws SQLException{
        return zona;
    }

/**
 *
 * @throws SQLException
 */
    public void zonas ()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        VecZona = null;        
        String query = "SQL_LISTAR_ZONAS";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                VecZona =  new Vector();
                
                while (rs.next()){
                    VecZona.add(Zona.load(rs));
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }


     // retorna el objeto ZONA de pendiendo el codigo
/**
 * 
 * @param cod
 * @throws SQLException
 */
    public void buscarZona (String cod) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        zona = null;
        boolean sw = false;        
        String query = "SQL_BUSCAR_ZONA";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cod);
                rs = st.executeQuery();                
                if (rs.next()){
                   zona = Zona.load(rs);                                     
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR ZONA" + e.getMessage() + "" + e.getErrorCode() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }


/**
 * 
 * @param nom
 * @throws SQLException
 */
    public void buscarZonaNombre (String nom) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        boolean sw = false;
        VecZona = null;        
        String query = "SQL_BUSCAR_NOMBRE";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, nom);
                rs = st.executeQuery();                
                VecZona =  new Vector();                
                while (rs.next()){
                    VecZona.add(Zona.load(rs));
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR ZONA" + e.getMessage() + "" + e.getErrorCode() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }


/**
 * 
 * @throws SQLException
 */
    public void modificarZona () throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        String query = "SQL_ACTUALIZAR_ZONA";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, zona.getNomZona());
                st.setString(2, zona.getUser_update());
                st.setString(3, zona.getCodZona());                
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL MODIFICAR ZONA" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }


/**
 * 
 * @throws SQLException
 */
    public void eliminarZona () throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        String query = "SQL_ELIMINAR_ZONA";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, zona.getUser_update());                
                st.setString(2, zona.getCodZona());   
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL ANULAR ZONA" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
  /**
   * Metodo: cambioZonaPlanilla, Metodo que modifica la zona correspondiente a una
   *          planilla en las trablas trafico e ingreso-trafico
   * @autor : Ing. Sandra Escalante
   * @param : numero de la planilla, codigo de la zona, nombre de la zona
   * @version : 1.0
   */
   public void cambioZonaPlanilla (String planilla, String zona, String nomzona, String distrito, String user, String base, String uzona) throws SQLException {
        PoolManager poolManager = null;
        Connection con = null;
        PreparedStatement st = null;
        PreparedStatement st1 = null;
        ResultSet rs = null;
        String query = "SQL_CAMBIO_ZONA_PLANILLA";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st1 = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st1.setString(1, zona);
                st1.setString(2, nomzona);
                st1.setString(3, planilla);
                st1.setString(4, zona);
                st1.setString(5, planilla);
                st1.executeUpdate();

                //registro la modificacion en el log
                st = con.prepareStatement("INSERT INTO LOG_MOV_TRAFICO VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                st.setString(1, "");
                st.setString(2, distrito);
                st.setString(3, planilla);
                st.setString(4, "");
                st.setString(5, "");
                st.setString(6, "");
                st.setString(7, "Cambio Zona");                    
                st.setString(8,  "0099-01-01 00:00:00");
                st.setString(9,  "");
                st.setString(10,  "");
                st.setString(11,  "");
                st.setString(12,  "Cambio Zona");
                st.setString(13, "now()");
                st.setString(14,  "0099-01-01 00:00:00");
                st.setString(15,  user);
                st.setString(16,  "now()");
                st.setString(17,  user);
                st.setString(18,  base);
                st.setString(19, zona);
                st.setString(20, uzona);
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL CAMBIAR DE ZONA UNA PLANILLA" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
   
   /**
   * Metodo: listarZonasFronterisas, busca las zona fronteriza
   * @autor : Ing. Diogenes Bastidas
   * @param : 
   * @version : 1.0
   */
   
   public void listarZonasFronterisas ()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        zonas = null;
       String query = "SQL_LISTAR_ZONASFRON";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                zonasFronterisas =  new LinkedList();
                while (rs.next()){
                    zonasFronterisas.add(Zona.load(rs));
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
   
   /**
   * Metodo: listarZonasNoFronterisas, busca las zona no fronteriza
   * @autor : Ing. Diogenes Bastidas
   * @param : 
   * @version : 1.0
   */
   
   public void listarZonasNoFronterisas ()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        zonas = null;
        String query = "SQL_LISTAR_ZONASNOFRON";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                zonasNoFronterisas =  new LinkedList();
                
                while (rs.next()){
                    zonasNoFronterisas.add(Zona.load(rs));
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
   
   /**
    * Getter for property zonasFronterisas.
    * @return Value of property zonasFronterisas.
    */
   public java.util.List getZonasFronterisas() {
       return zonasFronterisas;
   }
   
   
   /**
    * Getter for property zonasNoFronterisas.
    * @return Value of property zonasNoFronterisas.
    */
   public java.util.List getZonasNoFronterisas() {
       return zonasNoFronterisas;
   }
}
