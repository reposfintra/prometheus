/***********************************************************************************
 * Nombre clase : ............... Stdjob_tbldocDAO.java                            *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.                                        *
 * Autor :....................... Ing. Juan Manuel Escandon Perez                  *
 * Fecha :........................ 27 de octubre de 2005, 11:02 AM                 *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.*;

public class Stdjob_tbldocDAO {
        /*
         *DECLARACION DE LAS CONSULTAS SQL
         */
        
        private static String  SQLINSERT        =       "       INSERT INTO STDJOB_TBLDOC                                               "+
                                                        "       ( DSTRCT, DOCUMENT_TYPE, STD_JOB_NO, CREATION_USER, CREATION_DATE )     "+
                                                        "       VALUES ( ?, ?, ?, ?, 'now()' )                                          ";
        
        private static String  SQLLIST          =       "       SELECT REG_STATUS, dstrct,DOCUMENT_TYPE, STD_JOB_NO FROM STDJOB_TBLDOC   ";
        
        private static String  SQLUPDATEREG     =       "       UPDATE STDJOB_TBLDOC SET REG_STATUS = ?, USER_UPDATE = ?,               "+
                                                        "       LAST_UPDATE = 'now()' WHERE                                             "+
                                                        "       DOCUMENT_TYPE = ? AND STD_JOB_NO = ?                                    ";
        
        private static String SQLUPDATE         =       "       UPDATE STDJOB_TBLDOC SET DOCUMENT_TYPE = ?, USER_UPDATE = ?,            "+
                                                        "       LAST_UPDATE = 'now()' WHERE DOCUMENT_TYPE = ? AND STD_JOB_NO = ?        ";
        
        private static String SQLDELETE         =       "       DELETE FROM STDJOB_TBLDOC WHERE DOCUMENT_TYPE = ?                       "+
                                                        "       AND STD_JOB_NO = ?                                                      ";
        
        private static String SQLSEARCH         =       "       SELECT REG_STATUS, DOCUMENT_TYPE, STD_JOB_NO FROM STDJOB_TBLDOC         "+
                                                        "       WHERE                                                                   "+
                                                        "       DOCUMENT_TYPE = ? AND STD_JOB_NO = ?                                    ";

        private static String SQL_MOSTRAR_NOMBRE_TIPO_DOC = "   select document_name from tbldoc where document_type = ?                ";
        
        private static String SQLLISTJOB        =       "       SELECT * FROM stdjob_tbldoc WHERE std_job_no = ?                        ";
        /*
         *FIN DE LA DECLARACION
         */
        
        /** Creates a new instance of Stdjob_tbldocDAO */
        public Stdjob_tbldocDAO() {
        }
        
        /**
         * Metodo Insert, recibe los atributos del objeto de tipo Stdjob_tbldoc,
         * permite insertar un registro en la Base de datos
         * @autor : Ing. Juan Manuel Escandon Perez
         * @param : String distrito, String tipo de documento, String numero de Standart, String usuario de creacion
         * @throws : Excepcion al momento de insertar ( Duplicate Key )
         * @version : 1.0
         */
        public void Insert(String dstrct, String document_type , String std_job_no, String creation_user) throws SQLException {
                PreparedStatement st          = null;
                PoolManager       poolManager = PoolManager.getInstance();
                Connection        conPostgres = poolManager.getConnection("fintra");
                if (conPostgres == null)
                        throw new SQLException("Sin conexion");
                try {
                        st = conPostgres.prepareStatement(this.SQLINSERT);
                        st.setString(1, dstrct);
                        st.setString(2, document_type);
                        st.setString(3, std_job_no);
                        st.setString(4, creation_user);
                        st.executeUpdate();
                }
                catch(SQLException e) {
                        throw new SQLException("Error en rutina INSERT [Stdjob_tbldocDAO]... \n"+e.getMessage());
                }
                finally {
                        if(st!=null)  st.close();
                        poolManager.freeConnection("fintra",conPostgres);
                }
        }
        
        
        /**
         * Metodo Update, recibe los atributos del objeto de tipo Stdjob_tbldoc,
         * permite actualizar un registro en la Base de datos
         * @autor : Ing. Juan Manuel Escandon Perez
         * @param : String distrito, String nuevo tipo de documento, String nuevo numero de Standart, String usuario de modificacion
         *          String tipo de documento, String numero de Standart
         * @version : 1.0
         */
        public void Update(String document_type, String user_update, String std_job_no) throws SQLException {
                PreparedStatement st          = null;
                PoolManager       poolManager = PoolManager.getInstance();
                Connection        conPostgres = poolManager.getConnection("fintra");
                List lista           = new LinkedList();
                
                if (conPostgres == null)
                        throw new SQLException("Sin conexion");
                try {
                        st = conPostgres.prepareStatement(this.SQLLIST);
                        st.setString(1, document_type);
                        st.setString(2, user_update);
                        st.setString(3, std_job_no);
                        st.executeUpdate();
                }
                catch(SQLException e) {
                        throw new SQLException("Error en rutina UPDATE [Stdjob_tbldocDAO]... \n"+e.getMessage());
                }
                finally {
                        if(st!=null)  st.close();
                        poolManager.freeConnection("fintra",conPostgres);
                }
        }
        
        /**
         * Metodo List, lista todos los registros de la tabla stdjob_tbldoc
         * @autor : Ing. Juan Manuel Escandon Perez
         * @version : 1.0
         */
        public List List() throws Exception{
                PoolManager poolManager = PoolManager.getInstance();
                Connection con = poolManager.getConnection("fintra");
                if (con == null)
                        throw new SQLException("Sin conexion");
                PreparedStatement st = null;
                ResultSet rs         = null;
                List lista           = new LinkedList();
                try{
                        st = con.prepareStatement(this.SQLLIST);
                        rs=st.executeQuery();
                        if(rs!=null){
                                while(rs.next()){
                                        lista.add(Stdjob_tbldoc.load(rs));
                                }
                        }
                }catch(Exception e){
                        throw new SQLException("Error en rutina LIST [Stdjob_tbldocDAO].... \n"+ e.getMessage());
                }
                finally{
                        if(st!=null) st.close();
                        if(rs!=null) rs.close();
                        poolManager.freeConnection("fintra",con);
                }
                return lista;
        }
        
        /**
         * Metodo UpdateStatus, recibe los atributos del objeto de tipo Stdjob_tbldoc,
         * permite activar o anular un registro en la Base de datos
         * @autor : Ing. Juan Manuel Escandon Perez
         * @param : String reg_status, String tipo de documento, String numero de Standart, String usuario de modificacion
         * @version : 1.0
         */
        public void UpdateStatus(String reg_status, String user_update, String document_type,String std_job_no) throws SQLException {
                PreparedStatement st          = null;
                PoolManager       poolManager = PoolManager.getInstance();
                Connection        conPostgres = poolManager.getConnection("fintra");
                List lista           = new LinkedList();
                
                if (conPostgres == null)
                        throw new SQLException("Sin conexion");
                try {
                        st = conPostgres.prepareStatement(this.SQLUPDATEREG);
                        st.setString(1, reg_status);
                        st.setString(2, user_update);
                        st.setString(3, document_type);
                        st.setString(4, std_job_no);
                        st.executeUpdate();
                }
                catch(SQLException e) {
                        throw new SQLException("Error en rutina UPDATESTATUS [Stdjob_tbldocDAO]... \n"+e.getMessage());
                }
                finally {
                        if(st!=null)  st.close();
                        poolManager.freeConnection("fintra",conPostgres);
                }
        }
        
        /**
         * Metodo Delete, recibe los atributos del objeto de tipo Stdjob_tbldoc,
         * permite eliminar un registro en la Base de datos
         * @autor : Ing. Juan Manuel Escandon Perez
         * @param : String tipo de documento, String numero de Standart
         * @version : 1.0
         */
        public void Delete(String document_type,String std_job_no ) throws SQLException {
                PreparedStatement st          = null;
                PoolManager       poolManager = PoolManager.getInstance();
                Connection        conPostgres = poolManager.getConnection("fintra");
                if (conPostgres == null)
                        throw new SQLException("Sin conexion");
                try {
                        st = conPostgres.prepareStatement(this.SQLDELETE);
                        st.setString(1,  document_type);
                        st.setString(2,  std_job_no);
                        st.executeUpdate();
                }
                catch(SQLException e) {
                        throw new SQLException("Error en rutina DELETE [Stdjob_tbldocDAO]... \n"+e.getMessage());
                }
                finally {
                        if(st!=null)  st.close();
                        poolManager.freeConnection("fintra",conPostgres);
                }
        }
        
        
        /**
         * Metodo Search, recibe los atributos del objeto de tipo Stdjob_tbldoc,
         * permite buscar un registro en la Base de datos
         * @autor : Ing. Juan Manuel Escandon Perez
         * @param : String tipo de documento, String numero de Standart
         * @version : 1.0
         */
        public Stdjob_tbldoc Search(String document_type, String std_job_no ) throws Exception{
                PoolManager poolManager = PoolManager.getInstance();
                Connection con = poolManager.getConnection("fintra");
                if (con == null)
                        throw new SQLException("Sin conexion");
                PreparedStatement st = null;
                ResultSet rs         = null;
                Stdjob_tbldoc datos  = new Stdjob_tbldoc();
                try{
                        st = con.prepareStatement(this.SQLSEARCH);
                        st.setString(1, document_type);
                        st.setString(2, std_job_no);
                        
                        rs=st.executeQuery();
                        while(rs.next()){
                                datos = Stdjob_tbldoc.load(rs);
                                break;
                        }
                }catch(Exception e){
                        throw new SQLException("Error en rutina SEARCH [Stdjob_tbldocDAO].... \n"+ e.getMessage());
                }
                finally{
                        if(st!=null) st.close();
                        if(rs!=null) rs.close();
                        poolManager.freeConnection("fintra",con);
                }
                return datos;
        }
        
        /**
         * Metodo Exist, recibe los atributos del objeto de tipo Stdjob_tbldoc,
         * permite verificar si existe un registro en la Base de datos
         * @autor : Ing. Juan Manuel Escandon Perez
         * @param : String tipo de documento, String numero de Standart
         * @version : 1.0
         */
        public boolean Exist(String document_type, String std_job_no ) throws Exception{
                PoolManager poolManager = PoolManager.getInstance();
                Connection con = poolManager.getConnection("fintra");
                if (con == null)
                        throw new SQLException("Sin conexion");
                PreparedStatement st = null;
                ResultSet rs         = null;
                boolean flag         = false;
                try{
                        st = con.prepareStatement(this.SQLSEARCH);
                        st.setString(1, document_type);
                        st.setString(2, std_job_no);
                        
                        rs=st.executeQuery();
                        while(rs.next()){
                                flag = true;
                                break;
                        }
                }catch(Exception e){
                        throw new SQLException("Error en rutina BUSCAR [Stdjob_tbldocDAO].... \n"+ e.getMessage());
                }
                finally{
                        if(st!=null) st.close();
                        if(rs!=null) rs.close();
                        poolManager.freeConnection("fintra",con);
                }
                return flag;
        }
        
        /**
        * Metodo list_Stdjob, lista todos los registros de la tabla stdjob_tbldocd dados unos parametros
        * @autor : Ing. Jose de la rosa
        * @param : String numero de Standart
        * @version : 1.0
        */
        public List list_Stdjob(String job) throws Exception{
                PoolManager poolManager = PoolManager.getInstance();
                Connection con = poolManager.getConnection("fintra");
                if (con == null)
                        throw new SQLException("Sin conexion");
                PreparedStatement st = null;
                ResultSet rs         = null;
                List lista           = new LinkedList();
                try{
                        st = con.prepareStatement(this.SQLLISTJOB);
                        ////System.out.println("LISTADO " + st);
                        st.setString(1, job);
                        rs=st.executeQuery();
                        if(rs!=null){
                                while(rs.next()){
                                        lista.add(Stdjob_tbldoc.load(rs));
                                }
                        }
                }catch(Exception e){
                        throw new SQLException("Error en rutina LIST [Stdjob_tbldocDAO].... \n"+ e.getMessage());
                }
                finally{
                        if(st!=null) st.close();
                        if(rs!=null) rs.close();
                        poolManager.freeConnection("fintra",con);
                }
                return lista;
        }
                
        /**
        * Metodo nombreDocumento, retorna el nombre del tipo de documento
        * @autor : Ing. Jose de la rosa
        * @param : String tipo documento
        * @version : 1.0
        */
        public String nombreDocumento(String tipo) throws SQLException{
            Connection con= null;
            PreparedStatement st = null;
            ResultSet rs = null;       
            PoolManager poolManager = null;      
            String remesas = "";
            try{
                poolManager = PoolManager.getInstance();
                con = poolManager.getConnection("fintra");
                if (con != null ){
                    st = con.prepareStatement(this.SQL_MOSTRAR_NOMBRE_TIPO_DOC);
                    st.setString(1, tipo);
                    rs = st.executeQuery();

                    if (rs.next()){
                        remesas = rs.getString( "document_name");
                    }
                }
            }
            catch(SQLException e){
                throw new SQLException("ERROR DURANTE LA CONSULTA DE BUSQUEDA DEL NOMBRE DEL TIPO DE DOCUMENTO " + e.getMessage() + " " + e.getErrorCode());
            }
            finally{
                if (st != null){
                    try{
                       st.close();
                    }
                    catch(SQLException e){
                       throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                    }
                }

                if (con != null){
                    poolManager.freeConnection("fintra", con);                
                }
            }    
            return remesas;
    }
}
