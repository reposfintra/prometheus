/***********************************************************************************
 * Nombre clase : ............... RepInfoOtMimPosDAO.java                                   *
 * Descripcion :................. clase que maneja las consultas del reporte                                *
 * Autor :....................... Ing. Ivan Gomez                                  *
 * Fecha :....................... 22 de octubre de 2005, 11:02 AM                   *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/

package com.tsp.operation.model.DAOS;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import java.util.*;

/**
 *
 * @author amendez
 */
public class RepInfoOtMimPosDAO extends MainDAO{
    
    private Vector reporte;
    
    /** Creates a new instance of DAO */
    public RepInfoOtMimPosDAO() {
        super("RepInfoOtMimPosDAO.xml");
    }
    
    
     /**
     * Metodo que extrae la informacion de postgres y mims para hacer las comparaciones.
     * @autor.......Ivan Gomez
     * @param.......String fecha inicial i String fecha final 
     * @throws......SQLException
     * @version.....1.0.
     * @return.......
     */
    public void genRepInfoOtMimVsPos(String fecini, String fecfin) throws SQLException{
        PreparedStatement psttm = null;
        ResultSet rs = null;
        RepInfoOtMimVsPos repInfoOtMimVsPos = null;
        
        String fecha        ="";
        String ot           = "";
        String fact         = "";
        String docint       = "";
        String cantkg       = "";
        String contenedor   = "";
        String precinto     = "";
        String unidades     = "";
        
        String otpg         = "";
        String factpg       = "";
        String docintpg     = "";
        String cantkgpg     = "";
        String contenedorpg = "";
        String precintopg   = "";
        String unidadespg   = "";
        
        String diffact         = "";
        String difdocint       = "";
        String difcantkg       = "";
        String difcontenedor   = "";
        String difprecinto     = "";
        String difunidades     = "";
        
        try{
            psttm = crearPreparedStatement("SQL_OTS");
            psttm.setString(1, fecini+" 00:00:00");
            psttm.setString(2, fecfin+" 23:59:59");
            
            rs = psttm.executeQuery();
            
            reporte = new Vector();
            while(rs.next()){
                
                //Informacion extraida de postgres
                fecha        = rs.getString(1);
                otpg         = rs.getString(2);
                cantkgpg     = rs.getString(3);
                unidadespg   = rs.getString(4);
                precintopg   = getPrecinto(otpg);
                contenedorpg = getContenedor(otpg);
                docintpg     = getDoc(otpg,"008").toUpperCase();
                factpg       = getDoc(otpg,"009").toUpperCase();
                /////////////////////////////////////////////
                
                //Informacion extraida de mims
                ot           = otpg;
                fact         = getReferenceCode("002", ot).toUpperCase();
                docint       = getReferenceCode("001", ot).toUpperCase();
                cantkg       = getReferenceCode("013", ot).trim();
                if(cantkg.equals(""))
                    cantkg = "0";
                
                double valor  = Integer.parseInt(cantkg)/100;
                cantkg = String.valueOf(valor);
                
                contenedor = getReferenceCode("014", ot);
                precinto = getReferenceCode("015", ot);
                unidades = getReferenceCode("012", ot);
                ///////////////////////////////////////////////
                
                // buscando las diferencias
                double valor2 = Double.parseDouble(cantkgpg);
                if(valor == valor2)
                    difcantkg ="N";
                else
                    difcantkg ="S";
                
                difdocint     = isDifDoc(docintpg,docint);
                diffact       = isDifDoc(factpg,fact);
                difcontenedor = isDifDoc(contenedorpg,contenedor);
                difprecinto   = isDifDoc(precintopg,precinto);
                difunidades      = isDifDoc(unidadespg,unidades);
                /////////////////////////////////////////
                
                
                
                
                repInfoOtMimVsPos = new RepInfoOtMimVsPos();
                repInfoOtMimVsPos.setInfo(fecha, ot, docint, fact, cantkg, contenedor, precinto, unidades, otpg, docintpg, factpg, cantkgpg, contenedorpg, precintopg, unidadespg, difdocint, diffact, difcantkg, difcontenedor, difprecinto, difunidades);
                reporte.add(repInfoOtMimVsPos);
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR GENERANDO REPORTE DE OTS MIM VS PG - "+e.getMessage());
        }
        finally{
            if (psttm != null) try { psttm.close(); }catch(SQLException ignore){}
            desconectar("SQL_OTS");
        }
    }
    
    /**
     * Metodo que extrae los reference_code de mims
     * @autor.......Ivan Gomez
     * @param.......String refno, String numrem
     * @throws......SQLException
     * @version.....1.0.
     * @return...... String reference_code
     */
    private String getReferenceCode(String refno, String numrem) throws SQLException{
        PreparedStatement psttm = null;
        ResultSet rs = null;
        String refcode = "";
        try{
            psttm = crearPreparedStatement("SQL_REFERENCE_CODES");
            psttm.setString(1, numrem);
            psttm.setString(2, refno);
            rs = psttm.executeQuery();
            while(rs.next()){
                if(!refcode.equals("")) refcode+=",";
              
                int i = rs.getString(1).indexOf(":");
                if(i==-1){
                    refcode += rs.getString(1);
                }else{
                    refcode += rs.getString(1).substring(0,i);
                }
            }
        }
        catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR BUSCANDO REF CODE "+refno);
        }
        finally{
            if (psttm != null) try {psttm.close();}catch(SQLException ignore){}
            desconectar("SQL_REFERENCE_CODES");
        }
        return refcode;
    }
    
     /**
     * Metodo que extrae los documentos de postgres
     * @autor.......Ivan Gomez
     * @param.......String remesa, String tipo_doc 
     * @throws......SQLException
     * @version.....1.0.
     * @return...... String documento
     */
    private String getDoc(String remesa, String tipo_doc ) throws SQLException{
        PreparedStatement psttm = null;
        ResultSet rs = null;
        String dato = "";
        try{
            psttm = crearPreparedStatement("SQL_DOC_INTERNO_FACCIAL");
            psttm.setString(1, remesa);
            psttm.setString(2, tipo_doc);
            rs = psttm.executeQuery();
            
            while(rs.next()){
                if(!dato.equals("")) dato+=",";
                
                int i = rs.getString(1).indexOf(":");
                if(i==-1){
                    dato +=  rs.getString(1);
                }else{
                    dato +=  rs.getString(1).substring(0,i);
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO DOCUMENTOS O FACTURA COMERCIAL");
        }
        finally{
            if (psttm != null) try {psttm.close();}catch(SQLException ignore){}
            desconectar("SQL_DOC_INTERNO_FACCIAL");
        }
        return dato;
    }
    
    /**
     * Metodo que extrae los precintos de postgres
     * @autor.......Ivan Gomez
     * @param.......String remesa
     * @throws......SQLException
     * @version.....1.0.
     * @return...... String documento
     */
    private String getPrecinto(String remesa) throws SQLException{
        PreparedStatement psttm = null;
        ResultSet rs = null;
        String precinto = "";
        try{
            psttm = crearPreparedStatement("SQL_PRECINTO");
            psttm.setString(1, remesa);
            rs = psttm.executeQuery();
            while(rs.next()){
                if(!precinto.equals("")) precinto+=",";
                precinto +=  rs.getString(1);
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO PRECINTOS");
        }
        finally{
            if (psttm != null) try {psttm.close();}catch(SQLException ignore){}
            desconectar("SQL_PRECINTO");
        }
        return precinto;
    }
    
    
     /**
     * Metodo que extrae los contenedores de postgres
     * @autor.......Ivan Gomez
     * @param.......String remesa
     * @throws......SQLException
     * @version.....1.0.
     * @return...... String documento
     */
    private String getContenedor(String remesa) throws SQLException{
        PreparedStatement psttm = null;
        ResultSet rs = null;
        String contenedor = "";
        try{
            psttm = crearPreparedStatement("SQL_CONTENEDOR");
            psttm.setString(1, remesa);
            rs = psttm.executeQuery();
            
            
            while(rs.next()){
                if(!rs.getString(1).equals("")){
                    String[] vec = rs.getString(1).split(",");
                    for(int i=0;i<vec.length;i++){
                        contenedor += vec[i] + ",";
                    }
                }
            }
            if(!contenedor.equals("")){
                if(contenedor.substring(contenedor.length()-1, contenedor.length()).equals(",")){
                    contenedor = contenedor.substring(0,contenedor.length()-1);
                }
            }
            
            
        }
        catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO CONTENEDOR");
        }
        finally{
            if (psttm != null) try {psttm.close();}catch(SQLException ignore){}
            desconectar("SQL_CONTENEDOR");
        }
        return contenedor;
    }
    
    public Vector getReporte() {
        return reporte;
    }
    
    public void setReporte(Vector reporte) {
        this.reporte = reporte;
    }
    
     /**
     * Metodo que compara la informacion de popstgres contra la de mims
     * @autor.......Ivan Gomez
     * @param.......String remesa
     * @throws......SQLException
     * @version.....1.0.
     * @return...... String documento
     */
    private String isDifDoc(String docintpg, String docint) {
        String diferente="N";
        
        
        String []documentos = docintpg.split(",");
        for(int i=0; i< documentos.length;i++){
            if(docint.indexOf(documentos[i])==-1) {
                diferente ="S";
                break;
            }
        }
        
        
        return diferente;
    }
    
    
    
    
    //    public static void main(String a [])throws Exception{
    //        RepInfoOtMimPosDAO  x = new RepInfoOtMimPosDAO();
    //        String cont = x.getContenedor("A1028939");
    //        cont += x.getContenedor("A1029340");
    //        if(cont.substring(cont.length()-1, cont.length()).equals(",")){
    //            cont = cont.substring(0,cont.length()-1);
    //        }
    //        ////System.out.println("RESULT  "+ cont);
    //    }
}