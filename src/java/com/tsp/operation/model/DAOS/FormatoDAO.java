/************************************************************************************
 * Nombre clase : ............... ImpresionChequeDAO.java                           *
 * Autor :....................... Ing. Fernel Villacob                              *
 * Fecha :....................... 22/10/2006                                        *
 * Version :..................... 1.0                                               *
 * Copyright :................... Fintravalores S.A.                           *
 ************************************************************************************/



package com.tsp.operation.model.DAOS;





import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.Utility;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;



public class FormatoDAO extends MainDAO{
    
    private TreeMap Fremision;
    private TreeMap Fcostos;
    private TreeMap fcompensac;
    Vector vector =  null;
    
    public FormatoDAO() {
          super("FormatoDAO.xml");
    }
    
    
    
    public String reset(String val){
        return  (val==null)?"":val;
    }
    
    
    
    /**
     * M�todo que buscar formato aplicado al std
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/
    public String  getFormato(String std) throws Exception{        
        Connection con = null;//JJCastro fase2
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        String            query    = "SQL_FORMATO_STD";
        String            formato  = "";        
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, std);
                rs = st.executeQuery();
                if (rs.next()) {
                    formato = rs.getString("referencia");
                }

            }
        }catch(Exception e){
            throw new SQLException("getFormato --->" + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return formato;
    }
    
    
    
    
     /**
     * M�todo que buscar formato aplicado al std
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/
    public List  getCamposFormato(String formato) throws Exception{        
        Connection con = null;//JJCastro fase2
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        String            query    = "SQL_CAMPOS_FORMATO";
        List              lista    = new LinkedList();
        try{
            String  hoy      =  Utility.getHoy("-");
            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, formato );
            System.out.println("query de cargar campos "+st.toString());
            rs = st.executeQuery();
            while(rs.next()){
                EsquemaFormato  campo =  new  EsquemaFormato();
                   campo.setCampo      (   reset( rs.getString("nom_campo") )     );
                   campo.setDescripcion(   reset( rs.getString("descripcion") )   );
                   campo.setTitulo     (   reset( rs.getString("titulo") )        );
                   campo.setOrden      (          rs.getInt   ("orden")           );
                   campo.setPInicial   (          rs.getInt   ("pos_inicial")     );
                   campo.setPFinal     (          rs.getInt   ("pos_final")       );
                   campo.setTipo       (   reset( rs.getString("tipo") )          );
                   if( campo.getTipo().equals("DATE") )
                       campo.setContenido  ( hoy );
                   
                
                lista.add( campo );                
            campo = null;//Liberar Espacio JJCastro
            }
               
                
        }}catch(Exception e){
            throw new SQLException("getCamposFormato --->" + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    
    
    
    
    /**
     * M�todo que buscar datos del formato aplicado al documento
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/
    public List  searchCamposFormato(String distrito, String tipoDoc, String doc, String formato) throws Exception{        
        Connection con = null;//JJCastro fase2
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        String            query    = "SQL_BUSCAR_DATO_FORMATO_DOC";
        List              lista    = new LinkedList();
        String contenidos = "";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito );
            st.setString(2, tipoDoc  );
            st.setString(3, doc      );
            st.setString(4, formato  );
            System.out.println("query de busqueda  "+st.toString());
            rs = st.executeQuery();
            while(rs.next()){
                EsquemaFormato  campo =  new  EsquemaFormato();
                  
                   campo.setDistrito   (   reset( rs.getString("dstrct") )        );
                   campo.setTipoDoc    (   reset( rs.getString("tipo_doc") )      );
                   campo.setDoc        (   reset( rs.getString("documento") )     );
                   campo.setFormato    (   reset( rs.getString("formato") )       );                
                   campo.setCampo      (   reset( rs.getString("nom_campo") )     );
                   campo.setDescripcion(   reset( rs.getString("descripcion") )   );
                   campo.setTitulo     (   reset( rs.getString("titulo") )        );
                   campo.setOrden      (          rs.getInt   ("orden")           );
                   campo.setPInicial   (          rs.getInt   ("pos_inicial")     );
                   campo.setPFinal     (          rs.getInt   ("pos_final")       );
                   campo.setTipo       (   reset( rs.getString("tipo") )          );
                   campo.setContenido  (   reset( rs.getString("contenido") ).trim() );
                   campo.setRemision   (   reset( rs.getString("Remision") )      );
                   campo.setCentro_C   (   reset( rs.getString("Centro") )        );
                   campo.setCompresac  (   reset( rs.getString("compensac") )     );
                    contenidos = campo.getContenido();
                   if((contenidos.equals("FC_01")) || (contenidos.equals("FC_02")) || (contenidos.equals("FC_03")) || (contenidos.equals("FC_04")) || (contenidos.equals("FC_05")) || (contenidos.equals("FC_06")) ||(contenidos.equals("FC_07"))){
                    campo.setCentro_C(contenidos);
                     }   
                   
              
                lista.add( campo );                
            campo = null;//Liberar Espacio JJCastro
            }
            
               
                
        }}catch(Exception e){
            throw new SQLException("searchCamposFormato --->" + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    
    
    
    
    
    /**
     * M�todo que buscar los tipo de formato
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/
    public List  getTipoFormatos() throws Exception{        
        Connection con = null;//JJCastro fase2
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        String            query    = "SQL_TIPO_FORMATO";
        List              lista    = new LinkedList();
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            while(rs.next()){
                Hashtable  formato = new Hashtable();
                  formato.put("codigo",        reset(  rs.getString("table_code")  )  );
                  formato.put("descripcion",   reset(  rs.getString("descripcion") )  );                
                lista.add(formato);                              
          formato = null;//Liberar Espacio JJCastro
            }
                
        }}catch(Exception e){
            throw new SQLException("getTipoFormatos --->" + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    
    
    
     /**
     * M�todo que buscar los tipo de documentos de despacho.
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/
    public List  getTipoDoc() throws Exception{        
        Connection con = null;//JJCastro fase2
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        String            query    = "SQL_TIPO_DOCUMENTO";
        List              lista    = new LinkedList();
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            while(rs.next()){
                Hashtable  doc = new Hashtable();
                  doc.put("codigo",        reset(  rs.getString("document_type")  )  );
                  doc.put("descripcion",   reset(  rs.getString("document_name")  )  );                
                lista.add(doc);
                doc = null;//Liberar Espacio JJCastro
            }               
                
        }}catch(Exception e){
            throw new SQLException("getTipoDoc --->" + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    
    
    
    
    
    /**
     * M�todo que inserta datos del formato
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/
    public  String  insert(String distrito, String tipo_doc, String doc, String formato, String dato, String user,String Remision,String Centro,String Compensac) throws Exception{        
        Connection con = null;//JJCastro fase2
        PreparedStatement st       = null;
        String            query    = "SQL_INSERT_DATO_FORMATO";
        String            msj      = "Registro insertado exitosamente...";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito );            
            st.setString(2, tipo_doc );
            st.setString(3, doc      );            
            st.setString(4, formato  );
            st.setString(5, dato     );            
            st.setString(6, user     );   
            st.setString(7, Remision );   
            st.setString(8, Centro );
            st.setString(9, Compensac );
            System.out.println("query insert  "+st.toString());
            st.execute();            
                
        }}catch(Exception e){
            msj = "ERROR AL INSERTAR [FormatoDAO.insert] : " + e.getMessage();
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return msj;
    }
    
    
    
    /**
     * M�todo que actualiza datos del formato
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/
    public  String  update(String distrito, String tipo_doc, String doc, String formato, String dato, String user,String Remision, String Compensac) throws Exception{        
        Connection con = null;//JJCastro fase2
        PreparedStatement st       = null;
        String            query    = "SQL_UPDATE_DATO_FORMATO";
        String            msj      = "Registro actualizado exitosamente...";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dato);
                st.setString(2, user);
                st.setString(3, Remision);
                st.setString(4, Compensac);

                st.setString(5, distrito);
                st.setString(6, tipo_doc);
                st.setString(7, doc);
                st.setString(8, formato);
                System.out.println("query modificar  " + st.toString());
                st.execute();

            }
        }catch(Exception e){
            msj = "ERROR AL ACTUALIZAR [FormatoDAO.update] : " + e.getMessage();
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return msj;
    }
    
    
    
    
    /**
     * M�todo que elimina datos del formato
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/
    public  String  delete(String distrito, String tipo_doc, String doc, String formato) throws Exception{        
        Connection con = null;//JJCastro fase2
        PreparedStatement st       = null;
        String            query    = "SQL_DELETE_DATO_FORMATO";
        String            msj      = "Registro eliminado exitosamente...";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito );            
            st.setString(2, tipo_doc );
            st.setString(3, doc      );            
            st.setString(4, formato  );                      
            st.execute();            
                
        }}catch(Exception e){
            msj = "ERROR AL ELIMINAR [FormatoDAO.delete] : " + e.getMessage();
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return msj;
    }
    
    
    
    
    
    /**
     * M�todo que buscar el tama�o max del formato
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/
    public int  getTamanoMAX(String formato) throws Exception{        
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_TAMANO_MAX_FORMATO";
        int tamano = 0;
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, formato);
                rs = st.executeQuery();
                if (rs.next()) {
                    tamano = rs.getInt(1);
                }

            }
        }catch(Exception e){
            throw new SQLException("getTamanoMAX --->" + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return tamano;
    }
    
     /**************************************************
     * Metodo para obtener los datos iniciales para la Impresion de la Remesion de Mercancia
     * @author: Ing. Juan M. Escandon Perez
     **************************************************/    
    public void formato( String dstrct, String tipodoc, String formato, String documento) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_FORMATO";//JJCastro fase2
            
            //En este bloque se realiza la consulta y se hace la desconexion
            //el recorrido sigue despues...
            try{
                con = this.conectarJNDI(query);
                if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString( 1, dstrct );
                st.setString( 2, tipodoc );
                st.setString( 3, formato );
                
                String doc = ( documento!=null || !documento.equals("") )? "AND documento ='" + documento + "'" : "";
                String sql = st.toString().replaceAll( "#DOCUMENTO#", doc );
                
                System.out.println("planilla "+documento+ "  tipo doc "+tipodoc );
                System.out.println("query de formato"+st.toString());
                rs = st.executeQuery(sql);
            
            if( rs != null){
                
                vector = new Vector();
                
                while( rs.next() ){
                    String[] reporte = new String[5];
                    reporte[0] = rs.getString( "documento" ) != null? rs.getString( "documento" ) : "" ;
                    reporte[1] = rs.getString( "datos" ) != null? rs.getString( "datos" ) : "" ;
                    reporte[2] = rs.getString( "titulos" ) != null? rs.getString( "titulos" ) : "" ;
                    reporte[3] = rs.getString( "rem" ) != null? rs.getString( "rem" ) : "" ;
                    reporte[4] = rs.getString( "compensac" ) != null? rs.getString( "compensac" ) : "" ;
                    vector.add( reporte );
                    reporte = null;//Liberar Espacio JJCastro
                }
            }
            
        }}catch (Exception ex){
            throw new SQLException(" formato 2--> " + ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
   

    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return vector;
    }    
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        this.vector = vector;
    }   
    
    
    
    
    
    
    /*************************************************************
     *Metodo para obtener los datos del reporte 'Cuadro Azul'
     *@param: String dstrct distrito
     *@param: String tipodoc tipo de documento
     *@param: String formato formato del reporte
     *@param: String documento numero del documento
     *@throws: En caso de un error en la consulta
     *************************************************************/
    
    
    public ReporteCuadroAzul cuadroAzulFromDatoFormato( ReporteCuadroAzul dato, String dstrct, String documento) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_CUADRO_AZUL_DATO_FORMATO";//JJCastro fase2
        try{
            
            //En este bloque se realiza la consulta y se hace la desconexion
            //el recorrido sigue despues...
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString( 1, dstrct );
                st.setString( 2, documento );
                rs = st.executeQuery();
            if( rs != null){                               
                        if( rs.next() ){
                            String doc;
                            String datos;
                            doc = rs.getString( "documento" ) != null? rs.getString( "documento" ) : "" ;
                            datos = rs.getString( "datos" ) != null? rs.getString( "datos" ) : "" ;
                            String[] split = datos.split("-_-");

                            int tam = split.length;
                                if( tam ==7 || tam == 8 ){
                                    dato.setCost_center(  reset( split[0] ) );
                                    dato.setP_o(          reset( split[1] ) );
                                    dato.setD_o(          reset( split[2] ) );
                                    dato.setContenido(    reset( split[3] ) );
                                    dato.setSolicitante(  reset( split[4] ) );
                                    dato.setTel_ext_cel(  reset( split[5] ) );
                                    dato.setEmpresa_dpto( "" );

                                    if( tam == 8){
                                        dato.setEmpresa_dpto( reset( split[7] ) );
                                    }
                                }

                        }
             }
            
        }}catch (Exception ex){
            throw new SQLException(" FormatoDAO.cuadroAzul 2--> " + ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return dato;
    }


/**
 *
 * @param anio
 * @param nombremes
 * @param nummes
 * @param dstrct
 * @throws Exception
 */
    public void cuadroAzul( String anio, String  nombremes, String nummes, String dstrct ) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        String            query    = "SQL_CUADRO_AZUL_REMESA";
        List              lista    = new LinkedList();
        Calendar cal = Calendar.getInstance();
        
        try{
            int aaaa = Integer.parseInt( anio );
            int mm   = Integer.parseInt( nummes );
            // Se obtiene el ultimo dia del mes
            cal.set( aaaa, (mm-1) , 10 );
            int finmes =  cal.getActualMaximum( cal.DATE );
            String fecha_inicial = anio+"-"+nummes+"-01";
            String fecha_final   = anio+"-"+nummes+"-"+finmes;

            con = this.conectarJNDI(query);
                if (con != null) {
                    st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                    st.setString(1, fecha_inicial);
                    st.setString(2, fecha_final);
                    rs = st.executeQuery();
            
            this.vector = new Vector();
            
            int c = 1;
            
            com.tsp.util.Utility.rellenar( "", 2);
            
            while(rs.next()){
               
                ReporteCuadroAzul dato = new ReporteCuadroAzul();
                
                String despacho = this.generarDespachoNo( nombremes, anio, ""+c , reset( rs.getString("agencia") ) );
                
                dato.setDespacho( despacho );
                dato.setRemision(       reset( rs.getString("numrem") ) );
                dato.setFecha_despacho( reset( rs.getString("fecdsp")));                                
                
                dato.setOrigen(         reset( rs.getString("origen") ) );
                dato.setDestino(        reset( rs.getString("destino") ) );
                
                dato.setP_o("");
                dato.setD_o("");
                dato.setContenido("");                
                
                dato.setTipo_vehiculo(  reset( rs.getString("tipoveh") ) );
                dato.setColor(          reset( rs.getString("color") ) );
                dato.setPlaca(          reset( rs.getString("plaveh") ) );
                dato.setConductor(      reset( rs.getString("nomcond") ) );
                dato.setCedula(         reset( rs.getString("cedcond") ) );
                dato.setCelular(        reset( rs.getString("telefonocond") ) );
                
                dato.setSolicitante("");
                dato.setTel_ext_cel("");
                
                dato.setFecha_entrega(  reset( rs.getString("entrega") ) );
                dato.setNumero_factura( reset( rs.getString("numfact") ) );
                dato.setFecha_factura(  reset( rs.getString("fecha_factura") ) );
                
                dato.setValor_flete( reset( rs.getString("valor") ) );
                dato.setCost_center("");
                dato.setEmpresa_dpto("");
                //Se buscan los datos de la remesaa q se encuentren en la tabla dato_formato
                dato = this.cuadroAzulFromDatoFormato( dato, dstrct, dato.getRemision() );
                
                this.vector.add( dato );
                c++;
                dato = null;//Liberar Espacio JJCastro
            }
                        
        }}catch(Exception e){
            throw new SQLException( " FormatoDAO.cuadroAzulFromRemesa :" + e.getMessage() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }




/**
 *
 * @param mes
 * @param anio
 * @param consecutivo
 * @param agencia
 * @return
 */
    public String generarDespachoNo( String mes, String anio, String consecutivo, String agencia){
        
        String codigo;
        codigo = agencia;
        codigo += com.tsp.util.Utility.rellenar( consecutivo, 4);
        codigo += mes.substring( 0, 3 ).toUpperCase();
        codigo += anio.substring( 2, 4 );
        
        
        return codigo;

    }
    
    public String numRemision( String numpla, String fecha, String agencia ) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        String            query    = "SQL_CONCDESP";
        int               i        = 0;
        String            remision = "";
        try{
            String ano  = fecha.substring(0,4);
            String mes  = fecha.substring(5,7);
            
            int diasmes         = com.tsp.util.Util.diasDelMes(Integer.parseInt(mes), Integer.parseInt(ano));
            String fechainicial = ano+"-"+mes+"-"+"01"; 
            String fechafinal   = ano+"-"+mes+"-"+diasmes;
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, fechainicial );
            st.setString(2, fechafinal   );
            rs = st.executeQuery();
            while(rs.next()){
                i++;                
                if( numpla.equals(rs.getString("numpla")))
                    break;
            }
            
            String nombremes = com.tsp.util.Util.NombreMes(Integer.parseInt(mes));
            
            remision = this.generarDespachoNo( nombremes, ano, String.valueOf(i), agencia );

        }}catch(Exception e){
            throw new SQLException("numRemision --->" + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return remision;
    }

  /**************************************************
     * Metodo para obtener los datos complementario para la Impresion de la Remesion de Mercancia
     * @author: Ing. Juan M. Escandon Perez
     **************************************************/
public DatoFormato formatoComp(DatoFormato dato, String documento ) throws Exception{
    Connection con = null;//JJCastro fase2
    PreparedStatement st       = null;
        ResultSet         rs       = null;
        String            query    = "SQL_FORMATOCOMP";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, documento );
            rs = st.executeQuery();
            while(rs.next()){
                dato.setFechadespacho(rs.getString("fecdsp"));
                dato.setOrigen(rs.getString("origen"));
                dato.setDestino(rs.getString("destino"));
                dato.setRuta(rs.getString("ruta_pla"));
                dato.setPlaca(rs.getString("plaveh"));
                dato.setPiezas(rs.getString("cantpiezas"));
                dato.setTipo(rs.getString("tipoveh"));
                dato.setPeso(rs.getString("pesoreal"));
                dato.setCedcond(rs.getString("cedcond"));
                dato.setCelularcond(rs.getString("telefonocond"));
                dato.setNomcond(rs.getString("nomcond"));
                dato.setOt(rs.getString("numrem"));
                
                dato.setNombreenvio(rs.getString("despachador"));
                dato.setDirdestino(rs.getString("dirdestino"));
                dato.setTeldestino(rs.getString("teldestino"));
                dato.setContactodestino(rs.getString("contactodestino"));
                dato.setDirorigen(rs.getString("dirorigen"));
                dato.setTelorigen(rs.getString("telorigen"));
                dato.setContactoorigen(rs.getString("contactoorigen"));
                dato.setCadena_datos(rs.getString("datosagencia"));
                dato.setHoradespacho(rs.getString("horadsp"));
                
                /*Modificacion 12-01-07*/
                dato.setNumpla(rs.getString("numpla"));
                dato.setAgencia(rs.getString("agencia"));
                dato.setAuxfecha(rs.getString("fecha"));
                String remision = this.numRemision(dato.getNumpla(), dato.getAuxfecha(), dato.getAgencia() );
                dato.setRemesion(remision);
                break;
            }
        }}catch(Exception e){
            throw new SQLException("formatoComp --->" + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return dato;
    }


/**
 *
 * @throws Exception
 */
     public void buscarFormatosRemision()throws Exception{
        Connection con = null;//JJCastro fase2
        Fremision = null;
        Fremision = new TreeMap();
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_REMISION_MERCANCIAS";//JJCastro fase2
         try{
             con = this.conectarJNDI(query);
             if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
             rs = st.executeQuery();
                
                while(rs.next()){
                    Fremision.put(rs.getString("descripcion"), rs.getString("codigo"));
                }
           
        }}
        catch(SQLException e){
            throw new SQLException("ERROR SQL_REMISION_MERCANCIAS " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


 /**
  *
  * @throws Exception
  */
     public void buscarCentrocostos()throws Exception{
        Connection con = null;//JJCastro fase2
        Fcostos = null;
        Fcostos = new TreeMap();
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_CENTRO_COSTOS";//JJCastro fase2
        
         try {
             con = this.conectarJNDI(query);
             if (con != null) {
                 st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                 rs = st.executeQuery();

                 while (rs.next()) {
                     Fcostos.put(rs.getString("descripcion"), rs.getString("codigo"));
                 }

             }
         }
        catch(SQLException e){
            throw new SQLException("ERROR buscarCentrocostos " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
     /**
      * Getter for property Fremision.
      * @return Value of property Fremision.
      */
     public java.util.TreeMap getFremision() {
         return Fremision;
     }
     
     /**
      * Setter for property Fremision.
      * @param Fremision New value of property Fremision.
      */
     public void setFremision(java.util.TreeMap Fremision) {
         this.Fremision = Fremision;
     }
     
     /**
      * Getter for property Fcostos.
      * @return Value of property Fcostos.
      */
     public java.util.TreeMap getFcostos() {
         return Fcostos;
     }
     
     /**
      * Setter for property Fcostos.
      * @param Fcostos New value of property Fcostos.
      */
     public void setFcostos(java.util.TreeMap Fcostos) {
         this.Fcostos = Fcostos;
     }
     
     
     public String consultarCentro(String codigo) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String contenido="";
        String query = "SQL_BCENTRO_COSTO";//JJCastro fase2
         try {
             con = this.conectarJNDI(query);
             if (con != null) {
                 st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                 st.setString(1, codigo);
                 System.out.println("query de centro " + st.toString());
                 rs = st.executeQuery();
                 if (rs.next()) {
                     contenido = (rs.getString("descripcion") != null) ? rs.getString("descripcion") : "";
                 }
             }
         }
        catch(Exception e){
            throw new SQLException(
            "ERROR DURANTE EL PROCESO DE SQL_BCENTRO_COSTO " +
            e.getMessage() + " " /*+ e.getErrorCode()*/ );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return contenido;
        
    }

/**
 *
 * @throws Exception
 */
     public void buscarCompensac()throws Exception{
        Connection con = null;//JJCastro fase2
        fcompensac = null;
        fcompensac = new TreeMap();
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_COMPENSAC";//JJCastro fase2
         try {
             con = this.conectarJNDI(query);
             if (con != null) {
                 st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                 rs = st.executeQuery();

                 while (rs.next()) {
                     fcompensac.put(rs.getString("descripcion"), rs.getString("codigo"));
                 }

             }
         }
        catch(SQLException e){
            throw new SQLException("ERROR SQL_COMPENSAC " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
     
     /**
      * Getter for property fcompensac.
      * @return Value of property fcompensac.
      */
     public java.util.TreeMap getFcompensac() {
         return fcompensac;
     }
     
     /**
      * Setter for property fcompensac.
      * @param fcompensac New value of property fcompensac.
      */
     public void setFcompensac(java.util.TreeMap fcompensac) {
         this.fcompensac = fcompensac;
     }

/**
 * 
 * @param codigo
 * @return
 * @throws SQLException
 */
      public String consultarDescripcionRemesion (String codigo) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String contenido="";
        String query = "SQL_BDESCRIPCION_REMISION";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, codigo );
            System.out.println("query de SQL_BDESCRIPCION_REMISION "+st.toString());
            rs = st.executeQuery();
            if(rs.next()){
                  contenido = (rs.getString("descripcion")!=null)?rs.getString("descripcion"):"";
             }
        }}
        catch(Exception e){
            throw new SQLException(
            "ERROR DURANTE EL PROCESO DE SQL_BDESCRIPCION_REMISION " +
            e.getMessage() + " " /*+ e.getErrorCode()*/ );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return contenido;
        
    }
 
/**
 * 
 * @param codigo
 * @return
 * @throws SQLException
 */
    public String consultarCompensac (String codigo) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String contenido = "";
        String query = "SQL_DESCRIPCION_COMPENSAC";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, codigo);
                System.out.println("query de SQL_DESCRIPCION_COMPENSAC " + st.toString());
                rs = st.executeQuery();
                if (rs.next()) {
                    contenido = (rs.getString("descripcion") != null) ? rs.getString("descripcion") : "";
                }
            }
        }
        catch(Exception e){
            throw new SQLException(
            "ERROR DURANTE EL PROCESO DE SQL_DESCRIPCION_COMPENSAC " +
            e.getMessage() + " " /*+ e.getErrorCode()*/ );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return contenido;
        
    }  
     
}
