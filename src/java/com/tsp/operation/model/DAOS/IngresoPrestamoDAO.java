 /***************************************
    * Nombre Clase ............. IngresoPrestamoDAO.java
    * Descripci�n  .. . . . . .  Ejecuta los SQL  para recibir la cancelacion de cuotas de un prestamo
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  10/10/2005
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/



package com.tsp.operation.model.DAOS;



import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;




public class IngresoPrestamoDAO extends MainDAO{
    
    
    public IngresoPrestamoDAO() {
         super("IngresoPrestamoDAO.xml");
    }
    
    
    
    
    
    
      /**
     * M�todo que busca el prestamo  
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... String distrito, String tercero
     * @version..... 1.0.
     **/
     public Prestamo getPrestamos (String distrito, String tercero, String prestamo)throws Exception{
        Connection con = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        Prestamo          pt          = null;
        String            query       = "SQL_PRESTAMO_INGRESO";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,  distrito       );
            st.setString(2,  tercero        );
            st.setInt   (3,  Integer.parseInt(prestamo) );
            rs = st.executeQuery();
            if(rs.next()){
                pt = new Prestamo();
                
                pt.setDistrito          ( rs.getString("dstrct")       );
                pt.setTercero           ( rs.getString("tercero")      );
                pt.setId                ( rs.getInt   ("id")           );
                pt.setBeneficiario      ( rs.getString("beneficiario") );
                pt.setBeneficiarioName  ( rs.getString("payment_name") );
                pt.setMonto             ( rs.getDouble("monto")        );
                pt.setIntereses         ( rs.getDouble("interes")      );
                pt.setCuotas            ( rs.getInt   ("cuotas")       );
                pt.setFrecuencias       ( rs.getInt   ("periodos")     );
                pt.setTasa              ( rs.getDouble("tasa")         );                
                pt.setCapitalDescontado (rs.getDouble("vlracu_capdes") );
                pt.setInteresDescontado (rs.getDouble("vlracu_intdes") );
            }
            
            
        }}catch(Exception e) {
             throw new SQLException(" getPrestamos  -> " +e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return pt;
    }     
     
     
     
     
     
     
      /**
     * M�todo que busca las amortizaciones no canceladas para un prestamo
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... String distrito, String tercero
     * @version..... 1.0.
     **/
     public List getAmortizaciones(String distrito, String tercero, String prestamo)throws Exception{
         Connection con = null;
         PreparedStatement st          = null;
        ResultSet         rs          = null;
        List              lista       = new LinkedList();
        String            query       = "SQL_CUOTAS_POR_CANCELAR";
        try{
            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,  distrito       );
            st.setString(2,  tercero        );
            st.setInt   (3,  Integer.parseInt(prestamo) );
            rs = st.executeQuery();
            while(rs.next()){
                Amortizacion amort = new Amortizacion();
                   amort.setDstrct     ( rs.getString("dstrct")        );
                   amort.setPrestamo   ( rs.getString("prestamo")      );
                   amort.setItem       ( rs.getString("item")          );
                   amort.setTercero    ( rs.getString("tercero")       );
                   amort.setFechaPago  ( rs.getString("fecha_pago")    );
                   amort.setMonto      ( rs.getDouble("valor_monto")   );
                   amort.setCapital    ( rs.getDouble("valor_capital") );
                   amort.setInteres    ( rs.getDouble("valor_interes") );
                   amort.setTotalAPagar( rs.getDouble("valor_a_pagar") );
                   amort.setSaldo      ( rs.getDouble("valor_saldo")   );
                
                lista.add(amort);
           amort = null;//Liberar Espacio JJCastro
            }
            
            
        }}catch(Exception e) {
             throw new SQLException(" getPrestamos  -> " +e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }     
     
     
     
     
    
     /**
     * M�todo que cancela 
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... String distrito, String tercero
     * @version..... 1.0.
     **/
     public void cancelarPagoCuota(Amortizacion amort, String banco, String sucursal, String cheque, String corrida, String user)throws Exception{
        Connection         con       = null;
        PreparedStatement  st        = null; 
        String             query     = "SQL_CANCELAR_CUOTA";
        try{
            
            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
            String hoy = Util.getFechaActual_String(4);
            String sql = this.obtenerSQL( query ).replaceAll("#FECHA#", hoy).replaceAll("#BANCO#", banco).replaceAll("#SUCURSAL#", sucursal).replaceAll("#CHEQUE#", cheque).replaceAll("#CORRIDA#", corrida);
            
                st = con.prepareStatement(sql);
                st.setString(1, user);
                st.setString(2, amort.getDstrct());
                st.setString(3, amort.getTercero());
                st.setString(4, amort.getPrestamo());
                st.setString(5, amort.getItem());
                st.execute();
            
        }}catch(Exception e) {
             throw new SQLException(" cancelarPagoCuota  -> " +e.getMessage());
        }finally{
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }     
     
     
    
}
