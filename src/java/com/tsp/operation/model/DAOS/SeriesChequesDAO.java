/************************************************************************************
 * Nombre clase : ............... SeriesChequesDAO.java                             *
 * Descripcion :................. Permite manejar las series de cheque              *
 * Autor :....................... ING. FERNEL VILLACOB                              *
 * Fecha :....................... ABRIL 13 - 2007                                   *
 * Version :..................... 1.0                                               *
 * Copyright :................... Fintravalores S.A.                           *
 ************************************************************************************/





package com.tsp.operation.model.DAOS;



import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.Series;
import com.tsp.operation.model.beans.*;




public class SeriesChequesDAO  extends MainDAO {
    
   
    
    public SeriesChequesDAO() {
        super("SeriesChequesDAO.xml");
    }
    public SeriesChequesDAO(String dataBaseName) {
        super("SeriesChequesDAO.xml", dataBaseName);
    }
    
    
    
    
    public String reset(String val){
        return (val==null)?"":val;
    }
    
    
    
    
    
     /**
     * M�todo que carga la serie de cheque
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/
    public Series  getSeries( String distrito, String banco, String sucursal, String concepto,  String agencia, String usuario ) throws Exception{
        Connection con = null;
        PreparedStatement st      = null;
        ResultSet         rs      = null;
        Series            serie   = null;
        String            query   = "GET_SERIE";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito );
            st.setString(2, usuario  );
            st.setString(3, concepto );            
            st.setString(4, banco    );
            st.setString(5, sucursal );
            st.setString(6, agencia  );
            rs = st.executeQuery();
            
            if( rs.next() ){
                  serie = new Series();                   
                 
                  serie.setId                (        rs.getInt   ("id")                  );
                  serie.setConcepto          ( reset( rs.getString("concepto")          ) ); 
                  serie.setDstrct            ( reset( rs.getString("dstrct")            ) );                  
                  serie.setBanco             ( reset( rs.getString("branch_code")       ) );
                  serie.setAgencia_Banco     ( reset( rs.getString("bank_account_no")   ) );  
                  serie.setCuenta            ( reset( rs.getString("account_number")    ) );
                  serie.setAgency_id         ( reset( rs.getString("agency_id")         ) );
                  serie.setPrefijo           ( reset( rs.getString("prefix")            ) );
                  serie.setSerial_initial_no ( reset( rs.getString("serial_initial_no") ) );
                  serie.setSerial_fished_no  ( reset( rs.getString("serial_fished_no")  ) ); 
                  serie.setLast_number       (        rs.getInt("last_number")            ); 
                  serie.setDistrito          ( serie.getDstrct()                          );
                  
            }
            
            
            }}catch(Exception e){
            e.printStackTrace();
            throw new SQLException("Error en getSeries --->" + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return serie;        
    }
    
    
    
    
    
    
    
    
    /**
     * M�todo que devuelve el SQL para actualizar la serie
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/
    public String  updateSerieSQL( String id, String usuario ) throws Exception{        
        Connection con = null;
        StringStatement st      = null;
        String            query   = "ACTUALIZAR_SERIE";
        String            sql     = "";
        try{  
            
            st = new StringStatement(this.obtenerSQL(query),true);//JJCastro fase2
            st.setString(1, usuario );
            st.setString(2, id      );
            
            st.setString(3, id      );
            
            st.setString(4, usuario );
            st.setString(5, id      );            
            
            sql = st.getSql();
            
           }catch(Exception e){
            throw new SQLException("Error updateSerieSQL --->" + e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){st = null;}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sql;        
    }
    
    
    
    
    
    
    /**
     * M�todo que ejecuta la  actualizaci�n de la serie
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/
    public void  updateSerieEXE( String id, String usuario ) throws Exception{        
        Connection con = null;
        PreparedStatement st      = null;
        String            query   = "ACTUALIZAR_SERIE";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, usuario );
            st.setString(2, id      );            
            st.setString(3, id      );            
            st.setString(4, usuario );
            st.setString(5, id      );            
            
            st.executeQuery();
            
            }}catch(Exception e){
            throw new SQLException("Error updateSerieEXE --->" + e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    
    
    
    
    
    
     /**
     * M�todo que devuelve el SQL para actualizar la serie
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/
    public String  updateSerieNoSQL( String id, int last, String usuario ) throws Exception{        
        StringStatement st      = null;
        String            query   = "ACTUALIZAR_SERIE_NO";
        String            sql     = "";
        try{  
            

            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
            st.setInt   (1, last    );
            st.setString(2, usuario );
            st.setString(3, id      );            
            st.setString(4, id      );            
            st.setString(5, usuario );
            st.setString(6, id      );            
            
            sql = st.getSql();//JJCastro fase2
            
          }catch(Exception e){
            throw new SQLException("Error updateSerieNoSQL --->" + e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;            } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;        
    }
    
    
    
}
