/*
 * USUARIODAO.java
 *
 * Created on 19 de noviembre de 2004, 04:04 PM
 */

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.EncriptionException;

/**
 * Clase modificada por Alejandro Payares para adaptarla al sistema de consultas
 * SQL por medio de archivos XML. noviembre 17 de 2005.
 * @author  KREALES
 */
public class UsuarioDAO extends MainDAO {
    
    /** Creates a new instance of USUARIODAO */
    Usuario usuario;
    TreeMap Tusuarios;
    Vector vUsuarios;
    
    
    //DBastidas 16.12.05
    private String BUSCAR_EMAIL = "SELECT email " +
    "FROM usuarios "+
    "WHERE perfil = ? " +
    "      AND email LIKE '%@%.com%' ";
    
    //DLamadrid 23.12.05
    public static final String  USUARIOS_APROBACION="SELECT * FROM usuarios";
    
    //DLamadrid 05.01.06
    public static final String  USUARIOS_POR_DEPARTAMENTO="SELECT idusuario,nombre FROM usuarios where dpto=?";
    
    //DLamadrid 17.01.06
    private String USUARIOS_POR_NOMBRE = "Select idusuario, nombre from usuarios where nombre like upper(?) order by idusuario";
    private String EXISTE_USUARIO = "Select idusuario from usuarios where idusuario=?";
    
    //Dbastidas
    public static final String  LISTAR_USUARIOS="SELECT nombre, idusuario FROM usuarios ORDER BY nombre";
    
    public UsuarioDAO() {
        super("UsuarioDAO.xml");
    }
    
    private static final String SQL_OBTENER_USUARIO = "select * from Usuarios where upper(idusuario) = ?";
    private String S_EULC = "SELECT upper(idusuario) FROM usuarios WHERE upper(idusuario) = ? AND claveencr = ?";






    /**
     *
     * @param login
     * @throws SQLException
     */
    public void searchUsuario(String login )throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;        
        usuario=null;
        String query = "SQL_SEARCH_USUARIO";
        try {  
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,login.toUpperCase());
                rs = st.executeQuery();
                if(rs.next()){
                    usuario = Usuario.load(rs,true);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL USUARIO" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    public void searchUsuario(String login, String distrito )throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;        
        usuario=null;
        String query = "SQL_SEARCH_USUARIO_2";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,login.toUpperCase());
                st.setString(2,distrito.toUpperCase());
                rs = st.executeQuery();
                if(rs.next()){
                    usuario = Usuario.load(rs,true);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL USUARIO" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    public Usuario getUsuario() throws SQLException{
        return usuario;
    }



/**
 *
 * @param idusuario
 * @return
 * @throws SQLException
 */
    public Usuario obtenerUsuario(String idusuario) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_USUARIO";

        try {
            Usuario u = null;
            con = this.conectarJNDIFintra();
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1,idusuario.toUpperCase());
                rs = ps.executeQuery();
                if (rs.next()){
                    u = Usuario.load(rs,false);
                    // APAYARES 20060224
                    u.setPerfiles(buscarPerfilesDeUsuario(idusuario));
                    usuario = u;
                }
                return u;
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DE REPORTES VIAJES " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return null;
    }
    
    //////////sandrameg
    /**
     * 
     * @param login
     * @param claveEncr
     * @return
     * @throws SQLException
     */
    public boolean validarClave(String login, String claveEncr) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean ex = false;
        String query = "S_EULC";
        try{
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, login.toUpperCase());
                st.setString(2, claveEncr);
                rs = st.executeQuery();
                if ( rs.next()){
                    ex = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL LA VERIFICACION DE LA CLAVE DE USUARIO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return ex;
    }

/**
 * 
 * @param usuario
 * @param dstrct
 * @return
 * @throws SQLException
 */
    public boolean verificarDistrito(String usuario, String dstrct) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean respuesta = false;
        String query = "SQL_VERIFICAR_DISTRITO";
        
        try{
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, usuario.toUpperCase());
                st.setString(2, dstrct);
                rs = st.executeQuery();
                
                if (rs.next()){
                    respuesta = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL LA VERIFICACION DEL DISTRITO DEL USUARIO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;
    }


/**
 *
 * @param usuario
 * @param perfil
 * @return
 * @throws SQLException
 */
    public boolean verificarPerfil(String usuario, String perfil) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean respuesta = false;
        String query = "SQL_VERIFICAR_PERFIL";
        
        try{
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, usuario.toUpperCase());
                st.setString(2, perfil);
                rs = st.executeQuery();
                if (rs.next()){
                    respuesta = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL LA VERIFICACION DEL PERFIL DEL USUARIO "+ e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;
    }


/**
 *
 * @param usuario
 * @param dstrct
 * @param proyecto
 * @return
 * @throws SQLException
 */
    public boolean verificarProyecto(String usuario, String dstrct, String proyecto) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String vcampo = "", campo = "";
        boolean respuesta = false;
        String query = "SQL_VERIFICARPROYECTO";
        
        try{
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, usuario.toUpperCase());
                st.setString(2, proyecto);
                st.setString(3, dstrct);
                rs = st.executeQuery();
                if (rs.next()){
                    respuesta = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL LA VERIFICACION DEL PROYECTO DEL USUARIO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;
    }



    /**
     *
     * @param login
     * @param nclave
     * @throws SQLException
     */
    public void actualizarClave(String login, String nclave) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_QUERY_ACTUALIZAR_CLAVE";
        
        try{
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, nclave);
                st.setString(2, login.toUpperCase());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL LA ACTUALIZACION DE LA CLAVE DE USUARIO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }



 /**
  * 
  * @param login
  * @throws SQLException
  */
    public void actualizarFechasInc(String login) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ACTUALIZAR_FECHAS_INC";
        
        try{
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, login.toUpperCase());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL LA ACTUALIZACION DE LAS FECHAS " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    ///210905
    
    private String consultar_usuarios = "Select * from usuarios where reg_status = '' order by idusuario";
    private Vector usuarios;


 /**
  * 
  * @throws SQLException
  */
    public void obtenerUsuarios() throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        usuarios=null;
        String query = "SQL_CONSULTAR_INFO_USUARIOS";
        
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                usuarios = new Vector();
                while(rs.next()){
                    usuario = Usuario.load(rs,false);
                    usuarios.addElement(usuario);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS USUARIOS" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    public Vector getUsuarios() throws SQLException{
        return usuarios;
    }
    
    /**
     * Getter for property Tusuarios.
     * @return Value of property Tusuarios.
     */
    public java.util.TreeMap getTusuarios() {
        return Tusuarios;
    }
    
    /**
     * Setter for property Tusuarios.
     * @param Tusuarios New value of property Tusuarios.
     */
    public void setTusuarios(java.util.TreeMap Tusuarios) {
        this.Tusuarios = Tusuarios;
    }
    
    // LOS SIGUIENTES M�TODOS Y/O ATRIBUTOS FUERON COPIADOS DE LA APLICACI�N SOT
    // PARA IMPLEMENTAR EL MODULO DE ADMINISTRACI�N DE USUARIOS.
    // Alejandro Payares - nov 18 de 2005 - 4:14 pm
    private transient List usuariosCreados;
    
    /**
     * M�todo para crear un usuario nuevo.
     * @param createArguments arreglo que contiene los datos necesarios para<br>
     *                        crear un usuario nuevo.
     *        Los valores para insertar un nuevo usuario vienen en
     *        el siguiente orden dentro del arreglo "createArguments":
     *        createArguments[0]  = Clave privada para encriptar clave del usuario.
     *        createArguments[1]  = request.getParameter("nombre");
     *        createArguments[2]  = request.getParameter("direccion");
     *        createArguments[3]  = request.getParameter("pais");
     *        createArguments[4]  = request.getParameter("ciudad");
     *        createArguments[5]  = request.getParameter("email");
     *        createArguments[6]  = request.getParameter("telefono");
     *        createArguments[7]  = request.getParameter("tipoUsuario");
     *        createArguments[8]  = request.getParameter("nit");
     *        createArguments[9]  = request.getParameter("clienteDestinat");
     *        createArguments[10]  = request.getParameter("estado");
     *        createArguments[11] = request.getParameter("idUser");
     *        createArguments[12] = request.getParameter("pass1");
     * @throws SQLException si aparece un error de base de datos
     * @throws EncriptionException si ocurre un error durante la encriptacion
     *         de la clave del usuario.
     */
    public void agregarUsuario(String[] createArguments, String [] perfiles)
    throws SQLException, EncriptionException {
        String passwordEncriptado = Util.encript(
        createArguments[0], createArguments[12]
        );
        Connection con = null;
        PreparedStatement pstmt = null;
        String query = "SQL_AGREGAR_USUARIO";
        try {
            // Buscar los clientes y meterlos en una lista
            con = this.conectarJNDIFintra();
            if (con != null) {
            pstmt = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            pstmt.setString( 1, createArguments[1] );
            pstmt.setString( 2, createArguments[2] );
            pstmt.setString( 3, createArguments[3] );
            pstmt.setString( 4, createArguments[4] );
            pstmt.setString( 5, createArguments[5] );
            pstmt.setString( 6, createArguments[6] );
            pstmt.setString( 7, createArguments[7] );
            pstmt.setString( 8, createArguments[8] );
            pstmt.setString( 9, createArguments[9] );
            pstmt.setString(10, createArguments[10] );
            pstmt.setString(11, createArguments[11] );//login
            pstmt.setString(12, passwordEncriptado  );
            //AMENDEZ 20050628
            pstmt.setString(13, createArguments[13] );
            pstmt.setString(14, createArguments[14] );
            pstmt.setString(15, createArguments[15] );// cia o distrito
            pstmt.setBoolean(16, Boolean.valueOf(createArguments[16] ));
            pstmt.setString(17, createArguments[17] );
            
            // APAYARES 20051121
            pstmt.setString(18, createArguments[18] );// base
            
            
            ////System.out.println("agregando usuario: "+pstmt);
            pstmt.executeUpdate();
            
            String project = obtenerProyecto(createArguments[18]);
            agregarUsuarioProyecto(createArguments[15],createArguments[11],project,createArguments[19],createArguments[18]);
            agregarUsuarioPerfil(   createArguments[19], // usuario de creacion
            createArguments[11], // usuario nuevo
            perfiles // perfil(es)
            );
            
            
            
        }}
        catch( Exception ex ){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (pstmt  != null){ try{ pstmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
        
    }
    
    
    /**
     * Permite asignarle un perfil a un usuario.
     * @param creationUser El usuario que est� creando el perfil, es decir el usuario en sesi�n
     * @param idUser El usuario al cual se le asignar� el perfil
     * @param perfil El perfil a asignar
     * @throws SQLException Si algun error ocurre en la base de datos.
     */
    private void agregarUsuarioPerfil(String creationUser, String idUser, String [] perfiles)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_INSERT_PERFIL_USUARIO";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            for( int i=0; i< perfiles.length; i++ ){
                st.setString(1, perfiles[i]);
                st.setString(2, idUser);
                st.setString(3, creationUser);
                st.executeUpdate();
                st.clearParameters();
            }
        }}catch(SQLException e){
            throw new SQLException("ERROR:  " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


 /**
  *
  * @param distrito
  * @param login
  * @param project
  * @param loggedUser
  * @param base
  * @throws SQLException
  */
    private void agregarUsuarioProyecto(String distrito, String login, String project, String loggedUser, String base) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_AGREGAR_USUARIO_PROYECTO";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito);
                st.setString(2, login.toUpperCase());
                st.setString(3, project);
                st.setString(4, loggedUser);
                st.setString(5, loggedUser);
                st.setString(6, base);
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR:  " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * M�todo para actualizar un usuario ya creado.
     * @param userData arreglo que contiene los datos necesarios para<br>
     *                        actualizar los datos del usuario.
     *        Deben venir en el siguiente orden:
     *        userData[0]  = Clave privada para encriptar clave del usuario.<BR>
     *        userData[1]  = Nombre<BR>
     *        userData[2]  = NIT<BR>
     *        userData[3]  = Direccion<BR>
     *        userData[4]  = Telefono<BR>
     *        userData[5]  = E-Mail<BR>
     *        userData[6]  = Pais de origen<BR>
     *        userData[7]  = Ciudad<BR>
     *        userData[8]  = Tipo<BR>
     *        userData[9]  = Estado<BR>
     *        userData[10] = ID de usuario<BR>
     *        userData[11] = Clave<BR>
     *        userData[12] = Valor que determina si la clave fue cambiada.<BR>
     *        userData[13] = Cod. del cliente o destinatario asociado.
     *                       Si no aplica, su valor es la cadena vac�a.<BR>
     *        userData[14] = C�digo de la agencia.<BR>
     *        userData[15] = Acceso al plan de viaje: "1": S�;  "readonly": No.
     * @throws SQLException si aparece un error de base de datos
     * @throws EncriptionException si ocurre un error durante la encriptacion
     *         de la clave del usuario.
     */
    public void actualizarUsuario(String [] userData, String [] perfiles)
    throws SQLException, EncriptionException {
        String accesoPlVj = (userData[15] != null ? userData[15] : "0");
        String cia = (userData[16] != null ? userData[16] : "FINV");
        boolean updatePass = (userData[17] != null ? true : false);
        String dpto = (userData[18] != null ? userData[18] : "");
        String sql = this.obtenerSQL("SQL_ACTUALIZAR_USUARIO");
        
        // Si la clave cambi�, actualice el usuario con la nueva clave.
        if( userData[12].equals("S") ) {
            String claveEncr = Util.encript(userData[0], userData[11]);
            sql += ",    claveencr = '" + claveEncr    + "'";
        }
        sql += " WHERE upper(idusuario) = '" + userData[10].toUpperCase() + "'";
        Connection con = null;
        PreparedStatement pstmt = null;
        try {
        con = this.conectarJNDIFintra();
       
        if (con != null) {
        pstmt = con.prepareStatement(sql);//JJCastro fase2
        pstmt.setString(1,userData[1]);
        pstmt.setString(2,userData[2]);
        pstmt.setString(3,userData[3]);
        pstmt.setString(4,userData[4]);
        pstmt.setString(5,userData[5]);
        pstmt.setString(6,userData[6]);
        pstmt.setString(7,userData[7]);
        pstmt.setString(8,userData[8]);
        pstmt.setString(9,userData[9]);
        pstmt.setString(10,userData[13]);
        pstmt.setString(11,userData[14]);
        pstmt.setString(12,accesoPlVj);
        pstmt.setString(13,cia);
        pstmt.setBoolean(14,updatePass);
        pstmt.setString(15,dpto);
        pstmt.setString(16,userData[19]);
        //pstmt.setString(17,userData[21]);
        //cia = 16, project= buscarlo con la base, user = 20, login = 20
            pstmt.executeUpdate();
            modificarUsuarioProyecto(   userData[16], //distrito
            obtenerProyecto(userData[19]), // project
            userData[20], // loggedUser
            userData[19], // base
            userData[10] // idUsuario
            );
            modificarPerfiles(  perfiles,
            userData[20], // loggedUser
            userData[10] // idUsuario
            );
        }}catch(SQLException e){
            throw new SQLException("ERROR:  " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (pstmt  != null){ try{ pstmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }



 /**
  * 
  * @param perfiles
  * @param loggedUser
  * @param idusuario
  * @throws SQLException
  */
      public void modificarPerfiles(String [] perfiles, String loggedUser, String idusuario)throws SQLException {
         Connection con = null;
         PreparedStatement pstmt = null;
         String query = "SQL_BORRAR_PERFILES";
        try {
            // eliminamos los perfiles actuales del usuario
             con = this.conectarJNDIFintra();
            if (con != null) {
            pstmt = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            pstmt.setString(1, idusuario.toUpperCase() );
            pstmt.executeUpdate();
            // asignamos los nuevos perfiles
            this.agregarUsuarioPerfil(loggedUser, idusuario, perfiles);
        }}catch(SQLException e){
            throw new SQLException("ERROR:  " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (pstmt  != null){ try{ pstmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    //SET dstrct = ?,project = ?,user_update = ?,base = ? where login = ?
 /**
  * 
  * @param distrito
  * @param project
  * @param loggedUser
  * @param base
  * @param idUsuario
  * @throws SQLException
  */
      public void modificarUsuarioProyecto(String distrito, String project, String loggedUser, String base, String idUsuario)throws SQLException {
          Connection con = null;
          PreparedStatement pstmt = null;
          String query = "SQL_MODIFICAR_USUARIO_PROYECTO";
          try {
            if ( this.existeUsuarioProyecto(distrito, idUsuario, project) ) {
                con = this.conectarJNDIFintra();
                if (con != null) {
                pstmt = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                pstmt.setString(1, distrito );
                pstmt.setString(2, project );
                pstmt.setString(3, loggedUser.toUpperCase() );
                pstmt.setString(4, base );
                pstmt.setString(5, idUsuario.toUpperCase() );
                pstmt.executeUpdate();
            }
            else {
                this.agregarUsuarioProyecto(distrito, idUsuario.toUpperCase(), project, loggedUser, base);
            }
        }}catch(SQLException e){
            throw new SQLException("ERROR:  " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (pstmt  != null){ try{ pstmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * M&eacute;todo que elimina un usuario.
     * @param userID ID de usuario a eliminar
     * @throws SQLException si aparece un error de base de datos
     */
    public void eliminarUsuario(String userID) throws SQLException {
        Connection con = null;
        String query = "SQL_ELIMINAR_USUARIO";
        if (userID == null) {
            throw new SQLException("Se requiere pasar el codigo del usuario a Eliminar");
        }
        PreparedStatement pstmt = null;
        try {            
            // Buscar los clientes y meterlos en una lista
            con = this.conectarJNDIFintra();
            if (con != null) {
            pstmt = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            pstmt.setString(1, userID.toUpperCase() );
            pstmt.setString(2, userID.toUpperCase() );
            pstmt.setString(3, userID.toUpperCase() );
            pstmt.executeUpdate();
        }}catch(SQLException e){
            throw new SQLException("ERROR:  " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (pstmt  != null){ try{ pstmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Busca los usuarios que fueron creados por el de esta sesi�n.
     * Nota: Hay que tener en cuenta si es administrador, los muestra a todos, y
     *       si es cliente adm. de transporte, solo muestra los destinatarios que
     *       fueron creados por �ste.
     * @param userLoggedIn Usuario activo.
     * @throws SQLException Si un error de acceso a la base de datos ocurre.
     */
    public void usuariosCreadosSearch(final Usuario userLoggedIn, boolean guardarEnTreeMap)throws SQLException {
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        String query = "";
        if( userLoggedIn.getTipo().equals("CLIENTEADM") ){
            query = this.obtenerSQL("SQL_USUARIOS_CREADOS_SEARCH_CLIENTEADM");
            query = query.replaceAll("replacement_clienteDestinatario", userLoggedIn.getClienteDestinat() );
        }
        else {
            query = this.obtenerSQL("SQL_USUARIOS_CREADOS_SEARCH_ADMIN");
            //////System.out.println("buscando usuarios para tipo = "+userLoggedIn.getTipo());
        }
        Connection con = null;
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                // Seleccionar opciones activas del perfil.
                pstmt = con.prepareStatement(query);
                rs = pstmt.executeQuery();

                usuariosCreados = null;
                if (guardarEnTreeMap) {
                    this.Tusuarios = new TreeMap();
                } else {
                    usuariosCreados = new LinkedList();
                }
                while (rs.next()) {
                    Usuario u = Usuario.load(rs, false);
                    u.setNombre_agencia(rs.getString("nombre_agencia"));
                    if (guardarEnTreeMap) {
                        Tusuarios.put(u.getNombre(), u.getLogin());
                    } else {
                        usuariosCreados.add(u);
                    }
                }
            }
        } catch(SQLException e){
            throw new SQLException("ERROR:  " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (pstmt  != null){ try{ pstmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    public void usuariosCreadosSearch(final Usuario userLoggedIn)throws SQLException {
        this.usuariosCreadosSearch(userLoggedIn,false);
    }
    
    /**
     * Busca aquellos usuarios visibles para el loggedUser dado cuyo id de usuario comienze
     * con los criterios de busqueda dados
     * @param userLoggedIn El usuario en sesi�n
     * @param busqueda El texto de busqueda
     * @throws SQLException Si alg�n error ocurren en la base de datos.
     * @autor Alejandro Payares
     */
    public void buscarUsuarios(Usuario userLoggedIn, String busqueda)throws SQLException {
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        String query = "";
        if( userLoggedIn.getTipo().equals("CLIENTEADM") ){
            query = this.obtenerSQL("SQL_BUSQUEDA_USUARIOS_CLIENTEADM");
            query = query.replaceAll("replacement_clienteDestinatario", userLoggedIn.getClienteDestinat() );
        }
        else {
            query = this.obtenerSQL("SQL_BUSQUEDA_USUARIOS_ADMIN");
        }
        query = query.replaceAll("replacement_busqueda", busqueda.toUpperCase() + "%" );
        Connection con = null;
        try {
            con = this.conectarJNDIFintra();//JJCastro fase2
            pstmt = con.prepareStatement( query );
            rs = pstmt.executeQuery();
            usuariosCreados = null;
            if ( rs.next() ) {
                usuariosCreados = new LinkedList();
                do {
                    Usuario u = Usuario.load(rs,false);
                    u.setNombre_agencia(rs.getString("nombre_agencia"));
                    usuariosCreados.add( u );
                }while( rs.next() );
            }            
        }catch(SQLException e){
            throw new SQLException("ERROR:  " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (pstmt  != null){ try{ pstmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

/**
 * 
 * @param idusuario
 * @return
 * @throws SQLException
 */
    public Vector buscarPerfilesDeUsuario(String idusuario)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_PERFILES_USUARIO";
        Vector v = null;
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,idusuario);
            rs = st.executeQuery();
             v = new Vector();
            while( rs.next() ){
             v.addElement(rs.getString("perfil"));
            }            
            }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PROYECTO" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return v;
    }
    
    /**
     * Retorna una lista (buscada previamente por el m�todo {@link
     * #UsuariosCreadosSearch(Usuario) UsuariosCreadosSearch()}) con
     * los usuarios creados por otro.
     * @return Lista de usuarios encontrados.
     */
    public List getUsuariosCreados() {
        return usuariosCreados;
    }
    
    public String obtenerProyecto(String base)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_PROYECTO";
        String proy = "";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,base);
            rs = st.executeQuery();

            if(rs.next()){
              proy =  rs.getString(1);
            }

            }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PROYECTO" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return proy;
    }
    //DBastidas 16.12.05
    /**
     * Metodo buscarEmailPerfil, buasca los email de los usuarios relacionados,
     * a un perfil y retorna una cadena con los correos
     * @param: perfil
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public String buscarEmailPerfil(String perfil)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String correos="";
        boolean sw = false;
        String query = "BUSCAR_EMAIL";
        
        try {            
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,perfil);
                rs = st.executeQuery();
                
                while(rs.next()){
                    correos= correos + rs.getString("email")+";";
                    sw = true;
                }
                if(sw){
                    correos.substring(0,correos.length()-1);
                }
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA EMAIL DE USUARIO" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return correos;
        
    }
    
    
    //David 23.12.05
    /**
     * Este metodo retorna un Vector con los Usuarios de Aprobacion
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public Vector getUsuariosAprobacion() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        this.usuario = null;
        Vector vP;
        vP = new Vector();
        String query = "USUARIOS_APROBACION";
        try{
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                if(rs.next()){
                    Usuario us = new Usuario();
                    us.setIdusuario(rs.getString("idusuario").toUpperCase());
                    us.setNit(rs.getString("nit"));
                    us.setNombre(rs.getString("nombre"));
                    this.usuario = us;
                    vP.add(usuario);
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR USUARIOS_APROBACION: " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return vP;
    }
    
    /**
     * Metodo verifica si el usuario esta en un proyecto,
     * @return true si el usario se encuentra registrado en el proyecto
     * @param: distrito, usuario, proyecto
     * @autor : Ing. Alejandro Payares
     * @version : 1.0
     */
    public boolean existeUsuarioProyecto( String distrito, String idUsuario, String proyecto )throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_USUARIO_PROYECTO";
        boolean existe = false;//JJCastro fase2
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1,distrito);
            ps.setString(2,idUsuario.toUpperCase());
            ps.setString(3,proyecto);
            rs = ps.executeQuery();
            if(rs.next()){
                existe = true;
            }
        }}catch(SQLException e){
            throw new SQLException("ERROR existeUsuarioProyecto: " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return existe;
    }
    
    /**
     * Getter for property vUsuarios.
     * @return Value of property vUsuarios.
     */
    public java.util.Vector getVUsuarios() {
        return vUsuarios;
    }
    
    /**
     * Setter for property vUsuarios.
     * @param vUsuarios New value of property vUsuarios.
     */
    public void setVUsuarios(java.util.Vector vUsuarios) {
        this.vUsuarios = vUsuarios;
    }
    
     /* Procedimiento que genera un Vector con objetos tipo Usuarios pertenencientes a un departamento en especifico
      * parametros
      * @params String dpto ........ departamento de la empresa
      * @throws Exception.
      * @return
      * @autor : Ing. David Lamadrid
      * @version : 1.0
      * @see
      */
    public void getUsuariosPorDpto(String dpto) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "USUARIOS_POR_DEPARTAMENTO";
        this.usuario = null;
        this.vUsuarios = new Vector();
        try{
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dpto);
                rs = st.executeQuery();
                //////System.out.println("consulta "+st);
                while(rs.next()){
                    Usuario us = new Usuario();
                    us.setIdusuario(rs.getString("idusuario"));
                    us.setNombre(rs.getString("nombre"));
                    this.vUsuarios.add(us);
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL OBTENER EL PROVEEDOR " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    /**
     * Actualiza la ultima fecha en que el usuario dado entr� al sistema.
     * @param idusuario el id del usuario que ser� actualizado
     * @throws SQLException si algun error ocurre en la base de datos
     * @autor Alejandro Payares
     */
    public void actualizarFechaUltimoIngreso(String idusuario, String dstrct) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        String query = "SQL_ACTUALIZAR_FECHA_INGRESO";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, dstrct);
            ps.setString(2, idusuario);
            ps.executeUpdate();
        }}
        catch( Exception ex ){
            throw new SQLException("ERROR ACTUALIZANDO FECHA DE INGRESO: "+ex.getMessage());
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * verifica si existe un usuario en la tabla usuarios dado el login retorna false si no existe y true de lo contrario
     * @autor David Lamadrid
     * @throws SQLException
     * @version 1.0
     */
    public boolean existeUsuario(String login) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw=false;
        String query = "EXISTE_USUARIO";
        
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,login);
                rs = st.executeQuery();
                while(rs.next()){
                    sw=true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS USUARIOS" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
    
    /**
     * listarUsuarios lista los usuario registradosos
     * @param
     * @throws SQLException si algun error ocurre en la base de datos
     * @autor diogenes Bastidas
     */
    public Vector listarUsuarios() throws Exception {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        this.usuario = null;
        Vector vec = null;
        String query = "LISTAR_USUARIOS";
        try{
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();                
                vec = new Vector();
                while(rs.next()){
                    Usuario us = new Usuario();
                    us.setIdusuario(rs.getString("idusuario").toUpperCase());
                    us.setNombre(rs.getString("nombre"));
                    vec.add(us);
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL OBTENER EL USUARIO " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return vec;
    }
    /**
     * Setea un Vector con los registros de la tabla tabla usuarios que empiesen con la cadena de prametro nombre
     * @autor David Lamadrid
     * @throws SQLException
     * @version 1.0
     */
    public void obtenerUsuariosPorNombre(String nombre) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        usuarios=null;
        String query = "USUARIOS_POR_NOMBRE";

        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, nombre+"%");
                rs = st.executeQuery();
                usuarios = new Vector();
                while(rs.next()){
                    usuario = new Usuario();
                    usuario.setLogin(rs.getString("idusuario"));
                    usuario.setNombre(rs.getString("nombre"));
                    usuarios.add(usuario);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS USUARIOS" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
         /**
     * metodo que obtine el nombre de la agencia de un usuario
     * @autor Diogenes Bastidas
     * @throws SQLException
     * @version 1.0
     */
    public String obtenerAgenciaUsuario(String login) throws Exception{
        Connection con = null;
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        List              Lista    = new LinkedList();
        String agen = "";
        String query = "SQL_AGENCIA_USER";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, login);
            rs = st.executeQuery();
            while(rs.next()){
                agen = rs.getString("Agencia");
            }
        }}
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarUsuario [UsuarioDAO.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return agen;
    }
     /**
      * Metodo que retorna los datos de horas_zonificada dado un id de usuario
      * en un hashtable.
      * @parameter Login del usuario (String)
      * @autor Karen Reales
      * @throws SQLException
      * @version 1.0
      */
    public Hashtable getHoraZonificada(String login) throws Exception{
        Connection con = null;
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        Hashtable h = null;
        String query = "SQL_HORA_ZONIFICADA";
        
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, login);
            rs = st.executeQuery();
            if(rs.next()){
                h = new Hashtable();  
                h.put("horas", String.valueOf(rs.getInt("horaEntera")));
                h.put("minutos", String.valueOf(rs.getInt("minutosEntero")));
                h.put("segundos", String.valueOf(rs.getInt("segundosEntero")));
                
            }
            }}
        catch(Exception e) {
            throw new Exception("Error en rutina getHoraZonificada [UsuarioDAO.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return h;
    }

 /**
  * 
  * @param agencia
  * @throws SQLException
  */
     public void buscarUsuariosAgencia(String agencia)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Tusuarios=null;
         String query = "SQL_USUARIO_AGENCIA";
        
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,agencia);
                rs = st.executeQuery();
                Tusuarios = new TreeMap();
                Tusuarios.put("Seleccione un item", "");
                while(rs.next()){
                    Tusuarios.put(rs.getString("nombre"), rs.getString("idusuario"));
                }
                
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA LISTA DE USUARIOS POR AGENCIA" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
       /***
     * @param login
     * @throws SQLException
     */
    public boolean  ValidarOpcion(String login,String id_opcion )throws SQLException{
        boolean sw=false;
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        usuario=null;
        String query = "SQL_VALIDAR_OPCION";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,login.toUpperCase());
                st.setString(2,id_opcion);
                rs = st.executeQuery();
                if(rs.next()){
                   sw=true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA VALIDACION" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return sw;
    }
    
     public ArrayList<Usuario> buscarUsuariosPreaprobados()throws SQLException {
        ArrayList<Usuario> lista_usuarios=null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        String query = "SQL_BUSQUEDA_USUARIOS_PREAPROBADOS";
        Connection con = null;
        try {
            con = this.conectarJNDIFintra();
             pstmt = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = pstmt.executeQuery();
            if ( rs.next() ) {
                lista_usuarios= new   ArrayList();
                do {
                    Usuario u = new Usuario();
                    u.setIdusuario(rs.getString("idusuario"));
                    u.setPassword(rs.getString("clave"));
                    lista_usuarios.add(u);

                }while( rs.next() );
            }
        }catch(SQLException e){
            throw new SQLException("ERROR:  " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (pstmt  != null){ try{ pstmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista_usuarios;
    }
     
     public void saveRemoteAdress(String ipAddress, String login, String lat , String lon) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_SAVE_IP_ADDRESS";        
        try{
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, ipAddress);
                st.setString(2, login.toUpperCase());
                st.setString(3, login.toUpperCase());
                st.setString(4, lat);
                st.setString(5, lon);
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL LA ACTUALIZACION DE LA CLAVE DE USUARIO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

}