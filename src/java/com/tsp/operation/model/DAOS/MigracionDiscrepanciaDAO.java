/*
 * MigracionDiscrepanciaDAO.java
 *
 * Created on 14 de octubre de 2005, 09:30 AM
 */

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;

/**
 *
 * @author  dbastidas
 */
public class MigracionDiscrepanciaDAO extends MainDAO{
    
    /** Creates a new instance of MigracionDiscrepanciaDAO */
    public MigracionDiscrepanciaDAO() {
        super("MigracionDiscrepanciaDAO.xml");//JJCastro fase2
    }
    private MigracionDiscrepancia migdis;
    private Vector migDiscre;
         private static final  String listar  =
            "SELECT "+
            "       a.*,  "+
            "       b.descripcion AS desc_unidad,   "+
            "       c.nit         "+
            "FROM           "+
            "	(SELECT DISTINCT "+
            "		d.numpla,  "+
            "		d.creation_user as usuario,   "+
            "		To_Char(d.creation_date,'dd/MM/YY') as creacion,  "+
            "		d.fec_devolucion,              "+
            "		d.contacto,    "+                
            "		d.documento, "+
            "		COALESCE (dp.cod_unidad, '') AS cod_unidad, "+
            "		COALESCE (dp.cantidad,   0) AS cantidad, "+
            "		COALESCE (dp.descripcion, '') AS descripcion, "+
            "		COALESCE (dp.cod_producto, '') AS cod_producto, "+
            "		COALESCE (dp.cod_cdiscrepancia,'') AS cod_cdiscrepancia, "+
            "		COALESCE (dp.responsable, '') AS responsable,   "+
            "		COALESCE (dp.causa_dev, '') AS  causa_dev, "+
            "		d.fecmigracion "+
            "	FROM "+
            "		discrepancia d               "+
            "	LEFT JOIN discrepancia_producto dp ON ( dp.num_discre = d.num_discre AND dp.numpla = d.numpla AND dp.numrem = d.numrem AND dp.tipo_doc = d.tipo_doc  AND dp.documento = d.documento AND dp.tipo_doc_rel = d.tipo_doc_rel AND dp.documento_rel=d.documento_rel )    "+
            "	WHERE "+
            "		d.fecmigracion = '0099-01-01 00:00:00' AND "+
            "		d.base <> '' "+
            "	ORDER BY "+
            "		creacion, d.numpla  "+
            "	)A 	 "+
            "	LEFT JOIN tablagen B  ON (table_type = 'TUNIDAD' AND B.table_code = A.cod_unidad) "+
            "	LEFT JOIN usuarios C  ON (c.idusuario = A.usuario)";
        
    
    
      public static final String actualizar =  "update discrepancia set "+
                                            "  fecmigracion = ?,  "+
                                            "  usu_migracion = ?  "+
                                            "  where fecmigracion = '0099-01-01 00:00:00' and" +
                                            "        base <> '' ";
    
    
    public Vector MigrarDiscrepancia() throws SQLException {
        
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        migDiscre = null;
        String query = "SQL_LISTAR";
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                               
                migDiscre = new Vector(); 
                ////System.out.println(st);
                rs = st.executeQuery();
                
                while (rs.next()){
                    migdis = new MigracionDiscrepancia();
                    migdis.setOC(rs.getString("numpla"));
                    migdis.setItem("001");
                    migdis.setRaised_by(rs.getString("nit"));
                    migdis.setRaised_date(rs.getString("creacion"));
                    migdis.setDr_medium_ind("P");
                    migdis.setSupp_contated("N");
                    migdis.setSupp_contact("");
                    migdis.setDocumento( rs.getString("documento") );
                    migdis.setDiscrep_qty(rs.getString("cantidad"));
                    migdis.setDiscrep_oum(rs.getString("cod_unidad"));
                    migdis.setQi_code("");
                    migdis.setDr_desc(rs.getString("descripcion"));
                    migdis.setDr_ref(rs.getString("cod_producto"));
                    migdis.setDiscrp_type1(rs.getString("cod_cdiscrepancia"));
                    migdis.setDiscrp_type2(rs.getString("responsable"));
                    migdis.setDiscrp_type3(rs.getString("causa_dev"));
                    migdis.setDiscrp_type4("");
                    migdis.setDiscrp_type5("");
                    migdis.setDiscrp_type6("");
                    migdis.setHold_pmt_ind("Y");
                    migdis.setUpd_stats_ind("Y");
                    migdis.setCr_replace_ind("R");
                    migdis.setReplace_qty("");
                    migdis.setCredit_value("");
                    migDiscre.add(migdis); 
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL LISTAR IDENTIDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
       return migDiscre;
    }


/**
 * 
 * @param fecha
 * @param usuario
 * @throws SQLException
 */
    public void marcarMigracion(String fecha, String usuario) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        String query = "SQL_ACTUALIZAR";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, fecha);
                st.setString(2, usuario);
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL MARCAR LA DISCREPANCIA " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    } 
}
