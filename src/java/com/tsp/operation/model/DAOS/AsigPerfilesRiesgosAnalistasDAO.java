/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.AsigPerfilRiesgoAnalista;
import com.tsp.operation.model.beans.Rol;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
//import java.util.Vector;

/**
 *
 * @author Roberto Parra
 */
public class AsigPerfilesRiesgosAnalistasDAO extends MainDAO {
    
   /** Creates a new instance of PerfilDAO */
    public AsigPerfilesRiesgosAnalistasDAO() {
        super("AsigPerfilesRiesgoAnalistasDAO.xml", "fintra");
    }
    
    private AsigPerfilRiesgoAnalista AsigPerfil;
    private Rol Rol;
    /*
    private Vector vAnalistas;
    
       public Vector getVAnalistas() throws SQLException{
        return vAnalistas;
    }*/
      public ArrayList<AsigPerfilRiesgoAnalista> ListarAnalistas()throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_ANALISTAS";
        ArrayList<AsigPerfilRiesgoAnalista> lista= new ArrayList<>();
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                //st.setString( 1, usuario );
                rs = st.executeQuery();
               // vAnalistas = new Vector();                
                while(rs.next()){
                    AsigPerfil = new AsigPerfilRiesgoAnalista();
                    AsigPerfil.setIdusuario(rs.getString("idusuario"));
                    AsigPerfil.setNombre(rs.getString("nombre")); 
                    AsigPerfil.setIdrol(rs.getString("idrol"));
                    AsigPerfil.setRol(rs.getString("rol"));
                    AsigPerfil.setId_perfil(rs.getString("id_perfil"));
                    AsigPerfil.setPerfil(rs.getString("perfil"));
                    AsigPerfil.setEstado(rs.getString("estado"));
                    AsigPerfil.setPerfilAnalista(rs.getString("perfilAnalista"));
                    AsigPerfil.setMonto_minimo_decision(rs.getString("monto_minimo_decision"));
                    AsigPerfil.setMonto_decision(rs.getString("monto_decision"));
                    AsigPerfil.setEliminar("Eliminar");
                    AsigPerfil.setModificar("Modificar ");
                    lista.add(AsigPerfil);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return lista;
    }
      
       public ArrayList<AsigPerfilRiesgoAnalista> ListarAnalistasFabrica(String form)throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
          String query="";
        ArrayList<AsigPerfilRiesgoAnalista> lista= new ArrayList<>();
        if(form.equals("formModi")){
           query = "SQL_LISTAR_ANALISTAS_FABRICA_MODIFICACION";
         }else{
           query = "SQL_LISTAR_ANALISTAS_FABRICA_CREACION";
            }
        
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                //st.setString( 1, usuario );
                rs = st.executeQuery();
              //  vAnalistas = new Vector();                
                while(rs.next()){
                    AsigPerfil = new AsigPerfilRiesgoAnalista();
                    AsigPerfil.setIdusuario(rs.getString("idusuario")); 
                    AsigPerfil.setId_perfil(rs.getString("idperfil")); 
                    AsigPerfil.setEstado(rs.getString("estado")); 
                    AsigPerfil.setNombre(rs.getString("nombre"));
                    AsigPerfil.setMonto_minimo_decision(rs.getString("monto_minimo_decision"));
                    AsigPerfil.setMonto_decision(rs.getString("monto_decision"));
                    lista.add( AsigPerfil);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return lista;
    }
       
      public ArrayList<AsigPerfilRiesgoAnalista> ListarPerfiles()throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_PERFILES";
        ArrayList<AsigPerfilRiesgoAnalista> lista= new ArrayList<>();
        
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                //st.setString( 1, usuario );
                rs = st.executeQuery();
               // vAnalistas = new Vector();                
                while(rs.next()){
                    AsigPerfil = new AsigPerfilRiesgoAnalista();
                    AsigPerfil.setId_perfil(rs.getString("id")); 
                    AsigPerfil.setPerfil(rs.getString("descripcion"));
                    lista.add( AsigPerfil);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return lista;
    }
     
      
     public ArrayList<Rol> ListarRoles()throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_ROLES";
        ArrayList<Rol> lista= new ArrayList<>();
        
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                //st.setString( 1, usuario );
                rs = st.executeQuery();
               // vAnalistas = new Vector();                
                while(rs.next()){
                    Rol = new Rol();
                    Rol.setId(rs.getString("id")); 
                    Rol.setDescripcion(rs.getString("descripcion"));
                    lista.add(Rol);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return lista;
    }
     
    
     
      public ArrayList<AsigPerfilRiesgoAnalista> retornarPerfilesDelAnalista(String idusuario)throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_PERFILES_DEL_ANALISTA";
        ArrayList<AsigPerfilRiesgoAnalista> lista= new ArrayList<>();
        
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString( 1, idusuario );
                rs = st.executeQuery();
               // vAnalistas = new Vector();                
                while(rs.next()){
                    AsigPerfil = new AsigPerfilRiesgoAnalista();
                    AsigPerfil.setId_perfil(rs.getString("idperfil")); 
                    lista.add( AsigPerfil);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return lista;
    }
     
   
      
       public ArrayList<AsigPerfilRiesgoAnalista> RetornarDatosFormEdicion(String nit)throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_DATOS_FORMULRIO";
        ArrayList<AsigPerfilRiesgoAnalista> lista= new ArrayList<>();
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString( 1, nit );
                rs = st.executeQuery();
               // vAnalistas = new Vector();                
                while(rs.next()){
                    AsigPerfil = new AsigPerfilRiesgoAnalista();
                    AsigPerfil.setIdrol(rs.getString("idrol"));
                    lista.add(AsigPerfil);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL ROL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return lista;
    }     
      
      
      
      
   public String EliminarAsigRol(String nit)throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ELIMINAR_ASIGNACION_ROL";
        String respuesta="";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString( 1, nit );
             int est= st.executeUpdate();
           //  vAnalistas = new Vector();        
            if(est>0){
                  respuesta="ok";
            }
            else{
                  respuesta="Error al eliminar la asignacion";
            }
           
            
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
         return respuesta;
    }
   
      
      
    public String AsignarPerfiles(String nit, String  perfil, 
                                String  dstrct, String creation_user,String estado,
                                String monto_decision, String monto_minimo_decision)throws SQLException 
                             {
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ASIGNAR_PERFILES";
        String respuesta="";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
               // vAnalistas = new Vector();        
              if(nit.equals("")  || perfil.equals("") ||dstrct.equals("") || monto_decision.equals("") || monto_minimo_decision.equals("") )
              {
                  respuesta ="Todos los campos son obligatorios";
              }else{
                 
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, nit );
                st.setInt(2, Integer.parseInt(perfil) );
                st.setString( 3, dstrct );
                st.setString( 4, creation_user);
                st.setDouble( 5, Double.parseDouble(monto_decision));
                st.setDouble( 6, Double.parseDouble(monto_minimo_decision));
                st.setString( 7, estado);
                
             int est= st.executeUpdate();
             
            if(est>0){
                 respuesta ="ok";
            }else{  
                respuesta ="Error al asignar el perfil";
            }
              }  
            }else{  
                respuesta ="Error al asignar el perfil";
            }
        }catch(SQLException e){
             respuesta ="Error al asignar el perfil";
           // throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        
        return respuesta;
    }
    
    
        public String AsignarRol(String nit, String  idRol, 
                                String  dstrct, String creation_user)throws SQLException 
                             {
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ASIGNAR_ROL";
        String respuesta="";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
               // vAnalistas = new Vector();        
              if(nit.equals("")  ||  nit.equals("null")  || idRol.equals("")|| idRol.equals("null") || dstrct.equals("")|| dstrct.equals("null") ||creation_user.equals("")||creation_user.equals("null") )
              {
                  respuesta ="Todos los campos son obligatorios";
              }else{
                 
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, nit );
                st.setInt(2, Integer.parseInt(idRol) );
                st.setString( 3, dstrct );
                st.setString( 4, creation_user );
                
             int est= st.executeUpdate();
             
            if(est>0){
                 respuesta ="ok";
            }else{  
                respuesta ="Error al asignar el rol";
            }
              }  
            }else{  
                respuesta ="Error al asignar el rol";
            }
        }catch(SQLException e){
             respuesta ="Error al asignar el rol";
           // throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        
        return respuesta;
    }
        
    
   public String ModificarAsignacionRol(String  idRol,  String  dstrct, 
                                   String usuarioActual,String nit)throws SQLException 
                             {
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_MODIFICAR_ASIGNACION_ROL";
        String respuesta="";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
             //   vAnalistas = new Vector();        
              if(idRol.equals("") ||dstrct.equals("") ||usuarioActual.equals("") )
              {
                  respuesta="Todos los campos son obligatorios";
              }else{
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setInt(1, Integer.parseInt(idRol) );
                st.setString(2, dstrct );
                st.setString(3, usuarioActual );
                st.setString(4, nit );
                
             int est= st.executeUpdate();
             
            if(est>0){
                   respuesta="ok";
            }
            else{
                respuesta="Error al modificar la asignacion";
            }
              }  
              

            
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACION DE LA ASIGNACION " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return respuesta;
    }
   
          public String EliminarAsigPerfiles(String idanalista)throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ELIMINAR_ASOCIACION_DE_PERFILES";
        String respuesta="";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString( 1, idanalista );
             int est= st.executeUpdate();    
            if(est>0){
                 respuesta="ok";
            }
            else{
                respuesta ="Error al eliminar la relacion de los perfiles";
            }
           
            }
        }catch(SQLException e){
             respuesta ="Error al eliminar la relacion de los perfiles";
          //  throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return respuesta;
    }
      public String obtenerEstadoActualPerfil(String idanalista)throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_ESTADO_ASIGNACION_PERFIL";
        String estado="";
        try {
            
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString( 1, idanalista );
                rs = st.executeQuery();             
                while(rs.next()){
                    estado= rs.getString("estado"); 
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL ESTADO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return estado;
    }
      
          public String CambiarEstado(String nit,String idPerfil,String estado)throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_CAMBIAR_ESTADO";
        String respuesta="";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString( 1, estado );
                st.setString( 2, nit );
                st.setString( 3, idPerfil );
             int est= st.executeUpdate();    
            if(est>0){
                 respuesta="ok";
            }
            else{
                respuesta ="Error al cambiar el estado";
            }
           
            }
        }catch(SQLException e){
             respuesta ="Error al eliminar el perfil";
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return respuesta;
    }
}


