/* * electricaribeDAO.java * * Created on 1 de junio de 2009, 10:52 */
package com.tsp.operation.model.DAOS;

/** * * @author  Fintra */
import java.sql.*;
import com.tsp.operation.model.beans.*;
import java.util.*;

public class ClienteVerDAO extends MainDAO {

    String idusuario;
    private List listaPrefacturas;

    public ClienteVerDAO() {
        super("ClientesVerDAO.xml");
    }

 /**
  * 
  * @param cleca
  * @param usuario
  * @return
  * @throws Exception
  */
    public String insertarCl(ClienteEca cleca, String usuario) throws Exception {
        Connection con = null;
        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_INSERT_CLIENTE_ECA";
        try {
           st = new StringStatement (this.obtenerSQL(query), true);
            SerieGeneral s_id_cliente = getSerie("FINV", "OP", "CLIMS");
            setSerie("FINV", "OP", "CLIMS");
            String id_cliente = s_id_cliente.getUltimo_prefijo_numero();
            cleca.setId_cliente(id_cliente);
            st.setString(1, cleca.getId_cliente());
            st.setString(2, cleca.getNit());
            st.setString(3, cleca.getNombre());
            st.setString(4, cleca.getNombre_contacto());
            st.setString(5, cleca.getTel1());
            st.setString(6, cleca.getTel2());
            st.setString(7, cleca.getTipo());
            st.setString(8, cleca.getDepartamento());
            st.setString(9, cleca.getCiudad());
            st.setString(10, cleca.getDireccion());
            st.setString(11, cleca.getSector());
            st.setString(12, cleca.getNombre_representante());
            st.setString(13, cleca.getCargo_contacto());
            st.setString(14, cleca.getTel_representante());
            st.setString(15, cleca.getCelular_representante());
            st.setString(16, usuario);
            st.setString(17, usuario);
            st.setString(18, cleca.getId_ejecutivo());
            if (cleca.isOficial()) {
                st.setString(19, "S");
            } else {
                st.setString(19, "N");
            }
            respuesta = st.getSql();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return respuesta;
    }




    /**
     * 
     * @param hijo
     * @param padre
     * @param user
     * @return
     * @throws Exception
     */

    public String insertSubcliente(String hijo, String padre, String user) throws Exception {
        String query = "SQL_INSERTAR_SUBCLIENTE";
        String respuesta = "";
        StringStatement st = null;

        try {
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, hijo);
            st.setString(2, padre);
            st.setString(3, user);
            st.setString(4, user);
            respuesta = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_INSERTAR_SUBCLIENTE. \n " + e.getMessage());
        } finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }

        return respuesta;
    }

 /**
  * 
  * @param cleca
  * @param usuario
  * @return
  * @throws Exception
  */
    public String updatePadre(ClienteEca cleca, String usuario) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_UPDATE_PADRE";
        String sql = "";
        try {
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, cleca.getId_padre());
            st.setString(2, usuario);
            st.setString(3, cleca.getId_cliente());
            respuesta = st.getSql();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return respuesta;
    }

/**
 * 
 * @param ofca
 * @param usuario
 * @return
 * @throws Exception
 */
    public String insertarOf(OfertaElca ofca, String usuario) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_INSERT_OFERTA_ECA";
        try {
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            SerieGeneral s_id_cliente = getSerie("FINV", "OP", "OFMS");
            setSerie("FINV", "OP", "OFMS");
            ofca.setId_solicitud(s_id_cliente.getUltimo_prefijo_numero());

            st.setString(1, ofca.getId_solicitud());
            st.setString(2, ofca.getId_cliente());
            st.setString(3, ofca.getDescripcion());
            st.setString(4, usuario);
            st.setString(5, ofca.getNic());
            st.setString(6, ofca.getTipo_solicitud());
            st.setString(7, ofca.getTipo_solicitud());//091027
            st.setString(8, ofca.getOficial());
            respuesta = st.getSql();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return respuesta;
    }

/**
 * 
 * @param acceca
 * @param usuario
 * @return
 * @throws Exception
 */
    public String insertarAcc(AccionesEca acceca, String usuario) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_INSERT_ACCION_ECA";
        try {
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            SerieGeneral s_id_cliente = getSerie("FINV", "OP", "ACMS");
            setSerie("FINV", "OP", "ACMS");
            acceca.setId_accion(s_id_cliente.getUltimo_prefijo_numero());
            st.setString(1, acceca.getId_accion());
            st.setString(2, acceca.getId_solicitud());
            st.setString(3, acceca.getContratista());
            st.setString(4, acceca.getDescripcion());
            st.setString(5, usuario);
            respuesta = st.getSql();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return respuesta;
    }

/**
 * 
 * @param acceca
 * @param usuario
 * @return
 * @throws Exception
 */
    public String delAcc(AccionesEca acceca, String usuario) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_DELETE_ACCION_ECA";
        try {
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, usuario);
            st.setString(2, acceca.getId_accion());
            respuesta = st.toString();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return respuesta;
    }


/**
 * 
 * @param cleca
 * @param usuario
 * @param sw
 * @return
 * @throws Exception
 */
    public String updateCl(ClienteEca cleca, String usuario, boolean sw) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_UPDATE_CLIENTE_ECA";
        String sql = "";
        try {

            String param = "";
            param = param + "nit='" + cleca.getNit() + "', ";
            param = param + "nombre='" + cleca.getNombre() + "', ";
            param = param + "nombre_contacto='" + cleca.getNombre_contacto() + "', ";
            param = param + "tel1='" + cleca.getTel1() + "', ";
            param = param + "tel2='" + cleca.getTel2() + "', ";
            param = param + "tipo='" + cleca.getTipo() + "', ";
            param = param + "departamento='" + cleca.getDepartamento() + "', ";
            param = param + "ciudad='" + cleca.getCiudad() + "', ";
            param = param + "direccion='" + cleca.getDireccion() + "', ";
            param = param + "sector='" + cleca.getSector() + "', ";
            param = param + "nombre_representante='" + cleca.getNombre_representante() + "', ";
            param = param + "cargo_contacto='" + cleca.getCargo_contacto() + "', ";
            param = param + "tel_representante='" + cleca.getTel_representante() + "', ";
            param = param + "celular_representante='" + cleca.getCelular_representante() + "', ";
            param = param + "user_update='" + usuario + "', ";
            if (sw) {
                param = param + "id_ejecutivo='" + cleca.getId_ejecutivo() + "', ";
            }
            param = param + "esoficial='" + ((cleca.isOficial()) ? "S" : "N") + "', ";

           sql = this.obtenerSQL(query).replaceAll("#PARAM#", param);
           st = new StringStatement (sql, true);//JJCastro fase2
           st.setString(1, cleca.getId_cliente());
           respuesta = st.getSql();
           System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return respuesta;
    }



  /**
   * 
   * @param ofca
   * @param usuario
   * @return
   * @throws Exception
   */
    public String updateOf(OfertaElca ofca, String usuario) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_UPDATE_OFERTA_ECA";
        String sql = "";
        Connection conn = null;
        try {
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, ofca.getDescripcion());
            st.setString(2, ofca.getOficial());
            st.setString(3, usuario);
            st.setString(4, ofca.getId_solicitud());
            respuesta = st.getSql();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return respuesta;
    }


   
/**
 * 
 * @param idcliente
 * @param usuario
 * @param nics
 * @return
 * @throws Exception
 */
    public String insertarNics(String idcliente, String usuario, String[] nics) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_INSERT_NICS";
        try {
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            for (int i = 0; nics != null && i < nics.length; i++) {
                st.setString(1, nics[i]);
                st.setString(2, idcliente);
                st.setString(3, usuario);
                st.setString(4, usuario);
                respuesta = respuesta + st.getSql();
            }
            System.out.println(respuesta);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return respuesta;
    }


/**
 * 
 * @param cleca
 * @return
 * @throws Exception
 */
    public String buscarCl(ClienteEca cleca) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection conn = null;
        String respuesta = "";
        boolean sw = false;
        cleca.setCiudad("");
        cleca.setNombre_contacto("");
        cleca.setCelular_representante("");
        cleca.setDepartamento("");
        cleca.setDireccion("");
        cleca.setNit("");
        cleca.setCargo_contacto("");
        cleca.setNombre("");
        cleca.setNombre_representante("");
        cleca.setSector("");
        cleca.setTel1("");
        cleca.setTel2("");
        cleca.setTel_representante("");
        cleca.setTipo("");
        cleca.setCreation_user("");
        cleca.setId_ejecutivo("");
        cleca.setId_padre("");
        cleca.setNombre_padre("");

        try {
            conn = this.conectarJNDI("SQL_GET_CLIENTE");
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_CLIENTE"));
            st.setString(1, cleca.getId_cliente());
            rs = st.executeQuery();

            if (rs.next()) {
                cleca.setCiudad(rs.getString("ciudad"));
                cleca.setNombre_contacto(rs.getString("nombre_contacto"));
                cleca.setCelular_representante(rs.getString("celular_representante"));
                cleca.setDepartamento(rs.getString("departamento"));
                cleca.setDireccion(rs.getString("direccion"));
                cleca.setNit(rs.getString("nit"));
                cleca.setCargo_contacto(rs.getString("cargo_contacto"));
                cleca.setNombre(rs.getString("nombre"));
                cleca.setNombre_representante(rs.getString("nombre_representante"));
                cleca.setSector(rs.getString("sector"));
                cleca.setTel1(rs.getString("tel1"));
                cleca.setTel2(rs.getString("tel2"));
                cleca.setTel_representante(rs.getString("tel_representante"));
                cleca.setTipo(rs.getString("tipo"));
                cleca.setId_ejecutivo(rs.getString("nombre_ejecutivo"));
                cleca.setCreation_user(rs.getString("creation_user"));
                cleca.setNics(getNics(cleca.getId_cliente()));
                sw = true;
            }            

            if (sw) {
                st = conn.prepareStatement(this.obtenerSQL("SQL_GET_PADRE"));
                st.setString(1, cleca.getId_cliente());
                rs = st.executeQuery();

                if (rs.next()) {
                    cleca.setId_padre(rs.getString("id_cliente_padre"));
                    cleca.setNombre_padre(rs.getString("nombre"));
                }

            }


            if (!sw) {
                respuesta = "No hay ningun usuario con ese id";
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_CLENTE \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conn != null){ try{ this.desconectar(conn); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;
    }




/**
 * 
 * @param ofca
 * @return
 * @throws Exception
 */
    public String buscarOf(OfertaElca ofca) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection conn = null;
        String respuesta = "";
        boolean sw = false;
        ofca.setCreacion_fecha_entrega_oferta("");
        ofca.setCreation_date("");
        ofca.setCreation_user("");
        ofca.setDescripcion("");
        ofca.setFecha_entrega_oferta("");
        ofca.setId_cliente("");
        ofca.setId_oferta("");
        ofca.setLast_update("");
        ofca.setNic("");
        ofca.setNum_os("");
        ofca.setReg_status("");
        ofca.setUser_update("");
        ofca.setUsuario_entrega_oferta("");

        try {
            conn = this.conectarJNDI("SQL_GET_OFERTA");//JJCastro fase2
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_OFERTA"));
            st.setString(1, ofca.getId_solicitud());
            rs = st.executeQuery();
            if (rs.next()) {
                ofca.setCreacion_fecha_entrega_oferta(rs.getString("creacion_fecha_entrega_oferta"));
                ofca.setCreation_date(rs.getString("creation_date"));
                ofca.setCreation_user(rs.getString("creation_user"));
                ofca.setDescripcion(rs.getString("descripcion"));
                ofca.setFecha_entrega_oferta(rs.getString("fecha_entrega_oferta"));
                ofca.setId_cliente(rs.getString("id_cliente"));
                ofca.setId_oferta(rs.getString("id_oferta"));
                ofca.setLast_update(rs.getString("last_update"));
                ofca.setNic(rs.getString("nic"));
                ofca.setNum_os(rs.getString("num_os"));
                ofca.setReg_status(rs.getString("reg_status"));
                ofca.setUser_update(rs.getString("user_update"));
                ofca.setUsuario_entrega_oferta(rs.getString("usuario_entrega_oferta"));
                ofca.setTipo_solicitud(rs.getString("tipo_solicitud"));
                sw = true;
            }
            if (!sw) {
                respuesta = "No hay ninguna solicitud con ese id";
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_OFERTA \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conn != null){ try{ this.desconectar(conn); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;
    }



    /**
     *
     * @param acceca
     * @param nitprop
     * @return
     * @throws Exception
     */
    public ArrayList buscarAcc(AccionesEca acceca, String nitprop) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection conn = null;
        String respuesta = "";
        ArrayList arl = new ArrayList();
        boolean sw = false;
        try {
            conn = this.conectarJNDI("SQL_GET_ACCIONES_ECA");//JJCastro fase2
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_ACCIONES_ECA"));
            st.setString(1, acceca.getId_solicitud());
            if (nitprop.length() > 2 && nitprop.substring(0, 2).equals("CC")) {
                st.setString(2, nitprop);
            } else {
                st.setString(2, "%");
            }

            System.out.println("sqllll" + st.toString());
            rs = st.executeQuery();
            while (rs.next()) {
                AccionesEca newacc = new AccionesEca();
                newacc.setId_accion(rs.getString("id_accion"));
                newacc.setAdministracion(rs.getString("administracion"));
                newacc.setContratista(rs.getString("contratista"));
                newacc.setCreation_date(rs.getString("creation_date"));
                newacc.setCreation_user(rs.getString("creation_user"));
                newacc.setDescripcion(rs.getString("descripcion"));
                newacc.setEstado(rs.getString("estado"));
                newacc.setId_solicitud(rs.getString("id_solicitud"));
                newacc.setImprevisto(rs.getString("imprevisto"));
                newacc.setLast_update(rs.getString("last_update"));
                newacc.setMano_obra(rs.getString("mano_obra"));
                newacc.setMaterial(rs.getString("material"));
                newacc.setObservaciones(rs.getString("observaciones"));
                newacc.setPorc_administracion(rs.getString("porc_administracion"));
                newacc.setPorc_imprevisto(rs.getString("porc_imprevisto"));
                newacc.setPorc_utilidad(rs.getString("porc_utilidad"));
                newacc.setReg_status(rs.getString("reg_status"));
                newacc.setTipo_trabajo(rs.getString("tipo_trabajo"));
                newacc.setTransporte(rs.getString("transporte"));
                newacc.setUser_update(rs.getString("user_update"));
                newacc.setUtilidad(rs.getString("utilidad"));
                arl.add(newacc);
                sw = true;
            }
            if (!sw) {
                respuesta = "No hay ninguna accion con ese id";
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_ACCION \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conn != null){ try{ this.desconectar(conn); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arl;
    }

/**
 * 
 * @param param
 * @return
 * @throws Exception
 */
    public String[] getNics(String param) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        String[] arll = null;

        Connection conn = null;
        try {
            conn = this.conectarJNDI("SQL_GET_NICS");//JJCastro fase2
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_NICS"));
            st.setString(1, param);
            rs = st.executeQuery();
            while (rs.next()) {
                arl.add(rs.getString("nic"));
            }
            arll = new String[arl.size()];
            for (int i = 0; i < arl.size(); i++) {
                arll[i] = (String) arl.get(i);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_NICS \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conn != null){ try{ this.desconectar(conn); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arll;
    }


/**
 * 
 * @param param
 * @return
 * @throws Exception
 */
    public String buscarMails(String param) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        String arl = "";
        Connection conn = null;
        try {
            conn = this.conectarJNDI("SQL_GET_MAILS");//JJCastro fase2
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_MAILS"));
            st.setString(1, param);
            rs = st.executeQuery();
            while (rs.next()) {
                arl = arl + rs.getString("email") + ";";
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_MAILS \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conn != null){ try{ this.desconectar(conn); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arl;
    }

 /**
  *
  * @param param
  * @return
  * @throws Exception
  */
    public String getMailEjecutivo(String param) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        String arl = "";
        Connection conn = null;
        try {
            conn = this.conectarJNDI("SQL_GET_MAIL_EJECUTIVO");
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_MAIL_EJECUTIVO"));
            st.setString(1, param);
            rs = st.executeQuery();
            if (rs.next()) {
                arl = rs.getString("correo");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_MAIL_EJECUTIVO \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conn != null){ try{ this.desconectar(conn); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arl;
    }

/**
 *
 * @param param
 * @return
 * @throws Exception
 */
    public String getMailContratista(String param) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        String arl = "";

        Connection conn = null;
        try {
            conn = this.conectarJNDI("SQL_GET_MAIL_CONTRATISTA");//JJCastro fase2
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_MAIL_CONTRATISTA"));
            st.setString(1, param);
            rs = st.executeQuery();
            if (rs.next()) {
                arl = rs.getString("email");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_MAIL_CONTRATISTA \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conn != null){ try{ this.desconectar(conn); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arl;
    }

    
/**
 * 
 * @param param
 * @return
 * @throws Exception
 */
    public String getLoginEjecutivo(String param) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        String arl = "";
        Connection conn = null;
        try {
            conn = this.conectarJNDI("SQL_GET_LOGIN_EJECUTIVO");//JJCastro fase2
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_LOGIN_EJECUTIVO"));
            st.setString(1, param);
            rs = st.executeQuery();
            if (rs.next()) {
                arl = rs.getString("idusuario");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_LOGIN_EJECUTIVO \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conn != null){ try{ this.desconectar(conn); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arl;
    }


 /**
  *
  * @param param
  * @return
  * @throws Exception
  */
    public ArrayList getDatos(String param) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection conn = null;
        try {
            conn = this.conectarJNDI("SQL_GET_DATOS");
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_DATOS").replaceAll("#PARAM", param));
            rs = st.executeQuery();
            while (rs.next()) {
                arl.add(rs.getString("datos"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_SET_MS. \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conn != null){ try{ this.desconectar(conn); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arl;
    }


/**
 *
 * @return
 * @throws Exception
 */
    public ArrayList getEjecutivos() throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection conn = null;
        try {
            conn = this.conectarJNDI("SQL_GET_EJECUTIVOS");
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_EJECUTIVOS"));
            rs = st.executeQuery();
            while (rs.next()) {
                arl.add(new String[]{rs.getString("nombre"), rs.getString("id_ejecutivo") + "," + rs.getString("nombre")});
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_EJECUTIVOS. \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conn != null){ try{ this.desconectar(conn); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arl;
    }


/**
 * 
 * @return
 * @throws Exception
 */
    public ArrayList getPadres() throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection conn = null;
        try {
            conn = this.conectarJNDI("SQL_GET_PADRES");//JJCastro fase2
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_PADRES"));
            rs = st.executeQuery();
            while (rs.next()) {
                arl.add(new String[]{rs.getString("nombre"), rs.getString("id_cliente_padre")});
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_PADRES. \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conn != null){ try{ this.desconectar(conn); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arl;
    }

/**
 * 
 * @return
 * @throws Exception
 */
    public ArrayList getContratistas() throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection conn = null;
        try {
            conn = this.conectarJNDI("SQL_GET_CONTRATISTAS");//JJCastro fase2
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_CONTRATISTAS"));
            rs = st.executeQuery();
            while (rs.next()) {
                arl.add(new String[]{rs.getString("descripcion"), rs.getString("id_contratista")});
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_CONTRATISTAS. \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conn != null){ try{ this.desconectar(conn); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arl;
    }


/**
 * 
 * @return
 * @throws Exception
 */
    public ArrayList getEstado() throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection conn = null;
        try {
            conn = this.conectarJNDI("SQL_GET_ESTADOS");//JJCastro fase2
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_ESTADOS"));
            rs = st.executeQuery();
            while (rs.next()) {
                arl.add(new String[]{rs.getString("estado"), rs.getString("id_estado")});
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_ESTADOS. \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conn != null){ try{ this.desconectar(conn); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arl;
    }




/**
 * 
 * @param perfil
 * @param accion
 * @return
 * @throws Exception
 */
    public boolean ispermitted(String perfil, String accion) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection conn = null;
        String answ = "N";
        try {
            conn = this.conectarJNDI("SQL_GET_PERMISO");//JJCastro fase2
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_PERMISO"));
            st.setString(1, perfil);
            st.setString(2, accion);
            rs = st.executeQuery();
            if (rs.next()) {
                answ = rs.getString("dato");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_PERMISO. \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conn != null){ try{ this.desconectar(conn); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        if (answ.equals("S")) {
            return true;
        } else {
            return false;
        }
    }

/**
 * 
 * @param usuario
 * @return
 * @throws Exception
 */
    public String getPerfil(String usuario) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection conn = null;
        String answ = "";
        try {
            conn = this.conectarJNDI("SQL_GET_PERFIL");//JJCastro fase2
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_PERFIL"));
            st.setString(1, usuario);
            rs = st.executeQuery();
            if (rs.next()) {
                answ = rs.getString("dato");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_PERMISO. \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conn != null){ try{ this.desconectar(conn); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return answ;
    }


 /**
  * 
  * @param dstrct
  * @param agency_id
  * @param document_type
  * @return
  * @throws SQLException
  */
    public SerieGeneral getSerie(String dstrct, String agency_id, String document_type) throws SQLException {

        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SQL_GET_ULTIMO_NUMERO";
        SerieGeneral serie = null;
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, dstrct);
            st.setString(2, agency_id);
            st.setString(3, document_type);
            rs = st.executeQuery();
            if (rs.next()) {
                serie = new SerieGeneral();
                serie = (SerieGeneral.load(rs));
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE LA BUSQUEDA DE UNA SERIE. \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return serie;

    }



/**
 * 
 * @param dstrct
 * @param agency_id
 * @param document_type
 * @throws SQLException
 */
    public void setSerie(String dstrct, String agency_id, String document_type) throws SQLException {

        PreparedStatement st = null;
        Connection con = null;
        String query = "SQL_SET_ULTIMO_NUMERO";

        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, dstrct);
            st.setString(2, agency_id);
            st.setString(3, document_type);
            st.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA SERIE.  \n " + e.getMessage());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

    }


/**
 * 
 * @param comandosSQL
 * @throws SQLException
 */
    public void ejecutarSQL(Vector comandosSQL) throws SQLException {

        Connection con = null;
        Statement stmt = null;
        String query = "SQL_GET_CONECTION";
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
           if (con != null) {
                con.setAutoCommit(false);
                stmt = con.createStatement();
                for (int i = 0; i < comandosSQL.size(); i++) {
                    String comando = (String) comandosSQL.elementAt(i);
                    ////System.out.println(comando);
                    stmt.executeUpdate(comando);

                }

                con.commit();
            }
        } catch (SQLException e) {
            // Efectuando rollback en caso de error
            try {

                con.rollback();
            } catch (SQLException ignored) {
                System.out.println("error en execuuute" + ignored.toString());
                throw new SQLException("NO SE PUDO HACER ROLLBACK " + ignored.getMessage() + " " + ignored.getErrorCode() + " <br> La siguiente exception es : ----" + ignored.getNextException());
            }
            throw new SQLException("ERROR DURANTE LA TRANSACCION, LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS " + e.getMessage() + " " + e.getErrorCode() + " <br> La siguiente exception es : ----" + e.getNextException());
        }finally{
            if (stmt  != null){ try{ stmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


/**
 * 
 * @param accion
 * @return
 * @throws Exception
 */
    public String[] getDatosMensaje(String accion) throws Exception//091203
    {
        PreparedStatement st = null;
        ResultSet rs = null;
        String[] arll = new String[4];
        Connection conn = null;
        try {
            conn = this.conectarJNDI("SQL_GET_DATOS_MSG");//JJCastro fase2
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_DATOS_MSG"));
            st.setString(1, accion);
            rs = st.executeQuery();
            if (rs.next()) {
                arll[0] = rs.getString("nombre_contra");
                arll[1] = rs.getString("sector");
                arll[2] = rs.getString("nombre_cli");
                arll[3] = rs.getString("mail_contra");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("error en getDatosMensaje:" + e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_DATOS_MSG \n " + e.getMessage());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conn != null){ try{ this.desconectar(conn); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arll;
    }


/**
 * 
 * @param id
 * @return
 * @throws Exception
 */
    public boolean isOficial(String id) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_IS_OFICIAL";
        String sql = "";
        boolean ok = false;

        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            sql = this.obtenerSQL(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, id);
            rs = ps.executeQuery();

            if (rs.next()) {
                
                ok = true;
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return ok;
    }
}
