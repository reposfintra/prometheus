/***********************************************************************************
 * Nombre clase : ............... InfoActividadAction.java                         *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.                                       *
 * Autor :....................... Ing. Diogenes Antonio Bastidas Morales           *
 * Fecha :........................ 2 de septiembre de 2005, 03:40 PM                  *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  Diogenes
 */
public class InfoActividadDAO {
    
    /** Creates a new instance of InfoActividadAction */
    public InfoActividadDAO() {
    }
    private InfoActividad infoact;
    private Vector VecInfact;
    
    private static final String insertar = "insert into infoactividad (dstrct,codcli,cod_actividad,fecinicio,fecfin,duracion,documento,tiempodemora,causademora,resdemora,feccierre,observacion,base, cantrealizada, cantplaneada, referencia1, referencia2, refnumerica1, refnumerica2, creation_user,creation_date,user_update,last_update,numpla,numrem)" +
                                           "values ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )";
    
    private static String modificar="update                   "+
                                     "       infoactividad    "+
                                     "    set                 " +
                                     "     fecinicio = ?,     " +
                                     "     fecfin = ?,        " +
                                     "     duracion = ?,      " +
                                     "     documento = ?,     " +
                                     "     tiempodemora = ?,  " +
                                     "     causademora = ?, " +
                                     "     resdemora = ?," +
                                     "     feccierre = ?,  " +
                                     "     observacion = ?," +
                                     "     estado = ?,   " +
                                     "     user_update = ?, " +
                                     "     last_update = ?,  " +
                                     "     cantrealizada = ?, " +
                                     "     cantplaneada = ?," +
                                     "     referencia1 = ?," +
                                     "     referencia2 = ?," +
                                     "     refnumerica1 = ?," +
                                     "     refnumerica2 = ?  " +
                                     "  where              " +
                                     "         dstrct = ?    " +
                                     "     and codcli = ?    " +
                                     "     and cod_actividad= ?" +
                                     "     and numpla = ? " +
                                     "     and numrem = ? ";
    
    private static String buscar = " select *              " +
                                   "    from infoactividad " +
                                   " where                 " +
                                   "         estado = ''   " +
                                   "     and dstrct = ?    " +
                                   "     and codcli = ?    " +
                                   "     and cod_actividad= ?" +
                                   "     and numpla = ?" +
                                   "     and numrem = ? ";
    
    private static String anular=" update                "+
                                 "       infoactividad   "+
                                 "    set                " +
                                 "       estado = 'A'," +
                                 "       user_update = ?, " +
                                 "       last_update = ?" +
                                 "   where              " +
                                 "      dstrct = ?    " +
                                 "     and codcli = ?    " +
                                 "     and numrem = ?    " +
                                 "     and cod_actividad= ? ";
     
    
    private static String exiteanulada= " select *              " +
                                       "    from infoactividad " +
                                       " where                 " +
                                       "         estado = 'A'   " +
                                       "     and dstrct = ?    " +
                                       "     and codcli = ?    " +
                                       "     and numrem = ?    " +
                                       "     and cod_actividad= ? ";
    
    private static String Actplanilla="update planilla set cod_actividad = ? where numpla = ?";
    
    private static String fec_ult_act = "select feccierre from infoactividad " +
                                        " where " +
                                        "       estado = '' " +
                                        "   and dstrct = ? " +
                                        "   and codcli = ? " +
                                        "   and numpla = ? " +
                                        "   and numrem = ? " +
                                        "order by feccierre desc " +
                                        "limit 1";
    
    
    /**
     * Metodo setActividad, setea el objeto de tipo Infoactividad
     * @param: objeo Infoactividad
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    
    public void  setActividad (InfoActividad infact){
        this.infoact=infact;
    }
    /**
     * Metodo obtInfoActividad, obtiene  objeto de tipo Infoactividad
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public InfoActividad obtInfoActividad(){
        return infoact;
    }
    /**
     * Metodo obtVecInfoActividad, retorna el vector de Infoactividades
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public Vector obtVecInfoActividad (){
        return VecInfact;
    }
    /**
     * Metodo insertarInfoActividad, ingresa un registro en la tabla infoactividad
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void insertarInfoActividad() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(insertar);
                st.setString(1,infoact.getDstrct() );
                st.setString(2, infoact.getCodCliente() );
                st.setString(3,infoact.getCodActividad() );
                st.setString(4, infoact.getFecini() );
                st.setString(5, infoact.getFecfin() );
                st.setDouble(6, infoact.getDuracion() );
                st.setString(7, infoact.getDocumento() );
                st.setDouble(8, infoact.getTiempoDemora() );
                st.setString(9, infoact.getCausaDemora());
                st.setString(10, infoact.getResdemora() );
                st.setString(11, infoact.getFeccierre() );
                st.setString(12, infoact.getObservacion() );
                st.setString(13, infoact.getBase() );
                //Nuevo Campos 
                
                st.setString(14, infoact.getCantrealizada());
                st.setString(15, infoact.getCantplaneada());
                //Nuevo Campos 19-12-2005
                st.setString(16, infoact.getReferencia1());
                st.setString(17, infoact.getReferencia2());
                st.setInt(18, infoact.getRefnumerica1());
                st.setInt(19, infoact.getRefnumerica2());
                
                st.setString(20, infoact.getCreation_user() );
                st.setString(21, infoact.getCreation_date() );
                st.setString(22, infoact.getUser_update() );
                st.setString(23, infoact.getLast_update() );
                st.setString(24, infoact.getNumpla() );
                st.setString(25, infoact.getNumrem() );
                ////System.out.println(st);
                
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL INSERTAR LA INFOACTIVIDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }      
    }
    /**
     * Metodo modificarInfoActividad, modifica un registro en la tabla infoactividad
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void modificarInfoActividad() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;

        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(modificar);
                st.setString(1, infoact.getFecini() );
                st.setString(2, infoact.getFecfin() );
                st.setDouble(3, infoact.getDuracion() );
                st.setString(4, infoact.getDocumento() );
                st.setDouble(5, infoact.getTiempoDemora() );
                st.setString(6, infoact.getCausaDemora());
                st.setString(7, infoact.getResdemora() );
                st.setString(8, infoact.getFeccierre() );
                st.setString(9, infoact.getObservacion() );
                st.setString(10, infoact.getEstado());
                st.setString(11, infoact.getUser_update() );
                st.setString(12, infoact.getLast_update() );
                //Nuevo Campos
                st.setString(13, infoact.getCantrealizada());
                st.setString(14, infoact.getCantplaneada());
                //nuevo 19-12-2005
                st.setString(15, infoact.getReferencia1());
                st.setString(16, infoact.getReferencia2());
                st.setInt(17, infoact.getRefnumerica1());
                st.setInt(18, infoact.getRefnumerica2());
                
                st.setString(19, infoact.getDstrct() );
                st.setString(20, infoact.getCodCliente() );
                st.setString(21, infoact.getCodActividad() );
                st.setString(22, infoact.getNumpla() );
                st.setString(23, infoact.getNumrem() );

                ////System.out.println(st);
                
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL MODIFICAR INFOACTIVIDAD " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }  
    } 
    /**
     * Metodo buscarInfoActividad, retorna true o false si tiene informacion la actividad                  
     * @param:compa�ia, codigo actividad, cod cliente, conmpa�ia
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */ 
    public boolean buscarInfoActividad(String codact, String codcli ,String cia, String numpla, String numrem) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        infoact = null;
        boolean est = false;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(buscar);
                st.setString(1, cia );
                st.setString(2, codcli );
                st.setString(3, codact );
                st.setString(4, numpla);
                st.setString(5, numrem);
                
                //System.out.println(st);
                rs = st.executeQuery();
                
                if (rs.next()){
                     infoact =  InfoActividad.load(rs);
                     est = true;
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL BUSCAR INFOACTIVIDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return est;
    }
    /**
     * Metodo anularInfoActividad, anula actividades                  
     * @param:compa�ia, codigo actividad decripcion
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */ 
    public void anularInfoActividad() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        //////System.out.println("Dao ");
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(anular);
                st.setString(1, infoact.getUser_update() );
                st.setString(2, infoact.getLast_update() );
                st.setString(3, infoact.getDstrct() );
                st.setString(4, infoact.getCodCliente() );
                st.setString(5, infoact.getNumrem() );
                st.setString(6,infoact.getCodActividad() );
                 
                ////System.out.println("Anulado "+st);
                
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL ANULAR INFOACTIVIDAD " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }  
    } 
    /**
     * Metodo:  existeInfoActividadAnulada, retorna true o false si la infoactividad esta anulada.
     * @param:codigo actividad, codigo cliente, compa�ia
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */ 
    public boolean existeInfoActividadAnulada(String codact, String codcli ,String cia, String numrem) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        infoact = null;
        boolean sw = false;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(exiteanulada);
                st.setString(1, cia );
                st.setString(2, codcli );
                st.setString(3, numrem );
                st.setString(4, codact );
                ////System.out.println(st);
                
                rs = st.executeQuery();
                
                if (rs.next()){
                     sw = true;
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL BUSCAR LA INFOACTIVIDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }  
        return sw;
    }
    
    
    
    /**
     * Metodo:  actualizarActPlanilla, actualiza la actividad en la planilla.
     * @param: numpla, cod actividad
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */ 
    public void actualizarActPlanilla (String numpla, String act) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;

        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(Actplanilla);
                st.setString(1, act );
                st.setString(2, numpla );
                

                ////System.out.println(st);
                
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL MODIFICAR INFOACTIVIDAD " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }  
    } 
    
    ///-------------------------20 enero 2007----------------------------
    /**
     * Metodo fecha_ult_act_Registrada, retorna la fecha de cierre de la ultima actividad registrada      
     * @param:compa�ia,  cod cliente, numpla, numremesa
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */ 
    public String fecha_ult_act_Registrada(String codcli ,String cia, String numpla, String numrem) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        infoact = null;
        String est = "";
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(fec_ult_act);
                st.setString(1, cia );
                st.setString(2, codcli );
                st.setString(3, numpla);
                st.setString(4, numrem);
                
                ////System.out.println(st);
                rs = st.executeQuery();
                
                if (rs.next()){
                     est = rs.getString("feccierre");
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL BUSCAR FECHA ULTIMA ACT " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return est;
    }
    
}
