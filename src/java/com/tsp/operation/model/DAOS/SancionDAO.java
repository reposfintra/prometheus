/************************************************************************
 * Nombre clase: SancionDAO.java                                        *
 * Descripci�n: Clase que maneja las consultas de las sanciones.        *
 * Autor: Ing. Jose de la rosa                                          *
 * Fecha: Created on 19 de octubre de 2005, 04:59 PM                    *
 * Versi�n: Java 1.0                                                    *
 * Copyright: Fintravalores S.A. S.A.                              *
 ***********************************************************************/
package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  Jose
 */
public class SancionDAO {
    private Sancion sancion;
    private Vector VecSanciones;
    private final String SQL_INSERT = "insert into sancion (numpla, tipo_sancion, valor, observacion, rec_status, last_update, user_update, creation_date, creation_user,base,distrito ) " +
    "values (?,?,?,?, '' , 'now()', ?, 'now()', ?,?,?)";
    private final String SQL_UPDATE_SANCION = "update sancion set tipo_sancion=?, valor=?, observacion=?, " +
    "last_update='now()', user_update=?, rec_status=' ' where numpla=? and cod_sancion=?";
    private final String SQL_LISTAR_SANCIONES = "Select * from discrepancia where cod_planilla=?" +
    " and rec_status != 'A' and tipo_registro='S' order by creation_date";
    private final String SQL_LISTAR_SANCION = "select * from sancion";
    private final String SQL_EXISTE_SANCION = "select * from sancion  where rec_status!='A' and " +
    "numpla=? and cod_sancion=?";
    private final String SQL_SERCH_DETALLE_SANCION = "Select * from sancion where numpla like ? and tipo_sancion like ? and rec_status != 'A'";
    private final String SQL_ANULAR_SANCION = "update sancion set last_update='now()', user_update=?, rec_status='A' where numpla=? and cod_sancion=?";
    private final String SQL_LISTAR_SANCION_SIN_APROBACION = "Select * from sancion where fecha_aprobacion = '0099-01-01 00:00:00' and rec_status!='A'";
    private final String SQL_LISTAR_SANCION_CON_APROBACION = "Select * from sancion where fecha_aprobacion BETWEEN ? and ? and rec_status!='A'";
    private final String SQL_LISTAR_SANCION_SIN_MIGRACION = "Select * from sancion where fecmigracion = '0099-01-01 00:00:00' and rec_status!='A'";
    private final String SQL_LISTAR_SANCION_CON_MIGRACION = "Select * from sancion where fecmigracion BETWEEN ? and ? and rec_status!='A'";
    //Jose 09-11-2005
    private final String SQL_AUTORIZAR_SANCION = "update sancion set valor_aprobado_sancion=?, fecha_aprobacion='now()', usuario_aprobacion=? where numpla=? and cod_sancion=?";
    /** Creates a new instance of SancionDAO */
    public SancionDAO () {
    }
    
    /**
     * Metodo: getSancion, permite retornar un objeto de registros de sanci�n.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Sancion getSancion () {
        return sancion;
    }
    
    /**
     * Metodo: setSancion, permite obtiene un objeto de registros de sanci�n.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void setSancion (Sancion sancion) {
        this.sancion = sancion;
    }
    
    /**
     * Metodo: getSanciones, permite obtiene un vector de registros de sanci�n.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Vector getSanciones () {
        return VecSanciones;
    }
    
    /**
     * Metodo: setSanciones, permite obtiene un vector de registros de sanci�n.
     * @autor : Ing. Jose de la rosa
     * @param : vector
     * @version : 1.0
     */
    public void setSanciones (Vector VecSanciones) {
        this.VecSanciones = VecSanciones;
    }
    
    /**
     * Metodo: insertarSancion, permite ingresar un registro a la tabla sanci�n.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void insertarSancion () throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (this.SQL_INSERT);
                st.setString (1, sancion.getNumpla ());
                st.setString (2, sancion.getTipo_sancion ());
                st.setDouble (3, sancion.getValor ());
                st.setString (4, sancion.getObservacion ());
                st.setString (5, sancion.getUsuario_modificacion ());
                st.setString (6, sancion.getUsuario_creacion ());
                st.setString (7, sancion.getBase ());
                st.setString (8, sancion.getDistrito ());
                st.executeUpdate ();
                ////System.out.println (st);
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL INSERTAR LA SANCION " + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
    
    /**
     * Metodo: existeSancion, permite buscar una sanci�n dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : numero de la planilla y codigo de sancion
     * @version : 1.0
     */
    public boolean existeSancion (String numpla, String cod_sancion ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null ){
                st = con.prepareStatement (this.SQL_EXISTE_SANCION);
                st.setString (1, numpla);
                st.setString (2, cod_sancion);
                rs = st.executeQuery ();
                
                if (rs.next ()){
                    sw = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR LA SANCI�N " + e.getMessage () + "" + e.getErrorCode () );
        }
        finally {
            if (st != null){
                try{
                    st.close ();
                }catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage ());
                }
                if ( con != null){
                    poolManager.freeConnection ("fintra", con);
                }
            }
        }
        return sw;
    }
    
    /**
     * Metodo: updateDiscrepancia, permite actualizar una sancion.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void updateSancion () throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (this.SQL_UPDATE_SANCION);
                st.setString (1, sancion.getTipo_sancion ());
                st.setDouble (2, sancion.getValor ());
                st.setString (3, sancion.getObservacion ());
                st.setString (4, sancion.getUsuario_modificacion ());
                st.setString (5, sancion.getNumpla ());
                st.setInt (6, sancion.getCod_sancion ());
                st.executeUpdate ();
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA MODIFICACI�N DE LA SANCION " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally{
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
    }
    
    /**
     * Metodo: listSancion, permite listar todas las sanciones
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void listSancion ()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if(con!=null){
                st = con.prepareStatement (SQL_LISTAR_SANCION);
                rs= st.executeQuery ();
                this.VecSanciones = new Vector ();
                while(rs.next ()){
                    sancion = new Sancion ();
                    sancion.setNumpla (rs.getString ("numpla"));
                    sancion.setCod_sancion (rs.getInt ("cod_sancion"));
                    sancion.setValor (rs.getDouble ("valor"));
                    sancion.setTipo_sancion (rs.getString ("tipo_sancion"));
                    sancion.setObservacion (rs.getString ("observacion"));
                    sancion.setFecha_aprobacion (rs.getString ("fecha_aprobacion"));
                    sancion.setUsuario_aprobacion (rs.getString ("usuario_aprobacion"));
                    sancion.setValor_aprobado (rs.getDouble ("valor_aprobado_sancion"));
                    sancion.setUsuario_migracion (rs.getString ("usu_migracion"));
                    sancion.setFecha_migracion (rs.getString ("fecmigracion"));
                    VecSanciones.add (sancion);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA LISTA DE LA SANCIONES " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally{
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo: searchSancion, permite buscar una sanci�n dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : numero de la planilla y el codigo de la sanci�n.
     * @version : 1.0
     */
    public void searchSancion (String numpla, int cod_sancion )throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if(con!=null){
                st = con.prepareStatement (this.SQL_EXISTE_SANCION);
                st.setString (1, numpla);
                st.setInt (2, cod_sancion);
                rs= st.executeQuery ();
                VecSanciones = new Vector ();
                while(rs.next ()){
                    sancion = new Sancion ();
                    sancion.setNumpla (rs.getString ("numpla"));
                    sancion.setCod_sancion (rs.getInt ("cod_sancion"));
                    sancion.setValor (rs.getDouble ("valor"));
                    sancion.setTipo_sancion (rs.getString ("tipo_sancion"));
                    sancion.setObservacion (rs.getString ("observacion"));
                    sancion.setFecha_aprobacion (rs.getString ("fecha_aprobacion"));
                    sancion.setUsuario_aprobacion (rs.getString ("usuario_aprobacion"));
                    sancion.setValor_aprobado (rs.getDouble ("valor_aprobado_sancion"));
                    sancion.setUsuario_migracion (rs.getString ("usu_migracion"));
                    sancion.setFecha_migracion (rs.getString ("fecmigracion"));
                    VecSanciones.add (sancion);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA LISTA DE LA SANCION " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally {
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo: searchDetalleSancion, permite listar sanciones por detalles dados unos parametros.
     * @autor : Ing. Jose de la rosa
     * @param : numero de planilla y tipo de sanci�n
     * @version : 1.0
     */
    public void searchDetalleSancion (String numpla, String tipo_sancion) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        VecSanciones=null;
        
        try {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            boolean sw = false;
            if(con!=null){
                st = con.prepareStatement (SQL_SERCH_DETALLE_SANCION);
                st.setString (1, numpla+"%");
                st.setString (2, tipo_sancion+"%");
                rs = st.executeQuery ();
                VecSanciones = new Vector ();
                while(rs.next ()){
                    sancion = new Sancion ();
                    sancion.setNumpla (rs.getString ("numpla"));
                    sancion.setCod_sancion (rs.getInt ("cod_sancion"));
                    sancion.setValor (rs.getDouble ("valor"));
                    sancion.setTipo_sancion (rs.getString ("tipo_sancion"));
                    sancion.setObservacion (rs.getString ("observacion"));
                    sancion.setFecha_aprobacion (rs.getString ("fecha_aprobacion"));
                    sancion.setUsuario_aprobacion (rs.getString ("usuario_aprobacion"));
                    sancion.setValor_aprobado (rs.getDouble ("valor_aprobado_sancion"));
                    sancion.setUsuario_migracion (rs.getString ("usu_migracion"));
                    sancion.setFecha_migracion (rs.getString ("fecmigracion"));
                    VecSanciones.add (sancion);
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA BUSQUEDA DE LOS DETALLES DE LAS SANCIONES " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally {
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
    }
    
    /**
     * Metodo: anularSancion, permite anular una sanci�n.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void anularSancion () throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_ANULAR_SANCION);
                st.setString (1, sancion.getUsuario_modificacion ());
                st.setString (2, sancion.getNumpla ());
                st.setInt (3, sancion.getCod_sancion ());
                st.executeUpdate ();
            }
        }
        catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA ANULACION DE LA SANCION " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally {
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
    }
    
    /**
     * Metodo: listSancionSinAprobacion, permite listar una sanci�n sin fecha de aprobaci�n
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void listSancionSinAprobacion ()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if(con!=null){
                st = con.prepareStatement (SQL_LISTAR_SANCION_SIN_APROBACION);
                rs= st.executeQuery ();
                this.VecSanciones = new Vector ();
                while(rs.next ()){
                    sancion = new Sancion ();
                    sancion.setNumpla (rs.getString ("numpla"));
                    sancion.setCod_sancion (rs.getInt ("cod_sancion"));
                    sancion.setValor (rs.getDouble ("valor"));
                    sancion.setTipo_sancion (rs.getString ("tipo_sancion"));
                    sancion.setObservacion (rs.getString ("observacion"));
                    sancion.setFecha_aprobacion (rs.getString ("fecha_aprobacion"));
                    sancion.setUsuario_aprobacion (rs.getString ("usuario_aprobacion"));
                    sancion.setValor_aprobado (rs.getDouble ("valor_aprobado_sancion"));
                    sancion.setUsuario_migracion (rs.getString ("usu_migracion"));
                    sancion.setFecha_migracion (rs.getString ("fecmigracion"));
                    VecSanciones.add (sancion);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA LISTA DE LA DISCREPANCIA SIN CIERRE " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally{
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo: listSancionConAprobacion, permite listar una sanci�n con fecha de aprobaci�n dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : fecha de inicio y fecha de fin
     * @version : 1.0
     */
    public void listSancionConAprobacion (String fini,String ffin)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if(con!=null){
                st = con.prepareStatement (SQL_LISTAR_SANCION_CON_APROBACION);
                st.setString (1,fini+" 00:00");
                st.setString (2,ffin+" 23:59");
                rs= st.executeQuery ();
                this.VecSanciones = new Vector ();
                while(rs.next ()){
                    sancion = new Sancion ();
                    sancion.setNumpla (rs.getString ("numpla"));
                    sancion.setCod_sancion (rs.getInt ("cod_sancion"));
                    sancion.setValor (rs.getDouble ("valor"));
                    sancion.setTipo_sancion (rs.getString ("tipo_sancion"));
                    sancion.setObservacion (rs.getString ("observacion"));
                    sancion.setFecha_aprobacion (rs.getString ("fecha_aprobacion"));
                    sancion.setUsuario_aprobacion (rs.getString ("usuario_aprobacion"));
                    sancion.setValor_aprobado (rs.getDouble ("valor_aprobado_sancion"));
                    sancion.setUsuario_migracion (rs.getString ("usu_migracion"));
                    sancion.setFecha_migracion (rs.getString ("fecmigracion"));
                    VecSanciones.add (sancion);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA LISTA DE LA DISCREPANCIA CON CIERRE " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally{
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
    }
    
    /**
     * Metodo: listSancionSinMigracion, permite listar una sanci�n sin fecha de migraci�n
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void listSancionSinMigracion ()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if(con!=null){
                st = con.prepareStatement (SQL_LISTAR_SANCION_SIN_MIGRACION);
                rs= st.executeQuery ();
                this.VecSanciones = new Vector ();
                while(rs.next ()){
                    sancion = new Sancion ();
                    sancion.setNumpla (rs.getString ("numpla"));
                    sancion.setCod_sancion (rs.getInt ("cod_sancion"));
                    sancion.setValor (rs.getDouble ("valor"));
                    sancion.setTipo_sancion (rs.getString ("tipo_sancion"));
                    sancion.setObservacion (rs.getString ("observacion"));
                    sancion.setFecha_aprobacion (rs.getString ("fecha_aprobacion"));
                    sancion.setUsuario_aprobacion (rs.getString ("usuario_aprobacion"));
                    sancion.setValor_aprobado (rs.getDouble ("valor_aprobado_sancion"));
                    sancion.setUsuario_migracion (rs.getString ("usu_migracion"));
                    sancion.setFecha_migracion (rs.getString ("fecmigracion"));
                    VecSanciones.add (sancion);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA LISTA DE LA DISCREPANCIA SIN CIERRE " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally{
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo: listSancionConMigracion, permite listar una sanci�n con fecha de migraci�n dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : fecha de inicio y fecha de fin
     * @version : 1.0
     */
    public void listSancionConMigracion (String fini,String ffin)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if(con!=null){
                st = con.prepareStatement (SQL_LISTAR_SANCION_CON_MIGRACION);
                st.setString (1,fini+" 00:00");
                st.setString (2,ffin+" 23:59");
                rs= st.executeQuery ();
                this.VecSanciones = new Vector ();
                while(rs.next ()){
                    sancion = new Sancion ();
                    sancion.setNumpla (rs.getString ("numpla"));
                    sancion.setCod_sancion (rs.getInt ("cod_sancion"));
                    sancion.setValor (rs.getDouble ("valor"));
                    sancion.setTipo_sancion (rs.getString ("tipo_sancion"));
                    sancion.setObservacion (rs.getString ("observacion"));
                    sancion.setFecha_aprobacion (rs.getString ("fecha_aprobacion"));
                    sancion.setUsuario_aprobacion (rs.getString ("usuario_aprobacion"));
                    sancion.setValor_aprobado (rs.getDouble ("valor_aprobado_sancion"));
                    sancion.setUsuario_migracion (rs.getString ("usu_migracion"));
                    sancion.setFecha_migracion (rs.getString ("fecmigracion"));
                    VecSanciones.add (sancion);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA LISTA DE LA DISCREPANCIA CON CIERRE " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally{
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
    }
    
    /**
     * Metodo: autorizarSancion, permite autorizar una sancion.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void autorizarSancion () throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (this.SQL_AUTORIZAR_SANCION);
                st.setDouble (1, sancion.getValor_aprobado ());
                st.setString (2, sancion.getUsuario_aprobacion ());
                st.setString (3, sancion.getNumpla ());
                st.setInt (4, sancion.getCod_sancion ());
                st.executeUpdate ();
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA MODIFICACI�N DE LA SANCION " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally{
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
    }
}
