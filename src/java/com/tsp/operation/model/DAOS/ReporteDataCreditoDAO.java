/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.DAOS;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author maltamiranda
 */
public class ReporteDataCreditoDAO extends MainDAO {

    public ReporteDataCreditoDAO() {
        super("ReporteDataCreditoDAO.xml");
    }

    public String getClientes(String cl,String nomselect,String def) throws Exception
    {   Connection con=null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String  Query = "SQL_CLIENTES";
        String ret="";String sql="";
        try{
            sql=obtenerSQL(Query);
            con = conectarJNDI(Query);
            ps = con.prepareStatement(sql);
            ps.setString(1, nomselect);
            ps.setString(2, nomselect);
            ps.setString(3, cl);
            ps.setString(4, def);
            ps.setString(5, cl);
            rs = ps.executeQuery();
            ret=cl+";;;;;;;;;;";
            if (rs.next())
            ret=ret+rs.getString("nombre");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
        finally
        {
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
            this.desconectar(Query);
        }
       return ret;
    }
    public String getAfiliados(String cl,String nomselect,String def) throws Exception
    {   Connection con=null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String  Query = "SQL_AFILIADOS";
        String ret="";String sql="";
        try{
            sql=obtenerSQL(Query);
            con = conectarJNDI(Query);
            ps = con.prepareStatement(sql);
            ps.setString(1, nomselect);
            ps.setString(2, nomselect);
            ps.setString(3, cl);
            ps.setString(4, def);
            ps.setString(5, cl);
            rs = ps.executeQuery();
            ret=cl+";;;;;;;;;;";
            if (rs.next())
            ret=ret+rs.getString("nombre");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
        finally
        {
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
            this.desconectar(Query);
        }
       return ret;
    }

    public ArrayList buscar_negocios(String idclie, String negocio, String proveedor, String rangoi, String rangof, int columna,String fechaReporte)throws Exception {
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        ArrayList arl=null;
        String sql="";int i=1;
        try{
            sql=obtenerSQL("SQL_GET_NEGOCIOS");
            con = conectarJNDI("SQL_GET_NEGOCIOS");
            if(fechaReporte.equals("")){
                sql=sql.replaceAll("#FECHAREPORTE#", "--");
            }
            else{
                sql=sql.replaceAll("#FECHAREPORTE#", "");
            }
            if(idclie.equals("")){
                sql=sql.replaceAll("#CLIENTE#", "--");
            }
            else{
                sql=sql.replaceAll("#CLIENTE#", "");
            }
            if(negocio.equals("")){
                sql=sql.replaceAll("#NEGOCIO#", "--");
            }
            else{
                sql=sql.replaceAll("#NEGOCIO#", "");
            }
            if(proveedor.equals("")){
                sql=sql.replaceAll("#PROVEEDOR#", "--");
            }
            else{
                sql=sql.replaceAll("#PROVEEDOR#", "");
            }
            if(rangoi.equals("")){
                sql=sql.replaceAll("#FECHA#", "--");
            }
            else{
                sql=sql.replaceAll("#FECHA#", "");
            }
            ps  = con.prepareStatement(sql);
            ps.setString(i, fechaReporte);i++;
            ps.setString(i, idclie);i++;
            ps.setString(i, negocio);i++;
            ps.setString(i, proveedor);i++;
            ps.setString(i, rangoi);i++;
            ps.setString(i, rangof);i++;
            ps.setInt(i, columna);i++;
            rs=ps.executeQuery();
            arl=rstotbl(rs);
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println(e.getMessage());
            e.toString();
            throw new Exception("Error al obtener los Negocios"+e.toString());
        }
        finally{
            if(con!=null){desconectar(con);}
            if(ps!=null){ps.close();}
            if(rs!=null){rs.close();}
        }

        return arl;
    }
    public ArrayList rstotbl (ResultSet rs) throws Exception {
        ArrayList tabla=new ArrayList();
        while(rs.next()){
           ArrayList tabla2=new ArrayList();
           for(int i=1;i<=rs.getMetaData().getColumnCount();i++)
           {    tabla2.add(rs.getString(i));
           }
           tabla.add(tabla2);
        }
        return tabla;
    }

    public void agregar_negocios_acumulado(String[] obligaciones, String usuario)throws Exception {
        Connection con=null;
        PreparedStatement st=null;
        String sql="";
        try{
            sql=this.obtenerSQL("SQL_ADD_NEGOCIOS");
            con=this.conectarJNDI("SQL_ADD_NEGOCIOS");
            con.setAutoCommit(false);
            st=con.prepareStatement(sql);
            for (int i=0;i<obligaciones.length;i++){
                st.setString(1,obligaciones[i]);
                st.setString(2,usuario);
                st.execute();
            }
            con.commit();
        }
        catch(Exception e){
            con.rollback();
            e.printStackTrace();
            System.out.println(e.getMessage());
            throw new Exception( "Error al Agregar Negocios al Acumulado: " + e.toString());
        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(st!=null){st.close();}
        }
    }

    public ArrayList buscar_acumulados(int columna)throws Exception {
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        ArrayList arl=null;
        String sql="";int i=1;
        try{
            sql=obtenerSQL("SQL_GET_NEGOCIOS_ACUMULADOS");
            con = conectarJNDI("SQL_GET_NEGOCIOS_ACUMULADOS");
            ps  = con.prepareStatement(sql);
            ps.setInt(i, columna);i++;
            rs=ps.executeQuery();
            arl=rstotbl(rs);
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println(e.getMessage());
            throw new Exception("Error al obtener los Negocios"+e.toString());
        }
        finally{
            if(con!=null){desconectar(con);}
            if(ps!=null){ps.close();}
            if(rs!=null){rs.close();}
        }

        return arl;
    }

    public void eliminar_negocios_acumulado(String[] obligaciones)throws Exception {
        Connection con=null;
        PreparedStatement st=null;
        String sql="";
        try{
            sql=this.obtenerSQL("SQL_DEL_NEGOCIOS");
            con=this.conectarJNDI("SQL_DEL_NEGOCIOS");
            con.setAutoCommit(false);
            st=con.prepareStatement(sql);
            for (int i=0;i<obligaciones.length;i++){
                st.setString(1,obligaciones[i]);
                st.execute();
            }
            con.commit();
        }
        catch(Exception e){
            con.rollback();
            e.printStackTrace();
            System.out.println(e.getMessage());
            throw new Exception( "Error al Eliminar Negocios al Acumulado: " + e.toString());
        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(st!=null){st.close();}
        }
    }

    public void limpiar_negocios_acumulado()throws Exception {
        Connection con=null;
        PreparedStatement st=null;
        String sql="";
        try{
            sql=this.obtenerSQL("SQL_CL_NEGOCIOS");
            con=this.conectarJNDI("SQL_CL_NEGOCIOS");
            st=con.prepareStatement(sql);
            st.execute();
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println(e.getMessage());
            throw new Exception( "Error al Eliminar Negocios al Acumulado: " + e.toString());
        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(st!=null){st.close();}
        }
    }

    public String generarReporte()throws Exception {
        Connection con=null;
        PreparedStatement st=null;
        String sql="";
        ResultSet rs=null;
        String resultado="";
        try{
            sql=this.obtenerSQL("SQL_UPDATE_NEGOCIOS");
            con=this.conectarJNDI("SQL_UPDATE_NEGOCIOS");
            con.setAutoCommit(false);
            st=con.prepareStatement(sql);
            st.execute();
            st=con.prepareStatement(this.obtenerSQL("SQL_GET_ARCHIVO"));
            rs=st.executeQuery();
            if(rs.next()){
                resultado=rs.getString("arch");
            }
            con.commit();
        }
        catch(Exception e){
            con.rollback();
            e.printStackTrace();
            System.out.println(e.getMessage());
            throw new Exception( "Error al Generar Archivo Plano: " + e.toString());
        }
        finally{
            if(con!=null){this.desconectar(con);}
            if(st!=null){st.close();}
        }
        return resultado;
    }
}
