/******************************************************************
* Nombre ......................TblGeneralProgDAO.java
* Descripci�n..................Clase DAO para tablagen_prog (antes tablagen_prog)
* Autor........................ricardo rosero
* Fecha........................23/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
* modificado...................29/12/2005
*******************************************************************/

package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import java.sql.*;
import java.util.*;

public class TblGeneralProgDAO extends MainDAO {
    
    private TblGeneralProg tg;
    private Connection con;
    private Vector tblgens;
    
    /****  SQL QUERIES *****/
    
    private static String SQL_INSERT = "INSERT INTO tablagen_prog (dstrct, table_type, table_code," +
                                       "program, last_update, user_update, creation_date, creation_user, base) " +
                                       "VALUES(?,?,?,?,?,?,?,?,?)";
    
    private static String SQL_BUSCAR = "SELECT * FROM tablagen_prog WHERE table_type = ? AND table_code = ?";
    
    private static String SQL_UPDEXIST = "UPDATE " +
                                         "  tablagen_prog " +
                                         "SET " +
                                         "  reg_status='', program=?, program=?," +
                                         "  last_update='now()', user_update=?, base=?, dstrct=? " +
                                         "WHERE " +
                                         "  table_code=? AND table_type=?";
    
    private static String SQL_EXISTEDE = "SELECT table_type, table_code, program FROM tablagen_prog WHERE table_type=? AND table_code=? AND program=?";
    
    private static String SQL_EXISTEDEANULADO = "SELECT table_code,table_type FROM tablagen_prog WHERE table_code=? AND table_type=? AND reg_status='A'";
    
    private static String SQL_DELETEDE = "UPDATE tablagen_prog SET reg_status = ? WHERE table_code = ? AND table_type=?";
    
    private static String SQL_BUSCARTODOSDESCUENTOS = "SELECT table_code, table_type, program FROM tablagen_prog WHERE reg_status!='A'";

    private static String SQL_BUSCAR_programES = "SELECT table_code,  program FROM tablagen_prog WHERE reg_status!='A' AND table_type=? AND program=? ";
    
    private static String EXISTE = "SELECT * FROM tablagen_prog WHERE table_type = ? AND table_code = ? AND program = ? AND reg_status != 'A'";
    
    private static String SQL_UPDATE = "UPDATE tablagen_prog " +
                                       "SET last_update = ?, user_update = ?, reg_status = '' " +
                                       " WHERE table_type = ? AND table_code = ? AND program = ?";

    
    //Henry
    private static final String SEARCH_DATOS_TABLA_CLASE  = 
    "Select " +
    "      table_code," +
    "      program " +
    "from " +
    "      tablagen_prog " +
    "where" +
    "      table_type='CL' order by program";
    //Henry
     private static final String SEARCH_DATOS_TABLA_TIPO  = 
    "Select " +
    "      table_code," +
    "      program " +
    "from " +
    "      tablagen_prog " +
    "where" +
    "      table_type='TP' order by program";
     
     //Ricardo
     private static String SQL_GENERAL = "SELECT * FROM tablagen_prog";
     
     private static String SQL1 = SQL_GENERAL;// + " ORDER BY table_type, table_code";
     
     private static String SQL2 = SQL_GENERAL + " WHERE table_type LIKE ? AND reg_status != 'A' ORDER BY table_type, program, table_code";
     
     private static String SQL3 = SQL_GENERAL + " WHERE program LIKE ? AND reg_status != 'A' ORDER BY table_type, program, table_code";
     
     private static String SQL4 = SQL_GENERAL + " WHERE table_code LIKE ? AND reg_status != 'A' ORDER BY table_type, program, table_code";
     
     private static String SQL5 = SQL_GENERAL + " WHERE upper(table_type) LIKE upper(?) AND upper(program) LIKE upper(?) AND upper(table_code) LIKE upper(?) AND reg_status != 'A' ORDER BY table_type, program, table_code";
        
     private static String SQL6 = SQL_GENERAL + " WHERE table_type LIKE ? AND program LIKE ? AND reg_status != 'A' ORDER BY table_type, program, table_code";
     
     private static String SQL7 = SQL_GENERAL + " WHERE table_type LIKE ? AND table_code LIKE ? AND reg_status != 'A' ORDER BY table_type, program, table_code";
     
     private static String SQL8 = SQL_GENERAL + " WHERE program LIKE ? AND table_code LIKE ? AND reg_status != 'A' ORDER BY table_type, program, table_code";
     
     //AGREGADO DAVID PI�A LOPEZ
     private static String BUSCAR_CLIENTES_EMAIL =  "SELECT 	DISTINCT program " +
                                                    "FROM 	tablagen_prog " +
                                                    "WHERE 	table_type = 'INFCLI' ";
     private static String BUSCAR_EMAILS_CLIENTE = "SELECT 	b.table_type, b.table_code, c.descripcion, c.referencia " +
                                                    "FROM 	tablagen_prog a " +
                                                    "           INNER JOIN tablagen_prog b ON( " +
                                                    "           a.table_code = b.program) " +
                                                    "           INNER JOIN tablagen c ON( " +
                                                    "           b.table_code = c. table_code) " +
                                                    "WHERE 	a.table_type = 'INFCLI'	AND " +
                                                    "           b.table_type = 'EMAIL' AND " +
                                                    "           a.program = ? ";
     private static String ANULAR = "UPDATE tablagen_prog SET reg_status='A', last_update = 'now()', user_update = ? WHERE table_type=? AND table_code=? AND program=?";
     
    /** Creates a new instance of TblGeneralProgDAO */
    public TblGeneralProgDAO() {
        super("TblGeneralProgDAO.xml");//JJCastro fase2
    }
    
    /**
     * M�todo que setea un objeto TblGeneralProg
     * @autor.......ricardo rosero
     * @throws......SQLException
     * @version.....1.0.
     * @param.......TblGeneralProg de
     **/     
    public void setTG(TblGeneralProg tg){
        this.tg = tg;
    }
    
    /**
     * M�todo que retorna un objeto TblGeneralProg
     * @autor.......ricardo rosero
     * @throws......SQLException
     * @version.....1.0.
     * @return.......TblGeneralProg de
     **/     
    public TblGeneralProg getTblGeneralProg(){
        return this.tg;
    }
    
    /**
     * M�todo que modifica el reg_status
     * @autor.......Ricardo rosero
     * @param.......String table_type, String table_code          
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void anularTblGeneralProg() throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        String query = "ANULAR";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, tg.gettable_type());
                ps.setString(2, tg.gettable_code());
                ps.setString(3, tg.getprogram());
                ps.executeUpdate();
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
    * M�todo: existeTblGeneralProg
    * @autor: ....... Ing. Ricardo Rosero
    * @param: ....... table_type, table_code
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public boolean existeTblGeneralProg( String table_type, String table_code ) throws SQLException {
        boolean result = false; // El resultado de la funcion
        // Vamos a usar PreparedStatements
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "EXISTE";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString( 1, table_type + "%" );
                st.setString( 2, table_code + "%" );
                rs = st.executeQuery();
                if ( rs.next() ) {  // Hay datos retornados por la consulta
                    result = true;
                }
                else {
                    result = false;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return result;
    }
    
    /**
    * M�todo: listarPortable_code
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public void listarPortable_code(String table_type, String table_code)throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        tblgens = null;
        String query = "EXISTE";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, table_type + "%");
                st.setString(2, table_code + "%");
                rs = st.executeQuery();
                
                tblgens =  new Vector();
                
                while (rs.next()){
                    tblgens.add(tg.load(rs));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
    * M�todo: listarTodos
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public void listarTodos()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        tblgens = null;
        String query = "SQL1";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                tblgens =  new Vector();
                
                while (rs.next()){
                    tblgens.add(tg.load(rs));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
    * M�todo: listarTodos2
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public void listarTodos2()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        tblgens = null;
        String query = "SQL2";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, tg.gettable_type());
                rs = st.executeQuery();
                
                tblgens =  new Vector();
                
                while (rs.next()){
                    tblgens.add(tg.load(rs));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
    * M�todo: listarTodos3
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public void listarTodos3()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        tblgens = null;
        String query = "SQL3";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, tg.getprogram());
                rs = st.executeQuery();
                tblgens =  new Vector();
                while (rs.next()){
                    tblgens.add(tg.load(rs));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
    * M�todo: listarTodos4
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public void listarTodos4()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        tblgens = null;
        String query = "SQL4";//JJCastro fase2

        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, tg.gettable_code());
                rs = st.executeQuery();
                tblgens =  new Vector();
                
                while (rs.next()){
                    tblgens.add(tg.load(rs));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
    * M�todo: listarTodos5
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public void listarTodos5()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        tblgens = null;
        String query = "SQL5";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, tg.gettable_type());
                st.setString(2, tg.getprogram());
                st.setString(3, tg.gettable_code());
                rs = st.executeQuery();
                tblgens =  new Vector();
                
                while (rs.next()){
                    tblgens.add(tg.load(rs));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
    * M�todo: listarTodos6
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public void listarTodos6()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        tblgens = null;
        String query = "SQL6";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, tg.gettable_type());
                st.setString(2, tg.getprogram());
                rs = st.executeQuery();
                tblgens =  new Vector();
                
                while (rs.next()){
                    tblgens.add(tg.load(rs));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
    * M�todo: listarTodos7
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public void listarTodos7()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        tblgens = null;
        String query = "SQL7";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, tg.gettable_type());
                st.setString(2, tg.gettable_code());
                rs = st.executeQuery();
                tblgens =  new Vector();
                while (rs.next()){
                    tblgens.add(tg.load(rs));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
    * M�todo: listarTodos8
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public void listarTodos8()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        tblgens = null;
        String query = "SQL8";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, tg.getprogram());
                st.setString(2, tg.gettable_code());
                rs = st.executeQuery();
                tblgens =  new Vector();
                while (rs.next()){
                    tblgens.add(tg.load(rs));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
    * M�todo: obtTablaGen
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ none
    * @throws ....... none
    * @version ...... 1.0
    */   
    public Vector obtTablaGen(){
        return this.tblgens;
    }
    
    /**
     * M�todo que busca y setea un objeto TblGeneralProg
     * @autor.......ricardo rosero
     * @throws......SQLException
     * @version.....1.0.
     * @param.......TblGeneralProg de cargado con el set
     **/ 
    public void buscarTblGeneralProg() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        TblGeneralProg tmp = null;
        String query = "SQL_BUSCAR";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, tg.gettable_code());
            ps.setString(2, tg.gettable_type());            
            rs = ps.executeQuery();
            while(rs.next()){
                tmp = new TblGeneralProg();                
                tmp = tmp.load(rs);                                                            
            }                        
            setTG(tmp);            
        }}catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * M�todo que retorna un boolean si existe el TblGeneralProg o no
     * @autor.......ricardo rosero
     * @throws......SQLException
     * @version.....1.0.
     * @param.......c�digo del descuento cargado con el set
     **/     
    public boolean existeTblGeneralProg() throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_EXISTEDE";//JJCastro fase2
        boolean existe = false; //JJCastro fase2
        try{            
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, tg.gettable_type());
            ps.setString(2, tg.gettable_code());
            ps.setString(3, tg.getprogram());
            rs = ps.executeQuery();            
            existe =  rs.next();//JJCastro fase2
            
        }}catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return existe;
    }
    
    /**
     * M�todo que retorna un boolean si existe el TblGeneralProg o no, anulado
     * @autor.......ricardo rosero
     * @throws......SQLException
     * @version.....1.0.
     * @param.......c�digo del descuento cargado con el set
     **/   
    public boolean existeTblGeneralProgAnulado() throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_EXISTEDEANULADO";//JJCastro fase2
        boolean existe = false;//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, tg.gettable_code());
            ps.setString(2, tg.gettable_type());
            rs = ps.executeQuery();            
            existe =  rs.next();//JJCastro fase2
        }}catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return existe;//JJCastro fase2
    }
    
    
    /**
     * M�todo que agrega un TblGeneralProg
     * @autor.......ricardo rosero
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void addTblGeneralProg() throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        String query = "SQL_INSERT";//JJCastro fase2
        try{            
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1,tg.getDistrito());
            ps.setString(2,tg.gettable_type());
            ps.setString(3,tg.gettable_code());
            ps.setString(4,tg.getprogram());
            ps.setString(5,tg.getFm());
            ps.setString(6,tg.getUm());
            ps.setString(7,tg.getFC());
            ps.setString(8,tg.getUc());
            ps.setString(9,tg.getBase());
            ps.executeUpdate();
        }}catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
  
    
    /**
     * M�todo que elimina un TblGeneralProg
     * @autor.......ricardo rosero
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void deleteTblGeneralProg() throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        String query = "SQL_DELETEDE";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, "A");
            ps.setString(2, tg.gettable_code());
            ps.setString(3, tg.gettable_type());
            ps.executeUpdate();            
        }}catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * M�todo que setea un vector que contiene todos los objetos de la tabla
     * @autor.......ricardo rosero
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void buscarTodosTblGeneralProg() throws SQLException{        
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        tblgens = new Vector();
        String query = "SQL_BUSCARTODOSDESCUENTOS";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = ps.executeQuery();            
            while(rs.next()){
                TblGeneralProg tmp = new TblGeneralProg();
                tmp.settable_code(rs.getString(1));
                tmp.settable_type(rs.getString(2));
                tmp.setprogram(rs.getString(3));                
                tblgens.add(tmp);
            tmp = null;//Liberar Espacio JJCastro
            }
        }}catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * M�todo que guetea todas los detalles de TblGeneralProg
     * @autor.......ricardo rosero
     * @throws......SQLException
     * @version.....1.0.     
     * @return......Vector de objetos TblGeneralProg
     **/     
    public Vector getTodosTblGeneralProg(){
        return this.tblgens;
    }        
    
    /**
    * M�todo: buscarClientesEmail
    * @autor: ....... Ing. David Pi�a Lopez
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public void buscarClientesEmail()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        tblgens = null;
        String query = "BUSCAR_CLIENTES_EMAIL";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                tblgens =  new Vector();                
                while ( rs.next() ){
                    tblgens.add( rs.getString( "program" ) );
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
    * M�todo: buscarEmailsCliente
     *@param cod Codigo del cliente
    * @autor: ....... Ing. David Pi�a Lopez     
    * @throws ....... SQLException
    * @version ...... 1.0
    */
    public void buscarEmailsCliente( String cod )throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        tblgens = null;
        String query = "BUSCAR_EMAILS_CLIENTE";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString( 1, cod );
                rs = st.executeQuery();
                
                tblgens =  new Vector();                
                while (rs.next()){
                    tg = new TblGeneralProg();
                    tg.settable_type( rs.getString( "table_type" ) );//tipo EMAIL
                    tg.settable_code( rs.getString( "table_code" ) );//usuario email
                    tg.setFC( rs.getString( "descripcion" ) ); //El email
                    tg.setprogram( rs.getString( "referencia" ) );//I (Interno) o E (Externo)
                    tblgens.add( tg );
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    

/**
    * M�todo: existeTblGeneralProg
    * @autor: ....... Ing. Ricardo Rosero
    * @param: ....... table_type, table_code
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public boolean existeTblGeneralProg( String table_type, String table_code, String program ) throws SQLException {
        boolean result = false; // El resultado de la funcion
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "EXISTE";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString( 1, table_type);
                st.setString( 2, table_code);
                st.setString( 3, program);
                rs = st.executeQuery();
                result = rs.next();
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return result;
    }



private static String ESTA_ANULADO = "SELECT tablagen_prog FROM tablagen_prog WHERE table_type = ? AND table_code = ? AND program = ? AND reg_status = 'A'";

/**
    * M�todo: estaAnuladoTblGeneralProg
    * @autor: ....... Ing. Ricardo Rosero
    * @param: ....... table_type, table_code
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public boolean estaAnuladoTblGeneralProg( String table_type, String table_code, String program ) throws SQLException {
        boolean result = false; // El resultado de la funcion
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "ESTA_ANULADO";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString( 1, table_type);
                st.setString( 2, table_code);
                st.setString( 3, program);
                rs = st.executeQuery();
                result = rs.next();
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return result;
    }





/**
    * M�todo: listarTodos
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public void listarTodos(String table_type, String table_code, String program)throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        tblgens = null;
        String query = "SQL5";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, table_type+"%");
                st.setString(2, program+"%");
                st.setString(3, table_code+"%");
                rs = st.executeQuery();
                tblgens =  new Vector();
                while (rs.next()){
                    tblgens.add(tg.load(rs));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }



/**
     * M�todo que modifica un TblGeneralProg
     * @autor.......ricardo rosero
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void updateTblGeneralProg() throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        String query = "SQL_UPDATE";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, tg.getFm());
            ps.setString(2, tg.getUm());            
            ps.setString(3, tg.gettable_type());
            ps.setString(4, tg.gettable_code());
            ps.setString(5, tg.getprogram());
            ps.executeUpdate(); 
        }}catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }




/**
     * M�todo que actualiza el reg_status de un TblGeneralProg anulado
     * @autor.......ricardo rosero
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void updateTblGeneralProgAnulado() throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        String query = "ANULAR";//JJCastro fase2
        try{            
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, tg.getUm ());
            ps.setString(2, tg.gettable_type());
            ps.setString(3, tg.gettable_code());
            ps.setString(4, tg.getprogram());
            ps.executeUpdate();
        }}catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
}
