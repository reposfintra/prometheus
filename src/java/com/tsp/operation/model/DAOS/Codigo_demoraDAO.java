/*
 * Codigo_demoraDAO.java
 *
 * Created on 24 de junio de 2005, 05:14 PM
 */

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  Jose
 */
public class Codigo_demoraDAO {
    private Codigo_demora codigo_demora;
    private Vector codigo_demoras;
    /** Creates a new instance of Codigo_demoraDAO */
    public Codigo_demoraDAO() {
    }

    public Codigo_demora getCodigo_demora() {
        return codigo_demora;
    }

    public void setCodigo_demora(Codigo_demora codigo_demora) {
        this.codigo_demora = codigo_demora;
    }

    public Vector getCodigo_demoras() {
        return codigo_demoras;
    }

    public void setCodigo_demoras(Vector codigo_demoras) {
        this.codigo_demoras = codigo_demoras;
    }
    
    public void insertCodigo_demora() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("Insert into codigo_demora (cod_cdemora,desc_cdemora,creation_user,creation_date,user_update,last_update,cia,rec_status,base) values(?,?,?,'now()',?,'now()',?,' ',?)");
                st.setString(1,codigo_demora.getCodigo());
                st.setString(2,codigo_demora.getDescripcion());
                st.setString(3,codigo_demora.getCreation_user());
                st.setString(4,codigo_demora.getUser_update());
                st.setString(5,codigo_demora.getCia());
                st.setString(6,codigo_demora.getBase()); 
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DEL CODDIGO DE DEMORA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void searchCodigo_demora(String cod)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from codigo_demora where cod_cdemora=? and rec_status != 'A'");
                st.setString(1,cod);
                rs= st.executeQuery();
                codigo_demoras = new Vector();
                while(rs.next()){
                    codigo_demora = new Codigo_demora();
                    codigo_demora.setCodigo(rs.getString("cod_cdemora"));
                    codigo_demora.setDescripcion(rs.getString("desc_cdemora"));
                    codigo_demoras.add(codigo_demora);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DEL CODIGO DE DEMORA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public boolean existCodigo_demora(String cod) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("select * from codigo_demora where cod_cdemora=? and rec_status != 'A'");
                st.setString(1,cod);
                rs = st.executeQuery();
                if (rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL CODIGO DE LA DEMORA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
    public void listCodigo_demora()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from codigo_demora where rec_status != 'A' order by cod_cdemora");
                rs= st.executeQuery();
                codigo_demoras = new Vector();
                while(rs.next()){
                    codigo_demora = new Codigo_demora();
                    codigo_demora.setCodigo(rs.getString("cod_cdemora"));
                    codigo_demora.setDescripcion(rs.getString("desc_cdemora"));
                    codigo_demora.setBase(rs.getString("base"));
                    codigo_demoras.add(codigo_demora);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DEL CODIGO DE DEMORA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public void updateCodigo_demora(String cod, String desc, String usu, String base) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("update codigo_demora set desc_cdemora=?, last_update='now()', user_update=?, rec_status=' ', base = ? where cod_cdemora = ? ");
                st.setString(1,desc);
                st.setString(2,usu);
                st.setString(3,base);
                st.setString(4,cod);
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACI�N DEL CODIGO DE DEMORA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void anularCodigo_demora() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("update codigo_demora set rec_status='A',last_update='now()', user_update=? where cod_cdemora = ?");
                st.setString(1,codigo_demora.getUser_update());
                st.setString(2,codigo_demora.getCodigo());
                st.executeUpdate();
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DEL CODIGO DE DEMORA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void searchDetalleCodigo_demoras(String cod, String desc, String base) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        codigo_demoras=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            boolean sw = false;
            if(con!=null){
                st = con.prepareStatement("Select * from codigo_demora where cod_cdemora like ? and desc_cdemora like ? and base like ? and rec_status != 'A'");
                st.setString(1, cod+"%");
                st.setString(2, desc+"%");
                st.setString(3, base+"%");
                rs = st.executeQuery();
                codigo_demoras = new Vector();
                while(rs.next()){
                    codigo_demora = new Codigo_demora();
                    codigo_demora.setCodigo(rs.getString("cod_cdemora"));
                    codigo_demora.setDescripcion(rs.getString("desc_cdemora"));
                    codigo_demora.setRec_status(rs.getString("rec_status"));
                    codigo_demora.setUser_update(rs.getString("user_update"));
                    codigo_demora.setCreation_user(rs.getString("creation_user"));
                    codigo_demora.setBase(rs.getString("base"));
                    codigo_demoras.add(codigo_demora);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS ACTIVIDADES" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    public Vector listarCodigo_demora()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from codigo_demora where rec_status != 'A' order by cod_cdemora");
                rs= st.executeQuery();
                codigo_demoras = new Vector();
                while(rs.next()){
                    codigo_demora = new Codigo_demora();
                    codigo_demora.setCodigo(rs.getString("cod_cdemora"));
                    codigo_demora.setDescripcion(rs.getString("desc_cdemora"));
                    codigo_demora.setBase(rs.getString("base"));
                    codigo_demoras.add(codigo_demora);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DEL CODIGO DE DEMORA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return codigo_demoras;
    }
    
    /**
     *  @autor Tito Andr�s
     *
     */
    public String obtenerCodigo_demora(String cod)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String desc = "";
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from codigo_demora where cod_cdemora=?");
                ////System.out.println(st);
                st.setString(1,cod);
                rs= st.executeQuery();
                codigo_demoras = new Vector();
                if(rs.next()){
                    desc = rs.getString("desc_cdemora");
                }
                ////System.out.println("--" + desc);
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DEL CODIGO DE DEMORA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        return desc;
        
    }
}
