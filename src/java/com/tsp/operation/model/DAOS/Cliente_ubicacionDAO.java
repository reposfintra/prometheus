/************************************************************************
 * Nombre clase: Cliente_ubicacionDAO.java                              *
 * Descripci�n: Clase que maneja las consultas de las tabla             *
 *              cliente_ubicacion.                                      *
 * Autor: Ing. Jose de la rosa                                          *
 * Fecha: Created on 20 de enero de 2006, 08:40 AM                      *
 * Versi�n: Java 1.0                                                    *
 * Copyright: Fintravalores S.A. S.A.                              *
 ***********************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

public class Cliente_ubicacionDAO {
    private Cliente_ubicacion cliente_ubicacion;
    private Vector Veccliente_ubicacion;
    //Kreales
    /*
     *Atributo creado para listar en el despacho las ubicaciones
     *relacionadas a un cliente y que tengan productos en inventario.
     */
    private TreeMap ubicacion_prod;
    
    private static String SQL_INSERT = "Insert into cliente_ubicacion (cedula,cod_ubicacion,creation_user,creation_date,user_update,last_update,dstrct,reg_status,base) values(?,?,?,'now()',?,'now()',?,'',?)";
    private static String SQL_EXISTE = "SELECT cedula FROM cliente_ubicacion WHERE cedula=? AND cod_ubicacion=? AND reg_status != 'A'";
    private static String SQL_SEARCH = "SELECT * FROM cliente_ubicacion WHERE cedula=? AND cod_ubicacion=? AND reg_status != 'A'";
    private static String SQL_ANULAR = "UPDATE cliente_ubicacion SET reg_status='A',last_update='now()', user_update=? WHERE cedula = ? AND cod_ubicacion=?";
    private static String SQL_DETALLE = "SELECT * FROM cliente_ubicacion WHERE cedula LIKE ? AND cod_ubicacion LIKE ? AND reg_status != 'A'";
    private static String SQL_UPDATE = "UPDATE cliente_ubicacion SET reg_status='',last_update='now()', user_update=? WHERE cedula = ? AND cod_ubicacion=?";
    /*
     *Este query busca una lista de ubicaciones donde hay productos disponibles
     *en la tabla de inventario_discrepancia. -- kreales
     */ 
    private static String SQL_LISTARUBPRO = "SELECT 	U.cod_ubicacion, " +
    "		U.descripcion," +
    "		dp.cod_cdiscrepancia" +
    "      FROM 	inventario_discrepancia id, " +
    "		ubicacion u," +
    "		discrepancia_producto dp" +
    "      WHERE 	id.codcli = ? " +
    "		and id.numpla_despacho='' " +
    "		and u.cod_ubicacion=ubicacion" +
    "		and dp.distrito=id.dstrct" +
    "		and dp.num_discre = id.num_discre" +
    "		and dp.numpla = id.numpla" +
    "		and dp.cod_producto = id.cod_producto" +
    "		and dp.cod_cdiscrepancia not in (select table_code from tablagen where table_type = 'NOINV')";
    
    /*
     *Este query busca una lista de ubicaciones sin importar si los productos han sido
     *despachados en la tabla de inventario_discrepancia. -- kreales
     */ 
    private static String SQL_LISTARTODOSUBPRO =  "SELECT 	U.cod_ubicacion, " +
    "		U.descripcion," +
    "		dp.cod_cdiscrepancia" +
    "      FROM 	inventario_discrepancia id, " +
    "		ubicacion u," +
    "		discrepancia_producto dp" +
    "      WHERE 	id.codcli = ? " +
    "		and u.cod_ubicacion=ubicacion" +
    "		and dp.distrito=id.dstrct" +
    "		and dp.num_discre = id.num_discre" +
    "		and dp.numpla = id.numpla" +
    "		and dp.cod_producto = id.cod_producto" +
    "		and dp.cod_cdiscrepancia not in (select table_code from tablagen where table_type = 'NOINV')";
    
     /** Creates a new instance of Cliente_ubicacionDAO */
    public Cliente_ubicacionDAO () {
    }
    
    /**
     * Metodo: getCliente_ubicacion, permite retornar un objeto de registros de cliente_ubicacion.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Cliente_ubicacion getCliente_ubicacion () {
        return cliente_ubicacion;
    }
    
    /**
     * Metodo: setCliente_ubicacion, permite obtener un objeto de registros de cliente_ubicacion.
     * @autor : Ing. Jose de la rosa
     * @param : objeto
     * @version : 1.0
     */
    public void setCliente_ubicacion (Cliente_ubicacion cliente_ubicacion) {
        this.cliente_ubicacion = cliente_ubicacion;
    }
    
    /**
     * Metodo: getCliente_ubicaciones, permite retornar un vector de registros de cliente_ubicacion.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Vector getCliente_ubicaciones () {
        return Veccliente_ubicacion;
    }
    /**
     * Metodo: setCliente_ubicaciones, permite obtener un vector de registros de cliente_ubicacion.
     * @autor : Ing. Jose de la rosa
     * @param : vector
     * @version : 1.0
     */
    public void setCliente_ubicaciones (Vector Veccliente_ubicacion) {
        this.Veccliente_ubicacion = Veccliente_ubicacion;
    }
    
    /**
     * Metodo: insertCliente_ubicacion, permite ingresar un registro a la tabla cliente_ubicacion.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void insertCliente_ubicacion () throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_INSERT);
                st.setString (1,cliente_ubicacion.getCliente ());
                st.setString (2,cliente_ubicacion.getUbicacion ());
                st.setString (3,cliente_ubicacion.getCreation_user ());
                st.setString (4,cliente_ubicacion.getUser_update ());
                st.setString (5,cliente_ubicacion.getDstrct ());
                st.setString (6,cliente_ubicacion.getBase ());
                st.executeUpdate ();
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA INSERCION DE LA TABLA CLIENTE UBICACI�N " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally{
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
    }
    
    /**
     * Metodo: existCliente_ubicacion, permite buscar un cliente_ubicacion dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : numero de cedula y el codigo de la ubicacion
     * @version : 1.0
     */
    public boolean existCliente_ubicacion (String cedula, String ubicacion) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_EXISTE);
                st.setString (1,cedula);
                st.setString (2,ubicacion);
                rs = st.executeQuery ();
                if (rs.next ()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA BUSQUEDA DE LA UBICACION DEL CLIENTE " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally{
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
        return sw;
    }
    
    /**
     * Metodo: searchCliente_ubicacion, permite buscar una cliente_ubicacion dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : numero de cedula y el codigo de la ubicacion
     * @version : 1.0
     */
    public void searchCliente_ubicacion (String cedula, String ubicacion)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        cliente_ubicacion = null;
        try {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if(con!=null){
                st = con.prepareStatement (SQL_SEARCH);
                st.setString (1,cedula);
                st.setString (2,ubicacion);
                ////System.out.println("SCLI: "+st);
                rs= st.executeQuery ();                
                if(rs.next ()){
                    cliente_ubicacion = new Cliente_ubicacion ();
                    cliente_ubicacion = Cliente_ubicacion.load (rs);                   
                }
            }
            
        }catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA LISTA DE LA UBICACI�N DEL CLIENTE " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally{
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
        
    }
    
     /**
     * Metodo: searchDetalleCliente_ubicacion, permite buscar un cliente_ubicacion dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : numero de cedula y el codigo de la ubicacion.
     * @version : 1.0
     */
    public void searchDetalleCliente_ubicacion(String cedula, String ubicacion) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            boolean sw = false;
            if(con!=null){
                st = con.prepareStatement(SQL_DETALLE);
                st.setString (1,cedula+"%");
                st.setString (2,ubicacion+"%");
                rs = st.executeQuery();
                Veccliente_ubicacion = new Vector();
                while(rs.next()){
                    cliente_ubicacion = new Cliente_ubicacion ();
                    cliente_ubicacion = Cliente_ubicacion.load (rs);
                    Veccliente_ubicacion.add (cliente_ubicacion);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS CODIGOS DE LA DISCREPANCIAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo: anularCliente_ubicacion, permite anular un cliente_ubicacion.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void anularCliente_ubicacion () throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_ANULAR);
                st.setString (1,cliente_ubicacion.getUser_update ());
                st.setString (2,cliente_ubicacion.getCliente ());
                st.setString (3,cliente_ubicacion.getUbicacion ());
                st.executeUpdate ();
            }
        }
        catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA ANULACION DE LA UBICI�N " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally{
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
    }
    
    /**
     * Metodo: updateCliente_ubicacion, permite anular un cliente_ubicacion.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void updateCliente_ubicacion () throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_UPDATE);
                st.setString (1,cliente_ubicacion.getUser_update ());
                st.setString (2,cliente_ubicacion.getCliente ());
                st.setString (3,cliente_ubicacion.getUbicacion ());
                st.executeUpdate ();
            }
        }
        catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA ANULACION DE LA UBICI�N " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally{
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
    }    
    
    /**
     * Getter for property ubicacion_prod.
     * @return Value of property ubicacion_prod.
     */
    public java.util.TreeMap getUbicacion_prod() {
        return ubicacion_prod;
    }
    
    /**
     * Setter for property ubicacion_prod.
     * @param ubicacion_prod New value of property ubicacion_prod.
     */
    public void setUbicacion_prod(java.util.TreeMap ubicacion_prod) {
        this.ubicacion_prod = ubicacion_prod;
    }
    
    /**
     * M�todo que busca una lista ubicaciones dado el codigo de un cliente,
     * en la tabla de inventario donde haya existencia de producto.
     * @param.......Recibe un codigo de cliente
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.     
     **/     
    public void listarUbicaciones(String codcli)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ubicacion_prod = new TreeMap();
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(SQL_LISTARUBPRO);
                st.setString(1,codcli);
                ////System.out.println("Query "+st.toString());
                rs = st.executeQuery();
                ubicacion_prod.put(" Seleccione un Item","");
                while(rs.next()){
                    ubicacion_prod.put(rs.getString("descripcion"), rs.getString("cod_ubicacion"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS UBICACIONES DE UN CLIENTE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    /**
     * M�todo que busca una lista ubicaciones dado el codigo de un cliente,
     * en la tabla de inventario sin importar la existencia de producto.
     * @param.......Recibe un codigo de cliente
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.     
     **/     
    public void listarTodasUbicaciones(String codcli)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ubicacion_prod = new TreeMap();
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(SQL_LISTARTODOSUBPRO);
                st.setString(1,codcli);
                ////System.out.println("Query "+st.toString());
                rs = st.executeQuery();
                ubicacion_prod.put(" Seleccione un Item","");
                while(rs.next()){
                    ubicacion_prod.put(rs.getString("descripcion"), rs.getString("cod_ubicacion"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS UBICACIONES DE UN CLIENTE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
}
