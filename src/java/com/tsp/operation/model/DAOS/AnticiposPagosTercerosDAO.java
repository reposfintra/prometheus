/***************************************
 * Nombre Clase ............. AnticiposPagosTercerosDAO.java
 * Descripci�n  .. . . . . .  Ejecutamos los SQL para los anticipos pagos a terceros
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  04/08/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 **********************************************************************/

package com.tsp.operation.model.DAOS;


import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.TransaccionService;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class AnticiposPagosTercerosDAO extends MainDAO{


    public  double  VLR_TRANSFERENCIA   = 2205;
    public  double  VLR_EFECTIVO        = 12000;

    //Julio Barros
    public  double  VLR_EFECTIVO_BB     = 12000;
    public  double  VLR_TRANSFERENCIA_BB= 4781;

    private String  SEPARADOR           = "<BR>";
    private String  TABLA_VALORES       = "VALDEFAULT";
    private String  TABLA_ANTICIPO      = "PORANT";


    private String  TERCERO             = "TER";
    private String  PROPIETARIO         = "PRO";

    private String  SERIE_TRANSFERENCIA = "TRANSFERENCIA";
    private String COLUMNAS  =
    " a.id,                           "+
    " a.reg_status,                           "+
    " a.observacion_anulacion,                           "+
    " a.dstrct    ,                   "+
    " a.agency_id ,                   "+
    " a.pla_owner ,                   "+
    " a.planilla  ,                   "+
    " a.supplier  ,                   "+
    " a.proveedor_anticipo ,          "+
    " a.concept_code ,                "+
    " a.vlr      ,                    "+
    " a.vlr_for  ,                    "+
    " a.currency ,                    "+
    " SUBSTRING(a.creation_date,1,16) as fecha_anticipo,           "+
    " SUBSTRING(a.fecha_envio_fintra,1,16) as fecha_envio_fintra,           "+

    " a.aprobado,                     "+
    " substr(a.fecha_autorizacion,1,16)   as fecha_autorizacion ,   "+
    " a.user_autorizacion  ,          "+

    " a.transferido,                  "+
    " substr(a.fecha_transferencia,1,16)  as fecha_transferencia ,  "+
    " a.banco_transferencia ,         "+
    " a.cuenta_transferencia ,        "+
    " a.tcta_transferencia ,          "+
    " a.user_transferencia ,          "+

    " a.banco,                        "+
    " a.sucursal,                     "+
    " a.nombre_cuenta,                "+
    " a.cuenta,                       "+
    " a.tipo_cuenta,                  "+
    " a.nit_cuenta,                   "+

    " substr(a.fecha_migracion,1,16)  as fecha_migracion ,  "+
    " a.user_migracion ,              "+

    " a.factura_mims ,                "+
    " a.vlr_mims_tercero ,            "+
    " a.vlr_mims_propietario,         "+
    " a.estado_pago_tercero,          "+
    " a.estado_desc_propietario,      "+
    " substr(a.fecha_pago_tercero,1,16)      as fecha_pago_tercero ,      "+
    " substr(a.fecha_desc_propietario,1,16)  as fecha_desc_propietario ,  "+

    " a.cheque_pago_tercero ,         "+
    " a.cheque_desc_propietario,      "+
    " a.corrida_pago_tercero,         "+
    " a.corrida_desc_propietario,     "+

    " get_nombreagencia( substring(a.agency_id,1,2) ) as nombre_agencia,    "+

    " b.payment_name                   as nombre_proveedor,  "+
    " c.nombre                         as nombre_prpietario, "+

    " a.porcentaje,                  "+
    " a.vlr_descuento,               "+
    " a.vlr_neto,                    "+
    " a.vlr_combancaria,             "+
    " a.vlr_consignacion,            "+
    " a.reanticipo,                  "+
    " a.cedcon,                      "+
    " a.transferencia,               "+
    " a.liquidacion,                 "+
    " coalesce(a.secuencia,0) as secuencia ,                   "+
    " substring(calculo_dif_ant(a.creation_date,a.fecha_transferencia),1,5) as diferencia ,"+
    "rango_anticipos(calculo_dif_ant(a.creation_date,a.fecha_transferencia)) AS rango,"+
    "  a.creation_user              ";
    

    public AnticiposPagosTercerosDAO() {
        super("AnticiposPagosTercerosDAO.xml");
    }
    public AnticiposPagosTercerosDAO(String dataBaseName) {
        super("AnticiposPagosTercerosDAO.xml", dataBaseName);
    }



    public String reset(String val){
        if(val==null)
            val="";
        return val;
    }



    public String resetFechas(String val) throws Exception{
        if( val==null ||  val.equals("0099-01-01 00:00") )
            val = "";
        return val;
    }




    /**
     * Metodo que busca la serie para transferencia
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public String getSerieTransferencia()throws Exception{
        Connection con = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_BUSCAR_SERIE";
        String            number      = "";
        try{

            con = this.conectarJNDI( query );

            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, SERIE_TRANSFERENCIA  );
            rs = st.executeQuery();
            if(rs.next())
                number = rs.getString("proximo");

        }catch(Exception e) {
            throw new SQLException(" getSerieTransferencia  -> " +e.getMessage());
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return number;

    }



    /**
     * Metodo que actualiza la serie
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public void  actualizarSerie( )throws Exception{
        Connection con = null;
        PreparedStatement st          = null;
        String            query       = "SQL_UPDATE_SERIE";
        try{

            con = this.conectarJNDI( query );
            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, SERIE_TRANSFERENCIA  );
            st.setString(2, SERIE_TRANSFERENCIA  );
            st.execute();

        }catch(Exception e) {
            throw new SQLException(" actualizarSerie  -> " +e.getMessage());
        } finally {
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }








    /**
     * Metodo que devuelve la lista de cuentas del propietario
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public List  getListCuentas(String nit, String tipo)throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        List              lista       = new LinkedList();
        String            query       = "SQL_LISTA_CUENTAS";
        try{


            con = this.conectarJNDI(query);
            String valor = (tipo.equals("ALL"))?" like '%' " : " ='"+ tipo +"'";
            String sql   =  this.obtenerSQL(query).replaceAll("#VALOR#", valor );
            st = con.prepareStatement( sql );
            st.setString(1, nit  );
            //.out.println(sql);
            rs = st.executeQuery();
            while(rs.next()){

                Hashtable  cta  =  new  Hashtable();

                cta.put("dstrct",        reset(  rs.getString("dstrct") )        );
                cta.put("idmims",        reset(  rs.getString("idmims") )        );
                cta.put("nit",           reset(  rs.getString("nit") )           );
                cta.put("banco",         reset(  rs.getString("banco") )         );
                cta.put("sucursal",      reset(  rs.getString("sucursal") )      );
                cta.put("cuenta",        reset(  rs.getString("cuenta") )        );
                cta.put("tipo_cuenta",   reset(  rs.getString("tipo_cuenta") )   );
                cta.put("nombre_cuenta", reset(  rs.getString("nombre_cuenta") ) );
                cta.put("cedula_cuenta", reset(  rs.getString("cedula_cuenta") ) );
                cta.put("secuencia",     reset(  rs.getString("secuencia") )     );
                cta.put("primaria",      reset(  rs.getString("primaria") )      );

                lista.add( cta );
                cta = null;//Liberar Espacio JJCastro
            }


        }catch(Exception e) {
            throw new SQLException(" getListCuentas  -> " +e.getMessage());
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;

    }




    /**
     * Metodo que devuelve informaci�n de tablagen dependiendo el tipo
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public List  getTablaGen(String tipo)throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        List              lista       = new LinkedList();
        String            query       = "SQL_TABLAGEN";
        try{
            con = this.conectarJNDI( query );

            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, tipo  );
            rs = st.executeQuery();

            //.out.println( this.obtenerSQL(query) );
            //.out.println( "---> " + tipo );

            while(rs.next()){

                System.out.println(rs.getString("table_code"));

                Hashtable  tabla  =  new  Hashtable();
                tabla.put("codigo",           reset( rs.getString("table_code") ) );
                tabla.put("descripcion",      reset( rs.getString("descripcion")) );
                tabla.put("referencia",       reset( rs.getString("referencia") ) );

                lista.add( tabla );
                tabla = null;//Liberar Espacio JJCastro
            }


        }catch(Exception e) {
            throw new SQLException(" getTablaGen  -> " +e.getMessage());
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;

    }





    /**
     * Metodo que devuelve nombre del NIT
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public String  getNameNit(String nit)throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_NAME_NIT";
        String            name        = "";
        try{
            con = this.conectarJNDI( query );


            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, nit  );
            rs = st.executeQuery();
            if(rs.next())
                name = reset( rs.getString(1) );

        }catch(Exception e) {
            throw new SQLException(" getNameNit  -> " +e.getMessage());
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return name;
    }


    /**
     * Metodo que devuelve nombre del proveedor
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public String  getNamePropietario(String nit)throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_NAME_PROVEEDOR";
        String            name        = "";
        try{
            con = this.conectarJNDI( query );

            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, nit  );
            rs = st.executeQuery();
            if(rs.next())
                name = reset( rs.getString(1) );

        }catch(Exception e) {
            throw new SQLException(" getNamePropietario  -> " +e.getMessage());
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return name;
    }




    /**
     * Metodo que asina una cuenta para un anticipo
     * @param nit
     * @param sec
     * @param anticipo
     * @param Banco
     * @return 
     * @throws java.lang.Exception 
     * @autor: ....... Fernel Villacob
     * @version ...... 1.0
     */
    public String  asignatCta(String nit, String sec, String anticipo,String Banco)throws Exception{
        StringStatement st = null;
        String query = "SQL_ASIGNAR_CTA";
        String sql = "";
        String cadena = "";
        try{

            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, anticipo  );
            st.setString(2, nit       );
            st.setString(3, sec       );
            cadena = st.getSql();
            cadena += ";" + aplicarValores(nit,anticipo,Banco);

        }catch(Exception e) {
            throw new SQLException(" asignatCta  -> " +e.getMessage());
        } finally {
           return cadena;
        }
    }

    /**
     * Metodo que asina una cuenta por default para un anticipo
     * @param nit
     * @param anticipo
     * @param Banco
     * @return 
     * @throws java.lang.Exception
     * @autor: ....... julio Barros
     * @version ...... 1.0
     */
    public String  asignatCtaDef(String nit, String anticipo,String Banco)throws Exception{
       
        StringStatement st = null;
        String query = "SQL_ASIGNAR_CTA_DEF";
        String sql = "";
        String cadena = "";
        try{

            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, anticipo  );
            st.setString(2, nit       );
            cadena = st.getSql();
            cadena += ";" + aplicarValores(nit, anticipo, Banco);
        }catch(Exception e) {
            throw new SQLException(" asignatCta  -> " +e.getMessage());
        } finally {
           return cadena;
        }
    }


    /**
     * Metodo que busca el descuento para el nit
     * @param nit
     * @param anticipo
     * @param Banco
     * @return 
     * @throws java.lang.Exception 
     * @autor: ....... Fernel Villacob
     * @version ...... 1.0
     */
    public String aplicarValores(String  nit, String anticipo,String Banco)throws Exception{
      
        StringStatement st = null;
        String query = "SQL_TOTALES";
        String sql = "";
        String cadena = "";
        double              comision    = 0;
        try{
            AnticiposTerceros ANT  = getAnticipo(anticipo);
            String reanticipo      = ANT.getReanticipo();

            String esEfectivo = ANT.getDes_concept();
            //.out.println("esEFECTIVO__"+esEfectivo);

            double valor           = ANT.getVlr();
            String tipoCTA         = ANT.getTipo_cuenta();
            String bancof          = ANT.getBanco();

            int    secuencia       = ANT.getSecuencia();

            double porcentaje      = this.descuento(nit);
           


            if( reanticipo.equals("N") ){  // si es anticipo, buscamos el valor del anticipo por default.
                Hashtable  hh = getInfoBanco(TABLA_VALORES, TABLA_ANTICIPO);
                if(hh!=null)
                    porcentaje = Double.parseDouble( (String)hh.get("referencia") );
            }


            double descuento       = ( valor * porcentaje )/100;
            double neto            = valor - descuento ;
            //Julio Barros  -  Modificacion Asignacion de comisiones


            if( Banco.equals("OCCIDENTE") || Banco.equals("23") ){
                Banco="BANCO OCCIDENTE";
            }

            if( Banco.equals("null"     ) || Banco.equals("07") || Banco.equals("7B") ){
                Banco="BANCOLOMBIA";
            }

            //.out.println("bancof:_"+bancof);
           // .out.println("Banco:__"+Banco);
            if ( Banco.equals(bancof) ){
                comision        = (tipoCTA.equals("EF"))?this.VLR_EFECTIVO : this.VLR_TRANSFERENCIA ;
            }else if (!Banco.equals(bancof)){
                comision        = (tipoCTA.equals("EF"))?this.VLR_EFECTIVO_BB : this.VLR_TRANSFERENCIA_BB ;
            }
            if(esEfectivo.equals("EFECTIVO")){
                comision = 0;
            }
            double consignar       = (int)Math.round( neto - comision );


            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setDouble(1 , porcentaje );
            st.setDouble(2 , descuento  );
            st.setDouble(3 , neto       );
            st.setDouble(4 , comision   );
            st.setDouble(5 , consignar  );
            st.setString(6 , anticipo   );
            cadena=st.getSql();


        } catch (Exception e) {
            throw new SQLException(" aplicarValores  -> " + e.getMessage());
        } finally {
            return cadena;
        }
    }

    /**
     * Metodo que busca el descuento para el nit
     * @param nit
     * @param anticipo
     * @return 
     * @throws java.lang.Exception
     * @autor: ....... Julio Barros
     * @version ...... 1.0
     */
    public String aplicarValores2(String  nit, String anticipo)throws Exception{
       // Connection          con         = null;
        StringStatement st = null;
        String query = "SQL_TOTALES";
        String sql = "";
        String cadena = "";
        double              comision    = 0;
        try{
            AnticiposTerceros ANT  = getAnticipo(anticipo);
            String reanticipo      = ANT.getReanticipo();


            double valor           = ANT.getVlr();
            String tipoCTA         = ANT.getTipo_cuenta();
            String bancof          = ANT.getBanco();

            int    secuencia       = ANT.getSecuencia();

            double porcentaje      = this.descuento(nit);

            if( reanticipo.equals("N") ){  // si es anticipo, buscamos el valor del anticipo por default.
                Hashtable  hh = getInfoBanco(TABLA_VALORES, TABLA_ANTICIPO);
                if(hh!=null)
                    porcentaje = Double.parseDouble( (String)hh.get("referencia") );
            }


            double descuento       = ( valor * porcentaje )/100;
            double neto            = valor - descuento ;
            //Julio Barros  -  Modificacion Asignacion de comisiones
            comision        = 0 ;

            double consignar       = (int)Math.round( neto - comision );


            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setDouble(1 , porcentaje );
            st.setDouble(2 , descuento  );
            st.setDouble(3 , neto       );
            st.setDouble(4 , comision   );
            st.setDouble(5 , consignar  );
            st.setString(6 , anticipo   );
            cadena=st.getSql();


        }catch(Exception e) {
            throw new SQLException(" aplicarValores  -> " +e.getMessage());
        } finally {
         return cadena;
        }
    }



    /**
     * Metodo que busca el descuento para el nit
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public void  asignarDescuento(String  nit, double  valor)throws Exception{
        PreparedStatement st          = null;
        String            query       = "";
        Connection        con         = null;
        try{

            double descuento =  this.descuento(nit);
            query  =  (  descuento== 0)? "SQL_INSERT_DESCUENTO" :  "SQL_APLICAR_DESCUENTO" ;

            con = this.conectarJNDI(query);
            String  sql =  this.obtenerSQL(query).replaceAll("#NIT#", nit);
            st = con.prepareStatement( sql );
            st.setDouble(1 , valor );
            st.setString(2 , nit   );
            st.execute();

        }catch(Exception e) {
            throw new SQLException(" asignarDescuento  -> " +e.getMessage());
        } finally {
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }





    /**
     * M�todo que busca el anticipo
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  AnticiposTerceros getAnticipo( String anticipo ) throws Exception{
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_ANTICIPOS_TERCERO";
        AnticiposTerceros  ANT     = null;

        try{

            con = this.conectarJNDI(query);
            String sql   =   this.obtenerSQL( query ).replaceAll("#COLUMNAS#", this.COLUMNAS);
            st= con.prepareStatement(sql);
            st.setString(1, anticipo  );
            rs=st.executeQuery();
            if( rs.next() ){
                ANT = load(rs);
                ANT.setDes_concept(  reset(rs.getString("descripcion")!=null?rs.getString("descripcion"):""   ));
                //.out.println("DES_CONCEPT__"+ANT.getDes_concept());
            }
        }catch(Exception e){
            throw new Exception( " getAnticipo " + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return ANT;

    }




    /**
     * M�todo que agrupa anticipos por informacion del banco  a transferir
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List agrupar( String anticipos[] ) throws Exception{

        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_AGRUPAR";
        List               lista   = new LinkedList();

        try{

            String ids = "";
            for(int i=0;i<anticipos.length;i++){
                if(!ids.equals(""))  ids +=",";
                ids += "'" + anticipos[i] + "'";
            }


            con = this.conectarJNDI(query);
            String sql   =   this.obtenerSQL( query ).replaceAll("#ANTICIPOS#", ids );
            st= con.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                AnticiposTerceros anticipo = new  AnticiposTerceros();
                
                anticipo.setProveedor_anticipo(reset(rs.getString("proveedor_anticipo")));
                anticipo.setBanco(reset(rs.getString("banco")));
                anticipo.setNombre_cuenta(reset(rs.getString("nombre_cuenta")));
                anticipo.setCuenta(reset(rs.getString("cuenta")));
                anticipo.setTipo_cuenta(reset(rs.getString("tipo_cuenta")));
                anticipo.setNit_cuenta(reset(rs.getString("nit_cuenta")));
                anticipo.setPla_owner(reset(rs.getString("pla_owner")));
                anticipo.setConductor(reset(rs.getString("cedcon")));
                anticipo.setVlr(rs.getDouble("vlr"));
                anticipo.setVlrConsignar(rs.getDouble("vlr_consignacion"));
                anticipo.setReanticipo(reset(rs.getString("reanticipo"))         );

                lista.add(  anticipo );
            }


        }catch(Exception e){
            throw new Exception( "agrupar " + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;

    }



    /**
     * M�todo que agrupa anticipos por informacion del banco  a transferir
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List getAnticipos( String anticipos[] ) throws Exception{

        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_GET_ANTICIPOS";
        List               lista   = new LinkedList();

        try{

            String ids = "";
            for(int i=0;i<anticipos.length;i++){
                if(!ids.equals(""))  ids +=",";
                ids += "'" + anticipos[i] + "'";
            }


            con = this.conectarJNDI(query);
            String sql   =   this.obtenerSQL( query ).replaceAll("#ANTICIPOS#", ids );
            st= con.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                AnticiposTerceros anticipo = new  AnticiposTerceros();
                anticipo.setProveedor_anticipo( reset( rs.getString("proveedor_anticipo") )  );
                anticipo.setBanco             ( reset( rs.getString("banco") )               );
                anticipo.setNombre_cuenta     ( reset( rs.getString("nombre_cuenta") )       );
                anticipo.setCuenta            ( reset( rs.getString("cuenta") )              );
                anticipo.setTipo_cuenta       ( reset( rs.getString("tipo_cuenta") )         );
                anticipo.setNit_cuenta        ( reset( rs.getString("nit_cuenta") )          );
                anticipo.setPla_owner         ( reset( rs.getString("pla_owner") )           );
                anticipo.setPlanilla         ( reset( rs.getString("planilla") )           );
                anticipo.setConductor         ( reset( rs.getString("cedcon") )           );
                anticipo.setVlr               (        rs.getDouble("vlr")                   );
                anticipo.setVlrConsignar      (        rs.getDouble("vlr_consignacion")      );

                lista.add(  anticipo );
            }


        }catch(Exception e){
            throw new Exception( "agrupar " + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;

    }






    /**
     * M�todo que busca los anticipos pagos terceros pendientes por aprobar
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List searchAnticipos( String distrito,        String proveedor,
    String ckAgencia,       String Agencia ,
    String ckPropietario,   String Propietario,
    String ckPlanilla,      String Planilla,
    String ckPlaca ,        String Placa,
    String ckConductor,     String Conductor
    ) throws Exception{


        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_ANTICIPOS_POR_APROBAR";
        List               lista   = new LinkedList();

        try{


            String filtro = "";
            if( ckAgencia     !=null )  filtro += " AND substring(a.agency_id,1,2) ='"+ Agencia     +"'";
            if( ckPropietario !=null )  filtro += " AND a.pla_owner                ='"+ Propietario +"'";
            if( ckConductor   !=null )  filtro += " AND a.cedcon                   ='"+ Conductor   +"'";
            if( ckPlaca       !=null )  filtro += " AND a.supplier                 ='"+ Placa       +"'";
            if( ckPlanilla    !=null )  filtro += " AND a.planilla                 ='"+ Planilla    +"'";


            con = this.conectarJNDI(query);
            String sql   =   this.obtenerSQL( query ).replaceAll("#COLUMNAS#", this.COLUMNAS).replaceAll("#FILTRO#",filtro);
            st= con.prepareStatement(sql);
            st.setString(1, distrito    );
            st.setString(2, proveedor   );
            //.out.println(st.toString());
            rs=st.executeQuery();
            while(rs.next()){
                AnticiposTerceros anticipo = load(rs);
                anticipo = llenarReferencido( anticipo );
                lista.add(  anticipo );
                //navi
                 anticipo.setFecha_creacion           (  reset(rs.getString("creation_date")!=null?rs.getString("creation_date"):""         ) );
                 anticipo.setDes_concept              (  reset(rs.getString("descripcion")!=null?rs.getString("descripcion"):""   ));
                 //navi
            }


        }catch(Exception e){
            throw new Exception( "searchAnticipos " + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;

    }

    /**
     * M�todo que busca los anticipos  pagos terceros pendientes por transferir
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....2.0. 
     * @modificacion egonzalez
     **/
    public  List searchAnticipos_transferir( String distrito,  String proveedor,String Banco,String Nocuenta, String propietario, Usuario user ) throws Exception{
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_ANTICIPOS_POR_TRANSFERIR";
        String             query1  = "SQL_PLANILLAS_CONSOLIDADO";
        List               lista   = new LinkedList();
        TreeMap            t       = new TreeMap();


        try{


            String restriccion_propietario = "";
            if (Nocuenta.equals("FINTRA") && propietario!=null && !propietario.trim().equals("") ){
                restriccion_propietario = " AND a.pla_owner = '"+ propietario +"' ";
            }
            
            
            //aqui vamos a validar los permisos de usuarios 
            String datos[];
            datos = obtenerPermisoTSP(user).split(",");
            String filtroUsuarios="";
            
            for (int i = 0; i < datos.length; i++) {

                String texto = datos[i].trim();

                //efectivo yopal
                if (texto.equals("EFECTIVOYO")) {
                    filtroUsuarios += "AND (a.agency_id like 'YO%' and a.user_autorizacion  = '" + user.getLogin() + "' or ";
                }
                //efectivo duitama
                if (texto.equals("EFECTIVODU")) {
                    filtroUsuarios += "AND (a.agency_id like 'DU%' and a.user_autorizacion  = '" + user.getLogin() + "' or ";
                }
                //pronto pagos y efectivo
                if (texto.equals("PRONTOPAGO") && !filtroUsuarios.equals("")) {
                    filtroUsuarios += "(a.user_autorizacion = '" + user.getLogin() + "' AND  a.concept_code = '50'))";
                }

                //pronto pago
                if (texto.equals("PRONTOPAGO") && filtroUsuarios.equals("")) {
                    filtroUsuarios += "AND a.concept_code = '50'";
                }

                //transferencia
                if (texto.equals("TRANSFERENCIA")) {
                    filtroUsuarios += "AND ( a.concept_code = '01' AND a.con_ant_tercero = '02') ";
                }

                if (texto.equals("APROBADOSUSER")) {
                    filtroUsuarios = "AND (a.user_autorizacion='" + user.getLogin() + "') ";
                }

                //todos los anteriores
                if (texto.equals("TODOS")) {
                    filtroUsuarios = "";
                }

                
                
                System.out.println(datos[i]);
            }
            
            restriccion_propietario+=filtroUsuarios;
          
            String sql   =   this.obtenerSQL( query ).replaceAll("#COLUMNAS#", this.COLUMNAS).replaceAll("#FILTRO-POR-PROPIETARIO#",  restriccion_propietario);
            StringStatement st1 = new StringStatement(sql, true);
          
            st1.setString(1, distrito    );
            st1.setString(2, proveedor   );
            st1.setString(3, Nocuenta    );
            
            String sqlConsulta = st1.getSql();
            
            con = this.conectarJNDI(query1);
            String sql2 = this.obtenerSQL(query1);
            st= con.prepareStatement(sql2);
            st.setString(1, sqlConsulta);
            st.setString(2, Banco);
           
            rs=st.executeQuery();

            while(rs.next()){
                AnticiposTerceros anticipo = load(rs);

                anticipo.setDes_concept(reset(rs.getString("descripcion")!=null?rs.getString("descripcion"):"" ));
                anticipo.setAsesor       (reset(rs.getString("asesor")!=null?rs.getString("asesor"):""));
                anticipo.setReferenciado (reset(rs.getString("referenciado")!=null?rs.getString("referenciado"):""));
                anticipo.setStatus       (reset(rs.getString("status")!=null?rs.getString("status"):""));
                anticipo.setObservacion  (reset(rs.getString("obs")!=null?rs.getString("obs"):""));

                //anticipo = llenarReferencido( anticipo );
                if( !anticipo.getTipo_cuenta().trim().equals(""))
                    anticipo.setTieneCuenta( true );
                lista.add(  anticipo );
                ////.out.println("uno mas ");
            }


        }catch(Exception e){
            throw new Exception( " searchAnticipos_transferir " + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;

    }

    /**
     * Metodo que busca los anticipos pagos terceros
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List consultarAnticipos( String distrito,        String proveedor,
    String ckAgencia,       String Agencia ,
    String ckPropietario,   String Propietario,
    String ckPlanilla,      String Planilla,
    String ckPlaca ,        String Placa,
    String ckConductor,     String Conductor,
    String ckReanticipo,    String reanticipo,
    String ckFechas,        String fechaIni,  String fechaFin,

    String ckLiquidacion,   String Liquidacion,
    String ckTransferencia, String Transferencia,
    String ckFactura,       String Factura

    ) throws Exception{

        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_CONSULTA_ANTICIPOS_2";
        List               lista   = new LinkedList();

        try{

            String PAGADA = "50";

            String filtro = "";
            if( ckAgencia        !=null )  filtro += " AND a.agency_id  ='"+ Agencia     +"'";
            if( ckPropietario    !=null )  filtro += " AND a.pla_owner  ='"+ Propietario +"'";
            if( ckConductor      !=null )  filtro += " AND a.cedcon     ='"+ Conductor   +"'";
            if( ckPlaca          !=null )  filtro += " AND a.supplier   ='"+ Placa       +"'";
            if( ckPlanilla       !=null )  filtro += " AND a.planilla   ='"+ Planilla    +"'";
            if( ckReanticipo     !=null )  filtro += " AND a.reanticipo ='"+ reanticipo  +"'";
            if( ckFechas         !=null )  filtro += " AND a.fecha_anticipo  BETWEEN     '"+  fechaIni  +" 00:00:00' and '"+  fechaFin +" 23:59:59'";
            if( ckLiquidacion    !=null )  filtro += " AND a.liquidacion   ='"+ Liquidacion    +"'";
            if( ckTransferencia  !=null )  filtro += " AND a.transferencia ='"+ Transferencia  +"'";
            if( ckFactura        !=null )  filtro += " AND a.factura_mims  ='"+ Factura        +"'";




            con = this.conectarJNDI(query);
            String sql   =   this.obtenerSQL( query ).replaceAll("#COLUMNAS#", this.COLUMNAS).replaceAll("#FILTRO#",filtro);
            st= con.prepareStatement(sql);
            st.setString(1, distrito    );
            st.setString(2, proveedor   );

            rs=st.executeQuery();
            while(rs.next()){
                AnticiposTerceros anticipo = load2(rs);
                String  id           = String.valueOf( anticipo.getId() );
                String  factura      = anticipo.getFactura_mims();
                String  propietario  = anticipo.getPla_owner();
                anticipo = llenarReferencido(anticipo );
                anticipo.setDes_concept(  reset(rs.getString("descripcion")!=null?rs.getString("descripcion"):""   ));
            
                lista.add(  anticipo );
                
            }

        }catch(Exception e){
            throw new Exception( "consultarAnticipos " + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }








    /**
     * M�todo que busca el id mims del nit
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String getMIMS( String nit ) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_MIMS_NIT";
        String             mims    = "";
        try{
            con = this.conectarJNDI( query );

            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, nit   );
            rs = st.executeQuery();
            if( rs.next() )
                mims = rs.getString(1);

        }catch(Exception e){
            throw new Exception( "getMIMS " + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return mims;
    }





    /**
     * Metodo que actualiza datos de la factura
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public void  actualizaFactura(String  anticipo,  String estado, String fecha, String corrida, String cheque, String tipo  )throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null;
        String            query       = "";

        try{

            query  =  (tipo.equals(TERCERO))? "SQL_UPDATE_INFO_FACTURA_TERCERO" :  "SQL_UPDATE_INFO_FACTURA_PROPIETARIO" ;
            con = this.conectarJNDI( query );

            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, estado     );
            st.setString(2, fecha      );
            st.setString(3, cheque     );
            st.setString(4, corrida    );
            st.setString(5, anticipo   );
            st.execute();

        }catch(Exception e) {
            throw new SQLException(" actualizaFactura  -> " +e.getMessage());
        } finally {
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }



    /**
     * M�todo que busca estado de la factura en mims:
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  Hashtable getInfoFacturaMIMS( String distrito, String proveedor, String factura ) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_INFO_FACTURA";
        Hashtable          dato    = null;
        try{
            con = this.conectarJNDI( query );

            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, distrito   );
            st.setString(2, proveedor  );
            st.setString(3, factura    );

            ////.out.println("SQL " + st.toString() );

            rs = st.executeQuery();
            if( rs.next() ){
                dato =  new Hashtable();
                dato.put("fecha_pago",  reset( rs.getString(1) ) );
                dato.put("estado",      reset( rs.getString(2) ) );
                dato.put("corrida",     reset( rs.getString(3) ) );
                dato.put("cheque",      reset( rs.getString(4) ) );

            }


        }catch(Exception e){
            throw new Exception( "getInfoFacturaMIMS " + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return dato;
    }

    /**
     * M�todo que carga datos de los anticipos pagos terceros pendientes por aprobar
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public    AnticiposTerceros  load(ResultSet  rs)throws Exception{
        AnticiposTerceros anticipo =  new  AnticiposTerceros();
        try{

            anticipo.setReg_status               (((rs.getString("reg_status")).equals(""))?"":"ANULADO");
            anticipo.setObs_anulacion            (          rs.getString   ("observacion_anulacion")    );
            anticipo.setId                       (          rs.getInt   ("id")                          );
            anticipo.setDstrct                   (   reset( rs.getString("dstrct" )                   ) );
            anticipo.setAgency_id                (   reset( rs.getString("agency_id" )                ) );
            anticipo.setPla_owner                (   reset( rs.getString("pla_owner" )                ) );
            anticipo.setPlanilla                 (   reset( rs.getString("planilla" )                 ) );
            anticipo.setSupplier                 (   reset( rs.getString("supplier" )                 ) );
            anticipo.setProveedor_anticipo       (   reset( rs.getString("proveedor_anticipo" )       ) );
            anticipo.setConcept_code             (   reset( rs.getString("concept_code" )             ) );
            anticipo.setVlr                      (          rs.getDouble("vlr" )                        );
            anticipo.setVlr_for                  (          rs.getDouble("vlr_for" )                    );
            anticipo.setCurrency                 (   reset( rs.getString("currency" )                 ) );
            anticipo.setFecha_anticipo           (   resetFechas( rs.getString("fecha_anticipo" )           ) );

            anticipo.setAprobado                 (   reset( rs.getString("aprobado" )                 ) );
            anticipo.setFecha_autorizacion       (   resetFechas( rs.getString("fecha_autorizacion" )       ) );
            anticipo.setUser_autorizacion        (   reset( rs.getString("user_autorizacion" )        ) );

            anticipo.setTransferido              (   reset( rs.getString("transferido" )              ) );
            anticipo.setFecha_transferencia      (   resetFechas( rs.getString("fecha_transferencia" )      ) );
            
            anticipo.setPeriodo_Contabilizacion  (   rs.getString("periodo_contabilizacion" ) );
            anticipo.setDocum_Contable           (   rs.getString("docum_contable" ) );
            
            anticipo.setBanco_transferencia      (   reset( rs.getString("banco_transferencia" )      ) );
            anticipo.setCuenta_transferencia     (   reset( rs.getString("cuenta_transferencia" )     ) );
            anticipo.setTcta_transferencia       (   reset( rs.getString("tcta_transferencia" )       ) );

            anticipo.setUser_transferencia       (   reset( rs.getString("user_transferencia" )       ) );

            anticipo.setBanco                    (   reset( rs.getString("banco" )                    ) );
            anticipo.setSucursal                 (   reset( rs.getString("sucursal" )                 ) );
            anticipo.setNombre_cuenta            (   reset( rs.getString("nombre_cuenta" )            ) );
            anticipo.setCuenta                   (   reset( rs.getString("cuenta" )                   ) );
            anticipo.setTipo_cuenta              (   reset( rs.getString("tipo_cuenta" )              ) );
            anticipo.setNit_cuenta               (   reset( rs.getString("nit_cuenta" )               ) );

            anticipo.setFecha_migracion          (   resetFechas( rs.getString("fecha_migracion" )          ) );
            anticipo.setUser_migracion           (   reset( rs.getString("user_migracion" )           ) );
            anticipo.setFactura_mims             (   reset( rs.getString("factura_mims" )             ) );

            anticipo.setEstado_pago_tercero      (   reset( rs.getString("estado_pago_tercero" )      ) );
            anticipo.setEstado_desc_propietario  (   reset( rs.getString("estado_desc_propietario" )  ) );
            anticipo.setFecha_pago_tercero       (   resetFechas( rs.getString("fecha_pago_tercero" )       ) );
            anticipo.setFecha_desc_propietario   (   resetFechas( rs.getString("fecha_desc_propietario" )   ) );
            anticipo.setCheque_pago_tercero      (   reset( rs.getString("cheque_pago_tercero" )      ) );
            anticipo.setCheque_desc_propietario  (   reset( rs.getString("cheque_desc_propietario" )  ) );
            anticipo.setCorrida_pago_tercero     (   reset( rs.getString("corrida_pago_tercero" )     ) );
            anticipo.setCorrida_desc_propietario(   reset( rs.getString("corrida_desc_propietario" ) ) );

            anticipo.setVlr_mims_tercero         (          rs.getDouble("vlr_mims_tercero" )           );
            anticipo.setVlr_mims_propietario     (          rs.getDouble("vlr_mims_propietario" )       );

            anticipo.setNombreAgencia            (   reset( rs.getString("nombre_agencia" )           ) );
            anticipo.setNombreProveedor          (   reset( rs.getString("nombre_proveedor" )         ) );
            anticipo.setNombrePropietario        (   reset( rs.getString("nombre_prpietario" )        ) );

            anticipo.setPorcentaje               (          rs.getDouble("porcentaje" )                 );
            anticipo.setVlrDescuento             (          rs.getDouble("vlr_descuento")               );
            anticipo.setVlrNeto                  (          rs.getDouble("vlr_neto")                    );
            anticipo.setVlrComision              (          rs.getDouble("vlr_combancaria")             );
            anticipo.setVlrConsignar             (          rs.getDouble("vlr_consignacion")            );

            anticipo.setReanticipo               (  reset( rs.getString("reanticipo" )                ) );

            anticipo.setConductor                (  reset( rs.getString("cedcon" )                    ) );
            anticipo.setNombreConductor          (  getNameNit(  anticipo.getConductor()              ) );


            anticipo.setTransferencia            (  reset( rs.getString("transferencia" )             ) );
            anticipo.setLiquidacion              (  reset( rs.getString("liquidacion" )               ) );
            anticipo.setSecuencia                (         rs.getInt   ("secuencia")                    );
            anticipo.setUsuario_creacion         (  reset( rs.getString("creation_user" )               ) );
            anticipo.setDiferencia               ( reset( rs.getString("diferencia" )               ) );
            anticipo.setRango_diferencia         (reset( rs.getString("rango" )               ) );


        }catch(Exception e){
            throw new Exception( " load "+ e.getMessage());
        }
        return anticipo;
    }
    
      /**
     * M�todo que carga datos de los anticipos pagos terceros pendientes por aprobar
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public    AnticiposTerceros  load2(ResultSet  rs)throws Exception{
        AnticiposTerceros anticipo =  new  AnticiposTerceros();
        try{

            anticipo.setReg_status               (((rs.getString("reg_status")).equals(""))?"":"ANULADO");
            anticipo.setObs_anulacion            (          rs.getString   ("observacion_anulacion")    );
            anticipo.setId                       (          rs.getInt   ("id")                          );
            anticipo.setDstrct                   (   reset( rs.getString("dstrct" )                   ) );
            anticipo.setAgency_id                (   reset( rs.getString("agency_id" )                ) );
            anticipo.setPla_owner                (   reset( rs.getString("pla_owner" )                ) );
            anticipo.setPlanilla                 (   reset( rs.getString("planilla" )                 ) );
            anticipo.setSupplier                 (   reset( rs.getString("supplier" )                 ) );
            anticipo.setProveedor_anticipo       (   reset( rs.getString("proveedor_anticipo" )       ) );
            anticipo.setConcept_code             (   reset( rs.getString("concept_code" )             ) );
            anticipo.setVlr                      (          rs.getDouble("vlr" )                        );
            anticipo.setVlr_for                  (          rs.getDouble("vlr_for" )                    );
            anticipo.setCurrency                 (   reset( rs.getString("currency" )                 ) );
            anticipo.setFecha_anticipo           (   resetFechas( rs.getString("fecha_anticipo" )           ) );

            anticipo.setAprobado                 (   reset( rs.getString("aprobado" )                 ) );
            anticipo.setFecha_autorizacion       (   resetFechas( rs.getString("fecha_autorizacion" )       ) );
            anticipo.setUser_autorizacion        (   reset( rs.getString("user_autorizacion" )        ) );

            anticipo.setTransferido              (   reset( rs.getString("transferido" )              ) );
            anticipo.setFecha_transferencia      (   resetFechas( rs.getString("fecha_transferencia" )      ) );
            
            anticipo.setPeriodo_Contabilizacion  (   rs.getString("periodo_contabilizacion" ) );
            anticipo.setDocum_Contable           (   rs.getString("docum_contable" ) );
            
            anticipo.setBanco_transferencia      (   reset( rs.getString("banco_transferencia" )      ) );
            anticipo.setCuenta_transferencia     (   reset( rs.getString("cuenta_transferencia" )     ) );
            anticipo.setTcta_transferencia       (   reset( rs.getString("tcta_transferencia" )       ) );

            anticipo.setUser_transferencia       (   reset( rs.getString("user_transferencia" )       ) );

            anticipo.setBanco                    (   reset( rs.getString("banco" )                    ) );
            anticipo.setSucursal                 (   reset( rs.getString("sucursal" )                 ) );
            anticipo.setNombre_cuenta            (   reset( rs.getString("nombre_cuenta" )            ) );
            anticipo.setCuenta                   (   reset( rs.getString("cuenta" )                   ) );
            anticipo.setTipo_cuenta              (   reset( rs.getString("tipo_cuenta" )              ) );
            anticipo.setNit_cuenta               (   reset( rs.getString("nit_cuenta" )               ) );

            anticipo.setFecha_migracion          (   resetFechas( rs.getString("fecha_migracion" )          ) );
            anticipo.setUser_migracion           (   reset( rs.getString("user_migracion" )           ) );
            anticipo.setFactura_mims             (   reset( rs.getString("factura_mims" )             ) );

            anticipo.setEstado_pago_tercero      (   reset( rs.getString("estado_pago_tercero" )      ) );
            anticipo.setEstado_desc_propietario  (   reset( rs.getString("estado_desc_propietario" )  ) );
            anticipo.setFecha_pago_tercero       (   resetFechas( rs.getString("fecha_pago_tercero" )       ) );
            anticipo.setFecha_desc_propietario   (   resetFechas( rs.getString("fecha_desc_propietario" )   ) );
            anticipo.setCheque_pago_tercero      (   reset( rs.getString("cheque_pago_tercero" )      ) );
            anticipo.setCheque_desc_propietario  (   reset( rs.getString("cheque_desc_propietario" )  ) );
            anticipo.setCorrida_pago_tercero     (   reset( rs.getString("corrida_pago_tercero" )     ) );
            anticipo.setCorrida_desc_propietario(   reset( rs.getString("corrida_desc_propietario" ) ) );

            anticipo.setVlr_mims_tercero         (          rs.getDouble("vlr_mims_tercero" )           );
            anticipo.setVlr_mims_propietario     (          rs.getDouble("vlr_mims_propietario" )       );

            anticipo.setNombreAgencia            (   reset( rs.getString("nombre_agencia" )           ) );
            anticipo.setNombreProveedor          (   reset( rs.getString("nombre_proveedor" )         ) );
            anticipo.setNombrePropietario        (   reset( rs.getString("nombre_prpietario" )        ) );

            anticipo.setPorcentaje               (          rs.getDouble("porcentaje" )                 );
            anticipo.setVlrDescuento             (          rs.getDouble("vlr_descuento")               );
            anticipo.setVlrNeto                  (          rs.getDouble("vlr_neto")                    );
            anticipo.setVlrComision              (          rs.getDouble("vlr_combancaria")             );
            anticipo.setVlrConsignar             (          rs.getDouble("vlr_consignacion")            );

            anticipo.setReanticipo               (  reset( rs.getString("reanticipo" )                ) );

            anticipo.setConductor                (  reset( rs.getString("cedcon" )                    ) );
            anticipo.setNombreConductor          (  getNameNit(  anticipo.getConductor()              ) );


            anticipo.setTransferencia            (  reset( rs.getString("transferencia" )             ) );
            anticipo.setLiquidacion              (  reset( rs.getString("liquidacion" )               ) );
            anticipo.setSecuencia                (         rs.getInt   ("secuencia")                    );
            anticipo.setUsuario_creacion         (  reset( rs.getString("creation_user" )               ) );
            anticipo.setDiferencia               ( reset( rs.getString("diferencia" )               ) );
            anticipo.setRango_diferencia         (reset( rs.getString("rango" )               ) );
            anticipo.setFecha_trans_gasolina(reset(rs.getString("fecha_trans_gasolina")));
            anticipo.setUser_trans_gasolina(reset(rs.getString("user_trans_gasolina")));
            anticipo.setNombre_eds(reset(rs.getString("nombre_eds")));

        }catch(Exception e){
            throw new Exception( " load2 "+ e.getMessage());
        }
        return anticipo;
    }

    /**
     * Metodo que devuelve informaci�n del banco en tablagen dependiendo el tipo
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public Hashtable  getInfoBanco(String tipo, String codigo)throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        Hashtable         tabla       = null;
        String            query       = "SQL_BANCO_TGENERAL";
        try{
            con = this.conectarJNDI( query );

            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, tipo  );
            st.setString(2, codigo  );
            rs = st.executeQuery();
            if(rs.next()){
                tabla  =  new  Hashtable();
                tabla.put("codigo",           reset( rs.getString("table_code")  ) );
                tabla.put("descripcion",      reset( rs.getString("descripcion") ) );
                tabla.put("referencia",       reset( rs.getString("referencia")  ) );
            }

        }catch(Exception e) {
            throw new SQLException(" getInfoTablaGen  -> " +e.getMessage());
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return tabla;

    }

    /**
     * Metodo que devuelve el banco asociado al nit
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public Hashtable  getBancoNIT(String nit)throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        Hashtable         tabla       = null;
        String            query       = "SQL_DATOS_BANCO_NIT";
        try{
            con = this.conectarJNDI( query );

            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, nit  );
            rs = st.executeQuery();
            if(rs.next()){
                tabla  =  new  Hashtable();
                tabla.put("banco",         reset( rs.getString("banco")    ));
                tabla.put("sucursal",      reset( rs.getString("sucursal") ));
                tabla.put("mims",          reset( rs.getString("mims")     ));
            }

        }catch(Exception e) {
            throw new SQLException(" getBancoNIT  -> " +e.getMessage());
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return tabla;
    }

    /**
     * M�todo que aprueba los anticipos
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String  aprobar(List lista, String user) throws Exception{
        Connection con = null;
        StringStatement  st      = null;
        String           query   = "SQL_UPDATE_ANTICIPOS_APROBAR";
        String           sql     = "";
        String consulta = this.obtenerSQL(query);
        try{


            if(lista!=null &&  lista.size()>0){
                for(int i=0;i<lista.size();i++){
                    st = new StringStatement(consulta, true);//JJCastro fase2
                    AnticiposTerceros anticipo = (AnticiposTerceros)lista.get(i);
                    st.setString( 1,user);
                    st.setInt   ( 2,anticipo.getId() );
                    sql += st.getSql();
                st    = null;
                }
            }

            }catch(Exception e){
            throw new Exception( "aprobar " + e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sql;
    }


    /**
     * M�todo que aprueba los anticipos
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String  transferir(List lista, String user) throws Exception{
        Connection con = null;
        StringStatement  st      = null;
        String           query   = "SQL_UPDATE_ANTICIPOS_TRANSFERIDOS";
        String           sql     = "";
        String consulta     = this.obtenerSQL(query);
        try{
            
             DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
             Calendar cal = Calendar.getInstance();
             String fecha=dateFormat.format(cal.getTime());

            if(lista!=null &&  lista.size()>0){
                for(int i=0;i<lista.size();i++){
                st = new StringStatement(consulta, true);//JJCastro fase2
                    AnticiposTerceros anticipo = (AnticiposTerceros)lista.get(i);

                    st.setString(1, fecha);
                    st.setString(2, anticipo.getBanco_transferencia());
                    st.setString(3, anticipo.getCuenta_transferencia());
                    st.setString(4, anticipo.getTcta_transferencia());
                    st.setString(5, user);
                    st.setString(6, fecha);
                    st.setString(7, user);
                    st.setString(8, anticipo.getFactura_mims());
                    st.setString(9, anticipo.getTransferencia());
                    //where
                    st.setInt(10, anticipo.getId());

                    sql += st.getSql();
                    st = null;
                }
            }
            actualizarSerie( );

            }catch(Exception e){
            throw new Exception( "transferir " + e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sql;
    }


    /**
     * M�todo que aprueba los anticipos
     * @autor.......Jbarros
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String  cuenta_a_transferir(List lista) throws Exception{
        Connection con = null;
        StringStatement  st      = null;
        String           query   = "SQL_UPDATE_ANTICIPOS_A_TRANSFERIDOS";
        String           sql     = "";
        String consulta = this.obtenerSQL(query);
        try{

            if(lista!=null &&  lista.size()>0)
                for(int i=0;i<lista.size();i++){
                     st = new StringStatement(consulta, true);//JJCastro fase2
                    AnticiposTerceros anticipo = (AnticiposTerceros)lista.get(i);

                    st.setString(1, anticipo.getBanco_transferencia()  );
                    st.setString(2, anticipo.getCuenta_transferencia() );
                    st.setString(3, anticipo.getTcta_transferencia()   );

                    //where
                    st.setInt   (4, anticipo.getId()                   );

                    sql += st.getSql();
                    st = null;
                }

            actualizarSerie( );

        }catch(Exception e){
            throw new Exception( "transferir " + e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sql;
    }




    /**
     * M�todo que busca el nit del propietario asociado al usuario
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String getTerceroUser(String login) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_TERCERO_USER";
        String             nit     = "";
        try{
            con = this.conectarJNDI( query );
            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, login);
            rs=st.executeQuery();
            if( rs.next() ){
                nit = reset(rs.getString(1));
            }

        }catch(Exception e){
            throw new Exception( " getTerceroUser " + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return nit;
    }
    /**
     * M�todo que carga las agencias
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List searchAgencias() throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_AGENCIAS";
        List               lista   = new LinkedList();
        try{
            con = this.conectarJNDI( query );

            st = con.prepareStatement( this.obtenerSQL(query) );
            rs=st.executeQuery();
            while(rs.next()){
                Hashtable  agencia =  new Hashtable();
                agencia.put("codigo",  reset(rs.getString("id_agencia") ) );
                agencia.put("nombre",  reset(rs.getString("nombre")     ) );
                lista.add( agencia );
                agencia = null;//Liberar Espacio JJCastro
            }

        }catch(Exception e){
            throw new Exception( "searchAgencias " + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }

    /**
     * Metodo que devuelve el banco asociado al nit
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
   /* public List getMovpla(String planilla,  ImpresionCheque cheque)throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_BUSCAR_PLANILLA";
        List              lista       = new LinkedList();


        try{
            con = this.conectarJNDI( query );

            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, planilla                   );
            st.setString(2, cheque.getCheque()         );
            st.setString(3, cheque.getDistrito()       );
            st.setString(4, cheque.getBanco()          );
            st.setString(5, cheque.getAgenciaBanco()   );
            rs = st.executeQuery();
            while( rs.next() ){

                Hashtable  mov  =  new  Hashtable();
                mov.put("dstrct",             reset(  rs.getString("dstrct")      ) );
                mov.put("agency_id",          reset(  rs.getString("agency_id")   ) );
                mov.put("pla_owner",          reset(  rs.getString("pla_owner")   ) );
                mov.put("planilla",           reset(  rs.getString("planilla")    ) );
                mov.put("supplier",           reset(  rs.getString("supplier")    ) );
                mov.put("proveedor_anticipo", reset(  rs.getString("proveedor_anticipo")  ) );
                mov.put("concept_code",       reset(  rs.getString("concept_code")        ) );
                mov.put("currency",           reset(  rs.getString("currency")    ) );
                mov.put("date_doc",           reset(  rs.getString("date_doc")    ) );
                mov.put("base",               reset(  rs.getString("base")        ) );
                //mov.put("reanticipo",         reset(  rs.getString("reanticipo")  ) );
                mov.put("reanticipo",         "N"                                   );
                mov.put("vlr",                reset(  rs.getString("vlr")         ) );
                mov.put("vlr_for",            reset(  rs.getString("vlr_for")     ) );
                mov.put("cedcon",             reset(  rs.getString("cedcon")      ) );
                mov.put("liquidacion",        reset(  rs.getString("liquidacion") ) );

                //Hcastillo 2008-08-29//
                mov.put("cuent",       reset(  rs.getString("cuent") ) );
                mov.put("tip_cuenta" ,          reset(  rs.getString("tip_cuenta")));
                mov.put("nit_cuenta",         reset(  rs.getString("nit_cuenta")));
                mov.put("nombre_cuenta",      reset(  rs.getString("nombre_cuenta")));
                mov.put("banco",              reset(  rs.getString("banco") ));
                 mov.put("con_ant_tercero",        reset(  rs.getString("con_ant_tercero") ) );
                //Hcastillo//
                lista.add( mov );
            }

        }catch(Exception e) {
            throw new SQLException(" getMovpla  -> " +e.getMessage());
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;

    }*/

    /**
     * Metodo que devuelve el banco asociado al nit
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public List getMovpla(String planilla,  ImpresionCheque cheque)throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_BUSCAR_PLANILLA";
        List              lista       = new LinkedList();
        String            antu        =  "";
        PreparedStatement st2         = null;
        String            query2      ="SQL_BUSQ_CUENT";
        ResultSet         rs2          = null;
        try{
            con = this.conectarJNDI( query );

            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, planilla                   );
            st.setString(2, cheque.getCheque()         );
            st.setString(3, cheque.getDistrito()       );
            st.setString(4, cheque.getBanco()          );
            st.setString(5, cheque.getAgenciaBanco()   );
            rs = st.executeQuery();
            while( rs.next() ){

                Hashtable  mov  =  new  Hashtable();
                mov.put("dstrct",             reset(  rs.getString("dstrct")      ) );
                mov.put("agency_id",          reset(  rs.getString("agency_id")   ) );
                mov.put("pla_owner",          reset(  rs.getString("pla_owner")   ) );
                mov.put("planilla",           reset(  rs.getString("planilla")    ) );
                mov.put("supplier",           reset(  rs.getString("supplier")    ) );
                mov.put("proveedor_anticipo", reset(  rs.getString("proveedor_anticipo")  ) );
                mov.put("concept_code",       reset(  rs.getString("concept_code")        ) );
                mov.put("currency",           reset(  rs.getString("currency")    ) );
                mov.put("date_doc",           reset(  rs.getString("date_doc")    ) );
                mov.put("base",               reset(  rs.getString("base")        ) );
                //mov.put("reanticipo",         reset(  rs.getString("reanticipo")  ) );
                mov.put("reanticipo",         "N"                                   );
                mov.put("vlr",                reset(  rs.getString("vlr")         ) );
                mov.put("vlr_for",            reset(  rs.getString("vlr_for")     ) );
                mov.put("cedcon",             reset(  rs.getString("cedcon")      ) );
                mov.put("liquidacion",        reset(  rs.getString("liquidacion") ) );
                //Hcastillo 2008-08-29//
                mov.put("cuent",              reset(  rs.getString("cuent") ) );
                mov.put("con_ant_tercero",    reset(  rs.getString("con_ant_tercero") ) );

                antu=rs.getString("con_ant_tercero")!=null?rs.getString("con_ant_tercero"):"";
                if (antu.equals("02")){
                    st2=null;
                    rs2=null;
                    st2 = con.prepareStatement( this.obtenerSQL(query2) );
                    st2.setString(1,rs.getString("cedcon"));
                    st2.setString(2,rs.getString("cuent"));
                    rs2 = st2.executeQuery();
                    if (rs2.next()){
                        mov.put("tip_cuenta" ,        reset(  rs2.getString("tipo_cuenta")));
                        mov.put("nit_cuenta",         reset(  rs2.getString("cedula_cuenta")));
                        mov.put("nombre_cuenta",      reset(  rs2.getString("nombre_cuenta")));
                        mov.put("banco",              reset(  rs2.getString("banco") ));
                    }
                }else{
                    mov.put("tip_cuenta" ,        "");
                    mov.put("nit_cuenta",         "");
                    mov.put("nombre_cuenta",      "");
                    mov.put("banco",              "");


                }

                    //Hcastillo//
                lista.add( mov );
            }

        }catch(Exception e) {
            throw new SQLException(" getMovpla  -> " +e.getMessage());
        } finally {
            if (rs  != null)  rs.close();
            if (st  != null)  st.close();
            if (rs2  != null)  rs2.close();
            if (st2  != null) st2.close();
            this.desconectar(con);

        }
        return lista;

    }


    /**
     * M�todo que carga las planillas y las fechas  a reversar de una secuencia
     * @autor.......jbarros
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List Listar_OC_Fec(String sec) throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_LISTAR_OC_FEC";
        List              lista       = new LinkedList();
        boolean           bool        = false;
        try{
            con = this.conectarJNDI( query );

            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, sec        );
            rs = st.executeQuery();
            while(rs.next()){
                Hashtable  Rep  =  new  Hashtable();
                lista.add(rs.getString("documento"));
            }
        }catch(Exception e) {
            throw new SQLException(" search_Listar_OC_Fec  -> " +e.getMessage());
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }


    /**
     *Metodo que busca el anticipo aplicado a una planolla
     *@autor: Julio Barros
     *@param: Numero de la planilla, secuencia
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public Movpla searchMovPla(String numpla, String sec )throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Movpla movpla = null;
        String query = "SQL_SEARCH_MOVPLA_RT";
        try {
            con = this.conectarJNDI( query );

            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1,numpla);
            st.setString(2,sec);
            st.setString(3,sec);
            rs = st.executeQuery();
            if(rs.next()){
                movpla=Movpla.load(rs);
                movpla.setVlr_disc                  (-1*rs.getFloat   ("vlr_disc"));
                movpla.setVlr_for                   (-1*rs.getFloat   ("vlr_for"));
                movpla.setVlr                       (-1*rs.getFloat   ("vlr"));
                movpla.setCreation_date             ("now()");
                movpla.setReanticipo                ("Y");
                movpla.setBeneficiario              (rs.getString     ("beneficiario"));
                movpla.setBranch_code               (rs.getString     ("branch_code"));
                movpla.setBank_account_no           (rs.getString     ("bank_account_no"));
                movpla.setTercero                   (rs.getString     ("tercero"));
            }
        }catch(Exception e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA PLANILLA " + e.getMessage() );
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return movpla;
    }

    // Julio Barros 30-12-2006
    public String Eliminar_Extracto_ExtractoDetalle(String sec)throws SQLException{
        StringStatement st = null;
        String query = "SQL_ELIMINAR_EXTRACTO_EXTRACTODETALLE";
        String sql ="";
        try {
            st = new StringStatement(this.obtenerSQL(query));
            st.setString(sec);
            st.setString(sec);
            st.setString(sec);
            sql = st.getSql();

        }catch(Exception e){
            throw new SQLException("ERROR DURANTE SQL_ELIMINAR_EXTRACTO_EXTRACTODETALLE " + e.getMessage() );
        }
        finally{
            if (st  != null) st = null;
        }
        return sql;
    }

    /**
     * M�todo que carga los numeros de id de los propietarios y la secuencia
     * @autor.......jbarros
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List Listar_NitPro_Secuencia(String ids) throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_BUSCAR_PROPIETERIOS_SECUENCIA";
        List              lista       = new LinkedList();
        try{
            con = this.conectarJNDI( query );

            String sql   =   this.obtenerSQL( query ).replaceAll("#COLUMNAS#", ids);

            st = con.prepareStatement( sql );
            rs = st.executeQuery();
            while(rs.next()){
                Hashtable  Rep  =  new  Hashtable();
                Rep.put("Documento",         reset(  rs.getString("pla_owner")  ) );
                Rep.put("Secuencia",         reset(  rs.getString("secuencia")           ) );
                lista.add( Rep );
            }
        }catch(Exception e) {
            throw new SQLException(" SQL_BUSCAR_PROPIETERIOS_SECUENCIA  -> " +e.getMessage());
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }


    /*0 7 -  0 2  -  2 0 0 7*/

    /**
     * M�todo que inserta del movpla  registros a la tabla de anticipos para terceros
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... ImpresionCheque cheque, String usuario
     * @version..... 1.0.
     **/
    public synchronized String  InsertAnticipo(ImpresionCheque cheque,String agencia)throws Exception {

        StringStatement  st      = null;
        String          query   = "SQL_INSERT_ANTICIPO";
        String          sql     = "";
        String          consulta  = "";

        try{
            consulta = this.obtenerSQL(query);

            String[] vec = cheque.getPlanilla().split( SEPARADOR );
            for(int i=0;i<vec.length;i++){

                                    String oc = vec[i];
                                    List  planillas =  this.getMovpla(oc, cheque);

                                    for( int j=0; j<planillas.size();j++ ){

                                                        st = new StringStatement (consulta, true);//JJCastro fase2

                                                        Hashtable  mov  = (Hashtable) planillas.get(j);

                                                        String  nit            =  (String)mov.get("pla_owner");
                                                        double  valor          =  Double.parseDouble( (String)mov.get("vlr") );
                                                        String  reanticipo     =  (String)mov.get("reanticipo");
                                                        reanticipo             =  reanticipo.toUpperCase().trim().equals("N")?"N":"S";
                                                        String  beneficiario   = ( reanticipo.equals("S") )?nit :  (String)mov.get("cedcon") ;
                                                        double porcentaje      = this.descuento(nit);

                                                        if( reanticipo.equals("N") ){  // si es anticipo, buscamos el valor del anticipo por default.
                                                            Hashtable  hh = getInfoBanco(TABLA_VALORES, TABLA_ANTICIPO);
                                                            if(hh!=null)
                                                                porcentaje = Double.parseDouble( (String)hh.get("referencia") );
                                                        }


                                                        double descuento       = ( valor * porcentaje )/100;
                                                        double neto            =  valor - descuento ;
                                                        double comision        =  0;
                                                        double consignar       = (int)Math.round( neto - comision );
                                                        String liq = ( reanticipo.equals("N") )? "" : (String)mov.get("liquidacion");
                                                        Usuario usuario        = null;


                                                        st.setString(1, (String)mov.get("dstrct")             );
                                                        st.setString(2,  agencia  );//agencia de Despacho
                                                        st.setString( 3, nit                                  );
                                                        st.setString( 4,(String)mov.get("planilla")           );
                                                        st.setString( 5,(String)mov.get("supplier")           );
                                                        st.setString( 6,(String)mov.get("proveedor_anticipo") );
                                                        st.setString( 7,(String)mov.get("concept_code")       );
                                                        st.setString( 8,(String)mov.get("vlr")                );
                                                        st.setString( 9,(String)mov.get("vlr_for")            );
                                                        st.setString( 10,(String)mov.get("currency")           );
                                                        st.setString( 11,(String)mov.get("date_doc")           );
                                                        st.setString( 12,cheque.getUsuario()                  );
                                                        st.setString( 13,(String)mov.get("base")               );
                                                        st.setString( 14,reanticipo                           );

                                                        st.setDouble( 15,porcentaje   );
                                                        st.setDouble( 16,descuento    );
                                                        st.setDouble( 17,neto         );
                                                        st.setDouble( 18,comision     );
                                                        st.setDouble( 19,consignar    );
                                                        st.setString( 20,beneficiario );   // Beneficiario
                                                        st.setString( 21,liq          );
                                                        //Hcastillo 2008-08-29//
                                                        st.setString( 22,(String)mov.get("cuent")           );
                                                        st.setString( 23,(String)mov.get("tip_cuenta")           );
                                                        st.setString( 24,(String)mov.get("nit_cuenta")                  );
                                                        st.setString( 25,(String)mov.get("nombre_cuenta")               );
                                                        st.setString( 26,(String)mov.get("banco")                          );
                                                        st.setString(27,(String)mov.get("con_ant_tercero"));
                                                        //hcastillo//

                                                        sql += st.getSql();//JJCastro fase2
                                                        if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                                    }
            }
        }catch(Exception e){
            throw new SQLException(" InsertAnticipo"+ e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return sql;
    }



    /**
     * M�todo que inserta del movpla  registros a la tabla de anticipos para terceros
     * @autor....... mfontalvo
     * @throws...... Exception
     * @parameter... Movpla mov
     * @version..... 1.0.
     **/
    public synchronized String  InsertAnticipo(Movpla mov, Proveedor beneficiarioPP,int secuencia,String BanTR)throws Exception {

        StringStatement st      = null;
        String          query   = "SQL_INSERT_ANTICIPO_PP";
        String          sql     = "";

        try{
            //.out.println("BANCO : " + BanTR );
            String[] vec        =  BanTR.split("-");
            String   banco      =  vec[0];
            String   cta        =  vec[1];
            String   tipoCta    =  vec[2];

            String reanticipo = (mov.getReanticipo().equals("N")?"N":"S");
            double porcentaje      = this.descuento(mov.getPla_owner());
            if( reanticipo.equals("N") ){  // si es anticipo, buscamos el valor del anticipo por default.
                Hashtable  hh = getInfoBanco(TABLA_VALORES, TABLA_ANTICIPO);
                if(hh!=null)
                    porcentaje = Double.parseDouble( (String)hh.get("referencia") );
            }
            double descuento       = ( mov.getVlr() * porcentaje )/100;
            double neto            =  mov.getVlr() - descuento ;
            double comision        = ( beneficiarioPP.getC_tipo_cuenta().equals("EF")? 6000 : 2000 );
            double consignar       = (int) Math.round( neto - comision );

            st = new StringStatement(this.obtenerSQL(query));

            st.setString(  mov.getDstrct()           );
            st.setString(  get_AgencyID(mov.getPlanilla()));//agencia de Cumplido
            st.setString(  mov.getPla_owner()        );
            st.setString(  mov.getPlanilla()         );
            st.setString(  mov.getSupplier()         );
            st.setString(  mov.getProveedor()        );
            st.setString(  mov.getConcept_code()     );
            st.setDouble (  mov.getVlr()              );
            st.setDouble (  mov.getVlr_for()          );
            st.setString(  mov.getCurrency()         );
            st.setString(  "now()"                   );
            st.setString(  mov.getCreation_user()    );
            st.setString(  mov.getBase()             );
            st.setString(  reanticipo                );

            st.setDouble(  porcentaje   );
            st.setDouble(  descuento    );
            st.setDouble(  neto         );
            st.setDouble(  comision     );
            st.setDouble(  consignar    );
            st.setString(  mov.getBeneficiario()  );
            st.setString(  ""           );
            st.setString(  "S" );
            st.setString(  mov.getCreation_user() );
            st.setString(  "now()" );
            st.setString(  beneficiarioPP.getC_nit()           );
            st.setString(  beneficiarioPP.getC_payment_name()  );
            st.setString(  beneficiarioPP.getC_tipo_cuenta()   );
            st.setString(  beneficiarioPP.getC_branch_code()   );
            st.setString(  beneficiarioPP.getC_bank_account()  );
            st.setString(  beneficiarioPP.getC_numero_cuenta() );
            st.setInt   (  secuencia );

            st.setString( banco);
            st.setString( cta);
            st.setString( tipoCta);

            sql += st.getSql();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException(" InsertAnticipo "+ e.getMessage());
        }
        finally{
            if(st!=null) st = null;
        }
        return sql;
    }


    /**
     * M�todo busca la agencia de cumplido o de despacho de una planilla
     * @autor.......jbarros
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String  get_AgencyID(String planilla) throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_BUSCAR_AGENCIA_ID";
        String            AgcID       = "";

        try{
            con = this.conectarJNDI( query );

            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1,planilla);
            rs = st.executeQuery();
            if (rs.next()){
                AgcID = rs.getString     ("id_agencia");
            }
        }catch(Exception e) {
            throw new SQLException(" SQL_BUSCAR_AGENCIA_ID  -> " +e.getMessage());
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return AgcID;
    }


    /**
     * Metodo que busca el descuento para el nit
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public double descuento(String nit)throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_DESCUENTO_NIT";
        double            valor       = 0;
        try{
            con = this.conectarJNDI( query );

            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, nit       );
            rs = st.executeQuery();
            if ( rs.next() )
                valor = rs.getDouble(1);

        }catch(Exception e) {
            throw new SQLException(" descuento  -> " +e.getMessage());
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return valor;
    }

    /**
     * M�todo que la cuenta de banco girador, el nombre del banco del cliente y el tipo de cuenta
     * @autor.......jbarros
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List Bancos_AnticipoPT(String distr, String nit, String sec) throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_BUSCAR_APT_SEC";
        List              lista       = new LinkedList();
        boolean           bool        = false;
        try{
            con = this.conectarJNDI( query );

            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, distr      );
            st.setString(2, nit        );
            st.setString(3, sec        );
            rs = st.executeQuery();
            while(rs.next()){
                Hashtable  Rep  =  new  Hashtable();
                Rep.put("secuencia",        reset(  rs.getString("secuencia")         ) );
                Rep.put("Cuenta"   ,        reset(  rs.getString("cuenta_transferencia")         ) );
                Rep.put("banco"    ,        reset(  rs.getString("banco")          ) );
                Rep.put("tcta"     ,        reset(  rs.getString("tipo_cuenta")         ) );
                lista.add( Rep );
            }
        }catch(Exception e) {
            throw new SQLException(" SQL_BUSCAR_APT_SEC  -> " +e.getMessage());
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }







    /**
     * M�todo que extrae los propietarios que tiene prestamos pendientes pos liquidar
     * @autor.......mfontalvo
     * @throws......Exception
     * @version.....1.0.
     **/
    public TreeMap getPropietariosConPrestamos() throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_BUSCAR_PROPIETARIOS_CON_PRESTAMOS";
        TreeMap           lista       = new TreeMap();

        try{
            lista.put( "Seleccione un propietario", "" );

            con = this.conectarJNDI( query );
            st = con.prepareStatement( this.obtenerSQL(query) );
            rs = st.executeQuery();
            while(rs.next()){
                lista.put( reset(rs.getString("name")) , reset(rs.getString("code")));
            }
        }catch(Exception e) {
            e.printStackTrace();
            throw new SQLException(" getPropietariosConPrestamos  -> " +e.getMessage());
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }



    /**
     * M�todo que extrae los propietarios que tiene prestamos pendientes pos liquidar
     * @autor.......mfontalvo
     * @throws......Exception
     * @version.....1.0.
     **/
    public Vector getPlanillasAbonosPrestamos( String beneficiario ) throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_PLANILLAS_PENDIENTES";
        Vector            lista       = new Vector();

        try{

            con = this.conectarJNDI( query );
            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, beneficiario );
            rs = st.executeQuery();
            while(rs.next()){
                AnticiposTerceros anticipo = new AnticiposTerceros();
                anticipo.setDstrct               ( reset( rs.getString("dstrct"                ) ) );
                anticipo.setId                   ( rs.getInt   ("id") );
                anticipo.setPla_owner            ( reset( rs.getString("pla_owner"             ) ) );
                anticipo.setPlanilla             ( reset( rs.getString("planilla"              ) ) );
                anticipo.setSupplier             ( reset( rs.getString("supplier"              ) ) );
                anticipo.setProveedor_anticipo   ( reset( rs.getString("proveedor_anticipo"    ) ) );
                anticipo.setConcept_code         ( reset( rs.getString("concept_code"          ) ) );
                anticipo.setVlrNeto              ( rs.getDouble("vlr_neto")  );
                anticipo.setVlrComision          ( rs.getDouble("vlr_combancaria")  );
                anticipo.setConductor            ( reset( rs.getString("cedcon"                ) ) );
                anticipo.setNit_cuenta           ( reset( rs.getString("nit_cuenta"            ) ) );
                anticipo.setNombre_cuenta        ( reset( rs.getString("nombre_cuenta"         ) ) );
                anticipo.setTipo_cuenta          ( reset( rs.getString("tipo_cuenta"           ) ) );
                anticipo.setBanco                ( reset( rs.getString("banco"                 ) ) );
                anticipo.setSucursal             ( reset( rs.getString("sucursal"              ) ) );
                anticipo.setCuenta               ( reset( rs.getString("cuenta"                ) ) );
                anticipo.setBanco_transferencia  ( reset( rs.getString("banco_transferencia"   ) ) );
                anticipo.setCuenta_transferencia ( reset( rs.getString("cuenta_transferencia"  ) ) );
                anticipo.setTcta_transferencia   ( reset( rs.getString("tcta_transferencia"    ) ) );

                anticipo.setSecuencia            ( rs.getInt   ("secuencia") );

                lista.add( anticipo );
                anticipo = null;//Liberar Espacio JJCastro
            }
        }catch(Exception e) {
            e.printStackTrace();
            throw new SQLException(" getPlanillasAbonosPrestamos  -> " +e.getMessage());
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }



    /**
     * Metodo para guardar las amortizaciones de un prestamo en la base de datos
     * @autor mfontalvo
     * @param pt, Objeto prestamo que alamacena en una lista las amortizaciones
     * @param usuario, usuario que graba las amortizaciones
     * @throws Exception.
     */
    public String updatePlanillaPrestamosSQL ( AnticiposTerceros ant, String usuario ) throws Exception{

        if (ant==null) return "";

        StringStatement st  = null;
        String sql = "";
        try{

            st = new StringStatement(this.obtenerSQL("SQL_PLANILLAS_PENDIENTES_POR_PRESTAMO"));
            st.setString( usuario     );
            st.setInt   ( ant.getId() );
            sql += st.getSql();
        }catch(Exception e) {
            throw new SQLException(" PrestamoDAO: updatePlanillaPrestamosSQL  -> \n" +e.getMessage());
        }
        finally {
            if(st!=null)  st = null;
        }
        return sql;
    }




    /**
     * M�todo que inserta del movpla  registros a la tabla de anticipos para terceros
     * @autor....... mfontalvo
     * @throws...... Exception
     * @parameter... Movpla mov
     * @version..... 1.0.
     **/
    public synchronized String  InsertAnticipo(AnticiposTerceros ant, Usuario usuario )throws Exception {

        StringStatement st      = null;
        String          query   = "SQL_INSERT_ANTICIPO_PP";
        String          sql     = "";

        try{

            st = new StringStatement(this.obtenerSQL(query));

            st.setString(  ant.getDstrct()              );
            st.setString(  usuario.getId_agencia()      );//agencia de Cumplido
            st.setString(  ant.getPla_owner()           );
            st.setString(  ant.getPlanilla()            );
            st.setString(  ant.getSupplier()            );
            st.setString(  ant.getProveedor_anticipo()  );
            st.setString(  ant.getConcept_code()        );
            st.setDouble(  ant.getVlrNeto()             );
            st.setDouble(  ant.getVlrNeto()             );
            st.setString(  ant.getCurrency()            );
            st.setString(  "now()"                      );
            st.setString(  usuario.getLogin()           );
            st.setString(  usuario.getBase()            );
            st.setString(  "N"                          );
            st.setDouble(  0                            ); // procentaje
            st.setDouble(  0                            ); // valor_descuento
            st.setDouble(  ant.getVlrNeto()             ); // valor
            st.setDouble(  ant.getVlrComision()         ); // comision
            st.setDouble(  ant.getVlrNeto() -  ant.getVlrComision()   ); // neto
            st.setString(  ant.getConductor()           );
            st.setString(  ""                           ); // numero liquidacion
            st.setString(  "S"                          ); // aprobado?
            st.setString(  usuario.getLogin()           );
            st.setString(  "now()"                      );
            st.setString(  ant.getNit_cuenta()          );
            st.setString(  ant.getNombre_cuenta()       );
            st.setString(  ant.getTipo_cuenta()         );
            st.setString(  ant.getBanco()               );
            st.setString(  ant.getSucursal()            );
            st.setString(  ant.getCuenta()              );
            st.setInt   (  ant.getSecuencia()           ); // secuencia

            st.setString( ant.getBanco_transferencia()  );
            st.setString( ant.getCuenta_transferencia() );
            st.setString( ant.getTcta_transferencia()   );
            sql += st.getSql();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException(" InsertAnticipo "+ e.getMessage());
        }
        finally{
            if(st!=null) st = null;
        }
        return sql;
    }




    /**
     * M�todo que extrae los propietarios que tiene prestamos pendientes pos liquidar
     * @autor.......mfontalvo
     * @throws......Exception
     * @version.....1.0.
     **/
    public Vector getTransferenciasPorID( String ids ) throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_TRANSFERENCIAS_POR_IDS";
        Vector            lista       = new Vector();

        try{

            con = this.conectarJNDI( query );

            //.out.println(  this.obtenerSQL(query).replaceAll("#IDS#",ids) );

            st = con.prepareStatement( this.obtenerSQL(query).replaceAll("#IDS#",ids) );
            rs = st.executeQuery();
            while(rs.next()){
                AnticiposTerceros anticipo = new AnticiposTerceros();
                anticipo.setDstrct               ( reset( rs.getString("dstrct"                ) ) );
                anticipo.setId                   ( rs.getInt   ("id") );
                anticipo.setPla_owner            ( reset( rs.getString("pla_owner"             ) ) );
                anticipo.setPlanilla             ( reset( rs.getString("planilla"              ) ) );
                anticipo.setSupplier             ( reset( rs.getString("supplier"              ) ) );
                anticipo.setProveedor_anticipo   ( reset( rs.getString("proveedor_anticipo"    ) ) );
                anticipo.setConcept_code         ( reset( rs.getString("concept_code"          ) ) );
                anticipo.setVlrNeto              ( rs.getDouble("vlr_neto")  );
                anticipo.setVlrComision          ( rs.getDouble("vlr_combancaria")  );
                anticipo.setConductor            ( reset( rs.getString("cedcon"                ) ) );
                anticipo.setNit_cuenta           ( reset( rs.getString("nit_cuenta"            ) ) );
                anticipo.setNombre_cuenta        ( reset( rs.getString("nombre_cuenta"         ) ) );
                anticipo.setTipo_cuenta          ( reset( rs.getString("tipo_cuenta"           ) ) );
                anticipo.setBanco                ( reset( rs.getString("banco"                 ) ) );
                anticipo.setSucursal             ( reset( rs.getString("sucursal"              ) ) );
                anticipo.setCuenta               ( reset( rs.getString("cuenta"                ) ) );
                anticipo.setBanco_transferencia  ( reset( rs.getString("banco_transferencia"   ) ) );
                anticipo.setCuenta_transferencia ( reset( rs.getString("cuenta_transferencia"  ) ) );
                anticipo.setTcta_transferencia   ( reset( rs.getString("tcta_transferencia"    ) ) );

                anticipo.setSecuencia            ( rs.getInt   ("secuencia") );

                lista.add( anticipo );
            }
        }catch(Exception e) {
            e.printStackTrace();
            throw new SQLException(" getTransferenciasPorID  -> " +e.getMessage());
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }

      /**
     * M�todo que carga los reportes cumplidos en un rango de fechas
     * @autor.......jbarros
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List search_ReporteCumplidos(String fec1,String fec2) throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_REPORTE_CUMPLIDOS";
        List              lista       = new LinkedList();

        TablaGeneralManagerDAO TDAO   = new TablaGeneralManagerDAO(this.getDatabaseName());
        String CLI_FINTRA             = "CLIFINTRA";
        try{



            con = this.conectarJNDI( query );
            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, fec1 + " 00:00:00");
            st.setString(2, fec2 + " 23:59:59");
            rs = st.executeQuery();
            while(rs.next()){
                Hashtable  Rep  =  new  Hashtable();
                Rep.put("Documento",             reset(  rs.getString("cod_doc")         ) );
                Rep.put("Valor Planilla",        reset(  rs.getString("vlrpla2")         ) );
                Rep.put("Nit Propietario",       reset(  rs.getString("nitpro")          ) );
                Rep.put("nombre",                reset(  rs.getString("nombre")          ) );
                Rep.put("direccion",             reset(  rs.getString("direccion")       ) );
                Rep.put("Cod Ciudad",            reset(  rs.getString("codciu")          ) );
                Rep.put("telefono",              reset(  rs.getString("telefono")        ) );
                Rep.put("celular",               reset(  rs.getString("celular")         ) );
                Rep.put("e_mail",                reset(  rs.getString("e_mail")          ) );
                Rep.put("Usuario Creador",       reset(  rs.getString("creation_user")   ) );
                Rep.put("Fecha de creacion",     reset(  rs.getString("creation_date")   ) );
                Rep.put("id_agencia",            reset(  rs.getString("id_agencia")      ) );
                Rep.put("migrada_a_mims",        reset(  rs.getString("migrada_a_mims")  ) );


             // Buscar Referenciado
                String    nit   = reset(  rs.getString("nitpro")  );
                Hashtable TGEN  = TDAO.obtenerRegistroReferenciado_fintra( CLI_FINTRA ,nit );

                String asesor      = "";
                String referenciado= "";
                String status      = "";
                String obs         = "";

                if( TGEN != null ){
                    asesor      = (String)TGEN.get("ASESOR COMERCIAL");
                    referenciado= (String)TGEN.get("REFERENCIADO");
                    status      = (String)TGEN.get("STATUS");
                    obs         = (String)TGEN.get("OBSERVACION");
                }
                Rep.put("asesor"           , asesor        );
                Rep.put("referenciado"     , referenciado  );
                Rep.put("status"           , status        );
                Rep.put("observacion"      , obs           );


                lista.add( Rep );
            }


        }catch(Exception e) {
            throw new SQLException(" search_ReporteCumplidos  -> " +e.getMessage());
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            TDAO = null;
        }
        return lista;
    }

   /**
     * M�todo que busca datos refernciado del proveedor
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public AnticiposTerceros llenarReferencido(AnticiposTerceros anticipo)throws Exception{
        TablaGeneralManagerDAO TDAO   = new TablaGeneralManagerDAO(this.getDatabaseName());
        String CLI_FINTRA             = "CLIFINTRA";
        try{
            String    nit   = anticipo.getPla_owner();
            Hashtable TGEN  = TDAO.obtenerRegistroReferenciado_fintra( CLI_FINTRA ,nit );

            String asesor      = "";
            String referenciado= "";
            String status      = "";
            String obs         = "";

            if( TGEN != null ){
                asesor      = (String)TGEN.get("ASESOR COMERCIAL");
                referenciado= (String)TGEN.get("REFERENCIADO");
                status      = (String)TGEN.get("STATUS");
                obs         = (String)TGEN.get("OBSERVACION");
            }

            anticipo.setAsesor       ( asesor       );
            anticipo.setReferenciado ( referenciado );
            anticipo.setStatus       ( status       );
            anticipo.setObservacion  ( obs          );

        }catch(Exception e){
             e.printStackTrace();
        }finally{
           TDAO = null;
        }
        return anticipo;
    }

    public boolean acordeConExtractoDetalle(String idAnticipo) throws Exception{

        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_ANTICIPOS_EXTRACTOS_INCONSISTENTES";
        Vector            lista       = new Vector();
        boolean respuesta=true;
        try{

            con = this.conectarJNDI( query );
            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, idAnticipo );
            rs = st.executeQuery();
            if(rs.next()){
                respuesta=false;
            }
        }catch(Exception e) {
            System.out.println("error en acordeConExtractoDetalle"+e.toString()+"_"+e.getMessage());
            e.printStackTrace();
            throw new SQLException(" acordeConExtractoDetalle  -> " +e.getMessage());
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;

    }

    public  List agruparF( String anticipos[] ) throws Exception{
            Connection con = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            String             query   = "SQL_AGRUPAR_F";
            List               lista   = new LinkedList();

            try{

                String ids = "";
                for(int i=0;i<anticipos.length;i++){
                    if(!ids.equals(""))  ids +=",";
                    ids += "'" + anticipos[i] + "'";
                }


                con = this.conectarJNDI(query);//JJCastro fase2
                if (con!=null){
                String sql   =   this.obtenerSQL( query ).replaceAll("#ANTICIPOS#", ids );
                st= con.prepareStatement(sql);
                //.out.println("Queryyzzz--->"+st.toString());
                rs=st.executeQuery();
                while(rs.next()){
                    AnticiposTerceros anticipo = new  AnticiposTerceros();
                       anticipo.setBanco             ( reset( rs.getString("banco_transfer") )               );
                       anticipo.setNombre_cuenta     ( reset( rs.getString("nombre_cuenta") )       );
                       anticipo.setCuenta            ( reset( rs.getString("no_cuenta") )              );
                       anticipo.setTipo_cuenta       ( reset( rs.getString("tipo_cuenta") )         );
                       anticipo.setNit_cuenta        ( reset( rs.getString("cedula_cuenta") )          );
                       anticipo.setVlrConsignar      (        rs.getDouble("total")      );
                    lista.add(  anticipo );
                anticipo = null;//Liberar Espacio JJCastro
                }
            }}catch(Exception e){
                  throw new Exception( "agrupar " + e.getMessage());
            }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
            return lista;

     }



/**
 *
 * @param anticipos
 * @param comisionador
 * @param banc
 * @return
 * @throws Exception
 */
     public  List agruparFGas( String anticipos[] ,String comisionador,String banc) throws Exception{

            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            Connection         con     = null;
            String             query   = "SQL_AGRUPAR_FGAS";
            List               lista   = new LinkedList();

            try{

                String ids = "";
                for(int i=0;i<anticipos.length;i++){
                    if(!ids.equals(""))  ids +=",";
                    ids += "'" + anticipos[i] + "'";
                }


             con = this.conectarJNDI(query);//JJCastro fase2
             if (con != null) {
                 String sql = this.obtenerSQL(query).replaceAll("#ANTICIPOS#", ids);
                 st = con.prepareStatement(sql);
                 //.out.println("Queryyzzz--->"+st.toString());
                 rs = st.executeQuery();
                 while (rs.next()) {
                     AnticiposTerceros anticipo = new AnticiposTerceros();
                     anticipo.setBanco(reset(rs.getString("banco_transfer")));
                     anticipo.setNombre_cuenta(reset(rs.getString("nombre_cuenta")));
                     anticipo.setCuenta(reset(rs.getString("no_cuenta")));
                     anticipo.setTipo_cuenta(reset(rs.getString("tipo_cuenta")));
                     anticipo.setNit_cuenta(reset(rs.getString("cedula_cuenta")));
                     anticipo.setVlrConsignar(rs.getDouble("total"));

                     if (comisionador.equals("Estacion")) {
                       if (anticipo.getBanco() != null && anticipo.getBanco().equals("Bancolombia") && (banc.equals("07") || banc.equals("7B"))) {
                             anticipo.setVlrConsignar(anticipo.getVlrConsignar() - 2000);
                         } else {
                           if (anticipo.getBanco() != null && (banc.equals("07") || banc.equals("7B"))) {
                                 anticipo.setVlrConsignar(anticipo.getVlrConsignar() - 3500);
                             }
                         }
                     }

                     lista.add(anticipo);
                 anticipo = null;//Liberar Espacio JJCastro
                 }

             }
         }catch(Exception e){
                  throw new Exception( "agrupar " + e.getMessage());
            }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
            return lista;

     }

     /**
     * Agrupa los pagos de acuerdo al banco, sucursal, cliente, cuenta, valor a pagar.
     * @autor tmolina
     **/
     public  List groupByPrestamos( String anticipos[] ) throws Exception{

            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            Connection         con     = null;
            String             query   = "SQL_AGRUPAR_PRESTAMOS";
            List               lista   = new LinkedList();

            try{

                String ids = "";
                for(int i=0;i<anticipos.length;i++){
                    if(!ids.equals(""))  ids +=",";
                    ids += "'" + anticipos[i] + "'";
                }

                con = this.conectarJNDI(query);//JJCastro fase2
                if(con!=null){
                String sql   =   this.obtenerSQL( query ).replaceAll("#PRESTAMOS#", ids );
                st= con.prepareStatement(sql);
                //.out.println("QuerygroupByPrestamo--->"+st.toString());
                rs=st.executeQuery();
                while(rs.next()){
                    AnticiposTerceros anticipo = new  AnticiposTerceros();
                       anticipo.setBanco             ( reset( rs.getString("banco_transfer") )               );
                       anticipo.setNombre_cuenta     ( reset( rs.getString("nombre_cuenta") )       );
                       anticipo.setCuenta            ( reset( rs.getString("no_cuenta") )              );
                       anticipo.setTipo_cuenta       ( reset( rs.getString("tipo_cuenta") )         );
                       anticipo.setNit_cuenta        ( reset( rs.getString("cedula_cuenta") )          );
                       anticipo.setVlrConsignar      (        rs.getDouble("total")      );
                    lista.add(  anticipo );
                anticipo = null;//Liberar Espacio JJCastro
                }
                } }catch(Exception e){
                  throw new Exception( "GroupByPrestamoS" + e.getMessage());
            }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
            return lista;
     }



    /**
     * M�todo que busca las planillas tipo Abono prestamo ordenadas por fecha de anticipo
     * @autor   Tmolina
     * @throws  Exception
     * @version 15-may-2009
     **/
    public Vector getPlanillasAbonosPrestamosOrdenadas( String beneficiario ) throws Exception{
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_PLANILLAS_PENDIENTES_ORDENADAS";
        Vector            lista       = new Vector();

        try{

            con = this.conectarJNDI( query );
            st = con.prepareStatement( this.obtenerSQL(query) );
            st.setString(1, beneficiario );
            rs = st.executeQuery();
            while(rs.next()){
                AnticiposTerceros anticipo = new AnticiposTerceros();
                anticipo.setDstrct               ( reset( rs.getString("dstrct"                ) ) );
                anticipo.setId                   ( rs.getInt   ("id") );
                anticipo.setPla_owner            ( reset( rs.getString("pla_owner"             ) ) );
                anticipo.setPlanilla             ( reset( rs.getString("planilla"              ) ) );
                anticipo.setSupplier             ( reset( rs.getString("supplier"              ) ) );
                anticipo.setProveedor_anticipo   ( reset( rs.getString("proveedor_anticipo"    ) ) );
                anticipo.setConcept_code         ( reset( rs.getString("concept_code"          ) ) );
                anticipo.setVlrNeto              ( rs.getDouble("vlr_neto")  );
                anticipo.setVlrComision          ( rs.getDouble("vlr_combancaria")  );
                anticipo.setConductor            ( reset( rs.getString("cedcon"                ) ) );
                anticipo.setNit_cuenta           ( reset( rs.getString("nit_cuenta"            ) ) );
                anticipo.setNombre_cuenta        ( reset( rs.getString("nombre_cuenta"         ) ) );
                anticipo.setTipo_cuenta          ( reset( rs.getString("tipo_cuenta"           ) ) );
                anticipo.setBanco                ( reset( rs.getString("banco"                 ) ) );
                anticipo.setSucursal             ( reset( rs.getString("sucursal"              ) ) );
                anticipo.setCuenta               ( reset( rs.getString("cuenta"                ) ) );
                anticipo.setBanco_transferencia  ( reset( rs.getString("banco_transferencia"   ) ) );
                anticipo.setCuenta_transferencia ( reset( rs.getString("cuenta_transferencia"  ) ) );
                anticipo.setTcta_transferencia   ( reset( rs.getString("tcta_transferencia"    ) ) );
                anticipo.setFecha_anticipo     ( reset( rs.getString("fecha_anticipo"    ) ) );

                anticipo.setSecuencia            ( rs.getInt   ("secuencia") );

                lista.add( anticipo );
            }
        }catch(Exception e) {
            e.printStackTrace();
            throw new SQLException(" getPlanillasAbonosPrestamos  -> " +e.getMessage());
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }

    //--ADD 16-Junio-2009
    /**
     * M�todo que agrupa los negocios a transferir.
     * @autor   Tmolina
     * @throws  Exception
     * @version 16-Jun-2009
     **/
    public  List agruparN( String anticipos[],String cxp[] ) throws Exception{

            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            Connection         con     = null;
            String             query   = "SQL_AGRUPAR_N";
            List               lista   = new LinkedList();

            try{

                String ids = "";
                for(int i=0;i<anticipos.length;i++){
                    if(!ids.equals(""))  ids +=",";
                    ids += "'" + anticipos[i] + "'";
                }
                
                String docs = "";
                for(int i=0;i<cxp.length;i++){
                    if(!docs.equals(""))  docs +=",";
                    docs += "'" + cxp[i] + "'";
                }

                String neg,cxps="";
                neg=ids.replaceAll("'","''");cxps=docs.replaceAll("'", "''");
                //con = this.conectar(query);
                con = this.conectarJNDI( query );

                String sql   =   this.obtenerSQL( query ).replaceAll("#ANTICIPOS#", ids );
                sql    =   sql.replaceAll("#CXPS#", docs ).replaceAll("#VAR1#", neg).replaceAll("#VAR2#", cxps);
                
                st= con.prepareStatement(sql);
                //.out.println("Query--->"+st.toString());
                rs=st.executeQuery();
                while(rs.next()){
                    AnticiposTerceros anticipo = new AnticiposTerceros();
                    anticipo.setBanco(reset(rs.getString("banco_transfer")));
                    anticipo.setNombre_cuenta(reset(rs.getString("nombre_cuenta")));
                    anticipo.setCuenta(reset(rs.getString("no_cuenta")));
                    anticipo.setTipo_cuenta(reset(rs.getString("tipo_cuenta")));
                    anticipo.setNit_cuenta(reset(rs.getString("cedula_cuenta")));
                    anticipo.setVlrConsignar(rs.getDouble("total"));
                    anticipo.setLote_transferencia(reset(rs.getString("lote_transferencia")));
                    lista.add(  anticipo );
                anticipo = null;//Liberar Espacio JJCastro
                }


            }catch(Exception e){
                  throw new Exception( "Agrupar Negocios " + e.getMessage());
            }
            finally{
                if(st!=null) st.close();
                if(rs!=null) rs.close();
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
            return lista;

     }
    /**
     * Actualiza n bancos de n anticipos
     * @param lista ArrayList con los datos
     * @throws Exception Cuando hay error
     * @since 2010-05-13
     * @author rhonalf
     */
    public void updateBank(ArrayList lista) throws Exception{
        String sql = "";
        String query = "UPD_BANK";
        Vector batch = null;
        String dato1 = "";
        String dato2 = "";
        String dato3 = "";
        String dato4 = "";
        String[] vec = null;
        try {
            batch = new Vector();
            for (int i = 0; i < lista.size(); i++) {
                sql = this.obtenerSQL(query);
                vec = ((String)lista.get(i)).split(";-;");
                dato1 = vec[0];
                vec = vec[1].split(";_;");
                dato2 = vec[0];
                dato3 = vec[1];
                dato4 = vec[2];
                sql = sql.replaceAll("dato1", dato1);
                sql = sql.replaceAll("dato2", dato2);
                sql = sql.replaceAll("dato3", dato3);
                sql = sql.replaceAll("dato4", dato4);
                batch.add(sql);
            }
            ApplusDAO apdao = new ApplusDAO(this.getDatabaseName());
            apdao.ejecutarSQL(batch);
        }
        catch (Exception e) {
            throw new Exception("Error en anticipospagostercerosdao en updatebank: "+e.toString());
        }
    }
    public ArrayList ObtenerAnticiposNoTransferidos(String nit)throws Exception{
        Connection  con=null;
        ResultSet   rs=null;
        PreparedStatement ps=null;
        ArrayList ret=new ArrayList();
        try{
            con=conectarJNDI("SQL_OBTENER_ANTICIPOS_PARA_ANULAR");
            ps=con.prepareStatement(obtenerSQL("SQL_OBTENER_ANTICIPOS_PARA_ANULAR"));
            ps.setString(1, nit);
            rs=ps.executeQuery();
            TablaGeneralManagerDAO TDAO   = new TablaGeneralManagerDAO(this.getDatabaseName());
            while(rs.next()){
                ArrayList arl=new ArrayList();
                arl.add(rs.getString("secuencia"));
                arl.add(rs.getString("id"));
                arl.add(rs.getString("nombre_agencia"));
                arl.add(rs.getString("cedcon")+" - "+getNameNit(rs.getString("cedcon")));
                arl.add(rs.getString("pla_owner")+" - "+rs.getString("nombre_prpietario"));
                arl.add(rs.getString("supplier"));
                arl.add(rs.getString("planilla"));
                arl.add(rs.getString("fecha_anticipo"));
                arl.add(rs.getString("reanticipo"));
                arl.add(rs.getString("vlr"));

                Hashtable TGEN  = TDAO.obtenerRegistroReferenciado_fintra( "CLIFINTRA",rs.getString("pla_owner") );
                if( TGEN != null ){
                arl.add((String)TGEN.get("ASESOR COMERCIAL"));
                arl.add((String)TGEN.get("REFERENCIADO"));
                }
                else
                {   arl.add("");
                    arl.add("");
                }
                arl.add(rs.getString("creation_user"));
                arl.add(rs.getString("creation_date"));
                arl.add(rs.getString("concept_code"));
                ret.add(arl);
            }
        }
        catch(Exception e)
        {   e.printStackTrace();
            System.out.println(e.toString());
            throw new Exception( "Listar Anular Negocios " + e.getMessage());

        }
        finally{
                if(ps!=null) ps.close();
                if(rs!=null) rs.close();
                //this.desconectar(query);
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return ret;
    }

    public void AnularAnticipos(String login,String [] ids,String [] obs)throws Exception{
        Connection  con=null;
        PreparedStatement ps=null;
        String sql="";
        String consulta=obtenerSQL("SQL_ANULAR_ANTICIPOS");
        try{
            con=conectarJNDI("SQL_ANULAR_ANTICIPOS");
            for(int i=0;i<ids.length;i++)
            {   StringStatement ss= new StringStatement(consulta, true);
                ss.setString(1,obs[i]);
                ss.setString(2, login);
                ss.setString(3,ids[i]);
                ss.setString(4,ids[i]);
                sql=sql+ss.getSql();
            }
            ps=con.prepareStatement(sql);
            ps.executeUpdate();
        }
        catch(Exception e)
        {   e.printStackTrace();
            System.out.println(e.toString());
            throw new Exception( "Anular Negocios " + e.getMessage());

        }
        finally{
            if(ps!=null) ps.close();
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    public ArrayList obtainAnticiposSemiRepetidos(List ants)throws Exception{//20101013 se hallan los semirepetidos para la lista
        Connection  con=null;
        ResultSet   rs=null;
        PreparedStatement ps=null;
        ArrayList ret=new ArrayList();
        String ids="";
        try{

            for (int i=0;i<ants.size();i++){
                ids=ids+"'"+((AnticiposTerceros)ants.get(i)).getId()+"',";
            }
            ids=ids.substring(0,ids.length()-1);

            con=conectarJNDI("SQL_OBTENER_ANTICIPOS_SEMIREPETIDOS");
            ps=con.prepareStatement(obtenerSQL("SQL_OBTENER_ANTICIPOS_SEMIREPETIDOS").replaceAll("#losids#", ids));
            rs=ps.executeQuery();
            while(rs.next()){
                ret.add(rs.getString("id"));
            }
        }
        catch(Exception e)        {
            e.printStackTrace();
            System.out.println("error en obtainAnticiposSemiRepetidos"+e.toString());
            throw new Exception( "obtainAnticiposSemiRepetidos " + e.getMessage());
        }finally{
                if(ps!=null) ps.close();
                if(rs!=null) rs.close();
                //this.desconectar(query);
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return ret;  
    }






        /**
     * M�todo que lista de los prontopagos que no se an enviado por correo.
     * @autor   Jpinedo
     * @throws  Exception
     * @version 09-Jun-2011
     **/

        public List<AnticiposTerceros> getListaAnticiposEmailPP() throws Exception{
            List<AnticiposTerceros> lista = new LinkedList<AnticiposTerceros>();
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_ANTICIPOS_EMAIL_PP";
        AnticiposTerceros  atp     = new AnticiposTerceros();

        try{

            con = this.conectarJNDI(query);
            String sql   =   this.obtenerSQL( query );
            st= con.prepareStatement(sql);
            rs=st.executeQuery();
            while( rs.next() )
            {
                if(valida_lis_secuancias_anticipos(rs.getInt("secuencia"),lista))
                {
                AnticiposTerceros at = new AnticiposTerceros();

                at.setPla_owner(rs.getString("pla_owner"));
                at.setPlanilla(rs.getString("planilla"));
                at.setSecuencia(rs.getInt("secuencia"));
                at.setConcept_code("Concept_code");
                at.setDato_generico(rs.getString("e_mail"));
                lista.add(at);
                }

            }

        }catch(Exception e){
            throw new Exception( " getListaAnticiposEmail" + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;

    }



        /**
     * M�todo que lista de los anticipos (no prontopagos) que no se an enviado por correo.
     * @autor   Jpinedo
     * @throws  Exception
     * @version 09-Jun-2011
     **/
        public List<AnticiposTerceros> getListaAnticiposEmail() throws Exception{
        List<AnticiposTerceros> lista = new LinkedList<AnticiposTerceros>();
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_ANTICIPOS_EMAIL";
        AnticiposTerceros  atp     = new AnticiposTerceros();

        try{

            con = this.conectarJNDI(query);
            String sql   =   this.obtenerSQL( query );
            st= con.prepareStatement(sql);
            rs=st.executeQuery();
            while( rs.next() )
            {
                AnticiposTerceros at = new AnticiposTerceros();
                at.setPla_owner(rs.getString("pla_owner"));
                at.setPlanilla(rs.getString("planilla"));
                at.setSecuencia(rs.getInt("secuencia"));
                at.setConcept_code("Concept_code");
                 at.setDato_generico(rs.getString("e_mail"));
                lista.add(at);
            }

        }catch(Exception e){
            throw new Exception( " getListaAnticiposEmail" + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;

    }


        /**
     * M�todo que busca un prontopago por su secuencia o liquidacion.
     * @autor   Jpinedo
     * @throws  Exception
     * @version 09-Jun-2011
     **/

         public  AnticiposTerceros getAnticipo_x_secuencia( int secuencia ) throws Exception{
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_ANTICIPOS_TERCERO_X_SECUENCIA";
        AnticiposTerceros  atp     = new AnticiposTerceros();

        try{

            con = this.conectarJNDI(query);
            String sql   =   this.obtenerSQL( query );
            st= con.prepareStatement(sql);
            st.setInt(1,secuencia);
            rs=st.executeQuery();
            if( rs.next() ){
                atp.setPla_owner(rs.getString("pla_owner"));
                atp.setNombrePropietario(rs.getString("payment_name"));
                atp.setBanco(rs.getString("branch_code"));
                atp.setSucursal(rs.getString("bank_account_no"));
                atp.setSecuencia(rs.getInt("secuencia"));

            }
        }catch(Exception e){
            throw new Exception( " getAnticipo x secuencia" + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return atp;

    }



        /**
     * M�todo que busca un anticipo por su planilla.
     * @autor   Jpinedo
     * @throws  Exception
     * @version 09-Jun-2011
     **/
     public  AnticiposTerceros getAnticipo_x_planilla( String planilla ) throws Exception{
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_ANTICIPOS_TERCERO_X_PLANILLA";
        AnticiposTerceros  atp     = new AnticiposTerceros();

        try{

            con = this.conectarJNDI(query);
            String sql   =   this.obtenerSQL( query );
            st= con.prepareStatement(sql);
            st.setString(1,planilla);
            rs=st.executeQuery();
            while( rs.next() ){
                atp.setPla_owner(rs.getString("pla_owner"));
                atp.setNombrePropietario(rs.getString("payment_name"));
                atp.setBanco(rs.getString("banco"));
                atp.setSucursal(rs.getString("bank_account_no"));
                atp.setSupplier(rs.getString("supplier"));
                atp.setFecha_creacion(rs.getString("creation_date"));
                atp.setPlanilla(rs.getString("planilla"));
                atp.setVlr(rs.getDouble("vlr"));
                atp.setNombreConductor(rs.getString("nombre"));
                atp.setConductor(rs.getString("cedcon"));
                atp.setConcept_code(rs.getString("nombre_concepto"));
                atp.setUsuario_creacion(rs.getString("creation_user"));
                atp.setNombreAgencia(rs.getString("nombre_agencia"));


            }
        }catch(Exception e){
            throw new Exception( " getAnticipo x secuencia" + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return atp;

    }



        /**
     * M�todo que busca un anticipo  por su planilla y lo marca como enviado.
     * @autor   Jpinedo
     * @throws  Exception
     * @version 09-Jun-2011
     **/
    public void   MarcarAnticiposEmail(String  planilla) throws Exception{

        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_MARCAR_ANTICIPOS_EMAIL";
        try{
            con = this.conectarJNDI(query);
            String sql   =   this.obtenerSQL( query );
            st= con.prepareStatement(sql);
            st.setString(1, planilla);
            st.execute();

        }
        catch(Exception e){
            throw new Exception( " Error Marcando correo enviado" + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

    }

            /**
     * M�todo que busca un prontopagoo  por su secuencia y lo marca como enviado.
     * @autor   Jpinedo
     * @throws  Exception
     * @version 09-Jun-2011
     **/
    public void   MarcarAnticiposEmail_PP(int secuencia) throws Exception{

        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_MARCAR_ANTICIPOS_EMAIL_PP";
        try{
            con = this.conectarJNDI(query);
            String sql   =   this.obtenerSQL( query );
            st= con.prepareStatement(sql);
            st.setInt(1, secuencia);
            st.execute();

        }
        catch(Exception e){
            throw new Exception( " Error Marcando correo enviado" + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

    }


    /*******************************  validadr duplicidad de  Secuencias en lista anticipos ******************************/
    public static boolean valida_lis_secuancias_anticipos(int secuencia,List<AnticiposTerceros> lista) {
        boolean sw = true;
        int i = 0;
        while (i < lista.size()) {
            if (lista.get(i).getSecuencia()== secuencia) {
                sw = false;
            }
            i++;

        }
        return sw;
    }
    
    
    
    
    /**
     * Obtiene la siguiente secuencia de letras
     * @param secActual secuencia actual
     * @return siguientes letras de la secuencia
     */
    private String obtenerSiguiente(String secActual){
        StringBuilder alfabeto = new StringBuilder("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        StringBuilder actual = new StringBuilder(secActual);
        String siguiente = "";
        
        //Se busca la ultima letra
        String ultima = actual.substring(actual.length()-1);
        if(ultima.equals("Z")){
            if(secActual.length()>1){
                //Se busca lo que precede a la ultima letra
                String textAnterior = secActual.substring(0, secActual.length()-1);
                siguiente = obtenerSiguiente(textAnterior) + "A";
            }else{
                siguiente = "AA";
            }
        }else{
            int pos = alfabeto.indexOf(ultima);
            actual.replace(actual.length()-1, actual.length(), String.valueOf(alfabeto.charAt(pos+1)));
            siguiente = actual.toString();
        }
        
        return siguiente;
    }
    
    /**
     * Obtiene el valor de la secuencia del archivo para un banco
     * @param banco Codigo del banco
     * @param usuario codigo del usuario en sesion
     * @return valor de la secuencia del archivo
     * @throws Exception 
     */
    public String obtenerSecuenciaBanco(String banco, String usuario) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_OBTENER_SECUENCIA_BANCO";
        PreparedStatement st2 = null;
        Connection con2 = null;
        String query2 = "SQL_ACTUALIZAR_SECUENCIA_BANCO";
        String valorSecuencia = "";
        try{
            con = this.conectarJNDI(query);
            if ( con != null ){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, banco);
                rs = st.executeQuery();
                if(rs.next()){
                    if(rs.getString("secuencia").equals("-")){
                        //actualizar la fecha y colocar A
                        valorSecuencia = "A";
                    }else{
                        valorSecuencia = obtenerSiguiente(rs.getString("secuencia"));
                    }
                }
            }
            con2 = this.conectarJNDI(query2);
            if ( con2 != null ){
                st2 = con2.prepareStatement(this.obtenerSQL(query2));
                st2.setString(1, valorSecuencia);
                st2.setString(2, usuario);
                st2.setString(3, banco);
                st2.executeUpdate();
            }
        }catch(Exception e){
            throw new Exception("ERROR en obtenerSecuenciaBanco[AnticiposPagosTercerosDAO] " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
            if (st2  != null){ try{ st2 = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con2 != null){ try{ this.desconectar(con2); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return valorSecuencia;
    }
    
    public String obtenerInfoTextoEfecty(String[] negocios) {
        String text = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TEXT_EFECTY";
        try {

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setArray(1, con.createArrayOf("text", negocios));
            rs = ps.executeQuery();
            while (rs.next()) {
                text = rs.getString("respuesta");
            }
            text.substring(1);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return text;
        }

    }
     
    
    
    
    /**
     * Obtiene los permisos de usuario
     * @param usuario usuario en session
     * @return lista de permisos
     * @Autor egonzalez
     */
    private String obtenerPermisoTSP(Usuario user) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_BUSCAR_PERMISO_TSP";
        String result="";

        try {
            con = this.conectarJNDI(query);
            String sql = this.obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1,user.getLogin());
            rs=st.executeQuery();
            
            while( rs.next() ){
            
                result=rs.getString("dato");
            
            }

        } catch (Exception e) {
            throw new Exception(" Error Marcando correo enviado" + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return result ;
    }

    /**
     * M�todo obtiene las ultimas trasferencias realizadas a un propietario
     *
     * @autor.......Egonzalez
     * @throws......Exception
     * @version.....1.0.
     *
     */
    public ArrayList<AnticiposTerceros> ultimasTransferencias(String cuenta, String nit_cuenta) throws Exception {

        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_ULTIMAS_TRANSFERENCIAS";
        ArrayList<AnticiposTerceros> lista = new ArrayList<AnticiposTerceros>();

        try {

            con = this.conectarJNDI(query);
            String sql = this.obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, cuenta);
            st.setString(2, nit_cuenta);
            rs = st.executeQuery();
            while (rs.next()) {
                AnticiposTerceros anticipo = new AnticiposTerceros();
                anticipo.setVlr(rs.getDouble("vlr"));
                anticipo.setVlrConsignar(rs.getDouble("vlr_consignacion"));
          
                lista.add(anticipo);
}


        } catch (Exception e) {
            throw new Exception("agrupar " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return lista;

    }
    
    
     /**
     * M�todo Actualiza la planilla para que cuadre con el egreso.
     * @autor.......egonzalez
     * @throws......Exception
     * @version.....1.0.
     **/
    public String updatePesoAnticipo(AnticiposTerceros trans, int peso, Usuario user) throws Exception {
        Connection con = null;
        StringStatement st = null;
        String query = "SQL_UPDATE_ANTICIPO_TRANSFERENCIA";
        String sql = "";
        String consulta = this.obtenerSQL(query);
        try {

            st = new StringStatement(consulta, true);
            //campos a modificar
            st.setDouble(1,peso);
            st.setString(2, user.getLogin());
            // where 
            st.setString(3,trans.getNit_cuenta());
            st.setString(4,trans.getCuenta());
            st.setString(5,trans.getPla_owner());
            
            //segundo where
            st.setString(6,trans.getNit_cuenta());
            st.setString(7,trans.getCuenta());
            
            sql = st.getSql();
            st = null;

        } catch (Exception e) {
            throw new Exception("updatePesoAnticipo " + e.getMessage());
        } finally {//JJCastro fase2
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sql;
    }
    
    
     /**
     * Obtiene los permisos de usuario
     * @param usuario usuario en session
     * @return lista de permisos
     * @Autor egonzalez
     */
    public String buscarPermisoTransferencia(Usuario user) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_PERMISO_PAGOS";
        String result="";

        try {
            con = this.conectarJNDI(query);
            String sql = this.obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1,user.getLogin());
            rs=st.executeQuery();
            
            while( rs.next() ){
            
                result=rs.getString("dato");
            
            }

        } catch (Exception e) {
            throw new Exception(" Error Marcando correo enviado" + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return result ;
    }
    
    
    /**
     * M�todo que agrupa anticipos por informacion del banco  a transferir
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List agruparTransferencia( String anticipos[] ) throws Exception{

        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_AGRUPAR_TRANSFERENCIAS";
        List               lista   = new LinkedList();

        try{

            String ids = "";
            for(int i=0;i<anticipos.length;i++){
                if(!ids.equals(""))  ids +=",";
                ids += "'" + anticipos[i] + "'";
            }


            con = this.conectarJNDI(query);
            String sql   =   this.obtenerSQL( query ).replaceAll("#ANTICIPOS#", ids );
            st= con.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                AnticiposTerceros anticipo = new  AnticiposTerceros();
                
                anticipo.setPla_owner(reset(rs.getString("pla_owner")));
                anticipo.setProveedor_anticipo(reset(rs.getString("proveedor_anticipo")));
                anticipo.setBanco(reset(rs.getString("banco")));
                anticipo.setNombre_cuenta(reset(rs.getString("nombre_cuenta")));
                anticipo.setCuenta(reset(rs.getString("cuenta")));
                anticipo.setTipo_cuenta(reset(rs.getString("tipo_cuenta")));
                anticipo.setNit_cuenta(reset(rs.getString("nit_cuenta")));
               // anticipo.setConductor(reset(rs.getString("cedcon")));
                anticipo.setVlr(rs.getDouble("vlr"));
                anticipo.setVlrConsignar(rs.getDouble("vlr_consignacion"));
                anticipo.setReanticipo(reset(rs.getString("reanticipo"))         );

                lista.add(  anticipo );
            }


        }catch(Exception e){
            throw new Exception( "agrupar " + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;

    }
    
    /**
     * M�todo Actualiza la planilla para que cuadre con el egreso.
     * @autor.......egonzalez
     * @throws......Exception
     * @version.....1.0.
     **/
    public String updatePesoAnticipoTransferencia(AnticiposTerceros trans, int peso, Usuario user) throws Exception {
        Connection con = null;
        StringStatement st = null;
        String query = "SQL_UPDATE_ANTICIPO_TRANSFERENCIA_V2";
        String sql = "";
        String consulta = this.obtenerSQL(query);
        try {

            st = new StringStatement(consulta, true);
            //campos a modificar
            st.setDouble(1,peso);
            st.setString(2, user.getLogin());
            
            // where 
            st.setString(3, trans.getNit_cuenta());
            st.setString(4, trans.getCuenta());
            st.setString(5, trans.getPla_owner());
            //where sub select
            st.setString(6, trans.getNit_cuenta());
            st.setString(7, trans.getCuenta());
           

            sql = st.getSql();
            System.out.println("QueryPesoT:" + sql);
            st = null;

        } catch (Exception e) {
            throw new Exception("updatePesoAnticipo " + e.getMessage());
        } finally {//JJCastro fase2
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sql;
    }

}

