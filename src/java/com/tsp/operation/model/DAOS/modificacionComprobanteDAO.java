/*
 * modificacionComprobanteDAO.java
 *
 * Created on 17 de abril de 2008, 15:12
 */

package com.tsp.operation.model.DAOS;

import java.sql.*;

/**
 *
 * @author  Fintra
 */
public class modificacionComprobanteDAO extends MainDAO {
    static String tem;
    /** Creates a new instance of modificacionComprobanteDAO */
    public modificacionComprobanteDAO() {
        super("modificacionComprobanteDAO.xml");
         tem="ND";
    }
    public modificacionComprobanteDAO(String dataBaseName) {
        super("modificacionComprobanteDAO.xml", dataBaseName);
         tem="ND";
    }
    
    public String  actualizarComprobante(String doc,String tipodoc,String proveedor,String fecha,String periodo,String user,String grupo_transaccion,String tipondx)  throws Exception{
        //System.out.println("doc"+doc+"tipodoc"+tipodoc+"proveedor"+proveedor+"fecha"+fecha+"periodo"+periodo+"user"+user+"grupo_transaccion"+grupo_transaccion+"tipondx"+tipondx);
        String respuesta="mal";
        Connection         con     = null;
        PreparedStatement  st      = null;
        
        String tipond=tipondx;
        if (tipond==null){
            tipond="";
        }
        
        String             query   = "SQL_UPDATE_COMPROBANTE";
        
        try{
            //System.out.println("_aceptarAnticipoGasolina");
            con = this.conectarJNDI(query);//JJCastro fase2
            con.setAutoCommit(false);
            //System.out.println("_aceptarAnticipoGasolina2");
            String sql    =   this.obtenerSQL( query );
            /*if (tipodoc.equals("FAP")){
                sql=sql.replaceAll("WHERE","WHERE tercero='"+proveedor+"' AND ");
            }*/
            //System.out.println("sql"+sql+"_"+idAntGas+"loginx"+loginx);            
            st = con.prepareStatement( sql );
            
            st.setString(1, user);
            st.setString(2, periodo);
            st.setString(3, fecha);
            st.setString(4, doc);
            String tem2="";
            if ((tipodoc.equals("ND") && tipondx.equals("CXP")) || tipodoc.equals("NC")){//xxxxxx
                st.setString(5, tem);
                               
            }else{
                st.setString(5, tipodoc);
            }
            
            st.setString(6, grupo_transaccion);
            //st.setString(3, proveedor);
            
            st.setString(7, user);
            st.setString(8, periodo);
            st.setString(9, doc);
            //st.setString(10, tipodoc);
            if ((tipodoc.equals("ND") && tipondx.equals("CXP")) || tipodoc.equals("NC")){//xxxxxx
                st.setString(10, tem);
                if (tem.equals("ND")){tem2="FAP";}
                if (tem.equals("FAP")){tem2="NC";}
                if (tem.equals("NC")){tem2="ND";} 
                tem=tem2;
            }else{
                st.setString(10, tipodoc);
                
            }
            st.setString(11, grupo_transaccion);
            
            int affected =st.executeUpdate();
                        
            if (affected>0){
                respuesta="actualizacionexitosa";
                query   = "";
                st=null;
                //inicio de parte operativa
                if (tipodoc.equals("ICA") || tipodoc.equals("ICR") || tipodoc.equals("ING")){
                    query   = "SQL_UPDATE_INGRESO";
                    sql    =   this.obtenerSQL( query );
                    st = con.prepareStatement( sql );
                    st.setString(1, periodo);
                    st.setString(2, doc);
                    st.setString(3, tipodoc);
                    
                    st.setString(4, periodo);//090512
                    st.setString(5, doc);//090512
                    st.setString(6, tipodoc);//090512
                    
                }
                if (tipodoc.equals("FAC") || tipodoc.equals("FAG") || (tipodoc.equals("ND") && tipodoc.equals("CXC")) ){
                    query   = "SQL_UPDATE_FACTURA";
                    sql    =   this.obtenerSQL( query );
                    st = con.prepareStatement( sql );
                    st.setString(1, periodo);
                    st.setString(2, doc);
                    st.setString(3, tipodoc);
                }
                if (tipodoc.equals("EGR")){
                    query   = "SQL_UPDATE_EGRESO";
                    sql    =   this.obtenerSQL( query );
                    st = con.prepareStatement( sql );
                    st.setString(1, periodo);
                    st.setString(2, grupo_transaccion);
                    st.setString(3, doc);                    
                }
                if (tipodoc.equals("FAP") || tipodoc.equals("NC") || (tipodoc.equals("ND") && tipondx.equals("CXP")) ){
                    query   = "SQL_UPDATE_CXP_DOC";
                    sql    =   this.obtenerSQL( query );
                    st = con.prepareStatement( sql );
                    st.setString(1, periodo);
                    st.setString(2, proveedor);
                    st.setString(3, tipodoc);
                    st.setString(4, doc);
                }
                if (tipodoc.equals("IF")){
                    query   = "SQL_UPDATE_ING_FENALCO";
                    sql    =   this.obtenerSQL( query );
                    st = con.prepareStatement( sql );
                    st.setString(1, periodo);
                    st.setString(2, doc);                                         
                }
                if (tipodoc.equals("NEG")){
                    query   = "SQL_UPDATE_NEGOCIOS";
                    sql    =   this.obtenerSQL( query );
                    st = con.prepareStatement( sql );
                    st.setString(1, periodo);
                    st.setString(2, doc);
                }
                
                if (tipodoc.equals("AEF") || tipodoc.equals("AGA") || tipodoc.equals("AET") || tipodoc.equals("EXT")){//091106
                    query   = "SQL_UPDATE_ORDEN_SERVICIO";
                    sql    =   this.obtenerSQL( query );
                    st = con.prepareStatement( sql );
                    st.setString(1, periodo);
                    st.setString(2, tipodoc);
                    st.setString(3, doc);
                }
                
                if (st!=null){
                    //System.out.println("st"+st.toString());
                    affected =st.executeUpdate();
                    if (affected<=0){
                        respuesta="noapareciodocumento";
                        con.rollback();
                    }
                }
                
                
                //fin de parte operativa               
                if (respuesta.equals("actualizacionexitosa")){
                    con.commit();
                }
                
            }else{
                respuesta="noapareciocomprobante";
                con.rollback();
            }
            
            /*inicio de actualizar preanticipo
            //if (affected>0){                
            //}            
            //fin de actualizar preanticipo*/
            
            
            
        }catch(Exception e){
            con.rollback();
            System.out.println("error en anticipoGasolinaDAO en aceptar..."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            //System.out.println("finally");
            if (con != null){con.setAutoCommit(true);}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            
             if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            
            
        }
        //System.out.println("respuesta"+respuesta);
        return respuesta;
    }
    
    
}
