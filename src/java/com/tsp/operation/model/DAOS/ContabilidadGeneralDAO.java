package com.tsp.operation.model.DAOS;

import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.ParametrosBeans;
import com.tsp.operation.model.beans.Usuario;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

/**
 *
 * @author egonzalez
 */
public interface ContabilidadGeneralDAO {

    /**
     * Metodo que buscas las facturas a indemnizar a corte de foto cartera.
     *
     * @param query := nombre de la consulta que se va ejecutar
     * @param parametrosBeans :=Parametros de la consulta
     * @param user := beans con usuario de session.
     * @return
     */
    public String getSelectDataBaseJson(String query, ParametrosBeans parametrosBeans, Usuario user);

    /**
     * Metodo que construye insert para almacenar las facturas a indemnizar
     *
     * @param query := nombre de la consulta que se va ejecutar
     * @param objects :=Objeto json con los datos de la factura
     * @param user := beans con usuario de session.
     * @return
     */
    public String getInsertDataBaseJson(String query, JsonObject objects, Usuario user);

    /**
     * Metodo que muestra la cartera activa en fintra
     *
     * @param user
     * @return
     */
    public String carteraEn(Usuario user);

    /**
     * Creado por Harold Cuello. Metodo que muestra la cartera de acuerdo a la fiducia
     *
     * @param lineaNegocio
     * @param user
     * @return
     */
    public String UnidadNegocio(String lineaNegocio, Usuario user);

    /**
     * Creado por Harold Cuello. Metodo que muestra la cartera de acuerdo a la fiducia
     *
     * @param user
     * @return
     */
    public String CustodiaCartera(Usuario user);

    /**
     * Creado por Harold Cuello. Metodo que muestra la cartera de acuerdo a la fiducia
     *
     * @param FiduciaActual
     * @param user
     * @return
     */
    public String EndosarAfiducia(String FiduciaActual, Usuario user);

    /**
     * Metodo que construye insert para almacenar las facturas a indemnizar
     *
     * @param query := nombre de la consulta que se va ejecutar
     * @param LoteEndoso
     * @param lineaNegocio
     * @param UnidadNegocio
     * @param cartera_en
     * @param endosar_a
     * @param fechaCorte
     * @param checkEstadoSaldo
     * @param EstadoEndoso
     * @param objects :=Objeto json con los datos de la factura
     * @param user := beans con usuario de session.
     * @return
     */
    public String getInsertDataGenericJson(String query, String LoteEndoso, String lineaNegocio, String UnidadNegocio, String cartera_en, String endosar_a, String checkEstadoSaldo, String EstadoEndoso, JsonObject objects, Usuario user);

    /**
     * Metodo que construye insert para almacenar las facturas a indemnizar
     *
     * @param query := nombre de la consulta que se va ejecutar
     * @param objects :=Objeto json con los datos de la factura
     * @param user := beans con usuario de session.
     * @return
     */
    public String getUpdateDataGenericJson(String query, JsonObject objects, Usuario user);

    /**
     * Creado por Harold Cuello. Metodo que muestra la cartera de acuerdo a la fiducia
     *
     * @param user
     * @return
     */
    public String ValidarOneValue(Usuario user);

    /**
     * Metodo para la creacion de los documentos contables.
     *
     * @param user
     * @return
     */
    public String crearComprobanteDiarioEndoso(Usuario user);

    /**
     * Metodo para la creacion de los documentos contables.
     *
     * @param user
     * @return
     */
    public String crearComprobanteDiario(Usuario user);

    /**
     * Metodo que guarda las facturas desistidas.
     *
     * @param query
     * @param objects
     * @param user
     * @return
     */
    public String getInsertFacturaXdesistir(String query, JsonObject objects, Usuario user);

    /**
     * Metodo para crear comprobante diario para el desistimiento de facturas.
     *
     * @param user
     * @return
     */
    public String crearComprobanteDiarioDesistimiento(Usuario user);

    /**
     * M�todo para cargar las unidades de negocios activas
     *
     * @return JSON con las unidades de negocio
     */
    public String cargarTipoDiferidos();

    /**
     * M�todo para buscar la informaci�n de un diferido
     *
     * @param codigo c�digo del negocio
     * @return JSON con las cuotas del diferido
     */
    public String buscarDiferidos(String codigo, String tipo);

    /**
     * M�todo para adelantar las cuotas de un diferido
     *
     * @param codigo c�digo del negocio
     * @param diferidos n�mero de documentos de los diferidos
     * @return Mensaje de estado de la transacci�n
     */
    public String adelantarDiferidos(String codigo, String[] diferidos, Usuario usuario);

    /**
     * M�todo para anular las cuotas de un diferido
     *
     * @param codigo c�digo del negocio
     * @param diferidos n�mero de documentos de los diferidos
     * @return Mensaje de estado de la transacci�n
     */
    public String anularDiferidos(String codigo, String[] diferidos, Usuario usuario);

    /**
     * M�todo para buscar la informaci�n de un ingreso
     *
     * @param idIngreso n�mero del ingreso a buscar
     * @return JSON con la informaci�n del ingreso
     */
    public String buscarIngreso(String idIngreso);
    
    /**
     * M�todo para anular un ingreso
     *
     * @param idIngreso n�mero del ingreso a buscar
     * @param usuario nombre de usuario en sesi�n
     * @return JSON con la informaci�n del ingreso
     */
    public String anularIngreso(String idIngreso, String usuario);
    
    /**
     * Leer el archivo para la distribuci�n de la n�mina
     * @param is stream de datos del archivo
     * @return true si genera el archivo correctamente
     * @throws IOException
     * @throws SQLException 
     */
    public boolean leerArchivoEndosoEdu(InputStream is) throws IOException, SQLException;   
}
