/******************************************************************
 * Nombre ......................ConceptoPagoDAO.java
 * Descripci�n..................Conceptos de pago
 * Autor........................Armando Oviedo
 * Fecha........................10/10/2005
 * Versi�n......................1.0
 * Coyright.....................Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.DAOS.MainDAO;

public class ConceptoPagoDAO extends MainDAO {

        /** Creates a new instance of ConceptoPagoDAO */
    public ConceptoPagoDAO() {
        super ("ConceptoPagoDAO.xml");//JJCastro fase2
    }
    public ConceptoPagoDAO(String dataBaseName) {
        super ("ConceptoPagoDAO.xml", dataBaseName);//JJCastro fase2
    }
    
    private ConceptoPago cp;
    private List lista;
    private List listatmp;
    
    /************   SQL Queries     *************/
     private String SQL_INSERT           =   "INSERT INTO " +
    "   fin.concepto_pago (descripcion, controlparam, codpadre, orden, nivel, idfolder, " +
    "   referencia1, referencia2, referencia3, referencia4, dstrct, creation_date, creation_user, base, codigo) " +
    "VALUES " +
    "   (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
    private String SQL_UPDATE           =   "UPDATE " +
    "   fin.concepto_pago " +
    "SET " +
    "   descripcion = ?, codpadre = ?,  orden = ?, nivel = ?, idfolder = ?, " +
    "   referencia1 = ?, referencia2 = ?, referencia3 = ?, referencia4 = ?, last_update = ?, user_update = ?, base = ? " +
    "WHERE  " +
    "   codigo = ?";
    private String SQL_DELETE           =   "DELETE FROM fin.concepto_pago WHERE codigo = ?";
    private String SQL_NEXT             =   "SELECT MAX(substr(codigo, length(codigo)-1, length(codigo)))::numeric + 1  FROM fin.concepto_pago WHERE codpadre = ? ";
    private String SQL_SHOW             =   "SELECT " +
    "   A.codigo, A.nivel, A.idfolder, A.descripcion, A.codpadre, B.descripcion AS nompadre, A.controlparam, " +
    "   A.referencia1, A.referencia2 " +
    "FROM " +
    "   (   SELECT " +
    "           CP.codigo, CP.nivel, CP.idfolder, CP.descripcion, CP.codpadre, CP.controlparam, " +
    "           CP.referencia1, CP.referencia2 " +
    "       FROM " +
    "       fin.concepto_pago CP" +
    "   )A "+
    "LEFT JOIN " +
    "   fin.concepto_pago B " +
    "ON" +
    "   (B.codigo = A.codpadre) " +
    "WHERE                         " +
    "    A.codigo<> '0'            " +
    "ORDER BY                      " +
    "   A.nivel, A.descripcion  " ;
    
    
     private String MOSTRAR_CONTENIDO    =   "SELECT                        " +
    "   codpadre, codigo, descripcion, controlparam, nivel, idfolder, referencia1, referencia2, referencia3, referencia4 " +
    "FROM                          " +
    "   fin.concepto_pago              " +
    "WHERE                         " +
    "   codpadre = ? AND codigo <> '0' " +
    "ORDER BY                      " +
    "   codigo                     ";
    private String SQL_PADRENAME         =  "SELECT descripcion FROM fin.concepto_pago WHERE codigo=?";
    private String SQL_BUSCARHIJOS       =  "SELECT * FROM fin.concepto_pago WHERE codpadre = ? AND idfolder='Y' ORDER BY codigo";
    private String SQL_GETCONCEPTO       =  "SELECT * FROM fin.concepto_pago WHERE codigo=?";
    private String SQL_BUSCARHIJOS2      =  "SELECT * FROM fin.concepto_pago WHERE codpadre = ?";
    private String SQL_BUSCAR_HIJOS      =  "SELECT * FROM fin.concepto_pago WHERE codpadre = ? AND idfolder='Y' ORDER BY codigo";
    private String SQL_BUSCARCODIGO      =  "SELECT MAX(codigo) FROM fin.concepto_pago";
    private String SQL_INSERTDEFAULT     =  "INSERT INTO fin.concepto_pago(codigo, descripcion, codpadre, orden, nivel, idfolder) VALUES(?,?,?,?,?,?)";
    
    
    private static String SQL_NOMBREXCODIGO = "select codigo from fin.concepto_pago where upper(descripcion)=upper(?)";
    private static String SQL_HIJOS_2 = "select codigo,nivel,idfolder,descripcion,codpadre,descripcion,descripcion as nompadre,controlparam,referencia1, referencia2 from fin.concepto_pago where codigo=?";
    
   
    /**
     * Getter for property lista.
     * @return Value of property lista.
     */
    public java.util.List getLista() {
        return lista;
    }
    
    /**
     * Setter for property lista.
     * @param lista New value of property lista.
     */
    public void setLista(java.util.List lista) {
        this.lista = lista;
    }
    
    
    /**
     * M�todo que inicialisa el Metodo recursivo para generar los padres de un nodo en conceptos de pago
     * @autor.......David Lamadrid
     * @throws......SQLException
     * @version.....1.0.
     * @return......List lista
     */
    public void inicialisar(String codigo){
        this.lista = new LinkedList();
        listarhijos2(codigo);
    }
    
    /**
     * Metodo recursivo que genera los padres de un nodo en la tabla conceptos de pago
     * @autor.......David Lamadrid
     * @throws......SQLException
     * @version.....1.0.
     * @return......List lista
     */
    public void listarhijos2(String codigo){
        try{
            ConceptoPago tmp=listarHijosPorCodigo2(codigo);
            if(tmp!=null){
                this.lista.add(tmp);
                if(!tmp.getCodpadre().equals("-1")){
                    listarhijos2(tmp.getCodpadre());
                }
            }
        }
        catch( SQLException ex){
            
        }
    }
    
    /**
     * Metodo que retorna u Objeto tipo ConceptoPago ,dado el codigo del registro
     * @autor.......David Lamadrid
     * @throws......SQLException
     * @version.....1.0.
     * @return......ConceptoPago
     */
    public ConceptoPago listarHijosPorCodigo2(String codigo) throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        ConceptoPago tmp = new ConceptoPago();
        String query = "SQL_HIJOS_2";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, codigo);
                rs = ps.executeQuery();
                while (rs.next()) {
                    tmp = ConceptoPago.load2(rs);
                }


            }
        }catch(SQLException e){
            throw new SQLException("Error en MostrarContenido ConceptoPagoDAO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return tmp;
    }
    
    
    /**
     * Metodo que retorna un String con el codigo de un registro dada la descripcion
     * @autor.......David Lamadrid
     * @throws......SQLException
     * @version.....1.0.
     * @return......ConceptoPago
     */
    public String codigoPorNombre(String nombre) throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String codigo = "";
        String query = "SQL_NOMBREXCODIGO";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, nombre);
                rs = ps.executeQuery();
                while (rs.next()) {
                    codigo = rs.getString("codigo");
                }


            }
        }catch(SQLException e){
            throw new SQLException("Error en MostrarContenido ConceptoPagoDAO " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return codigo;
    }
    
    

    
    /**
     * M�todo que setea un concepto de pago
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @param.......ConceptoPago cp
     **/
    public void setCP(ConceptoPago cp){
        this.cp = cp;
    }
    
    /**
     * M�todo que setea un concepto de pago por default
     * @autor.......Armando Oviedo
     * @version.....1.0.
     **/
    public void insertDefault() throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_GETCONCEPTO";//JJCastro fase2
        try{
            int cent = 0;
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setInt( 1 , 0 );
            rs = ps.executeQuery();
            if(rs.next()){
                cent++;
            }
            if(cent==0){
                ps = con.prepareStatement(this.obtenerSQL("SQL_INSERTDEFAULT"));//JJCastro fase2
                ps.setInt(1, 0);
                ps.setString(2, "menu");
                ps.setString(3, "-1");
                ps.setInt(4, 0);
                ps.setInt(5, 0);
                ps.setString(6, "Y");
                ps.executeUpdate();
            }
        }}catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * M�todo utilizado para insertar un nuevo concepto
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     */
    public void insert() throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        String query = "SQL_INSERT";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1 , cp.getDescripcion());
            ps.setString(2 , cp.getControlparam());
            ps.setString(3 , cp.getCodpadre());
            ps.setInt(4 , cp.getOrden());
            ps.setInt(5 , cp.getNivel());
            ps.setString(6 , cp.getId_folder());
            ps.setString(7 , cp.getReferencia1());
            ps.setString(8 , cp.getReferencia2());
            ps.setString(9 , cp.getReferencia3()    );
            ps.setString(10 , cp.getReferencia4()    );
            ps.setString(11 , cp.getDstrct());
            ps.setString(12 , cp.getCreation_date());
            ps.setString(13 , cp.getCreation_user() );
            ps.setString(14 , cp.getBase());
            ps.setInt(15, Integer.parseInt(cp.getCodigo()));
            ps.executeUpdate();
        }}
        catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("Error en Insert ConceptoPagoDAO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * M�todo utilizado para actualizar un concepto de pago
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     */
    public void update() throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        String query = "SQL_UPDATE";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1 , cp.getDescripcion());
            ps.setString(2 , cp.getCodpadre());
            ps.setInt(3 , cp.getOrden());
            ps.setInt(4 , cp.getNivel());
            ps.setString(5 , cp.getId_folder());
            ps.setString(6 , cp.getReferencia1());
            ps.setString(7 , cp.getReferencia2());
            ps.setString(8 , cp.getReferencia3());
            ps.setString(9 , cp.getReferencia4());
            ps.setString(10 , cp.getLast_update());
            ps.setString(11 , cp.getUser_update());
            ps.setString(12 , cp.getBase());
            ps.setString(13, cp.getCodigo());
            ps.executeUpdate();
        }}
        catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("Error en Update ConceptoPagoDAO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * M�todo utilizado para borrar un concepto de pago
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     * @param.......String codigo
     */
    public void delete(String codigo) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        String query = "SQL_DELETE";//JJCastro fase2
        try{
            listatmp = new LinkedList();
            ConceptoPago tmp = getConcepto(codigo);
            listatmp.add(tmp);
            buscarHijos2(con, tmp.getCodigo(), tmp.getNivel());
            ////System.out.println("tama�o: "+listatmp.size());
            for(int i=0;i<listatmp.size();i++){
                ConceptoPago ctmp = (ConceptoPago)(listatmp.get(i));
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, ctmp.getCodigo());
                ps.executeUpdate();
            }
            listatmp = new LinkedList();
        }
        catch(Exception e){
            throw new Exception("Error en Delete ConceptoPagoDAO " + e.getMessage());
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * M�todo utilizado para retornar la estructura
     * del menu en una lista
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     * @return......List lista
     */
    public List mostrarContenido() throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        List lista = new LinkedList();
        String query = "SQL_SHOW";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = ps.executeQuery();
            while (rs.next()) {
                ConceptoPago tmp = ConceptoPago.load2(rs);
                ////System.out.println("codigo padre de lista: "+tmp.getCodpadre());
                lista.add(tmp);
            }
        }}
        catch(SQLException e){
            throw new SQLException("Error en MostrarContenido ConceptoPagoDAO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    
    /**
     * M�todo utilizado para retornar el siguiente id
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     * @param.......String codpadre
     * @return......String nextId
     */
    public String nextId(String codpadre) throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String nextId = "";
        int id = 1;
        String query = "SQL_NEXT";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, codpadre);
                rs = ps.executeQuery();
                while (rs.next()) {
                    id = rs.getInt(1);
                    break;
                }
                nextId = (codpadre.equalsIgnoreCase("0") ? "" : codpadre) + (id < 10 ? "0" : "") + id;
            }
        }
        catch(SQLException e){
            throw new SQLException("Error en nextId ConceptoPagoDAO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return nextId;
    }
    
    /**
     * M�todo utilizado para obtener el nombre del padre
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     * @param.......String codpadre
     * @return......String nompadre
     */
    public String getPadreName(String codpadre) throws SQLException{
        Connection con = null;//JJCastro fase2
        String nompadre="";
        Statement st = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        String query = "SQL_PADRENAME";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, codpadre);
            rs = ps.executeQuery();
            while(rs.next()){
                nompadre = rs.getString("descripcion");
            }
        }}
        catch(SQLException ex){
            throw new SQLException("Error en getPadreName ConceptoPagoDAO " + ex.getMessage() + " " + ex.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return nompadre;
    }
    
    /**
     * M�todo utilizado para buscar los hijos de un padre
     * @autor.......Armando Oviedo
     * @throws......Exception
     * @version.....1.0.
     * @param.......String codpadre
     * @return......String nompadre
     */
    public void buscarHijos(Connection conn, String id, String oculto)throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        boolean           liberar     = false;
        String query = "SQL_BUSCAR_HIJOS";//JJCastro fase2
        if (lista == null) lista = new LinkedList();

        try{
            st = conn.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1 , id);
            rs = st.executeQuery();
            while(rs.next()){
                ConceptoPago tmp = new ConceptoPago();
                tmp = tmp.load(rs);
                if(!tmp.getCodigo().equalsIgnoreCase(oculto)){
                    lista.add(tmp);
                    buscarHijos(conn, tmp.getCodigo(),oculto);
                }
            }
        }
        catch(Exception e){
            throw new Exception("Error en BuscarHijos ConceptoPagoDAO " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * M�todo utilizado para obtener la lista de folders
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @return......List lista
     */
    public List getListaFolders(){
        return this.lista;
    }
    
    /**
     * M�todo utilizado para reiniciar la lista de folders
     * @autor.......Armando Oviedo
     * @version.....1.0.
     */
    public void reiniciarListaFolders(){
        this.lista = new LinkedList();
    }
    
    /**
     * M�todo utilizado para obtener el objeto concepto pago
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @param.......String codpadre
     * @return......ConceptoPago tmp
     */
    public ConceptoPago getConcepto(String codigo) throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        ConceptoPago tmp = null;
        String query = "SQL_GETCONCEPTO";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, codigo);
            rs = ps.executeQuery();
            while(rs.next()){
                tmp = new ConceptoPago();
                tmp = tmp.load(rs);
            }
        }}
        catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return tmp;
    }
    
    /**
     * M�todo utilizado para buscar hijos
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @throws......Exception
     * @param.......String codpadre
     */
    public void buscarHijos2(Connection conn, String id, int nivel)throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String query = "SQL_BUSCARHIJOS2";//JJCastro fase2
        if (listatmp == null) listatmp = new LinkedList();

        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1 , id);
            rs = st.executeQuery();
            while(rs.next()){
                ConceptoPago tmp = new ConceptoPago();
                tmp = tmp.load(rs);
                tmp.setNivel(nivel);
                listatmp.add(tmp);
                if(tmp.getId_folder().equalsIgnoreCase("Y")) buscarHijos2(conn, tmp.getCodigo(),nivel+1);
            }
        }
        }catch(Exception e){
            throw new Exception("Error en BuscarHijos ConceptoPagoDAO " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * M�todo utilizado para actualizar la estructura del �rbol
     * @autor.......Armando Oviedo
     * @throws......Exception
     * @version.....1.0.
     * @param.......String codpadre, int nivelpadre, String codRama
     */
    public void updateTreeStructure(int nivelPadre, String codpadre, String codRama) throws Exception{
        try{
            ConceptoPago tmp = new ConceptoPago();
            tmp = getConcepto(codRama);
            tmp.setCodpadre(codpadre);
            tmp.setNivel(nivelPadre+1);
            ////System.out.println("antes de: "+nivelPadre);
            if(tmp.getId_folder().equalsIgnoreCase("Y")){
                nivelPadre = nivelPadre+2;
            }
            else{
                nivelPadre++;
            }
            ////System.out.println("Despu�s: "+nivelPadre);
            buscarHijos2(null,codRama,nivelPadre);
            ////System.out.println("Nivel padre: "+nivelPadre);
            setCP(tmp);
            update();
            for(int i=0;i<listatmp.size();i++){
                tmp = (ConceptoPago)(listatmp.get(i));
                setCP(tmp);
                update();
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    /**
     * M�todo utilizado para obtener el codigo del concepto
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     */
    public int getCodigoSerial() throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        int codigotemp = 1;
        String query = "SQL_BUSCARCODIGO";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = ps.executeQuery();
            if(rs.next()){
                codigotemp = rs.getInt(1)+1;
            }
        }}catch(SQLException ex){
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return codigotemp;
    }
    
      /**
     * M�todo BuscarConcepto  
     * @descripcion..Metodo para buscar el concepto de pago
     * @autor........Ivan Gomez
     * @throws.......SQLException
     * @version......1.0.
     * @return.......List lista
     */
    public void BuscarConcepto(String concepto) throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        this.lista = new LinkedList();
        String query = "SQL_BUSCAR_CONCEPTO";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, concepto);

                rs = ps.executeQuery();
                if (rs.next()) {
                    ConceptoPago tmp = new ConceptoPago();
                    tmp.setDescripcion(rs.getString(1));
                    tmp.setCodigo(rs.getString("referencia2") + "" + rs.getString("referencia1"));
                    tmp.setReferencia3(rs.getString("referencia3"));
                    tmp.setReferencia4(rs.getString("referencia4"));
                    lista.add(tmp);
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("Error en MostrarContenido ConceptoPagoDAO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
       
    }
     /**
     * M�todo utilizado para retornar todos los hijos de un padre
     * @autor.......Armando Oviedo --> modificado por David Pi�a Lopez
     * @throws......Exception
     * @version.....1.0.
     * @param.......String id
     * @return......List lista
     */
    public List mostrarContenido(String id)throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List listax = new LinkedList();
        String query = "MOSTRAR_CONTENIDO";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            stmt = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            while (rs.next()) {
                ConceptoPago item = new ConceptoPago();
                item.setCodpadre        ( rs.getString(1) );
                item.setCodigo          ( rs.getString(2) );
                item.setDescripcion     ( rs.getString(3) );
                item.setControlparam    ( rs.getString(4) );
                item.setNivel           ( rs.getInt   (5) );
                item.setId_folder       ( rs.getString(6) );
                item.setReferencia1     ( rs.getString(7) );
                item.setReferencia2     ( rs.getString(8) );
                item.setReferencia3     ( rs.getString(9) );
                item.setReferencia4     ( rs.getString(10) );
                listax.add              ( item            );
            }
        }}
        catch(SQLException e){
            throw new SQLException("Error en MostrarContenido [MenuDAO] " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (stmt  != null){ try{ stmt.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listax;
    }
    
      /**
     * M�todo BuscarReferencia3  
     * @descripcion..Metodo para buscar la referencia 3 
     * @autor........Ivan Gomez
     * @throws.......SQLException
     * @version......1.0.
     * @return.......List lista
     */
    public void BuscarReferencia3(String concepto,String cre) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        this.lista = new LinkedList();
        Connection con = null;
        try{
            String sql = this.obtenerSQL("SQL_BUSCAR_REFERENCIA3");
            sql = sql.replaceAll("#CRE#", "'"+cre+"%'");
            con = this.conectarJNDI("SQL_BUSCAR_REFERENCIA3");//JJCastro fase2
            ps = con.prepareStatement(sql);
            ps.setString(1, concepto);
           
            rs = ps.executeQuery();
            if(rs.next()) {
                ConceptoPago tmp = new ConceptoPago(); 
                tmp.setDescripcion(rs.getString(1));
                tmp.setCodigo(rs.getString("referencia2")+""+rs.getString("referencia1"));
                tmp.setReferencia3(rs.getString("referencia3"));
                lista.add(tmp);
            }
        }
        catch(SQLException e){
            throw new SQLException("Error en MostrarContenido BuscarReferencia3 " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
       
    }
}
