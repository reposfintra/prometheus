/********************************************************************
 *      Nombre Clase.................   DocumentoAplicacionDAO.java    
 *      Descripci�n..................   DAO de la tabla tblapl_doc    
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;
import com.tsp.operation.model.DAOS.MainDAO;

public class DocumentoAplicacionDAO extends MainDAO{
        private DocumentoAplicacion apl_doc;
        private Vector apl_docs;
        
        private static final String SQL_LISTAR_APL = "select * from tblapl_doc where dstrct=? and aplicacion=? and reg_status<>'A'";
        
        private static final String SQL_INSERT = "insert into tblapl_doc(reg_status, dstrct, documento," +
                " aplicacion, last_update, user_update, creation_date, creation_user, base) " +
                "values('',?,?,?,'now()',?,'now()',?,?)";
        
        private static final String SQL_UPDATE = "update tblapl_doc set reg_status=?, last_update='now()', user_update=?" +
                " where dstrct=? and documento=? and aplicacion=?";
        
        private static final String SQL_OBTENER = "select * from tblapl_doc where dstrct=? and documento=? and aplicacion=?";
        
        /** Creates a new instance of DocumentoActividadDAO */
        public DocumentoAplicacionDAO() {
          super ("DocumentoAplicacionDAO.xml");
        }
        
        /**
         * Inserta un nuevo registro.
         * @autor Tito Andr�s Maturana
         * @throws SQLException
         * @version 1.0
         */
        public void ingresar() throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;
                String query = "SQL_INSERT";
                try{
                    con = this.conectarJNDI(query);
                    if (con != null) {
                                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                                st.setString(1, apl_doc.getDistrito());
                                st.setString(2, apl_doc.getDocumento() );
                                st.setString(3, apl_doc.getAplicacion());
                                st.setString(4, apl_doc.getUsuario_creacion());
                                st.setString(5, apl_doc.getUsuario_creacion());
                                st.setString(6, apl_doc.getBase());
                                                               
                                st.executeUpdate();                                
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL INGRESAR LA RELACION APLICACION-DOCUMENTO: " + e.getMessage()+"" + e.getErrorCode());
                }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
                
        }
        
        /**
         * Actualiza un registro.
         * @autor Tito Andr�s Maturana
         * @throws SQLException
         * @version 1.0
         */
        public void update() throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;
                String query = "SQL_UPDATE";
        
                try{
                    con = this.conectarJNDI(query);
                    if (con != null) {
                                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                                st.setString(1, apl_doc.getEstado());
                                st.setString(2, apl_doc.getUsuario_modificacion());
                                st.setString(3, apl_doc.getDistrito());
                                st.setString(4, apl_doc.getDocumento());
                                st.setString(5, apl_doc.getAplicacion());
                                st.executeUpdate();
                                
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL ACTUALIZAR LA RELACION APLICACION-DOCUMENTO: " + e.getMessage()+"" + e.getErrorCode());
                }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }                
        }
        
        /**
         * Obtiene un listado a partir del c�digo de un aplicaci�n.
         * @autor Tito Andr�s Maturana
         * @param distrito Distrito.
         * @param aplicaciion C�digo de la aplicaci�n
         * @throws SQLException
         * @version 1.0
         */
        public void obtenerDocumentosAplicacion(String distrito, String aplicacion) throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;       
                this.apl_doc = null;
                this.apl_docs = new Vector();
                String query = "SQL_LISTAR_APL";
                try{
                    con = this.conectarJNDI(query);
                    if (con != null) {
                                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                                st.setString(1, distrito);
                                st.setString(2, aplicacion);
                                rs = st.executeQuery();
                                while( rs.next() ){
                                        this.apl_doc = new DocumentoAplicacion();
                                        apl_doc.Load(rs);
                                        apl_docs.add(apl_doc);
                                }
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL LISTAR LOD DOCUMENTOS POR APLICACION: " + e.getMessage()+"" + e.getErrorCode());
                }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
                
        }
        
        /**
         * Establece si existe un registro
         * @autor Tito Andr�s Maturana
         * @param distrito Distrito.
         * @param documento C�digo del documento
         * @param aplicaciion C�digo de la aplicaci�n
         * @throws SQLException
         * @version 1.0
         */
        public boolean existeAplicacionDocumento(String distrito, String documento, String aplicacion) throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;       
                boolean existe = false;
                String query = "SQL_OBTENER";
                try{
                    con = this.conectarJNDI(query);
                    if (con != null) {
                                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                                st.setString(1, distrito);
                                st.setString(2, documento);
                                st.setString(3, aplicacion);
                                                               
                                rs = st.executeQuery();
                                
                                while( rs.next() ){
                                        existe = true;
                                }
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL OBTENER LA APLICACION-DOCUMENTO: " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }                
                return existe;
        }
        
        /**
         * Obtiene un registro
         * @autor Tito Andr�s Maturana
         * @param distrito Distrito.
         * @param documento C�digo del documento
         * @param aplicaciion C�digo de la aplicaci�n
         * @throws SQLException
         * @version 1.0
         */
        public void obtenerAplicacionDocumento(String distrito, String documento, String aplicacion) throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;       
                this.apl_doc = null;
                String query = "SQL_OBTENER";
                try{
                    con = this.conectarJNDI(query);
                    if (con != null) {
                                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                                st.setString(1, distrito);
                                st.setString(2, documento);
                                st.setString(3, aplicacion);
                                rs = st.executeQuery();
                                while( rs.next() ){
                                        this.apl_doc = new DocumentoAplicacion();
                                        this.apl_doc.Load(rs);
                                }
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL OBTENER LA APLICACION-DOCUMENTO: " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        }
        
        /**
         * Getter for property apl_doc.
         * @return Value of property apl_doc.
         */
        public com.tsp.operation.model.beans.DocumentoAplicacion getApl_doc() {
                return apl_doc;
        }
        
        /**
         * Setter for property apl_doc.
         * @param apl_doc New value of property apl_doc.
         */
        public void setApl_doc(com.tsp.operation.model.beans.DocumentoAplicacion apl_doc) {
                this.apl_doc = apl_doc;
        }
        
        /**
         * Getter for property apl_docs.
         * @return Value of property apl_docs.
         */
        public java.util.Vector getApl_docs() {
                return apl_docs;
        }
        
        /**
         * Setter for property apl_docs.
         * @param apl_docs New value of property apl_docs.
         */
        public void setapl_docs(java.util.Vector Apl_docs) {
                this.apl_docs = apl_docs;
        }
        
}
