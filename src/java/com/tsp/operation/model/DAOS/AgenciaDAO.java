package com.tsp.operation.model.DAOS;

/**
 *
 * @author  Armando
 */
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  AMENDEZ
 */
public class AgenciaDAO extends MainDAO {
    private TreeMap cbxAgencia;
    private TreeMap cbxZona;
    private Vector agencias;
    
    //henry
    //    private Vector agencias;
    private Agencia agencia;
    
    
    
    /** Creates a new instance of CarroceriaDAO */
    public AgenciaDAO() {
        super("AgenciaDAO.xml");
    }
    public AgenciaDAO(String dataBaseName) {
        super("AgenciaDAO.xml", dataBaseName);
    }
    
    public TreeMap getCbxAgencia(){
        return this.cbxAgencia;
    }
    
    public void setCbxAgencia(TreeMap agencias){
        this.cbxAgencia = agencias;
    }


  /**
   *
   * @throws SQLException
   */
    public void searchZona() throws SQLException{
        Connection con = null;
        PreparedStatement sttm = null;
        ResultSet rs = null;
        cbxZona = null;
        cbxZona = new TreeMap();
        String query = "SQL_ZONA";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            sttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = sttm.executeQuery();
            while (rs.next()) {
                cbxZona.put(rs.getString(2), rs.getString(1));
            }
            cbxZona.put("", "");
        }}
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA AGENCIA" + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (sttm  != null){ try{ sttm.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Getter for property cbxZona.
     * @return Value of property cbxZona.
     */
    public java.util.TreeMap getCbxZona() {
        return cbxZona;
    }
    
    /**
     * Setter for property cbxZona.
     * @param cbxZona New value of property cbxZona.
     */
    public void setCbxZona(java.util.TreeMap cbxZona) {
        this.cbxZona = cbxZona;
    }
    
    public void searchAgencia() throws SQLException{
        Connection con = null;
        PreparedStatement sttm = null;
        ResultSet rs = null;
        cbxAgencia = null;
        cbxAgencia = new TreeMap();
        String query = "SQL_AGENCIA";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            sttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = sttm.executeQuery();
            while (rs.next()) {
                cbxAgencia.put(rs.getString(2), rs.getString(1));
            }
            cbxAgencia.put("", "");
            }}
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA AGENCIA" + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (sttm  != null){ try{ sttm.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    //ALEJANDRO
    private boolean guardarEnTreeMap = false;
    public void searchAgenciaDistrito(String distrito)throws SQLException{
        guardarEnTreeMap = true;
        listarPorWhere( "where dstrct=? and estado='TRUE'", new Object[] {distrito}
        , new Class[] {String.class} );
    }
    
    public Vector obtenerAgencias( String distrito ) throws SQLException {
        guardarEnTreeMap = false;
        return listarPorWhere( "where dstrct=? and estado='TRUE'", new Object[] {distrito}
        , new Class[] {String.class} );
    }
    
    public Agencia obtenerAgencia( String idAgencia ) throws SQLException {
        Vector datos = this.listarPorWhere("where id_agencia=?",new Object[]{idAgencia},new Class[] {String.class});
        if (datos.isEmpty()){
            return null;
        }
        else {
            return (Agencia) datos.firstElement();
        }
    }
    
    public void listarOrdenadas()throws SQLException{
        guardarEnTreeMap = true;
        listarPorWhere( "where id_agencia != '' and id_agencia != ' ' and nombre != '' and nombre != ' ' order by nombre",null
        , null );
    }
    
    //
    
      private Vector listarPorWhere( String where, Object[] parametros, Class[] tiposParametros ) throws  SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Vector datos = new Vector();


        if ( guardarEnTreeMap ){
            cbxAgencia = new TreeMap();
            cbxAgencia.put("TODAS","");
        }
        try {

            con = this.conectarJNDI("SQL_OBTENER_AGENCIAS");//JJCastro fase2
           if(con!=null){
                    String sql = this.obtenerSQL("SQL_OBTENER_AGENCIAS");
                    st = con.prepareStatement( ( sql  + " " + where ).trim() );
                    if ( parametros != null ) {
                        for ( int i = 0; i < tiposParametros.length; i++ ) {
                            if ( tiposParametros[0] == String.class ) {
                                st.setString( i + 1, parametros[i].toString() );
                            }
                            else if ( tiposParametros[0] == java.sql.Date.class ) {
                                st.setDate( i + 1, ( java.sql.Date ) parametros[i] );
                            }
                            else {
                                st.setInt( i + 1,
                                ( ( Integer ) parametros[i] ).intValue() );
                            }
                        }
                    }
                    rs = st.executeQuery();

                    while (rs.next()) {
                   Agencia ag = crearAgencia(rs);
                   if (guardarEnTreeMap) {//agasoc, nomciu
                       cbxAgencia.put(ag.getNombre(), ag.getId_agencia());
                   } else {
                       datos.addElement(ag);
                   }
               }
               
            
            
        }}catch ( Exception e ) {
            throw new SQLException(
            "ERROR DURANTE EL PROCESO DE LISTAR LOS RECURSOS DISPONIBLES " +
            e.getMessage() );
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return datos;
    }
    
    //JUANM
/**
 *
 * @param despachador
 * @return
 * @throws SQLException
 */
    public Vector  SEARCH_AGENCIAS_DESPACHADOR( String despachador ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        Vector      datos    = new Vector();
        String query = "SQL_AGENCIAS_CONDUCTOR";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, despachador);
                rs = st.executeQuery();
                while (rs.next()) {
                    datos.addElement(rs.getString(1));
                }
            }
        } catch (Exception e) {
            throw new SQLException("Error en rutina SEARCH_AGENCIA_CONDUCTOR [AGENCIADAO]... \n"+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return datos;
    }
    
    /**
     * crearAgencia
     *
     * @param rs ResultSet
     * @return Object
     */
    private Agencia crearAgencia( ResultSet rs ) throws SQLException {
        Agencia ag = new Agencia();
        ag.setDstrct( rs.getString( "dstrct" ) );
        ag.setEstado( rs.getString( "estado" ) );
        ag.setFecha_cambio_estado( rs.getString( "fecha_cambio_estado" ) );
        ag.setId_agencia( rs.getString( "id_agencia" ) );
        ag.setId_mims( rs.getString( "id_mims" ) );
        ag.setNombre( rs.getString( "nombre" ) );
        return ag;
    }
    
    /**
     * Getter for property agencias.
     * @return Value of property agencias.
     */
    public java.util.Vector getAgencias() {
        return agencias;
    }
    
    /**
     * Setter for property agencias.
     * @param agencias New value of property agencias.
     */
    public void setAgencias(java.util.Vector agencias) {
        this.agencias = agencias;
    }
    
    //HENRY
/**
 *
 * @throws SQLException
 */
    public void obtenerAgencias() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        agencias = new Vector();
        String query = "SQL_LISTAR";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            while (rs.next()) {
                agencia = new Agencia();
                agencia.setId_agencia(rs.getString("id_agencia"));
                agencia.setNombre(rs.getString("nombre"));
                agencias.addElement(agencia);
            }
            }}
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR AGENCIA" + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    public Vector getVectorAgencias(){
        return agencias;
    }
    
    
    /////juanm 061005
    /**
     *
     * @return
     * @throws SQLException
     */
    public String Agencias() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String agencias = "";
        String query = "SQL_LISTAR";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            while (rs.next()) {
                agencias += rs.getString("id_agencia")+":"+rs.getString("nombre")+";";
            }
            agencias = agencias.substring(0, agencias.length() - 1);
            
        }}
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR AGENCIA" + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return agencias;
    }
    
    //Jose 27/10/2005
 /***
  *
  * @param agencia
  * @return
  * @throws SQLException
  */
    public String agenciasRemesas(String agencia) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String agencias = "";
        String query = "SQL_LISTAR_AGENCIA_REMESA";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, agencia);
            rs = st.executeQuery();
            
            if (rs.next()){
                agencias = rs.getString( "agasoc");
            }
            }}
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR AGENCIA DE REMESA " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return agencias;
    }


/**
 *
 * @param agencia
 * @return
 * @throws SQLException
 */
    public String agenciasUsuarios(String agencia) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String agencias = "";
        String query = "SQL_LISTAR_AGENCIA_USUARIO";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, agencia);
            rs = st.executeQuery();
            if (rs.next()){
                agencias = rs.getString( "agasoc");
            }
            }}
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR AGENCIA DE USUARIO " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return agencias;
    }
    

/**
 * 
 * @param agencia
 * @return
 * @throws SQLException
 */
    public String agenciasPlanillas(String agencia) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String agencias = "";
        String query = "SQL_LISTAR_AGENCIA_PLANILLA";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, agencia);
            rs = st.executeQuery();
            
            if (rs.next()){
                agencias = rs.getString( "agasoc");
            }
            }}
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR AGENCIA DE LA PLANILLA " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return agencias;
    }
     /**
     * Carga una instancia de la clase <code>TreeMap</code> con el c�digo y el nombre de las agencias.
     * @autor Ing. Tito Andr�s Maturana
     * @throws SQLException si ocurre un error en la conexi�n con la Base de Datos.
     * @version 1.0
     */
    public void loadAgencias() throws SQLException{
        Connection con = null;
        PreparedStatement sttm = null;
        ResultSet rs = null;
        cbxAgencia = null;
        cbxAgencia = new TreeMap();
        String query = "SQL_LISTAR";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            sttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = sttm.executeQuery();
            
            while (rs.next()) {
                cbxAgencia.put(rs.getString(2), rs.getString(1));
            }
        }}catch(SQLException e){
            throw new SQLException("ERROR EN LOAD AGENCIAS" + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (sttm  != null){ try{ sttm.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
     /**
     * Metodo que retorna un vector de agencias de las remesas realizadas en una fecha dada.
     * @autor Ing. Karen Reales.
     * @param Codigo del cliente
     * @param Fecha inicial
     * @param Fecha final
     * @return Vector con agencias
     * @throws SQLException si ocurre un error en la conexi�n con la Base de Datos.
     * @version 1.0
     */
    public Vector  search_agencias_cliente( String cliente, String fecini, String fecfin ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        Vector      datos    = new Vector();
        String query = "SQL_LISTAR_AGENCIA_CLIENTE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, fecini );
            st.setString(2, fecfin );
            st.setString(3, cliente );
            //System.out.println("Query "+st);
            rs = st.executeQuery();
            while(rs.next()){
                datos.addElement( rs.getString("agcrem") );
            }
        }}
        catch(Exception e) {
            throw new SQLException("Error en rutina search_agencias_cliente [AGENCIADAO]... \n"+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return datos;
    }


 /**
  *
  * @throws SQLException
  */
    public void listar() throws SQLException{
        Connection con = null;
        PreparedStatement sttm = null;
        ResultSet rs = null;
        cbxAgencia = null;
        cbxAgencia = new TreeMap();
        String query = "SQL_LISTAR";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
             sttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = sttm.executeQuery();
            cbxAgencia.put("Seleccione Alguna", "NADA");
            while (rs.next()) {
                cbxAgencia.put(rs.getString(1)+"-"+rs.getString(2), rs.getString(1));
            }
        }}
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR AGENCIA" + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (sttm  != null){ try{ sttm.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    
    /**************************************************************/
    //FUNCION PARA MOSTRAR EL LISTADO DE LA AGENCIA DE FACTURACIUON
    //CREADO POR FILY STEVEN FERNANDEZ
    /**************************************************************/
    public List listarAgenciasXFacturacion() throws Exception{
        Connection con = null;
        PreparedStatement sttm = null;
        ResultSet rs = null;
        cbxAgencia = null;
        cbxAgencia = new TreeMap();
        List lista           = new LinkedList();
        String query = "SQL_LISTAR_X_FACTURACION";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
             sttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
             rs=sttm.executeQuery();
             //System.out.println("agencia de fac  "+sttm.toString());
            if(rs!=null){
                while(rs.next()){
                    Ciudad c = new Ciudad();
                    c.setCodCiu(rs.getString("id_agencia"));
                    c.setNomCiu(rs.getString("nombre"));
                    lista.add(c);
                }
            }
        }}
         catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR AGENCIA" + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (sttm  != null){ try{ sttm.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
}
