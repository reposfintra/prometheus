/*
 * Nombre        ConsultasGeneralesDeReportesDAO.java
 * Descripci�n   Clase para el acceso a los datos generales de los reportes configurables de SOT
 * Autor         Alejandro Payares
 * Fecha         14 de septiembre de 2005, 09:12 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.DAOS;

import java.sql.*;
import java.util.Hashtable;
import java.util.Vector;
import org.apache.log4j.Logger;

/**
 * Clase para el acceso a los datos generales de los reportes configurables de SOT
 * @author  Alejandro
 */
public class ConsultasGeneralesDeReportesDAO extends MainDAO {
    
    private Logger logger = Logger.getLogger(ConsultasGeneralesDeReportesDAO.class);
    private Hashtable detalleCampos;
    private Vector vectorCampos;
    
    /**
     * Creates a new instance of ConsultasGeneralesDeReportesDAO
     */
    public ConsultasGeneralesDeReportesDAO() {
        super("ConsultasGeneralesDeReportesDAO.xml");
    }
    public ConsultasGeneralesDeReportesDAO(String dataBaseName) {
        super("ConsultasGeneralesDeReportesDAO.xml", dataBaseName);
    }
    
    /**
     * busca los campos de un reporte para un cliente espec�fico o para el usuario dado.
     * si ninguna configuraci�n de reporte es encontrada para el cliente o usuario dado
     * es buscada entonces la lista de campos generales para el reporte dado a buscar.
     * @param codigoCliente el codigo del cliente al cual se le buscar� la configuraci�n espec�fica.
     * @param codigoReporte el c�digo del reporte a buscar.
     * @param tipocliente el tipo de cliente (C = cliente, D = destinatario)
     * @param loggedUser El usuario en sessi�n. A este usuario se le buscar� una configuraci�n de reporte
     * para el reporte dado. si no es encontrada se buscan los campos generales del reporte.
     * @throws SQLException si algun error ocurre en el acceso a los datos.
     * @return Un array de tipo String que contiene los campos encontrados para los parametros dados.
     */
    public String [] buscarCamposDeReporte(String codigoCliente,String codigoReporte, String tipocliente, String loggedUser) throws SQLException{
        String [] camposReporte = null;
        ResultSet rs         = null;
        PreparedStatement ps = null;
        try{
            // primero buscamos la configuraci�n de campos para el usuario en sessi�n
            ps = crearPreparedStatement("SQL_OBTENER_CAMPOS_REPORTE");
            ps.setString(1, loggedUser );
            ps.setString(2, codigoReporte );
            ps.setString(3, "U" );            
            rs = ps.executeQuery();
            if(rs.next()){
                String str = rs.getString("campos").trim();
                camposReporte = str.split(",");
            }
            else {
                rs.close();
                ps.close();
                // luego buscamos la configuraci�n para el cliente
                ps = crearPreparedStatement("SQL_OBTENER_CAMPOS_REPORTE");
                ps.setString(1, codigoCliente );
                ps.setString(2, codigoReporte );
                ps.setString(3, tipocliente );
                rs = ps.executeQuery();
                if(rs.next()){
                    String str = rs.getString("campos").trim();
                    camposReporte = str.split(",");
                }
                else {
                    // y por ultimo buscamos la general
                    ps = crearPreparedStatement("SQL_OBTENER_CAMPOS_DEFAULT_REPORTE");
                    ps.setString(1, codigoReporte);
                    rs = ps.executeQuery();
                    if ( rs.next() ){
                        String str = rs.getString("camposrpt");
                        camposReporte = str.split(",");
                    }
                    else {
                        camposReporte = null;
                    }
                    desconectar("SQL_OBTENER_CAMPOS_DEFAULT_REPORTE");
                }
                desconectar("SQL_OBTENER_CAMPOS_REPORTE");
            }
        }
        catch(SQLException e){
            throw new SQLException("CONSULTAS GENERALES DE REPORTES: ERROR OBTENIENDO CAMPOS DE REPORTE -> " + e.getMessage());
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        finally{
            if (ps != null) ps.close();
            desconectar("SQL_OBTENER_CAMPOS_REPORTE");
            return camposReporte;
        }
    }
    
    /**
     * Devuelve un Hashtable que contiene los campos con sus respectivos titulos. donde el
     * cada entrada al hash es una pareja llave=nombre del campo y valor=titulo del campo.
     * @param codReporte el codigo del reporte al que se le buscar�n los titulos.
     * @throws SQLException si algun error ocurre en el acceso a los datos.
     * @return un hash con los titulos de los campos del reporte.
     */
    public Hashtable obtenerTitulosDeReporte(String codReporte)throws SQLException{
        ResultSet rs         = null;
        PreparedStatement ps = null;
        Hashtable titulos = null;
        try {
            ps = crearPreparedStatement("SQL_OBTENER_TITULOS");
            ps.setString(1, codReporte);
            rs = ps.executeQuery();
            if ( rs.next() ) {
                titulos = new Hashtable();
                String [] vcampos = rs.getString("camposrpt").split(",");
                String [] vtitulos = rs.getString("nomcamposrpt").split(",");
                for( int i=0; i< vcampos.length; i++ ){
                    titulos.put(vcampos[i], vtitulos[i]);
                }
            }
        }
        catch( Exception ex ){
            logger.error("ERROR AL OBTENER TITULOS..."+ex.getMessage(),ex);
            throw new SQLException("ERROR AL OBTENER TITULOS..."+ex.getMessage());
        }
        finally {
            if ( rs != null ) {rs.close();}
            if ( ps != null ) {ps.close();}
            this.desconectar("SQL_OBTENER_TITULOS");
            return titulos;
        }
    }
    
    /**
     * Busca el detalle de los campos del reporte dado.
     * @param codReporte El codigo del reporte a buscar
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     * @autor Alejandro Payares
     */
    public void buscarDetalleDeCampos(String codReporte)throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = this.crearPreparedStatement("SQL_OBTENER_DETALLE_CAMPOS");
            ps.setString(1, codReporte);
            ResultSet rs = ps.executeQuery();
            detalleCampos = new Hashtable();
            vectorCampos = new Vector();
            while( rs.next() ){
                Hashtable fila = new Hashtable();
                fila.put("orden",rs.getString("orden"));
                fila.put("nomcampo",rs.getString("nomcampo"));
                fila.put("titulocampo",rs.getString("titulocampo"));
                fila.put("grupo",rs.getString("grupo"));
                fila.put("colorgrupo",rs.getString("colorgrupo"));
                detalleCampos.put(rs.getString("nomcampo"),fila);
                vectorCampos.addElement(fila);
            }
        }
        catch( Exception ex ){
            logger.info("ERROR AL OBTENER DETALLE DE CAMPOS DEL REPORTE "+codReporte,ex);
            throw new SQLException("ERROR AL OBTENER DETALLE DE CAMPOS DEL REPORTE "+codReporte+": "+ ex.getMessage());
        }
        finally {
            if( ps != null ){ ps.close(); }
            this.desconectar("SQL_OBTENER_DETALLE_CAMPOS");
        }
    }
    
    
    /**
     * Busca el detalle de los campos del reporte dado, pero solo trae el detalle de los campos dados.
     * @autor Alejandro Payares
     * @param campos Array con los campos que ser�n buscados
     * @param codReporte El codigo del reporte a buscar
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     */
    public void buscarDetalleDeCampos(String codReporte, String [] campos)throws SQLException {
        PreparedStatement ps = null;
        if ( campos == null || campos.length == 0 ) {
            buscarDetalleDeCampos(codReporte);
            return;
        }
        try {
            ps = this.crearPreparedStatement("SQL_OBTENER_DETALLE_CAMPOS_2");
            ps.setString(1, codReporte);
            String sql = ps.toString();
            
            StringBuffer sb = new StringBuffer();
            for( int i=0; i< campos.length; i++ ){
                sb.append("'"+campos[i]+"',");
            }
            sb = (StringBuffer) sb.deleteCharAt(sb.length()-1);
            sql = sql.replaceFirst("'campos'", sb.toString());
            
            ResultSet rs = ps.executeQuery(sql);
            detalleCampos = new Hashtable();
            vectorCampos = new Vector();
            while( rs.next() ){
                Hashtable fila = new Hashtable();
                fila.put("orden",rs.getString("orden"));
                fila.put("nomcampo",rs.getString("nomcampo"));
                fila.put("titulocampo",rs.getString("titulocampo"));
                fila.put("grupo",rs.getString("grupo"));
                fila.put("colorgrupo",rs.getString("colorgrupo"));
                detalleCampos.put(rs.getString("nomcampo"),fila);
                vectorCampos.addElement(fila);
            }
        }
        catch( Exception ex ){
            logger.info("ERROR AL OBTENER DETALLE DE CAMPOS DEL REPORTE "+codReporte,ex);
            throw new SQLException("ERROR AL OBTENER DETALLE DE CAMPOS DEL REPORTE "+codReporte+": "+ ex.getMessage());
        }
        finally {
            if( ps != null ){ ps.close(); }
            this.desconectar("SQL_OBTENER_DETALLE_CAMPOS_2");
        }
    }
    
    /**
     * Devuelve el detalle de los campos del reporte buscado por el metodo buscarDetalleDeCampos
     * @return Un Vector que contiene un Hashtable por cada fila encontrada para el reporte.
     * @autor Alejandro Payares
     */
    public Hashtable obtenerDetalleDeCampos(){
        return detalleCampos;
    }
    
    /**
     * Devuelve un vector con el detalle de todos los campos del reporte buscado por el metodo buscarDetalleDeCampos
     * @return vector con el detalle de todos los campos del reporte buscado por el metodo buscarDetalleDeCampos
     * @autor Alejandro Payares
     */    
    public Vector obtenerVectorDetalleCampos(){
        return vectorCampos;
    }
    
    /**
     * Elimina los datos encontrados por el metodo buscarDetalleDeCampos
     * @autor Alejandro Payares
     */    
    public void eliminarDetalle(){
        if ( detalleCampos != null ) {
            detalleCampos.clear();
            detalleCampos = null;
            vectorCampos.clear();
            vectorCampos = null;
        }
    }
}
