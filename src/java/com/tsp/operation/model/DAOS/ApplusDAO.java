/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.DAOS;

import java.sql.*;
import com.tsp.operation.model.beans.*;
import java.util.*;

/**
 *
 * @author Alvaro
 */
public class ApplusDAO extends MainDAO {


    /** Creates a new instance of ApplusDAO */
    public ApplusDAO() {
        super("ApplusDAO.xml");
    }
    public ApplusDAO(String dataBaseName) {
        super("ApplusDAO.xml", dataBaseName);
    }



    private List listaPrefacturas;
    private Contratista contratista;
    private Comision comision;
    private ImpuestoContrato impuestoContrato;
    private List listaPrefacturaResumen;
    private List listaDetallePrefactura;
    private List listaOfertaEca;
    private List listaOfertaEcaDetalle;



    /**
     * Lista las acciones de un contratista sin prefacturar
     * @id_contratista Identificacion del contratista, codigo interno

     */


    public void buscaPrefactura(String id_contratista)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_PREFACTURAR";
        listaPrefacturas = null;

        try {


            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, id_contratista );

            rs = st.executeQuery();

            listaPrefacturas =  new LinkedList();

            while (rs.next()){
                listaPrefacturas.add(Prefactura.load(rs));
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS A PREFACTURAR. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
    }


    /**
     * Returna una Lista las acciones de un contratista sin prefacturar
     */


    public List getPrefactura() {

        return listaPrefacturas;
    }



    /**
     * Localiza informacion de un contratista
     * @param id_contratista Codigo del contratista

     */


    public void buscaContratista(String id_contratista)throws SQLException{

        PreparedStatement st  = null;
        Connection        con = null;
        ResultSet         rs  = null;

        String            query       = "SQL_CONTRATISTA";
        contratista                   = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, id_contratista );

            rs = st.executeQuery();

            if(rs.next()){
                contratista = new Contratista();
                contratista = (Contratista.load(rs));
            }


        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL NOMBRE DEL CONTRATISTA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
    }



    /**
     * Retorna un contratista
     */


    public Contratista getContratista() {

        return contratista;
    }


    /**
     * Retorna el valor de la comision
     */


    public Comision getComision() {

        return comision;
    }




    /**
     * Localiza informacion de una comision
     * @param valor del contrato para determinar la comision

     */

    public void buscaComision(double valor)throws SQLException{

        PreparedStatement st  = null;
        Connection        con = null;
        ResultSet         rs  = null;

        String            query       = "SQL_COMISION";
        comision                      = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setDouble( 1, valor );
            st.setDouble( 2, valor );

            rs = st.executeQuery();

            if(rs.next()){
                comision = new Comision();
                comision = (Comision.load(rs));
            }


        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL NOMBRE DEL CONTRATISTA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
    }



    /**
     * Retorna el valor de la comision
     */

    public ImpuestoContrato getImpuestoContrato() {

        return impuestoContrato;
    }


    /**
     * Localiza impuesto
     * @param codigo_general_impuesto Codigo del impuesto en tabla RTCONTRATO para un impuesto particular
     * @param distrito                Sigla de la compania
     * @param tipo_impuesto           Tipo de impuesto (IVA,RFTE,RICA,RIVA en tabla de impuestos
     */

    public void buscaImpuestoContrato(String codigo_general_impuesto,String distrito,String tipo_impuesto)throws SQLException{

        PreparedStatement st  = null;
        Connection        con = null;
        ResultSet         rs  = null;

        String            query       = "SQL_IMPUESTO";
        impuestoContrato              = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, codigo_general_impuesto );
            st.setString( 2, distrito );
            st.setString( 3, tipo_impuesto );

            rs = st.executeQuery();

            if(rs.next()){
                impuestoContrato = new ImpuestoContrato();
                impuestoContrato = (ImpuestoContrato.load(rs));
            }


        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL IMPUESTO. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
    }




    /**
     * Actualiza la secuencia de una prefactura para un contratista
     * @param id_contratista Codigo del contratista

     */


    public void setSecuenciaPrefactura(String id_contratista)throws SQLException{

        PreparedStatement st  = null;
        Connection        con = null;

        String            query       = "SQL_SET_SECUENCIA_PREFACTURA";

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, id_contratista );

            st.executeUpdate();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA SECUENCIA DE LA PREFACTURA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
    }



    /**
     * Actualiza las acciones seleccionadas para prefacturar
     * @param id_accion Identificacion de la accion
     * @param prefactura Codigo de prefactura

     */


    public void setAccion(String id_accion, String prefactura,double comision_ejecutiva,
                          double comision_canal, String codigo_iva, double vlr_iva,
                          String login,String codigo_rmat, double vlr_rmat,
                          String codigo_rmao, double vlr_rmao,
                          String codigo_rotr, double vlr_rotr,
                          double porcentajeIva, double porcentajeRetMat,
                          double porcentajeRetMao, double porcentajeRetOtr,
                          double vlr_factoring,
                          double porcentajeFormula,
                          double vlr_formula, double base_iva,double vlr_formula_provintegral)throws SQLException{

        PreparedStatement st  = null;
        Connection        con = null;

        String            query       = "SQL_SET_ACCION";

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, prefactura );
            st.setDouble( 2, comision_ejecutiva );
            st.setDouble( 3, comision_canal );
            st.setString( 4, codigo_iva );
            st.setDouble( 5, vlr_iva );
            st.setString( 6, login );
            st.setString( 7, codigo_rmat );
            st.setDouble( 8, vlr_rmat );
            st.setString( 9, codigo_rmao );
            st.setDouble( 10, vlr_rmao );
            st.setString( 11, codigo_rotr );
            st.setDouble( 12, vlr_rotr );
            st.setDouble( 13, porcentajeIva );
            st.setDouble( 14, porcentajeRetMat );
            st.setDouble( 15, porcentajeRetMao );
            st.setDouble( 16, porcentajeRetOtr );
            st.setDouble( 17, vlr_factoring);
            st.setDouble( 18, porcentajeFormula );
            st.setDouble( 19, vlr_formula);
            st.setDouble(20, base_iva);
            st.setDouble(21, vlr_formula_provintegral);
            st.setString( 22, id_accion );

            st.executeUpdate();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LAS ACCIONES AL PREFACTURAR. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
    }




    /**
     * Lista de las prefacturas de un contratista
     * @id_contratista Identificacion del contratista, codigo interno
     * @fechaInicial Fecha inicial del rango a seleccionar
     * @fechaFinal Fecha final del rango a seleccionar
     */


    public void buscaPrefacturaResumen(String id_contratista,String fechaInicial,String fechaFinal)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_RESUMEN_PREFACTURA";
        listaPrefacturaResumen     = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, id_contratista );
            st.setString( 2, fechaInicial+" 00:00:01" );
            st.setString( 3, fechaFinal+" 23:59:59" );

            rs = st.executeQuery();

            listaPrefacturaResumen =  new LinkedList();

            while (rs.next()){
                listaPrefacturaResumen.add(PrefacturaResumen.load(rs));
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DEL RESUMEN DE PREFACTURAS. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
    }


    /**
     * Returna una Lista prefacturas resumidas
     */


    public List getPrefacturaResumen() {

        return listaPrefacturaResumen;
    }






    /**
     * Lista del detalle de la prefactura
     * @prefactura Numero de prefactura a localizar
     */


    public void buscaDetallePrefactura(String prefactura)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_DETALLE_PREFACTURA";
        listaDetallePrefactura     = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, prefactura);

            rs = st.executeQuery();

            listaDetallePrefactura =  new LinkedList();

            while (rs.next()){
                listaDetallePrefactura.add(DetallePrefactura.load(rs));
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DEL DETALLE DE LA PREFACTURA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
    }


    /**
     * Returna una Lista items de una prefactura
     */


    public List getDetallePrefactura() {

        return listaDetallePrefactura;
    }





    /**
     * Lista de prefacturas por contratista
     */


    public List getContratistaPrefactura()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs               = null;

        String            query    = "SQL_LISTA_A_FACTURAR";
        List listaPrefacturaContratista = null ;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );

            rs = st.executeQuery();

            listaPrefacturaContratista =  new LinkedList();

            while (rs.next()){
                listaPrefacturaContratista.add(PrefacturaContratista.load(rs));
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE PREFACTURAS A FACTURAR. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return listaPrefacturaContratista;
    }



    /**
     * Lista resumida de las prefacturas de un contratista para facturarlas
     */


    public List getFacturaGeneral()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_FACTURA_GENERAL";
        List listaFacturaGeneral        = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();

            listaFacturaGeneral =  new LinkedList();

            while (rs.next()){
                listaFacturaGeneral.add(FacturaGeneral.load(rs));
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE LAS PREFACTURAS PARA FACTURAR AL CONTRATISTA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return listaFacturaGeneral;
    }



    /**
     * Forma un comando sql para insertar la cabecera en cxp_doc
     * @prefactura Numero de prefactura a localizar
     */


    public String setCxp_doc(String dstrct,String proveedor, String tipo_documento,String documento,
                             String descripcion,String agencia,
                             String handle_code,String aprobador,String usuario_aprobacion,
                             String banco,String sucursal,String moneda, Double vlr_neto,
                             Double vlr_total_abonos,Double vlr_saldo,Double vlr_neto_me,Double vlr_total_abonos_me,
                             Double vlr_saldo_me, Double tasa,String observacion,String user_update,
                             String creation_user,String base,String clase_documento,String moneda_banco,
                             String fecha_documento,String fecha_vencimiento,String clase_documento_rel,
                             String last_update, String creation_date)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;

        String comando_sql  = "";

        String            query    = "SQL_INSERTAR_CXP_DOC";

        try {
            con   = this.conectar( query );
            String sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1,dstrct);
            st.setString(2,proveedor);
            st.setString(3,tipo_documento);
            st.setString(4,documento);
            st.setString(5,descripcion);
            st.setString(6,agencia);
            st.setString(7,handle_code);
            st.setString(8,aprobador);
            st.setString(9,usuario_aprobacion);
            st.setString(10,banco);
            st.setString(11,sucursal);
            st.setString(12,moneda);
            st.setDouble(13,vlr_neto);
            st.setDouble(14,vlr_total_abonos);
            st.setDouble(15,vlr_saldo);
            st.setDouble(16,vlr_neto_me);
            st.setDouble(17,vlr_total_abonos_me);
            st.setDouble(18,vlr_saldo_me);
            st.setDouble(19,tasa);
            st.setString(20,observacion);
            st.setString(21,user_update);
            st.setString(22,creation_user);
            st.setString(23,base);
            st.setString(24,clase_documento);
            st.setString(25,moneda_banco);
            st.setString(26,fecha_documento);
            st.setString(27,fecha_vencimiento);
            st.setString(28,clase_documento_rel);
            st.setString(29,last_update);
            st.setString(30,creation_date);


            comando_sql = st.toString();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA INSERTAR EN CXP_DOC. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(query);
        }
        return comando_sql;
    }





    /**
     * Forma un comando sql para insertar la cabecera en cxp_doc
     * @prefactura Numero de prefactura a localizar
     */


    public String setCxp_items_doc(String dstrct, String proveedor, String tipo_documento,
                                   String documento, String item,String descripcion,
                                   double vlr, double vlr_me, String codigo_cuenta,
                                   String user_update,
                                   String creation_user, String base,
                                   String concepto, String auxiliar,
                                   String last_update, String creation_date)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;

        String comando_sql  = "";

        String            query    = "SQL_INSERTAR_CXP_ITEMS_DOC";

        try {
            con   = this.conectar( query );
            String sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1,dstrct);
            st.setString(2,proveedor);
            st.setString(3,tipo_documento);
            st.setString(4,documento);
            st.setString(5,item);
            st.setString(6,descripcion);
            st.setDouble(7,vlr);
            st.setDouble(8,vlr_me);
            st.setString(9,codigo_cuenta);
            st.setString(10,user_update);
            st.setString(11,creation_user);
            st.setString(12,base);
            st.setString(13,concepto);
            st.setString(14,auxiliar);
            st.setString(15,last_update);
            st.setString(16,creation_date);

            comando_sql = st.toString();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA INSERTAR EN CXP_ITEMS_DOC. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(query);
        }
        return comando_sql;
    }





    /**
     * Forma un comando sql para actualizar la factura del contratista en las acciones
     * @id_contratista Identificacion del contratista
     * @prefactura Numero de prefactura facturada
     */


    public String setFacturaContratista(String factura_contratista, String fecha_factura_contratista,
                                        String id_contratista, String prefactura)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZA_FACTURA_CONTRATISTA";

        try {
            con   = this.conectar( query );
            String sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1,factura_contratista);
            st.setString(2,fecha_factura_contratista);
            st.setString(3,id_contratista);
            st.setString(4,prefactura);


            comando_sql = st.toString();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA ACTUALIZAR FECHA FACTURA CONTRATISTA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(query);
        }
        return comando_sql;
    }




    /**
     * Metodo EjecutarSQL, realiza comandos sql sobre la base de datos en
     * operaciones de tipo transaccion.
     * @param   : Vector de comandos de SQl
     * @autor   : Alvaro Pabon
     * @version : 1.0
     */

    public void ejecutarSQL(Vector comandosSQL) throws SQLException {

        Connection        con      = null;
        Statement        stmt      = null;

        String            query    = "SQL_GET_CONECTION";

        try{
            con   = this.conectarJNDI( query );

            if (con != null){

                con.setAutoCommit(false);
                stmt = con.createStatement();
                for(int i=0; i<comandosSQL.size();i++){
                    String comando =(String) comandosSQL.elementAt(i);
                    // System.out.println("comando"+comando);
                    stmt.executeUpdate(comando);

                }

                con.commit();
            }
        }catch (SQLException e) {
            // Efectuando rollback en caso de error
            try {

                con.rollback();
            }
            catch (SQLException ignored) {
                throw new SQLException("NO SE PUDO HACER ROLLBACK " + ignored.getMessage() + " " + ignored.getErrorCode()+" <br> La siguiente exception es : ----"+ignored.getNextException());
            }
            throw new SQLException("ERROR DURANTE LA TRANSACCION, LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS " + e.getMessage() + " " + e.getErrorCode()+" <br> La siguiente exception es : ----"+e.getNextException());
        }

        finally{
            stmt.close();
            this.desconectar(con);
        }


    }




    /**
     * Lista resumida de facturas elaboradas al contratista para crear factura
     * para pagar a Applus por concepto de retencion y bonificacion
     */


    public List getFacturaApplus(String fechaInicial,String fechaFinal, String tipoFactura)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_FACTURA_APPLUS_"+tipoFactura;
        List listaFacturaApplus    = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, fechaInicial + " 00:00:01" );
            st.setString( 2, fechaFinal + " 23:59:59" );

            rs = st.executeQuery();

            listaFacturaApplus =  new LinkedList();

            while (rs.next()){
                listaFacturaApplus.add(FacturaApplus.load(rs));
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE LAS FACTURAS DEL CONTRATISTA PARA FACTURARLE A APPLUS. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return listaFacturaApplus;
    }





    /**
     * Forma un comando sql para actualizar la factura de applus
     * @id_contratista Identificacion del contratista
     * @prefactura Numero de prefactura facturada
     */


    public String setFacturaApplus(String factura_applus, String fecha_factura_applus,
                                   String id_contratista, String prefactura, String tipoFactura)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZA_FACTURA_APPLUS_"+tipoFactura;

        try {
            con   = this.conectar( query );
            String sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1,factura_applus);
            st.setString(2,fecha_factura_applus);
            st.setString(3,id_contratista);
            st.setString(4,prefactura);


            comando_sql = st.toString();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA ACTUALIZAR FECHA FACTURA DE APPLUS. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(query);
        }
        return comando_sql;
    }






    /**
     * Lista las acciones de un contratista sin prefacturar
     * @id_contratista Identificacion del contratista, codigo interno

     */


    public void buscaOfertaEca()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_OFERTAS";
        listaOfertaEca = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );

            rs = st.executeQuery();

            listaOfertaEca =  new LinkedList();

            while (rs.next()){
                listaOfertaEca.add(OfertaEca.load(rs));
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS DE OFERTAS. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
    }

    /**
     * Returna una Lista de ofertas
     */


    public List getOfertaEca() {

        return listaOfertaEca;
    }


    public void buscaOfertaEcaDetalle(int id_orden, String fecha_financiacion)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_OFERTAS_DETALLE";
        listaOfertaEcaDetalle = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            System.out.println("id_orden"+id_orden);
            System.out.println(" fecha financiacion :" + fecha_financiacion);
            st.setString( 1, ""+id_orden );
            st.setString( 2, fecha_financiacion );
            st.setString( 3, fecha_financiacion );
            rs = st.executeQuery();            

            listaOfertaEcaDetalle =  new LinkedList();

            while (rs.next()){



                listaOfertaEcaDetalle.add(OfertaEcaDetalle.load(rs));
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS DE OFERTAS DETALLADAS. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
    }


    public List getOfertaEcaDetalle() {

        return listaOfertaEcaDetalle;
    }

    public double  getPorcentajeFactoringFintra(String clase_dtf, int cuotas_reales)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_PORCENTAJE_FACTORING";
        String            porcentaje_factoring_fintra      = "0.0";

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            String sCuotas_reales = Integer.toString(cuotas_reales);
            st.setString( 1, clase_dtf );
            st.setString( 2, sCuotas_reales );
            rs = st.executeQuery();

            if (rs.next()){
                porcentaje_factoring_fintra=rs.getString("descripcion");
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PORCENTAJE FACTORING FINTRA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return (Double.parseDouble(porcentaje_factoring_fintra)/100);
    }







    /**
     * Forma un comando sql para actualizar la factura de applus
     * @id_contratista Identificacion del contratista
     * @prefactura Numero de prefactura facturada
     */


    public String setSimboloVariable(String simbolo_variable, String observacion, int id_estado_negocio,
                                     int id_orden,String userx)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZA_SIMBOLO";

        try {
            con   = this.conectar( query );
            String sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1,simbolo_variable);
            st.setString(2,observacion);
            st.setInt(3,id_estado_negocio);
            
            st.setString(4,userx);
            st.setInt(5,id_orden);
            
            st.setString(6,simbolo_variable);     //091229
            st.setInt(7,id_estado_negocio);    //091229
            st.setString(8,userx); //091229
            st.setInt(9,id_orden); //091229
            
            comando_sql = st.toString();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA ACTUALIZAR SIMBOLO VARIABLE. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(query);
        }
        return comando_sql;
    }



    public String setLiquidacionEca(OfertaEcaDetalle ofertaEcaDetalle)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZA_LIQUIDACION_ECA";

        try {
            con   = this.conectar( query );
            String sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setDouble(1,ofertaEcaDetalle.getTotal_prev1());
            st.setDouble(2,ofertaEcaDetalle.getIva_total_prev1());
            st.setDouble(3,ofertaEcaDetalle.getComision_applus());
            st.setDouble(4,ofertaEcaDetalle.getComision_provintegral());
            st.setDouble(5,ofertaEcaDetalle.getComision_fintra());
            st.setDouble(6,ofertaEcaDetalle.getIva_comision_applus());
            st.setDouble(7,ofertaEcaDetalle.getIva_comision_provintegral());
            st.setDouble(8,ofertaEcaDetalle.getIva_comision_fintra());
            st.setDouble(9,ofertaEcaDetalle.getComision_eca());
            st.setDouble(10,ofertaEcaDetalle.getEca_oferta_calculada());
            st.setDouble(11,ofertaEcaDetalle.getFinanciacion_fintra());
            st.setInt(12,ofertaEcaDetalle.getCuotas_reales());
            st.setDouble(13,ofertaEcaDetalle.getDtf_semana());
            st.setDouble(14,ofertaEcaDetalle.getPorcentaje_factoring());
            st.setString(15,ofertaEcaDetalle.getFecha_financiacion());
            st.setDouble(16,ofertaEcaDetalle.getCuota_pago());
            st.setDouble(17,ofertaEcaDetalle.getPorcentaje_comision_applus());
            st.setDouble(18,ofertaEcaDetalle.getPorcentaje_comision_provintegral());
            st.setDouble(19,ofertaEcaDetalle.getPorcentaje_comision_fintra());
            st.setDouble(20,ofertaEcaDetalle.getPorcentaje_comision_eca());
            st.setDouble(21,ofertaEcaDetalle.getPorcentaje_iva());
            st.setDouble(22,ofertaEcaDetalle.getIva_comision_eca());
            st.setDouble(23,ofertaEcaDetalle.getComision_factoring_fintra());
            st.setDouble(24,ofertaEcaDetalle.getIva_comision_factoring_fintra());
            st.setDouble(25,ofertaEcaDetalle.getPorcentaje_factoring_fintra());
            st.setDouble(26,ofertaEcaDetalle.getPuntos_dtf());
            st.setDouble(27,ofertaEcaDetalle.getTotal_financiacion());

            st.setDouble(28,ofertaEcaDetalle.getEc_porcentaje_comision_provintegral());
            st.setDouble(29,ofertaEcaDetalle.getEc_porcentaje_iva());
            st.setDouble(30,ofertaEcaDetalle.getEc_porcentaje_factoring_fintra());
            st.setDouble(31,ofertaEcaDetalle.getEc_porcentaje_comision_applus());
            st.setDouble(32,ofertaEcaDetalle.getEc_porcentaje_comision_fintra());
            st.setDouble(33,ofertaEcaDetalle.getEc_porcentaje_comision_eca());
            st.setDouble(34,ofertaEcaDetalle.getEc_valor_mat());
            st.setDouble(35,ofertaEcaDetalle.getEc_comision_applus());
            st.setDouble(36,ofertaEcaDetalle.getEc_comision_provintegral());
            st.setDouble(37,ofertaEcaDetalle.getEc_comision_factoring_fintra());
            st.setDouble(38,ofertaEcaDetalle.getEc_comision_fintra());
            st.setDouble(39,ofertaEcaDetalle.getEc_comision_eca());
            st.setDouble(40,ofertaEcaDetalle.getEc_iva_valor_mat());
            st.setDouble(41,ofertaEcaDetalle.getEc_iva_comision_applus());
            st.setDouble(42,ofertaEcaDetalle.getEc_iva_comision_factoring_fintra ());
            st.setDouble(43,ofertaEcaDetalle.getEc_iva_comision_fintra());
            st.setDouble(44,ofertaEcaDetalle.getEc_iva_comision_provintegral());
            st.setDouble(45,ofertaEcaDetalle.getEc_iva_comision_eca());
            st.setDouble(46,ofertaEcaDetalle.getEc_financiacion_fintra());
            st.setDouble(47,ofertaEcaDetalle.getEc_cuota_pago());
            st.setDouble(48,ofertaEcaDetalle.getEc_total_financiacion ());
            st.setDouble(49,ofertaEcaDetalle.getIvaBonificacion() );
            st.setString(50,ofertaEcaDetalle.getId_accion());

            //inicio de colocar bien tipo_dtf
            //091229
            if (ofertaEcaDetalle.getPuntos_dtf()==9 || ofertaEcaDetalle.getPuntos_dtf()==11 || ofertaEcaDetalle.getPuntos_dtf()==12){
                st.setString(51,("DTF+"+ofertaEcaDetalle.getPuntos_dtf()).replaceAll(".0",""));
            }else{
                st.setString(51,"MAXIMA");
            }
                
            st.setString(52,""+ofertaEcaDetalle.getId_orden());
            //091229

            //091229
            if (ofertaEcaDetalle.getPuntos_dtf()==9 || ofertaEcaDetalle.getPuntos_dtf()==11 || ofertaEcaDetalle.getPuntos_dtf()==12){//090730
                st.setString(53,("DTF+"+ofertaEcaDetalle.getPuntos_dtf()).replaceAll(".0",""));    //090730
            }else{//090730
                st.setString(53,"MAXIMA");//090730
            }//090730                
            st.setString(54,""+ofertaEcaDetalle.getId_orden());//090730
            //091229
            //fin de colocar bien tipo_dtf                
                
            comando_sql = st.toString();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA ACTUALIZAR LIQUIDACION ECA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(query);
        }
        return comando_sql;
    }



    public List  buscaPrefacturaEca()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_BUSCA_PREFACTURA_ECA";
        List listaPrefacturaEca = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();

            listaPrefacturaEca =  new LinkedList();

            while (rs.next()){
                listaPrefacturaEca.add(PrefacturaEca.load(rs));
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS DE OFERTAS PARA FACTURAR A ECA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return listaPrefacturaEca;
    }


    


    public List  buscaPrefacturaEcaContratista(int id_orden)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_ORDEN_CONTRATISTA";
        List listaPrefacturaEcaContratista = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setInt( 1, id_orden );
            System.out.println("sqlll"+st.toString());
            rs = st.executeQuery();

            listaPrefacturaEcaContratista =  new LinkedList();

            while (rs.next()){
                listaPrefacturaEcaContratista.add(PrefacturaEcaContratista.load(rs));
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS DE ACCIONES DE CONTRATISTA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return listaPrefacturaEcaContratista;
    }

    
    
    
    
    

    public String setFactura(String dstrct, String tipo_documento, String documento,
                             String nit, String codcli, String concepto,
                             String fecha_factura, String fecha_vencimiento,
                             String fecha_impresion,String descripcion,
                             Double valor_factura, Double valor_abono, Double valor_saldo,
                             Double valor_facturame, Double valor_abonome, Double valor_saldome,
                             Double valor_tasa, String moneda,
                             int cantidad_items, String forma_pago, String agencia_facturacion,
                             String agencia_cobro,String zona, String base,String last_update, String user_update,
                             String creation_date, String creation_user, String cmc,
                             String formato, String agencia_impresion,
                             String tipo_ref1,String ref1,String tipo_ref2,String ref2)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;

        String comando_sql  = "";

        String            query    = "SQL_INSERTA_FACTURA";

        try {
            con   = this.conectar( query );
            String sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1,dstrct);
            st.setString(2,tipo_documento);
            st.setString(3,documento);
            st.setString(4,nit);
            st.setString(5,codcli);
            st.setString(6,concepto);
            st.setString(7,fecha_factura);
            st.setString(8,fecha_vencimiento);
            st.setString(9,fecha_impresion);
            st.setString(10,descripcion);
            st.setDouble(11,valor_factura);
            st.setDouble(12,valor_abono);
            st.setDouble(13,valor_saldo);
            st.setDouble(14,valor_facturame);
            st.setDouble(15,valor_abonome);
            st.setDouble(16,valor_saldome);
            st.setDouble(17,valor_tasa);
            st.setString(18,moneda);
            st.setInt(19,cantidad_items);
            st.setString(20,forma_pago);
            st.setString(21,agencia_facturacion);
            st.setString(22,agencia_cobro);
            st.setString(23,zona);
            st.setString(24,base);
            st.setString(25,last_update);
            st.setString(26,user_update);
            st.setString(27,creation_date);
            st.setString(28,creation_user);
            st.setString(29,cmc);
            st.setString(30,formato);
            st.setString(31,agencia_impresion);
            st.setString(32,tipo_ref1);
            st.setString(33,ref1);
            st.setString(34,tipo_ref2);
            st.setString(35,ref2);

            comando_sql = st.toString();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA AGREGAR UNA FACTURA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(query);
        }
        return comando_sql;
    }



    public String setFacturaDetalle(String dstrct, String tipo_documento, String documento,
                                    int item, String nit, String concepto,
                                    String descripcion, String codigo_cuenta_contable,
                                    double cantidad, double valor_unitario , double valor_unitariome , double valor_item,
                                    double valor_itemme , double valor_tasa, String moneda,
                                    String last_update, String user_update, String creation_date,
                                    String creation_user, String base, String auxiliar)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;

        String comando_sql  = "";

        String            query    = "SQL_INSERTA_FACTURA_DETALLE";

        try {
            con   = this.conectar( query );
            String sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1,dstrct);
            st.setString(2,tipo_documento);
            st.setString(3,documento);
            st.setInt(4,item);
            st.setString(5,nit);
            st.setString(6,concepto);
            st.setString(7,descripcion);
            st.setString(8,codigo_cuenta_contable);
            st.setDouble(9,cantidad);
            st.setDouble(10,valor_unitario);
            st.setDouble(11,valor_unitariome);
            st.setDouble(12,valor_item);
            st.setDouble(13,valor_itemme);
            st.setDouble(14,valor_tasa);
            st.setString(15,moneda);
            st.setString(16,last_update);
            st.setString(17,user_update);
            st.setString(18,creation_date);
            st.setString(19,creation_user);
            st.setString(20,base);
            st.setString(21,auxiliar);

            comando_sql = st.toString();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA INSERCION DE LOS REGISTROS DE DETALLE DE UNA FACTURA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(query);
        }
        return comando_sql;
    }





    public String setOferta(int id_orden, String factura_eca, String fecha_factura_eca)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZA_OFERTA";

        try {
            con   = this.conectar( query );
            String sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1,factura_eca);
            st.setString(2,fecha_factura_eca);
            st.setInt(3,id_orden);

            comando_sql = st.toString();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA OFERTA CON EL NUMERO DE FACTURA ECA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(query);
        }
        return comando_sql;
    }







    public String setFechaFacturaEca(int id_orden, String fecha_factura_eca)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZA_FECHA_FACTURA_ECA";

        try {
            con   = this.conectar( query );
            String sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1,fecha_factura_eca);
            st.setInt(2,id_orden);

            comando_sql = st.toString();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA FECHA DE FACTURA ECA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(query);
        }
        return comando_sql;
    }




    public List  buscaPrefacturaApp()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_GENERAR_FACTURA_APP";
        List listaPrefacturaEca = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();

            listaPrefacturaEca =  new LinkedList();

            while (rs.next()){
                listaPrefacturaEca.add(PrefacturaEca.load(rs));
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS DE OFERTAS PARA FACTURAR A APPLUS. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return listaPrefacturaEca;
    }



    public String setFacturaComision(int id_orden,String documento,String fecha_factura_eca)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZA_FACTURA_COMISION_APP";

        try {
            con   = this.conectar( query );
            String sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1,documento);
            st.setString(2,fecha_factura_eca);
            st.setInt(3,id_orden);

            comando_sql = st.toString();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA FACTURA APPLUS. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(query);
        }
        return comando_sql;
    }





    public List  buscaPrefacturaPro()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_GENERAR_FACTURA_PRO";
        List listaPrefacturaEca = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();

            listaPrefacturaEca =  new LinkedList();

            while (rs.next()){
                listaPrefacturaEca.add(PrefacturaEca.load(rs));
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS DE OFERTAS PARA FACTURAR A PROVINTEGRAL. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return listaPrefacturaEca;
    }



    public String setFacturaPro(int id_orden,String documento,String fecha_factura_eca)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZA_FACTURA_COMISION_PRO";

        try {
            con   = this.conectar( query );
            String sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1,documento);
            st.setString(2,fecha_factura_eca);
            st.setInt(3,id_orden);

            comando_sql = st.toString();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA FACTURA PROVINTEGRAL. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(query);
        }
        return comando_sql;
    }






    public List  buscaPrefacturaComisionEca()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_GENERAR_COMISION_FACTURA_ECA";
        List listaPrefacturaEca = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();

            listaPrefacturaEca =  new LinkedList();

            while (rs.next()){
                listaPrefacturaEca.add(PrefacturaEca.load(rs));
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS DE OFERTAS PARA FACTURAR A ECA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return listaPrefacturaEca;
    }



    public String setFacturaComisionEca(int id_orden,String documento,String fecha_factura_eca)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZA_FACTURA_COMISION_ECA";

        try {
            con   = this.conectar( query );
            String sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1,documento);
            st.setString(2,fecha_factura_eca);
            st.setInt(3,id_orden);

            comando_sql = st.toString();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA FACTURA ECA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(query);
        }
        return comando_sql;
    }





    public PuntosFinanciacion getPuntosFinanciacion(String esquema, String regulacion, double valor, String ano,
                                                    String trimestre, int cuotas)throws SQLException{

        
        System.out.println("esquema"+esquema+"regulacion"+regulacion+"valor"+valor+"ano"+ano+"trimestre"+trimestre+"cuotas"+cuotas);
        
        PreparedStatement st  = null;
        Connection        con = null;
        ResultSet         rs  = null;

        String            query1       = "SQL_PUNTOS_NUEVO_R";
        String            query2       = "SQL_PUNTOS_NUEVO_NR";
        String            query3       = "SQL_PUNTOS_VIEJO";
        String            query        = "";

        double            puntos       = 0.0;
        PuntosFinanciacion puntosFinanciacion = null;

        try {


            if ( (esquema.equalsIgnoreCase("NUEVO")) && (regulacion.equalsIgnoreCase("R")) ) {
                query =  query1 ;
            }
            if ( (esquema.equalsIgnoreCase("NUEVO")) && (regulacion.equalsIgnoreCase("NR")) ) {
                query =  query2 ;
            }
            if  (esquema.equalsIgnoreCase("VIEJO") ) {
                query =  query3 ;
            }

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );


            if ( (esquema.equalsIgnoreCase("NUEVO")) && (regulacion.equalsIgnoreCase("R")) ) {
                st.setInt( 1, cuotas );
                st.setInt( 2, cuotas );
                st.setString( 3, ano );
                st.setString( 4, ano );
                st.setString( 5, trimestre );
                st.setString( 6, trimestre );

            }
            if ( (esquema.equalsIgnoreCase("NUEVO")) && (regulacion.equalsIgnoreCase("NR")) ) {
                st.setDouble( 1, valor );
                st.setDouble( 2, valor );
                st.setString( 3, ano );
                st.setString( 4, ano );
                st.setString( 5, trimestre );
                st.setString( 6, trimestre );
            }


            rs = st.executeQuery();

            if(rs.next()){
                puntosFinanciacion = new PuntosFinanciacion();
                puntosFinanciacion = (PuntosFinanciacion.load(rs));
            }


        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS PUNTOS PARA LA FINANCIACION. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return puntosFinanciacion;
    }

    public String  buscaF_facturado_cliente(String orden)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_OBTENER_F_FACTURADO_CLIENTE";
        String f_facturado_cliente= "0099-01-01";

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, orden );
            rs = st.executeQuery();

            if (rs.next()){
                f_facturado_cliente=rs.getString("f_facturado_cliente");
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE buscaF_facturado_cliente. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return f_facturado_cliente;
    }












}
