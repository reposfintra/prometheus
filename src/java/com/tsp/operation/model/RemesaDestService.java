/*
 * RemesaDestService.java
 *
 * Created on 9 de diciembre de 2004, 08:05 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  KREALES
 */
public class RemesaDestService {
    
    RemesaDestDAO rd;
    /** Creates a new instance of RemesaDestService */
    public RemesaDestService() {
        rd= new RemesaDestDAO();
    }
    public String insertRemesa(RemesaDest rem, String base)throws SQLException{
        try{
            rd.setRemDes(rem);
            return rd.insert(base);
            
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void updateRemesa(RemesaDest rem)throws SQLException{
        try{
           rd.setRemDes(rem);
           rd.update();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public RemesaDest getRemDest()throws SQLException{
        
        return rd.getRemDes();
        
    }
    public List getList()throws SQLException{
        return rd.getList();
    }
    
    public void buscaRemesa(String numrem)throws SQLException{
        try{
            rd.searchLista(numrem);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void anularRemesa(RemesaDest rem)throws SQLException{
        try{
            rd.setRemDes(rem);
            rd.anular();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void buscaRemitentes(String numrem)throws SQLException{
        try{
            rd.searchRemitentes(numrem);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void buscaDestinatarios(String numrem)throws SQLException{
        try{
            rd.searchDestinatarios(numrem);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public List getRemitentes()throws SQLException{
        return rd.getRemitentes();
    }
    
    public List getDestinatarios()throws SQLException{
        return rd.getDestinatarios();
    }
    public boolean estaDest(String numrem, String dest )throws SQLException{
        return rd.estaDest(numrem, dest);
    }
    
}
