/*
 * StdjobService.java
 *
 * Created on 27 de mayo de 2005, 08:58 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

//import Model.Services.*;

/**
 *
 * @author  DIOGENES
 */
public class StdjobServices {
    
    private StdjobDAO stdjobd;
    
    public StdjobServices() {
        stdjobd = new StdjobDAO();
    }
    
    public StdJob BuscarStdjob(String dstrct, String std_job_no )throws SQLException{ 
        try{
           return stdjobd.BuscarStdjob(dstrct, std_job_no);
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
    }
    
}
