/*
 * HExport_MS230828.java
 *
 * Created on 5 de mayo de 2005, 08:48
 * modificado: egonzalez2014
 */

package com.tsp.operation.model;


import java.io.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;
import java.sql.*;
/**
 *
 * @author  kreales
 */
public class HExport_MST620 extends Thread{
    
    
    //MIGRACION CONDUCTOR PROPIETARIO
    private String Fecha;
    private String NombreArchivo;
    private String base;
    private Vector plas;
    
    private   String      unidad;
    private   String      name;
    private   String      fecha;
    private   String      Path_FileTxt;
    private   String      linea;
    private   FileReader  fileReader;
    private   int         posInitialPlaca ;
    private   int         posFishedPlaca  ;
    private   int         posInitialCedula ;
    private   int         posFishedCedula ;
    private   int         posInitialRemision ;
    private   int         posFishedRemision ;
    
    private  String          unidadWrite;
    private  String          nameFileRemision;
    private  String          nameFileCreacion;
    private  FileWriter      fileRemision;
    private  FileWriter      fileCreacion;
    private  BufferedWriter  bufferRemision;
    private  BufferedWriter  bufferCreacion;
    private  PrintWriter     lineaRemision;
    private  PrintWriter     lineaCreacion;
    
    private  FileWriter      fwC810;
    private  FileWriter      fwC760;
    private  BufferedWriter  bfC810;
    private  BufferedWriter  bfC760;
    private  PrintWriter     lineC810;
    private  PrintWriter     lineC760;
    
    
    
    
    //--- otras variables MIGRACION CONDUCTOR PROPIETARIO
    private Model model;
    private List listado760;
    private List listado810;
    private String tipo;
    
    ///160905
    private String idusuario;
    private String ruta;
    private String rutalog;
    Vector plarem;
    /** Creates a new instance of HExport_MS230828 */
    
    public HExport_MST620() {
    }
    // static Logger logger = Logger.getLogger(HExport_MST620.class);
    
    public void start(String Fecha, String base, Vector planillas,Model model,String tipo,String idusuario) {
        
        
        
        this.base=base.toLowerCase();
        this.tipo=tipo;
        this.idusuario=idusuario;
        this.Fecha         = Fecha.replaceAll("-","");
        
        //160905
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        ruta = rb.getString("ruta");
        
        File file = new File(ruta + "/exportar/migracion/"+this.idusuario);
        file.mkdirs();
        this.NombreArchivo = ruta + "/exportar/migracion/"+this.idusuario+"/MST620_"+  this.Fecha +".txt";
        rutalog = ruta + "/exportar/migracion/"+this.idusuario+"/log_migracion"+  this.Fecha +".txt";
        plas = planillas;
        iniVar(model, this.Fecha);
        iniTxt();
        
        
        super.start();
    }
    
    public synchronized void run(){
        String noSubidas="";
        plarem=new Vector();
        Vector consultas = new Vector();
        try{
            
            //nuevos datos
            String recibe = "Y";
            String completada = "Y";
            String secuencia = "01";
            String Linea="";
            PrintStream archivo = new PrintStream(new FileOutputStream(NombreArchivo));
            PrintStream log = new PrintStream(new FileOutputStream(rutalog));
            log.println("PLANILLAS Y/O REMESAS ENCONTRADAS EN MIMS.");
            noSubidas = "PLANILLAS Y/O REMESAS ENCONTRADAS EN MIMS.\n\n";
            //logger.info("Este es el tama�o del archivo..."+plas.size());
            PlanillaImpresionDAO pimp = new PlanillaImpresionDAO();
            Vector remesaMIMS=new Vector();
            Vector planillaMIMS=new Vector();
            
            model.LogProcesosSvc.InsertProceso("Generacion MST620", this.hashCode(), "Migracion Planillas Carga General", idusuario);
            
            boolean enviarMail = false;
            for(int i=0; i<plas.size();i++){
                
                DatosPlanilla datos = (DatosPlanilla) plas.elementAt(i);
                
                ////System.out.println("Planilla "+ datos.getNumeroPlanilla());
                ////System.out.println("Remesa "+datos.getNumeroRemision());
                
                String preq_no="";
                boolean escribir=false;
                boolean estaPla = false;
                boolean estaRem = pimp.estaEnMimsRem(datos.getNumeroRemision(), datos.getCia());
                //System.out.println("Esta remesa "+datos.getNumeroRemision()+" "+estaRem);
                if(tipo.equals("2")){
                    preq_no = pimp.estaEnMimsPla(datos.getNumeroPlanilla());
                    //logger.info("EL PREQ_NO DE: "+datos.getNumeroPlanilla()+" ES: "+preq_no);
                    estaPla = preq_no.equals("")?false:true;
                    //System.out.println("Esta planilla en Mims "+datos.getNumeroPlanilla()+" "+estaPla);
                    
                    if(estaPla){
                        
                        //logger.info("Se encontro la planilla "+datos.getNumeroPlanilla()+" en mims");
                        Vector remesas = pimp.buscarRemesas(datos.getNumeroPlanilla());
                        //System.out.println("Esta planilla tiene "+remesas.size()+" relacionadas");
                        
                        if(remesas.size()>1){
                            boolean sw=true;
                            for (int j = 0; j<plarem.size();j++){
                                RemPla pr= (RemPla)plarem.elementAt(j);
                                //logger.info("Comparo la planilla y remesa "+pr.getPlanilla()+pr.getRemesa()+" con la planilla y remesa: "+datos.getNumeroPlanilla()+datos.getNumeroRemision());
                                
                                String plarem = pr.getPlanilla()+pr.getRemesa();
                                String plarem2 = datos.getNumeroPlanilla()+datos.getNumeroRemision();
                                if(plarem.equals(plarem2)){
                                    //logger.info("Ya esta en el archivo..");
                                    sw= false;
                                }
                            }
                            //logger.info("Termine la comparacion....");
                            if(sw){
                                //logger.info("Se agrega la planilla con "+preq_no+" a la lista ....");
                                for(int j = 0; j<remesas.size();j++){
                                    RemPla pr= (RemPla)remesas.elementAt(j);
                                    pr.setObservacion(preq_no);
                                    plarem.add(pr);
                                }
                            }
                            
                        }
                        
                    }
                    
                }
                if(estaPla && estaRem){
                    
                    //logger.info("Se encontro planilla y remesas en mims");
                    escribir=false;
                    enviarMail=true;
                    log.println("REMESA : "+datos.getNumeroRemision()+" PLANILLA : "+datos.getNumeroPlanilla());
                    noSubidas +="REMESA : "+datos.getNumeroRemision()+" PLANILLA : "+datos.getNumeroPlanilla()+"\n";
                    
                }else{
                    //logger.info("Se encontro solo la planilla en mims");
                    escribir=true;
                    enviarMail=true;
                    planillaMIMS.add(datos.getNumeroPlanilla());
                    remesaMIMS.add(datos.getNumeroRemision());
                    log.println("PLANILLA : "+datos.getNumeroPlanilla()+" REMESA : "+datos.getNumeroRemision());
                    noSubidas +="PLANILLA : "+datos.getNumeroPlanilla()+" REMESA : "+datos.getNumeroRemision()+"\n";
                }
                
                
                if(escribir){
                    //BUSCO LOS DOCUMENTOS RELACIONADOS CON LA REMESA
                    //  log.println("Se ecribe el archivo ....");
                    remesa_doctoDAO remDoc = new remesa_doctoDAO();
                    ////System.out.println("Se van a migrar los documentos");
                    remDoc.MigrarDocumentos(datos.getNumeroRemision());
                    
                    Vector  documentos = remDoc.getDocumentos();
                    String docs="";
                    String facts="";
                    int catDocs =0;
                    int catFacts =0;
                    for (int j=0; j<documentos.size();j++){
                        remesa_docto rd = (remesa_docto) documentos.elementAt(j) ;
                        catDocs++;
                        
                        int swDoc = 0;
                        if(catDocs<=15){
                            docs  =docs + fill(rd.getDestinatario().equals("")?rd.getDocumento():rd.getDocumento()+":"+rd.getDestinatario(),25,0);
                            swDoc = 1;
                        }
                        //////System.out.println("Documento rel "+rd.getDocumento_rel());
                        if(catFacts<=15){
                            if(swDoc==1 && rd.getDocumento_rel().equals("") ){
                                facts  =facts + fill(rd.getDocumento_rel(),15,0);
                                catFacts++;
                            }
                            if(swDoc==1 && !rd.getDocumento_rel().equals("") ){
                                facts  =facts + fill(rd.getDocumento_rel(),15,0);
                                catFacts++;
                            }
                            
                        }
                    }
                    if(docs.length()<375){
                        docs=docs + fill("",375-docs.length(),0);
                    }
                    if(facts.length()<225){
                        facts=facts + fill("",225-facts.length(),0);
                    }
                    ////System.out.println("Listo.. migre docs...");
                    
                    String cantPla = datos.getPesoReal();
                    String cantRem = datos.getPesoReal();
                    if(tipo.equals("2")){
                        cantPla = datos.getCantPlanilla();
                        cantRem = datos.getCantRemesa();
                    }
                    
                    //Linea  = fill(datos.getNumeroPlanilla(),8,0);
                    Linea  = fill(datos.getNumeroRemision(),8,0);
                    
                    if(base.equals("spo")){
                        
                        Linea += fill(datos.getFechaPlanilla(),8,0);
                    }
                    else{
                        Linea += fill(this.Fecha,8,0);
                    }
                    Linea += fill(datos.getSJ(),6,0);
                    Linea += fill(datos.getPlacaVehiculo(),12,0);
                    Linea += fill(cantRem,9,1);
                    Linea += fill(datos.getCedulaConductor(),10,0);
                    ////System.out.println("Busco los documentos");
                    if (datos.getAnticipos()!=null){
                        Iterator it2 = datos.getAnticipos().iterator();
                        int j =0;
                        while (it2.hasNext()){
                            j++;
                            DatosDescuentosImp desc = (DatosDescuentosImp) it2.next();
                            ////logger.info("Escribiendo descuento no "+j);
                            if ("Sj".equalsIgnoreCase(desc.getTipo())){
                                //  //logger.info("Valor del descuento "+desc.getCodigo() +" "+desc.getValor());
                                if(desc.getCodigo().equals("ASRU ")){
                                    desc.setValor("1.7");
                                    //    //logger.info("se cambio el valor");
                                }
                                
                                Linea += fill(desc.getCodigo(),5,0) + fill(desc.getValor(),11,1);
                                
                            }
                            else if("CGAGEN".equalsIgnoreCase(desc.getTipo())){
                                
                                //SE VALIDA PARA QUE NO TOME LOS QUE SON ANTICIPOS SIN NUMERO DE CHEQUE
                                if(desc.getCodigo().equals("01") && !desc.getDocumento().equals("")){
                                    Linea += fill(desc.getProveedorMigracion(),5,0) + fill(desc.getValor(),11,1);
                                }
                                else if (!desc.getCodigo().equals("01")){
                                    Linea += fill(desc.getProveedorMigracion(),5,0) + fill(desc.getValor(),11,1);
                                }
                                else if(desc.getCodigo().equals("01") && desc.getDocumento().equals("")){
                                    Linea += fill(" ",5,0) + fill(" ",11,1);
                                }
                                
                                
                                // //logger.info("El descuento es de CARGA GENERAL");
                                
                                
                                
                            }
                            else{
                                Linea += fill(desc.getProveedorMigracion()+ datos.getCodigoAgenciaPlanilla() + desc.getConceptoMigracion(),5,0) +  fill(desc.getValor(),11,1);
                                
                            }
                            //model.movplaService.migrarMovpla(datos.getNumeroPlanilla(), desc.getCreacion_movpla().equals("")?"0099-01-01":desc.getCreacion_movpla());
                            ////System.out.println(model.PlanillaImpresionSvc.actualizarMigracion(datos.getNumeroPlanilla(),desc.getCreacion_movpla(),desc.getCodigo(),desc.getDocumento()));
                            consultas.add(model.PlanillaImpresionSvc.actualizarMigracion(datos.getNumeroPlanilla(),desc.getCreacion_movpla(),desc.getCodigo(),desc.getDocumento()));
                            
                        }
                        if(j<11){
                            int resto = 11-j;
                            int k =0;
                            while (k<=resto){
                                Linea +=fill("",16,0);
                                k++;
                            }
                        }
                    }
                    
                    //NUMERO DE LA PLANILLA
                    Linea += fill(datos.getNumeroPlanilla(),6,0);
                    //FLETE
                    Linea += fill(datos.getFlete()==null?"                 ":datos.getFlete(),11,1);
                    //DISTRITO
                    Linea += fill(datos.getCia(),4,0);
                    
                    
                    //ESTO SE ESCRIBE SOLO SI ES PARA CARGA GENERAL, NO EN CARBON
                    
                    if(tipo.equals("2")){
                        // //logger.info("Se guarda el cheque de la planilla "+datos.getNumeroPlanilla());
                        // model.movplaService.guardarCheque(datos.getNumeroPlanilla());
                        recibe = "N";
                        completada = "N";
                        secuencia = datos.getCf_code().length()>6?datos.getCf_code().substring(6):"01";
                        
                    }
                    
                    
                    //NUEVA FORMA DE LLENAR  DOCUMENTOS..
                    Linea += docs;
                    Linea += facts;
                    
                    //CANTIDAD REAL
                    
                    Linea +=datos.getCantidReal().equals("")?fill(datos.getCantidReal(),11,0):fill(datos.getCantidReal(),11,1);
                    //CONTENEDORES
                    String vecCont[] =datos.getContenedor().split(",");
                    for(int k =0; k<vecCont.length;k++){
                        if(k>1)
                            break;
                        Linea += fill(vecCont[k],20,0);
                    }
                    int rest = 2-vecCont.length;
                    
                    for(int k =1; k<=rest;k++){
                        Linea += fill("",20,0);
                    }
                    //PRECINTOS
                    String vec3[] =datos.getPrecinto().split(",");
                    int vac = 0;
                    int max =4;
                    if(vec3.length>1){
                        if(vec3[0].equals("")){
                            max = 6;
                        }
                    }
                    if(vec3.length<5){
                        max =vec3.length;
                    }
                    if(vec3.length>0){
                        for(int k =0; k<vec3.length;k++){
                            if(k>max)
                                break;
                            if(!vec3[k].equals("")){
                                Linea += fill(vec3[k],10,0);
                            }
                            else{
                                vac++;
                            }
                        }
                    }
                    rest = 5-vec3.length;
                    rest = rest +vac;
                    for(int k =1; k<=rest;k++){
                        Linea += fill("",10,0);
                    }
                    
                    //UNIDADES
                    Linea +=datos.getUnidades().equals("")?fill(datos.getUnidades(),5,0):fill(datos.getUnidades(),5,0);
                    //TRAILER
                    Linea += fill(datos.getTrailer().equals("NA")?"":datos.getTrailer(),12,0);
                    //FECHA SALIDA
                    if(!datos.getFechaSalida().equals("")){
                        Linea += fill(datos.getFechaSalida(),8,0);
                        Linea += "S1 ";
                        Linea += fill(datos.getHoraISalida(),4,0);
                        Linea += fill(datos.getHoraFSalida(),4,0);
                        
                    }
                    else
                        Linea += fill("",19,0);
                    
                    //FECHA CARGUE
                    if(!datos.getFechaCargue().equals("")){
                        Linea += fill(datos.getFechaCargue(),8,0);
                        Linea += "CG ";
                        Linea += fill(datos.getHoraICargue(),4,0);
                        Linea += fill(datos.getHoraFCargue(),4,0);
                        
                    }
                    else
                        Linea += fill("",19,0);
                    
                    //FECHA DESCARGUE
                    
                    if(!datos.getFechaDescargue().equals("")){
                        Linea += fill(datos.getFechaDescargue(),8,0);
                        Linea += "DE ";
                        Linea += fill(datos.getHoraIDescargue(),4,0);
                        Linea += fill(datos.getHoraFDescargue(),4,0);
                    }
                    else
                        Linea += fill("",19,0);
                    
                    //REMITENTE
                    Linea += fill(datos.getRemitente(),8,0);
                    //DESTINATARIO
                    Linea += fill(datos.getDestinatario(),8,0);
                    
                    
                    Linea += fill(recibe,1,0);
                    Linea += fill(completada,1,0);
                    Linea += fill(cantPla,9,1);
                    Linea += fill(secuencia,2,0);
                    Linea += fill(datos.getCedula(),10,2);
                    Linea += fill(datos.getDespachador().toUpperCase(),10,0);
                    Linea += fill(""+datos.getPorcent(),3,1);
                    
                    
                    //NUEVOS CAMPOS
                    archivo.println(Linea);
                }
            }
            archivo.close();
            log.close();
            model.despachoService.insertar(consultas);
            //logger.info("llego al fin del archivo.");
            //logger.info("Generando archivo MSO230.");
            //System.out.println("Se va a generar el MSO230 ");
            generarMSO230();
            //System.out.println("fin MSO230 ");
            //logger.info("Fin archivo MSO230.");
            //logger.info("Generando archivo de conductores.");
            this.GenerarArchivoConductor();
            //logger.info("Fin de Generacion archivo de conductores.");
            //logger.info("Generando archivo de Placas.");
            this.GenerarArchivoPlaca();
            //logger.info("Fin de Generacion archivo de placas.");
            
            //ENVIO EL EMAIL
            if(enviarMail){
                Email e = new Email();
                e.setEmailfrom("migraciondespacho@sanchezpolo.com");
                e.setSenderName("FINTRAVALORES");
                e.setEmailto("jgomesfintra@metrotel.com;");
                e.setEmailcopyto("");
                //e.setEmailcopyto("");
                e.setEmailsubject("INCONSISTENCIAS ARCHIVO DE MIGRACION");
                e.setEmailbody(noSubidas);
                com.tsp.operation.model.threads.SendMail s = new com.tsp.operation.model.threads.SendMail();
                s.start(e);
            }
            //AHORA SE CORRE EL PROCESO QUE GENERA CHEQUES
            /*if(tipo.equals("2")){
                HExport_Migracion hch = new HExport_Migracion();
                hch.start("jmaldonado");
            }*/
            if(remesaMIMS.size()>0){
                ReporteC620XLS rcxls = new ReporteC620XLS();
                rcxls.start(idusuario,remesaMIMS );
            }
            if(planillaMIMS.size()>0){
                remesa_doctoDAO rem = new remesa_doctoDAO();
                rem.searchRemesaDoc(planillaMIMS);
                ReporteRemesasPlanillasXLS rpla = new ReporteRemesasPlanillasXLS();
                rpla.start(idusuario, rem.getVectorReporteRemPla());
            }
            
            model.LogProcesosSvc.finallyProceso("Generacion MST620", this.hashCode(), idusuario, "PROCESO EXITOSO");
            
            
        }catch(Exception ex){
            ex.printStackTrace();
            //logger.info("Error en el hilo HExport_MST620 : \n"+ex.getMessage());
            try{
                model.LogProcesosSvc.finallyProceso("Generacion MST620", this.hashCode(),idusuario,"ERROR :" + ex.getMessage());
                //logger.info("Error: "+ex);
            }catch ( SQLException e) {
                //////System.out.println("Error guardando el proceso");
            }
        }
        /*finally{
         
            stop();
         
        }*/
    }
    
    public String fill(String Cadena, int Longitud, int Tipo){
        String Str = Cadena;
        switch (Tipo){
            case 0:
                for(int i=Str.length();i<Longitud;i++, Str+=" ");
                break;
            case 1:
                if(!Cadena.equals("")){
                    if(Str.indexOf(".")>=0){
                        String resto = Str.substring(Str.indexOf(".")+1,Str.length());
                        if (resto.length()<2) resto += "0";
                        Str = Str.substring(0,Str.indexOf(".")) + resto;
                        for(int i=Str.length();i<Longitud;i++, Str="0"+Str);
                    }
                    else
                        for(int i=Str.length();i<Longitud;i++, Str="0"+Str);
                    
                    
                    
                }
                break;
            case 2:
                int cant = Longitud - Str.length();
                if(cant>0){
                    String ceros = "";
                    for(int i =1; i<=cant ; i++){
                        ceros = ceros +"0";
                    }
                    Str = ceros+Str;
                }
                break;
                
        }
        return Str.substring(0,Longitud);
    }
    
    
    public void GenerarArchivoPlaca(){
        
        try{
            String          Ruta       = ruta + "/exportar/migracion/"+this.idusuario+"/MST620_"+Fecha.replaceAll("-","")+".txt";
            String          Ruta600    = ruta + "/exportar/migracion/"+this.idusuario+"/M600.csv";
            String          Ruta200    = ruta + "/exportar/migracion/"+this.idusuario+"/M200.csv";
            
            /*String          Ruta       = "/MS230828_"+Fecha.replaceAll("-","")+".txt";
            String          Ruta600    = "/M600.csv";
            String          Ruta200    = "/M200.csv";*/
            
            int             PInicial   = 22;
            int             PFinal     = PInicial + 6;
            int             CInicial   = 43;
            int             CFinal     = CInicial + 8;
            long            Fila       = 1;
            
            FileInputStream fptr     = null;
            DataInputStream f        = null;
            String          Linea    = null;
            List            Placas   = new LinkedList();
            List            Placas2  = new LinkedList();
            
            String prop200="";
            String cond200="";
            
            
            fptr = new FileInputStream(Ruta);
            f    = new DataInputStream(fptr);
            do {
                Placa datos = new Placa();
                Linea = f.readLine();
                if(Linea!=null){
                    datos.setPlaca(Linea.substring(PInicial, PFinal));
                    datos.setConductor(Linea.substring(CInicial, CFinal));
                    Placas.add(datos);
                }
            }while(Linea!=null);
            fptr.close();
            
            //Recorro la lista Placa y saco las placas que no esten en oracle para adicionarlas
            //en una nueva lista llamada Placa2
            Iterator it = Placas.iterator();
            while(it.hasNext()) {
                Placa datos  = (Placa) it.next();
                if(!model.placaService.Existe_Placa_En_Oracle(datos.getPlaca()))
                    Placas2.add(datos);
            }
            
            if(Placas2.size()>0) {
                //Busco en Postgres los Datos de las placas que se encuantran en la lista Placa2
                PrintWriter fp = new PrintWriter(new BufferedWriter(new FileWriter(Ruta600)));
                Linea = "";
                
                
                it = Placas2.iterator();
                while(it.hasNext()) {
                    Placa datos  = (Placa) it.next();
                    Placa Registro = model.placaService.Buscar_Placa_Postgres(datos.getPlaca());
                    
                    Linea =
                    datos.getPlaca()               +","+
                    "SS"             +","+
                    "EQUIPO CARBON"             +","+
                    "DS"                                                             +","+ //(Registro.getEstadoequipo()!=null?Registro.getEstadoequipo():"") +","+
                    "SS0335"     +","+
                    "SM" +","+
                    "C99999981"                                                      +","+
                    (Registro.getTenedor()!=null?Registro.getTenedor():"")           +","+
                    (datos.getConductor()!=null?datos.getConductor():"")             +","+
                    (Registro.getSerial()!=null?Registro.getSerial():"")             +","+
                    (Registro.getTara()!=null?Registro.getTara():"")                 +","+
                    (Registro.getVolumen()!=null?Registro.getVolumen():"")           +","+
                    (Registro.getMarca()!=null?Registro.getMarca():"")               +","+
                    (Registro.getModelo()!=null?Registro.getModelo():"")             +","+
                    (Registro.getLargo()!=null?Registro.getLargo():"")               +","+
                    (Registro.getAlto()!=null?Registro.getAlto():"")                 +","+
                    (Registro.getLlantas()!=null?Registro.getLlantas():"")           +","+
                    (Registro.getEnganche()!=null?Registro.getEnganche():"")         +","+
                    " "                                                              +","+
                    " "                                                              +","+
                    "30"                                                             +","+
                    (Registro.getColor()!=null?Registro.getColor():"")               +","+
                    (Registro.getNoejes()!=null?Registro.getNoejes():"")             +","+
                    "FV"                                                             +","+
                    (Registro.getAncho()!=null?Registro.getAncho():"")               +","+
                    (Registro.getEmpresaafil()!=null?Registro.getEmpresaafil():"")   +","+
                    (Registro.getPiso()!=null?Registro.getPiso():"")                 +","+
                    (Registro.getCargue()!=null?Registro.getCargue():"")             +","+
                    " "                                                              +","+
                    " "                                                              +","+
                    "MOTOR"                                                          +","+
                    (Registro.getNomotor()!=null?Registro.getNomotor():"")           +","+
                    "CA"                                                             +",";
                    fp.println(Linea);
                }
                fp.close();
                
                
                /****************************************************************************/
                
                //Busco en Postgres los Datos de las placas que se encuantran en la lista Placa2
                fp = new PrintWriter(new BufferedWriter(new FileWriter(Ruta200)));
                Linea = "";
                
                it = Placas2.iterator();
                while(it.hasNext()) {
                    Placa datos  = (Placa) it.next();
                    Placa datos2 = model.placaService.Buscar_Pro_Con(datos.getPlaca());
                    ProConBen Registro = model.placaService.Buscar_Pro_Con_Ben_Ban(datos.getPlaca(), datos2.getPropietario(), datos.getConductor());
                    
                    Linea = (datos.getPlaca()!=null?datos.getPlaca():"")                     +","+
                    (datos2.getPropietario()!=null?datos2.getPropietario():"")                 +","+
                    (datos2.getConductor()!=null?datos2.getConductor():"")           +","+
                    (Registro.getDireccionP()!=null?Registro.getDireccionP():"")     +","+
                    " "                                                              +","+
                    (Registro.getNombreC()!=null?Registro.getNombreC():"")           +","+
                    (Registro.getDireccionC()!=null?Registro.getDireccionC():"")     +","+
                    " "                                                              +","+
                    (Registro.getTelefono()!=null?Registro.getTelefono():"")         +","+
                    (Registro.getBeneficiario()!=null?Registro.getBeneficiario():"") +","+
                    " "                                                              +","+
                    " "                                                              +","+
                    "PL"                                                             +","+
                    "FC"                                                             +","+
                    " "                                                              +","+
                    " "                                                              +","+
                    (Registro.getBanco()!=null?Registro.getBanco():"")               +","+
                    (Registro.getCuenta()!=null?Registro.getCuenta():"")             +","+
                    (Registro.getMoneda()!=null?Registro.getMoneda():"")             +",";
                    fp.println(Linea);
                }
                
                fp.close();
            }
            
        }
        catch(Exception e){
        }
    }
    public void generarMSO230(){
        
        try{
            String Ruta       = ruta + "/exportar/migracion/"+this.idusuario+"/MS230_"+Fecha.replaceAll("-","")+".csv";
            
            if(plarem.size()>0) {
                //Busco en Postgres los Datos de las placas que se encuantran en la lista Placa2
                PrintWriter fp = new PrintWriter(new BufferedWriter(new FileWriter(Ruta)));
                for(int i =0;i<plarem.size();i++ ){
                    
                    RemPla pr =(RemPla) plarem.elementAt(i);
                    //System.out.println("Numre");
                    fp.println(pr.getObservacion()+","+"001"+","+pr.getRemesa()+","+pr.getPorcent()+","+pr.getAccount_code_c());
                    
                }
                fp.close();
                
            }
            
        }
        catch(Exception e){
        }
    }
    //_____________________________________________________________________________________
    // FUNCIONES DE MIGRACION PROPIETARIO CONDUCTOR
    //______________________________________________________________________________________
    
    
    public void GenerarArchivoConductor(){
//        try{
//            //logger.info("Inicio");
//            String cedula="";
//            String placa="";
//            String remision ="";
//            fileReader = new FileReader(Path_FileTxt);
//            BufferedReader archivo = new BufferedReader(fileReader);
//            listado760  = new LinkedList();
//            listado810  = new LinkedList();
//            
//            //--- Recorremos el archivo
//            while( (linea=archivo.readLine()) !=null){
//                int longLine  = linea.length();
//                if(longLine>=this.posFishedCedula){
//                    //--- obtenemos las varibles del archivo
//                    int longCedula= 0;
//                    int longCedulaPR= 0;
//                    cedula    = linea.substring(posInitialCedula,posFishedCedula).trim();
//                    String cedula2    = cedula;
//                    String cedula2txt = cedula;
//                    placa     = linea.substring(posInitialPlaca,posFishedPlaca).trim();
//                    remision  = linea.substring(posInitialRemision,posFishedRemision).trim();
//                    String cedulaPropietario = model.MigracionConductorSvc.getCCPropietario(remision);
//                    String ccPR    = cedulaPropietario;
//                    String ccPRtxt = cedulaPropietario;
//                    //---  realizamos el relleno de espacios
//                    longCedula = cedula.length();
//                    if(longCedula<10)
//                        for(int i=1;i<=(10-longCedula);i++){
//                            cedula2="0"+cedula2;
//                        }
//                    
//                    longCedulaPR = cedulaPropietario.length();
//                    if(longCedulaPR<10)
//                        for(int i=1;i<=(10-longCedulaPR);i++){
//                            ccPR="0"+ccPR;
//                        }
//                    //--- realizamos la existencia de la cedula en MIMS  y adicionamos en las listas para los archivos de Excel
//                    if(!cedula.equals("")            && !model.MigracionConductorSvc.exist760(cedula2))    add760(cedula,"CO");
//                    if(!cedula.equals("")            && !model.MigracionConductorSvc.exist810(cedula2))    add810(cedula);
//                    if(!cedulaPropietario.equals("") && !model.MigracionConductorSvc.exist760(ccPR))       add760(cedulaPropietario,"PR");
//                    if(!cedulaPropietario.equals("") && !model.MigracionConductorSvc.exist810(ccPR))       add810(cedulaPropietario);
//                    
//                    //--- verificamos que la cedula del propietario exista en la MSF200, y insertamos en el archivo CreacionPlaca.TXT
//                    if( !cedulaPropietario.equals("")  &&  !model.MigracionConductorSvc.exist200(ccPR))
//                        this.lineaCreacion.println(placa +","+ccPRtxt +","+cedula2txt+","+remision );
//                    //--- Insertamos en el archivo Remision.TXT
//                    this.lineaRemision.println(remision +","+ cedulaPropietario);
//                }
//            }
//            this.saveTXT();
//            if(listado810.size()>0 || listado760.size()>0)
//                generarFile();
//            
//            //logger.info("\n\n Finalizado....  \n");
//        }catch(Exception e){
//            //logger.info("ERROR :" +e.getMessage());
//        }
//        
//        
    }
    
    public void iniVar(Model model, String fecha1){
        this.model      = model;
        this.listado760 =null;
        this.listado810 =null;
        this.fecha =  fecha1 ;
    }
    
    
    public void iniTxt(){
        try{
            // archivo  de lectura
            this.unidad= ruta + "/exportar/migracion/"+this.idusuario+"/";///+this.Fecha+"/"+base+"/";
            // this.unidad="/";
            
            this.name  ="MST620_"+ fecha;
            this.Path_FileTxt     = unidad + name  +".txt";
            //logger.info(Path_FileTxt+"\n");
            this.posInitialPlaca  = 22;
            this.posFishedPlaca   = posInitialPlaca + 12;
            this.posInitialCedula = 43;
            this.posFishedCedula  = posInitialCedula + 10;
            this.posInitialRemision = 0;
            this.posFishedRemision  = this.posInitialRemision + 8;
            
            
            // archivos de escrituras
            
            this.unidadWrite= ruta + "/exportar/migracion/"+this.idusuario+"/";
            // this.unidadWrite="/Excel/";
            this.nameFileRemision= unidadWrite + "RemProp"+ fecha +".txt";
            this.nameFileCreacion= unidadWrite + "PlacaPropCond"+fecha+".txt";
            
            
            this.fileCreacion    = new FileWriter(this.nameFileCreacion);
            this.bufferCreacion  = new BufferedWriter(this.fileCreacion);
            this.lineaCreacion   = new PrintWriter(this.bufferCreacion);
            
            this.fileRemision    = new FileWriter(this.nameFileRemision);
            this.bufferRemision  = new BufferedWriter(this.fileRemision);
            this.lineaRemision   = new PrintWriter(this.bufferRemision);
            
            
            this.fwC810      = new FileWriter(unidadWrite +"C810.csv");
            this.bfC810      = new BufferedWriter(this.fwC810);
            this.lineC810    = new PrintWriter(this.bfC810);
            
            this.fwC760      = new FileWriter(unidadWrite +"C760.csv");
            this.bfC760      = new BufferedWriter(this.fwC760);
            this.lineC760    = new PrintWriter(this.bfC760);
            
            
        }
        catch(Exception e){
            ////System.out.print("Enumerationiniciar variables TXT :" + e.getMessage());
        }
        
    }
    
    public void saveTXT(){
        this.lineaCreacion.close();
        this.lineaRemision.close();
    }
    
    
    public void save(){
        this.lineC810.close();
        this.lineC760.close();
    }
    
    
    
    
    public void add760(String cedula, String tipo){
        if(!existe(listado760, cedula)){
            MigracionConductorPropietario migracion = new MigracionConductorPropietario();
            migracion.format();
            migracion.setCedula(cedula);
            migracion.setTipo(tipo);
            listado760.add(migracion);
        }
    }
    
    public void add810(String cedula){
        if(!existe(listado810, cedula)){
            MigracionConductorPropietario migracion = new MigracionConductorPropietario();
            migracion.format();
            migracion.setCedula(cedula);
            listado810.add(migracion);
        }
    }
    
    
    
    // Funcion para Verificar la Existencia de la cedula en el listado
    public boolean existe(List lista, String id){
        boolean estado = false;
        if(lista!=null && lista.size()>0){
            Iterator it = lista.iterator();
            while(it.hasNext()){
                MigracionConductorPropietario migracion = (MigracionConductorPropietario) it.next();
                if(migracion.getCedula().equals(id)){
                    estado = true;
                    break;
                }
            }
        }
        return estado;
    }
    
    public void generarFile() throws Exception{
        try{
            
            
            //--- El listado de la 760
            Iterator it760 = this.listado760.iterator();
            if(it760!=null){
                ConductorService  Svc = new ConductorService();
                while(it760.hasNext()){
                    MigracionConductorPropietario migracion = (MigracionConductorPropietario)it760.next();
                    String posicion = (migracion.getTipo().equals("CO"))?"CONDUCTOR":"CONDUCTOR";
                    migracion.setPosicion(posicion);
                    //--- buscamos los datos en nit
                    Svc.searchNit(migracion.getCedula());
                    Nit nit = Svc.getNit();
                    if(nit!=null)
                        migracion = insertNit(migracion,nit);
                    //--- buscamos los datos de conductores
                    Svc.searchConductor( migracion.getCedula());
                    Conductor conductor = Svc.getConductor();
                    if(conductor!=null)
                        migracion = insertConductor(migracion, conductor);
                    
                    copy760(migracion,posicion);
                }
            }
            
            
            //--- El listado de la 810
            Iterator it810 = this.listado810.iterator();
            if(it810!=null){
                ConductorService  Svc = new ConductorService();
                while(it810.hasNext()){
                    MigracionConductorPropietario migracion = (MigracionConductorPropietario)it810.next();
                    //--- buscamos los datos en nit
                    Svc.searchNit(migracion.getCedula());
                    Nit nit = Svc.getNit();
                    if(nit!=null)
                        migracion = insertNit(migracion,nit);
                    copy810(migracion);
                }
            }
            
            this.save();
            
        }catch(Exception e){
            throw new Exception("Rutina generarExcel():"+ e.getMessage());
        }
        
    }
    
    
    
    public MigracionConductorPropietario insertNit(MigracionConductorPropietario migracion,Nit nit){
        if(migracion!=null && nit !=null){
            ////System.out.print(nit.getAddress() + nit.getAgCode() + nit.getName() + nit.getNit()  );
            migracion.setCiudad( nit.getAgCode());
            migracion.setDireccion( nit.getAddress());
            String genero = ( (nit.getSex()==null) || (nit.getSex().equals("")) )?"U":nit.getSex();
            migracion.setGenero( genero );
            migracion.setFechaNacimiento(nit.getFechaNacido());
            String name = nit.getName().trim();
            String vector[] = name.split(",");
            String nombre1="";
            String nombre2="";
            String nombre3="";
            String nombre4="";
            if(vector.length>0){
                nombre1=vector[0];
                nombre2=vector.length>1?vector[1]:"";
                nombre3=vector.length>2?vector[2]:"";
                nombre4=vector.length>3?vector[3]:"";
            }
            
            if(!nombre1.equals("")){
                migracion.setNombre1(nombre1);
                migracion.setNombre2(nombre2);
                migracion.setApellido1(nombre3);
                migracion.setApellido2(nombre4);
            }
            else{
                migracion.setNombre1(nit.getPnombre());
                migracion.setNombre2("");
                migracion.setApellido1("");
                migracion.setApellido2("");
            }
        }
        return migracion;
    }
    
    
    
    public MigracionConductorPropietario insertConductor(MigracionConductorPropietario migracion,Conductor conductor){
        if(migracion!=null && conductor!=null){
            migracion.setFechaIngreso(conductor.getInitDate());
            migracion.setFechaNacimiento( conductor.getDateBirth());
        }
        return migracion;
    }
    
    
    
    
    public void copy810( MigracionConductorPropietario migracion){
        String nombre1 = "";
        String apellido1="";
        
        if(!migracion.getNombre1().trim().equals("")){
            nombre1=migracion.getNombre1();
        }
        if(!migracion.getApellido1().trim().equals("")){
            apellido1 = migracion.getApellido1();
        }
        
        
        this.lineC810.println(migracion.getCedula()    + "," +
        
        apellido1 + "," +
        migracion.getApellido2() + "," +
        nombre1   + "," +
        migracion.getNombre2()   + "," +
        "0"+ "," +
        "SM"
        );
    }
    
    
    
    public void copy760( MigracionConductorPropietario migracion,String posicion){
        java.util.Date d = new java.util.Date();
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        c.set(c.DATE,c.DATE-1);
        d = c.getTime();
        java.text.SimpleDateFormat s = new java.text.SimpleDateFormat("dd/MM/yy");
        String hoy = s.format(d);
        String genero = "U";
        if(!migracion.getGenero().trim().equals("")){
            genero =migracion.getGenero();
        }
        this.lineC760.println( migracion.getCedula() + "," +
        "CONDUCTOR"             + "," +
        genero + "," +
        "CO"   + "," +
        "SM" + "," +
        "01/01/69" + "," +
        hoy
        );
    }
    
    
    public  String getFormatDate(String fecha){
        String formato="";
        if(!fecha.equals("")){
            String  ano = fecha.substring(2,4);
            String  mes = fecha.substring(5,7);
            String  dia = fecha.substring(8,10);
            formato     = dia+"/"+mes+"/"+ano;
        }
        return formato;
    }
    
    
    public static void main(String a [])throws Exception{
        try{
            Model model = new Model();
            String base = "COL";
            String tipo="";
            String Fecha ="2007-01-27";
            tipo = "1";
            //model.PlanillaImpresionSvc.searchPlanillas(Fecha, base);
            model.PlanillaImpresionSvc.searchPlanillas_CGAGEN(Fecha, base);
            Vector vec = model.PlanillaImpresionSvc.getPlas();
            HExport_MST620 hilo = new HExport_MST620();
            
            //190905
            hilo.start(Fecha, base, vec,model,tipo, "KREALES");
            
        }catch(SQLException e){
            //////System.out.println("Error "+e.getMessage());
        }
    }
    
    
}
