/*
 * RemesaService.java
 *
 * Created on 23 de noviembre de 2004, 09:11 AM
 */

package com.tsp.operation.model;


import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

/**
 *
 * @author  KREALES
 */

public class RemesaService {
    
    private RemesaDAO remesa;
    private List      planillaserror;
    private List      planillasImp;
    private String    Numrem;
    private String    Fecha;
    private String    AgenciaUsuario;
    private List      Registros;
    private PlanillaService plaservice;
    private List remesas;
    private String textoExtendido;
    
    /** Creates a new instance of RemesaService */
    public RemesaService() {
        remesa = new RemesaDAO();
        plaservice = new PlanillaService();
    }
    public RemesaService(String dataBaseName) {
        remesa = new RemesaDAO(dataBaseName);
        plaservice = new PlanillaService(dataBaseName);
    }
    
    public void buscaRemesa_no_impresa(String numrem,String usuario,int tipo)throws SQLException{
        try{
            Registros = remesa.BuscarRemesas_no_impresas( numrem, usuario,tipo);
        }
        catch(Exception e){
            throw new SQLException("Error en la rutina buscaRemesa_no_impresa [RemesaService]...\n"+e.getMessage());
        }
    }
    public List getRemesasList() {
        return this.remesas;
    }
    public void setRemesasList(List remesas) {
        this.remesas=remesas;
    }
    public List getRegistros() {
        return this.Registros;
    }
    
    public void Actualizar_Remesa_Impresa(String Remesa) throws SQLException {
        try {
            remesa.Actualizar_Remesa_Impresa(Remesa);
        }
        catch(SQLException e) {
            throw new SQLException("Error en la rutina Actualizar_Remesa_Impresa [RemesaService]...\n"+e.getMessage());
        }
    }
    
    public void setNumrem(String valor) {
        this.Numrem = valor;
    }
    
    public void setFecha(String valor) {
        this.Fecha = valor;
    }
    
    public void setAgenciaUsuario(String valor) {
        this.AgenciaUsuario = valor;
    }
    public String insertRemesa(Remesa rem, String base)throws SQLException{
        try{
            remesa.setRemesa(rem);
            return remesa.insertRem(base);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public List buscarPlanillas(String rem)throws SQLException{
        remesa.searchPlanillas(rem);
        return remesa.getPlanillas();
    }
    public String updateRemesa(Remesa rem)throws SQLException{
        try{
            remesa.setRemesa(rem);
            return remesa.updateRem();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public Remesa getRemesa()throws SQLException{
        
        return remesa.getRemesa();
        
    }
    public void buscaRemesa(String rem)throws SQLException{
        try{
            remesa.searchRemesa(rem);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void buscaRemesa(String rem,String login)throws SQLException{
        try{
            remesa.searchRemesa(rem,login);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public String anularRemesa(Remesa rem)throws SQLException{
        try{
            remesa.setRemesa(rem);
            return remesa.anularRem();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void buscarPlanillaError(String no_rem)throws SQLException{
        try{
            planillaserror=new LinkedList();
            List planillas= buscarPlanillas(no_rem);
            Iterator it=planillas.iterator();
            while (it.hasNext()){
                Planilla pla = (Planilla) it.next();
                String planilla = pla.getNumpla();
                List remesas = plaservice.buscarRemesas(planilla);
                if(remesas.size()<=1){
                    planillaserror.add(pla);
                }
            }
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public List getPlasError()throws SQLException{
        return planillaserror;
    }
    public Vector getRemesas()throws SQLException{
        return remesa.getRemesas();
    }
    public List getPlasImp()throws SQLException{
        return planillasImp;
    }
    public void buscarPlanillaImp(String no_rem)throws SQLException{
        try{
            planillasImp=new LinkedList();
            remesa.searchPlanillasImp(no_rem);
            planillasImp = remesa.getPlanillas();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void consultaRemesa(String fechafin, String fechaini,  String sj, String anulada, String cumplido, String nomcli, String base,String remision,String tipo_doc, String document)throws SQLException{
        try{
            remesa.consultaRemesa(fechafin, fechaini,  sj, anulada, cumplido, nomcli,base,remision,tipo_doc,document);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void datosRemesa(String numrem)throws SQLException{
        try{
            remesa.datosRemesa(numrem);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public boolean estaRemision(String remision)throws SQLException{
        return remesa.estaRemision(remision);
        
    }
    public void asignarPadre(Remesa r)throws SQLException{
        remesa.setRemesa(r);
        remesa.asignarPadre();
    }
    public String insertRemPadre(String base, Remesa rem)throws SQLException{
        remesa.setRemesa(rem);
        return remesa.insertRemPadre(base);
    }
    public boolean estaPadre(String remesaP )throws SQLException{
        return remesa.estaPadre(remesaP);
    }
    ///////////////////////////////////////////////
    public void buscarTextoExtendido(String Distrito, String Proyecto)throws SQLException{
        try{
            this.textoExtendido = remesa.TextoExtendido(Distrito,Proyecto);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public String getTextoExtendido(){
        return this.textoExtendido;
    }
    public Vector obtenerRemesas() throws SQLException {
        return remesa.obtenerRemesas();
    }
    
    public Vector obtenerRemesasDePlanilla(String numpla) throws SQLException {
        return remesa.obtenerRemesas(numpla);
    }
    //PLANEACION
    
    public java.util.Vector getRem() {
        return remesa.getR();
    }
    
    /**
     * Setter for property r.
     * @param r New value of property r.
     */
    public void setR(java.util.Vector r) {
        remesa.setR(r);
    }
    
    /**
     * Getter for property rm.
     * @return Value of property rm.
     */
    public Remesa getRm() {
        return remesa.getRm();
    }
    
    /**
     * Setter for property rm.
     * @param rm New value of property rm.
     */
    public void setRm(Remesa rm) {
        remesa.setRm(rm);
    }
    
    public void list( String numpla )throws SQLException{
        remesa.list(numpla);
    }
    
    
    public void setRemesa(Remesa rem){
        remesa.setRemesa(rem);
    }
    
    ///juanm
    public void buscaRemesa_no_impresaPlanillas(String Numpla)throws SQLException{
        try{
            Registros = remesa.BuscarRemesas_no_impresasPorPlanillas(Numpla);
        }
        catch(SQLException e){
            throw new SQLException("Error en la rutina buscaRemesa_no_impresa [RemesaService]...\n"+e.getMessage());
        }
    }
    ///JOSE 211005
    //Jose
    public void consultaRemesaDocumento(String rem) throws SQLException{
        try{
            remesa.consultaRemesa(rem);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void consultaRemesaDiscrepancia(String planilla, String remes, String tipo_doc, String documento, int num_discre )throws SQLException{
        try{
            remesa.consultaRemesaDiscrepancia(planilla, remes, tipo_doc, documento, num_discre );
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public Vector getRemes() throws Exception{
        return remesa.getRemesas();
    }
    
    //JOse 03.11.05
    public String remesaStandarJob(String remesas) throws SQLException{
        return remesa.remesaStandarJob(remesas);
    }
    /**
     *Metodo para buscar las remesas relacionadas a una planilla y a ninguna otra
     *@autor: Karen Reales
     *@param: numpla es el numreo de la planilla con la cual se relaciona la remesa
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarRemesasAlone(String numrem) throws SQLException{
        remesa.buscarRemesasAlone(numrem);
    }
    //Jose 05.12.05
    public void buscaRemesas( String numrem ) throws SQLException{
        try{
            remesa.buscaRemesa(numrem);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    
    /**
     *Metodo que setea un Vector con los Clientes de una remesa dada el nombre del Cliente
     *@autor:David Lamadrid
     *@param: String cliente
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void clientesRemesa( String cliente) throws SQLException{
        remesa.clientesRemesa(cliente);
    }
    
    
    /**
     * Getter for property vClientes.
     * @return Value of property vClientes.
     */
    public java.util.Vector getVClientes() {
        return remesa.getVClientes();
    }
    /**
     * Metodo consultarInfoRemesa, busca la informacion correspondiente a la remesa,
     * @param: remesa y dstrct
     * @autor : Ing. Diogenes Bastidas Morales
     * @see : buscarInfoRemesa-remesaDAO
     * @version : 1.0
     */
    public void consultarInfoRemesa(String rem, String dstrct )throws SQLException{
        remesa.consultarInfoRemesa(rem,dstrct);
    }
    /**
     *Metodo que busca la lista de remesas para el control de aduana
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void bucarRemesasAduana(String frontera, String cliente, String estado_tra, String faccia, String tipo) throws SQLException{
        remesa.bucarRemesasAduana(frontera, cliente,estado_tra, faccia, tipo);
    }
    public void setRemesas(java.util.Vector r) {
        remesa.setRemesas(r);
    }
    /**
     *Metodo que permite obtener una remesa dado el numero de la remesa
     *@autor: David Pi�a
     *@param numrem N�mero de la Remesa
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void getRemesaIC( String numrem ) throws SQLException {
        remesa.getRemesaIC( numrem );
    }
    
    /**
     *Metodo que permite buscar los tiempos de los viajes de una OC
     *@autor: Jose de la rosa
     *@param fecha inicial y fecha final
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public Vector obtenerTiemposRemesa( String fecini, String fecfin, String cliente, String tipo_viaje) throws SQLException {
        return remesa.obtenerTiemposRemesa( fecini, fecfin, cliente, tipo_viaje );
    }
    /**
     *Metodo que permite calcular el tiempo de viaje de una ruta
     *@autor: David Pi�a
     *@param ruta Codigo de la ruta
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public float calcularDuracion( String ruta )  throws SQLException {
        return remesa.calcularDuracion( ruta );
    }
    
    public void consultaRemesaDiscrepancia(String rem1, String numpla) throws SQLException{
        remesa.consultaRemesaDiscrepancia(rem1, numpla);
    }
    /**
     * Getter for property padre.
     * @return Value of property padre.
     */
    public com.tsp.operation.model.beans.Remesa getPadre() {
        return remesa.getPadre();
    }
    
    /**
     * Setter for property padre.
     * @param padre New value of property padre.
     */
    public void setPadre(com.tsp.operation.model.beans.Remesa padre) {
        remesa.setPadre(padre);
    }
    /*************************************************************
     *Metodo que obtiene las remesas que tienen asociada la
     * remesa padre con el numero dado
     *@autor: Osvaldo P�rez
     *@param: padre, numero de la remesa padre de la que se desea
     *        consultar sus 'hijas'
     *@return: Vector, vector con los numeros de remesa
     *@throws: En caso de que un error de base de datos ocurra.
     *************************************************************/
    public Vector obtenerRemesasHijas( String padre ) throws Exception {
        return remesa.obtenerRemesasHijas(padre);
    }
    
    // LREALES - 22 ENERO 2007
    /** Funcion publica que obtiene el metodo getVector del DAO */
    public Vector getVector() throws SQLException {
        
        return remesa.getVector();
        
    }
    
    /** Funcion publica que obtiene el metodo getOtro_vector del DAO */
    public Vector getOtro_vector() throws SQLException {
        
        return remesa.getOtro_vector();
        
    }
    
    /** Funcion publica que obtiene el metodo bucarTiposReexpedicion del DAO */
    public void bucarTiposReexpedicion() throws SQLException {
        
        remesa.bucarTiposReexpedicion();
        
    }
    
    /** Funcion publica que obtiene el metodo guardarPlacaTrailer del DAO */
    public void guardarPlacaTrailer( String platlr, String numpla ) throws SQLException {
        
        remesa.guardarPlacaTrailer( platlr, numpla );
        
    }
    
    /** Funcion publica que obtiene el metodo existeTipoReexpedicion del DAO */
    public boolean existeTipoReexpedicion( String distrito, String numrem ) throws SQLException {
        
        return remesa.existeTipoReexpedicion( distrito, numrem );
        
    }
    
    /** Funcion publica que obtiene el metodo existeAgenteExportacion del DAO */
    public boolean existeAgenteExportacion( String distrito, String numrem ) throws SQLException {
        
        return remesa.existeAgenteExportacion( distrito, numrem );
        
    }
    
    /** Funcion publica que obtiene el metodo existeAgenteImportacion del DAO */
    public boolean existeAgenteImportacion( String distrito, String numrem ) throws SQLException {
        
        return remesa.existeAgenteImportacion( distrito, numrem );
        
    }
    
    /** Funcion publica que obtiene el metodo existeAlmacenadora del DAO */
    public boolean existeAlmacenadora( String distrito, String numrem ) throws SQLException {
        
        return remesa.existeAlmacenadora( distrito, numrem );
        
    }
    
    /** Funcion publica que obtiene el metodo existeFacturaComercial del DAO */
    public boolean existeFacturaComercial( String distrito, String numrem, String documento ) throws SQLException {
        
        return remesa.existeFacturaComercial( distrito, numrem, documento );
        
    }
    
    /** Funcion publica que obtiene el metodo insertarTipoReexpedicion del DAO */
    public void insertarTipoReexpedicion( String distrito, String numrem, String tipo_reexp, String usu, String base ) throws SQLException {
        
        remesa.insertarTipoReexpedicion( distrito, numrem, tipo_reexp, usu, base );
        
    }
    
    /** Funcion publica que obtiene el metodo insertarAgenteExportacion del DAO */
    public void insertarAgenteExportacion( String distrito, String numrem, String ageexp, String usu, String base ) throws SQLException {
        
        remesa.insertarAgenteExportacion( distrito, numrem, ageexp, usu, base );
        
    }
    
    /** Funcion publica que obtiene el metodo insertarAgenteImportacion del DAO */
    public void insertarAgenteImportacion( String distrito, String numrem, String ageimp, String usu, String base ) throws SQLException {
        
        remesa.insertarAgenteImportacion( distrito, numrem, ageimp, usu, base );
        
    }
    
    /** Funcion publica que obtiene el metodo insertarAlmacenadora del DAO */
    public void insertarAlmacenadora( String distrito, String numrem, String almacenadora, String usu, String base ) throws SQLException {
        
        remesa.insertarAlmacenadora( distrito, numrem, almacenadora, usu, base );
        
    }
    
    /** Funcion publica que obtiene el metodo insertarFacturaComercial del DAO */
    public void insertarFacturaComercial( String distrito, String numrem, String factcial, String coddest, String usu ) throws SQLException {
        
        remesa.insertarFacturaComercial( distrito, numrem, factcial, coddest, usu );
        
    }
    
    /** Funcion publica que obtiene el metodo actualizarTipoReexpedicion del DAO */
    public void actualizarTipoReexpedicion( String tipo_reexp, String usu, String distrito, String numrem ) throws SQLException {
        
        remesa.actualizarTipoReexpedicion( tipo_reexp, usu, distrito, numrem );
        
    }
    
    /** Funcion publica que obtiene el metodo actualizarAgenteExportacion del DAO */
    public void actualizarAgenteExportacion( String ageexp, String usu, String distrito, String numrem ) throws SQLException {
        
        remesa.actualizarAgenteExportacion( ageexp, usu, distrito, numrem );
        
    }
    
    /** Funcion publica que obtiene el metodo actualizarAgenteImportacion del DAO */
    public void actualizarAgenteImportacion( String ageimp, String usu, String distrito, String numrem ) throws SQLException {
        
        remesa.actualizarAgenteImportacion( ageimp, usu, distrito, numrem );
        
    }
    
    /** Funcion publica que obtiene el metodo actualizarAlmacenadora del DAO */
    public void actualizarAlmacenadora( String almacenadora, String usu, String distrito, String numrem ) throws SQLException {
        
        remesa.actualizarAlmacenadora( almacenadora, usu, distrito, numrem );
        
    }
    
    /** Funcion publica que obtiene el metodo actualizarFacturaComercial del DAO */
    public void actualizarFacturaComercial( String factcial, String usu, String distrito, String numrem, String factvieja ) throws SQLException {
        
        remesa.actualizarFacturaComercial( factcial, usu, distrito, numrem, factvieja );
        
    }
    
    /** Funcion publica que obtiene el metodo bucarDestinatariosDeUnCliente del DAO */
    public void bucarDestinatariosDeUnCliente( String codcli ) throws SQLException {
        
        remesa.bucarDestinatariosDeUnCliente( codcli );
        
    }
    
    /** Funcion publica que obtiene el metodo bucarDestinatariosDeUnaRemesa del DAO */
    public void bucarDestinatariosDeUnaRemesa( String numrem ) throws SQLException {
        
        remesa.bucarDestinatariosDeUnaRemesa( numrem );
        
    }
    
    /** Funcion publica que obtiene el metodo eliminarDestinatariosDeUnaRemesa del DAO */
    public void eliminarDestinatariosDeUnaRemesa( String numrem ) throws SQLException {
        
        remesa.eliminarDestinatariosDeUnaRemesa( numrem );
        
    }
    
    /** Funcion publica que obtiene el metodo insertarNuevosDestinatarios del DAO */
    public void insertarNuevosDestinatarios( String distrito, String codigo, String numrem, String usu, String base ) throws SQLException {
        
        remesa.insertarNuevosDestinatarios( distrito, codigo, numrem, usu, base );
        
    }
    
    /** Funcion publica que obtiene el metodo bucarRemesasParaModificar del DAO */
    public void bucarRemesasParaModificar( String factcial ) throws SQLException {
        
        remesa.bucarRemesasParaModificar( factcial );
        
    }
    
    public boolean existeTrailer(String placa)throws SQLException {
        
        return remesa.existeTrailer(placa);
        
    }
    
    /**
     *Metodo que permite buscar los tiempos de los viajes de una OC
     *@autor: Jose de la rosa
     *@param fecha inicial, fecha final, cliente y si estan cumplidas
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void consultaRemesasPagador( String fecini, String fecfin, String cliente, String cumplidas, String distrito) throws SQLException {
        remesa.consultaRemesasPagador(fecini, fecfin, cliente, cumplidas, distrito);
    }
    
    /**
     *Metodo que permite actualizar el campo pagador de la remesa
     *@autor: Jose de la rosa
     *@param fecha inicial, fecha final, cliente y si estan cumplidas
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String updateCod_pagador( String numrem, String cod_pagador )throws SQLException{
        return remesa.updateCod_pagador(numrem, cod_pagador);
    }
    
    public String obtenerClienteRemesa( String numrem ) throws SQLException {
        return remesa.obtenerClienteRemesa(numrem);
    }
    
    public String anularMovrem(String numrem, Usuario usuario) throws SQLException {
        return remesa.anularMovrem(numrem, usuario);
    }
    
    //
    
}