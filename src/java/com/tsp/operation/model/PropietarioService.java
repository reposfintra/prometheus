/*
 * PropietarioService.java
 *
 * Created on 14 de junio de 2005, 10:15 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;

/**
 *
 * @author  Sandrameg
 */
public class PropietarioService {
    
    private PropietarioDAO prop;
    /** Creates a new instance of PropietarioService */
    public PropietarioService() {
        prop = new PropietarioDAO();
    }
    public PropietarioService(String dataBaseName) {
        prop = new PropietarioDAO(dataBaseName);
    }
    
    public Propietario getPropietario(){
        return prop.getPropietario();
    }
    
    public void insert(Propietario p)throws SQLException{
        prop.setPropietario(p);
        prop.insert();        
    }
    
    public void update(Propietario p)throws SQLException{
        prop.setPropietario(p);
        prop.update();   
    }
    
    public void nullify(Propietario p)throws SQLException{
        prop.setPropietario(p);
        prop.nullify();   
    }
    
    public void buscar(String distrito)throws SQLException{
        prop.buscar(distrito);
    }
    
    public void buscarxCedula(String cedula)throws SQLException{
        prop.buscarxCedula(cedula);
    } 
    
    public void buscarxCedulaxDistrito(String cedula, String distrito)throws SQLException{
        prop.buscarxCedulaxDistrito(cedula, distrito);
    } 
    
    public NitSot buscarPropietarioNIT(String id_mims)throws SQLException{
        return prop.buscarPropietarioNIT(id_mims);
    }
    
    ////Modificacion x placa 151005
     public void buscarxCedulaNit(String cedula)throws SQLException{
        prop.buscarxCedulaNit(cedula);
    } 
}
