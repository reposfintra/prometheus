/*
 * BDOracleDAO.java
 *
 * Created on 20 de junio de 2005, 11:17 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import javax.swing.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  Jm
 */
public class BDOracleDAO {
    BDOracle bd;
    Vector b;
    /** Creates a new instance of BDOracleDAO */
    public BDOracleDAO() {
    }
    
    /**
     * Getter for property b.
     * @return Value of property b.
     */
    public java.util.Vector getB() {
        return b;
    }
    
    /**
     * Setter for property b.
     * @param b New value of property b.
     */
    public void setB(java.util.Vector b) {
        this.b = b;
    }
    
    /**
     * Getter for property bd.
     * @return Value of property bd.
     */
    public BDOracle getBd() {
        return bd;
    }
    
    /**
     * Setter for property bd.
     * @param bd New value of property bd.
     */
    public void setBd(BDOracle bd) {
        this.bd = bd;
    }
    public void list( String recurso )throws SQLException{
        String consulta = "select ASSOC_REC from prdmoe43.MSF010 where  Table_Type = '+ER' and Table_Code = ? ";
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("oracle");
            if(con!=null){
                st = con.prepareStatement(consulta);
                st.setString(1, recurso);
                rs= st.executeQuery();
                while(rs.next()){
                    bd = new BDOracle();
                    bd.setAsso_rec(rs.getString(1));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO OBTENCION DE INFORMACION DESDE ORACLE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("oracle", con);
            }
        }
        
    }
}
