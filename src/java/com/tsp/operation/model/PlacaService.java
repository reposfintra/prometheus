/************************************************************************
 * Nombre clase: PlacaService.java                                      *
 * Descripci�n: Clase que maneja los servicios al model relacionados    *
 *              con las placas                                          *
 * Autor: Desconocido                                                   *
 * Fecha: Created on 2 de noviembre de 2004, 05:05 PM                   *
 * Versi�n: Java 1.0                                                    *
 * Copyright: Fintravalores S.A. S.A.                              *
 ***********************************************************************/

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

public class PlacaService {
    
    private PlacaDAO placaDataAccess;
    private Placa placa_bean;

    /** Creates a new instance of PlacaService */
    public PlacaService () {
        placaDataAccess = new PlacaDAO ();
    }

    /**
    * Metodo Existe_Placa_En_Oracle, permite buscar una placa en oracle.
    * @autor : Ing. Miguel Angel
    * @see Existe_Placa_En_Oracle - PlacaDAO
    * @param : Placa
    * @version : 1.0
    */
    public boolean Existe_Placa_En_Oracle (String Placa) throws SQLException {
        boolean sw = false;
        try{
            sw = placaDataAccess.Buscar_Placa_Oracle (Placa);
        }
        catch(SQLException e){
            throw new SQLException ("Error en Existe_Placa [PlacaService]... \n"+e.getMessage ());
        }
        return sw;
    }

    /**
    * Metodo Buscar_Pro_Con, permite buscar un propietario.
    * @autor : Ing. Miguel Angel
    * @see Existe_Placa_En_Oracle - PlacaDAO
    * @param : Placa
    * @version : 1.0
    */
    public Placa Buscar_Pro_Con (String Placa) throws SQLException {
        Placa Registro = null;
        try {
            Registro = placaDataAccess.Buscar_Pro_Con (Placa);
        }
        catch(SQLException e){
            throw new SQLException ("Error en Buscar_Pro_Con_Ben_Ban [PlacaService]... \n"+e.getMessage ());
        }
        return Registro;
    }
    
    /**
    * Metodo Buscar_Pro_Con_Ben_Ban.
    * @autor : Desconocido
    * @see Buscar_Pro_Con_Ben_Ban - PlacaDAO
    * @param : Placa
    * @version : 1.0
    */
    public ProConBen Buscar_Pro_Con_Ben_Ban (String Placa, String Propietario, String Conductor) throws SQLException {
        ProConBen Registros = null;
        try {
            Registros = placaDataAccess.Buscar_Pro_Con_Ben_Ban (Placa, Propietario, Conductor);
        }
        catch(SQLException e){
            throw new SQLException ("Error en Buscar_Pro_Con_Ben_Ban [PlacaService]... \n"+e.getMessage ());
        }
        return Registros;
    }

    /**
    * Metodo Buscar_Placa_Postgres, permite buscar una placa en postgres.
    * @autor : Desconocido
    * @see Buscar_Placa_Postgres - PlacaDAO
    * @param : Placa
    * @version : 1.0
    */
    public Placa Buscar_Placa_Postgres (String Placa) throws SQLException{
        Placa Registro = null;
        try {
            Registro = placaDataAccess.Buscar_Placa_Postgres (Placa);
        }
        catch(SQLException e){
            throw new SQLException ("Error en Buscar_Placa_Postgres [PlacaService]... \n"+e.getMessage ());
        }
        return Registro;
    }
    
    
    /**
    * Metodo placaExist, permite saber si existe una placa.
    * @autor : Desconocido
    * @see placaExist - PlacaDAO
    * @param : Placa
    * @version : 1.0
    */
    public boolean placaExist (String noplaca) throws SQLException{
        boolean sw = false;
        try{
            sw = placaDataAccess.placaExist (noplaca);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
        return sw;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public Placa getPlaca ( ) {        
        return placaDataAccess.getPlaca ();        
    }
    
    /**
     * Setter for property placa.
     * @param veh New value of property placa.
     */
    public void setPlaca (Placa placa ) {        
        placaDataAccess.setPlaca(placa);        
    }
    
    /**
    * Metodo buscaPlaca, permite buscar los datos de una placa.
    * @autor : Desconocido
    * @see buscaPlaca - PlacaDAO
    * @param : Placa
    * @version : 1.0
    */
    public void buscaPlaca (String placa_no)throws SQLException{
        try{
            placaDataAccess.searchPlaca (placa_no);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
        
    }
    
    /**
    * Metodo buscaPlaca2, permite buscar los datos de una placa.
    * @autor : Desconocido
    * @see buscaPlaca2 - PlacaDAO
    * @param : Placa
    * @version : 1.0
    */
    public void buscaPlaca2 (String placa_no)throws SQLException{
        try{
            placaDataAccess.buscar (placa_no);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
        
    }
    
    /**
    * Metodo insertaPlaca, permite ingresar los datos de una placa a la BD.
    * @autor : Desconocido
    * @see insertaPlaca - PlacaDAO
    * @param : Objeto Placa
    * @version : 1.0
    */
    public void insertaPlaca (Placa placa) throws SQLException{
        try{
            placaDataAccess.setPlaca (placa);
            placaDataAccess.insertar ();
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
    /**
    * Metodo actualizaPlaca, permite actualizar los datos de una placa a la BD.
    * @autor : Desconocido
    * @see actualizaPlaca - PlacaDAO
    * @param : Objeto placa
    * @version : 1.0
    */
    public void actualizaPlaca (Placa placa) throws SQLException{
        try{
            placaDataAccess.actualizar (placa);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
     /**
    * Metodo actualizaPlaca, permite actualizar los datos de una placa a la BD y guarda historial de la modificacion de propietario.
    * @autor : Ing. Enrique De Lavalle.
    * @see actualizaPlaca - PlacaDAO
    * @param : Objeto placa
    * @version : 1.0
    */
   public void actualizarPlacaRegPropietarioPlaca(Placa placa, String nitP, String nombreP) throws SQLException{
         try{
            placaDataAccess.actualizar(placa, nitP, nombreP);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
   }
    
    
    
    /**
    * Metodo nombreNit, permite obtener el nombre de un usuario dada una cedula.
    * @autor : Desconocido
    * @see nombreNit - PlacaDAO
    * @param : nit
    * @version : 1.0
    */
    public String nombreNit (String cedula) throws SQLException{
        return placaDataAccess.nombreNit (cedula);
    }
    
    /**
    * Metodo nitExist, permite saber si el nit existe en la BD.
    * @autor : Desconocido
    * @see nitExist - PlacaDAO
    * @param : nit
    * @version : 1.0
    */
    public boolean nitExist (String nit) throws SQLException{
        boolean sw = false;
        try{
            sw = placaDataAccess.nitExist (nit);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
        return sw;
    }
    
    /**
    * Metodo nombreProp, permite obtener el nombre de un propietario dada una cedula.
    * @autor : Desconocido
    * @see nombreProp - PlacaDAO
    * @param : cudula
    * @version : 1.0
    */
    public String nombreProp (String cedula) throws SQLException{
        return placaDataAccess.nombreProp (cedula);
    }

    /**
    * Metodo placaVetado, permite saber si la placa se encuentra vetada.
    * @autor : Desconocido
    * @see placaVetado - PlacaDAO
    * @param : placa
    * @version : 1.0
    */
    public boolean placaVetado (String placa) throws SQLException{
        return placaDataAccess.placaVetado (placa);
    }
    
    /**
     * Getter for property vehiculos.
     * @return Value of property vehiculos.
     */
    public Placa getV () {
        return placaDataAccess.getVeh ();
    }
    
    /**
     * Setter for property veh.
     * @param veh New value of property veh.
     */
    public void setV (Placa veh) {
        placaDataAccess.setVeh (veh);
    }
    
    /**
     * Getter for property vehiculos.
     * @return Value of property vehiculos.
     */
    public java.util.Vector getVehiculos () {
        return placaDataAccess.getVehiculos ();
    }
    
    /**
     * Setter for property vehiculos.
     * @param vehiculos New value of property vehiculos.
     */
    public void setVehiculos (java.util.Vector vehiculos) {
        placaDataAccess.setVehiculos (vehiculos);
    }
    
    /**
    * Metodo listVeh, permite obtener el numero de placa y el recurso dado una parametro.
    * @autor : Desconocido
    * @see listVeh - PlacaDAO
    * @param : plaveh
    * @version : 1.0
    */
    public void listVeh ( String plaveh )throws SQLException{
        placaDataAccess.listVeh (plaveh);
    }
    
    /**
    * Metodo reiniciarPlaca, coloca el objeto de placa en null.
    * @autor : Desconocido
    * @see reiniciarPlaca - PlacaDAO
    * @param : placa
    * @version : 1.0
    */
    public void reiniciarPlaca (){
        placaDataAccess.reiniciar ();
    }
    
    /**
    * Metodo isTrailer, permite saber si la placa es un trailer.
    * @autor : Desconocido
    * @see isTrailer - PlacaDAO
    * @param : plaveh
    * @version : 1.0
    */
    public boolean isTrailer ( String plaveh )throws SQLException{
        return placaDataAccess.isTrailer (plaveh);
    }
    
    /**
    * Metodo isTrailer, permite saber si la placa es una tractomula.
    * @autor : Desconocido
    * @see isTrailer - PlacaDAO
    * @param : plaveh
    * @version : 1.0
    */
    public boolean isTractomula ( String plaveh )throws SQLException{
        return placaDataAccess.isTractomula (plaveh);
    }
    
    /**
    * Metodo actualizarCond, funcion que permite actualizar la cedula de conductor de una placa.
    * @autor : Desconocido
    * @see actualizarCond - PlacaDAO
    * @param : cedula conductor, placa
    * @version : 1.0
    */
    public void actualizarCond (String cedcon, String placa) throws SQLException{
        placaDataAccess.actualizarCond (cedcon, placa);
    }
    
    /**
    * Metodo buscarConductorPropietario, permite una busqueda de placas por cedula conductor o propietario.
    * @autor : Henry Osorio
    * @see buscarConductorPropietario - PlacaDAO
    * @param : cedula conductor, placa
    * @version : 1.0
    */
    public void buscarConductorPropietario (String busq, String cedula) throws SQLException{
        placaDataAccess.buscarPlacasConductor (busq, cedula);
    }
    
    /**
     * Setter for property id.
     * @param vehiculos New value of property id.
     */
    public void vPlacas (String id) throws SQLException{
        placaDataAccess.vPlacas (id);
    }
    
    /**
     * Getter for property id.
     * @return Value of property id.
     */
    public java.util.Vector getVPlacas (){
        return    placaDataAccess.getVPlacas ();
    }
    
    /**
     * Metodo vPlacas , Metodo que retorna un Vector de Vectores con las placas dado Un Striing con el caracter inicial de cualquier placa
     * @autor : Ing. David Lamadrid
     * @param : String id
     * @version : 1.0
     */
    public void setVPlacas (java.util.Vector vPlacas){
        placaDataAccess.setVPlacas (vPlacas);
    }
    
    
    /**
     * Metodo vPlacas , Metodo que retorna un Vector de Vectores con las placas dado Un Striing con el caracter inicial de cualquier placa
     * @autor : Ing. David Lamadrid
     * @param : String id
     * @version : 1.0
     */
    public boolean existePlaca (String id) throws SQLException{
        return placaDataAccess.existePlaca (id);
    }
    /**
     * Metodo archivoTxt , Metodo que llama una funcion que escribe un archivo txt con los registros de la tabla Placa
     * @autor : Ing. Leonardo Parody
     * @param : String ruta, String usuario, String campo,String tabla,String placa
     * @version : 1.0
     */
    public void archivoTxt (String ruta, String placa, String tabla, Usuario usuario, String campo) throws SQLException{
        try{
            String base = usuario.getBase ();
            ////System.out.println ("Vamos pal DAO");
            placaDataAccess.consultaTxt (ruta, base, tabla, placa, campo);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    /**
     * Obtiene el nombre de las columnas de la tabla placa
     * @autor Ing. Tito Andr�s Maturana
     * @version 1.0
     * @throws SQLException Si se presenta un error en la conexion con la base de datos.
     */
    public Vector obtenerNombresCamposPlaca () throws SQLException{
        Vector cols = new Vector ();
        
        ResultSetMetaData rsmd = placaDataAccess.obtenerMetadata ();
        
        for( int i=1; i<=rsmd.getColumnCount (); i++){
            cols.add (rsmd.getColumnName (i));
        }
        
        return cols;
    }
    
    /**
     * listarPlacaNoAprobada, lista las placa no aprobados de acuerdo al usuario
     * asignado aprobar las placas.
     * @autor Ing. Diogenes Bastidas Morales
     * @version 1.0
     * @see: listarPlacaNoAprobada - PlacaDAO
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public void listarPlacaNoAprobada (String login )throws SQLException{
        placaDataAccess.listarPlacaNoAprobada (login);
    }
    
    /**
     * aprobarRegistrio, aprueba los registros de la tabla placa
     * @autor Ing. Diogenes Bastidas Morales
     * @param: usuario, estado, placa
     * @see: aprobarRegistrio - PlacaDAO
     * @version 1.0
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public void aprobarRegistrio (String usuario, String estado, String placa ) throws SQLException{
        try{
            placaDataAccess.aprobarRegistrio (usuario, estado, placa);
        }
        catch(Exception e){
            throw new SQLException (e.getMessage ());
        }
    }
    
     /**
     * buscaPlacaTipo, busca tda la informacion en la tabla de placa
     * @autor Ing. Jose de la rosa
     * @param: placa, tipo
     * @see: buscaPlacaTipo - PlacaDAO
     * @version 1.0
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public void buscaPlacaTipo (String placa_no, String tipo)throws SQLException{
        try{
            placaDataAccess.buscarTipo (placa_no, tipo);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
        
    }
    
    /**
     * buscaTipoXPlaca, busca el tipo de una placa
     * @autor Ing. Andres Martinez
     * @param: placa
     * @version 1.0
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public String buscaTipoXPlaca (String placa_no)throws SQLException{
        String tipo="";
        try{
            tipo= placaDataAccess.buscaTipoXPlaca(placa_no);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
        return tipo;
    }
    
    
    
     /**
     * obtenerTreeMapLista, permite generar un objeto TreeMap dada una lista.
     * @autor Ing. Henry Osorio
     * @param: Lista LinkedList
     * @see: obtenerTreeMapLista - PlacaDAO
     * @version 1.0
     * @throws.
     */
    public TreeMap obtenerTreeMapLista(LinkedList tblmod){        
        TreeMap modelos = new TreeMap();
         for(int i = 0; i<tblmod.size(); i++){
             TablaGen tblm = (TablaGen) tblmod.get(i);	
             modelos.put (tblm.getDescripcion(),tblm.getTable_code());
         }
        return modelos;
    }
    /**
     * Metodo cambiarEstadoPlaca, cambia el estado, de una placa
     * Activo o Inactivo
     * param: placa, estado
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void cambiarEstadoPlaca(String doc, String estado )throws Exception {
        placaDataAccess.cambiarEstadoPlaca(doc, estado);
    }
    
     /**
     * Metodo buscarPlacaCGA, metodo que busca la placa y setea en el 
     * objeto solo el numero de la placa los nombres de propietario y conductor, el estado 
     * param: placa, estado
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarPlacaCGA(String placa) throws SQLException{
        placaDataAccess.buscarPlacaCGA(placa);
    }
    
    /**
     * Getter for property placa_bean.
     * @return Value of property placa_bean.
     */
    public com.tsp.operation.model.beans.Placa getPlaca_bean() {
        return placa_bean;
    }    
    
    /**
     * Setter for property placa_bean.
     * @param placa_bean New value of property placa_bean.
     */
    public void setPlaca_bean(com.tsp.operation.model.beans.Placa placa_bean) {
        this.placa_bean = placa_bean;
    }
    
    /**
     * buscarPlaca, busca tda la informacion en la tabla de placa
     * @autor Ing. Osvaldo P�rez Ferrer
     * @param: placa
     * @version 1.0
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public void buscarPlaca (String placa_no)throws SQLException{
        try{
            placaDataAccess.buscarPlaca(placa_no);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
        
    }
    
    /**
     * EliminarPlaca, 
     * @autor Ing. Luigi
     * @param: placa
     * @version 1.0
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public void eliminarPlaca (String placa_no)throws SQLException{
        try{
            placaDataAccess.eliminarPlaca(placa_no);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
        
    }
    
    /**
     * buscarEstadoPlaca, 
     * @autor Ing. Luigi
     * @param: placa
     * @version 1.0
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public String buscarEstadoPlaca (String placa_no)throws SQLException{
        try{
            return placaDataAccess.buscarEstadoPlaca(placa_no);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
        
    }
    
    /**
     * CambiarEstadoPlaca, 
     * @autor Ing. Luigi
     * @param: placa
     * @version 1.0
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public void cambiarEstadoPlacaAnulado (String placa_no)throws SQLException{
        try{
            placaDataAccess.cambiarEstadoPlacaAnulado(placa_no);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
        
    }
    
          /**
     * isGrupoDespacho, retorna true o false, si el grupo de la placa puede despachar o no
     * @autor Ing. Karen Reales
     * @param: cliente y grupo
     * @version 1.0
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public boolean isGrupoDespacho(String cliente, String grupo) throws SQLException{
        return placaDataAccess.isGrupoDespacho(cliente,grupo) ;
    }


     /**
     * verifica si existe el recurso ingresado para la placa, 
     * @autor Ing. Enrique De Lavalle
     * @param: placa
     * @version 1.0
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public boolean verificarRecurso (String idRecurso)throws SQLException{
        try{
            return (placaDataAccess.existeRecurso(idRecurso));
        }
        catch(SQLException e){
            e.printStackTrace();
            throw new SQLException (e.getMessage ());
        }
    }
    
    /**
     * Metodo q obtiene el tipo de placa de un recurso
     * @autor Ing. Enrique De Lavalle
     * @param: placa
     * @version 1.0
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public String obtenerTipoPlacaRecurso (String idRecurso)throws SQLException{
        try{
            return (placaDataAccess.obtenerTipoPlacaRecurso(idRecurso));
        }
        catch(SQLException e){
            e.printStackTrace();
            throw new SQLException (e.getMessage ());
        }
    }
    
    
    
}
