/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model;

/**
 *
 * @author aariza
 */

import java.util.ArrayList;
import com.tsp.operation.model.DAOS.ListaPoliticasReporteDAO;
import com.tsp.operation.model.beans.Politicas;
import com.tsp.operation.model.beans.PreReporteCentralRiesgo;
import com.tsp.operation.model.beans.listaPoliticaBeans;

public class ListaPoliticasReporteService {
    ListaPoliticasReporteDAO lpr;
    
    public ListaPoliticasReporteService(String dataBaseName) {
        lpr = new ListaPoliticasReporteDAO(dataBaseName);
    }
    
    public ArrayList<listaPoliticaBeans> getPoliticas()throws Exception{
  
       return lpr.getPoliticas();
    }
    
    public ArrayList getNomCovenios(String entidad)throws Exception{
    
    return lpr.getNomConvenios(entidad);
    }
    
    
    public ArrayList getEntidades() throws Exception{
    
    return lpr.getEntidades();
    }
    public ArrayList getConvenios() throws Exception{
    return lpr.getConvenios();
    }
    
   public ArrayList getEntidadConvenio() throws Exception{
   return lpr.getEntidadConvenio();
   }
    
    public String insertarPolitica(Politicas politica)throws Exception{
       return lpr.insertPolitica(politica);
    }
    
    public String insertEntidadConvenio(int entidad, int convenio)throws Exception{
    
    return lpr.insertEntidadConvenio(entidad, convenio);
    }
    
     public String insertEntidad(String entidad)throws Exception{
    
    return lpr.insertEntidad(entidad);
    }
    
    public void updatePolitica(Politicas politica, int idPolitica)throws Exception{
        lpr.updatePolitica(politica, idPolitica);
    
    }
    
    public String deletePolitica(int idPolitica)throws Exception{
       return lpr.deletePolitica(idPolitica);
    
    }
     public String deleteEntidadConvenio(int id)throws Exception{
       return lpr.deleteEntidadConvenio(id);
    
    }
    
     public ArrayList<PreReporteCentralRiesgo> getPreReporte(String rango1, String rango2, String convenio )throws Exception{
      return lpr.getPreReporte(rango1, rango2, convenio);
    }
     
       ArrayList resultado;

    public ArrayList getResultado() {
        return resultado;
    }

    public void setResultado(ArrayList resultado) {
        this.resultado = resultado;
    }

    public void getConsulta(String parametro, String rango1, String rango2, String convenio)throws Exception{
         this. setResultado(lpr.getConsulta(parametro,rango1,rango2,convenio));
      
    }
     
    //metodo para obtener los rangos de vencimiento.
    public String getVencimientosPoliticas(String politica, String central_riesgo)throws Exception{
     
        return lpr.getRangosVencimientos(politica, central_riesgo);
    
}
    
     //metodo para validar si es el primer reporte.
    public String getPrimerReporte(String parametro)throws Exception{
       
        return lpr.getPrimerReporte(parametro);
    
    }
    
     //metodo para validar si es el primer reporte.
    public void actulizarReporte(String parametro)throws Exception{
       
       lpr.actualizarReporte(parametro);
    
    }
     
     
     
     
    
}
