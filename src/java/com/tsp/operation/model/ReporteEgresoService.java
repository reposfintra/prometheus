/*
 * ReporteEgresoService.java
 *
 * Created on 13 de septiembre de 2005, 10:34 AM
 */

package com.tsp.operation.model;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
//import java.awt.*;
import java.util.*;


/**
 *
 * @author  Administrador
 */
public class ReporteEgresoService {
    private ReporteEgresoDAO   REDataAccess;
    private List               ListaRE;
    private List               ListaBancos;
    private List               ListaChkNomigrados;
    private ReporteEgreso      Datos;
    
    /** Creates a new instance of ReporteEgresoService */
    public ReporteEgresoService() {
        REDataAccess = new ReporteEgresoDAO();
        ListaRE = new LinkedList();
        ListaBancos = new LinkedList();
        ListaChkNomigrados = new LinkedList();
        Datos = new ReporteEgreso();
    }
    public ReporteEgresoService(String dataBaseName) {
        REDataAccess = new ReporteEgresoDAO(dataBaseName);
        ListaRE = new LinkedList();
        ListaBancos = new LinkedList();
        ListaChkNomigrados = new LinkedList();
        Datos = new ReporteEgreso();
    }
    
    
    
    
    public void LISTCHK(String fechaInicial, String banco, String sucursal) throws Exception {
        try{
            this.ReiniciarListaRE();
            this.ListaRE = REDataAccess.LISTCHK(fechaInicial,banco,sucursal);
        }
        catch(Exception e){
            throw new Exception("Error en LISTCHK [ReporteEgresoService]...\n"+e.getMessage());
        }
    }
    
    
    
    public void UPDATE( String ndoc,  String user, String doc, String banco, String sucursal, String distrito ) throws Exception {
        try{
            REDataAccess.UPDATE(ndoc, user, doc, banco, sucursal, distrito);
            // REDataAccess.UPDATEREGA(ndoc, user, doc, banco, sucursal, distrito);
        }
        catch(Exception e){
            throw new Exception("Error en UPDATE [ReporteEgresoService]...\n"+e.getMessage());
        }
    }
    
    
    public void UPDATEREGA( String ndoc,  String user, String doc, String banco, String sucursal, String distrito ) throws Exception {
        try{
            REDataAccess.UPDATEREGA(ndoc, user, doc, banco, sucursal, distrito);
        }
        catch(Exception e){
            throw new Exception("Error en UPDATE [ReporteEgresoService]...\n"+e.getMessage());
        }
    }
    
    public boolean SEARCH(String doc, String listaO, String banco, String sucursal) throws Exception {
        try{
            return REDataAccess.SEARCH(doc, listaO, banco,sucursal);
        }
        catch(Exception e){
            throw new Exception("Error en SEARCH [ReporteEgresoService]...\n"+e.getMessage());
        }
    }
    
    public ReporteEgreso BUSCAR(String doc) throws Exception {
        try{
            ////System.out.println("Service ");
            return REDataAccess.SEARCHRE(doc);
        }
        catch(Exception e){
            throw new Exception("Error en BUSCAR [ReporteEgresoService]...\n"+e.getMessage());
        }
    }
    
    public ReporteEgreso BUSCARITEM(String cheque) throws Exception {
        try{
            return REDataAccess.SearchItem(cheque);
        }
        catch(Exception e){
            throw new Exception("Error en BUSCARITEM [ReporteEgresoService]...\n"+e.getMessage());
        }
    }
    
    public List LISTCHKNOMIGRADOS() throws Exception {
        try{
            this.ReiniciarListaChk();
            return REDataAccess.LISTCHKNOMIGRADOS();
        }
        catch(Exception e){
            throw new Exception("Error en LISTCHKNOMIGRADOS [ReporteEgresoService]...\n"+e.getMessage());
        }
    }
    
    public void UPDATEMIGRACIONCHK( String user, String doc ) throws Exception {
        try{
            REDataAccess.UPDATEMIGRACIONCHK(user, doc);
        }
        catch(Exception e){
            throw new Exception("Error en UPDATEMIGRACIONCHK [ReporteEgresoService]...\n"+e.getMessage());
        }
    }
    
    public void ReiniciarDato(){
        this.Datos = null;
    }
    public void ReiniciarListaRE(){
        this.ListaRE = null;
    }
    
    public void ReiniciarListaBancos(){
        this.ListaBancos = null;
    }
    
    public void ReiniciarListaChk(){
        this.ListaChkNomigrados = null;
    }
    
    public ReporteEgreso getDato(){
        return this.Datos;
    }
    
    public List getListRE(){
        return this.ListaRE;
    }
    
    public List getListBancos(){
        return this.ListaBancos;
    }
    public String UPDATE( String ndoc,  String user, String doc, String banco, String sucursal, String distrito, String tipodoc, ReporteEgreso rp ) throws Exception {
        try{
            return REDataAccess.UPDATE(ndoc, user, doc, banco, sucursal, distrito, tipodoc, rp );
        }
        catch(Exception e){
            throw new Exception("Error en UPDATE ACTUALIZACION 1 [ReporteEgresoService]...\n"+e.getMessage());
        }
    }
    
    
    public String UPDATEREGA( String ndoc,  String user, String doc, String banco, String sucursal, String distrito, String tipodoc, ReporteEgreso rp ) throws Exception {
        try{
            return REDataAccess.UPDATEREGA(ndoc, user, doc, banco, sucursal, distrito, tipodoc, rp );
        }
        catch(Exception e){
            throw new Exception("Error en UPDATEREGA ACTUALIZACION 2 [ReporteEgresoService]...\n"+e.getMessage());
        }
    }
    
  
    
    public void LISTBANCO(String agencia, String fechaInicial, String fechaFinal) throws Exception {
        try{
            this.ReiniciarListaBancos();
            this.ListaBancos = REDataAccess.LISTBANCO(agencia, fechaInicial, fechaFinal);
        }
        catch(Exception e){
            throw new Exception("Error en LISTBANCOS [ReporteEgresoService]...\n"+e.getMessage());
        }
    }
    
    public void LISTBANCO(String agencia, String fechaInicial, String fechaFinal, String banco, String sucu ) throws Exception {
        try{
            this.ReiniciarListaBancos();
            this.ListaBancos = REDataAccess.LISTBANCO(agencia, fechaInicial, fechaFinal, banco, sucu );
        }
        catch(Exception e){
            throw new Exception("Error en LISTBANCOS [ReporteEgresoService]...\n"+e.getMessage());
        }
    }
       
    
   
    
    public void updateEgreso(String chk, String banco , String sucursal, String usuario ) throws Exception {
        try{
            REDataAccess.updateEgreso(chk, banco, sucursal, usuario);
        }
        catch(Exception e){
            throw new Exception("Error en updateEgreso [ReporteEgresoService]...\n"+e.getMessage());
        }
    }
    
    
    //Modificacion 13-01-077
    public List ordenamiento( List listaEgresos, List listaAnulados ){
        for( int i = 0 ; i < listaAnulados.size(); i++ ){
            ReporteEgreso obj1 = (ReporteEgreso)listaAnulados.get(i);
            String numchk      = ( obj1.getNumeroChk() != null )?obj1.getNumeroChk():"";
            int j              =  this.index(listaEgresos, numchk);
            //System.out.println("index " + j);
            listaEgresos.add(j, obj1);
        }
        
        return listaEgresos;
    }
    
    public int index( List lista , String numchk ){
        int i;
        int sw = 0;
        for( i = 0; i < lista.size(); i++ ){
            ReporteEgreso auxmen = (ReporteEgreso)lista.get(i);
            String obj           =  auxmen.getNumeroChk();
            int flag = obj.compareTo(numchk);
            if( flag > 0 ){
                sw       = i;
                break;
            }
            else if( flag == 0 ){
                sw = i;
                break;
            }
            
        }
        return sw;
    }
    
      /**
     * Lista los bancos de una agencia
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct Distrito
     * @param agencia C�digo de la agencia
     */     
    public void listBancoPorAgencia( String dstrct, String agencia, String fechaInicial, String fechaFinal, String usuario, String banco, String sucu, String proveedor ) throws Exception{
        this.ReiniciarListaBancos();
        this.ListaBancos = this.REDataAccess.listBancoPorAgencia(dstrct, agencia, fechaInicial, fechaFinal, usuario, banco, sucu, proveedor);
    }
      public void LISTREPORTEGRESO( String agencia, String fechaInicial, String fechaFinal, String usuario ) throws Exception {
        try{
            this.ReiniciarListaRE();
            this.ListaRE = REDataAccess.LISTREPORTEEGRESO(agencia, fechaInicial, fechaFinal, usuario);
        }
        catch(Exception e){
            throw new Exception("Error en LISTREPORTEEGRESO [ReporteEgresoService]...\n"+e.getMessage());
        }
    }
       public void LISTREPORTEGRESO(String agencia, String fechaInicial, String fechaFinal, String usuario, String banco, String sucu ) throws Exception {
        try{
            this.ReiniciarListaRE();
            this.ListaRE = REDataAccess.LISTREPORTEEGRESO(agencia, fechaInicial, fechaFinal, usuario, banco, sucu );
        }
        catch(Exception e){
            throw new Exception("Error en LISTREPORTEEGRESO [ReporteEgresoService]...\n"+e.getMessage());
        }
    }
    /*Modificacion de Reporte de Egresos*/
    public void LISTREPORTEGRESO(String distrito , String agencia, String fechaInicial, String fechaFinal, String usuario, String banco, String sucu, String proveedor ) throws Exception {
        try{
            this.ReiniciarListaRE();
            this.ListaRE = REDataAccess.LISTREPORTEEGRESO(distrito, agencia, fechaInicial, fechaFinal, usuario, banco, sucu, proveedor );
            
            //List aux = REDataAccess.egresosAnulados(distrito, banco, sucu, fechaInicial, fechaFinal, proveedor, usuario, agencia );
            //this.ListaRE = this.ordenamiento(this.ListaRE, aux );
            
        }
        catch(Exception e){
            throw new Exception("Error en LISTREPORTEEGRESO [ReporteEgresoService]...\n"+e.getMessage());
        }
    }
   public void listaChkReenumeracion( String banco, String sucursal, String query ) throws Exception {
        try{
            this.ReiniciarListaRE();
            this.ListaRE = REDataAccess.list_Reenumeracion(banco, sucursal, query);
        }
        catch(Exception e){
            throw new Exception("Error en LISTCHK [ReporteEgresoService]...\n"+e.getMessage());
        }
    }
    
    public boolean existe_Chk_RangoSerie( String rangoi, String rangof, String documento ) throws Exception {
         try{
             return REDataAccess.existe_Chk_RangoSerie(rangoi, rangof, documento);
         }
         catch(Exception e){
             throw new Exception("Error en existe_Chk_RangoSerie [ReporteEgresoService]...\n"+e.getMessage());
         }
     }
    
    public boolean existe_Egreso( String banco, String sucursal , String documento ) throws Exception {
         try{
             return REDataAccess.existe_Egreso(banco, sucursal, documento);
         }
         catch(Exception e){
             throw new Exception("Error en existe_Egreso [ReporteEgresoService]...\n"+e.getMessage());
         }
     }
}
