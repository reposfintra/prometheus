/*
 * StdjobplkcostoDAO.java
 *
 * Created on 25 de noviembre de 2004, 11:18 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  KREALES
 */
public class StdjobplkcostoDAO {
    
    private Stdjobplkcosto std;
    private TreeMap lista;
    private Vector vct;
    /** Creates a new instance of StdjobplkcostoDAO */
    public StdjobplkcostoDAO() {
        
    }
    public Stdjobplkcosto getStd(){
        return this.std;
    }
    public TreeMap getGrupos(){
        return this.lista;
    }
    public Vector getGruposVec(){
        return this.vct;
    }
    public void setStd(Stdjobplkcosto std){
        this.std=std;
    }
    
    public void searchStdjobplkcosto(String grup )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        std= null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("SELECT * FROM stdjobplkcosto where group_code=?");
                st.setString(1,grup);
                rs = st.executeQuery();
                if(rs.next()){
                    std=Stdjobplkcosto.load(rs);
                    
                }
                
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA SJGRU " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
    
    public void listar()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        lista= null;
        vct=null;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("SELECT * FROM stdjobplkcosto where reg_status=''");
                rs = st.executeQuery();
                lista = new TreeMap();
                vct = new Vector();
                while(rs.next()){
                    vct.add(Stdjobplkcosto.load(rs));
                    lista.put(rs.getString("group_code"), rs.getString("group_code"));
                    
                }
                
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA SJGRU " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
    
    
    public void insert()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("insert into stdjobplkcosto( dstrct,group_code,activation_date,ind_trip,costo_unitario,unit_cost,unit_transp,currency,creation_user)values(?,?,?,?,?,?,?,?,?)");
                st.setString(1, std.getDstrct());
                st.setString(2, std.getGroup_code());
                st.setString(3, std.getActivation_date());
                st.setString(4, std.getInd_trip());
                st.setFloat(5, std.getCosto_unitario());
                st.setFloat(6, std.getUnit_cost());
                st.setString(7, std.getUnit_transp());
                st.setString(8, std.getCurrency());
                st.setString(9, std.getCreation_user());
                st.executeUpdate();
                
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSECCION DEL SJGRU " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
    
    public void update()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update stdjobplkcosto set ind_trip=?,costo_unitario=?,unit_cost=?,unit_transp=?,currency=?,creation_user=? where dstrct=? and group_code=? and activation_date=?");
                st.setString(1, std.getInd_trip());
                st.setFloat(2, std.getCosto_unitario());
                st.setFloat(3, std.getUnit_cost());
                st.setString(4, std.getUnit_transp());
                st.setString(5, std.getCurrency());
                st.setString(6, std.getCreation_user());
                st.setString(7, std.getDstrct());
                st.setString(8, std.getGroup_code());
                st.setString(9, std.getActivation_date());
                
                st.executeUpdate();
                
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSECCION DEL SJGRU " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
    
    public void delete()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update stdjobplkcosto set reg_status='A', user_update=? where dstrct=? and group_code=? and activation_date=?");
                st.setString(1, std.getCreation_user());
                st.setString(2, std.getDstrct());
                st.setString(3, std.getGroup_code());
                st.setString(4, std.getActivation_date());
                
                st.executeUpdate();
                
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DEL SJGRU " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
}
