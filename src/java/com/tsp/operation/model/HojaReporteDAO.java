/*
 * HojaReporteDAO.java
 *
 * Created on 22 de noviembre de 2004, 10:49 AM
 */

package com.tsp.operation.model;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.services.ImagenService;
import java.util.*;
import java.sql.*;

/**
 *
 * @author  AMENDEZT
 */
public class HojaReporteDAO {
    
     private static final String SQL_CIUDAD =
    "SELECT nomciu, pais, tipociu FROM ciudad WHERE codciu = ? ";
     
    private static final String SQL_CREAR_HR =
    "INSERT INTO reporteconductor " +
    "(planilla, agencia, ruta, placa_vehiculo, placa_unidad_carga, contenedores, precinto, conductor, usuario, creation_user, cia) "+
    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    
    private static final String SQL_MOD_HR =
    "UPDATE reporteConductor " +
    "SET agencia = ?, ruta = ?, placa_vehiculo = ?, placa_unidad_carga = ?, contenedores = ?, " +
    "precinto = ?, conductor = ?, usuario = ? " +
    "WHERE planilla = ? " +
    "AND cia = ?";
    
    private static final String SQL_BUSCAR_HR =
    "SELECT planilla FROM reporteConductor WHERE planilla = ? AND cia = ?";
    
    private static final String SQL_INFO_HR_NEW =
    "SELECT * FROM reporteconductor WHERE planilla = ? AND cia = ?";
    
    private static final String SQL_INFO_HR =
    /*"SELECT planilla, placa_vehiculo, placa_unidad_carga, contenedores, precinto, ruta, usuario, conductor, TO_CHAR(creation_date, 'YYYY-MM-DD HH:MI:SS a.m.'), creation_user, TO_CHAR(print_date, 'YYYY-MM-DD HH:MI:SS a.m.'), print_user, cia " +
      "FROM reporteconductor "+
     "WHERE planilla = ? " +
       "AND cia = ? ";*/
    "SELECT c1.planilla, " +
    "       c1.placa_vehiculo, " +
    "       c1.placa_unidad_carga, " +
    "       c1.contenedores, " +
    "       c1.precinto, " +
    "       c1.ruta, " +
    "       c1.usuario, " +
    "       pl.cedcon AS conductor, " +
    "       TO_CHAR(c1.creation_date, 'YYYY-MM-DD HH:MI:SS a.m.'), " +
    "       c1.creation_user, " +
    "       TO_CHAR(c1.print_date, 'YYYY-MM-DD HH:MI:SS a.m.'), " +
    "       c1.print_user, " +
    "       c1.cia " +
    "FROM ( " +
    "   SELECT  planilla, " +
    "           placa_vehiculo, " +
    "           placa_unidad_carga, " +
    "           contenedores, " +
    "           precinto, " +
    "           ruta, " +
    "           usuario, " +
    "           creation_date, " +
    "           creation_user, " +
    "           print_date, " +
    "           print_user, " +
    "           cia " +
    "   FROM    reporteconductor " +
    "   WHERE   planilla = ? AND cia = ? ) c1 " +
    "LEFT JOIN  planilla pl ON ( pl.numpla = c1.planilla ) ";
    
    /**
     *Query pars extraer informacion relacionada a una planilla
     *como la placa del cabezote, la cedula del conductor
     */
    private static final String SQL_INF_PLANILLA =
    "SELECT a.cia, " +
    "a.numpla, "+
    "a.plaveh, "+
    "a.platlr, "+
    "a.oripla||a.despla, "+
    "a.cedcon, "+
    "a.contenedores, " +
    "a.precinto "+
    "FROM planilla a "+
    "WHERE a.numpla = ? "+
    "AND a.cia = ? ";
    
    private static final String SQL_CONDUCTOR =
    "SELECT nombre " +
    "FROM nit " +
    "WHERE cedula = ?";
            
    private static final String SQL_VIA =
    "SELECT descripcion, via FROM via WHERE origen || destino || secuencia = ? ";
    
    private static final String SQL_FOTO =
    "SELECT max(indice) " +
    "FROM histnit " +
    "WHERE nit = ?";
    
    //QUERY PARA OBTENER EL USUARIO
    //QUE IMPRIMIO POR PRIMERA VEZ UNA HOJA DE REPORTE
    private static final String SQL_USER_PRINT =
    "SELECT print_user "+
    "FROM reporteconductor "+
    "WHERE planilla = ? " +
    "AND cia = ?";
    
    //QUERY PARA GRABAR LA PRIMERA IMPRESION
    //DE UNA HOJA DE REPORTE
    private static final String SQL_FIRST_PRINT =
    "UPDATE reporteconductor "+
    "SET print_date = now(), "+
    "print_user = ?, "+
    "last_print_date = now(), "+
    "last_print_user = ? "+
    "WHERE planilla = ? " +
    "AND cia = ? ";
    
    //QUERY PARA GRABAR LA ULTIMA IMPRESION
    private static final String SQL_LAST_PRINT =
    "UPDATE reporteconductor "+
    "SET last_print_date = now(), "+
    "last_print_user = ? "+
    "WHERE planilla = ?" +
    "AND cia = ?";
    
    private static final String SQL_INFO_HR_MANUAL =
    "SELECT numpla, placa, cedcond, via "+
    "FROM plamanual "+
    "WHERE numpla = ? " +
    "AND cia = ?";
    
    /**
     *Query para obtener los numeros de cel, fijos, lineas gratuitas y avanteles
     *de acuerdo al pais.
     */
    private static final String SQL_TEL =
    "SELECT numero FROM tel_traf WHERE pais = ? AND medio = ? AND estado <> 'A'";
    
    /**
     *Query para buscar el pais al que pertenece una ciudad
     */
    private static final String SQL_PAIS =
    "SELECT country_code, country_name FROM ciudad, pais WHERE codciu = ? AND pais = country_code";
    
    private ReporteConductor rConductor;
    private ReporteHojaConductor rHConductor;
    
    /** Creates a new instance of HojaReporteDAO */
    public HojaReporteDAO() {
    }
    
    public void createHjReporte(ReporteConductor rc) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(SQL_CREAR_HR);
            psttm.setString(1, rc.getPlanilla());
            psttm.setString(2, rc.getAgencia());
            psttm.setString(3, rc.getRuta());
            psttm.setString(4, rc.getPlaca_vehiculo());
            psttm.setString(5, rc.getPlaca_unidad_carga());
            psttm.setString(6, rc.getContenedores());
            psttm.setString(7, rc.getPrecinto());
            psttm.setString(8, rc.getConductor());
            psttm.setString(9, rc.getUsuario());
            psttm.setString(10, rc.getCreation_user());
            psttm.setString(11, rc.getCia());
            
            psttm.executeUpdate();
        }
        catch(SQLException e){
            if (e.getSQLState().equals("23505"))
                updateHjReporte(rc);
            else
                throw new SQLException("ERROR DURANTE LA CREACION DE LA HOJA REPORTE CONDUCTOR" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    
    public void updateHjReporte(ReporteConductor rc) throws SQLException {
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(SQL_MOD_HR);
            
            psttm.setString(1, rc.getAgencia());
            psttm.setString(2, rc.getRuta());
            psttm.setString(3, rc.getPlaca_vehiculo());
            psttm.setString(4, rc.getPlaca_unidad_carga());
            psttm.setString(5, rc.getContenedores());
            psttm.setString(6, rc.getPrecinto());
            psttm.setString(7, rc.getConductor());
            psttm.setString(8, rc.getUsuario());
            
            psttm.setString(9, rc.getPlanilla());
            psttm.setString(10, rc.getCia());
            
            psttm.executeUpdate();
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACION DE LA HOJA REPORTE CONDUCTOR" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    
    public boolean hReporteExist(String distrito, String planilla) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(SQL_BUSCAR_HR);
            psttm.setString(1, planilla);
            psttm.setString(2, distrito);
            rs = psttm.executeQuery();
            if (rs.next()){
                sw = true;
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA HOJA DE REPORTE:" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
        return sw;
    }
    
    public void getHrInfoNew(String distrito, String planilla) throws SQLException {
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        rConductor = null;
        
        poolManager = PoolManager.getInstance();
        try{
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            //INFORMACION DE PLANILLA
            try{
                psttm = con.prepareStatement(SQL_INF_PLANILLA);
                psttm.setString(1, planilla);
                psttm.setString(2, distrito);
                rs = psttm.executeQuery();
                rConductor =  new ReporteConductor();
                if (rs.next()){
                    rConductor.setCia(rs.getString(1));
                    rConductor.setPlanilla(rs.getString(2));
                    rConductor.setPlaca_vehiculo(rs.getString(3));
                    rConductor.setPlaca_unidad_carga(rs.getString(4));
                    rConductor.setRuta(rs.getString(5));
                    rConductor.setConductor(rs.getString(6));
                    rConductor.setContenedores(rs.getString(7));
                    rConductor.setPrecinto(rs.getString(8));
                }
            }
            catch(SQLException e){
                throw new SQLException("ERROR OBTENIENDO INFORMACION DE PLANILLA: " + e.getMessage() + " " + e.getErrorCode());
            }
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    
    public void getHrInfoOld(String distrito, String planilla) throws SQLException {
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        rConductor = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(SQL_INFO_HR);
            psttm.setString(1, planilla);
            psttm.setString(2, distrito);
            rs = psttm.executeQuery();
            rConductor =  new ReporteConductor();
            rConductor.setPlanilla(planilla);
            if (rs.next()){
                rConductor.setPlaca_vehiculo(rs.getString(2));
                rConductor.setPlaca_unidad_carga(rs.getString(3));
                rConductor.setContenedores(rs.getString(4));
                rConductor.setPrecinto(rs.getString(5));
                rConductor.setRuta(rs.getString(6));
                rConductor.setUsuario(rs.getString(7));
                rConductor.setConductor(rs.getString(8));
                rConductor.setCreation_date(rs.getString(9));
                rConductor.setCreation_user(rs.getString(10));
                rConductor.setPrint_date(rs.getString(11));
                rConductor.setPrint_user(rs.getString(12));
                rConductor.setCia(rs.getString(13));
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR CONSULTANDO HOJA DE REPORTE: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }    
        
    }
    
    public void imprimirHojaReporte(String distrito, String planilla, String user) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        String usuario = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            try{
                psttm = con.prepareStatement(SQL_USER_PRINT);
                psttm.setString(1, planilla);
                psttm.setString(2, distrito);
                rs = psttm.executeQuery();
                if (rs.next()){
                    usuario = rs.getString("print_user");
                }
            }
            catch(SQLException e){
                throw new SQLException("ERROR CONSULTANDO USUARIO DE IMPRESION: " + e.getMessage() + " " + e.getErrorCode());
            }
            
            //SI usuario es igual a vacio entoces la hoja de reporte nunca ha sido impresa entonces se modfica
            //el usuario y la fecha de impresion
            //de lo contrario se modifican la fecha de ultima impresio y el usuario de la ultima impresion
            if (usuario.equals("")){
                try{
                    psttm = con.prepareStatement(SQL_FIRST_PRINT);
                    psttm.setString(1, user);
                    psttm.setString(2, user);
                    psttm.setString(3, planilla);
                    psttm.setString(4, distrito);
                    psttm.executeUpdate();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR GRABADO DATOS DE IMPRESION: " + e.getMessage() + " " + e.getErrorCode());
                }
            }
            else{
                try{
                    psttm = con.prepareStatement(SQL_LAST_PRINT);
                    psttm.setString(1, user);
                    psttm.setString(2, planilla);
                    psttm.setString(3, distrito);
                    psttm.executeUpdate();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR GRABADO DATOS DE ULTIMA IMPRESION: " + e.getMessage() + " " + e.getErrorCode());
                }
            }
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    
    /**
     *Extre la informacion para una hoja de reporte de una planilla manual
     */
    public void getHrManualInfo(String distrito, String planilla) throws SQLException {
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        rConductor = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(SQL_INFO_HR_MANUAL);
            psttm.setString(1, planilla);
            psttm.setString(2, distrito);
            rs = psttm.executeQuery();
            rConductor =  new ReporteConductor();
            rConductor.setPlanilla(planilla);
            if (rs.next()){
                rConductor.setPlaca_vehiculo(rs.getString(2));
                rConductor.setConductor(rs.getString(3));
                rConductor.setRuta(rs.getString(4));
                rConductor.setPlaca_unidad_carga("");
                rConductor.setContenedores("");
                rConductor.setPrecinto("");
                rConductor.setCia(distrito);
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR EXTRALLENDO INFORMACION PARA HOJA DE REPORTE MANUAL: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    
    /**
     *Metodo para obtener los numeros de comunicacion con trafico
     */
    private String getNumCom(String pais, String medio) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        String numeros = "";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(SQL_TEL);
            psttm.setString(1, pais);
            psttm.setString(2, medio);
            rs = psttm.executeQuery();
            while(rs.next()){
                numeros = numeros + rs.getString(1) + " - ";
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR EXTRALLENDO NUMEROS DE COMUNICACION: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
        return numeros;
    }
    
    /**
     * Getter for property rConductor.
     * @return Value of property rConductor.
     */
    public ReporteConductor getRConductor() {
        return rConductor;
    }
    
    /**
     * Setter for property rConductor.
     * @param rConductor New value of property rConductor.
     */
    public void setRConductor(ReporteConductor rConductor) {
        this.rConductor = rConductor;
    }
    
    /**
     * Getter for property rHConductor.
     * @return Value of property rHConductor.
     */
    public ReporteHojaConductor getRHConductor() {
        return rHConductor;
    }
    
    /**
     * Setter for property rHConductor.
     * @param rHConductor New value of property rHConductor.
     */
    public void setRHConductor(ReporteHojaConductor rHConductor) {
        this.rHConductor = rHConductor;
    }
    
    public static  void main(String[]qqqqq){
        String c = "tus nalgas";
        //System.out.println( c.startsWith("tus nal") );
    }
    public void generarReporteHojaConductor(ReporteConductor rConductor) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        rHConductor = null;
        String via = "";
        String nro_foto = "";
        int i = 0;
        
        String origen  = "";
        String destino = "";
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            //SE OBTIENEN DATOS DEL CONDUCTOR
            try{
                rHConductor =  new ReporteHojaConductor();
                psttm = con.prepareStatement(SQL_CONDUCTOR);
                psttm.setString(1, rConductor.getConductor());
                rs = psttm.executeQuery();
                if (rs.next()){
                    rHConductor.setConductor(rs.getString(1));
                }
                else
                    rHConductor.setConductor("");
                
                rHConductor.setCedula(rConductor.getConductor());
                rHConductor.setPlanilla(rConductor.getPlanilla());
                rHConductor.setPlaca_vehiculo(rConductor.getPlaca_vehiculo());
                rHConductor.setPlaca_unidad_carga(rConductor.getPlaca_unidad_carga());
                rHConductor.setPrecinto(rConductor.getPrecinto());
                rHConductor.setContenedores(rConductor.getContenedores());
                rHConductor.setComentario(rConductor.getComentario());
                rHConductor.setUsuario(rConductor.getUsuario());
                rHConductor.setCia(rConductor.getCia());
            }
            catch(SQLException e){
                throw new SQLException("ERROR OBTENIENDO DATOS DEL CONDUCTOR: " + e.getMessage() + " " + e.getErrorCode());
            }
            
            //SE OBTIENEN DATOS DE LA VIA
            try{
                psttm = con.prepareStatement(SQL_VIA);
                psttm.setString(1, rConductor.getRuta());
                rs = psttm.executeQuery();
                if (rs.next()){
                    rHConductor.setRuta(rs.getString("descripcion"));
                    via = rs.getString("via");
                }
            }
            catch(SQLException e){
                throw new SQLException("ERROR OBTENIENDO LA VIA: " + e.getMessage() + " " + e.getErrorCode());
            }
            
            //SE OBTIENEN LOS PUESTOS DE CONTROL
            try{
                Vector ciudades = new Vector();
                psttm = con.prepareStatement(SQL_CIUDAD);
                
                while (i < via.length()){
                    psttm.setString(1, via.substring(i, i+2));
                    
                    rs = psttm.executeQuery();
                    if (rs.next()){
                        //Osvaldo: las tres condiciones siguientes
                        String ciu  = rs.getString("nomciu");
                        String tipo = rs.getString("tipociu");
                        
                        if( i == 0 ){//Se agrega la primera ciudad (ORIGEN)
                            origen = rs.getString("pais")+"-"+via.substring(i, i+2);
                            ciudades.add( ciu );
                        }else if( tipo.equals("PC") ){//Se agrega si es Puesto de Control
                            ciudades.add( ciu );
                        }else if( (via.length() -2) == i  ){//Se agrega la ultima ciudad (DESTINO)
                            destino = rs.getString("pais")+"-"+via.substring(i, i+2);
                            ciudades.add( ciu );
                        }
                    }
                    i = i + 2;
                }
                rHConductor.setDetalleruta(ciudades);
            }
            catch(SQLException e){
                throw new SQLException("ERROR OBTENIENDO LOS PUESTOS DE CONTROL: " + e.getMessage() + " " + e.getErrorCode());
            }
            
            //OBTENER NUMEROS DE COMUNICACION DE TRAFICO
            String codPais = "";
            try{
                
                String fijo = "";
                String cels = "";
                String grat = "";
                String avan = "";                                
                
                psttm = con.prepareStatement(SQL_PAIS);
                psttm.setString( 1, origen.split("-")[1] );
                rs = psttm.executeQuery();
                if (rs.next()){
                    codPais = rs.getString(1);
                    rHConductor.setPais( (rs.getString(2))+"-_-");
                }
                
                fijo = getNumCom(codPais, "F")+"-_- ";
                cels = getNumCom(codPais, "C")+"-_- ";
                grat = getNumCom(codPais, "G")+"-_- ";
                avan = getNumCom(codPais, "A")+"-_- ";
                
                if( !origen.split("-")[0].equals(destino.split("-")[0]) ){
                    
                    psttm = con.prepareStatement(SQL_PAIS);
                    psttm.setString( 1, destino.split("-")[1] );
                    rs = psttm.executeQuery();
                    
                    if (rs.next()){                        
                        codPais = rs.getString(1);
                        rHConductor.setPais( rHConductor.getPais() + rs.getString(2));
                    }
                    
                    fijo += getNumCom(codPais, "F");
                    cels += getNumCom(codPais, "C");
                    grat += getNumCom(codPais, "G");
                    avan += getNumCom(codPais, "A");
                    
                }
                                
                rHConductor.setAvantel(avan);
                rHConductor.setTelCel(cels);
                rHConductor.setTelFijo(fijo);
                rHConductor.setLineaG(grat);
            }
            catch(SQLException e){
                throw new SQLException("ERROR OBTENIENDO LOS NUMEROS DE COMUNICACION CON TRAFICO: " + e.getMessage() + " " + e.getErrorCode());
            }
            
            
            
            //SE OBTIENEN LA FOTOGRAFIA DEL CONDUCTOR: Fernel Villacob
            //Modificado por LREALES - 24 Octubre 2006.
            try {
                
                ImagenService svc = new  ImagenService();
                svc.searchImagen( "","", "" ,null,null, "" ,  "003" , "032" , rConductor.getConductor(), "" , "" , "" ,"1", rConductor.getConductor() );
                List imgs  =   svc.getImagenes();
                
                if(imgs!=null  && imgs.size()>0){
                    
                    Imagen imagen = ( Imagen ) imgs.get(0);
                    rHConductor.setFoto( rConductor.getConductor() + "/" +  imagen.getFileName() );
                    
                }
                
            } catch ( Exception e ) {
                
                e.printStackTrace();
                throw new SQLException( "ERROR OBTENIENDO LA FOTOGRAFIA DEL CONDUCTOR: " + e.getMessage()  );
                
            }           
            
                        
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
}
