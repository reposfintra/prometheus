/*
 * EgresoDAO.java
 *
 * Created on 11 de enero de 2005, 08:49 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;
/**
 *
 * @author  KREALES
 */
public class EgresoDAO extends MainDAO{
    
    private Egreso egreso;
    private Vector egresos;
    private List egresosdet;
    private ImpresionCheque cheque;
    
    /** Creates a new instance of EgresoDAO */
    public EgresoDAO() {
        super("EgresoDAO.xml");
    }
    public EgresoDAO(String dataBaseName) {
        super("EgresoDAO.xml",dataBaseName);
    }
    public Vector getEgresos(){
        return egresos;
    }
    public List getEgresosDet(){
        return egresosdet;
    }
    public Egreso getEgreso(){
        return egreso;
    }
    
    private static final String SEARCH_EGRESO="select " +
    " e.dstrct," +
    " e.branch_code," +
    " e.bank_account_no," +
    " e.document_no," +
    " e.nit,	" +
    " e.payment_name," +
    " e.agency_id," +
    " tc.concept_desc," +
    " e.vlr," +
    " e.currency," +
    " e.creation_date" +
    " from    egreso e, tblcon tc" +
    " where   e.dstrct like ? and" +
    "         e.bank_account_no like ? and" +
    "         e.branch_code like ? and " +
    "         e.document_no like ? and" +
    "         e.nit like ? and " +
    "         e.payment_name like ? and" +
    "         e.concept_code = tc.concept_code and " +
    "         e.creation_date >=? and" +
    "         e.creation_date <=?";
    
    private static final String SEARCH_EGRESODET="Select ed.item_no," +
    "  tc.concept_desc," +
    "  ed.vlr," +
    "  ed.currency," +
    "  ed.oc" +
    "  from   " +
    "      egresodet ed, " +
    "      tblcon tc" +
    "  where " +
    "      ed.document_no  = ? and " +
    "      ed.concept_code = tc.concept_code" +
    "      order by item_no";
    
    public void consultaEgreso(String distrito,String cuenta,String banco,String doc_no,String nit,String nombre,String fechaini,String fechafin )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        egreso= null;
        egresos = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SEARCH_EGRESO);
                st.setString(1, distrito+"%");
                st.setString(2, cuenta+"%");
                st.setString(3, banco+"%");
                st.setString(4, doc_no+"%");
                st.setString(5, nit+"%");
                st.setString(6, nombre+"%");
                st.setString(7, fechaini+" 00:00:00");
                st.setString(8, fechafin+" 23:59:59");
                rs = st.executeQuery();
                egresos = new Vector();
                while(rs.next()){
                    egreso = new Egreso();
                    egreso.setAgency_id(rs.getString("agency_id"));
                    egreso.setBank_account_no(rs.getString("bank_account_no"));
                    egreso.setBranch_code(rs.getString("branch_code"));
                    egreso.setConcept_code(rs.getString("concept_desc"));
                    egreso.setCurrency(rs.getString("currency"));
                    egreso.setDocument_no(rs.getString("document_no"));
                    egreso.setDstrct(rs.getString("dstrct"));
                    egreso.setNit(rs.getString("nit"));
                    egreso.setPayment_name(rs.getString("payment_name"));
                    egreso.setVlr(rs.getFloat("vlr"));
                    egresos.add(egreso);
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL EGRESO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
        
    }
    
    public void buscarEgreso(String doc_no)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        egreso= null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("select e.*, tc.concept_desc from egreso e, tblcon tc where e.document_no = ? and e.concept_code=tc.concept_code");
                st.setString(1, doc_no);
                rs = st.executeQuery();
                if(rs.next()){
                    egreso = new Egreso();
                    egreso.setAgency_id(rs.getString("agency_id"));
                    egreso.setBank_account_no(rs.getString("bank_account_no"));
                    egreso.setBranch_code(rs.getString("branch_code"));
                    egreso.setConcept_code(rs.getString("concept_desc"));
                    egreso.setCurrency(rs.getString("currency"));
                    egreso.setDocument_no(rs.getString("document_no"));
                    egreso.setDstrct(rs.getString("dstrct"));
                    egreso.setNit(rs.getString("nit"));
                    egreso.setPayment_name(rs.getString("payment_name"));
                    egreso.setVlr(rs.getFloat("vlr"));
                    egreso.setCreation_date(rs.getString("creation_date"));
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL EGRESO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
        
    }
    
    public void buscarEgresosDet(String doc_no)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        egresosdet= null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SEARCH_EGRESODET);
                st.setString(1, doc_no);
                rs = st.executeQuery();
                egresosdet = new LinkedList();
                while(rs.next()){
                    Egreso egreso2 = new Egreso();
                    egreso2.setConcept_code(rs.getString("concept_desc"));
                    egreso2.setCurrency(rs.getString("currency"));
                    egreso2.setVlr(rs.getFloat("vlr"));
                    egreso2.setItem_no(rs.getString("item_no"));
                    egreso2.setOc(rs.getString("oc"));
                    egresosdet.add(egreso2);
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL EGRESO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
        
    }
    public String insertEgresoxAnticipo()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        egresosdet= null;
        String sql = "";
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                String banco [] = cheque.getBanco().split("/");
                String branch="";
                String account="";
                if(banco.length>0)
                    branch = banco[0];
                if(banco.length>1)
                    account = banco[1];
                
                st = con.prepareStatement("insert into egreso(dstrct,branch_code,bank_account_no, document_no,nit, payment_name, agency_id,concept_code,vlr,vlr_for,currency, creation_user,tipo_documento,usuario_impresion,pmt_date,printer_date,fecha_cheque,fecha_entrega)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,'now()','now()','now()','now()')");
                
                st.setString(1, cheque.getDistrito());
                st.setString(2, cheque.getBanco());
                st.setString(3, cheque.getAgenciaBanco());
                st.setString(4, cheque.getCheque());
                st.setString(5, cheque.getCedula());
                st.setString(6, cheque.getBeneficiario());
                st.setString(7, cheque.getAgencia());
                st.setString(8, "01");
                st.setDouble(9, cheque.getValor());
                st.setDouble(10,cheque.getValor());
                st.setString(11,cheque.getMoneda());
                st.setString(12, cheque.getUsuario());
                st.setString(13, "004");
                st.setString(14, cheque.getUsuario());
                sql = st.toString();
                
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DEL EGRESO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return sql;
        
    }
    
    public String insertItemxAnticipo()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        egresosdet= null;
        String sql = "";
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                String banco [] = cheque.getBanco().split("/");
                String branch="";
                String account="";
                if(banco.length>0)
                    branch = banco[0];
                if(banco.length>1)
                    account = banco[1];
                
                st = con.prepareStatement("insert into egresodet(dstrct,branch_code,bank_account_no, document_no,item_no,concept_code,vlr,vlr_for,currency, oc, creation_user,description)values(?,?,?,?,?,?,?,?,?,?,?,?)");
                
                st.setString(1, cheque.getDistrito());
                st.setString(2,cheque.getBanco());
                st.setString(3,cheque.getAgenciaBanco());
                st.setString(4,cheque.getCheque());
                st.setString(5,"001");
                st.setString(6, "01");
                st.setDouble(7, cheque.getValor());
                st.setDouble(8, cheque.getValor());
                st.setString(9, cheque.getMoneda());
                st.setString(10, cheque.getPlanilla());
                st.setString(11, cheque.getUsuario());
                st.setString(12, "CHEQUE ANTICIPO DE LA PLANILLA "+cheque.getPlanilla());
                sql = st.toString();
                
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DEL EGRESO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return sql;
        
    }
    
    /**
     * Setter for property egreso.
     * @param egreso New value of property egreso.
     */
    public void setEgreso(com.tsp.operation.model.beans.Egreso egreso) {
        this.egreso = egreso;
    }
    
    /**
     * Getter for property cheque.
     * @return Value of property cheque.
     */
    public com.tsp.operation.model.beans.ImpresionCheque getCheque() {
        return cheque;
    }
    
    /**
     * Setter for property cheque.
     * @param cheque New value of property cheque.
     */
    public void setCheque(com.tsp.operation.model.beans.ImpresionCheque cheque) {
        this.cheque = cheque;
    }
    
    /**
     * Procedimiento que retorna depepediendo del tipo por ejemplo
     * tipo = 0 -> cheques entregados
     * tipo = 1 -> cheques enviados
     * tipo = 2 -> cheques recibidos
     *
     * Metodo:          BuscarCheques
     * Descriocion :    Funcion publica que nos arroja la informacion de un documento en cuentas por pagar
     * @autor :         Mario Fontalvo - 2005-10-20
     * @modificado por: LREALES - mayo del 2006
     * @param:          Tipo = Indica el Tipo de Busqueda
     * @param:          args = Parametros de Busqueda (Banco, Sucursal, Agencia)
     * @return:         Listado de Cheques
     * @version :       1.0
     */
    public List BuscarCheques( int Tipo, String [] args ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet         rs = null;
        List              lista = new LinkedList();
        
        try{
            
            String info = "";
            String Query = this.obtenerSQL( "SQL_BuscarCheques" );
            
            // Seleccion del Tipo de Campo
            if( Tipo == 0 )
                info = Query.replaceAll( "#FECHAS#", " AND fecha_entrega = '0099-01-01' " );
            else if( Tipo == 1 )
                info = Query.replaceAll( "#FECHAS#", " AND fecha_entrega != '0099-01-01' AND fecha_envio = '0099-01-01' " );
            else if( Tipo == 2 )
                info = Query.replaceAll( "#FECHAS#", " AND fecha_entrega != '0099-01-01' AND fecha_envio != '0099-01-01' AND fecha_recibido = '0099-01-01' " );
            else
                info = Query.replaceAll( "#FECHAS#", "" );
            
            con = this.conectar( "SQL_BuscarCheques" );
            
            st = con.prepareStatement( info );
            ////System.out.println( "SQL_BuscarCheques: " + st );
            
            // Seteo de Parametros
            for ( int i = 0; args != null && i < args.length; i++ ){
                
                st.setString( i + 1 , ( args[i].equalsIgnoreCase("ALL")?"%":args[i] ) );
                
            }
            
            rs = st.executeQuery();
            
            while ( rs.next() ){
                
                Egreso dt = new Egreso();
                
                dt.setDstrct         ( rs.getString("dstrct"         ) );
                dt.setBranch_code    ( rs.getString("branch_code"    ) );
                dt.setBank_account_no( rs.getString("bank_account_no") );
                dt.setDocument_no    ( rs.getString("document_no"    ) );
                dt.setNit            ( rs.getString("nit"            ) );
                dt.setPayment_name   ( rs.getString("payment_name"   ) );
                dt.setPmt_date       ( rs.getString("fecha_cheque"   ) );
                dt.setFechaEntrega   ( rs.getString("fecha_entrega"  ) );
                dt.setFechaEnvio     ( rs.getString("fecha_envio"    ) );
                dt.setFechaRecibido  ( rs.getString("fecha_recibido" ) );
                dt.setVlr            ( rs.getFloat("vlr"             ) );
                dt.setCurrency       ( rs.getString("currency"       ) );
                
                lista.add( dt );
                
            }
            
            
        } catch( Exception e ){
            
            throw new Exception( "Error en la rutina BuscarCheques [EgresoDAO]...\n" + e.getMessage() );
            
        }
        
        finally{
            
            if( rs != null ) rs.close();
            if( st != null ) st.close();
            this.desconectar( "SQL_BuscarCheques" );
            
        }
        
        return lista;
        
    }
    
    /**
     * Procedimiento que actualiza la fecha de..
     * tipo = 0 -> cheques entregados
     * tipo = 1 -> cheques enviados
     * tipo = 2 -> cheques recibidos
     *
     * Metodo:          ActualizarFechasCheques
     * Descriocion :    Funcion publica que nos arroja la informacion de un documento en cuentas por pagar
     * @autor :         Mario Fontalvo - 2005-10-20
     * @modificado por: LREALES - mayo del 2006
     * @param:          Tipo = Indica el Tipo de Actualizaci�n
     * @param:          args = Parametros de Busqueda (Banco, Sucursal, Agencia)
     * @return:         Listado de Cheques
     * @version :       1.0
     */
    public void ActualizarFechasCheques( int Tipo, String [] args ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        
        try{
            
            String info = "";
            String Query = this.obtenerSQL( "SQL_ActualizarFechasCheques" );
            
            // Seleccion del Tipo de Campo
            if( Tipo == 0 )
                info = Query.replaceAll( "#CAMPOS#", " usuario_entrega = ?, fecha_entrega = ?, fecha_registro_entrega = ? " );
            else if( Tipo == 1 )
                info = Query.replaceAll( "#CAMPOS#", " usuario_envio = ?, fecha_envio = ?, fecha_registro_envio = ? " );
            else if( Tipo == 2 )
                info = Query.replaceAll( "#CAMPOS#", " usuario_recibido = ?, fecha_recibido = ?, fecha_registro_recibido = ? " );
            else
                info = Query.replaceAll( "#CAMPOS#", "" );
            
            con = this.conectar( "SQL_ActualizarFechasCheques" );
            
            st = con.prepareStatement( info );
            ////System.out.println( "SQL_ActualizarFechasCheques: " + st );
            
            // Seteo de Parametros
            for ( int i = 0; args != null && i < args.length; i++ ){
                
                st.setString( i + 1 , (args[i].equals("#FECHA#")?Util.getFechaActual_String(6):args[i]) );
                
            }
            
            st.executeUpdate();
            
        } catch( Exception e ){
            
            throw new Exception( "Error en la rutina ActualizarFechasCheques [EgresoDAO]...\n" + e.getMessage() );
            
        }
        
        finally{
            
            if( st != null ) st.close();
            this.desconectar( "SQL_ActualizarFechasCheques" );
            
        }
        
    }
                                
    
    
    /**
     * Metodo:          buscarChequesRangoFechas
     * Descriocion :    Funcion que permite obtener un lista de cheques a partir de un banco, una sucursal y un rango de fechas
     * @autor :         Ing. Juan M. Escand�n P - 2007-02-15
     * @param:          args = Parametros de Busqueda (Distrito, Banco, Sucursal, RangoInicial, RangoFinal)
     * @return:         Listado de Cheques
     * @version :       1.0
     */
    public List buscarChequesRangoFechas( String [] args ) throws Exception{
        
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        List              lista = new LinkedList();
        String            query = "SQL_BUSCARCHQRANGO";
        
        try{
            st           = this.crearPreparedStatement(query);
            // Seteo de Parametros
            for ( int i = 0; args != null && i < args.length; i++ ){
                st.setString( i + 1 , args[i] );
            }
            
            rs = st.executeQuery();
            
            while ( rs.next() ){
                Egreso dt = new Egreso();
                
                dt.setBranch_code       ( rs.getString("banco"              ) );//Banco
                dt.setBank_account_no   ( rs.getString("sucursal"           ) );//Sucursal
                dt.setDocument_no       ( rs.getString("numerochk"          ) );
                dt.setPayment_name      ( rs.getString("beneficiario"       ) );//Beneficiario
                dt.setVlr_for           ( rs.getFloat("valor"               ) );//Valor
                dt.setCurrency          ( rs.getString("moneda"             ) );//Moneda
                dt.setPrinter_date      ( rs.getString("fecha_impresion"    ) );//Fecha de Impresion
                dt.setUsuario_impresion( rs.getString("usuario_impresion"  ) );//Usuario de Impresion
                dt.setConcept_code      ( rs.getString("concept_code"       ) );//Concepto
                lista.add( dt );
            }
            
        } catch( Exception e ){
            throw new Exception( "Error en la rutina buscarChequesRangoFechas [EgresoDAO]...\n" + e.getMessage() );
        }
        finally{
            if( rs != null ) rs.close();
            if( st != null ) st.close();
            this.desconectar( query );
        }
        return lista;
    }
    
     /*
      * Descripcion :    Funcion que permite obtener todos los egresos
      * @autor :         David Pi�a Lopez
      * @version :       1.0
      */
    public void buscarEgresosCab( String fecini, String fecfin, String usuario, String tipo ) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        egresos = new Vector();
        String adicional = "";
        try{
            String sql = this.obtenerSQL( "SQL_BUSCAR_EGRESO" );
            if( !fecini.equals( "" ) && !fecfin.equals( "" )  ){
                adicional += " AND e.creation_date BETWEEN '" + fecini + " 00:00:00' AND '" + fecfin + " 23:59:59'";
            }
            
            if ( !usuario.trim().equals("") ){
                adicional += " AND e.creation_user = '"+ usuario.toUpperCase() +"' ";
            }
            
            if ( !tipo.trim().equals("") ){
                adicional += " AND e.concept_code = '"+ tipo +"' ";
            }

            if ( !egreso.getNit().trim().equals("") ){
                adicional += " AND e.nit = '"+ egreso.getNit() +"' ";
            }

           if ( !egreso.getDocumento().trim().equals("") ){
                adicional += " AND ed.documento = '"+ egreso.getDocumento() +"' ";
            }
           
            sql = sql.replaceAll( "#ADICIONAL#", adicional );
            
            con = this.conectar( "SQL_BUSCAR_EGRESO" );          
            st = con.prepareStatement(sql);
            
            st.setString( 1, egreso.getDstrct() );
            st.setString( 2, "%" + egreso.getBranch_code() + "%" );
            st.setString( 3, "%" + egreso.getBank_account_no() + "%" );
            st.setString( 4, "%" + egreso.getDocument_no() + "%"  ); // documento
            //sql = st.toString();
            rs = st.executeQuery();
            
            while ( rs.next() ){
                Egreso dt = new Egreso();
                dt.setDstrct         ( rs.getString("dstrct"         ) );
                dt.setBranch_code    ( rs.getString("branch_code"    ) );
                dt.setBank_account_no( rs.getString("bank_account_no") );
                dt.setDocument_no    ( rs.getString("document_no"    ) );
                dt.setPayment_name( rs.getString( "payment_name" ) );
                dt.setVlr    ( rs.getFloat( "vlr") );
                dt.setVlr_for( rs.getFloat( "vlr_for") );
                dt.setFechaEntrega( rs.getString( "fecha_cheque" ) ); //fecha cheque
                dt.setReg_status( rs.getString( "reg_status" ) ); //jose 2007-02-24
                //AMATURANA 30.04.2007
                dt.setValor_for(rs.getDouble("vlr_for"));
                dt.setValor(rs.getDouble("vlr"));
                egresos.add( dt );
            }
            
        }catch( Exception e ){
            throw new Exception( "Error en la rutina buscarEgresos [EgresoDAO]...\n" + e.getMessage() );
        }
        finally{
            if( rs != null ) rs.close();
            if( st != null ) st.close();
            this.desconectar( "SQL_BUSCAR_EGRESO" );
            
        }
    }
    
     /*
      * Descripcion :    Funcion que permite obtener todos los egresos
      * @autor :         David Pi�a Lopez
      * @version :       1.0
      */
    public void buscarEgresos() throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        egresos = new Vector();
        try{
            
            st = this.crearPreparedStatement("SQL_BUSCAR_EGRESODET");
            st.setString( 1, egreso.getDstrct() );
            st.setString( 2, "%" + egreso.getBranch_code() + "%" );
            st.setString( 3, "%" + egreso.getBank_account_no() + "%" );
            st.setString( 4, egreso.getDocument_no() ); // documento
            st.setString( 5, egreso.getConcept_code() );//tipo_documento
            rs=st.executeQuery();
            
            while ( rs.next() ){
                Egreso dt = new Egreso();
                dt.setDstrct         ( rs.getString("dstrct"         ) );
                dt.setBranch_code    ( rs.getString("branch_code"    ) );
                dt.setBank_account_no( rs.getString("bank_account_no") );
                dt.setDocument_no    ( rs.getString("document_no"    ) );
                dt.setPayment_name( rs.getString( "payment_name" ) );
                dt.setVlr( rs.getFloat( "vlr") );
                dt.setFechaEntrega( rs.getString( "fecha_cheque" ) ); //fecha cheque
                dt.setReg_status( rs.getString( "reg_status" ) ); //jose 2007-02-24
                //AMATURANA 30.04.2007
                dt.setValor_for(rs.getDouble("vlr_for"));
                dt.setValor(rs.getDouble("vlr"));
                egresos.add( dt );
            }
            
        }catch( Exception e ){
            throw new Exception( "Error en la rutina buscarEgresos [EgresoDAO]...\n" + e.getMessage() );
        }
        finally{
            if( rs != null ) rs.close();
            if( st != null ) st.close();
            this.desconectar( "SQL_BUSCAR_EGRESODET" );
            
        }
    }
    
     /* Descripcion :    Funcion que obtiene la cabecera de un egreso
      * @autor :         David Pi�a Lopez
      * @version :       1.0
      */
    public void obtenerCabeceraEgreso() throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        try{
            
            st = this.crearPreparedStatement("SQL_CONSULTAR_CABECERA_EGRESO");
            st.setString( 1, egreso.getDstrct() );
            st.setString( 2, egreso.getBranch_code() );
            st.setString( 3, egreso.getBank_account_no() );
            st.setString( 4, egreso.getDocument_no() );
            
            rs = st.executeQuery();
            egreso = new Egreso();
            if( rs.next() ){
                egreso.setEstado( rs.getString("estado") );
                egreso.setDstrct         ( rs.getString("dstrct"         ) );
                egreso.setBranch_code    ( rs.getString("branch_code"    ) );
                egreso.setBank_account_no( rs.getString("bank_account_no") );
                egreso.setDocument_no    ( rs.getString("document_no"    ) );
                egreso.setNit            ( rs.getString("nit"            ) );
                egreso.setPayment_name   ( rs.getString("payment_name"   ) );
                egreso.setAgency_id      ( rs.getString("agencia"        ) );
                egreso.setVlr            ( rs.getFloat("vlr"             ) );
                egreso.setVlr_for        ( rs.getFloat("vlr_for"         ) );//Osvaldo
                egreso.setCurrency       ( rs.getString("currency"       ) );
                egreso.setConcept_code(rs.getString("concept_desc"   ) );
                egreso.setItem_no        ( rs.getString("desc_documento" ) ); //Descripcion Documento
                egreso.setFechaEntrega   ( rs.getString("fecha_cheque") );//fecha cheque
                egreso.setTransaccion    ( rs.getInt("transaccion"));
                //AMATURANA 30.04.2007
                egreso.setValor_for(rs.getDouble("vlr_for"));
                egreso.setValor(rs.getDouble("vlr"));
            }
            
        }catch( Exception e ){
            throw new Exception( "Error en la rutina obtenerCabeceraEgreso [EgresoDAO]...\n" + e.getMessage() );
        }
        finally{
            if( rs != null ) rs.close();
            if( st != null ) st.close();
            this.desconectar( "SQL_CONSULTAR_CABECERA_EGRESO" );
            
        }
    }
    
    /*
     * Descripcion :    Funcion que obtiene el detalle de un egreso
     * @autor :         David Pi�a Lopez
     * @version :       1.0
     */
    public void obtenerDetalleEgreso() throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        egresos = new Vector();
        try{
            
            st = this.crearPreparedStatement("SQL_CONSULTAR_DET_EGRESO");
            st.setString( 1, egreso.getDstrct() );
            st.setString( 2, egreso.getBranch_code() );
            st.setString( 3, egreso.getBank_account_no() );
            st.setString( 4, egreso.getDocument_no() );
            
            rs = st.executeQuery();
            
            while ( rs.next() ){
                Egreso dt = new Egreso();
                dt.setItem_no( rs.getString("item_no") );
                dt.setVlr( rs.getFloat("vlr") );
                dt.setOc( rs.getString("oc") );
                dt.setDocument_no( rs.getString("documento") );
                dt.setPayment_name( rs.getString("description") );
                dt.setCurrency( rs.getString("currency") );
                dt.setVlr_for( rs.getFloat("vlr_for"));//AMATURANA
                //AMATURANA 30.04.2007
                dt.setValor_for(rs.getDouble("vlr_for"));
                dt.setValor(rs.getDouble("vlr"));
                egresos.add( dt );
            }
        }catch( Exception e ){
            throw new Exception( "Error en la rutina obtenerDetalleEgreso [EgresoDAO]...\n" + e.getMessage() );
        }
        finally{
            if( rs != null ) rs.close();
            if( st != null ) st.close();
            this.desconectar( "SQL_CONSULTAR_DET_EGRESO" );
            
        }
    }
    
    



    /***********************Ver valro egreso filtrado por la cxp**********************************/
    //jpinedo
     public double getVlr(String neg)throws SQLException{
       double vlr=0;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_GET_VLR";
        try {

            con = this.conectarJNDI(query);
            if(con!=null)
            {
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, neg);
            rs = ps.executeQuery();



                while(rs.next()){

                    vlr =rs.getDouble("vlr");

                }


        }
        }
        catch(SQLException e)
        {

            throw new SQLException("ERROR DURANTE get vlr " + e.getMessage() + " " + e.getErrorCode());
        }
        finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return vlr;
    }


}

