/*
 * MovPlaService.java
 *
 * Created on 26 de noviembre de 2004, 02:32 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  KREALES
 */
public class MovPlaService {
    
    MovPlaDAO movpla;
    /** Creates a new instance of MovPlaService */
    public MovPlaService() {
        movpla=new MovPlaDAO();
        
    }
    
    /*  Julio Barros 21-11-2006 */
     /*public String insertExtracto(Extracto ex)throws SQLException{
        try{
            //return movpla.insertExtracto(ex);
        }
        catch(SQLException e){
            throw new SQLException(" query "+e.getMessage());
        }
    }*/
    
    public String insertExtractoDetalle(ExtractoDetalle exd)throws SQLException{
        try{
            return movpla.insertExtractoDetalle(exd);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /*public String insertMovPla(Movpla pla, String base)throws SQLException{
        try{
            movpla.setPlanilla(pla);
            return movpla.insertMovPla(base);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }*/
    
    public String updateMovPla(Movpla pla)throws SQLException{
        try{
            movpla.setPlanilla(pla);
            return movpla.updateMovPla();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public Movpla getMovPla()throws SQLException{
        
        return movpla.getMovPla();
        
    }
    public void buscaMovpla(String pla, String concep_code)throws SQLException{
        try{
            movpla.searchMovPla(pla, concep_code);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void buscaMovpla(String pla, String concep_code, String creation_date)throws SQLException{
        try{
            movpla.searchMovPla(pla, concep_code, creation_date);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void searchCheque(String cheque, String banco, String sucursal )throws SQLException{
        movpla.searchCheque(cheque,banco,sucursal);
    }
    public String anularCheque()throws SQLException{
       return  movpla.anularCheque();
    }
    public void setMovPla(Movpla mp){
        movpla.setMovPla(mp);
    }
     public boolean existenCheques(String cheque1, String cheque2, String banco, String sucursal )throws SQLException{
        return  movpla.existenCheques(cheque1, cheque2, banco, sucursal);
    }
    public boolean existeSerie(String base,String banco, String cuenta,int ultnum)throws SQLException{
        return movpla.existeSerie(base,banco,cuenta,ultnum);
    }
    public String insertMovPlaChequeNull(Movpla pla,String base)throws SQLException{
        movpla.setMovPla(pla);
        return movpla.insertMovPlaChequeNull(base);
    }
    public String anularChequeImpreso()throws SQLException{
        return movpla.anularChequeImpreso();
    }
    public void BuscarChequeNoImpreso( String numpla )throws SQLException{
        movpla.BuscarChequeNoImpreso(numpla);
    }
    public void BuscarChequeImpreso( String cheque, String banco,  String sucursal )throws SQLException{
        movpla.BuscarChequeImpreso(cheque, banco, sucursal);
    }
    public void anularChequeNoImpreso()throws SQLException{
        movpla.anularChequeNoImpreso();
    }
    
    public boolean AnticipoAnulado(String planilla )throws SQLException{
        return movpla.AnticipoAnulado(planilla);
    }
    public boolean ChequeAnulado(String cheque, String banco, String sucursal )throws SQLException{
        return movpla.ChequeAnulado(cheque, banco, sucursal);
    }
    public Vector obtenerMovimientosNoMigrados() throws SQLException {
        return movpla.obtenerMovimientosNoMigrados();
    }
    
   
    public float obtenerValor(String distrito, String idAgencia, String numpla) throws SQLException {
        return movpla.obtenerValor(distrito,idAgencia,numpla);
    }
    public String deleteMovPla()throws SQLException{
        return movpla.deleteMovPla();
    }
    public float totalAjuste(String numpla )throws SQLException{
        return movpla.totalAjuste(numpla);
    }
    public void buscarAnticipo(String numpla)throws SQLException{
        movpla.buscarAnticipo(numpla);
    }
    
    // Alejandro Payares
    public Vector obtenerDatosInterfaceContable() throws SQLException {
        return movpla.obtenerDatosInterfaceContable();
    }
    
    public String obtenerFechaActualEnFormato(String patron) throws SQLException{
        return movpla.obtenerFechaActualEnFormato(patron);
    }
    
    public void actualizarMovimientosMigrados(Vector movimientos) throws SQLException{
        movpla.actualizarMovimientosMigrados(movimientos);
    }
    public String insertAutReanticipo()throws SQLException{
        return movpla.insertAutReanticipo();
    }
    public void buscarAnticipos(String numpla)throws SQLException{
        movpla.buscarAnticipos(numpla);
    }
    public void buscarCheques(String numpla)throws SQLException{
        movpla.buscarCheques(numpla);
    }
    public java.util.Vector getLista() {
        return movpla.getLista();
    }
    public void setLista(java.util.Vector lista) {
        movpla.setLista(lista);
    }
      /**
     *Metodo que busca los cheques generados de una planilla
     *@autor: Karen Reales
     *@param: Numero de la planilla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void guardarCheque(String numpla) throws  SQLException {
        movpla.guardarCheque(numpla);
    }
      /**
     *Metodo que inicializa el vector lista
     *@autor: Karen Reales
     */
    public void iniciarLista(){
        movpla.iniciarLista();
    }
       /**
     *Metodo que busca el cheque de una planilla especificar
     *@autor: Karen Reales
     *@param: Numero del cheque, Codigo de banco, Codigo agencia, Numero de la planilla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void searchChequePlanilla(String cheque, String banco, String sucursal,String numpla )throws SQLException{
        movpla.searchChequePlanilla(cheque, banco, sucursal,numpla);
    }
     /**
     *Metodo que busca un movpla dado un concepto y una planilla, si se encuentran
     *varios registos, se agrupan y se suma el valor
     *@autor: Karen Reales
     *@param: Numero de la planilla
     **@param: Codigo del concepto
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarValorConcepto(String numpla, String concep_code )throws SQLException{
        movpla.buscarValorConcepto(numpla, concep_code);
    }
         /**
     *Metodo que busca el valor o la suma de valores de reanticipos generados a una planilla
     *@autor: Karen Reales
     *@param: Numero de la planilla
     **@param: Codigo del concepto
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void searchReanticipos(String numpla, String concep_code )throws SQLException{
        movpla.searchReanticipos(numpla, concep_code);
    }
     /**
     *Metodo que busca un cheque imporeso de reanticpo 
     *@autor: Karen Reales
     *@param: Numero de la planilla
     **@param: Codigo del concepto
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void searchChequesReanticipos(String numpla, String numcheque )throws SQLException{
        movpla.searchChequesReanticipos(numpla, numcheque);
    }
        /**
     *Metodo que marca en fecha de migracion los registros 
     *@autor: Karen Reales
     *@param: Movpla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void migrarMovpla(String planilla, String creacion) throws SQLException {
        movpla.migrarMovpla(planilla, creacion);
    }
    /**
     *Metodo que genera una  lista de MOVPLA dado un numero de cheque
     *@autor: Karen Reales
     *@param: String numero de cheque
     *@param: String Banco
     *@param: String Agencia del banco
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void listaPlanillaCheque(String no_cheque, String branch, String account)throws SQLException{
        movpla.listaPlanillaCheque(no_cheque, branch, account);
    }
        /**
     * Metodo para extraer los movimientos para el proceso de migracion de 
     * cheques
     * @autor mfontalvo
     * @param fechaProceso fecha para restringir hasta donde se extraeran los datos
     * @throws Exception.
     */
    public Vector obtenerMovimientosNoMigrados(String fechaProceso) throws SQLException {
        return movpla.obtenerMovimientosNoMigrados(fechaProceso);
    }
    /**
     * Metodo para extraer los movimientos para el proceso de migracion de 
     * del archivo de formato ms266
     * @autor mfontalvo
     * @param fechaProceso fecha para restringir hasta donde se extraeran los datos
     * @throws Exception.
     */
    public Vector obtenerDatosInterfaceContable(String fechaProceso) throws SQLException {
        return movpla.obtenerDatosInterfaceContable(fechaProceso);
    }    
    /**
     * Metodo para actualizar los movimientos de planilla migrados
     * @autor mfontalvo
     * @param mp, movimiento de planilla a actualizar
     * @param fechaProceso fecha para restringir hasta donde se extraeran los datos
     * @throws Exception.
     */
    public void actualizarMovplaMigrado(Movpla mp, String fechaProceso) throws SQLException {
        movpla.actualizarMovplaMigrado(mp, fechaProceso);
    }
    
      /**
     *Metodo que lista los datos del Movpla de una planilla
     *@autor: Karen Reales
     *@param: Numero de la planilla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void listaAllMovPla(String numpla)throws SQLException{
        movpla.listaAllMovPla(numpla);
    }
    
    /**
     *Metodo que retorna el nit del propietario o el nit del beneficiario encontrado
     *en la tabla proveedor segun el propietario dado.
     *@autor: Karen Reales
     *@param: Tipo (C,T o P) y placa.
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String buscarBeneficiarioCheque(String tipo, String placa )throws SQLException{
        return movpla.buscarBeneficiarioCheque(tipo, placa);
    }

         /**
     *Metodo que busca los anticipos entregados a una planilla que no
     *hallan sido impresos.
     *@autor: Karen Reales
     *@param: Numero de la planilla
     *@throws: SQLException  En caso de que un error de base de datos ocurra.
     */
    public void buscarAnticipoSinCheque(String numpla, String nitcia)throws SQLException{
        movpla.buscarAnticipoSinCheque(numpla,nitcia);
    }
    
    public String insertMovPla2(Movpla pla, String base)throws SQLException{
        try{
            movpla.setPlanilla(pla);
            return movpla.insertMovPla2(base);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    
     /**
     * Metodo para actualizar los movimientos de planilla 
     * @autor mfontalvo
     * @param mp, objeto movpla migrado
     * @param fechProceso fecha de Proceso de migracion
     * @throws Exception.
     */    
    public void actualizarMovplaMigradoCHEQUE(Movpla mp, String fechaProceso) throws Exception {
        movpla.actualizarMovplaMigradoCHEQUE(mp, fechaProceso);
    }
    
    /**
   *Metodo que inserta los movimientos de valores relacionados a la
   *planilla en la tabla movpla
   *@autor Karen Reales, Andr�s Maturana
   *@param base
   *@throws En caso de que un error de base de datos ocurra.
   */

    public String insertMovPlaDouble(Movpla pla, String base)throws SQLException{
        try{
            movpla.setPlanilla(pla);
            return movpla.insertMovPlaDouble(base);
        }
        catch(SQLException e){
            e.printStackTrace();
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     *Metodo que reversa en movpla el cheque
     *@autor: Karen Reales
     *@param: Numero de la planilla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String registrarReversionCheque()throws SQLException{
        return movpla.registrarReversionCheque();
    }
    
      /**
     *Metodo que lista los datos del Movpla de una planilla con las facturas relacionadas
     *@autor: Ing. Juan M. Escandon 
     *@param: Numero de la planilla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void listaAllMovPla_Factura(String numpla)throws SQLException{
        movpla.listaAllMovPla_Factura(numpla);
    }


}
