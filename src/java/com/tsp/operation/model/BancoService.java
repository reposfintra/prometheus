package com.tsp.operation.model;

import com.tsp.operation.model.beans.Banco;
import java.sql.SQLException;
import com.tsp.operation.model.BancoDAO;
import java.util.Vector;
import java.util.*;//Tito Andr�s

public class BancoService {
    
    private BancoDAO dao;
    private TreeMap banco = new TreeMap();
    private TreeMap sucursal = new TreeMap();
    private TreeMap sucursalDes = new TreeMap();
    private Vector vecBancos   = new Vector();
    
    public BancoService() {
        dao = new BancoDAO();
    }
    public BancoService(String dataBaseName) {
        dao = new BancoDAO(dataBaseName);
    }
    public Banco getBannco( )throws SQLException{
        return dao.getBannco();
    }
    
    public Vector getBancos() {
        return dao.getBancos();
    }
    public void setBannco(Banco ban) {
        dao.setBannco(ban);
    }
    public boolean existeBanco(String dstrct, String branch_code, String bank_account_no) throws java.sql.SQLException{
        return this.dao.existeBanco(dstrct, branch_code, bank_account_no);
    }
    public void searchBanco(String dstrct, String branch_code, String bank_account_no, String account_number)throws SQLException{
        dao.searchBanco(dstrct, branch_code, bank_account_no, account_number);
    }
    public void updateBanco(Banco ban)throws SQLException{
        this.dao.setBannco(ban);
        this.dao.updateBanco();
    }
   /**
     * Obtiene un registro del archivo de banco.
     * @param distrito C�digo del distrito
     * @param banco Nombre del banco
     * @param sucursal Nombre de la sucursal
     * @param cuenta N�mero de la cuenta
     * @param agencia C�digo de la agencia
     * @param ccuenta C�digo de la cuenta
     * @param posbanc Activo para posicion bancaria (S o N)
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void searchDetalleBanco(String distrito, String banco, String sucursal, String cuenta, String agencia, String ccuenta, String pbanc) throws SQLException{
        try{
            dao.searchDetalleBanco(distrito, banco, sucursal, cuenta, agencia, ccuenta, pbanc);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void anularBanco(Banco ban)throws SQLException{
        try{
            dao.setBannco(ban);
            dao.anularBanco();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public void setBancos(Vector Bancos) {
        dao.setBancos(Bancos);
    }
    public void insertarBanco(Banco dao) throws java.sql.SQLException{
        this.dao.insertarBanco();
    }
    
    
    public Banco obtenerBanco(String dstrct, String branch_code, String bank_account_no) throws SQLException {
        return dao.obtenerBanco(dstrct, branch_code, bank_account_no);
    }
    
    public Vector obtenerBancos() throws SQLException{
        return dao.obtenerBancos();
    }
    
    //Tito Andr�s 28.09.2005
    public TreeMap obtenerNombresBancos() throws SQLException{
        TreeMap bancos = new TreeMap();
        
        Vector banks = this.obtenerBancos();
        ////System.out.println("tam"+ banks.size());
        for(int i=0; i<banks.size(); i++){
            Banco banc = (Banco) banks.elementAt(i);
            bancos.put(banc.getBanco(), banc.getBanco().trim());
        }
        banco = bancos; //Modificacion para no violar mvc
        return bancos;
    }
    
      public TreeMap obtenerSucursalesBanco(String banco) throws SQLException{
        TreeMap bancos = new TreeMap();
        
        Vector banks = this.obtenerBancos();
        this.setVecBancos(banks);
        
        for(int i=0; i<banks.size(); i++){
            Banco banc = (Banco) banks.elementAt(i);
            
            if( banc.getBanco().matches(banco) )
                bancos.put(banc.getBank_account_no(), banc.getBank_account_no());
        }
        sucursal = bancos; //Modificacion para obtener el TreeMap en la vista sin violar mvc
        return bancos;
    }
    
    public Vector obtenerBancosPorNombre(String nombre) throws SQLException{
        return dao.obtenerBancosPorNombre(nombre);
    }
    
    //David 23.12.05
    /**
     * Este m�todo genera un Vector con los Bancos pertenecientes a una agencia
     * @param String agencia
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public Vector obtenerBancosPorAgencia(String agencia) throws SQLException{
        return dao.obtenerBancosPorAgencia(agencia);
    }
    
    
    /**
     * Este m�todo genera un Treemap con los Bancos pertenecientes a una agencia
     * @param String agencia
     * @throws
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public TreeMap obtenerNombresBancosPorAgencia(String agencia) throws SQLException{
        TreeMap bancos = new TreeMap();
        
        Vector banks = this.obtenerBancosPorAgencia(agencia);
        
        for(int i=0; i<banks.size(); i++){
            Banco banc = (Banco) banks.elementAt(i);
            bancos.put(banc.getBanco(), banc.getBanco());
        }
        
        return bancos;
    }
    
    /**
     * Getter for property sucursal.
     * @return Value of property sucursal.
     */
    public java.util.TreeMap getSucursal() {
        return sucursal;
    }
     public java.util.TreeMap getSucursalDes() {
        return sucursalDes;
    }
    
    /**
     * Setter for property sucursal.
     * @param sucursal New value of property sucursal.
     */
    public void setSucursal(java.util.TreeMap sucursal) {
        this.sucursal = sucursal;
    }
    public void setSucursalDes(java.util.TreeMap sucursal) {
        this.sucursalDes = sucursalDes;
    }
  
    
    public void loadSusursales(String banco)throws SQLException{
        this.sucursal = obtenerSucursalesBanco(banco);
    }
    /**
     * Setea la propiedad banco con los bancos por agencia.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param agency C�digo de la agencia
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @see obtenerNombresBancosPorAgencia(String)
     * @version 1.0
     */
    public void loadBancosAgencia(String agency, String dstrct) throws SQLException{
        TreeMap bancos = new TreeMap();
        
        this.obtenerBancosAgenciaCia(agency, dstrct);
        Vector banks = dao.getBancos();
        
        for(int i=0; i<banks.size(); i++){
            Banco banc = (Banco) banks.elementAt(i);
            bancos.put(banc.getBanco(), banc.getBanco());
        }
        
        this.banco = bancos;
    }
    
    /**
     * Setea la propieda sucursal por banco.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param banco Nombre del banco
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @see obtenerSucursalesBanco(String)
     * @version 1.0
     */
    public void loadSucursales(String agencia, String banco, String dstrct) throws SQLException{
        TreeMap bancos = new TreeMap();
        
        this.obtenerSucursales(agencia, banco, dstrct);
        Vector banks = dao.getBancos();
        
        for(int i=0; i<banks.size(); i++){
            Banco banc = (Banco) banks.elementAt(i);
            bancos.put(banc.getBank_account_no(), banc.getBank_account_no());
        }
        
        this.sucursal = bancos;
    }
    
    /**
     * Obtiene las sucursales de los bancos teniendo en cuenta la agencia y el banco.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param agencia C�digo de la agencia
     * @param banco Nombre del banco
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @see com.tsp.operation.model.BancoDAO#obtenerSucursales(String, String)
     * @version 1.0
     */
    public void obtenerSucursales(String agencia, String banco, String dstrct) throws SQLException {
        dao.obtenerSucursalesPorAgencia(agencia, banco, dstrct);
    }
    
    /**
     * Obtiene los bancos activos para posici�n banacaria pertenecientes a un distrito.
     * teniendo en cuenta la agencia y el banco.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param agencia C�digo de la agencia
     * @param dstrct Distrito
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.1
     */
    public void obtenerBancosAgenciaCia(String agencia, String dstrct) throws SQLException {
        dao.obtenerBancosAgenciaCia(agencia, dstrct);
    }
    
    /**
     * Verifica si existe un registro en el archivo de banco.
     * @autor Ing. Andr�s Maturana D.
     * @param dstrct C�digo del distrito
     * @param branch_code Nombre del banco
     * @param bank_account_no Nombre de la sucursal
     * @param account_number N�mero de la cuenta
     * @returns <code>true</code> si existe, <code>false</code> si no.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String estadoBanco(String dstrct, String branch_code, String bank_account_no, String account_number) throws SQLException {
        return dao.estadoBanco(dstrct, branch_code, bank_account_no, account_number);
    }
    /**
      * Getter for property banco.
      * @return Value of property banco.
      */
     public java.util.TreeMap getBanco() {
         return banco;
     }
     
     /**
      * Setter for property banco.
      * @param banco New value of property banco.
      */
     public void setBanco(java.util.TreeMap banco) {
         this.banco = banco;
    }     
      /**
     * Obtiene los bancos activos para posici�n banacaria pertenecientes a un distrito - sin imporatar la posici�n bancaria.
     * teniendo en cuenta la agencia y el banco.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param agencia C�digo de la agencia
     * @param dstrct Distrito
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.1
     */
    public void obtenerBancos(String agencia, String dstrct) throws SQLException {
        dao.obtenerBancos(agencia, dstrct);
    }
    
    /**
     * Carga los nombres de los bancos en la propiedad banco.
     * @autor Ing. Andr�s Maturana D. 
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void loadBancos(String agencia, String dstrct) throws SQLException{
        this.banco = new TreeMap();
        this.obtenerBancos(agencia, dstrct);
        Vector vec = dao.getBancos();
        
        for(int i=0; i<vec.size(); i++){
            Banco banc = (Banco) vec.elementAt(i);
            this.banco.put(banc.getBanco(), banc.getBanco());
        }
    }
    
    /**
     * Carga los nombres de las sucursales en la propiedad sucursal.
     * @autor Ing. Andr�s Maturana D. 
     * @param banco Nombre del banco.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void loadSucursalesAgencia(String agencia, String banco, String dstrct) throws SQLException{
        this.sucursal = new TreeMap();
        this.obtenerSucursalesAgencia(agencia, banco, dstrct);
        Vector vec = dao.getBancos();
        
        for(int i=0; i<vec.size(); i++){
            Banco banc = (Banco) vec.elementAt(i);
            this.sucursal.put(banc.getBank_account_no(), banc.getBank_account_no());
        }
    }
    public void loadSucursalesAgenciaDes(String agencia, String banco, String dstrct) throws SQLException{
        this.sucursalDes = new TreeMap();
        this.obtenerSucursalesAgencia(agencia, banco, dstrct);
        Vector vec = dao.getBancos();
        
        for(int i=0; i<vec.size(); i++){
            Banco banc = (Banco) vec.elementAt(i);
            this.sucursalDes.put(banc.getBank_account_no(), banc.getBank_account_no());
        }
    }
    
    
    /**
     * Obtiene las sucursales de los bancos sin importar la posici�n banacaria
     * teniendo en cuenta la agencia y el banco.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param agencia C�digo de la agencia
     * @param banco Nombre del banco
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.1
     */
    public void obtenerSucursalesAgencia(String agencia, String banco, String dstrct) throws SQLException {
        dao.obtenerSucursales(agencia, banco, dstrct);
    }
    
    /**
     * Obtiene los bancos de un distrito
     * @autor Ing. David Pi�a Lopez
     * @param dstrct C�digo del distrito
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void obtenerBancos ( String dstrct ) throws SQLException {
        dao.obtenerBancos( dstrct );
    }
         
    /**
     * Getter for property vecBancos.
     * @return Value of property vecBancos.
     */
    public java.util.Vector getVecBancos() {
        return vecBancos;
    }    
    
    /**
     * Setter for property vecBancos.
     * @param vecBancos New value of property vecBancos.
     */
    public void setVecBancos(java.util.Vector vecBancos) {
        this.vecBancos = vecBancos;
    }
     /**
     * Obtiene un banco-sucursal
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct C�digo del distrito
     * @param banco Nombre del banco
     * @param sucursal Nomnre de la sucursal
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */    
    public void obtenerBancoSucursal(String dstrct, String banco, String sucursal) throws SQLException {
        this.dao.obtenerBancoSucursal(dstrct, banco, sucursal);
    }
   
     /**
     * Carga los nombres de las sucursales en la propiedad sucursal.
     * @autor Ing. Jose de la rosa
     * @param banco Nombre del banco.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void loadSucursalesAgenciaCuenta(String agencia, String banco, String dstrct) throws SQLException{
        this.sucursal = new TreeMap();
        this.obtenerSucursales(agencia, banco, dstrct);
        Vector vec = dao.getBancos();
        
        for(int i=0; i<vec.size(); i++){
            Banco banc = (Banco) vec.elementAt(i);
            this.sucursal.put(banc.getBank_account_no(), banc.getBank_account_no()+"/"+banc.getCuenta());
        }
    }
   
     /**
     * Verifica si una cuenta existe en la tabla con.cuentas
     * @autor LUIGI
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    
    public boolean existeCuenta(String cuenta) throws SQLException {
        return dao.existeCuenta(cuenta);
    }
    
    public String bancoAnulado(String distrito, String banco, String sucursal) throws SQLException {
        return dao.bancoAnulado(distrito, banco, sucursal);
    }
    
    public void bancoAnuladoCambiar(String distrito, String banco, String sucursal)throws SQLException {
        dao.bancoAnuladoCambiar(distrito, banco, sucursal);
    }
    
    public boolean bancoExiste(String distrito, String banco, String sucursal) throws SQLException {
        return dao.bancoExiste(distrito, banco, sucursal);
    }

    public String obtenerCuentaHC(String handle_code) throws SQLException {
        return dao.obtenerCuentaHC(handle_code);
    }

    public String obtenerCuentaGasto(String documento) throws SQLException {
        return dao.obtenerCuentaGasto(documento);
    }
    
    
}