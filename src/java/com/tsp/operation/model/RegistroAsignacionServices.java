package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import com.tsp.operation.model.beans.*;

public class RegistroAsignacionServices {

    RegistroAsignacionDAO regDao;

    public RegistroAsignacionServices() {
        regDao = new RegistroAsignacionDAO();
    }

    public void guardarRegistroAsignacion(RegistroAsignacion reg) throws SQLException {
        regDao.guardarRegistroAsignacion(reg);
    }
    //KREALES
    public void generarReporte(String fi, String ff) throws SQLException {
        regDao.generarReporte(fi, ff);
    }
    
    /**
     * ALEJANDRO
     *
     * @param recurso Recursosdisp
     * @param requerimiento ReqCliente
     */
    public void desasignarRecursosYRequerimientos( Recursosdisp recurso, ReqCliente requerimiento, boolean reasignar )throws SQLException {
        regDao.desasignarRecursosYRequerimientos( recurso, requerimiento,  reasignar );
    }
    
    public Recursosdisp obtenerRecursoDeRegistro(String distrito, String placa, String fechaDisponibilidad) throws SQLException {
        return regDao.obtenerRecursoDeRegistro(distrito, placa, fechaDisponibilidad);
    }
    
    public ReqCliente obtenerRequerimientoDeRegistro( String dstrct_code, String num_sec, String std_job_no,
                                            String fecha_dispo, String numpla ) throws SQLException {
        return regDao.obtenerRequerimientoDeRegistro(dstrct_code, num_sec, std_job_no, fecha_dispo, numpla);
    }
    
    public void insertarRegistrosNoAsignados() throws SQLException{
        regDao.insertarRegistrosNoAsignados();
    }
    public void limpiarAsignacion(Recursosdisp rec, ReqCliente req) throws SQLException{
        regDao.limpiarAsignacion(rec, req);
    }
     /**
     * Metodo que borra toda la taba de registro_asignacion
     * @throws SQLException
     */
    public void limpiarAsignacionCompleta() throws SQLException{
        regDao.limpiarAsignacionCompleta();
    }
}
