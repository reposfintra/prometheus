//*************************************** clase modificada
package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.*;
import javax.swing.*;
import java.text.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.Util;
import java.util.Calendar;
import com.tsp.operation.model.beans.Recursosdisp;
import org.apache.log4j.Logger;
import com.tsp.util.LogWriter;

/*
 * AsignarYSincronizar.java
 *
 * Created on June 18, 2005, 4:43 PM
 */

/**
 *
 * @author Alejandro
 */
public class AsignarYSincronizar implements Runnable {
    
    static Logger logger = Logger.getLogger(AsignarYSincronizar.class);
    
    private Model model = new Model();
    private String usu;
    private PrintWriter pw;
    private LogWriter logWriter;
    /** Creates a new instance of AsignarYSincronizar */
    public AsignarYSincronizar(String us) {
        
        Calendar hoy = Calendar.getInstance();
        java.util.Date pdate = hoy.getTime();
        SimpleDateFormat fecha = new java.text.SimpleDateFormat("yyyy-MM-dd");
        
        String fechadoc = fecha.format(pdate);
        
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String ruta = rb.getString("ruta")+ "/exportar/migracion/"+us;
        
        File file = new File(ruta);
        file.mkdirs();
        
        pw = new PrintWriter(System.err, true);
        
        String logFile = ruta + "/Planeacion"+fechadoc+".log";
        try{
            pw = new PrintWriter(new FileWriter(logFile, true), true);
        }catch(IOException e){
            e.printStackTrace();
        }
        logWriter = new LogWriter("Planeacion", LogWriter.INFO, pw);
        logWriter.setPrintWriter(pw);
        
        Thread t = new Thread( this, "Proceso de asignaci�n y sincronizaci�n" );
        usu = us;
        t.start();
        
    }
    
    /**
     * @param args the command line arguments
     */
    public void principalAYS( ) {
        new AsignarYSincronizar(usu);
    }
    
    private void ordenarRecursoPorPrioridad(int prioridad, String tipo, String recurso, String [][] cabezotes, String [][] trailers){
        if ( prioridad < 0 || prioridad > 4 ){
            return;
        }
        
        if ( tipo.equalsIgnoreCase("C") ){
            cabezotes[prioridad][0] = recurso;
            cabezotes[prioridad][1] = tipo;
        }
        else if ( tipo.equalsIgnoreCase("T") ){
            trailers[prioridad][0] = recurso;
            trailers[prioridad][1] = tipo;
        }
    }
    
    private int convertirAEntero(String str){
        try {
            return Integer.parseInt(str);
        }catch(Exception ex){
            return 0;
        }
    }
    
    public void run() {
        //System.out.println("Empece a correr....");
        logWriter.log("NUEVA ASIGNACION....",LogWriter.INFO);
        int dias_frontera=6;
        
        SimpleDateFormat timestam = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            
            File file = new File(path + "/exportar/migracion/ADMIN/");
            file.mkdirs();
            
            
            model.LogProcesosSvc.InsertProceso("Asignacion", this.hashCode(), "Recursos vs. Requerimientos", this.usu);
            
            model.regAsigService.limpiarAsignacionCompleta();
            
            Vector requerimientos = model.reqclienteServices.obtenerRequerimientosNoAsignados();
            Vector reqInt = new Vector();
            logWriter.log("Requerimientos no asignados = "+requerimientos.size(),LogWriter.INFO);
            logWriter.log("Requerimientos no asignados = "+requerimientos.size(),LogWriter.INFO);
            
            int KI = Integer.parseInt( rb.getString( "KI" ) );
            int KF = Integer.parseInt( rb.getString( "KF" ) );
            
            logWriter.log( "KI = " + KI,LogWriter.INFO );
            logWriter.log("KF = " + KF ,LogWriter.INFO);
            
            model.rdSvc.setAsignados(new Vector());
            
            for ( int i = 0; i < requerimientos.size(); i++ ) {
                boolean retornar=false;
                ReqCliente req = ( ReqCliente ) requerimientos.elementAt( i );
                String[][] cabezotesRequeridos = new String[5][2];
                String[][] trailersRequeridos = new String[5][2];
                
                int p = convertirAEntero(req.getprioridad1());
                ordenarRecursoPorPrioridad(p-1,req.getTipo_recurso1(),req.getrecurso1(),cabezotesRequeridos,trailersRequeridos);
                
                p = convertirAEntero(req.getprioridad2());
                ordenarRecursoPorPrioridad(p-1,req.getTipo_recurso2(),req.getrecurso2(),cabezotesRequeridos,trailersRequeridos);
                
                p = convertirAEntero(req.getprioridad3());
                ordenarRecursoPorPrioridad(p-1,req.getTipo_recurso3(),req.getrecurso3(),cabezotesRequeridos,trailersRequeridos);
                
                p = convertirAEntero(req.getprioridad4());
                ordenarRecursoPorPrioridad(p-1,req.getTipo_recurso4(),req.getrecurso4(),cabezotesRequeridos,trailersRequeridos);
                
                p = convertirAEntero(req.getprioridad5());
                ordenarRecursoPorPrioridad(p-1,req.getTipo_recurso5(),req.getrecurso5(),cabezotesRequeridos,trailersRequeridos);
                
                String [][] recursosRequeridos = new String [5][2];
                int j;
                logWriter.log("",LogWriter.INFO);
                logWriter.log("",LogWriter.INFO);
                logWriter.log("Requerimiento "+(i+1)+" STD "+req.getstd_job_no()+"... origen requerido: "+req.getorigen()+" Destino : "+req.getdestino()+", recursos requeridos:",LogWriter.INFO);
                logWriter.log("TRAILER QUE TRAE EL REQUERIMIENTO : "+req.getId_rec_tra(),LogWriter.INFO);
                
                for( j=0; j<5; j++ ){
                    if ( cabezotesRequeridos[j][0] != null ){
                        recursosRequeridos[j][0] = cabezotesRequeridos[j][0];
                        recursosRequeridos[j][1] = cabezotesRequeridos[j][1];
                        logWriter.log("Recurso requerido "+(j+1)+", tipo = "+cabezotesRequeridos[j][1]+", recurso: "+cabezotesRequeridos[j][0],LogWriter.INFO);
                    }
                    else {
                        break;
                    }
                }
                
                for( int k=0; j<5; k++,j++ ){
                    if ( trailersRequeridos[k][0] != null ){
                        recursosRequeridos[j][0] = trailersRequeridos[k][0];
                        recursosRequeridos[j][1] = trailersRequeridos[k][1];
                        logWriter.log("Recurso requerido "+(j+1)+", tipo = "+trailersRequeridos[k][1]+", recurso: "+trailersRequeridos[k][0],LogWriter.INFO);
                    }
                    else {
                        break;
                    }
                }
                
                /*
                 *Buscando recursos no asignados que tengan agencia asociada al destino igual a la agencia
                 *asociada al origen del requerimiento
                 */
                Vector recursos = model.rdSvc.obtenerRecursosNoAsignados( req, KI, KF );///modif
                logWriter.log("recursos encontrados en: "+req.getAgaAsoc()+" = "+recursos.size(),LogWriter.INFO);
                
                Vector trailersDisponibles = new Vector();
                Vector cabezotesDisponibles = new Vector();
                
                // buscamos entre los recursos aquellos que cumplan con las condiciones de fecha y hora del requerimiento
                for ( j = 0; j < recursos.size(); j++ ) {
                    
                    Recursosdisp rec = ( Recursosdisp ) recursos.elementAt( j );
                    if ( rec.getClase().equalsIgnoreCase( "T" ) ) {
                        trailersDisponibles.addElement( rec );
                    }
                    if(rec.getClase().equalsIgnoreCase( "C" )) {
                        cabezotesDisponibles.addElement( rec );
                        
                    }
                    logWriter.log("Placa: "+rec.getPlaca(),LogWriter.INFO);
                }
                if ( trailersDisponibles.isEmpty() && cabezotesDisponibles.isEmpty() ){
                    logWriter.log("Requerimiento "+(i+1)+" no pudo ser satisfecho por falta de recursos",LogWriter.INFO);
                    continue;
                }
                
                Recursosdisp cabIdeal = null; // el cabezote ideal
                Recursosdisp traIdeal = null; // el trailer ideal
                
                
                for ( j = 0; j < recursosRequeridos.length; j++ ) {
                    
                    if ( recursosRequeridos[j][1] != null && recursosRequeridos[j][1].equalsIgnoreCase( "C" ) ) {//prueba
                        
                        Recursosdisp cab = buscarRecurso( recursosRequeridos[j][0],cabezotesDisponibles,req );
                        
                        if ( ( cabIdeal = cab ) != null ) {
                            logWriter.log("Cabezote requerido encontrado entre los recursos disp -> placa = "+cab.getPlaca()+", recurso = "+cab.getTipo(),LogWriter.INFO);
                            break;
                        }
                    }
                }
                for ( j = 0; j < recursosRequeridos.length; j++ ) {
                    if ( recursosRequeridos[j][1] != null && recursosRequeridos[j][1].equalsIgnoreCase( "T" ) ) {//prueba
                        Recursosdisp tra = buscarRecurso( recursosRequeridos[j][0],
                        trailersDisponibles,req );
                        if ( ( traIdeal = tra ) != null ) {
                            logWriter.log("Trailer requerido encontrado entre los recursos disp -> placa = "+tra.getPlaca()+", recurso = "+tra.getTipo(),LogWriter.INFO);
                            break;
                        }
                    }
                }
                
                
                if( req.getId_rec_tra()!=null){
                    if(!req.getId_rec_tra().equals("")){
                        traIdeal=null;
                        logWriter.log("Encontre un trailer asignado a este requerimiento "+req.getId_rec_tra(),LogWriter.INFO);
                        
                        if(model.rdSvc.obtenerRecurso(req.getdstrct_code(), req.getId_rec_tra(), req.getfecha_dispo())!=null){
                            logWriter.log("Encontre los datos del recurso "+req.getId_rec_tra(),LogWriter.INFO);
                            traIdeal= model.rdSvc.obtenerRecurso(req.getdstrct_code(), req.getId_rec_tra(), req.getfecha_dispo());
                            logWriter.log("AGENCIA DEL TRAILER "+traIdeal.getAgencia(),LogWriter.INFO);
                            logWriter.log("DESTINO DEL VIAJE "+req.getdestino(),LogWriter.INFO);
                            
                            if(traIdeal.getAgencia()!=null){
                                
                                if(!req.getdestino().equals(traIdeal.getAgencia())){
                                    logWriter.log("Si se debe retortar",LogWriter.INFO);
                                    retornar=true;
                                }
                                else{
                                    logWriter.log("NO se debe retortar",LogWriter.INFO);
                                    retornar=false;
                                }
                            }
                        }else{
                            logWriter.log("NO Encontre los datos del recurso "+req.getId_rec_tra(),LogWriter.INFO);
                        }
                    }
                }
                
                /*if(traIdeal==null && traReq!=null){
                    traIdeal=traReq;
                    logWriter.log("AGENCIA DEL TRAILER "+traIdeal.getAgencia(),LogWriter.INFO);
                    if(traIdeal.getAgencia()!=null){
                        if(!traIdeal.getAgencia().equals("")){
                            if(!req.getdestino().equals(traIdeal.getAgencia())){
                                retornar=true;
                            }
                        }
                    }
                    logWriter.log("Trailer requerido encontrado entre los recursos disp -> placa = "+traIdeal.getPlaca()+", recurso = "+traIdeal.getTipo(),LogWriter.INFO);
                }
                 */
                if(cabIdeal!=null){
                    logWriter.log("asignando... requerimiento "+(i+1),LogWriter.INFO);
                    logWriter.log("Es internacional "+req.isInternacional(),LogWriter.INFO);
                    if(req.isInternacional()){
                        logWriter.log("**** ES UN REQUERIMIENTO INTERNACIONAL",LogWriter.INFO);
                        logWriter.log("Origen : "+req.getorigen()+" Destino: "+req.getdestino(),LogWriter.INFO);
                        //req.setnumpla(traIdeal.getPlaca());
                        if(!req.getorigen().equals("PA")  && !req.getdestino().equals("PA") ){
                            String placa="";
                            
                            
                            if(traIdeal!=null){
                                logWriter.log("El trailer no es nulo",LogWriter.INFO);
                                logWriter.log("La placa es "+traIdeal.getPlaca(),LogWriter.INFO);
                                placa =traIdeal.getPlaca();
                                traIdeal.setFrontera(true);
                            }
                            req.setid_rec_tra(placa);
                            reqInt = model.reqclienteServices.crearRequerimientoInternacional(req);
                            
                            if(reqInt.size()>0){
                                
                                ReqCliente rInt = (ReqCliente) reqInt.elementAt(0);
                                logWriter.log("Origen Primer Req ="+rInt.getorigen()+" Destino Primer Req= "+rInt.getdestino(),LogWriter.INFO);
                                logWriter.log("Fecha disponibilidad antes de asignar:  "+rInt.getfecha_dispo(),LogWriter.INFO);
                                
                                model.reqclienteServices.actualizarRequerimientoInternacional(rInt);
                                
                                //Como ya se habia encontrado ese recurso, se asigna.
                                logWriter.log("Realizamos la asignacion.. ",LogWriter.INFO);
                                asignar( cabIdeal, traIdeal, rInt );
                                logWriter.log("Listo! asignamos.. ",LogWriter.INFO);
                                
                                
                                logWriter.log("Si el trailer no es nulo marcamos frontera.. ",LogWriter.INFO);
                                if(traIdeal!=null){
                                    traIdeal.setNumpla_req(req.getstd_job_no());
                                    model.rdSvc.setRdisp(traIdeal);
                                    model.rdSvc.marcarFrontera();
                                    logWriter.log("se marco el trailer.. ",LogWriter.INFO);
                                }
                                
                                //Insertamos el nuevo requerimiento...
                                //logWriter.log("La fecha nueva de disponibilidad para el prox req es:  "+traIdeal==null?cabIdeal!=null?cabIdeal.getNuevaFechaDisponibilidad():"NINGUN DATO":traIdeal.getNuevaFechaDisponibilidad(),LogWriter.INFO);
                                
                                logWriter.log("La fecha nueva de disponibilidad del cabezote:  "+cabIdeal!=null?cabIdeal.getNuevaFechaDisponibilidad():"NINGUN DATO",LogWriter.INFO);
                                //logWriter.log("La fecha nueva de disponibilidad del trailer:  "+traIdeal!=null?traIdeal.getNuevaFechaDisponibilidad():"NINGUN DATO",LogWriter.INFO);
                                rInt = (ReqCliente) reqInt.elementAt(1);
                                
                                if(rInt!=null){
                                    logWriter.log("Agregamos el nuevo req_cliente a la tabla con origen "+rInt.getorigen()+" destino "+rInt.getdestino(),LogWriter.INFO);
                                    //System.out.println("Se agrega el nuevo req cliente con trailer "+req.getId_rec_tra());
                                    
                                    //Se tiene que sumar la constante dias_frontera a la fecha de disponibilidad.
                                    int year=Integer.parseInt(traIdeal==null?cabIdeal.getNuevaFechaDisponibilidad().substring(0,4):traIdeal.getNuevaFechaDisponibilidad().substring(0,4));
                                    int month=Integer.parseInt(traIdeal==null?cabIdeal.getNuevaFechaDisponibilidad().substring(5,7):traIdeal.getNuevaFechaDisponibilidad().substring(5,7))-1;
                                    int date= Integer.parseInt(traIdeal==null?cabIdeal.getNuevaFechaDisponibilidad().substring(8,10):traIdeal.getNuevaFechaDisponibilidad().substring(8,10));
                                    int hora=Integer.parseInt(traIdeal==null?cabIdeal.getNuevaFechaDisponibilidad().substring(11,13):traIdeal.getNuevaFechaDisponibilidad().substring(11,13));
                                    int minuto=Integer.parseInt(traIdeal==null?cabIdeal.getNuevaFechaDisponibilidad().substring(14,16):traIdeal.getNuevaFechaDisponibilidad().substring(14,16));
                                    
                                    Calendar fec_disp = Calendar.getInstance();
                                    fec_disp.set(year,month,date,hora,minuto,0);
                                    
                                    if(traIdeal==null){
                                        fec_disp.add(fec_disp.DATE, dias_frontera);
                                    }
                                    
                                    java.util.Date nueva_fecha = fec_disp.getTime();
                                    
                                    logWriter.log("Nueva fecha de disponibilidad "+timestam.format(nueva_fecha),LogWriter.INFO);
                                    rInt.setfecha_dispo(timestam.format(nueva_fecha));
                                    
                                    logWriter.log("Vamos a insertar en la base de datos el nuevo req",LogWriter.INFO);
                                    logWriter.log("DATOS DEL NUEVO REQ",LogWriter.INFO);
                                    
                                    nueva_fecha = new java.util.Date();
                                    rInt.setUsuario_creacion("AUTOMATICO");
                                    rInt.setfecha_creacion(timestam.format(nueva_fecha));
                                    rInt.setUsuario_actualizacion("AUTOMATICO");
                                    rInt.setfecha_actualizacion(timestam.format(nueva_fecha));
                                    logWriter.log("Placa de Trailer: "+placa,LogWriter.INFO);
                                    rInt.setId_rec_tra(placa);
                                    
                                    model.reqclienteServices.llenarReqcliente(rInt);
                                    requerimientos= new Vector();
                                    logWriter.log("Recargamos la lista de requerimientos..",LogWriter.INFO);
                                    requerimientos = model.reqclienteServices.obtenerRequerimientosNoAsignados();
                                    i=0;
                                }else{
                                    logWriter.log("El sig req es nulo...",LogWriter.INFO);
                                }
                                
                            }
                            
                        }
                        else{
                            asignar( cabIdeal, traIdeal, req );
                            
                            
                        }
                        
                    }
                    else{
                        logWriter.log("NO ES INTERNACIONAL",LogWriter.INFO);
                        if(cabIdeal ==null){
                            logWriter.log("Cabezote nulo",LogWriter.INFO);
                        }
                        if(traIdeal==null){
                            logWriter.log("Trailer nulo",LogWriter.INFO);
                        }
                        asignar( cabIdeal, traIdeal, req );
                        
                        
                    }
                    if(retornar){
                        logWriter.log("-*******RETORNOOOOO**********-",LogWriter.INFO);
                        logWriter.log("La fecha nueva de disponibilidad para el prox req es:  "+traIdeal.getNuevaFechaDisponibilidad(),LogWriter.INFO);
                        
                        int year=Integer.parseInt(traIdeal.getNuevaFechaDisponibilidad().substring(0,4));
                        int month=Integer.parseInt(traIdeal.getNuevaFechaDisponibilidad().substring(5,7))-1;
                        int date= Integer.parseInt(traIdeal.getNuevaFechaDisponibilidad().substring(8,10));
                        int hora=Integer.parseInt(traIdeal.getNuevaFechaDisponibilidad().substring(11,13));
                        int minuto=Integer.parseInt(traIdeal.getNuevaFechaDisponibilidad().substring(14,16));
                        
                        Calendar fec_disp = Calendar.getInstance();
                        fec_disp.set(year,month,date,hora,minuto,0);
                        java.util.Date nueva_fecha = fec_disp.getTime();
                        
                        logWriter.log("Nueva fecha de disponibilidad "+timestam.format(nueva_fecha),LogWriter.INFO);
                        req.setfecha_dispo(timestam.format(nueva_fecha));
                        
                        logWriter.log("Vamos a insertar en la base de datos el nuevo req de RETORNO",LogWriter.INFO);
                        
                        
                        
                        req.setUsuario_creacion("AUTOMATICO");
                        req.setfecha_creacion(timestam.format(nueva_fecha));
                        req.setUsuario_actualizacion("AUTOMATICO");
                        req.setfecha_actualizacion(timestam.format(nueva_fecha));
                        req.setorigen(req.getAgaAsocDest());
                        req.setdestino(traIdeal.getAgencia());
                        req.setId_rec_tra(traIdeal.getPlaca());
                        
                        logWriter.log("ORIGEN "+req.getorigen()+"- DESTINO "+req.getdestino(),LogWriter.INFO);
                        logWriter.log("FECHA DISPO "+req.getfecha_dispo(),LogWriter.INFO);
                        model.reqclienteServices.llenarReqcliente(req);
                        
                        requerimientos = model.reqclienteServices.obtenerRequerimientosNoAsignados();
                        i=0;
                    }
                }else{
                    logWriter.log("NO SE PUDO ASIGNAR EL REQUERIMIENTO..",LogWriter.INFO);
                }
                /*}*/
                logWriter.log("FIN DE LA BUSQUEDA --------"+i,LogWriter.INFO);
            }
            Vector asignados = model.rdSvc.getAsignados();
            logWriter.log("Asignando "+asignados.size()+" registros...",LogWriter.INFO);
            logWriter.log("Asignando "+asignados.size()+" registros...",LogWriter.INFO);
            for(int i=0; i<asignados.size(); i++){
                model.regAsigService.guardarRegistroAsignacion((RegistroAsignacion)asignados.elementAt(i));
            }
            
            logWriter.log("Guardando los no asignados...",LogWriter.INFO);
            logWriter.log("Guardando los no asignados...",LogWriter.INFO);
            model.regAsigService.insertarRegistrosNoAsignados();
            
            logWriter.log("Ejecutando proceso de diogenes...",LogWriter.INFO);
            logWriter.log("Ejecutando proceso de diogenes...",LogWriter.INFO);
            model.ipredoSvc.LlenarAgasoc();
            logWriter.log("Listo...!",LogWriter.INFO);
            
            model.LogProcesosSvc.finallyProceso("Asignacion", this.hashCode(), this.usu, "PROCESO EXITOSO");
        }
        
        catch ( Exception e ) {
            
            try{
                model.LogProcesosSvc.finallyProceso("Asignacion", this.hashCode(),this.usu,"ERROR :" + e.getMessage());
                logWriter.log("Error: "+e,LogWriter.INFO);
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso("Asignacion",this.hashCode(),this.usu,"ERROR :");
                    logWriter.log("Error: "+f,LogWriter.INFO);
                }catch(Exception p){    }
            }
            logWriter.log("Error: "+e.getMessage(),LogWriter.INFO);
            e.printStackTrace();
            
        }
        
    }
    
    private boolean requierenCabezotesYTrailers(String requeridos[][]){
        for(int i=0; i<requeridos.length; i++ ){
            if ( requeridos[i][1] != null && requeridos[i][1].equalsIgnoreCase("C") ){
                for(i=0; i<requeridos.length; i++ ){
                    if ( requeridos[i][1] != null && requeridos[i][1].equalsIgnoreCase("T") ){
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    private void asignar( Recursosdisp cabezote, Recursosdisp trailer, ReqCliente requerimiento ) throws
    SQLException {
        model.rdSvc.realizarAsignacion( cabezote, trailer, requerimiento, "A" );
    }
    
    private Recursosdisp buscarRecurso( String recurso, Vector lista, ReqCliente req ) {
        try{
            logWriter.log("RECURSO  A BUSCAR: "+recurso,LogWriter.INFO);
            
            for ( int i = 0; i < lista.size(); i++ ) {
                String tipo ="";
                Recursosdisp rec = ( Recursosdisp ) lista.elementAt( i );
                logWriter.log("Placa: "+rec.getPlaca(),LogWriter.INFO);
                if(recurso.equals("CSEN")){
                    logWriter.log("EL RECURSO ES CSEN",LogWriter.INFO);
                    tipo = "S";
                    String recursos = model.rdSvc.buscarRecursosxTipo(tipo);
                    logWriter.log("Recursos necesitados "+recursos,LogWriter.INFO);
                    logWriter.log("Recurso disponible "+rec.getTipo(),LogWriter.INFO);
                    String vecRec [] = recursos.split(";");
                    for(int j = 0; j<vecRec.length;j++){
                        if(rec.getTipo().equals(vecRec[j])){
                            logWriter.log("CUMPLIO LAS CONDICIONES...",LogWriter.INFO);
                            return rec;
                        }
                    }
                    
                }else if(recurso.equals("TRAC")){
                    logWriter.log("EL RECURSO ES TRAC",LogWriter.INFO);
                    tipo = "T";
                    String recursos = model.rdSvc.buscarRecursosxTipo(tipo);
                    logWriter.log("Recursos necesitados "+recursos,LogWriter.INFO);
                    logWriter.log("Recurso disponible "+rec.getTipo(),LogWriter.INFO);
                    String vecRec [] = recursos.split(";");
                    for(int j = 0; j<vecRec.length;j++){
                        if(rec.getTipo().equals(vecRec[j])){
                            logWriter.log("CUMPLIO LAS CONDICIONES...",LogWriter.INFO);
                            return rec;
                        }
                    }
                }
                else{
                    if(rec.getTipo().equals(recurso)){
                        logWriter.log("CUMPLIO LAS CONDICIONES...",LogWriter.INFO);
                        return rec;
                    }
                }
            }
        }
        catch ( Exception ex ) {
            ex.printStackTrace();
        }
        return null;
    }
    
    public boolean recursoPuedeViajarEnRuta( Recursosdisp recurso, String origen,
    String destino ) throws
    SQLException {
        return true;
        //return model.rdSvc.puedeViajarEnRuta( recurso.getPlaca(), origen, destino );
    }
    
    public boolean puedeTransportarCliente( Recursosdisp recurso, String cliente ) throws
    SQLException {
        return true;
        //return model.rdSvc.puedeTransportarCliente( recurso.getPlaca(), cliente );
    }
    
    /**
     * verificarQueRequerimientoEntraEnRangoDeRecurso
     *
     * @param req ReqCliente
     * @param rec Recursosdisp
     * @return boolean
     */
    private boolean requerimientoEntraEnRangoDeRecurso( ReqCliente req, Recursosdisp rec,
    int KI, int KF ) {
        Calendar fechaDisponibilidad = Util.crearCalendar( rec.getaFecha_disp() );
        Calendar inicioRango = ( Calendar ) fechaDisponibilidad.clone();
        int KIEnMilisegundos = KI *= 60 * 60 * 1000;
        inicioRango.setTimeInMillis( fechaDisponibilidad.getTimeInMillis() -
        KIEnMilisegundos );
        Calendar finRango = ( Calendar ) fechaDisponibilidad.clone();
        finRango.add( Calendar.HOUR, KF );
        fechaDisponibilidad = Util.crearCalendar( req.getfecha_dispo() );
        // guardar esta fecha en el recurso nos ahorrar� realizar este calculo de nuevo
        // cuando vayamos a asignar el recurso -> com.slt.RecursosdispDAO.asignarRecursoARequerimiento
        rec.setFechaInicio( inicioRango );
        logWriter.log("verificando si "+mostrarCalendar(fechaDisponibilidad)+" est� entre "+mostrarCalendar(inicioRango)+" y "+mostrarCalendar(finRango),LogWriter.INFO);
        return fechaDisponibilidad.after( inicioRango ) &&
        fechaDisponibilidad.before( finRango );
    }
    
    private String mostrarCalendar(Calendar c){
        StringBuffer sb = new StringBuffer();
        sb.append(c.get(c.DATE));
        sb.append("/");
        sb.append(c.get(c.MONTH));
        sb.append("/");
        sb.append(c.get(c.YEAR));
        sb.append(" ");
        sb.append(c.get(c.HOUR));
        sb.append(":");
        sb.append(c.get(c.MINUTE));
        return sb.toString();
    }
    
    public static void main(String a [])throws Exception{
        CruceAYSThread cays = new CruceAYSThread();
        try{
            cays.star( "KREALES");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
}


