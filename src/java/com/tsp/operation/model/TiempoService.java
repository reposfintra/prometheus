/*
 * TiempoService.java
 *
 * Created on 26 de enero de 2005, 04:29 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  KREALES
 */
public class TiempoService {
    
    TiempoDAO tiempo;
    
    /** Creates a new instance of TiempoService */
    public TiempoService() {
        tiempo = new TiempoDAO();
    }
    public void setTiempo(Tiempo t){
        tiempo.setTiempo(t);
    }
    public Tiempo getTiempo(){
        return tiempo.getTiempo();
    }
    
    public void insert()throws SQLException{
        tiempo.insert();
    }
    
    public void update()throws SQLException{
        tiempo.update();
    }
    public void anular()throws SQLException{
        tiempo.anular();
    }
    public void buscar(String distrito, String sj, String cf, String tiempo1, String tiempo2)throws SQLException{
        tiempo.buscar(distrito, sj, cf, tiempo1, tiempo2);
    }
    
    public boolean estaStandard(String sj, String cf)throws SQLException{
        return tiempo.estaStandard(sj, cf);
    }
    
    public boolean estaFechas(String sj, String codigo)throws SQLException{
        return tiempo.searchFechas(sj,codigo);
    }
    
    public void listar()throws SQLException{
        tiempo.listar();
    }
    public Vector getFechas(){
        return tiempo.getFechas();
    }
}
