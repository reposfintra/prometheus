/*
 * PlanillaAuxDAO.java
 *
 * Created on 22 de noviembre de 2004, 11:43 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.MainDAO;
/**
 *
 * @author  KREALES
 */
public class PlaauxDAO extends MainDAO {
    
    private Plaaux plaAux;
    /** Creates a new instance of PlanillaAuxDAO */
    public PlaauxDAO() {
        super("PlanillaAuxDAO.xml");
    }
    public void setPlaAux(Plaaux plaAux){
        this.plaAux = plaAux;
    }
    
    String  insertPlaAux(String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql ="";
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("insert into plaaux (dstrct,pla,full_weight,creation_date,creation_user,acpm_code,full_weight_m,empty_weight_m,base) values(?,?,?,'now()',?,?,?,?,?)");
                st.setString(1,plaAux.getDstrct());
                st.setString(2,plaAux.getPlanilla());
                st.setFloat(3,plaAux.getFull_weight());
                st.setString(4,plaAux.getCreation_user());
                st.setString(5,plaAux.getAcpm_code());
                st.setFloat(6, plaAux.getFull_weight_m());
                st.setFloat(7, plaAux.getEmpty_weight_m());
                st.setString(8,base);
                sql = st.toString();
                //st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE LAS PLANILLA AUXILIAR " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
    }
    public String updatePlaAux()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql = "";
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update plaaux set empty_weight=?, acpm_gln=?, acpm_supplier=?, ticket_a=?, ticket_b=?, ticket_c=?, ticket_supplier=?, user_update=?, full_weight=?, last_update='now()',acpm_code=?, tiket_code=? where pla=? ");
                st.setFloat(1,plaAux.getEmpty_weight());
                st.setFloat(2,plaAux.getAcpm_gln());
                st.setString(3,plaAux.getAcpm_supplier());
                st.setFloat(4,plaAux.getTicket_a());
                st.setFloat(5,plaAux.getTicket_b());
                st.setFloat(6,plaAux.getTicket_c());
                st.setString(7,plaAux.getTicket_supplier());
                st.setString(8,plaAux.getCreation_user());
                st.setFloat(9,plaAux.getFull_weight());
                st.setString(10,plaAux.getAcpm_code());
                st.setString(11,plaAux.getTiket_code());
                st.setString(12,plaAux.getPlanilla());
                
                sql = st.toString();
                //st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA PLANILLA AUXILIAR" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
    }
     /**
     *Metodo que retorna un comando sql para la anulacion de la planilla auxiliar
     *@autor: Karen Reales
     *@return: String con comando SQL
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String anularPlaaux()throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql="";
        try {
            st = this.crearPreparedStatement("SQL_ANULARPLAAUX");
            st.setString(1,plaAux.getPlanilla());
            sql  = st.toString();
            // st.executeUpdate();
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE LAS PLANILLAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_ANULARPLAAUX");
        }
        return sql;
    }
    
}
