/*
 * PlanViajeService.java
 *
 * Created on 22 de noviembre de 2004, 03:47 PM
 *
 */
package com.tsp.operation.model;

import java.sql.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
/**
 *
 * @author  AMENDEZ
 */
public class HReporteService {
    
    private HojaReporteDAO hReporteDataAccess;
    /** Creates a new instance of PlanViajeService */
    public HReporteService() {
        hReporteDataAccess = new HojaReporteDAO();
    }
    
    public boolean hReporteExist(String distrito, String planilla) throws SQLException{
        return hReporteDataAccess.hReporteExist(distrito, planilla);
    }
    
    public void getHrInfoNew(String ditrito, String planilla) throws SQLException{
        hReporteDataAccess.getHrInfoNew(ditrito, planilla);
    }
    
    public void getHrInfoOld(String ditrito, String planilla) throws SQLException{
        hReporteDataAccess.getHrInfoOld(ditrito, planilla);
    }
    
    public void createHjReporte(ReporteConductor rConductor) throws SQLException{
        hReporteDataAccess.createHjReporte(rConductor);
    }
    
    public void updateHjReporte(ReporteConductor rConductor) throws SQLException{
        hReporteDataAccess.updateHjReporte(rConductor);
    }
    
    public void generarReporteHojaConductor(ReporteConductor rConductor) throws SQLException{
        hReporteDataAccess.generarReporteHojaConductor(rConductor);
    }
    
    public ReporteConductor getRConductor(){
        return hReporteDataAccess.getRConductor();
    }
    
    public ReporteHojaConductor getRHConductor(){
        return hReporteDataAccess.getRHConductor();
    }
    
    public void imprimirHojaReporte(String ditrito, String planilla, String user) throws SQLException{
        hReporteDataAccess.imprimirHojaReporte(ditrito, planilla, user);
    }
    
    public void getHrManualInfo(String distrito, String planilla) throws SQLException {
        hReporteDataAccess.getHrManualInfo(distrito, planilla);
    }
}
