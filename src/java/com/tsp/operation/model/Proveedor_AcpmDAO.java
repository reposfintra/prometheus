package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

public class Proveedor_AcpmDAO{
    
    Proveedor_AcpmDAO(){
        
    }
    
    private List proveedorACPM;
    private Proveedor_Acpm pacpm;
    private Vector proveedores;
    private static final String SELECT_CONSULTA ="select  p.dstrct," +
    "        c.nomciu," +
    "        p.nit," +
    "        n.nombre, " +
    "        p.acpm_value," +
    "        p.moneda," +
    "        p.codigo_proveedor, " +
    "        p.max_efectivo, " +
    "        p.tipo_servicio " +
    "from    proveedor_acpm p, " +
    "        ciudad c," +
    "        nit n " +
    "where   c.codciu=p.city_code and" +
    "        n.cedula = p.nit     and" +
    "        c.nomciu like ?    and" +
    "        n.nombre like ?    and" +
    "        p.nit    like ?    and" +
    "        p.tipo_servicio like ? and" +
    "        p.reg_status = '' " +
    "order by p.nit";
    
    public List getProveedorACPM(){
        
        return proveedorACPM;
    }
    public Vector getProveedores(){
        
        return proveedores;
    }
    
    public void setProveedor(Proveedor_Acpm pacpm){
        this.pacpm = pacpm;
    }
    public void searchProveedorACPM(String dstrct )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        proveedorACPM = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("select p.*, n.nombre from proveedor_acpm p, nit n where p.nit=n.cedula and p.dstrct = ? and p.reg_status=''  ");
                st.setString(1,dstrct);
                
                rs = st.executeQuery();
                proveedorACPM = new LinkedList();
                
                while(rs.next()){
                    proveedorACPM.add(Proveedor_Acpm.load(rs));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PROOVEDOR" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    public void consultar(String ciudad, String nombre, String nit, String tipo_servicio )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        proveedores= null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(SELECT_CONSULTA);
                st.setString(1,ciudad+"%");
                st.setString(2,nombre+"%");
                st.setString(3,nit+"%");
                st.setString(4,tipo_servicio+"%");
                
                rs = st.executeQuery();
                proveedores = new Vector();
                
                while(rs.next()){
                    pacpm = new Proveedor_Acpm();
                    pacpm.setCity_code(rs.getString("nomciu"));
                    pacpm.setCodigo(rs.getString("codigo_proveedor"));
                    pacpm.setDstrct(rs.getString("dstrct"));
                    pacpm.setMax_e(rs.getFloat("max_efectivo"));
                    pacpm.setMoneda(rs.getString("moneda"));
                    pacpm.setNit(rs.getString("nit"));
                    pacpm.setNombre(rs.getString("nombre"));
                    pacpm.setTipo(rs.getString("tipo_servicio"));
                    pacpm.setValor(rs.getFloat("acpm_value"));
                    proveedores.add(pacpm);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PROOVEDOR" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    boolean existProveedorAcpm(String dstrct )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select  *  from proveedor_acpm where dstrct = ? and reg_status='' ");
                st.setString(1,dstrct);
                
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PROVEEDOR" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
        
    }
    
    public boolean existProveedor(String nit )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select  *  from proveedor_acpm where nit = ? ");
                st.setString(1,nit);
                
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL NIT DEL PROVEEDOR" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
        
    }
    
    public boolean existSucursal(String nit, String codigo )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from proveedor_acpm where nit = ? and codigo_proveedor = ?");
                st.setString(1,nit);
                st.setString(2,codigo);
                
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL NIT DEL PROVEEDOR" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
        
    }
    
    
    public void searchProveedor(String nit, String codigo )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        pacpm=null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select p.*, n.nombre from proveedor_acpm p, nit n where p.nit=n.cedula and p.nit = ? and codigo_proveedor=? and p.reg_status=''");
                st.setString(1, nit);
                st.setString(2, codigo);
                rs = st.executeQuery();
                if(rs.next()){
                    pacpm = Proveedor_Acpm.load(rs);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL USUARIO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public String searchCodigo(String nit )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        pacpm=null;
        int codigo=0;
        String codigo_p="";
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select max(codigo_proveedor)as codigo from proveedor_acpm where nit=? ");
                st.setString(1, nit);
                rs = st.executeQuery();
                if(rs.next()){
                    if(rs.getString("codigo")!=null){
                        if(!rs.getString("codigo").equals("")){
                            codigo=Integer.parseInt(rs.getString("codigo").substring(9));
                        }
                    }
                    codigo=codigo + 1;
                    codigo_p="Sucursal "+codigo;
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL CODIGO DEL PROVEEDOR " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return codigo_p;
    }
    
    public Proveedor_Acpm getProveedor() throws SQLException{
        return pacpm;
    }
    
    public void insertProveedor(String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("insert into proveedor_acpm (dstrct,city_code,nit,creation_date,creation_user,acpm_value,moneda,codigo_proveedor,max_efectivo,tipo_servicio,codigo_migracion,porcentaje,base) values(?,?,?,'now()',?,?,?,?,?,?,?,?,?)");
                st.setString(1, pacpm.getDstrct());
                st.setString(2, pacpm.getCity_code());
                st.setString(3, pacpm.getNit());
                st.setString(4, pacpm.getCreation_user());
                st.setFloat(5, pacpm.getValor());
                st.setString(6, pacpm.getMoneda());
                st.setString(7, pacpm.getCodigo());
                st.setFloat(8, pacpm.getMax_e());
                st.setString(9,pacpm.getTipo());
                st.setString(10,pacpm.getCod_Migracion());
                st.setFloat(11, pacpm.getPorcentaje());
                st.setString(12,base);
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE LAS PROVEEDOR_ACPM " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    public void updateProveedor()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update proveedor_acpm set  city_code=?, user_update=?, acpm_value=?, moneda=?, max_efectivo=? where nit=? and codigo_proveedor=? ");
                st.setString(1,pacpm.getCity_code());
                st.setString(2,pacpm.getCreation_user());
                st.setFloat(3, pacpm.getValor());
                st.setString(4, pacpm.getMoneda());
                st.setFloat(5, pacpm.getMax_e());
                st.setString(6,pacpm.getNit());
                st.setString(7,pacpm.getCodigo());
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DEL PROVEEDOR ACPM" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
     public void anularProveedor()throws SQLException{
         
         Connection con=null;
         PreparedStatement st = null;
         ResultSet rs = null;
         PoolManager poolManager = null;
         
         try {
             poolManager = PoolManager.getInstance();
             con = poolManager.getConnection("fintra");
             if(con!=null){
                 st = con.prepareStatement("update proveedor_acpm set reg_status='A' where nit=? and codigo_proveedor=? ");
                 st.setString(1,pacpm.getNit());
                 st.setString(2,pacpm.getCodigo());
                 st.executeUpdate();
                 
             }
         }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA ANULACION DEL PROVEEDOR ACPM" + e.getMessage() + " " + e.getErrorCode());
         }
         finally{
             if (st != null){
                 try{
                     st.close();
                 }
                 catch(SQLException e){
                     throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                 }
             }
             
             if (con != null){
                 poolManager.freeConnection("fintra", con);
             }
         }
         
     }
     
    
    
    
}




