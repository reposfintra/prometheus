/*
 * RecursosdispService.java
 *
 * Created on 17 de junio de 2005, 12:18 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import com.tsp.operation.model.beans.*;

/**
 *
 * @author  Jm
 */
public class RecursosServices {
    RecursosDAO rdDAO;

    /** Creates a new instance of RecursosdispService */
    public RecursosServices() {
        rdDAO = new RecursosDAO();
    }

     public Recursosdisp getRdisp() {
        return rdDAO.getRd();
    }

    /**
     * Setter for property rd.
     * @param rd New value of property rd.
     */
    public void setRdisp(Recursosdisp rd) {
        rdDAO.setRd(rd);
    }

    /**
     * Getter for property recursosd.
     * @return Value of property recursosd.
     */
    public java.util.Vector getRecursosdisp() {
        return rdDAO.getRecursosd();
    }

    /**
     * Setter for property recursosd.
     * @param recursosd New value of property recursosd.
     */
    public void setRecursosdisp(java.util.Vector recursosd) {
        rdDAO.setRecursosd(recursosd);
    }
     public void list()throws SQLException{
        rdDAO.list();
    }

    public Vector obtenerRecursosNoAsignados(String origen) throws SQLException {
        return rdDAO.obtenerRecursosNoAsignados(origen);
    }

    public boolean puedeViajarEnRuta(String placa, String origen, String destino) throws
        SQLException {
        return rdDAO.puedeViajarEnRuta(placa,origen,destino);
    }

    public boolean puedeTransportarCliente(String placa, String cliente) throws
        SQLException {
        return rdDAO.puedeTransportarCliente(placa,cliente);
    }

    public void realizarAsignacion( Recursosdisp cabezote, Recursosdisp trailer,
                                    ReqCliente requerimiento ) throws SQLException {
        rdDAO.realizarAsigancion(cabezote,trailer,requerimiento);
    }
}
