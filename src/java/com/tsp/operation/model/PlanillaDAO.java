package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.*;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.threads.LogErrores;


public class PlanillaDAO extends MainDAO {
    
    private Planilla pl;
    private Proveedores prov;
    private Informe inf;
    private Planilla pla=null;
    private Plaaux plaaux;
    
    private Vector plas;
    private Vector informes;
    private Vector clientes;
    private Vector proveedores;
    private Vector rutas;
    private Vector reporte;
    private Vector planillas2;
    private Vector plaIncompletas;
    
    private List remesas;
    private List planillas;
    
    private float saldo;
    private int registros;
    private int total = 0;
    
    private Vector plasindoc;
    
    //Juan 10-03-2006
    private Vector planillas3;
    private LinkedList auxiliar;
    
        private static final String SQL_CAMPOS_TRAILER = "SELECT "+
                                                    "p.numpla, "+
                                                    "p.platlr, "+
                                                    "p.plaveh, "+
                                                    "r.cliente, "+
                                                    "r.std_job_no, "+
                                                    "p.nitpro, "+
                                                    "p.vlrpla, "+
                                                    "p.fecpla "+
                                                    "FROM "+
                                                    "planilla p "+
                                                    "INNER JOIN plarem pr ON ( pr.numpla = p.numpla ) "+
                                                    "INNER JOIN remesa r ON ( r.numrem = pr.numrem ) "+
                                                    "WHERE "+
                                                    "p.platlr = ? AND "+
                                                    "p.numpla = ? AND "+
                                                    "p.reg_status != 'A' AND " +
                                                    "( p.base = 'COL' OR p.base = '' ) AND "+
                                                    "p.tipoviaje != 'VAC' AND "+
                                                    "substring( cf_code , 7, 1) != 'V' ";

    private static String SQL_INSERT="insert into planilla (numpla,fecpla,agcpla,oripla,despla,plaveh,cedcon, nitpro,platlr,fecdsp,cia,tipoviaje,despachador,unidcam,orden_carga,pesoreal,vlrpla,ruta_pla,precinto,unit_vlr ,currency,creation_date,base,printer_date,unit_cost,contenedores,tipocont,tipotrailer, proveedor, cf_code,vlrpla2,tiene_doc,cmc) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,'now()',?,'0099-01-01 00:00:00',?,?,?,?,?,?,?,?,?)";
    
    //Febrero 15 2007
    private static final String SQL_PLANILLA_REMESA_DISCREPANCIA = "SELECT DISTINCT "+
    " d.numpla, "+
    " d.numrem, "+
    " get_nombreciudad(r.orirem) as orirem, "+
    " get_nombreciudad( r.desrem ) as desrem, "+
    " r.fecrem, "+
    " get_nombrecliente(r.cliente) AS nomcli, "+
    " d.tipo_doc, "+
    " d.documento, "+
    " d.num_discre "+
    "FROM "+
    " remesa as r, "+
    " discrepancia as d "+
    "WHERE "+
    " d.numpla = ? AND "+
    " d.numrem = ? AND "+
    " d.tipo_doc = ? AND "+
    " d.documento = ? AND "+
    " d.num_discre = ? AND "+
    " d.numrem = r.numrem AND "+
    " d.rec_status != 'A'";
    
    private static String SQL_CAMBIOMONEDA_FECHA ="SELECT case when vlr_conver>0 then (1/vlr_conver) else vlr_conver end as valor" +
    "               FROM   TASA" +
    "               WHERE  FECHA ='NOW()'" +
    "                      AND MONEDA1 = 'PES'" +
    "                      AND MONEDA2 = ?";
    
    private static String SQL_CAMBIOMONEDA_NOFECHA = "SELECT case when vlr_conver>0  then (1/vlr_conver) else vlr_conver end as valor" +
    "               FROM   TASA" +
    "               WHERE   MONEDA1 = 'PES'" +
    "                      AND MONEDA2 = ?" +
    "                       order by fecha DESC";
    private static String SQL_LISTAR_REMESAS = "SELECT 	"+
    "	rem.*, "+
    "	r.numpla, "+
    "	cli.nomcli, "+
    "	cli.codcli,  "+
    "	r.porcent, "+
    "	CASE WHEN table_code != '' THEN table_code ELSE s.unidad END AS unidad "+
    "FROM 	"+
    "	plarem r,  "+
    "	remesa rem,  "+
    "	cliente cli, "+
    "	stdjob s "+
    "       LEFT JOIN tablagen t ON ( table_type = 'EQUIUNI' AND referencia = s.unidad ) "+
    "WHERE 	"+
    "	r.numpla = ? "+
    "	AND rem.numrem = r.numrem  "+
    "	AND rem.reg_status!='A'  "+
    "	AND r.reg_status!='A'    "+
    "	AND rem.cliente = cli.codcli "+
    "	AND dstrct_code = r.cia "+
    "	AND rem.std_job_no = s.std_job_no";
    
    private static String RecursosDisponibles = "   SELECT  p.cia, p.numpla, p.fecpla, p.fecdsp, p.oripla, p.despla, p.plaveh, p.platlr, p.fechaposllegada, p.reg_status, p.ult_repo, p.agasoc AS agencia, p.tipo_reporte         "+
    "       FROM                                                                                                                                                                      "+
    "   ( SELECT a.*, c.dato AS ult_repo, b.fecha_ult_reporte AS fec_ult_rep, d.agasoc, b.tipo_reporte FROM                                                                           "+
    "   planilla a                                                                                                                                                                    "+
    "   LEFT JOIN ciudad   d ON ( d.codciu   = a.oripla )                                                                                                                             "+
    "   LEFT JOIN trafico  b ON ( b.planilla = a.numpla )                                                                                                                             "+
    "   LEFT JOIN tablagen c ON ( c.table_code  = b.tipo_reporte AND c.table_type = 'REP' )                                                                                           "+
    "   WHERE (                                                                                                                                                                       "+
    "           ( a.fecdsp > ? AND a.FECDSP <= ? )                                                                                                                                    "+
    "       OR                                                                                                                                                                        "+
    "           ( a.cia = ? AND a.LAST_UPDATE > ? AND a.LAST_UPDATE <= ? )                                                                                                            "+
    "          )                                                                                                                                                                      "+
    "   AND UPPER(a.base) IN ('','COL') ) p                                                                                                                                           "+
    "   WHERE (                                                                                                                                                                       "+
    "   ( p.fec_ult_rep BETWEEN ( now() - CAST('3 DAY' AS INTERVAL) )  AND now()  AND  p.ult_repo = 'E' )                                                                             "+
    "       OR                                                                                                                                                                        "+
    "   ( p.fec_ult_rep BETWEEN ( now() - CAST('3 DAY' AS INTERVAL) )  AND now()  AND  p.ult_repo <> 'E' AND tipo_reporte <> '' )                                                     "+
    "   OR                                                                                                                                                                            "+
    "   (  p.fechaposllegada > ( now() - CAST('2 DAY' AS INTERVAL)  )  AND  p.ult_repo <> 'E' AND tipo_reporte <> '' )                                                                "+
    "        )                                                                                                                                                                        "+
    "   ORDER BY p.fecpla                                                                                                                                                             ";
    
    
     private static String INSERT_TRAFICO =
    "INSERT INTO trafico	" +
    "   (dstrct,  planilla," +
    "  placa," +
    "  cedcon," +
    "  cedprop," +
    "  origen," +
    "  destino," +
    "  zona," +
    "  fecha_despacho," +
    "  creation_user," +
    "  base," +
    " pto_control_proxreporte," +
    "fecha_prox_reporte," +
    "   cel_cond," +
    "cliente," +
    " fecha_salida,eintermedias      ) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
    
    
    private static final String SQL_PROD_FLOTA_PLACA_STDXLS = "select distinct sum(planilla.pesoreal), case when (to_char (PLANILLA.feccum,'HH24:mi')>'06:59' and to_char (PLANILLA.feccum,'HH24:mi')<='23:59')then to_char (PLANILLA.feccum,'DD') "+
    "when to_char(PLANILLA.feccum,'HH24:mi')>='00:00' and to_char (PLANILLA.feccum,'HH24:mi')<'07:00' then to_char (PLANILLA.feccum-'1d'::interval,'DD') end as dia, "+
    "count(PLANILLA.numpla) "+
    "from (SELECT * FROM PLANILLA WHERE FECCUM BETWEEN ? AND ? AND BASE in ( 'pco','spo') and reg_status <>'A') AS PLANILLA "+
    "LEFT JOIN PLAREM ON (PLANILLA.NUMPLA = PLAREM.NUMPLA) "+
    "LEFT JOIN REMESA ON (REMESA.NUMREM = PLAREM.NUMPLA) "+
    "LEFT OUTER JOIN NIT ON (PLANILLA.NITPRO = NIT.CEDULA) "+
    "where plaveh = ? AND NITPRO = ? AND std_job_no = ?"+
    "group by dia "+
    "order by dia";
    
    private static String SQL_NOMBRE_CLIS ="select distinct " +
    "       cliente," +
    "       nomcli" +
    " from (select numrem from plarem where numpla = ?)r" +
    " INNER JOIN REMESA ON (r.numrem = remesa.numrem AND REMESA.REG_STATUS='')" +
    " INNER JOIN CLIENTE ON (remesa.cliente = cliente.codcli)";
    //Karen 15-03-20056
    private static String   SQL_INFORMEDESPACHO="SELECT substr(rem.remision,3,7)as remision," +
    "      pla.plaveh, " +
    "      pla.cedcon, " +
    "      Upper(n.nombre) as nombre,   " +
    "      pla.pesoreal, " +
    "      pla.feccum,  " +
    "      pla.fecdsp, " +
    "      rem.descripcion, " +
    "      pla.numpla as planilla, " +
    "      rem.std_job_no," +
    "       Upper(c.nombre) as cnombre " +
    " FROM   PLANILLA pla LEFT OUTER JOIN Nit n ON (n.cedula = pla.cedcon )" +
    "       LEFT OUTER JOIN Nit c ON (c.cedula = pla.nitpro ) ," +
    "       PLAREM pr, " +
    "       REMESA rem " +
    " WHERE  pla.feccum BETWEEN ? AND ?" +
    "       and rem.std_job_no = ?" +
    "       and rem.cliente = ? " +
    "       and pr.numrem = rem.numrem " +
    "       and pr.numpla = pla.numpla " +
    "       and pla.reg_status=''" +
    "       and pla.base = ?" +
    "  order by pr.feccum";
    
    //Diogenes 08-02-2006
    private static String SQL_PLANILLAS_SINDOC = "SELECT d.*, " +
    "       e.std_job_desc, " +
    "       f.nomcli" +
    "       FROM " +
    "          (SELECT DISTINCT " +
    "              a.numpla," +
    "              a.fecdsp," +
    "              c.cliente," +
    "              c.std_job_no," +
    "              c.numrem   " +
    "            FROM   " +
    "               planilla a," +
    "               plarem b, " +
    "               remesa c" +
    "            WHERE " +
    "               a.fecdsp between ? AND ? " +
    "           AND a.tiene_doc = 'N'" +
    "           AND upper(a.despachador) = ? " +
    "           AND a.numpla = b.numpla " +
    "           AND a.reg_status != 'A' " +
    "           AND b.numrem = c.numrem " +
    "           AND substr (c.std_job_no ,1,3) NOT IN  ('127') )d" +
    "           LEFT JOIN stdjob e ON ( e.std_job_no = d.std_job_no )" +
    "           LEFT JOIN cliente f ON ( f.codcli = d.cliente ) ";
    
    
    
   /* private static String SQLPlanilla =     "   SELECT CIA, NUMPLA, FECPLA, FECDSP, ORIPLA, DESPLA, PLAVEH, PLATLR, FECHAPOSLLEGADA, REG_STATUS "+
    "   FROM planilla WHERE CIA = ? AND FECDSP > ? AND FECDSP <= ?                                      "+
    "   OR LAST_UPDATE > ? AND LAST_UPDATE <= ?        AND length(NUMPLA) < 7                           "+
    "   ORDER BY FECPLA                                                                                 ";*/
    
    
    private static String SQLPlanilla =    "   SELECT P.CIA, P.NUMPLA, P.FECPLA, P.FECDSP, P.ORIPLA, P.DESPLA, P.PLAVEH, P.PLATLR, P.FECHAPOSLLEGADA, P.REG_STATUS, "+
    "   R.STD_JOB_NO                                                                                                         "+
    "   FROM (select p.* from planilla p where P.CIA = ? AND P.FECDSP > ? AND P.FECDSP <= ?                                  "+
    "   OR P.LAST_UPDATE > ? AND P.LAST_UPDATE <= ?        AND length(P.NUMPLA) < 7  ) p                                     "+
    "   inner join plarem c on (c.numpla = p.numpla)                                                                         "+
    "   inner join remesa r on (r.numrem = c.numrem)                                                                         "+
    "   order by p.fecpla                                                                                                    ";
    
    private static final String SQL_OBTENER_PLANILLA_POR_NUMPLA = "select * from planilla where numpla = ?";
    
    private static final String SELECT_DESCUENTOS_VALOR="select 	tblcon.concept_code, tblcon.concept_desc," +
    "  sum(vlr * tblcon.ind_signo) as vlr, " +
    "	ind_vlr" +
    " from 	tblcon," +
    "	movpla" +
    " where 	tblcon.concept_code = movpla.concept_code " +
    "	and planilla = ?" +
    "	and ind_vlr<>'P'" +
    "	and tblcon.concept_code <> '09'" +
    " group by tblcon.concept_code, " +
    "	tblcon.concept_desc, " +
    "    	ind_vlr ";
    
    private static final String SELECT_DESCUENTOS_PORCENTAJES="select 	tblcon.concept_code, tblcon.concept_desc, " +
    "	vlr * tblcon.ind_signo as vlr," +
    "	ind_vlr" +
    " from 	tblcon," +
    "	movpla" +
    " where 	tblcon.concept_code = movpla.concept_code " +
    "	and planilla = ?" +
    "	and ind_vlr='P'" ;
    
    private static final String SELECT_VALOR_PLANILLA="select vlrpla" +
    " from planilla" +
    " where numpla = ?" ;
    
    private static final String SELECT_AJUSTE_PLANILLA="select vlr" +
    " from MOVPLA" +
    " where PLANILLA = ?" +
    "   AND CONCEPT_CODE  = '09'" ;
    
    private static final String SELECT_BUSCAR_EXTRAFLETES="SELECT vlr, codextraflete.descripcion" +
    " FROM   	MOVPLA," +
    "	codextraflete" +
    " WHERE  	PLANILLA = ?" +
    "	AND cod_descuento=CONCEPT_CODE" ;
    
    //ALEJANDRO
    private static final String SQL_OBTENER_PLANILLAS = "select *,TO_CHAR(printer_date,'YYYYMMDD') as fechaAux from planilla";//BETWEEN 4095 AND 12000
    private static final String SQL_OBTENER_PLANILLAS_ENTRE_FECHAS = "select * from planilla where fecpla between ? and ?";
    
    //Tito Andr�s
    private static final String SQL_INFORMACION_PLANILLA =
    "select  pla.numpla," +
    " plaveh," +
    " fecpla," +
    " pla.cedcon," +
    " coalesce(nit.nombre,'') as nombre," +
    " coalesce (ciu.nomciu,'') as origen," +
    " coalesce (ciu2.nomciu,'') as destino," +
    " coalesce (cli.nomcli,'') as cliente" +
    " from  planilla as pla " +
    " LEFT OUTER JOIN nit  on (pla.cedcon=nit.cedula )" +
    " LEFT OUTER JOIN ciudad as ciu on (ciu.codciu=pla.oripla)" +
    " LEFT OUTER JOIN ciudad as ciu2 on (ciu2.codciu=pla.despla)," +
    " plarem, " +
    " remesa as rem," +
    " cliente cli" +
    " where  pla.numpla=?" +
    " and pla.numpla=plarem.numpla " +
    " and plarem.numrem=rem.numrem" +
    " and rem.cliente=cli.codcli";
    //Tito Andr�s 27.09.2005
    private final String SQL_GET_NITPROPIETARIO =
    " select coalesce(nit.nombre,'') as propietario" +
    " from planilla as pla" +
    " left outer join nit on (pla.nitpro=nit.cedula)" +
    " where pla.numpla = ?";
    private static final String SQL_TRAMO_PLANILLA =
    "select via.via from planilla as pla, via " +
    " where pla.ruta_pla=via.origen||via.destino||via.secuencia " +
    "and numpla=?";
    private final String SQL_OBTENER_NOMBRE_CIUDAD = "select nomciu from ciudad where codciu = ?";
    //Jose
    private static final String SQL_PLACA_CONDUCTOR = "SELECT P.REG_STATUS AS ESTADO_DESPACHO, P.NUMPLA, R.REG_STATUS AS ESTADO_REMESA, R.NUMREM, P.FECPLA, N.NOMBRE, C.NOMCIU AS AGCPLA, C1.NOMCIU AS ORIPLA, C2.NOMCIU AS DESPLA , CL.NOMCLI AS CLIENTE, C3.NOMCIU AS ORIREM, C4.NOMCIU AS DESREM "+
    "FROM PLANILLA P, REMESA R, PLAREM PR, NIT N, CIUDAD C, CIUDAD C1 , CIUDAD C2, CIUDAD C3, CIUDAD C4, CLIENTE CL, PLACA PL "+
    "WHERE PL.PLACA = P.PLAVEH AND "+
    "P.NUMPLA = PR.NUMPLA AND "+
    "PR.NUMREM = R.NUMREM AND "+
    "P.CEDCON = N.CEDULA AND "+
    "P.AGCPLA = C.CODCIU AND "+
    "P.ORIPLA = C1.CODCIU AND "+
    "P.DESPLA = C2.CODCIU AND "+
    "R.ORIREM = C3.CODCIU AND "+
    "R.DESREM = C4.CODCIU AND "+
    "R.CLIENTE = CL.CODCLI AND "+
    "PL.PLACA = ? AND "+
    "P.FECPLA BETWEEN ? AND ? AND "+
    "P.ORIPLA LIKE ? AND "+
    "P.DESPLA LIKE ? AND "+
    "P.AGCPLA LIKE ? AND "+
    "P.CEDCON LIKE ? ";
    
    private static final String SQL_PLANILLA_USUARIO =
    "SELECT P.REG_STATUS AS ESTADO_DESPACHO, P.NUMPLA, R.REG_STATUS AS ESTADO_REMESA, R.NUMREM, P.FECPLA, N.NOMBRE, C.NOMCIU AS AGCPLA, C1.NOMCIU AS ORIPLA, C2.NOMCIU AS DESPLA , CL.NOMCLI AS CLIENTE, C3.NOMCIU AS ORIREM, C4.NOMCIU AS DESREM "+
    "FROM PLANILLA P, REMESA R, PLAREM PR, NIT N, CIUDAD C, CIUDAD C1 , CIUDAD C2, CIUDAD C3, CIUDAD C4, CLIENTE CL, PLACA PL "+
    "WHERE PL.PLACA = P.PLAVEH AND "+
    "P.NUMPLA = PR.NUMPLA AND "+
    "PR.NUMREM = R.NUMREM AND "+
    "P.CEDCON = N.CEDULA AND "+
    "P.AGCPLA = C.CODCIU AND "+
    "P.ORIPLA = C1.CODCIU AND "+
    "P.DESPLA = C2.CODCIU AND "+
    "R.ORIREM = C3.CODCIU AND "+
    "R.DESREM = C4.CODCIU AND "+
    "R.CLIENTE = CL.CODCLI AND "+
    "P.DESPACHADOR = ? AND "+
    "PL.PLACA LIKE ? AND "+
    "P.FECPLA BETWEEN ? AND ? AND "+
    "P.ORIPLA LIKE ? AND "+
    "P.DESPLA LIKE ? AND "+
    "P.AGCPLA LIKE ? AND "+
    "P.CEDCON LIKE ? ";
    
    ///jose 111005
    private static final String SQL_PLANILLA_XLS = "SELECT DISTINCT P.NUMPLA, P.FECPLA , C.NOMCIU AS AGENCIA, P.PLAVEH AS PLACA, N.NOMBRE AS CONDUCTOR, C1.NOMCIU AS ORIPLA, C2.NOMCIU AS DESPLA ,RMT.CREATION_DATE - P.FECDSP as DIFERENCIA "+
    "FROM INGRESO_TRAFICO AS IT, REP_MOV_TRAFICO AS RMT, PLANILLA AS P, CIUDAD AS C ,CIUDAD AS C1, CIUDAD AS C2, NIT AS N "+
    "WHERE IT.PLANILLA != RMT.NUMPLA AND "+
    "P.NUMPLA = IT.PLANILLA AND "+
    "P.ORIPLA = C1.CODCIU AND "+
    "P.DESPLA = C2.CODCIU AND "+
    "P.CEDCON = N.CEDULA AND "+
    "P.AGCPLA = C.CODCIU "+
    "ORDER BY P.NUMPLA";
    //jose 03-02-2006
    private static final String SQL_PLANILLA_REMESA = "select distinct p.numpla, rp.numrem, c1.nomciu as orirem, c2.nomciu as desrem, r.fecrem, c.nomcli, c.codcli "+
    "from planilla as p, plarem as rp, remesa as r, cliente as c, ciudad as c1, ciudad as c2 "+
    "where p.numpla = ? and "+
    "rp.numpla = p.numpla and "+
    "rp.numrem = r.numrem and "+
    "r.cliente = c.codcli and "+
    "c1.codciu = r.orirem and "+
    "c2.codciu = r.desrem";
    
    ///
    
    private static final String SQL_INFORME_FACTURACION = //ultima modificacion 02.11.2005
    "select std_job_no, substring(remision from 3) as remision, c1.feccum, c1.nomcond, c1.pesoreal, c1.plaveh " +
    "from " +
    "(select numrem, coalesce(nit.nombre,'') as nomcond, pla.feccum, pesoreal, plaveh " +
    "from planilla as pla " +
    "left outer join nit on (pla.cedcon=nit.cedula ) " +
    "left outer join plarem on (pla.numpla=plarem.numpla) " +
    "where " +
    "pla.feccum between  ? and ? and " +
    "pla.reg_status <> 'A') as c1 left outer join remesa as rem " +
    "on( c1.numrem=rem.numrem) " +
    "where " +
    "std_job_no in ( " +
    "   select sj " +
    "   from stdjobsel " +
    "   where sj like ? )" +
    "order by std_job_no, c1.feccum";
    
    private static final String SQL_INFORME_FACTURACION_127012 =//updated 2.12.2005
    "select std_job_no, substring(remision from 3) as remision, c1.feccum, c1.nomcond, c1.pesoreal, c1.plaveh " +
    "from " +
    "(select numrem, coalesce(nit.nombre,'') as nomcond, pla.feccum, pesoreal, plaveh " +
    "from planilla as pla " +
    "left outer join nit on (pla.cedcon=nit.cedula ) " +
    "left outer join plarem on (pla.numpla=plarem.numpla) " +
    "where " +
    "pla.feccum between  ? and ? and " +
    "pla.reg_status <> 'A') as c1 left outer join remesa as rem " +
    "on( c1.numrem=rem.numrem) " +
    "where " +
    "std_job_no like '127012' " +
    "and base = 'spo' " +
    "order by std_job_no, c1.feccum";
    ///
    //Ricardo 10.01.06
    private static final String SQL_REPORTE_ANTICIPOS =
    "select c2.remision, " +
    "	c2.fecdsp, " +
    "   c2.puerto, " + //ricardo
    "	c2.nitpro, " +
    "	c2.nomprop, " +
    "	c2.plaveh, " +
    "	c2.proveedor,  " +
    "	c2.provanticipo, " +
    "	c2.acpm_gln, " +
    "	c2.vr_acpm, " +
    "	c2.peajes,  " +
    "	c2.efectivo, " +
    "	c2.total  " +
    "from	(select remision, " +
    "		c1.fecdsp, " +
    "           c1.puerto, " + //ricardo
    "		c1.nitpro, " +
    "		c1.nomprop, " +
    "		c1.plaveh, " +
    "		c1.proveedor, " +
    "           c1.provanticipo, " +
    "		c1.acpm_gln, " +
    "		c1.vr_acpm, " +
    "		c1.peajes, " +
    "		c1.efectivo, "  +
    "		vr_acpm  +peajes+ efectivo as total  " +
    "	from  	(select numrem, " +
    "			nitpro, " +
    "			coalesce(nit.nombre,'') as nomprop, " +
    "			pla.fecdsp,  " +
    "			plaveh, " +
    "                   coalesce( tablagen.descripcion, '') as puerto,"+ //RICARDO
    "                   coalesce(movpla2.sucursal, '') as proveedor, " + //RICARDO
    "                   coalesce(movpla3.sucursal, '') as provanticipo, " + //RICARDO
    "			acpm_gln, " +
    "			coalesce(movpla2.vlr, 0.0) as vr_acpm,  " +
    "			coalesce(movpla1.vlr, 0.0) as peajes, " +
    "			coalesce(movpla3.vlr, 0.0) + coalesce(movpla4.vlr, 0.0) as efectivo  " +
    "		 from 	(SELECT * FROM (select * from planilla where (feccum = '0099-01-01' or feccum between ? and ? ) and base in ('spo','pco') and reg_status <> 'A')a WHERE CASE WHEN BASE='pco' then feccum between ? and ?  when BASE='spo' then corte between ? and ? end ) as pla  " +
    "			left outer join nit on ( pla.nitpro=nit.cedula )  " +
    "                   left outer join tablagen on ( pla.base = tablagen.table_code )  "+ //ricardo
    "			left outer join plarem on ( pla.numpla=plarem.numpla )  " +
    "			left outer join movpla as movpla1 on ( pla.numpla=movpla1.planilla and movpla1.concept_code='03' )  " +
    "			left outer join movpla as movpla2  on ( pla.numpla=movpla2.planilla and movpla2.concept_code='02' )  " +
    "			left outer join movpla as movpla3  on ( pla.numpla=movpla3.planilla and movpla3.concept_code='01' )  " +
    "			left outer join movpla as movpla4  on ( pla.numpla=movpla4.planilla and movpla4.concept_code='09' )  " +
    "			left outer join plaaux on( pla.numpla=plaaux.pla )  " +
    "		) as c1  " +
    "	left outer join remesa as rem on( c1.numrem=rem.numrem ) ) as c2  " +
    "	left outer join nit on( c2.proveedor=nit.cedula )  " +
    " order by c2.remision, c2.fecdsp  ";
    
    /*JuanM 30-11-05*/
    public static String SQLDOCUMENTOS  =   "  SELECT DOCUMENTO, DOCUMENTO_REL FROM REMESA_DOCTO WHERE NUMREM = ? ";
    
    /*jose 261105*/
    private static final String SQL_LUFKIN_XLS = "select distinct plaveh, nitpro, nombre, platlr "+
    "FROM    (SELECT * FROM PLANILLA WHERE FECCUM BETWEEN ? AND ? AND BASE in ( 'pco','spo') and reg_status <>'A') AS PLANILLA "+
    "LEFT OUTER JOIN NIT ON (PLANILLA.NITPRO = NIT.CEDULA) "+
    "where platlr like 'T___' "+
    "ORDER BY nombre , plaveh";
    
    private static final String SQL_FLOTA_INTERMEDIA_XLS = "select distinct plaveh, nitpro, nombre, max(platlr) AS platlr  "+
    "FROM (SELECT * FROM PLANILLA WHERE FECCUM BETWEEN ? AND ? AND BASE in ( 'pco','spo') and reg_status <>'A') AS PLANILLA "+
    "LEFT OUTER JOIN NIT ON (PLANILLA.NITPRO = NIT.CEDULA) "+
    "where plaveh||NITPRO not in "+
    "(SELECT PLACA||NITPRO FROM flota_directa) "+
    "GROUP BY plaveh, nitpro, nombre ORDER BY nombre , plaveh";
    
    private static final String SQL_FLOTA_DIRECTA_XLS = "select distinct plaveh, nitpro, nombre, max(platlr) AS platlr "+
    "FROM (SELECT * FROM PLANILLA WHERE FECCUM BETWEEN ? AND ? AND BASE in ( 'pco','spo') and reg_status <>'A') AS PLANILLA "+
    "LEFT OUTER JOIN NIT ON (PLANILLA.NITPRO = NIT.CEDULA) "+
    "where plaveh||NITPRO in "+
    "(SELECT PLACA||NITPRO FROM flota_directa) "+
    "GROUP BY plaveh, nitpro, nombre ORDER BY nombre , plaveh";
    
    
    
    private static final String SQL_PROD_FLOTA_XLS = "select distinct plaveh, nitpro, nombre, max(platlr) AS platlr "+
    "FROM    (SELECT * FROM PLANILLA WHERE FECCUM BETWEEN ? AND ? AND BASE in ( 'pco','spo') and reg_status <>'A') AS PLANILLA "+
    "LEFT OUTER JOIN NIT ON (PLANILLA.NITPRO = NIT.CEDULA) "+
    "GROUP BY plaveh, nitpro, nombre "+
    "ORDER BY nitpro, nombre, plaveh";
    
    private static final String SQL_PROD_FLOTA_STANDAR_XLS = "select distinct plaveh, nitpro, nombre, max(platlr) AS platlr  "+
    "FROM    (SELECT * FROM PLANILLA WHERE FECCUM BETWEEN ? AND ? AND BASE in ( 'pco','spo') and reg_status <>'A') AS PLANILLA "+
    "LEFT JOIN PLAREM ON (PLANILLA.NUMPLA = PLAREM.NUMPLA) "+
    "LEFT JOIN REMESA ON (REMESA.NUMREM = PLAREM.NUMPLA) "+
    "LEFT OUTER JOIN NIT ON (PLANILLA.NITPRO = NIT.CEDULA) " +
    "where std_job_no = ? GROUP BY plaveh, nitpro, nombre ORDER BY nombre , plaveh";
    
    
    private static final String SQL_PROD_FLOTA_PLACA_XLS = "select distinct sum(planilla.pesoreal), case when (to_char (PLANILLA.feccum,'HH24:mi')>'06:59' and to_char (PLANILLA.feccum,'HH24:mi')<='23:59')then to_char (PLANILLA.feccum,'DD') "+
    "when to_char(PLANILLA.feccum,'HH24:mi')>='00:00' and to_char (PLANILLA.feccum,'HH24:mi')<'07:00' then to_char (PLANILLA.feccum-'1d'::interval,'DD') end as dia, "+
    "count(PLANILLA.numpla) "+
    "from (SELECT * FROM PLANILLA WHERE FECCUM BETWEEN ? AND ? AND BASE in ( 'pco','spo') and reg_status <>'A') AS PLANILLA "+
    "LEFT JOIN PLAREM ON (PLANILLA.NUMPLA = PLAREM.NUMPLA) "+
    "LEFT JOIN REMESA ON (REMESA.NUMREM = PLAREM.NUMPLA) "+
    "LEFT OUTER JOIN NIT ON (PLANILLA.NITPRO = NIT.CEDULA) "+
    "where plaveh = ? AND NITPRO = ? "+
    "group by dia "+
    "order by dia";
    
    private static final String SQL_PROD_SJ_XLS = "select distinct sj AS std_job_no, sj_desc AS descripcion "+
    "FROM  	stdjobdetsel ORDER BY descripcion";
    
   /* private static final String SQL_PROD_SJ_XLS = "select distinct std_job_no, descripcion "+
    "FROM  	(SELECT * FROM PLANILLA WHERE FECCUM BETWEEN ? AND ? AND BASE in ( 'pco','spo') and reg_status <>'A') AS PLANILLA "+
    "LEFT JOIN PLAREM ON (PLANILLA.NUMPLA = PLAREM.NUMPLA) "+
    "LEFT JOIN REMESA ON (REMESA.NUMREM = PLAREM.NUMPLA) "+
    "where descripcion <> '' "+
    "ORDER BY descripcion";*/
    
    private static final String SQL_PLACA_TRAYLER_XLS = "select distinct platlr "+
    "from (SELECT * FROM PLANILLA WHERE FECCUM BETWEEN ? AND ? AND BASE in ( 'pco','spo') and reg_status <>'A') AS PLANILLA "+
    "LEFT OUTER JOIN NIT ON (PLANILLA.NITPRO = NIT.CEDULA) "+
    "where plaveh = ? AND NITPRO = ? ";
    /*fin*/
    /*Tito 17.01.2006*/
    private static final String SQL_REPORTE_ANTICIPOS_RESUMEN =
    "SELECT c2.proveedor, " +
    "       SUM(c2.acpm_gln) AS galones, " +
    "       SUM(c2.vr_acpm) AS valor  " +
    "FROM (" +
    "   SELECT  remision, " +
    "           c1.proveedor, " +
    "           c1.acpm_gln, " +
    "           c1.vr_acpm " +
    "   FROM (" +
    "       SELECT  numrem, " +
    "               COALESCE(movpla2.sucursal, '') AS proveedor, " +
    "               acpm_gln, " +
    "               COALESCE(movpla2.vlr, 0.0) as vr_acpm " +
    "       FROM (" +
    "           SELECT * " +
    "           FROM (" +
    "               SELECT * " +
    "               FROM planilla " +
    "               WHERE   (feccum = '0099-01-01' OR feccum BETWEEN ? AND ? ) " +
    "                       AND base IN ('spo','pco') and reg_status <> 'A') a " +
    "           WHERE   CASE WHEN BASE='pco' THEN feccum BETWEEN ? AND ? " +
    "                   WHEN BASE='spo' THEN corte BETWEEN ? AND ? END " +
    "       ) as pla " +
    "       LEFT OUTER JOIN plarem ON ( pla.numpla = plarem.numpla ) " +
    "       LEFT OUTER JOIN movpla AS movpla2 ON ( pla.numpla=movpla2.planilla AND movpla2.concept_code='02' ) " +
    "       LEFT OUTER JOIN plaaux ON ( pla.numpla = plaaux.pla ) " +
    "   ) AS c1         " +
    "   LEFT OUTER JOIN remesa AS rem ON ( c1.numrem = rem.numrem ) " +
    ") AS c2        " +
    "LEFT OUTER JOIN nit ON ( c2.proveedor = nit.cedula ) " +
    "GROUP BY c2.proveedor " +
    "ORDER BY c2.proveedor";
    /*fin*/
    /*13-12-05*/
    //ENTREGA ENTRE HOY  Y HOY - 2 DIAS
    private static String SQLRecursos1 =  "    SELECT 	P.CIA,                  "+
    "    P.NUMPLA,                       "+
    "    P.FECPLA,                       "+
    "    P.FECDSP,                       "+
    "    P.ORIPLA,                       "+
    "    P.DESPLA,                       "+
    "    P.PLAVEH,                       "+
    "    P.PLATLR,                       "+
    "    P.FECHAPOSLLEGADA,              "+
    "    P.REG_STATUS,                   "+
    "    R.STD_JOB_NO,                   "+
    "    t.feinftr                       "+
    "    FROM (select p.*                "+
    "    from   planilla p               "+
    "    where  P.CIA = ?                "+
    "    AND(( P.FECDSP > ? AND P.FECDSP <= ? ) OR (P.LAST_UPDATE > ? AND P.LAST_UPDATE <= ?))   "+
    "      ) p                                                                                   "+
    "    inner join plarem c on (c.numpla = p.numpla)                                            "+
    "    inner join remesa r on (r.numrem = c.numrem)                                                        "+
    "    inner join (select cia, tipomtr, planilla, feinftr, hoinftr from trafimo where tipomtr ='E'         "+
    "    AND feinftr BETWEEN now() - CAST('3 DAY' AS INTERVAL) and now()) t on (t.planilla = p.numpla)       "+
    "    order by p.fecpla                                                                                   ";
    
    //TIENE CUALQUIER MOVIMIENTO ENTRE HOY Y HACE 2 DIAS
    private static String SQLRecursos2 =  "    SELECT 	P.CIA,                  "+
    "     P.NUMPLA,                      "+
    "     P.FECPLA,                      "+
    "     P.FECDSP,                      "+
    "     P.ORIPLA,                      "+
    "     P.DESPLA,                      "+
    "     P.PLAVEH,                      "+
    "     P.PLATLR,                      "+
    "     P.FECHAPOSLLEGADA,             "+
    "     P.REG_STATUS,                  "+
    "     R.STD_JOB_NO                   "+
    "     FROM (select p.*               "+
    "     from   planilla p              "+
    "     where  P.CIA = ?               "+
    "             AND(( P.FECDSP > ? AND P.FECDSP <= ? )  OR (P.LAST_UPDATE > ? AND P.LAST_UPDATE <= ?))             "+
    "             AND NUMPLA  IN (select planilla  from trafimo where  tipomtr <>'E' and tipomtr <>'' and  feinftr BETWEEN now() - CAST('3 DAY' AS INTERVAL) and now() ) "+
    "             AND BASE NOT IN ('spo', 'pco')) p                                                                                                                      "+
    "        inner join plarem c on (c.numpla = p.numpla)                                                                                                                "+
    "        inner join remesa r on (r.numrem = c.numrem)                                                                                                                "+
    "   order by p.fecpla                                                                                                                                                ";
    
    //NO TIENE MOVIMIENTO Y LA FECHA DE POSLLEGADA ES MAYOR A HACE 2 DIAS
    private static String SQLRecursos3 =  "    SELECT 	P.CIA,                      "+
    "     P.NUMPLA,                          "+
    "     P.FECPLA,                          "+
    "     P.FECDSP,                          "+
    "     P.ORIPLA,                          "+
    "     P.DESPLA,                          "+
    "     P.PLAVEH,                          "+
    "     P.PLATLR,                          "+
    "     P.FECHAPOSLLEGADA,                 "+
    "     P.REG_STATUS,                      "+
    "     R.STD_JOB_NO                       "+
    "     FROM (select p.*                   "+
    "        from   planilla p               "+
    "        where  P.CIA = ?                "+
    "             AND(( P.FECDSP > ? AND P.FECDSP <= ? )  OR (P.LAST_UPDATE > ? AND P.LAST_UPDATE <= ?)) "+
    "             AND FECHAPOSLLEGADA > ( now() - CAST('2 DAY' AS INTERVAL)   )                                                                                 "+
    "             AND NUMPLA  not IN (select planilla  from trafimo where  tipomtr <>'E' and tipomtr <>'' and  feinftr BETWEEN now() - CAST('3 DAY' AS INTERVAL) and now() ) "+
    "         ) p                                                                                                                                                            "+
    "        inner join plarem c on (c.numpla = p.numpla)                                                                                                                    "+
    "        inner join remesa r on (r.numrem = c.numrem)                                                                                                                    "+
    "        order by p.fecpla                                                                                                                                               ";
    /*fin*/
    
    //jose 15-12-2005
    private static final String SQL_DESPACHOS_XLS =
    "SELECT DISTINCT "+
    "a.printer_date AS fecha_impresion, a.numpla, cl.nomcli AS cliente, "+
    "a.plaveh, p.conductor, max(a.creation_date) AS fecha_creacion, "+
    "a.despachador AS usuario, u.nomciu AS agencia_usuario, "+
    "co.nomciu, max(b.creation_date) AS fecha_placa, max(c.creation_date) AS fecha_conductor "+
    "FROM "+
    "placa as p, plarem pr, remesa r, cliente cl, planilla a "+
    "left join  doc.tblimagen b on (b.activity_type = '005'  AND b.document_type = '031' AND b.document = a.plaveh) "+
    "left join  doc.tblimagen c on (c.activity_type = '003'  AND c.document_type = '032' AND c.document = a.cedcon) "+
    "left join  (select u.idusuario, u.id_agencia, c.nomciu from usuarios u, ciudad c where c.codciu = u.id_agencia) u on ( u.idusuario = a.despachador) "+
    "left join ciudad co on ( co.codciu = a.oripla) "+
    "WHERE "+
    "a.fecdsp  BETWEEN ? AND  ? AND "+
    "plaveh = p.placa AND a.reg_status!='A' AND a.numpla = pr.numpla AND "+
    "pr.numrem = r.numrem AND cl.codcli = r.cliente AND a.printer_date <> '0099-01-01' "+
    "GROUP BY "+
    "a.printer_date, a.numpla, a.plaveh,  conductor, cl.nomcli, a.fecdsp, "+
    "a.despachador, u.nomciu, co.nomciu";
    
    private static final String SQL_TIPO_VIAJE = "SELECT DISTINCT "+
    "CASE WHEN co.agasoc = cd.agasoc THEN 'NACIONALES' "+
    "ELSE 'INTERNACIONALES' "+
    "END AS tipo_viaje "+
    "FROM "+
    "ciudad c, planilla a "+
    "left join ciudad co on ( co.codciu = a.oripla) "+
    "left join ciudad cd on ( cd.codciu = a.despla) "+
    "WHERE "+
    "numpla = ?";
    
    
    
    
    //jose 15/12/2005
    private static final String SQL_OBTENER_DATOS_PLANILLA = "" +
    "select p.vlrpla, r.std_job_no FROM " +
    "	(select * from planilla where numpla = ?) p" +
    "	inner join plarem on (plarem.numpla = p.numpla)" +
    "	inner join remesa r on (plarem.numrem = r.numrem)" +
    " ORDER BY r.numrem limit 1";
    /*fin*/
    
    //David 23.12.05
    private static String EXIST="SELECT * FROM planilla WHERE numpla =?";
    /*fin*/
    
    private static final String SQL_PLANILLA_GENERAL =
    "SELECT" +
    "      p.reg_status," +
    "      p.cia as distrito," +
    "      p.numpla, p.fecdsp," +
    "      p.plaveh, p.despachador, " +
    "      p.cedcon, pr.numrem, " +
    "      n.nombre,r.descripcion," +
    "      r.remision  " +
    "FROM" +
    "      planilla p, " +
    "      plarem pr, " +
    "      remesa r, " +
    "      nit n " +
    "WHERE " +
    "      p.plaveh  like ?  and " +
    "      p.nitpro like ? and " +
    "      p.cedcon like ? and " +
    "      pr.numpla=p.numpla and " +
    "      p.fecdsp >=? and " +
    "      p.fecdsp <= ? and " +
    "      n.cedula = p.cedcon and " +
    "      pr.numrem = r.numrem and " +
    "      p.base = ?  and" +
    "      p.despachador like ? and " +
    "      p.oripla like ? and " +
    "      p.despla like ? and " +
    "      p.agcpla like ? order by p.numpla "+
    "      and pr.reg_status ! = 'A'         ";
    /*  QRY ORIGINAL KAREN "select p.numpla, p.fecdsp,p.plaveh,p.despachador, p.cedcon, pr.numrem, n.nombre,r.descripcion,r.remision  from planilla p, plarem pr, remesa r, nit n where plaveh  like ?  and p.nitpro like ? and cedcon like ? and pr.numpla=p.numpla and fecdsp >=? and fecdsp<= ? and n.cedula = p.cedcon and pr.numrem = r.numrem and p.base =?  order by p.numpla"*/
    
    //JEscadon 26-12-06
    private static String Search = "SELECT numpla FROM planilla WHERE numpla = ? AND BASE = 'COL' ";
    
    
    /**
     *  Reporte de Extrafletes y Costos Reebolsables
     */
    private static String SQL_REP_EXTRAFLETES =
    "SELECT rem.numrem, " +
    "       COALESCE(get_nombrecliente(rem.cliente), 'NO ENCONTRADO') AS cliente, " +
    "       COALESCE(get_nombreusuario(usuario_aprobo), 'NO ENCONTRADO') AS usuario, " +
    "       COALESCE(obs.observacion, '') AS observacion, " +
    "       pl.numpla, " +
    "       pl.fecdsp, " +
    "       get_nombreciudad(oripla) || ' - ' || get_nombreciudad(despla) AS ruta, " +
    "       COALESCE(vlr_costo, 0.0) AS vlr, " +
    "       COALESCE(vlr_ingreso, 0.0) AS vlr_ingreso, " +
    "       COALESCE(codext.descripcion, '') AS desc, " +
    "       CASE WHEN tipo_costo='R' THEN 'Costo Reembolsable' ELSE 'Extraflete' END AS tipo, " +
    "       codext.codextraflete, " +
    "       array_to_string(array_accum(doc.documento),', ') AS documentos, " +
    "       array_to_string(array_accum(tdoc.document_name),', ') AS tdocumentos  " +
    "FROM (" +
    "   SELECT  numpla, " +
    "           fecdsp, " +
    "           cia, " +
    "           oripla, " +
    "           despla " +
    "   FROM    planilla " +
    "   WHERE   fecdsp BETWEEN ? AND ? AND reg_status!='A' ) AS pl " +
    "JOIN costo_reembolsables  creemb ON ( pl.numpla = creemb.numpla ) " +
    "JOIN codextraflete codext ON ( codext.codextraflete=creemb.codigo ) " +
    "LEFT JOIN observaciones_extraflete obs ON (pl.cia=obs.dstrct AND obs.numpla=pl.numpla AND codext.tipo=obs.tipo AND codext.codextraflete=obs.codextraflete) " +
    "JOIN plarem plr ON ( plr.numpla=pl.numpla ) " +
    "JOIN remesa rem ON ( rem.numrem=plr.numrem ) ";
    
    private static String SQL_REP_EXTRAFLETES_R =
    "SELECT rem.numrem, " +
    "       COALESCE(get_nombrecliente(rem.cliente), 'NO ENCONTRADO') AS cliente, " +
    "       COALESCE(get_nombreusuario(usuario_aprobo), 'NO ENCONTRADO') AS usuario, " +
    "       COALESCE(obs.observacion, '') AS observacion, " +
    "       pl.numpla, " +
    "       pl.fecdsp, " +
    "       get_nombreciudad(oripla) || ' - ' || get_nombreciudad(despla) AS ruta, " +
    "       COALESCE(vlr_costo, 0.0) AS vlr, " +
    "       COALESCE(vlr_ingreso, 0.0) AS vlr_ingreso, " +
    "       COALESCE(codext.descripcion, '') AS desc, " +
    "       'Costo Reembolsable' AS tipo, " +
    "       codext.codextraflete, " +
    "       array_to_string(array_accum(doc.documento),', ') AS documentos, " +
    "       array_to_string(array_accum(tdoc.document_name),', ') AS tdocumentos  " +
    "FROM (" +
    "   SELECT  numpla, " +
    "           fecdsp, " +
    "           cia, " +
    "           oripla, " +
    "           despla " +
    "   FROM    planilla " +
    "   WHERE   fecdsp BETWEEN ? AND ? AND reg_status!='A' ) AS pl " +
    "JOIN costo_reembolsables  creemb ON ( tipo_costo='R' AND pl.numpla = creemb.numpla ) " +
    "JOIN codextraflete codext ON ( codext.codextraflete=creemb.codigo ) " +
    "LEFT JOIN observaciones_extraflete obs ON (pl.cia=obs.dstrct AND obs.numpla=pl.numpla AND codext.tipo=obs.tipo AND codext.codextraflete=obs.codextraflete) " +
    "JOIN plarem plr ON ( plr.numpla=pl.numpla ) " +
    "JOIN remesa rem ON ( rem.numrem=plr.numrem ) ";
    
    private static String SQL_REP_EXTRAFLETES_E =
    "SELECT rem.numrem, " +
    "       COALESCE(get_nombrecliente(rem.cliente), 'NO ENCONTRADO') AS cliente, " +
    "       COALESCE(get_nombreusuario(usuario_aprobo), 'NO ENCONTRADO') AS usuario, " +
    "       COALESCE(obs.observacion, '') AS observacion, " +
    "       pl.numpla, " +
    "       pl.fecdsp, " +
    "       get_nombreciudad(oripla) || ' - ' || get_nombreciudad(despla) AS ruta, " +
    "       COALESCE(vlr_costo, 0.0) AS vlr, " +
    "       COALESCE(vlr_ingreso, 0.0) AS vlr_ingreso, " +
    "       COALESCE(codext.descripcion, '') AS desc, " +
    "       'Extraflete' AS tipo, " +
    "       codext.codextraflete, " +
    "       array_to_string(array_accum(doc.documento),', ') AS documentos, " +
    "       array_to_string(array_accum(tdoc.document_name),', ') AS tdocumentos  " +
    "FROM (" +
    "   SELECT  numpla, " +
    "           fecdsp, " +
    "           cia, " +
    "           oripla, " +
    "           despla " +
    "   FROM    planilla " +
    "   WHERE   fecdsp BETWEEN ? AND ? AND reg_status!='A' ) AS pl " +
    "JOIN costo_reembolsables  creemb ON ( tipo_costo='E' AND pl.numpla = creemb.numpla ) " +
    "JOIN codextraflete codext ON ( codext.codextraflete=creemb.codigo ) " +
    "LEFT JOIN observaciones_extraflete obs ON (pl.cia=obs.dstrct AND obs.numpla=pl.numpla AND codext.tipo=obs.tipo AND codext.codextraflete=obs.codextraflete) " +
    "JOIN plarem plr ON ( plr.numpla=pl.numpla ) " +
    "JOIN remesa rem ON ( rem.numrem=plr.numrem ) ";
    
    
    
    private static String SQL_VIAJE_XLS =
    "SELECT " +
    "     distinct sum(planilla.pesoreal) as toneladas, "+
    "CASE " +
    "     WHEN (to_char (PLANILLA.feccum,'HH24:mi')>'06:59' and to_char (PLANILLA.feccum,'HH24:mi')<='23:59')then to_char (PLANILLA.feccum,'DD') "+
    "WHEN to_char(PLANILLA.feccum,'HH24:mi')>='00:00' and to_char (PLANILLA.feccum,'HH24:mi')<'07:00' then to_char (PLANILLA.feccum-'1d'::interval,'DD') end as dia, "+
    "count(PLANILLA.numpla) as viajes "+
    "FROM" +
    "     (SELECT * FROM PLANILLA WHERE FECCUM BETWEEN ? AND ? AND BASE in ( 'pco','spo') and reg_status <>'A') AS PLANILLA "+
    "     LEFT JOIN PLAREM ON (PLANILLA.NUMPLA = PLAREM.NUMPLA) "+
    "     LEFT JOIN REMESA ON (REMESA.NUMREM = PLAREM.NUMREM )WHERE REMESA.std_job_no=? "+
    "     GROUP BY dia "+
    "     ORDER BY dia ";
    
    //amartinez 24-jul-2006
    private static final String veh_externos      =   "  SELECT a.*, b.agencia_disp FROM (SELECT max(a.fecha_disp) AS fecha_disp, a.placa, a.DSTRCT FROM veh_externos a WHERE a.fecha_disp BETWEEN ? AND ? AND a.dstrct = ? AND a.reg_status <> 'A' GROUP BY 2,3 ) A, veh_externos b WHERE a.placa = b.placa AND a.DSTRCT = b.DSTRCT AND a.fecha_disp = b.fecha_disp  ";
    
    /*Karen 14-03-2006*/
    private static final String SEARCH_OC_C =
    "Select 	p.*, " +
    "	CASE WHEN m.vlr is not null THEN m.vlr+p.vlrpla ELSE p.vlrpla end as vlr," +
    "	r.std_job_no, " +
    "	r.descripcion," +
    "	c.nombre," +
    "	ci.nomciu as origen, " +
    "	ci2.nomciu as destino," +
    "       coalesce(via.descripcion,'NO SE ENCONTRO RUTA') as descVia" +
    " from 	planilla p left join (select sum(vlr)as vlr, planilla from movpla where planilla = ? and concept_code= '09' group by planilla)m on(m.planilla=p.numpla) " +
    "       left outer join via on (via.origen||via.destino = p.ruta_pla)," +
    "       remesa r, " +
    "	plarem rp, " +
    "	nit c, " +
    "	ciudad ci, " +
    "	ciudad ci2 " +
    " where   rp.numrem=r.numrem " +
    "	and rp.numpla=p.numpla " +
    "	and p.numpla=? " +
    "	and p.cedcon=c.cedula " +
    "	and p.oripla=ci.codciu " +
    "	and p.despla=ci2.codciu ";
    private static String SQL_UPDATEPLA  ="update planilla set pesoreal=?, vlrpla=?, plaveh=?, platlr=?, cedcon=?, orden_carga=?, nitpro=?, precinto=?, fecdsp=?, unidcam=?, group_code=?, feccum=?,contenedores=?,tipocont=?,tipotrailer=?,unit_vlr=?, proveedor=?, last_update ='now()' where numpla=?";
    private static String SQL_SEARCHPLAC  ="Select 	p.*, " +
    "	CASE WHEN m.vlr is not null THEN m.vlr+p.vlrpla ELSE p.vlrpla end as vlr," +
    "	r.std_job_no, " +
    "	r.descripcion," +
    "	c.nombre," +
    "	ci.nomciu as origen, " +
    "	ci2.nomciu as destino," +
    "       coalesce(via.descripcion,'NO SE ENCONTRO RUTA') as descVia" +
    " from 	planilla p left join (select sum(vlr)as vlr, planilla from movpla where planilla = ? and concept_code= '09' group by planilla)m on(m.planilla=p.numpla) " +
    "       left outer join via on (via.origen||via.destino||secuencia = p.ruta_pla)," +
    "       remesa r, " +
    "	plarem rp, " +
    "	nit c, " +
    "	ciudad ci, " +
    "	ciudad ci2 " +
    " where   rp.numrem=r.numrem " +
    "	and rp.numpla=p.numpla " +
    "	and p.numpla=? " +
    "	and p.cedcon=c.cedula " +
    "	and p.oripla=ci.codciu " +
    "	and p.despla=ci2.codciu ";
    //Karen 15-03-2006
    private static String SQL_TOTALEQDESP  =
    "SELECT  " +
    "       count( distinct pla.plaveh) as cant " +
    "FROM   PLANILLA pla, " +
    "       PLAREM pr, " +
    "       REMESA rem " +
    "WHERE  pla.feccum BETWEEN ? AND ? " +
    "       and pr.numrem = rem.numrem " +
    "       and pr.numpla = pla.numpla" +
    "       and pla.reg_status=''" +
    "       and pla.base = ?";
    
    private static String SQL_LISTAR_PLANILLAS = "Select 	p.numpla, " +
    "	p.cedcon, " +
    "	p.plaveh, " +
    "	pr.numrem, " +
    "	n.nombre," +
    "	r.descripcion," +
    "   pr.porcent," +
    "   r.currency," +
    "   r.vlrrem" +
    " from  	planilla p left outer join nit n on (n.cedula = p.cedcon)" +
    " 	inner join plarem pr  on (pr.numpla = p.numpla)" +
    "	inner join remesa r on (r.numrem=pr.numrem)" +
    " where 	p.numpla =?" +
    "	and pr.reg_status =''";
    
    
    private static final String SQL_PLACA_TRANSITO ="SELECT NUMPLA, PLAVEH FROM PLANILLA WHERE PLAVEH =? AND PESOREAL=0 AND FECCUM='0099-01-01 00:00' AND BASE = 'pco'";
    
    private static String INSERT_ING_TRAFICO ="INSERT INTO Ingreso_trafico" +
    "	(dstrct," +
    "  planilla," +
    "  placa," +
    "  cedcon," +
    "  nomcond," +
    "  cedprop," +
    "  nomprop," +
    "  origen," +
    "  nomorigen," +
    "  destino," +
    "  nomdestino," +
    "  zona," +
    "  nomzona," +
    "  fecha_despacho," +
    "  creation_user," +
    "  base," +
    "  pto_control_proxreporte," +
    "  nompto_control_proxreporte," +
    "  fecha_prox_reporte," +
    "   via," +
    "   cel_cond," +
    "   cliente," +
    "   fecha_salida,   " +
    "   user_update," +
    "   pto_control_ultreporte," +
    "   nompto_control_ultreporte," +
    "   fecha_ult_reporte," +
    "   ult_observacion,eintermedias,neintermedias ) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
        
    /*08-05-2006 Juan*/
    private static String reporteDiarioDespacho     =   "    select  p.agcpla, p.fecdsp,  p.fecpla, p.numpla, p.reg_status,       "+
    "   To_Char(p.printer_date,'YYYY-MM-DD') as printer_date,                 "+
    "   c.nomciu as origen,  c1.nomciu as destino,  p.plaveh, p.unit_vlr as udespachada,             "+
    "   p.currency as monedaplanilla,  m.currency as monedaanticipo,                                 "+
    "    r.currency as monedaremesa,  r.pesoreal as cantidadfacturada,                               "+
    "    p.despachador, n.nombre as conductor, st.porcentaje_maximo_anticipo,st.unidad,              "+
    "    p.pesoreal, case when m1.vlr is null then p.vlrpla else p.vlrpla+ m1.vlr end as vlrpla,     "+
    "    m.anticipo, r.numrem,  cli.nomcli,                                                          "+
    "    r.std_job_no ||'-'||r.descripcion as std_job, r.vlrrem, p.contenedores,                     "+
    "    get_documentos(r.numrem, 'DOCUINTERNO') as documentosinternos,                              "+
    "    get_documentos(r.numrem, 'FACTCIAL') as facturacial,                                        "+
    "    get_documentos(r.numrem, 'DOCUREL') as relacionados                                         "+
    "    from                                                                                        "+
    "    planilla p                                                                                  "+
    "    left outer join ciudad c on (c.codciu = p.oripla)                                           "+
    "    left outer join ciudad c1 on (c1.codciu = p.despla)                                         "+
    "    left outer join                                                                             "+
    "    (select sum(vlr)as anticipo, planilla, currency from movpla                                 "+
    "    where concept_code = '01' group by planilla,currency) m on (m.planilla = p.numpla)          "+
    "    left outer join                                                                             "+
    "     (select sum(vlr)as vlr, planilla from movpla                                               "+
    "    where concept_code = '09' group by planilla) m1 on (m1.planilla = p.numpla)                 "+
    "    left outer join nit n on (n.cedula = p.cedcon)                                              "+
    "    join plarem pr on (pr.numpla = p.numpla)                                                    "+
    "    join remesa r on (r.numrem = pr.numrem)                                                     "+
    "    left outer join stdjob st on (st.dstrct_code = 'FINV' AND st.std_job_no = r.std_job_no)      "+
    "    left outer join cliente cli on ( cli.dstrct = 'FINV' AND cli.codcli = r.cliente )            "+
    "    where  fecdsp between ? and ?   #AGCPLA#                                                    "+
    "    and plaveh like ?  and cedcon like ? and despachador like ? order by fecdsp                 ";
    
    
    private static String valortotalingreso = " SELECT DISTINCT SJ.COD_EXTRAFLETE, SJ.VLR_INGRESO, SJ.MONEDA_INGRESO, SJ.VF_INGRESO, SJ.PORCENTAJE, "+
    " SJ.CLASE_VALOR                                                                                      "+
    " FROM MOVPLA MV, CODEXTRAFLETE CE, SJEXTRAFLETE SJ                                                   "+
    " WHERE MV.PLANILLA = ? AND MV.CONCEPT_CODE = CE.CODEXTRAFLETE                                        "+
    " AND CE.CODEXTRAFLETE = SJ.COD_EXTRAFLETE                                                            ";
    
    private static String totalingreso  =     " SELECT DISTINCT SJ.COD_EXTRAFLETE, SJ.VLR_INGRESO, SJ.MONEDA_INGRESO, SJ.VF_INGRESO, SJ.PORCENTAJE,"+
    " CE.DESCRIPCION, SJ.CLASE_VALOR                                                                     "+
    " FROM MOVPLA MV, CODEXTRAFLETE CE, SJEXTRAFLETE SJ                                                  "+
    " WHERE MV.PLANILLA = ? AND MV.CONCEPT_CODE = CE.CODEXTRAFLETE                                       "+
    " AND CE.CODEXTRAFLETE = SJ.COD_EXTRAFLETE                                                           ";
    
    private static String flete                     =   " SELECT 										"+
    "	CE.DESCRIPCION, 								"+
    "	MV.CONCEPT_CODE, 								"+
    "	MV.VLR,										"+
    "	suma.VALORTOTAL									"+
    "FROM 											"+
    "	MOVPLA MV,CODEXTRAFLETE CE,							"+
    "	(										"+
    "		SELECT SUM(VLR)AS VALORTOTAL, MV.PLANILLA            			"+
    "		FROM 									"+
    "			MOVPLA MV							"+
    "		JOIN    CODEXTRAFLETE CE ON ( CE.CODEXTRAFLETE = MV.CONCEPT_CODE )	"+
    "		GROUP BY MV.PLANILLA  							"+
    "	) AS suma									"+
    "WHERE 											"+
    "	MV.PLANILLA = ?  							"+
    "	AND CE.CODEXTRAFLETE = MV.CONCEPT_CODE  					"+
    "	AND SUMA.PLANILLA = MV.PLANILLA							";
    
    PlanillaDAO(){
        super("PlanillaDAO.xml");
    }
    PlanillaDAO(String dataBaseName){
        super("PlanillaDAO.xml", dataBaseName);
    }
    
    //METODOS PARA OBTENER Y SETEAR LOS ANTRIBUTOS DE PLANILLADAO
    public void setPlanilla(Planilla pla){
        this.pla = pla;
    }
    
    public Plaaux getPlaaux(){
        
        return this.plaaux;
    }
    public void setPlaaux(Plaaux p){
        
        this.plaaux = p;
    }
    public float getSaldo(){
        
        return this.saldo;
    }
    public List getRemesas(){
        
        return this.remesas;
    }
    
    public Vector getClientes(){
        
        return this.clientes;
    }
    public Vector getProveedores(){
        
        return this.proveedores;
    }
    public Vector getRutas(){
        
        return this.rutas;
    }
    public Vector getPlanillasVec(){
        
        return this.plas;
    }
    public Vector getInformes(){
        
        return this.informes;
    }
    public Planilla getPla(){
        
        return this.pla;
    }
    public Proveedores getProveedor(){
        
        return this.prov;
    }
    public Vector getPlanillasIncompletas(){
        
        return this.plaIncompletas;
    }
    
    public List getPlanillas(){
        
        return this.planillas;
    }
    
    
    
    //METODOS PARA BUSCAR
    public double buscarAnticipo(String planilla)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        double result=0;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("select sum (vlr_disc)as anticipo from movpla where planilla = ? ");
                st.setString(1, planilla);
                rs = st.executeQuery();
                if(rs.next()){
                    result = rs.getFloat("anticipo");
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL ANTICIPO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return result;
        
    }
    public String buscarFecCorte(String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        String result="";
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("select max(corte) as fec from planilla where base =?");
                st.setString(1, base);
                rs = st.executeQuery();
                if(rs.next()){
                    result = rs.getString("fec");
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL ANTICIPO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return result;
        
    }
    
    public int buscarTotalEquipo(String fechai, String fechaf, String cliente, String ruta, String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        int result=0;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("" +
                "SELECT  count( distinct pla.plaveh) as cant " +
                "FROM   PLANILLA pla, " +
                "       PLAREM pr, " +
                "       REMESA rem " +
                "WHERE  pla.corte like ? " +
                "       and rem.std_job_no = ?" +
                "       and rem.cliente = ? " +
                "       and pr.numrem = rem.numrem " +
                "       and pr.numpla = pla.numpla" +
                "       and pla.reg_status=''" +
                "       and pla.base = ?");
                st.setString(1, fechai+"%");
                st.setString(2, ruta);
                st.setString(3, cliente);
                st.setString(4, base);
                ////System.out.println("Sql: "+st.toString());
                rs = st.executeQuery();
                if(rs.next()){
                    result = rs.getInt("cant");
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA CANTIDAD DE EQUIPOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return result;
        
    }
    
    
    public int cantPlasIncompletas(String usuario)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        int cant=0;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("Select count(*) as cantidad from plaaux p, planilla pla where p.empty_weight = 0.0  and p.creation_user = ? and p.pla=pla.numpla");
                st.setString(1,usuario);
                
                rs = st.executeQuery();
                
                if(rs.next()){
                    cant=rs.getInt("cantidad");
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL CONTEO DE LAS PLANILLAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return cant;
        
    }
    public boolean existPlasIncompletas(String usuario)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("Select p.*, pla.plaveh from plaaux p, planilla pla where p.empty_weight = 0.0  and p.creation_user = ? and p.pla=pla.numpla order by pla");
                st.setString(1,usuario);
                
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS SI EXISTEN " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return sw;
        
    }
    
    public boolean existPlanillas(String usuario)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("Select p.*, pr.numrem, c.nombre from  planilla p, plarem pr, nit c  where p.despachador = ? and p.printer_date='0099-01-01 00:00:00' and p.numpla=pr.numpla and p.cedcon=c.cedula order by numpla");
                st.setString(1,usuario);
                
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS SI EXISTEN " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return sw;
        
    }
    
    public void searchPlasIncompletas(String base )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("Select 	pa.*, " +
                "	p.plaveh," +
                "       p.cedcon, " +
                "	r.remision, " +
                "	r.descripcion," +
                "	n.nombre" +
                " from 	plaaux pa, " +
                "	planilla p, " +
                "	plarem pr, " +
                "	remesa r," +
                "	nit n" +
                " where 	pa.empty_weight = 0.0  " +
                "	and p.base = ? " +
                "	and p.numpla=pa.pla " +
                "	and pa.reg_status='' " +
                "	and pr.numpla = p.numpla " +
                "	and r.numrem = pr.numrem   " +
                "	and n.cedula = p.cedcon" +
                " order by plaveh");
                st.setString(1,base);
                
                rs = st.executeQuery();
                plaIncompletas = new Vector();
                
                while(rs.next()){
                    plaaux= Plaaux.load(rs);
                    plaaux.setRemision(rs.getString("remision"));
                    plaaux.setSj(rs.getString("descripcion"));
                    plaaux.setConductor(rs.getString("cedcon")+ " - "+rs.getString("nombre"));
                    plaIncompletas.add(plaaux);//Aqui van PlaAux
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS Incompletas " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
    }
    
    public void buscarTransitoPlaca(String placa, String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("Select 	pa.*, " +
                "	p.plaveh," +
                "       p.cedcon, " +
                "	r.remision, " +
                "	r.descripcion," +
                "	n.nombre" +
                " from 	plaaux pa, " +
                "	planilla p, " +
                "	plarem pr, " +
                "	remesa r," +
                "	nit n" +
                " where 	pa.empty_weight = 0.0  " +
                "	and p.base = ? " +
                "	and p.numpla=pa.pla " +
                "	and pa.reg_status='' " +
                "	and pr.numpla = p.numpla " +
                "	and r.numrem = pr.numrem   " +
                "	and n.cedula = p.cedcon" +
                "       and p.plaveh = ?" +
                " order by plaveh");
                st.setString(1,base);
                st.setString(2,placa);
                
                rs = st.executeQuery();
                if(rs.next()){
                    plaaux= Plaaux.load(rs);
                    plaaux.setRemision(rs.getString("remision"));
                    plaaux.setSj(rs.getString("descripcion"));
                    plaaux.setConductor(rs.getString("cedcon")+ " - "+rs.getString("nombre"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS EN TRANSITO POR PLACA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
    }
    public void buscarTransitoRemision(String remision, String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        plaaux=null;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("Select 	pa.*, " +
                "	p.plaveh," +
                "       p.cedcon, " +
                "	r.remision, " +
                "	r.descripcion," +
                "	n.nombre" +
                " from 	plaaux pa, " +
                "	planilla p, " +
                "	plarem pr, " +
                "	remesa r," +
                "	nit n" +
                " where 	pa.empty_weight = 0.0  " +
                "	and p.base = ? " +
                "	and p.numpla=pa.pla " +
                "	and pa.reg_status='' " +
                "	and pr.numpla = p.numpla " +
                "	and r.numrem = pr.numrem   " +
                "	and n.cedula = p.cedcon" +
                "       and r.remision = ?" +
                " order by plaveh");
                st.setString(1,base);
                st.setString(2,remision);
                ////System.out.println("Consulta remision = "+st.toString());
                rs = st.executeQuery();
                if(rs.next()){
                    plaaux= Plaaux.load(rs);
                    plaaux.setRemision(rs.getString("remision"));
                    plaaux.setSj(rs.getString("descripcion"));
                    plaaux.setConductor(rs.getString("cedcon")+ " - "+rs.getString("nombre"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS EN TRANSITO POR REMISION " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
    }
    public void searchPlanillas(String usuario )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("Select p.*, pr.numrem, c.nombre, ci.nomciu as origen, ci2.nomciu as destino from  planilla p, plarem pr, nit c, plaaux pa, ciudad ci, ciudad ci2  where p.despachador = ? and p.printer_date='0099-01-01 00:00:00' and p.numpla=pr.numpla and p.cedcon=c.cedula and pa.empty_weight!=0.0 and pa.pla=p.numpla and p.oripla=ci.codciu and p.despla=ci2.codciu and p.reg_status='' order by numpla");
                st.setString(1,usuario);
                
                rs = st.executeQuery();
                planillas = new LinkedList();
                
                while(rs.next()){
                    pla=Planilla.load(rs);
                    pla.setNomCond(rs.getString("nombre"));
                    pla.setNumrem(rs.getString("numrem"));
                    pla.setNomori(rs.getString("origen"));
                    pla.setNomdest(rs.getString("destino"));
                    planillas.add(pla);
                    //Aqui van PlaAux
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS SIN IMPRIMIR  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
    }
    public void buscaFecha(String plaveh )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        pla = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("select fecdsp from planilla where plaveh = ? and reg_status = '' order by fecdsp desc");
                st.setString(1,plaveh);
                rs = st.executeQuery();
                if(rs.next()){
                    pla = new Planilla();
                    pla.setFecdsp(rs.getString("fecdsp"));
                    //Aqui van PlaAux
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS SIN IMPRIMIR  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
    }
    
    
    public void searchPColpapel(String usuario )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("Select p.*, pr.numrem, c.nombre, ci.nomciu as origen, ci2.nomciu as destino from  planilla p, plarem pr, nit c,  ciudad ci, ciudad ci2,remesa r  where p.despachador = ? and p.printer_date='0099-01-01 00:00:00' and p.numpla=pr.numpla and p.cedcon=c.cedula and p.oripla=ci.codciu and p.despla=ci2.codciu and p.reg_status='' and r.numrem = pr.numrem and r.reg_status=''  order by numpla");
                st.setString(1,usuario);
                ////System.out.println(""+st.toString());
                
                rs = st.executeQuery();
                planillas = new LinkedList();
                
                while(rs.next()){
                    pla=Planilla.load(rs);
                    pla.setNomCond(rs.getString("nombre"));
                    pla.setNumrem(rs.getString("numrem"));
                    pla.setNomori(rs.getString("origen"));
                    pla.setNomdest(rs.getString("destino"));
                    planillas.add(pla);
                    //Aqui van PlaAux
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS SIN IMPRIMIR  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
    }
    public void searchPla(String codpla )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaaux = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("Select pa.*, p.plaveh from plaaux pa, planilla p where  p.numpla=? and pa.pla=p.numpla ");
                st.setString(1,codpla);
                
                rs = st.executeQuery();
                
                while(rs.next()){
                    plaaux=Plaaux.load(rs);
                    //Aqui van PlaAux
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLASAuxiliares " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
    }
    
    
    public void searchSaldo(String codpla )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaaux = null;
        PoolManager poolManager = null;
        saldo= 0;
        this.informes = new Vector();
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                float valorpla=0;
                try {
                    //BUSCO LOS AJUSTES Y LOS SUMO A LA PLANILLA
                    st = con.prepareStatement(this.SELECT_VALOR_PLANILLA);
                    st.setString(1,codpla);
                    rs = st.executeQuery();
                    
                    while(rs.next()){
                        valorpla = rs.getFloat("vlrpla");
                    }
                }catch(SQLException e){
                    throw new SQLException("ERROR CONCEPTOS DE DESCUENTOS DE LA PLANILLA EN AJUSTE VLR PLA " + e.getMessage() + " " + e.getErrorCode());
                }
                
                
                try {
                    //BUSCO LOS AJUSTES Y LOS SUMO A LA PLANILLA
                    st = con.prepareStatement(this.SELECT_AJUSTE_PLANILLA);
                    st.setString(1,codpla);
                    rs = st.executeQuery();
                    
                    while(rs.next()){
                        Movpla m = new Movpla();
                        m.setConcept_code("AJUSTE AL VALOR DE LA PLANILLA");
                        m.setVlr(rs.getFloat("vlr"));
                        valorpla = valorpla + rs.getFloat("vlr");
                        informes.add(m);
                    }
                }catch(SQLException e){
                    throw new SQLException("ERROR CONCEPTOS DE DESCUENTOS DE LA PLANILLA EN AJUSTE VLR PLA " + e.getMessage() + " " + e.getErrorCode());
                }
                
                
                
                try {
                    //BUSCO LOS EXTRAFLETES Y LOS SUMO A LA PLANILLA
                    st = con.prepareStatement(this.SELECT_BUSCAR_EXTRAFLETES);
                    st.setString(1,codpla);
                    rs = st.executeQuery();
                    
                    while(rs.next()){
                        Movpla m = new Movpla();
                        m.setConcept_code(rs.getString("descripcion"));
                        m.setVlr(rs.getFloat("vlr"));
                        valorpla = valorpla + rs.getFloat("vlr");
                        informes.add(m);
                    }
                }catch(SQLException e){
                    throw new SQLException("ERROR CONCEPTOS DE DESCUENTOS DE LA PLANILLA EN EXTRAFLETE" + e.getMessage() + " " + e.getErrorCode());
                }
                
                try {
                    //BUSCO LOS DESCUENTOS QUE AFECTAN EL VALOR DE LA PLANILLA Y SON DE TIPO PORCENTAJE
                    st= con.prepareStatement(this.SELECT_DESCUENTOS_PORCENTAJES);
                    st.setString(1,codpla);
                    rs = st.executeQuery();
                    while(rs.next()){
                        Movpla m = new Movpla();
                        m.setConcept_code(rs.getString("concept_desc"));
                        m.setVlr((rs.getFloat("vlr")*valorpla)/100);
                        informes.add(m);
                    }
                }catch(SQLException e){
                    throw new SQLException("ERROR CONCEPTOS DE DESCUENTOS DE LA PLANILLA EN DESCUENTOS PORCENTAJE" + e.getMessage() + " " + e.getErrorCode());
                }
                //RETENCION EN LA FUENTE
                Movpla m = new Movpla();
                m.setConcept_code("RETENCION EN LA FUENTE");
                m.setVlr((-1*valorpla)/100);
                informes.add(m);
                try {
                    //BUSCO LOS DESCUENTOS QUE AFECTAN EL VALOR DE LA PLANILLA Y SON DE TIPO VALOR
                    st= con.prepareStatement(this.SELECT_DESCUENTOS_VALOR);
                    st.setString(1,codpla);
                    rs = st.executeQuery();
                    while(rs.next()){
                        m = new Movpla();
                        m.setConcept_code(rs.getString("concept_desc"));
                        m.setVlr(rs.getFloat("vlr"));
                        informes.add(m);
                    }
                }catch(SQLException e){
                    throw new SQLException("ERROR CONCEPTOS DE DESCUENTOS DE LA PLANILLA EN DESCUENTOS VALOR" + e.getMessage() + " " + e.getErrorCode());
                }
                
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS CONCEPTOS DE DESCUENTOS DE LA PLANILLA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
    }
    /**
     *Metodo: searchPlanillaC, Metodo que busca los datos de una planilla dada
     *@autor : Ing. Karen Reales
     *@version : 1.0
     */
    
    public void searchPlanillaC(String codpla )throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        pla = null;
        
        try {
            
            st = this.crearPreparedStatement("SQL_SEARCHPLAC");
            st.setString(1,codpla);
            st.setString(2,codpla);
            rs = st.executeQuery();
            
            if(rs.next()){
                pla=Planilla.load(rs);
                pla.setNomCond(rs.getString("nombre"));
                pla.setSj(rs.getString("std_job_no"));
                pla.setSj_desc(rs.getString("descripcion"));
                pla.setNomori(rs.getString("origen"));
                pla.setNomdest(rs.getString("destino"));
                pla.setContenedores(rs.getString("contenedores"));
                pla.setTipocont(rs.getString("tipocont"));
                pla.setUnit_transp(rs.getString("unit_vlr"));
                pla.setVlrpla(rs.getFloat("vlr"));
                pla.setTipotrailer(rs.getString("tipotrailer"));
                pla.setRuta_pla(rs.getString("descVia"));
                pla.setPrinter_date(rs.getString("Printer_date"));
                pla.setSticker(rs.getString("Sticker"));
                pla.setBase(rs.getString("base"));
                //Aqui van Planillas de colpapel
            }
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS CARGA VARIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_SEARCHPLAC");
            
            
        }
        
    }
    
    public void searchPlanillaCImp(String codpla )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        pla = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("Select p.*, r.std_job_no, r.descripcion,c.nombre,ci.nomciu as origen, ci2.nomciu as destino from planilla p, remesa r, plarem rp, nit c, ciudad ci, ciudad ci2 where   rp.numrem=r.numrem and rp.numpla=p.numpla and p.numpla=? and p.cedcon=c.cedula and p.oripla=ci.codciu and p.despla=ci2.codciu and p.reg_status='' and p.printer_date <>'0099-01-01 00:00:00'");
                st.setString(1,codpla);
                
                rs = st.executeQuery();
                
                if(rs.next()){
                    pla=Planilla.load(rs);
                    pla.setNomCond(rs.getString("nombre"));
                    pla.setSj(rs.getString("std_job_no"));
                    pla.setSj_desc(rs.getString("descripcion"));
                    pla.setNomori(rs.getString("origen"));
                    pla.setNomdest(rs.getString("destino"));
                    //Aqui van Planillas de colpapel
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS COLPAPEL " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
    }
    
    /**
     *Metodo que busca los datos de una planilla
     *@autor: Karen Reales
     *@param: Numero de la planilla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void searchPlanillaAnticipo(String codpla )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        pla = null;
        
        try {
            
            st = this.crearPreparedStatement("SQL_SEARCH_PLANILLAANTICIPO");
            st.setString(1,codpla);
            rs = st.executeQuery();
            
            if(rs.next()){
                pla = new Planilla();
                pla.setNumpla(rs.getString("numpla"));
                pla.setFecdsp(rs.getString("fecdsp"));
                pla.setPlaveh(rs.getString("plaveh"));
                pla.setNomCond(rs.getString("cedcon")+" "+rs.getString("nombrecond"));
                pla.setNitpro(rs.getString("nitpro")+" - "+rs.getString("nombreprop"));
                pla.setSj(rs.getString("std_job_no"));
                pla.setSj_desc(rs.getString("descripcion"));
                pla.setMoneda(rs.getString("currency"));
                pla.setVlrpla(rs.getFloat("vlrpla"));
                pla.setOrinom(rs.getString("origen"));
                pla.setDestinopla(rs.getString("destino"));
                pla.setProveedor(rs.getString("proveedor")!=null?rs.getString("proveedor"):"NO SE ENCONTRO CODIGO DE PROVEEDOR"+" "+rs.getString("banco")!=null?rs.getString("banco"):"NO SE ENCONTRO BANCO ASOCIADO");
                //AGREGO KAREN
                pla.setOripla(rs.getString("oripla"));
                pla.setDespla(rs.getString("despla"));
                pla.setCedcon(rs.getString("cedcon"));
                pla.setTotalajustes(0);
                pla.setTotalanticipo(0);
                float saldo =( pla.getVlrpla()+pla.getTotalajustes())-pla.getTotalanticipo();
                pla.setSaldo(saldo);
                    /*com.tsp.operation.model.StdjobdetselService stdService = new com.tsp.operation.model.StdjobdetselService();
                    stdService.searchStdJob(pla.getSj());*/
            }
            
            
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS DE ANTICIPO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            this.desconectar("SQL_SEARCH_PLANILLAANTICIPO");
        }
        
    }
    //ESTE METODO BUSCA LAS PLANILLAS DADO UN NUMERO DE REMISION PARA CARBON
    public void searchPlanilla(String codpla)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        pla = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(" Select  p.*, " +
                " pr.numrem,      " +
                "	c.nombre,       " +
                "	ci.nomciu as origen,    " +
                "	ci2.nomciu as destino,  " +
                "	r.std_job_no, " +
                "	r.descripcion as sj_desc,        " +
                "	prop.nombre as prop " +
                "from  	(select * from remesa where remision=? and reg_status<>'A'  )r" +
                "	inner join plarem pr on (pr.numrem = r.numrem)" +
                "	inner join planilla p on (p.numpla = pr.numpla)" +
                "	inner join plaaux pa on (pa.pla=pr.numpla)" +
                "	left outer join nit c on (c.cedula = p.cedcon)" +
                "	left outer join ciudad ci on (ci.codciu = p.oripla)" +
                "	left outer join ciudad ci2 on (ci2.codciu = p.despla)" +
                "	left outer join nit prop on (prop.cedula = p.nitpro)");
                st.setString(1,codpla);
                ////System.out.println("Buscar planilla "+st.toString());
                rs = st.executeQuery();
                
                if(rs.next()){
                    pla=Planilla.load(rs);
                    pla.setNomCond(rs.getString("nombre"));
                    pla.setSj(rs.getString("std_job_no"));
                    pla.setSj_desc(rs.getString("sj_desc"));
                    pla.setNomori(rs.getString("origen"));
                    pla.setNomdest(rs.getString("destino"));
                    pla.setNomprop(rs.getString("prop"));
                    //Aqui van Planillas
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
    }
    
    
    /**
     *Metodo: updatePla, Metodo que retorna un string con el comando
     *para la modificacion de la planilla
     *@autor : Ing. Karen Reales
     *@return: String de comando SQL
     *@version : 1.0
     */
    
    public String updatePla()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql = "";
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SQL_UPDATEPLA);
                st.setFloat(1,pla.getPesoreal());
                st.setFloat(2,pla.getVlrpla());
                st.setString(3, pla.getPlaveh());
                st.setString(4, pla.getPlatlr());
                st.setString(5,pla.getCedcon());
                st.setString(6,pla.getOrden_carga());
                st.setString(7,pla.getNitpro());
                st.setString(8,pla.getPrecinto());
                st.setString(9, pla.getFecdsp());
                st.setString(10, pla.getMoneda());
                st.setString(11, pla.getGroup_code());
                st.setString(12, pla.getFeccum());
                st.setString(13, pla.getContenedores());
                st.setString(14, pla.getTipocont());
                st.setString(15, pla.getTipotrailer());
                st.setString(16, pla.getUnit_transp());
                st.setString(17, pla.getProveedor());
                st.setString(18,pla.getNumpla());
                sql = st.toString();
                
                
                //st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LAS PLANILLAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return sql;
    }
    public void updateCorte(String corte, String numpla)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql = "";
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("update planilla set corte=?, feccum=?,last_update='now()' where numpla=?");
                st.setString(1,corte);
                st.setString(2,corte);
                st.setString(3,numpla);
                ////System.out.println(st.toString());
                
                st = con.prepareStatement("update plaaux set corte=?, last_update = 'now()' where pla=?");
                st.setString(1,corte);
                st.setString(2,numpla);
                ////System.out.println(st.toString());
                st.executeUpdate();
                
                st = con.prepareStatement("update movpla set corte=?, last_update = 'now()' where planilla=?");
                st.setString(1,corte);
                st.setString(2,numpla);
                ////System.out.println(st.toString());
                st.executeUpdate();
                
                
                st = con.prepareStatement("select numrem from plarem where numpla=?  ");
                st.setString(1,numpla);
                rs = st.executeQuery();
                
                if(rs.next()){
                    st = con.prepareStatement("update remesa set corte=?, lastupdate = 'now()' where numrem=?");
                    st.setString(1,corte);
                    st.setString(2,rs.getString("numrem"));
                    st.executeUpdate();
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LAS PLANILLAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
    }
    /**
     *Metodo que retorna el comando para anular una planilla
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String anularPla()throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql ="";
        try {
            st = this.crearPreparedStatement("SQL_ANULARPLA");
            st.setString(1,pla.getNumpla());
            sql = st.toString();
            
            st = this.crearPreparedStatement("SQL_INSERTARDOCANULADO");
            st.setString(1,pla.getDistrito());
            st.setString(2,pla.getNumpla());
            st.setString(3,pla.getDespachador());
            st.setString(4,pla.getDespachador());
            st.setString(5,pla.getBase());
            st.setString(6,pla.getCausa());
            st.setString(7,pla.getT_recuperacion());
            st.setString(8,pla.getObservacion());
            sql =sql +";"+ st.toString();
            //st.executeUpdate();
            
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE LAS PLANILLAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_INSERTARDOCANULADO");
            this.desconectar("SQL_ANULARPLA");
            
        }
        return sql;
    }
    
    public boolean estaTagTraffic(String trafic)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("select * from placa_trafico where traffic_tag=?");
                st.setString(1,trafic);
                rs = st.executeQuery();
                if(rs.next()){
                    sw=true;
                }
                
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE CODIGOS DE TRAFICOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return sw;
    }
    
    public boolean estaTagTrafficPlaca(String trafic, String placa)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("select * from placa_trafico where traffic_tag=? and placa =? and reg_status='' ");
                st.setString(1,trafic);
                st.setString(2,placa);
                rs = st.executeQuery();
                if(rs.next()){
                    sw=true;
                }
                
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE CODIGOS DE TRAFICOS Y PLACAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return sw;
    }
    public String buscarTrafficCode(String placa)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String trafic="";
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("select * from placa_trafico where placa=? and reg_status = ''");
                st.setString(1,placa);
                rs = st.executeQuery();
                if(rs.next()){
                    trafic = rs.getString("traffic_tag");
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE CODIGOS DE TRAFICOS POR PLACA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return trafic;
    }
    
    public String ActualizarTrafficTag(String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql="";
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                if(!this.estaTagTraffic(pla.getTraffic_code())){
                    
                    String trafico_viejo = this.buscarTrafficCode(pla.getPlaveh());
                    st = con.prepareStatement("update placa_trafico set reg_status='A' where traffic_tag=? and placa =?");
                    st.setString(1,trafico_viejo);
                    st.setString(2,pla.getPlaveh());
                    
                    sql = st.toString();
                    
                    st = con.prepareStatement("insert into placa_trafico (placa,traffic_tag,creation_user,base) values(?,?,?,?)");
                    st.setString(1,pla.getPlaveh());
                    st.setString(2,pla.getTraffic_code());
                    st.setString(3,pla.getDespachador());
                    st.setString(4,base);
                    sql = sql +";"+st.toString();
                    
                }
                
                // st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION E INSERCCION DE TRAFFIC TAG" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return sql;
    }
    
    public String searchRuta(String numpla )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String nombre="No se econtro";
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("select substring(pla.ruta_pla,1,2), substring(pla.ruta_pla,3,2), co.nomciu as or, cd.nomciu as des from ciudad co, ciudad cd, planilla pla where co.codciu=substring(pla.ruta_pla,1,2) and cd.codciu=substring(pla.ruta_pla,3,2) and pla.numpla=?");
                st.setString(1,numpla);
                rs = st.executeQuery();
                if(rs.next()){
                    nombre = rs.getString("or") +"-"+rs.getString("des");
                }
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS REMITENTES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
            
        }
        return nombre;
    }
    
    //INFORMES TIPO PRODECO DE 7AM A 7PM
    /**
     *Metodo: llenarInforme2, Metodo que busca todas las planillas
     *        de carbon en unas fechas dadas INFORMES TIPO PRODECO DE 7AM A 7PM
     *@autor : Ing. Karen Reales
     *@param : fecha inicio , fecha fin, base
     *@return: Numero de viajes
     *@version : 2.0
     */
    public void llenarInforme2(String fechai, String fechaf,String cliente,String ruta, String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        inf=null;
        informes=null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SQL_INFORMEDESPACHO);
                st.setString(1, fechai);
                st.setString(2, fechaf);
                st.setString(3, ruta);
                st.setString(4, cliente);
                st.setString(5, base);
                ////System.out.println("este es el  query "+st.toString());
                rs = st.executeQuery();
                int i= 0;
                informes= new Vector();
                while(rs.next()){
                    i++;
                    inf = new Informe();
                    inf.setRemision(rs.getString("remision"));
                    inf.setPlaca(rs.getString("plaveh"));
                    inf.setConductor(rs.getString("nombre"));
                    inf.setTonelaje(rs.getFloat("pesoreal"));
                    inf.setAnticipo(this.buscarAnticipo(rs.getString("planilla")));
                    inf.setFechaCump(rs.getString("feccum"));
                    inf.setNomprop(rs.getString("cnombre"));
                    informes.add(inf);
                    // ////System.out.println("Agrego un elemento");
                }
                ////System.out.println("Se encontraron "+i+" elemento contador");
                ////System.out.println("Se encontraron "+informes.size()+" elemento");
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CREACION DEL INFORME " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
        
    }
    
    public void llenarClientes2(String fechai, String fechaf, String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        clientes=null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("select r.cliente, count(r.cliente),c.nomcli from remesa r , cliente c where r.fecrem between ? and ? and r.base =? and c.codcli = r.cliente group by r.cliente, c.nomcli");
                st.setString(1, fechai);
                st.setString(2, fechaf);
                st.setString(3, base);
                rs = st.executeQuery();
                
                clientes= new Vector();
                while(rs.next()){
                    clientes.add(rs.getString("cliente")+";"+rs.getString("nomcli"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA DE LA LISTA DE CLIENTES  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
        
    }
    public void llenarRutas2(String fechai, String fechaf, String cliente, String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        rutas=null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("select std_job_no,descripcion, count(std_job_no) from remesa where creation_date between ? and ? and cliente = ?  and base = ? group by std_job_no,descripcion");
                st.setString(1, fechai);
                st.setString(2, fechaf);
                st.setString(3, cliente);
                st.setString(4, base);
                
                rs = st.executeQuery();
                
                rutas= new Vector();
                while(rs.next()){
                    rutas.add(rs.getString("descripcion")+","+rs.getString("std_job_no"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA DE LA LISTA DE RUTAS  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
        
    }
    public int buscarTotalEquipo2(String fechai, String fechaf, String cliente, String ruta, String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        int result=0;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("" +
                "SELECT  count( distinct pla.plaveh) as cant " +
                "FROM   PLANILLA pla, " +
                "       PLAREM pr, " +
                "       REMESA rem " +
                "WHERE  pla.feccum BETWEEN ? AND ? " +
                "       and rem.std_job_no = ?" +
                "       and rem.cliente = ? " +
                "       and pr.numrem = rem.numrem " +
                "       and pr.numpla = pla.numpla" +
                "       and pla.reg_status=''" +
                "       and pla.base = ?");
                st.setString(1, fechai);
                st.setString(2, fechaf);
                st.setString(3, ruta);
                st.setString(4, cliente);
                st.setString(5, base);
                ////System.out.println("Sql: "+st.toString());
                rs = st.executeQuery();
                if(rs.next()){
                    result = rs.getInt("cant");
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA CANTIDAD DE EQUIPOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return result;
        
    }
    public void llenarInformeEgeso2(String fechai, String fechaf, String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        inf=null;
        informes=null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("SELECT distinct substr(rem.remision,3,7)as remision," +
                "       pla.plaveh, " +
                "       pla.cedcon, " +
                "       n.nombre as nombre, " +
                "       pla.pesoreal, " +
                "       pr.feccum, " +
                "       pla.fecdsp," +
                "       rem.descripcion, " +
                "       pla.numpla as planilla," +
                "       rem.std_job_no," +
                "       mp.document as egreso " +
                "FROM   PLANILLA pla LEFT OUTER JOIN Nit n ON (n.cedula = pla.cedcon ) ," +
                "       PLAREM pr, " +
                "       REMESA rem," +
                "       MOVPLA mp " +
                "WHERE  pla.FECCUM BETWEEN ? AND ? " +
                "       and pr.numrem = rem.numrem " +
                "       and pr.numpla = pla.numpla " +
                "       and mp.document <>''" +
                "       and mp.planilla = pla.numpla" +
                "       and pla.reg_status='' " +
                "       and pla.base = ?" +
                " order by pr.feccum");
                st.setString(1, fechai);
                st.setString(2, fechaf);
                st.setString(3, base);
                
                ////System.out.println("este es el  query "+st.toString());
                rs = st.executeQuery();
                int i= 0;
                informes= new Vector();
                while(rs.next()){
                    i++;
                    inf = new Informe();
                    inf.setRemision(rs.getString("remision"));
                    inf.setPlaca(rs.getString("plaveh"));
                    inf.setConductor(rs.getString("nombre"));
                    inf.setTonelaje(rs.getFloat("pesoreal"));
                    inf.setAnticipo(this.buscarAnticipo(rs.getString("planilla")));
                    inf.setFechaCump(rs.getString("feccum"));
                    inf.setEgreso(rs.getString("egreso"));
                    inf.setCedula(rs.getString("cedcon"));
                    informes.add(inf);
                    // ////System.out.println("Agrego un elemento");
                }
                ////System.out.println("Se encontraron "+i+" elemento contador");
                ////System.out.println("Se encontraron "+informes.size()+" elemento");
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CREACION DEL INFORME DE EGRESO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
        
    }
    public void llenarInformeProveedores2(String fechai, String fechaf, String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("select n.nombre, pa.codigo_proveedor, pa.nit " +
                "from   proveedor_acpm pa, " +
                "       nit n " +
                "where  pa.reg_status = '' " +
                "       and n.cedula = pa.nit ");
                rs = st.executeQuery();
                proveedores = new Vector();
                while(rs.next()){
                    prov = new Proveedores();
                    prov.setNit(rs.getString("nit"));
                    prov.setNombre(rs.getString("nombre")+" "+rs.getString("codigo_proveedor"));
                    prov.setSucursal(rs.getString("codigo_proveedor"));
                    prov.setCantacpm(0);
                    prov.setCantPeaje(0);
                    prov.setTotale(0);
                    prov.setValorPeaje(0);
                    prov.setValoracpm(0);
                    
                    //AQUI BUSCAMOS LA CANTIDAD TOTAL DE ACPM POR PROVEEDOR
                    st = con.prepareStatement("select sum(pl.acpm_gln)as cantacpm " +
                    "from   plaaux pl " +
                    "where  pl.acpm_supplier =? " +
                    "       and pl.acpm_code =?" +
                    "       and pl.creation_date  BETWEEN ? AND ?" +
                    "       and pl.base =?  ");
                    st.setString(1, prov.getNit());
                    st.setString(2, prov.getSucursal());
                    st.setString(3, fechai);
                    st.setString(4, fechaf);
                    st.setString(5, base);
                    rs1 = st.executeQuery();
                    
                    if(rs1.next()){
                        prov.setCantacpm(rs1.getFloat("cantacpm"));
                    }
                    
                    //AQUI BUSCAMOS EL TOTAL EN PLATA DEL ACPM Q GENERO EL PROVEEDOR
                    st = con.prepareStatement("select sum (vlr) as valoracpm " +
                    "from   movpla " +
                    "where  proveedor_anticipo = ? " +
                    "       and sucursal= ?" +
                    "       and creation_date  BETWEEN ? AND ?" +
                    "       and concept_code = '02' " +
                    "       and base = ?");
                    st.setString(1, prov.getNit());
                    st.setString(2, prov.getSucursal());
                    st.setString(3, fechai);
                    st.setString(4, fechaf);
                    st.setString(5, base);
                    ////System.out.println("Sql  "+st.toString());
                    rs1 = st.executeQuery();
                    if(rs1.next()){
                        prov.setValoracpm(rs1.getFloat("valoracpm"));
                    }
                    
                    //AQUI BUSCAMOS EL TOTAL EN CANTIDAD DE LOS PEAJES
                    st = con.prepareStatement("select sum(ticket_a+ticket_b+ticket_c) as tiquetes" +
                    " from   plaaux  " +
                    " where  creation_date  BETWEEN ? AND ?" +
                    "       and ticket_supplier = ? " +
                    "       and tiket_code = ?" +
                    "       and base =? ");
                    st.setString(1, fechai);
                    st.setString(2, fechaf);
                    st.setString(3, prov.getNit());
                    st.setString(4, prov.getSucursal());
                    st.setString(5, base);
                    rs1 = st.executeQuery();
                    if(rs1.next()){
                        prov.setCantPeaje(rs1.getInt("tiquetes"));
                    }
                    //AQUI BUSCAMOS EL TOTAL EN PESOS DE LOS PEAJES
                    st = con.prepareStatement("select sum (vlr) as peajes " +
                    "from   movpla " +
                    "where  proveedor_anticipo = ? " +
                    "       and sucursal=? " +
                    "       and creation_date  BETWEEN ? AND ? " +
                    "       and concept_code = '03' " +
                    "       and base = ?");
                    st.setString(1, prov.getNit());
                    st.setString(2, prov.getSucursal());
                    st.setString(3, fechai);
                    st.setString(4, fechaf);
                    st.setString(5, base);
                    rs1 = st.executeQuery();
                    if(rs1.next()){
                        prov.setValorPeaje(rs1.getFloat("peajes"));
                    }
                    
                    //AQUI BUSCAMOS EL TOTAL DE EFECTIVO
                    st = con.prepareStatement("select sum (vlr)as efec " +
                    "from   movpla " +
                    "where  proveedor_anticipo = ? " +
                    "       and sucursal=?" +
                    "       and creation_date  BETWEEN ? AND ? " +
                    "       and (concept_code = '01' OR concept_code ='09'" +
                    "       and base =?) ");
                    st.setString(1, prov.getNit());
                    st.setString(2, prov.getSucursal());
                    st.setString(3, fechai);
                    st.setString(4, fechaf);
                    st.setString(5, base);
                    
                    rs1 = st.executeQuery();
                    if(rs1.next()){
                        prov.setTotale(rs1.getFloat("efec"));
                    }
                    
                    proveedores.add(prov);
                    
                }
                
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CREACION DEL INFORME DE PROVEEDORES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
        
    }
    public void reporteViajesPesos(String fech1,String fech2,String base )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("select p.nitpro, " +
                "                	n.nombre,  " +
                "                	p.plaveh,  " +
                "		        pa.full_weight as pesollenoC," +
                "			pa.full_weight_m as pesollenoM," +
                "                	TO_CHAR(p.fecdsp,'YYYYMMDD')as fecha " +
                "                 from 	planilla as p," +
                "			plaaux as pa," +
                "                	nit as n  " +
                "                 where p.plaveh in (select distinct plaveh from planilla where corte between ? and ?) and  " +
                "                	p.nitpro = n.cedula and " +
                "                	p.corte between ? and ? and " +
                "                        p.reg_status = '' and" +
                "			pa.pla = p.numpla" +
                "			and p.base = ?" +
                "                 order by  p.fecdsp");
                st.setString(1,fech1);
                st.setString(2,fech2);
                st.setString(3,fech1);
                st.setString(4,fech2);
                st.setString(5,base);
                ////System.out.println(""+st.toString());
                rs = st.executeQuery();
                reporte = new Vector();
                while(rs.next()){
                    pla = new Planilla();
                    pla.setNitpro(rs.getString("nitpro"));
                    pla.setNomCond(rs.getString("nombre"));
                    pla.setPlaveh(rs.getString("plaveh"));
                    pla.setFecdsp(rs.getString("fecha"));
                    pla.setPesoM(rs.getString("pesollenoM"));
                    pla.setPesoP(rs.getString("pesollenoC"));
                    reporte.add(pla);
                    
                    
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DE REPORTES VIAJES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
    }
    
    //Codigo agregado por Jesu Cuesta
    public void reporteViajes(String fech1,String fech2 )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("select p.nitpro," +
                "	n.nombre, " +
                "	p.plaveh, " +
                "	TO_CHAR(p.fecdsp,'YYYYMMDD')as fecha," +
                "       r.remision" +
                " from 	planilla as p, " +
                "	nit as n," +
                "       remesa r," +
                "       plarem pr " +
                " where p.plaveh in (select distinct plaveh from planilla where corte between ? and ?) and " +
                "	p.nitpro = n.cedula and" +
                "	p.corte between ? and ? and" +
                "       p.reg_status <> 'A'" +
                "       and pr.numpla=p.numpla" +
                "       and r.numrem = pr.numrem" +
                " order by p.plaveh, p.fecdsp");
                st.setString(1,fech1);
                st.setString(2,fech2);
                st.setString(3,fech1);
                st.setString(4,fech2);
                ////System.out.println(""+st.toString());
                rs = st.executeQuery();
                reporte = new Vector();
                while(rs.next()){
                    pla = new Planilla();
                    pla.setNitpro(rs.getString("nitpro"));
                    pla.setNomCond(rs.getString("nombre"));
                    pla.setPlaveh(rs.getString("plaveh"));
                    pla.setFecdsp(rs.getString("fecha"));
                    pla.setRemision(rs.getString("remision"));
                    
                    reporte.add(pla);
                    
                    
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DE REPORTES VIAJES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
    }
    
    /**
     * Getter for property reporte.
     * @return Value of property reporte.
     */
    public java.util.Vector getReporte() {
        return reporte;
    }
    
    //alejo
    public Planilla obtenerPlanillaPorNumero(String numpla) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            Planilla p = null;
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( this.getDatabaseName() );
            if ( con != null ) {
                PreparedStatement ps = con.prepareStatement(SQL_OBTENER_PLANILLA_POR_NUMPLA);
                ps.setString(1,numpla);
                ////System.out.println("obteniendo planilla: "+ps);
                rs = ps.executeQuery();
                if (rs.next()){
                    p = new Planilla();
                    p.setNumpla(rs.getString("numpla"));
                    p.setCedcon(rs.getString("cedcon"));
                    p.setPlaveh(rs.getString("plaveh"));
                    p.setDespachador(rs.getString("despachador"));
                    p.setNitpro(rs.getString("nitpro"));//jose 16.12.05
                }
                return p;
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DE REPORTES VIAJES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return null;
    }
    public String searchPlaTiempo(String codpla, String codtiempo )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaaux = null;
        PoolManager poolManager = null;
        String valor="";
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("select date_time_traffic from planilla_tiempo where pla =? and time_code=?");
                st.setString(1,codpla);
                st.setString(2,codtiempo);
                rs = st.executeQuery();
                
                if(rs.next()){
                    valor=rs.getString("date_time_traffic");
                    //Aqui van PlaAux
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLASAuxiliares " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return valor;
    }
    
    
    private Vector listarPorWhere( String where, Object[] parametros,
    Class[] tiposParametros ) throws
    SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Vector datos = new Vector();
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( this.getDatabaseName() );
            if ( con != null ) {
                st = con.prepareStatement( (SQL_OBTENER_PLANILLAS + " " + where).trim() );
                if (parametros != null ) {
                    for ( int i = 0; i < tiposParametros.length; i++ ) {
                        if ( tiposParametros[0] == String.class ) {
                            st.setString( i + 1, parametros[i].toString() );
                        }
                        else if ( tiposParametros[0] == java.sql.Date.class ) {
                            st.setDate( i + 1, ( java.sql.Date ) parametros[i] );
                        }
                        else {
                            st.setInt( i + 1,
                            ( ( Integer ) parametros[i] ).intValue() );
                        }
                    }
                }
                ////System.out.println(""+st);
                rs = st.executeQuery();
                while ( rs.next() ) {
                    datos.addElement(this.crearPlanilla(rs));
                }
                return datos;
            }
            
        }
        catch ( Exception e ) {
            e.printStackTrace();
            throw new SQLException(
            "ERROR DURANTE EL PROCESO DE LISTAR LOS RECURSOS DISPONIBLES " +
            e.getMessage() + " " /*+ e.getErrorCode()*/ );
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                    e.getMessage() );
                }
            }
            
            if ( con != null ) {
                poolManager.freeConnection( this.getDatabaseName(), con );
            }
            return datos;
        }
    }
    
    
    
    
    public Vector obtenerPlanillas() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            Planilla p = null;
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if ( con != null ) {
                PreparedStatement ps = con.prepareStatement( SQL_OBTENER_PLANILLAS );
                rs = ps.executeQuery();
                Vector v = new Vector();
                while ( rs.next() ) {
                    p = crearPlanilla(rs);
                    v.addElement(p);
                }
                return v;
            }
        }
        catch ( Exception ex ) {
            ex.printStackTrace();
            
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return null;
    }
    
    public Vector obtenerPlanillas(String desde, String hasta) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            Planilla p = null;
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( this.getDatabaseName() );
            if ( con != null ) {
                PreparedStatement ps = con.prepareStatement( SQL_OBTENER_PLANILLAS_ENTRE_FECHAS );
                ps.setString(1,desde);
                ps.setString(2,hasta);
                rs = ps.executeQuery();
                
                Vector v = new Vector();
                while ( rs.next() ) {
                    p = crearPlanilla(rs);
                    v.addElement(p);
                }
                return v;
            }
        }
        catch ( Exception ex ) {
            ex.printStackTrace();
            
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return null;
    }
    
    
    
    private Planilla crearPlanilla( ResultSet rs ) throws SQLException {
        Planilla p = new Planilla();
        p.setAgcpla( rs.getString( "agcpla" ) );
        //p.setBase( rs.getString( "base" ) );
        p.setCedcon( rs.getString( "cedcon" ) );
        p.setCelularcon( rs.getString( "celularcon" ) );
        p.setCia( rs.getString( "cia" ) );
        p.setContenedores( rs.getString( "contenedores" ) );
        p.setCurrency( rs.getString( "currency" ) );
        p.setDespla( rs.getString( "despla" ) );
        p.setFecdsp( rs.getString( "fecdsp" ) );
        p.setFechaposllegada( rs.getDate( "fechaposllegada" ) );
        p.setFecpla( rs.getDate( "fecpla" ) );
        p.setGroup_code( rs.getString( "group_code" ) );
        p.setNitpro( rs.getString( "nitpro" ) );
        p.setNomcon( rs.getString( "nomcon" ) );
        p.setNumpla( rs.getString( "numpla" ) );
        p.setObservacion( rs.getString( "observacion" ) );
        p.setOrden_carga( rs.getString( "orden_carga" ) );
        p.setOrinom( rs.getString( "orinom" ) );
        p.setOripla( rs.getString( "oripla" ) );
        p.setPesoreal( rs.getFloat( "pesoreal" ) );
        p.setPlaveh( rs.getString( "plaveh" ) );
        p.setPrecinto( rs.getString( "precinto" ) );
        p.setPrinter_date( rs.getString( "fechaAux" ) );
        p.setRuta_pla( rs.getString( "ruta_pla" ) );
        p.setTiempoentransito( rs.getFloat( "tiempoentransito" ) );
        p.setTipocont( rs.getString( "tipocont" ) );
        p.setTipoviaje( rs.getString( "tipoviaje" ) );
        p.setUltimoreporte( rs.getString( "ultimoreporte" ) );
        p.setUnidcam( rs.getString( "unidcam" ) );
        p.setUnit_cost( rs.getFloat( "unit_cost" ) );
        p.setUnit_vlr( rs.getString( "unit_vlr" ) );
        p.setVlrpla( rs.getFloat( "vlrpla" ) );
        p.setVlrpla2( rs.getFloat( "Vlrpla2" ) );
        return p;
    }
    
    //PLANEACION
    
    /**
     * Getter for property pl.
     * @return Value of property pl.
     */
    public Planilla getPl() {
        return pl;
    }
    
    /**
     * Setter for property pl.
     * @param pl New value of property pl.
     */
    public void setPl(Planilla pl) {
        this.pl = pl;
    }
    
    /**
     * Getter for property planillas.
     * @return Value of property planillas.
     */
    public java.util.Vector getPlanillas2() {
        return planillas2;
    }
    
    /**
     * Setter for property planillas.
     * @param planillas New value of property planillas.
     */
    public void setPlanillas(java.util.Vector planillas) {
        this.planillas = planillas;
    }
    public void list(String distrito,String fecha_ini, String fecha_final)throws SQLException{
       /* String consulta = "SELECT CIA, NUMPLA, FECPLA, FECDSP, ORIPLA, DESPLA, PLAVEH, PLATLR, FECHAPOSLLEGADA FROM planilla WHERE cia = ? AND fecpla Between ? AND  ? AND stapla <> 'A'";
        String consulta = "SELECT P.CIA, P.NUMPLA, P.FECPLA, P.FECDSP, P.ORIPLA, P.DESPLA, P.PLAVEH, P.PLATLR, P.FECHAPOSLLEGADA  FROM planilla p, tramo t WHERE p.cia = ? AND p.fecpla Between ? And ? AND t.origin = p.oripla"+
                           " AND t.destination = p.despla AND p.stapla <> 'A' ORDER BY P.FECPLA";*/
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(this.SQLPlanilla);
                st.setString(1, distrito);
                st.setString(2, fecha_ini);
                st.setString(3, fecha_final);
                st.setString(4, fecha_ini);
                st.setString(5, fecha_final);
                rs= st.executeQuery();
                ////System.out.println("Consulta planilla " +st);
                planillas2 = new Vector();
                while(rs.next()){
                    pl = new Planilla();
                    pl.setDistrito(rs.getString("cia"));
                    pl.setNumpla(rs.getString("numpla"));
                    pl.setFechapla(rs.getDate("fecpla"));
                    pl.setFechadespacho(rs.getTimestamp("fecdsp"));
                    pl.setOrigenpla(rs.getString("oripla"));
                    pl.setDestinopla(rs.getString("despla"));
                    pl.setPlaveh(rs.getString("plaveh"));
                    pl.setPlatr(rs.getString("platlr"));
                    pl.setFechaPLlegada(rs.getTimestamp("fechaposllegada"));
                    pl.setReg_status(rs.getString("reg_status"));
                    pl.setStd_job_no_rec(rs.getString("STD_JOB_NO"));
                    //////System.out.println("ANTES INSERTAR ");
                    this.registros++;
                    planillas2.add(pl);
                    // ////System.out.println("DESPUES INSERTAR");
                    
                }
                ////System.out.println("Fin del la lista "+planillas2.size());
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DE LISTAR LAS PLANILLAS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
    }
    
    /**
     * Getter for property registros.
     * @return Value of property registros.
     */
    public int getRegistros() {
        return registros;
    }
    
    /**
     * Setter for property registros.
     * @param registros New value of property registros.
     */
    public void setRegistros(int registros) {
        this.registros = registros;
    }
    
    
    
    ////TITO
    /**
     * Obtinen informaci�n de una planilla.
     * @autor Tito Andr�s
     * @param numpla N�mero de la planilla
     */
    public InfoPlanilla obtenerInformacionPlanilla(String numpla) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            InfoPlanilla p = null;
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( this.getDatabaseName() );
            if ( con != null ) {
                PreparedStatement ps = con.prepareStatement(this.SQL_INFORMACION_PLANILLA);
                ps.setString(1,numpla);
                rs = ps.executeQuery();
                if (rs.next()){
                    p = new InfoPlanilla();
                    p.setPlanilla(rs.getString(1));
                    p.setVehiculo(rs.getString(2));
                    p.setFecha(rs.getString(3));
                    p.setCedulaConductor(rs.getString(4));
                    p.setNombreConductor(rs.getString(5));
                    p.setOrigen(rs.getString(6));
                    p.setDestino(rs.getString(7));
                    p.setCliente(rs.getString(8));
                }
                return p;
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DE REPORTES VIAJES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return null;
    }
    
    /**
     * Obtinen el tramo de una planilla
     * @autor Tito Andr�s
     * @param numpla N�mero de la planilla
     */
    public String obtenerInformacionTramo(String numpla) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            String tramo = null;
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( this.getDatabaseName() );
            if ( con != null ) {
                PreparedStatement ps = con.prepareStatement(this.SQL_TRAMO_PLANILLA);
                ps.setString(1,numpla);
                rs = ps.executeQuery();
                if (rs.next()){
                    tramo = rs.getString(1);
                }
                return tramo;
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DE REPORTES VIAJES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return null;
    }
    
    public String obtenerNombreCiudad(String codigo) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( this.getDatabaseName());
            if ( con != null ) {
                PreparedStatement ps = con.prepareStatement( this.SQL_OBTENER_NOMBRE_CIUDAD );
                ps.setString( 1, codigo );
                rs = ps.executeQuery();
                if (rs.next()){
                    return rs.getString(1);
                }
            }
            return "(no encontrada)";
        }
        catch(Exception e){
            throw new SQLException(
            "ERROR DURANTE EL PROCESO DE LISTAR LOS RECURSOS DISPONIBLES " +
            e.getMessage() + " " /*+ e.getErrorCode()*/ );
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                    e.getMessage() );
                }
            }
            
            if ( con != null ) {
                poolManager.freeConnection( this.getDatabaseName(), con );
            }
        }
        
    }
    /**
     *Metodo: listaPlanillas, Metodo que busca un vector de remesas
     *        asociadas a la planilla dada
     *@autor : Ing. Karen Reales
     *@param : Numpla  Numero de la planilla
     *@version : 1.0
     */
    public void listaPlanillas(String numpla )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null,rs2=null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(this.SQL_LISTAR_PLANILLAS);
                st.setString(1,numpla);
                
                rs = st.executeQuery();
                this.plas = new Vector();
                
                while(rs.next()){
                    pla = new Planilla();
                    pla.setNomCond(rs.getString("nombre"));
                    pla.setNumrem(rs.getString("numrem"));
                    pla.setNumpla(rs.getString("numpla"));
                    pla.setPlaveh(rs.getString("plaveh"));
                    pla.setCedcon(rs.getString("cedcon"));
                    pla.setSj_desc(rs.getString("descripcion"));
                    pla.setPesoreal(rs.getFloat("porcent"));
                    pla.setVlrpla(rs.getFloat("vlrrem"));
                    if(rs.getString("currency")!=null){
                        if(!rs.getString("currency").equals("")&&!rs.getString("currency").equals("PES")){
                            //AHORA BUSCO EL VALOR EN PESOS DEL STANDARD
                            st = con.prepareStatement("SELECT (1/vlr_conver) as valor" +
                            "               FROM   TASA" +
                            "               WHERE  FECHA ='NOW()'" +
                            "                      AND MONEDA1 = 'PES'" +
                            "                      AND MONEDA2 = ?");
                            st.setString(1, rs.getString("currency"));
                            rs2 = st.executeQuery();
                            if(rs2.next()){
                                pla.setVlrpla((float)com.tsp.util.Util.redondear(rs.getFloat("vlrrem")*rs2.getDouble("valor"),0));
                            }else{
                                st = con.prepareStatement("SELECT (1/vlr_conver) as valor" +
                                "               FROM   TASA" +
                                "               WHERE   MONEDA1 = 'PES'" +
                                "                      AND MONEDA2 = ?" +
                                "                       order by fecha DESC");
                                st.setString(1, rs.getString("currency"));
                                rs2 = st.executeQuery();
                                if(rs2.next()){
                                    pla.setVlrpla((float)com.tsp.util.Util.redondear(rs.getFloat("vlrrem")*rs2.getDouble("valor"),0));
                                }
                            }
                            
                        }
                    }
                    
                    plas.add(pla);
                    
                    //Aqui van PlaAux
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS POR NUMERO DE PLANILLA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
    }
    
    //moneda uno es la moneda en la que se quiere convertir.
    //moneda dos la moneda a convertir.
    //fecha del cambio
    // valor del monto
    public double buscarValor(String moneda1, String moneda2, String fecha, float valor ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        double valor_cambiado =0;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null){
                st = con.prepareStatement("SELECT (1/vlr_conver) as valor" +
                "               FROM   TASA" +
                "               WHERE  FECHA =?" +
                "                      AND MONEDA1 = ?" +
                "                      AND MONEDA2 = ?");
                st.setString(1, fecha );
                st.setString(2, moneda1 );
                st.setString(3, moneda2 );
                ////System.out.println("Consulta tasa " + st );
                rs = st.executeQuery();
                
                if (rs.next()){
                    valor_cambiado =valor*rs.getDouble("valor");
                }
                else{
                    st = con.prepareStatement(" SELECT (1/vlr_conver) as valor" +
                    "   FROM   TASA" +
                    "   WHERE  MONEDA1 = ?" +
                    "          AND MONEDA2 = ?" +
                    "ORDER BY FECHA DESC") ;
                    st.setString(1, moneda1 );
                    st.setString(2, moneda2 );
                    rs = st.executeQuery();
                    
                    if (rs.next()){
                        valor_cambiado =valor*rs.getDouble("valor");
                    }
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR LA TASA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection(this.getDatabaseName(), con );
            }
        }
        return valor_cambiado;
    }
    
    
    //METODOS PARA CREAR INFORMES DE CARBON.
    
    public void llenarInforme(String fechai, String fechaf,String cliente,String ruta, String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        inf=null;
        informes=null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("SELECT substr(rem.remision,3,7)as remision," +
                "      pla.plaveh, " +
                "      pla.cedcon, " +
                "      n.nombre as nombre, " +
                "      pla.pesoreal, " +
                "      pr.feccum,  " +
                "      pla.fecdsp, " +
                "      rem.descripcion, " +
                "      pla.numpla as planilla, " +
                "      rem.std_job_no " +
                "FROM   PLANILLA pla LEFT OUTER JOIN Nit n ON (n.cedula = pla.cedcon ) ," +
                "       PLAREM pr, " +
                "       REMESA rem " +
                "WHERE  pla.corte like ? " +
                "       and rem.std_job_no = ?" +
                "       and rem.cliente = ? " +
                "       and pr.numrem = rem.numrem " +
                "       and pr.numpla = pla.numpla " +
                "       and pla.reg_status=''" +
                "       and pla.base = ?" +
                "  order by pr.feccum");
                st.setString(1, fechai+"%");
                st.setString(2, ruta);
                st.setString(3, cliente);
                st.setString(4, base);
                ////System.out.println("este es el  query "+st.toString());
                rs = st.executeQuery();
                int i= 0;
                informes= new Vector();
                while(rs.next()){
                    i++;
                    inf = new Informe();
                    inf.setRemision(rs.getString("remision"));
                    inf.setPlaca(rs.getString("plaveh"));
                    inf.setConductor(rs.getString("nombre"));
                    inf.setTonelaje(rs.getFloat("pesoreal"));
                    inf.setAnticipo(this.buscarAnticipo(rs.getString("planilla")));
                    inf.setFechaCump(rs.getString("fecdsp"));
                    informes.add(inf);
                    // ////System.out.println("Agrego un elemento");
                }
                ////System.out.println("Se encontraron "+i+" elemento contador");
                ////System.out.println("Se encontraron "+informes.size()+" elemento");
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CREACION DEL INFORME " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
        
    }
    public void llenarCorte(String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        inf=null;
        informes=null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("SELECT substr(rem.remision,3,7)as remision," +
                "      pla.plaveh, " +
                "      pla.cedcon, " +
                "      coalesce(n.nombre,'NN') as nombre, " +
                "      pla.pesoreal, " +
                "      pr.feccum,  " +
                "      pla.fecdsp, " +
                "      rem.descripcion, " +
                "      pla.numpla as planilla, " +
                "      rem.std_job_no " +
                "FROM   PLANILLA pla LEFT OUTER JOIN Nit n ON (n.cedula = pla.cedcon ) ," +
                "       PLAREM pr, " +
                "       REMESA rem " +
                "WHERE  pla.corte ='0099-01-01 00:00' "+
                "       and pla.base = ?" +
                "       and pla.reg_status=''" +
                "       and pr.numrem = rem.numrem " +
                "       and pr.numpla = pla.numpla " +
                "  order by pla.fecdsp");
                st.setString(1, base);
                ////System.out.println("este es el  query "+st.toString());
                rs = st.executeQuery();
                int i= 0;
                informes= new Vector();
                while(rs.next()){
                    i++;
                    inf = new Informe();
                    inf.setRemision(rs.getString("remision"));
                    inf.setPlaca(rs.getString("plaveh"));
                    inf.setConductor(rs.getString("nombre"));
                    inf.setTonelaje(rs.getFloat("pesoreal"));
                    inf.setAnticipo(this.buscarAnticipo(rs.getString("planilla")));
                    inf.setFechaCump(rs.getString("fecdsp"));
                    inf.setNumpla(rs.getString("planilla"));
                    informes.add(inf);
                    // ////System.out.println("Agrego un elemento");
                }
                ////System.out.println("Se encontraron "+i+" elemento contador");
                ////System.out.println("Se encontraron "+informes.size()+" elemento");
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CREACION DEL INFORME " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
        
    }
    
    public void llenarInformeProveedores(String fechai, String fechaf, String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("select n.nombre, pa.codigo_proveedor, pa.nit " +
                "from   proveedor_acpm pa, " +
                "       nit n " +
                "where  pa.reg_status = '' " +
                "       and n.cedula = pa.nit ");
                rs = st.executeQuery();
                proveedores = new Vector();
                while(rs.next()){
                    prov = new Proveedores();
                    prov.setNit(rs.getString("nit"));
                    prov.setNombre(rs.getString("nombre")+" "+rs.getString("codigo_proveedor"));
                    prov.setSucursal(rs.getString("codigo_proveedor"));
                    prov.setCantacpm(0);
                    prov.setCantPeaje(0);
                    prov.setTotale(0);
                    prov.setValorPeaje(0);
                    prov.setValoracpm(0);
                    
                    //AQUI BUSCAMOS LA CANTIDAD TOTAL DE ACPM POR PROVEEDOR
                    st = con.prepareStatement("select sum(pl.acpm_gln)as cantacpm " +
                    "from   plaaux pl " +
                    "where  pl.acpm_supplier =? " +
                    "       and pl.acpm_code =?" +
                    "       and pl.corte  like ? " +
                    "       and pl.base =?  ");
                    st.setString(1, prov.getNit());
                    st.setString(2, prov.getSucursal());
                    st.setString(3, fechai+"%");
                    st.setString(4, base);
                    rs1 = st.executeQuery();
                    
                    if(rs1.next()){
                        prov.setCantacpm(rs1.getFloat("cantacpm"));
                    }
                    
                    //AQUI BUSCAMOS EL TOTAL EN PLATA DEL ACPM Q GENERO EL PROVEEDOR
                    st = con.prepareStatement("select sum (vlr) as valoracpm " +
                    "from   movpla " +
                    "where  proveedor_anticipo = ? " +
                    "       and sucursal= ?" +
                    "       and corte  like ? " +
                    "       and concept_code = '02' " +
                    "       and base = ?");
                    st.setString(1, prov.getNit());
                    st.setString(2, prov.getSucursal());
                    st.setString(3, fechai+"%");
                    st.setString(4, base);
                    ////System.out.println("Sql  "+st.toString());
                    rs1 = st.executeQuery();
                    if(rs1.next()){
                        prov.setValoracpm(rs1.getFloat("valoracpm"));
                    }
                    
                    //AQUI BUSCAMOS EL TOTAL EN CANTIDAD DE LOS PEAJES
                    st = con.prepareStatement("select sum(ticket_a+ticket_b+ticket_c) as tiquetes" +
                    " from   plaaux  " +
                    " where  corte  like ? " +
                    "       and ticket_supplier = ? " +
                    "       and tiket_code = ?" +
                    "       and base =? ");
                    st.setString(1, fechai+"%");
                    st.setString(2, prov.getNit());
                    st.setString(3, prov.getSucursal());
                    st.setString(4, base);
                    rs1 = st.executeQuery();
                    if(rs1.next()){
                        prov.setCantPeaje(rs1.getInt("tiquetes"));
                    }
                    //AQUI BUSCAMOS EL TOTAL EN PESOS DE LOS PEAJES
                    st = con.prepareStatement("select sum (vlr) as peajes " +
                    "from   movpla " +
                    "where  proveedor_anticipo = ? " +
                    "       and sucursal=? " +
                    "       and corte like ?  " +
                    "       and concept_code = '03' " +
                    "       and base = ?");
                    st.setString(1, prov.getNit());
                    st.setString(2, prov.getSucursal());
                    st.setString(3, fechai+"%");
                    st.setString(4, base);
                    rs1 = st.executeQuery();
                    if(rs1.next()){
                        prov.setValorPeaje(rs1.getFloat("peajes"));
                    }
                    
                    //AQUI BUSCAMOS EL TOTAL DE EFECTIVO
                    st = con.prepareStatement("select sum (vlr)as efec " +
                    "from   movpla " +
                    "where  proveedor_anticipo = ? " +
                    "       and sucursal=?" +
                    "       and corte  like ? " +
                    "       and (concept_code = '01' OR concept_code ='09'" +
                    "       and base =?) ");
                    st.setString(1, prov.getNit());
                    st.setString(2, prov.getSucursal());
                    st.setString(3, fechai+"%");
                    st.setString(4, base);
                    
                    rs1 = st.executeQuery();
                    if(rs1.next()){
                        prov.setTotale(rs1.getFloat("efec"));
                    }
                    
                    proveedores.add(prov);
                    
                }
                
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CREACION DEL INFORME DE PROVEEDORES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
        
    }
    public void llenarInformeEgeso(String fechai, String fechaf, String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        inf=null;
        informes=null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("SELECT distinct substr(rem.remision,3,7)as remision," +
                "       pla.plaveh, " +
                "       pla.cedcon, " +
                "       n.nombre as nombre, " +
                "       pla.pesoreal, " +
                "       pr.feccum, " +
                "       pla.fecdsp," +
                "       rem.descripcion, " +
                "       pla.numpla as planilla," +
                "       rem.std_job_no," +
                "       mp.document as egreso " +
                "FROM   PLANILLA pla LEFT OUTER JOIN Nit n ON (n.cedula = pla.cedcon ) ," +
                "       PLAREM pr, " +
                "       REMESA rem," +
                "       MOVPLA mp " +
                "WHERE  pla.corte like ? " +
                "       and pr.numrem = rem.numrem " +
                "       and pr.numpla = pla.numpla " +
                "       and mp.document <>''" +
                "       and mp.planilla = pla.numpla" +
                "       and pla.reg_status='' " +
                "       and pla.base = ?" +
                " order by pr.feccum");
                st.setString(1, fechai+"%");
                st.setString(2, base);
                
                ////System.out.println("este es el  query "+st.toString());
                rs = st.executeQuery();
                int i= 0;
                informes= new Vector();
                while(rs.next()){
                    i++;
                    inf = new Informe();
                    inf.setRemision(rs.getString("remision"));
                    inf.setPlaca(rs.getString("plaveh"));
                    inf.setConductor(rs.getString("nombre"));
                    inf.setTonelaje(rs.getFloat("pesoreal"));
                    inf.setAnticipo(this.buscarAnticipo(rs.getString("planilla")));
                    inf.setFechaCump(rs.getString("feccum"));
                    inf.setEgreso(rs.getString("egreso"));
                    inf.setCedula(rs.getString("cedcon"));
                    informes.add(inf);
                    // ////System.out.println("Agrego un elemento");
                }
                ////System.out.println("Se encontraron "+i+" elemento contador");
                ////System.out.println("Se encontraron "+informes.size()+" elemento");
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CREACION DEL INFORME DE EGRESO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
        
    }
    
    public void llenarClientes(String fechai, String fechaf, String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        clientes=null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("select 	r.cliente, " +
                "	count(r.cliente)," +
                "	c.nomcli " +
                "from 	remesa r , " +
                "	planilla p, " +
                "	cliente c, " +
                "	plarem pr " +
                "where  pr.numrem = r.numrem" +
                "	and pr.numpla = p.numpla" +
                "	and p.corte like ?  " +
                "	and r.base =? " +
                "	and c.codcli = r.cliente " +
                "group by r.cliente, c.nomcli");
                st.setString(1, fechai+"%");
                st.setString(2, base);
                rs = st.executeQuery();
                
                clientes= new Vector();
                while(rs.next()){
                    clientes.add(rs.getString("cliente")+";"+rs.getString("nomcli"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA DE LA LISTA DE CLIENTES  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
        
    }
    public void llenarRutas(String fechai, String fechaf, String cliente, String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        rutas=null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("select 	r.std_job_no," +
                "	r.descripcion, " +
                "	count(r.std_job_no) " +
                "from 	remesa  r," +
                "	plarem  pr," +
                "	planilla  p " +
                "where 	pr.numrem = r.numrem	" +
                "       and pr.numpla = p.numpla" +
                "	and p.corte like ?  " +
                "	and r.cliente = ?" +
                "	and r.base = ? " +
                "group by std_job_no,descripcion");
                st.setString(1, fechai+"%");
                st.setString(2, cliente);
                st.setString(3, base);
                
                rs = st.executeQuery();
                
                rutas= new Vector();
                while(rs.next()){
                    rutas.add(rs.getString("descripcion")+","+rs.getString("std_job_no"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA DE LA LISTA DE RUTAS  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
        
    }
    
    /**
     * Metodo: buscarPlanillaxNumero, Metodo que verifica la existencia de una planilla
     *          en las tablas planilla e ingreso-trafico, obtiene informacion de la planilla
     *          y obtiene la zona en la q se ecuentra
     * @autor : Ing. Sandra Escalante
     * @param : numero de la planilla
     * @version : 1.0
     */
    public void buscarPlanillaxNumero( String numero ) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        pla = null;
        try{
            ps = crearPreparedStatement("SQL_PLANILLA_X_NUMERO");
            ps.setString(1, numero);
            rs = ps.executeQuery();
            if (rs.next()){
                pla = new Planilla();
                pla.setNumpla(rs.getString("planilla"));
                pla.setNomori(rs.getString("nomorigen"));
                pla.setNomdest(rs.getString("nomdestino"));
                pla.setPlaveh(rs.getString("placa"));
                pla.setNomCond(rs.getString("nomcond"));
                pla.setNomzona((rs.getString("nomzona") != null)?rs.getString("nomzona"):"No Registra");
                pla.setZona( (rs.getString("zona") != null)?rs.getString("zona"):"NR");
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL BUSCAR LA PLANILLA EN INGRESO TRAFICO, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_PLANILLA_X_NUMERO");
        }
    }
    
    //METODO QUE PERMITE ACTUALIZAR LAS TABLAS DE INGRESO_TRAFICO Y TRAFICO EN EL MOMENTO EN QUE SE
    //MODIFICA UNA PLANILLA.
    public void updateTrafico(String numpla) throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        pla = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null){
                
                st = con.prepareStatement("update Ingreso_trafico " +
                "   set placa = p.plaveh," +
                "       cedcon = p.cedcon," +
                "       nomcond= p.nomcond," +
                "       cedprop= p.nitpro," +
                "       nomprop= p.nomprop," +
                "       last_update = 'now()'" +
                "  from (SELECT  numpla," +
                "		plaveh," +
                "		cedcon," +
                "		c.nombre as nomcond," +
                "		nitpro," +
                "		coalesce(p.nombre,'No se encontro') as nomprop" +
                "	from planilla   left outer join nit c on planilla.cedcon = c.cedula " +
                "			left outer join nit p on planilla.plaveh = p.cedula" +
                "				where planilla.numpla = ?) AS p" +
                " where planilla= p.numpla");
                st.setString(1, numpla);
                st.executeUpdate();
                
                st = con.prepareStatement("update trafico " +
                "   set placa = p.plaveh," +
                "       cedcon = p.cedcon," +
                "       cedprop= p.nitpro," +
                "       last_update = 'now()'" +
                "  from (SELECT  numpla," +
                "		plaveh," +
                "		cedcon," +
                "		nitpro" +
                "	from planilla  " +
                "	where planilla.numpla = ?) AS p" +
                " where planilla = p.numpla");
                st.setString(1, numpla);
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL ACTUALIZAR EL REGISTRO EN INGRESO TRAFICO Y TRAFICO" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection(this.getDatabaseName(), con );
            }
        }
    }
    /**
     *Metodo que anula un registro en ingreso_trafico
     *@autor: Karen Reales
     *@param: Numero de la planilla
     *@return: Comando SQL
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void anulaTrafico(String numpla) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        pla = null;
        
        try{
            
            st = this.crearPreparedStatement("SQL_DELETETRAFICO");
            st.setString(1, numpla);
            st.executeUpdate();
            
              /*  st = con.prepareStatement("update trafico " +
                "   set reg_status ='A'" +
                " where planilla =?");
                st.setString(1, numpla);
                st.executeUpdate();*/
            
        }catch(SQLException e){
            throw new SQLException("ERROR AL ANULAR EL REGISTRO EN INGRESO TRAFICO Y TRAFICO" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_DELETETRAFICO");
        }
    }
    
    ///
    public void getInfoPlanilla(String numpla )throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        pla = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("select pla.numpla as planilla,  " +
                "                       coalesce (ciu.nomciu,'') as origen,  " +
                "                       coalesce (ciu2.nomciu,'') as destino,      " +
                "                       pla.plaveh as placa,       " +
                "                       pla.cedcon as cedulaCond, " +
                "               	coalesce(nit.nombre,'') as conductor,  " +
                "                	pla.fecdsp as fchdesp, " +
                "                	case when pla.platlr = '' then 'NA'" +
                "                       else pla.platlr " +
                "                       end as trailer, " +
                "                	case when pla.tipotrailer = '' then 'NA'" +
                "			     when pla.tipotrailer is null then 'NA'" +
                "			else pla.tipotrailer     " +
                "			end as trailerprop, " +
                "                	pla.contenedores as contendores, " +
                "                	case when pla.tipocont = '' then 'NA'" +
                "                             when pla.tipocont is null then 'NA'   " +
                "			else pla.tipocont" +
                "			end as contprop " +
                "                     from  planilla as pla   " +
                "                     LEFT OUTER JOIN nit  on (pla.cedcon=nit.cedula )  " +
                "                     LEFT OUTER JOIN ciudad as ciu on (ciu.codciu=pla.oripla)  " +
                "                     LEFT OUTER JOIN ciudad as ciu2 on (ciu2.codciu=pla.despla) " +
                "                     where  pla.numpla=? ");
                st.setString(1,numpla);
                rs = st.executeQuery();
                while(rs.next()){
                    pla = new Planilla();
                    pla.setNumpla(rs.getString("planilla"));
                    pla.setNomori(rs.getString("origen"));
                    pla.setNomdest(rs.getString("destino"));
                    pla.setFecdsp(rs.getString("fchdesp"));
                    pla.setPlaveh(rs.getString("placa"));
                    pla.setCedcon(rs.getString("cedulaCond"));
                    pla.setNomCond(rs.getString("conductor"));
                    pla.setPlatlr(rs.getString("trailer"));
                    pla.setTipotrailer(rs.getString("trailerprop"));
                    pla.setContenedores(rs.getString("contendores"));
                    pla.setTipocont(rs.getString("contprop"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA PLANILLA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
    }
    
    ///sandrameg 280905 Confirmacion trailer
    ///Metodo q modifica los campos platlr, tipotrailer, contendores, tipocont de un planilla
    public void confirmacionTrailer(Planilla p) throws SQLException{
        Connection con = null;
        PoolManager poolManager = null;
        PreparedStatement st = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            
            if (con != null){
                st = con.prepareStatement("update planilla set platlr = ?, tipotrailer=?, contenedores=?, tipocont=? where numpla = ?");
                st.setString(1, p.getPlatlr());
                st.setString(2, p.getTipotrailer());
                st.setString(3, p.getContenedores());
                st.setString(4, p.getTipocont());
                st.setString(5, p.getNumpla());
                ////System.out.println("ST " + st);
                st.executeUpdate();
            }
        }catch ( SQLException e){
            throw new SQLException(e.getMessage());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection(this.getDatabaseName(), con );
            }
        }
    }
    
    /**
     *  Obtiene el nombre del propietario de una planilla
     *  @autor Tito Andr�s 27.09.2005
     *  @param numpla N�mero de la planilla
     *  @throws SQLException
     */
    public String obtenerNombrePropietario(String numpla) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String nombre = "";
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( this.getDatabaseName());
            if ( con != null ) {
                PreparedStatement ps = con.prepareStatement( this.SQL_GET_NITPROPIETARIO );
                ps.setString( 1, numpla );
                rs = ps.executeQuery();
                if (rs.next()){
                    return rs.getString(1);
                }
            }
            
        }
        catch(Exception e){
            throw new SQLException(
            "ERROR DURANTE EL PROCESO DE OBTENER EL NOMBRE DEL PROPIETARIO DE UNA PLANILLA " +
            e.getMessage() + " " /*+ e.getErrorCode()*/ );
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                    e.getMessage() );
                }
            }
            
            if ( con != null ) {
                poolManager.freeConnection( this.getDatabaseName(), con );
            }
        }
        
        return nombre;
        
    }
    
    
    public String getPlacaPlanilla(String planilla) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String resp="";
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("select plaveh from planilla where numpla = ?");
                st.setString(1, planilla);
                ////System.out.println(st);
                rs = st.executeQuery();
                while(rs.next()){
                    resp = rs.getString("plaveh");
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA PLANILLA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return resp;
    }
    
    public void consultaPlanillaUsuario(String usuario, String placa, String fecini, String fecfin, String oripla, String despla, String agenpla, String conductor) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        reporte=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SQL_PLANILLA_USUARIO);
                st.setString(1, usuario);
                st.setString(2, placa+"%");
                st.setString(3, fecini+"%");
                st.setString(4, fecfin+"%");
                st.setString(5, oripla+"%");
                st.setString(6, despla+"%");
                st.setString(7, agenpla+"%");
                st.setString(8, conductor+"%");
                rs = st.executeQuery();
                reporte = new Vector();
                while(rs.next()){
                    pla = new Planilla();
                    pla.setNumpla(rs.getString("numpla"));
                    pla.setFecdsp(rs.getString("fecpla"));
                    pla.setOripla(rs.getString("oripla"));
                    pla.setDespla(rs.getString("despla"));
                    pla.setNumrem(rs.getString("numrem"));
                    pla.setNomCond(rs.getString("nombre"));
                    pla.setReg_status(rs.getString("estado_despacho"));
                    pla.setNomori(rs.getString("orirem"));
                    pla.setNomdest(rs.getString("desrem"));
                    pla.setStatus_220(rs.getString("estado_remesa"));
                    pla.setAgcpla(rs.getString("agcpla"));
                    pla.setNitpro(rs.getString("cliente"));
                    reporte.add(pla);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLACAS Y LOS CONDUCTORES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
    }
    
    ///jose 111005
    public List reportePlanillasXLS() throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        reporte=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SQL_PLANILLA_XLS);
                rs = st.executeQuery();
                planillas = new LinkedList();
                while(rs.next()){
                    pla = new Planilla();
                    pla.setNumpla(rs.getString("numpla"));//numero planilla
                    pla.setFecdsp(rs.getString("fecpla"));//fecha planilla
                    pla.setOripla(rs.getString("oripla"));//origen despacho
                    pla.setDespla(rs.getString("despla"));//destino despacho
                    pla.setNomCond(rs.getString("conductor"));//conductor
                    pla.setAgcpla(rs.getString("agencia"));//agencia despacho
                    pla.setPlaveh(rs.getString("placa"));//placa vehiculo
                    pla.setUltimoreporte(rs.getString("diferencia"));//diferencia de dias
                    planillas.add(pla);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS REPORTES DE PLANILLA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return planillas;
    }
    
    //jose 03-02-2006
    public void consultaPlanillaRemesa(String planilla) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        reporte=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SQL_PLANILLA_REMESA);
                st.setString(1, planilla);
                rs = st.executeQuery();
                reporte = new Vector();
                while(rs.next()){
                    pla = new Planilla();
                    pla.setNumpla(rs.getString("numpla"));
                    pla.setNumrem(rs.getString("numrem"));
                    pla.setNomori(rs.getString("orirem"));
                    pla.setFecdsp(rs.getString("fecrem"));
                    pla.setNomdest(rs.getString("desrem"));
                    pla.setNitpro(rs.getString("nomcli"));
                    pla.setClientes(rs.getString("codcli"));
                    reporte.add(pla);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
    }
    
    public void consultaPlanillaRemesaDiscrepancia(String planilla, String remesa, String tipo_doc, String documento, int num_discre ) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        reporte=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SQL_PLANILLA_REMESA_DISCREPANCIA);
                st.setString(1, planilla);
                st.setString(2, remesa);
                st.setString(3, tipo_doc);
                st.setString(4, documento);
                st.setInt(5, num_discre);
                rs = st.executeQuery();
                reporte = new Vector();
                while(rs.next()){
                    pla = new Planilla();
                    pla.setNumpla(rs.getString("numpla"));
                    pla.setNumrem(rs.getString("numrem"));
                    pla.setNomori(rs.getString("orirem"));
                    pla.setFecdsp(rs.getString("fecrem"));
                    pla.setNomdest(rs.getString("desrem"));
                    pla.setNitpro(rs.getString("nomcli"));
                    reporte.add(pla);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
    }
    
    //Tito Andres 31.10.2005
    public Vector obtenerInformeFacturacion(String fechai, String fechaf, String ruta) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        InformeFact ifact = null;
        RemisionIFact remfact = null;
        Vector informe = new Vector();
        Vector lineas = new Vector();
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(this.SQL_INFORME_FACTURACION);
                st.setString(1, fechai);
                st.setString(2, fechaf);
                st.setString(3, ruta);
                ////System.out.println("----------------------------------> consulta: " + st.toString());
                
                rs = st.executeQuery();
                
                while(rs.next()){
                    remfact = new RemisionIFact();
                    remfact.setConductor(rs.getString("nomcond"));
                    remfact.setFecha(rs.getString("feccum"));
                    remfact.setPlaca(rs.getString("plaveh"));
                    remfact.setRemision(rs.getString("remision"));
                    remfact.setTonelaje(Double.valueOf(rs.getString("pesoreal")));
                    remfact.setStd_job_no(rs.getString("std_job_no"));
                    
                    lineas.add(remfact);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA GENERACION DEL INFORME DE FACTURACION " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
        informe = new Vector();
        for( int i=0; i<lineas.size(); i++){
            ifact = new InformeFact();
            remfact = new RemisionIFact();
            remfact = (RemisionIFact) lineas.elementAt(i);
            
            ifact.setStd_job(remfact.getStd_job_no());
            Vector remisiones = new Vector();
            //////System.out.println("-------------------------- " + ifact.getStd_job() + "---------------------------");
            //////System.out.println("----------------> remision:" + remfact.getRemision() + " fecha: " + remfact.getFecha());
            for( int j=i; j<lineas.size(); j++ ){
                
                RemisionIFact aux = new RemisionIFact();
                aux = (RemisionIFact) lineas.elementAt(j);
                
                if( aux.getStd_job_no().matches(remfact.getStd_job_no()) ){
                    remisiones.add(aux);
                    //////System.out.println("*********> entro: " + aux.getRemision());
                    //////System.out.println("----------------> entro remision:" + aux.getRemision() + " fecha: " + aux.getFecha());
                    i = j;
                }
            }
            
            ifact.setRemisiones(remisiones);
            informe.add(ifact);
        }
        
        return informe;
    }
    
    //Tito Andres 02.12.2005
    public Vector obtenerInformeFacturacion127012(String fechai, String fechaf) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        InformeFact ifact = null;
        RemisionIFact remfact = null;
        Vector informe = new Vector();
        Vector lineas = new Vector();
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(this.SQL_INFORME_FACTURACION_127012);
                st.setString(1, fechai);
                st.setString(2, fechaf);
                ////System.out.println("----------------------------------> consulta 127012: " + st.toString());
                
                rs = st.executeQuery();
                
                while(rs.next()){
                    remfact = new RemisionIFact();
                    remfact = new RemisionIFact();
                    remfact.setConductor(rs.getString("nomcond"));
                    remfact.setFecha(rs.getString("feccum"));
                    remfact.setPlaca(rs.getString("plaveh"));
                    remfact.setRemision(rs.getString("remision"));
                    remfact.setTonelaje(Double.valueOf(rs.getString("pesoreal")));
                    remfact.setStd_job_no(rs.getString("std_job_no"));
                    
                    lineas.add(remfact);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA GENERACION DEL INFORME DE FACTURACION 127012 " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
        informe = new Vector();
        for( int i=0; i<lineas.size(); i++){
            ifact = new InformeFact();
            remfact = new RemisionIFact();
            remfact = (RemisionIFact) lineas.elementAt(i);
            
            ifact.setStd_job(remfact.getStd_job_no());
            Vector remisiones = new Vector();
            //////System.out.println("-------------------------- " + ifact.getStd_job() + "---------------------------");
            //////System.out.println("----------------> remision:" + remfact.getRemision() + " fecha: " + remfact.getFecha());
            for( int j=i; j<lineas.size(); j++ ){
                
                RemisionIFact aux = new RemisionIFact();
                aux = (RemisionIFact) lineas.elementAt(j);
                
                if( aux.getStd_job_no().matches(remfact.getStd_job_no()) ){
                    remisiones.add(aux);
                    //////System.out.println("*********> entro: " + aux.getRemision());
                    //////System.out.println("----------------> entro remision:" + aux.getRemision() + " fecha: " + aux.getFecha());
                    i = j;
                }
            }
            
            ifact.setRemisiones(remisiones);
            informe.add(ifact);
        }
        
        return informe;
    }
    
    //Tito Andres 04.11.2005
    /**
     * Genera el informe de anticipos
     * @autor Tito Andr�s Maturana
     * @param fechai Fecha inicial del reporte.
     * @param fechaf Fecha final del reporte.
     * @throws SQLException
     * @version 1.0
     */
    public Vector obtenerInformeAnticipos(String fechai, String fechaf) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        InformeFact ifact = null;
        ReporteAnticipos anticip = null;
        Vector informe = new Vector();
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(this.SQL_REPORTE_ANTICIPOS);
                st.setString(1, fechai);
                st.setString(2, fechaf);
                st.setString(3, fechai);
                st.setString(4, fechaf);
                st.setString(5, fechai);
                st.setString(6, fechaf);
                ////System.out.println("----------------------------------> consulta: " + st.toString());
                
                rs = st.executeQuery();
                
                while(rs.next()){
                    anticip = new ReporteAnticipos();
                    anticip.Load(rs);
                    //////System.out.println("�������������������������������������> remisi�n: " + anticip.getRemision());
                    informe.add(anticip);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA GENERACION DEL REPORTE DE ANTICIPOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
        return informe;
    }
    //Juan 15.11.05
    public double ValorTotalFlete(String planilla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        double valor = 0;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null){
                st = con.prepareStatement(" SELECT SUM(VLR)AS VALORTOTAL            "+
                " FROM MOVPLA MV, CODEXTRAFLETE CE        "+
                " WHERE                                   "+
                " MV.PLANILLA = ?                         "+
                " AND MV.CONCEPT_CODE = CE.CODEXTRAFLETE  "+
                " GROUP BY MV.PLANILLA                    ");
                st.setString(1, planilla );
                ////System.out.println("Consulta valor flete " + st );
                rs = st.executeQuery();
                
                if (rs.next()){
                    valor = rs.getDouble("VALORTOTAL");
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR EL VALOR" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection(this.getDatabaseName(), con );
            }
        }
        return valor;
    }
    
    
    public String TotalFlete(String planilla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String valor = "";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null){
                st = con.prepareStatement(" SELECT CE.DESCRIPCION, MV.CONCEPT_CODE, MV.VLR FROM MOVPLA MV, CODEXTRAFLETE CE WHERE PLANILLA = ?   "+
                " AND MV.CONCEPT_CODE = CE.CODEXTRAFLETE ");
                st.setString(1, planilla );
                ////System.out.println("Valor flete " + st );
                rs = st.executeQuery();
                
                while(rs.next()){
                    valor += rs.getString("DESCRIPCION")+" = "+ rs.getString("VLR")+";";
                }
                if( !valor.equals("") )
                    valor = valor.substring(0,valor.length()-1);
                ////System.out.println("VALOR  " + valor );
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR EL VALOR" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection(this.getDatabaseName(), con );
            }
        }
        return valor;
    }
    
    public double ValorTotalIngreso(String planilla, float vlr, String fechaplanilla, String moneda ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        double valor = 0;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null){
                st = con.prepareStatement(this.valortotalingreso);
                st.setString(1, planilla );
                rs = st.executeQuery();
                
                while(rs.next()){
                    //Si CLASE_VALOR = V se toma el mismo valor de la Tabla  SJ_EXTRAFLETE
                    if( rs.getString("CLASE_VALOR").equals("V")){
                        valor += rs.getDouble("VLR_INGRESO");
                    }
                    //Si VF_INGRESO = P se calcula el valor del porcentaje del extraflete en el valor de la Remesa
                    if( rs.getString("CLASE_VALOR").equals("P")){
                        if( moneda.equals(rs.getString("MONEDA_INGRESO"))){
                            valor += (vlr*rs.getDouble("PORCENTAJE"))/100;
                        }
                        else{
                            double vaux = this.buscarValor(rs.getString("MONEDA_INGRESO"), moneda, fechaplanilla, vlr );
                            valor += (vaux*rs.getDouble("PORCENTAJE")/100);
                        }
                        
                    }
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR EL VALOR" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection(this.getDatabaseName(), con );
            }
        }
        return valor;
    }
    
    public String TotalIngreso(String planilla, float vlr, String fechaplanilla, String moneda) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String valor = "";
        double vl = 0;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null){
                st = con.prepareStatement(this.totalingreso);
                st.setString(1, planilla );
                ////System.out.println("TOTAL INGRESO " + st );
                rs = st.executeQuery();
                
                while(rs.next()){
                    if( rs.getString("CLASE_VALOR").equals("P")){
                        if( moneda.equals(rs.getString("MONEDA_INGRESO"))){
                            vl = (vlr*rs.getDouble("PORCENTAJE"))/100;
                        }
                        else{
                            double vaux = this.buscarValor(rs.getString("MONEDA_INGRESO"), moneda, fechaplanilla, vlr );
                            vl += (vaux*rs.getDouble("PORCENTAJE")/100);
                        }
                        valor += rs.getString("DESCRIPCION")+" = "+ com.tsp.util.Util.customFormat(vl)+";";
                    }
                    else
                        valor += rs.getString("DESCRIPCION")+" = "+ com.tsp.util.Util.customFormat(rs.getDouble("VLR_INGRESO"))+";";
                }
                if( !valor.equals("") )
                    valor = valor.substring(0,valor.length()-1);
                ////System.out.println("VALOR  " + valor );
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR EL VALOR" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection(this.getDatabaseName(), con );
            }
        }
        return valor;
    }
    
    /*JuanM 30-11-05*/
    public String Documentos(String numrem) throws SQLException {
        PreparedStatement st          = null;
        ResultSet rs = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection(this.getDatabaseName());
        String documentos="";
        String resultado = "";
        
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQLDOCUMENTOS);
            st.setString(1, numrem);
            rs = st.executeQuery();
            while(rs.next()){
                if(!rs.getString("DOCUMENTO_REL").equals(""))
                    documentos += rs.getString("DOCUMENTO")+"/"+rs.getString("DOCUMENTO_REL")+";";
                else
                    documentos += rs.getString("DOCUMENTO")+";";
                
            }
            if(documentos.length()>0){
                documentos = documentos.substring(0,documentos.length()-1);
                for( int i = 0, pi = 0;i < documentos.length();i++){
                    if( ( (i+1) % 10 == 0 && i != 0) || (i == documentos.length()-1 )){
                        resultado += "\n"+documentos.substring(pi,i + 1);
                        pi = i + 1;
                    }
                }
            }
            
        }catch(SQLException e) {
            throw new SQLException("Error en rutina Documentos [PlanillaDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection(this.getDatabaseName(),conPostgres);
        }
        return resultado;
    }
    /*JuanM 30-11-05*/
    public String Document(String numrem) throws SQLException {
        PreparedStatement st          = null;
        ResultSet rs = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection(this.getDatabaseName());
        String documentos="";
        
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQLDOCUMENTOS);
            st.setString(1, numrem);
            rs = st.executeQuery();
            while(rs.next()){
                if(!rs.getString("DOCUMENTO_REL").equals(""))
                    documentos += rs.getString("DOCUMENTO")+"/"+rs.getString("DOCUMENTO_REL")+";";
                else
                    documentos += rs.getString("DOCUMENTO")+";";
                
            }
            if(documentos.length()>0){
                documentos = documentos.substring(0,documentos.length()-1);
            }
            
        }catch(SQLException e) {
            throw new SQLException("Error en rutina Documentos [PlanillaDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection(this.getDatabaseName(),conPostgres);
        }
        return documentos;
    }
    
    /**
     * Metodo: reporteLufkinXLS, permite listar un reporte de produccion de flota por trayler
     * @autor : Ing. Jose de la rosa
     * @param : fecha inicial y fecha final
     * @version : 1.0
     */
    public List reporteLufkinXLS(String fini, String ffin) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        planillas=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SQL_LUFKIN_XLS);
                st.setString(1, fini+" 07:00" );
                st.setString(2, ffin+" 06:59" );
                rs = st.executeQuery();
                planillas = new LinkedList();
                while(rs.next()){
                    pla = new Planilla();
                    pla.setPlaveh(rs.getString("plaveh"));//placa del vehiculo
                    pla.setNitpro(rs.getString("nitpro"));//origen despacho
                    pla.setNomCond(rs.getString("nombre"));//nombre conductor
                    pla.setPlatlr(rs.getString("platlr"));//trayley
                    planillas.add(pla);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS REPORTES DE PRODUCCION FLOTA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return planillas;
    }
    
    /**
     * Metodo: reporteProdFlotaXLS, permite listar un reporte de produccion de flota
     * @autor : Ing. Jose de la rosa
     * @param : fecha inicial y fecha final
     * @version : 1.0
     */
    public List reporteProdFlotaPlacasXLS(String fini, String ffin) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        planillas=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SQL_PROD_FLOTA_XLS);
                st.setString(1, fini+" 07:00" );
                st.setString(2, ffin+" 06:59" );
                rs = st.executeQuery();
                planillas = new LinkedList();
                while(rs.next()){
                    pla = new Planilla();
                    pla.setPlaveh(rs.getString("plaveh"));//placa del vehiculo
                    pla.setNitpro(rs.getString("nitpro"));//origen despacho
                    pla.setNomCond(rs.getString("nombre"));//nombre conductor
                    pla.setPlatlr(rs.getString("platlr"));
                    planillas.add(pla);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS REPORTES DE PRODUCCION FLOTA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return planillas;
    }
    /**
     * Metodo: reporteProdFlotaXLS, permite listar un reporte de produccion de flota
     * @autor : Ing. Jose de la rosa
     * @param : fecha inicial, fecha final, placa y un nit del proveedor
     * @version : 1.0
     */
    public List reporteProdFlotaPlacasDiasXLS(String fini, String ffin, String placa, String nit) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        planillas=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SQL_PROD_FLOTA_PLACA_XLS);
                st.setString(1, fini+" 07:00" );
                st.setString(2, ffin+" 06:59" );
                st.setString(3, placa );
                st.setString(4, nit );
                rs = st.executeQuery();
                planillas = new LinkedList();
                while(rs.next()){
                    pla = new Planilla();
                    pla.setFeccum(rs.getString("dia"));//dia
                    pla.setDias(rs.getInt("count"));//numero de dias
                    pla.setPesoreal(rs.getFloat("sum"));
                    planillas.add(pla);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS REPORTES DE PRODUCCION FLOTA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return planillas;
    }
    
    /**
     * Metodo: reporteProdStandarJobXLS, permite listar los detalles de los standar job
     * @autor : Ing. Jose de la rosa
     * @param : fecha inicial y fecha final
     * @version : 1.0
     */
    public List reporteProdStandarJobXLS() throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        planillas=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SQL_PROD_SJ_XLS);
                rs = st.executeQuery();
                planillas = new LinkedList();
                while(rs.next()){
                    pla = new Planilla();
                    pla.setSj(rs.getString("std_job_no"));//standar job
                    pla.setStatus_220(rs.getString("descripcion"));//nombre Standar Job
                    planillas.add(pla);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS REPORTES DE PRODUCCION FLOTA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return planillas;
    }
    
    
    
    /**
     * Metodo: nombrePlacaTrayler, retorna el trayler de la placa dada
     * @autor : Ing. Jose de la rosa
     * @param : fecha inicial, fecha final, placa y un nit del proveedor
     * @version : 1.0
     */
    public String nombrePlacaTrayler(String fini, String ffin, String placa, String nit) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String nombre="";
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SQL_PLACA_TRAYLER_XLS);
                st.setString(1, fini+" 07:00" );
                st.setString(2, ffin+" 06:59" );
                st.setString(3, placa );
                st.setString(4, nit );
                rs = st.executeQuery();
                while(rs.next()){
                    nombre = rs.getString("platlr");//trayler
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS TRAYLER DE LAS PLACAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return nombre;
    }
    
    /**
     * Metodo: reporteProdFlotaPlacasStandarXLS, permite listar un reporte de produccion de flota
     * @autor : Ing. Jose de la rosa
     * @param : fecha inicial y fecha final
     * @version : 1.0
     */
    public List reporteProdFlotaPlacasStandarXLS(String fini, String ffin, String standar) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        planillas=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SQL_PROD_FLOTA_STANDAR_XLS);
                st.setString(1, fini+" 07:00" );
                st.setString(2, ffin+" 06:59" );
                st.setString(3, standar);
                rs = st.executeQuery();
                planillas = new LinkedList();
                while(rs.next()){
                    pla = new Planilla();
                    pla.setPlaveh(rs.getString("plaveh"));//placa del vehiculo
                    pla.setNitpro(rs.getString("nitpro"));//origen despacho
                    pla.setNomCond(rs.getString("nombre"));//nombre conductor
                    pla.setPlatlr(rs.getString("platlr"));
                    planillas.add(pla);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS REPORTES DE PRODUCCION FLOTA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return planillas;
    }
    
    /**
     * Metodo: reporteFlotaIntermediaXLS, permite listar un reporte de produccion de flota
     * @autor : Ing. Jose de la rosa
     * @param : fecha inicial y fecha final
     * @version : 1.0
     */
    public List reporteFlotaIntermediaXLS(String fini, String ffin) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        planillas=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SQL_FLOTA_INTERMEDIA_XLS);
                st.setString(1, fini+" 07:00" );
                st.setString(2, ffin+" 06:59" );
                rs = st.executeQuery();
                planillas = new LinkedList();
                while(rs.next()){
                    pla = new Planilla();
                    pla.setPlaveh(rs.getString("plaveh"));//placa del vehiculo
                    pla.setNitpro(rs.getString("nitpro"));//origen despacho
                    pla.setNomCond(rs.getString("nombre"));//nombre conductor
                    pla.setPlatlr(rs.getString("platlr"));
                    planillas.add(pla);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS REPORTES DE PRODUCCION FLOTA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return planillas;
    }
    
     /* Metodo: reporteFlotaDirectaXLS, permite listar un reporte de produccion de flota directa
      * @autor : Ing. Jose de la rosa
      * @param : fecha inicial y fecha final
      * @version : 1.0
      */
    public List reporteFlotaDirectaXLS(String fini, String ffin) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        planillas=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SQL_FLOTA_DIRECTA_XLS);
                st.setString(1, fini+" 07:00" );
                st.setString(2, ffin+" 06:59" );
                rs = st.executeQuery();
                planillas = new LinkedList();
                while(rs.next()){
                    pla = new Planilla();
                    pla.setPlaveh(rs.getString("plaveh"));//placa del vehiculo
                    pla.setNitpro(rs.getString("nitpro"));//origen despacho
                    pla.setNomCond(rs.getString("nombre"));//nombre conductor
                    pla.setPlatlr(rs.getString("platlr"));
                    planillas.add(pla);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS REPORTES DE PRODUCCION FLOTA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return planillas;
    }
    
    /**
     * Genera el resumen del informe de anticipos
     * @autor Tito Andr�s Maturana
     * @param fechai Fecha inicial del reporte.
     * @param fechaf Fecha final del reporte.
     * @throws SQLException
     * @version 1.0
     */
    public Vector obtenerResumenInformeAnticipos(String fechai, String fechaf) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        InformeFact ifact = null;
        Vector informe = new Vector();
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(this.SQL_REPORTE_ANTICIPOS_RESUMEN);
                st.setString(1, fechai);
                st.setString(2, fechaf);
                st.setString(3, fechai);
                st.setString(4, fechaf);
                st.setString(5, fechai);
                st.setString(6, fechaf);
                //////System.out.println("----------------------------------> consulta: " + st.toString());
                
                rs = st.executeQuery();
                
                while(rs.next()){
                    Vector linea = new Vector();
                    //linea.add(rs.getString("puerto")); //ricardo
                    linea.add(rs.getString("proveedor"));
                    linea.add(rs.getString("galones"));
                    linea.add(rs.getString("valor"));
                    informe.add(linea);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA GENERACION DEL REPORTE DE ANTICIPOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
        return informe;
    }
    
    /**JEscandon 13.12.05*/
    /**
     * Genera Lista de recursos disponibles con ENTREGA ENTRE HOY  Y HOY - 2 DIAS
     * @autor Ing. Juan Manuel Escandon Perez
     * @param Distrito, fecha inicial, fecha final
     * @throws SQLException
     * @version 1.0
     */
    public void list1(String distrito,String fecha_ini, String fecha_final)throws SQLException{
       /* String consulta = "SELECT CIA, NUMPLA, FECPLA, FECDSP, ORIPLA, DESPLA, PLAVEH, PLATLR, FECHAPOSLLEGADA FROM planilla WHERE cia = ? AND fecpla Between ? AND  ? AND stapla <> 'A'";
        String consulta = "SELECT P.CIA, P.NUMPLA, P.FECPLA, P.FECDSP, P.ORIPLA, P.DESPLA, P.PLAVEH, P.PLATLR, P.FECHAPOSLLEGADA  FROM planilla p, tramo t WHERE p.cia = ? AND p.fecpla Between ? And ? AND t.origin = p.oripla"+
                           " AND t.destination = p.despla AND p.stapla <> 'A' ORDER BY P.FECPLA";*/
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(this.SQLRecursos1);
                st.setString(1, distrito);
                st.setString(2, fecha_ini);
                st.setString(3, fecha_final);
                st.setString(4, fecha_ini);
                st.setString(5, fecha_final);
                rs= st.executeQuery();
                ////System.out.println("Consulta planilla " +st);
                planillas2 = new Vector();
                while(rs.next()){
                    pl = new Planilla();
                    pl.setDistrito(rs.getString("cia"));
                    pl.setNumpla(rs.getString("numpla"));
                    pl.setFechapla(rs.getDate("fecpla"));
                    pl.setFechadespacho(rs.getTimestamp("fecdsp"));
                    pl.setOrigenpla(rs.getString("oripla"));
                    pl.setDestinopla(rs.getString("despla"));
                    pl.setPlaveh(rs.getString("plaveh"));
                    pl.setPlatr(rs.getString("platlr"));
                    pl.setFechaPLlegada(rs.getTimestamp("fechaposllegada"));
                    pl.setReg_status(rs.getString("reg_status"));
                    pl.setStd_job_no_rec(rs.getString("STD_JOB_NO"));
                    
                    ////System.out.println("STDJOB " + pl.getStd_job_no_rec());
                    //////System.out.println("ANTES INSERTAR ");
                    this.registros++;
                    planillas2.add(pl);
                    // ////System.out.println("DESPUES INSERTAR");
                    
                }
                ////System.out.println("Fin del la lista "+planillas2.size());
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DE LISTAR LAS PLANILLAS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
    }
    
    /**
     * Genera Lista de recursos disponibles que TIENE CUALQUIER MOVIMIENTO ENTRE HOY Y HACE 2 DIAS
     * @autor Ing. Juan Manuel Escandon Perez
     * @param Distrito, fecha inicial, fecha final
     * @throws SQLException
     * @version 1.0
     */
    public void list2(String distrito,String fecha_ini, String fecha_final)throws SQLException{
       /* String consulta = "SELECT CIA, NUMPLA, FECPLA, FECDSP, ORIPLA, DESPLA, PLAVEH, PLATLR, FECHAPOSLLEGADA FROM planilla WHERE cia = ? AND fecpla Between ? AND  ? AND stapla <> 'A'";
        String consulta = "SELECT P.CIA, P.NUMPLA, P.FECPLA, P.FECDSP, P.ORIPLA, P.DESPLA, P.PLAVEH, P.PLATLR, P.FECHAPOSLLEGADA  FROM planilla p, tramo t WHERE p.cia = ? AND p.fecpla Between ? And ? AND t.origin = p.oripla"+
                           " AND t.destination = p.despla AND p.stapla <> 'A' ORDER BY P.FECPLA";*/
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(this.SQLRecursos2);
                st.setString(1, distrito);
                st.setString(2, fecha_ini);
                st.setString(3, fecha_final);
                st.setString(4, fecha_ini);
                st.setString(5, fecha_final);
                rs= st.executeQuery();
                ////System.out.println("Consulta planilla " +st);
                planillas2 = new Vector();
                while(rs.next()){
                    pl = new Planilla();
                    pl.setDistrito(rs.getString("cia"));
                    pl.setNumpla(rs.getString("numpla"));
                    pl.setFechapla(rs.getDate("fecpla"));
                    pl.setFechadespacho(rs.getTimestamp("fecdsp"));
                    pl.setOrigenpla(rs.getString("oripla"));
                    pl.setDestinopla(rs.getString("despla"));
                    pl.setPlaveh(rs.getString("plaveh"));
                    pl.setPlatr(rs.getString("platlr"));
                    pl.setFechaPLlegada(rs.getTimestamp("fechaposllegada"));
                    pl.setReg_status(rs.getString("reg_status"));
                    pl.setStd_job_no_rec(rs.getString("STD_JOB_NO"));
                    //////System.out.println("ANTES INSERTAR ");
                    this.registros++;
                    planillas2.add(pl);
                    // ////System.out.println("DESPUES INSERTAR");
                    
                }
                ////System.out.println("Fin del la lista "+planillas2.size());
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DE LISTAR LAS PLANILLAS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
    }
    
    /**
     * Genera Lista de recursos disponibles que NO TIENE MOVIMIENTO Y LA FECHA DE POSLLEGADA ESTA ENTRE HOY Y HACE 2 DIAS
     * @autor Ing. Juan Manuel Escandon Perez
     * @param Distrito, fecha inicial, fecha final
     * @throws SQLException
     * @version 1.0
     */
    public void list3(String distrito,String fecha_ini, String fecha_final)throws SQLException{
       /* String consulta = "SELECT CIA, NUMPLA, FECPLA, FECDSP, ORIPLA, DESPLA, PLAVEH, PLATLR, FECHAPOSLLEGADA FROM planilla WHERE cia = ? AND fecpla Between ? AND  ? AND stapla <> 'A'";
        String consulta = "SELECT P.CIA, P.NUMPLA, P.FECPLA, P.FECDSP, P.ORIPLA, P.DESPLA, P.PLAVEH, P.PLATLR, P.FECHAPOSLLEGADA  FROM planilla p, tramo t WHERE p.cia = ? AND p.fecpla Between ? And ? AND t.origin = p.oripla"+
                           " AND t.destination = p.despla AND p.stapla <> 'A' ORDER BY P.FECPLA";*/
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(this.SQLRecursos3);
                st.setString(1, distrito);
                st.setString(2, fecha_ini);
                st.setString(3, fecha_final);
                st.setString(4, fecha_ini);
                st.setString(5, fecha_final);
                rs= st.executeQuery();
                ////System.out.println("Consulta planilla " +st);
                planillas2 = new Vector();
                while(rs.next()){
                    pl = new Planilla();
                    pl.setDistrito(rs.getString("cia"));
                    pl.setNumpla(rs.getString("numpla"));
                    pl.setFechapla(rs.getDate("fecpla"));
                    pl.setFechadespacho(rs.getTimestamp("fecdsp"));
                    pl.setOrigenpla(rs.getString("oripla"));
                    pl.setDestinopla(rs.getString("despla"));
                    pl.setPlaveh(rs.getString("plaveh"));
                    pl.setPlatr(rs.getString("platlr"));
                    pl.setFechaPLlegada(rs.getTimestamp("fechaposllegada"));
                    pl.setReg_status(rs.getString("reg_status"));
                    pl.setStd_job_no_rec(rs.getString("STD_JOB_NO"));
                    //////System.out.println("ANTES INSERTAR ");
                    this.registros++;
                    planillas2.add(pl);
                    // ////System.out.println("DESPUES INSERTAR");
                    
                }
                ////System.out.println("Fin del la lista "+planillas2.size());
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DE LISTAR LAS PLANILLAS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
    }
    
/* Metodo: obtenerDespacho, permite listar los campos asociados al trailer
 * @autor : Ing. Jose de la rosa
 * @param : fecha inicio y fecha fin
 * @version : 1.0
 */
    public List obtenerDespacho(String fechini, String fechfin) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        planillas=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SQL_DESPACHOS_XLS);
                st.setString(1, fechini+" 00:00");
                st.setString(2, fechfin+" 23:59");
                rs = st.executeQuery();
                planillas = new LinkedList();
                String cond = "",fcond="";
                String fplaca=  "";
                while(rs.next()){
                    pla = new Planilla();
                    cond = (rs.getString("conductor")!=null) ? rs.getString("conductor") : "";
                    fplaca = (rs.getString("fecha_placa")!=null) ? rs.getString("fecha_placa") : "";
                    fcond = (rs.getString("fecha_conductor")!=null) ? rs.getString("fecha_conductor") : "";
                    pla.setPrinter_date(rs.getString("fecha_impresion"));//fecha impresion
                    pla.setNumpla(rs.getString("numpla"));//numero de la planilla
                    pla.setNitpro(rs.getString("cliente"));//cliente
                    pla.setPlaveh(rs.getString("plaveh"));//placa vehiculo
                    pla.setNomCond(cond);//conductor
                    pla.setFecdsp(rs.getString("fecha_creacion"));//fecha creacion
                    pla.setDespachador(rs.getString("usuario"));//usuario creaci�n
                    pla.setAgcpla(rs.getString("agencia_usuario"));//agencia usuario
                    pla.setNomori(rs.getString("nomciu"));//agencia origen
                    pla.setFecha_placa(fplaca);//fecha placa imagen
                    pla.setFecha_conductor(fcond);//fecha conductor imagen
                    planillas.add(pla);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS CAMPOS DE DESPACHOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return planillas;
    }
    
    /**
     * Metodo: tipoViaje, permite obtener dada una planilla si el tipo de
     *                       viaje es nacional o internacional
     * @autor : Ing. Jose de la rosa
     * @param : numero planilla.
     * @version : 1.0
     */
    public String tipoViaje(String numpla) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sw = "";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null){
                st = con.prepareStatement(SQL_TIPO_VIAJE);
                st.setString(1,numpla);
                rs = st.executeQuery();
                if (rs.next()){
                    sw = rs.getString("tipo_viaje");
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL TIPO DE VIAJE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return sw;
    }
    
    //Jose 15/12/2005
    /**
     * Metodo: obtenerValorStandarPlanilla, permite obtener unos valores de la planilla
     * @autor : Ing. Jose de la rosa
     * @param : fecha inicial y fecha final
     * @version : 1.0
     */
    public Planilla obtenerValorStandarPlanilla(String numpla) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            Planilla p = null;
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( this.getDatabaseName() );
            if ( con != null ) {
                PreparedStatement ps = con.prepareStatement(SQL_OBTENER_DATOS_PLANILLA);
                ps.setString(1,numpla);
                rs = ps.executeQuery();
                if (rs.next()){
                    p = new Planilla();
                    p.setSj(rs.getString("std_job_no"));
                    p.setVlrpla(rs.getFloat("vlrpla"));
                }
                return p;
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DE PLANILLA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return null;
    }
    
    //David 23.12.05
    /**
     * Este m�todo genera un boolean true si existe el numero de de planilla y false si no existe
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @params : String numpla
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public boolean existPlanilla(String numpla)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(EXIST);
                st.setString(1,numpla);
                
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS SI EXISTEN " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return sw;
    }
    /**
     * Metodo que devuelve un string con un comando sql de update
     * para actualizar los datos de trafico e ingrso trafico en el campo cliente
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    
    public void actualizarTrafico() throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql ="";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null){
                
                st = con.prepareStatement("update ingreso_trafico set cliente =? where planilla = ?");
                st.setString(1, pla.getClientes());
                st.setString(2, pla.getNumpla());
                st.executeUpdate();
                
                st = con.prepareStatement("update trafico set cliente =? where planilla = ?");
                st.setString(1, pla.getClientes());
                st.setString(2, pla.getNumpla());
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR  ACTUALIZANDO TRAFICO Y TRAFICO" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection(this.getDatabaseName(), con );
            }
        }
    }
    
    
    //JEscandon 26-01-06
    /**
     * Este m�todo genera un boolean true si existe una planilla, teneniendo en cuenta la Base = COL o false de lo contrario
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @params : String numpla
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public boolean existPlanillaBaseCol(String numpla)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(Search);
                st.setString(1,numpla);
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE existPlanillaBaseCol " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return sw;
    }
    
    /**
     * Genera la informaci�n para el Reporte de Extrafletes y Costos Reembolsables
     * @autor Ing. Tito Andr�s Maturana D.
     * @param fechai Fecha inicial del per�odo
     * @param fechai Fecha final del per�odo
     * @param codcli C�digo del cliente
     * @param tipo Indica si es extraflete o costo reembolsable
     * @param doc Tipo de documento
     * @throws SQLException
     * @version 1.0.
     **/
    public void reporteExtrafletes(String fechai, String fechaf, String codcli, String tipo, String doc) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        this.reporte = new Vector();
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                String sql;
                
                if( tipo.length()==0 ){
                    sql = this.SQL_REP_EXTRAFLETES;
                } else if ( tipo.compareTo("R")==0 ) {
                    sql = this.SQL_REP_EXTRAFLETES_R;
                } else {
                    sql = this.SQL_REP_EXTRAFLETES_E;
                }
                
                if( doc.length()== 0 ){
                    sql += "LEFT JOIN remesa_docto doc ON ( doc.numrem = rem.numrem AND doc.tipo_doc IN ( '008', '009', 'REM', 'TTE', 'TRA', 'FAC', 'DOE', 'DEL', 'PED', 'DO', 'OC', 'VIA', 'DI' ) ) ";
                    
                    sql += "LEFT JOIN tbldoc tdoc ON ( doc.tipo_doc = tdoc.document_type ) " +
                    "WHERE rem.cliente LIKE ? " +
                    "GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12 " +
                    "ORDER BY pl.numpla";
                    
                    st = con.prepareStatement(sql);
                    
                    st.setString(1, fechai+ " 00:00:00");
                    st.setString(2, fechaf + " 23:59:59");
                    st.setString(3, codcli + "%");
                } else {
                    sql += "LEFT JOIN remesa_docto doc ON ( doc.numrem = rem.numrem AND doc.tipo_doc = ? ) " +
                    "LEFT JOIN tbldoc tdoc ON ( doc.tipo_doc = tdoc.document_type ) " +
                    "WHERE rem.cliente LIKE ? " +
                    "GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12 " +
                    "ORDER BY pl.numpla";
                    
                    st = con.prepareStatement(sql);
                    
                    st.setString(1, fechai+ " 00:00:00");
                    st.setString(2, fechaf + " 23:59:59");
                    st.setString(3, doc);
                    st.setString(4, codcli + "%");
                }
                
                
                
                ////System.out.println("..... consulta reporte extrafletes : " + st.toString() );
                
                rs = st.executeQuery();
                
                while(rs.next()){
                    Hashtable ht = new Hashtable();
                    
                    //ht.put("", rs.getString(""));
                    ht.put("numpla", rs.getString("numpla"));
                    ht.put("fecdsp", rs.getString("fecdsp"));
                    ht.put("ruta", rs.getString("ruta"));
                    ht.put("vlr", rs.getString("vlr"));
                    ht.put("vlr_ingreso", rs.getString("vlr_ingreso"));
                    ht.put("desc", rs.getString("desc"));
                    ht.put("numrem", rs.getString("numrem"));
                    ht.put("tipo", rs.getString("tipo"));
                    ht.put("cliente", rs.getString("cliente"));
                    ht.put("codigo", rs.getString("codextraflete"));
                    ht.put("autorizador", rs.getString("usuario"));
                    ht.put("observacion", rs.getString("observacion"));
                    
                    String tdocumentos = rs.getString("tdocumentos");
                    String documentos = rs.getString("documentos");
                    ht.put("documentos", documentos.length()==0 ? "NO REGISTRA" : documentos );
                    ht.put("tdocumentos", tdocumentos.length()==0 ? "NO REGISTRA" : tdocumentos);
                    
                    this.reporte.add(ht);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL REPORTE DE EXTRAFLETES Y COSTOS REEMBOLSABLES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
    }
    /**
     * Funcion que retorna la cantidad de planillas realizadas a una placa desde una
     * fecha dada hasta la fecha actual.
     * @autor Ing. Karen Reales
     * @param fechai Fecha inicial del per�odo
     ** @param placa Placa del vehiculo
     * @throws SQLException
     * @version 1.0.
     **/
    public int cantPlanillasPlaca(String fechai,String placa) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        int cant =0;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("select count(numpla)as cant from planilla where fecdsp between ? and 'now()' and plaveh = ?");
                st.setString(1, fechai+ " 00:00:00");
                st.setString(2, placa);
                ////System.out.println("Query cant : "+st);
                rs = st.executeQuery();
                
                if(rs.next()){
                    cant = rs.getInt("cant");
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO LA CANTIDAD DE VIAJES DE LA PLACA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return cant;
    }
    /**
     * Getter for property plasindoc.
     * @return Value of property plasindoc.
     */
    public java.util.Vector getPlasindoc() {
        return plasindoc;
    }
    
    /**
     * Metodo: buscarPlanillasinDocumetos, Metodo que busca las planillas creadas por un usuario
     *         que no tienen documentos completos. No teniene en cuenta las de carbon
     * @autor : Ing. Diogenes Bastidas Morales
     * @param : usuario, fecha inicio y fecha fin
     * @version : 1.0
     */
    public void buscarPlanillasinDocumentos(String usuario, String fecha1, String fecha2) throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        plasindoc = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null){
                st = con.prepareStatement(this.SQL_PLANILLAS_SINDOC);
                st.setString(1, fecha1+" 00:00");
                st.setString(2, fecha2+" 23:59");
                st.setString(3, usuario);
                plasindoc = new Vector();
                ////System.out.println(st);
                rs = st.executeQuery();
                while (rs.next()){
                    Planilla pla =  new Planilla();
                    pla.setNumpla(rs.getString("numpla"));
                    pla.setFechadespacho(rs.getTimestamp("fecdsp"));
                    pla.setStd_job_rec(rs.getString("std_job_desc"));
                    pla.setClientes(rs.getString("nomcli"));
                    pla.setNumrem(rs.getString("numrem"));
                    plasindoc.add(pla);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR LA PLANILLA SIN DOCUMENTOS " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection(this.getDatabaseName(), con );
            }
        }
        
    }
    /**
     * Genera Lista de recursos disponibles
     * @autor Ing. Juan Manuel Escandon Perez
     * @param Distrito, fecha inicial, fecha final
     * @throws SQLException
     * @version 1.0
     */
    public void ListaRecursosDisp(String distrito,String fecha_ini, String fecha_final)throws SQLException {
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(this.RecursosDisponibles);
                st.setString(1, fecha_ini);
                st.setString(2, fecha_final);
                st.setString(3, distrito);
                st.setString(4, fecha_ini);
                st.setString(5, fecha_final);
                rs= st.executeQuery();
                ////System.out.println("Consulta planilla " +st);
                planillas2 = new Vector();
                while(rs.next()){
                    pl = new Planilla();
                    pl.setDistrito(rs.getString("cia"));
                    pl.setNumpla(rs.getString("numpla"));
                    pl.setFechapla(rs.getDate("fecpla"));
                    pl.setFechadespacho(rs.getTimestamp("fecdsp"));
                    pl.setOrigenpla(rs.getString("oripla"));
                    pl.setDestinopla(rs.getString("despla"));
                    pl.setPlaveh(rs.getString("plaveh"));
                    pl.setPlatr(rs.getString("platlr"));
                    pl.setFechaPLlegada(rs.getTimestamp("fechaposllegada"));
                    pl.setReg_status(rs.getString("reg_status"));
                    pl.setAgcpla(rs.getString("AGENCIA"));//Jescandon 091006
                    this.registros++;
                    planillas2.add(pl);
                    
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DE LISTAR LAS PLANILLAS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
    }
    
    /**
     * Metodo: reporteViajesXLS, permite listar los viajes del reporte de planeaci�n
     * @autor : Ing. Jose de la rosa
     * @param : fecha inicial, fecha final, placa y el standar
     * @version : 1.0
     */
    public List reporteViajesXLS(String fini, String ffin, String estandar) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        planillas=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SQL_VIAJE_XLS);
                st.setString(1, fini+" 07:00" );
                st.setString(2, ffin+" 06:59" );
                st.setString(3, estandar );
                ////System.out.println(st+"*********");
                rs = st.executeQuery();
                planillas = new LinkedList();
                while(rs.next()){
                    pla = new Planilla();
                    pla.setFeccum(rs.getString("dia"));//dia
                    pla.setDias(rs.getInt("viajes"));//numero de dias
                    pla.setPesoreal(rs.getFloat("toneladas"));//toneladas
                    planillas.add(pla);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS VIAJES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return planillas;
    }
    /*****************************************************************************************
     * Metodo: ListaVehExternos
     * parametros: distrito, fecha inicial, fecha final.
     * autor: Ing. Andres MartinezInserta
     * descripcion: llama a la consulta veh_externos y setea el rsulset en un vector resultado.
     ******************************************************************************************/
    public void ListaVehExternos( String distrito,String fecha_ini, String fecha_final ) throws SQLException{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection(this.getDatabaseName());
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        planillas3 = new Vector();
        try{
            st = con.prepareStatement(this.veh_externos);
            st.setString(1, fecha_ini);
            st.setString(2, fecha_final);
            st.setString(3, distrito);
            ////System.out.println("query:"+st.toString());
            rs= st.executeQuery();
            ////System.out.println("Consulta planilla " + st );
            
            if(rs!=null){
                while(rs.next()){
                    pl = new Planilla();
                    pl.setDistrito(rs.getString("dstrct"));
                    pl.setDestinoRelacionado(rs.getString("agencia_disp"));
                    pl.setFechadespacho(rs.getTimestamp("fecha_disp"));
                    pl.setPlaveh(rs.getString("placa"));
                    
                    planillas3.add(pl);
                }
            }
        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("Error en rutina  [PlanillaDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection(this.getDatabaseName(),con);
        }
    }
    /**
     * Getter for property planillas3.
     * @return Value of property planillas3.
     */
    public java.util.Vector getPlanillas3() {
        return planillas3;
    }
    
    /**
     * Setter for property planillas3.
     * @param planillas3 New value of property planillas3.
     */
    public void setPlanillas3(java.util.Vector planillas3) {
        this.planillas3 = planillas3;
    }
    /**
     *Metodo: buscarTotalEquipo2, Metodo que retorna la cantidad total de
     *         equipos despachados de carbon en unas fechas dadas
     *@autor : Ing. Karen Reales
     *@param : fecha inicio , fecha fin, base
     *@return: Numero de viajes
     *@version : 1.0
     */
    public int buscarTotalEquipo2(String fechai, String fechaf, String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        int result=0;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SQL_TOTALEQDESP);
                st.setString(1, fechai);
                st.setString(2, fechaf);
                st.setString(3, base);
                ////System.out.println("Sql: "+st.toString());
                rs = st.executeQuery();
                if(rs.next()){
                    result = rs.getInt("cant");
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA CANTIDAD DE EQUIPOS TOTALES" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return result;
        
    }
    public void setPlanillasVec(Vector plas){
        
        this.plas=plas;
    }
    /**
     *Metodo: estaEnTrafico, Metodo que retorna true o false si la placa
     * que se esta registrando ya esta en transito.
     *@autor : Ing. Karen Reales
     *@param : placa
     *@return: True o False
     *@version : 1.0
     */
    public boolean estaEnTrafico(String placa)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        boolean result=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(this.SQL_PLACA_TRANSITO);
                st.setString(1, placa);
                ////System.out.println("Sql: "+st.toString());
                rs = st.executeQuery();
                if(rs.next()){
                    result = true;
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA PLACA EN TRANSITO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return result;
        
    }
    
    /**
     * Setter for property informes.
     */
    public void setInformes(Vector informes){
        
        this.informes=informes;
    }
    /**
     * Metodo que devuelve un string con los nombres de los clientes dada una planilla
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public String buscarClientes(String numpla) throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String clientes ="";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null){
                
                st = con.prepareStatement(SQL_NOMBRE_CLIS);
                st.setString(1, numpla);
                rs = st.executeQuery();
                while(rs.next()){
                    clientes = clientes+rs.getString("nomcli")+",";
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO LOS CLIENTES PARA INGRESO TRAFICO" + e.getMessage());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection(this.getDatabaseName(), con );
            }
        }
        return clientes;
    }
    /*Juan 09-02-2006*/
    public Informe getFlete( Informe inf ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String valor = "";
        double costoflete = 0;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null){
                st = con.prepareStatement( this.flete );
                st.setString(1, inf.getNumpla() );
                ////System.out.println("Valor flete " + st );
                rs = st.executeQuery();
                
                while(rs.next()){
                    valor += rs.getString("DESCRIPCION")+" = "+ rs.getString("VLR")+"<br>";
                    costoflete = rs.getDouble("VALORTOTAL");
                }
               /* if( !valor.equals("") )
                    valor = valor.substring(0,valor.length()-1);*/
                
                if( costoflete == 0 )
                    valor = "";
                
                inf.setVlrcostoflete(costoflete);
                inf.setCostoflete(valor);
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR EL VALOR" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection(this.getDatabaseName(), con );
            }
        }
        return inf;
    }
    /**fecha:2006-05-19
     * Metodo: reporteProdFlotaXLS, permite listar un reporte de produccion de flota
     * @autor : Ing. Jose de la rosa
     * @param : fecha inicial, fecha final, placa y un nit del proveedor
     * @version : 1.0
     */
    public List reporteProdFlotaPlacasDiasSTDXLS(String fini, String ffin, String placa, String nit, String standar) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        planillas=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SQL_PROD_FLOTA_PLACA_STDXLS);
                st.setString(1, fini+" 07:00" );
                st.setString(2, ffin+" 06:59" );
                st.setString(3, placa );
                st.setString(4, nit );
                st.setString(5, standar );
                rs = st.executeQuery();
                planillas = new LinkedList();
                while(rs.next()){
                    pla = new Planilla();
                    pla.setFeccum(rs.getString("dia"));//dia
                    pla.setDias(rs.getInt("count"));//numero de dias
                    pla.setPesoreal(rs.getFloat("sum"));
                    planillas.add(pla);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS REPORTES DE PRODUCCION FLOTA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return planillas;
    }
    /**
     * Este m�todo genera un boolean true si la planilla tiene numero de factura
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @params : String numpla
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public boolean tieneFactura(String codpla )throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw=false;
        try {
            
            st = this.crearPreparedStatement("SQL_TIENE_FACTURA");
            st.setString(1,codpla);
            rs = st.executeQuery();
            
            if(rs.next()){
                if(!rs.getString("factura").equals("")){
                    sw= true;
                }
            }
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR VERIFICANDO SI LA PLANILLA TIENE FACTURA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_TIENE_FACTURA");
            
        }
        return sw;
    }
    /**fecha:  12/06/2006
     * Metodo: getPlanillasSinPlanviaje, obtiene las planillas que no tiene plan de viaje
     *         que pertenecen a la agencia dada, retorna las planillas con fecha diez dias
     *         antes de la fecha actual
     * @autor : Osvaldo P�rez Ferrer
     * @param : codigo de la agencia
     * @version : 1.0
     */
    
    public Vector getPlanillasSinPlanviaje(String agencia) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Vector planillas = null;
        planillas=null;
        try {
            st = this.crearPreparedStatement("SQL_PLANILLAS_SINPLANVIAJE_AGENCIA");
            //st.setString(1,agencia);
            st.setString(1,agencia);
            rs = st.executeQuery();
            
            planillas = new Vector();
            
            while(rs.next()){
                
                pla = new Planilla();
                pla.setFecdsp(rs.getString("fecpla"));
                pla.setNumpla(rs.getString("numpla"));
                pla.setPlaveh(rs.getString("plaveh"));
                pla.setOripla(rs.getString("oripla"));
                pla.setDespla(rs.getString("despla"));
                pla.setCedcon(rs.getString("cedcon"));
                pla.setDespachador(rs.getString("despachador"));
                
                planillas.add(pla);
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS REPORTES DE PRODUCCION FLOTA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            this.desconectar("SQL_PLANILLAS_SINPLANVIAJE_AGENCIA");
        }
        return planillas;
    }
    
    
    /**
     *Metodo que permite obtener una remesa dado el numero de la remesa
     *@autor: David Pi�a
     *@param numpla N�mero de la planilla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void getPlanillaIC( String numpla ) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs  =null;
        try {
            st = this.crearPreparedStatement( "SQL_OBTENER_PLANILLA_IC" );
            st.setString( 1, numpla );
            rs = st.executeQuery();
            
            while (rs.next()){
                pla = new Planilla();
                pla.setNumpla( (rs.getString("numpla")!=null)?rs.getString("numpla"):"" );
                pla.setPlaveh( (rs.getString("plaveh")!=null)?rs.getString("plaveh"):"" );
                pla.setNitpro( (rs.getString("nitpro")!=null)?rs.getString("nitpro"):"" );
                pla.setNomprop( (rs.getString("nompro")!=null)?rs.getString("nompro"):"" );
                pla.setAgcpla( (rs.getString("agcpla")!=null)?rs.getString("agcpla"):"" );
                pla.setNomagc( (rs.getString("nomagencia")!=null)?rs.getString("nomagencia"):"" );
                pla.setOripla( (rs.getString("oripla")!=null)?rs.getString("oripla"):"" );
                pla.setNomori( (rs.getString("nomciu")!=null)?rs.getString("nomciu"):"" );
                pla.setDestinoRelacionado( (rs.getString("agasoc")!=null)?rs.getString("agasoc"):"" );
                pla.setNomdesrel( (rs.getString("nomagenciarel")!=null)?rs.getString("nomagenciarel"):"" );
                pla.setFeccum( (rs.getString("fecpla")!=null)?rs.getString("fecpla"):"" );
                
            }
        } catch( SQLException e ){
            throw new SQLException( "Error en getPlanillaIC [PlanillaDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }
        finally{
            if ( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            this.desconectar( "SQL_OBTENER_PLANILLA_IC" );
        }
    }
    /**
     * M�todo que obtiene el reporte de las planillas vacias
     * @autor.......Jose De La Rosa
     * @throws......SQLException
     * @version.....1.0.
     **/
    public Vector planillasVacias( String fecini, String fecfin ) throws SQLException{
        PreparedStatement ps = null,st=null;
        ResultSet rs = null, rs2=null;
        Vector vec = new Vector();
        pla = null;
        try{
            st = crearPreparedStatement("SQL_PLANILLAS_LLENAS");
            ps = crearPreparedStatement("SQL_PLANILLAS_VACIAS");
            ps.setString( 1, fecini+" 00:00" );
            ps.setString( 2, fecfin+" 23:59" );
            rs = ps.executeQuery();
            while( rs.next() ){
                pla = new Planilla();
                pla.setNumpla       ( rs.getString("pla_vac")          );// numero de la planilla vacia
                pla.setPlaveh       ( rs.getString("plaveh")           );// numero de la placa de la planilla vacia
                pla.setNumrem       ( rs.getString("rem_vac")          );// numero de la remesa de la planilla vacia
                pla.setFecdsp       ( rs.getString("fec_vac")          );// fecha de la planilla vacia
                pla.setNomcliente   ( rs.getString("cli_vac")          );// nombre del cliente de la planilla vacia
                pla.setOripla       ( rs.getString("nom_ori_vac")      );// nombre del origen de la planilla vacia
                pla.setDespla       ( rs.getString("nom_des_vac") );// nombre del destino de la planilla vacia
                pla.setNomcon       ( rs.getString("con_vac")          );// conductor del vacio
                pla.setStatus_220   ( rs.getString("tiene_reporte")    );// si la planilla tiene reportes en trafico
                
                st.clearParameters();
                st.setString(1,  rs.getString("agasoc")   );
                st.setString(2,  rs.getString("plaveh")   );
                st.setString(3,  rs.getString("fec_vac")  );
                rs2 = st.executeQuery();
                if( rs2.next() ){
                    pla.setGroup_code   ( rs2.getString("num_llen")        );// numero de la planilla llena
                    pla.setFeccum       ( rs2.getString("fec_llen")        );// fecha de la planilla llena
                    pla.setCedcon       ( rs2.getString("con_llen")        );// conductor de viaje lleno
                    pla.setClientes     ( rs2.getString("cli_llen")        );// nombre del cliente de la planilla llena
                    pla.setNomori       ( rs2.getString("nom_ori_llen")    );// nombre del origen de la planilla llena
                    pla.setNomdest      ( rs2.getString("nom_des_llen")    );// nombre del destino de la planilla llena
                    vec.add( pla );
                }
            }
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL BUSCAR EL REPORTE DE VACIOS, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_PLANILLAS_VACIAS");
            desconectar("SQL_PLANILLAS_LLENAS");
        }
        return vec;
    }
    /**
     *Metodo que anula un registro en ingreso_trafico
     *@autor: Jose de la rosa
     *@param: Numero de la planilla
     *@return: Comando SQL
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String eliminarTrafico(String numpla) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "";
        try{
            st = this.crearPreparedStatement("SQL_DELETETRAFICO");
            st.setString(1, numpla);
            sql = st.toString();
        }catch(SQLException e){
            throw new SQLException("ERROR AL ANULAR EL REGISTRO EN INGRESO TRAFICO Y TRAFICO" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_DELETETRAFICO");
        }
        return sql;
    }
    /**
     *Metodo que permite obtener el plan de viaje de una planilla
     *@autor: David Pi�a
     *@param fechainicio La fecha inicio de la planilla
     *@param fechafin La fecha fin de la planilla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void getPlanDeViaje( String fechainicio, String fechafin ) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs  =null;
        try {
            st = this.crearPreparedStatement( "SQL_OBTENER_PLANVIAJE" );
            st.setString( 1, fechainicio );
            st.setString( 2, fechafin );
            rs = st.executeQuery();
            planillas2 = new Vector();
            
            while (rs.next()){
                pla = new Planilla();
                pla.setNumpla( (rs.getString("numpla")!=null)?rs.getString("numpla"):"" );
                pla.setObservacion( (rs.getString("usergen")!=null)?rs.getString("usergen"):"" );
                pla.setPlaveh( (rs.getString("plaveh")!=null)?rs.getString("plaveh"):"" );
                pla.setTipoviaje( (rs.getString("tipoviaje")!=null)?rs.getString("tipoviaje"):"" );
                pla.setOrinom( (rs.getString("orides")!=null)?rs.getString("orides"):"" );
                pla.setNomagc( (rs.getString("agencia")!=null)?rs.getString("agencia"):"" );
                pla.setNomcliente( (rs.getString("cliente")!=null)?rs.getString("cliente"):"" );
                pla.setCelularcon( (rs.getString("celular")!=null)?rs.getString("celular"):"" );
                pla.setDespachador( (rs.getString("usuario")!=null)?rs.getString("usuario"):"" );
                pla.setFecdsp( (rs.getString("fechaModificacion")!=null)?rs.getString("fechaModificacion"):"" );
                pla.setFecha_salida( (rs.getString("fecha_salida")!=null)?rs.getString("fecha_salida"):"" );
                pla.setNomdesrel( (rs.getString("pl1")!=null)?rs.getString("pl1"):"" );//Pernoctacion 1
                pla.setNomdest( (rs.getString("pl2")!=null)?rs.getString("pl2"):"" );//Pernoctacion 2
                pla.setNomori( (rs.getString("pl3")!=null)?rs.getString("pl3"):"" );//Pernoctacion 3
                pla.setFeccum( (rs.getString("creation_date")!=null)?rs.getString("creation_date"):"" );//Fecha Creaci�n
                pla.setRuta_pla( (rs.getString("ruta")!=null)?rs.getString("ruta"):"" );
                pla.setNumrem( (rs.getString("planilla")!=null)?rs.getString("planilla"):"" );//Numero planilla hoja de reporte
                
                planillas2.add( pla );
                
            }
        } catch( SQLException e ){
            throw new SQLException( "Error en getPlanDeViaje [PlanillaDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }
        finally{
            if ( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            this.desconectar( "SQL_OBTENER_PLANVIAJE" );
        }
    }
    /**
     *Metodo que permite obtener la hoja de reporte de una planilla
     *@autor: David Pi�a
     *@param fechainicio La fecha inicio de la planilla
     *@param fechafin La fecha fin de la planilla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void getHojadeReporte( String fechainicio, String fechafin ) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs  =null;
        try {
            st = this.crearPreparedStatement( "SQL_OBTENER_HOJAREPORTE_PLANILLA" );
            st.setString( 1, fechainicio );
            st.setString( 2, fechafin );
            rs = st.executeQuery();
            planillas2 = new Vector();
            
            while (rs.next()){
                pla = new Planilla();
                pla.setNumpla( (rs.getString("numpla")!=null)?rs.getString("numpla"):"" );
                pla.setObservacion( (rs.getString("usergen")!=null)?rs.getString("usergen"):"" );
                pla.setPlaveh( (rs.getString("plaveh")!=null)?rs.getString("plaveh"):"" );
                pla.setTipoviaje( (rs.getString("tipoviaje")!=null)?rs.getString("tipoviaje"):"" );
                pla.setOrinom( (rs.getString("orides")!=null)?rs.getString("orides"):"" );
                pla.setNomagc( (rs.getString("agencia")!=null)?rs.getString("agencia"):"" );
                pla.setNomcliente( (rs.getString("cliente")!=null)?rs.getString("cliente"):"" );
                pla.setDespachador( (rs.getString("creation_user")!=null)?rs.getString("creation_user"):"" );
                pla.setFeccum( (rs.getString("creation_date")!=null)?rs.getString("creation_date"):"" );//Fecha Creaci�n
                pla.setPrinter_date( (rs.getString("print_date")!=null)?rs.getString("print_date"):"" );
                pla.setNomprop( (rs.getString("print_user")!=null)?rs.getString("print_user"):"" );
                pla.setNumrem( (rs.getString("planilla")!=null)?rs.getString("planilla"):"" );//Numero planilla hoja de reporte
                planillas2.add( pla );
                
            }
        } catch( SQLException e ){
            throw new SQLException( "Error en getPlanDeViaje [PlanillaDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }
        finally{
            if ( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            this.desconectar( "SQL_OBTENER_HOJAREPORTE_PLANILLA" );
        }
    }
    public Vector getResumenPro(){
        return this.reporte;
    }
    
    public void generarResumenPro( String fechai, String fechaf, String base ) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs  =null;
        
        RemisionIFact remfact = null;
        reporte = new Vector();
        try {
            if (base.equals("pco")){
                st = this.crearPreparedStatement( "SQL_GENERA_RESUMEN_PRO" );
            }else{
                st = this.crearPreparedStatement( "SQL_GENERA_RESUMEN_CAR" );
            }
            
            st.setString( 1, fechai );
            st.setString( 2, fechaf );
            st.setString( 3, base   );
            //System.out.println("query : " + st.toString());
            rs = st.executeQuery();
            
            String var1;
            while(rs.next()){
                remfact = new RemisionIFact();
                
                if (base.equals("pco")){
                    var1 =  rs.getString(1);
                    //System.out.println("String : " + var1.toString());
                    String[] c = var1.split(",");
                    remfact.setFecha(c[0]);
                    remfact.setStd_job_no(c[1]);
                    remfact.setNomStandar(getNombreStandar(c[1]));
                }else{
                    remfact.setStd_job_no(rs.getString("std_job_no"));
                    remfact.setNomStandar(getNombreStandar(rs.getString("std_job_no")));
                    remfact.setFecha(rs.getString("feccum"));
                }
                
                remfact.setViajes(rs.getInt("viajes"));
                remfact.setTonelaje(Double.valueOf(rs.getDouble("Toneladas")));
                
                reporte.add(remfact);
            }
            
        } catch( SQLException e ){
            throw new SQLException( "Error en generarResumenPro [PlanillaDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }
        finally{
            if ( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if (base.equals("pco")){
                this.desconectar( "SQL_GENERA_RESUMEN_PRO" );
            }else{
                this.desconectar( "SQL_GENERA_RESUMEN_CAR" );
            }
            
        }
    }
    
    public String getNombreStandar(String stdjob) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Vector planillas = null;
        String nombre = "";
        try {
            st = this.crearPreparedStatement("SQL_NOMBRE_STANDAR");
            st.setString(1,stdjob);
            rs = st.executeQuery();
            
            if(rs.next()){
                nombre = rs.getString(1);
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL NOMBRE DEL STANDAR" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            this.desconectar("SQL_NOMBRE_STANDAR");
        }
        return nombre;
    }
    
    
    
    /**
     * Metodo que busca una planilla de carbon
     * @autor Karen Reales
     * @param codpla : Numero de la planilla
     * throws SQLException: si una excepcion de sql sucede.
     */
    public void searchPlanilla2(String codpla )throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        pla = null;
        
        try {
            
            st = this.crearPreparedStatement("SQL_SEARCHPLACARBON");//m041005
            st.setString(1,codpla);
            ////System.out.println("Consulta "+st.toString());
            rs = st.executeQuery();
            
            while(rs.next()){
                pla=Planilla.load(rs);
                pla.setNomCond(rs.getString("nombre"));
                pla.setSj(rs.getString("std_job_no"));
                pla.setSj_desc(rs.getString("sj_desc"));
                pla.setNomori(rs.getString("origen"));
                pla.setNomdest(rs.getString("destino"));
                pla.setUnit_vlr(rs.getString("unit_vlr"));//m041005
                pla.setReg_status(rs.getString("reg_status"));//m041005
                pla.setBase(rs.getString("base"));//jose 2006-04-03
                pla.setSaldo(rs.getFloat("cantidad"));
                pla.setFactura(rs.getString("factura"));
                //Aqui van Planillas
            }
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_SEARCHPLACARBON");
        }
        
    }
    
    /**
     * Metodo que busca una lista de remesas relacionadas a una planilla
     * @autor Ing. Karen Reales.
     * @param Numero de la planilla
     * @throws SQLException
     * @version 1.0.
     **/
    public void searchRemesas(String numpla)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null,rs2 = null;
        plaIncompletas = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SQL_LISTAR_REMESAS);//m041005
                st.setString(1,numpla);
                rs = st.executeQuery();
                remesas = new LinkedList();
                
                while(rs.next()){
                    Remesa r = Remesa.load(rs);
                    r.setPorcentaje(rs.getFloat("porcent"));
                    r.setUnit(rs.getString("unidad"));//jose2006-12-18
                    r.setReg_status(rs.getString("reg_status"));//m041005
                    r.setFactura(rs.getString("doc_fac"));
                    if(rs.getString("currency")!=null){
                        if(!rs.getString("currency").equals("")&&!rs.getString("currency").equals("PES")){
                            //AHORA BUSCO EL VALOR EN PESOS DEL STANDARD
                            st = con.prepareStatement(SQL_CAMBIOMONEDA_FECHA);
                            st.setString(1, rs.getString("currency"));
                            rs2 = st.executeQuery();
                            if(rs2.next()){
                                r.setVlr_pesos(com.tsp.util.Util.redondear(rs.getFloat("vlrrem")*rs2.getDouble("valor"),0));
                            }else{
                                st = con.prepareStatement(SQL_CAMBIOMONEDA_NOFECHA);
                                st.setString(1, rs.getString("currency"));
                                rs2 = st.executeQuery();
                                if(rs2.next()){
                                    r.setVlr_pesos(com.tsp.util.Util.redondear(rs.getFloat("vlrrem")*rs2.getDouble("valor"),0));
                                }
                            }
                            
                        }
                    }
                    remesas.add(r);
                    //Aqui van PlaAux
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS REMESAS  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
    }
    public void consultaPlanilla(String placa, String fechaini, String fechafin, String cumplido, String anulada, String nit, String cedcon, String base, String despachador, String ori, String des, String agencia )throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        plas = null;
        String sql = "SQL_PLANILLA_GENERAL";
        
        try {
            
            st = this.crearPreparedStatement(sql);
            st.setString(1, placa+"%");
            st.setString(2, nit+"%");
            st.setString(3, cedcon+"%");
            st.setString(4, fechaini+" 00:00:00");
            st.setString(5, fechafin+" 23:59:59");
            st.setString(6, base);
            st.setString(7, despachador+"%");
            st.setString(8, ori+"%");
            st.setString(9, des+"%");
            st.setString(10, agencia+"%");
            //////System.out.println("---------CONSULTA PLANILLA "+st.toString());
            org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
            logger.info("?consulta planilla: " + st);
            rs = st.executeQuery();
            plas = new Vector();
            while(rs.next()){
                pla = new Planilla();
                pla.setNumpla(rs.getString("numpla"));
                pla.setPlaveh(rs.getString("plaveh"));
                pla.setCedcon(rs.getString("cedcon"));
                pla.setNumrem(rs.getString("numrem"));
                pla.setNomCond(rs.getString("nombre"));
                pla.setFecdsp(rs.getString("fecdsp"));
                pla.setSj_desc(rs.getString("descripcion"));
                pla.setRemision(rs.getString("remision"));
                pla.setDespachador(rs.getString("despachador"));
                pla.setReg_status(rs.getString("reg_status"));
                pla.setDistrito(rs.getString("distrito"));
                
                //AMATURANA
                pla.setCorrida_fra(rs.getString("corrida"));
                pla.setStatus_fra(rs.getString("status_fra"));
                pla.setFactura(rs.getString("factura"));
                pla.setNitpro(rs.getString("nitpro"));
                
                plas.add(pla);
            }
            
        } catch(SQLException e) {
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar(sql);
            
        }
        
    }
    
    public int cantPlanilla(String codpla )throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        pla = null;
        PoolManager poolManager = null;
        plas = null;
        int i =0;
        String sql = "SQL_CANT_PLANILLA";
        
        try {
            
            st = this.crearPreparedStatement(sql);
            
            st.setString(1,codpla);
            rs = st.executeQuery();
            plas = new Vector();
            while(rs.next()){
                i++;
                pla = new Planilla();
                pla.setNumpla(rs.getString("numpla"));
                pla.setPlaveh(rs.getString("plaveh"));
                pla.setCedcon(rs.getString("cedcon"));
                pla.setNumrem(rs.getString("numrem"));
                pla.setNomCond(rs.getString("nombre"));
                pla.setFecdsp(rs.getString("fecdsp"));
                pla.setSj_desc(rs.getString("descripcion"));
                pla.setRemision(rs.getString("remision"));
                pla.setDespachador(rs.getString("despachador"));
                pla.setReg_status(rs.getString("reg_status"));
                pla.setDistrito(rs.getString("cia"));
                //AMATURANA
                pla.setCorrida_fra(rs.getString("corrida"));
                pla.setStatus_fra(rs.getString("status_fra"));
                pla.setFactura(rs.getString("factura"));
                pla.setNitpro(rs.getString("nitpro"));
                plas.add(pla);
                //Aqui van Planillas de colpapel
            }
            
            
        } catch(SQLException e) {
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLASA COLPAPEL " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar(sql);
            
        }
        return i;
    }
    
    
    /**
     * Metodo inserta una planilla en la tabla de planilla
     * @autor Karen Reales
     * @param base : Base del proyecto
     *@return String con comando SQL
     * throws SQLException: si una excepcion de sql sucede.
     */
    public String insertPla(String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql="";
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                String fecha="'now()'";
                if (pla.getFecdsp()!=null)
                    fecha=pla.getFecdsp();
                st = con.prepareStatement(SQL_INSERT);
                st.setString(1,pla.getNumpla());
                st.setString(2, fecha);
                st.setString(3,pla.getAgcpla());
                st.setString(4,pla.getOripla());
                st.setString(5,pla.getDespla());
                st.setString(6,pla.getPlaveh());
                st.setString(7,pla.getCedcon());
                st.setString(8,pla.getNitpro());
                st.setString(9,pla.getPlatlr());
                st.setString(10,fecha);
                st.setString(11,pla.getCia());
                st.setString(12,pla.getTipoviaje());
                st.setString(13,pla.getDespachador());
                st.setString(14,pla.getMoneda());
                st.setString(15,pla.getOrden_carga());
                st.setFloat(16, pla.getPesoreal());
                st.setFloat(17, pla.getVlrpla());
                st.setString(18,pla.getRuta_pla());
                st.setString(19, pla.getPrecinto());
                st.setString(20, pla.getUnit_transp());
                st.setString(21, pla.getMoneda());
                st.setString(22, base);
                st.setFloat(23, pla.getUnit_cost());
                st.setString(24, pla.getContenedores());
                st.setString(25, pla.getTipocont());
                st.setString(26, pla.getTipotrailer());
                st.setString(27, pla.getProveedor());
                st.setString(28, pla.getCf_code());
                st.setFloat(29, pla.getVlrpla2());
                st.setString(30, pla.getTiene_doc());
                st.setString(31, pla.getCmc());
                
                /*st.setString(25, pla.getBanco());
                st.setString(26, pla.getCuenta());*/
                sql = st.toString();
                
                // st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE LAS PLANILLAS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return sql;
    }
    
    //modificado por LUIGI.. el 2007/02/28
    public void obtenerPLacaConductor(String placa, String fecini, String fecfin, String oripla, String despla, String agenpla, String conductor) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        reporte=null;
        total = 0 ;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            //boolean sw = false;
            if(con!=null){
                st = con.prepareStatement(SQL_PLACA_CONDUCTOR);
                st.setString(1, placa);
                st.setString(2, fecini+"%");
                st.setString(3, fecfin+"%");
                st.setString(4, oripla+"%");
                st.setString(5, despla+"%");
                st.setString(6, agenpla+"%");
                st.setString(7, conductor+"%");
                rs = st.executeQuery();
                reporte = new Vector();
                while(rs.next()){
                    pla = new Planilla();
                    pla.setPlaveh(placa);
                    pla.setFecha_salida(fecini);
                    pla.setFecplanilla(fecfin);
                    pla.setNumpla(rs.getString("numpla"));
                    pla.setFecdsp(rs.getString("fecpla"));
                    pla.setOripla(rs.getString("oripla"));
                    pla.setDespla(rs.getString("despla"));
                    pla.setNumrem(rs.getString("numrem"));
                    pla.setNomCond(rs.getString("nombre"));
                    if(!(rs.getString("estado_despacho").equals("A"))){
                        total = total + 1;
                    }
                    pla.setReg_status(rs.getString("estado_despacho"));
                    pla.setNomori(rs.getString("orirem"));
                    pla.setNomdest(rs.getString("desrem"));
                    pla.setStatus_220(rs.getString("estado_remesa"));
                    pla.setAgcpla(rs.getString("agcpla"));
                    pla.setNitpro(rs.getString("cliente"));
                    reporte.add(pla);
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLACAS Y LOS CONDUCTORES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
    }
    
/* Metodo: generarReporteDiario
 * Descriocion : Funcion publica que obtiene el Reporte Diario de los Despachos
 * @autor : LREALES
 * @param : distrito, fecha inicial, fecha final, placa del vehiculo, cedula del conductor, despachador, y agencias si las tienes
 * @version : 1.0
 */
    public void generarReporteDiario( String distrito, String desde, String hasta, String plaveh, String cedcon, String despachador, String agencias, String cliente, String usuario ) throws SQLException {
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        plaaux = null;
        String valor="";
        LogErrores log = new LogErrores();
        
        
        try {
            String reporteDD = "";
            String sql1 = this.obtenerSQL("SQL_REPORTE_DIARIO_DESPACHO");
            
            if( !agencias.equals("''") && !agencias.equals(""))
                reporteDD = sql1.replaceAll("#AGCPLA#", "AND p.agcpla IN ( " + agencias + " )");
            else
                reporteDD = sql1.replaceAll("#AGCPLA#", "");
            
            con = this.conectar("SQL_REPORTE_DIARIO_DESPACHO");
            st = con.prepareStatement(reporteDD );
            
            st.setString( 1, desde + " 00:00" );
            st.setString( 2, hasta + " 23:59" );
            st.setString( 3, plaveh + "%" );
            st.setString( 4, cedcon + "%" );
            st.setString( 5, despachador + "%" );
            st.setString( 6, cliente + "%" );
            
            
            //System.out.println("Reporte diario de Despacho " + st.toString());
            
            rs = st.executeQuery();
            informes = new Vector();
            auxiliar = new LinkedList();
            
            while(rs.next()){
                
                inf = new Informe();
                inf.setAgencia(rs.getString("agcpla"));
                inf.setNumpla(rs.getString("numpla"));
                inf.setFecha(rs.getString("printer_date").equals("0099-01-01")?"No Impresa":rs.getString("printer_date"));
                inf.setOrigen(rs.getString("origen"));
                inf.setDestino(rs.getString("destino"));
                inf.setPlaca(rs.getString("plaveh"));
                inf.setPlatlr( rs.getString( "platlr" ) );
                inf.setConductor(rs.getString("conductor"));
                inf.setCliente(rs.getString("nomcli"));
                inf.setTonelaje(rs.getFloat("pesoreal"));
                inf.setRemision(rs.getString("numrem"));
                inf.setStdjob(rs.getString("std_job"));
                inf.setVlrrem(rs.getFloat("vlrrem"));
                inf.setVlrpla(rs.getFloat("vlrpla"));
                inf.setAnticipo(rs.getDouble("anticipo"));
                
                //Reporter Diario
                //Unidades despachadas
                inf.setUnidadesdesp(rs.getString("udespachada"));
                //Moneda Planilla
                inf.setMonedapla(rs.getString("monedaplanilla"));
                //Moneda Anticipo
                inf.setMonedaant(rs.getString("monedaanticipo"));
                //Anticipo Maximo
                inf.setPorcentajemaximoant(rs.getDouble("porcentaje_maximo_anticipo"));
                //Moneda Remesa
                inf.setMonedarem(rs.getString("monedaremesa"));
                //Cantidad Facturada
                inf.setCantidadfacturada(rs.getDouble("cantidadfacturada"));
                //Unidad
                inf.setUnidad(rs.getString("unidad"));
                
                //Contenedores
                inf.setContenedores(rs.getString("contenedores"));
                
                inf.setDocumentos(rs.getString("documentosinternos") );
                
                String fechaplanilla = rs.getString("fecpla");
                
                
                //Despachador
                inf.setDespachador(rs.getString("despachador"));
                //Estado
                inf.setEstado(rs.getString("reg_status"));
                inf.setVlrcostoflete(rs.getFloat("total_costo"));
                inf.setVlringresoflete(rs.getFloat("total_ing"));
                inf.setCostoflete(rs.getString("extrafletes"));
                inf.setIngresoflete(rs.getString("ingextrafletes"));
                
                /*Jescandon 27-02-07*/
                inf.setTipocarga    ( rs.getString("tipo_carga") );//Tipo de Carga
                inf.setVacio        ( rs.getString("vacio" )     );//Vacio
                //Vlr Remesa
                inf.setVlr_rem_base( rs.getDouble("u_vlrrem")   );//Vlr Remesa Base 1
                inf.setVlr_rem_pro  ( rs.getDouble("u_vlrrem_prorrateado"));//Vlr Remesa Prorrateado 2
                
                /*Validaciones*/
                
                if( !inf.getVacio().equals("SI") ){
                    if( inf.getVlr_rem_base() == 0 ){
                        //No se puede realizar una division entre cero 0.
                        inf.setPor_rem_por(0);// Se asigna porcentaje de remesa prorrateado en Cero por default
                        inf.setFlag(false);
                        auxiliar.add(inf);
                    }
                    else{
                        double porcentaje_rem   = ( ( inf.getVlr_rem_pro() / inf.getVlr_rem_base() ) * 100 );// ( 2/1 )*100 3
                        inf.setPor_rem_por(  porcentaje_rem );// Porcentaje prorrateado de la Remesa
                    }
                    
                    //Vlr Planilla
                    inf.setVlr_pla_base( rs.getDouble("u_vlrpla")   );//Vlr Planilla Base 4
                    inf.setVlr_pla_pro  ( rs.getDouble("u_vlrpla_prorrateado")  );//Vlr Planilla Prorrateado 5
                    
                    /*Validaciones*/
                    if( inf.getVlr_pla_base() == 0 ){
                        //No se puede realizar una division entre cero 0.
                        inf.setPor_pla_por(0);// Se asigna porcentaje de planilla prorrateado en Cero por default
                        inf.setFlag(false);
                        auxiliar.add(inf);
                    }
                    else{
                        double porcentaje_pla   = ( ( inf.getVlr_pla_pro() / inf.getVlr_pla_base() ) * 100 );// ( 5/4 )*100 6
                        inf.setPor_pla_por(  porcentaje_pla );// Porcentaje prorrateado de la Planilla
                    }
                    
                    //Se calcula la utilidad 2 - 5
                    double utilidadbruta        = inf.getVlr_rem_pro() - inf.getVlr_pla_pro();
                    inf.setUtilidadbruta( utilidadbruta );
                    
                    if( inf.getVlr_rem_pro() == 0 ){
                        //No se puede realizar una division entre cero 0.
                        inf.setPor_utilidad_bruta(0);
                        inf.setFlag(false);
                        auxiliar.add(inf);
                    }
                    else{
                        double por_utilidadbruta    = ( ( inf.getUtilidadbruta() / inf.getVlr_rem_pro() ) * 100 )   ;
                        inf.setPor_utilidad_bruta(por_utilidadbruta);
                    }
                }
                else{
                    inf.setVlr_rem_base(0);
                    inf.setVlr_rem_pro(0);
                    inf.setPor_rem_por(0);
                    
                    inf.setVlr_pla_base(0);
                    inf.setVlr_pla_pro(0);
                    inf.setPor_pla_por(0);
                    
                    inf.setUtilidadbruta(0);
                    inf.setPor_utilidad_bruta(0);
                }//If validaciones
                
                informes.add(inf);
                
            }
            //Hilo de Errores
            log.start( usuario, auxiliar );
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL REPORTE DIARIO DE DESPACHO CGAGEN " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if ( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                    
                }
            }
            
            this.desconectar("SQL_REPORTE_DIARIO_DESPACHO");
            
        }
        
    }
    
    /**
     * Getter for property total.
     * @return Value of property total.
     */
    public int getTotal() {
        return total;
    }
    
    /**
     * Setter for property total.
     * @param total New value of property total.
     */
    public void setTotal(int total) {
        this.total = total;
    }
    /**
     * Metodo que busca una lista de remesas relacionadas a una planilla
     * @autor Ing. Karen Reales.
     * @param Numero de la planilla
     * @throws SQLException
     * @version 1.0.
     **/
    public void searchRemesas(String numpla, String numrem)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null,rs2 = null;
        //  plaIncompletas = null;
        
        try {
            
            
            con = this.conectar("SQL_BUSCARREMPLA");
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL("SQL_BUSCARREMPLA"));//m041005
                st.setString(1,numpla);
                st.setString(2,numrem);
                System.out.println("Query "+st);
                rs = st.executeQuery();
                remesas = new LinkedList();
                
                while(rs.next()){
                    Remesa r = Remesa.load(rs);
                    r.setPorcentaje(rs.getFloat("porcent"));
                    r.setUnit(rs.getString("unidad"));//jose2006-12-18
                    r.setReg_status(rs.getString("reg_status"));//m041005
                    r.setFactura(rs.getString("doc_fac"));
                    remesas.add(r);
                    //Aqui van PlaAux
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS REMESAS  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                this.desconectar("SQL_BUSCARREMPLA");
            }
        }
        
    }
    
    /**
     * Metodo para extraer los datos de distribucion de la planilla en 
     * base a lo porcentajes de la remesa
     * @autor mfontalvo
     * @param planilla, numero de la planilla
     * @throws Exception 
     * @return Vector, remesas relacionadas
     */
     public Vector obtenerDatosRPL(String numpla )throws Exception{
         PreparedStatement st    = null;
         ResultSet         rs    = null;
         String            sql   = "SQL_DATOS_RPL";
         Vector            datos = new Vector();
         try {
             st = this.crearPreparedStatement(sql);
             st.setString(1,numpla);
             rs = st.executeQuery();
             while(rs.next()){
                 Plarem p = new Plarem();
                 p.setNumpla        (rs.getString("numpla"));
                 p.setVlrpla        (rs.getDouble("vlrpla2"));
                 p.setAccount_code_c(rs.getString("account_code_c"));
                 p.setNumrem        (rs.getString("numrem"));
                 p.setAccount_code_i(rs.getString("account_code_i"));
                 p.setVlrrem        (rs.getDouble("vlrrem2"));
                 p.setPorcent       (rs.getString("porcent"));
                 datos.add(p);
             }
         } catch(Exception e) {
             throw new Exception(e.getMessage());
         } finally {
             if (st  != null) st.close();
             if (rs  != null) rs.close();
             this.desconectar(sql);

         }
         return datos;
     }
     
    
    //METODO QUE PERMITE GUARDAR LAS PLANILLAS DESPACHADAS EN LAS TABLAS Ingreso trafico y trafico
    /**
     *  Inserta un registro en trafico y en ingreso_trafico
     *  @autor Karen Reales
     *  @return String con comando
     *  @throws SQLException
     */
    public String insertTrafico() throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql = "";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null){
                
                st = con.prepareStatement(INSERT_ING_TRAFICO);
                st.setString(1, pla.getCia());
                st.setString(2, pla.getNumpla());
                st.setString(3, pla.getPlaveh());
                st.setString(4, pla.getCedcon());
                st.setString(5, pla.getNomCond());
                st.setString(6, pla.getNitpro());
                st.setString(7, pla.getNomprop());
                st.setString(8, pla.getOripla());
                st.setString(9, pla.getNomori());
                st.setString(10, pla.getDespla());
                st.setString(11, pla.getNomdest());
                st.setString(12, pla.getZona());
                st.setString(13, pla.getNomzona());
                st.setString(14, pla.getFecdsp());
                st.setString(15, pla.getDespachador());
                st.setString(16, pla.getBase());
                st.setString(17, pla.getProx_pto_control());
                st.setString(18, pla.getNom_prox_pto_control());
                st.setString(19, pla.getFec_prox_pto_control());
                st.setString(20, pla.getRuta_pla());
                st.setString(21, pla.getCelularcon());
                st.setString(22, pla.getClientes());
                st.setString(23, pla.getFecha_salida());
                st.setString(24, pla.getDespachador());
                st.setString(25, pla.getOripla());
                st.setString(26, pla.getNomori());
                st.setString(27, pla.getFecha_salida());
                st.setString(28,pla.getObservacion());
                st.setString(29, pla.getEintermedias());
                st.setString(30, pla.getNeintermedias());
                
                sql = st.toString();
                // st.executeUpdate();
                
                st = con.prepareStatement(INSERT_TRAFICO);
                st.setString(1,  pla.getCia());
                st.setString(2, pla.getNumpla());
                st.setString(3, pla.getPlaveh());
                st.setString(4, pla.getCedcon());
                st.setString(5, pla.getNitpro());
                st.setString(6, pla.getOripla());
                st.setString(7, pla.getDespla());
                st.setString(8, pla.getZona());
                st.setString(9, pla.getFecdsp());
                st.setString(10, pla.getDespachador());
                st.setString(11, pla.getBase());
                st.setString(12, pla.getProx_pto_control());
                st.setString(13, pla.getFec_prox_pto_control());
                st.setString(14, pla.getCelularcon());
                st.setString(15, pla.getClientes());
                st.setString(16, pla.getFecha_salida());
                st.setString(17, pla.getEintermedias());
                sql = sql+";"+st.toString();
                //st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL INGRESAR EL REGISTRO EN INGRESO TRAFICO Y TRAFICO" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection(this.getDatabaseName(), con );
            }
        }
        return sql;
    }
    
    public List obtenerCamposTrailer(String trailer, String planilla) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        planillas=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SQL_CAMPOS_TRAILER);
                st.setString(1, trailer);
                st.setString(2, planilla);
                rs = st.executeQuery();
                planillas = new LinkedList();
                while(rs.next()){
                    pla = new Planilla();
                    pla.setPlaveh(rs.getString("plaveh"));//placa del cabezote
                    pla.setClientes(rs.getString("cliente"));//cliente
                    pla.setPlatlr(rs.getString("platlr"));//placa trailer
                    pla.setNumpla(rs.getString("numpla"));//numero de la planilla
                    pla.setSj(rs.getString("std_job_no"));//numero del standar
                    pla.setVlrpla(rs.getFloat("vlrpla"));//valor planilla
                    pla.setNitpro(rs.getString("nitpro"));//nit proverdor
                    pla.setFechapla(rs.getDate("fecpla"));//fecha planilla
                    planillas.add(pla);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS CAMPOS DEL TRAILER " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return planillas;
    }
    
}

