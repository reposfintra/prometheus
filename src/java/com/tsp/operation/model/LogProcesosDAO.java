
package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.text.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;



public class LogProcesosDAO extends MainDAO{
    
   /* ____________________________________________________________________________________   
                                               METODOS
      ____________________________________________________________________________________  */  

    public LogProcesosDAO() {  
        super("LogProcesosDAO.xml");
    }
    
    public LogProcesosDAO(String dataBaseName) {  
        super("LogProcesosDAO.xml",dataBaseName);
    }
       
    void crearLOGPROCESO() throws SQLException{}  
    
    /**
     * M�todo que inserta un proceso
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/
 public void InsertarProceso(String proceso,int id,String descripcion, String usuario)throws SQLException {
      PreparedStatement st          = null;
      String            query       = "SQL_INSERTAR";
      try {
                      
            st = this.crearPreparedStatement(query);
            st.setString( 1, proceso);
            st.setInt   ( 2, id);
            st.setString( 3, descripcion);
            st.setString( 4, usuario);
            st.executeUpdate();       
            
      }
      catch(SQLException e){
          ////System.out.println("ERROR : InsertarProceso ");
          e.printStackTrace();
          throw new SQLException("No se pudo insertar el proceso "+ String.valueOf(id) +" -->"+ e.getMessage());
      }  
      finally{
         if(st!=null) st.close();
         this.desconectar(query);
      } 
   }
 

 
    
     /**
     * M�todo que anula un proceso
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/ 
  void AnularProceso(int id)throws SQLException {
      PreparedStatement st          = null;
      String            query       = "SQL_ANULAR";
      try {
                        
             st = this.crearPreparedStatement(query);
             st.setInt   (1,id);
             st.executeUpdate();
            
      }
      catch(SQLException e){
          ////System.out.println("ERROR : AnularProceso ");
          e.printStackTrace();
          throw new SQLException("No se pudo anular el proceso " + String.valueOf(id) +" -->"+ e.getMessage());
      }  
      finally{
         st.close();
         this.desconectar(query);
      } 
   }
    
    
    
     /**
     * M�todo que actualiza el estado de un proceso
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/
  void ActualizarProceso(String proceso,int id,String usuario, String comentario)throws SQLException {
      PreparedStatement st          = null;
      String            query       = "SQL_ACTUALIZAR";
      try {
                      
           st = this.crearPreparedStatement(query);
           st.setString( 1 , comentario);
           st.setString( 2 , proceso);
           st.setInt   ( 3 , id);
           st.setString( 4 , usuario);
           st.executeUpdate(); 
            
      }
      catch(SQLException e){
          ////System.out.println("ERROR : ActualizarProceso ");
          e.printStackTrace();
          throw new SQLException("No se pudo actualizar el proceso " + String.valueOf(id) +" -->"+ e.getMessage());
      }  
      finally{
         st.close();
         this.desconectar(query);
      } 
   }
  
    
    
      
  /**
     * M�todo que finaliza un proceso
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/
  void FinalizarProceso(String proceso,int id,String usuario, String comentario)throws Exception {
        PreparedStatement st          = null;
        String            query       = "SQL_FINALIZAR";                
        try {
            
            st = this.crearPreparedStatement(query);
            st.setString ( 1, comentario);
            st.setString ( 2, proceso);
            st.setInt    ( 3, id);
            st.setString ( 4, usuario);
            st.executeUpdate(); 
            
           
      //--- Send mail
        /*   String correo = this.getMail( usuario );
            if( !correo.equals("") ){
                Email mail = new Email();
                  mail.setEmailfrom   ( "procesos@mail.tsp.com" );
                  mail.setEmailto     ( correo);
                  mail.setEmailsubject( proceso + " : " + id);
                  mail.setEmailbody   ( comentario);
               EMailDAO.saveMail(mail); 
            }*/
            
        }
        catch(Exception e){
            ////System.out.println("ERROR : FinalizarProceso ");
            e.printStackTrace();
            throw new Exception("No se pudo Finalizar el proceso " + String.valueOf(id) +" -->"+ e.getMessage());
        }  
        finally{
             st.close();
             this.desconectar(query);
        } 
   }
    

  
  
  /**
     * M�todo que busca el email del usuario un proceso
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/
   public String getMail(String user) throws Exception{
      PreparedStatement st     = null;
      ResultSet         rs     = null;  
      String            query  = "SQL_SEARCH_MAIL_USUARIO";
      String            correo = "";      
      try{
          
          st = this.crearPreparedStatement(query);
          st.setString(1, user);
          rs = st.executeQuery();
          if( rs.next())
              correo = rs.getString(1);          
          
      } catch(SQLException e){
          throw new SQLException("No se pudo buscar el correo del usuario -->"+ e.getMessage());
      }  
      finally{
         if(st!=null) st.close();
         if(rs!=null) rs.close();
         this.desconectar(query);
      } 
      return correo;
   }
    
   
   
   
   
   
    
    
    // -----------------------------------------------------------------------------------------
  
  
   /**
     * M�todo que carga la lista de procesos segun el estado y fecha de consulta
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     * Modificado por: Luis Eduardo Frieri el 14/02/2007
     **/
  List  QueryLogProcesos(String estado, String fecha1,String fecha2, String user) throws SQLException{
        Connection        con           = null;
        PreparedStatement st            = null;
        ResultSet         rs            = null;
        
        List             listaProcesos  = null;
        String           condicion      = "";
        
        if(user.equals("ADMINISTRADOR")){
            
            String           query          = "SQL_PROCESOS_ADMIN";
            
            try {            

                 con = this.conectar(query);

                 if ( fecha1 != null && fecha2 != null && !fecha1.equals("") && !fecha2.equals("") && !fecha1.equals("null") && !fecha2.equals("null")){
                        condicion = (" AND SUBSTR (FECHA_INICIAL, 1,10) BETWEEN "+   
                                     "'"+fecha1+"'  AND  '"+fecha2+"'");
                 }

                 String sql    =   this.obtenerSQL( query ).replaceAll("#CONDICION#", condicion );
                 st            =   con.prepareStatement( sql );

                 if (estado.equals("TODO"))   st.setString(1,"%");                
                 else                         st.setString(1,estado+"%");  

                 rs=st.executeQuery();        
                 if(rs!=null){
                        listaProcesos= new LinkedList();
                        while(rs.next()){

                            LogProceso proceso  = new LogProceso();
                            proceso.setFechaInicial    ( rs.getString("fecha_inicial") );                        
                            proceso.setProceso         ( rs.getString("proceso")       );                        
                            proceso.setId              ( rs.getInt   ("id")            );                        
                            proceso.setDescripcion     ( rs.getString("descripcion")   );
                            proceso.setUsuario         ( rs.getString("usuario")       );                        
                            proceso.setEstado          ( rs.getString("estado")        );                        
                            proceso.setEstadoFinalizado( rs.getString("estado_finalizado"));                        
                            proceso.setFechaFinal      ( rs.getString("fecha_final")   );
                            proceso.setDuracion        ( rs.getString("duracion")      );

                            String  mayor              = rs.getString("mayor");
                            proceso.setColor("#ECE0D8");

                      //--- Asigno color del fondo
                            if( rs.getString("estado").equals("ACTIVO")){                            
                                if( mayor.equals("N")) proceso.setColor("#FFFF99");
                                else                   proceso.setColor("#EF9C9C");
                            }

                            listaProcesos.add(proceso); 

                        }
                  }


            }catch(SQLException e){
                ////System.out.println("ERROR : QueryLogProcesos ");
                e.printStackTrace();
                throw new SQLException("Error en la consulta de seleccion de Procesos"+" -->"+ e.getMessage() );
            }
            finally{
               if(st!=null) st.close();
               if(rs!=null) rs.close();
               this.desconectar(query);
           }
          return listaProcesos;
            
        }else{
            
            String           query          = "SQL_PROCESOS";
        
            try {            

                 con = this.conectar(query);

                 if ( fecha1 != null && fecha2 != null && !fecha1.equals("") && !fecha2.equals("") && !fecha1.equals("null") && !fecha2.equals("null")){
                        condicion = (" AND SUBSTR (FECHA_INICIAL, 1,10) BETWEEN "+   
                                     "'"+fecha1+"'  AND  '"+fecha2+"'");
                 }

                 String sql    =   this.obtenerSQL( query ).replaceAll("#CONDICION#", condicion );
                 st            =   con.prepareStatement( sql );

                 if (estado.equals("TODO"))   st.setString(1,"%");                
                 else                         st.setString(1,estado+"%");  

                 st.setString(2, user);  
                 rs=st.executeQuery();        
                 if(rs!=null){
                        listaProcesos= new LinkedList();
                        while(rs.next()){

                            LogProceso proceso  = new LogProceso();
                            proceso.setFechaInicial    ( rs.getString("fecha_inicial") );                        
                            proceso.setProceso         ( rs.getString("proceso")       );                        
                            proceso.setId              ( rs.getInt   ("id")            );                        
                            proceso.setDescripcion     ( rs.getString("descripcion")   );
                            proceso.setUsuario         ( rs.getString("usuario")       );                        
                            proceso.setEstado          ( rs.getString("estado")        );                        
                            proceso.setEstadoFinalizado( rs.getString("estado_finalizado"));                        
                            proceso.setFechaFinal      ( rs.getString("fecha_final")   );
                            proceso.setDuracion        ( rs.getString("duracion")      );

                            String  mayor              = rs.getString("mayor");
                            proceso.setColor("#ECE0D8");

                      //--- Asigno color del fondo
                            if( rs.getString("estado").equals("ACTIVO")){                            
                                if( mayor.equals("N")) proceso.setColor("#FFFF99");
                                else                   proceso.setColor("#EF9C9C");
                            }

                            listaProcesos.add(proceso); 

                        }
                  }


            }catch(SQLException e){
                ////System.out.println("ERROR : QueryLogProcesos ");
                e.printStackTrace();
                throw new SQLException("Error en la consulta de seleccion de Procesos"+" -->"+ e.getMessage() );
            }
            finally{
               if(st!=null) st.close();
               if(rs!=null) rs.close();
               this.desconectar(query);
           }
          return listaProcesos;

      }
        
        
    }
    
    /**
     * M�todo que finaliza un proceso se modifico where(agregando un nuevo filtro)
     * @autor....... mcastillo
     * @throws...... Exception
     * @version..... 1.0.
     **/
  void FinalizarProceso2(String proceso,int id,String usuario, String comentario)throws Exception {
        PreparedStatement st          = null;
        String            query       = "SQL_FINALIZAR_2";                
        try {
            
            st = this.crearPreparedStatement(query);
            st.setString ( 1, comentario);
            st.setString ( 2, proceso);
            st.setInt    ( 3, id);
            st.setString ( 4, usuario);
            st.executeUpdate(); 
            
        }
        catch(Exception e){
            ////System.out.println("ERROR : FinalizarProceso ");
            e.printStackTrace();
            throw new Exception("No se pudo Finalizar el proceso " + String.valueOf(id) +" -->"+ e.getMessage());
        }  
        finally{
             st.close();
             this.desconectar(query);
        } 
   }
  
    
 
   
  
       
       
}// FIN  DAO
