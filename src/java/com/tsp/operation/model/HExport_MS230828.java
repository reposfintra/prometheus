/*
 * HExport_MS230828.java
 *
 * Created on 22 de diciembre de 2004, 17:53
 */

package com.tsp.operation.model;


import java.io.*;
import java.util.List;
import java.util.Iterator;
import com.tsp.operation.model.beans.*;

/**
 *
 * @author  Administrador
 */
public class HExport_MS230828 extends Thread{
    
    private String Fecha;
    private String NombreArchivo;
    private List   Planillas;
        
    /** Creates a new instance of HExport_MS230828 */
    public HExport_MS230828() {
    }
    
    public void start(List Planillas, String Fecha, String Directorio) {
        this.Fecha         = Fecha.replaceAll("-", "");
        this.NombreArchivo = Directorio + "MS230828_"+  this.Fecha +".txt";
        this.Planillas     = Planillas;
        super.start();
    }
    
  public synchronized void run(){   
        try{  
            PrintWriter fp = new PrintWriter (new BufferedWriter(new FileWriter(this.NombreArchivo)));
            String Linea = "";
            Iterator it = this.Planillas.iterator();            
            while(it.hasNext()){
                DatosPlanilla datos = (DatosPlanilla) it.next();
                //Linea  = fill(datos.getNumeroPlanilla(),8,0);
                Linea  = fill(datos.getNumeroRemision(),8,0);
                Linea += fill(datos.getFechaPlanilla().replaceAll("-", ""),8,0);
                Linea += fill(datos.getSJ(),6,0);
                Linea += fill(datos.getPlacaVehiculo(),12,0);
                Linea += fill(datos.getPesoReal(),9,1);
                Linea += fill(datos.getCedulaConductor(),10,0);
                
                if (datos.getAnticipos()!=null){
                    Iterator it2 = datos.getAnticipos().iterator();
                    while (it2.hasNext()){
                        DatosDescuentosImp desc = (DatosDescuentosImp) it2.next();
                        if (desc.getCodigo().equals("PAPEL"))
                            Linea += "PAPEL" + fill(desc.getValor(),11,1); 
                        else if (desc.getCodigo().equals("DESC"))
                            Linea += "DESC "  + fill(desc.getValor(),11,1); 
                        else
                            Linea += fill(desc.getProveedorMigracion()+ datos.getCodigoAgenciaPlanilla() + desc.getConceptoMigracion(),5,0) 
                                  +  fill(desc.getValor(),11,1);                        
                    }
                }
                fp.println(Linea);
            }
            fp.close();
        }catch(Exception ex){
               ////System.out.println("Error en el hilo HExport_MS230828 : \n"+ex.getMessage());
        }
  } 
  
  public String fill(String Cadena, int Longitud, int Tipo){
      String Str = Cadena;
      switch (Tipo){
          case 0: 
              for(int i=Str.length();i<Longitud;i++, Str+=" ");
              break;
          case 1: 
              Str = Str.substring(0,Str.indexOf(".")) + Str.substring(Str.indexOf(".")+1,Str.length());
              for(int i=Str.length();i<Longitud;i++, Str="0"+Str);
              break;          
       /*   case 2:               
              if (Str.indexOf(".")==-1){
                  Str  = Str.substring(0,Str.indexOf("."));
                  Str += ".00";
              }
              for(int i=Str.length();i<Longitud;i++, Str="0"+Str);
              break;   */       
      }
      return Str.substring(0,Longitud);      
  }
  
}
