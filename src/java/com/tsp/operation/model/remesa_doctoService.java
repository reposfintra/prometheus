/*
 * remesa_doctoService.java
 *
 * Created on 12 de septiembre de 2005, 11:33 AM
 */

package com.tsp.operation.model;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.sql.*;
/**
 *
 * @author  Administrador
 */
public class remesa_doctoService {
    private remesa_doctoDAO    REMDOCDataAccess;
    private List               ListaREMDOC;
    private Vector             VectorREMDOC;
    private remesa_docto       Datos;
    private Vector             VC;//Juan 03.11.05
    private TreeMap            documents;
    private Vector docs;
    
    /** Creates a new instance of remesa_doctoService */
    public remesa_doctoService() {
        REMDOCDataAccess = new remesa_doctoDAO();
        ListaREMDOC = new LinkedList();
        VectorREMDOC = new Vector();
        Datos = new remesa_docto();
    }
    
    public void INSERTREL(String distrito, String numrem, String tipodoc, String documento, String tipodocrel, String documentorel, String destinatario, String usuario) throws Exception {
        try{
            REMDOCDataAccess.INSERT_REMDOC(distrito, numrem, tipodoc, documento, tipodocrel, documentorel, destinatario, usuario);
        }
        catch(Exception e){
            throw new Exception("Error en INSERTREL [remesa_doctoService]...\n"+e.getMessage());
        }
    }
    
    public void INSERT(String distrito, String numrem, String tipodoc, String documento, String usuario) throws Exception {
        try{
            REMDOCDataAccess.INSERT(distrito, numrem, tipodoc, documento, usuario);
        }
        catch(Exception e){
            throw new Exception("Error en INSERT [remesa_doctoService]...\n"+e.getMessage());
        }
    }
    //MOdificacion Karen 18-Jul-2006
    //MOdificacion Juan 20-02-2006
    public String INSERTDOCREL2( Vector documentos, String distrito, String numrem, String destinatario, String usuario )throws SQLException{
        String insert="";
        try{
            for (int i = 0; i < documentos.size(); i++){
                remesa_docto rem = (remesa_docto) documentos.elementAt(i);
                Vector hijos = rem.getHijos();
                if( hijos.size() >  0 ){
                    for(int j = 0; j < hijos.size();j++){
                        remesa_docto hijo = (remesa_docto)hijos.elementAt(j);
                        if( !REMDOCDataAccess.BUSCAR_RD(numrem, rem.getDestinatario(), rem.getTipo_doc(),rem.getDocumento(),
                        hijo.getTipo_doc(), hijo.getDocumento()) ){
                            if( !rem.getTipo_doc().equals("") && !rem.getDocumento().equals("") )
                                insert += REMDOCDataAccess.INSERT_REMDOC(distrito, numrem, rem.getTipo_doc(),
                                rem.getDocumento(), hijo.getTipo_doc(), hijo.getDocumento(), rem.getDestinatario(), usuario);
                        }
                    }
                }
                else{
                    if( !REMDOCDataAccess.BUSCAR_RD(numrem, rem.getDestinatario(), rem.getTipo_doc(),rem.getDocumento(), "", "") ){
                        if( !rem.getTipo_doc().equals("") && !rem.getDocumento().equals("") ){
                            insert +=REMDOCDataAccess.INSERT_REMDOC(distrito, numrem, rem.getTipo_doc(),
                            rem.getDocumento(), "", "", rem.getDestinatario(), usuario);
                        }
                    }
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("Error en INSERTDOCREL2 [remesa_doctoService]...\n"+e.getMessage());
        }
        return insert;
    }
    
    
    public void LIST(String numrem) throws Exception {
        try{
            this.ReiniciarLista();
            this.ListaREMDOC = REMDOCDataAccess.LIST(numrem);
        }
        catch(Exception e){
            throw new Exception("Error en LIST [remesa_doctoService]...\n"+e.getMessage());
        }
    }
    
    public void LISTVEC(String numrem) throws Exception {
        try{
            this.ReiniciarVector();
            this.VectorREMDOC = REMDOCDataAccess.LISTVector(numrem);
        }
        catch(Exception e){
            throw new Exception("Error en LISTVEC [remesa_doctoService]...\n"+e.getMessage());
        }
    }
    
    public void LISTTLBDOC() throws Exception {
        try{
            this.ReiniciarLista();
            this.ListaREMDOC = REMDOCDataAccess.LISTTLBDOC();
        }
        catch(Exception e){
            throw new Exception("Error en LISTTLBDOC [remesa_doctoService]...\n"+e.getMessage());
        }
    }
    
    public void UPDATE(String ndocumento, String ntipodoc, String usuario , String numrem, String documento, String tipodoc ) throws Exception {
        try{
            REMDOCDataAccess.UPDATE(ndocumento, ntipodoc, usuario, numrem, documento, tipodoc);
        }
        catch(Exception e){
            throw new Exception("Error en UPDATE [remesa_doctoService]...\n"+e.getMessage());
        }
    }
    
    public String LISTCOMBO() throws Exception {
        try{
            List LRD = REMDOCDataAccess.LISTTLBDOC();
            String lista ="";
            Iterator it = LRD.iterator();
            while (it.hasNext()){
                remesa_docto datos = (remesa_docto)it.next();
                lista += datos.getDocument_type()+":"+datos.getDocument_name()+";";
            }
            lista = lista.substring(0,lista.length()-1);
            return lista;
        }
        catch(Exception e){
            throw new Exception("Error en LISTCOMBO [remesa_doctoService]...\n"+e.getMessage());
        }
        
    }
    
    
    public String LISTSTRING(String numrem, String dest) throws Exception {
        try{
            String cadena = REMDOCDataAccess.LISTSTRING(numrem,dest);
            return cadena;
        }
        catch(Exception e){
            throw new Exception("Error en LISTSTRING [remesa_doctoService]...\n"+e.getMessage());
        }
        
    }
    
    public String LISTSTRINGDOC(String numrem) throws Exception {
        try{
            String cadena = REMDOCDataAccess.LISTSTRINGDOC(numrem);
            return cadena;
        }
        catch(Exception e){
            throw new Exception("Error en LISTSTRINGDOC [remesa_doctoService]...\n"+e.getMessage());
        }
        
    }
    public String INSERTDOC( remesa_docto remdoc ) throws SQLException{
        String insert="";
        try{
            if( !REMDOCDataAccess.BUSCAR(remdoc.getNumrem(),remdoc.getTipo_doc(),remdoc.getDocumento())){
                insert=REMDOCDataAccess.INSERT(remdoc.getDstrct(), remdoc.getNumrem(),remdoc.getTipo_doc(),remdoc.getDocumento(),remdoc.getCreation_user());
                
            }
        }
        catch(SQLException e){
            throw new SQLException("Error en INSERTDOC [remesa_doctoService]...\n"+e.getMessage());
        }
        return insert;
    }
    public String INSERTDOC( String documentos, String distrito, String numrem, String usuario ) throws SQLException{
        String insert="";
        try{
            String doc [] = documentos.split(";");
            for (int i = 0; i < doc.length; i++){
                String elem = doc[i];
                String elementos [] = elem.split(":");
                for( int j = 0; j < elementos.length; j+=2 ){
                    if(j+1 < elementos.length){
                        if( !elementos[j+1].equals("XX") )
                            if( !REMDOCDataAccess.BUSCAR(numrem,elementos[j],elementos[j+1]))
                                insert+=REMDOCDataAccess.INSERT(distrito, numrem, elementos[j], elementos[j+1], usuario);
                    }
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("Error en INSERTDOC [remesa_doctoService]...\n"+e.getMessage());
        }
        return insert;
    }
    
    
    public boolean EXISTE(String numrem, String destinatario, String td, String doc, String tdr, String docr )throws Exception {
        try{
            return REMDOCDataAccess.BUSCAR_RD(numrem, destinatario, td, doc, tdr, docr);
        }
        catch(Exception e){
            throw new Exception("Error en EXISTE [remesa_doctoService]...\n"+e.getMessage());
        }
    }
    
    public boolean EXISTE2(String numrem, String td, String doc )throws Exception {
        try{
            return REMDOCDataAccess.BUSCAR(numrem, td, doc);
        }
        catch(Exception e){
            throw new Exception("Error en EXISTE2 [remesa_doctoService]...\n"+e.getMessage());
        }
    }
    
    public String INSERTDOCREL( String documentos, String distrito, String numrem, String destinatario, String usuario )throws SQLException{
        String insert="";
        try{
            ////System.out.println(documentos);
            documentos = "0"+documentos;
            String doc [] = documentos.split("/");
            for (int i = 0; i < doc.length; i++){
                String elem = doc[i];
                String elementos [] = elem.split(";");
                String padre = elementos[0];
                String hijo = elementos[1];
                String spadre [] = padre.split(":");
                String shijo [] = hijo.split(":");
                
                
                String tipodocp = spadre[0];
                String documentop = spadre[1];
                String tipodoch = shijo[0];
                String documentoh = shijo[1];
                
                if( !documentop.equals("XX") || !documentoh.equals("XX") ){
                    if( !REMDOCDataAccess.BUSCAR_RD(numrem, destinatario, tipodocp,documentop, tipodoch, documentoh) )
                        insert +=REMDOCDataAccess.INSERT_REMDOC(distrito, numrem, tipodocp, documentop, tipodoch, documentoh, destinatario, usuario);
                }
                
            }
            
        }
        catch(SQLException e){
            throw new SQLException("Error en INSERTDOCREL [remesa_doctoService]...\n"+e.getMessage());
        }
        return insert;
    }
    
    
    
    /*public void INSERTDOCREL2( String documentos, String distrito, String numrem, String destinatario, String usuario )throws SQLException{
        try{
            ////System.out.println(documentos);
            documentos = "0"+documentos;
            String doc [] = documentos.split("/");
            for (int i = 0; i < doc.length; i++){
                String elem = doc[i];
                String elementos [] = elem.split(";");
                String padre = elementos[0];
                String hijo = elementos[1];
                String spadre [] = padre.split(":");
                String shijo [] = hijo.split(":");
     
     
                String tipodocp = spadre[0];
                String documentop = spadre[1];
                String tipodoch = shijo[0];
                String documentoh = shijo[1];
     
                tipodocp = (tipodocp.indexOf("�") >= 0 )?tipodocp.split("�")[1]:tipodocp;
     
                if( !documentop.equals("XX") || !documentoh.equals("XX") ){
                    if( !REMDOCDataAccess.BUSCAR_RD(numrem, destinatario, tipodocp,documentop, tipodoch, documentoh) )
                        REMDOCDataAccess.INSERT_REMDOC2(distrito, numrem, tipodocp, documentop, tipodoch, documentoh, destinatario, usuario);
                }
     
            }
     
        }
        catch(SQLException e){
            throw new SQLException("Error en INSERTDOCREL [remesa_doctoService]...\n"+e.getMessage());
        }
    }*/
    
    
    
    public void UPDATEDOCREL(String documentos, String usuario , String numrem, String destinatario ) throws Exception {
        try{
            ////System.out.println("documentos");
            String doc [] = documentos.split("/");
            for (int i = 0; i < doc.length; i++){
                String elem = doc[i];
                
                String elementos [] = elem.split("%");
                
                String modificado = elementos[0];
                String original = elementos[1];
                
                
                String smodificado [] = modificado.split("�");
                String soriginal [] = original.split("�");
                
                
                /*OBTENGO LA PAREJA PADRE MODIFICADO*/
                String papamodificado = smodificado[0];
                String tipodocm = papamodificado.split(":")[0];
                String docm = papamodificado.split(":")[1];
                
                
                String hijosmodificado = smodificado[1].substring( 0 , smodificado[1].length() - 1);
                
                
                /*OBTENGO LA PAREJA PADRE ORIGINAL*/
                String papaoriginal = soriginal[0];
                String tipodoco = papaoriginal.split(":")[0];
                String doco = papaoriginal.split(":")[1];
                
                
                
                String hijosoriginal = soriginal[1].substring( 0, soriginal[1].length() - 1 );
                
                //OBTENGO LOS HIJOS
                for( int j = 0 ; j < hijosmodificado.length(); j++){
                    String hijosm [] = hijosmodificado.split(";");
                    String hijoso [] = hijosoriginal.split(";");
                    
                    //RECORRO LOS HIJOS
                    for( int k = 0 ; k < hijosm.length ; k++){
                        
                        String hm = hijosm[k];
                        String ho = hijoso[k];
                        
                        String tipodocrelm = hm.split(":")[0];
                        String docrelm = hm.split(":")[1];
                        
                        String tipodocrelo = ho.split(":")[0];
                        String docrelo = ho.split(":")[1];
                        
                        if( !tipodoco.equals(tipodocm) || !doco.equals(docm) || !tipodocrelo.equals(tipodocrelm) || !docrelo.equals(docrelm)){
                            if( REMDOCDataAccess.BUSCAR_RD(numrem, destinatario,tipodoco, doco, tipodocrelo,docrelo))
                                REMDOCDataAccess.UPDATEREL(tipodocm, docm, tipodocrelm, docrelm, usuario, numrem, tipodoco, doco, tipodocrelo,docrelo);
                        }
                    }
                    
                }
                
                
                /*if( !ntipodoc.equals(tipodoc) || !ndocumento.equals(documento)){
                    REMDOCDataAccess.UPDATE(ndocumento, ntipodoc, usuario, numrem, documento, tipodoc);
                }*/
            }
            
        }
        catch(Exception e){
            throw new Exception("Error en UPDATEDOCREL [remesa_doctoService]...\n"+e.getMessage());
        }
    }
    
    public void ReiniciarDato(){
        this.Datos = null;
    }
    public void ReiniciarLista(){
        this.ListaREMDOC = null;
    }
    
    public remesa_docto getDato(){
        return this.Datos;
    }
    
    public List getList(){
        return this.ListaREMDOC;
    }
    
    public Vector getVector(){
        return this.VectorREMDOC;
    }
    
    public void ReiniciarVector(){
        this.VectorREMDOC = null;
    }
    
    //Rodrigo
    public void Listar(String numrem) throws Exception {
        try{
            this.ReiniciarLista();
            this.ListaREMDOC = REMDOCDataAccess.Listar(numrem);
        }
        catch(Exception e){
            throw new Exception("Error en Listar [remesa_doctoService]...\n"+e.getMessage());
        }
    }
    public java.util.Vector getDocumentos() {
        return REMDOCDataAccess.getDocumentos();
    }
    
    /**
     * Setter for property documentos.
     * @param documentos New value of property documentos.
     */
    public void setDocumentos(java.util.Vector documentos) {
        
        REMDOCDataAccess.setDocumentos(documentos);
    }
    
    
    
    
    public void MigrarDocumentos(String numrem) throws Exception{
        REMDOCDataAccess.MigrarDocumentos(numrem);
    }
    
    //Juan 04.11.05
    public void ReiniciarVec(){
        this.VC = null;
    }
    
    public Vector getVec(){
        return this.VC;
    }
    
    public void LISTVC(String numrem, String destinatario) throws Exception {
        try{
            this.ReiniciarVec();
            this.VC = REMDOCDataAccess.LISTVC(numrem, destinatario);
        }
        catch(Exception e){
            throw new Exception("Error en LISTVC [remesa_doctoService]...\n"+e.getMessage());
        }
    }
    
    
    public void DELETEREL(String numrem, String destinatario ) throws Exception {
        try{
            REMDOCDataAccess.DELETEREL(numrem, destinatario);
        }
        catch(Exception e){
            throw new Exception("Error en DELETE [remesa_doctoService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Getter for property documents.
     * @return Value of property documents.
     */
    public java.util.TreeMap getDocuments() {
        return documents;
    }
    
    /**
     * Setter for property documents.
     * @param documents New value of property documents.
     */
    public void setDocuments(java.util.TreeMap documents) {
        this.documents = documents;
    }
    /**
     * Obtiene un objeto <code>TreeMap</code> con los tipos de documentos de una remesa
     * @autor Ing. Tito Andr�s Maturana
     * @version 1.0
     * @throws SQLException si ocurre una excepcion con la conexion de la Base de Datos
     * @throws Exception Si ocurre una excepci�n en general
     */
    public void loadDocumentos() throws SQLException, Exception {
        this.LISTTLBDOC();
        Object[] lista = this.getList().toArray();
        this.documents = new TreeMap();
        
        for( int i=0; i<lista.length; i++ ){
            remesa_docto datos = new remesa_docto();
            datos = (remesa_docto) lista[i];
            this.documents.put(datos.getDocument_name(), datos.getDocument_type());
        }
    }
    /**
     * Procedimiento de busqueda de documentos relacionados con una remesa sin destinatario
     * @autor: Ing. Henry A. Osorio Gonzalez
     * @Modificado:  Ing. Diogenes Bastidas
     * @fecha:  2006-04-11
     * @see:   buscarDocumentosRemesa    remesa_doctoDAO
     * @params Remesa ...... Remesa a Consultar
     * @throws SQLException.
     */
    public void buscarDocumentosRemesa(String Remesa, String dsctrt)throws SQLException{
        REMDOCDataAccess.buscarDocumentosRemesa(Remesa,dsctrt);
    }
    
    
    /**
     * buscarDestinatariosRemesa busca los destinatario con los documentos
     * @autor: Ing. Diogenes Bastidas
     * @params Remesa ...... Remesa a Consultar
     * @throws SQLException.
     */
    public void buscarDestinatariosRemesa(String Remesa, String dsctrt)throws SQLException{
        REMDOCDataAccess.buscarDestinatariosRemesa(Remesa, dsctrt);
    }
    
    /**
     * Getter for property destinatariosdoc.
     * @return Value of property destinatariosdoc.
     */
    public java.util.Vector getDestinatariosdoc() {
        return REMDOCDataAccess.getDestinatariosdoc();
    }
    /**/
    public void INSERTDOCREL( Vector documentos, String distrito, String numrem, String destinatario, String usuario )throws SQLException{
        try{
            for (int i = 0; i < documentos.size(); i++){
                remesa_docto rem = (remesa_docto) documentos.elementAt(i);
                Vector hijos = rem.getHijos();
                for(int j = 0; j < hijos.size();j++){
                    remesa_docto hijo = (remesa_docto)hijos.elementAt(j);
                    if( !REMDOCDataAccess.BUSCAR_RD(numrem, destinatario, rem.getTipo_doc(),rem.getDocumento(), hijo.getTipo_doc(), hijo.getDocumento()) )
                        if( !rem.getTipo_doc().equals("") && !rem.getDocumento().equals("")){
                            REMDOCDataAccess.INSERT_REMDOC2(distrito, numrem, rem.getTipo_doc(), rem.getDocumento(), hijo.getTipo_doc(), hijo.getDocumento(), destinatario, usuario);
                            REMDOCDataAccess.UpdateTieneDoc(numrem);
                        }
                    
                    
                }
                if( hijos.size() == 0 ){
                    if( !REMDOCDataAccess.BUSCAR_RD(numrem, destinatario, rem.getTipo_doc(),rem.getDocumento(), "", "") )
                        if( !rem.getTipo_doc().equals("") && !rem.getDocumento().equals("") ){
                            REMDOCDataAccess.INSERT_REMDOC2(distrito, numrem, rem.getTipo_doc(), rem.getDocumento(), "", "", destinatario, usuario);
                            REMDOCDataAccess.UpdateTieneDoc(numrem);
                        }
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("Error en INSERTDOCREL [remesa_doctoService]...\n"+e.getMessage());
        }
    }
    
    
    
    
    
    public void INSERTDOCMAS( String documentos, String distrito, String numrem, String usuario ) throws SQLException{
        try{
            String doc [] = documentos.split(";");
            for (int i = 0; i < doc.length; i++){
                String elem = doc[i];
                String elementos [] = elem.split(":");
                for( int j = 0; j < elementos.length; j+=2 ){
                    if(j+1 < elementos.length){
                        if( !elementos[j+1].equals("XX") )
                            if( !REMDOCDataAccess.BUSCAR(numrem,elementos[j],elementos[j+1])){
                                REMDOCDataAccess.INSERTMAS(distrito, numrem, elementos[j], elementos[j+1], usuario);
                                REMDOCDataAccess.UpdateTieneDoc(numrem);
                            }
                    }
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("Error en INSERTDOC [remesa_doctoService]...\n"+e.getMessage());
        }
        
    }
    
    
    public void UPDATEDOC(String documentos, String usuario , String numrem ) throws Exception {
        try{
            String doc [] = documentos.split("/");
            for (int i = 0; i < doc.length; i++){
                String elem = doc[i];
                String elementos [] = elem.split(";");
                String original = elementos[0];
                String copia = elementos[1];
                String soriginal [] = original.split(":");
                String scopia [] = copia.split(":");
                
                
                String tipodoc    = soriginal[0];
                String documento  = soriginal[1];
                String ntipodoc   = scopia[0];
                String ndocumento = scopia[1];
                
                if( !ntipodoc.equals(tipodoc) || !ndocumento.equals(documento)){
                    if( REMDOCDataAccess.BUSCAR(numrem, tipodoc, documento) )
                        REMDOCDataAccess.UPDATE(ndocumento, ntipodoc, usuario, numrem, documento, tipodoc);
                    REMDOCDataAccess.UpdateTieneDoc(numrem);
                }
            }
            
        }
        catch(Exception e){
            throw new Exception("Error en INSERTDOCREL [remesa_doctoService]...\n"+e.getMessage());
        }
    }
    /////////////////////////////////ORDEN DE CARGUE///////////////////////////////////////////
    /**
     * buscarDocumentOC busca un documento especifico relacionado a una orden de cargue
     * @autor: Ing.Karen Reales
     * @params numrem : Numero de la remesa
     * @params destinatario: Codigo del destinatario
     * @params td: Tipo de documento
     * @params doc: Documento
     * @params tdr: Tipo de documento relacinado
     * @params docr: Documento relacionado
     * @return true o false si existe o no el documento
     * @throws SQLException.
     */
    public boolean buscarDocumentOC(String numrem, String destinatario, String td, String doc, String tdr, String docr ) throws SQLException {
        return REMDOCDataAccess.buscarDocumentOC(numrem, destinatario, td, doc, tdr, docr);
    }
    
    /**
     * insertOCargue_docto inserta un vector de documentos relacionados a la orden de cargue
     * @autor: Ing.Karen Reales
     * @params documentos : Vector con objetos de tipo Remesa_docto
     * @params destinatario: Codigo del destinatario
     * @params distrito: Distrito del usuario
     * @params numrem: Numero de la remesa
     * @params usuario: Login del usuario
     * @return String con comando SQL
     * @throws SQLException.
     */
    public String insertOCargue_docto( Vector documentos, String distrito, String numrem, String destinatario, String usuario )throws SQLException{
        String insert="";
        try{
            for (int i = 0; i < documentos.size(); i++){
                remesa_docto rem = (remesa_docto) documentos.elementAt(i);
                Vector hijos = rem.getHijos();
                if( hijos.size() >  0 ){
                    for(int j = 0; j < hijos.size();j++){
                        remesa_docto hijo = (remesa_docto)hijos.elementAt(j);
                        if( !REMDOCDataAccess.buscarDocumentOC(numrem, rem.getDestinatario(), rem.getTipo_doc(),rem.getDocumento(),
                        hijo.getTipo_doc(), hijo.getDocumento()) ){
                            if( !rem.getTipo_doc().equals("") && !rem.getDocumento().equals("") && !hijo.getDocumento().equals("") && !hijo.getTipo_doc().equals("") )
                                insert += REMDOCDataAccess.insertOCDocument(distrito, numrem, rem.getTipo_doc(),
                                rem.getDocumento(), hijo.getTipo_doc(), hijo.getDocumento(), rem.getDestinatario(), usuario);
                        }
                    }
                }
                else{
                    if( !REMDOCDataAccess.buscarDocumentOC(numrem, rem.getDestinatario(), rem.getTipo_doc(),rem.getDocumento(), "", "") ){
                        if( !rem.getTipo_doc().equals("") && !rem.getDocumento().equals("") ){
                            insert +=REMDOCDataAccess.insertOCDocument(distrito, numrem, rem.getTipo_doc(),
                            rem.getDocumento(), "", "", rem.getDestinatario(), usuario);
                        }
                    }
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("Error en insertOCargue_docto [remesa_doctoService]...\n"+e.getMessage());
        }
        return insert;
    }
    
    /**
     * listarDocOCargue Lista los documentos relacionados a una orden de cargue
     * @autor: Ing.Karen Reales
     * @params ocargue : Numero de la orden de cargue
     * @params destinatario: Codigo del destinatario
     * @throws SQLException.
     */
    public void listarDocOCargue(String ocargue, String destinatario) throws Exception {
        try{
            this.ReiniciarVec();
            this.VC = REMDOCDataAccess.listaDocOCargue(ocargue, destinatario);
        }
        catch(Exception e){
            throw new Exception("Error en listarDocOCargue [remesa_doctoService]...\n"+e.getMessage());
        }
    }
    /**
     * DeleteDocOcargue Borra de la base de datos los documentos internos relacionados a una
     * orden de carga y un destinatario
     * @autor: Ing.Karen Reales
     * @params ocargue : Numero de la orden de cargue
     * @params destinatario: Codigo del destinatario
     * @throws SQLException.
     */
    public void DeleteDocOcargue(String ocargue, String destinatario ) throws Exception {
        try{
            REMDOCDataAccess.DeleteDocsOcargue(ocargue, destinatario);
        }
        catch(Exception e){
            throw new Exception("Error en DELETE [remesa_doctoService]...\n"+e.getMessage());
        }
    }
    /**
     * insertImpoExpo inserta un vector de documentos relacionados a la orden de cargue
     * @autor: Ing.Karen Reales
     * @params documentos : Vector con objetos de tipo Remesa_docto
     * @params destinatario: Codigo del destinatario
     * @params distrito: Distrito del usuario
     * @params numrem: Numero de la remesa
     * @params usuario: Login del usuario
     * @return String con comando SQL
     * @throws SQLException.
     */
    public String insertImpoExpo(String distrito, String tipodoc, String documento,  String usuario,String fecha_sia,String fecha_eta,String descripcion ,String codcli) throws SQLException {
        return REMDOCDataAccess.insertImpoExpo(distrito, tipodoc,documento,usuario,fecha_sia,fecha_eta,descripcion ,codcli);
    }
    /**
     * buscarImpoExpo Metodo para buscar los datos de una importacion especifica
     * @autor: Ing.Karen Reales
     * @return String con comando SQL
     * @throws SQLException.
     */
    public remesa_docto buscarImpoExpo(String distrito,  String tipodoc, String documento) throws SQLException {
        return REMDOCDataAccess.buscarImpoExpo(distrito, tipodoc, documento);
    }
    /**
     * relacionImpoExpo inserta una relacion entre remesa e impoexpo
     * @autor: Ing.Karen Reales
     * @params documentos : Vector con objetos de tipo Remesa_docto
     * @params destinatario: Codigo del destinatario
     * @params distrito: Distrito del usuario
     * @params numrem: Numero de la remesa
     * @params usuario: Login del usuario
     * @return String con comando SQL
     * @throws SQLException.
     */
    public String relacionImpoExpo(String distrito, String numrem, String tipodoc, String documento,  String usuario) throws SQLException {
        return REMDOCDataAccess.relacionImpoExpo(distrito, numrem, tipodoc, documento, usuario);
    }
    /**
     * listaDocs Metodo para buscar la lista de documentos relacionados a una remesa.
     * @autor: Ing.Karen Reales
     * @return String con comando SQL
     * @throws SQLException.
     */
    public Vector listaDocs(String numrem) throws SQLException {
        return REMDOCDataAccess.listaDocs(numrem);
    }
    /**
     * borrarDocs Metodo para borrar la lista de documentos relacionados a una remesa que no esten
     * relacionados a un destinatario.
     * @autor: Ing.Karen Reales
     * @return String con comando SQL
     * @throws SQLException.
     */
    public String borrarDocs(String dstrct,String numrem) throws SQLException {
        return REMDOCDataAccess.borrarDocs(dstrct,numrem);
    }
    /**
     * borrarRelacion Metodo para borrar la relacion entre remesa e importacion
     * relacionados a un destinatario.
     * @autor: Ing.Karen Reales
     * @return String con comando SQL
     * @throws SQLException.
     */
    public String borrarRelacion(String dstrct,String numrem) throws SQLException {
        return REMDOCDataAccess.borrarRelacion(dstrct, numrem);
        
    }
    /**
     * updateImpoExpo inserta un vector de documentos relacionados a la orden de cargue
     * @autor: Ing.Karen Reales
     * @params documentos : Vector con objetos de tipo Remesa_docto
     * @params destinatario: Codigo del destinatario
     * @params distrito: Distrito del usuario
     * @params numrem: Numero de la remesa
     * @params usuario: Login del usuario
     * @return String con comando SQL
     * @throws SQLException.
     */
    public String updateImpoExpo(String distrito, String tipodoc, String documento,  String usuario,String fecha_sia,String fecha_eta,String descripcion) throws SQLException {
        return REMDOCDataAccess.updateImpoExpo(distrito, tipodoc,documento,usuario,fecha_sia,fecha_eta,descripcion);
    }
    
    /**
     * Getter for property docs.
     * @return Value of property docs.
     */
    public java.util.Vector getDocs() {
        return docs;
    }
    
    /**
     * Setter for property docs.
     * @param docs New value of property docs.
     */
    public void setDocs(java.util.Vector docs) {
        this.docs = docs;
    }
    
    /**
     * ListDocumentosDestinatarios Metodo para buscar la lista de documentos relacionados a una remesa y un destinatario.
     * @autor: Ing.Karen Reales
     * @return String con comando SQL
     * @throws SQLException.
     */
    public void ListDocumentosDestinatarios(String numrem, String destinatario) throws Exception {
        try{
            this.ReiniciarVec();
            this.VC = REMDOCDataAccess.ListDocumentosDestinatarios(numrem, destinatario);
        }
        catch(Exception e){
            throw new Exception("Error en ListDocumentosDestinatarios [remesa_doctoService]...\n"+e.getMessage());
        }
    }
    
    
    
    /**
     * DeleteDocumentosRelacionados Metodo para los documentos relacionados a una remesa y un destinatario
     * @autor: Ing.Juan Manuel Escandon
     * @return String con comando SQL
     * @throws SQLException.
     */
    public void DeleteDocumentosRelacionados( Vector documentos, String distrito, String numrem, String destinatario )throws SQLException{
        try{
            for (int i = 0; i < documentos.size(); i++){
                remesa_docto rem = (remesa_docto) documentos.elementAt(i);
                Vector hijos = rem.getHijos();
                for(int j = 0; j < hijos.size();j++){
                    remesa_docto hijo = (remesa_docto)hijos.elementAt(j);
                    REMDOCDataAccess.deleteDocumentosDestinatarios(numrem, rem.getTipo_doc(), rem.getDocumento(),hijo.getTipo_doc(), hijo.getDocumento(), destinatario, distrito);
                }
                if( hijos.size() == 0 ){
                    REMDOCDataAccess.deleteDocumentosDestinatarios(numrem, rem.getTipo_doc(), rem.getDocumento(), "", "", destinatario, distrito);
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("Error en INSERTDOCREL [remesa_doctoService]...\n"+e.getMessage());
        }
    }
    
    public void deleteDocs(String numrem, String tipo, String documento ) throws SQLException {
        REMDOCDataAccess.deleteDocs(numrem, tipo, documento);
    }
    
    // LREALES - MODIFICADO 5 DICIEMBRE 2006
    
    /** Funcion publica que obtiene el metodo getVector del DAO */
    public Vector getVectorImpoExpo() throws Exception {
        
        return REMDOCDataAccess.getVector();
        
    }
    
    /** Funcion publica que obtiene el metodo buscarCodCliRemesa del DAO */
    public String buscarCodCliRemesa( String numrem ) throws Exception {
        
        return REMDOCDataAccess.buscarCodCliRemesa( numrem );
        
    }
    
    /** Funcion publica que obtiene el metodo buscarUnImpoExpo del DAO */
    public void buscarUnImpoExpo( String distrito, String tipo_documento, String documento ) throws Exception {
        
        REMDOCDataAccess.buscarUnImpoExpo( distrito, tipo_documento, documento );
        
    }
    
    /** Funcion publica que obtiene el metodo insertarImpoExpo del DAO */
    public void insertarImpoExpo( String distrito, String tipo_documento, String documento, String fecha_sia, String fecha_eta, String descripcion, String usuario, String codcli ) throws Exception {
        
        REMDOCDataAccess.insertarImpoExpo( distrito, tipo_documento, documento, fecha_sia, fecha_eta, descripcion, usuario, codcli );
        
    }
    
    /** Funcion publica que obtiene el metodo actualizarImpoExpo del DAO */
    public void actualizarImpoExpo( String fecha_sia, String fecha_eta, String descripcion, String user_update, String distrito, String tipo_documento, String documento ) throws Exception {
        
        REMDOCDataAccess.actualizarImpoExpo( fecha_sia, fecha_eta, descripcion, user_update, distrito, tipo_documento, documento );
        
    }
    
    ////*****************************************************<MOdificaciones>*************************************
    ////*****************************************************<MOdificaciones>*************************************
    /**
     *Metodo que busca los docuemntos de la remesa
     *@autor: Diogenes Bastidas
     *@param: numrem Numero de la remesa
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void DocumentosRemesa(String numrem) throws SQLException{
        REMDOCDataAccess.DocumentosRemesa(numrem);
    }
    
    public void INSERTARDOC( Vector vecdoc, String distrito, String numrem, String usuario ) throws Exception{
        try{
            for(int i=0; i<vecdoc.size();i++){
                remesa_docto info = (remesa_docto) vecdoc.get(i);
                if( !REMDOCDataAccess.BUSCAR(numrem,info.getTipo_doc(),info.getDocumento())){
                    REMDOCDataAccess.INSERTMAS(distrito, numrem, info.getTipo_doc(),info.getDocumento(), usuario);
                    REMDOCDataAccess.UpdateTieneDoc(numrem);
                }
                else{
                    REMDOCDataAccess.UPDATE(info.getDocumento(), info.getTipo_doc(), usuario, numrem, info.getDocumento(), info.getTipo_doc());
                }
                //inserta en impo_expo
                if(info.getImportacion().equals("S") || info.getExportacion().equals("S")){
                    String codcli = REMDOCDataAccess.buscarCodCliRemesa( numrem );
                    
                    REMDOCDataAccess.buscarUnImpoExpo( distrito, info.getTipo_doc(), info.getDocumento() );
                    Vector vecimp = REMDOCDataAccess.getVector();
                    if ( vecimp.size() > 0 ) {
                        REMDOCDataAccess.actualizarImpoExpo(info.getFecha_sia(), info.getFecha_eta(), info.getDescripcion_cga(), usuario, distrito, info.getTipo_doc(), info.getDocumento() );
                    } else {
                        REMDOCDataAccess.insertarImpoExpo( distrito, info.getTipo_doc(), info.getDocumento(), info.getFecha_sia(), info.getFecha_eta(), info.getDescripcion_cga(), usuario, codcli );
                    }
                }
                
            }
        }
        catch(Exception e){
            throw new SQLException("Error en INSERTDOC [remesa_doctoService]...\n"+e.getMessage());
        }
        
    }
    
    /**
     *Metodo que borra los documento de impoexpo
     *@autor: Diogenes Bastidas
     *@param: distrito,tipo_documento, documento
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void borrarImpoExpo( String distrito, String tipo_documento, String documento ) throws Exception {
        try{
            REMDOCDataAccess.borrarImpoExpo(distrito,tipo_documento,documento);
        } catch( Exception e ) {
            e.printStackTrace();
            throw new Exception( "ERROR DURANTE 'borrarImpoExpo' - [remesa_doctoDAO].. " + e.getMessage() );
        }
    }
    
    
    
    public String MODIFICARDOC( Vector vecdoc, String distrito, String numrem, String usuario ) throws Exception{
        String SQL="",SQL2="";
        try{
            
            for(int i=0; i<vecdoc.size();i++){
                remesa_docto info = (remesa_docto) vecdoc.get(i);
                remesa_docto doc_impo=REMDOCDataAccess.buscarImpoExpo(distrito,info.getTipo_doc(),info.getDocumento());
                if(doc_impo!=null){
                    
                    SQL += REMDOCDataAccess.SQL_borrarImpoExpo(distrito, info.getTipo_doc_rel(), info.getDocumento_rel());
                    //System.out.println("->"+SQL);
                }
                //inserta en impo_expo
                SQL2 += REMDOCDataAccess.INSERT(distrito, numrem, info.getTipo_doc(), info.getDocumento(),usuario);
                if(info.getImportacion().equals("S") || info.getExportacion().equals("S")){
                    String codcli = REMDOCDataAccess.buscarCodCliRemesa( numrem );
                    SQL2 += REMDOCDataAccess.insertImpoExpo( distrito, info.getTipo_doc(), info.getDocumento(), usuario,info.getFecha_sia(), info.getFecha_eta(), info.getDescripcion_cga(),  codcli );
                }
                
            }
        }
        catch(Exception e){
            throw new SQLException("Error en INSERTDOC [remesa_doctoService]...\n"+e.getMessage());
        }
        
        return SQL+SQL2;
    }
    
}
