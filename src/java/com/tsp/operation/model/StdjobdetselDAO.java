/************************************************************************************
 * Nombre clase :                 StdjobdetselDAO.java                              *
 * Descripcion :                  Clase que maneja los DAO ( Data Access Object )   *
 *                                los cuales contienen los metodos que interactuan  *
 *                                con la BD para manipular las diferentes funciones *
 *                                para realizar despacho.
 * Autor :                        Ing. karen Reales                                 *
 * Fecha :                        30 de enero de 2006, 10:00 AM                     *
 * Version :                      1.0                                               *
 * Copyright :                    Fintravalores S.A.                           *
 ************************************************************************************/

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

public class StdjobdetselDAO{
    
    StdjobdetselDAO(){
        
    }
    
    //Campos de Standard Job
    private List standardProy;
    private Stdjobdetsel std;
    private Stdjobcosto sc;
    private Vector stdjob;
    private List costo;
    private List standards;
    private TreeMap ciudadesOri;
    private TreeMap stdjobTree;
    private TreeMap ciudadesDest;
    
    private String SQL_BUSCAR="Select  s.*,porcentaje_maximo_anticipo:: NUMERIC(10,1) as pa , '000'||SUBSTR(s.std_job_no,1,3) as customer_id from stdjob s where s.std_job_no = ?  and s.wo_type<>'IN'";
    
    
    /**
     * Metodo getStandardProy, retorna el valor del atributo standardProy
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    List getStandardProy(){
        
        return standardProy;
    }
    
    /**
     * Metodo getCosto, retorna el valor del atributo costo
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public List getCosto(){
        
        return costo;
    }
    
    /**
     * Metodo getStandards, retorna el valor del atributo standards
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public List getStandards(){
        
        return standards;
    }
    
    /**
     * Metodo getStdjob, retorna el valor del atributo stdjob
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public Vector getStdjob(){
        
        return stdjob;
    }
    /**
     * Metodo getStandardDetSel, retorna el valor del atributo std
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    Stdjobdetsel getStandardDetSel(){
        return std;
    }
    /**
     * Metodo setStdjob, setea el valor del atributo std
     * @autor : Ing. Karen Reales
     * @param : Objeto Stdjobdetsel
     * @version : 1.0
     */
    public void setStdjob(Stdjobdetsel std){
        this.std=std;
    }
    
    /**
     * Metodo getStandardDetSel, retorna el valor del atributo sc
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public Stdjobcosto getStdjobcosto(){
        return sc;
    }
    
    /**
     * Metodo getCiudadesOri, retorna el valor del atributo ciudadesOri
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public TreeMap getCiudadesOri(){
        
        return ciudadesOri;
    }
    
    /**
     * Metodo getCiudadesDest, retorna el valor del atributo ciudadesDest
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public TreeMap getCiudadesDest(){
        
        return ciudadesDest;
    }
    
    /**
     * Metodo getStdjobTree, retorna el valor del atributo stdjobTree
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public TreeMap getStdjobTree(){
        
        return stdjobTree;
    }
    
    /**
     * Metodo setStdjobTree, setea el valor del atributo stdjobTree
     * @autor : Ing. Karen Reales
     * @param : Objeto TreeMap
     * @version : 1.0
     */
    public void setStdjobTree(TreeMap t){
        
        stdjobTree=t;
    }
    
    /**
     * Metodo setCiudadesDest, setea el valor del atributo ciudadesDest
     * @autor : Ing. Karen Reales
     * @param : Objeto TreeMap
     * @version : 1.0
     */
    public void setCiudadesDest(TreeMap t){
        
        ciudadesDest=t;
    }
    
    /**
     * Metodo setCiudadesOri, setea el valor del atributo ciudadesOri
     * @autor : Ing. Karen Reales
     * @param : Objeto TreeMap
     * @version : 1.0
     */
    public void setCiudadesOri(TreeMap t){
        
        ciudadesOri=t;
    }
    
    /**
     * Metodo searchStandardDetSel, busca un Standard determinado en la tabla stdjobdetsel y carga el objeto
     * global std con la informacion que arrojada por la busqueda
     * @autor : Ing. Karen Reales
     * @param : String sj
     * @version : 1.0
     */
    void searchStandardDetSel(String sj)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        std=null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("Select  s.*, '000'||SUBSTR(s.sj,1,3) as customer_id from stdjobdetsel s where s.sj = ? and wo_type<>'IN'  ");
                st.setString(1,sj);
                
                rs = st.executeQuery();
                
                if(rs.next()){
                    std=Stdjobdetsel.load(rs);
                    std.setCliente(rs.getString("customer_id"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL STANDARD DETSEL" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo searchRutasCliente, busca la rutas asociadas aun cliente determinado y carga un TreeMap
     * con la informacion que es arrojada por la busqueda
     * @autor : Ing. Karen Reales
     * @param : String cliente
     * @version : 1.0
     */
    public void searchRutasCliente(String client)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        std=null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                
                st = con.prepareStatement("select  DISTINCT s.origin_code as origen, " +
                "	co.nomciu as nomorig " +
                " from 	stdjob s, " +
                "	ciudad co," +
                "       cliente c " +
                " where s.codcli = ?" +
                "       and s.tipo='N'" +
                "       AND S.wo_type<>'IN'" +
                "	and co.codciu=s.origin_code" +
                "       and s.codcli = c.codcli" +
                "       and c.estado !='I'" +
                " order by co.nomciu");
                st.setString(1,client);
                //System.out.println("Query :" +st.toString());
                rs = st.executeQuery();
                
                ciudadesOri = new TreeMap();
                ciudadesOri.put("Seleccione el origen", "");
                while(rs.next()){
                    ciudadesOri.put(rs.getString("nomorig"), rs.getString("origen"));
                }
                
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS RUTAS CLIENTES" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo searchDestinosOrigen, busca los destinos asociadas aun cliente determinado y un origen, carga un TreeMap
     * con la informacion que es arrojada por la busqueda
     * @autor : Ing. Karen Reales
     * @param : String cliente, String origen
     * @version : 1.0
     */
    public void searchDestinosOrigen(String client, String ori)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        std=null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                
                st = con.prepareStatement("select DISTINCT s.destination_code as destino, " +
                "	cd.nomciu as nomdes " +
                " from 	stdjob s, " +
                "	ciudad co, " +
                "	ciudad cd " +
                " where 	s.codcli = ? " +
                "       and s.origin_code=?" +
                "	and co.codciu=s.origin_code " +
                "	and cd.codciu=s.destination_code" +
                "       AND S.wo_type<>'IN'" +
                " order by cd.nomciu");
                st.setString(1,client);
                st.setString(2,ori);
                rs = st.executeQuery();
                ciudadesDest = new TreeMap();
                while(rs.next()){
                    ciudadesDest.put(rs.getString("nomdes"), rs.getString("destino"));
                }
                
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS DETINOS DE UN ORIGEN" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo searchStandaresOrDest, Carga un TreeMap de Standares, asociados a un Cliente, Origen y
     * Destino determinado, con la informacion que es arrojada por la busqueda
     * @autor : Ing. Karen Reales
     * @param : String cliente, String origen, String destino
     * @version : 1.0
     */
    public void searchStandaresOrDest(String client, String ori, String dest)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        std=null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                
                st = con.prepareStatement("select distinct s.std_job_no, " +
                "	s.std_job_desc, " +
                "	s.origin_code," +
                "	s.destination_code " +
                " from 	stdjob s 	" +
                " where s.codcli = ?" +
                "       AND  s.tipo <> 'G' " +
                "	AND S.ORIGIN_CODE = ?" +
                "       AND S.DESTINATION_CODE = ?" +
                "       AND s.wo_type<>'IN'" );
                st.setString(1,client);
                st.setString(2,ori);
                st.setString(3,dest);
                ////System.out.println(""+st.toString());
                
                rs = st.executeQuery();
                stdjobTree = new TreeMap();
                while(rs.next()){
                    stdjobTree.put(rs.getString("std_job_no")+"-"+rs.getString("std_job_desc"), rs.getString("std_job_no"));
                }
                
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL ORIGENES DESTINOS DE UN STANDARD " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo searchStandardProy, Carga un TreeMap de Standares, asociados a un Proyecto y una base
     * determinado, con la informacion que es arrojada por la busqueda
     * @autor : Ing. Karen Reales
     * @param : String proyecto, String base
     * @version : 1.0
     */
    void searchStandardProy(String proy ,String base )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        standardProy = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("Select  * from stdjobdetsel where base =?  ");
                st.setString(1, base);
                ////System.out.println(st.toString());
                rs = st.executeQuery();
                standardProy = new LinkedList();
                
                while(rs.next()){
                    standardProy.add(Stdjobdetsel.load(rs));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL STANDARD POR PROYECTO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo searchStdSel, Lista todos los registros de la tabla stdjobsel
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public void searchStdSel()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        standards = null;
        PoolManager poolManager = null;
        std = null;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("Select  * from stdjobsel  ");
                
                rs = st.executeQuery();
                standards = new LinkedList();
                
                while(rs.next()){
                    this.std = new Stdjobdetsel();
                    std.setSj(rs.getString("sj"));
                    standards.add(std);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL STANDARD" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    
    
    /**
     * Metodo searchCliente, busca los datos relacionados a un cliente determinado en la tabla cliente y carga el objeto
     * global std con la informacion que es arrojada por la busqueda
     * @autor : Ing. Karen Reales
     * @param : String cliente
     * @version : 1.0
     */
    public void searchCliente(String client)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        standards = null;
        PoolManager poolManager = null;
        std = null;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("Select  c.*, ciudad.nomciu from cliente c, ciudad where c.codcli=? and ciudad.codciu= c.agduenia and c.estado!='I'");
                st.setString(1, client);
                rs = st.executeQuery();
                if(rs.next()){
                    this.std = new Stdjobdetsel();
                    std.setAg_cliente(rs.getString("nomciu"));
                    std.setCliente(rs.getString("nomcli"));
                    std.setCodCliente(rs.getString("codcli"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL STANDARD" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    
    /**
     * Metodo searchStandardCli, busca los datos relacionados a un cliente determinado en la tabla cliente y carga el objeto
     * global std con la informacion que es arrojada por la busqueda
     * @autor : Ing. Karen Reales
     * @param : String cliente
     * @version : 1.0
     */
    public void searchStandardCli(String cliente )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        standardProy = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("select s.std_job_no, s.std_job_desc, d.nomciu as origin_name, e.nomciu as destination_name, c.nomcli, s.peso_lleno_maximo " +
                "                          from      stdjob s, " +
                "                                    cliente c, " +
                "                                    ciudad d, " +
                "                                    ciudad e" +
                "                          where     c.nomcli like ? " +
                "                          and c.codcli='000'||substr(s.std_job_no,1,3)" +
                "                          and d.codciu=origin_code" +
                "                          and e.codciu = destination_code" +
                "                           and S.wo_type<>'IN'" +
                "                          order by s.std_job_no ");
                st.setString(1,"%"+cliente+"%");
                
                rs = st.executeQuery();
                this.stdjob = new Vector();
                while(rs.next()){
                    std = new Stdjobdetsel();
                    std.setSj(rs.getString("std_job_no"));
                    std.setSj_desc(rs.getString("std_job_desc"));
                    std.setOrigin_name(rs.getString("origin_name"));
                    std.setDestination_name(rs.getString("destination_name"));
                    std.setCliente(rs.getString("nomcli"));
                    std.setEstaStd(false);
                    std.setPeso(rs.getFloat("peso_lleno_maximo"));
                    if(this.existStandard(rs.getString("std_job_no"))==true){
                        std.setEstaStd(true);
                    }
                    this.stdjob.add(std);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL STANDARD" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    
    public boolean estaStandardJob(String sj )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        std = null;
        PoolManager poolManager = null;
        boolean sw=false;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("select * from stdjob where std_job_no = ?");
                st.setString(1,sj);
                
                rs = st.executeQuery();
                if(rs.next()){
                    sw = true;
                }
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL STANDARD" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
        
    }
    
    
    public void buscarStandardJob(String sj )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        std = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("select dstrct_code,std_job_no,'000'||substr(std_job_no,1,3) as codcli, peso_lleno_maximo from stdjob where std_job_no = ? and wo_type<>'IN'");
                st.setString(1,sj);
                
                rs = st.executeQuery();
                if(rs.next()){
                    std = new Stdjobdetsel();
                    std.setDstrct(rs.getString("dstrct_code"));
                    std.setSj(rs.getString("std_job_no"));
                    std.setCliente(rs.getString("codcli"));
                    std.setPeso(rs.getFloat("peso_lleno_maximo"));
                    std.setSj_desc(rs.getString("std_job_desc"));//Tito Andr�s 02.11.2005
                }
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL STANDARD" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    
    boolean existStandardProy(String proy )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        standardProy = null;
        PoolManager poolManager = null;
        boolean sw=false;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select  * from stdjobdetsel");
                rs = st.executeQuery();
                if(rs.next()){
                    sw=true;
                }
            }
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL STANDARD" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
    public boolean existStandard(String sj )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        standardProy = null;
        PoolManager poolManager = null;
        boolean sw=false;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select  * from stdjobsel where sj = ?");
                st.setString(1, sj);
                rs = st.executeQuery();
                if(rs.next()){
                    sw=true;
                }
            }
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL STANDARD" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
    public void consultarStandardJob(String stdjob, String cliente )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        this.stdjob = null;
        costo = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("select s.std_job_no, s.std_job_desc, s.origin_name, s.destination_name, c.nomcli, s.account_code_i, s.account_code_c from stdjob s, cliente c where s.std_job_no like ? and c.nomcli like ? and c.codcli='000'||substr(s.std_job_no,1,3) order by s.std_job_no");
                st.setString(1,stdjob+"%");
                st.setString(2,"%"+cliente+"%");
                
                rs = st.executeQuery();
                standardProy = new LinkedList();
                this.stdjob = new Vector();
                while(rs.next()){
                    std = new Stdjobdetsel();
                    std.setSj              ( rs.getString("std_job_no"));
                    std.setSj_desc         ( rs.getString("std_job_desc"));
                    std.setOrigin_name     ( rs.getString("origin_name"));
                    std.setDestination_name( rs.getString("destination_name"));
                    std.setCliente         ( rs.getString("nomcli"));
                    std.setAccount_code_c  ( rs.getString("account_code_c"));
                    std.setAccount_code_i  ( rs.getString("account_code_i"));
                    this.stdjob.add(std);
                }
                
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA DE LOS STANDARES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    public void insertStdJob(String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        standardProy = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("insert into stdjobsel (dstrct, sj, customer_id,creation_user,base) values (?,?,?,?,?) ");
                st.setString(1, std.getDstrct());
                st.setString(2, std.getSj());
                st.setString(3, std.getCliente());
                st.setString(4, std.getCreation_user());
                st.setString(5, base);
                st.executeUpdate();
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL STANDARD" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    public void updateStdJob()throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        standardProy = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("update stdjob set peso_lleno_maximo=? where std_job_no =? ");
                st.setFloat(1, std.getPeso());
                st.setString(2, std.getSj());
                ////System.out.println(st.toString());
                st.executeUpdate();
                
                st = con.prepareStatement("update stdjobcostfull set peso_lleno_maximo=? where sj =? ");
                st.setFloat(1, std.getPeso());
                st.setString(2, std.getSj());
                ////System.out.println(st.toString());
                st.executeUpdate();
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL STANDARD" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public void deleteStdJob()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        standardProy = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("delete from stdjobsel where sj=? ");
                st.setString(1, std.getSj());
                st.executeUpdate();
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL STANDARD" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    //@autor Tito Andr�s Maturana 03.11.2005
    public void getTreeMapStdSel() throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        standards = null;
        PoolManager poolManager = null;
        std = null;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("Select  * from stdjobdetsel where reg_status<>'A'  ");
                
                rs = st.executeQuery();
                this.stdjobTree = new TreeMap();
                this.stdjobTree.put("Todas las rutas.", "TR");
                
                while(rs.next()){
                    this.std = new Stdjobdetsel();
                    std.setSj(rs.getString("sj"));
                    std.setSj_desc(rs.getString("sj_desc"));
                    this.stdjobTree.put(std.getSj() + " - " + std.getSj_desc(), std.getSj());
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL STANDARD" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    
    
    /**
     * Metodo searchStdJob, busca un Standard determinado en la tabla stdjob y carga el objeto
     * global std con la informacion que es arrojada por la busqueda
     * @autor : Ing. Karen Reales
     * @param : String sj
     * @version : 1.0
     */
    public void searchStdJob(String sj)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null, rs2=null;
        standards = null;
        PoolManager poolManager = null;
        std = null;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("Select  s.std_job_no," +
                "	s.std_job_desc," +
                "	s.unidad," +
                "	s.vlr_freight," +
                "	s.origin_code," +
                "	s.currency," +
                "	s.units_required," +
                "	s.bloquea_despacho," +
                "	s.porcentaje_maximo_anticipo:: NUMERIC(10,1) as pa, " +
                "	s.cod_pagador ," +
                "	c.estado" +
                " from 	stdjob s" +
                "	inner join cliente c on (c.codcli =s.codcli)" +
                " where 	dstrct_code='FINV' " +
                "	and std_job_no=? " +
                "	and wo_type<>'IN'" +
                "	and c.estado !='I'");
                st.setString(1, sj);
                rs = st.executeQuery();
                if(rs.next()){
                    this.std = new Stdjobdetsel();
                    std.setSj(rs.getString("std_job_no"));
                    std.setSj_desc(rs.getString("std_job_desc"));
                    std.setUnit_of_work(rs.getString("unidad"));
                    std.setPorcentaje_ant(rs.getString("pa")+"%");
                    std.setVlr_freight(rs.getFloat("vlr_freight"));
                    std.setOrigin_code(rs.getString("origin_code"));
                    std.setCurrency(rs.getString("currency"));
                    std.setUnidades(rs.getFloat("units_required"));
                    std.setPagador(rs.getString("cod_pagador"));
                    if(rs.getString("bloquea_despacho").equals("S")){
                        std.setBloquea_despacho(true);
                        
                    }
                    //PREGUNTO SI LA MONEDA DEL STANDARD ES DIFERENTE DE PESO.
                    if(!rs.getString("currency").equals("")&&!rs.getString("currency").equals("PES")){
                        //AHORA BUSCO EL VALOR EN PESOS DEL STANDARD
                        st = con.prepareStatement("SELECT (1/vlr_conver) as valor" +
                        "               FROM   TASA" +
                        "               WHERE  cia='FINV'" +
                        "                      AND MONEDA1 = 'PES'" +
                        "                      AND MONEDA2 = ?" +
                        "                      AND FECHA ='NOW()'");
                        st.setString(1, rs.getString("currency"));
                        rs2 = st.executeQuery();
                        if(rs2.next()){
                            std.setVlr_pes(com.tsp.util.Util.redondear(rs.getFloat("vlr_freight")*rs2.getDouble("valor"),0));
                        }
                    }
                }
                
                
            }
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL STANDARD" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo searchStandard, busca un Standard determinado en la tabla stdjob y carga el objeto
     * global std con la informacion que es arrojada por la busqueda
     * @autor : Ing. Karen Reales
     * @param : String sj
     * @version : 1.0
     */
    public void searchStandard(String sj)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        std=null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(SQL_BUSCAR);
                st.setString(1,sj);
                ////System.out.println(""+st.toString());
                
                rs = st.executeQuery();
                
                if(rs.next()){
                    std = new Stdjobdetsel();
                    std.setDestination_code(rs.getString("destination_code"));
                    std.setOrigin_code(rs.getString("origin_code"));
                    std.setSj_desc(rs.getString("std_job_desc"));
                    std.setUnit_of_work(rs.getString("unit_of_work"));
                    std.setCurrency(rs.getString("currency"));
                    std.setVlr_freight(rs.getFloat("vlr_freight"));
                    std.setCliente(rs.getString("customer_id"));
                    std.setSj(rs.getString("std_job_no"));
                    std.setBloquea_despacho(false);
                    std.setWoType(rs.getString("wo_type"));
                    std.setClasificacion(rs.getInt("clas_carga"));
                    std.setVlr_mercancia(rs.getFloat("vlr_mercancia"));
                    std.setPagador(rs.getString("cod_pagador"));
                    if(rs.getString("bloquea_despacho").equals("S")){
                        std.setBloquea_despacho(true);
                        
                    }
                    std.setAccount_code_c(rs.getString("account_code_c"));
                    std.setAccount_code_i(rs.getString("account_code_i"));
                    std.setTipoCarga(rs.getString("codtipocarga"));
                    std.setPorcentaje_ant(rs.getString("pa")+"%");
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL STANDARD SOLO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    
    public void consultarStandardJob( String stdjob, String cliente, String estado, String tipo ) throws SQLException {
        //System.out.println("*** stdjob: "+stdjob+" - cliente: "+cliente+" - estado: "+estado+" - tipo: "+tipo);
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        this.stdjob = null;
        costo = null;
        PoolManager poolManager = null;
        
        String temp = "";
        String temp2 = "";
        String stdjob2 = "";
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if ( estado.equals("A") ) {
                temp = "AND s.reg_status = '' ";
            } else if ( estado.equals("I") ) {
                temp = "AND s.reg_status = 'A' ";
            }
            
            if ( stdjob.equals( "" ) && tipo.equals( "G" ) ) {
                temp2 = "s.std_job_no LIKE ? ";
                stdjob = "%G%";
            } else if ( stdjob.equals( "" ) && tipo.equals( "E" ) ) {
                temp2 = "s.std_job_no NOT LIKE ? ";
                stdjob = "%G%";
            } else if ( stdjob.equals( "G" ) && tipo.equals( "E" ) ) {
                temp2 = "s.std_job_no NOT LIKE ? AND s.std_job_no LIKE ? ";
                stdjob = "___G%";
                stdjob2 = "%G%";
            } else if ( stdjob.equals( "G" ) && tipo.equals( "G" ) ) {
                temp2 = "s.std_job_no LIKE ? ";
                stdjob = "___G%";
            } else {
                temp2 = "s.std_job_no LIKE ? ";
                stdjob = "%" + stdjob + "%";
            }
            //System.out.println("+++ stdjob: "+stdjob+" - cliente: "+cliente+" - estado: "+estado+" - tipo: "+tipo);
            String query =
            "SELECT "+
            "s.reg_status, s.std_job_no, s.std_job_desc, get_nombreciudad( s.origin_code ) AS origin_name, get_nombreciudad( s.destination_code ) AS destination_name, s.stdjobgen, "+
            "c.nomcli "+
            "FROM "+
            "stdjob s, cliente c "+
            "WHERE "+
            temp2+
            "AND c.nomcli LIKE ? "+
            "AND c.codcli = '000' || substr( s.std_job_no, 1, 3 ) "+
            temp+
            "ORDER BY "+
            "s.std_job_no ";
            //System.out.println("--- query: "+query);
            if ( con != null ) {
                
                st = con.prepareStatement( query );
                //"select s.std_job_no, s.std_job_desc, s.origin_name, s.destination_name, c.nomcli from stdjob s, cliente c where s.std_job_no like ? and c.nomcli like ? and c.codcli='000'||substr(s.std_job_no,1,3) order by s.std_job_no");
                
                if ( stdjob2.equals("") ) {
                    st.setString( 1, stdjob );
                    st.setString( 2, "%" + cliente + "%" );
                } else {
                    st.setString( 1, stdjob );
                    st.setString( 2, stdjob2 );
                    st.setString( 3, "%" + cliente + "%" );
                }
                
                //System.out.println("/// st: "+st);
                rs = st.executeQuery();
                
                standardProy = new LinkedList();
                this.stdjob = new Vector();
                
                while( rs.next() ) {
                    
                    std = new Stdjobdetsel();
                    
                    std.setSj(rs.getString("std_job_no"));
                    std.setSj_desc(rs.getString("std_job_desc"));
                    std.setOrigin_name(rs.getString("origin_name"));
                    std.setDestination_name(rs.getString("destination_name"));
                    std.setCliente(rs.getString("nomcli"));
                    
                    this.stdjob.add(std);
                    
                }
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CONSULTA DE LOS STANDARES " + e.getMessage() + " " + e.getErrorCode());
        }
        
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
    }
    
    
    
    
    
    public void datosStdjob( String sj ) throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null, rs2 = null;
        std=null;
        sc=null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            String query =
            "SELECT "+
            "s.std_job_no, s.std_job_desc, s.work_group, s.wo_type, s.originator_id, "+
            "s.maint_type, s.vlr_freight, s.currency, s.project_no, s.unit_of_work, "+
            "get_nombreciudad( s.origin_code ) AS origin_name, s.origin_code, get_nombreciudad( s.destination_code ) AS destination_name, s.destination_code, "+
            "c.nomcli, c.codcli, s.account_code_i, s.account_code_c "+
            "FROM "+
            "stdjob s, "+
            "cliente c "+
            "WHERE "+
            "s.std_job_no = ? "+
            "AND c.codcli = '000' || substr( s.std_job_no, 1, 3 ) ";
            
            if ( con != null ) {
                
                //BUSCO LOS DATOS DEL STDJOBDETSEL
                st = con.prepareStatement( query );
                
                st.setString( 1, sj );
                
                rs = st.executeQuery();
                
                //BUSCO LA LISTA DE STDJOBCOSTO
                st = con.prepareStatement("select sc.ft_code, sc.cf_code, d.nomciu as origen , e.nomciu as destino, sc.cost, sc.unit_cost, sc.unit_transp, sc.currency " +
                "from stdjobcostfull sc,  ciudad d, ciudad e " +
                "where d.codciu = substr(sc.ft_code,1,2) and e.codciu=substr(sc.ft_code,3,2) and sc.sj=? ");
                st.setString(1,sj);
                rs2 = st.executeQuery();
                
                if(rs.next()){
                    std = new Stdjobdetsel();
                    std.setSj(rs.getString("std_job_no"));
                    std.setSj_desc(rs.getString("std_job_desc"));
                    std.setWork_group(rs.getString("work_group"));
                    std.setWoType(rs.getString("wo_type"));
                    std.setOriginator_id(rs.getString("originator_id"));
                    std.setMaint_type(rs.getString("maint_type"));
                    std.setVlr_freight(rs.getFloat("vlr_freight"));
                    std.setCurrency(rs.getString("currency"));
                    std.setProject_no(rs.getString("project_no"));
                    std.setUnit_of_work(rs.getString("unit_of_work"));
                    std.setOrigin_name(rs.getString("origin_name"));
                    std.setOrigin_code(rs.getString("origin_code"));
                    std.setDestination_name(rs.getString("destination_name"));
                    std.setDestination_code(rs.getString("destination_code"));
                    std.setCliente(rs.getString("nomcli"));
                    std.setCodCliente(rs.getString("codcli"));
                    
                    std.setAccount_code_c(rs.getString("account_code_c"));
                    std.setAccount_code_i(rs.getString("account_code_i"));
                }
                costo = new LinkedList();
                while(rs2.next()){
                    sc = new Stdjobcosto();
                    sc.setRuta(rs2.getString("origen")+"-"+rs2.getString("destino"));
                    sc.setCost(rs2.getFloat("cost"));
                    sc.setUnit_cost(rs2.getFloat("unit_cost"));
                    sc.setUnit_transp(rs2.getString("unit_transp"));
                    sc.setCurrency(rs2.getString("currency"));
                    sc.setFt_code(rs2.getString("ft_code"));
                    sc.setCf_code(rs2.getString("cf_code"));
                    costo.add(sc);
                }
                
            }
            
        } catch ( SQLException e ) {
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE LA BUSQUEDA DEL STANDARD " + e.getMessage() + " " + e.getErrorCode() );
        }
        
        finally{
            if ( st != null ){
                try{
                    st.close();
                } catch ( SQLException e ){
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
        }
        
    }
    
    /**
     * Setter for property stdjob.
     * @param stdjob New value of property stdjob.
     */
    public void setStdjob(java.util.Vector stdjob) {
        this.stdjob = stdjob;
    }
    
}
/*******************************************************************
 * Entregado a karen 15 de Febrero 2007
 ********************************************************************/
