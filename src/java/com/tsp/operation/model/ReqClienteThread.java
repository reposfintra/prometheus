/*
 * ReqClienteThread.java
 *
 * Created on 24 de junio de 2005, 09:31 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.util.*;
import java.util.Date;
import java.text.*;
import java.sql.SQLException;
import com.tsp.operation.model.beans.*;

/**
 *
 * @author  Sandrameg
 */
public class ReqClienteThread extends Thread {
    
    private Model  model;
    private String usuario;
    private String fi;
    private String ff;
    private String distrito;
    private String tipo;    
    private String procesoName;
    private LlenarReqCliente llreqcli;
    private String des;
    private String hoy;
    
    /** Creates a new instance of ReqClienteThread */
    public ReqClienteThread(){
    }
    
    public void star (String u, String fi, String ff, String d, String tp) {
        this.usuario = u;
        this.fi = fi;
        this.ff = ff;
        this.distrito = d;
        this.tipo = tp;
        SimpleDateFormat s=null;
        s = new SimpleDateFormat("yyyy-MM-dd");
        hoy = s.format(new java.util.Date());
        if( tp.equals("nuevo_pto") ){
            this.procesoName = "Nuevos Requerimiento del Cliente";
            this.des = "Periodo desde "+fi+" Hasta "+ff;
        }
        else if ( tp.equals("act_pto")){
            this.procesoName = "Actualizar Requerimiento del Cliente";
            this.des = "Actualizando";
        }
        else if(tp.equals("nuevo_fron")){
            this.procesoName = "Nuevos Requerimiento por Frontera";
            this.des = "Nuevos Requerimiento por Frontera";
        }
        else{
            this.procesoName = "Actualizar Requerimiento por Frontera";
            this.des = "Actualizando";
        }
        super.start();
    }
      
    public synchronized void run()  {
        model = new Model();
        try {
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.usuario);  
            llreqcli = new LlenarReqCliente(usuario,fi,ff,distrito, tipo);            
            llreqcli.principalReqCliente();  
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.usuario, "PROCESO EXITOSO");
        }
        catch(Exception e){
            try{
                 model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.usuario,"ERROR :" + e.getMessage()); 
             }
             catch(Exception f){ 
                try{ 
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.usuario,"ERROR :"); 
                }catch(Exception p){    }
             }
        }  
    }
}

