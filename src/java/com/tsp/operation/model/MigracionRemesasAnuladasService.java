/*
 * MigracionRemesasAnuladasService.java
 *
 * Created on 16 de julio de 2005, 01:27 PM
 */

package com.tsp.operation.model;

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;


/**
 *
 * @author  Tito Andr�s Maturana
 */
public class MigracionRemesasAnuladasService {
    
    private MigracionRemesasAnuladasDAO remesas_anuladas;
    
    /** Creates a new instance of MigracionRemesasAnuladasService */
    public MigracionRemesasAnuladasService() {
    	this.remesas_anuladas = new MigracionRemesasAnuladasDAO();
    }
    
    public Vector obtenerRemesasAnuladas(){
    	Vector remesasAnuladasVector = new Vector();
    	    	   	
        try{
            this.remesas_anuladas.obtenerRemesasAnuladas();            
            remesasAnuladasVector = this.remesas_anuladas.getRemesas_anuladas();
        }
        catch(Exception exc){
            ////System.out.println(exc.getMessage());
        }
    	
    	return	remesasAnuladasVector;
    
    }
    
    public void actualizarRegistros(String usuario){        
        try{
            this.remesas_anuladas.actualizarRegistros(usuario);
        }
        catch(Exception exc){
            ////System.out.println(exc.getMessage());
        }
    }
    
    
    
}
