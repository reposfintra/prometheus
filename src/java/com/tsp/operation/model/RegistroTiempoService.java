/*
 * RegistroTiempoService.java
 *
 * Created on 30 de noviembre del 2004, 3:40
 */

package com.tsp.operation.model;

import java.io.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  mfontalvo
 */
public class RegistroTiempoService {
    private RegistroTiempoDAO RTDataAccess;
    // variables para manipualr los registros de los tiempos de la palnilla
    private DatosPlanilla     Planilla;                  // DATOS GENERALES DE LA PLANILLA
    private List              ListaDelay;                // LISTA DE LOS CODIGOS Y DESCRIPCION DE LOS DELAY ASOCIADOS A UN SJ DEFINIDO POR LA PLANILLA
    private List              ListaTBLTiempo;            // LISTA DE LOS TBLTIEMPO ASOCIADAS AL SJ DE LA PLANILLA
    private List              ListaPlanillaTiempos;      // LISTA DE DE LOS REGISTROS DE TBLTIEMPO EMPAREJADOS CON UNA PLANILLA
    
    // variable para la lista de planillas y sus tiempos buscados en un rano de fecha
    private List              ListaPlanillas;
    
    
    /** Creates a new instance of RegistroTiempoService */
    public RegistroTiempoService()throws Exception {
	try{
           RTDataAccess = new RegistroTiempoDAO();
        }catch(Exception ex){
                throw new Exception ("Error en RegistroTiempoService [RegistroTiempoService]...\n"+ex.getMessage());
        }        
    }
    
    public void BuscarDatosPlanilla(String Planilla)throws Exception{        
	try{           
        this.ReiniciarPlanilla();
	    this.Planilla = RTDataAccess.BuscarDatosPlanilla(Planilla);
	}catch(Exception ex){
	    throw new Exception ("Error en BuscarDatosPlanilla [RegistroTiempoService]...\n"+ex.getMessage());
	}        
    }

    public void BuscarListaDelay(String Planilla)throws Exception{        
	try{           
        this.ReiniciarListaDelay();
	    this.ListaDelay = RTDataAccess.BuscarDelay(Planilla);
	}catch(Exception ex){
	    throw new Exception ("Error en BuscarDatosPlanilla [RegistroTiempoService]...\n"+ex.getMessage());
	}        
    }

   public void BuscarListaTBLTiempo(String Planilla)throws Exception{        
	try{           
        this.ReiniciarListaTBLTiempo();
	    this.ListaTBLTiempo = RTDataAccess.BuscarRegistrosTBLTiempo(Planilla);
	}catch(Exception ex){
	    throw new Exception ("Error en BuscarListaTBLTiempo [RegistroTiempoService]...\n"+ex.getMessage());
	}        
    }   
   
   //BUSCA TODAS LOS REGISTRO DE LA PLANILLA ASOCIADA A CADA REGISTRO QUE SE ENCUENTRA ALMACENADO EN LA LISTA TBLTIEMPO
   public void BuscarPlanillaTiempo(String Planilla)throws Exception{
	try{
            this.BuscarListaTBLTiempo(Planilla);
            this.ReiniciarListaPlanillaTiempos();
            this.ListaPlanillaTiempos = RTDataAccess.BuscarTiempoPlanilla(this.ListaTBLTiempo,Planilla);
	}catch(Exception ex){
	    throw new Exception ("Error en BuscarPlanillaTiempo [RegistroTiempoService]...\n"+ex.getMessage());
	}
    }

    public void Insertar(String Distrito, String Planilla, String SJ, String CF_Code, String Time_Code, String Secuence, String DTT, String Delay_Code, String Observation, String TimeDelay, String Usuario) throws Exception {
	try{
	    RTDataAccess.Insertar(Distrito,Planilla,SJ, CF_Code,Time_Code,Secuence,DTT,Delay_Code,Observation, TimeDelay ,Usuario);
	}catch(Exception ex){
	    throw new Exception ("Error en Insertar [RegistroTiempoService]...\n"+ex.getMessage());
	}
    }

    public void Modificar(String Distrito, String Planilla, String SJ, String CF_Code, String Time_Code, String Secuence, String DTT, String Delay_Code, String Observation, String TimeDelay, String Usuario) throws Exception {
	try{
	    RTDataAccess.Modificar(Distrito,Planilla,SJ, CF_Code,Time_Code,Secuence,DTT,Delay_Code,Observation, TimeDelay ,Usuario);
	}catch(Exception ex){
	    throw new Exception ("Error en Modificar [RegistroTiempoService]...\n"+ex.getMessage());
	}
    }
    
    public String Nombre(String Cod)throws Exception{
        try{
            return RTDataAccess.BuscarNombreCiudad(Cod);
        }catch(Exception e){return null; }
    }
    
    
    
    
    /***************************************************************************/
    /* procedimiento para buscar el listado de planillas en un rango de tiempo */
    
    public void BuscarPlanillas(String FechaInicial, String FechaFinal)throws Exception{
        try{
            this.ReiniciarListaPlanillas();
            this.ListaPlanillas = RTDataAccess.BuscarPlanillas(FechaInicial,FechaFinal);
        }catch(Exception e){
            throw new Exception ("Error en BuscarPlanillas [RegistroTiempoService]"+e.getMessage());
        }
    }
    
    
    /****************************************************************************/
    
    // limpiar
    public void ReiniciarPlanilla(){
        this.Planilla = null;
    }
    public void ReiniciarListaDelay(){
        this.ListaDelay = null;
    }
    public void ReiniciarListaTBLTiempo(){
        this.ListaTBLTiempo = null;
    }
    public void ReiniciarListaPlanillaTiempos(){
        this.ListaPlanillaTiempos = null;
    }  
    public void ReiniciarListaPlanillas(){
        this.ListaPlanillas=null;
    }
    
    //Getter
    public DatosPlanilla getDatosPlanilla(){
        return this.Planilla;
    }
    public List getListaDelay(){
        return this.ListaDelay;
    }
    public List getListaTBLTiempo(){
        return this.ListaTBLTiempo;
    }    
    public List getListaPlanillaTiempos(){
        return this.ListaPlanillaTiempos;
    }    
    public List getListaPlanillas(){
        return this.ListaPlanillas;
    }
}
