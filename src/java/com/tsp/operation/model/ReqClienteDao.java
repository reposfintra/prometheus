/************* clase modificada ********************
 *
 * /*
 * ReqClienteDao.java
 *
 * Created on 27 de mayo de 2005, 02:17 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  mcelin
 */
public class ReqClienteDao {
    
    /** Creates a new instance of ReqClienteDao */
    public ReqClienteDao() {
    }
    
    private ReqCliente reqcliente;
    
     private static final String SQL_REMESAS_PLANEACION = "  SELECT" +
    "	DISTINCT r.numrem as ot, " +
    "	r.des_ot" +
    "   FROM  (        " +
    "      SELECT p.numpla," +
    "             r.numrem," +
    "      c1.agasoc  as ori_ot,  " +
    "      c2.agasoc  as des_ot                         " +
    "      FROM   planilla p " +
    "      INNER JOIN plarem pl ON ( pl.numpla = p.numpla  )  " +
    "      INNER JOIN remesa r  ON ( r.numrem  = pl.numrem AND r.reg_status != 'A' )  " +
    "      INNER JOIN  ciudad c1 ON ( c1.codciu=r.orirem    )  " +
    "      INNER JOIN  ciudad c2 ON ( c2.codciu=r.desrem    )" +
    "      WHERE                                               " +
    "	     p.planeacion_frontera = 'S'                 " +
    "         AND p.cia = ?       " +
    "         AND p.reg_status != 'A')r ";
    
    private static final String SQL_EXISTE_PLANILLA_DESTINO =
    " SELECT p.numpla," +
    "      c.agasoc as despla " +
    "     FROM " +
    "		plarem pl,                   " +
    "        	planilla p " +
    "     INNER JOIN ciudad c ON(c.codciu=p.despla AND c.agasoc = ? ) " +
    "     WHERE                              " +
    "                pl.numrem = ? " +
    "            AND pl.numpla = p.numpla ";
    
    
    private static final String SQL_PLANILLAS = "SELECT p.fecdsp," +
                                                "       p.numpla" +
                                                "     FROM " +
                                                "    	plarem pl,                   " +
                                                "        	planilla p " +
                                                "     WHERE                              " +
                                                "                pl.numrem = ?" +
                                                "            AND pl.numpla = p.numpla " +
                                                "order by fecdsp DESC ";
    
    private static final String SQL_ACTUALIZAR_PLANILLA ="UPDATE planilla " +
     "SET    planeacion_frontera = 'N' " +
     "     FROM  " +
     "            plarem pl   " +
     "     WHERE                              " +
     "            pl.numrem  = ?  " +
     "        AND pl.numpla  =  planilla.numpla ";
    
     private static final String SQL_ACT_PLANILLA ="UPDATE planilla    	" +
    " SET planeacion_frontera = 'N'" +
    " WHERE" +
    "    numpla =  ? ";
    private static final  String Select_Consulta =  "insert                   "+
    " into 	               "+
    "    req_cliente (         "+
    "        dstrct_code,      "+
    "	num_sec,	       "+
    "	std_job_no,            "+
    "	numpla,                "+
    "	fecha_dispo,           "+
    "	cliente,               "+
    "	origen,                "+
    "	destino,               "+
    "	clase_req,             "+
    "	tipo_rec1,             "+
    "	tipo_rec2,             "+
    "	tipo_rec3,             "+
    "	tipo_rec4,             "+
    "	tipo_rec5,             "+
    "	recurso1,              "+
    "	recurso2,              "+
    "	recurso3,              "+
    "	recurso4,              "+
    "	recurso5,              "+
    "	prioridad1,            "+
    "	prioridad2,            "+
    "	prioridad3,            "+
    "	prioridad4,            "+
    "	prioridad5,            "+
    "	usuario_creacion,      "+
    "	fecha_creacion,        "+
    "	usuario_actualizacion, "+
    "	fecha_actualizacion," +
    "   id_rec_tra )  "+
    " values (?,                   "+
    "	?,                     "+
    "	?,                     "+
    "	?,                     "+
    "	?,                     "+
    "	?,                     "+
    "	?,                     "+
    "	?,                     "+
    "	?,                     "+
    "	?,                     "+
    "	?,                     "+
    "	?,                     "+
    "	?,                     "+
    "	?,                     "+
    "	?,                     "+
    "	?,                     "+
    "	?,                     "+
    "	?,                     "+
    "	?,                     "+
    "	?,                     "+
    "	?,                     "+
    "	?,                     "+
    "	?,                     "+
    "	?,                     "+
    "	?,                     "+
    "	?,                     "+
    "	?,                     "+
    "	?," +
    "   ?)                     ";
    
    private static final  String actreqclien =      " update                   "+
    " req_cliente              "+
    "    set                   "+
    "	  cliente = ?,         "+
    "	origen = ?,            "+
    "	destino = ?,           "+
    "	clase_req = ?,         "+
    "	tipo_rec1 = ?,         "+
    "	tipo_rec2 = ?,         "+
    "	tipo_rec3 = ?,         "+
    "	tipo_rec4 = ?,         "+
    "	tipo_rec5 = ?,         "+
    "	recurso1 = ?,          "+
    "	recurso2 = ?,          "+
    "	recurso3 = ?,          "+
    "	recurso4 = ?,          "+
    "	recurso5 = ?,          "+
    "	prioridad1 = ?,        "+
    "	prioridad2 = ?,        "+
    "	prioridad3 = ?,        "+
    "	prioridad4 = ?,        "+
    "	prioridad5 = ?,        "+
    "	usuario_actualizacion = ?,"+
    "	fecha_actualizacion = ?   "+
    "where                        "+
    "      dstrct_code = ?        "+
    "	   and num_sec = ?        "+
    "	   and std_job_no = ?     "+
    "	   and numpla = ?         "+
    "	   and fecha_dispo = ?    ";
    
    private static StdJob stdjob;//declaro el objeto stdjob
    
    
    private Pto_ventas pto_venta;
    private ReqFrontera reqfrontera;
    
    private Vector VecPto_Venta, VecReqFron;
    
    //Modificado 09.12.05
    private static final  String Select_ReqVenta =   "SELECT * FROM                                      " +
    "   fin.pto_ventas                                      " +
    "   WHERE      DSTRCT_CODE = ?                      " +
    "          AND ANO||MES BETWEEN ? AND ?             " +
    "          AND substr (std_job_no ,1,3) NOT IN  ('734', '127' )  " +
    "          AND estado = '' order by ano, mes        ";
    
    private static final  String Select_ReqVen =    "SELECT * FROM                                      " +
    "   fin.pto_ventas                                      " +
    "   WHERE      DSTRCT_CODE = ?                      " +
    "          AND ANO||MES BETWEEN ? AND ?             " +
    "          AND fecha_creacion > ?                   " +
    "          AND substr (std_job_no ,1,3) NOT IN  ('734', '127' )    " +
    "          AND estado = '' order by ano, mes        ";
    
    
    private static final  String Select_ReqVenAct = "SELECT * FROM " +
    "   fin.pto_ventas                       " +
    "              WHERE DSTRCT_CODE = ? " +
    "          AND fecha_actualizacion between ? and ? " +
    "          AND fecha_actualizacion > fecha_creacion " +
    "          AND substr (std_job_no ,1,3) NOT IN  ('734', '127' )    "+
    "          order by ano, mes";
    
    
    private static final String Delete_reqventa =   " delete                    "+
    " from                      "+
    "    req_cliente            "+
    " where                     "+
    "         std_job_no = ?    "+
    "     and fecha_dispo = ?   "+
    "     and dstrct_code = ?   ";
    
    private static final String Delete_reqven =    " delete                                  "+
    "  from                                   "+
    "     req_cliente                         "+
    "  where                                  "+
    "          fecha_creacion between ? and ? "+
    "      and clase_req = 'VE'               ";
    
    
    private static final  String Select_ReqFron =  " select distinct                   "+
    "       r.numrem     as ot,         "+
    "       r.orirem     as ori_ot,     "+
    "       r.desrem     as des_ot,      "+
    "       p.numpla     as oc,         "+
    "       p.fecpla     as fecha_oc,   "+
    "       p.oripla     as ori_oc,     "+
    "       p.despla     as des_oc,     "+
    "       p.fechaposllegada as fec_dis," +
    "       p.platlr     as trailer,    " +
    "       p.plaveh     as cabezote,   " +
    "       r.cliente    as cliente,    "+
    "       r.std_job_no as stdjob,     "+
    "       s.wo_type    as tipo_std,   "+
    "       s.*                         "+
    " from                              "+
    "	   remesa r,                   "+
    "	   plarem pl,                  "+
    "	   planilla p,                 "+
    "	   stdjob s                    "+
    " where                             "+
    "        p.planeacion_frontera = 'S' "+
    "    and p.cia = ?                  "+
    "    and p.reg_status != 'A'              "+
    "    and p.numpla = pl.numpla       "+
    "    and pl.numrem = r.numrem       "+
    "    and r.std_job_no = s.std_job_no  " +
    "    and r.reg_status != 'A'        " +
    "    and s.dstrct_code = ?   ";
    
    
    private static final String Select_ReqFronAct =" select distinct                    "+
    "       r.numrem     as ot,         "+
    "       r.orirem     as ori_ot,     "+
    "       r.desrem     as des_ot,      "+
    "       p.numpla     as oc,         "+
    "       p.fecpla     as fecha_oc,   "+
    "       p.oripla     as ori_oc,     "+
    "       p.despla     as des_oc,     "+
    "       p.fechaposllegada as fec_dis," +
    "       p.platlr     as trailer,    " +
    "       p.plaveh     as cabezote,   " +
    "       r.cliente    as cliente,    "+
    "       r.std_job_no as stdjob,     "+
    "       s.wo_type    as tipo_std,   "+
    "       s.*                         "+
    " from                               "+
    "       remesa r,                    "+
    "	   plarem pl,                   "+
    "	   planilla p,                  "+
    "	   stdjob s                     "+
    "  where                             " +
    "           p.planeacion_frontera = 'S'"+
    "        and p.last_update between ? and ?     "+
    "        and p.cia = ?                         "+
    "        and p.last_update > ?                 "+
    "        and p.numpla = pl.numpla              "+
    "        and pl.numrem = r.numrem              "+
    "        and r.std_job_no = s.std_job_no       "+
    "        and s.dstrct_code = ?                 ";
    
    
    
    private static final String Delete_reqFron ="  delete     "+
    "  from       "+
    "       req_cliente "+
    "  where      "+
    "          numpla = ?  " +
    "      and std_job_no = ?";
    
    private static final String Select_tiporec = " select		"+
    "	tipo            "+
    " from			"+
    "	tipo_rec        "+
    " where                 "+
    "	codigo = ?      "+
    "   and dstrct_code = ? ";
    
    private static final String Select_tiempo ="  select                " +
    "          tiempo        "+
    "  from    tramo         "+
    "  where   origin = ?    "+
    "      and destination = ? ";
    
    private static final String Select_AgasocXnom = " select        "+
    "      agasoc   "+
    " from          "+
    "      ciudad   "+
    " where         "+
    "   nomciu = ?  ";
    
    private static final String Select_AgasocXcod = " select        "+
    "      agasoc   "+
    " from          "+
    "      ciudad   "+
    " where         "+
    "   codciu = ?  ";
    private static final String Select_Fecha = " select                       "+
    "       min (feinftr) as fec,  "+
    "       min (horareal) as hor  "+
    " from                         "+
    "       trafimo                "+
    " where                        "+
    "       planilla = ?           ";
    
    private static final String Select_ReqFronReg = " select distinct                "+
    "	a.*,                         "+
    "	r.numrem as ot,              "+
    "	r.orirem as ori_ot,          "+
    "	r.desrem as des_ot           "+
    " from                             "+
    "	(select                      "+
    "		numpla as oc,        "+
    "		origen as ori_oc,    "+
    "		destino as des_oc,   "+
    "             std_job_no as stdjob "+
    "	 from                        "+
    "		req_cliente          "+
    "	where                        "+
    "		    fecha_creacion between ? and ? " +
    "             and clase_req = 'FS' )a,  "+
    "	plarem pl,                        "+
    "	remesa r                          "+
    " where                                 "+
    "       a.OC = pl.numpla                "+
    "   and pl.numrem = r.numrem            ";
    
    //////alpall
    private static final String selectByEstado =
    "select 	req_cliente.*, " +
    "	ciudad.agasoc," +
    "	ciudad.pais as po," +
    "	ciudadd.pais as pd," +
    "	case when ciudad.pais <>  ciudadd.pais then true else false end as internacional," +
    "   ciudadd.agasoc as agDest," +
    "   case when inter.dato is not null then true else false end as inter " +
    " from 	req_cliente" +
    " LEFT OUTER JOIN (SELECT dato, " +
    "                                   std_job_no  " +
    "        				 FROM   wgroup_standart,  " +
    "        					tablagen  " +
    "        				 where  table_type='WG'  " +
    "        					and table_code=work_group  " +
    "        					and dato='IN') as inter on (inter.std_job_no=req_cliente.std_job_no), " +
    "	ciudad, " +
    "	ciudad as ciudadd" +
    " where 	req_cliente.estado = ? " +
    "	and req_cliente.origen = ciudad.codciu " +
    "	and req_cliente.destino = ciudadd.codciu" +
    " order by dstrct_code,fecha_dispo,std_job_no,num_sec";
    private static final String SQL_OBTENER_REQUERIMIENTO =
    "select * from req_cliente where dstrct_code = ? and num_sec = ? and std_job_no = ? and fecha_dispo = ? and numpla = ?";
    //Diogenes 16.11.05
    private static final String viajeInternacional = "SELECT a.Pais as origen, " +
    "  b.pais as destino      " +
    "  from                   " +
    "  (select Pais FROM CIUDAD WHERE codciu = ? )a, " +
    "  (select Pais FROM CIUDAD WHERE codciu = ? )b ";
    
    private static final String buscarFrontera = "select frontera_asoc FROM CIUDAD WHERE codciu = ? and estado = 'A'";
    
    private static final String SQL_CLASE_TIPO_RECURSO =" SELECT recurso, tipo_recurso" +
    " FROM recursos" +
    " WHERE" +
    "    equipo = ?";
    
    
    private static final String Delete_reqfron =    " delete                                  "+
    "  from                                   "+
    "     req_cliente                         "+
    "  where                                  "+
    "     clase_req = 'FS'               ";
    
    
    
    
    public void EliminarReq_fronN( )throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(Delete_reqfron);
                ////System.out.println("Elimina req "+st);
                
                st.executeUpdate();
                
                
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ELIMINACION DE REQVENTA MOD " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        
    }
    
    /**
     * Metodo listarRemesasPlaneacion, lista las remesas que cuyas planillas
     *  tengan planeacion_planilla en S
     * @param: distrito
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public Vector listarRemesasPlaneacion(String dstrct )throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null,rs1= null;
        PoolManager poolManager = null;
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                
                st = con.prepareStatement(SQL_REMESAS_PLANEACION);
                st.setString(1,dstrct);
                ////System.out.println(st);
                
                rs = st.executeQuery();
                
                VecReqFron =  new Vector();
                
                while (rs.next()){
                    reqfrontera = new ReqFrontera();
                    reqfrontera.setnumrem(rs.getString("ot") );
                    reqfrontera.setdesrem( rs.getString("des_ot") );                    
                    VecReqFron.add(reqfrontera);
                }
            }
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA REMESAS PLANEACION" + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return VecReqFron;
    }
    
    
    /**
     * Metodo  existePlanillaDestinoFinal, verifica si existen planillas qeu cuya,
     * aagencia asociada del destino sea igual a la agencia asociada del dstino de la remesa
     * @param: numero remesa, agencia asociada destino de la remesa
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean existePlanillaDestinoFinal(String numrem,String agasocrem) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_EXISTE_PLANILLA_DESTINO);
                st.setString(1, agasocrem );
                st.setString(2, numrem );               
                rs = st.executeQuery();
                
                if (rs.next()){
                    sw = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR IDENTIDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return sw;
    }
    
    /**
     * Metodo actualizarPlanillaPlaneacion, actualiza las planillas de esa remesa,
     * el campo planeacion_frontera en N
     * @param: numrem
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void actualizarPlanillaPlaneacion(String numrem) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_ACTUALIZAR_PLANILLA);
                st.setString(1, numrem );
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL MODIFICAR ACTIVIDAD " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }



/**
     * Metodo  buscarPlanillasAct, busca las planillas de una remesa
     * y actualiza todas sus planillas escluyendo la ultima planilla creada 
     * @param: numero remesa
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarPlanillasAct(String numrem) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sw = "";
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_PLANILLAS);
                st.setString(1, numrem );
                
                rs = st.executeQuery();
                rs.next();
                st = con.prepareStatement(SQL_ACT_PLANILLA);
                while (rs.next()){
                    st.setString(1, rs.getString("numpla"));
                    st.executeUpdate();
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR LAS PLANILLAS Y ACTUALIZAR" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){ st.close(); }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        if(!sw.equals("")){
            sw = sw.substring(0,sw.length()-1);
        }
    
    }
    
    /**
     * Metodo actualizarPlanillaPlan, actualiza las planillas de esa remesa,
     * el campo planeacion_frontera en N
     * @param: planillas a actualizar
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void actualizarPlanillaPlan(String planillas) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_ACT_PLANILLA);
                st.setString(1, planillas );
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL MODIFICAR ACTIVIDAD " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    //recorre la tabla de Pto_venta y retorna un vector con los viajes correspondientes al la fecha. deacuerdo aun rango de fechas
    public Vector listarPto_Venta( String ano1, String mes1, int dia1, String ano2, String mes2, int dia2, String dstrct  )throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        VecPto_Venta = null;
        int dia=0,mes=0,ano=0;
        int diaIni=dia1, diaFin=dia2;
        String CDia="CDIA",a="",M1="",M2="",m="0";
        
        
        try{
            //////System.out.println("Voy a realizar la conexcion BD sot");
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(Select_ReqVenta);
                st.setString(1,dstrct);
                st.setString(2,ano1+mes1);
                st.setString(3,ano2+mes2);
                rs = st.executeQuery();
                VecPto_Venta =  new Vector();
                
                while (rs.next()){
                    
                    if (!mes1.equalsIgnoreCase(mes2)){
                        if ( rs.getString("mes").equalsIgnoreCase(mes1) ){
                            diaIni = dia1;
                            diaFin = 31;
                        }
                        else if ( rs.getString("mes").equalsIgnoreCase(mes2) ){
                            diaIni = 1;
                            diaFin = dia2;
                        }
                        else{
                            diaIni = 1;
                            diaFin = 31;
                        }
                    }
                    
                    for (dia = diaIni; dia<=diaFin; dia++ ){  //recorro los campos de los dias de un registro
                        if ( dia < 10 ){
                            CDia = CDia+"0"+dia;
                        }
                        else{
                            CDia = CDia+dia;
                        }
                        if ( rs.getInt(CDia) != 0 ){
                            pto_venta = new Pto_ventas();
                            pto_venta.setdstrct_code( rs.getString( "dstrct_code" ) );
                            pto_venta.setstd_job_no( rs.getString("std_job_no") );
                            pto_venta.setano( rs.getString("ano") );
                            pto_venta.setmes( rs.getString("mes") );
                            if ( dia < 10 ){
                                pto_venta.setdia("0"+dia);
                            }
                            else
                                pto_venta.setdia(""+dia);
                            pto_venta.setviaje(rs.getInt(CDia) );
                            pto_venta.setcliente( rs.getString("cliente") );
                            pto_venta.setestado(rs.getString("estado") );
                            pto_venta.setUsuario_creacion(rs.getString("Usuario_creacion") );
                            pto_venta.setfecha_creacion(rs.getTimestamp("fecha_creacion") );
                            pto_venta.setUsuario_actualizacion(rs.getString("Usuario_actualizacion") );
                            pto_venta.setfecha_actualizacion(rs.getTimestamp("fecha_actualizacion") );
                            //////System.out.println("Fecha Dispo "+rs.getString("ano")+"-"+rs.getString("mes")+"-"+dia +" STDJOB "+rs.getString("std_job_no"));
                            VecPto_Venta.add(pto_venta);
                            
                        }
                        CDia="CDIA";
                    }
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA PTO_VENTA " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return VecPto_Venta;
    }
    
    public Vector listarPto_VentaA( String ano1, String mes1, int dia1, String ano2, String mes2, int dia2, String dstrct, String fin_pro  )throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        VecPto_Venta = null;
        int dia=0,mes=0,ano=0;
        int diaIni=dia1, diaFin=dia2;
        String CDia="CDIA",a="",M1="",M2="",m="0";
        
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(Select_ReqVen);
                st.setString(1,dstrct);
                st.setString(2,ano1+mes1);
                st.setString(3,ano2+mes2);
                st.setString(4,fin_pro);
                rs = st.executeQuery();
                
                VecPto_Venta =  new Vector();
                
                while (rs.next()){
                    
                    if (!mes1.equalsIgnoreCase(mes2)){
                        if ( rs.getString("mes").equalsIgnoreCase(mes1) ){
                            diaIni = dia1;
                            diaFin = 31;
                        }
                        else if ( rs.getString("mes").equalsIgnoreCase(mes2) ){
                            diaIni = 1;
                            diaFin = dia2;
                        }
                        else{
                            diaIni = 1;
                            diaFin = 31;
                        }
                    }
                    for (dia = diaIni; dia<=diaFin; dia++ ){  //recorro los campos de los dias de un registro
                        if ( dia < 10 ){
                            CDia = CDia+"0"+dia;
                        }
                        else{
                            CDia = CDia+dia;
                        }
                        if ( rs.getInt(CDia)> 0 ){
                            pto_venta = new Pto_ventas();
                            pto_venta.setdstrct_code( rs.getString( "dstrct_code" ) );
                            pto_venta.setstd_job_no( rs.getString("std_job_no") );
                            pto_venta.setano( rs.getString("ano") );
                            pto_venta.setmes( rs.getString("mes") );
                            if ( dia < 10 ){
                                pto_venta.setdia("0"+dia);
                            }
                            else
                                pto_venta.setdia(""+dia);
                            pto_venta.setviaje(rs.getInt(CDia) );
                            pto_venta.setcliente( rs.getString("cliente") );
                            pto_venta.setestado(rs.getString("estado") );
                            pto_venta.setUsuario_creacion(rs.getString("Usuario_creacion") );
                            pto_venta.setfecha_creacion(rs.getTimestamp("fecha_creacion") );
                            pto_venta.setUsuario_actualizacion(rs.getString("Usuario_actualizacion") );
                            pto_venta.setfecha_actualizacion(rs.getTimestamp("fecha_actualizacion") );
                            VecPto_Venta.add(pto_venta);
                            
                        }
                        CDia="CDIA";
                    }
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA PTO_VENTA " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return VecPto_Venta;
    }
    
    //cartura de los Pto_venta modificadas para actualizar la tabla req_cliente
    public Vector listarPto_VentaAct(  String dstrct, String fecI_pro, String fecF_pro )throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        VecPto_Venta = null;
        
        String CDia="CDIA",a="",M1="",M2="",m="0";
        
        String ano1=fecI_pro.substring(0,4);
        String mes1=fecI_pro.substring(5,7);
        int dia1= Integer.parseInt(fecI_pro.substring(8,10));
        String ano2=fecF_pro.substring(0,4);
        String mes2=fecF_pro.substring(5,7);
        int dia2= Integer.parseInt(fecF_pro.substring(8,10));
        int diaIni=dia1;
        int diaFin=dia2;
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(Select_ReqVenAct);
                st.setString(1,dstrct);
                st.setString(2,fecI_pro);
                st.setString(3,fecF_pro);
                
                rs = st.executeQuery();
                
                VecPto_Venta =  new Vector();
                
                while (rs.next()){
                    
                    if (!mes1.equalsIgnoreCase(mes2)){
                        if ( rs.getString("mes").equalsIgnoreCase(mes1) ){
                            diaIni = dia1;
                            diaFin = 31;
                        }
                        else if ( rs.getString("mes").equalsIgnoreCase(mes2) ){
                            diaIni = 1;
                            diaFin = dia2;
                        }
                        else{
                            diaIni = 1;
                            diaFin = 31;
                        }
                    }
                    for (int dia = diaIni; dia<=diaFin; dia++ ){ //recorro los campos de los dias de un registro
                        if ( dia < 10 ){
                            CDia = CDia+"0"+dia;
                        }
                        else{
                            CDia = CDia+dia;
                        }
                        if ( rs.getInt(CDia)> 0 ){
                            pto_venta = new Pto_ventas();
                            pto_venta.setdstrct_code( rs.getString( "dstrct_code" ) );
                            pto_venta.setstd_job_no( rs.getString("std_job_no") );
                            pto_venta.setano( rs.getString("ano") );
                            pto_venta.setmes( rs.getString("mes") );
                            if ( dia < 10 ){
                                pto_venta.setdia("0"+dia);
                            }
                            else
                                pto_venta.setdia(""+dia);
                            pto_venta.setviaje(rs.getInt(CDia) );
                            pto_venta.setcliente( rs.getString("cliente") );
                            pto_venta.setestado(rs.getString("estado") );
                            pto_venta.setUsuario_creacion(rs.getString("Usuario_creacion") );
                            pto_venta.setfecha_creacion(rs.getTimestamp("fecha_creacion") );
                            pto_venta.setUsuario_actualizacion(rs.getString("Usuario_actualizacion") );
                            pto_venta.setfecha_actualizacion(rs.getTimestamp("fecha_actualizacion") );
                            VecPto_Venta.add(pto_venta);
                            
                        }
                        CDia="CDIA";
                    }
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA PTO_VENTA MODIFICADAS " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return VecPto_Venta;
    }
    
    //elimina de la tabla req_cliente los registros que han sido actulaizados
    public void EliminarReq_venAct(String fecha, String stdjob, String dstrct )throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(Delete_reqventa);
                st.setString(1,stdjob);
                st.setString(2,fecha);
                st.setString(3,dstrct);
                
                st.executeUpdate();
                
                
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ELIMINACION DE REQVENTA MOD " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        
    }
    
    public void EliminarReq_venN(String fechaI, String fechaF )throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(Delete_reqven);
                st.setString(1,fechaI+" 08:00:00");
                st.setString(2,fechaF+" 23:59:00" );
                
                
                st.executeUpdate();
                
                
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ELIMINACION DE REQVENTA MOD " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        
    }
    
    
    
    public Vector listarReqFrontera(String dstrct )throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null,rs1= null;
        PoolManager poolManager = null;



        try{

            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){

                st = con.prepareStatement(Select_ReqFron);
                st.setString(1,dstrct);
                st.setString(2,dstrct);

                rs = st.executeQuery();

                VecReqFron =  new Vector();

                while (rs.next()){
                    reqfrontera = new ReqFrontera();
                    reqfrontera.setcliente( rs.getString("cliente") );
                    reqfrontera.setnumrem(rs.getString("ot") );
                    reqfrontera.setorirem( rs.getString("ori_ot") );
                    reqfrontera.setdesrem( rs.getString("des_ot") );
                    reqfrontera.setnumpla( rs.getString("oc") );
                    reqfrontera.setoripla( rs.getString("ori_oc") );
                    reqfrontera.setdespla( rs.getString("des_oc") );
                    reqfrontera.setfecdispo( rs.getString("fec_dis") );
                    reqfrontera.setfecpla(rs.getString("fecha_oc"));
                    reqfrontera.setId_rec_tra(rs.getString("trailer"));
                    reqfrontera.setEquipo_aso(rs.getString("cabezote"));
                    st = con.prepareStatement("select stdjobgen, tipo_recurso1, tipo_recurso2,tipo_recurso3,tipo_recurso4,tipo_recurso5,recurso1,recurso2, recurso3, recurso4,"+
                    " recurso5, prioridad1, prioridad2, prioridad3,prioridad4,prioridad5 from stdjob where std_job_no = ?  and dstrct_code = ? ");
                    st.setString(1,rs.getString("stdjob"));
                    st.setString(2,dstrct);

                    rs1 = st.executeQuery();
                    //verificacion

                   
                    
                    if (rs1.next()){
                        if( rs1.getString("stdjobgen").trim().equals("") || rs1.getString("stdjobgen").equalsIgnoreCase("CASUAL") ){
                            reqfrontera.setstd_job_no( rs.getString("stdjob") );
                            reqfrontera.setStdjob_esp( rs.getString("stdjob") );
                            reqfrontera.setTipo_recurso1("");
                            reqfrontera.setTipo_recurso2("");
                            reqfrontera.setTipo_recurso3("");
                            reqfrontera.setTipo_recurso4("");
                            reqfrontera.setTipo_recurso5("");
                            reqfrontera.setrecurso1( "" );
                            reqfrontera.setrecurso2( "" );
                            reqfrontera.setrecurso3( "" );
                            reqfrontera.setrecurso4( "" );
                            reqfrontera.setrecurso5( "" );
                            reqfrontera.setprioridad1( "0" );
                            reqfrontera.setprioridad2( "0" );
                            reqfrontera.setprioridad3( "0" );
                            reqfrontera.setprioridad4( "0" );
                            reqfrontera.setprioridad5( "0" );
                            reqfrontera.setError("Stdjob General No encontrado");
                        }
                        else{
                            reqfrontera.setStdjob_esp( rs.getString("stdjob") );
                            reqfrontera.setstd_job_no( rs1.getString("stdjobgen") );
                            reqfrontera.setTipo_recurso1( rs1.getString("tipo_recurso1"));
                            reqfrontera.setTipo_recurso2( rs1.getString("tipo_recurso2"));
                            reqfrontera.setTipo_recurso3( rs1.getString("tipo_recurso3"));
                            reqfrontera.setTipo_recurso4( rs1.getString("tipo_recurso4"));
                            reqfrontera.setTipo_recurso5( rs1.getString("tipo_recurso5"));
                            reqfrontera.setrecurso1( rs1.getString("recurso1") );
                            reqfrontera.setrecurso2( rs1.getString("recurso2") );
                            reqfrontera.setrecurso3( rs1.getString("recurso3") );
                            reqfrontera.setrecurso4( rs1.getString("recurso4") );
                            reqfrontera.setrecurso5( rs1.getString("recurso5") );
                            reqfrontera.setprioridad1( rs1.getString("prioridad1") );
                            reqfrontera.setprioridad2( rs1.getString("prioridad2") );
                            reqfrontera.setprioridad3( rs1.getString("prioridad3") );
                            reqfrontera.setprioridad4( rs1.getString("prioridad4") );
                            reqfrontera.setprioridad5( rs1.getString("prioridad5") );
                            reqfrontera.setError("");
                        }
                    }

                    VecReqFron.add(reqfrontera);
                }
            }


        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA REQFRONTERA" + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return VecReqFron;
    }
    
    
    
    public Vector listarReqFronAct(String fecini, String fecfin , String dstrct )throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null,rs1=null;
        PoolManager poolManager = null;
        
        
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(Select_ReqFronAct);
                st.setString(1,fecini);
                st.setString(2,fecfin);
                st.setString(3,dstrct);
                st.setString(4,fecini);
                st.setString(5,dstrct);
                
                rs = st.executeQuery();
                
                VecReqFron =  new Vector();
                
                while (rs.next()){
                    reqfrontera = new ReqFrontera();
                    reqfrontera.setcliente( rs.getString("cliente") );
                    reqfrontera.setnumrem(rs.getString("ot") );
                    reqfrontera.setorirem( rs.getString("ori_ot") );
                    reqfrontera.setdesrem( rs.getString("des_ot") );
                    reqfrontera.setnumpla( rs.getString("oc") );
                    reqfrontera.setoripla( rs.getString("ori_oc") );
                    reqfrontera.setdespla( rs.getString("des_oc") );
                    reqfrontera.setfecdispo( rs.getString("fec_dis") );
                    reqfrontera.setfecpla(rs.getString("fecha_oc"));
                    st = con.prepareStatement("select stdjobgen, tipo_recurso1, tipo_recurso2,tipo_recurso3,tipo_recurso4,tipo_recurso5,recurso1,recurso2, recurso3, recurso4,"+
                    " recurso5, prioridad1, prioridad2, prioridad3,prioridad4,prioridad5 from stdjob where std_job_no = ?  ");
                    st.setString(1,rs.getString("stdjob"));
                    
                    rs1 = st.executeQuery();
                    if (rs1.next()){
                        if( rs1.getString("stdjobgen").trim().equals("") || rs1.getString("stdjobgen").equalsIgnoreCase("CASUAL") ){
                            reqfrontera.setstd_job_no( rs.getString("stdjob") );
                            reqfrontera.setTipo_recurso1(" ");
                            reqfrontera.setTipo_recurso2(" ");
                            reqfrontera.setTipo_recurso3(" ");
                            reqfrontera.setTipo_recurso4(" ");
                            reqfrontera.setTipo_recurso5(" ");
                            reqfrontera.setrecurso1( "" );
                            reqfrontera.setrecurso2( "" );
                            reqfrontera.setrecurso3( "" );
                            reqfrontera.setrecurso4( "" );
                            reqfrontera.setrecurso5( "" );
                            reqfrontera.setprioridad1( "0" );
                            reqfrontera.setprioridad2( "0" );
                            reqfrontera.setprioridad3( "0" );
                            reqfrontera.setprioridad4( "0" );
                            reqfrontera.setprioridad5( "0" );
                            reqfrontera.setError("Stdjob General No encontrado");
                        }
                        else{
                            reqfrontera.setstd_job_no( rs1.getString("stdjobgen") );
                            reqfrontera.setTipo_recurso1( rs1.getString("tipo_recurso1"));
                            reqfrontera.setTipo_recurso2( rs1.getString("tipo_recurso2"));
                            reqfrontera.setTipo_recurso3( rs1.getString("tipo_recurso3"));
                            reqfrontera.setTipo_recurso4( rs1.getString("tipo_recurso4"));
                            reqfrontera.setTipo_recurso5( rs1.getString("tipo_recurso5"));
                            reqfrontera.setrecurso1( rs1.getString("recurso1") );
                            reqfrontera.setrecurso2( rs1.getString("recurso2") );
                            reqfrontera.setrecurso3( rs1.getString("recurso3") );
                            reqfrontera.setrecurso4( rs1.getString("recurso4") );
                            reqfrontera.setrecurso5( rs1.getString("recurso5") );
                            reqfrontera.setprioridad1( rs1.getString("prioridad1") );
                            reqfrontera.setprioridad2( rs1.getString("prioridad2") );
                            reqfrontera.setprioridad3( rs1.getString("prioridad3") );
                            reqfrontera.setprioridad4( rs1.getString("prioridad4") );
                            reqfrontera.setprioridad5( rs1.getString("prioridad5") );
                            reqfrontera.setError("");
                        }
                    }
                    
                    
                    VecReqFron.add(reqfrontera);
                }
            }
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA REQFRONTERA MOD" + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return VecReqFron;
    }
    
    public void EliminarReq_fronAct(String numpla, String stdjob )throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if (con != null){
                st = con.prepareStatement(Delete_reqFron);
                st.setString(1,numpla);
                st.setString(2,stdjob);
                
                st.executeUpdate();
                
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ELIMINACION REQFRONTERA " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        
    }
    
    public Vector listarReqFronRegistrados(String fecini, String fecfin )throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(Select_ReqFronReg);
                st.setString(1,fecini);
                st.setString(2,fecfin);
                rs = st.executeQuery();
                
                VecReqFron =  new Vector();
                
                while (rs.next()){
                    reqfrontera = new ReqFrontera();
                    reqfrontera.setnumrem(rs.getString("ot") );
                    reqfrontera.setorirem( rs.getString("ori_ot") );
                    reqfrontera.setdesrem( rs.getString("des_ot") );
                    reqfrontera.setnumpla( rs.getString("oc") );
                    reqfrontera.setoripla( rs.getString("ori_oc") );
                    reqfrontera.setdespla( rs.getString("des_oc") );
                    reqfrontera.setstd_job_no( rs.getString("stdjob") );
                    
                    VecReqFron.add(reqfrontera);
                }
            }
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA REQCLIENTE" + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return VecReqFron;
    }
    
    //buscar agensia asociada X nombre
    public String Buscar_agasocXnom( String nomciu )throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String Valor = "";
        
        try{
            //////System.out.println("Voy a realizar la conexcion con BD sot");
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(Select_AgasocXnom);
                st.setString(1,nomciu);
                
                rs = st.executeQuery();
                
                
                while (rs.next()){
                    Valor = rs.getString("agasoc");
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA de Agencia Asociada" + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return Valor;
        
    }
    //buscar agensia asociada X codigo
    public String Buscar_agasocXcod( String codciu )throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String Valor = "";
        
        try{
            //////System.out.println("Voy a realizar la conexcion con BD sot");
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(Select_AgasocXcod);
                st.setString(1,codciu);
                
                rs = st.executeQuery();
                
                
                while (rs.next()){
                    Valor = rs.getString("agasoc");
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA de Agencia Asociada" + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return Valor;
        
    }
    // buaca el tipo de recurso en tipo_rec
    public String Buscar_tiporec( String trec, String dstrct)throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String Valor = "";
        
        try{
            //////System.out.println("Voy a realizar la conexcion con BD sot");
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(Select_tiporec);
                st.setString(1,trec );
                st.setString(2,dstrct);
                
                rs = st.executeQuery();
                
                
                while (rs.next()){
                    Valor = rs.getString("tipo");
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA de tipo recurso" + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return Valor;
        
    }
    
    //busca la fecha y la hora en que se fue la carga
    public String Buscar_Fechadespacho( String numpla, String t )throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String Valor = "", fecha="",hor="";
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(Select_Fecha);
                st.setString(1,numpla);
                
                
                rs = st.executeQuery();
                
                
                while (rs.next()){
                    fecha = rs.getString("fec");
                    hor  = rs.getString("hor");
                }
            }
            if ( fecha != null  ){
                
                int tiempo = (int)(Double.parseDouble(t)*60);
                int ano=Integer.parseInt(fecha.substring(0,4));
                int mes=Integer.parseInt(fecha.substring(4,6));
                int dia= Integer.parseInt(fecha.substring(6,8));
                int hora=Integer.parseInt(hor.substring(0,3));
                int min=Integer.parseInt(hor.substring(4,5));
                int sec=Integer.parseInt(hor.substring(6,7));
                Calendar c = Calendar.getInstance();
                c.set(ano,mes,dia,hora,min+tiempo,sec);
                Valor = c.get(c.YEAR)+"-"+agregar0(c.get(c.MONTH))+"-"+agregar0(c.get(c.DATE))+" "+agregar0(c.get(c.HOUR_OF_DAY))+":"+agregar0(c.get(c.MINUTE))+":"+agregar0(c.get(c.SECOND));
            }
            else
                Valor =" ";
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA de Agencia Asociada" + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return Valor;
        
    }
    //busca el tiempo de la tabla tramo
    public String Buscar_Tiempo( String origen, String destino )throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String Valor = "";
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(Select_tiempo);
                st.setString(1,origen);
                st.setString(2,destino);
                
                
                rs = st.executeQuery();
                
                
                while (rs.next()){
                    Valor = rs.getString("tiempo");
                    
                }
                if (Valor.length() == 0)
                    Valor = " ";
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA de FECHA" + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return Valor;
        
    }
    
    //insertar la informacion a la tabla req_cliente
    public void llenarReqcliente( ReqCliente reqcliente )throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                
                st = con.prepareStatement(Select_Consulta);
                st.setString(1,reqcliente.getdstrct_code());
                st.setInt(2,reqcliente.getnum_sec() );
                st.setString(3,reqcliente.getstd_job_no() );
                st.setString(4, reqcliente.getnumpla() );
                st.setString(5,reqcliente.getfecha_dispo() );
                st.setString(6,reqcliente.getcliente() );
                st.setString(7,reqcliente.getorigen() );
                st.setString(8,reqcliente.getdestino() );
                st.setString(9,reqcliente.getclase_req() );
                st.setString(10,reqcliente.getTipo_recurso1() );
                st.setString(11,reqcliente.getTipo_recurso2() );
                st.setString(12,reqcliente.getTipo_recurso3() );
                st.setString(13,reqcliente.getTipo_recurso4() );
                st.setString(14,reqcliente.getTipo_recurso5() );
                st.setString(15,reqcliente.getrecurso1() );
                st.setString(16,reqcliente.getrecurso2() );
                st.setString(17,reqcliente.getrecurso3() );
                st.setString(18,reqcliente.getrecurso4() );
                st.setString(19,reqcliente.getrecurso5() );
                st.setString(20,reqcliente.getprioridad1() );
                st.setString(21,reqcliente.getprioridad2() );
                st.setString(22,reqcliente.getprioridad3() );
                st.setString(23,reqcliente.getprioridad4() );
                st.setString(24,reqcliente.getprioridad5() );
                st.setString(25,reqcliente.getUsuario_creacion() );
                st.setString(26,reqcliente.getfecha_creacion() );
                st.setString(27,reqcliente.getUsuario_actualizacion());
                st.setString(28,reqcliente.getfecha_actualizacion() );
                st.setString(29,reqcliente.getId_rec_tra());
                
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            e.printStackTrace();
            ////System.out.println("Error... el req cliente existe");
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        
    }
    
    public void ModificarReqcliente( ReqCliente reqcliente )throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                
                st = con.prepareStatement(actreqclien);
                st.setString(1,reqcliente.getcliente() );
                st.setString(2,reqcliente.getorigen() );
                st.setString(3,reqcliente.getdestino() );
                st.setString(4,reqcliente.getclase_req() );
                st.setString(5,reqcliente.getTipo_recurso1() );
                st.setString(6,reqcliente.getTipo_recurso2() );
                st.setString(7,reqcliente.getTipo_recurso3() );
                st.setString(8,reqcliente.getTipo_recurso4() );
                st.setString(9,reqcliente.getTipo_recurso5() );
                st.setString(10,reqcliente.getrecurso1() );
                st.setString(11,reqcliente.getrecurso2() );
                st.setString(12,reqcliente.getrecurso3() );
                st.setString(13,reqcliente.getrecurso4() );
                st.setString(14,reqcliente.getrecurso5() );
                st.setString(15,reqcliente.getprioridad1() );
                st.setString(16,reqcliente.getprioridad2() );
                st.setString(17,reqcliente.getprioridad3() );
                st.setString(18,reqcliente.getprioridad4() );
                st.setString(19,reqcliente.getprioridad5() );
                st.setString(20,reqcliente.getUsuario_actualizacion());
                st.setString(21,reqcliente.getfecha_actualizacion() );
                st.setString(22,reqcliente.getdstrct_code());
                st.setInt(23,reqcliente.getnum_sec() );
                st.setString(24,reqcliente.getstd_job_no() );
                st.setString(25, reqcliente.getnumpla() );
                st.setString(26,reqcliente.getfecha_dispo() );
                
                
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL INSERTAR EN LA TABLA REQCLIENTE" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        
    }
    public String agregar0( int val ){
        if (val < 10 ){
            return "0"+val;
        }
        else
            return val+"";
    }
    
    //////////////alpall
    /********************************** METODOS NUEVOS *************************/
    
    /**
     * Obtiene todos los requerimientos del cliente que tengan el estado dado
     * @return Vector un vector con los requerimientos que tengan el estado dado
     */
    public Vector obtenerRequerimientosEnEstado( String estado ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Vector datos = new Vector();
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra" );
            
            if ( con != null ) {
                
                st = con.prepareStatement( selectByEstado );
                st.setString( 1, estado );
                rs = st.executeQuery();
                //System.out.println("Query para buscar requerimientos :"+st.toString());
                while ( rs.next() ) {
                    //  //System.out.println("Encontro datos...");
                    ReqCliente r = new ReqCliente();
                    r.setdstrct_code( rs.getString( "dstrct_code" ) );
                    r.setestado( rs.getString( "estado" ) );
                    r.setnum_sec( rs.getInt( "num_sec" ) );
                    r.setstd_job_no( rs.getString( "std_job_no" ) );
                    r.setnumpla( rs.getString( "numpla" ) );
                    r.setfecha_dispo( rs.getString( "fecha_dispo" ) );
                    r.setcliente( rs.getString( "cliente" ) );
                    r.setorigen( rs.getString( "origen" ) );
                    r.setAgaAsoc(rs.getString("agasoc"));
                    r.setdestino( rs.getString( "destino" ) );
                    r.setclase_req( rs.getString( "clase_req" ) );
                    r.setTipo_recurso1( rs.getString( "tipo_rec1" ) );
                    r.setTipo_recurso2( rs.getString( "tipo_rec2" ) );
                    r.setTipo_recurso3( rs.getString( "tipo_rec3" ) );
                    r.setTipo_recurso4( rs.getString( "tipo_rec4" ) );
                    r.setTipo_recurso5( rs.getString( "tipo_rec5" ) );
                    r.setrecurso1( rs.getString( "recurso1" ) );
                    r.setrecurso2( rs.getString( "recurso2" ) );
                    r.setrecurso3( rs.getString( "recurso3" ) );
                    r.setrecurso4( rs.getString( "recurso4" ) );
                    r.setrecurso5( rs.getString( "recurso5" ) );
                    r.setprioridad1( rs.getString( "prioridad1" ) );
                    r.setprioridad2( rs.getString( "prioridad2" ) );
                    r.setprioridad3( rs.getString( "prioridad3" ) );
                    r.setprioridad4( rs.getString( "prioridad4" ) );
                    r.setprioridad5( rs.getString( "prioridad5" ) );
                    r.setfecha_asign( rs.getString( "fecha_asing" ) );
                    r.setnuevafecha_dispo( rs.getString( "nuevafecha_dispo" ) );
                    r.setid_rec_cab( rs.getString( "id_rec_cab" ) );
                    r.setid_rec_tra( rs.getString( "id_rec_tra" ) );
                    r.setId_rec_tra( rs.getString("id_rec_tra"));
                    r.setfecha_posibleentrega( rs.getString( "fecha_posibleentrega" ) );
                    r.setnuevafecha_dispo( rs.getString( "nuevafecha_dispo" ) );
                    r.settipo_asign(rs.getString("tipo_asing"));
                    r.setInternacional(rs.getBoolean("internacional"));
                    //r.setInternacional(false);
                    r.setPaisD(rs.getString("pd"));
                    r.setPaisO(rs.getString("po"));
                    r.setAgaAsocDest(rs.getString("agDest"));
                    if(rs.getBoolean("internacional")&& !rs.getBoolean("inter")){
                        Vector v =this.crearRequerimientoInternacional(r);
                        if(v.size()>0){
                            ReqCliente r2 = (ReqCliente) v.elementAt(0);
                            r.setFrontera(r2.getdestino());
                            
                        }
                    }
                    datos.addElement( r );
                    
                }
            }
        }
        catch ( SQLException e ) {
            throw new SQLException( "ERROR AL SELECCIONAR EN LA TABLA REQCLIENTE" +
            e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO" +
                    e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        return datos;
    }
    
    //dstrct_code, num_sec, std_job_no, fecha_dispo, numpla
    public ReqCliente obtenerRequerimiento( String dstrct_code, String num_sec, String std_job_no,
    String fecha_dispo, String numpla ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        ReqCliente r = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra" );
            if ( con != null ) {
                st = con.prepareStatement( SQL_OBTENER_REQUERIMIENTO );
                st.setString( 1, dstrct_code );
                st.setString( 2, num_sec );
                st.setString( 3, std_job_no );
                st.setString( 4, fecha_dispo );
                st.setString( 5, numpla );
                
                rs = st.executeQuery();
                //GenerarReporteDiarioAction.imprimirExcepcion( new Exception(st.toString()), "c:/sqlReq.txt" );
                if ( rs.next() ) {
                    r = this.crearRequerimiento( rs );
                }
                
            }
        }
        catch ( SQLException e ) {
            //GenerarReporteDiarioAction.imprimirExcepcion( e, "c:/errorReq.txt" );
            
            throw new SQLException( "ERROR AL SELECCIONAR EN LA TABLA REQCLIENTE" +
            e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO" +
                    e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        return r;
    }
    
    private ReqCliente crearRequerimiento( ResultSet rs ) throws SQLException {
        ReqCliente r = new ReqCliente();
        
        try{
            r.setdstrct_code( rs.getString( "dstrct_code" ) );
            r.setestado( rs.getString( "estado" ) );
            r.setnum_sec( rs.getInt( "num_sec" ) );
            r.setstd_job_no( rs.getString( "std_job_no" ) );
            r.setnumpla( rs.getString( "numpla" ) );
            r.setfecha_dispo( rs.getString( "fecha_dispo" ) );
            r.setcliente( rs.getString( "cliente" ) );
            r.setorigen( rs.getString( "origen" ) );
            r.setAgaAsoc(rs.getString("agasoc"));
            r.setdestino( rs.getString( "destino" ) );
            r.setclase_req( rs.getString( "clase_req" ) );
            r.setTipo_recurso1( rs.getString( "tipo_rec1" ) );
            r.setTipo_recurso2( rs.getString( "tipo_rec2" ) );
            r.setTipo_recurso3( rs.getString( "tipo_rec3" ) );
            r.setTipo_recurso4( rs.getString( "tipo_rec4" ) );
            r.setTipo_recurso5( rs.getString( "tipo_rec5" ) );
            r.setrecurso1( rs.getString( "recurso1" ) );
            r.setrecurso2( rs.getString( "recurso2" ) );
            r.setrecurso3( rs.getString( "recurso3" ) );
            r.setrecurso4( rs.getString( "recurso4" ) );
            r.setrecurso5( rs.getString( "recurso5" ) );
            r.setprioridad1( rs.getString( "prioridad1" ) );
            r.setprioridad2( rs.getString( "prioridad2" ) );
            r.setprioridad3( rs.getString( "prioridad3" ) );
            r.setprioridad4( rs.getString( "prioridad4" ) );
            r.setprioridad5( rs.getString( "prioridad5" ) );
            r.setfecha_asign( rs.getString( "fecha_asing" ) );
            r.setnuevafecha_dispo( rs.getString( "nuevafecha_dispo" ) );
            r.setid_rec_cab( rs.getString( "id_rec_cab" ) );
            r.setid_rec_tra( rs.getString( "id_rec_tra" ) );
            r.setfecha_posibleentrega( rs.getString( "fecha_posibleentrega" ) );
            r.setnuevafecha_dispo( rs.getString( "nuevafecha_dispo" ) );
            r.settipo_asign(rs.getString("tipo_asing"));
            r.setInternacional(rs.getBoolean("internacional"));
            r.setPaisD(rs.getString("paisd"));
            r.setPaisO(rs.getString("paiso"));
        }catch ( SQLException e ) {
            //GenerarReporteDiarioAction.imprimirExcepcion( e, "c:/errorReq.txt" );
            throw new SQLException( "ERROR AL SELECCIONAR EN LA TABLA REQCLIENTE" +
            e.getMessage() + "" + e.getErrorCode() );
        }
        return r;
    }
    
    /**Diogenes 15-11-2005**/
    /**
     * Metodo viajeInternacional, busca el origen y destino en la tabla ciudad
     *       returna true si tienen pais diferente
     * @autor : Ing. Diogenes Bastidas Morales
     * @param : codigo de origen, codigo destino
     * @version : 1.0
     */
    
    public boolean viajeInternacional( String origen, String destino )throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                
                st = con.prepareStatement(viajeInternacional);
                st.setString(1,origen);
                st.setString(2,destino );
                
                
                rs = st.executeQuery();
                
                if ( rs.next() ) {
                    if( ! rs.getString("origen").equals( rs.getString("destino") ) ){
                        sw=true;
                    }
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL INSERTAR EN LA TABLA REQCLIENTE" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return sw;
    }
    
    /**
     * Metodo buscarFronteraAsoc, busca la frontera asociada al codigo de la ciudad
     *       returna el codigo del la  frontera
     * @autor : Ing. Diogenes Bastidas Morales
     * @param : codigo de la ciudad
     * @version : 1.0
     */
    
    public String buscarFronteraAsoc( String codciu )throws SQLException{
        
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String frontera = "";
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                
                st = con.prepareStatement(buscarFrontera);
                st.setString(1,codciu);
                
                
                rs = st.executeQuery();
                
                if ( rs.next() ) {
                    frontera = rs.getString("frontera_asoc");
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR LA FRONTERA " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return frontera;
    }
    
    /**
     * Metodo crearRequerimientoInternacional
     * @autor : Karen Reales
     * @param : Requerimiento a internacionalizar
     * @return: Vector de Requerimientos
     * @version : 1.0
     */
    
    public Vector crearRequerimientoInternacional(ReqCliente ri )throws SQLException{
        
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        Vector reqInt = new Vector();
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                /*
                 *Obtenemos el standard y buscamos la WO del standard para especificar
                 *la frontera
                 */
                ////System.out.println("Se va a crear dos requerimientos....");
                String sj = ri.getstd_job_no();
                ////System.out.println("El standard es : "+sj);
                String frontera1="";
                String frontera2="";
                String  origen=ri.getorigen();
                String  destino=ri.getdestino();
                ReqCliente r1= ReqCliente.copiarRequerimiento(ri);
                ReqCliente r2= ReqCliente.copiarRequerimiento(ri);
                int sw=0;
                
                st = con.prepareStatement("select wo_type from stdjob where dstrct_code='FINV' and std_job_no =?");
                st.setString(1,sj);
                rs = st.executeQuery();
                ////System.out.println("El query del standard es : "+st.toString());
                if ( rs.next() ) {
                    ////System.out.println("Se encontro el standard....");
                    if(rs.getString("wo_type").equals("RM")){
                        ////System.out.println("La frontera es Paraguachon....");
                        frontera1 = ri.getPaisO().equals("CO")?"PA":"PA";
                        frontera2 = ri.getPaisO().equals("CO")?"PA":"PA";
                    }
                    else if(rs.getString("wo_type").equals("RC")){
                        ////System.out.println("La frontera es Cucuta....");
                        frontera1 = ri.getPaisO().equals("CO")?"CU":"ST";
                        frontera2 = ri.getPaisO().equals("CO")?"ST":"CU";
                    }
                    else{
                        frontera1 = ri.getdestino();
                        frontera2 = ri.getdestino();
                        sw=1;
                    }
                    /*
                     *El primer requerimiento va del origen a la agencia asociada a la frontera del origen
                     */
                    r1.setOrigen_ini(ri.getorigen());
                    r1.setDestino_ini(ri.getdestino());
                    r1.setorigen(origen);
                    r1.setdestino(frontera1);
                    r1.setAgaAsocDest(frontera1);
                    ////System.out.println("El primer requerimiento va de: "+r1.getorigen()+" hasta la frontera: "+r1.getdestino());
                    ////System.out.println("Fecha de Disponibilidad del primer requerimiento : "+r1.getfecha_dispo());
                    reqInt.add(0,r1);
                    
                    if(sw==0){
                        
                    /*
                     *El segundo requerimiento va del la agencia asociada a la frontera2 del origen al destino
                     */
                        r2.setOrigen_ini(ri.getorigen());
                        r2.setDestino_ini(ri.getdestino());
                        r2.setAgaAsoc(frontera2);
                        r2.setorigen(frontera2);
                        r2.setdestino(destino);
                        ////System.out.println("El segundo requerimiento va de: "+r2.getorigen()+" hasta la frontera: "+r2.getdestino());
                        ////System.out.println("Fecha de Disponibilidad del segundo requerimiento : "+r2.getfecha_dispo());
                        reqInt.add(1,r2);
                    }else{
                        reqInt.add(1,null);
                    }
                }
                
                ////System.out.println("Listo Termine de crear los requerimientos....");
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR LA FRONTERA " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return reqInt;
    }
    /**
     * Metodo actualizaRequerimientoInternacional
     * @autor : Karen Reales
     * @param : Requerimiento a internacionalizar
     * @return: Vector de Requerimientos
     * @version : 1.0
     */
    
    public void actualizarRequerimientoInternacional(ReqCliente requerimiento )throws SQLException{
        
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        Vector reqInt = new Vector();
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                
                st = con.prepareStatement( "update req_cliente set origen_ini=? , destino_ini=? , destino=? where dstrct_code = ? and num_sec = ? and std_job_no = ? and fecha_dispo = ? and numpla = ?" );
                st.setString( 1, requerimiento.getOrigen_ini() );
                st.setString( 2, requerimiento.getDestino_ini());
                st.setString( 3, requerimiento.getdestino());
                st.setString( 4, requerimiento.getdstrct_code() );
                st.setInt( 5, requerimiento.getnum_sec() );
                st.setString( 6, requerimiento.getstd_job_no() );
                st.setString( 7, requerimiento.getfecha_dispo() );
                st.setString(8, requerimiento.getnumpla() );
                ////System.out.println("Query actualizacion req "+st.toString());
                st.execute();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL ACTUALIZAR EL ORIGEN Y DESTINO INICIAL DEL REQUERIMIENTO " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    /* Metodo  buscarTipoClaseRecurso, busca el tipo y la calse del recurso
     * @param: placa
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public Hashtable buscarTipoClaseRecurso(String placa) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Hashtable info = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_CLASE_TIPO_RECURSO);
                st.setString(1, placa );
                
                rs = st.executeQuery();
                
                if (rs.next()){
                    info = new Hashtable();
                    info.put("Clase", rs.getString("recurso"));
                    info.put("Tipo", rs.getString("tipo_recurso"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR IDENTIDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return info;
    }
    
   
}
