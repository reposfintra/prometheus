package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;


public class Proveedor_TiquetesService{
    
    private Proveedor_TiquetesDAO proveedor;
    
    Proveedor_TiquetesService(){
        
        proveedor = new Proveedor_TiquetesDAO();
    }
    
    public boolean existProveedor(String nit, String sucursal )throws SQLException{
        
        return proveedor.existProveedor(nit,sucursal);
        
    }
    
    
    public List getProveedoresTIQUETES(String dstrct )throws SQLException{
        
        List proveedores=null;
        
        proveedor.searchProveedorTIQUETES(dstrct);
        proveedores = proveedor.getProveedorTIQUETES();
        
        return proveedores;
        
    }
    
    public boolean existProveedoresTiquetes(String dstrct )throws SQLException{
        
        return proveedor.existProveedorTiquetes(dstrct);
        
    }
    public Proveedor_Tiquetes getProveedor( )throws SQLException{
        
        return proveedor.getProveedor();
        
    }
    
    public void buscaProveedor(String nit, String sucursal)throws SQLException{
        try{
            proveedor.searchProveedor(nit,sucursal);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    public void insertProveedor(Proveedor_Tiquetes pt, String base)throws SQLException{
        try{
            proveedor.setProveedor(pt);
            proveedor.insertProveedor(base);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    public void anularProveedor(Proveedor_Tiquetes pt)throws SQLException{
        try{
            proveedor.setProveedor(pt);
            proveedor.anularProveedor();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    public void updateProveedor(Proveedor_Tiquetes pt)throws SQLException{
        try{
            proveedor.setProveedor(pt);
            proveedor.updateProveedor();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    public void consultaProveedor(String ciudad, String nombre, String nit)throws SQLException{
        try{
            proveedor.consultar(ciudad, nombre, nit);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    
    public Vector getProveedores(){
        return proveedor.getProveedores();
    }
    
    /*Diogenes
     */
    public boolean existProveedorAnulado(String nit, String sucursal)throws SQLException{
            return proveedor.existProveedorAnulado(nit, sucursal);
    }
    
    public void activarProveedor(Proveedor_Tiquetes pt)throws SQLException{
            try{
                proveedor.setProveedor(pt);
                proveedor.activarProveedor();
            }
            catch(SQLException e){
                throw new SQLException(e.getMessage());      
            }
    }
}    






