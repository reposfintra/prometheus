/*
 * NitDAO.java
 *
 * Created on 26 de mayo de 2005, 03:33 PM
 */

package com.tsp.operation.model;


import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;

/**
 *
 * @author  kreales
 */
public class NitDAO {
    
    /** Creates a new instance of NitDAO */
    public NitDAO() {
    }
    public NitDAO(String dataBaseName) {
        this.dataBaseName = dataBaseName;
    }
    private Nit prop;
    private Vector propietarios;
    private String dataBaseName;
    //AMENDEZ 20050708
    private static final String SEARCH_NIT="SELECT * FROM  NIT  WHERE CEDULA = ?  ";
    
    // INSERT
    //AMENDEZ 20050708 
    private static final String INSERT_NIT=
    "INSERT INTO NIT (estado, cedula, id_mims, nombre, direccion, codciu, coddpto, codpais, "+
                     "telefono, celular, e_mail, sexo, fechanac, fechacrea, usuariocrea, fechaultact, "+
                     "usuario) "+
             "VALUES ('A',?,?,?,?,?,?,?,?,?,?,?,?,now(),?,now(),?)";
       
    //  UPDATE
    //AMENDEZ 20050708
    private static final String UPDATE_NIT=
    "UPDATE NIT SET estado = ?, id_mims = ?, nombre = ?, direccion = ?, codciu = ?, "+
                   "coddpto = ?, codpais = ?, telefono = ?, celular = ?, e_mail = ?, "+
                   "sexo = ?, fechanac = ?, usuario = ?, fechaultact = now() "+
             "WHERE cedula = ?";
      
      
      //PROPIETARIO....
    //ALEJANDRO
    private final String SQL_OBTENER_NOMBRE = "select nombre from nit where cedula = ?";
    
    //JUANM
    private final String SQL_GETPROP    = "SELECT NOMBRE, DIRECCION, TELEFONO, CODCIU FROM NIT WHERE CEDULA = ?";
    
   public void insertProp()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            if(con!=null){
                st = con.prepareStatement("insert into identificacion_prop (cedula, p_nombre, s_nombre, p_apellido, s_apellido,dstrct,base, creation_user ) values (?,?,?,?,?,?,?,?)");
                st.setString(1, prop.getNit());
                st.setString(2, prop.getPnombre());
                st.setString(3, prop.getSnombre());
                st.setString(4, prop.getPapellido());
                st.setString(5, prop.getSapellido());
                st.setString(6, prop.getDstrct());
                st.setString(7, prop.getBase());
                st.setString(8, prop.getUser());
                st.executeUpdate();
               
                st = con.prepareStatement("insert into nit (cedula, nombre, base, creation_user ) values (?,?,?,?)");
                st.setString(1, prop.getNit());
                st.setString(2, prop.getPnombre()+" "+prop.getSnombre()+" "+prop.getPapellido()+" "+prop.getSapellido());
                st.setString(3, prop.getBase());
                st.setString(4, prop.getUser());
                st.executeUpdate();
               
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DEL NIT" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
    }
    public void updateProp()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            if(con!=null){
                st = con.prepareStatement("update identificacion_prop set  p_nombre=?, s_nombre=?, p_apellido=?, s_apellido=?, user_update=? where cedula=? and dstrct=? and base=?");
                st.setString(1, prop.getPnombre());
                st.setString(2, prop.getSnombre());
                st.setString(3, prop.getPapellido());
                st.setString(4, prop.getSapellido());
                st.setString(5, prop.getUser());
                st.setString(6, prop.getNit());
                st.setString(7, prop.getDstrct());
                st.setString(8, prop.getBase());
                st.executeUpdate();
               
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DEL NIT" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
    }
   
    public void deleteProp()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            if(con!=null){
                st = con.prepareStatement("update identificacion_prop  set reg_status  ='A' where cedula=? and dstrct=? and base=?");
                st.setString(1, prop.getNit());
                st.setString(2, prop.getPnombre());
                st.setString(3, prop.getSnombre());
                st.setString(4, prop.getPapellido());
                st.setString(5, prop.getSapellido());
                st.setString(6, prop.getDstrct());
                st.setString(7, prop.getBase());
                st.setString(8, prop.getUser());
                st.executeUpdate();
               
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DEL NIT" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
    }
   
    public boolean estaProp()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            if(con!=null){
                st = con.prepareStatement("select * from identificacion_prop where cedula = ? and base =? and dstrct =? and reg_status = ''");
                st.setString(1, prop.getNit());
                st.setString(2, prop.getBase());
                st.setString(3, prop.getDstrct());
                rs = st.executeQuery();
                if(rs.next()){
                    sw = true;
                }
                st = con.prepareStatement("select * from nit where cedula = ?");
                st.setString(1, prop.getNit());
                rs = st.executeQuery();
                if(rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL  PROPIETARIO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
        return sw;
    }
    public void listaPropietarios()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        propietarios = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            if(con!=null){
                st = con.prepareStatement("select * from identificacion_prop where base =? and reg_status = ''");
                st.setString(1, prop.getBase());
                rs = st.executeQuery();
                propietarios = new Vector();
                while(rs.next()){
                    prop = new Nit();
                    prop.setNit(rs.getString("Cedula"));
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL  PROPIETARIO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
    }
    
    
    /**
     * Getter for property propietarios.
     * @return Value of property propietarios.
     */
    public java.util.Vector getPropietarios() {
        return propietarios;
    }
    
    /**
     * Setter for property propietarios.
     * @param propietarios New value of property propietarios.
     */
    public void setPropietarios(java.util.Vector propietarios) {
        this.propietarios = propietarios;
    }
    
    /**
     * Getter for property prop.
     * @return Value of property prop.
     */
    public com.tsp.operation.model.beans.Nit getProp() {
        return prop;
    }
    
    /**
     * Setter for property prop.
     * @param prop New value of property prop.
     */
    public void setProp(com.tsp.operation.model.beans.Nit prop) {
        this.prop = prop;
    }
    /****************************************************************************************************/
    //AMENDEZ 20050708
    
    public NitSot searchNit(String id) throws SQLException{
           PoolManager poolManager  = PoolManager.getInstance();
           Connection conPostgres = poolManager.getConnection(this.dataBaseName);
           if (conPostgres == null)
              throw new SQLException("Sin conexion");
           PreparedStatement st=null;
           ResultSet rs=null;
           NitSot nit=null;
           try{
                st= conPostgres.prepareStatement(SEARCH_NIT);
                st.setString(1, id);
                rs=st.executeQuery();
                if(rs!=null)
                  if(rs.next()){
                       nit = new NitSot();
                       nit.setEstado(rs.getString(1));
                       nit.setCedula( rs.getString(2) );
                       nit.setId_mims( rs.getString(3) );
                       nit.setNombre( rs.getString(4) );
                       nit.setDireccion( rs.getString(5) );
                       nit.setCodciu( rs.getString(6) );
                       nit.setCoddpto( rs.getString(7) );
                       nit.setCodpais( rs.getString(8) );
                       nit.setTelefono( rs.getString(9) );
                       nit.setCellular( rs.getString(10) );
                       nit.setE_mail( rs.getString(11) );
                       nit.setFechaultact(rs.getString(12));
                       nit.setUsuario(rs.getString(13));
                       nit.setFechacrea(rs.getString(14));
                       nit.setUsuariocrea(rs.getString(15));
                       nit.setSexo(rs.getString(16) );
                       nit.setFechanac(rs.getString(17));
                  }
           }
           catch(Exception e){
               throw new SQLException("Error en la busqueda especifica del nit..." + id + "---" + e.getMessage());
           }
           finally{
               if(st!=null) st.close();
               if(rs!=null) rs.close();  
               poolManager.freeConnection(this.dataBaseName,conPostgres);
           }
           return nit;
    }
    
    public void insertNit(NitSot nit)throws SQLException {        
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection(this.dataBaseName);
        if (conPostgres == null)
          throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        try{
            st= conPostgres.prepareStatement(INSERT_NIT);
            st.setString(1, nit.getCedula());
            st.setString(2, nit.getId_mims());
            st.setString(3, nit.getNombre());
            st.setString(4, nit.getDireccion());
            st.setString(5, nit.getCodciu());
            st.setString(6, nit.getCoddpto());
            st.setString(7, nit.getCodpais());
            st.setString(8, nit.getTelefono());
            st.setString(9, nit.getCellular());
            st.setString(10, nit.getE_mail());
            st.setString(11, nit.getSexo());
            st.setString(12, nit.getFechanac());
            st.setString(13, nit.getUsuariocrea());
            st.setString(14, nit.getUsuario());           
            st.executeUpdate();            
        }catch(Exception e){
            throw new SQLException("No se pudo insertar el nit  "+ nit.getCedula() + " en nit  " + " -->" + e.getMessage());
        }
        finally{
           if(st!=null) st.close();
           poolManager.freeConnection(this.dataBaseName,conPostgres);
        } 
    }
 
     //_______________________________________________________________________________________
     //   UPDATE 
     
    public void updateNit(NitSot nit)throws SQLException {        
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection(this.dataBaseName);
        if (conPostgres == null)
          throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        try{
           if(!nit.getCedula().equals("")) {
                st= conPostgres.prepareStatement(UPDATE_NIT);
                st.setString(1, nit.getEstado());
                st.setString(2, nit.getId_mims());
                st.setString(3, nit.getNombre());
                st.setString(4, nit.getDireccion());
                st.setString(5, nit.getCodciu());
                st.setString(6, nit.getCoddpto());
                st.setString(7, nit.getCodpais());
                st.setString(8, nit.getTelefono());
                st.setString(9, nit.getCellular());
                st.setString(10, nit.getE_mail());
                st.setString(11, nit.getSexo());
                st.setString(12, nit.getFechanac());
                st.setString(13, nit.getUsuario());
                st.setString(14, nit.getCedula());            
                st.executeUpdate();             
           }  
        }catch(Exception e){
            throw new SQLException("No se pudo modificar el nit "+ nit.getCedula() +" en nit" + "---" + e.getMessage());
        }
        finally{
           if(st!=null) st.close();
           poolManager.freeConnection(this.dataBaseName,conPostgres);
        } 
    }
    public String obtenerNombre(String cedcon) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( this.dataBaseName );
            if ( con != null ) {
                PreparedStatement ps = con.prepareStatement( SQL_OBTENER_NOMBRE );
                ps.setString( 1, cedcon );
                rs = ps.executeQuery();
                if (rs.next()){
                    return rs.getString(1);
                }
            }
            return "(no encontrado)";
        }
        catch(Exception e){
            throw new SQLException(
                "ERROR DURANTE EL PROCESO DE LISTAR LOS RECURSOS DISPONIBLES " +
                e.getMessage() + " " /*+ e.getErrorCode()*/ );
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                                            e.getMessage() );
                }
            }

            if ( con != null ) {
                poolManager.freeConnection(this.dataBaseName,  con );
            }
        }

    }
    
    public void getPropietario(String cedcon) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( this.dataBaseName );
            if ( con != null ) {
                PreparedStatement ps = con.prepareStatement( this.SQL_GETPROP );
                ps.setString( 1, cedcon );
                rs = ps.executeQuery();
                if (rs.next()){
                    prop = new Nit();
                    prop.setName(rs.getString("Nombre"));
                    prop.setAddress(rs.getString("Direccion"));
                    prop.setPhone(rs.getString("Telefono"));
                    prop.setAgCode(rs.getString("Codciu"));
                }
            }
        }
        catch(Exception e){
            throw new SQLException(
                "ERROR DURANTE EL PROCESO DE LISTAR LOS RECURSOS DISPONIBLES " +
                e.getMessage() + " " /*+ e.getErrorCode()*/ );
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                                            e.getMessage() );
                }
            }

            if ( con != null ) {
                poolManager.freeConnection(this.dataBaseName,  con );
            }
        }

    }
    /**
     * Metodo buscarUsuarioPorNit_Nombre, metodo que busca un usuario por cudula o por nombre
     * @param:
     * @autor : Ing. Jdelarosa
     * @version : 1.0
     */
    public void buscarUsuarioPorNit_Nombre(String ced_nom) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        propietarios = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( this.dataBaseName );
            if ( con != null ) {
                PreparedStatement ps = con.prepareStatement( "SELECT nombre, cedula FROM nit WHERE cedula LIKE ? OR UPPER(nombre) LIKE UPPER(?) ORDER BY nombre" );
                ps.setString( 1, ced_nom+"%" );
                ps.setString( 2, ced_nom+"%" );
                rs = ps.executeQuery();
                propietarios = new  Vector();
                while (rs.next()){
                    prop = new Nit();
                    prop.setName( rs.getString("nombre")!=null?rs.getString("nombre"):"" );
                    prop.setNit( rs.getString("cedula")!=null? rs.getString("cedula"):"" );
                    propietarios.add( prop );
                }
            }
        }
        catch(Exception e){
            throw new SQLException(
                "ERROR DURANTE EL PROCESO DE LISTAR LOS RECURSOS DISPONIBLES " + e.getMessage() + " ");
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                                            e.getMessage() );
                }
            }

            if ( con != null ) {
                poolManager.freeConnection(this.dataBaseName,  con );
            }
        }

    }
}
