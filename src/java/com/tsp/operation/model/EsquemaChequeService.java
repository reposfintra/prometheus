
package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.operation.model.beans.*;


public class EsquemaChequeService {
    
   private EsquemaChequeDAO  EsquemaDataAccess;
   private EsquemaCheque     esquema;
   private List              listado;
   private List              bancos;
   
   
 // Inicializamos las variables  

    public EsquemaChequeService(){
       this.EsquemaDataAccess = new EsquemaChequeDAO();
       this.esquema = null;
       this.listado = null;
       this.bancos  = null;
    }
    public EsquemaChequeService(String dataBaseName){
       this.EsquemaDataAccess = new EsquemaChequeDAO(dataBaseName);
       this.esquema = null;
       this.listado = null;
       this.bancos  = null;
    }
   
    public void reset(){
        this.esquema = null;
        this.listado = null;
    }
    
    

    
 //--- insertamos el registro de esquema
    public void insert(EsquemaCheque esquema, String usuario, String base) throws SQLException{       
       try{
          EsquemaDataAccess.insert(esquema, usuario,base);  
       }catch(Exception e){
          throw new SQLException(e.getMessage());  
       }
    }
   
    
   //--- buscamos un esquemas  especifico
    public EsquemaCheque searchEsquema(EsquemaCheque esquema)throws SQLException{
       EsquemaCheque objeto=null;
       try{
          objeto = EsquemaDataAccess.searchEsquema(esquema); 
       }catch(Exception e){
          throw new SQLException(e.getMessage());   
       }
       return objeto;
    }
    
    
  //--- buscamos el listado de esquemas  
    public List search()throws SQLException{
       try{
          listado = EsquemaDataAccess.search(); 
       }catch(Exception e){
          throw new SQLException(e.getMessage());   
       }
       return listado;
    }
    
    
 //--- buscamos el listado de bancos
     public List searchBancos()throws SQLException{
       try{
          bancos = EsquemaDataAccess.searchBanck(); 
       }catch(Exception e){
          throw new SQLException(e.getMessage());   
       }
       return bancos;
    }
    
     
     
 //--- eliminacion
     public void delete(EsquemaCheque esquema)throws SQLException{
        try{
          EsquemaDataAccess.delete(esquema);
        }catch(Exception e){
          throw new SQLException(e.getMessage());   
        }
     }
     
     
 //--- actualizacion
     public void update(EsquemaCheque esquema)throws SQLException{
        try{
          EsquemaDataAccess.update(esquema);
        }catch(Exception e){
          throw new SQLException(e.getMessage());   
        }
     }
     
     
     
 //--- setiamos variables
     
     public void setEsquema(EsquemaCheque esquema){
         this.esquema = esquema;
     }
     
     public void setList(List lista){
         this.listado = lista;
     }
    
 //--- devolvemos los objetos
    
    public EsquemaCheque getEsquema(){
      return this.esquema;  
    }
    
    public List getList(){
      return this.listado;
    }
    
    public List getBancos(){
        return this.bancos;
    }
    
}//end SERVICE
