/*
 * RecursosdispService.java
 *
 * Created on 17 de junio de 2005, 12:18 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  Jm
 */
public class RecursosdispService {
    RecursosdispDAO rdDAO;
    
    /** Creates a new instance of RecursosdispService */
    public RecursosdispService() {
        rdDAO = new RecursosdispDAO();
    }
    
    public Recursosdisp getRdisp() {
        return rdDAO.getRd();
    }
    
    /**
     * Setter for property rd.
     * @param rd New value of property rd.
     */
    public void setRdisp(Recursosdisp rd) {
        rdDAO.setRd(rd);
    }
    
    /**
     * Getter for property recursosd.
     * @return Value of property recursosd.
     */
    public java.util.Vector getRecursosdisp() {
        return rdDAO.getRecursosd();
    }
    
    /**
     * Setter for property recursosd.
     * @param recursosd New value of property recursosd.
     */
    public void setRecursosdisp(java.util.Vector recursosd) {
        rdDAO.setRecursosd(recursosd);
    }
    public void list( String distrito )throws SQLException{
        rdDAO.list(distrito);
    }
    
    public void insert()throws SQLException{
        rdDAO.insert();
    }
    
    public void insertEquipoA()throws SQLException{
        rdDAO.insertEquipoA();
    }
    public void DeleteAll()throws SQLException{
        rdDAO.DeleteAll();
    }
    public void DeleteAct( String d , String fi, String ff  )throws SQLException{
       rdDAO.DeleteAct(d, fi, ff );
    }
    
    public void DeleteRD( String placa )throws SQLException{
       rdDAO.DeleteRD(placa);
    }
    
    public void Update()throws SQLException{
        rdDAO.Update();
    }
    
    public void UpdateRA()throws SQLException{
        rdDAO.UpdateRA();
    }
    
    ////alpy
    public Vector obtenerRecursosNoAsignados(ReqCliente req, int KI, int KF) throws SQLException {
        return rdDAO.obtenerRecursosNoAsignados(req,KI,KF);
    }

    public boolean puedeViajarEnRuta(String placa, String origen, String destino) throws
        SQLException {
        return rdDAO.puedeViajarEnRuta(placa,origen,destino);
    }

    public boolean puedeTransportarCliente(String placa, String cliente) throws
        SQLException {
        return rdDAO.puedeTransportarCliente(placa,cliente);
    }

    public void realizarAsignacion( Recursosdisp cabezote, Recursosdisp trailer,
                                    ReqCliente requerimiento, String tipoAsig ) throws SQLException {
        rdDAO.realizarAsignacion(cabezote,trailer,requerimiento,tipoAsig);
    }
    
    public java.util.Vector getAsignados() {
        return rdDAO.getAsignados();
    }
    
    /**
     * Setter for property asignados.
     * @param asignados New value of property asignados.
     */
    public void setAsignados(java.util.Vector asignados) {
        rdDAO.setAsignados(asignados);
    }
    
    public Recursosdisp obtenerRecurso(String distrito, String placa, String fechaDisponibilidad) throws SQLException {
        return rdDAO.obtenerRecurso(distrito, placa, fechaDisponibilidad);
    }

     public String buscarRecursosxTipo( String tipo ) throws SQLException {
         return rdDAO.buscarRecursosxTipo(tipo);
     }
     /*13.12.05*/
     public boolean BuscarRecurso(String placa) throws SQLException {
         return rdDAO.BuscarRecurso(placa);
     }
    
     /*Jescandon 11-01-06*/
     /**
      * Metodo Search_record, Busca un registro de la tabla record_tabla dada una placa
      * @autor : Ing. Juan Manuel Escandon Perez
      * @see   : Search_record - RecursosdispDAO
      * @param : String Numero de placa
      * @version : 1.0
      */
     public void Search_record ( String placa )throws Exception{
         try{
             rdDAO.Search_record (placa);
         }catch(Exception e){
             throw new Exception ("Error en Search_record [RecursosdispService]...\n"+e.getMessage ());
         }
     }
     
     /** Metodo Actualizar_record, Actualiza la tabla de record_placa
      * @autor : Ing. Juan Manuel Escandon Perez
      * @version : 1.0
      */
     public void Actualizar_record ()throws Exception{
         try{
             List lista = rdDAO.list_viajes ();
             Iterator it = lista.iterator ();
             while(it.hasNext ()){
                 Recursosdisp rd = (Recursosdisp)it.next ();
                 if( !rdDAO.Search_r (rd.getPlaca () )){
                     rdDAO.Insert_record ("FINV", rd.getNo_Viajes (), rd.getPlaca (), rd.getUsuario_creacion ());
                 }
                 else{
                     rdDAO.Update_record (rd.getNo_Viajes (), rd.getUsuario_creacion (), rd.getPlaca ());
                 }
             }
         }
         catch(Exception e){
             throw new Exception ("Error en Actualizar_record [RecursosdispService]...\n"+e.getMessage ());
         }
     }
     
     /*Jescandon 13-01-06*/     
     /**
       * Metodo Search_Wgroup, Busca todos los standares relacionados a una placa a travez de los
       * work_group
       * @autor : Ing. Juan Manuel Escandon Perez
       * @see   : Search_Wgroup - RecursosdispDAO
       * @param : String Numero de placa
       * @version : 1.0
       */
     public String Search_Wgroup( String placa )throws Exception{
         try{
             return rdDAO.Search_Wgroup(placa);
         }catch(Exception e){
             throw new Exception("Error en Search_Wgroup [RecursosdispService]...\n"+e.getMessage());
         }
     }
     
     /* M�todo obtenerRecursosByWhere, obtiene registros de recursos_disp
     *       que contengan el dato en la columna dada
     * @author: Osvaldo P�rez Ferrer
     * @param: colum, columna por la que se va a filtar
     * @param: dato, cadena a buscar
     * @throws: En caso de que un error de base de datos ocurra. 
     */
    public void obtenerRecursosByWhere(String where, String dstrct) throws SQLException{
        rdDAO.obtenerRecursosByWhere(where, dstrct);
    }
    
    /* M�todo updateRecursoAsignar, actualiza el recurso disponible
     *         despu�s de asignado 
     * @author: Osvaldo P�rez Ferrer
     * @param: placa, placa del recurso
     * @param: fechadisp, fecha del recurso a actualizar
     * @param: destino, nuevo destino del recurso
     * @param: fecha, nueva fecha_disp para el recurso
     * @param: usuario, usuario que asign� recurso
     * @throws: En caso de que un error de base de datos ocurra.
     */
    public void updateRecursoAsignar(String placa, String fechadisp, String destino, String fecha, String usuario) throws SQLException{
        rdDAO.updateRecursoAsignar(placa, fechadisp, destino, fecha, usuario);
    }
    
    /* M�todo obtenerRecursos, obtiene recursos de la tabla recursos
     * @author: Osvaldo P�rez Ferrer     
     * @throws: En caso de que un error de base de datos ocurra.
     */
    public Vector obtenerRecursos() throws SQLException{
         return rdDAO.obtenerRecursos();
    }
    
    public boolean BuscarRecursoTrailer(String placa) throws SQLException {
         return rdDAO.BuscarRecursoTrailer(placa);
     }
    /**
     * Metodo  insertFrontera, inserta los recursos disponibles por frontera
     * @param: 
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
      public void insertFrontera()throws SQLException{
          rdDAO.insertFrontera();
      }
      public void marcarFrontera()throws SQLException{
        rdDAO.marcarFrontera();
    }
    
}
