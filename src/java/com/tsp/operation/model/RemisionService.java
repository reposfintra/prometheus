
package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.operation.model.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;


public class RemisionService {
    
    private RemisionDAO  RemisionDataAccess;
    private List listadoRemisiones;
    private List listadoImpresion;
    private Remision  remision;
    private List movimiento;
    
    // Inicializamos las variables
    
    public RemisionService() {
        RemisionDataAccess = new  RemisionDAO();
        listadoRemisiones = null;
        listadoImpresion  = null;
        remision = null;
    }
    
    
    //--- Devolvemos el listado general de remisiones
    public List getList(){
        return this.listadoRemisiones;
    }
    
    public void setList(List lista){
        this.listadoRemisiones = lista;
    }
    
    public void setPrinter(List lista){
        this.listadoImpresion = lista;
    }
    
    public List getPrinter(){
        return this.listadoImpresion;
    }
    
    
    
    //--- devolvemos la lista de remisiones seleccionadas y no impresos
    public List getListImpresion(){
        listadoImpresion=null;
        if(this.listadoRemisiones!=null && this.listadoRemisiones.size()>0){
            Iterator it = this.listadoRemisiones.iterator();
            int sw = 0;
            while(it.hasNext()){
                Remision   remision = (Remision) it.next();
                if(!remision.getPrinter() &&  remision.getSeleted()){
                    if(sw==0) listadoImpresion = new LinkedList();
                    sw++;
                    listadoImpresion.add(remision);
                }
            }
        }
        return listadoImpresion;
    }
    
    
    //--- Cancelamos la remision de la lista de impresión.
    public void cancel(int id){
        Iterator it = this.listadoRemisiones.iterator();
        if(it!=null){
            while(it.hasNext()){
                Remision remision = (Remision)it.next();
                if(remision.getId()==id){
                    remision.setPrinter(false);
                    remision.setSeleted(false);
                    break;
                }
            }
        }
    }
    
    
    
    //---  borramos la remision de la lista
    public void delete(Remision delete){
        List list=null;
        if(this.listadoRemisiones!=null){
            Iterator it= this.listadoRemisiones.iterator();
            list = new LinkedList();
            while(it.hasNext()){
                Remision remision = (Remision) it.next();
                if(remision.getId()!=delete.getId())
                    list.add(remision);
            }
        }
        this.listadoRemisiones=list;
    }
    
    
    
    //--- devolvemos la remision para ser impresa
    public Remision getRemisionPrinter(int id){
        Remision next=null;
        Iterator it = listadoImpresion.iterator();
        if(it!=null){
            while(it.hasNext()){
                Remision remision = (Remision)it.next();
                if(remision.getId()==id){
                    next = remision;
                    break;
                }
            }
        }
        return next;
    }
    
    
    //--- activamos la remision para ser impresa
    public void active(int id){
        if(this.listadoRemisiones!=null){
            Iterator it = this.listadoRemisiones.iterator();
            if(it!=null){
                while(it.hasNext()){
                    Remision remision = (Remision)it.next();
                    if(remision.getId()==id){
                        remision.setPrinter(false);
                        remision.setSeleted(true);
                        break;
                    }
                }
            }
        }
    }
    
    
    
    //--- variable remision del service
    public void setRemision(Remision objeto){
        this.remision = objeto;
    }
    
    public Remision getRemision(){
        return this.remision;
    }
    
    
    
    
    
    
    
    
    
    // METODOS  SQL
    
    
    //--- Verificamos que Existe serie banco
    public boolean ExistSerie()throws Exception{
        boolean estado=false;
        try{
            estado=RemisionDataAccess.ExisteSerie();
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return estado;
    }
    
    
    
    //---  Realizamos la consulta de las remisiones
    public String searchRemisiones(String proyecto, String distrito,String agencia, String login, String base)throws Exception{
        String comentario="";
        try{
            listadoRemisiones=RemisionDataAccess.QueryRemisiones(proyecto, distrito, agencia, login,base);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        if(listadoRemisiones==null || listadoRemisiones.size()==0) comentario="(Service) No hay remisiones para ser impresas....";
        return comentario;
    }
    
    
    
    
    
    // UPDATE
    
    //--- Actualizo la remision
    public  void  updateRemision(String remision, String distrito) throws Exception{
        try{
            RemisionDataAccess.ActualizarRemision(remision, distrito);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    //--- Actualizar la serie
    public void updateSeries(String distrito,String agencia,String banco, String ageBanco, String cuenta,String login) throws Exception{
        try{
            RemisionDataAccess.ActualizarSeries(distrito, agencia, banco,ageBanco, cuenta ,login);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    
    public Series getSerie(String distrito,String agencia,String banco, String ageBanco, String cuenta) throws Exception{
        Series serie=null;
        try{
            serie= RemisionDataAccess.getSeries(distrito,agencia,banco,ageBanco,cuenta);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return serie;
    }
    
    //--- Finalizar la serie
    public void finallySeries(String distrito,String agencia,String banco, String ageBanco,String cuenta, String usuario)throws Exception{
        try{
            RemisionDataAccess.FinalizarSeries(distrito, agencia,  banco, ageBanco,cuenta, usuario);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    
    //---  Actualizar Moviento de la OC
    public void updateMOVOC(String distrito,String agencia,String egreso,String item,String oc,String login) throws Exception{
        try{
            RemisionDataAccess.ActualizarMOVOC( distrito,agencia, egreso, item,oc,login);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    
    //---  Actualizamos el egreso
    public void updateEgreso(String distrito, String banco, String ageBanco,String egreso,String concepto, String nit,String nombre, String agencia,double valor1, double valor2,String moneda,String login)
    throws Exception {
        try{
            RemisionDataAccess.ActualizarEgreso(distrito,banco, ageBanco, egreso,concepto, nit,nombre, agencia,valor1,  valor2,moneda, login);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    
    
    //--- Actualizamos los detalles del egreso
    public void updateDetalleEgreso(String distrito, String banco, String ageBanco,String egreso,String item, String concepto,String oc,double valor1, double valor2,String moneda,String login)
    throws Exception{
        try{
            RemisionDataAccess.ActualizarDetalleEgreso(distrito,banco, ageBanco, egreso, item, concepto, oc, valor1, valor2, moneda, login);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    
    
    //--- buscamos los movimientos  de la planilla
    public List searchMovimiento(String oc, String distrito, String agencia)throws Exception{
        List movimiento=null;
        try{
            movimiento = RemisionDataAccess.BuscarMovimiento(oc, distrito, agencia);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return movimiento;
    }
    
    
    
    //OTRAS RUTINAS
    
    
    public DatosRem  searchDatosRem(String distrito, String planilla) throws SQLException{
        DatosRem dato=null;
        try{
            dato = RemisionDataAccess.getDatosRem(distrito, planilla);
        }catch(Exception e){
            throw new SQLException(e.getMessage());
        }
        return dato;
    }
    public DatosRem  searchDatosRem(String distrito, String planilla, String numrem) throws SQLException{
        DatosRem dato=null;
        try{
            dato = RemisionDataAccess.getDatosRem(distrito, planilla, numrem);
        }catch(Exception e){
            throw new SQLException(e.getMessage());
        }
        return dato;
    }
    
    
    public Plaaux searchAux(String distrito, String planilla) throws SQLException{
        Plaaux aux=null;
        try{
            aux = RemisionDataAccess.searchAux( distrito, planilla);
        }catch(Exception e){
            throw new SQLException(e.getMessage());
        }
        return aux;
    }
    
    
    public ValorAnticipo searchAnticipo(String distrito, String agencia, String planilla ,String concepto)throws SQLException{
        ValorAnticipo anticipo=null;
        try{
            anticipo = RemisionDataAccess.serachAnticipo( distrito, agencia, planilla , concepto);
        }catch(Exception e){
            throw new SQLException(e.getMessage());
        }
        return anticipo;
        
    }
    
    public Banco searchBancos(String distrito, String agencia, String moneda,String base) throws SQLException{
        Banco banco=null;
        try{
            banco = RemisionDataAccess.searchBancos(distrito, agencia, moneda, base);
        }catch(Exception e){
            throw new SQLException(e.getMessage());
        }
        return banco;
    }
    ///Diogenes
    public void buscarMovimiento(String oc, String distrito, String agencia)throws Exception{
        try{
            movimiento = RemisionDataAccess.BuscarMovimiento(oc, distrito, agencia);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * Getter for property movimiento.
     * @return Value of property movimiento.
     */
    public java.util.List getMovimiento() {
        return movimiento;
    }    
    
    /**
     * Setter for property movimiento.
     * @param movimiento New value of property movimiento.
     */
    public void setMovimiento(java.util.List movimiento) {
        this.movimiento = movimiento;
    }
    
}// FIN SERVICE
