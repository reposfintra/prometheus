/*
 * Class.java
 *
 * Created on 9 de noviembre de 2004, 02:45 PM
 */

package com.tsp.operation.model;

import java.sql.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import java.util.*;
/**
 *
 * @author  AMENDEZ
 */
public class MarcaDAO {
    private TreeMap cbxmarca;
    
    public static final String SQL_LISTAR =
    "SELECT * FROM marca_placa ORDER BY descripcion";
   
    /** Creates a new instance of CarroceriaDAO */
    public MarcaDAO() {
    }
    
    public void setCbxMarca(TreeMap marcas){
        this.cbxmarca = marcas;
    }
    
    public TreeMap getCbxMarca(){
        return this.cbxmarca;
    }
    
    public void listar() throws SQLException{
        cbxmarca = null;
        cbxmarca = new TreeMap();
        Connection con = null;
        Statement sttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            sttm = con.createStatement();
            rs = sttm.executeQuery(SQL_LISTAR);
            while (rs.next()) {
                cbxmarca.put(rs.getString(2), rs.getString(1));
                ////System.out.println("MARCAS " + cbxmarca.lastKey());
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR MARCA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (sttm != null){
                try{
                   sttm.close();
                }
                catch(SQLException e){
                   throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);                
            }
        }     
    }
    public String getMarca(String codigo) throws SQLException{
        Connection con = null;
        Statement sttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        String marca = "";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            sttm = con.createStatement();            
            rs = sttm.executeQuery("SELECT descripcion FROM marca_placa where codigo='"+codigo+"'");
            
            if (rs.next()) {
                marca = rs.getString(1);
            }
            return marca;
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR MARCA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (sttm != null){
                try{
                   sttm.close();
                }
                catch(SQLException e){
                   throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);                
            }
        }     
    }
    
}
