/*
 * MigracionRemesasAnuladasService.java
 *
 * Created on 21 de julio de 2005, 12:27 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;


/**
 *
 * @author  Henry
 */
public class MigracionPlanillaAnuladaAnticipoService {
    
    private MigracionPlanillaAnuladaAnticipoDAO dao;
    
    /** Creates a new instance of MigracionRemesasAnuladasService */
    public MigracionPlanillaAnuladaAnticipoService() {
    	dao = new MigracionPlanillaAnuladaAnticipoDAO();
    }
    public Vector obtenerPlanillasAnuladasSinAnticipo(String fecIni, String fecFin) throws SQLException{    	 
    	this.dao.obtenerPlanillasAnuladaSinAnticipo(fecIni,fecFin);
        return dao.getPlanillaAnuladaSinAnticipo();
    }    
    public Vector obtenerPlanillasAnuladasConAnticipo(String fecIni, String fecFin) throws SQLException{    	 
    	this.dao.obtenerPlanillasAnuladaConAnticipo(fecIni,fecFin);
        return dao.getPlanillaAnuladaConAnticipo();
    }    
    public String getUltimaFechaProceso() throws IOException{
        return this.dao.getUltimaFechaProceso();
    }
    public void cargarArchivoPropiedades() throws IOException{
        //this.dao.cargarPropiedades();
    }
    public void setUltimaFechaProceso(String fecha) throws Exception{
        try{
            this.dao.setUltimafechaCreacion(fecha);
        }catch(IOException e){            
            throw new Exception("ERROR ESCRIBIENDO EN EL ARCHIVO DE PROPIEDADES LA FECHA DE CORTE "+e.getMessage());            
        }
    }
        
}
