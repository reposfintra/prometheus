/*
 * Planilla_TiempoService.java
 *
 * Created on 24 de noviembre de 2004, 11:19 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  KREALES
 */
public class Planilla_TiempoService {
    
    private Planilla_TiempoDAO pla_tiempo;
    
    /** Creates a new instance of Planilla_TiempoService */
    public Planilla_TiempoService() {
        pla_tiempo= new Planilla_TiempoDAO();
    }
    public Planilla_Tiempo getTiempo(){
        return pla_tiempo.getPlaTiempo();
    }
     
    public void buscarUltFecha(String planilla)throws SQLException{
        try{
           pla_tiempo.buscaUltimaFecha(planilla);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public String insertTiempo(Planilla_Tiempo pla, String base)throws SQLException{
        try{
           pla_tiempo.setPlaTiempo(pla);
           return pla_tiempo.insertTiempo(base);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public String anularPlanilla(Planilla_Tiempo plat)throws SQLException{
        try{
            pla_tiempo.setPlaTiempo(plat);
            return pla_tiempo.anularPlaTiempo();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public String updateTiempo(Planilla_Tiempo plat)throws SQLException{
        pla_tiempo.setPlaTiempo(plat);
        return pla_tiempo.updatePlaTiempo();
    }
}
