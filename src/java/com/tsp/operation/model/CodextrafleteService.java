/*
 * CodextrafleteService.java
 *
 * Created on 10 de julio de 2005, 05:53 PM
 */

package com.tsp.operation.model;

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  INTEL
 */
public class CodextrafleteService {
    
    private CodextrafleteDAO codigoExtra;
    private Codextraflete Codextra;
    private List lista;
    /** Creates a new instance of CodextrafleteService */
    public CodextrafleteService() {
        codigoExtra = new CodextrafleteDAO();
    }
    
    
    public boolean existCodigoExtraflete(String codigo)throws SQLException{
        
        return codigoExtra.existCodigoExtraflete(codigo);
        
    }
    
    
    public void insertCodextraflete(Codextraflete codextra)throws SQLException{
        try{
            codigoExtra.setCodigoextraflete(codextra);
            codigoExtra.insertCodextraflete();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    
    public void updateCodextraflete(Codextraflete codextra)throws SQLException{
        try{
            codigoExtra.setCodigoextraflete(codextra);
             ////System.out.println("ya setio");
            codigoExtra.updateCodextraflete();
             ////System.out.println("ya modifico");
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public Codextraflete getconsultar() {
        return Codextra;
    }
    
    public void consultarCodextraflete(Codextraflete codextra)throws SQLException{
        try{
            codigoExtra.setCodigoextraflete(codextra);
            codigoExtra.consultarCodextraflete();
            Codextra = codigoExtra.getCodextraflete();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    
    
    public void deleteCodextraflete(Codextraflete codextra)throws SQLException{
        try{
            codigoExtra.setCodigoextraflete(codextra);
            codigoExtra.deleteCodextraflete();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    
   public List getListarCodextraflete() {
        return lista;
    }
    
    public void listarCodextraflete()throws SQLException{
        try{
            lista = null;
            lista = codigoExtra.listarCodextraflete();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    } 
    /**
     * Setter for property Codextra.
     * @param Codextra New value of property Codextra.
     */
    public void setCodextra(com.tsp.operation.model.beans.Codextraflete Codextra) {
        this.Codextra = Codextra;
    } 
    
     /**
     * Determina si un extraflete ya existe y cual es su estado
     * @autor Ing. Andr�s Maturana
     * @param codigo C�digo del extraflete
     * @throws <code>SQLException</code>
     * @version 1.0
     */
     public String existCodExtraflete(String codigo )throws SQLException{
         return this.codigoExtra.existCodExtraflete(codigo);
     }

    
    
   
}

