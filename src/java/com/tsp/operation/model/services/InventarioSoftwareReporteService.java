/******************************************************************
* Nombre ......................ReporteInventarioSoftwareService.java
* Descripci�n..................Clase Service para realizar el 
                               inventario de archivos slt
* Autor........................Ing. Armando Oviedo
* Fecha........................26/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.sql.*;

/**
 *
 * @author  Armando Oviedo
 */
public class InventarioSoftwareReporteService {
    
    private Vector listaProgramas;
    private InventarioSoftwareReporteDAO isdao;
    private Vector vectorBaseDatos;
    private Vector vectorResultados;    
    
    
    
   // Fernel 07/01/2006
   private List listaEquipos;
   private List listaSitios;
   private String equipo;
   private String sitio;
    
    
    
    
    
    /** Creates a new instance of ReporteInventarioSoftwareService */
    public InventarioSoftwareReporteService() {        
        isdao          = new InventarioSoftwareReporteDAO();
        listaEquipos   = new LinkedList();
        listaSitios    = new LinkedList();
    }
    
    
    
    
    
     /**
     * M�todo que busca los diferentes equipos en tabla general
     * @autor.......Fernel 07/01/2006              
     * @version.....1.0.     
     **/
    public void searchEquipos()throws Exception{
        try{
            this.listaEquipos = this.isdao.getTables(   this.isdao.TABLA_COMPUTO );
        }catch(Exception e){
            throw new Exception( e.getMessage());
        }
    }
    
    
    
    /**
     * M�todo que busca los diferentes sitios en tabla general
     * @autor.......Fernel 07/01/2006              
     * @version.....1.0.     
     **/
     public void searchSitios()throws Exception{
        try{
            this.listaSitios = this.isdao.getTables(   this.isdao.TABLA_SITIOS );
        }catch(Exception e){
            throw new Exception( e.getMessage());
        }
    }
     
     
    
    /**
     * M�todo que devuelva la lista de equipos
     * @autor.......Fernel 07/01/2006              
     * @version.....1.0.     
     **/
    public List getEquipos(){
        return this.listaEquipos;
    }
     
    
    
    /**
     * M�todo que devuelva la lista de sitios
     * @autor.......Fernel 07/01/2006              
     * @version.....1.0.     
     **/
    public List getSitios(){
        return this.listaSitios;
    }
        
    
    
     /**
     * M�todo que setea el equipo
     * @autor.......Fernel      
      * @version.....1.0.
     * @param.......String val
     **/
    public void setEquipo(String val){
        this.equipo = val;
    }
    
    /**
     * M�todo que setea el sitio
     * @autor.......Fernel      
      * @version.....1.0.
     * @param.......String val
     **/
    public void setSitio(String val){
        this.sitio = val;
    }
    
    
    /**
     * M�todo que devuelve el equipo
     * @autor.......Fernel      
      * @version.....1.0.
     **/
    public String getEquipo(){
        return this.equipo ;
    }
    
    
    /**
     * M�todo que devuelve el sitio
     * @autor.......Fernel      
      * @version.....1.0.
     **/
    public String getSitio(){
        return this.sitio ;
    }
    
    
    
    
    
    
    
    
    
    /**
     * M�todo que retorna un vector de objetos de tipo InventarioSoftware
     * @autor.......Armando Oviedo               
     * @version.....1.0.     
     **/
    public Vector getListaProgramas(){
        return this.listaProgramas;
    }
    
    
    
    
    /**
     * M�todo que busca el sitio en busca de archivos 
     * de todos los tipos exceptuando los .class
     * @autor.......Armando Oviedo          
     * @throws......Exception
     * @version.....1.0.
     * @param.......String ruta, Usuario user
     **/
    public void buscarListaProgramas(String ruta, Usuario user) throws Exception{
        try{
            if(listaProgramas==null){
                listaProgramas = new Vector();
            }
            File f = new File(ruta);
            if(f.isDirectory()){
                FilenameFilter filter = new FilenameFilter() {
                    public boolean accept( File path, String name){
                        String url = path.getAbsolutePath() + "\\" + name;
                        File f = new File(url);
                        return ( !name.toLowerCase().endsWith(".class") ) || ( f!=null && f.isDirectory() );
                    }
                };
                File []arc  =  f.listFiles(filter);
                File []file =  Ordenar(arc);
                for(int i=0;i<file.length;i++){
                    File tmp = file[i];
                    buscarListaProgramas(tmp.getAbsolutePath(), user);
                }
            }
            else{
                InventarioSoftware isoft = new InventarioSoftware();
                
                    isoft.setEquipo       (this.equipo );
                    isoft.setSitio        (this.sitio  );
                    
                    isoft.setBase         (user.getBase());                
                    isoft.setDstrct       (user.getDstrct());
                    isoft.setCreationUser (user.getLogin());
                    isoft.setUserUpdate   (user.getLogin());                
                    isoft.setNombre       (getNombre(f.getName()));                            
                    java.util.Date d = new java.util.Date(f.lastModified());                                
                    isoft.setFecha        (d);                
                    isoft.setTamano       (f.length());
                    isoft.setRuta         (f.getAbsolutePath());
                    isoft.setExtension    (getExtension(f.getName()));
                    isoft.setProfundidad  (profundidad(f.getPath()));                      

                listaProgramas.add(isoft);
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }            
    
    
    
    
    
    /**
     * M�todo que retorna el nombre de un archivo sin su extensi�n
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......String nombre
     * @see.........InventarioSoftwareReporteDAO
     **/
    public String getNombre(String nombre){
        return this.isdao.getNombre(nombre);
    }
    
    /**
     * M�todo que retorna un vector de Strings que contiene
     * la cadena de la ruta del archivo, dividida por el caracter / 
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......String path
     * @see.........InventarioSoftwareReporteDAO
     **/
    public String[] profundidad(String path){
        return this.isdao.profundidad(path);
    }
    
    /**
     * M�todo que obtiene la extensi�n de un archivo
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......String nombre completo del archivo sin la ruta (String filename)
     * @see.........InventarioSoftwareReporteDAO
     **/
    public String getExtension(String filename){
        return this.isdao.getExtension(filename);
    }
    
    /**
     * Ordena un arreglo de archivos por el tipo (Directorio o Archivo).
     * @autor Ing. Armando Oviedo
     * @param arg El arreglo de archivos
     * @throws Exception
     * @version 1.0
     */
    public static File[] Ordenar(File []arg){
        File temp = null;
        for (int i = 0; i<arg.length;i++)
            for (int j = i+1; j<arg.length ;j++){
                if( arg[i].isFile() && arg[j].isDirectory()){
                    temp = arg[i];
                    arg[i] = arg[j];
                    arg[j] = temp;
                }
            }
        return arg;
    }        
    
    
    /**
     * M�todo que retorna un boolean si existen o no items en la base de datos
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     * @see.........InventarioSoftwareReporteDAO
     **/
    public boolean existeReporte() throws SQLException{
        return this.isdao.existeReporte(this.equipo, this.sitio);
    }
    
    
    /**
     * M�todo que graba un vector de tipo inventario software en la base de datos
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......Vector listaProgramas
     * @see.........InventarioSoftwareReporteDAO
     **/
    public void grabarInventario() throws SQLException{        
        this.isdao.grabarInventario(getListaProgramas());                
    }
    
    /**
     * M�todo que setea un objeto de tipo InventarioSoftware
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......InventarioSoftware is
     * @see.........InventarioSoftwareReporteDAO
     **/
    public void setInventarioSoftware(InventarioSoftware is){
        this.isdao.setInventarioSoftware(is);
    }
    
    /**
     * M�todo que setea un vector de tipo InventarioSoftware
     * @autor.......Armando Oviedo               
     * @version.....1.0.
     * @param.......Vector vecbd     
     **/
    public void setVectorBaseDatos(Vector vecbd){
        this.vectorBaseDatos = vecbd;
    }
    
    /**
     * M�todo que guetea un vector de objetos de tipo InventarioSoftware
     * @autor.......Armando Oviedo               
     * @version.....1.0.          
     **/
    public Vector getVectorBaseDatos(){
        return this.vectorBaseDatos;
    }
    
    /**
     * M�todo que setea un vector de tipo InventarioSoftware
     * @autor.......Armando Oviedo               
     * @version.....1.0.
     * @param.......Vector vecbd     
     **/
    public void setVectorResultados(Vector resultados){
        this.vectorResultados = resultados;
    }
    
    public Vector getVectorResultados(){
        return this.vectorResultados;
    }
    
    public void obtenerReporteBaseDatos() throws SQLException{
        this.isdao.obtenerReporteBaseDatos(this.equipo, this.sitio);
        setVectorBaseDatos(this.isdao.getVectorProgramas());
    }
    
    public void setListaProgramas(Vector listaProgramas){
        this.listaProgramas =listaProgramas;
    }
    
    
    
    
    //hay que encontrar una forma de marcar los items del sitio
    //M�todo que compara 2 vectores que corresponden al de base de datos y al del sitio
    public void compararInventario(Vector listabd, Vector listaSitio){
        vectorResultados = new Vector();
        for(int i=0;i<listabd.size();i++){
            InventarioSoftware tmpbd = (InventarioSoftware)(listabd.elementAt(i));
            int sw = 0;
            for(int j=0;j<listaSitio.size();j++){
                InventarioSoftware tmps = (InventarioSoftware)(listaSitio.elementAt(j));                
                //Si existe pero fue modificado
                if(tmpbd.getNombre().equalsIgnoreCase(tmps.getNombre()) && tmpbd.getRuta().equalsIgnoreCase(tmps.getRuta()) && tmpbd.getFecha().getTime()!= tmps.getFecha().getTime()){
                    tmpbd.setPropiedad("Modificado");                    
                    sw++;
                }
                //Si existe y son iguales
                else if(tmpbd.getNombre().equalsIgnoreCase(tmps.getNombre()) && tmpbd.getRuta().equalsIgnoreCase(tmps.getRuta()) && tmpbd.getFecha().getTime()== tmps.getFecha().getTime()){
                    tmpbd.setPropiedad("Iguales");
                    sw++;
                }                
            }
            //Si fue eliminado
            if(sw!=0){
                tmpbd.setPropiedad("Eliminado");
            }
            listabd.setElementAt(tmpbd, i);
        }
        
    }  
    
    
    public void deleteTablaInventarioSoftware() throws SQLException{
        this.isdao.deleteTablaInventarioSoftware(this.equipo, this.sitio);
    }
}
