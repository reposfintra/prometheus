package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.CajasEfectivoDAO;
import com.tsp.operation.model.beans.CajaIngreso;
import com.tsp.operation.model.beans.CajaPago;
import com.tsp.operation.model.beans.factura;
import com.tsp.operation.model.beans.factura_detalle;
import com.tsp.util.Util;
import java.util.ArrayList;
import java.util.Date;
import java.util.TreeMap;

/**
 * Service para el programa de cajas de efectivo
 * 6/02/2012
 * @author darrieta
 */
public class CajasEfectivoService {
    
    private CajasEfectivoDAO dao;

    public CajasEfectivoService() {
        dao = new CajasEfectivoDAO();
    }
    public CajasEfectivoService(String dataBaseName) {
        dao = new CajasEfectivoDAO(dataBaseName);
    }
    
    
    /**
     * Inserta un registro en la tabla caja_ingreso
     * @param ingreso bean con los datos a ingresar
     * @return sql generado
     * @throws Exception 
     */
    public String insertarIngresoCaja(CajaIngreso ingreso) throws Exception{
        return dao.insertarIngresoCaja(ingreso);
    }
    
    /**
     * consulta el saldo inicial para una fecha y si no existe lo calcula e inserta el registro
     * @param fecha fecha en la que se quiere consultar el saldo
     * @param agencia
     * @param dstrct
     * @param usuario
     * @return saldo inicial en la fechaInicial
     * @throws Exception 
     */
    public double obtenerSaldoInicial(Date fecha, String agencia, String dstrct, String usuario) throws Exception{       
        double saldoInicial = 0;
        String strFecha = Util.dateFormat(fecha); //Fecha convertida a string
        String maxFecha = dao.consultarMaxFecha(strFecha, agencia); //ultima fecha en el historial
        if(maxFecha == null){
            dao.insertarSaldoCaja(dstrct, agencia, strFecha, saldoInicial, usuario);
        }else{
            saldoInicial = dao.consultarSaldoInicial(dstrct, agencia, maxFecha);
            Date maxDate = Util.ConvertiraDate1(maxFecha);
            if(Util.fechasDiferenciaEnDias_v2(maxDate, fecha) != 0){                
                Double totalAnticipos = dao.consultarTotalAnticipos(agencia, maxFecha, Util.fechaMenosUnDia(strFecha));
                if(totalAnticipos != null){
                    saldoInicial -= totalAnticipos.doubleValue();
                }
                Double totalPagos = dao.consultarTotalPagos(dstrct, agencia, maxFecha, Util.fechaMenosUnDia(strFecha));
                if(totalPagos != null){
                    saldoInicial -= totalPagos.doubleValue();
                }
                dao.insertarSaldoCaja(dstrct, agencia, strFecha, saldoInicial, usuario);
            }
        }
        return saldoInicial;
    }
    
    /**
     * Obtiene el saldo en canje de una agencia
     * @param dstrct
     * @param agencia
     * @return
     * @throws Exception 
     */
    public double consultarSaldoTotalCanje(String dstrct, String agencia) throws Exception{
        return dao.consultarSaldoTotalCanje(dstrct, agencia);
    }
    
    /**
     * Obtiene los saldos en canje de una agencia
     * @param dstrct
     * @param agencia
     * @return TreeMap<String, Double>
     * @throws Exception 
     */
    public TreeMap<String, Double> consultarSaldosCanje(String dstrct, String agencia) throws Exception{
        return dao.consultarSaldosCanje(dstrct, agencia);
    }
    
    /**
     * Confirma la recepcion del dinero en caja
     * @param ingreso
     * @return sql generado
     * @throws Exception 
     */
    public String confirmarIngresoCaja(CajaIngreso ingreso) throws Exception{
        return dao.confirmarIngresoCaja(ingreso);
    }
    
    /**
     * Suma el saldo del ingreso confirmado al saldo inicial del dia
     * @param ingreso
     * @return sql generado
     * @throws Exception 
     */
    public String actualizarSaldoCaja(String dstrct, String agencia, String numIngreso, String usuario) throws Exception{
        return dao.actualizarSaldoCaja(dstrct, agencia, numIngreso, usuario);
    }
    
    /**
     * Obtiene los pagos realizados por una agencia en un rango de fechas
     * @param dstrct distrito del usuario en sesion
     * @param agencia agencia a buscar
     * @param fechaInicial fecha inicial
     * @param fechaFinal fecha final
     * @return ArrayList<CajaPago> con los resultados obtenidos
     * @throws Exception 
     */
    public ArrayList<CajaPago> consultarPagosAgencia(String dstrct, String agencia, String fechaInicial, String fechaFinal) throws Exception{
        return dao.consultarPagosAgencia(dstrct, agencia, fechaInicial, fechaFinal);
    }
    
    /**
     * Obtiene los anticipos realizados por una agencia en un rango de fechas
     * @param dstrct distrito del usuario en sesion
     * @param agencia agencia a buscar
     * @param fechaInicial fecha inicial
     * @param fechaFinal fecha final
     * @return ArrayList<CajaPago> con los resultados obtenidos
     * @throws Exception 
     */
    public ArrayList<CajaPago> consultarPagosAnticipos(String dstrct, String agencia, String fechaInicial, String fechaFinal) throws Exception{
        return dao.consultarPagosAnticipos(dstrct, agencia, fechaInicial, fechaFinal);
    }
    
    /**
     * Inserta un registro en la tabla caja_pago
     * @param pago
     * @return sql generado
     * @throws Exception 
     */
    public String insertarCajaPago(CajaPago pago) throws Exception{
        return dao.insertarCajaPago(pago);
    }
    
    /**
     * Obtiene el numero para la cxc a generar
     * @throws Exception 
     */
    public String consultarSerieCXC(String document_type) throws Exception{
        return dao.consultarSerieCXC(document_type);
    }
    
    public String ingresarFactura(factura factu,String distrito,String usuario,String base) throws Exception {
        return dao.ingresarFactura(factu, distrito, usuario, base);
    }
    
    public String ingresarDetalleFactura(factura_detalle factud,String distrito,String usuario,String base) throws Exception {
        return dao.ingresarDetalleFactura(factud, distrito, usuario, base);
    }
}
