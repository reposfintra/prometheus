 /**************************************************************************
 * Nombre clase: DescuentoTercmService.java                                *
 * Descripci�n: Clase que maneja los servicios de las consultas para la    *
 *              creacion de boletas                                        *
 * Autor: Ing. Ivan DArio Gomez Vanegas                                    *
 * Fecha: Created on 23 de diciembre de 2005, 08:20 AM                        *
 * Versi�n: Java 1.0                                                       *
 * Copyright: Fintravalores S.A. S.A.                                 *
 ***************************************************************************/

package com.tsp.operation.model.services;

import java.util.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  Ivan Dario Gomez Vanegas
 */
public class DescuentoTercmService {
    private DescuentoTercmDAO DescuentoTercmDataAccess;   
    private List listado;
    private List ListValores;
    private List ListaImpresion;
    private List Existe;
    private List ExisteImpresa;
    private List Prefactura;
    private List PrefacturaCreada;
    private List listaAnulacion;
    /** Creates a new instance of DescuentoTercmService */
    
    public DescuentoTercmService() {
        DescuentoTercmDataAccess = new DescuentoTercmDAO();
        listado                  = new LinkedList();
        ListValores              = new LinkedList();
        ListaImpresion           = new LinkedList();
        Existe                   = new LinkedList();
        ExisteImpresa            = new LinkedList();
        Prefactura               = new LinkedList();
        PrefacturaCreada         = new LinkedList();
        listaAnulacion           = new LinkedList();
    }
     /************************************************************************
     * Metodo   : BuscarValor, retorna una lista con los datos de la planilla.
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @see     : BuscarValor - DescuentoTercmDAO
     * @param   : recibe la planilla
     * @version : 1.0
     *************************************************************************/
    public void BuscarValor(String OC) throws Exception {
        try{
            this.listado = DescuentoTercmDataAccess.BuscarValor(OC); 
        }catch(Exception e){
            throw new Exception("Error en BuscarValor [DescuentoTercmService]...\n"+e.getMessage());
        }
    }
    
     /************************************************************************
     * Metodo   : Valor(), retorna una lista con los valores disponibles para una boleta.
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @see     : Valor - DescuentoTercmDAO
     * @param   : 
     * @version : 1.0
     *************************************************************************/
    public void Valor() throws Exception {
        try{
            this.ListValores = DescuentoTercmDataAccess.Valor(); 
        }catch(Exception e){
            throw new Exception("Error en Valor [DescuentoTercmService]...\n"+e.getMessage());
        }
    }
    
     /************************************************************************
     * Metodo   : GuardarBoletas, Guarda las boletas creadas, con todos
     *            los param recibidos en la jsp.
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @see     : GuardarBoletas - DescuentoTercmDAO
     * @param   : la planilla, placa, cedula, y  el numero de boletas a guardar 
     * @version : 1.0
     *************************************************************************/  
     public void GuardarBoletas(String OC, String Placa, int Conductor, String[]Boletas, int ValorOC, String Usuario) throws Exception {
        try{
             DescuentoTercmDataAccess.GuardarBoletas(OC, Placa, Conductor, Boletas, ValorOC, Usuario); 
        }catch(Exception e){
            throw new Exception("Error en GuardarBoletas [DescuentoTercmService]...\n"+e.getMessage());
        }
    }
     
      /************************************************************************
     * Metodo   : BuscarBoleta, Busca las boletas que no se han impreso
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @see     : BuscarBoleta - DescuentoTercmDAO
     * @param   : recibe la planilla
     * @version : 1.0
     *************************************************************************/
    public void BuscarBoleta(String OC) throws Exception {
        try{
            this.ListaImpresion = DescuentoTercmDataAccess.BuscarBoleta(OC); 
        }catch(Exception e){
            throw new Exception("Error en BuscarBoleta [DescuentoTercmService]...\n"+e.getMessage());
        }
    } 
    /************************************************************************
   * Metodo   : BuscarBoletaAnular, Busca las boletas que no se han impreso
   * @autor   : Ing. Ivan Dario Gomez Vanegas
   * @see     : BuscarBoletaAnular - DescuentoTercmDAO
   * @param   : recibe la planilla
   * @version : 1.0
   *************************************************************************/
    public void   BuscarBoletaAnular(String OC) throws Exception {
         try{
              this.listaAnulacion = DescuentoTercmDataAccess.BuscarBoletaAnular(OC); 
            }catch(Exception e){
                 throw new Exception("Error en BuscarBoletaAnular [DescuentoTercmService]...\n"+e.getMessage());
           }
    }
     /************************************************************************
     * Metodo   : UpdateFechaImpresion, guarda la fecha de impresion
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @see     : UpdateFechaImpresion - DescuentoTercmDAO
     * @param   : recibe la planilla
     * @version : 1.0
     *************************************************************************/
     public void UpdateFechaImpresion(String OC) throws Exception {
        try{
             DescuentoTercmDataAccess.UpdateFechaImpresion(OC); 
        }catch(Exception e){
            throw new Exception("Error en UpdateFechaImpresion [DescuentoTercmService]...\n"+e.getMessage());
        }
    }
     
      /************************************************************************
     * Metodo   : Verificar, Verifica si las boletas de una planilla fueron creadas
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @see     : Verificar - DescuentoTercmDAO
     * @param   : recibe la planilla
     * @version : 1.0
     *************************************************************************/
     public void Verificar(String OC) throws Exception {
        try{
             this.Existe = DescuentoTercmDataAccess.Verificar(OC); 
        }catch(Exception e){
            throw new Exception("Error en UpdateFechaImpresion [DescuentoTercmService]...\n"+e.getMessage());
        }
    }
    
       /************************************************************************
     * Metodo   : VerificarImpresa, Verifica si las boletas de una planilla 
     *            fueron creadas y si fueron impresas
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @see     : VerificarImpresa - DescuentoTercmDAO
     * @param   : recibe la planilla
     * @version : 1.0
     *************************************************************************/
     public void VerificarImpresa(String OC) throws Exception {
        try{
             this.ExisteImpresa = DescuentoTercmDataAccess.VerificarImpresa(OC); 
        }catch(Exception e){
            throw new Exception("Error en UpdateFechaImpresion [DescuentoTercmService]...\n"+e.getMessage());
        }
    }
       /************************************************************************
     * Metodo   : UpdatePrefactura, desmarca las boletas que no entran en la prefactura 
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @see     : UpdatePrefactura - DescuentoTercmDAO
     * @param   : recibe array con los numeros de las boletas
     * @version : 1.0
     *************************************************************************/
    public void UpdatePrefactura(String[] boletas) throws Exception {
         try{
             DescuentoTercmDataAccess.UpdatePrefactura(boletas);  
        }catch(Exception e){
            throw new Exception("Error en UpdateFacturada [DescuentoTercmService]...\n"+e.getMessage());
        }
    }
    
        /************************************************************************
     * Metodo   : PreFactura, Lista todas las boletas que ya estan impresas
     *            y no se han facturado
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @see     : PreFActura - DescuentoTercmDAO
     * @param   : fecha inicial y fecha final 
     * @version : 1.0
     *************************************************************************/
    public void PreFactura(String fecini, String fecfin) throws Exception {
        try{
            this.Prefactura = DescuentoTercmDataAccess.PreFActura(fecini,fecfin); 
        }catch(Exception e){
            throw new Exception("Error en BuscarBoleta [DescuentoTercmService]...\n"+e.getMessage());
        }
    }  
    
      /************************************************************************
     * Metodo   : Prefacturar, marca las boletas Prefacturadas 
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @see     : Prefacturar - DescuentoTercmDAO
     * @param   : recibe la fecha inicial y fecha final
     * @version : 1.0
     *************************************************************************/
     public void Prefacturar(String fecini, String fecfin) throws Exception {
          try{
             DescuentoTercmDataAccess.Prefacturar(fecini,fecfin);  
        }catch(Exception e){
            throw new Exception("Error en Prefacturar [DescuentoTercmService]...\n"+e.getMessage());
        }
     }
     /************************************************************************
     * Metodo   : BuscarPreFactura, Lista las boletas de la prefactura digitada
     *            y no se han facturado
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @see     : BuscarPreFactura - DescuentoTercmDAO
     * @param   : numer de la prefactura
     * @version : 1.0
     *************************************************************************/
     public void BuscarPreFactura(String NunPrefactura) throws Exception {
         try{
           this.PrefacturaCreada = DescuentoTercmDataAccess.BuscarPreFactura(NunPrefactura);  
         }catch(Exception e){
            throw new Exception("Error en BuscarPreFactura [DescuentoTercmService]...\n"+e.getMessage());
          }
     } 
     
      /************************************************************************
     * Metodo   : AnularBoletas, Anula las boletas de una oc 
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @see     : AnularBoletas - DescuentoTercmDAO
     * @param   : recibe la planilla
     * @version : 1.0
     *************************************************************************/
     public void AnularBoletas(String OC, String Usuario ) throws Exception {
          try{
               DescuentoTercmDataAccess.AnularBoletas(OC, Usuario);     
          }catch (Exception e){
              throw new Exception("Error en AnularBoletas [DescuentoTercmService]...\n"+e.getMessage());
          }       
     }
     
    public List getListado()          { return this.listado;         }
    public List getListValores()      { return this.ListValores;     }
    public List getListaImpresion()   { return this.ListaImpresion;  }
    public List getExiste()           { return this.Existe;          }
    public List getExisteImpresa()    { return this.ExisteImpresa;   }
    public List getPrefactura()       { return this.Prefactura;      }
    public List getPrefacturaCreada() { return this.PrefacturaCreada;}
    public List getListaAnulacion()   { return this.listaAnulacion;  }
    
   
    
}
