/*
 * Nombre        GenerarFacturasAvalService.java
 * Descripci�n   Clase para el acceso a los datos  del programa generar facturas de aval
 * Autor         Iris vargas
 * Fecha         28 de abril de 2012, 12:08 PM
 * Versi�n       1.0
 * Coyright      Geotech S.A.
 */
package com.tsp.operation.model.services;

import com.tsp.finanzas.contab.model.DAO.ComprobantesDAO;
import com.tsp.finanzas.contab.model.DAO.ContabilizacionNegociosDAO;
import com.tsp.finanzas.contab.model.DAO.MovAuxiliarDAO;
import com.tsp.finanzas.contab.model.beans.Comprobantes;
import com.tsp.operation.model.DAOS.ProcesoIndemnizacionDAO;
import com.tsp.operation.model.beans.BeanGeneral;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

/**
 * Clase para el acceso a los datos de generacion de facturas de aval
 * @author Iris Vargas
 */
public class ProcesoIndemnizacionServices {

    ProcesoIndemnizacionDAO dao;
    private ComprobantesDAO comproDAO;
    private ContabilizacionNegociosDAO contdao;
    private MovAuxiliarDAO movDAO;
    private  boolean                     process;

    public ProcesoIndemnizacionServices() {
        dao = new ProcesoIndemnizacionDAO();
        movDAO         = new  MovAuxiliarDAO();
        comproDAO      = new  ComprobantesDAO();
        contdao            = new  ContabilizacionNegociosDAO();
        process=false;
    }
    public ProcesoIndemnizacionServices(String dataBaseName) {
        dao = new ProcesoIndemnizacionDAO(dataBaseName);
        movDAO         = new  MovAuxiliarDAO(dataBaseName);
        comproDAO      = new  ComprobantesDAO(dataBaseName);
        contdao            = new  ContabilizacionNegociosDAO(dataBaseName);
        process=false;
    }
      
    public Comprobantes buscarNegocio(String negocio, String numero_titulo) throws java.lang.Exception {
        return dao.buscarNegocio(negocio, numero_titulo);

    }

    public String getAccountDES(String oc) throws Exception {
        return dao.getAccountDES(oc);
    }

    /**
     * M�todo que inserta el comprobante y comprodet
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String insertar(Comprobantes comprobante, String user) throws Exception {
        String error = "";
        try {
            error = evaluarComprobante(comprobante);
            if (error.equals("")) {
                List items = comprobante.getItems();
                List aux = new LinkedList();
                int sw = 0;
                // Validamos los items:
                if (items != null) {
                    for (int j = 0; j < items.size(); j++) {
                        Comprobantes comprodet = (Comprobantes) items.get(j);
                        String evaDet = evaluarComprobante(comprodet);
                        if (evaDet.equals("")) {
                            aux.add(comprodet);
                        } else {
                            error = "Error item " + j + " " + evaDet;   // Guardamos el item
                            sw = 1;
                            break;
                        }
                    }
                }
                if (sw == 0) {
                    items = aux;  // Items q cumplen para ser insertados
                    if (items != null && items.size() > 0) {
                        // Tomamos la secuencia:
                        int sec = comproDAO.getGrupoTransaccion();
                        comprobante.setGrupo_transaccion(sec);
                        error = contdao.InsertComprobante(comprobante, user);
                        error += contdao.InsertComprobanteDetalle2(items, sec, user);

                    } else {
                        error = "Error No presenta items validos";
                    }
                }
            } else {
                error = "Error" + error;
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(" INSERTAR COMPROBANTE NEGOCIOS : " + e.getMessage());
        }
        return error;
    }

    /**
     * M�todo que evalua datos v�lidos del comprobante y comprodet
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String evaluarComprobante(Comprobantes comprobante) throws Exception {
        String msj = "";
        try {
            //System.out.println("Evalua");
            // Campos comunes:
            if (comprobante.getDstrct().trim().equals("")) {
                msj += " No presenta distrito";
            }
            if (comprobante.getTipodoc().trim().equals("")) {
                msj += " No presenta tipo de documento";
            }
            if (comprobante.getNumdoc().trim().equals("")) {
                msj += " No presenta n�mero de documento";
            }
            if (comprobante.getPeriodo().trim().equals("")) {
                msj += " No presenta periodo de contabilizaci�n";
            }
            if (comprobante.getDetalle().trim().equals("")) {
                msj += " No presenta una descripci�n o detalle";
            }
            if (comprobante.getBase().trim().equals("")) {
                msj += " No presenta base";
            }


            String tipo = comprobante.getTipo();

            if (tipo.equals("C")) {

                if (comprobante.getTotal_debito() != comprobante.getTotal_credito()) {
                    msj += " Est� descuadrado en valores debitos y cr�ditos";
                }
                if (comprobante.getSucursal().trim().equals("")) {
                    msj += " No presenta sucursal";
                }
                if (comprobante.getFechadoc().trim().equals("")) {
                    msj += " No presenta fecha de documento";
                }
                if (comprobante.getTercero().trim().equals("")) {
                    msj += " No presenta tercero";
                }
                if (comprobante.getMoneda().trim().equals("")) {
                    msj += " No presenta moneda";
                }
                if (comprobante.getAprobador().trim().equals("")) {
                    msj += " No presenta aprobador";
                }

            } else {

                if (comprobante.getCuenta().trim().equals("")) {
                    msj += " No presenta cuenta";
                } else {
                    String cta = comprobante.getCuenta();
                    Hashtable cuenta = movDAO.getCuenta(comprobante.getDstrct(), cta);

                    if (cuenta != null) {

                        String requiereAux = (String) cuenta.get("auxiliar");
                        String requiereTercero = (String) cuenta.get("tercero");

                        // Tercero.
                        if (requiereTercero.equals("S") && comprobante.getTercero().trim().equals("")) {
                            msj += " La cuenta " + cta + " requiere tercero  y el item no presenta tercero";
                        }

                        // Auxiliar.
                        if (requiereAux.equals("S")) {

                            if (comprobante.getAuxiliar().trim().equals("")) {
                                msj += " La cuenta " + cta + " requiere auxiliar y el item no presenta auxiliar";
                            }

                            if (!contdao.existeCuentaSubledger(comprobante.getDstrct(), cta, comprobante.getAuxiliar())) {
                                msj += "La cuenta " + cta + " no tiene el subledger " + comprobante.getAuxiliar() + " asociado";
                            }
                        }

                    } else {
                        msj += " La cuenta " + cta + " no existe o no est� activa";
                    }

                }

            }

            System.out.println("Msg" + msj);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
        return msj;
    }

     public String ingresarCXC(BeanGeneral bg) throws SQLException {
         return dao.ingresarCXC(bg);
     }

     public String ingresarDetalleCXC(BeanGeneral bg, String item) throws SQLException {
         return dao.ingresarDetalleCXC(bg, item);
     }

     public String ingresarCXP(BeanGeneral bg) throws SQLException {
         return dao.ingresarCXP(bg);
     }

     public String ingresarDetalleCXP(BeanGeneral bg) throws SQLException {
              return dao.ingresarDetalleCXP(bg);
     }

    public Comprobantes itemscopy(Comprobantes neg, String parm, double parm2, String tipo) throws Exception {
        Comprobantes item = new Comprobantes();
        try {
            item.setTipo("D");
            item.setDstrct(neg.getDstrct());
            item.setTipodoc(neg.getTipodoc());
            item.setNumdoc(neg.getNumdoc());
            item.setGrupo_transaccion(neg.getGrupo_transaccion());
            item.setPeriodo(neg.getPeriodo());
            item.setTercero(neg.getTercero());
            item.setDocumento_interno(neg.getNumdoc());
            item.setDocrelacionado(neg.getDocrelacionado());
            item.setTipo_operacion(neg.getTipo_operacion());
            item.setTdoc_rel(neg.getTdoc_rel());
            item.setAbc(neg.getAbc());
            item.setUsuario(neg.getUsuario());
            item.setBase(neg.getBase());
            item.setCuenta(parm);
            item.setDetalle(dao.getAccountDES(parm));
            if (tipo.equals("D")) {
                item.setTotal_credito(0.0);
                item.setTotal_debito(parm2);
            } else {
                item.setTotal_credito(parm2);
                item.setTotal_debito(0.0);
            }
            item.setAuxiliar("");
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        return item;
    }

     /**
     * M�todo que los negocios que ya tienen vencido el plazo para indemnizar
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List getNegociosVencidaIndemnizacion(String fecha) throws Exception{
        return dao.getNegociosVencidaIndemnizacion(fecha);
    }


    /**
     * Getter for property process.
     * @return Value of property process.
     */
    public boolean isProcess() {
        return process;
    }

    /**
     * Setter for property process.
     * @param process New value of property process.
     */
    public void setProcess(boolean process) {
        this.process = process;
    }



}
