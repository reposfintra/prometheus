/****************************************************************************************************
 * Nombre clase: Acuerdo_especialService.java                                                       *
 * Descripci�n: Clase que maneja los servicios al model relacionados con los acuerdos especiales.   *
 * Autor: Ing. Jose de la rosa                                                                      *
 * Fecha: 6 de diciembre de 2005, 10:54 AM                                                          *
 * Versi�n: Java 1.0                                                                                *
 * Copyright: Fintravalores S.A. S.A.                                                          *
 ***************************************************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

public class Acuerdo_especialService {
    private Acuerdo_especialDAO acuerdo_especial;
    /** Creates a new instance of Acuerdo_especialService */
    public Acuerdo_especialService () {
        acuerdo_especial = new Acuerdo_especialDAO();
    }
    /**
     * Metodo: getAcuerdo_especial, permite retornar un objeto de registros de Acuerdo especial.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */    
    public Acuerdo_especial getAcuerdo_especial( )throws SQLException{
        return acuerdo_especial.getAcuerdo_especial();
    }
    
    /**
     * Metodo: getAcuerdo_especiales, permite retornar un vector de registros de Acuerdo especial.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */    
    public Vector getAcuerdo_especiales()throws SQLException{
        return acuerdo_especial.getAcuerdo_especiales();
    }
    
    /**
     * Metodo: setAcuerdo_especial, permite obtener un objeto de registros de Acuerdo especial.
     * @autor : Ing. Jose de la rosa
     * @param : objeto
     * @version : 1.0
     */
    public void setAcuerdo_especial (Acuerdo_especial acuerdo)throws SQLException {
        acuerdo_especial.setAcuerdo_especial(acuerdo);
    }
    
    /**
     * Metodo: setAcuerdo_especiales, permite obtener un vector de registros de Acuerdo especial.
     * @autor : Ing. Jose de la rosa
     * @param : vector
     * @version : 1.0
     */    
    public void setAcuerdo_especiales(Vector acuerdo_especiales)throws SQLException{
        acuerdo_especial.setAcuerdo_especiales(acuerdo_especiales);
    }
    
    /**
    * Metodo insertAcuerdo_especial, ingresa los acuerdos especiales (Acuerdo_especial).
    * @autor : Ing. Jose de la rosa
    * @see insertarAcuerdo_especial - Acuerdo_especialDAO
    * @param : Acuerdo_especial
    * @version : 1.0
    */    
    public void insertAcuerdo_especial(Acuerdo_especial user) throws SQLException{
        try{
            acuerdo_especial.setAcuerdo_especial(user);
            acuerdo_especial.insertAcuerdo_especial();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }    
    
    /**
    * Metodo existAcuerdo_especial, obtiene la informacion de los acuerdos especiales dado unos parametros
    * (standar, codigo).
    * @autor : Ing. Jose de la rosa
    * @see existAcuerdo_especial - Acuerdo_especialDAO
    * @param : nit, propietario y placa
    * @version : 1.0
    */       
    public boolean existAcuerdo_especial(String standar, String distrito, String concepto, String tipoP) throws SQLException{
        try{
            return acuerdo_especial.existAcuerdo_especial(standar, distrito, concepto, tipoP);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo: existStandarTipo, permite buscar un Acuerdo especial dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @see existAcuerdo_especial - Acuerdo_especialDAO     
     * @param : numero del estandar, el tipo de acuerdo.
     * @version : 1.0
     */
    public void existStandarTipo (String standar, String tipo, String distrito, String tipoP) throws SQLException{
        try{
            acuerdo_especial.existStandarTipo(standar, tipo, distrito, tipoP);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }    
    
    /**
    * Metodo searchAcuerdo_especial, permite buscar un acuerdo especial dado unos parametros
    * (standar, codigo).
    * @autor : Ing. Jose de la rosa
    * @see searchAcuerdo_especial - Acuerdo_especialDAO
    * @param : numero del standar y el codigo de concepto
    * @version : 1.0
    */      
    public void searchAcuerdo_especial(String standar, String distrito, String concepto, String tipoP)throws SQLException{
        try{        
            acuerdo_especial.searchAcuerdo_especial(standar, distrito, concepto, tipoP);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
    * Metodo listAcuerdo_especial, lista la informacion de los acuerdos especiales
    * @autor : Ing. Jose de la rosa
    * @see listAcuerdo_especial - Acuerdo_especialDAO
    * @param :
    * @version : 1.0
    */    
    public void listAcuerdo_especial(String distrito)throws SQLException{
        try{         
            acuerdo_especial.listAcuerdo_especial(distrito);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
    * Metodo updateAcuerdo_especial, permite actualizar un acuerdo especial dados unos parametros
    * @autor : Ing. Jose de la rosa
    * @see updateAcuerdo_especial - Acuerdo_especialDAO
    * @param : 
    * @version : 1.0
    */     
    public void updateAcuerdo_especial(Acuerdo_especial res)throws SQLException{
        try{       
            acuerdo_especial.setAcuerdo_especial(res);
            acuerdo_especial.updateAcuerdo_especial();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }

    /**
    * Metodo anularAcuerdo_especial, permite anular un acuerdo especial de la bd
    * (Acuerdo_especial).
    * @autor : Ing. Jose de la rosa
    * @see anularAcuerdo_especial - Acuerdo_especialDAO
    * @param : Acuerdo_especial
    * @version : 1.0
    */      
    public void anularAcuerdo_especial(Acuerdo_especial res)throws SQLException{
        try{
            acuerdo_especial.setAcuerdo_especial(res);
            acuerdo_especial.anularAcuerdo_especial();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo: searchDetalleAcuerdo_especial, permite listar los acuerdos especiales por detalles dados unos parametros.
     * (standar, codigo).
     * @autor : Ing. Jose de la rosa
     * @see anularAcuerdo_especial - Acuerdo_especialDAO
     * @param : numero del standar y el codigo de concepto.
     * @version : 1.0
     */       
    public void searchDetalleAcuerdo_especial (String tipo, String standar, String codigo, String distrito,String tipoP) throws SQLException{
        try{
            acuerdo_especial.searchDetalleAcuerdo_especial(tipo, standar, codigo, distrito, tipoP);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    
}
