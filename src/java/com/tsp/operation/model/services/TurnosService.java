
/***********************************************************************************
 * Nombre clase : ............... DespachoManualService.java                            *
 * Descripcion :................. Clase que instancia los metodos de DespachoManualDAO  *
 *                                con la BD.                                            *
 * Autor :....................... Ing. David Lamadrid                                   *
 * Fecha :....................... 15 de noviembre de 2005, 11:02 AM                     *
 * Version :..................... 1.0                                                   *
 * Copyright :................... Fintravalores S.A.                               *
 ***********************************************************************************/

package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.sql.*;

/**
 *
 * @author  dlamadrid
 */
public class TurnosService {
    
    private TurnosDAO dao;
    
    /** Creates a new instance of TurnosService */
    public TurnosService () {
         this.dao= new TurnosDAO ();
    }
    
      /**
     * Metodo que inserta un registro en la tabla turnos
     * @autor.......David Lamadrid
     * @param.......
     * @see.........
     * @throws......SQLException
     * @version.....1.0.
     * @return.......
     */
    public void insertar () throws SQLException {
        dao.insertar ();
    }
    
     /**
     * Getter for property turno.
     * @return Value of property turno.
     */
    public com.tsp.operation.model.beans.Turnos getTurno () {
        return dao.getTurno ();
    }
    
    /**
     * Metodo que elimina un registro en la tabla turnos
     * @autor.......David Lamadrid
     * @param String usuario,String fecha,String h_entrada,String h_salida
     * @see
     * @throws SQLException
     * @version 1.0.
     * @return
     */
    public void eliminar(String usuario,String fecha,String h_entrada,String h_salida) throws SQLException {
        dao.eliminar (usuario, fecha,h_entrada,h_salida);
    }
    /**
     * Setter for property turno.
     * @param turno New value of property turno.
     */
    public void setTurno (com.tsp.operation.model.beans.Turnos turno) {
        dao.setTurno (turno);
    }
    
    /**
     * Getter for property vTurnos.
     * @return Value of property vTurnos.
     */
    public java.util.Vector getVTurnos () {
        return dao.getVTurnos ();        
    }
    
      /**
     * Setter for property vTurnos.
     * @param vTurnos New value of property vTurnos.
     */
    public void setVTurnos (java.util.Vector vTurnos) {
        dao.setVTurnos (vTurnos);
    }
    
    /**
     * Metodo turnosPorCodigo , Metodo que llena un Vector con resitros de turnos pertenecientes a un usuario
     * @autor : Ing. David Lamadrid
     * @param : String usuario 
     * @version : 1.0
     */
    public void turnosPorCodigo (String usuario,String Fecini, String Fecfin) throws SQLException{
        dao.turnosPorCodigo (usuario,Fecini,Fecfin);  
    }
    
    /**
     * Metodo existe_turno metodo que retorna un true si existe un turno y false si no
     * @autor : Ing. David Lamadrid
     * @param : String usuario,String fecha,String h_entrada,String h_salida
     * @version : 1.0
     */
    public boolean existe_turno(String usuario,String fecha,String h_entrada,String h_salida) throws SQLException
    {
        return dao.existe_turno (usuario,fecha, h_entrada,h_salida);
    }
    
    /**
     * Metodo que actualiza un registro en la tabla de turnos 
     * @autor.......David Lamadrid
     * @param.......
     * @see.........
     * @throws......SQLException
     * @version.....1.0.
     * @return.......
     */
     public void actualizarTurno(String fecha,String h_entrada,String h_salida) throws SQLException {
        dao.actualizarTurno (fecha,h_entrada,h_salida);
    }
    
    /**
     * Metodo que retorna un registro de turnos dadp la llave Primaria
     * @autor : Ing. David Lamadrid
     * @param : String usuario 
     * @version : 1.0
     */
    public void turnosPorId (String usuario,String fecha,String h_entrada,String h_salida) throws SQLException
    {
        dao.turnosPorId (usuario, fecha, h_entrada, h_salida);
    }
    
    /**
     * Metodo que retorna una cadena con una hora en formato sql dados las ,horas ,dias y el horario
     * @autor : Ing. David Lamadrid
     * @param : String dia,int  hora,int minutos
     * @version : 1.0
     */
    public String generarHora(String dia,int  hora,int minutos){   
        String salida="";
        if(dia.equals ("PM")){
            if(hora < 12 ){
                hora+=12;
            }
        }
        else{
            if(hora == 12){
                hora=0;
            }
        }
        salida=hora+":"+minutos+":00";
        return salida;
    }
    
    /**
     * Metodo que retorna un untero con la hora dada la fecha
     * @autor : Ing. David Lamadrid
     * @param : String fecha
     * @version : 1.0
     */
    public int obtenerHora(String fecha){
        String va1 = fecha.substring (0,fecha.indexOf (":"));
        int hora=Integer.parseInt(va1);
        if(hora >12){
           hora=hora-12;
        }
        if(hora == 0){
            hora=12;
        }
        return hora;
    }
    
    /**
     * Metodo que retorna un untero con los minutos dada la fecha
     * @autor : Ing. David Lamadrid
     * @param : String fecha
     * @version : 1.0
     */
    public int obtenerMinutos(String fecha){
        String va1 = fecha.substring (fecha.indexOf (":")+1,fecha.lastIndexOf (":"));
        ////System.out.println("va1 "+va1);
        int minutos=Integer.parseInt(va1);
        return minutos;
    }
    
     /**
     * Metodo que retorna un String  con el horario AM - PM dada la fecha
     * @autor : Ing. David Lamadrid
     * @param : String fecha
     * @version : 1.0
     */
    public String obtenerHorario(String fecha){
        String va1 = fecha.substring (0,fecha.indexOf (":"));
        int hora=Integer.parseInt(va1);
        String h="AM";
        if(hora >=12){
           h="PM";
        }
        return h;
    }
    
     /**
     * Metodo que verifica si un turno al ser insertado es valido en secuencia
     * @autor : Ing. David Lamadrid
     * @param : String usuario,String fecha,String h_entrada,String h_salida
     * @version : 1.0
     */
    public boolean turnoValido(String usuario,String fecha,String h_entrada,String h_salida) throws SQLException
    {
        return dao.turnoValido (usuario, fecha, h_entrada, h_salida);
    }
    
    /**
     * Metodo obtener_turno metodo que retorna el turno con el usuario, fecha, 
     *        hora_entrada y hora_salida dada 
     * @autor : Ing. Osvaldo P�rez Ferrer
     * @param : String usuario,String fecha,String h_entrada,String h_salida
     * @version : 1.0
     */
    public Turnos obtener_turno(String usuario,String fecha,String h_entrada,String h_salida) throws SQLException{
        return dao.obtener_turno(usuario, fecha, h_entrada, h_salida);
    }
    
    /**
     * Metodo que inserta un registro con todos los campos en la tabla turnos
     * @autor.......Ing. Osvaldo P�rez Ferrer
     * @throws......SQLException
     * @version.....1.0.
     */
    public void update( String usuario ) throws SQLException {
        dao.update(usuario);
    }
}
