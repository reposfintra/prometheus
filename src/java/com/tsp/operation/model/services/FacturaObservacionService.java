/**
* Autor : Ing. Roberto Rocha P..
* Date  : 10 de Julio de 2007
* Copyrigth Notice : Fintravalores S.A. S.A
* Version 1.0
-->
<%--
-@(#)
--Descripcion : Services que maneja los avales de Fenalco
**/

package com.tsp.operation.model.services;

import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.*;

public class FacturaObservacionService {
    
   
    FacturaObservacionDAO obs;
    public FacturaObservacionService()
    {
        obs= new FacturaObservacionDAO();
    }
    public FacturaObservacionService(String dataBaseName) {
        obs= new FacturaObservacionDAO(dataBaseName);
    }
    
    public String insert(BeanGeneral bg ) throws java.sql.SQLException{
       return(obs.InsertObs(bg));
    } 
    
    public Vector Listado(String a ) throws java.sql.SQLException{
       Vector b=new Vector();
       try{
            b=obs.ObsxFact(a);
       }
        catch (Exception e)
        {
           e.printStackTrace();
        }
       return(b);
    }
  
    
    /**
     * Este metodo inserta observaciones generales a todas las facturas de un cliente
     * @return el mensaje de exito o fracaso del proceso 
     * @throws Exception si ocurre algun error insertando o actualizando en la base de datos
     */
    /*javier en 20080610*/
    public String insertAll(BeanGeneral bg) throws Exception{
        return (obs.insertObsAll(bg));
    }
    
}

