 /**************************************************************
  * Nombre Clase ............. FormatoServices.java            *
  * Descripci�n  .. . . . . .  Generamos Los formatos          *
  * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ            *
  * Fecha . . . . . . . . . .  22/10/2006                      *
  * versi�n . . . . . . . . .  1.0                             *
  * Copyright ...............  Transportes Sanchez Polo S.A.   *
  **************************************************************/



package com.tsp.operation.model.services;



import com.tsp.operation.model.DAOS.FormatoDAO;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.DatoFormato;
import java.util.*;



public class FormatoServices {
    
    private  FormatoDAO   FormatoDataAccess;
    private  List         campos;
    private  String       formato;
    private  List         tFormatos;
    
    
    private  List         tipoDocumento;
    
    
    public FormatoServices() {
        FormatoDataAccess  = new FormatoDAO();
        campos             = new LinkedList();
        formato            = "";
        tFormatos          = new LinkedList();
        tipoDocumento      = new LinkedList();
    }
    
    
    
    
    
    
    /**
     * M�todo que carga los formatos
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void loadTFormatos() throws Exception{        
        try{
            tFormatos  =  this.FormatoDataAccess.getTipoFormatos();            
        }catch(Exception e){ 
            throw new Exception( " loadTFormatos() " + e.getMessage());
        }
    }
    
    
    
    
     /**
     * M�todo que carga los tipo de documentos
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void loadTipoDocumentos() throws Exception{        
        try{
            tipoDocumento  = this.FormatoDataAccess.getTipoDoc();           
        }catch(Exception e){ 
            throw new Exception( " loadTFormatos() " + e.getMessage());
        }
    }
    
    
    
    public void init(){
       campos             = new LinkedList();
       formato            = "";
       tipoDocumento      = new LinkedList();
    }
    
    
    
    /**
     * M�todo que carga los campos del formato
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void loadCampos(String formato) throws Exception{        
        try{
            this.formato = formato;
            campos       =  this.FormatoDataAccess.getCamposFormato(formato);            
        }catch(Exception e){ 
            throw new Exception( " loadCampos() " + e.getMessage());
        }
    }
    
    
    
    
    /**
     * M�todo que busca los datos del formato para el documento
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void searchDatos(String distrito, String tipoDoc, String doc, String formato) throws Exception{        
        try{
             campos       =  this.FormatoDataAccess.searchCamposFormato( distrito,  tipoDoc, doc, formato );            
        }catch(Exception e){ 
            throw new Exception( " searchDatos() " + e.getMessage());
        }
    }
    
    
    
    
     /**
     * M�todo que busca el formato aplicar al std
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String getFormato(String std) throws Exception{
        String formato = "";
        try{
             formato  = this.FormatoDataAccess.getFormato(std);            
        }catch(Exception e){ 
            throw new Exception( " getFormato() " + e.getMessage());
        }
        return formato;        
    }
    
    
    
    
    /**
     * M�todo que inserta datos
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String insertar(String distrito, String tipo_doc, String doc, String formato, String dato, String user,String remision,String Centro,String Compensac) throws Exception{
        String msj = "";
        try{
             msj   = this.FormatoDataAccess.insert(distrito, tipo_doc, doc, formato, dato, user,remision,Centro,Compensac );            
        }catch(Exception e){ 
            throw new Exception( e.getMessage() );
        }
        return msj;        
    }
    
    
    
    /**
     * M�todo que actualiza datos
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String update(String distrito, String tipo_doc, String doc, String formato, String dato, String user,String Remision,String Compensac) throws Exception{
        String msj = "";
        try{
             msj   = this.FormatoDataAccess.update(distrito, tipo_doc, doc, formato, dato, user,Remision, Compensac );            
        }catch(Exception e){ 
            throw new Exception( e.getMessage() );
        }
        return msj;        
    }
    
    
    
    /**
     * M�todo que elimina datos
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String delete(String distrito, String tipo_doc, String doc, String formato) throws Exception{
        String msj = "";
        try{
             msj   = this.FormatoDataAccess.delete(distrito, tipo_doc, doc, formato );            
        }catch(Exception e){ 
            throw new Exception( e.getMessage() );
        }
        return msj;        
    }
    
    
    
    /**
     * M�todo que busca el tama�o del formato
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public int   getTamanoFormato(String formato) throws Exception{
        int  tamano = 0;
        try{
             tamano  = this.FormatoDataAccess.getTamanoMAX(formato);         
        }catch(Exception e){ 
            throw new Exception( " getTamanoFormato() " + e.getMessage());
        }
        return tamano;        
    }
    
    
    
    
    
    
    
    
    /**
     * Getter for property campos.
     * @return Value of property campos.
     */
    public java.util.List getCampos() {
        return campos;
    }    
    
    /**
     * Setter for property campos.
     * @param campos New value of property campos.
     */
    public void setCampos(java.util.List campos) {
        this.campos = campos;
    }    
    
    /**
     * Getter for property formato.
     * @return Value of property formato.
     */
    public java.lang.String getFormato() {
        return formato;
    }    
    
    /**
     * Setter for property formato.
     * @param formato New value of property formato.
     */
    public void setFormato(java.lang.String formato) {
        this.formato = formato;
    }
    
    /**
     * Getter for property tFormatos.
     * @return Value of property tFormatos.
     */
    public java.util.List getTFormatos() {
        return tFormatos;
    }
    
    /**
     * Setter for property tFormatos.
     * @param tFormatos New value of property tFormatos.
     */
    public void setTFormatos(java.util.List tFormatos) {
        this.tFormatos = tFormatos;
    }
    
    /**
     * Getter for property tipoDocumento.
     * @return Value of property tipoDocumento.
     */
    public java.util.List getTipoDocumento() {
        return tipoDocumento;
    }
    
    /**
     * Setter for property tipoDocumento.
     * @param tipoDocumento New value of property tipoDocumento.
     */
    public void setTipoDocumento(java.util.List tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }
    
     /**************************************************
     * Metodo para obtener los datos iniciales para la Impresion de la Remesion de Mercancia
     * @author: Ing. Juan M. Escandon Perez
     **************************************************/
    public void formato( String dstrct, String tipodoc, String formato, String documento) throws Exception{
        FormatoDataAccess.formato(dstrct, tipodoc, formato, documento);
    }
    
    /**************************************************
     * Metodo para obtener los datos complementario para la Impresion de la Remesion de Mercancia
     * @author: Ing. Juan M. Escandon Perez
     **************************************************/
    public DatoFormato formatoComp(DatoFormato dato, String documento )throws Exception{
        return FormatoDataAccess.formatoComp(dato, documento);
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return FormatoDataAccess.getVector();
    }    

    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        FormatoDataAccess.setVector(vector);
    }
    
     /*************************************************************
     *Metodo para obtener los datos del reporte 'Cuadro Azul'
     *@param: String anio anio del reporte
     *@param: String nombremes nombre del mes del reporte Ej: Enero
     *@param: String nummes numero del mes del reporte Ej: '01'
     *@param: String dstrct distrito
     *@throws: En caso de un error en la consulta
     *************************************************************/
     public void cuadroAzul( String anio, String  nombremes, String nummes, String dstrct ) throws Exception{
         FormatoDataAccess.cuadroAzul(anio, nombremes, nummes, dstrct);
     }    
      public String consultarCentro(String codigo) throws Exception {
           return FormatoDataAccess.consultarCentro(codigo);
      }
      public String consultarDescripcionRemesion (String codigo) throws Exception {
          return FormatoDataAccess.consultarDescripcionRemesion (codigo);
      }
      public String consultarCompensac (String codigo) throws Exception {
          return  FormatoDataAccess.consultarCompensac (codigo);
      }
     
     public TreeMap getbuscarFormatosRemision()throws Exception{
        TreeMap Remisiones=null;
        FormatoDataAccess.buscarFormatosRemision();
        Remisiones = FormatoDataAccess.getFremision();
        return Remisiones;
    }
     public TreeMap buscarCentrocostos()throws Exception{
        TreeMap costos =null;
        FormatoDataAccess.buscarCentrocostos();
        costos = FormatoDataAccess.getFcostos();
        return costos;
    }
     
     public TreeMap buscarCompensac()throws Exception{
        TreeMap compensac =null;
        FormatoDataAccess.buscarCompensac();
        compensac = FormatoDataAccess.getFcompensac();
        return compensac;
    }
    
}
