/*
 * ComparacionPlanillasService.java
 *
 * Created on 3 de agosto de 2006, 03:09 PM
 */

package com.tsp.operation.model.services;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.Model;
/**
 *
 * @author  ALVARO
 */
public class ComparacionPlanillasService {
    private ComparacionplanillasDAO comparacionplanillasdao;
    /** Creates a new instance of ComparacionPlanillasService */
    public ComparacionPlanillasService() {
        comparacionplanillasdao = new ComparacionplanillasDAO();
    }
    public ComparacionPlanillasService(String dataBaseName) {
        comparacionplanillasdao = new ComparacionplanillasDAO(dataBaseName);
    }
    
    /**
     * Getter for property comparacionplanillasdao.
     * @return Value of property comparacionplanillasdao.
     */
    public com.tsp.operation.model.DAOS.ComparacionplanillasDAO getComparacionplanillasdao() {
        return comparacionplanillasdao;
    }
    
    /**
     * Setter for property comparacionplanillasdao.
     * @param comparacionplanillasdao New value of property comparacionplanillasdao.
     */
    public void setComparacionplanillasdao(com.tsp.operation.model.DAOS.ComparacionplanillasDAO comparacionplanillasdao) {
        this.comparacionplanillasdao = comparacionplanillasdao;
    }
    public boolean buscarPlanillasPostgres(String f1, String f2)throws SQLException{
        try{
            return comparacionplanillasdao.buscarPlanillasPostgres(f1,f2);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public boolean buscarPlanillasOracle()throws SQLException{
        try{
            return comparacionplanillasdao.buscarPlanillasOracle(); 
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public java.util.Vector getVPostgres() {
        return comparacionplanillasdao.getVPostgres();
    }
    
    public java.util.Vector getVOracle() {
        return comparacionplanillasdao.getVOracle();
    }
}
