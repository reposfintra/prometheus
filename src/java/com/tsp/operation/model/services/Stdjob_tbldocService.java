/***************************************************************************
 * Nombre clase : ............... Stdjob_tbldocService.java                *
 * Descripcion :................. Clase que maneja los Servicios           *
 *                                asignados a Model relacionados con el   *     
 *                                programa de Standart job tipo de         *     
 *                                documentos                               *
 * Autor :....................... Ing. Juan Manuel Escandon Perez          *
 * Fecha :........................ 27 de octubre de 2005, 01:33 PM         *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.sql.*;

public class Stdjob_tbldocService {
        
        /*
        *Declaracion de atributos
        */
        private Stdjob_tbldocDAO        STDataAccess;
        private List                    ListaST;
        private Stdjob_tbldoc           Datos;
        
        /** Creates a new instance of Stdjob_tbldocService */
        public Stdjob_tbldocService() {
                ListaST = new LinkedList();
                Datos = new Stdjob_tbldoc();
                STDataAccess = new Stdjob_tbldocDAO();
        }
        
        
         /**
         * Metodo Insert, recibe los atributos del objeto de tipo Stdjob_tbldoc, 
         * permite insertar un registro en la Base de datos
         * @autor : Ing. Juan Manuel Escandon Perez
         * @param : String distrito, String tipo de documento, String numero de Standart, String usuario de creacion
         * @see   : Insert - Stdjob_tbldocDAO
         * @throws : Excepcion al momento de insertar ( Duplicate Key )
         * @version : 1.0
         */
        public void Insert(String dstrct, String document_type , String std_job_no, String creation_user) throws Exception {
                try{
                        STDataAccess.Insert(dstrct, document_type, std_job_no, creation_user);
                }
                catch(Exception e){
                        throw new Exception("Error en INSERT [Stdjob_tbldocService]...\n"+e.getMessage());
                }
        }
        
        
         /**
         * Metodo Update, recibe los atributos del objeto de tipo Stdjob_tbldoc, 
         * permite actualizar un registro en la Base de datos
         * @autor : Ing. Juan Manuel Escandon Perez
         * @param : String distrito, String nuevo tipo de documento, String nuevo numero de Standart, String usuario de modificacion
         *          String tipo de documento, String numero de Standart 
         * @see   : Update - Stdjob_tbldocDAO
         * @version : 1.0
         */
        public void Update(String document_type, String user_update, String std_job_no) throws Exception {
                try{
                        STDataAccess.Update(document_type, user_update, std_job_no);
                }
                catch(Exception e){
                        throw new Exception("Error en UPDATE [Stdjob_tbldocService]...\n"+e.getMessage());
                }
        }
        
        
         /**
         * Metodo UpdateStatus, recibe los atributos del objeto de tipo Stdjob_tbldoc, 
         * permite activar o anular un registro en la Base de datos
         * @autor : Ing. Juan Manuel Escandon Perez
         * @param : String reg_status, String tipo de documento, String numero de Standart, String usuario de modificacion
         * @see   : UpdateStatus - Stdjob_tbldocDAO
         * @version : 1.0
         */
        public void UpdateStatus(String reg_status, String user_update, String document_type,String std_job_no) throws Exception {
                try{
                        STDataAccess.UpdateStatus(reg_status, user_update, document_type, std_job_no);
                }
                catch(Exception e){
                        throw new Exception("Error en UPDATESTATUS [Stdjob_tbldocService]...\n"+e.getMessage());
                }
        }
        
        /**
         * Metodo List, lista todos los registros de la tabla stdjob_tbldoc
         * @autor : Ing. Juan Manuel Escandon Perez
         * @see   : List - Stdjob_tbldocDAO
         * @version : 1.0
         */
        public void List() throws Exception {
                try{
                        this.ReiniciarLista();
                        this.ListaST = STDataAccess.List();
                }
                catch(Exception e){
                        throw new Exception("Error en LIST [Stdjob_tbldocService]...\n"+e.getMessage());
                }
        }
        
        /**
         * Metodo Delete, recibe los atributos del objeto de tipo Stdjob_tbldoc, 
         * permite eliminar un registro en la Base de datos
         * @autor : Ing. Juan Manuel Escandon Perez
         * @param : String tipo de documento, String numero de Standart
         * @see   : Delete - Stdjob_tbldocDAO
         * @version : 1.0
         */
        public void Delete(String document_type,String std_job_no ) throws Exception {
                try{
                        STDataAccess.Delete(document_type, std_job_no);
                }
                catch(Exception e){
                        throw new Exception("Error en DELETE [Stdjob_tbldocService]...\n"+e.getMessage());
                }
        }
        
        /**
         * Metodo Search, recibe los atributos del objeto de tipo Stdjob_tbldoc, 
         * permite buscar un registro en la Base de datos
         * @autor : Ing. Juan Manuel Escandon Perez
         * @param : String tipo de documento, String numero de Standart
         * @see   : Search - Stdjob_tbldocDAO
         * @version : 1.0
         */
        public void Search(String document_type, String std_job_no ) throws Exception {
                try{
                        this.ReiniciarDato();
                        this.Datos = STDataAccess.Search(document_type, std_job_no);
                }
                catch(Exception e){
                        throw new Exception("Error en SEARCH [Stdjob_tbldocService]...\n"+e.getMessage());
                }
        }
        
         /**
         * Metodo Exist, recibe los atributos del objeto de tipo Stdjob_tbldoc, 
         * permite verificar si existe un registro en la Base de datos
         * @autor : Ing. Juan Manuel Escandon Perez
         * @param : String tipo de documento, String numero de Standart
         * @see   : Search - Stdjob_tbldocDAO
         * @version : 1.0
         */
        public boolean Exist(String document_type, String std_job_no ) throws Exception {
                try{
                        this.ReiniciarDato();
                        return STDataAccess.Exist(document_type, std_job_no);
                }
                catch(Exception e){
                        throw new Exception("Error en BUSCAR [Stdjob_tbldocService]...\n"+e.getMessage());
                }
        }
        
          /**
         * Metodo ReiniciarDato, le asigna el valor null al atributo Datos, 
         * @autor : Ing. Juan Manuel Escandon Perez
         * @version : 1.0
         */
        public void ReiniciarDato(){
                this.Datos = null;
        }
        
         /**
         * Metodo ReiniciarLista, le asigna el valor null al atributo ListaST, 
         * @autor : Ing. Juan Manuel Escandon Perez
         * @version : 1.0
         */
        public void ReiniciarLista(){
                this.ListaST = null;
        }
        
         /**
         * Metodo getDato, retorna el valor del atributo Datos, 
         * @autor : Ing. Juan Manuel Escandon Perez
         * @version : 1.0
         */
        public Stdjob_tbldoc getDato(){
                return this.Datos;
        }
        
         /**
         * Metodo getList, retorna el valor del atributo ListaST, 
         * @autor : Ing. Juan Manuel Escandon Perez
         * @version : 1.0
         */
        public List getList(){
                return this.ListaST;
        }
        
        /**
        * Metodo nombreDocumento, retorna el nombre del tipo de documento
        * @autor : Ing. Jose de la rosa
        * @param : String tipo documento
        * @see   : list_Stdjob - Stdjob_tbldocDAO
        * @version : 1.0
        */
        public String nombreDocumento (String tipo) throws Exception{
                try{
                        return this.STDataAccess.nombreDocumento (tipo);
                }
                catch(Exception e){
                        throw new Exception("Error en Nombre [Stdjob_tbldocService]...\n"+e.getMessage());
                }            
        }
        
        /**
        * Metodo list_Stdjob, lista todos los registros de la tabla stdjob_tbldocd dados unos parametros
        * @autor : Ing. Jose de la rosa
        * @param : String numero de Standart
        * @see   : list_Stdjob - Stdjob_tbldocDAO
        * @version : 1.0
        */
        public void list_Stdjob( String job) throws Exception {
                try{
                        this.ReiniciarLista();
                        this.ListaST = STDataAccess.list_Stdjob(job);
                }
                catch(Exception e){
                        throw new Exception("Error en LIST [Stdjob_tbldocService]...\n"+e.getMessage());
                }
        }
        
}
