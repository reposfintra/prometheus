
/***************************************************************************
 * Nombre clase : ............... RemesaSinDocService.java                 *
 * Descripcion :................. Clase que maneja los Servicios           *
 *                                asignados a Model relacionados con el    *
 *                                programa de generacion del reporte de    *
 *                                remesas sin documentos relacionados      *
 * Autor :....................... Ing. Juan Manuel Escandon Perez          *
 * Fecha :........................ 18 de noviembre de 2005, 10:21 AM       *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.util.*;
public class RemesaSinDocService {
    private RemesaSinDocDAO    RSDDataAccess;
    private List               ListaRSD;
    private RemesaSinDoc       Datos;
    /** Creates a new instance of RemesaSinDocService */
    public RemesaSinDocService() {
        RSDDataAccess = new RemesaSinDocDAO();
        ListaRSD = new LinkedList();
        Datos = new RemesaSinDoc();
    }
    
     
    /**
     * Metodo listRemesasSinDoc, lista todas las remesas que no tienen documentos asociados o documentos asociados al 
     * destinatario entre un rango de fechas , y con agencia y usuario de creacion como parametros 
     * adicionales
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String agencia , String usuario, String fecha inicial , String fecha final
     * @see   : listRemesaSinDoc - RemesaSinDocDAO
     * @version : 1.0
     */
    public void listRemesasSinDoc( String agencia, String usuario, String fechaInicial, String fechaFinal ) throws Exception {
        try{
            this.ReiniciarListaRSD();
            this.ListaRSD = RSDDataAccess.listRemesasSinDoc(agencia, usuario, fechaInicial, fechaFinal);
        }
        catch(Exception e){
            throw new Exception("Error en listRemesasSinDoc [RemesasSinDocService]...\n"+e.getMessage());
        }
    }
    
    
    /**
     * Metodo ReiniciarDato, le asigna el valor null al atributo Datos,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public void ReiniciarDato(){
        this.Datos = null;
    }
    
    /**
     * Metodo ReiniciarListaRE, le asigna el valor null al atributo ListaRSD,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public void ReiniciarListaRSD(){
        this.ListaRSD = null;
    }
    
    
    /**
     * Metodo getDato, retorna el valor del atributo Datos,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public RemesaSinDoc getDato(){
        return this.Datos;
    }
    
    /**
     * Metodo getListRE, retorna el valor del atributo ListaRSD,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public List getListRSD(){
        return this.ListaRSD;
    }
    
    
    
}
