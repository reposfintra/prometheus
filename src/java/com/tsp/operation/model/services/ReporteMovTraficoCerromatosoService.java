/********************************************************************
 *  Nombre Clase.................   ReportePlacaFotoService.java
 *  Descripci�n..................   Service del DAO de la tabla (rep_mov_trafico, cliente, remesa, plarem)
 *  Autor........................   Ing. Leonardo Parodi
 *  Fecha........................   06.01.2005
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  EQUIPO12
 */
public class ReporteMovTraficoCerromatosoService {
        
        ReporteMovTraficoCerromatosoDAO dao;
        /** Creates a new instance of ReporteMovTraficoCerromatosoService */
        public ReporteMovTraficoCerromatosoService() {
                dao = new ReporteMovTraficoCerromatosoDAO();
        }
        
        /**
         * Metodo ReporteMovTrafCerromatoso , Metodo llama a ReporteMovTrafCerromatoso en el DAO
         * @autor : Ing. Leonardo Parody
         * @return : List
         * @version : 1.0
         */
        
        public List ReporteMovTrafCerromatoso() throws SQLException{
                //////System.out.println("estoy en el service y voy a ejecutar el metodo");
                return dao.ReporteMovTrafCerromatoso();
        }
        
}
