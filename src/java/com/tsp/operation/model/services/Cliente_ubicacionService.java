/************************************************************************************
 * Nombre clase: Cliente_ubicacionService.java                                      *
 * Descripci�n: Clase que maneja los servicios al model relacionados                *
 *              con cliente_ubicacion.                                              *
 * Autor: Ing. Rodrigo Salazar                                                      *
 * Fecha: 20 de enero de 2006, 09:17 AM                                             *
 * Versi�n: Java 1.0                                                                *
 * Copyright: Fintravalores S.A. S.A.                                          *
 ***********************************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

public class Cliente_ubicacionService {
    private Cliente_ubicacionDAO cliente_ubicacion;
    /** Creates a new instance of Cliente_ubicacionService */
    public Cliente_ubicacionService () {
        cliente_ubicacion = new Cliente_ubicacionDAO ();
    }
    
    /**
     * Metodo: getCliente_ubicacion, permite retornar un objeto de registros de cliente_ubicacion.
     * @autor : Ing. Jose de la rosa
     * @see getCliente_ubicacion - Cliente_ubicacionDAO
     * @param :
     * @version : 1.0
     */
    public Cliente_ubicacion getCliente_ubicacion ( )throws SQLException{
        return cliente_ubicacion.getCliente_ubicacion ();
    }
    
    /**
     * Metodo: getCliente_ubicaciones, permite retornar un vector de registros de cliente_ubicacion.
     * @autor : Ing. Jose de la rosa
     * @see getCliente_ubicaciones - Cliente_ubicacionDAO
     * @param :
     * @version : 1.0
     */
    public Vector getCliente_ubicaciones () {
        return cliente_ubicacion.getCliente_ubicaciones ();
    }
    
    /**
     * Metodo: setCliente_ubicaciones, permite obtener un vector de registros de cliente_ubicacion.
     * @autor : Ing. Jose de la rosa
     * @see setCliente_ubicaciones - Cliente_ubicacionDAO
     * @param : vector
     * @version : 1.0
     */
    public void setCliente_ubicaciones (Vector cliente_ubicaciones) {
        cliente_ubicacion.setCliente_ubicaciones (cliente_ubicaciones);
    }
    
    /**
     * Metodo: insertCliente_ubicacion, permite ingresar un registro a la tabla cliente_ubicacion.
     * @autor : Ing. Jose de la rosa
     * @see insertCliente_ubicacion - Cliente_ubicacionDAO
     * @param :
     * @version : 1.0
     */
    public void insertCliente_ubicacion (Cliente_ubicacion user) throws SQLException{
        try{
            cliente_ubicacion.setCliente_ubicacion (user);
            cliente_ubicacion.insertCliente_ubicacion ();
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
    /**
     * Metodo: existCliente_ubicacion, permite buscar un cliente_ubicacion dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @see existCliente_ubicacion - Cliente_ubicacionDAO
     * @param : numero de cedula y el codigo de la ubicacion
     * @version : 1.0
     */
    public boolean existCliente_ubicacion (String cedula, String ubicacion) throws SQLException{
        try{
            return cliente_ubicacion.existCliente_ubicacion (cedula, ubicacion);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
    /**
     * Metodo: searchCliente_ubicacion, permite buscar una cliente_ubicacion dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @see searchCliente_ubicacion - Cliente_ubicacionDAO
     * @param : numero de cedula y el codigo de la ubicacion
     * @version : 1.0
     */
    public void searchCliente_ubicacion (String cedula, String ubicacion)throws SQLException{
        try{
            cliente_ubicacion.searchCliente_ubicacion (cedula, ubicacion);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
    /**
     * Metodo: searchDetalleCliente_ubicacion, permite buscar un cliente_ubicacion dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @see searchDetalleCliente_ubicacion - Cliente_ubicacionDAO
     * @param : numero de cedula y el codigo de la ubicacion.
     * @version : 1.0
     */
    public void searchDetalleCliente_ubicacion (String cedula, String ubicacion) throws SQLException{
        try{
            cliente_ubicacion.searchDetalleCliente_ubicacion (cedula, ubicacion);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
    /**
     * Metodo: anularCliente_ubicacion, permite anular un cliente_ubicacion.
     * @autor : Ing. Jose de la rosa
     * @see anularCliente_ubicacion - Cliente_ubicacionDAO
     * @param :
     * @version : 1.0
     */
    public void anularCliente_ubicacion (Cliente_ubicacion user) throws SQLException{
        try{
            cliente_ubicacion.setCliente_ubicacion (user);
            cliente_ubicacion.anularCliente_ubicacion ();
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
    /**
     * Metodo: updateCliente_ubicacion, permite anular un cliente_ubicacion.
     * @autor : Ing. Jose de la rosa
     * @see anularCliente_ubicacion - Cliente_ubicacionDAO
     * @param :
     * @version : 1.0
     */
    public void updateCliente_ubicacion (Cliente_ubicacion user) throws SQLException{
        try{
            cliente_ubicacion.setCliente_ubicacion (user);
            cliente_ubicacion.updateCliente_ubicacion ();
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }    
    
    
     /**
     * M�todo que busca una lista ubicaciones dado el codigo de un cliente,
     * en la tabla de inventario donde haya existencia de producto.
     * @param.......Recibe un codigo de cliente
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.     
     **/     
    public void listarUbicaciones(String codcli)throws SQLException{
        cliente_ubicacion.listarUbicaciones(codcli);
    }
    
    /**
     * Getter for property ubicacion_prod.
     * @return Value of property ubicacion_prod.
     */
    public java.util.TreeMap getUbicacion_prod() {
        return cliente_ubicacion.getUbicacion_prod();
    }
    
    /**
     * Setter for property ubicacion_prod.
     * @param ubicacion_prod New value of property ubicacion_prod.
     */
    public void setUbicacion_prod(java.util.TreeMap ubicacion_prod) {
        cliente_ubicacion.setUbicacion_prod(ubicacion_prod);
    }
     /**
     * M�todo que busca una lista ubicaciones dado el codigo de un cliente,
     * en la tabla de inventario sin importar la existencia de producto.
     * @param.......Recibe un codigo de cliente
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.     
     **/     
    public void listarTodasUbicaciones(String codcli)throws SQLException{
        cliente_ubicacion.listarTodasUbicaciones(codcli);
    }
    
}
