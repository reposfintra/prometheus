/*
    Document   : Modulo de Requisiciones Fintra
    Created on : 10/07/2013, 11:00:00 AM
    Author     : hcuello
*/

package com.tsp.operation.model.services;

import java.util.ArrayList;
import com.tsp.operation.model.DAOS.RequisicionesDAO;
import com.tsp.operation.model.beans.BeansGenericoRta;
import com.tsp.operation.model.beans.CmbGeneralScBeans;
import com.tsp.operation.model.beans.CmbGenericoBeans;
import com.tsp.operation.model.beans.Politicas;
import com.tsp.operation.model.beans.RequisicionBeans;
import com.tsp.operation.model.beans.RequisicionesListadoBeans;
import com.tsp.operation.model.beans.Usuario;
import java.util.List;
import java.util.ResourceBundle;

public class RequisicionesService {
    RequisicionesDAO lpr; // = new RequisicionesDAO();
    
    /*
    public RequisicionesService() {
        lpr = new RequisicionesDAO();
    }*/
    
    public RequisicionesService(String dataBaseName) {
        this.lpr = new RequisicionesDAO(dataBaseName);
    }

    public ArrayList<CmbGenericoBeans> GetComboGenerico(String Query, String IdCmb, String DescripcionCmb, String wuereCmb)throws Exception{
       return lpr.GetComboGenerico(Query, IdCmb, DescripcionCmb, wuereCmb);
    }

    public ArrayList<CmbGeneralScBeans> GetComboGenericoStr(String Query, String IdCmb, String DescripcionCmb, String wuereCmb)throws Exception{
       return lpr.GetComboGenericoStr(Query, IdCmb, DescripcionCmb, wuereCmb);
    }
    
    public ArrayList<BeansGenericoRta> RespuestaUnica(String Query, String Var1, String Var2, String Var3, String Var4)throws Exception{
       return lpr.GetRespuestaUnica(Query, Var1, Var2, Var3, Var4);
    }
    
    public ArrayList<RequisicionesListadoBeans> RequisicionesListadoBeans(String Query, String Estado, String Mes, String Ano, String Proceso, String Asignada, String TipoReq, String Prioridad, String LoginUser, String wHrItem, String wFiltro)throws Exception{
       return lpr.GetRequisicionesListado(Query, Estado, Mes, Ano, Proceso, Asignada, TipoReq, Prioridad, LoginUser, wHrItem, wFiltro);
    }
    
    public ArrayList<RequisicionesListadoBeans> RequisicionesListareas(String Query, String ReqId, String EstadoTarea, String LoginUser, String wHrItem)throws Exception{
       return lpr.GetRequisicionesTareas(Query, ReqId, EstadoTarea, LoginUser, wHrItem);
    }

    
    public String InsertarRequisicion(String Dstrct, int TipoRequisicion, int ProcesoSgc, String Iduser, String Asunto, String Descripcion, int Prioridad, String CmbPartners)throws Exception{
       return lpr.InsertRequisiciones(Dstrct, TipoRequisicion, ProcesoSgc, Iduser, Asunto, Descripcion, Prioridad, CmbPartners);
    }
    
    public String ActualizarRequisicion(String Dstrct, int TipoRequisicion, int ProcesoSgc, String Iduser, String Asunto, String Descripcion, int Prioridad, int ReqId)throws Exception{
       return lpr.ActualizaRequisicion(Dstrct, TipoRequisicion, ProcesoSgc, Iduser, Asunto, Descripcion, Prioridad, ReqId);
    }

    public String ActualizarRqResponsable(String Responsable, int ReqId)throws Exception{
       return lpr.ActualizaRqResponsable(Responsable, ReqId);
    }
    
    public ArrayList<RequisicionesListadoBeans> RequisicionesIndicadores(String Query, String Ano, String Mes, String Proceso)throws Exception{
       return lpr.GetRqIndicadores(Query, Ano, Mes, Proceso);
    }
    
    //public ArrayList<String> InsertActividadesRequisicion(ArrayList<RequisicionesListadoBeans> requisicionesListadoBeanses, Usuario usuario, int TipoTarea, int IdReqTask) throws Exception {
        //ArrayList<String> tareasQuerys = lpr.GuardarTareas(requisicionesListadoBeanses, usuario, TipoTarea, IdReqTask);
        //return tareasQuerys;
    //}
    
    public void InsertActividadesRequisicion(ArrayList<RequisicionesListadoBeans> requisicionesListadoBeanses, Usuario usuario, int TipoTarea, int IdReqTask) throws Exception {
        lpr.GuardarTareas(requisicionesListadoBeanses, usuario, TipoTarea, IdReqTask);
    }
    
    public void GuardarAccionRequisicion(Usuario usuario, int IdReqTask, int IdActionRq, String DscAction, int IdEstadoRq, int TipoTarea) throws Exception {
        lpr.GuardarAccionRq(usuario, IdReqTask, IdActionRq, DscAction, IdEstadoRq, TipoTarea);
        
    }    

    public String  listarTareas(String usuario)throws Exception{
       return lpr.listarTareas(usuario);
    }      
    
    public String  listarTareas(String idProceso, int anio, int mes)throws Exception{
       return lpr.listarTareas(idProceso, anio, mes);
    }     
    
     public ArrayList<RequisicionBeans>listarPrioridadRequisicion()throws Exception{
       return lpr.listarPrioridadRequisicion();
    } 
     
     public String guardarPrioridadRequisicion(String descripcion, String color, String usuario) throws Exception {
        return lpr.guardarPrioridadRequisicion(descripcion, color, usuario);
    }

    public String editarPrioridadRequisicion(String descripcion, String color, String usuario, String id) throws Exception {
        return lpr.editarPrioridadRequisicion(descripcion, color, usuario, id);
    }

    public String cambiarestadoPriRequisicion(String usuario, String id) throws Exception {
        return lpr.cambiarestadoPriRequisicion(usuario, id);
    }

    public ArrayList<RequisicionBeans> listarEstadoRequisicion() throws Exception {
        return lpr.listarEstadoRequisicion();
    }

    public String guardarEstadoRequisicion(String descripcion, String usuario) throws Exception {
        return lpr.guardarEstadoRequisicion(descripcion, usuario);
    }

    public String editarEstadoRequisicion(String descripcion, String usuario, String id) throws Exception {
        return lpr.editarEstadoRequisicion(descripcion, usuario, id);
    }

    public String cambiarestadoRequisicion(String usuario, String id) throws Exception {
        return lpr.cambiarestadoRequisicion(usuario, id);
    }
    
    public ArrayList<RequisicionBeans> listarTipoTarea() throws Exception {
        return lpr.listarTipoTarea();
    }
    
     public String guardarTipoTarea(String descripcion, String usuario) throws Exception {
        return lpr.guardarTipoTarea(descripcion, usuario);
    }
     
      public String editarTipoTarea(String descripcion, String usuario, String id) throws Exception {
        return lpr.editarTipoTarea(descripcion, usuario, id);
    }
      
      public String cambiarestadoTipoTarea(String usuario, String id) throws Exception {
        return lpr.cambiarestadoTipoTarea(usuario, id);
    }
      
    public ArrayList<RequisicionBeans> listarEstadoTipoTarea() throws Exception {
        return lpr.listarEstadoTipoTarea();
    }

    public String guardarEstadoTipoTarea(String descripcion, String usuario) throws Exception {
        return lpr.guardarEstadoTipoTarea(descripcion, usuario);
    }

    public String editarEstadoTipoTarea(String descripcion, String usuario, String id) throws Exception {
        return lpr.editarEstadoTipoTarea(descripcion, usuario, id);
    }

    public String cambiarestTipoTarea(String usuario, String id) throws Exception {
        return lpr.cambiarestTipoTarea(usuario, id);
    }
    
    public ArrayList<RequisicionBeans> listarTipoRequisicion() throws Exception {
        return lpr.listarTipoRequisicion();
    }

    public String guardarTipoRequisicion(String descripcion, String usuario, String eficacia,String eficiencia) throws Exception {
        return lpr.guardarTipoRequisicion(descripcion,  usuario,  eficacia, eficiencia);
    }

    public String editarTipoRequisicion(String descripcion, String usuario, String id,String eficacia, String eficiencia) throws Exception {
        return lpr.editarTipoRequisicion(descripcion, usuario, id, eficacia, eficiencia);
    }

    public String cambiarEstadoTipoRequisicion(String usuario, String id) throws Exception {
        return lpr.cambiarEstadoTipoRequisicion(usuario, id);
    }
    
    public List getNombresArchivos(String numreq)
    {
       java.util.List lista =null;
        try
        {   
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String rutaOrigen  = rb.getString("rutaImagenes")+"gestionadministrativa/requisiciones/";
            lista= lpr.searchNombresArchivos(rutaOrigen,numreq);                      
        }
        catch(Exception e)
        {
            System.out.println("errorrr::"+e.toString()+"__"+e.getMessage());
        }
        return lista;
    }
    
    public boolean almacenarArchivoEnCarpetaUsuario(String documento, String loginx, String nomarchivo) throws Exception {
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String rutaOrigen = rb.getString("rutaImagenes") + "gestionadministrativa/requisiciones/";
            String rutaDestino = rb.getString("ruta") + "/images/multiservicios/" + loginx + "/";

            return lpr.copiarArchivo(documento, ("" + rutaOrigen), ("" + rutaDestino), nomarchivo);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
    
    public boolean eliminarArchivo(String documento, String nomarchivo) throws Exception {
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String rutaOrigen = rb.getString("rutaImagenes") + "gestionadministrativa/requisiciones/";           

            return lpr.eliminarArchivo(("" + rutaOrigen), documento,  nomarchivo);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
    
    public String  listarActividadesUsuario(String idProceso, int idUsuario, int anio, int mes)throws Exception{
       return lpr.listarActividadesUsuario(idProceso, idUsuario, anio, mes);
    }  

}
