/*********************************************************************************
 * Nombre clase :      PublicacionService.java                                   *
 * Descripcion :       Service del PublicacionService.java                       *
 * Autor :             LREALES                                                   *
 * Fecha :             11 de abril de 2006, 12:45 PM                             *
 * Version :           1.0                                                       *
 * Copyright :         Fintravalores S.A.                                   *
 *********************************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import java.io.*;

public class PublicacionService {
    
    private PublicacionDAO publicacion;
    
    /** Creates a new instance of PublicacionService */
    public PublicacionService(){
        publicacion = new PublicacionDAO();
    }
    public PublicacionService(String dataBaseName){
        publicacion = new PublicacionDAO(dataBaseName);
    }
    
    
    /** Funcion publica que obtiene el metodo getTitulosTree del DAO */
    public TreeMap getTitulosTree(){
        
        return publicacion.getTitulosTree();
        
    }
    
    /** Funcion publica que obtiene el metodo setTitulosTree del DAO */
    public void setTitulosTree( TreeMap t ){
        
        publicacion.setTitulosTree( t );
        
    }
    
    /** Funcion publica que obtiene el metodo getTreeMapTitulos del DAO */
    public void getTreeMapTitulos() throws SQLException{
        
        this.publicacion.getTreeMapTitulos();
        
    }
    
    /** Funcion publica que obtiene el metodo insertPublicacion del DAO */
    public void insertPublicacion( String distrito, String id_opcion, String texto, String tipo, String codigo_titulo, String creation_user, String base )throws Exception{
        
        this.publicacion.insertPublicacion( distrito, id_opcion, texto, tipo, codigo_titulo, creation_user, base );
        
    }
    
    /** Funcion publica que obtiene el metodo getPublicaciones del DAO */
    public Vector getVectorPublicaciones() throws SQLException {
        
        return publicacion.getPublicaciones();
        
    }
    
    /** Funcion publica que obtiene el metodo listaPublicaciones del DAO */
    public void listaPublicaciones( String creation_user ) throws SQLException {
        
        publicacion.listaPublicaciones( creation_user );
        
    }
    
    /** Funcion publica que obtiene el metodo obtenerPublicacion del DAO */
    public void obtenerPublicacion( String dstrct, int id_opcion, String creation_date, String creation_user ) throws SQLException {
        
        publicacion.obtenerPublicacion( dstrct, id_opcion, creation_date, creation_user );
        
    }
    
    /** Funcion publica que obtiene el metodo getUsuario() del DAO */
    public String getUsuario() {
        
        return publicacion.getUsuario();
        
    }
    /** Funcion publica que obtiene el metodo Update_Publicacion_Especifica del DAO */
    public void Update_Publicacion_Especifica( String texto, String dstrct, int id_opcion, String creation_date, String creation_user ) throws SQLException {
        
        publicacion.Update_Publicacion_Especifica( texto, dstrct, id_opcion, creation_date, creation_user );
        
    }
    
    /** Funcion publica que obtiene el metodo Delete_Publicacion_Especifica del DAO */
    public void Delete_Publicacion_Especifica( String dstrct, int id_opcion, String creation_date, String creation_user ) throws SQLException {
        
        publicacion.Delete_Publicacion_Especifica( dstrct, id_opcion, creation_date, creation_user );
        
    }
    /**
     * Metodo Buscar_Publicaciones, busca publicaciones
     * @autor  Ing. Andres Martinez
     * @see  setContacto(Contacto contacto), insertarContacto()
     * @version  1.0
     */
    public void buscarPublicaciones(String distrito, String perfil,String usuario)throws SQLException {
        try{
            publicacion.buscarPublicaciones(distrito,perfil,usuario);
            
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    /**
     * Getter for property VecPublicaciones.
     * @return Value of property VecPublicaciones.
     */
    public java.util.Vector getVecPublicaciones() {
        return publicacion.getVecPublicaciones();
    }
    
    public void contadorPublicaciones() throws SQLException{
        publicacion.contadorPublicaciones();
    }
    
    
    /**
     * Metodo: insertarPublicacionNoVencida, permite ingresar un registro en la tabla publicacion_no_vencida de la BD.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String insertarPublicacionNoVencida(Publicacion publica) throws SQLException {
        publicacion.setPublicacion(publica);
        return publicacion.insertarPublicacionNoVencida();
    }
    
    /**
     * Metodo: eliminarPublicacionesNoVencidas, permite eliminar las publicaciones ya leidas
     * @autor : Ing. Jose de la rosa
     * @param : numero de la planilla, numero de la remesa, tipo de documento y documento
     * @version : 1.0
     */
    public String eliminarPublicacionesNoVencidas() throws SQLException {
        return publicacion.eliminarPublicacionesNoVencidas();
    }
    
    /**
     * Metodo: obtenerPublicacionesActuales, permite listar unas publicaciones que aun no han sido publicadas
     * @autor : Ing. Jose de la rosa
     * @param : fecha de ultima publicacion
     * @version : 1.0
     */
    public void obtenerPublicacionesActuales(String fecha) throws SQLException{
        try{
            publicacion.obtenerPublicacionesActuales(fecha);
        }catch(Exception ex){
            ex.getMessage();
        }
    }
    
    /**
     * Metodo: historialPublicaciones, permite listar unas publicaciones que aun no han sido publicadas
     * @autor : Ing. Jose de la rosa
     * @param : fecha de ultima publicacion
     * @version : 1.0
     */
    public void historialPublicaciones(String perfil) throws SQLException{
        try{
            publicacion.historialPublicaciones(perfil);
        }catch(Exception ex){
            ex.getMessage();
        }
    }
    
   
   
    /**
     * Metodo: setFechaUltimoCorte, permite actualizar la fecha de corte en el properties
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void setFechaUltimoCorte(String fecha) throws Exception{
        try{
            publicacion.setFechaUltimoCorte(fecha);
        }catch(Exception ex){
            ex.getMessage();
        }
    }
    
    /**
     * Getter for property publicacion.
     * @return Value of property publicacion.
     */
    public com.tsp.operation.model.beans.Publicacion getPublicacion() {
        return publicacion.getPublicacion();
    }
    
    /**
     * Metodo: actualizarPublicacionUsuarios, permite actualizar si los usuarios tienen publicaciones o no en la tabla usuarios.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String actualizarPublicacionUsuarios() throws SQLException {
        return publicacion.actualizarPublicacionUsuarios();
    }
    public String getPublica(){
        return publicacion.getPublica();
    }
    /**
     * Metodo verificarpublicaciones, busca publicaciones de tabla usuario S/N
     * @autor  Ing. Andres Martinez
     * @version  1.0
     */
    public void verificarpublicaciones(String usuario)throws SQLException {
        try{
            publicacion.verificarpublicaciones(usuario);
            
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
     public String fechaCorte ( ){
        return publicacion.fechaCorte ();
    }
    
        /**
     * Getter for property VecPublicaciones.
     * @return Value of property VecPublicaciones.
     */
    public java.util.Vector getVecDatos() {
        return publicacion.getVecDatos ();
    }
    
    public String esUsuarioConsorcio(String usuari) {//090710
        String respuesta="";
        try{    
            respuesta=publicacion.esUsuarioConsorcio (usuari); 
        }catch(Exception e){
            System.out.println("error en esUsuarioConsorcio"+e.toString()+"__"+e.getMessage());   
        }      
        return respuesta;
    }
    
}
