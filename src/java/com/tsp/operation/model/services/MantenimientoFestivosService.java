/*
 *
 * creada el 2 de enero de 2007
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.util.*;
import java.sql.*;
/**
 *
 * @author  Ing Luis Eduardo Frieri
 */
public class MantenimientoFestivosService {
    
    private MantenimientoFestivosDAO FestivosDataAccess;
    /** Creates a new instance of CarroceriaService */
    public MantenimientoFestivosService() {
        FestivosDataAccess = new MantenimientoFestivosDAO();
    }
    
    public void insertarFestivo(String reg_status, String dstrct, String fecha, String descripcion, String user_update, String creation_user, String base, String pais) throws SQLException{
        FestivosDataAccess.insertarFestivos(reg_status, dstrct, fecha, descripcion, user_update, creation_user, base, pais);
    }
     
    public void eliminarFestivo(String fecha) throws SQLException{
        FestivosDataAccess.eliminarFestivos(fecha);
    }
    
    public void modificarFestivo(String pais, String descripcion, String usuario, String buscfecha) throws SQLException{
        FestivosDataAccess.modificarFestivos(pais, descripcion, usuario, buscfecha);
    }
    
    public void listarFestivo() throws SQLException{
        FestivosDataAccess.ListarFestivos();
    }
    
    public void BuscarFestivo(String fechai, String fechaf, String pais) throws SQLException{
        FestivosDataAccess.buscarFestivo(fechai, fechaf, pais);
    }
    
    public boolean existeFestivo(String fecha, String pais) throws SQLException{
        return FestivosDataAccess.existeFestivos(fecha, pais);
    }
    
    public Vector getVector() throws SQLException{
        return FestivosDataAccess.getVector();
    }
    
}
