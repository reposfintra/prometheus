/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.SmsDAO;
import com.tsp.operation.model.beans.Sms;
import java.util.List;

/**
 *
 * @author dev1
 */
public class Smservice
{

    SmsDAO smsDao;

    public Smservice() {
        smsDao = new SmsDAO();
    }
    public Smservice(String dataBaseName) {
        smsDao = new SmsDAO(dataBaseName);
    }

    

     public Sms envia_SMS_HTTP(Sms sms) throws Exception
     {
         smsDao.envia_SMS_HTTP(sms);
     return sms;
     }




     public void InsertarSMS(Sms sms) throws Exception
     {
         smsDao.InsertarSMS(sms);
     //return "";
     }

     public  List ListarSms( String nit,String cell,String estado,String tipo,String fecha_inicial,String fecha_final) throws Exception
     {
         return smsDao.ListarSms(nit, cell, estado, tipo, fecha_inicial, fecha_final);
     }



            public void   Actualizar_Alarma_Sms(String estado) throws Exception
            {
                 smsDao.Actualizar_Alarma_Sms(estado);
            }

}
