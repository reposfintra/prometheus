/*
* Nombre        ReporteInformacionAlClienteService.java
* Descripción   Clase que presta los servicios de acceso a los datos del reporte de información al cliente
* Autor         Alejandro Payares
* Fecha         21 de septiembre de 2005, 10:25 AM
* Versión       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/
 
package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.ReporteInformacionAlClienteDAO;
import java.util.TreeMap;
import java.util.Vector;
import java.util.Hashtable;
import java.sql.SQLException;
import com.tsp.operation.model.*;

/**
 * Clase que presta los servicios de acceso a los datos del reporte de información al cliente
 * @author  Alejandro Payares
 */
public class ReporteInformacionAlClienteService {
    
    private ReporteInformacionAlClienteDAO dao;
    
    /** Creates a new instance of ReporteInformacionAlClienteService */
    public ReporteInformacionAlClienteService() {
        dao = new ReporteInformacionAlClienteDAO();
    }
    public ReporteInformacionAlClienteService(String dataBaseName) {
        dao = new ReporteInformacionAlClienteDAO(dataBaseName);
    }
    
    /**
     * @see la documentación del mismo metodo en ReporteInformacionAlClienteDAO
     */
    public TreeMap obtenerTiposDeCarga() throws SQLException{
        return dao.obtenerTiposDeCarga();
    }
    
    /**
     * @see la documentación del mismo metodo en ReporteInformacionAlClienteDAO
     */
    public void buscarTiposDeCarga(String codigoCliente, String loggedUser)throws SQLException{
        dao.buscarTiposDeCarga(codigoCliente,loggedUser);
    }
    
    /**
     * @see la documentación del mismo metodo en ReporteInformacionAlClienteDAO
     */
    public void buscarDatosDeReporte(String args[]) throws SQLException {
        dao.buscarDatosDeReporte(args);
    }
    
    /**
     * @see la documentación del mismo metodo en ReporteInformacionAlClienteDAO
     */  
    public String [] obtenerCamposDeReporte(){
        return dao.obtenerCamposDeReporte();
    }
    
    /**
     * @see la documentación del mismo metodo en ReporteInformacionAlClienteDAO
     */
    public Vector obtenerDatosReporte(){
        return dao.obtenerDatosReporte();
    }
    
    /**
     * @see la documentación del mismo metodo en ReporteInformacionAlClienteDAO
     */
    public Hashtable obtenerTitulosDeReporte() throws SQLException{
        return dao.obtenerTitulosDeReporte();
    }
    
    /**
     * @see la documentación del mismo metodo en ReporteInformacionAlClienteDAO
     */
    public String obtenerEncabezadoTablaReporte(boolean isListaTipoViajeNA){
        return dao.obtenerEncabezadoTablaReporte(isListaTipoViajeNA);
    }
    
    /**
     * @see la documentación del mismo metodo en ReporteInformacionAlClienteDAO
     */
    public String [] obtenerTodosLosCampos() throws SQLException {
        return dao.obtenerTodosLosCampos();
    }
    
    /**
     * Busca el detalle de los campos del reporte dado, pero solo trae el detalle de los campos dados.
     * @param codReporte El codigo del reporte a buscar
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     * @autor Alejandro Payares
     */
    public void buscarDetalleDeCampos(String codReporte, String [] campos)throws SQLException {
        dao.buscarDetalleDeCampos(codReporte, campos);
    }
    
    /**
     * Devuelve el detalle de los campos del reporte buscado por el metodo buscarDetalleDeCampos
     * @return Un Vector que contiene un Hashtable por cada fila encontrada para el reporte.
     * @autor Alejandro Payares
     */
    public Hashtable obtenerDetalleDeCampos(){
        return dao.obtenerDetalleDeCampos();
    }
}
