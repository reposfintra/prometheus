/********************************************************************
 *      Nombre Clase.................   EstadViajesService.java
 *      Descripci�n..................   Service de la tabla estadistica_viajes
 *      Autor........................   Ing. Tito Andr�s Maturana De La Cruz
 *      Fecha........................   15 de febrero de 2006, 02:28 PM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  Ing. Tito Andr�s Maturana De La Cruz EMPRESA=Transportes S�nchez Polo S.A.
 */
public class EstadViajesService {
    private EstadViajesDAO dao;
    
    /** Crea una nueva instancia de  EstadViajesService */
    public EstadViajesService() {
        dao = new EstadViajesDAO();
    }
    
    /**
     * Obtiene la cantidad de viajes por Propietario/Conductor y Tipo de carga.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param ano A�o del per�odo
     * @param mes Mes del per�odo
     * @param cia Distrito
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public  synchronized void obtenerViajes(int ano, String mes, String cia) throws SQLException{
        dao.obtenerViajes(ano, mes, cia);
    }
    
    /**
     * Verifica si existe un registro en el archivo estadistica_viajes
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public  synchronized boolean existe() throws SQLException{
        return dao.existe();
    }
    
    /**
     * Inserta un registro en el archivo estadistica_viajes
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public  synchronized void insertar() throws SQLException{
        dao.insertar();
    }
    
    /**
     * Actualiza un registro en el archivo estadistica_viajes
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public  synchronized void actualizar() throws SQLException{
        dao.actualizar();
    }
    
    /**
     * Getter for property nviajes.
     * @return Value of property nviajes.
     */
    public  synchronized com.tsp.operation.model.beans.EstadViajes getNviajes() {
        return dao.getNviajes();
    }
    
    /**
     * Setter for property nviajes.
     * @param nviajes New value of property nviajes.
     */
    public synchronized  void setNviajes(com.tsp.operation.model.beans.EstadViajes nviajes) {
        dao.setNviajes(nviajes);
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public synchronized  java.util.Vector getVector() {
        return dao.getVector();
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public synchronized  void setVector(java.util.Vector vector) {
        dao.setVector(vector);
    }
    
    /**
     * Obtiene todos los registro del archivo estadistica_viajes cuyo a�o sea mator o igual que el suministrado.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param ano A�o. 
     * @param tipo C - Conductor, P - Propietario.
     * @param cia Distrito
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public synchronized void listar(int ano, String tipo, String cia) throws SQLException{
        dao.listar(ano, tipo, cia);
    }
    
    /**
     * Actualiza el total de viajes en el archivo conductor
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param nit Nit del conductor
     * @param nviajes Cantidad de viajes realizados 
     * @param user Login del usuario que actualiza
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public synchronized void actualizarViajes(String nit, int nviajes, String user) throws SQLException{
        dao.actualizarViajes(nit, nviajes, user);
    }
    
}
