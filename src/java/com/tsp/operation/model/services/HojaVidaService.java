/***********************************************************************************
 * Nombre clase : ............... HojaVidaService.java                             *
 * Descripcion :................. Clase que maneja los Servicios                   *
 *                                asignados a Model relacionados con el            *
 *                                programa de Autorizacion de cuentas por pagar    *
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                     *
 * Fecha :....................... 10 de octubre de 2005, 11:02 AM                  *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import java.util.*;

public class HojaVidaService {    
    
    private HojaVida hv;    
    
    public HojaVidaService() {}
    
    /**
     * Getter for property hv.
     * @return Value of property hv.
     */
    public HojaVida getHojaVida() {
        return hv;
    }
    
    /**
     * Setter for property hv.
     * @param hv New value of property hv.
     */
    public void setHojaVida(HojaVida hv) {
        this.hv = hv;
    }    
}
    