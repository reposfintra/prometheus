/***************************************
 * Nombre Clase   AdicionOPServices.java
 * Descripci�n    Permite formar facturas adicional a la OP para una planilla. Los movimientos de la
 *                planilla que no esten involucrados en la OP y que no esten contabilizados.
 * Autor          FERNEL VILLACOB DIAZ
 * Fecha          14/11/2006
 * versi�n        1.0
 * Copyright      Transportes Sanchez Polo S.A.
 *******************************************/




package com.tsp.operation.model.services;



import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.util.Utility;
import com.tsp.util.Util;
import java.util.*;
import java.io.*;

import com.tsp.finanzas.contab.model.DAO.MovAuxiliarDAO;
import com.tsp.finanzas.contab.model.DAO.ContabilizacionFacturasDAO;



public class AdicionOPServices   extends LiquidarOCService{
    
    
    private  OpDAO              OpDataAccess;
    private  AdicionOPDAO       AdicionOPDataAccess;
    private  TasaService        svcTasa;
    
    private  List               listOCs;
    
    
    private String  VENEZUELA_PLACA  = "VE";   
    private String  TIPO_FACTURA     = "010";
    private String  TIPO_CREDITO     = "035";
    private String  TIPO_DEBITO      = "036";
    
    
    private String  CLASE_DOC_FAC    = "4";
        
    
    OPImpuestos     IMP_RETEFUENTE;
    OPImpuestos     IMP_RETEIVA;
    
    private String  PESO       = "PES";
    private String  BOLIVAR    = "BOL";
    private int     DECIMALES  = 2;
    
    
    
    private  MovAuxiliarDAO              movDAO;    
    private  ContabilizacionFacturasDAO  contabDAO;
    
    
    public AdicionOPServices() {
        OpDataAccess         =  new  OpDAO();
        AdicionOPDataAccess  =  new  AdicionOPDAO();
        svcTasa              =  new  TasaService();
        listOCs              =  new  LinkedList();
        movDAO               =  new  MovAuxiliarDAO();
        contabDAO            = new ContabilizacionFacturasDAO();
    }
    
    
    
    
    
    
     /**
     * M�todo  realiza la generacion de facturas para planillas con mov. no incluidos en la op
     * @autor fvillacob
     * @throws Exception
     * @version 1.0.
     **/
    public void generarFacturasAdicionOP(String distrito)throws Exception{
        listOCs =  new LinkedList();
        try{
            listOCs  =  this.AdicionOPDataAccess.getOCMovPendientes(distrito);
            for(int i=0;i<listOCs.size();i++){
                OP   op    =  (OP) listOCs.get(i);
                
                if(op.getComentario().equals("")){
                   List items =   this.AdicionOPDataAccess.getMovPendientesOC(op.getDstrct(), op.getOc(), op.getProveedor(), op.getHandle_code() );
                   op.setItem(items);  
                }
                
                Liquidar(op);
                   
            }            
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
    
    
    
    /**
     * M�todo  realiza la liquidacion
     * @autor fvillacob
     * @throws Exception
     * @version 1.0.
     **/
    public void Liquidar(OP   op)throws Exception{
        try{
            
            if(op.getComentario().equals("")){
            
            
                        String hoy         = Utility.getHoy("-");  
                        String monedaLocal = op.getMonedaLocal();
                        String moneda      = op.getMoneda();


                        String msj = "";


                     // VALIDACION CABECERA:
                        if( monedaLocal.trim().equals("")       )  msj   += " No existe moneda Local "; 
                        if( op.getProveedor().trim().equals("") )  msj   += " No presenta proveedor establecido ";             
                        if( op.getBanco().trim().equals("")     )  msj   += " El proveedor no presenta banco ";
                        if( op.getSucursal().trim().equals("")  )  msj   += " El  proveedor no presenta sucursal de banco ";
                        if( moneda.trim().equals("")            )  msj   += " El banco  "+  op.getBanco() +" "+ op.getSucursal() +" del proveedor  no presenta moneda o no existe";
                        if( op.getOrigenOC().equals("")         )  msj   += " No presenta Origen "; 



                        List items =  op.getItem();


                     // CONVERSION DE MONEDAS:   
                        if( msj.equals("") ){

                              for(int i=0;i<items.size();i++){
                                  OPItems  item       = (OPItems) items.get(i);
                                  
                                  double   vlrMov     = item.getVlr();
                                  double   vlrMov_ME  = vlrMov;
                                  String   monedaItem = item.getMoneda();
                                  String   cta        = item.getCodigo_cuenta();
                                  String   auxCTA     = item.getAuxiliar();

                               // Completamos datos de los Items----------------:
                                  item.setProveedor ( op.getProveedor()  );
                                  item.setDocumento ( op.getDocumento()  );
                                  item.setBase      ( op.getBase()       );   
                                  item.setReteFuente( op.getReteFuente() );
                                  item.setReteIca   ( op.getReteIca()    );
                                  item.setReteIva   ( op.getReteIva()    );                      

                               // ----------------------------------------------

                                  
                                  
                                  if( monedaItem.trim().equals(""))   
                                      msj   += "El item "+ item.getDescripcion() +" no presenta  moneda ";                      

                                  
                                  
                                  if( !cta.trim().equals("")){
                                      
                                       Hashtable  cuenta =  movDAO.getCuenta( item.getDstrct() , cta  );
                                       if( cuenta!= null){
                                      
                                                 if( auxCTA.equals("S") ){
                                                      if(  item.getAuxiliar().equals("")  )
                                                           msj += "La cuenta " + cta +" requiere auxiliar y el item " +  item.getDescripcion()  +" no presenta";                                      
                                                      if( !contabDAO.existeCuentaSubledger( item.getDstrct() , cta , item.getAuxiliar()  )   )
                                                           msj += "La cuenta " + cta + " no tiene el subledger "+ item.getAuxiliar()  +" asociado";
                                                 }
                                      
                                                 if(  item.getIndicador().toUpperCase().equals("V")){ // VALOR

                                                      // convertir moneda movimiento a la del banco VLR_ME
                                                          if( !moneda.equals( monedaItem ))  {          
                                                                 Tasa  obj  =   svcTasa.buscarValorTasa(monedaLocal,  monedaItem , moneda , hoy);
                                                                 if( obj==null)
                                                                     msj += "No hay tasa de conversi�n de " + monedaItem + " a "+ moneda  ;
                                                                 else
                                                                     vlrMov_ME =  obj.getValor_tasa() *  vlrMov_ME;
                                                          }

                                                     // convertir moneda banco a la local   VLR_LOCAL
                                                           if( !monedaLocal.equals( moneda )){
                                                                Tasa  obj  =   svcTasa.buscarValorTasa(monedaLocal,  moneda , monedaLocal , hoy);
                                                                 if( obj==null)
                                                                     msj += "No hay tasa de conversi�n de " + moneda + " a "+ monedaLocal;
                                                                 else
                                                                     vlrMov  =  obj.getValor_tasa() *  vlrMov;
                                                            }
                                                            else
                                                                 vlrMov  = vlrMov_ME;

                                                       item.setVlr_me  ( vlrMov_ME   );
                                                       item.setVlr     ( vlrMov      );
                                                       item.setMoneda  ( moneda      );

                                                  }
                                                 
                                     }else
                                         msj += "La cuenta contable " + cta + " no existe o no est� activa.";
                                      
                                      
                                  }else
                                      msj   += "El item "+ item.getDescripcion() +" no presenta  cuenta contable "; 

                              }

                        }




                        if( msj.equals("") ){

                          // Valor mov. tipo indicador P : Porcentaje:
                             items  =  calPorcentajeItems(op.getVlrOc(), items, monedaLocal,moneda, hoy);

                         //  Redondeamos valores Items:
                             items  =  super.redondear(items);



                             
                         // EXCLUIMOS VALORES ITEMS DE VALORES CEROS:
                             List  aux =  new LinkedList();
                             for(int hh=0; hh<items.size(); hh++){
                                 OPItems  item       = (OPItems) items.get(hh);
                                 if( item.getVlr_me() != 0 )
                                     aux.add( item );
                             }
                             items = aux;
                             

                             
                         //  IMPUESTOS:
                             String  retenedor    =  op.getRetenedor().toUpperCase();
                             String  placa        =  op.getPlaca();
                             String  pais_placa   =  super.LiquidarOCDataAccess.paisPlaca(placa);

                             if(  !pais_placa.equals( VENEZUELA_PLACA )  && !moneda.equals( BOLIVAR )     ) {
                                   if(  !retenedor.equals("S")  ){

                                         String     fechaFINANO     =  Util.getFechaActual_String(1) +"-12-31";                  // Fecha Fin de A�o.
                                         Hashtable  DATOS_ORIGEN    =  OpDataAccess.getDatosAgencia( op.getOrigenOC() );

                                         if( DATOS_ORIGEN == null ){
                                             msj += " El origen de la oc no se encuentra registrado en la tabla de ciudad ";
                                         }else{
                                              String codigoICA     =  (String)DATOS_ORIGEN.get("codica");
                                              String aplica        =  (String)DATOS_ORIGEN.get("aplica");
                                              if(  aplica.toUpperCase().equals("S") || aplica.toUpperCase().equals("Y")  ){                                 
                                                     codigoICA     =  super.LiquidarOCDataAccess.codigoImp_tablaGen(codigoICA);   //Equivalente al codigo en tablagen.                                                                                       
                                                     IMP_RETEICA   =  OpDataAccess.getImpuestoAgencia ( op.getDstrct(), codigoICA , fechaFINANO, op.getOrigenOC() );
                                              }
                                         }

                                         IMP_RETEFUENTE      =  OpDataAccess.getImpuestoAgencia ( op.getDstrct(), OpDataAccess.CODERETEFUENTE, fechaFINANO, op.getOrigenOC() ); 

                                        // Aplicamos Impuestos a los Items:       
                                           for(int i=0; i<items.size();i++){
                                             OPItems item     =  (OPItems) items.get(i);
                                                     item     =  this.calculoImpuesto( item, DATOS_ORIGEN );
                                           } 


                                   }                        

                             }



                         // TOTALES:    
                            op.setVlrNeto     (  super.calculoNetoLiquidacion   ( items ) );
                            op.setVlr_neto_me (  super.calculoNetoMELiquidacion ( items ) );

                            double tasa = 1; 
                            Tasa   obj  =   svcTasa.buscarValorTasa(monedaLocal,  moneda , monedaLocal , hoy);
                            if( obj!=null)
                                tasa = obj.getValor_tasa();
                            op.setTasa( tasa );


                        // DEFINIMOS TIPO DOCUMENTO DE ACUERDO AL VALOR:  
                            
                            String  tipo_documento = TIPO_FACTURA;
                            
                            // Si la factura tiene Saldo, creamos una NC o ND asociada a la factura.
                            if( op.getSaldo() != 0 ){
                                if( op.getVlr_neto_me()>=0 )  tipo_documento = TIPO_DEBITO;    // NOTA DEBITO
                                if( op.getVlr_neto_me()<0 ){
                                    tipo_documento = TIPO_CREDITO;                             // NOTA CREDITO
                                    op.setVlr_neto_me(  op.getVlr_neto_me() *  -1 );
                                    op.setVlrNeto    (  op.getVlrNeto()     *  -1 );
                                }
                            }
                            
                            
                            if( op.getEstado().equals("A")  ) 
                                tipo_documento = TIPO_FACTURA;
                            
                            op.setClase_doc_rel("0");
                            if( tipo_documento.equals( TIPO_FACTURA ) ){                      // SI ES FACTURA
                                op.setDocumento_relacionado("");
                                op.setTipo_documento_rel   ("");
                                op.setClase_doc_rel        ("4");
                            }

                            op.setTipo_documento( tipo_documento  ); 


                            
                         // Aplicamos el Tipo de documento para los items:
                            for(int i=0;i<items.size();i++){
                                  OPItems  item       = (OPItems) items.get(i);
                                  item.setTipo_documento( op.getTipo_documento() ); 
                                  
                                  if( tipo_documento.equals( TIPO_CREDITO )   ){  // Invertimos el valor
                                      item.setVlr_me          ( item.getVlr_me()            *  -1  );
                                      item.setVlr             ( item.getVlr()               *  -1  );                                      
                                      item.setVlrReteFuente   ( item.getVlrReteFuente()     *  -1  );
                                      item.setVlrReteFuente_ME( item.getVlrReteFuente_ME()  *  -1  );
                                      item.setVlrReteIca      ( item.getVlrReteIca()        *  -1  );
                                      item.setVlrReteIca_ME   ( item.getVlrReteIca_ME()     *  -1  );                                      
                                  }
                                  
                            }
                            

                        }            


                        op.setComentario( msj );
                        op.setItem( items );
                        
            }
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }        
    }
    
    
    
    
    
    
    /**
     * M�todo calcularPorcentajeItems, calcula el valor del movimiento de planilla Indicador P con respecto a la sumatoria de los movimientos Asigando V
     * @autor...fvillacob
      * @return OP (orden de pago Objeto)
     * @throws..Exception
     * @version..1.0.
     **/
    public List calPorcentajeItems(double vlrOC, List lista, String monedaLocal, String monedaBanco, String fecha ) throws Exception{
        try{
            
            double vlrocTotal  = vlrOC;
            if( lista !=null  && lista.size()>0 ){
                
                // Sumatoria de los items ASIGNADOR = V
                for(int i=0;i<lista.size();i++){
                    OPItems  item  = (OPItems) lista.get(i);
                    if( item.getAsignador().toUpperCase().equals("V")){
                        vlrocTotal  += item.getVlr();
                    }
                }
                
                // Calculamos valor item indicador P  = ( sumatoria items asignador V * valor item P ) /100
                for(int i=0;i<lista.size();i++){
                    OPItems  item  = (OPItems) lista.get(i);
                    if( item.getIndicador().toUpperCase().equals("P")){
                        double  vlr      =  (vlrocTotal  * item.getVlr())/100;
                        double  me       =  opDAO.getValorMoneda( monedaLocal, vlr , monedaBanco, fecha, item.getDstrct() );
                        
                        item.setVlr   ( vlr );
                        item.setVlr_me( me  );
                    }
                }
            }
            
        }catch(Exception e){
            throw new Exception( " calcularPorcentajeItems "+ e.getMessage());
        }
        return lista;
    }
    
    
    
    
    // IMPUESTOS :
    
    /**
     * M�todo calculoImpuesto, calcula valor impuestos a los item
     * @autor...fvillacob
     * @param item
     * @return OPItems
     * @throws..Exception
     * @version..1.0.
     **/
    public OPItems calculoImpuesto( OPItems item , Hashtable datosAgencia ) throws Exception{
        try{
            
            List impuestos = new LinkedList();
            
            if(  item.getAsignador().toUpperCase().equals("V")){   //  V:valor  S: saldo
                
               
                if( item.getDstrct().equals( OpDataAccess.COLOMBIA )){
                    
                  
                    String  aplica     =  (String)datosAgencia.get("aplica"); 
                    String  pais       =  (String)datosAgencia.get("pais"); 
                    
                    
                    double  vlrImp     = 0;
                    double  vlrImp_ME  = 0;                    
                    String  moneda     =   item.getMoneda();
                                        
                    
                    
                    //RETEFUENTE...Si el proveedor (nitpro OC) es agente retefuente
                    if( item.getReteFuente().equals("S")){
                        
                        if ( pais.toUpperCase().equals("CO") ){
                            
                            vlrImp        =  ( ( item.getVlr()     *  this.IMP_RETEFUENTE.getPorcentaje() ) /100   )  * this.IMP_RETEFUENTE.getSigno();
                            vlrImp_ME     =  ( ( item.getVlr_me()  *  this.IMP_RETEFUENTE.getPorcentaje() ) /100   )  * this.IMP_RETEFUENTE.getSigno();
                            
                            // Redondeo para peso y bolivar
                            if( moneda.equals(PESO)  ||   moneda.equals(BOLIVAR) ){
                                vlrImp     =  (int)Math.round( vlrImp    );
                                vlrImp_ME  =  (int)Math.round( vlrImp_ME );
                            }
                            else{
                                vlrImp     =   Util.roundByDecimal( vlrImp,    DECIMALES  );
                                vlrImp_ME  =   Util.roundByDecimal( vlrImp_ME,  DECIMALES  );
                            }
                            
                            item.setVlrReteFuente   ( vlrImp    );
                            item.setVlrReteFuente_ME( vlrImp_ME );
                            if( vlrImp!=0 ||  vlrImp_ME!=0 )
                                impuestos.add(IMP_RETEFUENTE);
                            
                        }
                    }
                    
                    
                    //RETEICA...Si la ciudad aplica RETEICA
                    if(  aplica.toUpperCase().equals("S") || aplica.toUpperCase().equals("Y")){
                        
                        vlrImp     =  (  ( item.getVlr()     *  this.IMP_RETEICA.getPorcentaje() ) /100 )  * this.IMP_RETEICA.getSigno();
                        vlrImp_ME  =  (  ( item.getVlr_me()  *  this.IMP_RETEICA.getPorcentaje() ) /100 )  * this.IMP_RETEICA.getSigno();
                        
                        // Redondeo para peso y bolivar
                        if( moneda.equals(PESO)  ||   moneda.equals(BOLIVAR) ){
                            vlrImp     =  (int)Math.round( vlrImp    );
                            vlrImp_ME  =  (int)Math.round( vlrImp_ME );
                        }else{
                            vlrImp     =  Util.roundByDecimal( vlrImp,     DECIMALES  );
                            vlrImp_ME  =  Util.roundByDecimal( vlrImp_ME,  DECIMALES  );
                        }                        
                        
                        item.setVlrReteIca    ( vlrImp    );
                        item.setVlrReteIca_ME ( vlrImp_ME );
                        if( vlrImp!=0 ||  vlrImp_ME!=0 )
                            impuestos.add(IMP_RETEICA);                        
                    }
                    
                    
                }
                
            }
            
            //registramos los impuestos del item
            item.setImpuestos(impuestos);
            
        }catch(Exception e){
            throw new Exception( e.getMessage());
        }
        return item;
    }
    
    
    
    
    
    
    
    
    
    /**
     * Getter for property listOCs.
     * @return Value of property listOCs.
     */
    public java.util.List getListOCs() {
        return listOCs;
    }    
    
    /**
     * Setter for property listOCs.
     * @param listOCs New value of property listOCs.
     */
    public void setListOCs(java.util.List listOCs) {
        this.listOCs = listOCs;
    }    
    
    
    
    
    
    
    
    // --------------------------------------------------------------------
    
     public static void main(String[] args)throws Exception{
        System.out.println("Inicializado....");
        AdicionOPServices x = new AdicionOPServices();
        x.generarFacturasAdicionOP("FINV");
        List  lista  = x.getListOCs();
        OpService svc = new OpService();
        
        String urlXLS    = "C:/ops.xls";
        String urlERROR  = "C:/ops.txt";
        
        Usuario usuario = new Usuario();
        usuario.setLogin("FVILLACOB");
        
        svc.insertList(lista, usuario, urlXLS, urlERROR, "ADICION" );
        System.out.println("Finalizado....");
        
    }
     
     
     
    
}
