// Decompiled by DJ v3.9.9.91 Copyright 2005 Atanas Neshkov  Date: 2008-02-07 07:36:56 p.m.
// Home Page : http://members.fortunecity.com/neshkov/dj.html  - Check often for new version!
// Decompiler options: packimports(3)
// Source File Name:   AnticipoGasolinaService.java

package com.tsp.operation.model.services;

import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.tsp.operation.model.DAOS.AnticipoGasolinaDAO;
import com.tsp.operation.model.beans.AnticipoGasolina;
import com.tsp.operation.model.beans.Imagen;
import com.tsp.util.Util;
import java.awt.Color;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.ResourceBundle;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import com.lowagie.text.Image;

import java.io.ByteArrayInputStream;
import java.util.Dictionary;
import java.util.Hashtable;
import javax.servlet.ServletException;
import java.util.List;
import java.util.LinkedList;
import java.io.ByteArrayOutputStream;

import java.io.File;
import java.io.FileInputStream;

import javax.servlet.http.HttpServletRequest;

// Referenced classes of package com.tsp.operation.model.services:
//            ImagenService

public class AnticipoGasolinaService
{

    public AnticipoGasolinaService()
    {
        imagenConductor = "";
        imagenFirma = "";
        ruta = "";
        ruta_img = "";
        carpeta = "images/fotos";
        estado_foto=false;
        nombre_archivo="";
        nombreNewFotoBd="";
        directorioArchivos="";
        idAntGas="";
        loginx="";

        preanticipos=new ArrayList();
        PreanticiposConsultados=new ArrayList();
        PreanticiposConduc=new ArrayList();

        AnticipoGasolinaDataAccess = new AnticipoGasolinaDAO();
    }
    public AnticipoGasolinaService(String dataBaseName)
    {
        imagenConductor = "";
        imagenFirma = "";
        ruta = "";
        ruta_img = "";
        carpeta = "images/fotos";
        estado_foto=false;
        nombre_archivo="";
        nombreNewFotoBd="";
        directorioArchivos="";
        idAntGas="";
        loginx="";

        preanticipos=new ArrayList();
        PreanticiposConsultados=new ArrayList();
        PreanticiposConduc=new ArrayList();

        AnticipoGasolinaDataAccess = new AnticipoGasolinaDAO(dataBaseName);
    }

    public boolean isAnticipoGasolina(String conductor1, String placa1, String login1)
        throws Exception
    {
        return AnticipoGasolinaDataAccess.isAnticipoGasolina(conductor1, placa1, login1);
    }

    public ArrayList getAnticiposGasolina()
    {
        return AnticipoGasolinaDataAccess.getAnticiposGasolina();
    }

    public String aceptarAnticipoGasolina(String idAntGas1, String loginx1, String valEfectiv, String valGasolin,String vlrnetoreal)
        throws Exception
    {
        idAntGas=idAntGas1;
        loginx=loginx1;
        String respuesta=AnticipoGasolinaDataAccess.aceptarAnticipoGasolina(idAntGas, loginx, valEfectiv, valGasolin,vlrnetoreal);
        //System.out.println("aceptarAnticipoGasolina en service"+respuesta);
        if (respuesta.equals("Anticipo de Estaci�n de Gasolina Aceptado.")){
            //System.out.println("si");
            guardarArchivoEnBd();
        }
        return respuesta;
    }

    public AnticipoGasolina getAnticipoGasolina(String id1,String login)
        throws Exception
    {
        return AnticipoGasolinaDataAccess.getAnticipoGasolina(id1,login);
    }

    public String getIdAnticipoGasolinaAceptado()
    {
        return AnticipoGasolinaDataAccess.getIdAnticipoGasolinaAceptado();
    }

    public String getValGasolinaAceptado()
    {
        return AnticipoGasolinaDataAccess.getValGasolinaAceptado();
    }

    public String getValEfectivoAceptado()
    {
        return AnticipoGasolinaDataAccess.getValEfectivoAceptado();
    }

    public boolean getEstado_foto()
    {
        return estado_foto;
    }

    public void setEstado_foto(boolean estado_foto1)
    {
        estado_foto=estado_foto1;
    }

    public String getNombre_archivo()
    {
        return nombre_archivo;
    }

    public void setNombre_archivo(String nombre_archivo1)
    {
        nombre_archivo=nombre_archivo1;
    }

    public void prepararPdfAnticipoGasolina(String idAnticipoGasolinaAceptado1, String valEfectiv, String valGasolin,String login)
        throws Exception
    {
        AnticipoGasolina anticipoGasolina = getAnticipoGasolina(idAnticipoGasolinaAceptado1,login);

        fuente_normal = new Font(bf, 7F);
        fuente_negrita = new Font(bf, 7F);
        fuente_negrita_azul = new Font(bf, 7F);
        fuente_negrita.setStyle(1);
        fuente_negrita_azul.setStyle(1);
        fuente_negrita_azul.setColor(Color.BLUE);
        fondo = new Color(191, 223, 255);
        generarPdfAnticipoGasolina(anticipoGasolina, valEfectiv, valGasolin);
    }

    public void generarPdfAnticipoGasolina(AnticipoGasolina anticipoGasolina, String valEfectiv, String valGasolin)
    {

        try
        {
            bf = BaseFont.createFont("Helvetica", "Cp1252", false);
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = (new StringBuilder()).append(rb.getString("ruta")).append("/pdf/PdfGasolina.pdf").toString();
            //System.out.println((new StringBuilder()).append("ruta").append(ruta).toString());
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(ruta));
            com.lowagie.text.Rectangle page = document.getPageSize();
            document.open();
            //System.out.println("antes de newpage");
            document.newPage();
            //System.out.println("despues de newpage");
            PdfPTable table = new PdfPTable(7);
            table.setWidthPercentage(100F);
            PdfPCell cell_superior_vacia = new PdfPCell();
            cell_superior_vacia.setColspan(7);
            cell_superior_vacia.setBorderWidthTop(1.0F);
            cell_superior_vacia.setBorderWidthBottom(0.0F);
            cell_superior_vacia.setBorderWidthLeft(1.0F);
            cell_superior_vacia.setBorderWidthRight(1.0F);
            cell_superior_vacia.setPhrase(new Phrase("", fuente_normal));
            table.addCell(cell_superior_vacia);
            PdfPCell cell = new PdfPCell();
            cell.setColspan(7);
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(1.0F);
            cell.setHorizontalAlignment(1);
            cell.setPhrase(new Phrase("FINTRAVALORES S.A.", fuente_negrita_azul));
            cell.setVerticalAlignment(5);
            table.addCell(cell);
            PdfPCell cell_izquierda = new PdfPCell();
            cell_izquierda.setColspan(1);
            cell_izquierda.setBorderWidthTop(0.0F);
            cell_izquierda.setBorderWidthBottom(0.0F);
            cell_izquierda.setBorderWidthLeft(1.0F);
            cell_izquierda.setBorderWidthRight(0.0F);
            cell_izquierda.setPhrase(new Phrase("", fuente_normal));
            table.addCell(cell_izquierda);
            PdfPCell cell_vacia = new PdfPCell();
            cell_vacia.setColspan(1);
            cell_vacia.setBorderWidthTop(0.0F);
            cell_vacia.setBorderWidthBottom(0.0F);
            cell_vacia.setBorderWidthLeft(0.0F);
            cell_vacia.setBorderWidthRight(0.0F);
            cell_vacia.setPhrase(new Phrase("", fuente_normal));
            table.addCell(cell_vacia);
            table.addCell(cell_vacia);
            table.addCell(cell_vacia);
            table.addCell(cell_vacia);
            table.addCell(cell_vacia);
            PdfPCell cell_derecha = new PdfPCell();
            cell_derecha.setColspan(1);
            cell_derecha.setBorderWidthTop(0.0F);
            cell_derecha.setBorderWidthBottom(0.0F);
            cell_derecha.setBorderWidthLeft(0.0F);
            cell_derecha.setBorderWidthRight(1.0F);
            cell_derecha.setPhrase(new Phrase("", fuente_normal));
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell.setColspan(1);
            cell.setBorderWidthTop(1.0F);
            cell.setBorderWidthBottom(1.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(1.0F);
            cell.setHorizontalAlignment(1);
            cell.setBackgroundColor(fondo);
            cell.setPhrase(new Phrase("AGENCIA", fuente_negrita));
            table.addCell(cell);
            cell.setBorderWidthLeft(0.0F);
            cell.setPhrase(new Phrase("CONDUCTOR", fuente_negrita));
            table.addCell(cell);
            cell.setPhrase(new Phrase("PROPIETARIO", fuente_negrita));
            table.addCell(cell);
            cell.setPhrase(new Phrase("PLACA", fuente_negrita));
            table.addCell(cell);
            cell.setPhrase(new Phrase("PLANILLA", fuente_negrita));
            table.addCell(cell);
            cell.setBackgroundColor(Color.WHITE);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthTop(0.0F);
            cell.setPhrase(new Phrase((new StringBuilder()).append("").append(anticipoGasolina.getNombreAgencia()).toString(), fuente_normal));
            table.addCell(cell);
            cell.setBorderWidthLeft(0.0F);
            cell.setPhrase(new Phrase((new StringBuilder()).append("").append(anticipoGasolina.getConductor()).append(" ; ").append(anticipoGasolina.getNombreConductor()).toString(), fuente_normal));
            table.addCell(cell);
            cell.setPhrase(new Phrase((new StringBuilder()).append("").append(anticipoGasolina.getPla_owner()).append(" ; ").append(anticipoGasolina.getNombrePropietario()).toString(), fuente_normal));
            table.addCell(cell);
            cell.setPhrase(new Phrase((new StringBuilder()).append("").append(anticipoGasolina.getSupplier()).toString(), fuente_normal));
            table.addCell(cell);
            cell.setPhrase(new Phrase((new StringBuilder()).append("").append(anticipoGasolina.getPlanilla()).toString(), fuente_normal));
            table.addCell(cell);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell.setBackgroundColor(fondo);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthTop(1.0F);
            cell.setPhrase(new Phrase("FECHA", fuente_negrita));
            table.addCell(cell);
            cell.setBorderWidthLeft(0.0F);
            cell.setPhrase(new Phrase("VALOR ANTICIPO - PREANTICIPO", fuente_negrita));
            table.addCell(cell);
            cell.setPhrase(new Phrase("VALOR A ENTREGAR", fuente_negrita));
            table.addCell(cell);
            cell.setColspan(1);
            cell.setPhrase(new Phrase("LOGIN", fuente_negrita));
            table.addCell(cell);
            cell.setPhrase(new Phrase("FECHA ACTUAL", fuente_negrita));

            table.addCell(cell);
            cell.setColspan(1);
            cell.setBackgroundColor(Color.WHITE);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthTop(0.0F);
            cell.setPhrase(new Phrase((new StringBuilder()).append("").append(anticipoGasolina.getFecha_anticipo()).toString(), fuente_normal));
            table.addCell(cell);
            cell.setBorderWidthLeft(0.0F);
            //cell.setPhrase(new Phrase((new StringBuilder()).append("$").append(Util.customFormat(anticipoGasolina.getVlr())).toString(), fuente_normal));
            cell.setPhrase(new Phrase((new StringBuilder()).append("$").append(Util.customFormat(anticipoGasolina.getVlr())).append(" - $"+Util.customFormat(anticipoGasolina.getVlrNeto()-(Double.parseDouble(anticipoGasolina.getVlr_gasolina())+Double.parseDouble(anticipoGasolina.getVlr_efectivo())))).toString(), fuente_normal));
            table.addCell(cell);
            //cell.setPhrase(new Phrase((new StringBuilder()).append("$").append(Util.customFormat(anticipoGasolina.getVlrNeto())).toString(), fuente_normal));
            cell.setPhrase(new Phrase((new StringBuilder()).append("$").append(Util.customFormat(Double.parseDouble(anticipoGasolina.getVlr_gasolina())+Double.parseDouble(anticipoGasolina.getVlr_efectivo()))).toString(), fuente_normal));
            //Double.parseDouble(anticipoGasolina.getVlr_gasolina())+Double.parseDouble(anticipoGasolina.getVlr_efectivo())
            table.addCell(cell);
            cell.setColspan(1);
            cell.setPhrase(new Phrase((new StringBuilder()).append("").append(anticipoGasolina.getAsesor()).toString(), fuente_normal));
            table.addCell(cell);
            cell.setPhrase(new Phrase((new StringBuilder()).append("").append(com.tsp.util.Util.getFullDate()).toString(), fuente_normal));

            table.addCell(cell);
            cell.setColspan(1);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(0.0F);
            cell.setBorderWidthRight(0.0F);
            table.addCell(cell_izquierda);
            cell.setPhrase(new Phrase("_____________", fuente_normal));
            table.addCell(cell);
            cell_vacia.setColspan(1);
            table.addCell(cell_vacia);
            cell.setPhrase(new Phrase("_____________", fuente_normal));
            table.addCell(cell);
            table.addCell(cell_vacia);
            cell.setPhrase(new Phrase("_____________", fuente_normal));
            table.addCell(cell);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell.setPhrase(new Phrase("SELLO ESTACI\323N", fuente_negrita_azul));
            table.addCell(cell);
            table.addCell(cell_vacia);
            cell.setPhrase(new Phrase("CONDUCTOR:", fuente_negrita_azul));
            table.addCell(cell);
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta_img = (new StringBuilder()).append(rb.getString("ruta")).append("/").append(carpeta).append("/").toString();
            //System.out.println((new StringBuilder()).append("ruta_img= ").append(ruta_img).toString());
            //System.out.println((new StringBuilder()).append("nombre_archivo").append(nombre_archivo).toString());

            //Image jpg = Image.getInstance(""+nombre_archivo);//se crea una imagen
            String usuarioPrivilegiado="9000157545";//tem . usuario que no toma foto
            String usuarioPrivilegiado2="8260036491";
            Image jpg = null;

            if (usuarioPrivilegiado.equals(anticipoGasolina.getAsesor()) || usuarioPrivilegiado2.equals(anticipoGasolina.getAsesor()) || true){// || true
                table.addCell(cell_vacia);
            }else{
                //inicio de img
                if(carpeta.equals("documentos/imagenes")){
                    jpg = Image.getInstance(ruta_img+""+nombre_archivo);//se crea una imagen
                }else{
                    jpg = Image.getInstance(""+nombre_archivo);//se crea una imagen
                }

                jpg.scaleToFit(30,20);//se reduce la imagen
                cell_vacia.setImage(jpg);//se mete la imagen en la celda

                table.addCell(cell_vacia);

                cell_vacia.setImage(null);//se mete la imagen null en la celda
                //fin de img
            }

            cell.setPhrase(new Phrase("ADMIN. ESTACI\323N", fuente_negrita_azul));
            table.addCell(cell);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            PdfPCell cell_inferior_vacia = new PdfPCell();
            cell_inferior_vacia.setColspan(7);
            cell_inferior_vacia.setBorderWidthTop(0.0F);
            cell_inferior_vacia.setBorderWidthBottom(1.0F);
            cell_inferior_vacia.setBorderWidthLeft(1.0F);
            cell_inferior_vacia.setBorderWidthRight(1.0F);
            cell_inferior_vacia.setPhrase(new Phrase("", fuente_normal));
            table.addCell(cell_inferior_vacia);
            cell_vacia.setColspan(7);
            table.addCell(cell_vacia);
            //System.out.println("antes de add table");
            document.add(table);
            //System.out.println("despues de add table");
            table = new PdfPTable(7);
            table.setWidthPercentage(100F);
            cell_superior_vacia = new PdfPCell();
            cell_superior_vacia.setColspan(7);
            cell_superior_vacia.setBorderWidthTop(1.0F);
            cell_superior_vacia.setBorderWidthBottom(0.0F);
            cell_superior_vacia.setBorderWidthLeft(1.0F);
            cell_superior_vacia.setBorderWidthRight(1.0F);
            cell_superior_vacia.setPhrase(new Phrase("", fuente_normal));
            table.addCell(cell_superior_vacia);
            cell = new PdfPCell();
            cell.setColspan(7);
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(1.0F);
            cell.setHorizontalAlignment(1);
            cell.setPhrase(new Phrase("ESTACION DE GASOLINA", fuente_negrita_azul));
            cell.setVerticalAlignment(5);
            table.addCell(cell);
            cell_izquierda = new PdfPCell();
            cell_izquierda.setColspan(1);
            cell_izquierda.setBorderWidthTop(0.0F);
            cell_izquierda.setBorderWidthBottom(0.0F);
            cell_izquierda.setBorderWidthLeft(1.0F);
            cell_izquierda.setBorderWidthRight(0.0F);
            cell_izquierda.setPhrase(new Phrase("", fuente_normal));
            table.addCell(cell_izquierda);
            cell_vacia = new PdfPCell();
            cell_vacia.setColspan(1);
            cell_vacia.setBorderWidthTop(0.0F);
            cell_vacia.setBorderWidthBottom(0.0F);
            cell_vacia.setBorderWidthLeft(0.0F);
            cell_vacia.setBorderWidthRight(0.0F);
            cell_vacia.setPhrase(new Phrase("", fuente_normal));
            table.addCell(cell_vacia);
            table.addCell(cell_vacia);
            table.addCell(cell_vacia);
            table.addCell(cell_vacia);
            table.addCell(cell_vacia);
            cell_derecha = new PdfPCell();
            cell_derecha.setColspan(1);
            cell_derecha.setBorderWidthTop(0.0F);
            cell_derecha.setBorderWidthBottom(0.0F);
            cell_derecha.setBorderWidthLeft(0.0F);
            cell_derecha.setBorderWidthRight(1.0F);
            cell_derecha.setPhrase(new Phrase("", fuente_normal));
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell.setColspan(1);
            cell.setBorderWidthTop(1.0F);
            cell.setBorderWidthBottom(1.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(1.0F);
            cell.setHorizontalAlignment(1);
            cell.setBackgroundColor(fondo);
            cell.setPhrase(new Phrase("AGENCIA", fuente_negrita));
            table.addCell(cell);
            cell.setBorderWidthLeft(0.0F);
            cell.setPhrase(new Phrase("CONDUCTOR", fuente_negrita));
            table.addCell(cell);
            cell.setPhrase(new Phrase("PROPIETARIO", fuente_negrita));
            table.addCell(cell);
            cell.setPhrase(new Phrase("PLACA", fuente_negrita));
            table.addCell(cell);
            cell.setPhrase(new Phrase("PLANILLA", fuente_negrita));
            table.addCell(cell);
            cell.setBackgroundColor(Color.WHITE);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthTop(0.0F);
            cell.setPhrase(new Phrase((new StringBuilder()).append("").append(anticipoGasolina.getNombreAgencia()).toString(), fuente_normal));
            table.addCell(cell);
            cell.setBorderWidthLeft(0.0F);
            cell.setPhrase(new Phrase((new StringBuilder()).append("").append(anticipoGasolina.getConductor()).append(" ; ").append(anticipoGasolina.getNombreConductor()).toString(), fuente_normal));
            table.addCell(cell);
            cell.setPhrase(new Phrase((new StringBuilder()).append("").append(anticipoGasolina.getPla_owner()).append(" ; ").append(anticipoGasolina.getNombrePropietario()).toString(), fuente_normal));
            table.addCell(cell);
            cell.setPhrase(new Phrase((new StringBuilder()).append("").append(anticipoGasolina.getSupplier()).toString(), fuente_normal));
            table.addCell(cell);
            cell.setPhrase(new Phrase((new StringBuilder()).append("").append(anticipoGasolina.getPlanilla()).toString(), fuente_normal));
            table.addCell(cell);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell.setBackgroundColor(fondo);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthTop(1.0F);
            cell.setPhrase(new Phrase("FECHA", fuente_negrita));
            table.addCell(cell);
            cell.setBorderWidthLeft(0.0F);
            cell.setPhrase(new Phrase("VALOR ANTICIPO - PREANTICIPO", fuente_negrita));
            table.addCell(cell);
            cell.setPhrase(new Phrase("VALOR A ENTREGAR", fuente_negrita));
            table.addCell(cell);
            cell.setColspan(1);
            cell.setPhrase(new Phrase("LOGIN", fuente_negrita));
            table.addCell(cell);
            cell.setPhrase(new Phrase("FECHA ACTUAL", fuente_negrita));
            table.addCell(cell);
            cell.setColspan(1);
            cell.setBackgroundColor(Color.WHITE);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthTop(0.0F);
            cell.setPhrase(new Phrase((new StringBuilder()).append("").append(anticipoGasolina.getFecha_anticipo()).toString(), fuente_normal));
            table.addCell(cell);
            cell.setBorderWidthLeft(0.0F);
            //cell.setPhrase(new Phrase((new StringBuilder()).append("$").append(Util.customFormat(anticipoGasolina.getVlr())).toString(), fuente_normal));
            cell.setPhrase(new Phrase((new StringBuilder()).append("$").append(Util.customFormat(anticipoGasolina.getVlr())).append(" - $"+Util.customFormat(anticipoGasolina.getVlrNeto()-(Double.parseDouble(anticipoGasolina.getVlr_gasolina())+Double.parseDouble(anticipoGasolina.getVlr_efectivo())))).toString(), fuente_normal));
            table.addCell(cell);
            //cell.setPhrase(new Phrase((new StringBuilder()).append("$").append(Util.customFormat(anticipoGasolina.getVlrNeto())).toString(), fuente_normal));
            cell.setPhrase(new Phrase((new StringBuilder()).append("$").append(Util.customFormat(Double.parseDouble(anticipoGasolina.getVlr_gasolina())+Double.parseDouble(anticipoGasolina.getVlr_efectivo()))).toString(), fuente_normal));
            table.addCell(cell);
            cell.setColspan(1);
            cell.setPhrase(new Phrase((new StringBuilder()).append("").append(anticipoGasolina.getAsesor()).toString(), fuente_normal));
            table.addCell(cell);
            cell.setPhrase(new Phrase((new StringBuilder()).append("").append(com.tsp.util.Util.getFullDate()).toString(), fuente_normal));
            table.addCell(cell);
            cell.setColspan(1);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(0.0F);
            cell.setBorderWidthRight(0.0F);
            table.addCell(cell_izquierda);
            cell.setPhrase(new Phrase("_____________", fuente_normal));
            table.addCell(cell);
            cell_vacia.setColspan(1);
            table.addCell(cell_vacia);
            cell.setPhrase(new Phrase("_____________", fuente_normal));
            table.addCell(cell);
            table.addCell(cell_vacia);
            cell.setPhrase(new Phrase("_____________", fuente_normal));
            table.addCell(cell);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell.setPhrase(new Phrase("SELLO ESTACI\323N", fuente_negrita_azul));
            table.addCell(cell);
            table.addCell(cell_vacia);
            cell.setPhrase(new Phrase("CONDUCTOR:", fuente_negrita_azul));
            table.addCell(cell);

            if (usuarioPrivilegiado.equals(anticipoGasolina.getAsesor()) || usuarioPrivilegiado2.equals(anticipoGasolina.getAsesor())  || true){// || true
                table.addCell(cell_vacia);
            }else{
                //inicio de img
                cell_vacia.setImage(jpg);//se mete la imagen en la celda
                table.addCell(cell_vacia);
                cell_vacia.setImage(null);//se mete la imagen en la celda
                //fin de img
            }

            cell.setPhrase(new Phrase("ADMIN. ESTACI\323N", fuente_negrita_azul));
            table.addCell(cell);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            cell_inferior_vacia = new PdfPCell();
            cell_inferior_vacia.setColspan(7);
            cell_inferior_vacia.setBorderWidthTop(0.0F);
            cell_inferior_vacia.setBorderWidthBottom(1.0F);
            cell_inferior_vacia.setBorderWidthLeft(1.0F);
            cell_inferior_vacia.setBorderWidthRight(1.0F);
            cell_inferior_vacia.setPhrase(new Phrase("", fuente_normal));
            table.addCell(cell_inferior_vacia);
            cell_vacia.setColspan(7);
            table.addCell(cell_vacia);
            //System.out.println("antes de add table");
            document.add(table);
            //System.out.println("despues de add table");
            table = new PdfPTable(7);
            table.setWidthPercentage(100F);
            cell_superior_vacia = new PdfPCell();
            cell_superior_vacia.setColspan(7);
            cell_superior_vacia.setBorderWidthTop(1.0F);
            cell_superior_vacia.setBorderWidthBottom(0.0F);
            cell_superior_vacia.setBorderWidthLeft(1.0F);
            cell_superior_vacia.setBorderWidthRight(1.0F);
            cell_superior_vacia.setPhrase(new Phrase("", fuente_normal));
            table.addCell(cell_superior_vacia);
            cell = new PdfPCell();
            cell.setColspan(7);
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(1.0F);
            cell.setHorizontalAlignment(1);
            cell.setPhrase(new Phrase("CLIENTE", fuente_negrita_azul));
            cell.setVerticalAlignment(5);
            table.addCell(cell);
            cell_izquierda = new PdfPCell();
            cell_izquierda.setColspan(1);
            cell_izquierda.setBorderWidthTop(0.0F);
            cell_izquierda.setBorderWidthBottom(0.0F);
            cell_izquierda.setBorderWidthLeft(1.0F);
            cell_izquierda.setBorderWidthRight(0.0F);
            cell_izquierda.setPhrase(new Phrase("", fuente_normal));
            table.addCell(cell_izquierda);
            cell_vacia = new PdfPCell();
            cell_vacia.setColspan(1);
            cell_vacia.setBorderWidthTop(0.0F);
            cell_vacia.setBorderWidthBottom(0.0F);
            cell_vacia.setBorderWidthLeft(0.0F);
            cell_vacia.setBorderWidthRight(0.0F);
            cell_vacia.setPhrase(new Phrase("", fuente_normal));
            table.addCell(cell_vacia);
            table.addCell(cell_vacia);
            table.addCell(cell_vacia);
            table.addCell(cell_vacia);
            table.addCell(cell_vacia);
            cell_derecha = new PdfPCell();
            cell_derecha.setColspan(1);
            cell_derecha.setBorderWidthTop(0.0F);
            cell_derecha.setBorderWidthBottom(0.0F);
            cell_derecha.setBorderWidthLeft(0.0F);
            cell_derecha.setBorderWidthRight(1.0F);
            cell_derecha.setPhrase(new Phrase("", fuente_normal));
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell.setColspan(1);
            cell.setBorderWidthTop(1.0F);
            cell.setBorderWidthBottom(1.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(1.0F);
            cell.setHorizontalAlignment(1);
            cell.setBackgroundColor(fondo);
            cell.setPhrase(new Phrase("AGENCIA", fuente_negrita));
            table.addCell(cell);
            cell.setBorderWidthLeft(0.0F);
            cell.setPhrase(new Phrase("CONDUCTOR", fuente_negrita));
            table.addCell(cell);
            cell.setPhrase(new Phrase("PROPIETARIO", fuente_negrita));
            table.addCell(cell);
            cell.setPhrase(new Phrase("PLACA", fuente_negrita));
            table.addCell(cell);
            cell.setPhrase(new Phrase("PLANILLA", fuente_negrita));
            table.addCell(cell);
            cell.setBackgroundColor(Color.WHITE);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthTop(0.0F);
            cell.setPhrase(new Phrase((new StringBuilder()).append("").append(anticipoGasolina.getNombreAgencia()).toString(), fuente_normal));
            table.addCell(cell);
            cell.setBorderWidthLeft(0.0F);
            cell.setPhrase(new Phrase((new StringBuilder()).append("").append(anticipoGasolina.getConductor()).append(" ; ").append(anticipoGasolina.getNombreConductor()).toString(), fuente_normal));
            table.addCell(cell);
            cell.setPhrase(new Phrase((new StringBuilder()).append("").append(anticipoGasolina.getPla_owner()).append(" ; ").append(anticipoGasolina.getNombrePropietario()).toString(), fuente_normal));
            table.addCell(cell);
            cell.setPhrase(new Phrase((new StringBuilder()).append("").append(anticipoGasolina.getSupplier()).toString(), fuente_normal));
            table.addCell(cell);
            cell.setPhrase(new Phrase((new StringBuilder()).append("").append(anticipoGasolina.getPlanilla()).toString(), fuente_normal));
            table.addCell(cell);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell.setBackgroundColor(fondo);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthTop(1.0F);
            cell.setPhrase(new Phrase("FECHA", fuente_negrita));
            table.addCell(cell);
            cell.setBorderWidthLeft(0.0F);
            cell.setPhrase(new Phrase("VALOR ANTICIPO - PREANTICIPO", fuente_negrita));
            table.addCell(cell);
            cell.setPhrase(new Phrase("VALOR A ENTREGAR", fuente_negrita));
            table.addCell(cell);
            cell.setColspan(1);
            cell.setPhrase(new Phrase("LOGIN", fuente_negrita));
            table.addCell(cell);
            cell.setPhrase(new Phrase("FECHA ACTUAL", fuente_negrita));
            table.addCell(cell);
            cell.setColspan(1);
            cell.setBackgroundColor(Color.WHITE);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthTop(0.0F);
            cell.setPhrase(new Phrase((new StringBuilder()).append("").append(anticipoGasolina.getFecha_anticipo()).toString(), fuente_normal));
            table.addCell(cell);
            cell.setBorderWidthLeft(0.0F);
            //cell.setPhrase(new Phrase((new StringBuilder()).append("$").append(Util.customFormat(anticipoGasolina.getVlr())).toString(), fuente_normal));
            cell.setPhrase(new Phrase((new StringBuilder()).append("$").append(Util.customFormat(anticipoGasolina.getVlr())).append(" - $"+Util.customFormat(anticipoGasolina.getVlrNeto()-(Double.parseDouble(anticipoGasolina.getVlr_gasolina())+Double.parseDouble(anticipoGasolina.getVlr_efectivo())))).toString(), fuente_normal));
            table.addCell(cell);
            //cell.setPhrase(new Phrase((new StringBuilder()).append("$").append(Util.customFormat(anticipoGasolina.getVlrNeto())).toString(), fuente_normal));
            cell.setPhrase(new Phrase((new StringBuilder()).append("$").append(Util.customFormat(Double.parseDouble(anticipoGasolina.getVlr_gasolina())+Double.parseDouble(anticipoGasolina.getVlr_efectivo()))).toString(), fuente_normal));
            table.addCell(cell);
            cell.setColspan(1);
            cell.setPhrase(new Phrase((new StringBuilder()).append("").append(anticipoGasolina.getAsesor()).toString(), fuente_normal));
            table.addCell(cell);
            cell.setPhrase(new Phrase((new StringBuilder()).append("").append(com.tsp.util.Util.getFullDate()).toString(), fuente_normal));
            table.addCell(cell);
            cell.setColspan(1);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(0.0F);
            cell.setBorderWidthRight(0.0F);
            table.addCell(cell_izquierda);
            cell.setPhrase(new Phrase("_____________", fuente_normal));
            table.addCell(cell);
            cell_vacia.setColspan(1);
            table.addCell(cell_vacia);
            cell.setPhrase(new Phrase("", fuente_normal));
            table.addCell(cell);
            table.addCell(cell_vacia);
            cell.setPhrase(new Phrase("_____________", fuente_normal));
            table.addCell(cell);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell.setPhrase(new Phrase("SELLO ESTACI\323N", fuente_negrita_azul));
            table.addCell(cell);
            table.addCell(cell_vacia);
            cell.setPhrase(new Phrase("", fuente_negrita_azul));
            table.addCell(cell);
            table.addCell(cell_vacia);
            cell.setPhrase(new Phrase("ADMIN. ESTACI\323N", fuente_negrita_azul));
            table.addCell(cell);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            table.addCell(cell_izquierda);
            cell_vacia.setColspan(5);
            table.addCell(cell_vacia);
            table.addCell(cell_derecha);
            cell_inferior_vacia = new PdfPCell();
            cell_inferior_vacia.setColspan(7);
            cell_inferior_vacia.setBorderWidthTop(0.0F);
            cell_inferior_vacia.setBorderWidthBottom(1.0F);
            cell_inferior_vacia.setBorderWidthLeft(1.0F);
            cell_inferior_vacia.setBorderWidthRight(1.0F);
            cell_inferior_vacia.setPhrase(new Phrase("", fuente_normal));
            table.addCell(cell_inferior_vacia);
            cell_vacia.setColspan(7);
            table.addCell(cell_vacia);
            //System.out.println("antes de add table");
            document.add(table);
            //System.out.println("despues de add table");
            table = new PdfPTable(7);
            table.setWidthPercentage(100F);
            cell.setColspan(3);
            cell.setBorderWidthTop(1.0F);
            cell.setBorderWidthBottom(1.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(1.0F);
            fuente_negrita.setColor(64, 0, 0);
            //entregable=Double.parseDouble(anticipoGasolina.getVlr_gasolina())+Double.parseDouble(anticipoGasolina.getVlr_efectivo())
            //cell.setPhrase(new Phrase((new StringBuilder()).append("\nVALOR COMBUSTIBLE\n\n$").append(Util.customFormat(Double.parseDouble(anticipoGasolina.getVlr_gasolina()))).append(" \n ").append("SELLO ESTACION").toString(), fuente_negrita));
            cell.setPhrase(new Phrase((new StringBuilder()).append("\nVALOR COMBUSTIBLE\n\n$").append(Util.customFormat(Double.parseDouble(anticipoGasolina.getVlr_gasolina()))).append(" \n\n\n\n\n\n\n\n ").append("_____________\n\n").append("SELLO ESTACION\n\n").toString(), fuente_negrita));
            table.addCell(cell);
            cell_vacia.setColspan(1);
            table.addCell(cell_vacia);
            cell.setPhrase(new Phrase((new StringBuilder()).append("\nVALOR EFECTIVO\n\n$").append(Util.customFormat(Double.parseDouble(anticipoGasolina.getVlr_efectivo()))).append(" \n\n\n\n\n\n\n\n ").append("_____________\n\n").append("SELLO ESTACION\n\n").toString(), fuente_negrita));
            table.addCell(cell);
            //System.out.println("antes de add table");
            document.add(table);
            //System.out.println("despues de add table");
            document.newPage();
            //System.out.println("antes de documento cerrado");
            document.close();
            //System.out.println("documento cerrado");
        }
        catch(Exception e)
        {
            System.out.println((new StringBuilder()).append("pdf exception_").append(e.toString()).append("____").append(e.getMessage()).toString());
        }
    }

    public String getImagenConductor(String conductor, String loginx)
    {
        String respuesta = null;
        try
        {
            ImagenService imagenSvc = new ImagenService();
            imagenSvc.searchImagen("1", "1", "1", null, null, null, "003", "032", conductor, null, null, null, null, loginx);
            java.util.List lista = imagenSvc.getImagenes();
            Imagen img;
            if(lista != null && lista.size() > 0)
            {
                img = (Imagen)lista.get(0);
                respuesta = (new StringBuilder()).append(loginx).append("/").append(img.getNameImagen()).toString();
            }
            img = null;
            lista = null;
            imagenSvc = null;
        }
        catch(Exception e)
        {
            System.out.println((new StringBuilder()).append("e").append(e.toString()).toString());
        }
        imagenConductor = respuesta;
        return respuesta;
    }

    public String getImagenConductor(String conductor, String loginx, String activity_type, String document_type, String cantidad)
    {
        String respuesta = null;
        try
        {
            ImagenService imagenSvc = new ImagenService();
            imagenSvc.searchImagen("1", "1", "1", null, null, null, activity_type, document_type, conductor, null, null, null, cantidad, loginx);
            java.util.List lista = imagenSvc.getImagenes();
            Imagen img;
            if(lista != null && lista.size() > 0)
            {
                img = (Imagen)lista.get(0);
                respuesta = (new StringBuilder()).append(loginx).append("/").append(img.getNameImagen()).toString();
            }
            img = null;
            lista = null;
            imagenSvc = null;
        }
        catch(Exception e)
        {
            System.out.println((new StringBuilder()).append("e").append(e.toString()).toString());
        }
        imagenConductor = respuesta;
        return respuesta;
    }

    public String getImagenFirma(String conductor, String loginx)
    {
        String respuesta = null;
        try
        {
            ImagenService imagenSvc = new ImagenService();
            imagenSvc.searchImagen("1", "1", "1", null, null, null, "003", "039", conductor, null, null, null, null, loginx);
            java.util.List lista = imagenSvc.getImagenes();
            Imagen img;
            if(lista != null)
            {
                img = (Imagen)lista.get(0);
                respuesta = (new StringBuilder()).append(loginx).append("/").append(img.getNameImagen()).toString();
            }
            img = null;
            lista = null;
            imagenSvc = null;
        }
        catch(Exception e)
        {
            System.out.println((new StringBuilder()).append("e").append(e.toString()).toString());
        }
        imagenFirma = respuesta;
        return respuesta;
    }

    public void setNombreNewFotoBd(String nombreNewFotoBd1){
        nombreNewFotoBd=nombreNewFotoBd1;
    }

    public void setDirectorioArchivos(String directorioArchivos1){
        directorioArchivos=directorioArchivos1;
    }

    public void guardarArchivoEnBd(){
        try{
            //variables:  soloNombreArchivo,model.AnticiposGasolinaService.getNombre_archivo(),directorioArchivos,
                //conductor,loginx
            //System.out.println("guardarArchivoEnBd");
            List archivos   = new LinkedList();
            //System.out.println("soloNombreArchivo"+nombreNewFotoBd);
            archivos.add(""+nombreNewFotoBd);
            //System.out.println("model.AnticiposGasolinaService.getNombre_archivo()"+model.AnticiposGasolinaService.getNombre_archivo());
            imagenSvc = new ImagenService();
            imagenSvc.setImagenes(archivos);

            Dictionary fields   = new Hashtable();
            fields.put("actividad",     "024" );
            fields.put("tipoDocumento", "031"   );
            fields.put("documento",     idAntGas );
            fields.put("agencia",       "OP"   );
            fields.put("usuario",       loginx);

            String ruta               = directorioArchivos;//
            //System.out.println("ruta"+ruta);
            File  file                = new File( ruta );

            String comentario="";

            if(file.exists()){
               //System.out.println("existe");
               File []arc =  file.listFiles();
               for (int i=0;i< arc.length;i++){
                   //System.out.println("in for"+arc[i].getAbsolutePath()+"__"+arc[i].getName()+"__"+arc[i].getPath());
                     if( ! arc[i].isDirectory()){
                           //System.out.println("no dir");
                           String name = arc[i].getName();
                           //System.out.println("name"+name);
                           if( imagenSvc.isLoad( imagenSvc.getImagenes(), name)   && imagenSvc.ext(name) ){
                                  //System.out.println("isload_"+file+"_"+name);
                                  FileInputStream       in  = new FileInputStream(file  +"/"+  name);
                                  //System.out.println("in");
                                  ByteArrayOutputStream out = new ByteArrayOutputStream();
                                  ByteArrayInputStream  bfin;
                                  //System.out.println("antes de read");
                                  int input                 = in.read();
                                  //System.out.println("despues de read");
                                  byte[] savedFile          = null;
                                  while(input!=-1){
                                      //System.out.println("while");
                                      out.write(input);
                                      input  = in.read();
                                      //System.out.println("fin de while");
                                  }
                                  //System.out.println("antes de close");
                                  out.close();
                                  //System.out.println("despues de close");
                                  savedFile  = out.toByteArray();
                                  //System.out.println("saved");
                                  bfin       = new ByteArrayInputStream( savedFile );
                                  //System.out.println("bfin ya");
                                  fields.put("imagen", name );
                                  //System.out.println("antes de comentario");
                              // Insertar imagen
                                 comentario += this.insertImagen(bfin, savedFile.length ,  fields);
                                 //System.out.println("insertada");
                           }
                     }
                }
            }

            imagenSvc.reset();

        }catch(Exception ee){
            System.out.println("eee"+ee.toString()+"_"+ee.getMessage());
        }
    }

    public String insertImagen(ByteArrayInputStream bfin, int longitud,  Dictionary fields) throws ServletException{
        String estado   = "";
        String fileName = (String)fields.get("imagen");
        //System.out.println("longitud"+longitud+"bfin"+bfin.toString());

        try{

        imagenSvc.setEstado(false);
        imagenSvc.insertImagen(bfin,longitud,fields);
        estado = "Imagen "+ fileName +" ha sido guardada <br>";
        imagenSvc.setEstado(true);

        }catch(Exception e){
         estado =  "No se pudo guardar la imagen "+  fileName  +"<br>" + e.getMessage() ;
        }
        return estado;
  }

  public ArrayList getAnticiposGasolinaAprobados(String estaci) throws Exception{
        anticiposGasolinaAprobados=AnticipoGasolinaDataAccess.getAnticiposGasolinaAprobados(estaci);
        return anticiposGasolinaAprobados;

  }

  public ArrayList getAnticiposGasolinaFacturados() throws Exception{
        anticiposGasolinaFacturados=AnticipoGasolinaDataAccess.getAnticiposGasolinaFacturados();
        return anticiposGasolinaFacturados;
  }

  public List  getSeleccionFacturable(String[] ids ) throws Exception{
        List seleccion = new LinkedList();

        List lista     = anticiposGasolinaAprobados;

        try{
            if( ids!= null && lista!=null)
                for(int i=0;i<ids.length;i++){
                    int id = Integer.parseInt( ids[i] );

                    for(int j=0;j<lista.size();j++){
                        AnticipoGasolina anticipo =(AnticipoGasolina)lista.get(j);
                        if(  anticipo.getId()==id ){

                             seleccion.add(anticipo);
                             lista.remove(j);
                             break;

                        }
                    }
                }

        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return seleccion;
  }

  public List  getSeleccionTransferible(String[] ids ) throws Exception{
        List seleccion = new LinkedList();

        List lista     = anticiposGasolinaTransferibles;

        try{
            if( ids!= null && lista!=null)
                for(int i=0;i<ids.length;i++){
                    String[] tem=(String[])ids[i].split(",");
                    int id = Integer.parseInt( tem[0] );

                    for(int j=0;j<lista.size();j++){
                        AnticipoGasolina anticipo =(AnticipoGasolina)lista.get(j);
                        if(  anticipo.getId()==id ){

                             seleccion.add(anticipo);
                             lista.remove(j);
                             break;

                        }
                    }
                }

        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return seleccion;
    }

    public String facturarAnticipos(List seleccion, String loginx,String documento) throws Exception {
        return AnticipoGasolinaDataAccess.facturarAnticipos(seleccion,loginx,documento);
    }

    public String transferirAnticipos(List seleccion, String loginx,String comisionador) throws Exception {
        return AnticipoGasolinaDataAccess.transferirAnticipos(seleccion,loginx,comisionador);
    }

    public void searchAnticipos       (    String distrito,        String proveedor,
                                            String ckAgencia,       String Agencia ,
                                            String ckPropietario,   String Propietario,
                                            String ckPlanilla,      String Planilla,
                                            String ckPlaca ,        String Placa,
                                            String ckConductor,     String Conductor,
                                            String ckReanticipo,    String reanticipo,
                                            String ckFechas,        String fechaIni,  String fechaFin,
                                            String ckLiquidacion,   String Liquidacion,
                                            String ckTransferencia, String Transferencia,
                                            String ckFactura,       String Factura

                                      ) throws Exception{


         try{

             lista = this.AnticipoGasolinaDataAccess.consultarAnticipos(distrito, proveedor, ckAgencia, Agencia, ckPropietario, Propietario, ckPlanilla, Planilla, ckPlaca, Placa , ckConductor, Conductor, ckReanticipo, reanticipo, ckFechas, fechaIni, fechaFin,  ckLiquidacion, Liquidacion, ckTransferencia, Transferencia, ckFactura, Factura );

         }catch(Exception e){
            throw new Exception(e.getMessage());
         }
    }

    public java.util.ArrayList getLista() {
        return lista;
    }

    public void loadRequest( HttpServletRequest request)throws Exception{
        try{
            reset();
            //estacionGas.put("distrito",      reset( (request.getParameter("distrito")==null)?"TSP":request.getParameter("distrito") )  );
            //estacionGas.put("idmims",        reset( request.getParameter("mims")          ) );
            estacionGas.put("nit",           reset( request.getParameter("nit")           ) );
            estacionGas.put("tel",         reset( request.getParameter("tel")         ) );
            //estacionGas.put("lugar",      reset( request.getParameter("lugar")      ) );
            estacionGas.put("dir",        reset( request.getParameter("dir")        ) );
            estacionGas.put("id",          reset( request.getParameter("id")          ) );
            estacionGas.put("tipo_id", reset( request.getParameter("tipo_id") ) );
            estacionGas.put("nombre",    reset( request.getParameter("nombre") ) );
            estacionGas.put("e_mail",          reset( request.getParameter("e_mail")       ) );
            //estacionGas.put("descuento",     reset( request.getParameter("descuento")     ) );
            estacionGas.put("c_paiso",     reset( request.getParameter("c_paiso")     ) );
            estacionGas.put("c_origen",     reset( request.getParameter("c_origen")     ) );

            estacionGas.put("nombre_estacion",     reset( request.getParameter("nombre_estacion")     ) );
            estacionGas.put("login_estacion",     reset( request.getParameter("login_estacion")     ) );

        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }

    public void reset(){
        estacionGas         = new  Hashtable();
        //listado                 = new  LinkedList();
    }

    public String reset(String val){
        if(val==null)
           val = "";
        return val;
    }

    public String insert()throws Exception{
        String msj = "";
        try{

             String login_estacion      =  (String) this.estacionGas.get("login_estacion");
             //System.out.println("nit"+nit);
             if (!(this.AnticipoGasolinaDataAccess.existeEstacion(login_estacion)) ){

                msj= this.AnticipoGasolinaDataAccess.saveEstacion(estacionGas);

             }else{
                msj = "Dado login ya est� asociado a una estaci�n." ;
             }


        }catch(Exception e){
           throw new Exception(e.getMessage());
        }
        return msj;
    }

    public String update()throws Exception{
        String msj = "";
        try{

             String login_estacion      =  (String) this.estacionGas.get("login_estacion");
             //System.out.println("nit"+nit);
             if ((this.AnticipoGasolinaDataAccess.existeEstacion(login_estacion)) ){

                msj= this.AnticipoGasolinaDataAccess.updateEstacion(estacionGas);

             }else{
                msj = "Dado login no est� asociado a una estaci�n." ;
             }


        }catch(Exception e){
           throw new Exception(e.getMessage());
        }
        return msj;
    }

    public java.util.List getCiudades() {
        return ciudades;
    }
    public void setCiudades(List ciudadesx) {
        ciudades=ciudadesx;
    }
    public java.util.List getEstaciones() {
        return estaciones;
    }

    public String listarEstaciones() throws Exception{

        estaciones= this.AnticipoGasolinaDataAccess.listarEstaciones();
        if (estaciones.size()>0){
            return "Consulta terminada.";
        }else{
            return "No hay estaciones.";
        }
    }

    public boolean isLogin(String login_usuario)  throws Exception{
        return this.AnticipoGasolinaDataAccess.isLogin(login_usuario);
    }

    public String asignarLoginDescuento(String login_usuario,String descuento_login,String usuario) throws Exception{
        return this.AnticipoGasolinaDataAccess.asignarLoginDescuento(login_usuario,descuento_login,usuario);
    }

    public boolean existeParLoginDescuento(String login_usuario)  throws Exception{
        return this.AnticipoGasolinaDataAccess.existeParLoginDescuento(login_usuario);
    }

    public void searchPreAnticipos (String fechaIni,  String fechaFin,String loginx) throws Exception{

         try{

             PreanticiposConsultados = this.AnticipoGasolinaDataAccess.consultarPreanticipos(fechaIni, fechaFin,loginx);

         }catch(Exception e){
            throw new Exception(e.getMessage());
         }
    }

    public void searchPreAnticiposAnulables (String fechaIni,  String fechaFin,String loginx) throws Exception{

         try{
             fechaIniGlobal=fechaIni;
             fechaFinGlobal=fechaFin;
             loginxGlobal=loginx;
             PreanticiposConsultadosAnulables = this.AnticipoGasolinaDataAccess.consultarPreanticiposAnulables(fechaIni, fechaFin,loginx);

         }catch(Exception e){
            throw new Exception(e.getMessage());
         }
    }

    public java.util.ArrayList getPreAnticiposConsultados() {
        return PreanticiposConsultados;
    }

    public void resetPreAnticiposConsultados() {
         PreanticiposConsultados=new ArrayList();
    }


    public ArrayList getPreanticipos()  throws Exception{
        return this.preanticipos;
    }

    public void setPreanticipos(ArrayList preanticipos1)  throws Exception{
        this.preanticipos=preanticipos1;
    }

    public boolean aceptarPreanticipos(ArrayList preanticipos1,String loginx)  throws Exception{
        return this.AnticipoGasolinaDataAccess.aceptarPreanticipos(preanticipos1,loginx);

    }

    public void reiniciarArregloPreanticipos(){
        this.preanticipos=null;
        this.preanticipos=new ArrayList();
    }

    public ArrayList searchPreAnticiposConductor (String conduc) throws Exception{

         try{

             PreanticiposConduc = this.AnticipoGasolinaDataAccess.consultarPreAnticiposConductor(conduc);

         }catch(Exception e){
            System.out.println("en predecond"+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
         }
         return PreanticiposConduc;
    }

    public java.util.TreeMap getTreeMapEstaciones() throws  Exception
    {
        return  this.AnticipoGasolinaDataAccess.getTreeMapEstaciones();
    }

    public ArrayList getAnticiposGasolinaTransferibles(String estaci) throws Exception{
        anticiposGasolinaTransferibles=AnticipoGasolinaDataAccess.getAnticiposGasolinaTransferibles(estaci);
        return anticiposGasolinaTransferibles;

    }

    public String AnularPreAnticipos(String[] preanticiposaanular) throws Exception{
        String tem=this.AnticipoGasolinaDataAccess.AnularPreAnticipos(preanticiposaanular);
        PreanticiposConsultadosAnulables = this.AnticipoGasolinaDataAccess.consultarPreanticiposAnulables(fechaIniGlobal, fechaFinGlobal,loginxGlobal);
        return tem;
    }

    public ArrayList getPreAnticiposConsultadosAnulables()  throws Exception{
        return this.PreanticiposConsultadosAnulables;
    }


    public String obtenerValoresAnticiposService(String id) throws Exception{
        return AnticipoGasolinaDataAccess.obtenerValoresAnticipos(id);
    }

    public void setCarpeta(String carpeta){
        this.carpeta = carpeta;
    }


    public String setCxpItemsService(String[] anticipos, String documento, String login) throws Exception{
        return AnticipoGasolinaDataAccess.setCxpItems(anticipos, documento, login);
    }

    public String setCxpDocService(String anticipo, String documento, String login, double sumaAnticipos) throws Exception{
        return AnticipoGasolinaDataAccess.setCxpDoc(anticipo, documento, login, sumaAnticipos);
    }

    public String obtenerValorCxpDocService(String noFactura) throws java.lang.Exception{
        return AnticipoGasolinaDataAccess.obtenerValorCxpDoc(noFactura);
    }

 /*   public void actualizarValorCxpDocService(String noFactura, String  valor, String proveedor) throws java.lang.Exception{
        AnticipoGasolinaDataAccess.actualizarValorCxpDoc(noFactura,valor, proveedor);
    }*/

    ////Miguel Altamiranda///
    public boolean isAnticipoGasolina2(String conductor1, String placa1, String login1, String planilla)
        throws Exception
    {
        return AnticipoGasolinaDataAccess.isAnticipoGasolina2(conductor1, placa1, login1, planilla);
    }
    public boolean isAnticipoGasolina3(String conductor1, String placa1, String login1, String planilla)
        throws Exception
    {
        return AnticipoGasolinaDataAccess.isAnticipoGasolina3(conductor1, placa1, login1, planilla);
    }
    ///Fin Miguel Altamiranda///


        /**
     * Consulta con el login del usuario los datos de estacion
     * @param usuario Login del usuario
     * @return TreeMap con los datos
     * @throws Exception Cuando hay error
     */
    public java.util.TreeMap treeMapEstacionesUsuario(String usuario) throws  Exception{
        return this.AnticipoGasolinaDataAccess.treeMapEstacionesUsuario(usuario);
    }

    /**
     * Consulta si el login del usuario es de una estacion
     * @param usuario Login del usuario
     * @return Si es o no usuario de estacion
     * @throws Exception Cuando hay error
     */
    public boolean esEstacionUsuario(String usuario) throws Exception {
        return this.AnticipoGasolinaDataAccess.esEstacionUsuario(usuario);
    }

    /**
     * Genera el consecutivo para la cxp automatica
     * @param seriecode Nombre del document_type en la tabla de series
     * @return Cadena con el codigo generado
     * @throws Exception Cuando hay error
     */
    public String codigoCXPAuto(String seriecode) throws Exception{
        return this.AnticipoGasolinaDataAccess.codigoCXPAuto(seriecode);
    }

    /**
     * Busca datos de una estacion
     * @param login_estacion login de la estacion
     * @return Objeto BeanGeneral con los datos encontrados
     * @throws Exception Cuando hay error
     */
    public com.tsp.operation.model.beans.BeanGeneral datosEstacion(String login_estacion) throws Exception{
        return this.AnticipoGasolinaDataAccess.datosEstacion(login_estacion);
    }



    private AnticipoGasolinaDAO AnticipoGasolinaDataAccess;
    private java.util.List listAnticiposGasolina;
    BaseFont bf;
    Font fuente_normal;
    Font fuente_negrita;
    Font fuente_negrita_azul;
    Color fondo;
    Phrase frase;
    String imagenConductor;
    String imagenFirma;
    String ruta;
    ResourceBundle rb;
    String ruta_img;
    String carpeta;
    String nombre_archivo;
    boolean estado_foto;
    String nombreNewFotoBd;
    String directorioArchivos;
    String idAntGas;
    String loginx;
    ImagenService imagenSvc;
    ArrayList anticiposGasolinaAprobados;
    ArrayList anticiposGasolinaTransferibles;
    ArrayList anticiposGasolinaFacturados;
    ArrayList                   lista;

    //private  List              listado;
    private  Hashtable         estacionGas;
    List ciudades;
    List estaciones;

    ArrayList preanticipos,PreanticiposConsultados,PreanticiposConsultadosAnulables;
    ArrayList                   PreanticiposConduc;
    String fechaIniGlobal,  fechaFinGlobal,loginxGlobal;
}
