/*
 * Nombre        ReporteTiemposPuestosDeControlService.java
 * Descripci�n   Clase que presta los servicios para el acceso a los datos del reporte.
 * Autor         Alejandro Payares
 * Fecha         1 de febrero de 2006, 03:30 PM
 * Version       1.0
 * Coyright      Transportes Sanchez Polo SA.
 */

package com.tsp.operation.model.services;

import com.tsp.exceptions.InformationException;

import com.tsp.operation.model.DAOS.ReporteTiemposPuestosDeControlDAO;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 * Clase que presta los servicios para el acceso a los datos del reporte.
 * @author Alejandro Payares
 */
public class ReporteTiemposPuestosDeControlService {
    
    private ReporteTiemposPuestosDeControlDAO dao;
    
    /**
     * Crea una nueva instancia de ReporteTiemposPuestosDeControlService
     * @autor  Alejandro Payares
     */
    public ReporteTiemposPuestosDeControlService() {
        dao = new ReporteTiemposPuestosDeControlDAO();
    }
    
    /**
     * Busca los datos para el reporte. Los datos encontrados son almacenados en una lista de tipo
     * <CODE>java.util.LinkedList</CODE> donde cada elemento en un <CODE>java.util.Hashtable</CODE>
     * que contiene los datos correspondiente a una fila del reporte.
     * @param fechaInicio La fecha de inicio de busqueda del reporte.
     * @param fechaFin La fecha de fin de busqueda del reporte.
     * @throws SQLException Si algun error con la base de datos ocurre.
     */    
    public void buscarDatos(String fechaInicio, String fechaFin ) throws SQLException {
        dao.buscarDatos(fechaInicio, fechaFin);
    }
    
    /**
     * Devuelve los datos encontrados por el m�todo buscarDatos().
     * @return La lista con los datos del reporte.
     */  
    public LinkedList obtenerDatos(){
        return dao.obtenerDatos();
    }
    
    /**
     * Elimina los datos arrojados por el reporte, es util para liberar memoria en el servidor
     */
    public void borrarDatos(){
        dao.borrarDatos();
    }
    
    /**
     * Calcula el tiempo promedio entre los puestos de control dados
     * @param PCOrigen El puesto de control de origen
     * @param PCDestino El puesto de control de destino
     * @throws SQLException Si algun error ocurre en la base de datos
     * @throws InformationException Si los puestos de control no son adyacentes.
     */    
    public void calcularTiempoPromedioEntrePuestosDeControl(String PCOrigen, String PCDestino )throws SQLException, InformationException {
        dao.calcularTiempoPromedioEntrePuestosDeControl(PCOrigen, PCDestino);
    }
    
    public String obtenerPromedio(){
        return dao.obtenerPromedio();
    }
    
}
