/********************************************************************
 *      Nombre Clase.................   DocumentoAplicacionService.java    
 *      Descripci�n..................   Service de la tabla tblcat_doc    
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  Andres
 */
public class DocumentoAplicacionService {
        private DocumentoAplicacionDAO apl_doc;
        private String varSeparadorJS = "~";
        private String  varCamposJS;
        private String appselected = "";
        
        /** Creates a new instance of DocumentoAplicacionService */
        public DocumentoAplicacionService() {
                this.apl_doc = new DocumentoAplicacionDAO();
        }
        
        /**
         * Obtiene un listado a partir del c�digo de un aplicaci�n.
         * @autor Tito Andr�s Maturana
         * @param distrito Distrito.
         * @param aplicaciion C�digo de la aplicaci�n
         * @throws SQLException
         * @version 1.0
         */
        public Vector obtenerDocumentosAplicacion(String distrito, String app)
                throws SQLException{
                        
                this.apl_doc.obtenerDocumentosAplicacion(distrito, app);
                return this.apl_doc.getApl_docs();
        }
        
        /**
         * Genera una cadena de caracteres de la declaraci�n de
         * un arreglo en lenguaje JavaScript a partir del listado de
         * aplicaciones de un distrito.
         * @autor Tito Andr�s Maturana
         * @param distrito Distrito de las aplicaciones.
         * @param app C�digo de la aplicaci�n
         * @throws SQLException
         * @version 1.0
         */
        public void GenerarJSCampos(String distrito, String app) 
                throws SQLException{
                        
                AplicacionDAO appDAO = new AplicacionDAO();
                DocumentoDAO docDAO = new DocumentoDAO();
                String var = "\n var CamposJSDoc = [ ";
                Vector docs = this.obtenerDocumentosAplicacion(distrito, app);
                if (docs!=null)
                        for (int i=0; i<docs.size(); i++){
                                DocumentoAplicacion apl_doc = (DocumentoAplicacion) docs.elementAt(i);                                
				docDAO.obtenerDocumento(apl_doc.getDistrito(), apl_doc.getDocumento());
				Documento doc = docDAO.getDoc();
                                var += "\n '" + doc.getC_document_type() + varSeparadorJS + doc.getC_document_name() +"',";
                        }
                var = var.substring(0,var.length()-1) + "];";
                var += "\n var CamposJSApp = [ ";
                appDAO.obtener(distrito, this.getAppselected());                
                Aplicacion appc = appDAO.getApp();
                var += "\n '" + appc.getC_codigo() + varSeparadorJS + appc.getC_descripcion() +"',";
                var = var.substring(0,var.length()-1) + "];";
                appDAO.setApp(null);
                
                this.varCamposJS = var;
        }
        
        /**
         * Inserta un nuevo registro.
         * @autor Tito Andr�s Maturana
         * @param obj Objeto de la clase DocumentoAplicacion a ingresar
         * @throws SQLException
         * @version 1.0
         */
        public void ingresarDocumentoAplicacion(DocumentoAplicacion obj)
                throws SQLException{
                
                this.apl_doc.setApl_doc(obj);
                this.apl_doc.ingresar();
        }
        
        /**
         * Actualiza un registro.
         * @autor Tito Andr�s Maturana
         * @param obj Objeto de la clase DocumentoAplicacion a actualizar
         * @throws SQLException
         * @version 1.0
         */
        public void actualizarDocumentoAplicacion(DocumentoAplicacion obj)
                throws SQLException{
                
                this.apl_doc.setApl_doc(obj);
                this.apl_doc.update();
        }
        
        /**
         * Establece si existe un registro
         * @autor Tito Andr�s Maturana
         * @param distrito Distrito.
         * @param documento C�digo del documento
         * @param aplicaciion C�digo de la aplicaci�n
         * @throws SQLException
         * @version 1.0
         */
        public boolean existeAplicacionDocumento(String cia, String doc, String act)
                throws SQLException{
        
                return this.apl_doc.existeAplicacionDocumento(cia, doc, act);
        }
        
        /**
         * Obtiene un registro
         * @autor Tito Andr�s Maturana
         * @param distrito Distrito.
         * @param documento C�digo del documento
         * @param aplicaciion C�digo de la aplicaci�n
         * @throws SQLException
         * @version 1.0
         */
        public DocumentoAplicacion obtenerAplicacionDocumento(String cia, String doc, String act)
                throws SQLException{
                
                this.apl_doc.obtenerAplicacionDocumento(cia, doc, act);
                return this.apl_doc.getApl_doc();
        }
        
        
        /**
         * Getter for property varCamposJS.
         * @return Value of property varCamposJS.
         */
        public java.lang.String getVarCamposJS() {
                return varCamposJS;
        }
        
        /**
         * Setter for property varCamposJS.
         * @param varCamposJS New value of property varCamposJS.
         */
        public void setVarCamposJS(java.lang.String varCamposJS) {
                this.varCamposJS = varCamposJS;
        }
        
        /**
         * Getter for property docselected.
         * @return Value of property docselected.
         */
        public java.lang.String getAppselected() {
                return appselected;
        }        
        
        /**
         * Setter for property docselected.
         * @param docselected New value of property docselected.
         */
        public void setAppselected(java.lang.String appselected) {
                this.appselected = appselected;
        }
        
}
