/******************************************************************
* Nombre                        FitmenService.java
* Descripci�n                   Clase Service para la tabla fitmen
* Autor                         ricardo rosero
* Fecha                         10/01/2006
* Versi�n                       1.0
* Coyright                      Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  rrosero
 */
public class FitmenService {
    //atributos
    private FitmenDAO fitDAO;
    
    /** Creates a new instance of FitmenService */
    public FitmenService() {
        fitDAO = new FitmenDAO();
    }
    
    /**
     * Getter for property fitDAO.
     * @return Value of property fitDAO.
     */
    public com.tsp.operation.model.DAOS.FitmenDAO getFitDAO() {
        return fitDAO;
    }
    
    /**
     * Setter for property fitDAO.
     * @param fitDAO New value of property fitDAO.
     */
    public void setFitDAO(com.tsp.operation.model.DAOS.FitmenDAO fitDAO) {
        this.fitDAO = fitDAO;
    }
    
    /**
     * adiciona un nuevo fitmen a la base de datos
     * @autor                       Karen Reales
     * @param                       none
     * @throws                      SQLException
     * @version                     2.0
     */
    public String addFitmen() throws SQLException{
        return fitDAO.addFitmen();
        
    }
    
    /**
     * cambia el estado de un fitmen a "anulado"
     * @autor                       ricardo rosero
     * @param                       none
     * @throws                      SQLException
     * @version                     1.0
     */
    public void anularFitmen() throws SQLException{
        try{
            fitDAO.anularFitmen();
        }
        catch(SQLException sql){
            sql.printStackTrace();
        }
    }
    
    /**
     * elimina todos los registros de la tabla fitmen
     * @autor                       ricardo rosero
     * @param                       none
     * @throws                      SQLException
     * @version                     1.0
     */
    public void deleteFitmen() throws SQLException{
        try{
            fitDAO.deleteFitmen();
        }
        catch(SQLException sql){
            sql.printStackTrace();
        }
    }
    
    /**
     * verifica si un fitmen existe o no en la base de datos
     * @autor                       ricardo rosero
     * @param                       none
     * @throws                      SQLException
     * @version                     1.0
     */
    public boolean existeFitmen() throws SQLException{
        boolean existe = false;
        try{
            existe = fitDAO.existeFitmen();
        }
        catch(SQLException sql){
            sql.printStackTrace();
        }
        return existe;
    }
    
    /**
     * obtiene un vector con todos los registros de la tabla fitmen
     * @autor                       ricardo rosero
     * @param                       none
     * @throws                      SQLException
     * @version                     1.0
     */
    public Vector obtenerInformeUbicacion() throws SQLException{
        ////System.out.println("Estoy en el service");
        return fitDAO.obtenerInformeUbicacion();
    }
    
    /**
     * retorna un vector
     * @autor                       ricardo rosero
     * @param                       none
     * @throws                      SQLException
     * @version                     1.0
     */
    public Vector getFitVec(){
        return fitDAO.getFitVec();
    }
    
    /**
     * setea un vector
     * @autor                       ricardo rosero
     * @param                       none
     * @throws                      SQLException
     * @version                     1.0
     */
    public void setFitVec(Vector fit){
        fitDAO.setFitVec(fit);
    }
    
    /**
     * actualiza un registro de la tabla fitmen
     * @autor                       ricardo rosero
     * @param                       none
     * @throws                      SQLException
     * @version                     1.0
     */
    public String updateFitmen() throws SQLException{
       return fitDAO.updateFitmen();
       
    }
    /**
     * M�todo que busca un fitmen dada la placa del trailer
     * @autor                       ricardo rosero
     * @throws                      SQLException
     * @version                     1.0.
     **/
    public void buscarFitmen(String trailer) throws SQLException{
        fitDAO.buscarFitmen(trailer);
    }
    public Fitmen getFitmen() {
        return fitDAO.getFit();
    }
    public void setFitmen(Fitmen fit) {
        fitDAO.setFit(fit);
    }
}
