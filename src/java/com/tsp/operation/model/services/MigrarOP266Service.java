   /***************************************
    * Nombre Clase ............. MigrarOP266DAO.java
    * Descripci�n  .. . . . . .  Permite Setear listado de Facturas de OC anuladas
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  12/10/2005
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/



package com.tsp.operation.model.services;

import com.tsp.operation.model.*;
import java.io.*;
import java.util.*;
import com.tsp.operation.model.DAOS.*;



public class MigrarOP266Service {
    
    
    private MigrarOP266DAO  MigrarOP266DataAccess;
    private List            lista;
    
    
    public MigrarOP266Service() {
        MigrarOP266DataAccess = new MigrarOP266DAO();
        lista                 = null;
    }
    
    
    
    
    public void serachOP()throws Exception{
        try{
            this.lista  = this.MigrarOP266DataAccess.searchOCAnuladas();
        }catch(Exception e){
            throw new Exception ( e.getMessage());
        }
    }
    
    
    public List getListadoOP(){
        return this.lista;
    }
    
    
    
    
    public void updateDocumento()throws Exception{
        try{
            this.MigrarOP266DataAccess.UpdateDocument(this.lista);
        }catch(Exception e){
            throw new Exception ( e.getMessage());
        }
    }
    
    
}
