/***************************************************
 * Nombre clase :  ClienteActividadService.java
 * Descripcion : Clase que maneja los Servicios
 *               asignados a Model relacionados con el
 *               programa de clienteactividad
 * Autor :    Ing. Diogenes Antonio Bastidas Morales
 * Fecha :    29 de agosto de 2005, 10:57 AM     
 * Version :   1.0                                  
 * Copyright : Fintravalores S.A.              
 ***************************************************/
 

package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;


public class ClienteActividadService {
    private ClienteActividadDAO clientactd;
    private Planilla pla;
    private Cliente cliente;
    private List listaClientes;
    /** Creates a new instance of ClienteActividadService */
    public ClienteActividadService() {
        clientactd = new ClienteActividadDAO();
    }
    
    public ClienteActividad obtClienteActividad(){
        return clientactd.obtClienteActividad();
    }
    
    public Vector obtVecClientAct(){
        return clientactd.obtVecClientAct();
    }
    /**
     * Metodo insertarClientAct, ingresa un registro en la tabla clienteactividad
     * @param: insertarClientAct - ClienteActividadService
     * @see: 
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void insertarClientAct(ClienteActividad ca) throws SQLException {
        try{
            clientactd.setClienteActividad(ca);
            clientactd.insertarClientAct();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo modificarClientAct, modifica un registro en la tabla clienteactividad
     * @param: objeto actividad
     * @see: modificarClientAct -ClientaActividadDAO
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void modificarClientAct(ClienteActividad ca) throws SQLException {
        try{
            clientactd.setClienteActividad(ca);
            clientactd.modificarClientAct();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo listarClientAct, lista las actividades de los clientes                   
     * @ param: 
     * @ see:listarClientAct - ClienteActividadDAO
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */ 
    public void listarClientAct( ) throws SQLException {
        try{
            clientactd.listarClientAct();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    } 
    /**
     * Metodo listarClientActxBusq, lista las actividades del cliente dependiendo al codigo del cliente,
     * codigo de actividad, tipo viaje. nota: la busqueda es con like.                                
     * @param:compa�ia, codigo actividad, cliente, tipo 
     * @see:listarClientActxBusq - ClienteActividadDAO
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void listarClientActxBusq(String cia, String act, String clien, String tipo ) throws SQLException {
        try{
            clientactd.listarClientActxBusq(cia, act, clien, tipo);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo  buscarClientAct, busca la activida dependiendo al codigo del cliente,
     * codigo de actividad, tipo viaje
     * @param: cliente, actividad, tipo, compa�ia
     * @see: buscarClientAct - ClienteActividadDAO
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarClientAct(String cli,String act, String tipo,String cia) throws SQLException {
        try{
            clientactd.buscarClientAct(cli, act, tipo, cia);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo  ExisteClientActAnulada, retorna true o false si la relacion clienteactividad esta anulada.
     * @param:cliente,actividad,tipo,distrito
     * @see:existeClientActAnulada- ClienteActividadDAO
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean existeClientActAnulada(String cli,String act, String tipo,String cia) throws SQLException {
        try{
            return clientactd.existeClientActAnulada(cli, act, tipo, cia);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
     /**
     * Metodo  anularClientAct, anula de la tabla  clienteactividad.
     * @param: objeto clienteactividad
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: anularClientAct - ClienteActividadDAO
     * @version : 1.0
     */
    public void anularClientAct(ClienteActividad ca) throws SQLException {
        try{
            clientactd.setClienteActividad(ca);
            clientactd.anularClientAct();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo ListarActiClient, lista las actividades asignadas un cliente                   
     * @param: distrito, cliente
     * @see: listarActiClient - ClienteActividadDAO
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */ 
    public void listarActiClient(String cia,  String clien ) throws SQLException {
        try{
            clientactd.listarActiClient(cia, clien);
        }catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
     /**
     * Metodo  ActiClientTipo, retorna true si encuentra la activida dependiendo al codigo del cliente,
     * cliente, tipo
     * @param: distrito, cliente, tipo viaje 
     * @see: actiClientTipo - ClienteActividadDAO
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean actiClientTipo(String cia,  String clien, String tipo ) throws SQLException {
        try{
            return  clientactd.actiClientTipo(cia, clien, tipo);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo  existeSecuencia, retorna true existe la secuencia dependiendo distrito, cliente, nro sec tipo
     * @param: distrito, cliente, tipo viaje 
     * @see:  existeSecuencia - ClienteActividadDAO
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean existeSecuencia(String cia,  String clien, int sec, String tipo ) throws SQLException {
        return clientactd.existeSecuencia(cia, clien, sec, tipo);
    }
    /**
     * Metodo  buscarActCliente, returna true si en cuentra la actividad activa para ese cliente, tipo y numrem
     * este metodo verifica el listado de actividades del cliente y se esta cerrada la actividad busca la siguiente
     * actividad asignada a ese cliente
     * @param: cliente, actividad, tipo, compa�ia
     * @see: buscarActCliente - ClienteActividadDAO
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean buscarActCliente(String cia,  String clien, String tipo, String numrem ) throws SQLException {
        boolean estado = false;
        if ( clientactd.buscarActCliente(cia, clien, tipo) ){
            //busco la ultima aciviadad cerrada
            ClienteActividad act_cerrada = clientactd.buscarUltimaActividadCerrada(cia, clien, tipo, numrem);
            Vector actividadesclien = clientactd.obtVecClientAct();
            if ( act_cerrada == null ){
                clientactd.setClienteActividad((ClienteActividad) actividadesclien.get(0));
                estado = true;
            }
            else{
                for (int i=0 ; i< actividadesclien.size() ; i++ ){
                    ClienteActividad actividad = (ClienteActividad) actividadesclien.get(i);
                    //buscar la actividad cerrada para asignar la siguente
                    if (actividad.getCodActividad().equals(act_cerrada.getCodActividad()) ){
                        if ((i+1) < actividadesclien.size() ){
                            clientactd.setClienteActividad((ClienteActividad) actividadesclien.get(i+1));
                            estado = true;
                        }
                        else{
                            clientactd.setClienteActividad((ClienteActividad) actividadesclien.get(actividadesclien.size()-1));
                            estado = true;
                        }
                    }
                }
            }
        }
        
        return estado;
    }
    
    /**
     * Getter for property pla.
     * @return Value of property pla.
     */
    public com.tsp.operation.model.beans.Planilla getPla() {
        return pla;
    }
    
    /**
     * Setter for property pla.
     * @param pla New value of property pla.
     */
    public void setPla(com.tsp.operation.model.beans.Planilla pla) {
        this.pla = pla;
    }
        /**
     * Metodo consultarActividades, lista las actividades asignadas un cliente x remesa                
     * @param: del numero de la remesa
     * @see: consultarActividades - ClienteActividadDAO
     * @autor : LREALES
     * @version : 1.0
     */ 
    public void consultarActividades( String remesa ) throws SQLException {
        
        try{
            
            clientactd.consultarActividades( remesa );
            
        } catch( SQLException e ){
            
            throw new SQLException( e.getMessage() );
        }
        
    }
    
    
    /**
     * M�todo buscarCliente  metodo para buscar los datos de un cliente
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String nombre de la tabla
     * @version.....1.0.
     **/
    public void buscarCliente(String codcli)throws SQLException {
        this.setCliente(clientactd.buscarCliente(codcli));
    }
    
    /**
     * M�todo buscarCliente  metodo para buscar los datos de un cliente
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String nombre de la tabla
     * @version.....1.0.
     **/
    public void buscarClientePorNombre(String nomcli)throws Exception {
        this.setListaClientes(clientactd.buscarClientePorNombre(nomcli));
    }
    
    /**
     * Getter for property cliente.
     * @return Value of property cliente.
     */
    public com.tsp.operation.model.beans.Cliente getCliente() {
        return cliente;
    }    
    
    /**
     * Setter for property cliente.
     * @param cliente New value of property cliente.
     */
    public void setCliente(com.tsp.operation.model.beans.Cliente cliente) {
        this.cliente = cliente;
    }
    
    /**
     * Getter for property listaClientes.
     * @return Value of property listaClientes.
     */
    public java.util.List getListaClientes() {
        return listaClientes;
    }
    
    /**
     * Setter for property listaClientes.
     * @param listaClientes New value of property listaClientes.
     */
    public void setListaClientes(java.util.List listaClientes) {
        this.listaClientes = listaClientes;
    }
    
}
