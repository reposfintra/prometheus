/*********************************************************************************
 * Nombre clase :      ImprimirOrdenDeCargaService.java                          *
 * Descripcion :       Service del ImprimirOrdenDeCargaService.java              *
 * Autor :             LREALES                                                   *
 * Fecha :             12 de mayo de 2006, 1:45 PM                               *
 * Version :           1.0                                                       *
 * Copyright :         Fintravalores S.A.                                   *
 *********************************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;

public class ImprimirOrdenDeCargaService {
    
    private ImprimirOrdenDeCargaDAO imprimir_oc;
    private String duplicado;
    
    /** Creates a new instance of ImprimirOrdenDeCargaService */
    public ImprimirOrdenDeCargaService () {
        
        imprimir_oc = new ImprimirOrdenDeCargaDAO ();
        
    }
    
    /** Funcion publica que obtiene el metodo getVec_lista del DAO */
    public Vector getVec_lista () throws SQLException {
        
        return imprimir_oc.getVec_lista();
        
    }    
    
    /** Funcion publica que obtiene el metodo getVec_buscar del DAO */
    public Vector getVec_buscar () throws SQLException {
        
        return imprimir_oc.getVec_buscar();
        
    } 
    
    /** Funcion publica que obtiene el metodo getHojaOrden del DAO */
    public HojaOrdenDeCarga getHojaOrden () throws SQLException {
       
        return imprimir_oc.getHojaOrden();
        
    }
    
    /** Funcion publica que obtiene el metodo existeOrdenDeCarga del DAO */
    public boolean existeOrdenDeCarga ( String distrito, String orden ) throws SQLException {
        
        imprimir_oc.existeOrdenDeCarga ( distrito, orden );
        return imprimir_oc.existeOrdenDeCarga( distrito, orden );
        
    }
    
    /** Funcion publica que obtiene el metodo listaOrdenDeCarga del DAO */
    public void listaOrdenDeCarga ( String distrito, String orden ) throws SQLException {
        
        imprimir_oc.listaOrdenDeCarga ( distrito, orden );
        
    }
    
    /** Funcion publica que obtiene el metodo buscarOrdenDeCarga del DAO */
    public void buscarOrdenDeCarga ( String distrito, String orden ) throws SQLException {
        
        imprimir_oc.buscarOrdenDeCarga ( distrito, orden );
        
    }
        
    /** Funcion publica que obtiene el metodo Update_Fecha_De_Impresion del DAO */
    public void Update_Fecha_De_Impresion ( String user_update, String distrito, String orden ) throws SQLException {
        
        imprimir_oc.Update_Fecha_De_Impresion ( user_update, distrito, orden );
        
    }
        
    /**
     * Getter for property duplicado.
     * @return Value of property duplicado.
     */
    public java.lang.String getDuplicado() {
        return duplicado;
    }
    
    /**
     * Setter for property duplicado.
     * @param duplicado New value of property duplicado.
     */
    public void setDuplicado(java.lang.String duplicado) {
        this.duplicado = duplicado;
    }
     public void setHoc(com.tsp.operation.model.beans.HojaOrdenDeCarga hoc) {
        imprimir_oc.setHoc(hoc);
    }
     /**
     *Metodo que inserta orden de carga 
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void insert () throws SQLException {
        imprimir_oc.insert();
    }
     /**
     *Metodo que modifica una orden de carga 
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void update () throws SQLException {
        imprimir_oc.update();
    }
     /**
     *Metodo que anula una orden de carga 
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void delete () throws SQLException {
        imprimir_oc.delete();
    }
    /* Metodo que retorna un String con comandos sql para actualizar
     *una orden de cargue con el numero de la planilla que la utilizo
     *@return: String con comandos sql
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra. */
    public String marcar() throws SQLException {
        return imprimir_oc.marcar();
    }
     /**
     *Metodo llena un vector de ordenes de cargue realizadas por un usuario
     *@autor: Karen Reales
     *@param: Usuario usuario
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void listar(Usuario usuario) throws SQLException {
        imprimir_oc.listar(usuario);
    }
    
     /**
     *Metodo llena un vector que contiene el reporte de cumplimiento
     *@autor: Osvaldo P�rez Ferrer
     *@param: String inicio
     *@param: String fin
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public Vector reporteCumplimiento(String inicio, String fin) throws SQLException{
        return imprimir_oc.obtenerReporteCumplimiento(inicio, fin);
    }
    
     /**
     *Metodo llena un vector de ordenes de cargue filtradas por el usuario
     *@autor: Karen Reales
     *@param: String usuario, String agencia, String cliente
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void filtrar(String usuario, String agencia, String cliente) throws SQLException {
        imprimir_oc.filtrar(usuario, agencia, cliente);
    }

}
