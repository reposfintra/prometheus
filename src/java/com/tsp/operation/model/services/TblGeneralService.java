
/******************************************************************
* Nombre ......................TblGeneralService.java
* Descripci�n..................Clase Service para tabla general
* Autor........................Armando Oviedo
* Fecha........................15/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/
 
package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  Armando Oviedo
 */
public class TblGeneralService {
    
    private TblGeneralDAO tgdao;
    //HOsorio
    private TreeMap tipos = new TreeMap();
    private TreeMap clases = new TreeMap();
    //SEscalante
    private TreeMap causas_demora = new TreeMap();
    private TreeMap causas_ccelcond = new TreeMap();
    //JEscandon
    private TreeMap work_group = new TreeMap();
    private TreeMap usuarios_aut = new TreeMap();
    //LParody
    private TreeMap sub_work_group = new TreeMap();
    private TreeMap group = new TreeMap();
    private TreeMap clase_cuenta = new TreeMap();
    private TreeMap unidad_proyecto = new TreeMap();
    private TreeMap elemento_gasto = new TreeMap();
    private TreeMap area = new TreeMap();
    private TreeMap agencia = new TreeMap();  
    private TreeMap ciudad = new TreeMap();   
    
    //Jose
    private TreeMap rin = new TreeMap();
    private TreeMap piso = new TreeMap();
    private TreeMap modelo = new TreeMap();
    private TreeMap titulo = new TreeMap();
    
    //Tito
    private TreeMap metodos = new TreeMap();
    
    
    // ivan 
    private List autXCP = new LinkedList();
    
    /** Creates a new instance of TblGeneralService */
    public TblGeneralService() {
        tgdao = new TblGeneralDAO();
    }
    public TblGeneralService(String dataBaseName) {
        tgdao = new TblGeneralDAO(dataBaseName);
    }
    
    /**
     * M�todo que setea un objeto TblGeneral
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......TblGeneral de
     **/
    public void setTblGeneral(TblGeneral tg){
        tgdao.setTG(tg);
    }
    
    /**
     * M�todo que retorna un objeto TblGeneral
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @return.......TblGeneral de
     **/     
    public TblGeneral getTblGeneral(){
        return tgdao.getTblGeneral();
    }
    
    /**
     * M�todo que guetea todas los detalles de tblgeneral
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     * @return......Vector de objetos TblGeneral
     **/     
    public Vector getTodosTblGeneral(){
        return tgdao.getTodosTblGeneral();
    }
    
    /**
     * M�todo que busca una lista de codigo y descripcion dado un codigo de tabla
     * y un codigo de programa
     * @param.......Recibe un codigo de tabla y un codigo de programa
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.     
     **/     
     public void buscarLista(String tabla, String programa)throws SQLException{
         tgdao.buscarLista(tabla, programa);
     }
    /**
     * M�todo que obtiene el atributo Lista_des
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.     
     **/     
    public java.util.TreeMap getLista_des() {
        return tgdao.getLista_des();
    }
    
    /**
     * M�todo que setea el atributo Lista_des
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.     
     **/     
    public void setLista_des(java.util.TreeMap lista_des) {
        tgdao.setLista_des( lista_des);
    }  

    /**
     * Getter for property causas.
     * @return Value of property causas.
     */
    public java.util.TreeMap getTipos() {
        return tipos;
    }
    /**
     * Metodo searchDatosTablaTipo, Busca los datos de la tabla Tipo en la tabla general
     * @autor : Ing. Henry A. Osorio Gonz�lez     
     * @see : searchDatosTablaTipo  -   TblGeneralDAO
     * @version : 1.0
     */         
    public void searchDatosTablaTipo() throws SQLException {
        tgdao.buscarLista("ET", "PLACA");
        tipos = tgdao.getLista_des();        
    }
    /**
     * Metodo searchDatosTablaClase, Busca los datos de la tabla Clase en la tabla general
     * @autor : Ing. Henry A. Osorio Gonz�lez     
     * @see : searchDatosTablaTipo  -   TblGeneralDAO
     * @version : 1.0
     */
     public void searchDatosTablaClase() throws SQLException {
        tgdao.buscarLista("EC", "PLACA");
        clases = tgdao.getLista_des();        
    }
    
    /**
     * Obtiene los c�digos de causas de demora en la tabla general
     * @autor Ing. Tito Andr�s Maturana
     * @returns Objeto <code>TreeMap</code> con los c�digos y descripciones de las causas de demora
     * @see com.tsp.operation.model.DAOS.TblGeneralDAO#buscarLista(String, String)
     * @see com.tsp.operation.model.DAOS.TblGeneralDAO#getLista_des()
     * @version 1.0
     */        
     public void searchDatosTablaCausasDemora() throws SQLException {
        tgdao.buscarLista("CD", "TRAFICO");
        causas_demora = tgdao.getLista_des();        
    }
    
    /**3157056073
     * Obtiene los c�digos de causas de cambio de celular de un conductor
     * @autor Ing. Sandra Escalante
     * @returns Objeto <code>TreeMap</code> con los c�digos y descripciones de las causas de cambio de celular de un conductor
     * @see com.tsp.operation.model.DAOS.TblGeneralDAO#buscarLista(String, String)
     * @see com.tsp.operation.model.DAOS.TblGeneralDAO#getLista_des()
     * @version 1.0
     */
     public void searchDatosTablaCambioCelCond() throws SQLException {
        tgdao.buscarLista("TCC", "TRAFICO");
        causas_ccelcond = tgdao.getLista_des();        
    }
    
    /**
     * Getter for property causas.
     * @return Value of property causas.
     */
    public java.util.TreeMap getClases() {
        return clases;
    }
    
    /**
     * Getter for property causas.
     * @return Value of property causas_demora.
     */
    public java.util.TreeMap getCausasDemora() {
        return causas_demora;
    }
    
    /**
     * Getter for property causas.
     * @return Value of property causas_ccelcond.
     */
    public java.util.TreeMap getCausasCambioCelCond() {
        return causas_ccelcond;
    }
    
    /**
     * Getter for property work_group.
     * @return Value of property work_group.
     */
    public java.util.TreeMap getWork_group() {
        return work_group;
    }    
   
    /**
     * Setter for property work_group.
     * @param work_group New value of property work_group.
     */
    public void setWork_group(java.util.TreeMap work_group) {
        this.work_group = work_group;
    }
    
    /**
     * Metodo searchDatosTablaWorkGroup, Busca los datos de la tabla work_group en la tabla general
     * @autor : Ing. Juan Manuel Escandon Perez     
     * @version : 1.0
     */         
    public void searchDatosTablaWorkGroup() throws SQLException {
        tgdao.buscarLista("WG", "WORKGROUP");
        work_group = tgdao.getLista_des();        
    }
    
    /**
     * Metodo searchDatosTablaWorkGroup, Busca los datos de la tabla work_group en la tabla general
     * @autor : Ing. Leonardo Parody     
     * @version : 1.0
     */         
    public void searchDatosTablaSubWorkGroup() throws SQLException {
        tgdao.buscarListaCodigo("SW", "SUBWORKGROUP");
        sub_work_group = tgdao.getLista_des();        
    }
    /**
     * Metodo searchDatosTablaClaseCuenta, Busca el codigo y la descripcion de la tabla clase_cuenta en la tabla general
     * @autor : Ing. Leonardo Parody     
     * @version : 1.0
     */         
    public void searchDatosTablaClaseCuenta() throws SQLException {
        tgdao.buscarListaCodigo("DIV", "FINANZAS");
        clase_cuenta = tgdao.getLista_des();        
    }
    /**
     * Metodo searchDatosTablaUnidaProyecto, Busca el codigo y la descripcion de la tabla unidad_proyecto en la tabla general
     * @autor : Ing. Leonardo Parody     
     * @version : 1.0
     */         
    public void searchDatosTablaUnidaProyecto() throws SQLException {
        tgdao.buscarListaCodigo("UPR", "FINANZAS");
        unidad_proyecto = tgdao.getLista_des();        
    }
    /**
     * Metodo searchDatosTablaElementoGasto, Busca el codigo y la descripcion de la tabla elemento_gasto en la tabla general
     * @autor : Ing. Leonardo Parody     
     * @version : 1.0
     */         
    public void searchDatosTablaElementoGasto() throws SQLException {
        tgdao.buscarListaCodigo("EEE", "FINANZAS");
        elemento_gasto = tgdao.getLista_des();        
    }
     /**
     * Metodo searchDatosTablaArea, Busca el codigo y la descripcion de la tabla area en la tabla general
     * @autor : Ing. Leonardo Parody     
     * @version : 1.0
     */         
    public void searchDatosTablaArea() throws SQLException {
        tgdao.buscarListaCodigo("AAA", "FINANZAS");
        area = tgdao.getLista_des();        
    }
    
    /**
     * Metodo searchDatosTablaAgencia, Busca el codigo y la descripcion de la tabla agencias en la tabla general
     * @autor : Ing. Leonardo Parody     
     * @version : 1.0
     */         
    public void searchDatosTablaAgencia() throws SQLException {
        tgdao.buscarListaCodigo("CIU", "FINANZAS");
        agencia = tgdao.getLista_des();        
    }

/**
     * Getter for property clase_cuenta.
     * @return Value of property clase_cuenta.
     */
    public java.util.TreeMap getClaseCuenta() throws SQLException {
        return clase_cuenta;
    }
    /**
     * Getter for property unidad_proyecto.
     * @return Value of property unidad_proyecto.
     */
    public java.util.TreeMap getClaseUnidadProyecto() throws SQLException {
        return unidad_proyecto;
    }
    
    /**
     * Getter for property elemento_gasto.
     * @return Value of property elemento_gasto.
     */
    public java.util.TreeMap getClaseElementoGasto() throws SQLException {
        return elemento_gasto;
    }
    
    /**
     * Getter for property area.
     * @return Value of property area.
     */
    public java.util.TreeMap getClaseArea() throws SQLException {
        return area;
    }
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.util.TreeMap getClaseAgencia() throws SQLException {
        return agencia;
    }
    /**
     * Metodo searchDatosTablaWorkGroup, Busca el codigo y la descripcion de la tabla work_group en la tabla general
     * @autor : Ing. Leonardo Parody     
     * @version : 1.0
     */         
    public void searchDatosTablaGroup() throws SQLException {
        tgdao.buscarListaCodigo("WG", "WORKGROUP");
        group = tgdao.getLista_des();        
    }
    /**
     * Getter for property sub_work_group.
     * @return Value of property sub_work_group.
     */
    public java.util.TreeMap getSub_work_group() throws SQLException {
        this.searchDatosTablaSubWorkGroup();
        return sub_work_group;
    } 
    /**
     * Getter for property group.
     * @return Value of property group.
     */
    public java.util.TreeMap getGroup() throws SQLException {
        this.searchDatosTablaGroup();
        return group;
    }
    

    /**
     * Getter for property rin.
     * @return Value of property rin.
     */
    public java.util.TreeMap getRin () {
        return rin;
    }
    
    /**
     * Setter for property rin.
     * @param rin New value of property rin.
     */
    public void setRin (java.util.TreeMap rin) {
        this.rin = rin;
    }
    
    /**
     * Getter for property piso.
     * @return Value of property piso.
     */
    public java.util.TreeMap getPiso () {
        return piso;
    }
    
    /**
     * Setter for property piso.
     * @param piso New value of property piso.
     */
    public void setPiso (java.util.TreeMap piso) {
        this.piso = piso;
    }
    
    /**
     * Getter for property modelo.
     * @return Value of property modelo.
     */
    public java.util.TreeMap getModelo () {
        return modelo;
    }
    
    /**
     * Setter for property modelo.
     * @param modelo New value of property modelo.
     */
    public void setModelo (java.util.TreeMap modelo) {
        this.modelo = modelo;
    }
    
    /**
     * Getter for property titulo.
     * @return Value of property titulo.
     */
    public java.util.TreeMap getTitulo () {
        return titulo;
    }
    
    /**
     * Setter for property titulo.
     * @param titulo New value of property titulo.
     */
    public void setTitulo (java.util.TreeMap titulo) {
        this.titulo = titulo;
    }
       /**
     * Busca los datos de la tabla m�todo en la tabla general
     * @autor Ing. Tito Andr�s Maturana D.
     * @version 1.0
     */         
    public void searchDatosTablaMetodo() throws SQLException {
        tgdao.buscarListaCodigo("TMETOD", "INDICADORES");
        this.metodos = tgdao.getLista_des();        
    }
    
    /**
     * Getter for property metodos.
     * @return Value of property metodos.
     */
    public java.util.TreeMap getMetodos() {
        return metodos;
    }
    
    /**
     * Setter for property metodos.
     * @param metodos New value of property metodos.
     */
    public void setMetodos(java.util.TreeMap metodos) {
        this.metodos = metodos;
    }
    /**
     * M�todo que busca una lista de codigo y descripcion dado un codigo de tabla
     * y un codigo de programa
     * @param.......Recibe un codigo de tabla y un codigo de programa
     * @autor.......Leonardo Parody
     * @throws......SQLException
     * @version.....1.0.     
     **/     
    public void buscarListaCodigo(String tabla, String programa)throws SQLException{
        tgdao.buscarListaCodigo(tabla, programa);
    }
    
     /**
     * M�todo que busca una lista de codigo y descripcion dado un codigo de tabla
     * y un codigo de programa
     * @param.......Recibe un codigo de tabla y un codigo de programa
     * @autor.......Ivan Gomez 
     * @throws......SQLException
     * @version.....1.0.     
     **/     
     public void buscarListaSinDato(String tabla, String programa)throws SQLException{
         tgdao.buscarListaSinDato(tabla, programa);
     }
      /**
    * Metodo searchDatosTablaAutCXP, Busca el codigo y la descripcion de la tabla AUTCXP en la tabla general
     * @autor : Ing. Juan Manuel Escandon 
     * @version : 1.0
     */         
    public void searchDatosTablaAUTCXP() throws SQLException {
        tgdao.buscarListaCodigo("AUTCXP", "USUARIOSCXP");
        usuarios_aut = tgdao.getLista_des();        
    }
    
    /**
     * M�todo que busca una lista de codigo y descripcion dado un codigo de tabla
     * y un codigo de programa
     * @param.......Recibe un codigo de tabla y un codigo de programa
     * @autor.......Ivan Gomez
     * @throws......SQLException
     * @version.....1.0.     
     **/     
    public void buscarAutXCP()throws SQLException{
        this.setAutXCP(tgdao.buscarAutXCP("AUTCXP", "USUARIOSCXP"));
    }
     
    /**
     * Getter for property usuarios_aut.
     * @return Value of property usuarios_aut.
     */
    public java.util.TreeMap getUsuarios_aut() {
        return usuarios_aut;
    }
    
    /**
     * Setter for property usuarios_aut.
     * @param usuarios_aut New value of property usuarios_aut.
     */
    public void setUsuarios_aut(java.util.TreeMap usuarios_aut) {
        this.usuarios_aut = usuarios_aut;
    }
    /**
     * M�todo que verifica si existe el dato de un programa en tablagen_Prog para una tabla dada
     * @param tabla el nombre de la tabla
     * @param programa el nombre del programa
     * @param codigo el codigo del programa buscado
     * @return Si existen datos
     * @autor David Pi�a Lopez     
     **/     
    public boolean existeCodigo( String tabla, String programa, String codigo )throws SQLException{
        return tgdao.existeCodigo( tabla, programa, codigo );
    }
    
    
//Jescandon 13-10-06
    public boolean existeRegistro(String tabla, String codigo)throws SQLException{
        return tgdao.existeRegistro(tabla, codigo);
    }
    
    /**
     * Getter for property autXCP.
     * @return Value of property autXCP.
     */
    public java.util.List getAutXCP() {
        return autXCP;
    }
    
    /**
     * Setter for property autXCP.
     * @param autXCP New value of property autXCP.
     */
    public void setAutXCP(java.util.List autXCP) {
        this.autXCP = autXCP;
    }
    
}
