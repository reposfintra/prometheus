/********************************************************************
 *      Nombre Clase.................   Perfil_vistaService.java
 *      Descripci�n..................   Service de Perfil_vistaDAO
 *      Autor........................   Rodrigo Salazar
 *      Fecha........................   13.07.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

public class Perfil_vistaService {
    
    private Perfil_vistaDAO perfil_vista;
    private String pagina;
    
    /** Creates a new instance of Perfil_vistaService */
    public Perfil_vistaService() {
        perfil_vista = new Perfil_vistaDAO();
    }
    
    /**
     * Obtiene la propiedad perfil_vista
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public Perfil_vista getPerfil_vista( )throws SQLException{
        return perfil_vista.getPerfil_vista();
    }
    
    /**
     * Obtiene la propiedad perfil_vistas
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public Vector getPerfil_vistas() {
        return perfil_vista.getPerfil_vistas();
    }
    
    /**
     * Establece la propiedad perfil_vistas
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public void setPerfil_vistas(Vector Perfil_vistas) {
        perfil_vista.setPerfil_vistas(Perfil_vistas);
    }
    
    /**
     * Inserta un nuevo registro en la tabla perfil_vista
     * @autor Rodrigo Salazar
     * @param pv Perfil-vista a ingresar
     * @throws SQLException
     * @version 1.0
     */
    public void insertPerfil_vista(Perfil_vista pv) throws SQLException{
        try{
            perfil_vista.setPerfil_vista(pv);
            perfil_vista.insertPerfil_vista();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Verifica la existencia de un nuevo registro en la tabla perfil_vista
     * @autor Rodrigo Salazar
     * @param perfil Nombre del perfil
     * @throws SQLException
     * @version 1.0
     */
    public boolean existPerfil_vista(String cod) throws SQLException{
        return perfil_vista.existPerfil_vista(cod);
    }
    
    /**
     * Obtiene un registro en la tabla perfil_vista
     * @autor Rodrigo Salazar
     * @param cod C�digo del perfil-vista
     * @throws SQLException
     * @version 1.0
     */
    public void serchPerfil_vista(String cod)throws SQLException{
        perfil_vista.searchPerfil_vista(cod);
    }
    
    /**
     * Obtiene todos los registros no anulados de la tabla perfil_vista
     * @autor Rodrigo Salazar
     * @throws SQLException
     * @version 1.0
     */
    public void listPerfil_vista()throws SQLException{
        perfil_vista.listPerfil_vista();
    }
    
    /**
     * Actualiza un registro en la tabla perfil_vista
     * @autor Rodrigo Salazar
     * @param perfil Nombre del perfil
     * @param pagina C�digo de la p�gina JSP
     * @param campo Nombre del campo
     * @param visible Propiedad de visibilidad del campo
     * @param editable Propiedad de edici�n del campo
     * @param usu Login del usuario
     * @throws SQLException
     * @version 1.0
     */
    public void updatePerfil_vista(String perfil,String pagina,String campo,String visible, String editable, String usu)throws SQLException{
        perfil_vista.updatePerfil_vista( perfil, pagina, campo, visible, editable, usu);
    }
    
    /**
     * Anula un registro en la tabla perfil_vista
     * @autor Rodrigo Salazar
     * @param unit Perfil-vista a anular
     * @throws SQLException
     * @version 1.0
     */
    public void anularPerfil_vista(Perfil_vista unit)throws SQLException{
        try{
            perfil_vista.setPerfil_vista(unit);
            perfil_vista.anularPerfil_vista();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Obtiene un registro en la tabla perfil_vista
     * @autor Rodrigo Salazar
     * @param perfil Nombre del perfil
     * @param pagina C�digo de la p�gina
     * @throws SQLException
     * @version 1.0
     */
    public Vector searchDetallePerfil_vistas(String perfil, String pagina) throws SQLException{
        try{
            return perfil_vista.searchDetallePerfil_vistas(perfil, pagina);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Obtiene todos los registros no anulados de la tabla perfil_vista
     * @autor Rodrigo Salazar
     * @throws SQLException
     * @version 1.0
     */
    public Vector listarPerfil_vistas() throws SQLException{
        try{
            return perfil_vista.listarPerfil_vistas();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Obtiene un registro en la tabla perfil_vista
     * @autor Rodrigo Salazar
     * @param perfil Nombre del perfil
     * @param pagina C�digo de la p�gina
     * @param campo Nombre del campo
     * @throws SQLException
     * @version 1.0
     */
    public Perfil_vista buscarPerfil_vista(String perfil, String pagina, String campo) throws SQLException{
        try{
            Perfil_vista s = perfil_vista.buscarPerfil_vista(perfil,pagina,campo);
            return s;
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Elimina un registro en la tabla perfil_vista
     * @autor Rodrigo Salazar
     * @param perfil Nombre del perfil
     * @param pagina C�digo de la p�gina JSP
     * @throws SQLException
     * @version 1.0
     */
    public void eliminarPerfil_vista(String perfil, String pagina) throws SQLException{
        try{
            perfil_vista.eliminarPerfil_vista(perfil,pagina);
            
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Consulta las restricciones y/o permisos de un usuario sobre 
     * una determinada p�gina JSP
     * @autor Rodrigo Salazar
     * @param login Login del usuario
     * @param pagina C�digo de la p�gina JSP
     * @throws SQLException
     * @version 1.0
     */
    public Vector consultPerfil_vistas(String login, String pagina) throws SQLException{
        try{
            this.pagina = pagina;
            return perfil_vista.consultPerfil_vistas(login, pagina);            
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Establece los permisos del perfil del usuario sobre el campo
     * @param campo El campo cuyo tipo se va a determinar
     * @param pvpag Vector de permisos del perfil del usuario sobre los campos de la p�gina
     * @throws Exception
     * @version 1.0
     */
    public String propiedad(String campo,Vector pvpag){
        Perfil_vista pv = new Perfil_vista();
        String a = "";
        for(int i=0; i<pvpag.size(); i++){
            pv= (Perfil_vista) pvpag.elementAt(i);
            if(pv.getCampo().equals(campo)){
                ////System.out.println("visible"+pv.getVisible());
                ////System.out.println("Editable"+pv.getEditable());
                if(pv.getVisible().equals("checked")){
                    ////System.out.println("visible if"+pv.getVisible());
                    a +=" type= hidden ";
                }
                if(pv.getEditable().equals("checked")){
                    ////System.out.println("Editable if"+pv.getEditable());
                    a +=" readonly= ";
                }
                ////System.out.println("a = "+a);
                return a;
            }
            ////System.out.println("for"+i+" campo=" + pv.getCampo()+ " size"+pvpag.size() );
        }
        ////System.out.println("fin for"+a+ "size"+pvpag.size());
        return a;
    }

     /**
     * Consulta las p�ginas asociadas a un determinado perfil
     * @autor Tito Andr�s Maturana
     * @param login Nombre del perfil
     * @throws SQLException
     * @version 1.0
     */
    public Vector consultarPerfil(String login) throws SQLException{
        return perfil_vista.consultarPerfil(login);
    }
    
    /**
     * Consulta los perfiles asociados a una determinada p�gina
     * @autor Tito Andr�s Maturan
     * @param vista C�digo de la p�gina JSP
     * @throws SQLException
     * @version 1.0
     */
    public Vector consultarVista(String vista) throws SQLException{
        return perfil_vista.consultarVista(vista);
    }
    
    /**
     * Obtiene un arreglo con informaci�n de los archivos y
     * carpetas de una directorio.
     * @autor Tito Andr�s Maturana
     * @param url Url del directorio
     * @throws Exception
     * @version 1.0
     */
    public ExploradorJSP explorar(String url) throws Exception{
        return ExploradorJSP.Load(url);
    }
    
    /**
     * Obtiene el tipo al que pertenece el campo
     * carpetas de una directorio.
     * @autor Leonardo Parody
     * @param campo El campo cuyo tipo se va a determinar
     * @param pagina P�gina JSP a la que pertenece el campo
     * @throws Exception
     * @version 1.0
     */
    public void Tipo_Campo(String campo,String pagina) throws SQLException{
        try{
            perfil_vista.consultarTipo_campo(campo, pagina);
        }catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Establece los permisos del perfil del usuario sobre el campo
     * @autor Leonardo Parody
     * @param ruta Es la ruta donde se encuenta el campo (Si es imagen)
     * @param campo El campo cuyo tipo se va a determinar
     * @param pvpag Vector de permisos del perfil del usuario sobre los campos de la p�gina
     * @throws Exception
     * @version 1.0
     */
    public String propiedad(String ruta, String campo,Vector pvpag, String onclick)throws SQLException{
        Perfil_vista pv = new Perfil_vista();
        String t = "";
        String a = "";
        ////System.out.println(" Voy a mirar lo del pvpg");
        ////System.out.println(pvpag != null);
        ////System.out.println(pvpag.size());
        if (pvpag != null && pvpag.size()>0){
            if (pvpag.size()>0){
                for(int i=0; i<pvpag.size(); i++){
                    pv = (Perfil_vista) pvpag.elementAt(i);
                    try{
                        this.Tipo_Campo(campo, this.pagina);
                    }catch(SQLException e){
                        throw new SQLException(e.getMessage());
                    }
                    t = perfil_vista.getTipo();
                    ////System.out.println("-------TIPO DE CAMPO--------"+t+"------"+pv.getCampo());
                    ////System.out.println("------------CAMPO--------"+campo+"-----"+pv.getCampo());
                    if((pv.getCampo().equals(campo))&&(t.equals("c"))){
                        ////System.out.println("visible"+pv.getVisible());
                        ////System.out.println("Editable"+pv.getEditable());
                        if(pv.getVisible().equals("checked")){
                            ////System.out.println("visible if"+pv.getVisible());
                            a +=" type= hidden ";
                        }
                        if(pv.getEditable().equals("checked")){
                            ////System.out.println("Editable if campo"+pv.getEditable());
                            a +=" readonly= ";
                        }
                        ////System.out.println("a = "+a);
                    }else if ((pv.getCampo().equals(campo))&&(t.equals("i"))){
                        ////System.out.println("---------Entre al if que dice tito--------");
                        if(pv.getEditable().equals("checked")){
                            ////System.out.println("Editable if imagen"+pv.getEditable());
                            a = ruta + "Disable.gif\"";
                        }else{
                            a = ruta + ".gif\""+onclick;
                        }
                        
                    }else if ((pv.getCampo().equals(campo))&&(t.equals("s"))){
                        ////System.out.println("Entre a revisar los select----------------");
                        if(pv.getEditable().equals("checked")){
                            ////System.out.println("Editable if select"+pv.getEditable());
                            a = "disabled";
                        }
                        if(pv.getVisible().equals("checked")){
                            a ="style=\"visibilility=hidden\"";
                        }
                    }else if ((pv.getCampo().equals(campo))&&(t.equals("l"))){
                        ////System.out.println("Entre a revisar los links----------------");
                        if(pv.getEditable().equals("checked")){
                            ////System.out.println("Editable if select"+pv.getEditable());
                            a = "disabled";
                        }
                        if(pv.getVisible().equals("checked")){
                            a = "style=\"visibility=hidden\"";
                        }
                    }else if ((pv.getCampo().equals(campo))&&(t.equals("b"))){
                        ////System.out.println("Entre a revisar los checkbox----------------");
                        if(pv.getEditable().equals("checked")){
                            ////System.out.println("Editable if select"+pv.getEditable());
                            a = "disabled";
                        }
                        if(pv.getVisible().equals("checked")){
                            a = "style=\"visibility=hidden\"";
                        }
                    }
                    ////System.out.println("for"+i+" campo=" + pv.getCampo()+ " size"+pvpag.size() );
                }
                ////System.out.println("fin for"+a+ "size"+pvpag.size());
            }else {
                try{
                    this.Tipo_Campo(campo, this.pagina);
                }catch(SQLException e){
                    throw new SQLException(e.getMessage());
                }
                t = perfil_vista.getTipo();
                if (t.equals("i")){
                    a = ruta + ".gif\"" + onclick;
                }else if (t.equals("c")){
                    a = "";
                }else if  (t.equals("l")){
                    a = onclick;
                }else if (t.equals("b")){
                    a = "";
                }else if (t.equals("s")){
                    a = "";
                }
                
            }
        } else {
            if (!onclick.equals("")){
                if (ruta.equals("")){
                    a = onclick;
                }else {
                    ////System.out.println(" ya mir� lo del pvpg");
                    a = ruta + ".gif\"" + onclick;
                    ////System.out.println("------------------ onclick------"+a);
                }
            } else {
                a = "";
            }
        }
        return a;
    }
    
    /**
     * Retorna el Tipo de un campo.
     * @autor Leonardo Parody Ponce.
     * @param user Login del usuario
     * @throws SQLException
     * @version 1.0
     */
    
    public String Tipo() throws SQLException{
        return perfil_vista.getTipo();
    }
     
     /**
     * Establece los permisos del perfil del usuario sobre el campo
     * @param ruta Ruta 
     * @param campo El campo cuyo tipo se va a determinar
     * @param pvpag Vector de permisos del perfil del usuario sobre los campos de la p�gina
     * @onclick Eventos javascript.
     * @throws Exception
     * @version 1.0
     */
     public String property(String ruta, String campo,Vector pvpag, String onclick)throws SQLException{
         org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
         
         Perfil_vista pv = new Perfil_vista();
         String t = "";
         String a = "";
         ////System.out.println(" Voy a mirar lo del pvpg");
         ////System.out.println(pvpag != null);
         ////System.out.println(pvpag.size());
         if (pvpag != null && pvpag.size()>0){
             if (pvpag.size()>0){
                 a = "";
                 for(int i=0; i<pvpag.size(); i++){
                     pv = (Perfil_vista) pvpag.elementAt(i);
                     try{
                         this.Tipo_Campo(campo, this.pagina);
                     }catch(SQLException e){
                         throw new SQLException(e.getMessage());
                     }
                     t = perfil_vista.getTipo();
                     ////System.out.println("-------TIPO DE CAMPO--------"+t+"------"+pv.getCampo());
                     ////System.out.println("------------CAMPO--------"+campo+"-----"+pv.getCampo());
                     
                     if(pv.getCampo().equals(campo)){
                         logger.info("CAMPO: " + campo + " | TIPO: " + t);
                     }
                     
                     if((pv.getCampo().equals(campo))&&(t.equals("text")||t.equals("input:text"))){
                         ////System.out.println("visible"+pv.getVisible());
                         ////System.out.println("Editable"+pv.getEditable());
                         if(pv.getVisible().equals("checked")){
                             ////System.out.println("visible if"+pv.getVisible());
                             a +=" type= hidden ";
                         }
                         if(pv.getEditable().equals("checked")){
                             ////System.out.println("Editable if campo"+pv.getEditable());
                             a +=" readonly= ";
                         }
                         ////System.out.println("a = "+a);
                     }else if((pv.getCampo().equals(campo))&&(t.equals("button"))){
                         ////System.out.println("visible"+pv.getVisible());
                         ////System.out.println("Editable"+pv.getEditable());
                         if(pv.getVisible().equals("checked")){
                             ////System.out.println("visible if"+pv.getVisible());
                             a +=" type= hidden ";
                         }
                         if(pv.getEditable().equals("checked")){
                             ////System.out.println("Editable if campo"+pv.getEditable());
                             a +=" readonly= ";
                         }
                         ////System.out.println("a = "+a);
                     }else if ((pv.getCampo().equals(campo))&&(t.equals("imagen"))){
                         ////System.out.println("---------Entre al if que dice tito--------");
                         if(pv.getEditable().equals("checked")){
                             ////System.out.println("Editable if imagen"+pv.getEditable());
                             int index = ruta.lastIndexOf(".");                             
                             a = ruta.substring(0,index) + "Disable.gif\"";
                         }else{
                             a = ruta + ".gif\" "+onclick;//AMATURANA 29.11.2006
                         }
                         
                     }else if ((pv.getCampo().equals(campo))&&(t.equals("select")||t.equals("input:select"))){
                         //logger.info("Entre a revisar los select: " + campo);
                         if(pv.getEditable().equals("checked")){
                             ////System.out.println("Editable if select"+pv.getEditable());
                             a = "disabled";
                         }
                         if(pv.getVisible().equals("checked")){
                             a ="style=\"visibility:hidden\"";
                         }
                     }else if ((pv.getCampo().equals(campo))&&(t.equals("link"))){
                         ////System.out.println("Entre a revisar los links----------------");
                         if(pv.getEditable().equals("checked")){
                             ////System.out.println("Editable if select"+pv.getEditable());
                             a = "disabled";
                         }
                         if(pv.getVisible().equals("checked")){
                             a = "style=\"visibility:hidden\"";
                         }
                     }else if ((pv.getCampo().equals(campo))&&(t.equals("checkbox")||t.equals("input:checkbox"))){
                         ////System.out.println("Entre a revisar los checkbox----------------");
                         if(pv.getEditable().equals("checked")){
                             ////System.out.println("Editable if select"+pv.getEditable());
                             a = "disabled";
                         }
                         if(pv.getVisible().equals("checked")){
                             a = "style=\"visibility:hidden\"";
                         }
                     }
                     ////System.out.println("for"+i+" campo=" + pv.getCampo()+ " size"+pvpag.size() );
                 }
                 if ( a.length()==0 ) {
                    return (ruta + " " + onclick);//AMATURANA 29.11.2006
                 }
                 ////System.out.println("fin for"+a+ "size"+pvpag.size());
             }else {
                 try{
                     this.Tipo_Campo(campo, this.pagina);
                 }catch(SQLException e){
                     throw new SQLException(e.getMessage());
                 }
                 t = perfil_vista.getTipo();
                 if (t.equals("imagen")){
                     a = ruta + ".gif\" " + onclick;//AMATURANA 29.11.2006
                 }else if (t.equals("texto")){
                     a = "";
                 }else if (t.equals("button")){
                     a = "";
                 }else if  (t.equals("link")){
                     a = onclick;
                 }else if (t.equals("checkbox")){
                     a = "";
                 }else if (t.equals("select")){
                     a = "";
                 }
                 
             }
         } else {
             if (!onclick.equals("")){
                 if (ruta.equals("")){
                     a = onclick;
                 }else {
                     ////System.out.println(" ya mir� lo del pvpg");
                     a = ruta + ".gif\" " + onclick;
                     ////System.out.println("------------------ onclick------"+a);
                 }
             } else {
                 a = "";
             }
         }
         
         logger.info("property: " + a);
         
         return a;
     }
    
}
