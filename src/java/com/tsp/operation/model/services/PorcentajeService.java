
    
    /*
 * PorcentajeService.java
 *
 * Created on 30 de abril de 2006, 11:55
 */

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.threads.ExportarPorcentajes;
import com.tsp.operation.model.Model;
/**
 *
 * @author  Amartinez
 */
public class PorcentajeService {
    ExportarPorcentajes h;
    private PorcentajeDao porcentajeDao;
    /** Creates a new instance of FacturaAutomaticaService */
    public PorcentajeService() {
       porcentajeDao = new PorcentajeDao();
    }
    
     /**
     * Metodo generaFactura, genera factura de un cliente en un rango de fecha
     * @autor : Ing. Fernel villacot
     * @see : generaFactura  -  FacturaAutomaticaDAO
     * @param : String fechaIni 
     * @version : 1.0
     */
    
    public boolean buscarPorcentajesPostgres(String f1, String f2)throws SQLException{
        try{
            return porcentajeDao.buscarPorcentajesPostgres(f1,f2);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public boolean buscarPorcentajesOracle()throws SQLException{
        try{
            return porcentajeDao.buscarPorcentajesOracle(); 
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public java.util.Vector getVPostgres() {
        return porcentajeDao.getVPostgres();
    }
    
    public java.util.Vector getVOracle() {
        return porcentajeDao.getVOracle();
    }
    public void setExportarPorcentajes(String fechaInicio,String fechaFinal,Model model, Usuario usuario) {
      h = new ExportarPorcentajes(fechaInicio,fechaFinal,model, usuario);  
    }
    public void Start()throws Exception{
        try{
            h.start();
        }catch(Exception e){
            e.printStackTrace();
            
        }
    }
     public java.lang.String getFecha1() {
        return porcentajeDao.getFecha1();
    }
      public java.lang.String getFecha2() {
        return porcentajeDao.getFecha2();
    }
}
