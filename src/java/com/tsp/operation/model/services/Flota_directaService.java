/********************************************************************************************
 * Nombre clase: Flota_directaService.java                                                  *
 * Descripci�n: Clase que maneja los servicios al model relacionados con la flota directa.  *
 * Autor: Ing. Jose de la rosa                                                              *
 * Fecha: 2 de diciembre de 2005, 10:36 AM                                                  *
 * Versi�n: Java 1.0                                                                        *
 * Copyright: Fintravalores S.A. S.A.                                                  *
 *******************************************************************************************/


package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

public class Flota_directaService {
    private Flota_directaDAO flota_directa;
    /** Creates a new instance of Flota_directaService */
    public Flota_directaService () {
        flota_directa = new Flota_directaDAO();
    }
    
    public Flota_directa getFlota_directa( )throws SQLException{
        return flota_directa.getFlota_directa();
    }
    
    public Vector getFlota_directas()throws SQLException{
        return flota_directa.getFlota_directas();
    }
    
    public void setFlota_directas(Vector flota_directas)throws SQLException{
        flota_directa.setFlota_directas(flota_directas);
    }
    
    /**
    * Metodo insertFlota_directa, ingresa las flotas directas (Flota_directa).
    * @autor : Ing. Jose de la rosa
    * @see insertarFlota_directa - Flota_directaDAO
    * @param : flota_directa
    * @version : 1.0
    */    
    public void insertFlota_directa(Flota_directa user) throws SQLException{
        try{
            flota_directa.setFlota_directa(user);
            flota_directa.insertFlota_directa();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }    
    
    /**
    * Metodo existFlota_directa, obtiene la informacion de las flotas directas dado unos parametros
    * (nit, propietario, placa).
    * @autor : Ing. Jose de la rosa
    * @see existFlota_directa - Flota_directaDAO
    * @param : nit, propietario y placa
    * @version : 1.0
    */       
    public boolean existFlota_directa(String nit, String placa) throws SQLException{
        try{
            return flota_directa.existFlota_directa(nit, placa);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
    * Metodo searchFlota_directa, permite buscar una flota directa dado unos parametros
    * (nit, propietario, placa).
    * @autor : Ing. Jose de la rosa
    * @see searchFlota_directa - Flota_directaDAO
    * @param : nit, propietario y placa
    * @version : 1.0
    */      
    public void searchFlota_directa(String nit, String placa)throws SQLException{
        try{        
            flota_directa.searchFlota_directa(nit, placa);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
    * Metodo listFlota_directa, lista la informacion de las discrepancias
    * @autor : Ing. Jose de la rosa
    * @see listFlota_directa - Flota_directaDAO
    * @param :
    * @version : 1.0
    */    
    public void listFlota_directa()throws SQLException{
        try{         
            flota_directa.listFlota_directa();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
    * Metodo updateFlota_directa, permite actualizar una flota directa dados unos parametros
    * (nnit, nnombre, nplaca, usuario, nit, nombre, placa).
    * @autor : Ing. Jose de la rosa
    * @see updateFlota_directa - Flota_directaDAO
    * @param : nuevo nit, nuevo propietario, nuevo placa, usuario, nit, propietario y placa
    * @version : 1.0
    */     
    public void updateFlota_directa(String nnit, String nplaca, String usuario,String nit, String placa)throws SQLException{
        try{        
            flota_directa.updateFlota_directa(nnit, nplaca, usuario, nit, placa);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }

    /**
    * Metodo anularFlota_directa, permite anular una flota directa de la bd
    * (flota_directa).
    * @autor : Ing. Jose de la rosa
    * @see anularFlota_directa - Flota_directaDAO
    * @param : Flota_directa
    * @version : 1.0
    */      
    public void anularFlota_directa(Flota_directa res)throws SQLException{
        try{
            flota_directa.setFlota_directa(res);
            flota_directa.anularFlota_directa();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo: searchDetalleFlota_directa, permite listar flotas directas por detalles dados unos parametros.
     * (nit, propie, placa)
     * @autor : Ing. Jose de la rosa
     * @see anularFlota_directa - Flota_directaDAO
     * @param : numero de nit, nombre del propietario y la placa.
     * @version : 1.0
     */       
    public void searchDetalleFlota_directa(String nit, String placa) throws SQLException{
        try{
            flota_directa.searchDetalleFlota_directa(nit, placa);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
}
