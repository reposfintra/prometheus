/*
 * Nombre        UbicacionService.java
 * Autor         Ing. Jesus Cuesta
 * Fecha         21 de junio de 2005, 02:42 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.services;

import java.util.*;
import java.sql.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;

public class UbicacionService {
    
    public UbicacionDAO ubDAO;
    
    /** Creates a new instance of UbicacionService */
    public UbicacionService() {
        ubDAO = new UbicacionDAO();
    }
    
    public com.tsp.operation.model.beans.Ubicacion getUb() {
        return ubDAO.getUb();
    }
    
    /**
     * Setter for property ub.
     * @param ub New value of property ub.
     */
    public void setUb(com.tsp.operation.model.beans.Ubicacion ub) {
        ubDAO.setUb(ub);
    }
    
    /**
     * Getter for property Ubicaciones.
     * @return Value of property Ubicaciones.
     */
    public java.util.Vector getUbicaciones() {
        return ubDAO.getUbicaciones();
    }
    
    /**
     * Setter for property Ubicaciones.
     * @param Ubicaciones New value of property Ubicaciones.
     */
    public void setUbicaciones(java.util.Vector ubicaciones) {
        ubDAO.setUbicaciones(ubicaciones);
    }
    
    /**
     * Metodo: agregarUbicacion, registra una nueva ubicacion 
     * @autor : Ing. Jesus Cuestas
     * @see agregarUbicaciones()     
     * @version : 1.0
     */
    public void agregarUbicacion()throws SQLException{
        ubDAO.agregarUbicacion();
    }
    
    /**
     * Metodo: listarUbicaciones, obtiene todas las ubicaciones
     * @autor : Ing. Jesus Cuestas
     * @see listarUbicaciones()     
     * @version : 1.0
     */
    public void listarUbicaciones()throws SQLException{
        ubDAO.listarUbicaciones();
    }
    
    /**
     * Metodo: modificarUbicacion, actualiza una ubicacion
     * @autor : Ing. Jesus Cuestas
     * @see modificarUbicaciones()     
     * @version : 1.0
     */
    public void modificarUbicacion()throws SQLException{
        ubDAO.modificarUbicacion();
    }
    
    /**
     * Metodo: anularUbicacion, anula un registro
     * @autor : Ing. Jesus Cuestas
     * @see anularUbicaciones()     
     * @version : 1.0
     */
    public void anularUbicacion()throws SQLException{
        ubDAO.anularUbicacion();
    }
    
    /**
     * Metodo: existeUbicacionAnulada, verifica si una ubicacion esta anulada
     * @autor : Ing. Jesus Cuestas
     * @see existeUbicacionAnulada(String cod, String cia)     
     * @version : 1.0
     */
    public boolean existeUbicacionAnulada(String cod, String cia)throws SQLException{
        return ubDAO.existeUbicacionAnulada(cod,  cia);
    }
    
    /**
     * Metodo: existeUbicacion, verifica la existencia de la ubicacion
     * @autor : Ing. Jesus Cuestas
     * @see existeUbicacion()     
     * @version : 1.0
     */
    public boolean existeUbicacion (String cod, String cia)throws SQLException{
        return ubDAO.existeUbicacion(cod, cia);
    }
    
    /**
     * Metodo: consultarUbicaciones, registra una nueva ubicacion 
     * @autor : Ing. Jesus Cuestas
     * @see consultarUbicaciones()     
     * @version : 1.0
     */
    public void consultarUbicaciones()throws SQLException{
        ubDAO.consultarUbicaciones();
    }
    
    /**
     * Metodo: buscarUbicacion, busca una ubicacion dado un codigo
     * @autor : Ing. Jesus Cuestas
     * @see buscarUbicacion(String cod, String cia)
     * @version : 1.0
     */
    public void buscarUbicacion(String cod, String cia)throws SQLException{
        ubDAO.buscarUbicacion(cod, cia);
    }
        
    /**
     * Metodo: listarUbicacionesClientes, permite listar las ubicaciones de los clientes
     * @autor : Ing. Jose De La Rosa
     * @see listarUbicacionesClientes - UbicacionDAO    
     * @version : 1.0
     */
    public void listarUbicacionesClientes ( String remesa )throws SQLException{
        try{
            ubDAO.listarUbicacionesClientes (remesa);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }    
}
