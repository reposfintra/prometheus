/***************************************
    * Nombre Clase ............. ReportePlanillasViaServices.java
    * Descripci�n  .. . . . . .  Generamos archivos planillas por via
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  09/08/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/




package com.tsp.operation.model.services;




import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.ReportePlanillasViaDAO;





public class ReportePlanillasViaServices {
    
    private  ReportePlanillasViaDAO   ReportePlanillasViaDataAccess;
    private  List                     lista;
    
    private  List                     clientes;
    private  List                     origenes;
    private  List                     destino;
    private  List                     vias;
    
    
    
    
    public ReportePlanillasViaServices() {
           ReportePlanillasViaDataAccess  =  new  ReportePlanillasViaDAO();
           lista                          =  new  LinkedList();
           
           origenes                       =  new  LinkedList();
           destino                        =  new  LinkedList();
           vias                           =  new  LinkedList();
           clientes                       =  new  LinkedList();
    }
    
    
    
    
    
    private void init(){
        lista   =  new  LinkedList();
    }
    
    
    
    
    
    
    
    
    
     /**
     * M�todo que carga los clientes
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void LoadClientes()throws Exception{
        try{
            this.clientes  = this.ReportePlanillasViaDataAccess.getClientes();
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
    
    /**
     * M�todo que carga los origenes
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void LoadOrigen()throws Exception{
        try{
            this.origenes = this.ReportePlanillasViaDataAccess.getOrigenes();
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
    /**
     * M�todo que carga los destino desde ese origen
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void LoadDestino(String origen)throws Exception{
        try{
            this.destino  = this.ReportePlanillasViaDataAccess.getDestino(origen);
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
    /**
     * M�todo que carga las posibles vias  del origen al destino
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void LoadVIAS(String origen, String destino)throws Exception{
        try{
            this.vias =  this.ReportePlanillasViaDataAccess.getVIAS(origen, destino);
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
    
    
    
    
    /**
     * M�todo que busca las planillas
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void searchPlanillas( String distrito, String cliente, String fechaIni, String fechaFin, String origen, String destino, String via) throws Exception{
       try{
           this.lista  =  this.ReportePlanillasViaDataAccess.getPlanillas(distrito, cliente, fechaIni, fechaFin, origen, destino, via);
           
       }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
        
    }
    
    
    
    
    /**
     * M�todo que busca PC de la via
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public List  getPCs( String via ) throws Exception{
       List pcs =  new LinkedList();
       try{
           pcs =  this.ReportePlanillasViaDataAccess.getPC(via);
           
       }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
       return pcs; 
    }
    
    
    
    
    
    /**
     * M�todo que busca nombre de la ciudad
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String getNombreCiudad(String code)throws Exception{
        String name = code;
        try{
           Hashtable infoPC  = this.ReportePlanillasViaDataAccess.getInfoCiudad(code);
           if( infoPC!=null )                           
                name =  (String) infoPC.get("nomciu");
           
        }catch(Exception e){
           throw new Exception( e.getMessage() );
        }
        return name;
    }
    
    
    
    
    
    
    
    
    /**
     * M�todo que complementa info del viaje
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public Hashtable  complementar(Hashtable oc, List listaPc )throws Exception{
        try{
              
            String separador  = ",";
            String horaHoy    = Util.getFechaActual_String(6).replaceAll("/","-");
            
         // FECHAS REPORTES EN PC:
             String   po_no         = (String)oc.get("oc");
             List    fechasReportes = new LinkedList();
             
             for(int i=0;i<listaPc.size();i++){
                 Hashtable  pc       = (Hashtable) listaPc.get(i);
                 String     puesto   = (String) pc.get("puesto");
                 String     fecha    = this.ReportePlanillasViaDataAccess.getFechaReporte(po_no, puesto);
                 fechasReportes.add( fecha );
             }
             oc.put("fechasReportes",  fechasReportes );
             
             
           
             
             
        // REPORTES ASOCIADOS:
           
           String  observacion       = "";
           String  sitiosPernotadas  = "";
           String  fechaPernotada    = "";
           String  fechaInicio       = "";
           String  fechaParqueos     = "";
           String  salidaParqueos    = "";
           String  difPernotadas     = "00:00";
           String  inicio_demora     = "";
           String  fin_demora        = "";
           String  estado_demora     = "";
           String  difDemoras        = "";
           String  tEfectivo         = "";
           String  tEfec_demora      = "";
           String  descargue         = "";
           String  pcMontelibano     = "00:00";
           String  novedad           = "";
           
             
           
           List  listReportes  =  this.ReportePlanillasViaDataAccess.getListReportes(po_no);
           for(int i=0;i<listReportes.size();i++){
                 Hashtable infoPC  =  (Hashtable)listReportes.get(i);
                 
                 String    fecha     =  (String)infoPC.get("fecha");
                 String    obs       =  (String)infoPC.get("observacion");
                 String    pc        =  (String)infoPC.get("ciudad");
                 String    descPC    =  (String)infoPC.get("nombre");
                 String    tipo      =  (String)infoPC.get("tipo");
                 String    code      =  (String)infoPC.get("dato");
                 String    programa  =  (String)infoPC.get("programa");
                 
                 
                
                 
              // Pernoctaderos
                 if( tipo.equals("13") ) { 
                     sitiosPernotadas += descPC + separador;
                     fechaPernotada   += fecha  + separador;
                     difPernotadas     = "";
                     int   sw          = 0;
                     for(int j=i+1; j<listReportes.size(); j++){
                           Hashtable infoPC_  =  (Hashtable)listReportes.get(j); 
                           String    code_    =  (String)infoPC_.get("dato"); 
                           String    tipo_    =  (String)infoPC_.get("tipo");
                           String    fecha_   =  (String)infoPC_.get("fecha");
                           if( (!code.equals("DET")  &&  !code.equals("O"))  ||  tipo_.equals("EVIA") ||  tipo_.equals("REI")){
                                fechaInicio   += fecha_ + separador;
                                difPernotadas += this.ReportePlanillasViaDataAccess.getDiferenciaFormatoHoras( fecha_  , fecha) + separador;
                                break;    
                           }
                     } 
                     if(difPernotadas.equals("") )
                        difPernotadas = this.ReportePlanillasViaDataAccess.getDiferenciaFormatoHoras( horaHoy  ,  fecha ) + separador; 
                         
                 }
                 
                 
                                  
                 if(  programa.equals("DETENCION") ) {
                     
                     if( !novedad.equals("") )
                          novedad  += separador;
                     novedad  +=  (String)infoPC.get("novedad"); 
                     
                     // Observacion :
                     if ( !observacion.equals("") )
                           observacion += separador;
                     observacion     +=  obs;  
                     
                 }
                 
                 
                 
                 
               // Parqueadero
                 if( tipo.equals("PAR") ) { 
                     fechaParqueos   += fecha  + separador;
                     for(int j=i+1; j<listReportes.size(); j++){
                           Hashtable infoPC_  =  (Hashtable)listReportes.get(j); 
                           String    code_    =  (String)infoPC_.get("dato"); 
                           String    tipo_    =  (String)infoPC_.get("tipo");
                           String    fecha_   =  (String)infoPC_.get("fecha");
                           if( (!code.equals("DET")  &&  !code.equals("O"))  ||  tipo_.equals("EVIA") ){
                                salidaParqueos  += fecha_ + separador;
                                break;    
                           }
                     } 
                 }
                 
           }
           
           oc.put("sitiosPernotadas",   sitiosPernotadas  );
           oc.put("fechaPernotada",     fechaPernotada    );
           oc.put("fechaInicio",        fechaInicio       );
           oc.put("observacion",        observacion       );           
           oc.put("fechaParqueos",      fechaParqueos     );
           oc.put("salidaParqueos",     salidaParqueos    );
           oc.put("difPernotadas",      difPernotadas     );
           oc.put("novedad",            novedad           );  
           
           
         // DEMORAS
           List  demoras         =  this.ReportePlanillasViaDataAccess.getDemoras(po_no);
           for(int i=0;i<demoras.size();i++){
               Hashtable infoPC   =  (Hashtable)   demoras.get(i);
               inicio_demora     +=  (String)infoPC.get("fechademora");
               fin_demora        +=  (String)infoPC.get("fecfindem");
               estado_demora     +=  (String)infoPC.get("estado");
               if( estado_demora.equals("FINALIZADA") )
                   difDemoras        +=   this.ReportePlanillasViaDataAccess.getDiferenciaFormatoHoras( fin_demora, inicio_demora ) + separador;
           }
           
           if( difDemoras.equals("") )  difDemoras = "00:00";
           
           oc.put("inicio_demora",    inicio_demora     );
           oc.put("fin_demora",       fin_demora        );
           oc.put("estado_demora",    estado_demora     );
           oc.put("difDemoras",       difDemoras        );
             
            
           
           
           
           
         // FORMULAS 
         
            String   tViaje =  (String)oc.get("tViaje_HHMM");
            String[] t1     =  difPernotadas.split(separador);
            for(int i=0; i<t1.length;i++){
                String t   = t1[i];
            // 1.1  tEfectivo
                    String   diftEfectivo =  this.ReportePlanillasViaDataAccess.getDiferenciaHoras( tViaje, t );
                    if( !tEfectivo.equals("") )
                         tEfectivo += separador;
                    tEfectivo  +=  diftEfectivo;     
                
                    String[] t2      =  difDemoras.split(separador);
                    for(int j=0; j<t2.length;j++){
                        String tt    = t2[j];
                      //1.2  tEfec_demora
                             if ( !tEfec_demora.equals("")  )
                                   tEfec_demora += separador;
                             tEfec_demora += this.ReportePlanillasViaDataAccess.getDiferenciaHoras( diftEfectivo, tt );
                    }
                
            }
            
            
            // 1.3  Descargue
            String entrega = (String)oc.get("entrega");
            String[] t3      =  fechaParqueos.split(separador);
            for(int i=0; i<t3.length;i++){
                String t    = t3[i];
                if(!entrega.equals("")  &&  !t.equals("")  )
                    descargue  +=   this.ReportePlanillasViaDataAccess.getDiferencia( entrega, t, "DD HH24:MI") + separador;
            }
            if( descargue.equals("") )  descargue="00:00";
            
            
          
       // Permanecia Montelibano:
           String  fechaReprtPlaneta     = this.ReportePlanillasViaDataAccess.getFechaReporte(po_no, "PR");
           String  fechaReprtMontelibano = this.ReportePlanillasViaDataAccess.getFechaReporte(po_no, "WM");
           if( !fechaReprtPlaneta.equals("") && !fechaReprtMontelibano.equals("")  ){
                pcMontelibano                 = this.ReportePlanillasViaDataAccess.getDiferenciaFormatoHoras( fechaReprtPlaneta, fechaReprtMontelibano);
                pcMontelibano                 = this.ReportePlanillasViaDataAccess.getDiferenciaHoras( pcMontelibano, "01:25");            
           }
           oc.put("pcMontelibano",  pcMontelibano        );           
           
            
            
           oc.put("tEfectivo",          tEfectivo        );
           oc.put("tEfec_demora",       tEfec_demora     );
           oc.put("descargue",          descargue        );
           
           
        }catch(Exception e){
            throw new Exception( " complementar "+e.getMessage() );
        }        
        return oc;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * Getter for property lista.
     * @return Value of property lista.
     */
    public java.util.List getLista() {
        return lista;
    }    
    
    /**
     * Setter for property lista.
     * @param lista New value of property lista.
     */
    public void setLista(java.util.List lista) {
        this.lista = lista;
    }    
    
    /**
     * Getter for property origenes.
     * @return Value of property origenes.
     */
    public java.util.List getOrigenes() {
        return origenes;
    }    
    
    /**
     * Setter for property origenes.
     * @param origenes New value of property origenes.
     */
    public void setOrigenes(java.util.List origenes) {
        this.origenes = origenes;
    }    
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.util.List getDestino() {
        return destino;
    }    
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino(java.util.List destino) {
        this.destino = destino;
    }    
    
    /**
     * Getter for property vias.
     * @return Value of property vias.
     */
    public java.util.List getVias() {
        return vias;
    }    
    
    /**
     * Setter for property vias.
     * @param vias New value of property vias.
     */
    public void setVias(java.util.List vias) {
        this.vias = vias;
    }    
    
    /**
     * Getter for property clientes.
     * @return Value of property clientes.
     */
    public java.util.List getClientes() {
        return clientes;
    }    
    
    /**
     * Setter for property clientes.
     * @param clientes New value of property clientes.
     */
    public void setClientes(java.util.List clientes) {
        this.clientes = clientes;
    }    
    
    
    
}
