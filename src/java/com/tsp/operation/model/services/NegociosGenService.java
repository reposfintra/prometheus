
/**
* Autor : Ing. Roberto Rocha P..
* Date  : 10 de Julio de 2007
* Copyrigth Notice : Fintravalores S.A. S.A
* Version 1.0
-->
<%--
-@(#)
--Descripcion : Services que maneja los avales de Fenalco
**/

package com.tsp.operation.model.services;

import com.google.gson.JsonObject;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.*;
import com.tsp.util.Util;
import com.tsp.operation.model.EgresoService;
import com.tsp.operation.model.TransaccionService;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NegociosGenService {
    
    private NegociosDAO list;
    private String a;
    private Aval_Fenalco Aval;
    private Vector Avales;
    private TreeMap Cust;
    //private Avales_FenalcoDAO AvalesData; 
    private String ckestS=null;        
    private String ckdateS=null;
    private String cknitaS=null;
    private String cknomaS=null;
    private String cknitcS=null;
    private String cknomcS=null;
    
    private String cknitcodS=null;
    private String cknomcodS=null;

    private Vector Datos;
    private Vector Datos2;
    
    private String ckfapS=null;
    private String ckfdeS=null;
    
     //variable Global.
    private double valor_cxp=0;
    
    public NegociosGenService() {
        list= new NegociosDAO();
    }
    
    public NegociosGenService(String dataBaseName) {
        list= new NegociosDAO(dataBaseName);
    }


 public void listado(Negocios neg,String numero_solicitud,String ciclo,String numero_form, Convenio convenio, NegocioTrazabilidad negtraza) throws java.sql.SQLException{
        //System.out.println( "Entro al listado de negocios"+neg.getCod_cli() );
        list.NegociosSave(neg,numero_solicitud,ciclo,numero_form, convenio,negtraza);

    }
    /**
     * Atualiza el negocio cuando ya ha ocurrido una liquidacion.
     * @autor Iris Vargas
     * @throws SQLException
     */
    public void negocioUpdate(Negocios neg,String numero_solicitud,String ciclo,String numero_form, Convenio convenio, NegocioTrazabilidad negtraza) throws java.sql.SQLException{
        list.NegociosUpdate(neg,numero_solicitud,ciclo,numero_form,convenio,negtraza);

    }
    
    public void negocioUpdateDS(String cod_neg,String actividad,String estado)throws java.sql.SQLException{
        try {
            list.NegociosUpdateDS(cod_neg, actividad, estado);
        } catch (Exception ex) {
            Logger.getLogger(NegociosGenService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
  
  
   public void listneg(String ckest,String est,String ckdate,String fi,String ff,String ckfap,String fi1,String ff1,String ckfde,String fi2,String ff2,String cknita, String nita,String cknoma,String noma,String cknitc,String nitc,String cknomc,String nomc,String cknitcod,String nitcod,String cknomcod,String nomcod,String ckcodxc,String codxc,String vista)throws ServletException, InformationException,Exception
    {
        ckestS      = ckest;
        ckdateS     = ckdate;
        cknitaS     = cknita;
        cknomaS     = cknoma;
        cknitcS     = cknitc;
        cknomcS     = cknomc;
        cknitcodS   = cknitcod;
        cknomcodS   = cknomcod;
        ckfapS      = ckfap;
        ckfdeS      = ckfde;
        try{
            list.ListadoDatos(ckestS,est,ckdateS,fi,ff,ckfap,fi1,ff1,ckfde,fi2,ff2,cknitaS,nita,cknomaS,noma,cknitcS,nitc,cknomcS,nomc,cknitcodS,nitcod,cknomcodS,nomcod,ckcodxc,codxc,vista);

         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
    public void listneg2(String ckest,String est,String ckdate,String fi,String ff,String ckfap,String fi1,String ff1,String ckfde,String fi2,String ff2,String cknitc,String nitc,String cknomc,String nomc,String cknitcod,String nitcod,String cknomcod,String nomcod,String ckcodxc,String user, String vista)throws ServletException, InformationException,Exception
    {
        ckestS      = ckest;       
        ckdateS     = ckdate;
        ckfapS      = ckfap;
        ckfdeS      = ckfde;
        cknitcS     = cknitc;
        cknomcS     = cknomc;
        cknitcodS   = cknitcod;
        cknomcodS   = cknomcod;

        try{ 
            list.ListadoDatos2(ckestS,est,ckdateS,fi,ff,ckfap,fi1,ff1,ckfde,fi2,ff2,cknitcS,nitc,cknomcS,nomc,cknitcodS,nitcod,cknomcodS,nomcod,user, vista);
            
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }

    public void denny(String ob,String cod,String est)throws ServletException, InformationException,Exception
    {
        try{ 
            list.dennyneg(ob,cod,est);  
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    public void habReliquidar(String cod,String tasa)throws java.sql.SQLException, Exception
    {
        try{
            list.habReliquidar(cod,tasa);
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
     public void admit(String user,String ob,String cod,String nuav,String bcoch)throws ServletException, InformationException,Exception
    {
        try{ 
            list.admitneg(user,ob,cod,nuav,bcoch); 
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
     
    public String admit(String user,String ob,String cod,String nuav,String bcoch, String numeroCuentaCheque, String estado_neg)throws ServletException, InformationException,Exception
    {   String sql="";
        try{ 
            sql=list.admitneg(user,ob,cod,nuav,bcoch, numeroCuentaCheque,estado_neg);//Acepta el negocio
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
        return sql;
    }
     
     public String factsave(BeanGeneral bg)throws ServletException, InformationException,Exception
    {   String sql="";
        try{ 
            sql=list.fact(bg);
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
        return sql;
    }
     
     public void listb(String opcion)throws ServletException, InformationException,Exception
    {
        try{ 
            list.ListadoB(opcion);  
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
     
     ///listado de negocios a tranferir
     public void listt(String[] nits)throws ServletException, InformationException,Exception
    {
        try{ 
            list.ListadoT(nits);
         }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
     
      ///listado de negocios a tranferir
     public void listt(String[] nits, String fechaInicio, String fechaFin)throws ServletException, InformationException,Exception
    {
        try{ 
            list.ListadoT(nits, fechaInicio, fechaFin);
         }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
     
    public String relsave(BeanGeneral bg)throws ServletException, InformationException,Exception
    {
        try{ 
            return list.rel(bg);
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
    public java.util.Vector getListNeg()
    {
        return  list.getDatos();
    }
    
     public java.util.Vector getListNeg2()
    {
        return  list.getDatos2();
    }
     
     public java.util.TreeMap getClientes()throws ServletException, InformationException,Exception
    {
        return  list.getCust();
    }
    
    public java.util.TreeMap getProv()throws ServletException, InformationException,Exception
    {
        return  list.getProv();
    }
    
    public boolean existe(BeanGeneral bg,String op)throws ServletException, InformationException,Exception
    {
        boolean sw=false;
        try{ 
            sw=list.exists(bg,op);
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
        return sw;
    }
    
     public void delete(BeanGeneral bg)throws ServletException, InformationException,Exception
    {
        try{ 
            list.deleteCA(bg);
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
     
      public void deleteU(BeanGeneral bg)throws ServletException, InformationException,Exception
    {
        try{ 
            list.deleteCAU(bg);
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
     public void uptranf(String cod)throws ServletException, InformationException,Exception
    {
        try{ 
            list.transfneg(cod);  
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
     
    public String bcosp(String codn)throws ServletException, InformationException,Exception
    {
        String res="";
        try{ 
            res=list.bcop(codn);  
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
        return res;
    }
    
    public String bcomis(String codcom)throws ServletException, InformationException,Exception
    {
        String comi="";
        try{ 
            comi=list.bcom(codcom);  
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
        return comi;
    }
    
     public void actcom(String codn,String com,String nomb,String codb)throws ServletException, InformationException,Exception
    {
        try{ 
            list.actcomD(codn,com,nomb,codb);  
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
     
     public Vector listafil()throws ServletException, InformationException,Exception
    {
       
        try{ 
            list.lafilDAO();
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
        return list.getl();
    }
     
     public Vector listcli(String a)throws ServletException, InformationException,Exception
    {
       
        try{ 
            list.lcliDAO(a);
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
        
        return list.getl();
    }
    
     
     public void LDim(String a,String b)throws Exception
     {
         Datos = new Vector();
         try{ 
            Datos=list.LDim(a,b);   
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }

     }
     
     public Vector VectIn()
     {
         return Datos;
     }
     
     public void LCod(String a,String b)throws Exception
     {
         Datos2 = new Vector();
         try{ 
            Datos2=list.LCod(a,b);  
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }

     }
     
      public Vector VectCod()
     {
         return Datos2;
     }
     
    public void actaux()throws Exception
     {
         try{ 
            list.ActAux();
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
     }
    
    public String newfact(String a)throws Exception
     {
         String b="";
         try{ 
            b=list.newfactDAO(a);   
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
         return b;

     }
    //codigo_factura,convenio, fecha_inicio, fecha_fi, cliente
     public String newfact2(String a, String convenio, String b,String c, String cliente, Usuario usuario)throws Exception
     {
         String d="";
         try{
            d=list.newfactDAO2(a,convenio, b,c, cliente, usuario);
             }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
         return d;

     }
    
    public void saveEg(String[] codn,String a)throws ServletException, InformationException,Exception
    {
        try{ 
            for(int i=0;i<codn.length;i++)
            {
                list.seveEGDAO(codn[i],a);    
            }
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
     public double valor(String a,String b,String c)throws Exception
     {
        double res=0;
         try{ 
            res=list.Valor(a,b,c);  
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
        return res;
     }
     
    public ArrayList<String> subled(BeanGeneral bg)throws ServletException, InformationException,Exception
    {   ArrayList<String> sql;
        try{ 
            sql=list.subled(bg); 
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
        return sql;
    }
      
     public String Conse(BeanGeneral bg)throws Exception
     {
        String res="";
         try{ 
            res=list.ConseDAO(bg); 
         }
            catch(SQLException e){
             res="Error en la Insercion";
        }
        return res;
     }
      
     
     public void letras(BeanGeneral bg)throws Exception
     {
         try{ 
            list.Letras(bg); 
         }
            catch(SQLException e){ 
        }
     }
     
     public BeanGeneral RetLet()
     {
         return list.RetLet();
     }
     
     public BeanGeneral mailinfo(String a)throws Exception
     {
        BeanGeneral res= new BeanGeneral();
         try{ 
            res=list.MailDAO(a);   
         }
            catch(SQLException e){
             throw new SQLException (e.getMessage ());
        }
        return res;
     }
   public ArrayList<String> CCfactsave(BeanGeneral bg)throws ServletException, InformationException,Exception
    {
        try{ 
            return list.CCfact(bg);
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
   public java.util.TreeMap GetBcos()throws ServletException, InformationException,Exception
    {
        return  list.GetBankFid();
    }
   
   public BeanGeneral getReporteFenalcoService(String noFactura) throws Exception{
       //Retorna el BeanGeneral con el reporte de la factura devuelta
       return list.getReporteFenalcoDAO(noFactura);
   }
   
    public Ingreso getIngreso(Ingreso ingreso) throws Exception
    {
        return  list.getIngreso(ingreso);
    }

   public ArrayList<String> newfactRemix(String a, String documento)throws Exception
     {
         ArrayList<String> b= new ArrayList<String>();
         try{
            b=list.newfactDAORemix(a, documento);
         }
         catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
         return b;

     }
   
   public void setCheques(ArrayList cheques) throws Exception {
       list.setCheques(cheques);
   }
   
   public ArrayList obtainDocsAceptados(String codnegx) throws Exception {
       return list.obtainDocsAceptados(codnegx) ;
   }
   
   ////agregado por Miguel Altamiranda V
    public void ingresar_ordenes2(String exno_orden,String exnum_aval,String no_orden,String nomafiliado, String ciudad, String nom_cli,String cod_cli,String ch,String no_cta,String cod_banco,String valor,String plaza, String fecha, String c_fintra, String a_favor_de, String num_aval, String c_custodia, String dirbanco, String fecha_operacion) throws java.sql.SQLException{
        list.ingresar_ordenes2(exno_orden,exnum_aval,no_orden,nomafiliado, ciudad, nom_cli, cod_cli, ch, no_cta, cod_banco, valor, plaza, fecha, c_fintra, a_favor_de, num_aval, c_custodia, dirbanco, fecha_operacion);
    }   

    public String getno_orden(String numaval) throws java.sql.SQLException{
        return list.getno_orden(numaval);
    }


    public void ingresar_ordenes(String no_orden,String nomafiliado, String ciudad, String nom_cli,String cod_cli,String ch,String no_cta,String cod_banco,String valor,String plaza, String fecha, String c_fintra, String a_favor_de, String num_aval, String c_custodia, String dirbanco, String cod_neg, String fecha_operacion ) throws java.sql.SQLException{
        list.ingresar_ordenes(no_orden,nomafiliado, ciudad, nom_cli, cod_cli, ch, no_cta, cod_banco, valor, plaza, fecha, c_fintra, a_favor_de, num_aval, c_custodia, dirbanco, cod_neg, fecha_operacion);
    }   

    public ArrayList getAfiliado(String nombre) throws java.sql.SQLException{
        //System.out.println( "Entro al listado de negocios"+neg.getCod_cli() );
        return list.getAfiliado(nombre);
    }
    public ArrayList getCtasFintra()throws java.sql.SQLException{
        return list.getCtasFintra();
    }


    public void admit(String estado_neg,String cod)throws ServletException, InformationException,Exception//agregado por Miguel Altamiranda
    {
        try{ 
            list.admitneg(estado_neg,cod);//Acepta el negocio 
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }

    public ArrayList getCiudades()throws SQLException
    {   return list.getCiudades();
    }

    public ArrayList getOperaciones(String fechai, String fechaf)throws SQLException
    {   return list.getOperaciones(fechai,fechaf);
    }

    public ArrayList getdatosnegocio(String codn)throws SQLException
    {   return list.getdatosnegocio(codn);
    }
    
    public void admit(String user,String ob,String cod,String estado_neg)throws ServletException, InformationException,Exception
    {
        try{ 
            list.admitneg(user,ob,cod,estado_neg);//Acepta el negocio 
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
    public BeanGeneral letras(String negocio)throws Exception{ 
         try{
             BeanGeneral bg = list.newLetras(negocio);
             RMCantidadEnLetras c = new RMCantidadEnLetras();
             String[] a;
             a = c.getTexto(Double.valueOf(bg.getValor_03()).doubleValue());
             String res = "";
             for(int i=0;i<a.length;i++){
                 res = res + ((String)a[i]).replace("-"," ");
             }
             bg.setValor_14(res);//Total a Pagar en Letras

             //--Inicio Obtener La fecha de vencimiento del ultimo documento
             String  fechai = bg.getValor_02();//Fecha de negocio
             int dia = Integer.parseInt(fechai.substring(8,10));
             int mes = Integer.parseInt(fechai.substring(5,7));
             int ano = Integer.parseInt(fechai.substring(0,4));
             String fmost = "";
             String mesmostrable = "";
             int mesmostrabletem, anomostrable = 0;
             java.util.Calendar fechaTemp = Util.crearCalendarDate(fechai);
             for(int i=0;i<Integer.valueOf(bg.getValor_04()).intValue();i++){
                 fechaTemp = Util.TraerFecha(i+1, dia, mes, ano, "30");
                 fmost     = Util.crearStringFechaDateLetraRemix(fechaTemp);
                 mesmostrabletem = (Integer.valueOf(fmost.substring(3,5)).intValue()%12)+1;
		 mesmostrable="";
		 if (mesmostrabletem<10){
                     mesmostrable="0"+mesmostrabletem;
		 }else{
                     mesmostrable=""+mesmostrabletem;
		 }
		 anomostrable=Integer.valueOf(fmost.substring(6,10)).intValue();
		 if (mesmostrable.equals("01") ){
                     anomostrable=anomostrable+1;
		 }
                 fechaTemp = Util.crearCalendarDate(fechai);
             }
             bg.setValor_15(anomostrable+"-"+mesmostrable+"-"+fmost.substring(0,2));//Fecha Vencimiento ultimo Documento
             //--Fin
             
            return bg; 
         }catch(SQLException e){ 
             throw new SQLException (e.getMessage ());
         }
     }
  //--Add 2009-Junio-01
     
    /**
     * Actualiza el valor de la comision de un determinado negocio
     * @param codn Codigo del negocio
     * @param com comision
     * @param nomb Banco
     * @param codb sucursal
     * @autor tmolina
     * @throws Exception
     */
     public String actcomString(String cxp,String codn,String com,String nomb,String codb)throws ServletException, InformationException,Exception{
         try{
             return list.actcomDString(cxp,codn,com,nomb,codb);
         }catch(SQLException e){
             throw new SQLException (e.getMessage ());
         }
     }
     
    /**
     * Atualiza el negocio para indicar que fue transferido.
     * @param cod Codigo del Negocio
     * @autor tmolina 
     * @throws SQLException
     */
     public String uptranfString(String cod)throws ServletException, InformationException,Exception{
         try{
             return list.transfnegString(cod);  
         }catch(SQLException e){
             throw new SQLException (e.getMessage ());
         }
     }
    
    /**
     * Obtiene informaci�n referente a un negocio para la elaboraci�n de un e-mail
     * @param a Codigo del Negocio
     * @autor tmolina 
     * @throws SQLException
     */
     public BeanGeneral mailinfo2(String a)throws Exception{
         BeanGeneral res= new BeanGeneral();
         try{
             res=list.MailDAO2(a);   
         }catch(SQLException e){
             throw new SQLException (e.getMessage ());
         }
         return res;
     }
     

       /**
        * Crea el egreso del negocio transferido y actualiaza la cxp.
        * @param codn Codigo del Negocio
        * @param a Usuario en sesion
        * @param banco Banco de transferencia
        * @param sucursal Sucuarsal trasnferencia
        * @param comision Comision bancaria
        * @autor tmolina
        * @moficado ivargas
        * @throws SQLException
        */
     public ArrayList saveEGDAOString(String cxp,String codn, Usuario a, String banco, String sucursal, String comision)throws ServletException, InformationException,Exception{
         try{
             ArrayList saveEGDAOString = list.saveEGDAOString(cxp,codn, a, banco, sucursal, comision);
             this.setValor_cxp(list.getValor_cxp());
             return saveEGDAOString;
         }catch(SQLException e){
             throw new SQLException (e.getMessage ());
         }
     }

     public ArrayList getDatos(String tabla, String negocio)throws Exception
     {  return  list.getDatos(tabla, negocio);
     }

     public String getDatos_pagare(String negocio)throws Exception
     {  return  list.getDatos_pagare(negocio);
     }

     public void setCodeudor(String codneg,String idcodeudor,String idendoso,String domiciliopago,String ciudadotorgamiento,String domi2,String afavor)throws Exception
     {  list.setCodeudor(codneg, idcodeudor, idendoso, domiciliopago, ciudadotorgamiento, domi2, afavor);
     }

     public String UpCP(String a) throws Exception{
        return list.UpCP(a);
     }

    /**
     * Busca los datos de un negocio
     * @param codNegocio codigo del negocio a buscar
     * @return bean Negocios con la informacion encontrada
     * @throws Exception
     */
    public Negocios buscarNegocio(String codNegocio) throws Exception {
        return list.buscarNegocio(codNegocio);
    }

    /**
     * Busca los detalles de las cuotas de un negocio
     * @param codNegocio codigo del negocio a buscar
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList buscarDetallesNegocio(String codNegocio) throws Exception {
        return list.buscarDetallesNegocio(codNegocio);
    }

    public ArrayList<String> generarDocumentosNegocio(BeanGeneral bg, Usuario usuario) throws Exception {
        ArrayList<String> listSql = new ArrayList<String>();

        //Obtener la informacion del convenio
        GestionConveniosDAO convdao = new GestionConveniosDAO(usuario.getBd());
        Convenio convenio = convdao.buscar_convenio(usuario.getBd(), bg.getValor_17());

       String[] datosCXC = list.buscarCuentasCXC(bg.getValor_17(), bg.getValor_18(), bg.getValor_16());

        //Guardar prefijos y cuentas
        bg.setValor_20(datosCXC[0]); //Prefijo
        bg.setValor_21(datosCXC[1]); //Cuenta CXC
        bg.setValor_22(datosCXC[2]); //HC CXC
        bg.setValor_23(convenio.getHc_cxp()); //HC CXP
        bg.setValor_24(convenio.getCuenta_cxp()); //Cuenta CXP
        
        String documento = list.UpCP(convenio.getPrefijo_cxp());//obtiene el numero de documento para la CXP
        int numCuotas = Integer.parseInt(bg.getValor_02()); //numero de cuotas del negocio
        if(convenio.getTipo().equals("Microcredito")){

            //Ingresa la CXP
            listSql.add(list.ingresarCXPMicrocredito(bg, documento, convenio));
            listSql.add(list.ingresarDetalleCXPMicrocredito(bg, documento, convenio));
            listSql.add(list.ingresarDetalleCXPMicrocreditoItem2(bg, documento, convenio));
            if(list.aplicarDeduccionFianza(bg.getValor_06().trim())){                
                String documentoFianza = list.UpCPFianza(convenio.getPrefijo_cxp_fianza());//obtiene el numero de documento para la CXP
                String[] datosCXPFianza = list.buscarCuentasFianza(convenio.getPrefijo_cxp_fianza(),Integer.parseInt(convenio.getId_convenio()));
                String nitEmpresaFianza = list.obtenerProveedorFianza(convenio.getId_convenio(),Integer.parseInt(bg.getValor_02()));
                double vlr_estudio=list.valorCentral("VLRCENTRAL", "MICRO");
                double vlr_fianza = obtenerValorFianza(bg.getValor_17(),Double.parseDouble(bg.getValor_29()),Integer.parseInt(bg.getValor_02()),bg.getValor_30(),Integer.parseInt(bg.getValor_34()),Integer.parseInt(bg.getValor_35()),bg.getValor_33());
                double vlr_iva_fianza = list.obtenerValorIVAFianza(bg.getValor_17(),Double.parseDouble(bg.getValor_29()),Integer.parseInt(bg.getValor_02()));
                double vd = Double.parseDouble(bg.getValor_29()) - vlr_estudio;// - vlr_fianza; se comenta ya que se capitaliza el aval 
//                listSql.add(list.ingresarDetalleCXPMicrocreditoItem3(bg, documento,vlr_fianza,nitEmpresaFianza));
//                listSql.add(list.ingresarDetalleCXPMicrocreditoItem4(bg, documento,vlr_fianza,nitEmpresaFianza));
                listSql.add(list.ingresarCabeceraCXPMicrocreditoFianza(bg, documentoFianza, convenio, datosCXPFianza[0]));
                listSql.add(list.ingresarDetalleCXPMicrocreditoFianza(bg, documentoFianza, convenio, datosCXPFianza[1]));
                listSql.add(list.insertarTablaControlMicrocreditoFianza(bg,documentoFianza, nitEmpresaFianza,convenio.getAgencia(), vd, vlr_fianza, vlr_iva_fianza));   
            }
            
            if (bg.getValor_31().equals("R")){// si politica es 'R' (Retanqueo) se reliza una nota credito por el valor del retanquero a la cxp del cliente
            String valor_retanqueo =bg.getValor_32();
            String serialNc=list.serialNcMicro("NC_MICROCRED");
            listSql.add(list.crearNcRetanqueoMicro(bg,documento,valor_retanqueo,serialNc));             
            listSql.add(list.crearDetalleNcRetanqueoMicro(bg,documento,valor_retanqueo,serialNc)); 
            listSql.add(list.updateCxpMicrocredito(bg,documento,valor_retanqueo));
            }

             if(!convenio.isCat()){
                int secNC = 1;
                if(convenio.isDescuenta_gmf()){
                //nota credito gmf
                    int max_cuota = Integer.parseInt(convenio.getCuota_gmf()); //Numero max de cuota para descontar gmf
                    if(max_cuota>=numCuotas){
                    double valorgmf=list.calcularGMF(bg.getValor_11(), (float)convenio.getPorc_gmf());
                    double valorgmf2=list.calcularGMF(bg.getValor_11(), (float)convenio.getPorc_gmf2());
                    listSql.add(list.ingresarNC_GMF(bg, documento+"-"+secNC, convenio,(valorgmf+valorgmf2) ));
                    listSql.add(list.ingresarDetalleNC_GMF(bg, documento+"-"+secNC, convenio,valorgmf,"1",convenio.getCuenta_gmf() ));
                    listSql.add(list.ingresarDetalleNC_GMF(bg, documento+"-"+secNC, convenio,valorgmf2,"2", convenio.getCuenta_gmf2()));
                    secNC++;
                    }
                }
            }
             
            

        }else{

        
        
        bg.setValor_25(convenio.getCuenta_gmf()); //Cuenta gravamen al movimiento financiero
        bg.setValor_26(convenio.getCuenta_aval()); //Cuenta descuenta aval
        bg.setValor_47((float)convenio.getPorc_gmf()); //porcentaje gravamen al movimiento financiero
        
        int secNC = 1;

        //Ciclo para generar las facturas
        for (int i = 1; i < numCuotas + 1; i++) {
            listSql.add(list.ingresarCXC(bg, datosCXC[0], i));
            listSql.add(list.ingresarDetalleCXC(bg, datosCXC[0], i)); // factura_detalle
        }

        list.UpCP(datosCXC[0]); //Actualiza el numero de la serie para la CXC
        

        //Ingresa la CXP
        listSql.add(list.ingresarCXP(bg, documento, convenio));
        listSql.add(list.ingresarDetalleCXP(bg, documento, convenio));
        

        if(convenio.isDescuenta_gmf()){
            //nota credito gmf
            
            int max_cuota = Integer.parseInt(convenio.getCuota_gmf()); //Numero max de cuota para descontar gmf
            if(max_cuota>=numCuotas){
            double valorgmf=list.calcularGMF(bg.getValor_11(), (float)convenio.getPorc_gmf());
            double valorgmf2=list.calcularGMF(bg.getValor_11(), (float)convenio.getPorc_gmf2());
            listSql.add(list.ingresarNC_GMF(bg, documento+"-"+secNC, convenio,(valorgmf+valorgmf2) ));
            listSql.add(list.ingresarDetalleNC_GMF(bg, documento+"-"+secNC, convenio,valorgmf,"1",convenio.getCuenta_gmf() ));
            listSql.add(list.ingresarDetalleNC_GMF(bg, documento+"-"+secNC, convenio,valorgmf2,"2", convenio.getCuenta_gmf2()));
            secNC++;
            }
        }

        //nota credito descuenta aval
        if(convenio.isDescuenta_aval()){
            listSql.add(list.ingresarNC_Aval(bg, documento+"-"+secNC));
            listSql.add(list.ingresarDetalleNC_Aval(bg, documento+"-"+secNC));
            secNC++;
        }

        //Inserta los ingresos diferidos
        InteresesFenalcoDAO intdao = new InteresesFenalcoDAO(this.list.getDatabaseName());
        //metodo para insertar los ingresos insertarIngFenalco(cod_neg, cod_cli, usuario, documentType, cmc);
        listSql.addAll(intdao.insertarIngFenalco(bg.getValor_06(), bg.getValor_01(), bg.getValor_04(), convenio.getPrefijo_diferidos(), convenio.getHc_diferidos()));

        }
        
        return listSql;
    }

    public ArrayList <Aval> searchAvales(String numAval) throws Exception {
        return list.searchAvales(numAval);
    }

     public String IntermediarioAval(String negocio) throws Exception {
        return list.IntermediarioAval(negocio);
    }
    /* Metodo: updateFacturaDevuelta, permite marcar una factura como devuelta
     * @autor : Ing. Iris Vargas
     * @param :  factura, distrito, usuario
     * @version : 1.0
     */
    public String updateFacturaDevuelta(String factura, String distrito, String usuario) throws SQLException {
        return list.updateFacturaDevuelta(factura, distrito, usuario);
    }

    public void avalarNegocio(String cod,String aval,String usuario)throws java.sql.SQLException, Exception
    {
        try{
            list.avalarNegocio(cod, aval, usuario);
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
 ///temporal jpinedo

       public double Total_pagado( Vector vn )
       {

           double vlr=0;
           Negocios neg = new Negocios();
           for (int i=0;i<vn.size();i++)
           {
               neg = (Negocios)vn.get(i);
               vlr=vlr+neg.getVr_desem();
           }
           return vlr;
    }


        /**jpinedo
     * devuelve un vector con los negocios trasferido en una misma hora filtrado por afiliado
     * @param documento La factura a buscar
     * @return Listado con los datos encontrados
     * @throws Exception Cuando hay un error
     */
    public Vector getGrupoFecDesembolso(Vector vn,String nit,String fecha_desm) throws Exception{
        Vector tem =new Vector();
        try {

            for(int i=0;i<vn.size();i++)
            {
                Negocios  neg = (Negocios)vn.get(i);
                if(neg.getBcod().equals(nit) && neg.getFechatran().substring(0, 18).equals(fecha_desm))
                {
                    tem.add(neg);
                }
                neg=null;
            }


        }
        catch (Exception e) {
            throw new Exception("Error en agurpando negocios  "+e.toString());
        }
        return this.setNegociosVlr(tem);
    }




        /**jpinedo
     * devuelve un vector con los negocios setiando el valor desembolso con el vlr del egreso
     * @param documento La factura a buscar
     * @return
     * @throws Exception Cuando hay un error
     */
    public Vector setNegociosVlr(Vector vn) throws Exception{
        Vector tem =new Vector();
         EgresoService egserv = new EgresoService (this.list.getDatabaseName());
        try {

            for(int i=0;i<vn.size();i++)
            {
                Negocios  neg = (Negocios)vn.get(i);
               neg.setVr_desem(egserv.getVlr(neg.getCod_negocio()));
               tem.add(neg);
               neg=null;
            }


        }
        catch (Exception e) {
            throw new Exception("Error en setiando vlr en engocios negocios  "+e.toString());
        }
        return tem;
    }

public Vector VerNits(Vector negs) throws Exception
        {
            String negocios="";
            for (int i = 0; i < negs.size()-1; i++)
            {
                negocios=negs.get(i)+"','"+negocios;
            }
              negocios=negocios+ negs.get(negs.size()-1);
            return list.VerNits(negocios);
        }




        public Vector VerInfoPago(Vector negs,String nit) throws Exception
        {
            String negocios="";
            for (int i = 0; i < negs.size()-1; i++)
            {
                negocios=negs.get(i)+"','"+negocios;
            }
              negocios=negocios+ negs.get(negs.size()-1);
            return list.VerInfoPago(nit, negocios);
        }
 public boolean  tieneCreditoFintra(String cedula) throws Exception {
         return list.tieneCreditoFintra(cedula);
     }
 
 
 public String avalarNeg(String cod, String aval, String usuario) throws java.sql.SQLException, Exception {

        return list.avalarNeg(cod, aval, usuario);

    }

    public String avalarForm(String cod, String aval, String usuario) throws java.sql.SQLException, Exception {

        return list.avalarForm(cod, aval, usuario);

    }

    public void listneg(String tab, String remp) throws ServletException, InformationException, Exception {

        try {
            list.ListadoDatos(tab, remp);

        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
    
    public boolean permisosFenalco(String usuario, String tabla) {

        boolean sw = false;
        try {
            sw = list.PermisosFenalco(usuario, tabla);
        } catch (Exception ex) {
            Logger.getLogger(NegociosGenService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return sw;

    }
    
     public String permisosRAD(String usuario, String tabla) {

        String sw = "0";
        try {
            sw = list.PermisosRAD(usuario, tabla);
        } catch (Exception ex) {
            Logger.getLogger(NegociosGenService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return sw;

    }

    public ArrayList<DocumentosNegAceptado> getLiquidacion() {
        return list.getLiquidacion();
    }

    public void setLiquidacion(ArrayList<DocumentosNegAceptado> liquidacion) {
        list.setLiquidacion(liquidacion);
    }
    
     public void calcularLiquidacionMicrocredito(Negocios neg , Usuario usuario, Tipo_impuesto impuesto,String renovacion,String numSolc) throws Exception {
        GestionConveniosDAO convenioDao = new GestionConveniosDAO(usuario.getBd());        
        Convenio convenio = convenioDao.buscar_convenio(usuario.getBd(), neg.getId_convenio()+"");
        double valornegocio = neg.getVr_negocio();
        int numCuotas = neg.getNodocs();
        double tasaInteres =Double.parseDouble(neg.getTasa()) / 100;
        double porcCat = neg.getPorcentaje_cat() / 100;
        double vlrCapacitacion = neg.getValor_capacitacion();
        double vlrSeguro = neg.getValor_seguro()/neg.getNodocs();
        double vlrCentral = neg.getValor_central();
        double cuotaManejo = this.getValor_Cuota_Manejo(String.valueOf(neg.getId_convenio()));
        String fechaNegocio = neg.getFechatran()!=null?neg.getFechatran():neg.getFecha_neg();
        String fechaItem =Util.fechaMasDias(neg.getFecha_liquidacion(), Integer.parseInt(neg.getFpago()));
        double interesEA = Math.pow(1 + tasaInteres, 12) - 1;
        FenalcoFintraDAO fendao = new FenalcoFintraDAO(usuario.getBd());
        String cod_negocio=neg.getCod_negocio();
        double capital = 0;
        double saldoinicial = 0;
        double saldofinal = 0;
        double capitalItem = 0;
        double capacitacionItem;
        double valorCuota = 0;
        double interesItem = 0;
        double seguroItem = 0;
        double catItem = 0;
        String codigo=numSolc;
        String fechaAnterior = fechaNegocio;
        String [] fechasliq=new String [numCuotas]  ;

        ArrayList<DocumentosNegAceptado> items = new ArrayList();

        if (neg.getTipo_cuota().equals("CPFCTV")) {
            capital = valornegocio;
            saldoinicial = capital;
            seguroItem = vlrSeguro;
            capacitacionItem = Math.round(vlrCapacitacion / numCuotas);
            capitalItem = Math.round(capital / numCuotas);
             if(convenio.isCat()){
                catItem = Math.round(((porcCat  * valornegocio)* ((impuesto.getPorcentaje1() / 100) + 1))/ numCuotas);
            }
            
            for (int i = 0; i < numCuotas; i++) {
                fechasliq[i] = fechaItem;
                fechaItem = Util.fechaMasDias(fechaItem, Integer.parseInt(this.ultimoDiaMes(fechaItem)));
            }

            for (int i = 1; i <= numCuotas; i++) {
                if (i == numCuotas) {
                    capitalItem = capital - capitalItem * (numCuotas - 1);
                    capacitacionItem = vlrCapacitacion - capacitacionItem * (numCuotas - 1);
                }
                fechaItem = fechasliq[i - 1];
//                while (fendao.buscarFestivo(fechaItem)) {
//                    fechaItem = Util.fechaMasDias(fechaItem, 1);
//                }
                DocumentosNegAceptado item = new DocumentosNegAceptado();
                item.setFecha(fechaItem);
                item.setItem(i + "");
                item.setSaldo_inicial(saldoinicial);
                item.setSeguro(seguroItem);
                item.setCapacitacion(capacitacionItem);
                item.setCapital(capitalItem);
                item.setCat(catItem);
                item.setCuota_manejo(cuotaManejo);
                interesItem = Math.round(saldoinicial * Math.pow(1 + interesEA, (((double) Util.fechasDiferenciaEnDias(Util.convertiraDate(fechaAnterior), Util.convertiraDate(fechaItem))) / 360)) - saldoinicial);
                fechaAnterior = fechaItem;
                item.setInteres(interesItem);
                valorCuota = capitalItem + interesItem + capacitacionItem + catItem + seguroItem + cuotaManejo;
                if(i==1){
                   valorCuota = valorCuota  + vlrCentral;
                }
                item.setValor(valorCuota);
                saldofinal = saldoinicial - capitalItem;
                item.setSaldo_final(saldofinal);
                saldoinicial=saldofinal;
                item.setDias(Util.fechasDiferenciaEnDias( Util.convertiraDate(fechaNegocio), Util.convertiraDate(fechaItem))+"");
                items.add(item);
            }

        } else if (neg.getTipo_cuota().equals("CTFCPV")) {
            capital = valornegocio;
            saldoinicial = capital;
            seguroItem = vlrSeguro;
            capacitacionItem = Math.round(vlrCapacitacion / numCuotas);
            double vpTotal = 0;
            double vp = 0;


            for (int i = 0; i < numCuotas; i++) {
                fechasliq[i] = fechaItem;
                fechaItem = Util.fechaMasDias(fechaItem, Integer.parseInt(this.ultimoDiaMes(fechaItem)));
            }
      
           
            String fechaBaseRenovacion="0099-01-01";
            String fechaPrimeraCuota =""; 
            if(renovacion.equals("S")){
            
                System.err.println("es renovacion :"+renovacion );
            //calculamos los interes desde la fecha de negocio hasta la primera cuota
            fechaPrimeraCuota = fechasliq[0];
//            while (fendao.buscarFestivo(fechaPrimeraCuota)) {
//                fechaPrimeraCuota = Util.fechaMasDias(fechaPrimeraCuota, 1);
//            }
            System.out.println("Fecha Primera Cuota: " + fechaPrimeraCuota);

            //fecha base renovacion.
            SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date nuevaFecha = null;
            nuevaFecha = formatoFecha.parse(fechaPrimeraCuota);

            Calendar cal = Calendar.getInstance();
            cal.setTime(nuevaFecha);
            cal.add(Calendar.DATE, -30);
            fechaBaseRenovacion = formatoFecha.format(cal.getTime());
            fechaAnterior = fechaBaseRenovacion;
            System.out.println("Fecha base renovacion: " + fechaBaseRenovacion);
            //fin de la fecha base
            System.out.println("Fecha negocio: "+fechaNegocio);
            System.out.println("Diferencia en dias :"+ Util.fechasDiferenciaEnDias(Util.convertiraDate(fechaNegocio), Util.convertiraDate(fechaBaseRenovacion)));
            double interesPrimeraCuota = Math.round(saldoinicial * Math.pow(1 + interesEA, (((double) Util.fechasDiferenciaEnDias(Util.convertiraDate(fechaNegocio), Util.convertiraDate(fechaBaseRenovacion))) / 360)) - saldoinicial);
            System.out.println("Intereses Calculados Primera Cuota : " + interesPrimeraCuota);
            //aqui asignamos el nuevo capital del negocio.            
            capital = capital + (interesPrimeraCuota < 0 ? 0 : interesPrimeraCuota);
            saldoinicial = capital;
            System.out.println("Capital + Intereses Primera Cuaota : "+ capital);
            System.out.println("Saldo Inicial : "+ saldoinicial);
            
            }
            
            for (int i = 1; i <= numCuotas; i++) {
                 fechaItem = fechasliq[i - 1];
//                while (fendao.buscarFestivo(fechaItem)) {
//                    fechaItem = Util.fechaMasDias(fechaItem, 1);
//                }
                vp = Math.pow(1 + interesEA, (-(((double) Util.fechasDiferenciaEnDias(Util.convertiraDate(renovacion.equals("S") ? fechaBaseRenovacion : neg.getFecha_liquidacion()), Util.convertiraDate(fechaItem))) / 360)));
                vpTotal = vpTotal + vp;
            }
            fechaItem = Util.fechaMasDias(neg.getFecha_liquidacion(), Integer.parseInt(neg.getFpago()));
           
            
            if(convenio.isCat()){
                 catItem = Math.round(((porcCat * capital) * ((impuesto.getPorcentaje1() / 100) + 1)) / numCuotas);
            }
            
           
             //calculo de valores de la polizas cuota cuota 
            double vlr_poliza_cuota_cuota = obtenerValorPoliza(neg.getId_convenio(),convenio.getId_Sucursal(),capital,neg.getNodocs(),
                    fechaItem,neg.getTipo_cuota(),convenio.getAgencia(),fechaBaseRenovacion,renovacion,codigo,usuario);
            
            double vlr_poliza_financiada  = obtener_valor_poliza_financiada(numSolc);
            
            double  vlr_total_poliza_financiada=vlr_poliza_financiada*numCuotas;
            saldoinicial=saldoinicial+vlr_total_poliza_financiada;
            //valorCuota=valorCuota-vlr_poliza_financiada;
            
            
            for (int i = 1; i <= numCuotas; i++) {

                DocumentosNegAceptado item = new DocumentosNegAceptado();
                fechaItem = fechasliq[i - 1];
//                while (fendao.buscarFestivo(fechaItem)) {
//                    fechaItem = Util.fechaMasDias(fechaItem, 1);
//                }
                item.setFecha(fechaItem);
                item.setItem(i + "");
                item.setSaldo_inicial(saldoinicial);
                item.setSeguro(seguroItem);
                item.setCuota_manejo(cuotaManejo);
                item.setCat(catItem);
                interesItem = Math.round(saldoinicial * Math.pow(1 + interesEA, (((double) Util.fechasDiferenciaEnDias(Util.convertiraDate(fechaAnterior), Util.convertiraDate(fechaItem))) / 360)) - saldoinicial);
                fechaAnterior = fechaItem;
                item.setInteres(interesItem);
                capitalItem = Math.round((capital / vpTotal) - interesItem);
                if (i == numCuotas && i!=1) {
                    capitalItem = saldofinal;
                    capacitacionItem = vlrCapacitacion - capacitacionItem * (numCuotas - 1);
                }
                item.setCapital(capitalItem-vlr_poliza_financiada);
                item.setCapacitacion(capacitacionItem);
                valorCuota = (capital / vpTotal) + capacitacionItem + catItem + seguroItem + cuotaManejo+vlr_poliza_cuota_cuota - vlr_poliza_financiada;
                if(i==1){
                   valorCuota = valorCuota  + vlrCentral;
                }
                item.setValor(valorCuota);
                saldofinal = saldoinicial - capitalItem;
                item.setSaldo_final(saldofinal);
                saldoinicial = saldofinal;
                item.setDias(Util.fechasDiferenciaEnDias(Util.convertiraDate(fechaNegocio), Util.convertiraDate(fechaItem)) + "");
                item.setCapital_Poliza(vlr_poliza_cuota_cuota);
                items.add(item);
            }

        }

        this.setLiquidacion(items);
    }

    public void insertNegocioMicrocredito(Negocios neg, String numero_form, NegocioTrazabilidad negtraza) throws SQLException {
        list.insertNegocioMicrocredito(neg, numero_form,negtraza);
    }

      /**
     * Atualiza el negocio cuando ya ha ocurrido una liquidacion de microcredito.
     * @autor Iris Vargas
     * @throws SQLException
     */
    public void updateNegocioMicrocredito(Negocios neg,  String numero_form, NegocioTrazabilidad negtraza) throws SQLException {
        list.updateNegocioMicrocredito(neg, numero_form, negtraza);
    }

    
    public String ultimoDiaMes(String fecha) throws Exception {
         return list.ultimoDiaMes(fecha);
     }
    
//    public String ultimoDiaMes() throws Exception {
//         return list.ultimoDiaMes();
//     }
    
    
    /**
     * Datos del cliente del negocio
     * @param cod_neg Codigo del negocio
     * @return Listado con los datos
     * @throws Exception Cuando hay error
     */
    public Cliente datosCliente(String cod_neg) throws Exception{
        return list.datosCliente(cod_neg);
    }

    
    /**
     * M�todo que trae los negocios pendientes  para la creacion de la cxc de intereses microcredito por fecha de vencimiento
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public ArrayList<BeanGeneral> datosNegInteresMC() throws Exception {
        return list.datosNegInteresMC();
    }
    
    /**
     * M�todo que trae los negocios pendientes  para la creacion de la cxc de intereses microcredito por fecha de vencimiento
     * @param ciclo
     * @param periodo
     * @return 
     * @throws java.lang.Exception
     * @autor.......egonzalez
     * @throws......Exception
     * @version.....1.0.
     **/
    public ArrayList<BeanGeneral> datosNegInteresMIMC(String ciclo, String periodo) throws Exception {
        return list.datosNegInteresMIMC(ciclo,periodo);
    }

    public String ingresarCXCMicrocreditoPost(BeanGeneral bg, double valor_factura, String documento) throws SQLException {
        return list.ingresarCXCMicrocreditoPost(bg, valor_factura, documento);
    }

    public String ingresarDetalleCXCMicrocreditoPost(BeanGeneral bg, double valor_factura, String documento, String item) throws SQLException {
        return list.ingresarDetalleCXCMicrocreditoPost(bg, valor_factura, documento, item);

    }

    /**
     * M�todo que trae los negocios pendientes  para la creacion de la cxc de intereses microcredito de la primera cuota en fin de mes
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public ArrayList<BeanGeneral> datosNegInteresMCFMesPC() throws Exception {
        return list.datosNegInteresMCFMesPC();
    }

    /**
     * M�todo que trae los negocios pendientes  para la creacion de la cxc de intereses microcredito en fin de mes
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public ArrayList<BeanGeneral> datosNegInteresMCFMes() throws Exception {
        return list.datosNegInteresMCFMes();
    }

     public String updateInteresCausado(double valor, String fecha, String negocio, String item) throws SQLException {
        return list.updateInteresCausado(valor, fecha, negocio, item);
     }

     

     /**
     * M�todo que trae los datos necesarios para la creacion de la cxc de comision cat
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public ArrayList<BeanGeneral> datosCxcCat() throws Exception {
        return list.datosCxcCat();
    }
    
         /**
     * M�todo que trae los datos necesarios para la creacion de la cxc de comision cat
     * @param ciclo
     * @param periodo
     * @return 
     * @throws java.lang.Exception 
     * @autor.......egonzalez
     * @throws......Exception
     * @version.....1.0.
     **/
    public ArrayList<BeanGeneral> datosCxcCat(String ciclo, String periodo) throws Exception {
        return list.datosCxcCat(ciclo, periodo);
    }

     public String updateDocumentoCat( String documento, String negocio, String item) throws SQLException {
         return list.updateDocumentoCat(documento, negocio, item);
     }
     
     /**
     * DEVUELVE  TIPO DE PROCESO DE UN NEGOCIO
     * @param cod_neg Codigo del negocio
     * @return Listado con los datos
     * @throws Exception Cuando hay error
     */
    public String getTipoProceso(String cod_neg) throws Exception{
        return list.getTipoProceso(cod_neg);
    }


    /**
     * Trae el numero de negocio de un cliente el dia de hoy
     * @autor Iris Vargas
     * @throws SQLException
     */
    public int NumNegocioClienteHoy(String cliente) throws SQLException {
        return list.NumNegocioClienteHoy(cliente);
    }

    public String generarDocumentos(BeanGeneral bg, Usuario usuario,Negocios neg) throws Exception {

         String sql = "";

        //Obtener la informacion del convenio
        GestionConveniosDAO convdao = new GestionConveniosDAO(usuario.getBd());
        Convenio convenio = convdao.buscar_convenio(usuario.getBd(), bg.getValor_17());

        String[] datosCXC = list.buscarCuentasCXC(bg.getValor_17(), bg.getValor_18(), bg.getValor_16());

        //Guardar prefijos y cuentas
        bg.setValor_20(datosCXC[0]); //Prefijo
        bg.setValor_21(datosCXC[1]); //Cuenta CXC
        bg.setValor_22(datosCXC[2]); //HC CXC
        bg.setValor_23(convenio.getHc_cxp()); //HC CXP
        bg.setValor_24(convenio.getCuenta_cxp()); //Cuenta CXP
        String documento = list.UpCP(convenio.getPrefijo_cxp());//obtiene el numero de documento para la CXP
        int numCuotas = Integer.parseInt(bg.getValor_02()); //numero de cuotas del negocio
        int secNC = 1;

        //Ciclo para generar las facturas
            for (int i = 1; i < numCuotas + 1; i++) {
                int item=1;
                sql += list.ingresarCXCNegocio(bg, datosCXC[0], i, i, this.getLiquidacion(),convenio.isMediador_aval());
                double valor_item =this.getLiquidacion().get(i - 1).getCapital();
                bg.setValor_03(Math.round(valor_item)+"");
                bg.setValor_21(datosCXC[1]);
                sql += list.ingresarDetalleCXCNegocio(bg, datosCXC[0], i, item, "CAPITAL"); // factura_detalle
                if (!convenio.isMediador_aval()) {
                    item++;
                    valor_item = this.getLiquidacion().get(i - 1).getInteres();
                    bg.setValor_03(Math.round(valor_item) + "");
                    sql += list.ingresarDetalleCXCNegocio(bg, datosCXC[0], i, item, "INTERESES"); // factura_detalle
                }
                if (this.getLiquidacion().get(i - 1).getCustodia()>0) {
                    item++;
                    valor_item = this.getLiquidacion().get(i - 1).getCustodia();
                    bg.setValor_03(Math.round(valor_item) + "");
                    sql += list.ingresarDetalleCXCNegocio(bg, datosCXC[0], i, item, "CUSTODIA"); // factura_detalle
                }
                if (this.getLiquidacion().get(i - 1).getRemesa()>0) {
                    item++;
                    valor_item =this.getLiquidacion().get(i - 1).getRemesa();
                    bg.setValor_03(Math.round(valor_item) + "");
                    sql += list.ingresarDetalleCXCNegocio(bg, datosCXC[0], i, item, "REMESA"); // factura_detalle
                }
                if (this.getLiquidacion().get(i - 1).getSeguro()>0) {
                    item++;
                    valor_item = this.getLiquidacion().get(i - 1).getSeguro();
                    bg.setValor_03(Math.round(valor_item) + "");
                    sql += list.ingresarDetalleCXCNegocio(bg, datosCXC[0], i, item, "SEGURO"); // factura_detalle
                }

            }
            list.UpCP(datosCXC[0]); //Actualiza el numero de la serie para la CXC

            // generar factura por valor de aval si no se financio y no es un negocio de aval
            if(!neg.isFinanciaAval() && !this.isNegocioAval(neg.getCod_negocio()))
            {
                int item=1;              
                bg.setValor_03(neg.getvalor_aval()+"");
                sql += list.ingresarCXCAvalNegocio(bg, datosCXC[0],0, this.getLiquidacion());
                sql += list.ingresarDetalleCXCNegocio(bg,  datosCXC[0], 0, item, "AVAL"); // factura_detalle
            }


            //Ingresa la CXP afiliado se valida que no es un negocio de aval
            if (!this.isNegocioAval(neg.getCod_negocio())) {
                    sql += list.ingresarCXP(bg, documento, convenio);
                    sql += list.ingresarDetalleCXP(bg, documento, convenio);                
            }


            //Ingresa la CXP de aval
            //creamos uan variable para almacenar el nit del cliente.
            String codcli=bg.getValor_01();
            if ((!neg.isFinanciaAval() ) || isNegocioAval(neg.getCod_negocio())) {

                if (!neg.getNegocio_rel().equals("")) {
                    bg.setValor_29(neg.getVr_negocio() + "");// valor negocio de aval
                } else {
                    bg.setValor_29(neg.getvalor_aval() + "");// valor aval
                }
                documento = list.UpCP(convenio.getPrefijo_cxp_avalista());//obtiene el numero de documento para la CXP
                //almacena el nit original del cliente.
                //codcli=bg.getValor_01();
                bg.setValor_01(convenio.getNit_anombre());// nit
                bg.setValor_23(convenio.getHc_cxp_avalista());
                bg.setValor_24(convenio.getCuenta_cxp_avalista());
                String fechaActual = Util.getFechaActual_String(4);
                bg.setValor_30(fechaActual);
                sql += list.ingresarCXPAval(bg, documento, convenio);
                sql += list.ingresarDetalleCXPAval(bg, documento, convenio);
            }


            //Inserta los ingresos diferidos
            if(!convenio.isMediador_aval())
            {
            InteresesFenalcoDAO intdao = new InteresesFenalcoDAO(this.list.getDatabaseName());
            //metodo para insertar los ingresos insertarIngFenalco(cod_neg, cod_cli, usuario, documentType, cmc);
            sql += intdao.insertarIngFenalco(bg.getValor_06(), codcli, bg.getValor_04(), convenio.getPrefijo_diferidos(), convenio.getHc_diferidos());
            }

           if (convenio.isDescuenta_gmf()) {
                //nota credito gmf
                int max_cuota = Integer.parseInt(convenio.getCuota_gmf()); //Numero max de cuota para descontar gmf
                if (max_cuota >= numCuotas) {
                    double valorgmf = list.calcularGMF(bg.getValor_11(), (float) convenio.getPorc_gmf());
                    double valorgmf2 = list.calcularGMF(bg.getValor_11(), (float) convenio.getPorc_gmf2());
                    sql += list.ingresarNC_GMF(bg, documento + "-" + secNC, convenio, (valorgmf + valorgmf2));
                    sql += list.ingresarDetalleNC_GMF(bg, documento + "-" + secNC, convenio, valorgmf, "1", convenio.getCuenta_gmf());
                    sql += list.ingresarDetalleNC_GMF(bg, documento + "-" + secNC, convenio, valorgmf2, "2", convenio.getCuenta_gmf2());
                    secNC++;
                }
            }
        return sql;
    }
    

  public ArrayList<DocumentosNegAceptado> getLiquidacionAval() {
        return list.getLiquidacionAval();
    }

    public void setLiquidacionAval(ArrayList<DocumentosNegAceptado> liquidacion) {
        list.setLiquidacionAval(liquidacion);
    }


    public Negocios getNegocio() {
        return list.getNegocio();
    }

    public void setNegocio(Negocios negocio) {
        this.list.setNegocio(negocio);
    }

    public Negocios getNegocioAval() {
        return list.getNegocioAval();
    }

    public void setNegocioAval(Negocios negocioAval) {
        list.setNegocioAval(negocioAval);
    }
    


 public  void calcularLiquidacionNegocio(Negocios neg , Usuario usuario, Tipo_impuesto impuesto) throws Exception {

       
        GestionCondicionesService  gcservic = new GestionCondicionesService(usuario.getBd());

        double valornegocio = neg.getVr_negocio();
        int numCuotas = neg.getNodocs();
        double tasaInteres =Double.parseDouble(neg.getTasa()) / 100;
        double vlrSeguro = neg.getValor_seguro()/neg.getNodocs();

       
        String fechaNegocio = neg.getFechatran()!=null && !neg.getFechatran().equals("0099-01-01 00:00:00") ?neg.getFechatran():neg.getFecha_neg();
        String fechaItem =neg.getFecha_liquidacion();//Util.fechaMasDias(neg.getFecha_liquidacion(), Integer.parseInt(neg.getFpago()));
        double interesEA = Math.pow(1 + tasaInteres, 12) - 1;
        FenalcoFintraDAO fendao = new FenalcoFintraDAO(usuario.getBd());
        double capital = 0;
        double saldoinicial = 0;
        double saldofinal = 0;
        double capitalItem = 0;
        double custodiaItem;
        double valorCuota = 0;
        double interesItem = 0;
        double seguroItem = 0;
        double remesaItem = 0;
        double cuotaManejo = 0;
        String fechaAnterior = fechaNegocio;
        String [] fechasliq=new String [numCuotas]  ;

        ArrayList<DocumentosNegAceptado> items = new ArrayList(); 
            capital = valornegocio;
            saldoinicial = capital;
            seguroItem = vlrSeguro;
            custodiaItem = neg.getVr_cust();
            double vpTotal = 0;
            double vp = 0;
            cuotaManejo = this.getValor_Cuota_Manejo(String.valueOf(neg.getId_convenio()));
      
            for (int i = 0; i < numCuotas; i++) {
                fechasliq[i] = fechaItem;
                fechaItem = Util.fechaMasDias(fechaItem, Integer.parseInt(this.ultimoDiaMes(fechaItem)));
            }
            for (int i = 1; i <= numCuotas; i++) {
                 fechaItem = fechasliq[i - 1];
//                while (fendao.buscarFestivo(fechaItem)) {
//                    fechaItem = Util.fechaMasDias(fechaItem, 1);
//                }
                vp = Math.pow(1 + interesEA, (-(((double) Util.fechasDiferenciaEnDias(Util.convertiraDate(fechaNegocio), Util.convertiraDate(fechaItem))) / 360)));
                vpTotal = vpTotal + vp;
            }
            fechaItem = neg.getFecha_liquidacion();// Util.fechaMasDias(neg.getFecha_liquidacion(), Integer.parseInt(neg.getFpago()));
            for (int i = 1; i <= numCuotas; i++) {

                DocumentosNegAceptado item = new DocumentosNegAceptado();
                fechaItem = fechasliq[i - 1];
//                while (fendao.buscarFestivo(fechaItem)) {
//                    fechaItem = Util.fechaMasDias(fechaItem, 1);
//                }
                item.setFecha(fechaItem);
                item.setItem(i + "");
                item.setSaldo_inicial(saldoinicial);
                item.setSeguro(seguroItem);
                item.setCuota_manejo(cuotaManejo);
                if(neg.getMod_rem().equals(0)){
                item.setRemesa(Double.parseDouble(neg.getvalor_remesa()));
                }
                item.setCustodia(neg.getVr_cust());
                double dias = Util.fechasDiferenciaEnDias(Util.convertiraDate(fechaAnterior), Util.convertiraDate(fechaItem));
//                if (dias == 31 || i == 1) {
//                    dias = dias - 1;
//                }
                
                interesItem = Math.round(saldoinicial * Math.pow(1 + interesEA, (dias / 360)) - saldoinicial);
                fechaAnterior = fechaItem;
                item.setInteres(interesItem);
                capitalItem = Math.round((capital / vpTotal) - interesItem);
                if (i == numCuotas && i!=1) {
                    capitalItem = saldofinal;
                    //capacitacionItem = vlrCapacitacion - capacitacionItem * (numCuotas - 1);
                }
                item.setCapital(capitalItem);
                 remesaItem= ((neg.getPor_rem()/100)* ((capital / vpTotal) + custodiaItem +  seguroItem));
                valorCuota = Math.round((capital / vpTotal) +  seguroItem+remesaItem+cuotaManejo);

                if (i == numCuotas) {
                   double df = (capitalItem+seguroItem+custodiaItem+remesaItem+interesItem+cuotaManejo)-valorCuota;
                   item.setInteres(interesItem-df);
                }
                
            
                item.setRemesa(remesaItem);
                item.setValor(valorCuota);
                saldofinal = saldoinicial - capitalItem;
                item.setSaldo_final(saldofinal);
                saldoinicial = saldofinal;
                item.setDias(Util.fechasDiferenciaEnDias(Util.convertiraDate(fechaNegocio), Util.convertiraDate(fechaItem)) + "");
                items.add(item);
            }

            // Calcular Aval
            double vlrAval = 0;
            ArrayList Avales=null;
                try
                {
                Avales = gcservic.buscarRangosAval(neg.getIdProvConvenio(), neg.getTneg(), Integer.parseInt(neg.getFpago()), false);
                 if(Avales==null || Avales.size()==0)
                {
                 throw new Exception("Error: No existen condiciones de aval para estos parametros");
                } else if(items.size() > Avales.size()) {
                    throw new Exception("Error: No hay condiciones de aval para mas de "+Avales.size()+" cuota(s)");
                } else { 
                    if(!String.valueOf(neg.getId_convenio()).equals("35")){
                        vlrAval = CalcularAval(items, Avales);
                    }
                }
             }
            catch(Exception e)
            {
                throw new Exception("No existen condiciones de aval para estos parametros o para esta cantidad de cuotas");
            }
            
            vlrAval =vlrAval+ (vlrAval * (impuesto.getPorcentaje1() / 100));
            neg.setVr_aval(vlrAval);
            this.setLiquidacion(items);


         
        

    }




   public void calcularLiquidacionNegocioAval(Negocios neg , Usuario usuario, Tipo_impuesto impuesto) throws Exception {
        GestionCondicionesService  gcservic = new GestionCondicionesService(usuario.getBd());
        double valornegocio = neg.getVr_negocio();
        int numCuotas = neg.getNodocs();
        double tasaInteres =Double.parseDouble(neg.getTasa()) / 100;
        double vlrSeguro = neg.getValor_seguro()/neg.getNodocs();
        String fechaNegocio = neg.getFechatran()!=null && !neg.getFechatran().equals("0099-01-01 00:00:00") ?neg.getFechatran():neg.getFecha_neg();
        String fechaItem = neg.getFecha_liquidacion();  //Util.fechaMasDias(neg.getFecha_liquidacion(), Integer.parseInt(neg.getFpago()));
        double interesEA = Math.pow(1 + tasaInteres, 12) - 1;
        FenalcoFintraDAO fendao = new FenalcoFintraDAO(usuario.getBd());
        double capital = 0;
        double saldoinicial = 0;
        double saldofinal = 0;
        double capitalItem = 0;
        double valorCuota = 0;
        double interesItem = 0;
        double seguroItem = 0;
        double remesaItem = 0;
        double cuotaManejo = 0;
        String fechaAnterior = fechaNegocio;
        String [] fechasliq=new String [numCuotas]  ;

        ArrayList<DocumentosNegAceptado> items = new ArrayList();
            capital = valornegocio;
            saldoinicial = capital;
            seguroItem = vlrSeguro;
            double vpTotal = 0;
            double vp = 0;

            for (int i = 0; i < numCuotas; i++) {
                fechasliq[i] = fechaItem;
                fechaItem = Util.fechaMasDias(fechaItem, Integer.parseInt(this.ultimoDiaMes(fechaItem)));
            }
            for (int i = 1; i <= numCuotas; i++) {
                 fechaItem = fechasliq[i - 1];
//                while (fendao.buscarFestivo(fechaItem)) {
//                    fechaItem = Util.fechaMasDias(fechaItem, 1);
//                }
                vp = Math.pow(1 + interesEA, (-(((double) Util.fechasDiferenciaEnDias(Util.convertiraDate(fechaNegocio), Util.convertiraDate(fechaItem))) / 360)));
                vpTotal = vpTotal + vp;
            }
            fechaItem = neg.getFecha_liquidacion();//Util.fechaMasDias(neg.getFecha_liquidacion(), Integer.parseInt(neg.getFpago()));
            for (int i = 1; i <= numCuotas; i++) {

                DocumentosNegAceptado item = new DocumentosNegAceptado();
                fechaItem = fechasliq[i - 1];
//                while (fendao.buscarFestivo(fechaItem)) {
//                    fechaItem = Util.fechaMasDias(fechaItem, 1);
//                }
                item.setFecha(fechaItem);
                item.setItem(i + "");
                item.setSaldo_inicial(saldoinicial);
                item.setSeguro(seguroItem);
                item.setRemesa(Double.parseDouble(neg.getvalor_remesa()));
                double dias = Util.fechasDiferenciaEnDias(Util.convertiraDate(fechaAnterior), Util.convertiraDate(fechaItem));
                interesItem = Math.round(saldoinicial * Math.pow(1 + interesEA, (dias / 360)) - saldoinicial);
                fechaAnterior = fechaItem;
                item.setInteres(interesItem);
                capitalItem = Math.round((capital / vpTotal) - interesItem);
                if (i == numCuotas && i!=1) {
                    capitalItem = Math.round(saldofinal);
                }
                item.setCapital(capitalItem);
                 remesaItem= (neg.getPor_rem()/100)* ((capital / vpTotal) +  seguroItem);
                valorCuota = Math.round((capital / vpTotal) +  seguroItem+remesaItem);

                if (i == numCuotas) {
                   double df = (capitalItem + interesItem +  Math.round(remesaItem)) - valorCuota;
                   item.setInteres(interesItem-df);
                }
                item.setRemesa(remesaItem);
                item.setValor(valorCuota);
                saldofinal = saldoinicial - capitalItem;
                item.setSaldo_final(saldofinal);
                saldoinicial = saldofinal;
                item.setDias(Util.fechasDiferenciaEnDias(Util.convertiraDate(fechaNegocio), Util.convertiraDate(fechaItem)) + "");
                item.setCuota_manejo(cuotaManejo);
                items.add(item);
            }          
            neg.setVr_aval(0);
            this.setLiquidacionAval(items);

    }





 /**
     * calcula el valor de alval del negocio
     * @autor jpinedo
     * @throws SQLException
     */
    public Double CalcularAval(ArrayList<DocumentosNegAceptado> items, ArrayList Avales) throws SQLException {
        double valorAval = 0;
        double porcentaje = 0;
        double valorAvalCuota = 0;

        for (int i = 0; i < items.size(); i++) {
            porcentaje = (Double.parseDouble("" + Avales.get(i))) / 100;
            valorAvalCuota = (items.get(i).getCapital() + items.get(i).getInteres()) * porcentaje;
            valorAval = valorAval + valorAvalCuota;
        }


        return valorAval;
    }

    public String GuardarNegocio(Negocios neg, String numero_form, Convenio convenio, NegocioTrazabilidad negtraza) throws java.sql.SQLException {
        return list.GuardarNegocio(neg, numero_form, convenio, negtraza);

    }

    public String GuardarNegocioAval(Negocios neg, String numero_form, Convenio convenio, NegocioTrazabilidad negtraza) throws java.sql.SQLException {
        return list.GuardarNegocioAval(neg, numero_form, convenio, negtraza);
    }



    public String InsertaDocumentosForms(Negocios neg,String numero_form) throws Exception
    {
            return  list.InsertaDocumentosForms(neg, numero_form);
    }


        /**
     * Busca los datos de un negocio
     * @param codNegocio codigo del negocio a buscar
     * @return bean Negocios con la informacion encontrada
     * @throws Exception
     */
    public Negocios buscarNegocioAval(String codNegocio) throws Exception {
        return list.buscarNegocioAval(codNegocio);
    }
    
    
    public boolean isNegocioAval(String neg ) throws Exception{
            return list.isNegocioAval(neg);
     }

    public String getNumeroPagare(String neg) throws Exception {
        return list.getNumeroPagare(neg);
    }

    public void actualizaNumeroPagare(String neg, String numero_pagare) throws SQLException {
        list.actualizaNumeroPagare(neg, numero_pagare);
    }

    public String getNumeroFacVentaAval(String neg) throws Exception {
        return list.getNumeroFacVentaAval(neg);
    }
    

    public void actualizaNumeroFacVentaAval(String neg, String numero_pagare) throws SQLException {
        list.actualizaNumeroFacVentaAval(neg, numero_pagare);
    }
    
    
  public String ActualizarNegocioRLQ(Negocios neg, String numero_form, Convenio convenio, NegocioTrazabilidad negtraza) throws java.sql.SQLException {
        return list.ActualizarNegocioRLQ(neg, numero_form, convenio, negtraza);

    }

                  public String ActualizarNegocioAvalRLQ(Negocios neg, String numero_form, Convenio convenio, NegocioTrazabilidad negtraza) throws java.sql.SQLException {
        return list.ActualizarNegocioAvalRLQ(neg, numero_form, convenio, negtraza);

    }
     public String updateFormulario(Negocios neg, String numero_form) {
        return list.updateFormulario(neg, numero_form);
    }

    public String insertNegocio(Negocios neg) {
        return list.insertNegocio(neg);
    }
    
    public String generarCXCMicrocredito(Negocios neg, Usuario usuario, Convenio convenio) throws Exception {


        try {

            TransaccionService tService = new TransaccionService(usuario.getBd());
            InteresesFenalcoDAO intdao = new InteresesFenalcoDAO(this.list.getDatabaseName());
            tService.crearStatement();
            BeanGeneral bg = new BeanGeneral();
            bg.setValor_01(neg.getCod_cli()); //codcli - nit del cliente
            bg.setValor_02(neg.getNodocs() + ""); // numero de cuotas
            bg.setValor_03((neg.getTotpagado() / neg.getNodocs()) + ""); //Valor de las cuotas
            bg.setValor_04(usuario.getLogin()); //usuario en sesion
            bg.setValor_05(neg.getTneg()); //tipo de neg: cheque, letra, etc
            bg.setValor_06(neg.getCod_negocio()); //codigo del negocio
            bg.setValor_07(usuario.getBase()); //base
            bg.setValor_08("RD-" + neg.getCod_cli());
            bg.setValor_09(neg.getNit_tercero()); //nit tercero (nit del afiliado)
            bg.setValor_10(neg.getTotpagado() + ""); //total pagado(valor del campo tot_pagado en la tabla negocios)
            bg.setValor_11(neg.getVr_desem() + ""); //valor del desembolso
            bg.setValor_12(neg.getFecha_neg()); //fecha del negocio
            bg.setValor_13(neg.getNumaval()); //numero de aval
            bg.setValor_15(neg.getvalor_aval()); //valor del aval
            bg.setValor_16(neg.getTneg()); //Codigo del titulo valor
            bg.setValor_17(neg.getId_convenio() + ""); //codigo del convenio
            bg.setValor_18(usuario.getDstrct()); //distrito del usuario en sesion
            bg.setValor_27(neg.getValor_capacitacion() + ""); //valor capacitacion
            bg.setValor_28(neg.getValor_seguro() + ""); //valor seguro
            bg.setValor_29(neg.getVr_negocio() + ""); // valor negocio
            //Ciclo para generar las facturas
            String[] datosCXC = list.buscarCuentasCXC(bg.getValor_17(), bg.getValor_18(), bg.getValor_16());
	    bg.setValor_20(datosCXC[0]); //Prefijo
            bg.setValor_22(datosCXC[2]); //HC CXC 
            bg.setValor_23(neg.getTipo_liq());

            for (int i = 1; i < this.getLiquidacion().size() + 1; i++) {
                int item = 1;
                bg.setValor_03((i == 1 && convenio.isCentral()) ? convenio.getValor_central() + "" : "0");
                tService.getSt().addBatch(list.ingresarCXCMicrocredito(bg, datosCXC[0],i,Integer.parseInt(this.getLiquidacion().get(i - 1).getItem()), this.getLiquidacion()));
                double valor_item = this.getLiquidacion().get(i - 1).getCapital();
                bg.setValor_03(valor_item + "");
                bg.setValor_21(datosCXC[1]);
                tService.getSt().addBatch(list.ingresarDetalleCXCMicrocredito(bg, datosCXC[0],Integer.parseInt(this.getLiquidacion().get(i - 1).getItem()), item, "CAPITAL")); // factura_detalle
                if (convenio.isCapacitacion()) {
                    item++;
                    valor_item = this.getLiquidacion().get(i - 1).getCapacitacion();
                    bg.setValor_03(valor_item + "");
                    bg.setValor_21(convenio.getCuenta_capacitacion());
                    tService.getSt().addBatch(list.ingresarDetalleCXCMicrocredito(bg, datosCXC[0], Integer.parseInt(this.getLiquidacion().get(i - 1).getItem()), item, "CAPACITACION")); // factura_detalle
                }
                if (convenio.isSeguro()) {
                    item++;
                    valor_item = this.getLiquidacion().get(i - 1).getSeguro();
                    bg.setValor_03(valor_item + "");
                    bg.setValor_21(convenio.getCuenta_seguro());
                    tService.getSt().addBatch(list.ingresarDetalleCXCMicrocredito(bg, datosCXC[0], Integer.parseInt(this.getLiquidacion().get(i - 1).getItem()), item, "SEGURO")); // factura_detalle
                }
                if (i == 1 && convenio.isCentral()) {
                    item++;
                    valor_item = convenio.getValor_central();
                    bg.setValor_03(valor_item + "");
                    bg.setValor_21(convenio.getCuenta_central());
                    tService.getSt().addBatch(list.ingresarDetalleCXCMicrocredito(bg, datosCXC[0], Integer.parseInt(this.getLiquidacion().get(i - 1).getItem()), item, "CENTRAL")); // factura_detalle
                }
                 
                //cargos a la factura
                for (CargosFijosConvenios cfc : convenio.getCargos_fijos_convenios()) {
                    //intereses
                    if(cfc.getDescripcion().trim().equals("INTERES") && cfc.isActivo()){
                        item++;
                        valor_item =this.getLiquidacion().get(i - 1).getInteres();
                        bg.setValor_03(valor_item + "");
                        bg.setValor_21(datosCXC[1]);
                        tService.getSt().addBatch(list.ingresarDetalleCXCMicrocredito(bg, datosCXC[0], Integer.parseInt(this.getLiquidacion().get(i - 1).getItem()), item, cfc.getDescripcion())); 
                        
                        //creamos el diferido para el concepto
                        if(cfc.getEsdiferido().equals("S"))
                        agregarDiferidoNegocio(tService, intdao, bg, cfc, "SQL_CONSULTA_INTERES_NEGS", "SQL_INSERT_DIF_CARGO_FIJO", i,this.getLiquidacion().get(i - 1).getItem());
                    
                    }
                    //comision mipyme
                    if(cfc.getDescripcion().trim().equals("CAT") && cfc.isActivo()){
                        item++;
                        valor_item =this.getLiquidacion().get(i - 1).getCat();
                        bg.setValor_03(valor_item + "");
                        bg.setValor_21(datosCXC[1]);
                        tService.getSt().addBatch(list.ingresarDetalleCXCMicrocredito(bg, datosCXC[0], Integer.parseInt(this.getLiquidacion().get(i - 1).getItem()), item, cfc.getDescripcion())); 
                        
                        //creamos el diferido para el concepto
                        if(cfc.getEsdiferido().equals("S"))
                        agregarDiferidoNegocio(tService, intdao, bg, cfc, "SQL_CONSULTA_CAT_NEGS", "SQL_INSERT_DIF_CARGO_FIJO", i, this.getLiquidacion().get(i - 1).getItem());
                                                     
                    }
                    //cuota de administracion.
                    if(cfc.getDescripcion().trim().equals("CUOTA-ADMINISTRCION") && cfc.isActivo()){
                        item++;
                        valor_item =this.getLiquidacion().get(i - 1).getCuota_manejo();
                        bg.setValor_03(valor_item + "");
                        bg.setValor_21(datosCXC[1]);   
                        tService.getSt().addBatch(list.ingresarDetalleCXCMicrocredito(bg, datosCXC[0], Integer.parseInt(this.getLiquidacion().get(i - 1).getItem()), item, cfc.getDescripcion())); 
                        
                        //creamos el diferido para el concepto
                        if(cfc.getEsdiferido().equals("S"))
                        agregarDiferidoNegocio(tService, intdao, bg, cfc,"SQL_CONSULTA_CUOTA_ADMIN_NEGS", "SQL_INSERT_DIF_CARGO_FIJO", i,this.getLiquidacion().get(i - 1).getItem());
                    
                    }
                    if(cfc.getDescripcion().trim().equals("POLIZAS") && cfc.isActivo()){
                        item++;
                        valor_item =this.getLiquidacion().get(i - 1).getCapital_Poliza();
                        bg.setValor_03(valor_item + "");
                        bg.setValor_21(datosCXC[1]);   
                        tService.getSt().addBatch(list.ingresarDetalleCXCMicrocredito(bg, datosCXC[0], Integer.parseInt(this.getLiquidacion().get(i - 1).getItem()), item, cfc.getDescripcion())); 
                        
                        //creamos el diferido para el concepto
                        if(cfc.getEsdiferido().equals("S"))
                        agregarDiferidoNegocio(tService, intdao, bg, cfc,"SQL_CONSULTA_POLIZAS_NEGS", "SQL_INSERT_DIF_CARGO_FIJO", i, this.getLiquidacion().get(i - 1).getItem());
                    
                    }
                    
                    if(cfc.getDescripcion().trim().equals("CAPITAL-AVAL") && cfc.isActivo()){
                        item++;
                        valor_item =this.getLiquidacion().get(i - 1).getCapital_aval();
                        bg.setValor_03(valor_item + "");
                        bg.setValor_21(datosCXC[1]);   
                        tService.getSt().addBatch(list.ingresarDetalleCXCMicrocredito(bg, datosCXC[0], Integer.parseInt(this.getLiquidacion().get(i - 1).getItem()), item, cfc.getDescripcion())); 
                        
                        //creamos el diferido para el concepto
                        if(cfc.getEsdiferido().equals("S"))
                        agregarDiferidoNegocio(tService, intdao, bg, cfc,"SQL_CONSULTA_CAPITALAVAL_NEGS", "SQL_INSERT_DIF_CARGO_FIJO", i, this.getLiquidacion().get(i - 1).getItem());                    
                    }
                    
                    if(cfc.getDescripcion().trim().equals("INTERES-AVAL") && cfc.isActivo()){
                        item++;
                        valor_item =this.getLiquidacion().get(i - 1).getInteres_aval();
                        bg.setValor_03(valor_item + "");
                        bg.setValor_21(datosCXC[1]);   
                        tService.getSt().addBatch(list.ingresarDetalleCXCMicrocredito(bg, datosCXC[0], Integer.parseInt(this.getLiquidacion().get(i - 1).getItem()), item, cfc.getDescripcion())); 
                        
                        //creamos el diferido para el concepto
                        if(cfc.getEsdiferido().equals("S"))
                        agregarDiferidoNegocio(tService, intdao, bg, cfc,"SQL_CONSULTA_INTERESAVAL_NEGS", "SQL_INSERT_DIF_CARGO_FIJO", i, this.getLiquidacion().get(i - 1).getItem());                    
                    }
                     
                    if(cfc.getDescripcion().trim().equals("SEGURO-DEUDOR") && cfc.isActivo()){
                        item++;
                        valor_item =this.getLiquidacion().get(i - 1).getSeguro();
                        bg.setValor_03(valor_item + "");
                        bg.setValor_21(datosCXC[1]);   
                        tService.getSt().addBatch(list.ingresarDetalleCXCMicrocredito(bg, datosCXC[0], Integer.parseInt(this.getLiquidacion().get(i - 1).getItem()), item, cfc.getDescripcion())); 
                        
                        //creamos el diferido para el concepto
                        if(cfc.getEsdiferido().equals("S"))
                        agregarDiferidoNegocio(tService, intdao, bg, cfc,"SQL_CONSULTA_SEGURO_DEUDOR_NEGS", "SQL_INSERT_DIF_CARGO_FIJO", i, this.getLiquidacion().get(i - 1).getItem());                    
                    }
                    
                    if(cfc.getDescripcion().trim().equals("AVAL") && cfc.isActivo()){
                        item++;
                        valor_item =this.getLiquidacion().get(i - 1).getAval();
                        bg.setValor_03(valor_item + "");
                        bg.setValor_21(datosCXC[1]);   
                        tService.getSt().addBatch(list.ingresarDetalleCXCMicrocredito(bg, datosCXC[0], Integer.parseInt(this.getLiquidacion().get(i - 1).getItem()), item, cfc.getDescripcion())); 
                        
                        //creamos el diferido para el concepto
                        if(cfc.getEsdiferido().equals("S"))
                        agregarDiferidoNegocio(tService, intdao, bg, cfc,"SQL_CONSULTA_AVAL_NEGS", "SQL_INSERT_DIF_CARGO_FIJO", i, this.getLiquidacion().get(i - 1).getItem());                    
                    }
                    
                    //aqui se agregan otros cargos fijos en caso de se necesarios
                    
                }//fin cargo fijos de la factura.
                
            }
            list.UpCP(datosCXC[0]); //Actualiza el numero de la serie para la CXC
            
            tService.execute();

        } catch (SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(NegociosGenService.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
        return "";
    }

    /**
     * M�todo que trae las cuotas de un negocio que tiene intereses pendientes por causar
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public ArrayList<BeanGeneral> interesesPendientes(String negocio) throws Exception {
        return list.interesesPendientes(negocio);
    }

    /**
     * M�todo que trae las cuotas de un negocio que tiene facturas cat pendientes por causar
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public ArrayList<BeanGeneral> catPendientes(String negocio) throws Exception {
        return list.catPendientes(negocio);
    }

    /**
     * M�todo que trae el query para la anulacion de factura de interes de una cuota o factura
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public String anularFacturaInteres(String factura) throws SQLException {
        return list.anularFacturaInteres(factura);
    }   
    
    public String InsertaDocumentosFormsReliquidacionConvenio(Negocios neg,String numero_form) throws Exception
    {
            return  list.InsertaDocumentosFormsReliquidacionConvenio(neg, numero_form);
    }

     public String NegociosGuardar(Negocios neg,String numero_solicitud,String ciclo,String numero_form, Convenio convenio, NegocioTrazabilidad negtraza) throws java.sql.SQLException{
       return list.NegociosGuardar(neg,numero_solicitud,ciclo,numero_form, convenio,negtraza);
    }


      public String MarcarNegocio(String negocio ,String negocio_rel,String concepto_rel) throws Exception{
       return list.MarcarNegocio(negocio, negocio_rel, concepto_rel);
    }

    public String DeleteDocumentosForms(Negocios neg, String numero_form) throws Exception {
        return list.DeleteDocumentosForms(neg, numero_form);
    }
    public String actualiza_perfeccionamiento(String cod, String estado) throws SQLException, Exception {
                return list.actualiza_perfeccionamiento(cod, estado);
    }
    
    public ArrayList<Proveedor> filtroProveedoresTransferencias(String negocio, String banco) throws Exception {

        return list.filtroProveedoresTransferencias(negocio, banco);
   }

     public boolean isCxpAfiliado(String neg ,String cxp) throws Exception{
            return list.isCxpAfiliado(neg, cxp);
     }

     /**
     * Atualiza la Fecha_Factura_Aval cuando es generada la misma y es priemra vez que se genera.
     * @autor David Paz
     * @throws SQLException
     */
     public void ActualizaFechaFacturaAval(String cod_neg, String fecha_factura_aval) throws java.sql.SQLException{
        list.ActualizaFechaFacturaAval(cod_neg, fecha_factura_aval);
    }
     
    /**
     * Ejecuta la funcion EndosoTercero .
     * @autor Egonzalez
     * @throws SQLException
     */
     public String runEndosoTercero(String numFactura, String convenio) throws java.sql.SQLException{

         return list.runEndosoTersero(numFactura, convenio) ;
    }
     

    public String getAccountToTxt(String cta) throws SQLException {
        return list.accountToTxt(cta);
    }
    
     public String generarCXCMicrocreditoReestructuracion(Negocios neg, Usuario usuario, Convenio convenio, ArrayList<DocumentosNegAceptado> liquidacion) throws Exception {


        try {

            TransaccionService tService = new TransaccionService(usuario.getBd());
            InteresesFenalcoDAO intdao = new InteresesFenalcoDAO(this.list.getDatabaseName());
            tService.crearStatement();
            BeanGeneral bg = new BeanGeneral();
            bg.setValor_01(neg.getCod_cli()); //codcli - nit del cliente
            bg.setValor_02(neg.getNodocs() + ""); // numero de cuotas
            bg.setValor_03((neg.getTotpagado() / neg.getNodocs()) + ""); //Valor de las cuotas
            bg.setValor_04(usuario.getLogin()); //usuario en sesion
            bg.setValor_05(neg.getTneg()); //tipo de neg: cheque, letra, etc
            bg.setValor_06(neg.getCod_negocio()); //codigo del negocio
            bg.setValor_07(usuario.getBase()); //base
            bg.setValor_08("RD-" + neg.getCod_cli());
            bg.setValor_09(neg.getNit_tercero()); //nit tercero (nit del afiliado)
            bg.setValor_10(neg.getTotpagado() + ""); //total pagado(valor del campo tot_pagado en la tabla negocios)
            bg.setValor_11(neg.getVr_desem() + ""); //valor del desembolso
            bg.setValor_12(neg.getFecha_neg()); //fecha del negocio
            bg.setValor_13(neg.getNumaval()); //numero de aval
            bg.setValor_15(neg.getvalor_aval()); //valor del aval
            bg.setValor_16(neg.getTneg()); //Codigo del titulo valor
            bg.setValor_17(neg.getId_convenio() + ""); //codigo del convenio
            bg.setValor_18(usuario.getDstrct()); //distrito del usuario en sesion
            bg.setValor_27(neg.getValor_capacitacion() + ""); //valor capacitacion
            bg.setValor_28(neg.getValor_seguro() + ""); //valor seguro
            bg.setValor_29(neg.getVr_negocio() + ""); // valor negocio
            //Ciclo para generar las facturas
            String[] datosCXC = list.buscarCuentasCXC(bg.getValor_17(), bg.getValor_18(), bg.getValor_16());
	    bg.setValor_20(datosCXC[0]); //Prefijo
            bg.setValor_22(datosCXC[2]); //HC CXC 

            for (int i = 1; i < neg.getNodocs() + 1; i++) {
                int item = 1;
                bg.setValor_03((i == 1 && convenio.isCentral()) ? convenio.getValor_central() + "" : "0");
                tService.getSt().addBatch(list.ingresarCXCMicrocreditoReestructuracion(bg, datosCXC[0], i, liquidacion));
                double valor_item = liquidacion.get(i - 1).getCapital();
                bg.setValor_03(valor_item + "");
                bg.setValor_21(datosCXC[1]);
                tService.getSt().addBatch(list.ingresarDetalleCXCMicrocredito(bg, datosCXC[0], Integer.parseInt(liquidacion.get(i - 1).getItem()), item, "CAPITAL")); // factura_detalle
                if (convenio.isCapacitacion()) {
                    item++;
                    valor_item = liquidacion.get(i - 1).getCapacitacion();
                    bg.setValor_03(valor_item + "");
                    bg.setValor_21(convenio.getCuenta_capacitacion());
                    tService.getSt().addBatch(list.ingresarDetalleCXCMicrocredito(bg, datosCXC[0], Integer.parseInt(liquidacion.get(i - 1).getItem()), item, "CAPACITACION")); // factura_detalle
                }
                if (convenio.isSeguro()) {
                    item++;
                    valor_item = liquidacion.get(i - 1).getSeguro();
                    bg.setValor_03(valor_item + "");
                    bg.setValor_21(convenio.getCuenta_seguro());
                    tService.getSt().addBatch(list.ingresarDetalleCXCMicrocredito(bg, datosCXC[0], Integer.parseInt(liquidacion.get(i - 1).getItem()), item, "SEGURO")); // factura_detalle
                }
                if (i == 1 && convenio.isCentral()) {
                    item++;
                    valor_item = convenio.getValor_central();
                    bg.setValor_03(valor_item + "");
                    bg.setValor_21(convenio.getCuenta_central());
                    tService.getSt().addBatch(list.ingresarDetalleCXCMicrocredito(bg, datosCXC[0], Integer.parseInt(liquidacion.get(i - 1).getItem()), item, "CENTRAL")); // factura_detalle
                }
                
                //cargos a la factura
                for (CargosFijosConvenios cfc : convenio.getCargos_fijos_convenios()) {
                    //intereses
                    if(cfc.getDescripcion().trim().equals("INTERES") && cfc.isActivo()){
                        item++;
                        valor_item =this.getLiquidacion().get(i - 1).getInteres();
                        //valor_item =liquidacion.get(i - 1).getInteres();
                        bg.setValor_03(valor_item + "");
                        bg.setValor_21(datosCXC[1]);
                        tService.getSt().addBatch(list.ingresarDetalleCXCMicrocredito(bg, datosCXC[0], Integer.parseInt(liquidacion.get(i - 1).getItem()), item, cfc.getDescripcion())); 
                        
                        //creamos el diferido para el concepto
                        if(cfc.getEsdiferido().equals("S"))
                        agregarDiferidoNegocio(tService, intdao, bg, cfc, "SQL_CONSULTA_INTERES_NEGS", "SQL_INSERT_DIF_CARGO_FIJO", i,liquidacion.get(i - 1).getItem() );
                    
                    }
                    //comision mipyme
                    if(cfc.getDescripcion().trim().equals("CAT") && cfc.isActivo()){
                        item++;
                        valor_item = valor_item =this.getLiquidacion().get(i - 1).getCat();
                        //valor_item = valor_item =liquidacion.get(i - 1).getCat();
                        bg.setValor_03(valor_item + "");
                        bg.setValor_21(datosCXC[1]);
                        tService.getSt().addBatch(list.ingresarDetalleCXCMicrocredito(bg, datosCXC[0], Integer.parseInt(liquidacion.get(i - 1).getItem()), item, cfc.getDescripcion())); 
                        
                        //creamos el diferido para el concepto
                       if(cfc.getEsdiferido().equals("S"))
                        agregarDiferidoNegocio(tService, intdao, bg, cfc, "SQL_CONSULTA_CAT_NEGS", "SQL_INSERT_DIF_CARGO_FIJO", i, liquidacion.get(i - 1).getItem());
                                                     
                    }
                    //cuota de administracion.
                    if(cfc.getDescripcion().trim().equals("CUOTA-ADMINISTRCION") && cfc.isActivo()){
                        item++;
                        valor_item =this.getLiquidacion().get(i - 1).getCuota_manejo();
                        //valor_item =liquidacion.get(i - 1).getCuota_manejo();
                        bg.setValor_03(valor_item + "");
                        bg.setValor_21(datosCXC[1]);   
                        tService.getSt().addBatch(list.ingresarDetalleCXCMicrocredito(bg, datosCXC[0], Integer.parseInt(liquidacion.get(i - 1).getItem()), item, cfc.getDescripcion())); 
                        
                        //creamos el diferido para el concepto
                       if(cfc.getEsdiferido().equals("S"))
                        agregarDiferidoNegocio(tService, intdao, bg, cfc,"SQL_CONSULTA_CUOTA_ADMIN_NEGS", "SQL_INSERT_DIF_CARGO_FIJO", i, liquidacion.get(i - 1).getItem());
                    
                    }
                    
                    if(cfc.getDescripcion().trim().equals("SEGURO-DEUDOR") && cfc.isActivo()){
                        item++;
                        valor_item =this.getLiquidacion().get(i - 1).getSeguro();
                        bg.setValor_03(valor_item + "");
                        bg.setValor_21(datosCXC[1]);   
                        tService.getSt().addBatch(list.ingresarDetalleCXCMicrocredito(bg, datosCXC[0], Integer.parseInt(this.getLiquidacion().get(i - 1).getItem()), item, cfc.getDescripcion())); 
                        
                        //creamos el diferido para el concepto
                        if(cfc.getEsdiferido().equals("S"))
                        agregarDiferidoNegocio(tService, intdao, bg, cfc,"SQL_CONSULTA_SEGURO_DEUDOR_NEGS", "SQL_INSERT_DIF_CARGO_FIJO", i, this.getLiquidacion().get(i - 1).getItem());                    
                    }
                    
                    if(cfc.getDescripcion().trim().equals("AVAL") && cfc.isActivo()){
                        item++;
                        valor_item =this.getLiquidacion().get(i - 1).getAval();
                        bg.setValor_03(valor_item + "");
                        bg.setValor_21(datosCXC[1]);   
                        tService.getSt().addBatch(list.ingresarDetalleCXCMicrocredito(bg, datosCXC[0], Integer.parseInt(this.getLiquidacion().get(i - 1).getItem()), item, cfc.getDescripcion())); 
                        
                        //creamos el diferido para el concepto
                        if(cfc.getEsdiferido().equals("S"))
                        agregarDiferidoNegocio(tService, intdao, bg, cfc,"SQL_CONSULTA_AVAL_NEGS", "SQL_INSERT_DIF_CARGO_FIJO", i, this.getLiquidacion().get(i - 1).getItem());                    
                    }
                    
                    //aqui se agregan otros cargos fijos en caso de se necesarios
                    
                }//fin cargo fijos de la factura.
                
            }
            list.UpCP(datosCXC[0]); //Actualiza el numero de la serie para la CXC

            tService.execute();

        } catch (SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(NegociosGenService.class.getName()).log(Level.SEVERE, null, ex);
            return "error";
        }
        return "OK";
    }
     
     
    public ArrayList<BeanGeneral> cargarNegocios(String negocio, String banco) throws SQLException {
        return list.cargarNegocios(negocio,banco);
    }

     public ArrayList<BeanGeneral> cargarNegociosLibranza(String negocio, String banco) throws SQLException {
        return list.cargarNegociosLibranza(negocio,banco);
    }
     
      public ArrayList<BeanGeneral> cargarNegociosCHequeLibranza(String doc_rel) throws SQLException {
        return list.cargarNegociosCHequeLibranza(doc_rel);
    }
 
    public String perfeccionarNegocioLibranza(String negocio,Usuario usuario) throws SQLException {
        return list.perfeccionarNegocioLibranza(negocio ,usuario);
    }
    
    public String perfeccionarNegocioLI(String negocio,Usuario usuario) throws SQLException {
        return list.perfeccionarNegocioLI(negocio ,usuario);
    }

    public String marcarCXPGeneradoCheque(String cxp, String negocios, Usuario usuario) throws SQLException {
        return list.marcarCXPGeneradoCheque(cxp, negocios, usuario);
    }
    
    public String CxCLibranza(String negocios, Usuario usuario) throws SQLException {
        return list.CxCLibranza( negocios, usuario);
    }

    public String exportarPdf(String cxp, String negocios,String banco_transferencia, Usuario usuario,String Conv) throws SQLException {
        return list.datosPdf(cxp, negocios,banco_transferencia, usuario,Conv);
    }
     
    public ArrayList<BeanGeneral> cargarNegociosProveedor(String[] nits, String fechaInicio, String fechaFin, String un_neg, String banco) throws SQLException {
        return list.cargarNegociosProveedor(nits,fechaInicio,fechaFin,un_neg,banco);
    }

    public String cargarComision() throws SQLException {
        return list.cargarComision();
    }
    
   
    public ArrayList<String> generarDocumentosFenalco(BeanGeneral bg, Usuario usuario, Negocios neg) throws Exception {

        ArrayList<String> listSql = new ArrayList<String>();
        
        //Obtener la informacion del convenio
        GestionConveniosDAO convdao = new GestionConveniosDAO(usuario.getBd());
        Convenio convenio = convdao.buscar_convenio(usuario.getBd(), bg.getValor_17());
        Deducciones  deduccion = convdao.buscar_deducciones_convenio(usuario.getBd(), convenio.getId_convenio());

        //Guardar prefijos y cuentas
        bg.setValor_23(convenio.getHc_cxp()); //HC CXP
        bg.setValor_24(convenio.getCuenta_cxp()); //Cuenta CXP
        String documento = list.UpCP(convenio.getPrefijo_cxp());//obtiene el numero de documento para la CXP
        int numCuotas = Integer.parseInt(bg.getValor_02()); //numero de cuotas del negocio
        int secNC = 1;
        String idconvenio=deduccion.getid_convenio();
        String modalidad=deduccion.getmodalidad();
        double valor = 0; 
        double vs = Double.valueOf(bg.getValor_11()).doubleValue();
        //Ciclo para generar las facturas
        //Ingresa la CXP afiliado se valida que no es un negocio de aval
        if (!this.isNegocioAval(neg.getCod_negocio())) {
            
            String politica=neg.getPolitica();
            
            
            
             if (politica.equals("R") && neg.getId_convenio()==58){
                
             String num_cxp_renovacion = list.UpCP("CXPRENOVACION");
            
             listSql.add(list.ingresarCxpRenovacion(bg, num_cxp_renovacion, convenio,neg));
             listSql.add(list.ingresarDetalleCxpRenovacion(bg, num_cxp_renovacion, convenio,neg));
            
            }
            
            listSql.add(list.ingresarCXP(bg, documento, convenio));
            listSql.add(list.ingresarDetalleCXP(bg, documento, convenio));
         
            if (!idconvenio.equals("")){
                
            if(modalidad.equals("P")){    
                
             valor=vs*deduccion.getperc_cobrar()/100;
             
             }else{
               // Sila modalidad de la deduccion no es por porcentaje (P), se valida si el monto esta en el rango para la deduccion  
               valor = (vs>=deduccion.getdesembolso_inicial() && vs<=deduccion.getdesembolso_final()) ? deduccion.getvalor_cobrar() : 0;
            }
                if (valor!=0){
            
                String serialingreso = list.serialingreso();//obtiene el numero de documento para el ingreso
                String serialCXC = list.serialCxc();//obtiene el numero de documento para la CXC
               // double deducciones =Double.parseDouble(list.deducciones(convenio));//porcentaje de descuento    
                listSql.add(list.crearNcConsumo(bg,documento,valor));             
                listSql.add(list.crearNcDetalleConsumo(bg,documento,valor));             
                listSql.add(list.crearIngresoConsumo(bg,serialingreso,valor));             
                listSql.add(list.crearIngresoDetalleConsumo(bg,serialingreso,valor,serialCXC));             
                listSql.add(list.crearCxcConsumo(bg,serialCXC,valor));             
                listSql.add(list.crearCxcDetalleConsumo(bg,serialCXC,valor));             
                listSql.add(list.crearCxcDetalleConsumoIVA(bg,serialCXC,valor));
                listSql.add(list.updateserialcxc()); 
                listSql.add(list.updateserialingreso()); 
                listSql.add(list.updateCxpConsumo(bg,documento,valor));             
                listSql.add(list.updateCxcConsumo(bg,serialCXC,valor)); 
                
            }
            
            }
            //consultas la tabla nueva de deducciones fintra por el convenio o unidad negocio...
            ///haces el calculo con el monto 
            //ingresarNC_Aval
        }

         //Ingresa la CXP de aval
        //creamos uan variable para almacenar el nit del cliente.
        String codcli = bg.getValor_01();
        if ((!neg.isFinanciaAval()) || isNegocioAval(neg.getCod_negocio())) {

            if (!neg.getNegocio_rel().equals("")) {
                bg.setValor_29(neg.getVr_negocio() + "");// valor negocio de aval
            } else {
                bg.setValor_29(neg.getvalor_aval() + "");// valor aval
            }

            //almacena el nit original del cliente.
            bg.setValor_01(convenio.getNit_anombre());// nit
            bg.setValor_23(convenio.getHc_cxp_avalista());
            bg.setValor_24(convenio.getCuenta_cxp_avalista());
            String fechaActual = Util.getFechaActual_String(4);
            bg.setValor_30(fechaActual);
            if (neg.isAval_por_cuota()) {
                bg.setValor_29(neg.getCuota_aval_mensual() + "");                
                for (int i = 0; i < neg.getNodocs(); i++) {
                    documento = list.UpCP(convenio.getPrefijo_cxp_avalista());//obtiene el numero de documento para la CXP    
                    listSql.add(list.ingresarCXPAval(bg, documento, convenio));
                    listSql.add(list.ingresarDetalleCXPAval(bg, documento, convenio));
                    listSql.add(list.InsertHistoricoDeduccionesFianza(bg, neg, documento, convenio));

                    fechaActual = Util.addMonthsDate(fechaActual, 1);
                    bg.setValor_30(fechaActual);
                }

            } else {
                documento = list.UpCP(convenio.getPrefijo_cxp_avalista());//obtiene el numero de documento para la CXP
                listSql.add(list.ingresarCXPAval(bg, documento, convenio));
                listSql.add(list.ingresarDetalleCXPAval(bg, documento, convenio));
                listSql.add(list.InsertHistoricoDeduccionesFianza(bg, neg, documento, convenio));
            }
        }

        //Inserta los ingresos diferidos
        InteresesFenalcoDAO intdao = new InteresesFenalcoDAO(this.list.getDatabaseName());
        if (!convenio.isMediador_aval()) {          
            //metodo para insertar los ingresos insertarIngFenalco(cod_neg, cod_cli, usuario, documentType, cmc);
            listSql.addAll(intdao.insertarIngFenalco(bg.getValor_06(), codcli, bg.getValor_04(), convenio.getPrefijo_diferidos(), convenio.getHc_diferidos()));
        }
        
        //Insertamos el diferido de la cuota administracion ojo cambiar 
//        if (!convenio.getPrefijo_cuota_admin().equals("")) {
//            ArrayList<String> insertarIngFenalcoCargosFijos = intdao.insertarIngFenalcoCargosFijos("SQL_CONSULTA_CUOTA_ADMIN_NEGS", "SQL_INSERT_ING_CUOTA_ADMIN",
//                    bg.getValor_06(), codcli, bg.getValor_04(),
//                    convenio.getPrefijo_cuota_admin(), convenio.getHc_cuota_admin(),neg.getTipo_liq());
//            
//            if (!insertarIngFenalcoCargosFijos.isEmpty()) {
//                listSql.addAll(insertarIngFenalcoCargosFijos);
//            }
//        }

        if (convenio.isDescuenta_gmf()) {
            //nota credito gmf
            int max_cuota = Integer.parseInt(convenio.getCuota_gmf()); //Numero max de cuota para descontar gmf
            if (max_cuota >= numCuotas) {
                double valorgmf = list.calcularGMF(bg.getValor_11(), (float) convenio.getPorc_gmf());
                double valorgmf2 = list.calcularGMF(bg.getValor_11(), (float) convenio.getPorc_gmf2());
                listSql.add(list.ingresarNC_GMF(bg, documento + "-" + secNC, convenio, (valorgmf + valorgmf2)));
                listSql.add(list.ingresarDetalleNC_GMF(bg, documento + "-" + secNC, convenio, valorgmf, "1", convenio.getCuenta_gmf()));
                listSql.add(list.ingresarDetalleNC_GMF(bg, documento + "-" + secNC, convenio, valorgmf2, "2", convenio.getCuenta_gmf2()));
                secNC++;
            }
        }
        return listSql;
    }
    
    
    public ArrayList generarDocumentosCXC(BeanGeneral bg, Usuario usuario, Negocios neg, ArrayList<DocumentosNegAceptado> liquidacion) throws Exception {

        ArrayList query = new ArrayList();

        //Obtener la informacion del convenio
        GestionConveniosDAO convdao = new GestionConveniosDAO(usuario.getBd());
        Convenio convenio = convdao.buscar_convenio(usuario.getBd(), bg.getValor_17());

        String[] datosCXC = list.buscarCuentasCXC(bg.getValor_17(), bg.getValor_18(), bg.getValor_16());

        //Guardar prefijos y cuentas
        bg.setValor_20(datosCXC[0]); //Prefijo
        bg.setValor_21(datosCXC[1]); //Cuenta CXC
        bg.setValor_22(datosCXC[2]); //HC CXC
        bg.setValor_23(convenio.getHc_cxp()); //HC CXP
        bg.setValor_24(convenio.getCuenta_cxp()); //Cuenta CXP
        int numCuotas = Integer.parseInt(bg.getValor_02()); //numero de cuotas del negocio

        //Ciclo para generar las facturas
        for (int i = 1; i < liquidacion.size() + 1; i++) {
            
            int item = 1;
            bg.setValor_21(datosCXC[1]); //Cuenta CXC
            query.add(list.ingresarCXCNegocio(bg, datosCXC[0], i,Integer.parseInt(liquidacion.get(i - 1).getItem()),liquidacion, convenio.isMediador_aval()));

            double valor_item = liquidacion.get(i - 1).getCapital();
            bg.setValor_03(Math.round(valor_item) + "");
            bg.setValor_21(datosCXC[1]);
            query.add(list.ingresarDetalleCXCNegocio(bg, datosCXC[0],Integer.parseInt(liquidacion.get(i - 1).getItem()), item, "CAPITAL")); // factura_detalle

            if (!convenio.isMediador_aval()) {
                item++;
                valor_item = liquidacion.get(i - 1).getInteres();
                bg.setValor_03(Math.round(valor_item) + "");
                query.add(list.ingresarDetalleCXCNegocio(bg, datosCXC[0], Integer.parseInt(liquidacion.get(i - 1).getItem()), item, "INTERESES")); // factura_detalle
            }
            if (liquidacion.get(i - 1).getCustodia() > 0) {
                item++;
                valor_item = liquidacion.get(i - 1).getCustodia();
                bg.setValor_03(Math.round(valor_item) + "");
                query.add(list.ingresarDetalleCXCNegocio(bg, datosCXC[0], Integer.parseInt(liquidacion.get(i - 1).getItem()), item, "CUSTODIA")); // factura_detalle
            }
            if (liquidacion.get(i - 1).getRemesa() > 0) {
                item++;
                valor_item = liquidacion.get(i - 1).getRemesa();
                bg.setValor_03(Math.round(valor_item) + "");
                query.add(list.ingresarDetalleCXCNegocio(bg, datosCXC[0], Integer.parseInt(liquidacion.get(i - 1).getItem()), item, "REMESA")); // factura_detalle
            }
            if (liquidacion.get(i - 1).getSeguro() > 0) {
                item++;
                valor_item = liquidacion.get(i - 1).getSeguro();
                bg.setValor_03(Math.round(valor_item) + "");
                query.add(list.ingresarDetalleCXCNegocio(bg, datosCXC[0], Integer.parseInt(liquidacion.get(i - 1).getItem()), item, "SEGURO-DEUDOR")); // factura_detalle
            }
            
            if (liquidacion.get(i - 1).getCuota_manejo() > 0) {
                item++;
                valor_item = liquidacion.get(i - 1).getCuota_manejo();
                bg.setValor_03(Math.round(valor_item) + "");
                query.add(list.ingresarDetalleCXCNegocio(bg, datosCXC[0], Integer.parseInt(liquidacion.get(i - 1).getItem()), item, "CUOTA-ADMINISTRACION")); // factura_detalle
            }
            
            if (liquidacion.get(i - 1).getAval() > 0) {
                item++;
                valor_item = liquidacion.get(i - 1).getAval();
                bg.setValor_03(Math.round(valor_item) + "");
                query.add(list.ingresarDetalleCXCNegocio(bg, datosCXC[0], Integer.parseInt(liquidacion.get(i - 1).getItem()), item, "AVAL")); // factura_detalle
            }

        }
        list.UpCP(datosCXC[0]); //Actualiza el numero de la serie para la CXC

        // generar factura por valor de aval si no se financio y no es un negocio de aval
        if (!neg.isFinanciaAval() && !this.isNegocioAval(neg.getCod_negocio()) && !convenio.getPrefijo_negocio().equals("FINTRA")) {
            
            int item = 1;
            bg.setValor_03(neg.getvalor_aval() + "");
            query.add(list.ingresarCXCAvalNegocio(bg, datosCXC[0], 0, liquidacion));
            query.add(list.ingresarDetalleCXCNegocio(bg, datosCXC[0], 0, item, "AVAL")); // factura_detalle
        }

        return query;
    }

    public void updateEstadoNegocio(String neg, String login, String estado, String actividad) throws SQLException {
        list.updateEstadoNegocio(neg,login,estado,actividad);
    }

    public String obtenerUltimoConceptoTraza(String codneg) throws SQLException {
        return list.obtenerUltimoConceptoTraza(codneg);
    }

    public ArrayList anularCXPNegocio(String neg, String login) throws Exception {
        ArrayList lista = new ArrayList();
        try {
            String numCXP = list.obtenerNumCXPNegocio(neg);
            lista.add(list.anularCXPNegocio(neg,login));
            lista.add(list.anularCXPItemsDocNegocio(numCXP));
            lista.add(list.anularIngresoDifFenalco(neg));
            
        } catch (SQLException ex) {
            Logger.getLogger(NegociosGenService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    public String obtenerNegocioAval(String neg) throws SQLException {
        return list.obtenerNegocioAval(neg);
    }

    public String obtenerCxPAval(String negAval) throws SQLException {
        return list.obtenerCxPAval(negAval);
    }

    public boolean esTransferido(String nit_tercero) throws Exception {
        return list.esTransferido(nit_tercero);
    }

    public ArrayList<String> generarDocumentosNegocioSeguro(BeanGeneral bg, Usuario usuario, Negocios neg) throws Exception {
        ArrayList<String> listSql = new ArrayList<String>();
        
        //Obtener la informacion del convenio
        GestionConveniosDAO convdao = new GestionConveniosDAO(usuario.getBd());
        Convenio convenio = convdao.buscar_convenio(usuario.getBd(), bg.getValor_17());

        String[] datosCXC = list.buscarCuentasCXC(bg.getValor_17(), bg.getValor_18(), bg.getValor_16());

        //Guardar prefijos y cuentas
        bg.setValor_20(datosCXC[0]); //Prefijo
        bg.setValor_21(datosCXC[1]); //Cuenta CXC
        bg.setValor_22(datosCXC[2]); //HC CXC
        bg.setValor_23(convenio.getHc_cxp()); //HC CXP
        bg.setValor_24(convenio.getCuenta_cxp()); //Cuenta CXP
        int numCuotas = Integer.parseInt(bg.getValor_02()); //numero de cuotas del negocio

        //Ciclo para generar las facturas y la CxP
        for (int i = 1; i < numCuotas + 1; i++) {
            int item = 1;
            listSql.add(list.ingresarCXCNegocio(bg, datosCXC[0], i, i, this.getLiquidacion(), convenio.isMediador_aval()));
            double valor_item = this.getLiquidacion().get(i - 1).getCapital();
            bg.setValor_03(Math.round(valor_item) + "");
            bg.setValor_21(datosCXC[1]);
            listSql.add(list.ingresarDetalleCXCNegocio(bg, datosCXC[0], i, item, "CAPITAL")); // factura_detalle
            if (!convenio.isMediador_aval()) {
                item++;
                valor_item = this.getLiquidacion().get(i - 1).getInteres();
                bg.setValor_03(Math.round(valor_item) + "");
                listSql.add(list.ingresarDetalleCXCNegocio(bg, datosCXC[0], i, item, "INTERESES")); // factura_detalle
            }
            if (this.getLiquidacion().get(i - 1).getCustodia() > 0) {
                item++;
                valor_item = this.getLiquidacion().get(i - 1).getCustodia();
                bg.setValor_03(Math.round(valor_item) + "");
                listSql.add(list.ingresarDetalleCXCNegocio(bg, datosCXC[0], i, item, "CUSTODIA")); // factura_detalle
            }
            if (this.getLiquidacion().get(i - 1).getRemesa() > 0) {
                item++;
                valor_item = this.getLiquidacion().get(i - 1).getRemesa();
                bg.setValor_03(Math.round(valor_item) + "");
                listSql.add(list.ingresarDetalleCXCNegocio(bg, datosCXC[0], i, item, "REMESA")); // factura_detalle
            }
            if (this.getLiquidacion().get(i - 1).getSeguro() > 0) {
                item++;
                valor_item = this.getLiquidacion().get(i - 1).getSeguro();
                bg.setValor_03(Math.round(valor_item) + "");
                listSql.add(list.ingresarDetalleCXCNegocio(bg, datosCXC[0], i, item, "SEGURO")); // factura_detalle
            }
            //Ingresa la CXP afiliado se valida que no es un negocio de aval
            if (!this.isNegocioAval(neg.getCod_negocio())) {
                listSql.add(list.ingresarCXPNgSeguro(bg, convenio, i,this.getLiquidacion().get(i - 1).getFecha()));
                listSql.add(list.ingresarDetalleCXPNgSeguro(bg, convenio, i));
            }

        }
        list.UpCP(datosCXC[0]); // Actualiza el numero de la serie para la CXC
        list.UpCP(convenio.getPrefijo_cxp()); // Actualiza el numero de la serie para la CXP
        String codcli = bg.getValor_01();
        
        if (!convenio.getId_convenio().equals("35")) {
        // generar factura por valor de aval si no se financio y no es un negocio de aval
            if (!neg.isFinanciaAval() && !this.isNegocioAval(neg.getCod_negocio())) {
                int item = 1;
                bg.setValor_03(neg.getvalor_aval() + "");
                listSql.add(list.ingresarCXCAvalNegocio(bg, datosCXC[0], 0, this.getLiquidacion()));
                listSql.add(list.ingresarDetalleCXCNegocio(bg, datosCXC[0], 0, item, "AVAL")); // factura_detalle
            }

        //Ingresa la CXP de aval
        //creamos uan variable para almacenar el nit del cliente.
            if ((!neg.isFinanciaAval()) || isNegocioAval(neg.getCod_negocio())) {

                if (!neg.getNegocio_rel().equals("")) {
                    bg.setValor_29(neg.getVr_negocio() + "");// valor negocio de aval
                } else {
                    bg.setValor_29(neg.getvalor_aval() + "");// valor aval
                }
                String documento = list.UpCP(convenio.getPrefijo_cxp_avalista());//obtiene el numero de documento para la CXP
                //almacena el nit original del cliente.
                bg.setValor_01(convenio.getNit_anombre());// nit
                bg.setValor_23(convenio.getHc_cxp_avalista());
                bg.setValor_24(convenio.getCuenta_cxp_avalista());
                String fechaActual = Util.getFechaActual_String(4);
                bg.setValor_30(fechaActual);
                listSql.add(list.ingresarCXPAval(bg, documento, convenio));
                listSql.add(list.ingresarDetalleCXPAval(bg, documento, convenio));
            }
        }

        //Inserta los ingresos diferidos
        if (!convenio.isMediador_aval()) {
            InteresesFenalcoDAO intdao = new InteresesFenalcoDAO(this.list.getDatabaseName());
            //metodo para insertar los ingresos insertarIngFenalco(cod_neg, cod_cli, usuario, documentType, cmc);
            listSql.addAll(intdao.insertarIngFenalco(bg.getValor_06(), codcli, bg.getValor_04(), convenio.getPrefijo_diferidos(), convenio.getHc_diferidos()));
        }

        return listSql;
    }
    
    public ArrayList<CmbGeneralScBeans> cargarUnidadesNegocio(String area) throws Exception{
        return list.cargarUnidadesNegocio(area);
    }

    public boolean isCxPAval(String cxp) throws Exception {
        return list.isCxPAval(cxp);
    }

    public int obtenerCicloFacturacion(String vcodn) throws SQLException {
        return list.obtenerCicloFacturacion(vcodn);
    }

    public void actualizarCicloAval(String vcodn, int ciclo) throws SQLException {
        list.actualizarCicloAval(vcodn,ciclo);
    }

    public void actualizarCicloSeguros(String vcodn, int ciclo) throws SQLException {
        list.actualizarCicloSeguros(vcodn,ciclo);
    }

    public void actualizarCicloGps(String vcodn, int ciclo) throws SQLException {
        list.actualizarCicloGps(vcodn,ciclo);
    }

    public void actualizarCicloNegocio(String vcodn, int ciclo) throws SQLException {
        list.actualizarCicloNegocio(vcodn,ciclo);
    }
   
    public boolean permisoGeneraPagare(Negocios neg) {
        try {
            return list.permisosGenerarPagare(neg);
        } catch(Exception e) {
            return false;
        }
    }
    
    public ArrayList generarDocumentosCXCReestructuracionFenalco(BeanGeneral bg, Usuario usuario, Negocios neg, ArrayList<DocumentosNegAceptado> liquidacion) throws Exception {

        ArrayList query = new ArrayList();

        //Obtener la informacion del convenio
        GestionConveniosDAO convdao = new GestionConveniosDAO(usuario.getBd());
        Convenio convenio = convdao.buscar_convenio(usuario.getBd(), bg.getValor_17());

        String[] datosCXC = list.buscarCuentasCXC(bg.getValor_17(), bg.getValor_18(), bg.getValor_16());

        //Guardar prefijos y cuentas
        bg.setValor_20(datosCXC[0]); //Prefijo
        bg.setValor_21(datosCXC[1]); //Cuenta CXC
        bg.setValor_22(datosCXC[2]); //HC CXC
        bg.setValor_23(convenio.getHc_cxp()); //HC CXP
        bg.setValor_24(convenio.getCuenta_cxp()); //Cuenta CXP
        int numCuotas = Integer.parseInt(bg.getValor_02()); //numero de cuotas del negocio

        //Ciclo para generar las facturas
        for (int i = 1; i < numCuotas + 1; i++) {
            int item = 1;
            query.add(list.ingresarCXCNegocio(bg, datosCXC[0], i,i, liquidacion, convenio.isMediador_aval()));

            double valor_item = liquidacion.get(i - 1).getCapital();
            bg.setValor_03(Math.round(valor_item) + "");
            bg.setValor_21(datosCXC[1]);
            query.add(list.ingresarDetalleCXCNegocio(bg, datosCXC[0], i, item, "CAPITAL")); // factura_detalle

            if (!convenio.isMediador_aval()) {
                item++;
                valor_item = liquidacion.get(i - 1).getInteres();
                bg.setValor_03(Math.round(valor_item) + "");
                query.add(list.ingresarDetalleCXCNegocio(bg, datosCXC[0], i, item, "INTERESES")); // factura_detalle
            }
            if (liquidacion.get(i - 1).getCustodia() > 0) {
                item++;
                valor_item = liquidacion.get(i - 1).getCustodia();
                bg.setValor_03(Math.round(valor_item) + "");
                query.add(list.ingresarDetalleCXCNegocio(bg, datosCXC[0], i, item, "CUSTODIA")); // factura_detalle
            }
            if (liquidacion.get(i - 1).getRemesa() > 0) {
                item++;
                valor_item = liquidacion.get(i - 1).getRemesa();
                bg.setValor_03(Math.round(valor_item) + "");
                query.add(list.ingresarDetalleCXCNegocio(bg, datosCXC[0], i, item, "REMESA")); // factura_detalle
            }
            if (liquidacion.get(i - 1).getSeguro() > 0) {
                item++;
                valor_item = liquidacion.get(i - 1).getSeguro();
                bg.setValor_03(Math.round(valor_item) + "");
                query.add(list.ingresarDetalleCXCNegocio(bg, datosCXC[0], i, item, "SEGURO")); // factura_detalle
            }

        }
        list.UpCP(datosCXC[0]); //Actualiza el numero de la serie para la CXC
        
        return query;
    }
    
    public String estadoReestructuracion(String negocio) {
        try {
            return list.estadoReestructuracion(negocio);
        } catch(Exception e) {
            return "";
        }
    }
    
      public void  insert_cxp_banco_transf(String documento, String bank, Double valor_cxp, Usuario usuario)throws Exception{   
             list.insert_cxp_banco_transf(documento, bank, valor_cxp, usuario);        
     }
      
      
    public double getValor_cxp() {
        return valor_cxp;
    }

    public void setValor_cxp(double valor_cxp) {
        this.valor_cxp = valor_cxp;
    }
    
    public String esCompraCartera(String negocio,Usuario usuario) throws SQLException {
        return list.esCompraCartera(negocio, usuario);
    }    
    
     /**
     * M�todo que trae los negocios pendientes  para la creacion de la cxc de cuota de manejo por fecha de vencimiento
     * @param ciclo
     * @param periodo
     * @return 
     * @throws java.lang.Exception
     * @autor.......mcastillo
     * @throws......Exception
     * @version.....1.0.
     **/
    public ArrayList<BeanGeneral> datosNegCuotaManejo(String ciclo, String periodo) throws Exception {
        return list.datosNegCuotaManejo(ciclo,periodo);
    }
    
    public String updateCuotaManejoCausado(double valor, String fecha, String negocio, String item)  throws Exception {
        return list.updateCuotaManejoCausado(valor, fecha, negocio, item);
    }
    
    public Double getValor_Cuota_Manejo(String id_convenio) throws Exception {
         return list.obtenerValorCuotaManejo(id_convenio);
    }

    public ArrayList<BeanGeneral> cuotaManejoPendientes(String negocio) throws Exception {
        return list.cuotaManejoPendientes(negocio);
    }
    
    public boolean existeValorFianza(String id_convenio, int plazo) {
        try {
            return list.existeValorFianza(id_convenio, plazo);
        } catch(Exception e) {
            return false;
        }
    }

    public double obtenerValorFianza(String id_convenio, double vlr_negocio, int plazo, String numSolc,int id_producto, int id_cobertura,String nit_empresa) throws Exception { 
         return list.obtenerValorFianza(id_convenio, vlr_negocio, plazo, numSolc,id_producto, id_cobertura, nit_empresa);      
    }
     
  public String obtenerUltimoPagareVigenteCliente(String negocio, String nit_solicitante, String nit_codeudor, String nit_estudiante, String nit_afiliado) throws Exception {
       return list.obtenerUltimoPagareVigenteCliente(negocio, nit_solicitante, nit_codeudor, nit_estudiante, nit_afiliado);
   }
  
    public void insertarHistoricoPagare(Negocios neg, String nit_solicitante, String nit_codeudor, String nit_estudiante, String pagare, String solicitud, String nit_afiliado, Usuario usuario) throws Exception {
        list.insertarHistoricoPagare(neg, nit_solicitante, nit_codeudor, nit_estudiante, pagare, solicitud, nit_afiliado, usuario);
    }
    
    public JsonObject getInfoCuentaEnvioCorreoAprob(){
        return list.getInfoCuentaEnvioCorreoAprob();
    }
    
    public String obtenerMailEnvioCorreoAprobCredito(String num_solicitud) throws SQLException {
        return list.obtenerMailEnvioCorreoAprobCredito(num_solicitud);
    }
    
    public void insertarNuevoPagare(Negocios neg, String solicitud, Usuario usuario) throws Exception {
        list.insertarNuevoPagare(neg, solicitud, usuario);
    }
    
    public String obtenerTipoCliente(String identificacion) {
        try {
            return list.obtenerTipoCliente(identificacion);
        } catch (Exception e) {
            return "";
        }
    }
    
    public String obtenerUltimaFechaPago(String identificacion) {
        try {
            return list.obtenerUltimaFechaPago(identificacion);
        } catch (Exception e) {
            return (com.tsp.util.Util.AnoActual() + "-" + com.tsp.util.Util.getFechaActual_String(3) + "-" + com.tsp.util.Util.getFechaActual_String(5));
        }
    }
    
    public ArrayList obtenerValorPimeraCuota(String cod_neg)throws Exception {
        return list.obtenerValorPimeraCuota(cod_neg);
    }
    
    private void agregarDiferidoNegocio(TransaccionService tService,InteresesFenalcoDAO intdao,
                                        BeanGeneral bg,CargosFijosConvenios cfc, String select_query,
                                        String insert_query, int i, String cuota) throws Exception{
        
        //Generamos los diferido para todas la cuotas en una iteracion.
        if (i == 1) {
            ArrayList<String> diferidoConcepto = intdao.insertarIngFenalcoCargosFijos(select_query, insert_query,
                                                            bg.getValor_06(), bg.getValor_01(), bg.getValor_04(),
                                                            cfc.getPrefijo(), cfc.getHc_diferido(),bg.getValor_23(), cuota);
            //agregamos los querys en tService.Batch()
            for (String StrinQuery : diferidoConcepto) {
                tService.getSt().addBatch(StrinQuery);
            }
        }// fin diferidos 
   
    }
    
    
    public BeanGeneral buscarPreaprobadoform(String identificacion) throws SQLException {
        return list.buscarPreaprobadoForm(identificacion);
       
    }
    
      public String generarDocumentosCompraCartera(BeanGeneral bg, Usuario usuario,int idconvenio) throws SQLException {
       
         return list.GenerarDocumentosCompraCartera(bg, idconvenio);             
             
       
      }
      
      public String perfeccionarMicrocredito(String negocio,Usuario usuario) throws SQLException {
        return list.perfeccionarMicrocredito(negocio ,usuario);
     }
      
      public String perfeccionarNegocioMicrocredito(String negocio,Usuario usuario) throws SQLException {
        return list.perfeccionarNegocioMicrocredito(negocio ,usuario);
    }
      
       public String marcarCXPGeneradoChequeMicro(String cxp, String negocios, Usuario usuario) throws SQLException {
        return list.marcarCXPGeneradoChequeMicro(cxp, negocios, usuario);
    }
       public boolean validarcompracartera(String form) throws SQLException, Exception {
        return list.validarcompracartera(form);
    }
       
        public String uptdatefehadesem(String negocio)throws ServletException, InformationException,Exception{
       return list.uptdatefehadesem(negocio);
        
     }
        
    public String generar_Nc_Y_Cxps_Polizas(Negocios negocio,Usuario usuario) throws SQLException{
      return list.generar_Nc_Y_Cxps_Polizas(negocio,usuario);
    }

    private double obtenerValorPoliza(int id_convenio, String id_Sucursal, double capital, int nodocs, String fechaItem, String tipo_cuota, String agencia, String fechaBaseRenovacion, String renovacion, String codigo,Usuario usuario) throws Exception {
    return list.obtenerValorPoliza(id_convenio,id_Sucursal,capital,nodocs,fechaItem,tipo_cuota,agencia,fechaBaseRenovacion,renovacion,codigo,usuario); 
        
    }

    public double total_valor_poliza(String numSolc) throws Exception {
        return list.total_valor_poliza(numSolc);
    }

    private double obtener_valor_poliza_financiada(String numSolc) throws Exception {
       return list.obtener_valor_poliza_financiada(numSolc);
    }

    public String anular_documentos_educativo_renovado(SolicitudAval solicitud, String usuario) {
        return list.anular_documentos_educativo_renovado(solicitud,usuario);
    }
    
     public ArrayList<BeanGeneral> cargarNegociosArchivoPlano(String negocio, String banco) throws SQLException {
         return list.cargarNegociosArchivoPlano(negocio,banco);
    }

    public void liquidarNegocioMicrocredito(Negocios negocio, SolicitudAval solicitud, Convenio convenio) throws SQLException {
      list.liquidarNegocioMicrocredito(negocio, solicitud, convenio);
    }

    public void anular_documentos_micro_retanqueo(SolicitudAval solicitud, String login){
        list.anular_documentos_micro_retanqueo(solicitud, login);
    }
    
        /**
     * Busca los detalles de las cuotas de un negocio
     * @param codNegocio codigo del negocio a buscar
     * @param tipo_liquidacion
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList buscarDetallesNegocio(String codNegocio, String tipo_liquidacion) throws Exception {
        return list.buscarDetallesNegocio(codNegocio, tipo_liquidacion);
    }

    public String serialNegocios(String prefijo) throws Exception {
     return list.serialNegocios(prefijo);
    }

    public double obtenerTir(int numcuotas,double tcuota,double valornegocio,String fechaliqui,String fechaPrimeraCuota)throws Exception {
       return list.obtenerTir(numcuotas,tcuota,valornegocio,fechaliqui,fechaPrimeraCuota);
    }

    public ArrayList buscarDetallesNegocioRefi(String codNegocio, String tipo_liq) throws Exception {
     return list.buscarDetallesNegocio(codNegocio,tipo_liq);   
    }
    
    public boolean generarCarteraLibranzas(String codNegocio, Usuario user) throws Exception {
     return list.generarCarteraLibranza(codNegocio,user);   
    }
}
