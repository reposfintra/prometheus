/***************************************************************************
 * Nombre clase : ............... ReporteTarjetaService.java               *
 * Descripcion :................. Clase que maneja los Servicios           *
 *                                asignados a Model relacionados con el    *
 *                                programa de RegistroTarjeta              *
 * Autor :....................... Ing. Juan Manuel Escandon Perez          *
 * Fecha :........................ 6 de diciembre de 2005, 08:38 AM        *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.sql.*;
/**
 *
 * @author  JuanM
 */
public class Caja_lecService {
    /*
     *Declaracion de atributos
     */
    private Caja_lecDAO  RTDataAccess;
    private List         ListaRT;
    private Caja_lec     Datos;
    /** Creates a new instance of RegistroTarjetaService */
    public Caja_lecService() {
        RTDataAccess    = new Caja_lecDAO();
        ListaRT         = new LinkedList();
        Datos           = new Caja_lec();
    }
    
    /**
     * Metodo Insert, a�ade un nuevo registro a la tabla RegistroTarjeta
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String dstrct, String num_tarjeta , String placa, String cedula, String creation_user, String user_update
     * @see   : Insert - RegistroTarjetaDAO
     * @version : 1.0
     */
    public void Insert(String num_caja, String cliente, String descripcion, String dato, String creation_user, String user_update) throws Exception {
        try{
            RTDataAccess.INSERT(num_caja, cliente, descripcion, dato, creation_user, user_update);
        }
        catch(Exception e){
            throw new Exception("Error en Insert [Caja_lecService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Metodo UpdateEstado, modifica el campo reg_status en la tabla RegistroTarjeta
     * para un reg_status y una placa, std_job_no correspondiente
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String reg_status, String placa , String std_job
     * @see   : UpdateEstado - FlotaDAO
     * @version : 1.0
     */
    public void UpdateEstado(String reg_status, String num_caja, String usuario) throws Exception {
        try{
            RTDataAccess.CESTADO( reg_status, num_caja, usuario );
        }
        catch(Exception e){
            throw new Exception("Error en Update [Caja_lecService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Metodo Delete, elimina un registro de la tabla RegistroTarjeta
     * para un num_tarjeta, cedula  y una fecha_creacion correspondiente
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String num_tarjeta , String std_job, String cedula
     * @see   : Delete - FlotaDAO
     * @version : 1.0
     */
    public void Delete( String num_caja ) throws Exception {
        try{
            RTDataAccess.DELETE( num_caja );
        }
        catch(Exception e){
            throw new Exception("Error en Delete [Caja_lecService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Metodo List, lista todos los registros  no anulados de la tabla registrotarjeta
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String placa
     * @see   : ListxPlaca - FlotaDAO
     * @version : 1.0
     */
    public void List() throws Exception {
        try{
            ReiniciarLista();
            ListaRT = RTDataAccess.LIST();
        }
        catch(Exception e){
            throw new Exception("Error en List [Caja_lecService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Metodo List, lista los registros la tabla registrotarjeta de acuerdo a los parametros de busqueda
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String placa
     * @see   : ListxPlaca - FlotaDAO
     * @version : 1.0
     */
    public void ListBus( String num_caja, String cliente, String descripcion , String dato ) throws Exception {
        try{
            ReiniciarLista();
            ListaRT = RTDataAccess.LISTBUSQUEDA(num_caja, cliente, descripcion, dato);
        }
        catch(Exception e){
            throw new Exception("Error en ListBus [Caja_lecService]...\n"+e.getMessage());
        }
    }
    
    
    
    /**
     * Metodo Buscar, retorna un true si encuentra un registro dentro de la
     * tabla RegistroTarjeta, para un num_tarjeta, y una fecha_creacion correspondiente
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String num_tarjeta, String cedula, String fecha_creacion
     * @see   : Buscar - RegistroTarjetaDAO
     * @version : 1.0
     */
    public boolean Buscar( String num_caja ) throws SQLException {
        try{
            return RTDataAccess.BUSCARCAJA( num_caja );
        }
        catch(Exception e){
            throw new SQLException("Error en Buscar [Caja_lecService]...\n"+e.getMessage());
        }
    }
    
    
    
    
    
    /**
     * Metodo Buscar, retorna un true si encuentra un registro dentro de la
     * tabla RegistroTarjeta, para un num_tarjeta, y una fecha_creacion correspondiente
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String num_tarjeta, String cedula, String fecha_creacion
     * @see   : Buscar - RegistroTarjetaDAO
     * @version : 1.0
     */
    public void Search( String num_caja ) throws SQLException {
        try{
            this.ReiniciarDato();
            this.Datos = RTDataAccess.SEARCHCAJA( num_caja );
        }
        catch(Exception e){
            throw new SQLException("Error en Search [Caja_lecService]...\n"+e.getMessage());
        }
    }
    
    
    /**
     * Metodo Update, modifica un registro en la tabla RegistroTarjeta
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String nnum_tarjeta, String nplaca, String ncedula, String ncreation_user, String nuser_update, String num_tarjeta, String cedula, String fecha_creacion
     * @see   : Update - FlotaDAO
     * @version : 1.0
     */
    public void Update( String num_caja, String nnum_caja, String ncliente, String ndescripcion, String ndato, String nuser_update ) throws Exception {
        try{
            RTDataAccess.UPDATE( num_caja, nnum_caja, ncliente, ndescripcion, ndato, nuser_update );
        }catch(Exception e){
            throw new Exception("Error en Update [Caja_lecService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Metodo secuencia, metodo que devuelve la secuencia de grabacion de acuerdo el numero de caja
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String num_tarjeta
     * @see   : secuencia - Caja_lecDAO
     * @version : 1.0
     */
    public String secuencia( String num_caja ) throws Exception {
        try{
            return RTDataAccess.secuencia(num_caja);
        }catch(Exception e){
            throw new Exception("Error en secuencia [Caja_lecService]...\n"+e.getMessage());
        }
    }
    
    public void ReiniciarDato(){
        this.Datos = null;
    }
    public void ReiniciarLista(){
        this.ListaRT = new LinkedList();
    }
    
    public Caja_lec getDato(){
        return this.Datos;
    }
    
    public List getList(){
        return this.ListaRT;
    }
    
}
