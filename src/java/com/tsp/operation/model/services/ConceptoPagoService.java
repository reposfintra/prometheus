/******************************************************************
 * Nombre ......................ConceptoPagoService.java
 * Descripci�n..................Conceptos de pago
 * Autor........................Armando Oviedo
 * Fecha........................10/10/2005
 * Versi�n......................1.0
 * Coyright.....................Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;

public class ConceptoPagoService {
    
    private ConceptoPagoDAO cpsdao;
    private List    ListaFolder;
    private List    ListaTraza;
    private List    ListaFolder2;
    private List    ListaTraza2;
    private String  raiz;
    private List        opcionesMenu;
    private String      codeJSOpcionesMenu;
    private String varSeparadorJS = "~";
    private ConceptoPago conceptopago;
    
    /** Creates a new instance of ConceptoPagoService */
    public ConceptoPagoService() {
        cpsdao = new ConceptoPagoDAO();
        reiniciar();
        opcionesMenu = null;
        codeJSOpcionesMenu = "";
    }
    public ConceptoPagoService(String dataBaseName) {
        cpsdao = new ConceptoPagoDAO(dataBaseName);
        reiniciar();
        opcionesMenu = null;
        codeJSOpcionesMenu = "";
    }
        
    /**
     * M�todo utilizado para insertar un nuevo concepto
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @see.........ConceptoPagoDAO.java
     * @param.......ConceptoPago tmp
     * @version.....1.0.
     */
    public void insert(ConceptoPago tmp) throws SQLException{
        try{
            cpsdao.setCP(tmp);
            cpsdao.insert();
        }
        catch(SQLException ex){
            throw new SQLException(ex.getMessage());
        }
    }
    
    /**
     * M�todo utilizado para actualizar un concepto de pago
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     * @see.........ConceptoPagoDAO.java
     * @param.......ConceptoPago tmp
     */
    public void update(ConceptoPago tmp) throws SQLException{
        try{
            cpsdao.setCP(tmp);
            cpsdao.update();
        }
        catch(SQLException ex){
            throw new SQLException(ex.getMessage());
        }
    }
    /**
     * M�todo utilizado para borrar un concepto de pago
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     * @param.......String codigo
     * @see.........ConceptoPagoDAO.java
     */
    public void delete(String codigo) throws Exception{
        try{
            cpsdao.delete(codigo);
        }
        catch(SQLException ex){
            throw new SQLException(ex.getMessage());
        }
    }
    
    /**
     * M�todo utilizado para retornar la estructura
     * del menu en una lista
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     * @see.........ConceptoPagoDAO.java
     * @return......List lista
     */
    public List mostrarContenido() throws SQLException{
        return cpsdao.mostrarContenido();
    }
    
    /**
     * M�todo utilizado para retornar todos los hijos de un padre
     * @autor.......Armando Oviedo
     * @throws......Exception
     * @version.....1.0.
     * @param.......String id
     * @see.........ConceptoPagoDAO.java
     * @return......List lista
     */
    public void mostrarContenido(String id) throws Exception{
        ListaFolder = cpsdao.mostrarContenido(id);
    }
    
    /**
     * M�todo utilizado para retornar el siguiente id
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     * @param.......String codpadre
     * @see.........ConceptoPagoDAO.java
     * @return......String nextId
     */
    public String nextId(String codpadre)throws Exception{
        return cpsdao.nextId(codpadre);
    }
    
    /**
     * M�todo utilizado para actualizar el contenido del menu
     * @autor.......Armando Oviedo
     * @throws......Exception
     * @see.........ConceptoPagoDAO.java
     * @version.....1.0.
     */
    public void actualizarContenido() throws Exception {
        try{
            ConceptoPago cp = getUTraza();
            ListaFolder = cpsdao.mostrarContenido(  (cp!=null?cp.getCodigo():"0") );
        }
        catch (Exception ex){
            throw new Exception("Error en ActualizarContenido [MenuService] ... \n" + ex.getMessage());
        }
    }
    
    /**
     * M�todo utilizado para adicionar un elemento a la traza
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @param.......ConceptoPago tmp
     */
    public void addTraza(ConceptoPago tmp){
        if (ListaTraza==null) ListaTraza = new LinkedList();
        ListaTraza.add(tmp);
    }
    
    /**
     * M�todo utilizado para remover un elemento en la traza
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @see.........ConceptoPagoDAO.java
     */
    public void removeTraza(){
        if (ListaTraza!=null && ListaTraza.size()>0) ListaTraza.remove(ListaTraza.size()-1);
    }
    
    /**
     * M�todo utilizado para obtener un elemento de la traza
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @see.........ConceptoPagoDAO.java
     * @return......ConceptoPago
     */
    public ConceptoPago getUTraza(){
        return ((ListaTraza!=null && ListaTraza.size()>0)? (ConceptoPago) ListaTraza.get(ListaTraza.size()-1):null);
    }
    
    /**
     * M�todo utilizado para retornar un array de strings
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @param.......String arreglo[]
     * @return......String arreglo[]
     */
    public String arrayToString(String [] arreglo){
        String lista = "";
        for (int i=0;arreglo!=null && i<arreglo.length;lista+=arreglo[i]+",",i++);
        return (lista.equals("")?"":(lista.substring(0, lista.length()-1)));
        
    }
    
    /**
     * M�todo utilizado para retornar una lista de folders
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @return......List ListaFolder
     */
    public List getListaFolder(){
        return ListaFolder;
    }
    
    /**
     * M�todo utilizado para retornar el siguiente id
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @return......List listatraza
     */
    public List getListaTraza(){
        return ListaTraza;
    }
    
    /**
     * M�todo utilizado para obtener el nombre de la raiz
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @return......String raiz
     */
    public String getRaiz(){
        return raiz;
    }
    
    /**
     * M�todo utilizado para retornar el separador
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @return......String separador
     */
    public String getVarJSSeparador(){
        return "\n var SeparadorJS = '" + this.varSeparadorJS + "';";
    }
    
    /**
     * M�todo utilizado para retornar el trazado
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @return......String trazado
     */
    public String getTrazado(){
        String traza = "/";
        if (ListaTraza!=null){
            for (int i=0;i<ListaTraza.size();i++){
                ConceptoPago cp = (ConceptoPago) ListaTraza.get(i);
                traza += cp.getDescripcion() + "/";
            }
        }
        return traza;
    }
    
    /**
     * M�todo utilizado para cargar el menu
     * @autor.......Armando Oviedo
     * @throws......Exception
     * @version.....1.0.
     */
    public void load()  throws Exception {
        try{
            searchMenu();
            generateMenu();
        }
        catch( Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * M�todo utilizado para buscar el menu
     * @autor.......Armando Oviedo
     * @throws......Exception
     * @version.....1.0.
     * @see.........ConceptoPagoDAO.java
     */
    public void searchMenu() throws Exception{
        try{
            this.opcionesMenu  = this.cpsdao.mostrarContenido();
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * M�todo utilizado para generar el menu
     * @autor.......Armando Oviedo
     * @throws......Exception
     * @version.....1.0.
     */
    public void generateMenu() throws Exception{
        try{
            if (opcionesMenu!=null){
                Iterator it =  opcionesMenu.iterator();
                String   code    = "", sw="";
                int level   = 0;
                while(it.hasNext()){
                    ConceptoPago tmp = (ConceptoPago) it.next();
                 /*if( tmp.getId_folder().equalsIgnoreCase("Y") && tmp.getNivel()==1)
                    if( !sw.equals("")  && !tmp.getCodigo().equals(sw)){
                        String var = "\n\t  " + (sw.equals("0")?"menu": "op" + sw);//(sw.split(" "))[0];
                        //code += codeCierre( var );
                    }  */
                    code += (tmp.getId_folder().equalsIgnoreCase("Y")?this.codeFolder(tmp): this.codeNodo(tmp));
                    //sw    = (tmp.getId_folder().equalsIgnoreCase("Y") && tmp.getNivel()==1)? tmp.getCodigo(): sw;
                }
                this.codeJSOpcionesMenu = code;
            }
        }
        catch(Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * M�todo utilizado para retornar el siguiente id
     * @autor.......Armando Oviedo
     * @throws......Exception
     * @version.....1.0.
     * @param.......ConceptoPago tmp
     * @see.........ConceptoPagoDAO.java
     * @return......String js
     */
    public String codeFolder(ConceptoPago tmp) throws Exception{
        String js = "";
        try{
            String var = (tmp.getCodigo().equals("0")?"menu": "op" +tmp.getCodigo());
            String padreName = (tmp.getCodpadre().equals("0")?"menu": "op" +tmp.getCodpadre());
            
            js=
            "\n\t  " + padreName +".addItem('"+ tmp.getDescripcion() +"','','text','',''); "+
            "\n\t  var " + var +" = null;                               "+
            "\n\t  " + var +"     = new MTMenu();                       "+
            "\n\t  " + padreName +".makeLastSubmenu("+ var +",false);   ";
        }
        catch(Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        return js;
    }
    
    /**
     * M�todo utilizado para retornar el codigo del nodo
     * @autor.......Armando Oviedo
     * @throws......Exception
     * @version.....1.0.
     * @param.......ConceptoPago tmp
     * @return......String js
     */
    public String codeNodo(ConceptoPago tmp) throws Exception{
        String js = "";
        try{
            String padreName = (tmp.getCodpadre().equals("0")?"menu": "op" + tmp.getCodpadre());
            js =  "\n\t  " + padreName +".addItem('" + tmp.getDescripcion()+"','"+ tmp.getControlparam().replaceAll("\n","") +"','text','',''); \n";
        }
        catch(Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        return js;
    }
    
    /**
     * M�todo utilizado para retornar el ultimo submenu
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @param.......String var
     * @return......String js
     */
    public String codeCierre(String var){
        String js   = " menu.makeLastSubmenu("+ var +",false); \n ";
        return js;
    }
    
    /**
     * M�todo utilizado para retornar el menu completo
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @return......String codeJSOpcionesMenu
     */
    public String getMenuJsp(){
        return codeJSOpcionesMenu;
    }
    
    /**
     * M�todo utilizado para reiniciar los listados
     * @autor.......Armando Oviedo
     * @version.....1.0.
     */
    public void reiniciar(){
        this.ListaFolder     = null;
        this.ListaTraza      = null;
        this.raiz = "0";
    }
    
    /**
     * M�todo utilizado para buscar los hijos de un padre
     * @autor.......Armando Oviedo
     * @throws......Exception
     * @version.....1.0.
     * @param.......String codpadre
     * @return......String nompadre
     */
    public void buscarHijos(String codpadre, String oculto) throws Exception{
        Connection con = null;
        cpsdao.reiniciarListaFolders();
        cpsdao.buscarHijos(con, codpadre, oculto);
    }
    
    /**
     * M�todo utilizado para obtener la lista de folders
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @return......List lista
     */
    public List getSoloFolders(){
        return cpsdao.getListaFolders();
    }
    
    /**
     * M�todo utilizado para actualizar el folder de destino
     * @autor.......Armando Oviedo
     * @throws......Exception
     * @version.....1.0.
     * @param.......ConceptoPago tmp, String codpadre
     */
    public void updateDestFolfer(ConceptoPago tmp, String codpadre) throws Exception{
        ConceptoPago padre = cpsdao.getConcepto(codpadre);
        cpsdao.updateTreeStructure( padre.getNivel(), padre.getCodigo(), tmp.getCodigo());
    }
    
    /**
     * M�todo utilizado para buscar un concepto pago
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @param.......String codpadre
     * @see.........ConceptoPagoDAO
     * @return......ConceptoPago tmp
     */
    public void buscarConceptoPago(String codigo) throws SQLException{
        setConceptoPago(cpsdao.getConcepto(codigo));
    }
    
    /**
     * M�todo utilizado para setear un concepto de pago
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @param.......ConceptoPago conceptopago
     * @see.........ConceptoPagoDAO
     */
    public void setConceptoPago(ConceptoPago conceptopago){
        this.conceptopago = conceptopago;
    }
    
    /**
     * M�todo utilizado para obtener el objeto concepto pago
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @see.........ConceptoPagoDAO
     * @return......ConceptoPago tmp
     */
    public ConceptoPago getConceptoPago(){
        return this.conceptopago;
    }
    
    /**
     * M�todo utilizado para obtener el codigo del concepto
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     */
    public int getCodigoSerial() throws SQLException{
        return cpsdao.getCodigoSerial();
    }
    
    /**
     * M�todo que setea un concepto de pago por default
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @see.........ConceptoPagoDAO.java
     **/
    public void insertDefault() throws SQLException{
        cpsdao.insertDefault();
    }
    /**
     * M�todo utilizado para cargar el menu dada la carpeta y la agencia
     * @autor.......David Lamadrid M.
     * @throws......Exception
     * @version.....1.0.
     */
    public void load(String nombreCarpeta,String ag)  throws Exception {
        try{
            searchMenu();
            generateMenu2(nombreCarpeta,ag);
        }
        catch( Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * M�todo utilizado para generar el menu
     * @autor.......David lamadrid M
     * @throws......Exception
     * @version.....1.0.
     */
    public void generateMenu2(String nombreCarpeta,String ag) throws Exception{
        try{
            if (opcionesMenu!=null){
                Iterator it =  opcionesMenu.iterator();
                String   code    = "", sw="";
                int level   = 0;
                String desplegado="false";
                this.cpsdao.inicialisar(this.cpsdao.codigoPorNombre(nombreCarpeta));
                List lista=this.cpsdao.getLista();
                boolean sw1=false;
                while(it.hasNext()){
                    sw1=false;
                    ConceptoPago tmp = (ConceptoPago) it.next();
                    if(lista!=null){
                        for(int i=0;i <lista.size();i++){
                            ConceptoPago tmp1 = (ConceptoPago)lista.get(i);
                            if(tmp1.getDescripcion().equalsIgnoreCase(tmp.getDescripcion())){
                                sw1=true;
                            }
                        }
                    }
                    if(sw1==true){
                        code += (tmp.getId_folder().equalsIgnoreCase("Y")?this.codeFolder2(tmp,"true"): this.codeNodo1(tmp,ag));
                    }
                    else{
                        code += (tmp.getId_folder().equalsIgnoreCase("Y")?this.codeFolder2(tmp,"false"): this.codeNodo1(tmp,ag));
                    }
                    //sw    = (tmp.getId_folder().equalsIgnoreCase("Y") && tmp.getNivel()==1)? tmp.getCodigo(): sw;
                }
                this.codeJSOpcionesMenu = code;
            }
        }
        catch(Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * M�todo utilizado para retornar el siguiente id
     * @autor.......david Lamadrid
     * @throws......Exception
     * @version.....1.0.
     * @param.......ConceptoPago tmp
     * @see.........ConceptoPagoDAO.java
     * @return......String js
     */
    public String codeFolder2(ConceptoPago tmp,String desplegado) throws Exception{
        String js = "";
        try{
            String var = (tmp.getCodigo().equals("0")?"menu": "op" +tmp.getCodigo());
            String padreName = (tmp.getCodpadre().equals("0")?"menu": "op" +tmp.getCodpadre());
            js=
            "\n\t  " + padreName +".addItem('"+ tmp.getDescripcion() +"','','code','',''); "+
            "\n\t  var " + var +" = null;                               "+
            "\n\t  " + var +"     = new MTMenu();                       "+
            "\n\t  " + padreName +".makeLastSubmenu("+ var +","+desplegado+");   ";
        }
        catch(Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        return js;
    }
   
    /**
     * M�todo utilizado para retornar el codigo del nodo
     * @autor.......David Lamadrid
     * @throws......Exception
     * @version.....1.0.
     * @param.......ConceptoPago tmp   ,ag Agencia
     * @return......String js
     */
    public String codeNodo1(ConceptoPago tmp,String ag) throws Exception{
        String js = "";
        try{
            String padreName = (tmp.getCodpadre().equals("0")?"menu": "op" + tmp.getCodpadre());

            js =  "\n\t  " + padreName +".addItem('" + tmp.getDescripcion()+"','"+ tmp.getDescripcion() + ":" + tmp.getReferencia1().replaceAll("\n","") + ":" + tmp.getReferencia2().replaceAll("\n","") + "','code','',''); \n";
            // js =  js + "\n\t " + padreName +".onclick=new Function(\"asignar()\")";
        }
        catch(Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }        
        return js;
    }
    
     /**
     * M�todo getListaConcepto
     *@descripcion..metodopara obtener la lis te conceptos
     * @autor.......Ivan Gomez
     * @throws......SQLException
     * @version.....1.0.
     */
    public List getListaConcepto() throws SQLException{
        return cpsdao.getLista();
    }
      /**
     * M�todo BuscarConcepto  
     * @descripcion..Metodo para buscar el concepto de pago
     * @autor........Ivan Gomez
     * @throws.......SQLException
     * @see.........ConceptoPagoDAO
     * @version......1.0.
     * @return.......List lista
     */
    public void  BuscarConcepto(String concepto) throws Exception{
        try{
            cpsdao.BuscarConcepto(concepto); 
       
        } catch(Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    
      /**
     * M�todo BuscarReferencia3  
     * @descripcion..Metodo para buscar la referencia 3 
     * @autor........Ivan Gomez
     * @throws.......SQLException
     * @version......1.0.
     * @return.......List lista
     */
    public void BuscarReferencia3(String concepto,String cre) throws Exception{
        try{
            cpsdao.BuscarReferencia3(concepto,cre); 
       
        } catch(Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        } 
    }
    
    
}
