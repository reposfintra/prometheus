package com.tsp.operation.model.services;

import com.fintra.ws.datacredito.*;
import com.fintra.ws.interfaz.WSInterfaz;
import com.fintra.ws.interfaz.WSInterfaz_Service;
import com.tsp.operation.model.DAOS.TablaGenManagerDAO;
import com.tsp.operation.model.DAOS.WSHistCreditoDAO;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import org.w3c.dom.*;
import org.xml.sax.InputSource;


/**
 * service para las tablas del web service de historia de credito de datacredito<br/>
 * 26/08/2011<br/>
 * @author darrieta - GEOTECH SOLUTIONS S.A.
 */
public class WSHistCreditoService {

    WSHistCreditoDAO dao;
    TransaccionService tService;
    Usuario usuario;
    String tipoIdentificacion = "";
    String identificacion = "";
    String nitEmpresa="";
    String mensaje = "";  


    public WSHistCreditoService(String dataBaseName) {
        dao = new WSHistCreditoDAO(dataBaseName);
        tService = new TransaccionService(dataBaseName);
    }


    /**
     * Guarda la informacion de la persona
     * @param persona
     * @return sql generado
     * @throws Exception
     */
    public String guardarPersona(Persona persona) throws Exception{
        String sql = "";
        if(dao.existePersonaEmpresa(persona)){
            sql = dao.editarPersona(persona);
        }else{
            sql = dao.insertarPersona(persona);
        }
        return sql;
    }

    /**
     * Elimina toda la informacion asociada a una persona
     * @param identificacion
     * @param tipoIdentificacion
     * @return sql generado
     * @throws Exception
     */
    public String eliminarInfoPersona(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        String sql = dao.eliminarCuentasAhorro(identificacion, tipoIdentificacion,nitEmpresa)+"; ";
        sql += dao.eliminarCuentasCorriente(identificacion, tipoIdentificacion,nitEmpresa)+"; ";
        sql += dao.eliminarCuentasCartera(identificacion, tipoIdentificacion,nitEmpresa)+"; ";
        sql += dao.eliminarAlertas(identificacion, tipoIdentificacion,nitEmpresa)+"; ";
        sql += dao.eliminarConsultas(identificacion, tipoIdentificacion,nitEmpresa)+"; ";
        sql += dao.eliminarEndeudamientoGlobal(identificacion, tipoIdentificacion,nitEmpresa)+"; ";
        sql += dao.eliminarScores(identificacion, tipoIdentificacion,nitEmpresa)+"; ";
        sql += dao.eliminarTarjetasCredito(identificacion, tipoIdentificacion,nitEmpresa)+"; ";
        sql += dao.eliminarReclamos(identificacion, tipoIdentificacion,nitEmpresa)+"; ";
        sql += dao.eliminarValores(identificacion, tipoIdentificacion,nitEmpresa)+"; ";
        sql += dao.eliminarAlivios(identificacion, tipoIdentificacion,nitEmpresa)+"; ";
        sql += dao.eliminarComentarios(identificacion, tipoIdentificacion,nitEmpresa)+"; ";
        sql += dao.eliminarRespuestaPersonalizada(identificacion, tipoIdentificacion,nitEmpresa)+"; ";
        sql += dao.eliminarRespuestaPersonalizadaDecisor(identificacion, tipoIdentificacion, nitEmpresa)+"; ";
        return sql;
    }

    /**
     * Inserta una cuenta de ahorro en la tabla
     * @param cuenta informacion de la cuenta a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarCuentaAhorro(CuentaAhorro cuenta) throws Exception{
        return dao.insertarCuentaAhorro(cuenta);
    }

    /**
     * Inserta una cuenta de ahorro Decisor en la tabla 
     * @param cuenta informacion de la cuenta a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarCuentaAhorroDecisor(CuentaAhorroDecisor cuenta) throws Exception{
        return dao.insertarCuentaAhorroDecisor(cuenta);
    }

    /**
     * Inserta una cuenta corriente en la tabla
     * @param cuenta informacion de la cuenta a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarCuentaCorriente(CuentaCorriente cuenta) throws Exception{
        return dao.insertarCuentaCorriente(cuenta);
    }

    /**
     * Inserta una cuenta corriente Decisor en la tabla
     * @param cuenta informacion de la cuenta a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarCuentaCorrienteDecisor(CuentaCorrienteDecisor cuenta) throws Exception{
        return dao.insertarCuentaCorrienteDecisor(cuenta);
    }

    /**
     * Inserta una cuenta de cartera en la tabla
     * @param cuenta informacion de la cuenta a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarCuentaCartera(CuentaCartera cuenta) throws Exception{
        return dao.insertarCuentaCartera(cuenta);
    }

    /**
     * Inserta una cuenta de cartera decisor en la tabla
     * @param cuenta informacion de la cuenta a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarCuentaCarteraDecisor(CuentaCarteraDecisor cuenta) throws Exception{
        return dao.insertarCuentaCarteraDecisor(cuenta);
    }

    /**
     * Inserta una cuenta corriente en la tabla
     * @param tarjeta informacion de la cuenta a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarTarjetaCredito(TarjetaCredito tarjeta) throws Exception{
        return dao.insertarTarjetaCredito(tarjeta);
    }

    /**
     * Inserta una trajeta Credito decisor en la tabla
     * @param tarjeta informacion de la cuenta a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarTarjetaCreditoDecisor(TarjetaCreditoDecisor tarjeta) throws Exception{
        return dao.insertarTarjetaCreditoDecisor(tarjeta);
    }

    /**
     * Inserta un endeudamiento global en la tabla
     * @param endeudamiento informacion de la cuenta a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarEndeudamientoGlobal(EndeudamientoGlobal endeudamiento) throws Exception{
        return dao.insertarEndeudamientoGlobal(endeudamiento);
    }

    /**
     * Inserta un endeudamiento global Decisor en la tabla
     * @param endeudamiento informacion de la cuenta a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarEndeudamientoGlobalDecisor(EndeudamientoGlobalDecisor endeudamiento) throws Exception{
        return dao.insertarEndeudamientoGlobalDecisor(endeudamiento);
    }


    /**
     * Inserta una cuenta de cartera en la tabla
     * @param consulta informacion de la cuenta a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarConsulta(Consulta consulta) throws Exception{
        return dao.insertarConsulta(consulta);
    }

    /**
     * Inserta una consulta Decisor en la tabla
     * @param consulta informacion de la cuenta a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarConsultaDecisor(ConsultaDecisor consulta) throws Exception{
        return dao.insertarConsultaDecisor(consulta);
    }

    /**
     * Inserta una cuenta de cartera en la tabla
     * @param alerta informacion de la cuenta a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarAlerta(Alerta alerta) throws Exception{
        return dao.insertarAlerta(alerta);
    }

    /**
     * Inserta una Alerta Decisor en la tabla
     * @param alerta informacion de la cuenta a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarAlertaDecisor(AlertaDecisor alerta) throws Exception{
        return dao.insertarAlertaDecisor(alerta);
    }

    /**
     * Inserta un reclamo en la tabla
     * @param reclamo Bean con la informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarReclamo(Reclamo reclamo) throws Exception{
        return dao.insertarReclamo(reclamo);
    }
    
    /**
     * Inserta un registro de valores en la tabla
     * @param valor Bean con la informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarValor(Valor valor) throws Exception{
        return dao.insertarValor(valor);
    }

    /**
     * Inserta un registro de alivio en la tabla
     * @param alivio Bean con la informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarAlivio(Alivio alivio) throws Exception{
        return dao.insertarAlivio(alivio);
    }

    /**
     * Inserta un comentario en la tabla
     * @param comentario informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarComentario(ComentarioInforme comentario) throws Exception{
        return dao.insertarComentario(comentario);
    }
    
    /**
     * Inserta un score en la tabla
     * @param score informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarScore(Score score) throws Exception{
        return dao.insertarScore(score);
    }
    
    /**
     * Inserta un score Decisor en la tabla
     * @param score informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarScoreDecisor(ScoreDecisor score) throws Exception{
        return dao.insertarScoreDecisor(score);
    }
    
    /**
     * Inserta una razon de un score en la tabla
     * @param razon informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarRazon(Razon razon) throws Exception{
        return dao.insertarRazon(razon);
    }
    
    /**
     * Inserta una linea de respuesta personalizada
     * @param respuesta informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarRespuestaPersonalizada(RespuestaPersonalizada respuesta) throws Exception{
        return dao.insertarRespuestaPersonalizada(respuesta);
    }
    
 
    
     /**
     * Inserta una linea de respuesta personalizada Decisor
     * @param respuesta informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarRespuestaPersonalizadaDecisor(RespuestaPersonalizadaDecisor respuesta) throws Exception{
        return dao.insertarRespuestaPersonalizadaDecisor(respuesta);
    }
    
    public RespuestaSuperfil parseXML(String xml) throws Exception {
        RespuestaSuperfil respuesta = new RespuestaSuperfil();
        try {

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new InputSource(new ByteArrayInputStream(xml.getBytes("utf-8"))));

            Element root = doc.getDocumentElement();
            root.normalize();
            respuesta.setCodigoRespuesta(root.getAttribute("respuesta"));
            if (root.getFirstChild() != null) {
                tService.crearStatement();
                obtenerPersona(root);
                String[] sql = eliminarInfoPersona(identificacion, tipoIdentificacion, nitEmpresa).split(";");
                for(int i=0; i < sql.length; i++){
                   tService.getSt().addBatch(sql[i]); 
                }
                obtenerCuentasAhorro(root);
                obtenerCuentasCorriente(root);
                obtenerTarjetasCredito(root);
                obtenerCuentasCartera(root);
                obtenerEndeudamientoGlobal(root);
                obtenerConsultas(root);
                obtenerAlertas(root);
                obtenerComentarios(root);
                obtenerScore(root);
                obtenerRespuestaPersonalizada(root);
                tService.execute();
                
                //Se consulta el score para esa identificacion
                Score score=dao.buscarScore(identificacion, tipoIdentificacion,nitEmpresa);
                respuesta.setScore(score.getPuntaje());
                respuesta.setClasificacion(score.getClasificacion());
            }

        } catch (Throwable t) {
            t.printStackTrace();
            throw new Exception("ha ocurrido un error guardando los datos "+t.getMessage());
        }
        return respuesta;
    }


    public void obtenerPersona(Element root) throws Exception{
        NodeList listPersonas = null;
        Persona persona = new Persona();
        Element id=null;
        Element edad=null;
        if(root.getElementsByTagName("naturalNacional").getLength()>0){//Natural Nacional
            listPersonas = root.getElementsByTagName("naturalNacional");
            persona.setTipoIdentificacion("1");
            Element elPersona = (Element) listPersonas.item(0);
            if(!elPersona.getAttribute("nombres").isEmpty()){
                persona.setNombre(elPersona.getAttribute("nombres"));
            }
            if(!elPersona.getAttribute("primerApellido").isEmpty()){
                persona.setPrimerApellido(elPersona.getAttribute("primerApellido"));
            }
            if(!elPersona.getAttribute("segundoApellido").isEmpty()){
                persona.setSegundoApellido(elPersona.getAttribute("segundoApellido"));
            }
            if(!elPersona.getAttribute("nombreCompleto").isEmpty()){
                persona.setNombreCompleto(elPersona.getAttribute("nombreCompleto"));
            }
            if(!elPersona.getAttribute("genero").isEmpty()){
                persona.setGenero(elPersona.getAttribute("genero"));
            }
            if(!elPersona.getAttribute("validada").isEmpty()){
                persona.setValidada(Boolean.parseBoolean(elPersona.getAttribute("validada")));
            }

            id = (Element) elPersona.getElementsByTagName("identificacion").item(0);
            //Se obtiene la informacion de la identificacion
            if(!id.getAttribute("estado").isEmpty()){
                persona.setEstadoId(id.getAttribute("estado"));
            }
            if(!id.getAttribute("fechaExpedicion").isEmpty()){
                long fechaExpedicion = Long.parseLong(id.getAttribute("fechaExpedicion"));
                persona.setFechaExpedicionId(new Timestamp(fechaExpedicion));
            }
            if(!id.getAttribute("ciudad").isEmpty()){
                persona.setCiudadId(id.getAttribute("ciudad"));
            }
            if(!id.getAttribute("departamento").isEmpty()){
                persona.setDepartamentoId(id.getAttribute("departamento"));
            }
            if(!id.getAttribute("estado").isEmpty()){
                persona.setEstadoId(id.getAttribute("estado"));
            }
            if(!id.getAttribute("numero").isEmpty()){
                persona.setIdentificacion(identificacion);
            }


            //Se obtiene la informacion de la edad
            if(elPersona.getElementsByTagName("edad").getLength()>0){
                edad = (Element) elPersona.getElementsByTagName("edad").item(0);
                if(!edad.getAttribute("min").isEmpty()){
                    persona.setEdadMin(edad.getAttribute("min"));
                }
                if(!edad.getAttribute("max").isEmpty()){
                    persona.setEdadMax(edad.getAttribute("max"));
                }
            }

        }else if(root.getElementsByTagName("naturalExtranjera").getLength()>0){//Natural extranjera
            listPersonas = root.getElementsByTagName("naturalExtranjera");
            persona.setTipoIdentificacion("4");
            Element elPersona = (Element) listPersonas.item(0);
            if(!elPersona.getAttribute("nombre").isEmpty()){
                persona.setNombre(elPersona.getAttribute("nombre"));
            }
            if(!elPersona.getAttribute("nacionalidad").isEmpty()){
                persona.setNacionalidad(elPersona.getAttribute("nacionalidad"));
            }
            if(!elPersona.getAttribute("identificacion").isEmpty()){
                persona.setIdentificacion(identificacion);
            }

        }else if(root.getElementsByTagName("juridicaNacional").getLength()>0){//Juridica Nacional
            listPersonas = root.getElementsByTagName("juridicaNacional");
            persona.setTipoIdentificacion("2");
            Element elPersona = (Element) listPersonas.item(0);
            if(!elPersona.getAttribute("nombre").isEmpty()){
                persona.setNombre(elPersona.getAttribute("nombre"));
            }
            if(!elPersona.getAttribute("identificacion").isEmpty()){
                persona.setIdentificacion(identificacion);
            }

        }else if(root.getElementsByTagName("juridicaExtranjera").getLength()>0){//Juridica extranjera
            listPersonas = root.getElementsByTagName("juridicaExtranjera");
            persona.setTipoIdentificacion("3");
            Element elPersona = (Element) listPersonas.item(0);
            if(!elPersona.getAttribute("nombre").isEmpty()){
                persona.setNombre(elPersona.getAttribute("nombre"));
            }
            if(!elPersona.getAttribute("identificacion").isEmpty()){
                persona.setIdentificacion(identificacion);
            }

        }
        persona.setNitEmpresa(nitEmpresa);
        persona.setCreationUser(usuario.getLogin());
        persona.setUserUpdate(usuario.getLogin());
        persona.setWebService("HC");
        tService.getSt().addBatch(this.guardarPersona(persona));
    }

    public void obtenerCuentasAhorro(Element root) throws Exception{
        NodeList listCuentas = root.getElementsByTagName("cuentaAhorro");
        int numCuentas = listCuentas.getLength();
        for (int i = 0; i < numCuentas; i++) {
            Element elCuenta = (Element) listCuentas.item(i);
            CuentaAhorro cuenta = new CuentaAhorro();
            if(!elCuenta.getAttribute("estado").isEmpty()){
                cuenta.setEstado(elCuenta.getAttribute("estado"));
            }
            if(!elCuenta.getAttribute("entidad").isEmpty()){
                cuenta.setEntidad(elCuenta.getAttribute("entidad"));
            }
            if(!elCuenta.getAttribute("ultimaActualizacion").isEmpty()){
                long fecha = Long.parseLong(elCuenta.getAttribute("ultimaActualizacion"));
                cuenta.setUltimaActualizacion(new Timestamp(fecha));
            }
            if(!elCuenta.getAttribute("numeroCuenta").isEmpty()){
                cuenta.setNumeroCuenta(elCuenta.getAttribute("numeroCuenta"));
            }
            if(!elCuenta.getAttribute("fechaApertura").isEmpty()){
                long fecha = Long.parseLong(elCuenta.getAttribute("fechaApertura"));
                cuenta.setFechaApertura(new Timestamp(fecha));
            }
            if(!elCuenta.getAttribute("oficina").isEmpty()){
                cuenta.setOficina(elCuenta.getAttribute("oficina"));
            }
            if(!elCuenta.getAttribute("ciudad").isEmpty()){
                cuenta.setCiudad(elCuenta.getAttribute("ciudad"));
            }
            if(!elCuenta.getAttribute("bloqueada").isEmpty()){
                cuenta.setBloqueada(Boolean.parseBoolean(elCuenta.getAttribute("bloqueada")));
            }
            if(!elCuenta.getAttribute("codSuscriptor").isEmpty()){
                cuenta.setCodSuscriptor(elCuenta.getAttribute("codSuscriptor"));
            }
            if(!elCuenta.getAttribute("situacionTitular").isEmpty()){
                cuenta.setSituacionTitular(Short.parseShort(elCuenta.getAttribute("situacionTitular")));
            }
            cuenta.setTipoIdentificacion(tipoIdentificacion);
            cuenta.setIdentificacion(identificacion);
            cuenta.setCreationUser(usuario.getLogin());
            cuenta.setUserUpdate(usuario.getLogin());
            cuenta.setNitEmpresa(nitEmpresa);

            tService.getSt().addBatch(this.insertarCuentaAhorro(cuenta));

            NodeList listReclamos = elCuenta.getElementsByTagName("reclamo");
            if(listReclamos.getLength()>0){
                Element elReclamo = (Element) listReclamos.item(0);
                Reclamo reclamo = new Reclamo();
                reclamo.setTipoPadre("CAH");
                if(!elReclamo.getAttribute("tipoLeyenda").isEmpty()){
                    reclamo.setTipoLeyenda(elReclamo.getAttribute("tipoLeyenda"));
                }
                if(!elReclamo.getAttribute("fechaCierre").isEmpty()){
                    long fecha = Long.parseLong(elReclamo.getAttribute("fechaCierre"));
                    reclamo.setFechaCierre(new Timestamp(fecha));
                }
                if(!elReclamo.getAttribute("estado").isEmpty()){
                    reclamo.setEstado(elReclamo.getAttribute("estado"));
                }
                if(!elReclamo.getAttribute("tipo").isEmpty()){
                    reclamo.setTipo(elReclamo.getAttribute("tipo"));
                }
                if(!elReclamo.getAttribute("fecha").isEmpty()){
                    long fecha = Long.parseLong(elReclamo.getAttribute("fecha"));
                    reclamo.setFecha(new Timestamp(fecha));
                }
                if(!elReclamo.getAttribute("ratificado").isEmpty()){
                    reclamo.setRatificado(Boolean.parseBoolean(elReclamo.getAttribute("ratificado")));
                }
                reclamo.setTexto(elReclamo.getTextContent());
                reclamo.setTipoIdentificacion(tipoIdentificacion);
                reclamo.setIdentificacion(identificacion);
                reclamo.setCreationUser(usuario.getLogin());
                reclamo.setUserUpdate(usuario.getLogin());
                reclamo.setNitEmpresa(nitEmpresa);
                tService.getSt().addBatch(this.insertarReclamo(reclamo));
            }            
        }
    }

    public void obtenerCuentasCorriente(Element root) throws Exception{
        NodeList listCuentas = root.getElementsByTagName("cuentaCorriente");
        int numCuentas = listCuentas.getLength();
        for (int i = 0; i < numCuentas; i++) {
            Element elCuenta = (Element) listCuentas.item(i);
            CuentaCorriente cuenta = new CuentaCorriente();
            if(!elCuenta.getAttribute("estado").isEmpty()){
                cuenta.setEstado(elCuenta.getAttribute("estado"));
            }
            if(!elCuenta.getAttribute("entidad").isEmpty()){
                cuenta.setEntidad(elCuenta.getAttribute("entidad"));
            }
            if(!elCuenta.getAttribute("ultimaActualizacion").isEmpty()){
                long fecha = Long.parseLong(elCuenta.getAttribute("ultimaActualizacion"));
                cuenta.setUltimaActualizacion(new Timestamp(fecha));
            }
            if(!elCuenta.getAttribute("numeroCuenta").isEmpty()){
                cuenta.setNumeroCuenta(elCuenta.getAttribute("numeroCuenta"));
            }
            if(!elCuenta.getAttribute("fechaApertura").isEmpty()){
                long fecha = Long.parseLong(elCuenta.getAttribute("fechaApertura"));
                cuenta.setFechaApertura(new Timestamp(fecha));
            }
            if(!elCuenta.getAttribute("oficina").isEmpty()){
                cuenta.setOficina(elCuenta.getAttribute("oficina"));
            }
            if(!elCuenta.getAttribute("ciudad").isEmpty()){
                cuenta.setCiudad(elCuenta.getAttribute("ciudad"));
            }
            if(!elCuenta.getAttribute("bloqueada").isEmpty()){
                cuenta.setBloqueada(Boolean.parseBoolean(elCuenta.getAttribute("bloqueada")));
            }
            if(!elCuenta.getAttribute("codSuscriptor").isEmpty()){
                cuenta.setCodSuscriptor(elCuenta.getAttribute("codSuscriptor"));
            }
            if(!elCuenta.getAttribute("situacionTitular").isEmpty()){
                cuenta.setSituacionTitular(elCuenta.getAttribute("situacionTitular"));
            }
            cuenta.setTipoIdentificacion(tipoIdentificacion);
            cuenta.setIdentificacion(identificacion);
            cuenta.setCreationUser(usuario.getLogin());
            cuenta.setUserUpdate(usuario.getLogin());
            cuenta.setNitEmpresa(nitEmpresa);

            tService.getSt().addBatch(this.insertarCuentaCorriente(cuenta));

            NodeList listReclamos = elCuenta.getElementsByTagName("reclamo");
            if(listReclamos.getLength()>0){
                Element elReclamo = (Element) listReclamos.item(0);
                Reclamo reclamo = new Reclamo();
                reclamo.setTipoPadre("CCO");
                if(!elReclamo.getAttribute("tipoLeyenda").isEmpty()){
                    reclamo.setTipoLeyenda(elReclamo.getAttribute("tipoLeyenda"));
                }
                if(!elReclamo.getAttribute("fechaCierre").isEmpty()){
                    long fecha = Long.parseLong(elReclamo.getAttribute("fechaCierre"));
                    reclamo.setFechaCierre(new Timestamp(fecha));
                }
                if(!elReclamo.getAttribute("estado").isEmpty()){
                    reclamo.setEstado(elReclamo.getAttribute("estado"));
                }
                if(!elReclamo.getAttribute("tipo").isEmpty()){
                    reclamo.setTipo(elReclamo.getAttribute("tipo"));
                }
                if(!elReclamo.getAttribute("fecha").isEmpty()){
                    long fecha = Long.parseLong(elReclamo.getAttribute("fecha"));
                    reclamo.setFecha(new Timestamp(fecha));
                }
                if(!elReclamo.getAttribute("ratificado").isEmpty()){
                    reclamo.setRatificado(Boolean.parseBoolean(elReclamo.getAttribute("ratificado")));
                }
                reclamo.setTexto(elReclamo.getTextContent());
                reclamo.setTipoIdentificacion(tipoIdentificacion);
                reclamo.setIdentificacion(identificacion);
                reclamo.setCreationUser(usuario.getLogin());
                reclamo.setUserUpdate(usuario.getLogin());
                reclamo.setNitEmpresa(nitEmpresa);

                tService.getSt().addBatch(this.insertarReclamo(reclamo));
            }
        }
    }

    public void obtenerTarjetasCredito(Element root) throws Exception{
        NodeList listTarjetas = root.getElementsByTagName("tarjetaCredito");
        int numTarjetas = listTarjetas.getLength();
        for (int i = 0; i < numTarjetas; i++) {
            Element elTarjeta = (Element) listTarjetas.item(i);
            TarjetaCredito tarjeta = new TarjetaCredito();
            if(!elTarjeta.getAttribute("estado").isEmpty()){
                tarjeta.setEstado(elTarjeta.getAttribute("estado"));
            }
            if(!elTarjeta.getAttribute("estado48").isEmpty()){
                tarjeta.setEstado48(elTarjeta.getAttribute("estado48"));
            }
            if(!elTarjeta.getAttribute("entidad").isEmpty()){
                tarjeta.setEntidad(elTarjeta.getAttribute("entidad"));
            }
            if(!elTarjeta.getAttribute("ultimaActualizacion").isEmpty()){
                long fecha = Long.parseLong(elTarjeta.getAttribute("ultimaActualizacion"));
                tarjeta.setUltimaActualizacion(new Timestamp(fecha));
            }
            if(!elTarjeta.getAttribute("numero").isEmpty()){
                tarjeta.setNumero(elTarjeta.getAttribute("numero"));
            }
            if(!elTarjeta.getAttribute("fechaApertura").isEmpty()){
                long fecha = Long.parseLong(elTarjeta.getAttribute("fechaApertura"));
                tarjeta.setFechaApertura(new Timestamp(fecha));
            }
            if(!elTarjeta.getAttribute("fechaVencimiento").isEmpty()){
                long fecha = Long.parseLong(elTarjeta.getAttribute("fechaVencimiento"));
                tarjeta.setFechaVencimiento(new Timestamp(fecha));
            }
            if(!elTarjeta.getAttribute("comportamiento").isEmpty()){
                tarjeta.setComportamiento(elTarjeta.getAttribute("comportamiento"));
            }
            if(!elTarjeta.getAttribute("amparada").isEmpty()){
                tarjeta.setAmparada(Boolean.parseBoolean(elTarjeta.getAttribute("amparada")));
            }
            if(!elTarjeta.getAttribute("formaPago").isEmpty()){
                tarjeta.setFormaPago(elTarjeta.getAttribute("formaPago"));
            }
            if(!elTarjeta.getAttribute("bloqueada").isEmpty()){
                tarjeta.setBloqueada(Boolean.parseBoolean(elTarjeta.getAttribute("bloqueada")));
            }
            if(!elTarjeta.getAttribute("codSuscriptor").isEmpty()){
                tarjeta.setCodSuscriptor(elTarjeta.getAttribute("codSuscriptor"));
            }
            if(!elTarjeta.getAttribute("positivoNegativo").isEmpty()){
                tarjeta.setPositivoNegativo(elTarjeta.getAttribute("positivoNegativo"));
            }
            if(!elTarjeta.getAttribute("oficina").isEmpty()){
                tarjeta.setOficina(elTarjeta.getAttribute("oficina"));
            }
            if(!elTarjeta.getAttribute("situacionTitular").isEmpty()){
                tarjeta.setSituacionTitular(Integer.parseInt(elTarjeta.getAttribute("situacionTitular")));
            }
            if(!elTarjeta.getAttribute("estadoOrigen").isEmpty()){
                tarjeta.setEstadoOrigen(elTarjeta.getAttribute("estadoOrigen"));
            }
            if(!elTarjeta.getAttribute("prescripcion").isEmpty()){
                tarjeta.setPrescripcion(elTarjeta.getAttribute("prescripcion"));
            }
            tarjeta.setTipoIdentificacion(tipoIdentificacion);
            tarjeta.setIdentificacion(identificacion);
            tarjeta.setCreationUser(usuario.getLogin());
            tarjeta.setUserUpdate(usuario.getLogin());
            tarjeta.setNitEmpresa(nitEmpresa);

            tService.getSt().addBatch(this.insertarTarjetaCredito(tarjeta));

            NodeList listReclamos = elTarjeta.getElementsByTagName("reclamo");
            if(listReclamos.getLength()>0){
                Element elReclamo = (Element) listReclamos.item(0);
                Reclamo reclamo = new Reclamo();
                reclamo.setTipoPadre("TCR");
                if(!elReclamo.getAttribute("tipoLeyenda").isEmpty()){
                    reclamo.setTipoLeyenda(elReclamo.getAttribute("tipoLeyenda"));
                }
                if(!elReclamo.getAttribute("fechaCierre").isEmpty()){
                    long fecha = Long.parseLong(elReclamo.getAttribute("fechaCierre"));
                    reclamo.setFechaCierre(new Timestamp(fecha));
                }
                if(!elReclamo.getAttribute("estado").isEmpty()){
                    reclamo.setEstado(elReclamo.getAttribute("estado"));
                }
                if(!elReclamo.getAttribute("tipo").isEmpty()){
                    reclamo.setTipo(elReclamo.getAttribute("tipo"));
                }
                if(!elReclamo.getAttribute("fecha").isEmpty()){
                    long fecha = Long.parseLong(elReclamo.getAttribute("fecha"));
                    reclamo.setFecha(new Timestamp(fecha));
                }
                if(!elReclamo.getAttribute("ratificado").isEmpty()){
                    reclamo.setRatificado(Boolean.parseBoolean(elReclamo.getAttribute("ratificado")));
                }
                reclamo.setTexto(elReclamo.getTextContent());
                reclamo.setTipoIdentificacion(tipoIdentificacion);
                reclamo.setIdentificacion(identificacion);
                reclamo.setCreationUser(usuario.getLogin());
                reclamo.setUserUpdate(usuario.getLogin());

                tService.getSt().addBatch(this.insertarReclamo(reclamo));
            }

            NodeList listValores = elTarjeta.getElementsByTagName("valores");
            int numValores = listValores.getLength();
            for (int j = 0; j < numValores; j++) {
                Element elValor = (Element) listValores.item(j);
                Valor valor = new Valor();
                valor.setTipoPadre("TCR");
                //Los datos corresponden al valor real. No estan en miles
                if(!elValor.getAttribute("cupo").isEmpty()){
                    valor.setCupo(Double.parseDouble(elValor.getAttribute("cupo")));
                }
                if(!elValor.getAttribute("saldoActual").isEmpty()){
                    valor.setSaldoActual(Double.parseDouble(elValor.getAttribute("saldoActual")));
                }
                if(!elValor.getAttribute("saldoMora").isEmpty()){
                    valor.setSaldoMora(Double.parseDouble(elValor.getAttribute("saldoMora")));
                }
                if(!elValor.getAttribute("cuota").isEmpty()){
                    valor.setCuota(Double.parseDouble(elValor.getAttribute("cuota")));
                }
                valor.setTipoIdentificacion(tipoIdentificacion);
                valor.setIdentificacion(identificacion);
                valor.setCreationUser(usuario.getLogin());
                valor.setUserUpdate(usuario.getLogin());
                valor.setNitEmpresa(nitEmpresa);

                tService.getSt().addBatch(this.insertarValor(valor));
            }

            NodeList listAlivio = elTarjeta.getElementsByTagName("alivio");
            if(listAlivio.getLength()>0){
                Element elAlivio = (Element) listAlivio.item(0);
                Alivio alivio = new Alivio();
                alivio.setTipoPadre("TCR");
                if(!elAlivio.getAttribute("estado").isEmpty()){
                    alivio.setEstado(elAlivio.getAttribute("estado"));
                }
                if(!elAlivio.getAttribute("mes").isEmpty()){
                    alivio.setMes(elAlivio.getAttribute("mes"));
                }
                alivio.setTipoIdentificacion(tipoIdentificacion);
                alivio.setIdentificacion(identificacion);
                alivio.setCreationUser(usuario.getLogin());
                alivio.setUserUpdate(usuario.getLogin());

                tService.getSt().addBatch(this.insertarAlivio(alivio));
            }
        }
    }


    public void obtenerCuentasCartera(Element root) throws Exception{
        NodeList listCuentas = root.getElementsByTagName("cuentaCartera");
        int numCuentas = listCuentas.getLength();
        for (int i = 0; i < numCuentas; i++) {
            Element elCuenta = (Element) listCuentas.item(i);
            CuentaCartera cuenta = new CuentaCartera();
            if(!elCuenta.getAttribute("bloqueada").isEmpty()){
                cuenta.setBloqueada(Boolean.parseBoolean(elCuenta.getAttribute("bloqueada")));
            }
            if(!elCuenta.getAttribute("priodicidad").isEmpty()){
                cuenta.setPeriodicidad(elCuenta.getAttribute("priodicidad"));
            }
            if(!elCuenta.getAttribute("comportamiento").isEmpty()){
                cuenta.setComportamiento(elCuenta.getAttribute("comportamiento"));
            }
            if(!elCuenta.getAttribute("fechaApertura").isEmpty()){
                long fechaApertura = Long.parseLong(elCuenta.getAttribute("fechaApertura"));
                cuenta.setFechaApertura(new Timestamp(fechaApertura));
            }
            if(!elCuenta.getAttribute("fechaVencimiento").isEmpty()){
                long fechaVenc = Long.parseLong(elCuenta.getAttribute("fechaVencimiento"));
                cuenta.setFechaVencimiento(new Timestamp(fechaVenc));
            }
            if(!elCuenta.getAttribute("numeroObligacion").isEmpty()){
                cuenta.setNumeroObligacion(elCuenta.getAttribute("numeroObligacion"));
            }
            if(!elCuenta.getAttribute("ultimaActualizacion").isEmpty()){
                long fecha = Long.parseLong(elCuenta.getAttribute("ultimaActualizacion"));
                cuenta.setUltimaActualizacion(new Timestamp(fecha));
            }
            if(!elCuenta.getAttribute("entidad").isEmpty()){
                cuenta.setEntidad(elCuenta.getAttribute("entidad"));
            }
            if(!elCuenta.getAttribute("estado").isEmpty()){
                cuenta.setEstado(elCuenta.getAttribute("estado"));
            }
            if(!elCuenta.getAttribute("estado48").isEmpty()){
                cuenta.setEstado48(elCuenta.getAttribute("estado48"));
            }
            if(!elCuenta.getAttribute("tipoObligacion").isEmpty()){
                cuenta.setTipoObligacion(elCuenta.getAttribute("tipoObligacion"));
            }
            if(!elCuenta.getAttribute("tipoCuenta").isEmpty()){
                cuenta.setTipoCuenta(elCuenta.getAttribute("tipoCuenta"));
            }
            if(!elCuenta.getAttribute("garante").isEmpty()){
                cuenta.setGarante(elCuenta.getAttribute("garante"));
            }
            if(!elCuenta.getAttribute("formaPago").isEmpty()){
                cuenta.setFormaPago(elCuenta.getAttribute("formaPago"));
            }
            if(!elCuenta.getAttribute("codSuscriptor").isEmpty()){
                cuenta.setCodSuscriptor(elCuenta.getAttribute("codSuscriptor"));
            }
            if(!elCuenta.getAttribute("positivoNegativo").isEmpty()){
                cuenta.setPositivoNegativo(elCuenta.getAttribute("positivoNegativo"));
            }
            if(!elCuenta.getAttribute("oficina").isEmpty()){
                cuenta.setOficina(elCuenta.getAttribute("oficina"));
            }
            if(!elCuenta.getAttribute("mesePermanencia").isEmpty()){
                cuenta.setMesesPermanencia(Short.parseShort(elCuenta.getAttribute("mesePermanencia")));
            }
            if(!elCuenta.getAttribute("situacionTitular").isEmpty()){
                cuenta.setSituacionTitular(elCuenta.getAttribute("situacionTitular"));
            }
            if(!elCuenta.getAttribute("estadoOrigen").isEmpty()){
                cuenta.setEstadoOrigen(elCuenta.getAttribute("estadoOrigen"));
            }
            if(!elCuenta.getAttribute("tipoContrato").isEmpty()){
                cuenta.setTipoContrato(elCuenta.getAttribute("tipoContrato"));
            }
            if(!elCuenta.getAttribute("ejecucionContrato").isEmpty()){
                cuenta.setEjecucionContrato(Short.parseShort(elCuenta.getAttribute("ejecucionContrato")));
            }
            if(!elCuenta.getAttribute("prescripcion").isEmpty()){
                cuenta.setPrescripcion(elCuenta.getAttribute("prescripcion"));
            }
            cuenta.setTipoIdentificacion(tipoIdentificacion);
            cuenta.setIdentificacion(identificacion);
            cuenta.setCreationUser(usuario.getLogin());
            cuenta.setUserUpdate(usuario.getLogin());
            cuenta.setNitEmpresa(nitEmpresa);

            tService.getSt().addBatch(this.insertarCuentaCartera(cuenta));

            NodeList listReclamos = elCuenta.getElementsByTagName("reclamo");
            if(listReclamos.getLength()>0){
                Element elReclamo = (Element) listReclamos.item(0);
                Reclamo reclamo = new Reclamo();
                reclamo.setTipoPadre("CCA");
                if(!elReclamo.getAttribute("tipoLeyenda").isEmpty()){
                    reclamo.setTipoLeyenda(elReclamo.getAttribute("tipoLeyenda"));
                }
                if(!elReclamo.getAttribute("fechaCierre").isEmpty()){
                    long fecha = Long.parseLong(elReclamo.getAttribute("fechaCierre"));
                    reclamo.setFechaCierre(new Timestamp(fecha));
                }
                if(!elReclamo.getAttribute("estado").isEmpty()){
                    reclamo.setEstado(elReclamo.getAttribute("estado"));
                }
                if(!elReclamo.getAttribute("tipo").isEmpty()){
                    reclamo.setTipo(elReclamo.getAttribute("tipo"));
                }
                if(!elReclamo.getAttribute("fecha").isEmpty()){
                    long fecha = Long.parseLong(elReclamo.getAttribute("fecha"));
                    reclamo.setFecha(new Timestamp(fecha));
                }
                if(!elReclamo.getAttribute("ratificado").isEmpty()){
                    reclamo.setRatificado(Boolean.parseBoolean(elReclamo.getAttribute("ratificado")));
                }
                reclamo.setTexto(elReclamo.getTextContent());
                reclamo.setTipoIdentificacion(tipoIdentificacion);
                reclamo.setIdentificacion(identificacion);
                reclamo.setCreationUser(usuario.getLogin());
                reclamo.setUserUpdate(usuario.getLogin());
                reclamo.setNitEmpresa(nitEmpresa);

                tService.getSt().addBatch(this.insertarReclamo(reclamo));
            }

            NodeList listValores = elCuenta.getElementsByTagName("valores");
            int numValores = listValores.getLength();
            for (int j = 0; j < numValores; j++) {
                Element elValor = (Element) listValores.item(j);
                Valor valor = new Valor();
                valor.setTipoPadre("CCA");
                if(!elValor.getAttribute("valorInicial").isEmpty()){
                    valor.setValorInicial(Double.parseDouble(elValor.getAttribute("valorInicial")));
                }
                if(!elValor.getAttribute("saldoActual").isEmpty()){
                    valor.setSaldoActual(Double.parseDouble(elValor.getAttribute("saldoActual")));
                }
                if(!elValor.getAttribute("saldoMora").isEmpty()){
                    valor.setSaldoMora(Double.parseDouble(elValor.getAttribute("saldoMora")));
                }
                if(!elValor.getAttribute("cuota").isEmpty()){
                    valor.setCuota(Double.parseDouble(elValor.getAttribute("cuota")));
                }
                if(!elValor.getAttribute("cuotasCanceladas").isEmpty()){
                    valor.setCuotasCanceladas(Integer.parseInt(elValor.getAttribute("cuotasCanceladas")));
                }
                if(!elValor.getAttribute("totalCuotas").isEmpty()){
                    valor.setTotalCuotas(Integer.parseInt(elValor.getAttribute("totalCuotas")));
                }
                if(!elValor.getAttribute("maximaMora").isEmpty()){
                    valor.setMaximaMora(Integer.parseInt(elValor.getAttribute("maximaMora")));
                }
                valor.setTipoIdentificacion(tipoIdentificacion);
                valor.setIdentificacion(identificacion);
                valor.setCreationUser(usuario.getLogin());
                valor.setUserUpdate(usuario.getLogin());
                valor.setNitEmpresa(nitEmpresa);

                tService.getSt().addBatch(this.insertarValor(valor));
            }

            NodeList listAlivio = elCuenta.getElementsByTagName("alivio");
            if(listAlivio.getLength()>0){
                Element elAlivio = (Element) listAlivio.item(0);
                Alivio alivio = new Alivio();
                alivio.setTipoPadre("CCA");
                if(!elAlivio.getAttribute("estado").isEmpty()){
                    alivio.setEstado(elAlivio.getAttribute("estado"));
                }
                if(!elAlivio.getAttribute("mes").isEmpty()){
                    alivio.setMes(elAlivio.getAttribute("mes"));
                }
                alivio.setTipoIdentificacion(tipoIdentificacion);
                alivio.setIdentificacion(identificacion);
                alivio.setCreationUser(usuario.getLogin());
                alivio.setUserUpdate(usuario.getLogin());

                tService.getSt().addBatch(this.insertarAlivio(alivio));
            }
        }
    }

    public void obtenerEndeudamientoGlobal(Element root) throws Exception{
        NodeList listCuentas = root.getElementsByTagName("endeudamientoGlobal");
        int numRegistros = listCuentas.getLength();
        for (int i = 0; i < numRegistros; i++) {
            Element elEndeudamiento = (Element) listCuentas.item(i);
            EndeudamientoGlobal endeudamiento = new EndeudamientoGlobal();
            if(!elEndeudamiento.getAttribute("calificacion").isEmpty()){
                endeudamiento.setCalificacion(elEndeudamiento.getAttribute("calificacion"));
            }
            if(!elEndeudamiento.getAttribute("saldoPendiente").isEmpty()){
                endeudamiento.setSaldoPendiente(Double.parseDouble(elEndeudamiento.getAttribute("saldoPendiente")));
            }
            if(!elEndeudamiento.getAttribute("tipoCredito").isEmpty()){
                endeudamiento.setTipoCredito(elEndeudamiento.getAttribute("tipoCredito"));
            }
            if(!elEndeudamiento.getAttribute("moneda").isEmpty()){
                endeudamiento.setMoneda(elEndeudamiento.getAttribute("moneda"));
            }
            if(!elEndeudamiento.getAttribute("numeroCreditos").isEmpty()){
                endeudamiento.setNumeroCreditos(Integer.parseInt(elEndeudamiento.getAttribute("numeroCreditos")));
            }
            if(!elEndeudamiento.getAttribute("fechaReporte").isEmpty()){
                long fecha = Long.parseLong(elEndeudamiento.getAttribute("fechaReporte"));
                endeudamiento.setFechaReporte(new Timestamp(fecha));
            }
            if(!elEndeudamiento.getAttribute("entidad").isEmpty()){
                endeudamiento.setEntidad(elEndeudamiento.getAttribute("entidad"));
            }
            if(!elEndeudamiento.getAttribute("garantia").isEmpty()){
                endeudamiento.setGarantia(elEndeudamiento.getAttribute("garantia"));
            }
            endeudamiento.setTipoIdentificacion(tipoIdentificacion);
            endeudamiento.setIdentificacion(identificacion);
            endeudamiento.setCreationUser(usuario.getLogin());
            endeudamiento.setUserUpdate(usuario.getLogin());
            endeudamiento.setNitEmpresa(nitEmpresa);

            tService.getSt().addBatch(this.insertarEndeudamientoGlobal(endeudamiento));

        }
    }

    public void obtenerConsultas(Element root) throws Exception{
        NodeList listConsultas = root.getElementsByTagName("consulta");
        int numRegistros = listConsultas.getLength();
        for (int i = 0; i < numRegistros; i++) {
            Element elConsulta = (Element) listConsultas.item(i);
            Consulta consulta = new Consulta();
            if(!elConsulta.getAttribute("fecha").isEmpty()){
                long fecha = Long.parseLong(elConsulta.getAttribute("fecha"));
                consulta.setFecha(new Timestamp(fecha));
            }
            if(!elConsulta.getAttribute("tipoCuenta").isEmpty()){
                consulta.setTipoCuenta(elConsulta.getAttribute("tipoCuenta"));
            }
            if(!elConsulta.getAttribute("entidad").isEmpty()){
                consulta.setEntidad(elConsulta.getAttribute("entidad"));
            }
            if(!elConsulta.getAttribute("oficina").isEmpty()){
                consulta.setOficina(elConsulta.getAttribute("oficina"));
            }
            if(!elConsulta.getAttribute("ciudad").isEmpty()){
                consulta.setCiudad(elConsulta.getAttribute("ciudad"));
            }
            if(!elConsulta.getAttribute("razon").isEmpty()){
                consulta.setRazon(elConsulta.getAttribute("razon"));
            }
            consulta.setTipoIdentificacion(tipoIdentificacion);
            consulta.setIdentificacion(identificacion);
            consulta.setCreationUser(usuario.getLogin());
            consulta.setUserUpdate(usuario.getLogin());
            consulta.setNitEmpresa(nitEmpresa);

            tService.getSt().addBatch(this.insertarConsulta(consulta));

        }
    }

    public void obtenerAlertas(Element root) throws Exception{
        NodeList listAlertas = root.getElementsByTagName("alerta");
        int numRegistros = listAlertas.getLength();
        for (int i = 0; i < numRegistros; i++) {
            Element elAlerta = (Element) listAlertas.item(i);
            Alerta alerta = new Alerta();
            if(!elAlerta.getAttribute("colocacion").isEmpty()){
                long fecha = Long.parseLong(elAlerta.getAttribute("colocacion"));
                alerta.setColocacion(new Timestamp(fecha));
            }
            if(!elAlerta.getAttribute("vencimiento").isEmpty()){
                long fecha = Long.parseLong(elAlerta.getAttribute("vencimiento"));
                alerta.setVencimiento(new Timestamp(fecha));
            }
            if(!elAlerta.getAttribute("modificacion").isEmpty()){
                long fecha = Long.parseLong(elAlerta.getAttribute("modificacion"));
                alerta.setModificacion(new Timestamp(fecha));
            }
            if(!elAlerta.getAttribute("codigo").isEmpty()){
                alerta.setCodigo(elAlerta.getAttribute("codigo"));
            }
            if(!elAlerta.getAttribute("texto").isEmpty()){
                alerta.setTexto(elAlerta.getAttribute("texto"));
            }
            
            NodeList listFuente = elAlerta.getElementsByTagName("fuente");
            if(listFuente.getLength()>0){
                Element elFuente = (Element) listFuente.item(0);
                if(!elFuente.getAttribute("codigo").isEmpty()){
                    alerta.setCodigoFuente(elFuente.getAttribute("codigo"));
                }
            }
            
            alerta.setTipoIdentificacion(tipoIdentificacion);
            alerta.setIdentificacion(identificacion);
            alerta.setCreationUser(usuario.getLogin());
            alerta.setUserUpdate(usuario.getLogin());
            alerta.setNitEmpresa(nitEmpresa);

            tService.getSt().addBatch(this.insertarAlerta(alerta));

        }
    }

    public void obtenerComentarios(Element root) throws Exception{
        NodeList listComentarios = root.getElementsByTagName("comentario");
        int numRegistros = listComentarios.getLength();
        for (int i = 0; i < numRegistros; i++) {
            Element elComentario = (Element) listComentarios.item(i);
            ComentarioInforme comentario = new ComentarioInforme();

            if(!elComentario.getAttribute("tipo").isEmpty()){
                comentario.setTipo(elComentario.getAttribute("tipo"));
            }
            if(!elComentario.getAttribute("fechaVencimiento").isEmpty()){
                long fecha = Long.parseLong(elComentario.getAttribute("fechaVencimiento"));
                comentario.setFechaVencimiento(new Timestamp(fecha));
            }
            comentario.setTexto(elComentario.getTextContent());
            comentario.setTipoIdentificacion(tipoIdentificacion);
            comentario.setIdentificacion(identificacion);
            comentario.setCreationUser(usuario.getLogin());
            comentario.setUserUpdate(usuario.getLogin());
            comentario.setNitEmpresa(nitEmpresa);

            tService.getSt().addBatch(this.insertarComentario(comentario));

        }
    }

    public void obtenerScore(Element root) throws Exception{
        NodeList listScores = root.getElementsByTagName("score");
        int numRegistros = listScores.getLength();
        for (int i = 0; i < numRegistros; i++) {
            Element elScore = (Element) listScores.item(i);
            Score score = new Score();

            if(!elScore.getAttribute("tipo").isEmpty()){
                score.setTipo(Integer.parseInt(elScore.getAttribute("tipo")));
            }
            if(!elScore.getAttribute("puntaje").isEmpty()){
                score.setPuntaje((int)Float.parseFloat(elScore.getAttribute("puntaje")));
            }
            
            if(!elScore.getAttribute("clasificacion").isEmpty()){
                score.setClasificacion(elScore.getAttribute("clasificacion"));
            }else{
                score.setClasificacion("");
            }
            
            score.setTipoIdentificacion(tipoIdentificacion);
            score.setIdentificacion(identificacion);
            score.setCreationUser(usuario.getLogin());
            score.setUserUpdate(usuario.getLogin());
            score.setNitEmpresa(nitEmpresa);

            tService.getSt().addBatch(this.insertarScore(score));
            
            NodeList listRazones = elScore.getElementsByTagName("razon");
            int numRazones = listRazones.getLength();
            for (int j = 0; j < numRazones; j++) {
                Element elRazon = (Element) listRazones.item(j);
                Razon razon = new Razon();
                if(!elRazon.getAttribute("codigo").isEmpty()){
                    razon.setCodigo(elRazon.getAttribute("codigo"));
                }
                razon.setCreationUser(usuario.getLogin());
                razon.setUserUpdate(usuario.getLogin());

                tService.getSt().addBatch(this.insertarRazon(razon));
            }

        }
    }

    public void obtenerRespuestaPersonalizada(Element root) throws Exception{
        NodeList listRespPersonalizada = root.getElementsByTagName("respuestaPersonalizada");
        int numResp = listRespPersonalizada.getLength();
        for (int i = 0; i < numResp; i++) {
            Element elResp = (Element) listRespPersonalizada.item(i);

            NodeList listLineas = elResp.getElementsByTagName("linea");
            int numLineas = listLineas.getLength();

            for (int j = 0; j < numLineas; j++) {
                Element elLinea = (Element) listLineas.item(j);
                RespuestaPersonalizada lineaResp = new RespuestaPersonalizada();
                lineaResp.setLinea(elLinea.getTextContent());
                lineaResp.setCreationUser(usuario.getLogin());
                lineaResp.setUserUpdate(usuario.getLogin());
                lineaResp.setTipoIdentificacion(tipoIdentificacion);
                lineaResp.setIdentificacion(identificacion);
                lineaResp.setNitEmpresa(nitEmpresa);

                tService.getSt().addBatch(this.insertarRespuestaPersonalizada(lineaResp));
            }
        }
    }
    
    
    /**
     * Genera un archivo txt con la respuesta recibida del web service
     * @param solicitud datos enviados al web service
     * @param xml datos recibidoa
     * @throws Exception 
     */
    private void generarTXT(String solicitud, String xml) throws IOException{
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String ruta = rb.getString("ruta") + "/WEB-INF/classes/logsDatacredito/";
            File archivo = new File(ruta);            
            if (!archivo.exists()) {
                archivo.mkdirs();
            }

            FileWriter fw = new FileWriter(ruta + "hcredito_"+Util.getFechaActual_String(8)+".txt", true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter salida = new PrintWriter(bw);
            if(solicitud!=null){
                salida.println("SOLICITUD "+Util.getFechaActual_String(9)+":");
                salida.println(solicitud);
            }
            if(xml!=null){
                salida.println("RESPUESTA:");
                salida.print(xml);
                salida.println();
                salida.println();
            }
            salida.println();
            salida.close();
        } catch(java.io.IOException ioex) {
          throw ioex;
        }
    }
    
    /**
     * Realiza la consulta a datacredito de la historia de credito de una persona y la almacena en la BD
     * @param formulario
     * @param usuario 
     */
    public RespuestaSuperfil consultarHistoriaCredito(FormularioSuperfil formulario, Usuario usuario){
        RespuestaSuperfil respuesta = new RespuestaSuperfil();
        Timestamp fecha = new Timestamp(new java.util.Date().getTime());
        try{
            this.usuario = usuario;
            tipoIdentificacion = formulario.getTipoIdentificacion();
            identificacion = formulario.getIdentificacion();
            nitEmpresa=formulario.getNitEmpresa();
            //Verificar si la persona fue consultada recientemente
            TablaGenManagerDAO tgendao = new TablaGenManagerDAO(usuario.getBd());
            TablaGen t = tgendao.obtenerInformacionDato("PARAM_WSDC", "DIAS_CONSULTA_HC");
            
            int dias = dao.buscarDiasHC(formulario.getIdentificacion(), formulario.getTipoIdentificacion(), formulario.getNitEmpresa());
            if(dias==-1 || dias > Integer.parseInt(t.getReferencia())){
                //Obtener el id y clave para consultar en datacredito
                t = dao.obtenerConexion(nitEmpresa);
                if(t != null){
                    ServicioHistoriaCredito hc = new ServicioHistoriaCredito_Impl();
                    HCServiceImpl hcPort = hc.getServicioHistoriaCredito();

                    String xmlFormulario = null;
                    xmlFormulario = generarXmlFormulario(formulario);
                    dao.insertarHistoricoPeticion("H", fecha, usuario.getLogin(), Integer.parseInt(tipoIdentificacion), identificacion, formulario.getPrimerApellido(), xmlFormulario, formulario.getNitEmpresa());
                    generarTXT("tipo:"+formulario.getTipoIdentificacion()+" id:"+formulario.getIdentificacion()+" apellido:"+formulario.getPrimerApellido()+" paramForm:"+xmlFormulario, null);
                    String xml = hcPort.consultarHC(t.getReferencia(), t.getDato(), formulario.getTipoIdentificacion(), formulario.getIdentificacion(), formulario.getPrimerApellido(), xmlFormulario);
                    generarTXT(null, xml);
                    respuesta = parseXML(xml);
                }else{
                    respuesta.setCodigoRespuesta("F05");
                }
            }else{
                //Se consulta el score para esa identificacion
                Score score=dao.buscarScore(identificacion, tipoIdentificacion, nitEmpresa);
                respuesta.setScore(score.getPuntaje());
                respuesta.setClasificacion(score.getClasificacion());
            }
            
        } catch(javax.xml.rpc.ServiceException ex) {
            respuesta.setCodigoRespuesta("F02");
            ex.printStackTrace();
        } catch(java.rmi.RemoteException ex) {
            respuesta.setCodigoRespuesta("F02");
            ex.printStackTrace();
        } catch(IOException ioex){
            respuesta.setCodigoRespuesta("F03");
            ioex.printStackTrace();
        } catch (SQLException sqlEx) {
            respuesta.setCodigoRespuesta("F04");
            sqlEx.printStackTrace();
        } catch(Exception ex){
            respuesta.setCodigoRespuesta("F01");
            try {
                generarTXT(null, ex.toString()+"\n"+ex.getMessage());
            } catch (IOException ex1) {
                Logger.getLogger(WSHistCreditoService.class.getName()).log(Level.SEVERE, null, ex1);
            }
            ex.printStackTrace();
        }
        dao.actualizarRespuesta("H", fecha, usuario.getLogin(), respuesta.getCodigoRespuesta());
        return respuesta;
    }
    
    /**
     * Coloca todos los parametros del objeto recibido en un xml
     * @param formulario
     * @return xml generado
     */
    public String generarXmlFormulario(FormularioSuperfil formulario ) throws SQLException{
        String num_form=dao.obtenerNumeroFormulario(formulario.getNitEmpresa());
        String xml = "<Parametros>"
                + "<Parametro tipo=\"T\" nombre =\"00\" valor =\""+num_form+"\"/>"
                + "<Parametro tipo=\"N\" nombre =\"01\" valor =\""+formulario.getFechaNacimiento()+"\"/>"
                + "<Parametro tipo=\"N\" nombre =\"02\" valor =\""+(formulario.getTipoProducto()==null?"00":formulario.getTipoProducto())+"\"/>"
                + "<Parametro tipo=\"N\" nombre =\"03\" valor =\""+(formulario.getServicio()==null?"00":formulario.getServicio())+"\"/>"
                + "<Parametro tipo=\"N\" nombre =\"04\" valor =\""+(formulario.getCiudadMatricula()==null?"00":formulario.getCiudadMatricula())+"\"/>"
                + "<Parametro tipo=\"N\" nombre =\"05\" valor =\""+(formulario.getTipoVivienda()==null?"00":formulario.getTipoVivienda())+"\"/>"
                + "<Parametro tipo=\"N\" nombre =\"06\" valor =\""+(formulario.getIngreso()==null?"00":formulario.getIngreso())+"\"/>"
                + "<Parametro tipo=\"N\" nombre =\"07\" valor =\""+(formulario.getVlrCuotaMensual()==null?"00":formulario.getVlrCuotaMensual())+"\"/>"
                + "<Parametro tipo=\"N\" nombre =\"08\" valor =\""+(formulario.getMontoSolicitado()==null?"00":formulario.getMontoSolicitado())+"\"/>"
                + "<Parametro tipo=\"N\" nombre =\"09\" valor =\""+(formulario.getVlrVehiculo()==null?"00":formulario.getVlrVehiculo())+"\"/>"
                + "<Parametro tipo=\"N\" nombre =\"10\" valor =\""+(formulario.getVlrSemestre()==null?"00":formulario.getVlrSemestre())+"\"/>"
                + "<Parametro tipo=\"N\" nombre =\"11\" valor =\""+(formulario.getPlazo()==null?"00":formulario.getPlazo())+"\"/>"
                + "<Parametro tipo=\"N\" nombre =\"12\" valor =\""+(formulario.getPlazoUniversitario()==null?"00":formulario.getPlazoUniversitario())+"\"/>"
                + "<Parametro tipo=\"N\" nombre =\"13\" valor =\""+(formulario.getHaTenidoCreditoFintra()==null?"00":formulario.getHaTenidoCreditoFintra())+"\"/>"
                + "<Parametro tipo=\"N\" nombre =\"14\" valor =\""+(formulario.getArriendo()==null?"00":formulario.getArriendo())+"\"/>"
                + "<Parametro tipo=\"N\" nombre =\"15\" valor =\""+(formulario.getEstadoCivil()==null?"00":formulario.getEstadoCivil())+"\"/>"
                + "<Parametro tipo=\"N\" nombre =\"16\" valor =\""+(formulario.getConyugueTrabaja()==null?"00":formulario.getConyugueTrabaja())+"\"/>"
                + "</Parametros>";
        return xml;
    }

    /**
     * Busca listado de registro de un tabla especifica
     * @param webService
     * @param tabla
     * @return listado con datos coincidentes
     * @throws Exception cuando hay error
     */
    public ArrayList buscarTabla(String webService,String tabla) throws Exception{
        return dao.buscarTabla(webService, tabla);
    }

     /**
     * Busca listado de registro de un tabla y referencia especifica
     * @param webService
     * @param tabla
     * @return listado con datos coincidentes
     * @throws Exception cuando hay error
     */
    public ArrayList buscarTablaRef(String webService,String tabla, String ref) throws Exception{
        return dao.buscarTablaRef(webService, tabla, ref);
    }

    /**
     * Busca el codigo por medio de la tabla y la equivalencia
     * @param webService
     * @param tabla
     * @param equivalencia
     * @return codigo
     * @throws Exception cuando hay error
     */
    public String buscarCodigo(String webService,String tabla, String equivalencia) throws Exception{
        return dao.buscarCodigo(webService, tabla, equivalencia);
    }

    /**
     * Busca el valor por medio de la tabla y el codigo
     * @param webService
     * @param tabla
     * @param codigo
     * @return codigo
     * @throws Exception cuando hay error
     */
    public String buscarValor(String webService,String tabla, String codigo) throws Exception{
        return dao.buscarValor(webService, tabla, codigo);
    }

     /* Busca trae el valor total de las cuotas vigentes como titular de una persona
     * @param identificacion
     * @throws Exception cuando hay error
     */
    public double buscarSumCuotas(String identificacion, String nitEmpresa) throws Exception{
        return dao.buscarSumCuotas(identificacion, nitEmpresa);
    }
    

    /**
     * Vefifica la existencia de una persona en la bd
     * @throws Exception
     */
    public boolean existePersona(Persona persona) throws Exception{
        return dao.existePersona(persona);
    }
    
    /**
     * Consulta la informacion de una persona
     * @param identificacion
     * @param tipoIdentificacion
     * @return bean Persona con la informacion consultada
     * @throws Exception 
     */
    public Persona consultarPersona(String identificacion, String tipoIdentificacion, boolean fenalco) throws Exception{
        return dao.consultarPersona(identificacion, tipoIdentificacion,fenalco);
    }

    /**
     * Consulta la informacion de una persona Decisor
     * @param identificacion
     * @param tipoIdentificacion
     * @return bean Persona con la informacion consultada
     * @throws Exception 
     */
    public Persona consultarPersonaDecisor(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        return dao.consultarPersonaDecisor(identificacion, tipoIdentificacion,nitEmpresa);
    }

    /**
     * Obtiene los comentarios de la historia de credito de una persona
     * @param identificacion
     * @param tipoIdentificacion
     * @return ArrayList<ComentarioInforme>
     * @throws Exception 
     */
    public ArrayList<ComentarioInforme> consultarComentarios(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        return dao.consultarComentarios(identificacion, tipoIdentificacion,nitEmpresa);
    }
    
    /**
     * Obtiene las alertas de una persona
     * @param identificacion
     * @param tipoIdentificacion
     * @return ArrayList<Alerta>
     * @throws Exception 
     */
    public ArrayList<Alerta> consultarAlertas(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        return dao.consultarAlertas(identificacion, tipoIdentificacion,nitEmpresa);
    }
    
    /**
     * Obtiene las consultas de una persona
     * @param identificacion
     * @param tipoIdentificacion
     * @return ArrayList<Consulta>
     * @throws Exception 
     */
    public ArrayList<Consulta> consultarConsultas(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        return dao.consultarConsultas(identificacion, tipoIdentificacion,nitEmpresa);
    }
    
    /**
     * Obtiene los reclamos de una persona
     * @param identificacion
     * @param tipoIdentificacion
     * @return ArrayList<Reclamo>
     * @throws Exception 
     */
    public ArrayList<Reclamo> consultarReclamos(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        return dao.consultarReclamos(identificacion, tipoIdentificacion,nitEmpresa);
    }
    
    /**
     * Obtiene los scores y razones de una persona
     * @param identificacion
     * @param tipoIdentificacion
     * @return ArrayList<Score>
     * @throws Exception 
     */
    public ArrayList<Score> consultarScores(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        return dao.consultarScores(identificacion, tipoIdentificacion,nitEmpresa);
    }
    
    /**
     * Obtiene las respuestas personalizadas de una persona
     * @param identificacion
     * @param tipoIdentificacion
     * @return ArrayList<RespuestaPersonalizada>
     * @throws Exception 
     */
    public ArrayList<RespuestaPersonalizada> consultarRespuestaPersonalizada(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        return dao.consultarRespuestaPersonalizada(identificacion, tipoIdentificacion,nitEmpresa);
    }
    
    /**
     * Obtiene los totales, saldos, cupos y moras de una persona
     * @param identificacion
     * @param tipoIdentificacion
     * @return ArrayList<Valor>
     * @throws Exception 
     */
    public ArrayList<Valor> consultarTotales(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        return dao.consultarTotales(identificacion, tipoIdentificacion,nitEmpresa);
    }
    
    /**
     * Consulta las obligaciones abiertas de una persona
     * @param identificacion
     * @param tipoIdentificacion
     * @return ArrayList<CuentaCartera> con la informacion de las cuentas de ahorro, corriente, cartera y tarjetas de credito de la persona
     * @throws Exception 
     */
    public ArrayList<CuentaCartera> consultarObligacionesAbiertas(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        return dao.consultarObligacionesAbiertas(identificacion, tipoIdentificacion,nitEmpresa);
    }
    
    /**
     * Consulta las obligaciones cerradas de una persona
     * @param identificacion
     * @param tipoIdentificacion
     * @return ArrayList<CuentaCartera> con la informacion de las cuentas de ahorro, corriente, cartera y tarjetas de credito cerradas de la persona
     * @throws Exception 
     */
    public ArrayList<CuentaCartera> consultarObligacionesCerradas(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        return dao.consultarObligacionesCerradas(identificacion, tipoIdentificacion,nitEmpresa);
    }
    
    /**
     * Obtiene el endeudamiento global clasificado de una identificación
     * @param identificacion
     * @param tipoIdentificacion
     * @return ArrayList<EndeudamientoGlobal> con los resultados obtenidos
     * @throws Exception 
     */
    public ArrayList<EndeudamientoGlobal> consultarEndeudamientoGlobal(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        return dao.consultarEndeudamientoGlobal(identificacion, tipoIdentificacion,nitEmpresa);
    }
    
    public RespuestaSuperfil consultarHistoriaCredito(String tipoIdentificacion,String identificacion, String primerApellido, Usuario usuario){
        RespuestaSuperfil respuesta = new RespuestaSuperfil();
        Timestamp fecha = new Timestamp(new java.util.Date().getTime());
        try{
            this.usuario = usuario;
            this.tipoIdentificacion = tipoIdentificacion;
            this.identificacion = identificacion;

            //Verificar si la persona fue consultada recientemente
            TablaGenManagerDAO tgendao = new TablaGenManagerDAO();
            TablaGen t = tgendao.obtenerInformacionDato("PARAM_WSDC", "DIAS_CONSULTA_HC_TSP");

            int dias = dao.buscarDiasHC(identificacion, tipoIdentificacion,nitEmpresa);
            if(dias==-1 || dias > Integer.parseInt(t.getReferencia())){
                //Obtener el id y clave para consultar en datacredito
                t = dao.obtenerConexion(nitEmpresa);
                if(t != null){
                    ServicioHistoriaCredito hc = new ServicioHistoriaCredito_Impl();
                    HCServiceImpl hcPort = hc.getServicioHistoriaCredito();

                    dao.insertarHistoricoPeticion("H", fecha, usuario.getLogin(), Integer.parseInt(tipoIdentificacion), identificacion, primerApellido, "", nitEmpresa);
                    generarTXT("tipo:"+tipoIdentificacion+" id:"+identificacion+" apellido:"+primerApellido, null);
                    String xml = hcPort.consultarHC(t.getReferencia(), t.getDato(), tipoIdentificacion, identificacion, primerApellido);
                    generarTXT(null, xml);
                    respuesta = parseXML(xml);
                }else{
                    respuesta.setCodigoRespuesta("F05");
                }
            }else{
                //Se consulta el score para esa identificacion
                Score score=dao.buscarScore(identificacion, tipoIdentificacion,nitEmpresa);
                respuesta.setScore(score.getPuntaje());
                respuesta.setClasificacion(score.getClasificacion());
            }

        } catch(javax.xml.rpc.ServiceException ex) {
            respuesta.setCodigoRespuesta("F02");
            ex.printStackTrace();
        } catch(java.rmi.RemoteException ex) {
            respuesta.setCodigoRespuesta("F02");
            ex.printStackTrace();
        } catch(IOException ioex){
            respuesta.setCodigoRespuesta("F03");
            ioex.printStackTrace();
        } catch (SQLException sqlEx) {
            respuesta.setCodigoRespuesta("F04");
            sqlEx.printStackTrace();
        } catch(Exception ex){
            respuesta.setCodigoRespuesta("F01");
            try {
                generarTXT(null, ex.getMessage() + "/n" + ex.toString());
            } catch (IOException ex1) {
                Logger.getLogger(WSHistCreditoService.class.getName()).log(Level.SEVERE, null, ex1);
            }
            ex.printStackTrace();
        }
        dao.actualizarRespuesta("H", fecha, usuario.getLogin(), respuesta.getCodigoRespuesta());
        return respuesta;
    }

      /**
       * jpinedo
     * Valida unica cauzal de rechazo
     * @param identificacion
     * @param tipoIdentificacion
     * @return boolen 
     * @throws Exception
     */
            public boolean validarCausalMLE(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
             return dao.validarCausalMLE(identificacion, tipoIdentificacion, nitEmpresa);
    }
            
    /**
     * Realiza la consulta a datacredito de la historia de credito de una persona en decisor y la almacena en la BD
     * @param formulario
     * @param usuario 
     */
    public RespuestaDecisor  consultarHistoriaCreditoDecisor(FormularioDecisor formulario, Usuario usuario){
         RespuestaDecisor respuesta = new RespuestaDecisor();
         Timestamp fecha = new Timestamp(new java.util.Date().getTime());
        try{
            //parametros de configuracion.
            this.usuario = usuario;
            tipoIdentificacion = formulario.getTipoIdentificacion();
            identificacion = formulario.getIdentificacion();
            nitEmpresa=formulario.getNitEmpresa();
            //Verificar si la persona fue consultada recientemente en data credito
            TablaGenManagerDAO tgendao = new TablaGenManagerDAO(usuario.getBd());
            TablaGen t = tgendao.obtenerInformacionDato("PARAM_WSDC", "DIAS_CONSULTA_HC");
            int dias = dao.buscarDiasHC(formulario.getIdentificacion(), formulario.getTipoIdentificacion(), formulario.getNitEmpresa());
   
            //preguntamos si el numero de dias es es -1 o mayor 180 dias
            if(dias==-1 || dias > Integer.parseInt(t.getReferencia())){
                //Obtener el id y clave para consultar en datacredito en decisor 
                t = dao.obtenerConexionDecisor(nitEmpresa);
                if (t != null) {
            
                    //establecemos comunicacion con el webservice decisor
                   // com.fintra.wsdecisor.datacredito.ServicioHistoriaCredito_Impl hcd = new com.fintra.wsdecisor.datacredito.ServicioHistoriaCredito_Impl();
                   // com.fintra.wsdecisor.datacredito.HCServiceImpl2 hcsd = hcd.getServicioHistoriaCredito();
          
                    String xmlFormulario = null;
                    xmlFormulario = generarXmlFormularioDecisor(formulario, t);
                    if (!xmlFormulario.equals("")) {

                        dao.insertarHistoricoPeticion("H", fecha, usuario.getLogin(), Integer.parseInt(tipoIdentificacion), identificacion, formulario.getPrimerApellido(), xmlFormulario, formulario.getNitEmpresa());
                        generarTXT("tipo:" + formulario.getTipoIdentificacion() + " id:" + formulario.getIdentificacion() + " apellido:" + formulario.getPrimerApellido() + " paramForm:" + xmlFormulario, null);

                        String xml = consumoHCPuente(xmlFormulario, nitEmpresa); //hcsd.consultarHC2(xmlFormulario);
                        generarTXT(null, xml);
                     
                        respuesta = parseDecisorXML(xml);

                    }
             
                }else{
                    respuesta.setCodigoRespuesta("F05");
                }
            }else{
                //Se consulta el score para esa identificacion
                Score score=dao.buscarScore(identificacion, tipoIdentificacion, nitEmpresa);
                respuesta.setScore(score.getPuntaje());
                respuesta.setClasificacion(score.getClasificacion());
            }
            
        } catch(javax.xml.rpc.ServiceException ex) {
            respuesta.setCodigoRespuesta("F02");
            ex.printStackTrace();
        } catch(java.rmi.RemoteException ex) {
            respuesta.setCodigoRespuesta("F02");
            ex.printStackTrace();
        } catch(IOException ioex){
            respuesta.setCodigoRespuesta("F03");
            ioex.printStackTrace();
        } catch (SQLException sqlEx) {
            respuesta.setCodigoRespuesta("F04");
            sqlEx.printStackTrace();
        } catch(Exception ex){
            respuesta.setCodigoRespuesta("F01");
            try {
                generarTXT(null, ex.toString()+"\n"+ex.getMessage());
            } catch (IOException ex1) {
                Logger.getLogger(WSHistCreditoService.class.getName()).log(Level.SEVERE, null, ex1);
            }
            ex.printStackTrace();
        }
        dao.actualizarRespuesta("H", fecha, usuario.getLogin(), respuesta.getCodigoRespuesta());
        return respuesta;
    }  
    
    
    /**
     * Coloca todos los parametros del objeto recibido en un xml
     * @param formulario
     * @return xml generado
     */
    public String generarXmlFormularioDecisor(FormularioDecisor formulario, TablaGen t ) throws SQLException{
       
        String straid = dao.getStraid_and_Strnam(formulario.getNitEmpresa(), "STRAID");
        String strnam = dao.getStraid_and_Strnam(formulario.getNitEmpresa(), "STRNAM");
        String xml = "";
        if (!straid.equals("") && !strnam.equals("")) {
            xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                    + "<Solicitud>"
                        + "<Solicitud clave=\""+t.getDato()+"\" identificacion=\""+formulario.getIdentificacion()+"\" primerApellido=\""+formulario.getPrimerApellido()+"\" producto=\"64\" tipoIdentificacion=\""+formulario.getTipoIdentificacion()+"\" usuario=\""+t.getReferencia()+"\" >"
                            + "<Parametros>"
                                + "<Parametro tipo=\"T\" nombre =\"STRAID\" valor =\"" + straid + "\"/>" //id estrategia
                                + "<Parametro tipo=\"T\" nombre =\"STRNAM\" valor =\"" + strnam + "\"/>" //nombre de las estrategia
                                + "<Parametro tipo=\"N\" nombre =\"Fr_Anio_Nacimiento\" valor =\"" + formulario.getAnio_Nacimiento() + "\"/>"
                                + "<Parametro tipo=\"N\" nombre =\"Fr_Mes_Nacimiento\" valor =\"" + formulario.getMes_Nacimiento() + "\"/>"
                                + "<Parametro tipo=\"N\" nombre =\"Fr_Tipo_Producto\" valor =\"" + (formulario.getTipo_Producto() == null ? "0" : formulario.getTipo_Producto()) + "\"/>"
                                + "<Parametro tipo=\"N\" nombre =\"Fr_Servicio\" valor =\"" + (formulario.getServicio() == null ? "0" : formulario.getServicio()) + "\"/>"
                                + "<Parametro tipo=\"N\" nombre =\"Fr_Ciudad_Matricula\" valor =\"" + (formulario.getCiudad_Matricula() == null ? "0" : formulario.getCiudad_Matricula()) + "\"/>"
                                + "<Parametro tipo=\"N\" nombre =\"Fr_Tipo_Vivienda\" valor =\"" + (formulario.getTipo_Vivienda() == null ? "0" : formulario.getTipo_Vivienda()) + "\"/>"
                                + "<Parametro tipo=\"N\" nombre =\"Fr_Arriendo\" valor =\"" + (formulario.getArriendo() == null ? "0" : formulario.getArriendo()) + "\"/>"
                                + "<Parametro tipo=\"N\" nombre =\"Fr_Estado_Civil\" valor =\"" + (formulario.getEstado_Civil() == null ? "0" : formulario.getEstado_Civil()) + "\"/>"
                                + "<Parametro tipo=\"N\" nombre =\"Fr_Conyugue\" valor =\"" + (formulario.getConyugue() == null ? "0" : formulario.getConyugue()) + "\"/>"
                                + "<Parametro tipo=\"N\" nombre =\"Fr_Ingreso\" valor =\"" + (formulario.getIngreso() == null ? "0" : formulario.getIngreso()) + "\"/>"
                                + "<Parametro tipo=\"N\" nombre =\"Fr_Monto_Solicitado\" valor =\"" + (formulario.getMonto_Solicitado() == null ? "0" : formulario.getMonto_Solicitado()) + "\"/>"
                                + "<Parametro tipo=\"N\" nombre =\"Fr_Cuota_Credito_Solicitado\" valor =\"" + (formulario.getCuota_Credito_Solicitado() == null ? "0" : formulario.getCuota_Credito_Solicitado()) + "\"/>"
                                + "<Parametro tipo=\"N\" nombre =\"Fr_Valor_Vehiculo\" valor =\"" + (formulario.getValor_Vehiculo() == null ? "0" : formulario.getValor_Vehiculo()) + "\"/>"
                                + "<Parametro tipo=\"N\" nombre =\"Fr_Valor_Semestre\" valor =\"" + (formulario.getValor_Semestre() == null ? "0" : formulario.getValor_Semestre()) + "\"/>"
                                + "<Parametro tipo=\"N\" nombre =\"Fr_Plazo\" valor =\"" + (formulario.getPlazo() == null ? "0" : formulario.getPlazo()) + "\"/>"
                                + "<Parametro tipo=\"N\" nombre =\"Fr_Credito_Fenalco_Bolivar\" valor =\"" + (formulario.getCredito_Fenalco_Bolivar() == null ? "0" : formulario.getCredito_Fenalco_Bolivar()) + "\"/>"
                            + "</Parametros>"
                        + "</Solicitud>"
                    + "</Solicitud>";
        }


        return xml;

    }
    
    public RespuestaDecisor parseDecisorXML(String xml) throws Exception {
        RespuestaDecisor respuesta = new RespuestaDecisor();
        try {

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new InputSource(new ByteArrayInputStream(xml.getBytes("utf-8"))));

            Element root = doc.getDocumentElement();
            root.normalize();
            NodeList listInforme = null;
            if (root.getElementsByTagName("Informe").getLength() > 0) {
                listInforme = root.getElementsByTagName("Informe");
                Element elInforme = (Element) listInforme.item(0);
                if (!elInforme.getAttribute("respuesta").isEmpty()) {
                    respuesta.setCodigoRespuesta(elInforme.getAttribute("respuesta"));
                }

            }
           
            if (root.getFirstChild() != null) {
                tService.crearStatement();
                obtenerPersonaDecisor(root);
                String[] sql = eliminarInfoPersona(identificacion, tipoIdentificacion, nitEmpresa).split(";");
                for(int i=0; i < sql.length; i++){
                   tService.getSt().addBatch(sql[i]); 
                }
                obtenerCuentasAhorroDecisor(root);
                obtenerCuentasCorrienteDecisor(root);
                obtenerTarjetasCreditoDecisor(root);
                obtenerCuentasCarteraDecisor(root);
                obtenerEndeudamientoGlobalDecisor(root);
                obtenerConsultasDecisor(root);
                obtenerAlertasDecisor(root);
               // obtenerComentarios(root);
                obtenerScoreDecisor(root);
                obtenerRespuestaPersonalizadaDecisor(root);
                tService.execute();
                
                //Se consulta el score para esa identificacion
                Score score=dao.buscarScore(identificacion, tipoIdentificacion,nitEmpresa);
                respuesta.setScore(score.getPuntaje());
                respuesta.setClasificacion(score.getClasificacion());
            }

        } catch (Throwable t) {
            t.printStackTrace();
            throw new Exception("ha ocurrido un error guardando los datos "+t.getMessage());
        }
        return respuesta;
    }

     public void obtenerPersonaDecisor(Element root) throws Exception{
        NodeList listPersonas = null;
        Persona persona = new Persona();
        Element id=null;
        Element edad=null;
        if(root.getElementsByTagName("NaturalNacional").getLength()>0){//Natural Nacional
            listPersonas = root.getElementsByTagName("NaturalNacional");
            persona.setTipoIdentificacion("1");
            Element elPersona = (Element) listPersonas.item(0);
            
            //datos personales de la persona.
            if(!elPersona.getAttribute("nombres").isEmpty()){
                persona.setNombre(elPersona.getAttribute("nombres"));
            }
            if(!elPersona.getAttribute("primerApellido").isEmpty()){
                persona.setPrimerApellido(elPersona.getAttribute("primerApellido"));
            }
            if(!elPersona.getAttribute("segundoApellido").isEmpty()){
                persona.setSegundoApellido(elPersona.getAttribute("segundoApellido"));
            }
            if(!elPersona.getAttribute("nombreCompleto").isEmpty()){
                persona.setNombreCompleto(elPersona.getAttribute("nombreCompleto"));
            }
            if(!elPersona.getAttribute("genero").isEmpty()){
                persona.setGenero(elPersona.getAttribute("genero"));
            }
            if(!elPersona.getAttribute("validada").isEmpty()){
                persona.setValidada(Boolean.parseBoolean(elPersona.getAttribute("validada")));
            }

            id = (Element) elPersona.getElementsByTagName("Identificacion").item(0);
            //Se obtiene la informacion de la identificacion
            if(!id.getAttribute("estado").isEmpty()){
                persona.setEstadoId(id.getAttribute("estado"));
            }
            if(!id.getAttribute("fechaExpedicion").isEmpty()){
               // long fechaExpedicion = Long.parseLong(id.getAttribute("fechaExpedicion"));
                persona.setFechaExpedicionId((Timestamp.valueOf(id.getAttribute("fechaExpedicion")+" 00:00:00")));
            }
            if(!id.getAttribute("ciudad").isEmpty()){
                persona.setCiudadId(id.getAttribute("ciudad"));
            }
            if(!id.getAttribute("departamento").isEmpty()){
                persona.setDepartamentoId(id.getAttribute("departamento"));
            }
            if(!id.getAttribute("numero").isEmpty()){
                persona.setIdentificacion(identificacion);
            }


            //Se obtiene la informacion de la edad
            if(elPersona.getElementsByTagName("Edad").getLength()>0){
                edad = (Element) elPersona.getElementsByTagName("Edad").item(0);
                if(!edad.getAttribute("min").isEmpty()){
                    persona.setEdadMin(edad.getAttribute("min"));
                }
                if(!edad.getAttribute("max").isEmpty()){
                    persona.setEdadMax(edad.getAttribute("max"));
                }
            }

        }else if(root.getElementsByTagName("NaturalExtranjera").getLength()>0){//Natural extranjera
           /* listPersonas = root.getElementsByTagName("NaturalExtranjera");
            persona.setTipoIdentificacion("4");
            Element elPersona = (Element) listPersonas.item(0);
            if(!elPersona.getAttribute("nombre").isEmpty()){
                persona.setNombre(elPersona.getAttribute("nombre"));
            }
            if(!elPersona.getAttribute("nacionalidad").isEmpty()){
                persona.setNacionalidad(elPersona.getAttribute("nacionalidad"));
            }
            if(!elPersona.getAttribute("identificacion").isEmpty()){
                persona.setIdentificacion(identificacion);
            }*/

        }else if(root.getElementsByTagName("JuridicaNacional").getLength()>0){//Juridica Nacional
           /* listPersonas = root.getElementsByTagName("JuridicaNacional");
            persona.setTipoIdentificacion("2");
            Element elPersona = (Element) listPersonas.item(0);
            if(!elPersona.getAttribute("nombre").isEmpty()){
                persona.setNombre(elPersona.getAttribute("nombre"));
            }
            if(!elPersona.getAttribute("identificacion").isEmpty()){
                persona.setIdentificacion(identificacion);
            }*/

        }else if(root.getElementsByTagName("JuridicaExtranjera").getLength()>0){//Juridica extranjera
           /* listPersonas = root.getElementsByTagName("JuridicaExtranjera");
            persona.setTipoIdentificacion("3");
            Element elPersona = (Element) listPersonas.item(0);
            if(!elPersona.getAttribute("nombre").isEmpty()){
                persona.setNombre(elPersona.getAttribute("nombre"));
            }
            if(!elPersona.getAttribute("identificacion").isEmpty()){
                persona.setIdentificacion(identificacion);
            }*/

        }
        persona.setNitEmpresa(nitEmpresa);
        persona.setCreationUser(usuario.getLogin());
        persona.setUserUpdate(usuario.getLogin());
        persona.setWebService("HC");
        tService.getSt().addBatch(this.guardarPersona(persona));
    }
     
   public void obtenerCuentasAhorroDecisor(Element root) throws Exception{
       
        Element estado=null;
        NodeList listCuentas = root.getElementsByTagName("CuentaAhorro");
        int numCuentas = listCuentas.getLength();
        for (int i = 0; i < numCuentas; i++) {
            Element elCuenta = (Element) listCuentas.item(i);
            CuentaAhorroDecisor cuenta = new CuentaAhorroDecisor();
        
             if(!elCuenta.getAttribute("bloqueada").isEmpty()){
                cuenta.setBloqueada(Boolean.parseBoolean(elCuenta.getAttribute("bloqueada")));
            }
            if (!elCuenta.getAttribute("entidad").isEmpty()) {
                cuenta.setEntidad(elCuenta.getAttribute("entidad"));
            }
            if (!elCuenta.getAttribute("numero").isEmpty()) {
                cuenta.setNumero(elCuenta.getAttribute("numero"));
            }
            if(!elCuenta.getAttribute("fechaApertura").isEmpty()){
                //long fecha = Long.parseLong(elCuenta.getAttribute("fechaApertura"));
                cuenta.setFechaApertura(Timestamp.valueOf(elCuenta.getAttribute("fechaApertura")+" 00:00:00"));
            }
            if(!elCuenta.getAttribute("situacionTitular").isEmpty()){
                cuenta.setSituacionTitular(Short.parseShort(elCuenta.getAttribute("situacionTitular")));
            }
            if(!elCuenta.getAttribute("oficina").isEmpty()){
                cuenta.setOficina(elCuenta.getAttribute("oficina"));
            }
            if(!elCuenta.getAttribute("ciudad").isEmpty()){
                cuenta.setCiudad(elCuenta.getAttribute("ciudad"));
            }
            
           /* if(!elCuenta.getAttribute("codigoDaneCiudad").isEmpty()){
                cuenta.setCodigoDaneCiudad(elCuenta.getAttribute("codigoDaneCiudad"));
            }*/
            
            //obtengo el estado de la cuenta... 
            estado = (Element) elCuenta.getElementsByTagName("Estado").item(0);
            if(estado !=null) {
                
                if(!estado.getAttribute("codigo").isEmpty()){
                    cuenta.setEstado(estado.getAttribute("codigo"));
                }
                if(!estado.getAttribute("fecha").isEmpty()){
                    //long fecha = Long.parseLong(estado.getAttribute("fecha"));
                    cuenta.setUltimaActualizacion(Timestamp.valueOf(estado.getAttribute("fecha")+" 00:00:00"));
                }
          
            }
            cuenta.setTipoIdentificacion(tipoIdentificacion);
            cuenta.setIdentificacion(identificacion);
            cuenta.setCreationUser(usuario.getLogin());
            cuenta.setUserUpdate(usuario.getLogin());
            cuenta.setNitEmpresa(nitEmpresa);

            tService.getSt().addBatch(this.insertarCuentaAhorroDecisor(cuenta));
/* Preunguntar lo d elso reclamos en la trama de salida.
           NodeList listReclamos = elCuenta.getElementsByTagName("reclamo");
            if(listReclamos.getLength()>0){
                Element elReclamo = (Element) listReclamos.item(0);
                Reclamo reclamo = new Reclamo();
                reclamo.setTipoPadre("CAH");
                if(!elReclamo.getAttribute("tipoLeyenda").isEmpty()){
                    reclamo.setTipoLeyenda(elReclamo.getAttribute("tipoLeyenda"));
                }
                if(!elReclamo.getAttribute("fechaCierre").isEmpty()){
                    long fecha = Long.parseLong(elReclamo.getAttribute("fechaCierre"));
                    reclamo.setFechaCierre(new Timestamp(fecha));
                }
                if(!elReclamo.getAttribute("estado").isEmpty()){
                    reclamo.setEstado(elReclamo.getAttribute("estado"));
                }
                if(!elReclamo.getAttribute("tipo").isEmpty()){
                    reclamo.setTipo(elReclamo.getAttribute("tipo"));
                }
                if(!elReclamo.getAttribute("fecha").isEmpty()){
                    long fecha = Long.parseLong(elReclamo.getAttribute("fecha"));
                    reclamo.setFecha(new Timestamp(fecha));
                }
                if(!elReclamo.getAttribute("ratificado").isEmpty()){
                    reclamo.setRatificado(Boolean.parseBoolean(elReclamo.getAttribute("ratificado")));
                }
                reclamo.setTexto(elReclamo.getTextContent());
                reclamo.setTipoIdentificacion(tipoIdentificacion);
                reclamo.setIdentificacion(identificacion);
                reclamo.setCreationUser(usuario.getLogin());
                reclamo.setUserUpdate(usuario.getLogin());
                reclamo.setNitEmpresa(nitEmpresa);
                tService.getSt().addBatch(this.insertarReclamo(reclamo));
              
            }      */      
        }
    }
   
   public void obtenerCuentasCorrienteDecisor(Element root) throws Exception{
        
       Element estado=null;
       NodeList listCuentas = root.getElementsByTagName("CuentaCorriente");
        int numCuentas = listCuentas.getLength();
        for (int i = 0; i < numCuentas; i++) {
            Element elCuenta = (Element) listCuentas.item(i);
            CuentaCorrienteDecisor cuenta = new CuentaCorrienteDecisor();
  
            if (!elCuenta.getAttribute("bloqueada").isEmpty()) {
                cuenta.setBloqueada(Boolean.parseBoolean(elCuenta.getAttribute("bloqueada")));
            }
           
            if (!elCuenta.getAttribute("entidad").isEmpty()) {
                cuenta.setEntidad(elCuenta.getAttribute("entidad"));
            }
            
            if (!elCuenta.getAttribute("numero").isEmpty()) {
                cuenta.setNumero(elCuenta.getAttribute("numero"));
            }
           
            if (!elCuenta.getAttribute("fechaApertura").isEmpty()) {
                //long fecha = Long.parseLong(elCuenta.getAttribute("fechaApertura"));
                cuenta.setFechaApertura(Timestamp.valueOf(elCuenta.getAttribute("fechaApertura")+" 00:00:00"));
            }
           
          /*  if (!elCuenta.getAttribute("calificacion").isEmpty()) {
                cuenta.setCalificacion(elCuenta.getAttribute("calificacion"));
            }*/

            if (!elCuenta.getAttribute("situacionTitular").isEmpty()) {
                cuenta.setSituacionTitular(elCuenta.getAttribute("situacionTitular"));
            }
            
            if(!elCuenta.getAttribute("oficina").isEmpty()){
                cuenta.setOficina(elCuenta.getAttribute("oficina"));
            }
          
            if(!elCuenta.getAttribute("ciudad").isEmpty()){
                cuenta.setCiudad(elCuenta.getAttribute("ciudad"));
            }
            
          /*  if(!elCuenta.getAttribute("codigoDaneCiudad").isEmpty()){
                cuenta.setCodigoDaneCiudad(elCuenta.getAttribute("codigoDaneCiudad"));
            }
            */
            if(!elCuenta.getAttribute("codSuscriptor").isEmpty()){
                cuenta.setCodSuscriptor(elCuenta.getAttribute("codSuscriptor"));
            }
            
          
            //obtengo el estado de la cuenta... 
            estado = (Element) elCuenta.getElementsByTagName("Estado").item(0);
            if (estado != null) {
               
                if (!estado.getAttribute("codigo").isEmpty()) {
                    cuenta.setEstado(estado.getAttribute("codigo"));
                }
                if (!estado.getAttribute("fecha").isEmpty()) {
                    // long fecha = Long.parseLong(estado.getAttribute("fecha"));
                    cuenta.setUltimaActualizacion(Timestamp.valueOf(estado.getAttribute("fecha") + " 00:00:00"));
                }
            }
            
            cuenta.setTipoIdentificacion(tipoIdentificacion);
            cuenta.setIdentificacion(identificacion);
            cuenta.setCreationUser(usuario.getLogin());
            cuenta.setUserUpdate(usuario.getLogin());
            cuenta.setNitEmpresa(nitEmpresa);

            tService.getSt().addBatch(this.insertarCuentaCorrienteDecisor(cuenta));

           /* NodeList listReclamos = elCuenta.getElementsByTagName("reclamo");
            if(listReclamos.getLength()>0){
                Element elReclamo = (Element) listReclamos.item(0);
                Reclamo reclamo = new Reclamo();
                reclamo.setTipoPadre("CCO");
                if(!elReclamo.getAttribute("tipoLeyenda").isEmpty()){
                    reclamo.setTipoLeyenda(elReclamo.getAttribute("tipoLeyenda"));
                }
                if(!elReclamo.getAttribute("fechaCierre").isEmpty()){
                    long fecha = Long.parseLong(elReclamo.getAttribute("fechaCierre"));
                    reclamo.setFechaCierre(new Timestamp(fecha));
                }
                if(!elReclamo.getAttribute("estado").isEmpty()){
                    reclamo.setEstado(elReclamo.getAttribute("estado"));
                }
                if(!elReclamo.getAttribute("tipo").isEmpty()){
                    reclamo.setTipo(elReclamo.getAttribute("tipo"));
                }
                if(!elReclamo.getAttribute("fecha").isEmpty()){
                    long fecha = Long.parseLong(elReclamo.getAttribute("fecha"));
                    reclamo.setFecha(new Timestamp(fecha));
                }
                if(!elReclamo.getAttribute("ratificado").isEmpty()){
                    reclamo.setRatificado(Boolean.parseBoolean(elReclamo.getAttribute("ratificado")));
                }
                reclamo.setTexto(elReclamo.getTextContent());
                reclamo.setTipoIdentificacion(tipoIdentificacion);
                reclamo.setIdentificacion(identificacion);
                reclamo.setCreationUser(usuario.getLogin());
                reclamo.setUserUpdate(usuario.getLogin());
                reclamo.setNitEmpresa(nitEmpresa);

                tService.getSt().addBatch(this.insertarReclamo(reclamo));
            }*/
        }
    }
   
   public void obtenerTarjetasCreditoDecisor(Element root) throws Exception{
       Element estado = null, amparada = null, valores=null;
       NodeList listTarjetas = root.getElementsByTagName("TarjetaCredito");
        int numTarjetas = listTarjetas.getLength();
        for (int i = 0; i < numTarjetas; i++) {
            Element elTarjeta = (Element) listTarjetas.item(i);
            TarjetaCreditoDecisor tarjeta = new TarjetaCreditoDecisor();
            if(!elTarjeta.getAttribute("bloqueada").isEmpty()){
                tarjeta.setBloqueada(Boolean.parseBoolean(elTarjeta.getAttribute("bloqueada")));
            }
            if(!elTarjeta.getAttribute("entidad").isEmpty()){
                tarjeta.setEntidad(elTarjeta.getAttribute("entidad"));
            }
            
            if(!elTarjeta.getAttribute("numero").isEmpty()){
                tarjeta.setNumero(elTarjeta.getAttribute("numero"));
            }
            
            if(!elTarjeta.getAttribute("fechaApertura").isEmpty()){
                //long fecha = Long.parseLong(elTarjeta.getAttribute("fechaApertura"));
                tarjeta.setFechaApertura(Timestamp.valueOf(elTarjeta.getAttribute("fechaApertura")+" 00:00:00"));
            }
            
            if(!elTarjeta.getAttribute("fechaVencimiento").isEmpty()){
               // long fecha = Long.parseLong(elTarjeta.getAttribute("fechaVencimiento"));
                tarjeta.setFechaVencimiento(Timestamp.valueOf(elTarjeta.getAttribute("fechaVencimiento")+" 00:00:00"));
            }
            
            if(!elTarjeta.getAttribute("comportamiento").isEmpty()){
                tarjeta.setComportamiento(elTarjeta.getAttribute("comportamiento"));
            }
            
            if(!elTarjeta.getAttribute("formaPago").isEmpty()){
                tarjeta.setFormaPago(elTarjeta.getAttribute("formaPago"));
            }
            
            if(!elTarjeta.getAttribute("situacionTitular").isEmpty()){
                tarjeta.setSituacionTitular(Integer.parseInt(elTarjeta.getAttribute("situacionTitular")));
            }
            
            
            if(!elTarjeta.getAttribute("oficina").isEmpty()){
                tarjeta.setOficina(elTarjeta.getAttribute("oficina"));
            }
            
            /*if(!elTarjeta.getAttribute("codigoDaneCiudad").isEmpty()){
                tarjeta.setCodigoDaneCiudad(elTarjeta.getAttribute("codigoDaneCiudad"));
            }*/
            
       

            //obtengo el estado de la cuenta... 
            estado = (Element) elTarjeta.getElementsByTagName("EstadoPago").item(0);
            if (!estado.getAttribute("codigo").isEmpty()) {
                tarjeta.setEstado(estado.getAttribute("codigo"));
            }
            if (!estado.getAttribute("fecha").isEmpty()) {
               // long fecha = Long.parseLong(estado.getAttribute("fecha"));
                tarjeta.setUltimaActualizacion(Timestamp.valueOf(estado.getAttribute("fecha")+" 00:00:00"));
            }

            //Estado origen
            estado = (Element) elTarjeta.getElementsByTagName("EstadoOrigen").item(0);
            if (!estado.getAttribute("codigo").isEmpty()) {
                tarjeta.setEstadoOrigen(estado.getAttribute("codigo"));
            }

   
            //amparada 
            amparada = (Element) elTarjeta.getElementsByTagName("Caracteristicas").item(0);
            if(!amparada.getAttribute("amparada").isEmpty()){
                tarjeta.setAmparada(Boolean.parseBoolean(amparada.getAttribute("amparada")));
            }
            
  
            tarjeta.setTipoIdentificacion(tipoIdentificacion);
            tarjeta.setIdentificacion(identificacion);
            tarjeta.setCreationUser(usuario.getLogin());
            tarjeta.setUserUpdate(usuario.getLogin());
            tarjeta.setNitEmpresa(nitEmpresa);

            tService.getSt().addBatch(this.insertarTarjetaCreditoDecisor(tarjeta));

            /*NodeList listReclamos = elTarjeta.getElementsByTagName("reclamo");
            if(listReclamos.getLength()>0){
                Element elReclamo = (Element) listReclamos.item(0);
                Reclamo reclamo = new Reclamo();
                reclamo.setTipoPadre("TCR");
                if(!elReclamo.getAttribute("tipoLeyenda").isEmpty()){
                    reclamo.setTipoLeyenda(elReclamo.getAttribute("tipoLeyenda"));
                }
                if(!elReclamo.getAttribute("fechaCierre").isEmpty()){
                    long fecha = Long.parseLong(elReclamo.getAttribute("fechaCierre"));
                    reclamo.setFechaCierre(new Timestamp(fecha));
                }
                if(!elReclamo.getAttribute("estado").isEmpty()){
                    reclamo.setEstado(elReclamo.getAttribute("estado"));
                }
                if(!elReclamo.getAttribute("tipo").isEmpty()){
                    reclamo.setTipo(elReclamo.getAttribute("tipo"));
                }
                if(!elReclamo.getAttribute("fecha").isEmpty()){
                    long fecha = Long.parseLong(elReclamo.getAttribute("fecha"));
                    reclamo.setFecha(new Timestamp(fecha));
                }
                if(!elReclamo.getAttribute("ratificado").isEmpty()){
                    reclamo.setRatificado(Boolean.parseBoolean(elReclamo.getAttribute("ratificado")));
                }
                reclamo.setTexto(elReclamo.getTextContent());
                reclamo.setTipoIdentificacion(tipoIdentificacion);
                reclamo.setIdentificacion(identificacion);
                reclamo.setCreationUser(usuario.getLogin());
                reclamo.setUserUpdate(usuario.getLogin());

                tService.getSt().addBatch(this.insertarReclamo(reclamo));
            }*/

            NodeList listValores = elTarjeta.getElementsByTagName("Valores");
            int numValores = listValores.getLength();
            for (int j = 0; j < numValores; j++) {
                Element elValor = (Element) listValores.item(j);
                Valor valor = new Valor();
                valor.setTipoPadre("TCR");
                //Los datos corresponden al valor real. No estan en miles
                valores = (Element) elValor.getElementsByTagName("Valor").item(0);

                if (valores != null) {//preguntamos si es diferente de null

                    if (!valores.getAttribute("cupoTotal").isEmpty()) {
                    valor.setCupo(Double.parseDouble(valores.getAttribute("cupoTotal")));
                }
                    if (!valores.getAttribute("saldoActual").isEmpty()) {
                    valor.setSaldoActual(Double.parseDouble(valores.getAttribute("saldoActual")));
                }
                    if (!valores.getAttribute("saldoMora").isEmpty()) {
                    valor.setSaldoMora(Double.parseDouble(valores.getAttribute("saldoMora")));
                }
                    if (!valores.getAttribute("cuota").isEmpty()) {
                    valor.setCuota(Double.parseDouble(valores.getAttribute("cuota")));
                }
                valor.setTipoIdentificacion(tipoIdentificacion);
                valor.setIdentificacion(identificacion);
                valor.setCreationUser(usuario.getLogin());
                valor.setUserUpdate(usuario.getLogin());
                valor.setNitEmpresa(nitEmpresa);

                tService.getSt().addBatch(this.insertarValor(valor));

            }
            }

          /*  NodeList listAlivio = elTarjeta.getElementsByTagName("alivio");
            if(listAlivio.getLength()>0){
                Element elAlivio = (Element) listAlivio.item(0);
                Alivio alivio = new Alivio();
                alivio.setTipoPadre("TCR");
                if(!elAlivio.getAttribute("estado").isEmpty()){
                    alivio.setEstado(elAlivio.getAttribute("estado"));
                }
                if(!elAlivio.getAttribute("mes").isEmpty()){
                    alivio.setMes(elAlivio.getAttribute("mes"));
                }
                alivio.setTipoIdentificacion(tipoIdentificacion);
                alivio.setIdentificacion(identificacion);
                alivio.setCreationUser(usuario.getLogin());
                alivio.setUserUpdate(usuario.getLogin());

                tService.getSt().addBatch(this.insertarAlivio(alivio));
            }*/
        }
    }
   
    public void obtenerCuentasCarteraDecisor(Element root) throws Exception{
        Element caracteristicas = null, estados=null, valores=null ; 
        NodeList listCuentas = root.getElementsByTagName("CuentaCartera");
        int numCuentas = listCuentas.getLength();
        for (int i = 0; i < numCuentas; i++) {
            Element elCuenta = (Element) listCuentas.item(i);
            CuentaCarteraDecisor cuenta = new CuentaCarteraDecisor();
            
            if (!elCuenta.getAttribute("bloqueada").isEmpty()) {
                cuenta.setBloqueada(Boolean.parseBoolean(elCuenta.getAttribute("bloqueada")));
            }

            if (!elCuenta.getAttribute("entidad").isEmpty()) {
                cuenta.setEntidad(elCuenta.getAttribute("entidad"));
            }

            if (!elCuenta.getAttribute("numero").isEmpty()) {
                cuenta.setNumero(elCuenta.getAttribute("numero"));
            }

            if (!elCuenta.getAttribute("fechaApertura").isEmpty()) {
               // long fechaApertura = Long.parseLong(elCuenta.getAttribute("fechaApertura"));
                cuenta.setFechaApertura(Timestamp.valueOf(elCuenta.getAttribute("fechaApertura")+" 00:00:00"));
            }
            if (!elCuenta.getAttribute("fechaVencimiento").isEmpty()) {
               // long fechaVenc = Long.parseLong(elCuenta.getAttribute("fechaVencimiento"));
                cuenta.setFechaVencimiento(Timestamp.valueOf(elCuenta.getAttribute("fechaVencimiento")+" 00:00:00"));
            }

            if (!elCuenta.getAttribute("comportamiento").isEmpty()) {
                cuenta.setComportamiento(elCuenta.getAttribute("comportamiento"));
            }
            
            if (!elCuenta.getAttribute("formaPago").isEmpty()) {
                cuenta.setFormaPago(elCuenta.getAttribute("formaPago"));
            }
            
            if (!elCuenta.getAttribute("situacionTitular").isEmpty()) {
                cuenta.setSituacionTitular(elCuenta.getAttribute("situacionTitular"));
            }
           
            if (!elCuenta.getAttribute("oficina").isEmpty()) {
                cuenta.setOficina(elCuenta.getAttribute("oficina"));
            }
            
            if(!elCuenta.getAttribute("codSuscriptor").isEmpty()){
                cuenta.setCodSuscriptor(elCuenta.getAttribute("codSuscriptor"));
            }
            
           /*if(!elCuenta.getAttribute("priodicidad").isEmpty()){
                cuenta.setPeriodicidad(elCuenta.getAttribute("priodicidad"));
            } */
            
            //Caracteristicas. 
            caracteristicas=(Element) elCuenta.getElementsByTagName("Caracteristicas").item(0);
            if(caracteristicas != null){
                
                if(!caracteristicas.getAttribute("tipoCuenta").isEmpty()){
                    cuenta.setTipoCuenta(caracteristicas.getAttribute("tipoCuenta"));
                }

                if(!caracteristicas.getAttribute("tipoObligacion").isEmpty()){
                    cuenta.setTipoObligacion(caracteristicas.getAttribute("tipoObligacion"));
                }

                if(!caracteristicas.getAttribute("tipoContrato").isEmpty()){
                    cuenta.setTipoContrato(caracteristicas.getAttribute("tipoContrato"));
                }

                if(!caracteristicas.getAttribute("ejecucionContrato").isEmpty()){
                    cuenta.setEjecucionContrato(Short.parseShort(caracteristicas.getAttribute("ejecucionContrato")));
                }

                if(!caracteristicas.getAttribute("mesesPermanencia").isEmpty()){
                    cuenta.setMesesPermanencia(Short.parseShort(caracteristicas.getAttribute("mesesPermanencia")));
                }

                if(!caracteristicas.getAttribute("garantia").isEmpty()){
                    cuenta.setGarantia(caracteristicas.getAttribute("garantia"));
                }

                 if(!caracteristicas.getAttribute("calidadDeudor").isEmpty()){
                    cuenta.setCalidadDeudor(caracteristicas.getAttribute("calidadDeudor"));
                }
            }       
            //estados cartera.
            estados= (Element) elCuenta.getElementsByTagName("EstadoPago").item(0);
            if(estados != null ){
                if(!estados.getAttribute("codigo").isEmpty()){
                    cuenta.setEstado(estados.getAttribute("codigo"));
                }

                if (!estados.getAttribute("fecha").isEmpty()) {
                   // long fecha = Long.parseLong(estados.getAttribute("fecha"));
                    cuenta.setUltimaActualizacion(Timestamp.valueOf(estados.getAttribute("fecha")+" 00:00:00"));
                }
            }
            //Estado origen
            estados = (Element) elCuenta.getElementsByTagName("EstadoOrigen").item(0);
          
            if(estados != null)
                if (!estados.getAttribute("codigo").isEmpty()) {
                    cuenta.setEstadoOrigen(estados.getAttribute("codigo"));
                }
            
          
            cuenta.setTipoIdentificacion(tipoIdentificacion);
            cuenta.setIdentificacion(identificacion);
            cuenta.setCreationUser(usuario.getLogin());
            cuenta.setUserUpdate(usuario.getLogin());
            cuenta.setNitEmpresa(nitEmpresa);

            tService.getSt().addBatch(this.insertarCuentaCarteraDecisor(cuenta));

           /* NodeList listReclamos = elCuenta.getElementsByTagName("reclamo");
            if(listReclamos.getLength()>0){
                Element elReclamo = (Element) listReclamos.item(0);
                Reclamo reclamo = new Reclamo();
                reclamo.setTipoPadre("CCA");
                if(!elReclamo.getAttribute("tipoLeyenda").isEmpty()){
                    reclamo.setTipoLeyenda(elReclamo.getAttribute("tipoLeyenda"));
                }
                if(!elReclamo.getAttribute("fechaCierre").isEmpty()){
                    long fecha = Long.parseLong(elReclamo.getAttribute("fechaCierre"));
                    reclamo.setFechaCierre(new Timestamp(fecha));
                }
                if(!elReclamo.getAttribute("estado").isEmpty()){
                    reclamo.setEstado(elReclamo.getAttribute("estado"));
                }
                if(!elReclamo.getAttribute("tipo").isEmpty()){
                    reclamo.setTipo(elReclamo.getAttribute("tipo"));
                }
                if(!elReclamo.getAttribute("fecha").isEmpty()){
                    long fecha = Long.parseLong(elReclamo.getAttribute("fecha"));
                    reclamo.setFecha(new Timestamp(fecha));
                }
                if(!elReclamo.getAttribute("ratificado").isEmpty()){
                    reclamo.setRatificado(Boolean.parseBoolean(elReclamo.getAttribute("ratificado")));
                }
                reclamo.setTexto(elReclamo.getTextContent());
                reclamo.setTipoIdentificacion(tipoIdentificacion);
                reclamo.setIdentificacion(identificacion);
                reclamo.setCreationUser(usuario.getLogin());
                reclamo.setUserUpdate(usuario.getLogin());
                reclamo.setNitEmpresa(nitEmpresa);

                tService.getSt().addBatch(this.insertarReclamo(reclamo));
            }
            **/

            NodeList listValores = elCuenta.getElementsByTagName("Valores");
            int numValores = listValores.getLength();
            for (int j = 0; j < numValores; j++) {
                Element elValor = (Element) listValores.item(j);
                Valor valor = new Valor();
                valor.setTipoPadre("CCA");
                valores = (Element) elValor.getElementsByTagName("Valor").item(0);
                if (valores != null) {//si los valores son diferentes de null
                    if (!valores.getAttribute("valorInicial").isEmpty()) {
                    valor.setValorInicial(Double.parseDouble(valores.getAttribute("valorInicial")));
                }
                    if (!valores.getAttribute("saldoActual").isEmpty()) {
                    valor.setSaldoActual(Double.parseDouble(valores.getAttribute("saldoActual")));
                }
                    if (!valores.getAttribute("saldoMora").isEmpty()) {
                    valor.setSaldoMora(Double.parseDouble(valores.getAttribute("saldoMora")));
                }
                    if (!valores.getAttribute("cuota").isEmpty()) {
                    valor.setCuota(Double.parseDouble(valores.getAttribute("cuota")));
                }
                    if (!valores.getAttribute("cuotasCanceladas").isEmpty()) {
                    valor.setCuotasCanceladas(Integer.parseInt(valores.getAttribute("cuotasCanceladas")));
                }
                    if (!valores.getAttribute("totalCuotas").isEmpty()) {
                    valor.setTotalCuotas(Integer.parseInt(valores.getAttribute("totalCuotas")));
                }
                    if (!valores.getAttribute("diasMora").isEmpty()) {
                    valor.setMaximaMora(Integer.parseInt(valores.getAttribute("diasMora")));
                }
                valor.setTipoIdentificacion(tipoIdentificacion);
                valor.setIdentificacion(identificacion);
                valor.setCreationUser(usuario.getLogin());
                valor.setUserUpdate(usuario.getLogin());
                valor.setNitEmpresa(nitEmpresa);

                tService.getSt().addBatch(this.insertarValor(valor));
            }
            }

           /* NodeList listAlivio = elCuenta.getElementsByTagName("alivio");
            if(listAlivio.getLength()>0){
                Element elAlivio = (Element) listAlivio.item(0);
                Alivio alivio = new Alivio();
                alivio.setTipoPadre("CCA");
                if(!elAlivio.getAttribute("estado").isEmpty()){
                    alivio.setEstado(elAlivio.getAttribute("estado"));
                }
                if(!elAlivio.getAttribute("mes").isEmpty()){
                    alivio.setMes(elAlivio.getAttribute("mes"));
                }
                alivio.setTipoIdentificacion(tipoIdentificacion);
                alivio.setIdentificacion(identificacion);
                alivio.setCreationUser(usuario.getLogin());
                alivio.setUserUpdate(usuario.getLogin());

                tService.getSt().addBatch(this.insertarAlivio(alivio));
            }*/
        }
    }
    
    public void obtenerEndeudamientoGlobalDecisor(Element root) throws Exception{
        Element estado = null, garantia = null;
        NodeList listCuentas = root.getElementsByTagName("EndeudamientoGlobal");
        int numRegistros = listCuentas.getLength();
        for (int i = 0; i < numRegistros; i++) {
            Element elEndeudamiento = (Element) listCuentas.item(i);
            EndeudamientoGlobalDecisor endeudamiento = new EndeudamientoGlobalDecisor();
            //validamos que solo se inserten los endeudamientos correspondientes al sector financiero = 1

                if (!elEndeudamiento.getAttribute("calificacion").isEmpty()) {
                endeudamiento.setCalificacion(elEndeudamiento.getAttribute("calificacion"));
            }
                if (!elEndeudamiento.getAttribute("saldoPendiente").isEmpty()) {
                endeudamiento.setSaldoPendiente(Double.parseDouble(elEndeudamiento.getAttribute("saldoPendiente")));
            }
                if (!elEndeudamiento.getAttribute("tipoCredito").isEmpty()) {
                endeudamiento.setTipoCredito(elEndeudamiento.getAttribute("tipoCredito"));
            }
                if (!elEndeudamiento.getAttribute("moneda").isEmpty()) {
                endeudamiento.setMoneda(elEndeudamiento.getAttribute("moneda"));
            }
                if (!elEndeudamiento.getAttribute("numeroCreditos").isEmpty()) {
                endeudamiento.setNumeroCreditos(Integer.parseInt(elEndeudamiento.getAttribute("numeroCreditos")));
            }
                if (!elEndeudamiento.getAttribute("fechaReporte").isEmpty()) {
                //long fecha = Long.parseLong(elEndeudamiento.getAttribute("fechaReporte"));
                    endeudamiento.setFechaReporte(Timestamp.valueOf(elEndeudamiento.getAttribute("fechaReporte") + " 00:00:00"));
            }
            
            //Estado origen
            estado = (Element) elEndeudamiento.getElementsByTagName("Entidad").item(0);
            if(estado !=null)
                if (!estado.getAttribute("nombre").isEmpty()) {
                    endeudamiento.setEntidad(estado.getAttribute("nombre"));
                }
          
           //La garantia
            garantia = (Element) elEndeudamiento.getElementsByTagName("Garantia").item(0);
            if(garantia !=null)
                if (!garantia.getAttribute("tipo").isEmpty()) {
                    endeudamiento.setGarantia(garantia.getAttribute("tipo"));
                }

           
            endeudamiento.setTipoIdentificacion(tipoIdentificacion);
            endeudamiento.setIdentificacion(identificacion);
            endeudamiento.setCreationUser(usuario.getLogin());
            endeudamiento.setUserUpdate(usuario.getLogin());
            endeudamiento.setNitEmpresa(nitEmpresa);

            //validamos esta el sector financiero debe ser uno 
            if (!estado.getAttribute("sector").isEmpty() && estado.getAttribute("sector").equals("1")) {
                tService.getSt().addBatch(this.insertarEndeudamientoGlobalDecisor(endeudamiento));
            }

    }
    }
    
   public void obtenerConsultasDecisor(Element root) throws Exception{
       
        NodeList listConsultas = root.getElementsByTagName("Consulta");
        int numRegistros = listConsultas.getLength();
        for (int i = 0; i < numRegistros; i++) {
            Element elConsulta = (Element) listConsultas.item(i);
            ConsultaDecisor consulta = new ConsultaDecisor();
            if(!elConsulta.getAttribute("fecha").isEmpty()){
               // long fecha = Long.parseLong(elConsulta.getAttribute("fecha"));
                consulta.setFecha(Timestamp.valueOf(elConsulta.getAttribute("fecha")+" 00:00:00"));
            }
            if(!elConsulta.getAttribute("tipoCuenta").isEmpty()){
                consulta.setTipoCuenta(elConsulta.getAttribute("tipoCuenta"));
            }
            if(!elConsulta.getAttribute("entidad").isEmpty()){
                consulta.setEntidad(elConsulta.getAttribute("entidad"));
            }
            if(!elConsulta.getAttribute("oficina").isEmpty()){
                consulta.setOficina(elConsulta.getAttribute("oficina"));
            }
            if(!elConsulta.getAttribute("ciudad").isEmpty()){
                consulta.setCiudad(elConsulta.getAttribute("ciudad"));
            }
            if(!elConsulta.getAttribute("razon").isEmpty()){
                consulta.setRazon(elConsulta.getAttribute("razon"));
            }
            consulta.setTipoIdentificacion(tipoIdentificacion);
            consulta.setIdentificacion(identificacion);
            consulta.setCreationUser(usuario.getLogin());
            consulta.setUserUpdate(usuario.getLogin());
            consulta.setNitEmpresa(nitEmpresa);

            tService.getSt().addBatch(this.insertarConsultaDecisor(consulta));

        }
    } 
   
     public void obtenerAlertasDecisor(Element root) throws Exception{
        NodeList listAlertas = root.getElementsByTagName("Alerta");
        int numRegistros = listAlertas.getLength();
        for (int i = 0; i < numRegistros; i++) {
            Element elAlerta = (Element) listAlertas.item(i);
            AlertaDecisor alerta = new AlertaDecisor();
            if(!elAlerta.getAttribute("colocacion").isEmpty()){
                //long fecha = Long.parseLong(elAlerta.getAttribute("colocacion"));
                alerta.setColocacion(Timestamp.valueOf(elAlerta.getAttribute("colocacion")+" 00:00:00"));
            }
            if(!elAlerta.getAttribute("vencimiento").isEmpty()){
               // long fecha = Long.parseLong(elAlerta.getAttribute("vencimiento"));
                alerta.setVencimiento(Timestamp.valueOf(elAlerta.getAttribute("vencimiento")+" 00:00:00"));
            }
            if(!elAlerta.getAttribute("modificacion").isEmpty()){
               // long fecha = Long.parseLong(elAlerta.getAttribute("modificacion"));
                alerta.setModificacion(Timestamp.valueOf(elAlerta.getAttribute("modificacion")+" 00:00:00"));
            }
            if(!elAlerta.getAttribute("codigo").isEmpty()){
                alerta.setCodigo(elAlerta.getAttribute("codigo"));
            }
            if(!elAlerta.getAttribute("texto").isEmpty()){
                alerta.setTexto(elAlerta.getAttribute("texto"));
            }
            
            NodeList listFuente = elAlerta.getElementsByTagName("Fuente");
            if(listFuente.getLength()>0){
                Element elFuente = (Element) listFuente.item(0);
                if(!elFuente.getAttribute("codigo").isEmpty()){
                    alerta.setCodigoFuente(elFuente.getAttribute("codigo"));
                }
            }
            
            alerta.setTipoIdentificacion(tipoIdentificacion);
            alerta.setIdentificacion(identificacion);
            alerta.setCreationUser(usuario.getLogin());
            alerta.setUserUpdate(usuario.getLogin());
            alerta.setNitEmpresa(nitEmpresa);

            tService.getSt().addBatch(this.insertarAlertaDecisor(alerta));

        }
    }
     
    public void obtenerScoreDecisor(Element root) throws Exception{
        NodeList listScores = root.getElementsByTagName("Score");
        int numRegistros = listScores.getLength();
        for (int i = 0; i < numRegistros; i++) {
            Element elScore = (Element) listScores.item(i);
            ScoreDecisor score = new ScoreDecisor();

            if(!elScore.getAttribute("tipo").isEmpty()){
                score.setTipo(Integer.parseInt(elScore.getAttribute("tipo")));
            }
            if(!elScore.getAttribute("puntaje").isEmpty()){
                score.setPuntaje((int)Float.parseFloat(elScore.getAttribute("puntaje")));
            }
            
            if(!elScore.getAttribute("clasificacion").isEmpty()){
                score.setClasificacion(elScore.getAttribute("clasificacion"));
            }else{
                score.setClasificacion("");
            }
            
            score.setTipoIdentificacion(tipoIdentificacion);
            score.setIdentificacion(identificacion);
            score.setCreationUser(usuario.getLogin());
            score.setUserUpdate(usuario.getLogin());
            score.setNitEmpresa(nitEmpresa);

            //tService.getSt().addBatch(this.insertarScoreDecisor(score));
            
            //mismo metodo que en superfil.
            NodeList listRazones = elScore.getElementsByTagName("Razon");
            String sql = "";
            int numRazones = listRazones.getLength();
            Razon razon = null;
            for (int j = 0; j < numRazones; j++) {
                Element elRazon = (Element) listRazones.item(j);
                razon = new Razon();
                if(!elRazon.getAttribute("codigo").isEmpty()){
                    razon.setCodigo(elRazon.getAttribute("codigo"));
                    score.setPuntaje((int)Float.parseFloat(elRazon.getAttribute("codigo")));
                }
                razon.setCreationUser(usuario.getLogin());
                razon.setUserUpdate(usuario.getLogin());

                sql += this.insertarRazon(razon)+";";

               // tService.getSt().addBatch(this.insertarRazon(razon));
            }

            
            //para guardar el puntaje...
             tService.getSt().addBatch(this.insertarScoreDecisor(score));
           //razon
            if (!sql.equals("")) {
                String[] listSql = sql.split(";");
                for (int k = 0; k < listSql.length; k++) {
                    tService.getSt().addBatch(listSql[k]);
                }
            }

        }
    }

    public void obtenerRespuestaPersonalizadaDecisor(Element root) throws Exception {
        NodeList listRespPersonalizada = root.getElementsByTagName("RespuestaPersonalizada");
        int numResp = listRespPersonalizada.getLength();
        HashMap consecutivo = new HashMap();
        for (int i = 0; i < numResp; i++) {
            Element elResp = (Element) listRespPersonalizada.item(i);
            RespuestaPersonalizadaDecisor lineaResp = null;
            NodeList listLineas = elResp.getElementsByTagName("Linea");
            int numLineas = listLineas.getLength();

            for (int j = 0; j < numLineas; j++) {
                Element elLinea = (Element) listLineas.item(j);
                lineaResp = new RespuestaPersonalizadaDecisor();
                String vector[] = this.getRespuestaConsecutivo(elLinea.getTextContent());


                // contenedor.add("","");
                consecutivo.put(vector[0], vector[1]);
                consecutivo.put(vector[2], vector[3]);
                lineaResp.setConsecutivo(consecutivo);
               // String desCausal = vector[0];
                
                if (vector[0].equals("Var_Descripcion_Causal")) {
                    lineaResp.setLinea(vector[1]);
                 } else if(vector[2].equals("Var_Descripcion_Causal")) {
                    lineaResp.setLinea(vector[3]);
                }

               /* if (vector[2].equals("Var_Descripcion_Causal")) {
                    lineaResp.setLinea(vector[3]);
                } else {
                    break;
                }*/


                // lineaResp.setLinea(elLinea.getTextContent());
                lineaResp.setCreationUser(usuario.getLogin());
                lineaResp.setUserUpdate(usuario.getLogin());
                lineaResp.setTipoIdentificacion(tipoIdentificacion);
                lineaResp.setIdentificacion(identificacion);
                lineaResp.setNitEmpresa(nitEmpresa);
                
                if(vector[0].equals("Var_Descripcion_Causal")||vector[2].equals("Var_Descripcion_Causal")){
                  tService.getSt().addBatch(this.insertarRespuestaPersonalizadaDecisor(lineaResp));
                }
               
            }
            tService.getSt().addBatch(dao.guardarRepuestaPerDecisor(consecutivo,lineaResp));
        }
    }
     
   public String[] getRespuestaConsecutivo(String text){
       
        String salida= text.replace("][","#").replace("[", "").replace("]", "");
        String vectorSalida[]=salida.split("#");
        
        for (int i = 0; i < vectorSalida.length; i++) {
            String object = vectorSalida[i];
            System.out.println(object);
        }
        
       return vectorSalida;
   }

   /**
     *
     * @param txt
     * @return
     */
    public static String encriptar(String txt) {

        char array[] = txt.toCharArray();
        for (int i = 0; i < array.length; i++) {
            array[i] = (char) (array[i] + (char) 5);
        }
        String encriptado = String.valueOf(array);

        return encriptado;
    }
    
    private String consumoHCPuente(String formulario, String empresa) {
        WSInterfaz_Service service = new WSInterfaz_Service();
        WSInterfaz port = service.getWSInterfazPort();

        //enviamos la cabecera de la peticion.
        String user = encriptar("fintra");
        String pass = encriptar("superpassword");
        Map<String, Object> requestContext = ((BindingProvider) port).getRequestContext();
        String url;
        url = (String) service.getWSDLDocumentLocation().toString();
        requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);
        Map<String, List<String>> requestHeaders = new HashMap<String, List<String>>();
        requestHeaders.put("username", Collections.singletonList(user));
        requestHeaders.put("Password", Collections.singletonList(pass));
        requestContext.put(MessageContext.HTTP_REQUEST_HEADERS, requestHeaders);

        return port.consumoHCPuente(formulario, empresa);
    }
    
   
}
