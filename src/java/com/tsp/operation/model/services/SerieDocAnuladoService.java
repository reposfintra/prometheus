/*
 * SerieDocAnuladoService.java
 *
 * Created on 19 de marzo de 2007, 11:09 AM
 */
/*
* Nombre        SerieDocAnuladoService.java
* Descripci�n   Service para el acceso a los datos del archivo de serie de documentos anulados
* Autor         Ing. Andr�s Maturana D.
* Fecha         19 de marzo de 2007
* Versi�n       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.sql.*;
import java.io.*;

/**
 *
 * @author  Andres
 */
public class SerieDocAnuladoService {
    
    SerieDocAnuladoDAO dao;
    
    /** Creates a new instance of SerieDocAnuladoService */
    public SerieDocAnuladoService() {
        dao = new SerieDocAnuladoDAO();
    }
    public SerieDocAnuladoService(String dataBaseName) {
        dao = new SerieDocAnuladoDAO(dataBaseName);
    }
    
    /**
     * Metodo de insercion
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param doc Documento anulado a insertar
     * @throws SQLException
     * @version 1.0
     */
    public void insertar() throws SQLException{
        this.dao.insertar();
    }
    
    /**
     * Getter for property doc.
     * @return Value of property doc.
     */
    public com.tsp.operation.model.beans.SerieDocAnulado getDoc() {
        return dao.getDoc();
    }
    
    /**
     * Setter for property doc.
     * @param doc New value of property doc.
     */
    public void setDoc(com.tsp.operation.model.beans.SerieDocAnulado doc) {
        dao.setDoc(doc);
    }
    
    /**
     * Verifica la existencia de un registro
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param doc Documento anulado a insertar
     * @throws SQLException
     * @version 1.0
     */
    public String existe() throws SQLException{
        return dao.existe();
    }
    
    /**
     * Actualiza un registro
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param doc Documento anulado a insertar
     * @throws SQLException
     * @version 1.0
     */
    public void actualizar() throws SQLException{
        dao.actualizar();
    }
    
    /**
     * Busqueda con los filtros dados
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param fechai Fecha inicial
     * @param fechaf Fecha final
     * @param login Login del Usuario de Creaci�n del registro
     * @throws SQLException
     * @version 1.0
     */
    public void search(String fechai, String fechaf, String login) throws SQLException{
        dao.search(fechai, fechaf, login);
    }
    
    /**
     * Getter for property documentos.
     * @return Value of property documentos.
     */
    public java.util.Vector getDocumentos() {
        return dao.getDocumentos();
    }
    
    /**
     * Setter for property documentos.
     * @param documentos New value of property documentos.
     */
    public void setDocumentos(java.util.Vector documentos) {
        dao.setDocumentos(documentos);
    }
    
    /**
     * Obtener un registro
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public void obtener() throws SQLException{
        dao.obtener();
    }
    
}
