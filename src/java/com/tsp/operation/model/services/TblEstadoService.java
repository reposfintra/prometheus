/*
 * TblEstadoService.java
 *
 * Created on 5 de octubre de 2005, 10:35 AM
 */

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  Andres
 */
public class TblEstadoService {
        private TblEstadoDAO estado;
        private TblEstado cod_estado;
        
        /** Creates a new instance of TblEstadoService */
        public TblEstadoService() {
                this.estado = new TblEstadoDAO();
                this.cod_estado = null;
        }
        
               
        public void insertEstado(String filename, ByteArrayInputStream bfin, int longitud,  Dictionary fields, 
                String contentType, String user, String agencia, String base) throws Exception{
                        
                try{
                        this.estado.insertEstado(filename, bfin,longitud,fields,contentType, user, agencia, base);       
                }catch(Exception e){  
                        throw new Exception( e.getMessage()); 
                }
        }
        
        public void updateEstado(String filename, ByteArrayInputStream bfin, int longitud,  Dictionary fields, 
                String contentType, String user) throws Exception{
                        
                try{
                        this.estado.updateEstado(filename, bfin,longitud,fields,contentType, user);       
                }catch(Exception e){  
                        throw new Exception( e.getMessage()); 
                }
        }
        
        public void updateEstado(TblEstado estado, String tipo, String codestado)
                throws SQLException{
                this.estado.setEstado(estado);
                this.estado.updateEstado(tipo, codestado);
        }
        
        public void anularEstado(String tipo, String codestado, String user)
                throws SQLException{
                
                this.estado.anularEstado(tipo, codestado, user);
        }
        
        public TblEstado obtenerEstado(String tipo, String codestado, String ruta) throws SQLException{
                this.estado.obtenerEstado(tipo, codestado, ruta);
                return this.estado.getEstado();
        }
        
        public Vector obtenerEstados() throws SQLException{
                this.estado.obtenerEstados();
                return this.estado.getEstados();
        }
        
        public boolean existeEstado(String tipo, String codestado) throws SQLException{
                return this.estado.existeEstado(tipo, codestado);
        }

        /**
         * Getter for property cod_estado.
         * @return Value of property cod_estado.
         */
        public com.tsp.operation.model.beans.TblEstado getCod_estado() {
                return cod_estado;
        }
        
        /**
         * Setter for property cod_estado.
         * @param cod_estado New value of property cod_estado.
         */
        public void setCod_estado(com.tsp.operation.model.beans.TblEstado cod_estado) {
                this.cod_estado = cod_estado;
        }
        
}
