/********************************************************************
 *      Nombre Clase.................   AdminHistoricoService.java
 *      Descripci�n..................   service de la tabla AdminHistoricos
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.util.Util;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  dlamadrid
 */
public class AdminHistoricoService
{
        
        AdminHistoricoDAO adminD;
        /** Creates a new instance of AdminHistoricoService */
        public AdminHistoricoService ()
        {
                adminD = new AdminHistoricoDAO ();
        }
        
        /**
         * Metodo que setea el vector de objetos tipo Adminhistorico
         * @autor.......David Lamadrid
         * @param.......Vector historico
         * @see.........AdminHistoricoDAO.java setHistorico (Vector historico)
         * @version.....1.0.
         * @return.......
         */
        public void setHistorico (Vector historico)
        {
                adminD.setHistorico (historico);
        }
        
        /**
         * Metodo que obtiene el vector de objetos tipo Adminhistorico
         * @autor.......David Lamadrid
         * @param.......
         * @see.........AdminHistoricoDAO.java getHistorico ()
         * @version.....1.0.
         * @return.......Vector historico
         */
        public Vector getHistorico ()
        {
                return adminD.getHistorico ();
        }
        
        /**
         * Metodo que obtiene un objeto tipo Adminhistorico
         * @autor.......David Lamadrid
         * @param.......
         * @see.........AdminHistoricoDAO.java ,getAdmin ()
         * @version.....1.0.
         * @return.......AdminHistorico admin
         */
        public AdminHistorico getAdmin ()
        {
                return adminD.getAdmin ();
        }
        
        /**
         * Metodo que setea un objeto tipo Adminhistorico
         * @autor.......David Lamadrid
         * @param.......AdminHistorico admin
         * @see.........AdminHistoricoDAO.java,setAdmin (AdminHistorico admin)
         * @version.....1.0.
         * @return.......
         */
        public void setAdmin (AdminHistorico admin)
        {
                adminD.setAdmin (admin);
        }
        
        
        /**
         * Metodo que inserta un registro en la tabla admin_historicos
         * @autor.......David Lamadrid
         * @param.......
         * @see.........AdminHistoricoDAO.java,insertar ()
         * @throws......SQLException
         * @version.....1.0.
         * @return.......
         */
        public void insertar () throws SQLException
        {
                adminD.insertar ();
                ////System.out.println ("pasa por insertar dao");
        }
        
        /**
         * Metodo que sete el Vector historico con una lista de registros de  admin_historicos
         * @autor.......David Lamadrid
         * @param.......
         * @see.........AdminHistoricoDAO.java,listarHistorico ()
         * @throws......
         * @version.....1.0.
         * @return.......
         */
        public void listarHistorico ()throws SQLException
        {
                adminD.listarHistorico ();
        }
        
        /**
         * Metodo que actualiza un registro de admin_historicos
         * @autor.......David Lamadrid
         * @param.......String nombreTabla
         * @see.........AdminHistoricoDAO.java,actualizarHistorico (String nombreTabla)
         * @throws......SQLException registro ya existe en el sitema
         * @version.....1.0.
         * @return.......
         */
        public void actualizarHistorico (String nombreTabla) throws SQLException
        {
                adminD.actualizarHistorico (nombreTabla);
        }
        
        /**
         * Metodo que elimina un registro de admin_historico dado el nombre de la tabla
         * @autor.......David Lamadrid
         * @param.......String nombreTabla,eliminarHistorico (String oid)
         * @see.........AdminHistoricoDAO.java,eliminarHistorico (String oid)
         * @throws......SQLException registro no existe en el sitema
         * @version.....1.0.
         * @return.......
         */
        public void eliminarHistorico (String oid) throws SQLException
        {
                adminD.eliminarHistorico (oid);
        }
        
        /**
         * Metodo que verifica si existe un registro con un nombre de tabla
         * @autor.......David Lamadrid
         * @param.......
         * @see.........AdminHistoricoDAO.java,existeHistorico (String nombreTabla)
         * @throws......SQLException
         * @version.....1.0.
         * @return.......boolean existe(true si existe ,false si no existe)
         */
        public boolean  existeHistorico (String nombreTabla)throws SQLException
        {
                return adminD.existeHistorico (nombreTabla);
        }
        
        /**
         * Metodo que retorna un String con una fecha final dada una fecha inicial y un nuemro de dias
         * @autor.......David Lamadrid
         * @param.......String fechai,int n(numero de dias)
         * @see.........AdminHistoricoDAO.java,fechaFinal (String fechai,int n)
         * @throws......registro no existe en el sitema
         * @version.....1.0.
         * @return.......String fechaf(fecha Final)
         */
        public  String fechaFinal (String fechai,int n)
        {
                return adminD.fechaFinal (fechai,n);
        }
        
        
        /**
         * Metodo que sete el Vector historico con una lista de registros de  admin_historicos dado el codigo del registro
         * @autor.......David Lamadrid
         * @param.......String id(Codigo del registro)
         * @see.........AdminHistoricoDAO.java,listarHistoricoPorId (String id)
         * @throws...... SQLException
         * @version.....1.0.
         * @return.......
         */
        public void listarHistoricoPorId (String id)throws SQLException
        {
                adminD.listarHistoricoPorId (id);
        }
        
        
        /**
         * Metodo que retorna true si existe una tabla en el sitema dado el nombre de la tabla
         * @autor.......David Lamadrid
         * @param.......String nombreTabla
         * @see.........AdminHistoricoDAO.java,existeTabla (String nombre )
         * @throws......registro no existe en el sitema SQLException
         * @version.....1.0.
         * @return.......boolen x(true si existe ,false si no existe)
         */
        public  boolean existeTabla (String nombre ) throws SQLException
        {
                return adminD.existeTabla (nombre);
        }
        
}
