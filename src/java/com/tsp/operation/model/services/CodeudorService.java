/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * CodeudorService.java : clase intermediaria entre el action y el acceso a la base de datos
 * Copyright 2010 Rhonalf Martinez Villa <rhonaldomaster@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import java.util.*;
import com.tsp.operation.model.DAOS.*;
/**
 *
 * @author rhonalf
 */
public class CodeudorService {

    CodeudorDAO cdao;

    public CodeudorService(){
        this.cdao = new CodeudorDAO();
    }

    /**
     * Busca un dato basado en un filtro
     * @param filtro el filtro a aplicar
     * @param cadena el dato a buscar
     * @return ArrayList con objetos Codeudor con los datos encontrados
     * @throws Exception cuando hay error
     */
    public ArrayList listaResults(String filtro,String cadena) throws Exception{
        ArrayList lista = null;
        try {
            lista = cdao.listaResults(filtro, cadena);
        }
        catch (Exception e) {
            throw new Exception("Error al listar resultados: "+e.toString());
        }
        return lista;
    }

    /**
     * Busca los datos de un codeudor
     * @param filtro nombre de la columna por la cual se hace el fitro
     * @param cadena el dato a buscar en la columna especificada
     * @return objeto Codeudor con los datos
     * @throws Exception cuando hay un error
     */
    public Codeudor buscarDatosCodeudor(String filtro,String cadena) throws Exception{
        Codeudor codeudor = null;
        try {
            codeudor = cdao.buscarDatosCodeudor(filtro, cadena);
        }
        catch (Exception e) {
            throw new Exception("Error al buscar datos del codeudor: "+e.toString());
        }
        return codeudor;
    }

    /**
     * Actualiza los datos de un codeudor
     * @param codeudor Objeto Codeudor con los datos
     * @throws Exception Cuando hay un error
     */
    public void actualizarCodeudor(Codeudor codeudor) throws Exception{
        try {
            cdao.actualizarCodeudor(codeudor);
        }
        catch (Exception e) {
            throw new Exception("Error al actualizar los datos del codeudor: "+e.toString());
        }
    }

}
