/***************************************
* Nombre Clase ............. CuotasService.java
* Descripci�n  .. . . . . .  Ofrece los servicios para las cuotas
* Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
* Fecha . . . . . . . . . .  09/02/2006
* versi�n . . . . . . . . .  1.0
* Copyright ...Transportes Sanchez Polo S.A.
*******************************************/


package com.tsp.operation.model.services;





import java.io.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.CuotasDAO;
import com.tsp.operation.model.DAOS.CXPDocDAO;



public class CuotasService {
    
    
    private  CuotasDAO  CuotasDataAccess;
    private  List       listCuotas;
    private  List       listMigracion;
    
    private  List       listPrestamos;
    
    private  String     eventoBtn;
    
    
    private  CXPDocDAO  CXPDocDAOAccess;
    
    
    public CuotasService() {
        CuotasDataAccess = new CuotasDAO();
        listCuotas       = new LinkedList(); 
        listMigracion    = new LinkedList(); 
        listPrestamos    = new LinkedList();
        eventoBtn        = "";
    }
    
        
    
    
    /**
     * M�todo que carga las lista de cuotas de un tercero en un rango de fechas que no se hallan migrados
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/
    public void searchCuotas(String distrito, String tercero, String fechaCorte,  int limit)throws Exception{
        listCuotas    = new LinkedList();
        listMigracion = new LinkedList();
        try{
            listCuotas  = this.CuotasDataAccess.getCuotas(distrito, tercero, fechaCorte, limit);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
        
    
    
    
    
    /**
     * M�todo que carga las lista de prestamos de un tercero con proveedor especifico para migrar
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/
    public void searchPrestamosProveedor(String distrito, String tercero, String[] proveedores)throws Exception{
        listPrestamos   = new LinkedList();
        listCuotas      = new LinkedList();
        listMigracion   = new LinkedList();
        try{
            String listProveedores = this.getString(proveedores);
            listPrestamos  = this.CuotasDataAccess.getPrestamosProveedores( distrito, tercero, listProveedores );
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    
    
    
    /**
     * M�todo que devuelve un String de un Array
     * @autor....... fvillacob
     * @version..... 1.0.
     **/
    public String getString(String[] array){
        String lista = "";
        if(array!=null){
            for(int i=0; i<array.length;i++){
                String code = array[i];
                if(!lista.equals("")) lista+=",";
                    lista+= "'" + code + "'";
            }
        }
        return lista;
    }
    
    
    
    
    /**
     * M�todo que devuelve el prestamo correspondiente al id
     * @autor....... fvillacob
     * @version..... 1.0.
     **/
    public  ResumenPrestamo  getPrestamoPropietario(int id){
        ResumenPrestamo  ptm = new ResumenPrestamo();
        for(int i=0;i<this.listPrestamos.size();i++){
            ResumenPrestamo  prestamo = (ResumenPrestamo) this.listPrestamos.get(i);
            if(prestamo.getId()==id){
                ptm = prestamo;
                break;
            }
        }
        return ptm;
    }
    
    
    
    
    
    
    /**
     * M�todo que setea la lista de cuotas
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/
    public void resetListMigracion(){
        listMigracion       = new LinkedList();
    }
    
    
    
    
     /**
     * M�todo que activa las cuotas
     * @autor....... fvillacob
     * @parameter... int id
     * @version..... 1.0.
     **/
    public void active(int id){
        if(this.listCuotas!=null){
            for(int i=0; i< listCuotas.size(); i++){
                Amortizacion  amort = (Amortizacion) listCuotas.get(i);
                if(amort.getIdObjeto()==id){
                    amort.setSeleccionada( true );
                    break;
                }
            }
        }
    }

    
    
    
    
     /**
     * M�todo que llena la lista para migrar y retira los objeto del listado de cuotas general
     * @autor....... fvillacob
     * @parameter... int id
     * @version..... 1.0.
     **/
    public void formarMigracion() throws Exception{
        try{  
            resetListMigracion();
            List  aux = new LinkedList();
             if(this.listCuotas!=null){
                for(int i=0; i< listCuotas.size(); i++){
                    Amortizacion  amort = (Amortizacion) listCuotas.get(i);
                    if(amort.isSeleccionada())
                        this.listMigracion.add(amort);
                    else
                        aux.add(amort);
                }
             }
             listCuotas = aux;
            
        }catch(Exception e){
            throw new Exception( "formarMigracion() " + e.getMessage());
        }
    }
    
    
    
    
    
     /**
     * M�todo que llena la lista para migrar y retira los objeto del listado de cuotas general
     * @autor....... fvillacob
     * @parameter... int id
     * @version..... 1.0.
     **/
    public void addMigracion() throws Exception{
        try{  
            
            if(this.listMigracion==null)
               this.listMigracion = new LinkedList();
           
            if(this.listCuotas!=null){
                 for(int i=0; i< listCuotas.size(); i++){
                      Amortizacion  amort = (Amortizacion) listCuotas.get(i);
                      if(amort.isSeleccionada()){
                          int sw=0;
                          
                          // No metemos las ya seleccionadas:
                          for(int j=0; j< listMigracion.size(); j++){
                              Amortizacion  amortSelect = (Amortizacion) listMigracion.get(j);
                              if(    amortSelect.getPrestamo().equals(  amort.getPrestamo() )
                                  && amortSelect.getItem().    equals(  amort.getItem()     )
                                 ){
                                  sw=1;
                                  break;
                              }
                              
                          }
                          
                          if(sw==0)
                              this.listMigracion.add(amort);
                     }
                 }
            }
            
            
        }catch(Exception e){
            throw new Exception( "formarMigracion() " + e.getMessage());
        }
    }
    
    
    
    
    
    
    
    
    
    
     /**
     * M�todo que devuelve el objeto correspondiente al id
     * @autor....... fvillacob
     * @parameter... int id
     * @version..... 1.0.
     **/
    public Amortizacion getAmortizacion(int id){
         Amortizacion obj = new Amortizacion();
         if(this.listCuotas!=null){
              for(int i=0; i< listCuotas.size(); i++){
                    Amortizacion  amort = (Amortizacion) listCuotas.get(i);
                    if(amort.getIdObjeto()==id){
                        obj = amort;
                        break;
                    }
              }
         }
         return obj;
    }
    
    
    
    
    
     /**
     * M�todo que actualiza la fecha de pago de una cuota
     * @autor....... fvillacob
     * @parameter... Amortizacion amort, String user
     * @version..... 1.0.
     **/
    public void updateFechaPago(Amortizacion amort, String user) throws Exception{
        try{
             this.CuotasDataAccess.updateFechaPago(amort, user);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    
    
 
    
    /**
     * M�todo que busca datos de un proveedor
     * @autor....... fvillacob
     * @parameter... String nit
     * @version..... 1.0.
     **/
    public Hashtable getProveedor(String nit) throws Exception{
        Hashtable  ht = new Hashtable();
        try{
             ht = this.CuotasDataAccess.getDatosProveedor(nit);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return ht;
    }
    
    
    
    /**
     * M�todo que busca la agencia de un banco
     * @autor....... jbarros
     * @parameter... String nit
     * @version..... 1.0.
     **/
     public String getAgenciaBanco(String banco,String sucursal) throws Exception { 
        String  agencia  ="";
        try{
            //agencia = this.CuotasDataAccess.getAgencia(banco,sucursal);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return agencia;
    }
    
    
    
    
    
    
    
    /**
     * Getter for property listCuotas.
     * @return Value of property listCuotas.
     */
    public java.util.List getListCuotas() {
        return listCuotas;
    }    
    
    /**
     * Setter for property listCuotas.
     * @param listCuotas New value of property listCuotas.
     */
    public void setListCuotas(java.util.List listCuotas) {
        this.listCuotas = listCuotas;
    }    
    
    
    
    
    
    /**
     * Getter for property listMigracion.
     * @return Value of property listMigracion.
     */
    public java.util.List getListMigracion() {
        return listMigracion;
    }
    
    /**
     * Setter for property listMigracion.
     * @param listMigracion New value of property listMigracion.
     */
    public void setListMigracion(java.util.List listMigracion) {
        this.listMigracion = listMigracion;
    }
    
    
    
    
    
    
    
    /**
     * Getter for property listPrestamos.
     * @return Value of property listPrestamos.
     */
    public java.util.List getListPrestamos() {
        return listPrestamos;
    }
    
    /**
     * Setter for property listPrestamos.
     * @param listPrestamos New value of property listPrestamos.
     */
    public void setListPrestamos(java.util.List listPrestamos) {
        this.listPrestamos = listPrestamos;
    }
    
    
    
    
    
    
    /**
     * Getter for property eventoBtn.
     * @return Value of property eventoBtn.
     */
    public java.lang.String getEventoBtn() {
        return eventoBtn;
    }
    
    /**
     * Setter for property eventoBtn.
     * @param eventoBtn New value of property eventoBtn.
     */
    public void setEventoBtn(java.lang.String eventoBtn) {
        this.eventoBtn = eventoBtn;
    }
    
    /**
     * M�todo que actualiza la fecha y usuario de migracion de una cuota
     * @autor....... fvillacob
     * @parameter... Amortizacion amort, String user
     * @version..... 1.0.
     **/
    public String updateMigracion(Amortizacion amort, String user) throws Exception{
        try{
             return this.CuotasDataAccess.updateMigracion(amort, user);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
}
