package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.WSLocalizacionDAO;
import com.fintra.ws.datacredito.*;
import com.tsp.operation.model.DAOS.TablaGenManagerDAO;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.Direccion;
import com.tsp.operation.model.beans.EmailLocalizacion;
import com.tsp.operation.model.beans.Entidad;
import com.tsp.operation.model.beans.Persona;
import com.tsp.operation.model.beans.TablaGen;
import com.tsp.operation.model.beans.Telefono;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Util;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ResourceBundle;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.*;
import org.xml.sax.InputSource;
import java.sql.SQLException;

/**
 * Service para las tablas del web service de localizacion RECONOCER+ de datacredito<br/>
 * 1/09/2011<br/>
 * @author darrieta - GEOTECH SOLUTIONS S.A.
 */
public class WSLocalizacionService {

    WSLocalizacionDAO dao;
    Usuario usuario;
    String tipoIdentificacion = "";
    String identificacion = "";
    String mensaje = "";
    TransaccionService tService;

    public WSLocalizacionService() {
        dao = new WSLocalizacionDAO();
        tService = new TransaccionService();
    }
    public WSLocalizacionService(String dataBaseName) {
        dao = new WSLocalizacionDAO(dataBaseName);
        tService = new TransaccionService(dataBaseName);
    }

    /**
     * Elimina toda la informacion asociada a una persona
     * @param identificacion
     * @param tipoIdentificacion
     * @return sql generado
     * @throws Exception
     */
    public String eliminarInfoPersona(String identificacion, String tipoIdentificacion) throws Exception{
        String sql = dao.eliminarDirecciones(identificacion, tipoIdentificacion)+"; ";
        sql += dao.eliminarTelefonos(identificacion, tipoIdentificacion)+"; ";
        sql += dao.eliminarEmails(identificacion, tipoIdentificacion)+"; ";

        return sql;
    }

    /**
     * Guarda la informacion de la entidad
     * @param entidad
     * @return sql generado
     * @throws Exception
     */
    public String guardarEntidad(Entidad entidad) throws Exception{
        String sql = "";
        if(dao.existeEntidad(entidad)){
            sql = dao.editarEntidad(entidad);
        }else{
            sql = dao.insertarEntidad(entidad);
        }
        return sql;
    }

    /**
     * Inserta un direccion en la tabla
     * @param direccion informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarDireccion(Direccion direccion) throws Exception{
        return dao.insertarDireccion(direccion);
    }

    /**
     * Inserta un telefono en la tabla
     * @param telefono informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarTelefono(Telefono telefono) throws Exception{
        return dao.insertarTelefono(telefono);
    }

    /**
     * Inserta un email en la tabla
     * @param email informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarEmail(EmailLocalizacion email) throws Exception{
        return dao.insertarEmail(email);
    }
    
    
    public void parseXML(String xml) {
        try {
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new InputSource(new ByteArrayInputStream(xml.getBytes("utf-8"))));

            Element root = doc.getDocumentElement();
            root.normalize();
            if (root.getFirstChild() == null) {
                mensaje = "La respuesta no fue positiva. Numero de respuesta:"+root.getAttribute("respuesta");
            } else {
                tService.crearStatement();
                obtenerPersona(root);
                tService.getSt().addBatch(this.eliminarInfoPersona(identificacion, tipoIdentificacion));
                obtenerDirecciones(root);
                obtenerTelefonos(root,"Telefono");
                obtenerTelefonos(root,"Celular");
                obtenerEmails(root);
                tService.execute();
                mensaje="Se han guardado exitosamente los datos recibidos";
            }

        } catch (Throwable t) {
            mensaje="ha ocurrido un error guardando los datos";
            t.printStackTrace();
        }
    }


    public void obtenerPersona(Element root) throws Exception{
        Persona persona = new Persona();
        NodeList listPersonas = null;
        Element id=null;
        Element edad=null;
        if(root.getElementsByTagName("NaturalNacional").getLength()>0){//Natural Nacional
            listPersonas = root.getElementsByTagName("NaturalNacional");
            persona.setTipoIdentificacion("1");
            Element elPersona = (Element) listPersonas.item(0);
            if(!elPersona.getAttribute("nombres").isEmpty()){
                persona.setNombre(elPersona.getAttribute("nombres"));
            }
            if(!elPersona.getAttribute("primerApellido").isEmpty()){
                persona.setPrimerApellido(elPersona.getAttribute("primerApellido"));
            }
            if(!elPersona.getAttribute("segundoApellido").isEmpty()){
                persona.setSegundoApellido(elPersona.getAttribute("segundoApellido"));
            }
            if(!elPersona.getAttribute("nombreCompleto").isEmpty()){
                persona.setNombreCompleto(elPersona.getAttribute("nombreCompleto"));
            }
            if(!elPersona.getAttribute("genero").isEmpty()){
                persona.setGenero(elPersona.getAttribute("genero"));
            }
            if(!elPersona.getAttribute("validada").isEmpty()){
                persona.setValidada(Boolean.parseBoolean(elPersona.getAttribute("validada")));
            }

            id = (Element) elPersona.getElementsByTagName("Identificacion").item(0);
            //Se obtiene la informacion de la identificacion
            if(!id.getAttribute("estado").isEmpty()){
                persona.setEstadoId(id.getAttribute("estado"));
            }
            if(!id.getAttribute("fechaExpedicion").isEmpty()){
                long fechaExpedicion = Long.parseLong(id.getAttribute("fechaExpedicion"));
                persona.setFechaExpedicionId(new Timestamp(fechaExpedicion));
            }
            if(!id.getAttribute("ciudad").isEmpty()){
                persona.setCiudadId(id.getAttribute("ciudad"));
            }
            if(!id.getAttribute("departamento").isEmpty()){
                persona.setDepartamentoId(id.getAttribute("departamento"));
            }
            if(!id.getAttribute("estado").isEmpty()){
                persona.setEstadoId(id.getAttribute("estado"));
            }
            if(!id.getAttribute("numero").isEmpty()){
                persona.setIdentificacion(identificacion);
            }


            //Se obtiene la informacion de la edad
            if(elPersona.getElementsByTagName("Edad").getLength()>0){
                edad = (Element) elPersona.getElementsByTagName("Edad").item(0);
                if(!edad.getAttribute("min").isEmpty()){
                    persona.setEdadMin(edad.getAttribute("min"));
                }
                if(!edad.getAttribute("max").isEmpty()){
                    persona.setEdadMax(edad.getAttribute("max"));
                }
            }

        }else if(root.getElementsByTagName("NaturalExtranjera").getLength()>0){//Natural extranjera
            listPersonas = root.getElementsByTagName("NaturalExtranjera");
            persona.setTipoIdentificacion("4");
            Element elPersona = (Element) listPersonas.item(0);
            if(!elPersona.getAttribute("nombre").isEmpty()){
                persona.setNombre(elPersona.getAttribute("nombre"));
            }
            if(!elPersona.getAttribute("nacionalidad").isEmpty()){
                persona.setNacionalidad(elPersona.getAttribute("nacionalidad"));
            }
            if(!elPersona.getAttribute("identificacion").isEmpty()){
                persona.setIdentificacion(identificacion);
            }

        }else if(root.getElementsByTagName("JuridicaNacional").getLength()>0){//Juridica Nacional
            listPersonas = root.getElementsByTagName("JuridicaNacional");
            persona.setTipoIdentificacion("2");
            Element elPersona = (Element) listPersonas.item(0);
            if(!elPersona.getAttribute("nombre").isEmpty()){
                persona.setNombre(elPersona.getAttribute("nombre"));
            }
            if(!elPersona.getAttribute("identificacion").isEmpty()){
                persona.setIdentificacion(identificacion);
            }

        }else if(root.getElementsByTagName("JuridicaExtranjera").getLength()>0){//Juridica extranjera
            listPersonas = root.getElementsByTagName("JuridicaExtranjera");
            persona.setTipoIdentificacion("3");
            Element elPersona = (Element) listPersonas.item(0);
            if(!elPersona.getAttribute("nombre").isEmpty()){
                persona.setNombre(elPersona.getAttribute("nombre"));
            }
            if(!elPersona.getAttribute("identificacion").isEmpty()){
                persona.setIdentificacion(identificacion);
            }

        }
        
        persona.setCreationUser(usuario.getLogin());
        persona.setUserUpdate(usuario.getLogin());
        persona.setWebService("LOCALIZACION");

        WSHistCreditoService wsHCService = new WSHistCreditoService(usuario.getBd());
        tService.getSt().addBatch(wsHCService.guardarPersona(persona));
    }


    public void obtenerDirecciones(Element root) throws Exception{
        NodeList listDirecciones = root.getElementsByTagName("Direccion");
        int numRegistros = listDirecciones.getLength();
        for (int i = 0; i < numRegistros; i++) {
            Element elDireccion = (Element) listDirecciones.item(i);
            Direccion direccion = new Direccion();

            if(!elDireccion.getAttribute("probabilidadEntrega").isEmpty()){
                direccion.setProbabilidadEntrega(elDireccion.getAttribute("probabilidadEntrega"));
            }
            if(!elDireccion.getAttribute("numReportes").isEmpty()){
                direccion.setNumReportes(Integer.parseInt(elDireccion.getAttribute("numReportes")));
            }
            if(!elDireccion.getAttribute("actualizacion").isEmpty()){
                long fecha = Long.parseLong(elDireccion.getAttribute("actualizacion"));
                direccion.setActualizacion(new Timestamp(fecha));
            }
            if(!elDireccion.getAttribute("creacion").isEmpty()){
                long fecha = Long.parseLong(elDireccion.getAttribute("creacion"));
                direccion.setCreacion(new Timestamp(fecha));
            }
            if(!elDireccion.getAttribute("tipo").isEmpty()){
                direccion.setTipo(elDireccion.getAttribute("tipo"));
            }
            if(!elDireccion.getAttribute("fuente").isEmpty()){
                direccion.setFuente(elDireccion.getAttribute("fuente"));
            }
            if(!elDireccion.getAttribute("estrato").isEmpty()){
                direccion.setEstrato(elDireccion.getAttribute("estrato"));
            }

            NodeList listDato = elDireccion.getElementsByTagName("Dato");
            if(listDato.getLength()>0){
                Element elDato = (Element) listDato.item(0);
                NodeList listParteDireccion = elDato.getElementsByTagName("ParteDireccion");
                for (int j = 0; j < listParteDireccion.getLength(); j++) {
                    Element elParteDireccion = (Element) listParteDireccion.item(j);
                    String tipo = elParteDireccion.getAttribute("tipo");
                    if(tipo.equals("1")){
                        direccion.setDireccion(elParteDireccion.getAttribute("valor"));
                    }else if(tipo.equals("2")){
                        direccion.setCiudad(elParteDireccion.getAttribute("valor"));
                    }else if(tipo.equals("3")){
                        direccion.setDepartamento(elParteDireccion.getAttribute("valor"));
                    }else if(tipo.equals("4")){
                        direccion.setPais(elParteDireccion.getAttribute("valor"));
                    }else if(tipo.equals("5")){
                        direccion.setNuevaNomenclatura(elParteDireccion.getAttribute("valor"));
                    }
                }
            }

            NodeList listEntidad = elDireccion.getElementsByTagName("Entidad");
            for (int j = 0; j < listEntidad.getLength(); j++) {
                Element elEntidad = (Element) listEntidad.item(j);
                int contAttr = 0; //Cuenta el numero de atributos que contiene el elemento  entidad, para verificar que no se inserte vacio
                Entidad entidad = new Entidad();

                if(!elEntidad.getAttribute("codigoSuscriptor").isEmpty()){
                    entidad.setCodSuscriptor(elEntidad.getAttribute("codigoSuscriptor"));
                    contAttr++;
                }
                if(!elEntidad.getAttribute("nombreSuscriptor").isEmpty()){
                    entidad.setNombreSuscriptor(elEntidad.getAttribute("nombreSuscriptor"));
                    contAttr++;
                }
                if(!elEntidad.getAttribute("nit").isEmpty()){
                    entidad.setNit(elEntidad.getAttribute("nit"));
                    contAttr++;
                }
                if(!elEntidad.getAttribute("contrato").isEmpty()){
                    entidad.setContrato(elEntidad.getAttribute("contrato"));
                    contAttr++;
                }
                if(entidad.getCodSuscriptor()!=null && !entidad.getCodSuscriptor().isEmpty() && contAttr>0){
                    tService.getSt().addBatch(this.guardarEntidad(entidad));
                    direccion.setEntidad(entidad.getCodSuscriptor());
                }
            }

            direccion.setTipoIdentificacion(tipoIdentificacion);
            direccion.setIdentificacion(identificacion);
            direccion.setCreationUser(usuario.getLogin());
            direccion.setUserUpdate(usuario.getLogin());

            tService.getSt().addBatch(this.insertarDireccion(direccion));

        }
    }

    public void obtenerTelefonos(Element root, String tipoTelefono) throws Exception{
        NodeList listTelefonos = root.getElementsByTagName(tipoTelefono);
        int numRegistros = listTelefonos.getLength();
        for (int i = 0; i < numRegistros; i++) {
            Element elTelefono = (Element) listTelefonos.item(i);
            Telefono telefono = new Telefono();

            if(!elTelefono.getAttribute("numReportes").isEmpty()){
                telefono.setNumReportes(Integer.parseInt(elTelefono.getAttribute("numReportes")));
            }
            if(!elTelefono.getAttribute("actualizacion").isEmpty()){
                long fecha = Long.parseLong(elTelefono.getAttribute("actualizacion"));
                telefono.setActualizacion(new Timestamp(fecha));
            }
            if(!elTelefono.getAttribute("creacion").isEmpty()){
                long fecha = Long.parseLong(elTelefono.getAttribute("creacion"));
                telefono.setCreacion(new Timestamp(fecha));
            }
            if(!elTelefono.getAttribute("tipo").isEmpty()){
                telefono.setTipo(elTelefono.getAttribute("tipo"));
            }
            if(!elTelefono.getAttribute("fuente").isEmpty()){
                telefono.setFuente(elTelefono.getAttribute("fuente"));
            }
            if(!elTelefono.getAttribute("reportado").isEmpty()){
                telefono.setReportado(elTelefono.getAttribute("reportado"));
            }

            NodeList listDato = elTelefono.getElementsByTagName("Dato");
            if(listDato.getLength()>0){
                Element elDato = (Element) listDato.item(0);
                if(!elDato.getAttribute("nombreCiudad").isEmpty()){
                    telefono.setNombreCiudad(elDato.getAttribute("nombreCiudad"));
                }
                if(!elDato.getAttribute("nombreDepartamento").isEmpty()){
                    telefono.setNombreDepartamento(elDato.getAttribute("nombreDepartamento"));
                }
                if(!elDato.getAttribute("codigoPais").isEmpty()){
                    telefono.setCodigoPais(Integer.parseInt(elDato.getAttribute("codigoPais")));
                }
                if(!elDato.getAttribute("codigoArea").isEmpty()){
                    telefono.setCodigoArea(Integer.parseInt(elDato.getAttribute("codigoArea")));
                }
                if(!elDato.getAttribute("numero").isEmpty()){
                    telefono.setNumero(Long.parseLong(elDato.getAttribute("numero")));
                }
            }

            NodeList listEntidad = elTelefono.getElementsByTagName("Entidad");
            for (int j = 0; j < listEntidad.getLength(); j++) {
                Element elEntidad = (Element) listEntidad.item(j);
                int contAttr = 0; //Cuenta el numero de atributos que contiene el elemento entidad, para verificar que no se inserte vacio
                Entidad entidad = new Entidad();

                if(!elEntidad.getAttribute("codigoSuscriptor").isEmpty()){
                    entidad.setCodSuscriptor(elEntidad.getAttribute("codigoSuscriptor"));
                    contAttr++;
                }
                if(!elEntidad.getAttribute("nombreSuscriptor").isEmpty()){
                    entidad.setNombreSuscriptor(elEntidad.getAttribute("nombreSuscriptor"));
                    contAttr++;
                }
                if(!elEntidad.getAttribute("nit").isEmpty()){
                    entidad.setNit(elEntidad.getAttribute("nit"));
                    contAttr++;
                }
                if(!elEntidad.getAttribute("contrato").isEmpty()){
                    entidad.setContrato(elEntidad.getAttribute("contrato"));
                    contAttr++;
                }
                if(entidad.getCodSuscriptor()!=null && !entidad.getCodSuscriptor().isEmpty() && contAttr>0){
                    tService.getSt().addBatch(this.guardarEntidad(entidad));
                    telefono.setEntidad(entidad.getCodSuscriptor());
                }
            }

            telefono.setTipoIdentificacion(tipoIdentificacion);
            telefono.setIdentificacion(identificacion);
            telefono.setCreationUser(usuario.getLogin());
            telefono.setUserUpdate(usuario.getLogin());

            tService.getSt().addBatch(this.insertarTelefono(telefono));

        }
    }


    public void obtenerEmails(Element root) throws Exception{
        NodeList listEmails = root.getElementsByTagName("Email");
        int numRegistros = listEmails.getLength();
        for (int i = 0; i < numRegistros; i++) {
            Element elEmail = (Element) listEmails.item(i);
            EmailLocalizacion email = new EmailLocalizacion();

            if(!elEmail.getAttribute("numReportes").isEmpty()){
                email.setNumReportes(Integer.parseInt(elEmail.getAttribute("numReportes")));
            }
            if(!elEmail.getAttribute("actualizacion").isEmpty()){
                long fecha = Long.parseLong(elEmail.getAttribute("actualizacion"));
                email.setActualizacion(new Timestamp(fecha));
            }
            if(!elEmail.getAttribute("creacion").isEmpty()){
                long fecha = Long.parseLong(elEmail.getAttribute("creacion"));
                email.setCreacion(new Timestamp(fecha));
            }
            if(!elEmail.getAttribute("reportado").isEmpty()){
                email.setReportado(elEmail.getAttribute("reportado"));
            }
            if(!elEmail.getAttribute("tipo").isEmpty()){
                email.setTipo(elEmail.getAttribute("tipo"));
            }
            if(!elEmail.getAttribute("fuente").isEmpty()){
                email.setFuente(elEmail.getAttribute("fuente"));
            }

            NodeList listDato = elEmail.getElementsByTagName("Dato");
            if(listDato.getLength()>0){
                Element elDato = (Element) listDato.item(0);
                if(!elDato.getAttribute("direccion").isEmpty()){
                    email.setDireccion(elDato.getAttribute("direccion"));
                }
            }

            NodeList listEntidad = elEmail.getElementsByTagName("Entidad");
            for (int j = 0; j < listEntidad.getLength(); j++) {
                Element elEntidad = (Element) listEntidad.item(j);
                int contAttr = 0; //Cuenta el numero de atributos que contiene el elemento entidad, para verificar que no se inserte vacio
                Entidad entidad = new Entidad();

                if(!elEntidad.getAttribute("codigoSuscriptor").isEmpty()){
                    entidad.setCodSuscriptor(elEntidad.getAttribute("codigoSuscriptor"));
                    contAttr++;
                }
                if(!elEntidad.getAttribute("nombreSuscriptor").isEmpty()){
                    entidad.setNombreSuscriptor(elEntidad.getAttribute("nombreSuscriptor"));
                    contAttr++;
                }
                if(!elEntidad.getAttribute("nit").isEmpty()){
                    entidad.setNit(elEntidad.getAttribute("nit"));
                    contAttr++;
                }
                if(!elEntidad.getAttribute("contrato").isEmpty()){
                    entidad.setContrato(elEntidad.getAttribute("contrato"));
                    contAttr++;
                }
                if(entidad.getCodSuscriptor()!=null && !entidad.getCodSuscriptor().isEmpty() && contAttr>0){
                    tService.getSt().addBatch(this.guardarEntidad(entidad));
                    email.setEntidad(entidad.getCodSuscriptor());
                }
            }

            email.setTipoIdentificacion(tipoIdentificacion);
            email.setIdentificacion(identificacion);
            email.setCreationUser(usuario.getLogin());
            email.setUserUpdate(usuario.getLogin());

            tService.getSt().addBatch(this.insertarEmail(email));

        }
    }
    
    /**
     * Genera un archivo txt con la respuesta recibida del web service
     * @param solicitud datos enviados al web service
     * @param xml datos recibidos
     * @throws Exception 
     */
    private void generarTXT(String solicitud, String xml) throws IOException{
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String ruta = rb.getString("ruta") + "/WEB-INF/classes/logsDatacredito/";
            File archivo = new File(ruta);            
            if (!archivo.exists()) {
                archivo.mkdirs();
            }

            FileWriter fw = new FileWriter(ruta + "localizacion_"+Util.getFechaActual_String(8)+".txt", true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter salida = new PrintWriter(bw);
            salida.println("SOLICITUD "+Util.getFechaActual_String(9)+":");
            salida.println(solicitud);
            salida.println("RESPUESTA:");
            salida.print(xml);
            salida.println();
            salida.println();
            salida.close();
        } catch(java.io.IOException ioex) {
          throw ioex;
        }
    }
    
    public String consultarLocalizacion(String tipoIdentificacion, String identificacion, String primerApellido, Usuario usuario){
        
        try {
            
            this.identificacion = identificacion;
            this.tipoIdentificacion = tipoIdentificacion;
            this.usuario = usuario;
            
            TablaGenManagerDAO tgendao = new TablaGenManagerDAO();
            TablaGen t = tgendao.obtenerInformacionDato("PARAM_WSDC", "USUARIO_LOCALIZACION");
            if(t != null){
            
                ServicioLocalizacion2 wsLocalizacion = new ServicioLocalizacion2_Impl();
                ServicioLocalizacion wsLocalizacionPort = wsLocalizacion.getServicioLocalizacion2();

                //((Stub) wsLocalizacionPort).setTimeout(60000);
                
                String solicitud = "<SolicitudDatosLocalizacion tipoIdentificacion=\"" + tipoIdentificacion + "\" identificacion=\"" + identificacion + "\" usuario=\""+t.getReferencia()
                                   + "\" clave=\""+t.getDato()+"\" primerApellido=\"" + primerApellido + "\"></SolicitudDatosLocalizacion>";
                String respuesta = wsLocalizacionPort.consultarDatosLocalizacion(solicitud);
                generarTXT(solicitud, respuesta);
                parseXML(respuesta);
            }else{
                mensaje = "El usuario no esta autorizado para consultar a datacredito";
            }
        } catch(javax.xml.rpc.ServiceException ex) {
            ex.printStackTrace();
        } catch(java.rmi.RemoteException ex) {
            System.out.println("TIMEOUT");
            ex.printStackTrace();
        } catch(IOException ioex){
            ioex.printStackTrace();
        } catch(Exception ex){
            ex.printStackTrace();
        }
        return mensaje;
    }        
}
