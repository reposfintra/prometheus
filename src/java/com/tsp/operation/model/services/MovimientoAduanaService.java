/*
 * Nombre        MovimientoAduanaService.java
 * Descripci�n   Ofrece los servicios para el acceso a los datos de movimiento_aduana
 * Autor         Alejandro Payares
 * Fecha         19 de enero de 2006, 06:04 PM
 * Version       1.0
 * Coyright      Transportes Sanchez Polo SA.
 */

package com.tsp.operation.model.services;


import com.tsp.operation.model.DAOS.MovimientoAduanaDAO;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 * Ofrece los servicios para el acceso a los datos de movimiento_aduana
 * @author Alejandro Payares
 */
public class MovimientoAduanaService {
    
    private MovimientoAduanaDAO dao;
    
    /**
     * Crea una nueva instancia de MovimientoAduanaService
     * @autor  Alejandro Payares
     */
    public MovimientoAduanaService() {
        dao = new MovimientoAduanaDAO();
    }
    
    /**
     * Busca los movimientos de aduana para la remesa dada, el rsultado de la busqueda lo guarda en una lista
     * @param numrem el numero de la remesa
     * @throws SQLException si algun error en la base de datos ocurre
     */    
    public void buscarMovimientos(String numrem)throws SQLException{
        dao.buscarMovimientos(numrem);
    }
    
    /**
     * Devuelve la lista con los movimientos de aduana encontrados por el m�todo buscarMovimientos
     * @return La lista con los movimientos de aduana encontrados
     */    
    public LinkedList obtenerMovimientos(){
        return dao.obtenerMovimientos();
    }
    
}
