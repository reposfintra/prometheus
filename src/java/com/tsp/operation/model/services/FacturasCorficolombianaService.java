

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.FacturasCorficolombianaDAO;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class FacturasCorficolombianaService {
    ArrayList resultado,facturasRes;

    private String ruta;

    public ArrayList getResultado() {
        return resultado;
    }

    public void setResultado(ArrayList resultado) {
        this.resultado = resultado;
    }

    public ArrayList getReportes() throws Exception
    {   FacturasCorficolombianaDAO cd= new FacturasCorficolombianaDAO();
        return cd.getReportes();
    }

    public void getConsultaNms()throws Exception{
        FacturasCorficolombianaDAO cd= new FacturasCorficolombianaDAO();
        this. setResultado(cd.getConsultaNms());
        //return cd.getConsulta(id_reporte, tipo, periodo);
    }

    public void getConsultaRes()throws Exception{
        FacturasCorficolombianaDAO cd= new FacturasCorficolombianaDAO();
        this. setFacturasRes(cd.getConsultaRes());
        //return cd.getConsulta(id_reporte, tipo, periodo);
    }

    public String generarFacturas(String[] facturas,String usuario) throws Exception{
        FacturasCorficolombianaDAO cd= new FacturasCorficolombianaDAO();
        String ret=cd.generarFacturas(facturas,usuario);
        generarArchivo(usuario,"REPFIDECA");
        return ret;
    }

    public String generarReporte(String[] facturas,String usuario,String fecha) throws Exception{
        FacturasCorficolombianaDAO cd= new FacturasCorficolombianaDAO();
        String ret=cd.generarReporteRc(facturas,usuario,fecha);
        generarArchivo(usuario,"REPFIDECAR");
        return ret;
    }

    public void generarArchivo(String usuario,String tipo) throws Exception{
        FacturasCorficolombianaDAO cd= new FacturasCorficolombianaDAO();
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario;
        File archivo = new File( ruta );
        archivo.mkdirs();
        String  url   ="";
        if(tipo.equals("REPFIDECA")){
            url=ruta + "/ReporteFiduciaCartera" +  (new SimpleDateFormat("yyyMMdd_hhmmss")).format( new Date())+".txt";

        }else{
            url=ruta + "/ReporteFiduciaRecaudos" +  (new SimpleDateFormat("yyyMMdd_hhmmss")).format( new Date())+".txt";
        }
        PrintWriter pw = new PrintWriter( new BufferedWriter( new FileWriter(url) )  );
        pw.print(cd.generarReporte(tipo));
        pw.close();
    }

    public ArrayList getFacturasRes() {
        return facturasRes;
    }

    public void setFacturasRes(ArrayList facturasRes) {
        this.facturasRes = facturasRes;
    }


}