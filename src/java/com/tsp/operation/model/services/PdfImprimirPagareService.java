/*************************************************************************
 * Nombre Clase ............. PdfImprimirPagare.java
 * Descripci�n .. . . . . . . FV- 210 Impresion de pagares
 * Autor . . . . . . . . . .  Ing. Jose Avila
 * Fecha . . . . . . . . . .  24/07/2012
 * versi�n . . . . . . . . .  1.0
 * Copyright................  Fintra S.A.
************************************************************************/
package com.tsp.operation.model.services;

import com.lowagie.text.*;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.BaseFont;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
import java.io.*;

import java.awt.Color;
import java.sql.SQLException;

/**
 *
 * @author Ing. Jose Avila
 */
public class PdfImprimirPagareService {

 
    int diay = 0;
    int mesy = 0;
    int anoy = 0;
    int enum_letras = 0;
    String ecc_representante = "";
    String erepresentada = "";
    String ecc_deudor = "";
    String ecc_codeudor = "";
    String eswcodeudor = "";
    Document document;
    int n = 0;
    int contador = 0;
    int cantidad_de_letras = 7;
    PdfPTable table;
    float[] widths = {0.12f, 0.17f, 0.1f, 0.27f, 0.17f, 0.17f};
    float[] widthsCabecera = {0.2f, 0.3f, 0.15f, 0.35f};
    BaseFont bf;
    Font fuente_normal = new Font(bf, 9);
    Font fuente_normal_grande = new Font(bf, 20);
    Font fuente_negrita_grande = new Font(bf, 20f);
    Font fuente_negrita8 = new Font(bf, 9);
    Font fuente_normal_media = new Font(bf, 14);
    Font fuente_negrita_media = new Font(bf, 14);
    PdfWriter writer;
    Rectangle page;
    PdfPCell linea_blanco;
    SimpleDateFormat fmt = null;
    String ruta = "";

    //Variables de datos
    String orden = "";
    Model model;
    File pdf;
    Model modelito;
    BeanGeneral bg;
    String x = "xxx";
    Phrase texto_principal;
    String no_aprobacion = "", ciudad = "", fecha_otorgamiento = "", fecha_vencimiento = "", consecutivo = "", digito = "", nombre = "", cedula = "", ciudad_cedula = "", representado = "", nombre_codeudor = "", cedula_codeudor = "", dia_pagare = "", mes_pagare = "", ano_pagare = "", empresa = "", plata_letra = "", plata_numero = "", empresa_facultada = "", ciudad_pago = "", cedula_deudor = "", cedula_firma_codeudor = "";
    String fecha_otorgamiento_inicial = "";//mar14d8
    java.util.Calendar Fecha_Negocio;
    java.util.Calendar Fecha_Temp;
    ResourceBundle rb;
    Phrase frase;
    ProveedorService proveedorService;
    Convenio convenio = new Convenio();
    GestionConveniosService convenio_service;
    NitDAO nitDAO;
    
     String acreedor = "";
     String nit = "";
     String np = "";
     
     SerieGeneral serie= new SerieGeneral(); 
     SerieGeneralService serieGeneralService;
     Usuario usuario; 
     GestionSolicitudAvalService gestion_solicitud_service;
     NegociosGenService Negociossvc;
    
    //Phrase frase2;

    /**
     * Creates a new instance of PdfImprimirPagareService
     * @param dataBaseName
     */
//    public PdfImprimirPagareService() throws Exception {
//        bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
//        fuente_negrita_grande.setStyle(com.lowagie.text.Font.BOLD);
//        fuente_negrita8.setStyle(com.lowagie.text.Font.BOLD);
//        fuente_negrita_media.setStyle(com.lowagie.text.Font.BOLD); 
//        proveedorService = new ProveedorService();
//        convenio_service = new GestionConveniosService();
//        nitDAO = new NitDAO();
//        serieGeneralService = new SerieGeneralService() ;
//        gestion_solicitud_service = new GestionSolicitudAvalService();
//        Negociossvc = new NegociosGenService();
//    }
    public PdfImprimirPagareService(String dataBaseName) throws Exception {
        bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        fuente_negrita_grande.setStyle(com.lowagie.text.Font.BOLD);
        fuente_negrita8.setStyle(com.lowagie.text.Font.BOLD);
        fuente_negrita_media.setStyle(com.lowagie.text.Font.BOLD); 
        proveedorService = new ProveedorService(dataBaseName);
        convenio_service = new GestionConveniosService(dataBaseName);
        nitDAO = new NitDAO(dataBaseName);
        serieGeneralService = new SerieGeneralService(dataBaseName);
        gestion_solicitud_service = new GestionSolicitudAvalService(dataBaseName);
        Negociossvc = new NegociosGenService(dataBaseName);
    }

  

    public void generarPdfNuevoPagare(Negocios negocio, SolicitudPersona sp,  Proveedor pro,  SolicitudAval aval, Usuario usuario) {
        try {
            //.out.println("gpdfpag");

            /**************************************************************
             *Bloque para validar el titular del pagare cuando es educativo
             **************************************************************/
            SolicitudPersona sc = gestion_solicitud_service.buscarPersona(Integer.parseInt(aval.getNumeroSolicitud()), "C");
            //si no existe un cod_deudor entoces se consulta el estudiante.
            SolicitudPersona sEstudiante = null;
            if(sc== null){
            
               sEstudiante=gestion_solicitud_service.buscarPersona(Integer.parseInt(aval.getNumeroSolicitud()),"E");
            
            }
            
             
           // SolicitudPersona sc = gestion_solicitud_service.buscarPersona(Integer.parseInt(aval.getNumeroSolicitud()), "C");
            Font fuente_normal2 = new Font(bf, 6);
            Font fuente_normal2_blanca = new Font(bf, 6);
            fuente_normal2_blanca.setColor(Color.WHITE);
            Font fuente_negrita2 = new Font(bf, 7);
            fuente_negrita2.setStyle(com.lowagie.text.Font.BOLD);
            
        
            
            String fechanegocio = negocio.getFecha_neg();// bg.getValor_02();
            Calendar temcal = Calendar.getInstance();
            temcal.set(Integer.parseInt(fechanegocio.substring(0, 4)), Integer.parseInt(fechanegocio.substring(5, 7)) - 1, Integer.parseInt(fechanegocio.substring(8, 10)));
            String fechanegocioletra = Util.ObtenerFechaCompleta(temcal) + "" ;  
            
            String fechaActual = null;
            Date date = new Date();
            SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
                if(fechaActual == null){
                    fechaActual = s.format(date);
                }
         
            temcal.set(Integer.parseInt(fechaActual.substring(0, 4)), Integer.parseInt(fechaActual.substring(5, 7)) - 1, Integer.parseInt(fechaActual.substring(8, 10)));
            String fechaletra = Util.ObtenerFechaCompleta(temcal) + "" ;   
            
            np = Negociossvc.getNumeroPagare(negocio.getCod_negocio());
            if (np == null || np.equals("")) {
                serie = serieGeneralService.getSerie(usuario.getCia(), usuario.getId_agencia(), "PGA");
                np = serie.getUltimo_prefijo_numero();
                serieGeneralService.setSerie(usuario.getCia(), usuario.getId_agencia(), "PGA"); //Incrementa el numero de ingreso
                Negociossvc.actualizaNumeroPagare(negocio.getCod_negocio(), np);
            }


            RMCantidadEnLetras c = new RMCantidadEnLetras();
            String[] a;
            a = c.getTexto(Math.round(negocio.getVr_negocio()));

            String res = "";
            for (int i = 0; i < a.length; i++) {
                res = res + ((String) a[i]).replace("-", "");
            }
            String valorenletras = res.trim();//Valor Total en Letras


                        
                
            // obtengo los datos del negocio
             int nudocs = negocio.getNodocs();
             float tasa = Float.parseFloat(negocio.getTasa());
             
             convenio=convenio_service.buscar_convenio(usuario.getBd(), String.valueOf(negocio.getId_convenio()));                 
   
                  
            NitSot nit_mediador = nitDAO.searchNit(convenio.getNit_mediador());
            CiudadService ciudad_service = new CiudadService();
            String ciudadpagare ="";
            if (convenio.isMediador_aval()) {
                acreedor = nit_mediador.getNombre();
                nit = convenio.getNit_mediador();
                ciudadpagare = ciudad_service.buscarNomCiudadxCodigo(nit_mediador.getCodciu());
            } else {
                acreedor = pro.getC_payment_name();
                nit = (negocio.getId_convenio() == 17 || negocio.getId_convenio() == 22 || negocio.getId_convenio() == 35) ? "8020220161" : negocio.getNit_tercero();//ojo el convenio cambia para piloto y productivo
                NitSot info_nit = nitDAO.searchNit(nit);
                ciudadpagare = ciudad_service.buscarNomCiudadxCodigo(info_nit.getCodciu());
            }


             String nombrecliente = sp.getNombre();            



            try {
                /*
                 * Font fuente_negrita_azul; fuente_negrita_azul = new Font(bf,
                 * 7F); fuente_negrita_azul.setStyle(1);
            fuente_negrita_azul.setColor(Color.BLUE);
                 */
                bf = BaseFont.createFont("Helvetica", "Cp1252", false);
                rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                ruta = (new StringBuilder()).append(rb.getString("ruta")).append("/pdf/pagarePersonaNatural" + ".pdf").toString();

                Document document = new Document();
                PdfWriter.getInstance(document, new FileOutputStream(ruta));
                com.lowagie.text.Rectangle page = document.getPageSize();

                document.open();
                //.out.println("antes de newpage");
                document.newPage();
                //.out.println("despues de newpage");
                PdfPTable table = new PdfPTable(1);
                //PdfPTable tablegrande = new PdfPTable(1);

                table.setWidthPercentage(100F);
                PdfPCell cell_superior_vacia = new PdfPCell();
                cell_superior_vacia.setColspan(1);
                cell_superior_vacia.setBorderWidthTop(1.0F);
                cell_superior_vacia.setBorderWidthBottom(0.0F);
                cell_superior_vacia.setBorderWidthLeft(1.0F);
                cell_superior_vacia.setBorderWidthRight(1.0F);
                cell_superior_vacia.setPhrase(new Phrase("", fuente_normal2));
                table.addCell(cell_superior_vacia);
                PdfPCell cell = new PdfPCell();
                PdfPCell cellrectangle = new PdfPCell();

                cell.setPaddingLeft(11);
                cell.setPaddingRight(11);

                cell.setColspan(1);
                cell.setBorderWidthTop(0.0F);
                cell.setBorderWidthBottom(0.0F);
                cell.setBorderWidthLeft(1.0F);
                cell.setBorderWidthRight(1.0F);
                cell.setHorizontalAlignment(1);
                cell.setPhrase(new Phrase("", fuente_negrita2));
                cell.setVerticalAlignment(5);
                table.addCell(cell);
                cell.setPhrase(new Phrase("", fuente_negrita2));
                table.addCell(cell);
                cell.setPhrase(new Phrase("PAGAR� No. " + np + "\n\n\n\n\n", fuente_negrita2));
                table.addCell(cell);
                cell.setPhrase(new Phrase("", fuente_negrita2));
                table.addCell(cell);
                table.addCell(cell);
                cell.setHorizontalAlignment(cell.ALIGN_JUSTIFIED);

                Phrase respuesta = new Phrase("");
                respuesta.add(new Phrase("Lugar y fecha de Firma: ", fuente_normal2));
                if (negocio.getCod_negocio().substring(0, 2).equals("FB")) {
                    respuesta.add(new Phrase("CARTAGENA", fuente_negrita2));
                } else {
                    respuesta.add(new Phrase(ciudadpagare, fuente_negrita2));
                }
                respuesta.add(new Phrase(", ", fuente_normal2));
                respuesta.add(new Phrase(fechanegocioletra, fuente_negrita2));
                respuesta.add(new Phrase("\nValor $ ", fuente_normal2));
                respuesta.add(new Phrase(Util.customFormat(negocio.getVr_negocio()), fuente_negrita2));
                respuesta.add(new Phrase(" ", fuente_normal2));
                respuesta.add(new Phrase(valorenletras, fuente_negrita2));
                respuesta.add(new Phrase("\nPlazo: ", fuente_normal2));
                respuesta.add(new Phrase(nudocs+" meses", fuente_negrita2));
                respuesta.add(new Phrase("\nIntereses corrientes durante el plazo: ", fuente_normal2));
                respuesta.add(new Phrase(tasa+" %", fuente_negrita2));
                respuesta.add(new Phrase("\nIntereses de mora: A la tasa m�xima permitida por la ley. ", fuente_normal2));
                respuesta.add(new Phrase("\nPersona a quien debe hacerse el pago (acreedor): ", fuente_normal2));
                respuesta.add(new Phrase(acreedor+" NIT. "+nit, fuente_negrita2));
                //ciudad del pagare 
                if(negocio.getCod_negocio().substring(0, 2).equals("FB")){
                respuesta.add(new Phrase("\nLugar donde se efectuar� el pago: Cartagena (Bolivar). ", fuente_normal2));
                }else {
                respuesta.add(new Phrase("\nLugar donde se efectuar� el pago: Barranquilla (Atl�ntico). ", fuente_normal2));
                }
                
                 if (aval.getTipoPersona().equalsIgnoreCase("N")) {    
                    respuesta.add(new Phrase("\n\nYo (nosotros) ", fuente_normal2));
                    respuesta.add(new Phrase(nombrecliente, fuente_negrita2));
                     //validar que el tipo de producto sea universitario 01
                     if (aval.getProducto().equals("01")) {
                         //preguntamos si tiene codeudor
                         if (sc != null) {
                             respuesta.add(new Phrase(" y "+ sc.getNombre(), fuente_negrita2));
                         } else if(sEstudiante !=null){
                             respuesta.add(new Phrase(" y "+sEstudiante.getNombre(), fuente_negrita2));
                         }
                     } else {

                         //respuesta.add(new Phrase(nombrecliente, fuente_negrita2));
                         if (sc != null) {
                             respuesta.add(new Phrase(" y " + sc.getNombre(), fuente_negrita2));
                         } else {
                             respuesta.add(new Phrase(" ", fuente_negrita2));
                         }
                     }
                    respuesta.add(new Phrase(" identificado(s) como aparece al pie de mi (nuestras) firma (s), actuando en nombre propio declaro (amos): ", fuente_normal2));
                }                
                 if (aval.getTipoPersona().equalsIgnoreCase("J")) {
                    String RepLegalNombre = sp.getRepresentanteLegal();
                    respuesta.add(new Phrase("\n\nYo "+RepLegalNombre, fuente_normal2));
                    respuesta.add(new Phrase(" actuando en nombre y representaci�n de la empresa "+nombrecliente +" y "+RepLegalNombre+" actuando en nombre propio, identificados como aparece al pie de mi (nuestras) firma(s), declaramos:", fuente_normal2));
                }

                respuesta.add(new Phrase("\n\nPRIMERA.- OBJETO:", fuente_negrita2));
                respuesta.add(new Phrase(" Que por virtud del presente t�tulo valor pagar� (mos) incondicionalmente a la orden de: ", fuente_normal2));
                respuesta.add(new Phrase(acreedor, fuente_negrita2));
                respuesta.add(new Phrase(" o a quien represente sus derechos, en la ciudad indicada, y en las fechas de amortizaci�n por cuotas se�aladas en la cl�usula tercera de este pagar�, la suma de ", fuente_normal2));
                respuesta.add(new Phrase(valorenletras+" $ "+Util.customFormat(negocio.getVr_negocio()), fuente_negrita2));
                respuesta.add(new Phrase(" m�s los intereses se�alados en la cl�usula segunda de este documento.", fuente_normal2));
                
                respuesta.add(new Phrase("\n\nSEGUNDA.- INTERESES:", fuente_negrita2));
                respuesta.add(new Phrase(" Que sobre la suma debida  se reconocer�n intereses corrientes a una tasa nominal mensual del ", fuente_normal2));
                respuesta.add(new Phrase(tasa+"", fuente_negrita2));
                respuesta.add(new Phrase(" %. Sin embargo, en caso de mora en el cumplimiento de las cuotas se�aladas en la cl�usula tercera de este pagar�, cancelar� intereses de mora a la tasa m�xima permitida por la ley, de manera mensual y sobre el saldo insoluto que llegue a estar en mora.", fuente_normal2));

                respuesta.add(new Phrase("\n\nTERCERA.- PLAZO.", fuente_negrita2));
                respuesta.add(new Phrase(" Que pagar� el capital indicado en la cl�usula primera de este pagar� mediante ", fuente_normal2));
                respuesta.add(new Phrase("("+nudocs+")", fuente_negrita2));
                respuesta.add(new Phrase(" cuotas iguales, mensuales y sucesivas, de acuerdo a las condiciones acordadas verificables en la tabla de amortizaci�n recibida.", fuente_normal2));
                
                respuesta.add(new Phrase("\n\nCUARTA.- CLAUSULA ACELERATORIA: "+acreedor, fuente_negrita2));
                respuesta.add(new Phrase(" o el tenedor legitimo de este pagar� podr� declarar de plazo vencido la obligaci�n a que se refiere este pagar� y exigir el pago total, judicial o extra judicialmente, en los siguientes casos: ", fuente_normal2));
                respuesta.add(new Phrase(" a)", fuente_negrita2));
                respuesta.add(new Phrase(" el incumplimiento en cualquier forma, de las estipulaciones contenidas en este documento, ", fuente_normal2));
                respuesta.add(new Phrase(" b)", fuente_negrita2));
                respuesta.add(new Phrase(" a mora en el pago de una o m�s cuotas de capital e intereses o de cualquier otra obligaci�n directa, indirecta, conjunta o separada tenga (mos) para con ", fuente_normal2));
                respuesta.add(new Phrase(acreedor, fuente_negrita2));
                respuesta.add(new Phrase(" o el tenedor legitimo de este pagar�,", fuente_normal2));
                respuesta.add(new Phrase(" c)", fuente_negrita2));
                respuesta.add(new Phrase(" el hecho de ser demandado o embargado ante cualquier autoridad por cualquier persona,", fuente_normal2));
                respuesta.add(new Phrase(" d)", fuente_negrita2));
                respuesta.add(new Phrase(" el giro de cheques sin provisi�n de fondos, e) por solicitar o ser declarado en proceso de concordato, liquidaci�n u otro similar,", fuente_normal2));
                respuesta.add(new Phrase(" f)", fuente_negrita2));
                respuesta.add(new Phrase(" por existencia discrepancia o inexactitud de la informaci�n que hemos suministrado en los balances,", fuente_normal2));
                respuesta.add(new Phrase(" g)", fuente_negrita2));
                respuesta.add(new Phrase(" si a juicio de ", fuente_normal2));
                respuesta.add(new Phrase(acreedor, fuente_negrita2));
                respuesta.add(new Phrase(" o del tenedor legitimo de este pagar� se observa variaci�n en cualquiera de mi (nuestra) situaci�n financiera, jur�dica, econ�mica o en esquema de propiedad o administraci�n con respecto a aquellas sobre las cuales fueron aprobados lo cr�ditos, de manera que pongan en peligro el pago oportuno de las obligaciones a mi (nuestro) cargo,", fuente_normal2));
                respuesta.add(new Phrase(" h)", fuente_negrita2)); 
                respuesta.add(new Phrase(" cuando ", fuente_normal2));
                respuesta.add(new Phrase(acreedor, fuente_negrita2));
                respuesta.add(new Phrase(" o el tenedor legitimo de este pagar� establezca que he (mos) incurrido en alguna de las conductas descritas como delitos en el C�digo Penal o en otras disposiciones legales o reglamentarias que tengan relaci�n con el lavado de activos.", fuente_normal2));
                
                        
                respuesta.add(new Phrase("\n\nQUINTA.- DOMICILIO:", fuente_negrita2));
                respuesta.add(new Phrase(" Declaro (amos) que renuncio (amos) a mi (nuestro) domicilio.", fuente_normal2));
                
                respuesta.add(new Phrase("\n\nSEXTA.- IMPUESTO DE TIMBRE:", fuente_negrita2));
                respuesta.add(new Phrase(" Los gastos por concepto del impuesto de timbre, si se causaren, correr�n a cargo de los deudores y podr�n serles cobrados aun mediante proceso ejecutivo si no los cancelaren en la fecha en que se suscribe este pagare.", fuente_normal2));
               
                respuesta.add(new Phrase("\n\nOCTAVA.-", fuente_negrita2));
                respuesta.add(new Phrase(" Declaro (amos) excusada la presentaci�n y la noticia de rechazo.", fuente_normal2));
                
                //item noveno para la mostrar la fecha del ultimo pago.
                String fechaUltmo=gestion_solicitud_service.ultimaFecha(negocio.getCod_negocio());
                Calendar fehaUltimoPago= Calendar.getInstance();
                fehaUltimoPago.set(Integer.parseInt(fechaUltmo.substring(0, 4)), Integer.parseInt(fechaUltmo.substring(5, 7)) - 1, Integer.parseInt(fechaUltmo.substring(8, 10)));
                respuesta.add(new Phrase("\n\nNOVENA.-", fuente_negrita2));
                respuesta.add(new Phrase("VENCIMIENTO: ", fuente_normal2));
                respuesta.add(new Phrase(Util.ObtenerFechaCompleta(fehaUltimoPago) + "", fuente_negrita2));
                
                respuesta.add(new Phrase("\n\nEl pagar� as� llenado ser� exigible inmediatamente y prestar� merito ejecutivo sin m�s requisitos ni requerimientos.", fuente_normal2));
                if (negocio.getCod_negocio().substring(0, 2).equals("FB")) {
                    respuesta.add(new Phrase("\n\nEn Constancia de lo anterior, se suscribe este documento en la ciudad de Cartagena de indias, el d�a ", fuente_normal2));
                } else {
                    respuesta.add(new Phrase("\n\nEn Constancia de lo anterior, se suscribe este documento en la ciudad de Barranquilla el d�a ", fuente_normal2));
                }
                respuesta.add(new Phrase(fechanegocioletra, fuente_negrita2));
                respuesta.add(new Phrase("\n\n\n\n\n\n\n\n\n\n", fuente_negrita2));
                
             
            

                //**************************************************************************
                
                
                PdfPTable tabla_temp = new PdfPTable(4);

                java.awt.Color color_fuente_header = new java.awt.Color(0xAA, 0xAA, 0xAA);

                Font fuente_header = new Font(Font.TIMES_ROMAN, 7, Font.BOLD);
                fuente_header.setColor(color_fuente_header);

                float[] medidaCeldas = {0.250f,0.250f,0.250f,0.250f};
                tabla_temp.setWidths(medidaCeldas);

                PdfPCell cell1 = new PdfPCell(); //fila 1

                cell1 = new PdfPCell();
                cell1.setBorderWidthTop(0);
                cell1.setBorderWidthLeft(1.0f);
                cell1.setBorderWidthRight(0);
                cell1.setBorderWidthBottom(0);
                cell1.setPhrase(new Phrase("", fuente_header));
                cell1.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                cell1.addElement(this.tabla1(aval));// agrego tabla 1
                tabla_temp.addCell(cell1);
                tabla_temp.setWidthPercentage(100);
                
                PdfPCell cell2 = new PdfPCell(); //fila 2

                cell2 = new PdfPCell();
                cell2.setBorderWidthTop(0);
                cell2.setBorderWidthLeft(0);
                cell2.setBorderWidthRight(0);
                cell2.setBorderWidthBottom(0);
                cell2.setPhrase(new Phrase("", fuente_header));
                cell2.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                cell2.addElement(this.tablaHuella1());
                tabla_temp.addCell(cell2);
                tabla_temp.setWidthPercentage(100);
                
                PdfPCell cell3 = new PdfPCell(); //fila 3

                cell3 = new PdfPCell();
                cell3.setBorderWidthTop(0);
                cell3.setBorderWidthLeft(0);
                cell3.setBorderWidthRight(0);
                cell3.setBorderWidthBottom(0);
                cell3.setPhrase(new Phrase("", fuente_header));
                cell3.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                cell3.addElement(this.tabla2());
                tabla_temp.addCell(cell3);
                tabla_temp.setWidthPercentage(100);
                
                PdfPCell celda_temp = new PdfPCell(); //fila 4

                celda_temp = new PdfPCell();
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                celda_temp.addElement(this.tablaHuella2());// agrego tabla 1
                celda_temp.setBorderWidthTop(0);
                celda_temp.setBorderWidthLeft(0);
                celda_temp.setBorderWidthRight(1.0f);
                celda_temp.setBorderWidthBottom(0); 
                tabla_temp.addCell(celda_temp);
 
                
                
                
               // document.add(tabla_temp);
                 
                 
                 
                 
                 
               
                
                cell.setPhrase(respuesta);
                table.addCell(cell);
                document.add(table);
               
                
            
                
                
                //Dibujo las tablas pa las huellas y las firma
                table = new PdfPTable(6);
                table.setWidthPercentage(100F);
                cell.setHorizontalAlignment(cell.ALIGN_CENTER);

                cell.setBorderWidthTop(0);
                cell.setBorderWidthBottom(0);
                cell.setBorderWidthLeft(1.0F);
                cell.setBorderWidthRight(0);
                cell.setPhrase(new Phrase("", fuente_normal2));
                //cell.setPhrase(new Phrase(texto22, fuente_normal));
                table.addCell(cell);//1
                cellrectangle.setBorderWidthTop(0);
                cellrectangle.setBorderWidthBottom(0);
                cellrectangle.setBorderWidthLeft(0);
                cellrectangle.setBorderWidthRight(0);

                cellrectangle.setPhrase(new Phrase("\n\n\n\n\n\n\n\n", fuente_normal2));
                //cellrectangle.setPaddingLeft(35);
                //cellrectangle.setPaddingRight(35);
                table.addCell(cellrectangle);//2
                cell.setBorderWidthTop(0);
                cell.setBorderWidthBottom(0);
                cell.setBorderWidthLeft(0);
                cell.setBorderWidthRight(0);
                cell.setPhrase(new Phrase("", fuente_normal2));
                table.addCell(cell);//3
                table.addCell(cell);//4
                table.addCell(cellrectangle);//5
                cell.setPhrase(new Phrase("", fuente_normal2));
                cell.setBorderWidthTop(0.0F);
                cell.setBorderWidthBottom(0.0F);
                cell.setBorderWidthLeft(0.0F);
                cell.setBorderWidthRight(1.0F);
                table.addCell(cell);//6

                cell.setBorderWidthTop(0.0F);
                cell.setBorderWidthBottom(0.0F);
                cell.setBorderWidthLeft(1.0F);
                cell.setBorderWidthRight(0.0F);
                cell.setPhrase(new Phrase("", fuente_normal2));
                table.addCell(cell); //1
                cell.setBorderWidthTop(0.0F);
                cell.setBorderWidthBottom(0.0F);
                cell.setBorderWidthLeft(0.0F);
                cell.setBorderWidthRight(0.0F);
           //     cell.setPhrase(new Phrase(texto23y25, fuente_normal2));
                table.addCell(cell);//2
                cell.setPhrase(new Phrase("", fuente_normal2));
                table.addCell(cell);//3
                table.addCell(cell);//4
            //    cell.setPhrase(new Phrase(texto23y25, fuente_normal2));
                table.addCell(cell);//5
                cell.setPhrase(new Phrase("", fuente_normal2));
                cell.setBorderWidthTop(0.0F);
                cell.setBorderWidthBottom(0.0F);
                cell.setBorderWidthLeft(0.0F);
                cell.setBorderWidthRight(1.0F);
                table.addCell(cell);//6

                cell.setBorderWidthTop(0.0F);
                cell.setBorderWidthBottom(1.0F);
                cell.setBorderWidthLeft(1.0F);
                cell.setBorderWidthRight(0.0F);
                cell.setHorizontalAlignment(cell.ALIGN_LEFT);
                cell.setColspan(3);
             //   cell.setPhrase(text22);
                table.addCell(cell);   //1  y 2 y 3
                cell.setColspan(1);
                cell.setBorderWidthTop(0.0F);
                cell.setBorderWidthBottom(0.0F);
                cell.setBorderWidthLeft(0.0F);
                cell.setBorderWidthRight(0.0F);
                cell.setPhrase(new Phrase("     ", fuente_normal2));
                //table.addCell(cell);//3
                cell.setColspan(3);
           //     cell.setPhrase(text24);
                cell.setBorderWidthTop(0.0F);
                cell.setBorderWidthBottom(1.0F);
                cell.setBorderWidthLeft(0.0F);
                cell.setBorderWidthRight(1.0F);
                table.addCell(cell);//4 y 5 y 6
                cell.setColspan(1);
               
                
                document.add(tabla_temp);
            

                document.add(table);
                //.out.println("despues de add table");
                document.newPage();
                //.out.println("antes de documento cerrado");
                document.close();
                //.out.println("documento cerrado"); 


            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("ERROR en generarPdfNuevoPagare" + e.toString() + "__" + e.getMessage());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("error en servicillo__" + ex.toString() + "__" + ex.getMessage());
        }
    }
    
    public boolean generarPdfPagareVacio(Negocios negocio, SolicitudAval aval, boolean imprimirPagare, Usuario usuario) {
        boolean swNuevoPagare = false;
        try {                 
            Font fuente_normal2 = new Font(bf, 9);
            Font fuente_negrita2 = new Font(bf, 9);
            fuente_negrita2.setStyle(com.lowagie.text.Font.BOLD);
            
            GestionSolicitudAvalService gsas = new GestionSolicitudAvalService(usuario.getBd());
            SolicitudPersona sp = gsas.buscarPersona(Integer.parseInt(aval.getNumeroSolicitud()), "S");
            SolicitudPersona spc = gsas.buscarPersona(Integer.parseInt(aval.getNumeroSolicitud()), "C");
            SolicitudPersona spe = gsas.buscarPersona(Integer.parseInt(aval.getNumeroSolicitud()), "E");
            String nit_solicitante = sp.getIdentificacion();
            String nit_codeudor = (spc != null) ? spc.getIdentificacion() : "";
            String nit_estudiante = (spe != null) ? spe.getIdentificacion() : "";
            String nit_afiliado = negocio.getNit_tercero();
    
            if (negocio.getNum_pagare() == null || negocio.getNum_pagare().equals("")) {
                String ultimo_pagare_vigente = ""; //Negociossvc.obtenerUltimoPagareVigenteCliente(negocio.getCod_negocio(), nit_solicitante, nit_codeudor, nit_estudiante, nit_afiliado);
                if(!ultimo_pagare_vigente.equals("")){  
                    swNuevoPagare = false;
                    negocio.setNum_pagare(ultimo_pagare_vigente);
                    Negociossvc.actualizaNumeroPagare(negocio.getCod_negocio(), negocio.getNum_pagare());                   
                }else{              
                    swNuevoPagare = true;
                  //  np = negocio.getCod_negocio().startsWith("MC") ? "MC" : negocio.getCod_negocio().startsWith("LB") ? "LB" : negocio.getCod_negocio().startsWith("FB") ? "FB" : negocio.getCod_negocio().startsWith("FA") ? "FA":"F";
                    np ="PG"; 
                    serie = serieGeneralService.getSerie(usuario.getCia(), np, "PGAV");
                    negocio.setNum_pagare(serie.getUltimo_prefijo_numero());
                    serieGeneralService.setSerie(usuario.getCia(), np, "PGAV"); //Incrementa el numero de ingreso
                    Negociossvc.actualizaNumeroPagare(negocio.getCod_negocio(), negocio.getNum_pagare()); 
                    Negociossvc.insertarHistoricoPagare(negocio, nit_solicitante, nit_codeudor, nit_estudiante, negocio.getNum_pagare(), aval.getNumeroSolicitud(), nit_afiliado, usuario);
                    Negociossvc.insertarNuevoPagare(negocio,aval.getNumeroSolicitud(),usuario);
                }          
            }
    
            boolean persona = aval.getTipoPersona().equalsIgnoreCase("N")?true:false;
          
            try {
               if(imprimirPagare){
                
                bf = BaseFont.createFont("Helvetica", "Cp1252", false);
                rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                ruta = (new StringBuilder()).append(rb.getString("ruta")).append("/pdf/pagarePersonaNatural" + ".pdf").toString();

                Document document = new Document(PageSize.LETTER, 30, 20, 20, 20);
                PdfWriter.getInstance(document, new FileOutputStream(ruta));
                
                document.open();
                
                document.newPage();
                
                Paragraph parrafo = new Paragraph();
                parrafo.setAlignment(Paragraph.ALIGN_JUSTIFIED);
                parrafo.add(new Phrase("PAGAR� No: "+negocio.getNum_pagare(),fuente_negrita2));
                parrafo.add(new Phrase("\nFECHA VENCIMIENTO: _____________________",fuente_negrita2));
                parrafo.add(new Phrase("\nCIUDAD: _____________________\n\n",fuente_negrita2));
                document.add(parrafo);
                //parrafo nombres
                parrafo = new Paragraph();
                parrafo.setAlignment(Paragraph.ALIGN_JUSTIFIED);
                parrafo.add(new Phrase("Yo (Nosotros), ___________________________________________, ________________________________________________, ________________________________________________ y ___________________________________________________ mayor (es) de edad, identificado(s) con c�dula de ciudadan�a No _____________________, _____________________, _____________________ y ____________________ respectivamente, actuando en mi (nuestro) nombre",fuente_normal2));
                if(!persona) {
                    parrafo.add(new Phrase(" y/o a su vez actuando como representante legal de la(s) sociedad(es) denominada(s) __________________________________, identificada con Nit. No. _________________ y __________________________________, identificada con Nit. No. _________________, ",fuente_normal2));
                }
                parrafo.add(new Phrase("manifestamos que por virtud del presente t�tulo valor, pagar� (mos) solidaria e incondicionalmente en dinero efectivo a la orden de ________________________________________________, o a quien represente sus derechos, en su oficina ubicada en la ciudad de _________________ Rep�blica de Colombia el d�a ____ del mes de _______________ del a�o ________ la suma de:",fuente_normal2));
                if(persona) {
                    parrafo.add(new Phrase("___________________________________________________",fuente_normal2));
                } else {
                    parrafo.add(new Phrase("_______________________________________________________",fuente_normal2));
                }
                parrafo.add(new Phrase(" ($___________________________) como saldo insoluto de las obligaciones que mantenemos a su favor.",fuente_normal2));
                document.add(parrafo);
                //parrafo intereses
                parrafo = new Paragraph(11f);
                parrafo.setAlignment(Paragraph.ALIGN_JUSTIFIED);
                parrafo.add(new Phrase("\nIntereses de Mora:",fuente_negrita2));
                parrafo.add(new Phrase(" reconocer� (mos) intereses de mora a la tasa m�xima legalmente permitida. ",fuente_normal2));
                parrafo.add(new Phrase("Impuestos:",fuente_negrita2));
                parrafo.add(new Phrase(" as� mismo pagar� (mos)  los impuestos que cause el presente documento. ",fuente_normal2));
                parrafo.add(new Phrase("Renuncias:",fuente_negrita2));
                parrafo.add(new Phrase(" renuncio (amos) al domicilio, a la presentaci�n del protesto, al aviso de requerimiento judicial o extrajudicial. ",fuente_normal2));
                parrafo.add(new Phrase("Costos y Gastos:",fuente_negrita2));
                parrafo.add(new Phrase(" acepto (amos)  que asumo los gastos y costos que se deriven judicial y extrajudicialmente en las condiciones previstas por la ley y en los siguientes casos: ",fuente_normal2));
                parrafo.add(new Phrase("a.",fuente_negrita2));
                parrafo.add(new Phrase(" Por mora en el pago del capital y/o intereses de cualquier obligaci�n que tenga a favor del mismo tenedor legitimo del t�tulo; ",fuente_normal2));
                parrafo.add(new Phrase("b.",fuente_negrita2));
                parrafo.add(new Phrase(" Si en forma conjunta o separada fu�ramos perseguidos judicialmente por cualquiera persona en ejercicio de cualquier acci�n; ",fuente_normal2));
                parrafo.add(new Phrase("c.",fuente_negrita2));
                parrafo.add(new Phrase(" Por haberse desmeritado las garant�as reales o personales constituidas para asegurar el cumplimiento de las obligaciones contra�das a favor del tenedor leg�timo de este pagar�, de modo que ya no parezcan suficientes para tal efecto, a juicio de dicho tenedor leg�timo; ",fuente_normal2));
                parrafo.add(new Phrase("d.",fuente_negrita2));
                parrafo.add(new Phrase(" Por encontrarme en estado de insolvencia, cesaci�n de pago y/o liquidaci�n; ",fuente_normal2));
                parrafo.add(new Phrase("e.",fuente_negrita2));
                parrafo.add(new Phrase(" Por incurrir en alguna de las conductas tipificadas como lavado de activos o conductas relacionadas con este descritas como delitos en el c�digo penal o en otras disposiciones legales; ",fuente_normal2));
                parrafo.add(new Phrase("f.",fuente_negrita2));
                parrafo.add(new Phrase(" Por honorarios de abogado, costas y agencias en derecho. ",fuente_normal2));
                parrafo.add(new Phrase("Cesi�n:",fuente_negrita2));
                parrafo.add(new Phrase("acepto desde ahora cualquier cesi�n o endoso que del cr�dito y del t�tulo valor respectivamente hiciere a cualquier t�tulo el tenedor leg�timo de este pagar�. ",fuente_normal2));
                document.add(parrafo);
                //parrafo clausura
                parrafo = new Paragraph(11f);
                parrafo.setAlignment(Paragraph.ALIGN_JUSTIFIED);
                parrafo.add(new Phrase("\nCLAUSULA ACELERATORIA:",fuente_negrita2));
                parrafo.add(new Phrase(" El tenedor podr� declarar insubsistente los plazos de esta obligaci�n y exigir el pago inmediato de la totalidad del cr�dito, judicial o extrajudicial en los siguientes casos: ",fuente_normal2));
                parrafo.add(new Phrase("a.",fuente_negrita2));
                parrafo.add(new Phrase(" Por incumplimiento de cualquiera de las obligaciones que adquirimos por el presente pagare; ",fuente_normal2));
                parrafo.add(new Phrase("b.",fuente_negrita2));
                parrafo.add(new Phrase(" la mora o el simple retardo en el pago de las cuotas pactadas; ",fuente_normal2));
                parrafo.add(new Phrase("c.",fuente_negrita2));
                parrafo.add(new Phrase(" Si los bienes del deudor son perseguidos por cualquier persona en ejercicio de cualquier acci�n; ",fuente_normal2));
                parrafo.add(new Phrase("d.",fuente_negrita2));
                parrafo.add(new Phrase(" Si a juicio de el tenedor leg�timo de este pagar� se observa variaci�n en cualquiera de mi (nuestra) situaci�n financiera, jur�dica, econ�mica o en esquema de propiedad o administraci�n con respecto a aquellas sobre las cuales fueron aprobados los cr�ditos, de manera que se ponga en peligro el pago oportuno de las obligaciones a mi (nuestro) cargo.",fuente_normal2));
                parrafo.add(new Phrase("De conformidad con lo establecido en el ",fuente_normal2));
                parrafo.add(new Phrase("art�culo 622 del c�digo de comercio",fuente_negrita2));
                parrafo.add(new Phrase(", los autorizo (amos) expresa e irrevocablemente para llenar el presente t�tulo valor en los espacios en blanco correspondientes.",fuente_normal2));
                parrafo.add(new Phrase("En constancia se suscribe en la ciudad de ______________________ a los ____ d�as del mes de ______________ de 20____.",fuente_normal2));
                document.add(parrafo);
                
                document.add(this.huellas(persona));
                document.add(this.huellas(persona));
                
                /*******************************nueva pagina******************************/
                document.newPage();
                
                parrafo = new Paragraph();
                parrafo.setAlignment(Paragraph.ALIGN_CENTER);
                parrafo.add(new Phrase("CARTA DE INSTRUCCIONES ANEXA AL PAGARE CON ESPACIOS EN BLANCO\n",fuente_negrita2));
                document.add(parrafo);
                
                parrafo = new Paragraph();
                parrafo.setAlignment(Paragraph.ALIGN_JUSTIFIED);
                parrafo.add(new Phrase("PAGAR� No: "+negocio.getNum_pagare(),fuente_negrita2));
                parrafo.add(new Phrase("\nFECHA VENCIMIENTO: _____________________",fuente_negrita2));
                parrafo.add(new Phrase("\nCIUDAD: _____________________\n\n",fuente_negrita2));
                document.add(parrafo);
                
                //parrafo nombres
                parrafo = new Paragraph();
                parrafo.setAlignment(Paragraph.ALIGN_JUSTIFIED);
                parrafo.add(new Phrase("Yo (Nosotros) ___________________________________________, ________________________________________________, ________________________________________________ y ___________________________________________________ mayor (es) de edad, identificado(s) con cedula de ciudadan�a No _____________________, _____________________, _____________________ y ____________________ respectivamente, por medio del presente documento autorizo (amos) a ____________________________________ o al tenedor leg�timo de este instrumento, cuando lo estimen conveniente y sin necesidad de aviso o requerimiento previo y haciendo uso de las facultades conferidas en el ",fuente_normal2));
                parrafo.add(new Phrase("art�culo 622 del c�digo de comercio",fuente_negrita2));
                parrafo.add(new Phrase(", llene los espacios que se han dejado en blanco en el ",fuente_normal2));
                parrafo.add(new Phrase("pagar� No. "+negocio.getNum_pagare(),fuente_negrita2));
                parrafo.add(new Phrase(", de conformidad con las siguientes instrucciones:",fuente_normal2));
                document.add(parrafo);
                //parrafo instrucciones
                parrafo = new Paragraph(11f);
                parrafo.setAlignment(Paragraph.ALIGN_JUSTIFIED);
                parrafo.add(new Paragraph("\n1. La cuant�a del t�tulo valor ser� igual a la suma de todos las obligaciones exigibles a mi (nuestro)  cargo que existan al momento de llenar los espacios en blanco que por capital, intereses,  costos legales para el cobro de dicho instrumento y por cualquier otro concepto se refleje en sus libros de contabilidad, as� como tambi�n, el valor que conste en cualquier t�tulo, letra de cambio, cheque, factura cambiar�a por nosotros suscrita, sean tales valores acumulados o separados y siempre incluyen capital e intereses corrientes.",fuente_normal2));
                parrafo.add(new Paragraph("2. La fecha del vencimiento ser� la misma en que sea llenado por ustedes en el documento, siendo exigible de manera inmediata las obligaciones en el contenidas a mi (nuestro) cargo sin necesidad de requerimiento judicial o extrajudicial para su cumplimiento, la ciudad ser� aquella en la cual se haya otorgado la obligaci�n respaldada con el pagare, el tenedor leg�timo de este pagare podr� dar por vencido anticipadamente el plazo pendiente para el pago de la obligaci�n y proceder al cobro total del capital pendiente de pago con sus correspondientes intereses y dem�s accesorios cuando haya incumplimiento por parte m�a (nuestra), o por la mora o simple retardo en el pago de las cuotas pactadas.",fuente_normal2));
                parrafo.add(new Paragraph("3. La tasa de inter�s de mora ser� la m�xima permitida por las disposiciones vigentes decretadas mediante resoluci�n de la superintendencia bancaria el d�a en que se diligencia el t�tulo. ",fuente_normal2));
                parrafo.add(new Paragraph("4. Tambi�n podr� hacerse exigible el titulo cuando por haberse desmeritado las garant�as reales o personales constituidas para asegurar el cumplimiento de las obligaciones contra�das a favor del tenedor leg�timo de este pagare de modo que ya parezcan suficientes para tal efecto a juicio de dicho tenedor leg�timo.",fuente_normal2));
                parrafo.add(new Paragraph("5. como complemento de las anteriores instrucciones ustedes podr�n llenar los espacios en blanco del pagare tambi�n por el valor del saldo de nuestra deuda por todos los conceptos cuando tengan conocimiento de que se est� tramitando concordato preventivo u obligatorio con nuestros acreedores o que me(nos) declare (mos) en estado de quiebra por que contra m� (nosotros) se adelante cualquier proceso ejecutivo u otro procedimiento similar, o cuando se compruebe mi(nuestra) eventual insolvencia o la venta de mi(nuestro) activos.",fuente_normal2));
                parrafo.add(new Paragraph("6. El t�tulo as� llenado presta merito ejecutivo, pudiendo ustedes o cualquiera otro legitimo tenedor exigir su pago o cancelaci�n por v�a judicial, sin perjuicio de las dem�s acciones.",fuente_normal2));
                parrafo.add(new Paragraph("\nEsta carta de instrucciones hace parte integral del documento contenido en el pagare No "+negocio.getNum_pagare()+" y la extendemos de conformidad con lo dispuesto en el art�culo 622 del c�digo de comercio.",fuente_normal2));
                parrafo.add(new Paragraph("\nEn constancia se suscribe en la ciudad de ______________________ a los ____ d�as del mes de ______________ de 20____.",fuente_normal2));
                document.add(parrafo);
                
                document.add(this.huellas(persona));
                document.add(this.huellas(persona));
                
                document.newPage();
                document.close();
                
               }
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("ERROR en generarPdfPagareVacio" + e.toString() + "__" + e.getMessage());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("error en servicillo__" + ex.toString() + "__" + ex.getMessage());
        }
        return swNuevoPagare;
    }
    
    private PdfPTable huellas (boolean tipo_persona) throws DocumentException {
        Font fuente_negrita = new Font(bf, 7);
        fuente_negrita.setStyle(com.lowagie.text.Font.BOLD);
        String nit = ((tipo_persona)?"\n\n\n":"\n\nNIT: _________________________________________")+"\n\n";
        
        PdfPTable temp = new PdfPTable(4);
        temp.setWidthPercentage(95);
        temp.setWidths(new float[]{35, 10, 35, 10});
        
        PdfPCell celda = new PdfPCell();
        celda.setBorderColor(Color.WHITE);
        celda.setColspan(4);
        celda.setPhrase(new Phrase("\n",fuente_negrita));
        temp.addCell(celda);
        
        celda = new PdfPCell();
        celda.setBorderColor(Color.WHITE);
        Phrase frase = new Phrase();
        frase.add(new Phrase("\n\nFirma Deudor: ________________________________",fuente_negrita));
        frase.add(new Phrase("\n\nNombre Completo: _____________________________",fuente_negrita));
        frase.add(new Phrase("\n\nCC No.: ______________________________________",fuente_negrita));
        frase.add(new Phrase(nit, fuente_negrita));
        celda.setPhrase(frase);
        temp.addCell(celda);
        
        PdfPTable huella = new PdfPTable(1);
        huella.setWidthPercentage(95);
        huella.addCell(new PdfPCell());
        celda = new PdfPCell(huella);
        celda.setPadding(5);
        celda.setBorderColor(Color.WHITE);
        temp.addCell(celda);
        
        celda = new PdfPCell();
        celda.setPaddingLeft(5);
        celda.setBorderColor(Color.WHITE);
        frase = new Phrase();
        frase.add(new Phrase("\nFirma Deudor\nSolidario:_____________________________________",fuente_negrita));
        frase.add(new Phrase("\n\nNombre Completo: _____________________________",fuente_negrita));
        frase.add(new Phrase("\n\nCC No.: ______________________________________",fuente_negrita));
        frase.add(new Phrase(nit, fuente_negrita));
        celda.setPhrase(frase);
        temp.addCell(celda);
        
        huella = new PdfPTable(1);
        huella.setWidthPercentage(95);
        huella.addCell(new PdfPCell());
        celda = new PdfPCell(huella);
        celda.setPadding(5);
        celda.setBorderColor(Color.WHITE);
        temp.addCell(celda);
        
        if(!tipo_persona) {
            celda = new PdfPCell();
            celda.setColspan(2);
            celda.setBorderColor(Color.WHITE);
            frase = new Phrase();
            frase.add(new Phrase("\nRAZ�N y/o DENOMINACI�N SOCIAL: ______________________________",fuente_negrita));
            celda.setPhrase(frase);
            temp.addCell(celda);
            
            celda = new PdfPCell();
            celda.setColspan(2);
            celda.setPaddingLeft(5);
            celda.setBorderColor(Color.WHITE);
            frase = new Phrase();
            frase.add(new Phrase("\nRAZ�N y/o DENOMINACI�N SOCIAL: ______________________________",fuente_negrita));
            celda.setPhrase(frase);
            temp.addCell(celda);
        }
        
        return temp;
    }
    
 /************************************** DATOS TABLA 1 ***************************************************/
    protected PdfPTable tabla1( SolicitudAval aval) throws DocumentException, SQLException {



        PdfPTable tabla_temp = new PdfPTable(1);
       
        java.awt.Color color_fuente_header = new java.awt.Color(0xAA, 0xAA, 0xAA);

        Font fuente_header = new Font(Font.TIMES_ROMAN, 7, Font.BOLD);
            fuente_header.setColor(color_fuente_header);
            
        Font fuente = new Font(Font.TIMES_ROMAN, 6);
        Font fuenteB = new Font(Font.TIMES_ROMAN, 6, Font.BOLD);

        float[] medidaCeldas = {0.1000f};
        tabla_temp.setWidths(medidaCeldas);

        PdfPCell celda_temp = new PdfPCell();

        celda_temp = new PdfPCell();
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        if (aval.getTipoPersona().equalsIgnoreCase("N")) {    
        celda_temp.setPhrase(new Phrase("\n    __________________________________\n    Firma Deudor\n    Nombre Completo:___________________________\n    CC No.:___________________________ \n\n\n ", fuente));
        }
        if (aval.getTipoPersona().equalsIgnoreCase("J")) {    
        celda_temp.setPhrase(new Phrase("\n    ______________________________________\n    Firma Representante Legal\n    Nombre Empresa:_______________________\n    Nit No.:_______________________________\n    C.C. de Rep. Legal:______________________\n\n\n ", fuente));
        }
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
            
        return tabla_temp;
    }

    
    /************************************** DATOS TABLA HUELLA 1 ***************************************************/
    protected PdfPTable tablaHuella1() throws DocumentException, SQLException {



        PdfPTable tabla_temp = new PdfPTable(1);
       
        java.awt.Color color_fuente_header = new java.awt.Color(0xAA, 0xAA, 0xAA);

        Font fuente_header = new Font(Font.TIMES_ROMAN, 7, Font.BOLD);
            fuente_header.setColor(color_fuente_header);

        float[] medidaCeldas = {0.1000f};
        tabla_temp.setWidths(medidaCeldas);

        PdfPCell celda_temp = new PdfPCell();

        celda_temp = new PdfPCell();
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0.5f);
        celda_temp.setBorderWidthRight(0.5f);
        celda_temp.setBorderWidthBottom(0.5f); 
        celda_temp.setPhrase(new Phrase("\n\n\n\n\n\n\n", fuente_header));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(50);
            
        return tabla_temp;
    }
    
    
    /************************************** DATOS TABLA 2 ***************************************************/
    protected PdfPTable tabla2() throws DocumentException, SQLException {



        PdfPTable tabla_temp = new PdfPTable(1);
       
        java.awt.Color color_fuente_header = new java.awt.Color(0xAA, 0xAA, 0xAA);

        Font fuente_header = new Font(Font.TIMES_ROMAN, 7, Font.BOLD);
            fuente_header.setColor(color_fuente_header);
            
        Font fuente = new Font(Font.TIMES_ROMAN, 6);
        Font fuenteB = new Font(Font.TIMES_ROMAN, 6, Font.BOLD);

        float[] medidaCeldas = {0.1000f};
        tabla_temp.setWidths(medidaCeldas);

        PdfPCell celda_temp = new PdfPCell();

        celda_temp = new PdfPCell();
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setPhrase(new Phrase("\n_________________________________\nFirma Del Deudor Solidario\nNombre Completo:__________________________\nCC No.:___________________________ \n\n\n ", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
            
        return tabla_temp;
    }
       
 /************************************** DATOS TABLA HUELLA 2 ***************************************************/
    protected PdfPTable tablaHuella2() throws DocumentException, SQLException {



        PdfPTable tabla_temp = new PdfPTable(1);
       
        java.awt.Color color_fuente_header = new java.awt.Color(0xAA, 0xAA, 0xAA);

        Font fuente_header = new Font(Font.TIMES_ROMAN, 7, Font.BOLD);
            fuente_header.setColor(color_fuente_header);

        float[] medidaCeldas = {0.1000f};
        tabla_temp.setWidths(medidaCeldas);

        PdfPCell celda_temp = new PdfPCell();

        celda_temp = new PdfPCell();
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0.5f);
        celda_temp.setBorderWidthRight(0.5f);
        celda_temp.setBorderWidthBottom(0.5f); 
        celda_temp.setPhrase(new Phrase("\n\n\n\n\n\n\n", fuente_header));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(50);
            
        return tabla_temp;
    }

   
}
