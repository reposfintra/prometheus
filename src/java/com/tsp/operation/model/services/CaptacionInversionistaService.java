package com.tsp.operation.model.services;

import com.itextpdf.text.pdf.RandomAccessFileOrArray;
import com.itextpdf.text.pdf.codec.TiffImage;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;
import com.sun.imageio.plugins.jpeg.JPEGImageWriter;
import com.tsp.operation.model.*;
import com.tsp.operation.model.DAOS.CaptacionInversionistaDAO;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;
import com.tsp.util.Util;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageOutputStream;
import javax.servlet.*;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPosition;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.CategoryLabelWidthType;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.DefaultPieDataset;
import org.jfree.data.PieDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.text.TextBlockAnchor;
import org.jfree.ui.RectangleAnchor;

/**
 * Service para el nuevo programa de captaciones 24/02/2012
 *
 * @author darrieta
 */
public class CaptacionInversionistaService extends PdfPageEventHelper {

    CaptacionInversionistaDAO dao;
    
    public CaptacionInversionistaService() {
        dao = new CaptacionInversionistaDAO();
    }
    public CaptacionInversionistaService(String dataBaseName) {
        dao = new CaptacionInversionistaDAO(dataBaseName);
    }
    

    double total_retiros = 0;
    double total_consignaciones = 0;
    double total_intereses = 0;
    double total_retenciones = 0;
    double saldo_inicial = 0;
    double saldo_final = 0;
    ExcelApplication excel;
    int fila = 0;

    public NitSot buscarNit(String id) throws Exception {
        return dao.buscarNit(id);
    }

    /**
     * Inserta un registro en la tabla inversionista
     *
     * @param inversionista
     * @return sql generado
     * @throws Exception
     */
    public String insertarInversionista(Inversionista inversionista) throws Exception {
        return dao.insertarInversionista(inversionista);
    }

    /**
     * Obtiene las subcuentas de un inversionista
     *
     * @param nit identificacion del inversionista
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<Inversionista> consultarSubcuentas(String nit, String dstrct) throws Exception {
        return dao.consultarSubcuentas(nit, dstrct);
    }

    /**
     * Obtiene una subcuenta especifica de un inversionista
     *
     * @param nit identificacion del inversionista
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public Inversionista consultarSubcuentaInversionista(String dstrct, String nit, int subcuenta) throws Exception {
        return dao.consultarSubcuentaInversionista(dstrct, nit, subcuenta);
    }

    /**
     * Obtiene una subcuenta especifica de un inversionista
     *
     * @param nit identificacion del inversionista
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> consultarUltimosMovimientos(String dstrct, String nit, String subcuenta, String mes) throws Exception {
        return dao.consultarUltimosMovimientos(dstrct, nit, subcuenta, mes);
    }

    /**
     * Obtiene una subcuenta especifica de un inversionista
     *
     * @param nit identificacion del inversionista
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> consultarRetirosXconfirmar(String dstrct) throws Exception {
        return dao.consultarRetirosXconfirmar(dstrct);
    }

    /**
     * Obtiene lista de retiros por transferir
     *
     * @param nit identificacion del inversionista
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> consultarRetirosXtransferir(String dstrct) throws Exception {
        return dao.consultarRetirosXtransferir(dstrct);
    }

    /**
     * Obtiene una subcuenta especifica de un inversionista
     *
     * @param nit identificacion del inversionista
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> consultarRetirosXaprobar(String dstrct) throws Exception {
        return dao.consultarRetirosXaprobar(dstrct);
    }

    /**
     * Obtiene el maximo numero de subcuentas de un inversionista
     *
     * @param nit identificacion del inversionista
     * @param dstrct distrito del usuario en sesion
     * @return maximo numero de subcuentas del inversionista
     * @throws Exception
     */
    public int consultarMaxSubcuenta(String nit, String dstrct) throws Exception {
        return dao.consultarMaxSubcuenta(nit, dstrct);
    }

    /**
     * Edita un registro en la tabla inversionista
     *
     * @param inversionista
     * @return sql generado
     * @throws Exception
     */
    public String editarInversionista(Inversionista inversionista) throws Exception {
        return dao.editarInversionista(inversionista);
    }

    /**
     * Obtiene los inversionistas activos
     *
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<Inversionista> consultarInversionistas(String dstrct) throws Exception {
        return dao.consultarInversionistas(dstrct);
    }

    public String guardarDatosLiquidacion(Inversionista inversionista) throws Exception {
        return dao.guardarDatosLiquidacion(inversionista);
    }

    public String updateNit(NitSot nit) throws Exception {
        return dao.updateNit(nit);
    }

    public String guardarMovimiento(MovimientosCaptaciones movimiento) throws Exception {
        return dao.guardarMovimiento(movimiento);
    }

    /**
     * consulta movimientos para recalcular interes apartir de un movimiento
     * dado en adelante
     *
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> getMovimientosRecalcular(MovimientosCaptaciones movimiento) throws Exception {
        return dao.getMovimientosRecalcular(movimiento);
    }

    /**
     * consulta movimientos para recalcular interes apartir de un movimiento
     * dado en adelante
     *
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> getMovimientosRecalcularRetiros(MovimientosCaptaciones movimiento) throws Exception {
        return dao.getMovimientosRecalcularRetiros(movimiento);
    }

    /**
     * consulta movimientos para recalcular interes apartir de un movimiento
     * dado en adelante
     *
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public String updateMovimientos(ArrayList<MovimientosCaptaciones> lista_movimientos) throws Exception {
        String sql = "";
        TransaccionService tService = new TransaccionService(this.dao.getDatabaseName());
        tService.crearStatement();
        for (int i = 1; i < lista_movimientos.size(); i++) {
            // sql=sql+dao.updateMovimientos(lista_movimientos.get(i));
            tService.getSt().addBatch(dao.updateMovimientos(lista_movimientos.get(i)));
        }

        tService.execute();
        return sql;
    }

    /**
     * consulta movimientos para recalcular interes apartir de un movimiento
     * dado en adelante
     *
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> RecalcularMovimientos(Inversionista subcuenta, ArrayList<MovimientosCaptaciones> lista_movimientos) throws Exception {

        double subtotal = 0;
        double saldo_final = 0;
        for (int i = 1; i < lista_movimientos.size(); i++) {
            if (subcuenta.getTipo_interes().equals("C")) {
                lista_movimientos.get(i).setSaldo_inicial(lista_movimientos.get(i - 1).getSaldo_final());
                lista_movimientos.get(i).setValor_intereses(this.getIntereses(subcuenta, lista_movimientos.get(i - 1), lista_movimientos.get(i)));

                //si es retiro total se calcula las retenciones
                if (lista_movimientos.get(i).getTipo_movimiento().equals("RT")) {
                    lista_movimientos.get(i).setValor_retefuente(this.getRetefuente(subcuenta, lista_movimientos));
                    lista_movimientos.get(i).setValor_reteica(this.getReteica(subcuenta, lista_movimientos));
                }
                subtotal = (lista_movimientos.get(i).getSaldo_inicial() + lista_movimientos.get(i).getValor_intereses() - lista_movimientos.get(i).getValor_retefuente() - lista_movimientos.get(i).getValor_reteica());
                saldo_final = (subtotal + lista_movimientos.get(i).getConsignacion() - lista_movimientos.get(i).getRetiro());
                lista_movimientos.get(i).setSubtotal(subtotal);
                lista_movimientos.get(i).setSaldo_final(saldo_final);
            } else {
                if (subcuenta.getTipo_interes().equals("S")) {
                    //base intereses
                    if (lista_movimientos.get(i - 1).getRetiro() == 0) {
                        lista_movimientos.get(i).setBase_intereses(lista_movimientos.get(i - 1).getSaldo_final() - lista_movimientos.get(i - 1).getIntereses_acomulados());
                    } else {
                        lista_movimientos.get(i).setBase_intereses(lista_movimientos.get(i - 1).getSaldo_final());
                    }

                    //se calculan intereses
                    lista_movimientos.get(i).setValor_intereses(this.getIntereses(subcuenta, lista_movimientos.get(i - 1), lista_movimientos.get(i)));
                    //

                    //si es retiro total se calcula las retenciones
                    if (lista_movimientos.get(i).getTipo_movimiento().equals("RT")) {
                        lista_movimientos.get(i).setValor_retefuente(this.getRetefuente(subcuenta, lista_movimientos));
                        lista_movimientos.get(i).setValor_reteica(this.getReteica(subcuenta, lista_movimientos));
                    }

                    if (lista_movimientos.get(i - 1).getRetiro() >= lista_movimientos.get(i - 1).getIntereses_acomulados()) {///calcular intereses acomulados
                        lista_movimientos.get(i).setIntereses_acomulados(lista_movimientos.get(i).getValor_intereses());
                    } else {
                        lista_movimientos.get(i).setIntereses_acomulados(lista_movimientos.get(i - 1).getIntereses_acomulados() + lista_movimientos.get(i).getValor_intereses());
                    }

                    lista_movimientos.get(i).setSaldo_final(lista_movimientos.get(i).getBase_intereses() + lista_movimientos.get(i).getIntereses_acomulados() - lista_movimientos.get(i).getValor_retefuente()
                            + lista_movimientos.get(i).getValor_reteica() + lista_movimientos.get(i).getConsignacion() - lista_movimientos.get(i).getRetiro());

                }
            }

        }

        return lista_movimientos;
    }

    /**
     * seta el movimiento final de cierre
     *
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public MovimientosCaptaciones CalcularMovimientoCierre(Inversionista subcuenta, MovimientosCaptaciones movimiento) throws Exception {

        double subtotal = 0;
        double saldo_final = 0;

        movimiento.setValor_retefuente(this.getRetefuenteCierre(subcuenta, movimiento));//se pasa por parametro la lista de intereses ose se calcula en el cquery inicial
        movimiento.setValor_reteica(this.getReteicaCierre(subcuenta, movimiento));

        if (subcuenta.getTipo_interes().equals("C")) {
             //  movimiento.setSaldo_inicial(lista_movimientos.get(i - 1).getSaldo_final());
            //  movimiento.get(i).setValor_intereses(this.getIntereses(subcuenta, lista_movimientos.get(i - 1), lista_movimientos.get(i))); ya se calculo

                //si es retiro total se calcula las retenciones
            subtotal = (movimiento.getSaldo_inicial() + movimiento.getValor_intereses() - movimiento.getValor_retefuente() - movimiento.getValor_reteica());
            saldo_final = (subtotal);
            movimiento.setSubtotal(subtotal);
            movimiento.setSaldo_final(saldo_final);
        } else {
            if (subcuenta.getTipo_interes().equals("S")) {
                //base intereses
                if (movimiento.getRetiro() == 0) {
                    movimiento.setBase_intereses(movimiento.getSaldo_final() - movimiento.getIntereses_acomulados());
                } else {
                    movimiento.setBase_intereses(movimiento.getSaldo_final());
                }

                if (movimiento.getRetiro() >= movimiento.getIntereses_acomulados()) {///calcular intereses acomulados
                    movimiento.setIntereses_acomulados(movimiento.getValor_intereses());
                } else {
                    movimiento.setIntereses_acomulados(movimiento.getIntereses_acomulados() + movimiento.getValor_intereses());
                }

                movimiento.setSaldo_final(movimiento.getBase_intereses() + movimiento.getIntereses_acomulados() - movimiento.getValor_retefuente()
                        + movimiento.getValor_reteica() + movimiento.getConsignacion() - movimiento.getRetiro());

            }
        }

        return movimiento;
    }

    /**
     * consulta movimientos para recalcular interes apartir de un movimiento
     * dado en adelante
     *
     * @param dstrct distrito del usuario en sesion
     * @return double con los resultados obtenidos
     * @throws Exception
     */
    public double getIntereses(Inversionista subcuenta, MovimientosCaptaciones movimiento_anterior, MovimientosCaptaciones movimiento) {
        Date fechaInicial = Util.ConvertiraDate1(movimiento_anterior.getFecha());//movimiento anterior
        Date fechaFinal = Util.ConvertiraDate1(movimiento.getFecha());//movimiento actual

        double intereses = 0;
        long numDias = 0;

        if (subcuenta.getDias_calendario() == 365)//validacion base dias  intereses
        {
            long diff = fechaFinal.getTime() - fechaInicial.getTime();
            numDias = diff / (1000 * 60 * 60 * 24);
        } else {
            numDias = Util.dias360(fechaInicial, fechaFinal);
        }
        if (subcuenta.getTipo_interes().equals("C")) {

            double exponente = ((double) numDias / subcuenta.getAno_base());
            double base = (1 + (subcuenta.getTasa_ea() / 100));
            intereses = movimiento.getSaldo_inicial() * (Math.pow(base, exponente)) - movimiento.getSaldo_inicial();
        } else {
            if (subcuenta.getTipo_interes().equals("S")) {
                double tasa_diaria = (subcuenta.getTasa_diaria() / 100);
                intereses = movimiento.getBase_intereses() * numDias * tasa_diaria;
            }

        }
        return intereses;
    }

    /**
     * consulta movimientos para recalcular interes apartir de un movimiento
     * dado en adelante
     *
     * @param dstrct distrito del usuario en sesion
     * @return double con los resultados obtenidos
     * @throws Exception
     */
    public double getInteresesCausacion(Inversionista subcuenta, MovimientosCaptaciones movimiento) {
        Date fechaInicial = Util.ConvertiraDate1(movimiento.getFecha());//movimiento anterior
        Date fechaFinal = Util.ConvertiraDate1(movimiento.getFecha_causacion());//fecha causacion fecha_causaccion

        double intereses = 0;
        long numDias = 0;

        if (subcuenta.getDias_calendario() == 365)//validacion base dias  intereses
        {
            long diff = fechaFinal.getTime() - fechaInicial.getTime();
            numDias = diff / (1000 * 60 * 60 * 24);
        } else {
            numDias = Util.dias360(fechaInicial, fechaFinal);
        }
        if (subcuenta.getTipo_interes().equals("C")) {

            double exponente = ((double) numDias / subcuenta.getAno_base());
            double base = (1 + (subcuenta.getTasa_ea() / 100));
            intereses = movimiento.getSaldo_inicial() * (Math.pow(base, exponente)) - movimiento.getSaldo_inicial();
        } else {
            if (subcuenta.getTipo_interes().equals("S")) {
                double tasa_diaria = (subcuenta.getTasa_diaria() / 100);

                //base intereses
                if (movimiento.getRetiro() == 0) {
                    movimiento.setBase_intereses(movimiento.getSaldo_inicial() - movimiento.getIntereses_acomulados());
                } else {
                    movimiento.setBase_intereses(movimiento.getSaldo_inicial());
                }

                intereses = movimiento.getBase_intereses() * numDias * tasa_diaria;
            }

        }
        return intereses;
    }

    /**
     * calcula el total de retefuente hasta la fecha
     *
     * @param dstrct distrito del usuario en sesion
     * @return double con los resultados obtenidos
     * @throws Exception
     */
    public double getRetefuente(Inversionista inversionista, ArrayList<MovimientosCaptaciones> lista_movimientos) throws SQLException {
        Tipo_impuestoService TimpuestoSvc = new Tipo_impuestoService(this.dao.getDatabaseName());
        Tipo_impuesto impuestos_rfte = TimpuestoSvc.buscarImpuestoxCodigo(inversionista.getDstrct(), inversionista.getRetefuente());

        double retfuente = 0;
        double intereses = 0;
        for (int i = 0; i < lista_movimientos.size(); i++) {//
            intereses = intereses + lista_movimientos.get(i).getValor_intereses();
        }
        retfuente = intereses * (Math.abs(impuestos_rfte.getPorcentaje1()) / 100);
        return retfuente;
    }

    /**
     * calcula el total de retefuente hasta la fecha
     *
     * @param dstrct distrito del usuario en sesion
     * @return double con los resultados obtenidos
     * @throws Exception
     */
    public double getRetefuenteCierre(Inversionista inversionista, MovimientosCaptaciones movimiento) throws SQLException {
        Tipo_impuestoService TimpuestoSvc = new Tipo_impuestoService(this.dao.getDatabaseName());
        Tipo_impuesto impuestos_rfte = TimpuestoSvc.buscarImpuestoxCodigo(movimiento.getDstrct(), inversionista.getRetefuente());
        double retfuente = 0;
        double intereses = 0;
        retfuente = (movimiento.getIntereses_acomulados() + movimiento.getValor_intereses()) * (Math.abs(impuestos_rfte.getPorcentaje1()) / 100);
        return retfuente;
    }

    public double getRetefuenteRI(Inversionista inversionista, MovimientosCaptaciones movimiento) throws SQLException {
        Tipo_impuestoService TimpuestoSvc = new Tipo_impuestoService(this.dao.getDatabaseName());
        Tipo_impuesto impuestos_rfte = TimpuestoSvc.buscarImpuestoxCodigo(movimiento.getDstrct(), inversionista.getRetefuente());
        double retfuente = 0;
        double intereses = 0;
        retfuente = (movimiento.getValor_intereses()) * (Math.abs(impuestos_rfte.getPorcentaje1()) / 100);
        return retfuente;
    }

    /**
     * calcula el total de retefuente hasta la fecha
     *
     * @param dstrct distrito del usuario en sesion
     * @return double con los resultados obtenidos
     * @throws Exception
     */
    public double getReteicaRI(Inversionista inversionista, MovimientosCaptaciones movimiento) throws SQLException {
        Tipo_impuestoService TimpuestoSvc = new Tipo_impuestoService(this.dao.getDatabaseName());
        Tipo_impuesto impuestos_rica = TimpuestoSvc.buscarImpuestoxCodigo(movimiento.getDstrct(), inversionista.getReteica());

        double retfuente = 0;
        double intereses = 0;
        retfuente = (movimiento.getValor_intereses()) * (Math.abs(impuestos_rica.getPorcentaje1()) / 100);
        return retfuente;
    }

    /**
     * calcula el total de retefuente hasta la fecha
     *
     * @param dstrct distrito del usuario en sesion
     * @return double con los resultados obtenidos
     * @throws Exception
     */
    public double getReteicaCierre(Inversionista inversionista, MovimientosCaptaciones movimiento) throws SQLException {
        Tipo_impuestoService TimpuestoSvc = new Tipo_impuestoService(this.dao.getDatabaseName());
        Tipo_impuesto impuestos_rica = TimpuestoSvc.buscarImpuestoxCodigo(movimiento.getDstrct(), inversionista.getReteica());

        double retfuente = 0;
        double intereses = 0;
        retfuente = (movimiento.getIntereses_acomulados() + movimiento.getValor_intereses()) * (Math.abs(impuestos_rica.getPorcentaje1()) / 100);
        return retfuente;
    }

    /**
     * calcula el total de retefuente hasta la fecha
     *
     * @param dstrct distrito del usuario en sesion
     * @return double con los resultados obtenidos
     * @throws Exception
     */
    public double getTotalInteres(ArrayList<MovimientosCaptaciones> lista_movimientos) throws SQLException {
        double intereses = 0;
        for (int i = 0; i < lista_movimientos.size(); i++) {//
            intereses = intereses + lista_movimientos.get(i).getValor_intereses();
        }

        return intereses;
    }

    /**
     * calcula el total de reteica hasta la fecha
     *
     * @param dstrct distrito del usuario en sesion
     * @return double con los resultados obtenidos
     * @throws Exception
     */
    public double getReteica(Inversionista inversionista, ArrayList<MovimientosCaptaciones> lista_movimientos) throws SQLException {
        Tipo_impuestoService TimpuestoSvc = new Tipo_impuestoService(this.dao.getDatabaseName());
        Tipo_impuesto impuestos_rica = TimpuestoSvc.buscarImpuestoxCodigo(inversionista.getDstrct(), inversionista.getReteica());
        double reteica = 0;
        double intereses = 0;
        for (int i = 0; i < lista_movimientos.size(); i++) {
            intereses = intereses + lista_movimientos.get(i).getValor_intereses();
        }
        reteica = intereses * (Math.abs(impuestos_rica.getPorcentaje1()) / 100);
        return reteica;
    }

    /* calcula el total de reteica hasta la fecha
     * @param dstrct distrito del usuario en sesion
     * @return double con los resultados obtenidos
     * @throws Exception
     */
    public double getValorMaxRetirototal(Inversionista inversionista, MovimientosCaptaciones movimiento_final) throws SQLException, Exception {
        double vt = 0;
        double interes_actual = 0;
        double saldo_actual = 0;
        int n = 0;
        ArrayList<MovimientosCaptaciones> lista_movimientos = new ArrayList<MovimientosCaptaciones>();
        lista_movimientos = dao.consultarMovimientosPosCausacion(inversionista.getDstrct(), inversionista.getNit(), String.valueOf(inversionista.getSubcuenta()));

        if (lista_movimientos.size() == 0) {
            lista_movimientos = dao.consultarMovimientosIniciales(inversionista.getDstrct(), inversionista.getNit(), String.valueOf(inversionista.getSubcuenta()));
        }

        lista_movimientos.add(movimiento_final);
        if (lista_movimientos.size() > 0) {
            n = lista_movimientos.size();
        }
        double rtft = this.getRetefuente(inversionista, lista_movimientos);
        double rica = this.getReteica(inversionista, lista_movimientos);
        saldo_actual = (lista_movimientos.get(n - 1).getSaldo_inicial());
        interes_actual = (lista_movimientos.get(n - 1).getValor_intereses());
        vt = (saldo_actual + interes_actual) - rtft - rica;
        return vt;

    }

    /**
     * consulta movimientos para recalcular interes apartir de un movimiento
     * dado en adelante
     *
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> getRetirosChequeados(ArrayList<MovimientosCaptaciones> lista_movimientos, String[] registros) throws Exception {
        ArrayList<MovimientosCaptaciones> lista_retiros_chequeados = new ArrayList<MovimientosCaptaciones>();

        for (int i = 0; i < registros.length; i++) {
            for (int j = 0; j < lista_movimientos.size(); j++) {
                String reg = registros[i];
                if (lista_movimientos.get(j).getNo_transaccion().equals(reg)) {
                    lista_retiros_chequeados.add(lista_movimientos.get(j));
                }
            }
        }

        return lista_retiros_chequeados;
    }

    public String confirmarRetiro(MovimientosCaptaciones movimiento) throws Exception {
        return dao.confirmarRetiro(movimiento);
    }

    public String actualizaFechaRetiro(MovimientosCaptaciones movimiento) throws Exception {
        return dao.actualizaFechaRetiro(movimiento);
    }

    public String aprobarCxp_retiro(String usuario, String dstrct, String tipo_doc, String documento, String nit) throws Exception {
        return dao.aprobarCxp_retiro(usuario, dstrct, tipo_doc, documento, nit);
    }

    /**
     * consulta movimientos para recalcular interes apartir de un movimiento
     * dado en adelante
     *
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public MovimientosCaptaciones getRetiroSession(ArrayList<MovimientosCaptaciones> lista_movimientos, String retiro) throws Exception {
        MovimientosCaptaciones movimiento = new MovimientosCaptaciones();
        for (int j = 0; j < lista_movimientos.size(); j++) {

            if (lista_movimientos.get(j).getNo_transaccion().equals(retiro)) {
                movimiento = lista_movimientos.get(j);
            }
        }
        return movimiento;
    }

    /**
     * Obtiene una lista de todos lso movimientops con las distinctas cuentas
     * utilizadas
     *
     * @param nit identificacion del inversionista
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<MovimientosCaptaciones> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> consultarCuentasTransferencias(String dstrct, String nit, String tipo) throws Exception {
        return dao.consultarCuentasTransferencias(dstrct, nit, tipo);
    }

    /**
     * Obtiene una lista de todos lso movimientops con las distinctas cuentas
     * utilizadas
     *
     * @param nit identificacion del inversionista
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<MovimientosCaptaciones> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> getMovimientosCausacion(String dstrct) throws Exception {
        return dao.getMovimientosCausacion(dstrct);
    }

    /**
     * Obtiene una lista de todos lso movimientops con las distinctas cuentas
     * utilizadas
     *
     * @param nit identificacion del inversionista
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<MovimientosCaptaciones> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> getMovimientosPosCausacion(String dstrct, String nit, String subcuenta) throws Exception {
        return dao.consultarMovimientosPosCausacion(dstrct, nit, subcuenta);
    }

    /**
     * Obtiene una lista de todos lso movimientops con las distinctas cuentas
     * utilizadas
     *
     * @param nit identificacion del inversionista
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<MovimientosCaptaciones> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> consultarSubcuentasMovimientosInversionista(String dstrct, String nit) throws Exception {
        return dao.consultarSubcuentasMovimientosInversionista(dstrct, nit);
    }

    /**
     * Obtiene una lista de todos lso movimientops con las distinctas cuentas
     * utilizadas
     *
     * @param nit identificacion del inversionista
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<MovimientosCaptaciones> con los resultados obtenidos
     * @throws Exception
     */
    public String consultarFechaUltimoCierre(String dstrct, String nit, String subcuenta) throws Exception {
        return dao.consultarFechaUltimoCierre(dstrct, nit, subcuenta);
    }

    public String crearEgresoRetiro(MovimientosCaptaciones movimiento) throws Exception {
        return dao.crearEgresoRetiro(movimiento);

    }

    public String crearEgresoDetalleRetiro(MovimientosCaptaciones movimiento) throws Exception {
        return dao.crearEgresoDetalleRetiro(movimiento);
    }

    public String actualizarCxp_retiro(MovimientosCaptaciones movimiento) throws Exception {
        return dao.actualizarCxp_retiro(movimiento);

    }

    public ArrayList<MovimientosCaptaciones> agruparRetiros(String retiros[]) throws Exception {
        return dao.agruparRetiros(retiros);
    }

    /**
     * Obtiene los movimientos del mes
     *
     * @param mes a consultar
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exceptionfin elaboracion pdf solicitud de aval
     */
    public ArrayList<MovimientosCaptaciones> consultarMovimientosMes(String dstrct, String nit, String subcuenta, String periodo) throws Exception {
        return dao.consultarMovimientosMes(dstrct, nit, subcuenta, periodo);
    }

    /**
     * *********************************************soporte pdf captaciones ***********************************************************
     */
    public boolean exportarExtractoPdf(String userlogin, Inversionista inversionista, ArrayList<MovimientosCaptaciones> lista_movimientos, String periodo) throws Exception {
        String url_logo = "fintrapdf.gif";

        boolean generado = true;
        String directorio = "";
        String documento_detalle = "";
        ResourceBundle rb = null;
        try {
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            directorio = this.directorioArchivo(userlogin, inversionista.getNit() + "_" + inversionista.getSubcuenta() + periodo, "pdf");
            Font fuente = new Font(Font.HELVETICA, 8);
            Font fuenteB = new Font(Font.HELVETICA, 8, Font.BOLD);
            Document documento = null;
            documento = this.createDoc("V");

            PdfWriter Pdfriter = PdfWriter.getInstance(documento, new FileOutputStream(directorio));
            documento.setFooter(this.getMyFooter());
            Pdfriter.setPageEvent(new CaptacionInversionistaService(this.dao.getDatabaseName()));

            documento.open();

            Image img = Image.getInstance(rb.getString("ruta") + "/images/" + url_logo);
            img.scaleToFit(130, 130);
            documento.add(img);
            documento.add(Chunk.NEWLINE);
            Paragraph p = new Paragraph("EXTRACTO DE CUENTA", fuenteB);
            p.setAlignment(Paragraph.ALIGN_CENTER);
            documento.add(p);
            p = new Paragraph("Periodo : " + Util.NombreMes(Integer.parseInt(periodo.substring(5, 6))) + " - " + periodo.substring(0, 4), fuente);
            p.setAlignment(Paragraph.ALIGN_CENTER);
            documento.add(p);
            documento.add(Chunk.NEWLINE);

           // poneImagen(documento, rb.getString("ruta") + "/images/marca_agua.gif");
            /*----------------------------------DatosInversionista--------------------------------------*/
            PdfPTable tabla_datos = this.TablaContenedora(inversionista, lista_movimientos);
            tabla_datos.setWidthPercentage(100);
            documento.add(tabla_datos);
            documento.add(Chunk.NEWLINE);
            documento.add(Chunk.NEWLINE);

            p = new Paragraph("Lista De Movimientos ", fuenteB);
            p.setAlignment(Element.ALIGN_LEFT);
            documento.add(p);
            documento.add(Chunk.NEWLINE);
            /*----------------------------------lista_movimientos--------------------------------------*/
            PdfPTable tabla_movimientos = this.Movimientos(lista_movimientos, inversionista.getTipo_interes());
            tabla_movimientos.setWidthPercentage(100);
            documento.add(tabla_movimientos);
            documento.close();

        } catch (Exception e) {
            generado = false;
            System.out.println("error al generar pdf : " + e.toString());
            e.printStackTrace();
        }

        return generado;
    }

    /**
     * ******************************************************************************************************************
     */
    private Document createDoc(String op) {///hora horizontal
        int alto = 842;
        int ancho = 595;
        if (op.equals("V"))//vertical
        {
            alto = ancho;
            ancho = 842;
        }

        Rectangle pageSize = new Rectangle(ancho, alto);

        Document doc = new Document(pageSize, 60, 40, 40, 40);//
        doc.addAuthor("Fintra S.A");
        doc.addSubject("Informe Mesual");

        return doc;
    }

    private static void poneImagen2(Document document, String archivo) {
        RandomAccessFileOrArray ra = null;
        boolean resultado = false;
        try {
            ra = new RandomAccessFileOrArray("/home/jpinedo/Sitios/fintra/build/web/images/fintrapdf.tif");
            com.itextpdf.text.Image img = TiffImage.getTiffImage(ra, 1);
            if (img != null) {
                //Se valida la escala de la im�gen
                if (img.getScaledWidth() > 600 || img.getScaledHeight() > 800) {
                    //Se reduce la escala de la im�gen
                    img.scaleToFit(615, 825);
                }
                //Se indica la posicion donde se colocara la im�gen en el PDF
                img.setAbsolutePosition(0, 0);
                //Se inserta la im�gen en el PDF
                document.add((Element) img);
            }
            ra.close();
        } catch (Exception e) {
            System.out.println("Error al agregar imagen al PDF " + e.getMessage());
        }
    }

    /**
     * ************************************ DATOS Inversionistas**************************************************
     */
    protected PdfPTable DatosInversionista(Inversionista inversionista) throws DocumentException, SQLException {

        NitDAO nitDAO = new NitDAO(this.dao.getDatabaseName());
        NitSot objnit = new NitSot();
        objnit = nitDAO.searchNit(inversionista.getNit());
        String fecha = Util.getFechahoy();
        PdfPTable tabla_temp = new PdfPTable(2);
        java.awt.Color fondo = new java.awt.Color(0x33, 0x66, 0x33);
        java.awt.Color color_fuente_header = new java.awt.Color(0xFF, 0xFF, 0xFF);

        Font fuente_header = new Font(Font.TIMES_ROMAN, 9, Font.BOLD);
        fuente_header.setColor(color_fuente_header);
        //double vlr=this.Total_pagado(lista);
        double vlr = 10;
        float[] medidaCeldas = {0.280f, 0.720f};
        tabla_temp.setWidths(medidaCeldas);

        Font fuente = new Font(Font.TIMES_ROMAN, 9);
        Font fuenteB = new Font(Font.TIMES_ROMAN, 9, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Informac�on Titular", fuente_header));
        celda_temp.setBackgroundColor(fondo);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda_temp.setColspan(2);
        tabla_temp.addCell(celda_temp);

        // fila 1
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Nombre", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(inversionista.getNombre_nit(), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("No ID", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(inversionista.getNit(), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Direccion", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(objnit.getDireccion(), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        CiudadService cs = new CiudadService(this.dao.getDatabaseName());

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Ciudad", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(cs.obtenerNombreCiudad(objnit.getCodciu()), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setHorizontalAlignment(tabla_temp.ALIGN_LEFT);
        tabla_temp.setWidthPercentage(80);

        return tabla_temp;
    }

    /**
     * ************************************ DATOS Inversionistas**************************************************
     */
    protected PdfPTable TablaContenedora(Inversionista inversionista, ArrayList<MovimientosCaptaciones> lista_movimientos) throws DocumentException, SQLException {

        String fecha = Util.getFechahoy();
        PdfPTable tabla_temp = new PdfPTable(2);
        //double vlr=this.Total_pagado(lista);
        double vlr = 10;
        float[] medidaCeldas = {0.500f, 0.500f};
        tabla_temp.setWidths(medidaCeldas);

        Font fuente = new Font(Font.HELVETICA, 9);
        Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();

        this.Totales(lista_movimientos);

        // fila 1
        celda_temp = new PdfPCell();
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.addElement(this.DatosInversionista(inversionista));
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.addElement(this.Resumen());
        celda_temp.setHorizontalAlignment(PdfPCell.RIGHT);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        return tabla_temp;
    }

    /**
     * ************************************ Movimientos**************************************************
     */
    protected PdfPTable Resumen() throws DocumentException {
          //totales

        PdfPTable tabla_temp = new PdfPTable(2);
        try {

            java.awt.Color fondo = new java.awt.Color(0x33, 0x66, 0x33);
            java.awt.Color color_fuente_header = new java.awt.Color(0xFF, 0xFF, 0xFF);

            float[] medidaCeldas = {0.500f, 0.500F};
            tabla_temp.setWidths(medidaCeldas);

            Font fuente = new Font(Font.TIMES_ROMAN, 9);
            Font fuenteB = new Font(Font.TIMES_ROMAN, 9, Font.BOLD);
            Font fuente_header = new Font(Font.TIMES_ROMAN, 9, Font.BOLD);
            fuente_header.setColor(color_fuente_header);
            PdfPCell celda_temp = new PdfPCell();
            celda_temp = new PdfPCell();

            // columna 1
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Resumen Movimientos", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setColspan(2);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Saldo Inicial", fuenteB));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(Util.customFormat(this.saldo_inicial), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Consignacion", fuenteB));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(Util.customFormat(this.total_consignaciones), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Retiros", fuenteB));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(Util.customFormat(this.total_retiros), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Intereses Generados", fuenteB));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(Util.customFormat(this.total_intereses), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Retenciones Aplicadas", fuenteB));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(Util.customFormat(this.total_retenciones), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Saldo Final", fuenteB));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(Util.customFormat(this.saldo_final), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);

            tabla_temp.addCell(celda_temp);
            tabla_temp.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            tabla_temp.setWidthPercentage(60);

        } catch (Exception e) {

            System.out.println("error al generar pdf : " + e.toString());
            e.printStackTrace();
        }
        return tabla_temp;

    }

    /**
     * ************************************ Movimientos**************************************************
     */
    protected PdfPTable Movimientos(ArrayList<MovimientosCaptaciones> lista_movimientos, String tipo_interes) throws DocumentException {

        double vlr = 0, ip = 0;
        PdfPTable tabla_temp = new PdfPTable(11);
        try {

            java.awt.Color fondo = new java.awt.Color(0x33, 0x66, 0x33);
            java.awt.Color color_fuente_header = new java.awt.Color(0xFF, 0xFF, 0xFF);
            float[] medidaCeldas = {0.100f, 0.100f, 0.100f, 0.100f, 0.100f, 0.100f, 0.100f, 0.100f, 0.100f, 0.100f, 0.100f};
            tabla_temp.setWidths(medidaCeldas);

            Font fuente = new Font(Font.TIMES_ROMAN, 8);
            Font fuenteB = new Font(Font.TIMES_ROMAN, 8, Font.BOLD);
            Font fuente_header = new Font(Font.TIMES_ROMAN, 8, Font.BOLD);
            fuente_header.setColor(color_fuente_header);
            PdfPCell celda_temp = new PdfPCell();
            celda_temp = new PdfPCell();

            // columna 1
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Transaccion", fuente_header));
            celda_temp.setBackgroundColor(fondo);

            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Fecha", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            if (tipo_interes.equals("C")) {
                celda_temp.setPhrase(new Phrase("Saldo Inicial", fuente_header));
            } else {
                celda_temp.setPhrase(new Phrase("Capital", fuente_header));
            }
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            if (tipo_interes.equals("S")) {
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("Interes Acomulado", fuente_header));
                celda_temp.setBackgroundColor(fondo);
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);
            }

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Interes", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Retefuente", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Reteica", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            if (tipo_interes.equals("C")) {
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("Subtotal", fuente_header));
                celda_temp.setBackgroundColor(fondo);
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);
            }
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Consignacion", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Retiro", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Saldo Final", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Concepto Transacci�n", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            for (int i = 0; i < lista_movimientos.size(); i++) {
                MovimientosCaptaciones movimiento = (MovimientosCaptaciones) lista_movimientos.get(i);

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase(movimiento.getNo_transaccion(), fuenteB));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);

                String fecha = movimiento.getFecha().substring(0, 10);
                String anio = fecha.substring(2, 4);//a�o
                String dia = fecha.substring(8, 10);//dia
                int mes = Integer.parseInt(fecha.substring(5, 7));//mes
                fecha = dia + "-" + Util.NombreMes(mes).substring(0, 3) + "-" + anio;

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase(fecha, fuente));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);

                celda_temp = new PdfPCell();
                if (tipo_interes.equals("C")) {
                    celda_temp.setPhrase(new Phrase(Util.customFormat(movimiento.getSaldo_inicial()), fuente));
                } else {
                    celda_temp.setPhrase(new Phrase(Util.customFormat(movimiento.getBase_intereses()), fuente));
                }
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase(Util.customFormat(movimiento.getValor_intereses()), fuente));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);

                if (tipo_interes.equals("S")) {
                    celda_temp = new PdfPCell();
                    celda_temp.setPhrase(new Phrase(Util.customFormat(movimiento.getValor_intereses()), fuente));
                    celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                    tabla_temp.addCell(celda_temp);
                }

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase(Util.customFormat(movimiento.getValor_retefuente()), fuente));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase(Util.customFormat(movimiento.getValor_reteica()), fuente));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);

                if (tipo_interes.equals("C")) {
                    celda_temp = new PdfPCell();
                    celda_temp.setPhrase(new Phrase(Util.customFormat(movimiento.getSubtotal()), fuente));
                    celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                    tabla_temp.addCell(celda_temp);
                }

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase(Util.customFormat(movimiento.getConsignacion()), fuente));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase(Util.customFormat(movimiento.getRetiro()), fuente));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase(Util.customFormat(movimiento.getSaldo_final()), fuente));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase(movimiento.getConcepto_transaccion(), fuente));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);

            }
        } catch (Exception e) {

            System.out.println("error al generar pdf : " + e.toString());
            e.printStackTrace();
        }
        return tabla_temp;

    }

    public String directorioArchivo(String user, String cons, String extension) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user.toUpperCase();
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
            ruta = ruta + "/" + cons +/* "_" + fmt.format(new Date()) +*/ "." + extension;
        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }

    public String directorioArchivo(String user) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user.toUpperCase();
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }

        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }

    public HeaderFooter getMyFooter() throws ServletException {
        try {

            Phrase p = new Phrase();
            Font fuente = new Font(Font.HELVETICA, 8);
            Font fuenteB = new Font(Font.HELVETICA, 8, Font.BOLD);
            p.clear();

            p.add(new Paragraph("Carrera 53 # 79 - 01 Local 205 Barranquilla, Colombia\n", fuenteB));
            p.add(new Paragraph("PBX: 57 5 3679900 FAX: 57 5 3679906\n", fuenteB));
            p.add(new Paragraph("www.fintra.co\n", fuenteB));
            HeaderFooter header = new HeaderFooter(p, false);
            header.setBorder(Rectangle.NO_BORDER);
            header.setAlignment(Paragraph.ALIGN_CENTER);

            return header;
        } catch (Exception ex) {
            throw new ServletException("Header Error");
        }
    }

    /**
     * Genera el xls del estado de cuenta
     *
     * @param user Usuario que lo genera
     * @param listafacts Lista de facturas
     * @param listaing Lista de ingresos
     * @param listahead Datos para el encabezado
     * @param sal Saldo del negocio
     * @param vec Otros datos del cliente que se incluyen
     * @throws Exception Cuando hay un error
     */
    public boolean exportarExtractoXls(String userlogin, Inversionista inversionista, ArrayList<MovimientosCaptaciones> lista_movimientos, String periodo) throws Exception {

        /**
         * ******** extracto pronto pago**************
         */
        float tl = 0;
        boolean generado = true;
        String directorio = "";
        String documento_detalle = "";
        String placa = "";
        ResourceBundle rb = null;
        try {
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");

            directorio = this.directorioArchivo(userlogin, inversionista.getNit() + "_" + inversionista.getSubcuenta() + periodo, "xls");
            ExcelApplication excel = this.instanciar("Extracto");
            excel.combinarCeldas(0, 0, 2, 4);
            excel.combinarCeldas(4, 4, 4, 5);
            fila = fila + 3;
            String ruta_img = rb.getString("ruta") + "/images/fintrapdf.gif";
            excel.insertaImagen(ruta_img, 0, 0, 10, 200, 0, 0, 1, 2);

            excel.setDataCell(fila, 0, "INVERSIONISTA");
            excel.setCellStyle(fila, 0, excel.getStyle("estilo2"));
            excel.setDataCell(fila, 1, inversionista.getNombre_subcuenta());
            excel.setCellStyle(fila, 1, excel.getStyle("estilo2"));

            excel.setCellStyle(fila + 1, 0, excel.getStyle("estilo2"));
            excel.setDataCell(fila + 1, 0, "NIT");
            excel.setDataCell(fila + 1, 1, inversionista.getNit());
            excel.setCellStyle(fila + 1, 1, excel.getStyle("estilo2"));

            excel.setCellStyle(fila + 2, 0, excel.getStyle("estilo2"));
            excel.setDataCell(fila + 2, 0, "FECHA GENERACION");
            String fecha_generacion = Util.getFechahoy();
            excel.setDataCell(fila + 2, 1, fecha_generacion.substring(8, 10) + " de " + Utility.NombreMes(Integer.parseInt(fecha_generacion.substring(5, 7))) + " de " + fecha_generacion.substring(0, 4));
            excel.setCellStyle(fila + 2, 1, excel.getStyle("estilo2"));

            //totales
            this.Totales(lista_movimientos);

            excel.setDataCell(4, 4, "Resumen De Movimientos");
            excel.setCellStyle(4, 4, excel.getStyle("estilo2"));
            excel.setDataCell(5, 4, "Saldo Inicial");
            excel.setCellStyle(5, 4, excel.getStyle("estilo2"));
            excel.setDataCell(6, 4, "Consignaciones");
            excel.setCellStyle(6, 4, excel.getStyle("estilo2"));
            excel.setDataCell(7, 4, "Retiros");
            excel.setCellStyle(7, 4, excel.getStyle("estilo2"));

            excel.setDataCell(8, 4, "Intereses Generados");
            excel.setCellStyle(8, 4, excel.getStyle("estilo2"));

            excel.setDataCell(9, 4, "Retenciones Aplicadas");
            excel.setCellStyle(9, 4, excel.getStyle("estilo2"));

            excel.setDataCell(9, 4, "Saldo Final");
            excel.setCellStyle(9, 4, excel.getStyle("estilo2"));

            excel.setDataCell(5, 5, this.saldo_inicial);
            excel.setCellStyle(5, 5, excel.getStyle("numero"));
            excel.setDataCell(6, 5, this.total_consignaciones);
            excel.setCellStyle(6, 5, excel.getStyle("numero"));
            excel.setDataCell(7, 5, this.total_retiros);
            excel.setCellStyle(7, 5, excel.getStyle("numero"));

            excel.setDataCell(8, 5, this.total_intereses);
            excel.setCellStyle(8, 5, excel.getStyle("numero"));

            excel.setDataCell(9, 5, this.total_retenciones);
            excel.setCellStyle(9, 5, excel.getStyle("numero"));

            excel.setDataCell(9, 5, this.saldo_final);
            excel.setCellStyle(9, 5, excel.getStyle("numero"));

            fila = fila + 2;

            MovimientosXLS(lista_movimientos, inversionista.getTipo_interes());
            fila = 0;
            excel.saveToFile(directorio);
        } catch (Exception e) {
            generado = false;
            throw new Exception("Error al generar el archivo xls: " + e.toString());

        }

        return generado;
    }

    /**
     * Crea un objeto tipo ExcelApplication
     *
     * @param descripcion Nombre de la hoja principal del libro
     * @return Objeto ExcelApplication creado
     * @throws Exception Cuando hay error
     */
    private ExcelApplication instanciar(String descripcion) throws Exception {
        excel = new ExcelApplication();

        try {
            excel.createSheet(descripcion);

            excel.createColor((short) 11, (byte) 255, (byte) 255, (byte) 255);//Blanco
            excel.createColor((short) 9, (byte) 26, (byte) 126, (byte) 0);//Verde dark
            excel.createColor((short) 10, (byte) 37, (byte) 69, (byte) 255);//Azul oscuro
            excel.createColor((short) 8, (byte) 234, (byte) 241, (byte) 221);//Verde claro
            //(153, 204, 255);

            excel.createFont("Titulo", "Arial", (short) 11, true, (short) 12);
            excel.createFont("Subtitulo", "Verdana", (short) 0, true, (short) 10);
            excel.createFont("Titulo", "Verdana", (short) 11, true, (short) 10);
            excel.createFont("Contenido", "Verdana", (short) 0, false, (short) 10);
            excel.createFont("t_liqui", "Verdana", (short) 0, true, (short) 10);//aliniado ala ezquierda negrita
            excel.createFont("Valor_total", "Verdana", (short) 0, true, (short) 10);//aliniado ala ezquierda negrita

            excel.createStyle("estilo3", excel.getFont("Contenido"), (short) 11, true, "@");
            excel.createStyle("estilo2", excel.getFont("Titulo"), (short) 9, true, "@", (short) 2);
            excel.createStyle("estilo5", excel.getFont("Subtitulo"), (short) 8, true, "@", (short) 2);//centrado cabecera tabla
            excel.createStyle("estilo1", excel.getFont("Contenido"), (short) 11, true, "@", (short) 2);//centrado normal

            excel.createStyle("estilo6", excel.getFont("Contenido"), (short) 11, true, "#", (short) 1);//aliniado ala ezquierda normal
            excel.createStyle("estilo7", excel.getFont("t_liqui"), (short) 11, true, "@", (short) 1);//aliniado ala ezquierda negrita
            excel.createStyle("estilo8", excel.getFont("Valor_total"), (short) 11, true, "@");// contenido negrita
            excel.createStyle("estilo9", excel.getFont("Valor_total"), (short) 11, true, "@", (short) 1);// contenido negrita centrado
            excel.createStyle("numero", excel.getFont("Contenido"), (short) 11, true, "#,###");//centrado normal

        } catch (Exception e) {
            throw new Exception("Error al instanciar el objeto: " + e.toString());
        }
        return excel;
    }

    /**
     * Crea un tabla con los datos de las facturas del propietario de la
     * liquidacion OC
     *
     * @throws DocumentException
     * @author JPINEDO-FINTRA
     * @date 12/04/2011
     * @version 1.0
     */
    protected void MovimientosXLS(ArrayList<MovimientosCaptaciones> lista_movimientos, String tipo_interes) throws DocumentException, Exception {
        fila = fila + 8;
        int col = 0;
        /*---------RECORRER CADA DOCUMETO-----------*/

        excel.cambiarAnchoColumna(0, 6000);

        // fila 4
        excel.setDataCell(fila, col, "Transaccion");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));

        col = col + 1;
        excel.setDataCell(fila, col, "Fecha");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 5000);
        col++;

        if (tipo_interes.equals("C")) {
            excel.setDataCell(fila, col, "Saldo Inicial");
        } else {
            excel.setDataCell(fila, col, "Capital");
        }
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 5000);

        col++;
        excel.setDataCell(fila, col, "Intereses");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 5000);

        if (tipo_interes.equals("S")) {
            col++;
            excel.setDataCell(fila, col, "Intereses Acomulados");
            excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
            excel.cambiarAnchoColumna(col, 5000);
        }

        col++;
        excel.setDataCell(fila, col, "Retefuente");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 5000);

        col++;
        excel.setDataCell(fila, col, "Reteica");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 5000);

        if (tipo_interes.equals("C")) {
            col++;
            excel.setDataCell(fila, col, "Subtotal");
            excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
            excel.cambiarAnchoColumna(col, 5000);
        }
        col++;
        excel.setDataCell(fila, col, "Consignacion");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 5000);
        col++;
        excel.setDataCell(fila, col, "Retiro");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 5000);
        col++;
        excel.setDataCell(fila, col, "Saldo Final");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 5000);

        col++;
        excel.setDataCell(fila, col, "Tipo Transferencia");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 5000);

        col++;
        excel.setDataCell(fila, col, "Banco");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 5000);

        col++;
        excel.setDataCell(fila, col, "Titular Cuenta");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 5000);

        col++;
        excel.setDataCell(fila, col, "Nit Cuenta");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 5000);

        col++;
        excel.setDataCell(fila, col, "Numero De Cuenta");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 5000);

        col++;
        excel.setDataCell(fila, col, "Tipo Cuenta");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 5000);

        col++;
        excel.setDataCell(fila, col, "Nit Beneficiario");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 5000);

        col++;
        excel.setDataCell(fila, col, "Beneficiario");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 5000);

        col++;
        excel.setDataCell(fila, col, "Cheque Cruzado");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 5000);

        col++;
        excel.setDataCell(fila, col, "Primer Beneficiario");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 7000);

        excel.setDataCell(fila, col, "Beneficiario Captacion");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 7000);

        excel.setDataCell(fila, col, "Concepto Transacci�n");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 7000);
        
        fila = fila + 1;
        for (int i = 0; i < lista_movimientos.size(); i++) {
            col = 0;
            MovimientosCaptaciones movimiento = (MovimientosCaptaciones) lista_movimientos.get(i);
            excel.setDataCell(i + fila, col, movimiento.getNo_transaccion());
            excel.setCellStyle(i + fila, col, excel.getStyle("estilo1"));

            col = col + 1;

            excel.setDataCell(i + fila, col, movimiento.getFecha().substring(0, 10));
            excel.setCellStyle(i + fila, col, excel.getStyle("estilo1"));
            col++;
            if (tipo_interes.equals("C")) {
                excel.setDataCell(i + fila, col, movimiento.getSaldo_inicial());
            } else {
                excel.setDataCell(i + fila, col, movimiento.getBase_intereses());
            }

            excel.setCellStyle(i + fila, col, excel.getStyle("numero"));
            col++;
            excel.setDataCell(i + fila, col, movimiento.getValor_intereses());
            excel.setCellStyle(i + fila, col, excel.getStyle("numero"));

            if (tipo_interes.equals("S")) {
                col++;
                excel.setDataCell(i + fila, col, movimiento.getIntereses_acomulados());
                excel.setCellStyle(i + fila, col, excel.getStyle("numero"));
            }

            col++;
            excel.setDataCell(i + fila, col, movimiento.getValor_retefuente());
            excel.setCellStyle(i + fila, col, excel.getStyle("numero"));
            col++;
            excel.setDataCell(i + fila, col, movimiento.getValor_reteica());
            excel.setCellStyle(i + fila, col, excel.getStyle("numero"));

            if (tipo_interes.equals("C")) {
                col++;
                excel.setDataCell(i + fila, col, movimiento.getSubtotal());
                excel.setCellStyle(i + fila, col, excel.getStyle("numero"));
            }
            col++;
            excel.setDataCell(i + fila, col, movimiento.getConsignacion());
            excel.setCellStyle(i + fila, col, excel.getStyle("numero"));
            col++;
            excel.setDataCell(i + fila, col, movimiento.getRetiro());
            excel.setCellStyle(i + fila, col, excel.getStyle("numero"));
            col++;
            excel.setDataCell(i + fila, col, movimiento.getSaldo_final());
            excel.setCellStyle(i + fila, col, excel.getStyle("numero"));
            ///// datos transferencias

            col++;
            excel.setDataCell(i + fila, col, movimiento.getTipo_transferencia());
            excel.setCellStyle(i + fila, col, excel.getStyle("estilo1"));

            col++;
            excel.setDataCell(i + fila, col, movimiento.getBanco());
            excel.setCellStyle(i + fila, col, excel.getStyle("estilo3"));

            col++;
            excel.setDataCell(i + fila, col, movimiento.getTitular_cuenta());
            excel.setCellStyle(i + fila, col, excel.getStyle("estilo1"));

            col++;
            excel.setDataCell(i + fila, col, movimiento.getNit_cuenta());
            excel.setCellStyle(i + fila, col, excel.getStyle("estilo1"));

            col++;
            excel.setDataCell(i + fila, col, movimiento.getCuenta());
            excel.setCellStyle(i + fila, col, excel.getStyle("estilo1"));

            col++;
            excel.setDataCell(i + fila, col, movimiento.getTipo_cuenta());
            excel.setCellStyle(i + fila, col, excel.getStyle("estilo1"));

            col++;
            excel.setDataCell(i + fila, col, movimiento.getNit_beneficiario());
            excel.setCellStyle(i + fila, col, excel.getStyle("estilo1"));

            col++;
            excel.setDataCell(i + fila, col, movimiento.getNombre_beneficiario());
            excel.setCellStyle(i + fila, col, excel.getStyle("estilo1"));

            col++;
            excel.setDataCell(i + fila, col, movimiento.getCheque_cruzado());
            excel.setCellStyle(i + fila, col, excel.getStyle("estilo1"));

            col++;
            excel.setDataCell(i + fila, col, movimiento.getConcepto_transaccion());
            excel.setCellStyle(i + fila, col, excel.getStyle("estilo1"));
            
            col++;
            excel.setDataCell(i + fila, col, movimiento.getCheque_primer_beneficiario());
            excel.setCellStyle(i + fila, col, excel.getStyle("estilo1"));
            col++;
            excel.setDataCell(i + fila, col, movimiento.getNombre_beneficiario_captacion());
            excel.setCellStyle(i + fila, col, excel.getStyle("estilo1"));
            
         }

    }

    public void Totales(ArrayList<MovimientosCaptaciones> lista_movimientos) throws DocumentException {
        this.saldo_inicial = lista_movimientos.get(0).getSaldo_inicial();
        this.saldo_final = lista_movimientos.get(lista_movimientos.size() - 1).getSaldo_final();
        for (int i = 0; i < lista_movimientos.size(); i++) {
            MovimientosCaptaciones movimiento = (MovimientosCaptaciones) lista_movimientos.get(i);
            this.total_retiros = this.total_retiros + movimiento.getRetiro();
            this.total_consignaciones = this.total_consignaciones + movimiento.getConsignacion();
            this.total_intereses = this.total_intereses + movimiento.getValor_intereses();
            this.total_retenciones = this.total_retenciones + movimiento.getValor_retefuente() + movimiento.getValor_reteica();

        }

    }


    /*
     *  devuelve el retiro total con sus interes calculados
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public MovimientosCaptaciones getRetiroRecalculado(ArrayList<MovimientosCaptaciones> lista_movimientos, MovimientosCaptaciones retiro) throws Exception {

        for (int j = 0; j < lista_movimientos.size(); j++) {

            if (lista_movimientos.get(j).getNo_transaccion().equals(retiro.getNo_transaccion())) {
                retiro = lista_movimientos.get(j);
                retiro.setValor_intereses(this.getTotalInteres(lista_movimientos));
                return retiro;
            }

        }

        return retiro;
    }

    public static ArrayList<MovimientosCaptaciones> cloneList(ArrayList<MovimientosCaptaciones> list) throws CloneNotSupportedException {
        ArrayList<MovimientosCaptaciones> clonedList = new ArrayList<MovimientosCaptaciones>();
        for (int i = 0; i < list.size(); i++) {
            MovimientosCaptaciones mov = new MovimientosCaptaciones();
            mov = (MovimientosCaptaciones) list.get(i).clone();
            clonedList.add(mov);
        }
        return clonedList;
    }

    /**
     * Obtiene los movimientos de informe cierre
     *
     * @param dstrct distrito del usuario en sesion
     * @param periodo
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> getInformeCierreInversionistas(String dstrct, String periodo) throws Exception {
        return dao.getInformeCierreInversionistas(dstrct, periodo);
    }

    public ArrayList<MovimientosCaptaciones> getInformeCierreSocio(String dstrct, String periodo) throws Exception {
        return dao.getInformeCierreSocio(dstrct, periodo);
    }

    public ArrayList<MovimientosCaptaciones> getInformeCierreOtros(String dstrct, String periodo) throws Exception {
        return dao.getInformeCierreOtros(dstrct, periodo);
    }

    /**
     * *********************************************informe pdf captaciones ***********************************************************
     */
    public boolean exportarInformePdf(String userlogin, ArrayList<MovimientosCaptaciones> lista_movimientos, String periodo) throws Exception {
        String url_logo = "fintrapdf.gif";

        boolean generado = true;
        String directorio = "";
        String documento_detalle = "";
        ResourceBundle rb = null;
        try {
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            directorio = this.directorioArchivo(userlogin, "informe" + periodo, "pdf");
            Font fuente = new Font(Font.HELVETICA, 8);
            Font fuenteB = new Font(Font.HELVETICA, 8, Font.BOLD);
            Document documento = null;
            documento = this.createDoc("H");
            PdfWriter Pdfriter = PdfWriter.getInstance(documento, new FileOutputStream(directorio));

            documento.setFooter(this.getMyFooter());

            documento.open();
            documento.newPage();
            Image img = Image.getInstance(rb.getString("ruta") + "/images/" + url_logo);
            img.scaleToFit(130, 130);
            documento.add(img);

            documento.add(Chunk.NEWLINE);
            documento.add(new Paragraph(" 802.022.016-1", fuenteB));
            documento.add(Chunk.NEWLINE);

            /*----------------------------------DatosGenerales--------------------------------------*/
            PdfPTable tabla_datos = this.Datosgenerales(periodo);
            tabla_datos.setWidthPercentage(100);
            documento.add(tabla_datos);
            documento.add(Chunk.NEWLINE);
            documento.add(Chunk.NEWLINE);

            Paragraph p;
            p = new Paragraph("Lista De Movimientos ", fuenteB);
            p.setAlignment(Element.ALIGN_LEFT);
            documento.add(p);
            documento.add(Chunk.NEWLINE);
            /*----------------------------------lista_movimientos--------------------------------------*/
            PdfPTable tabla_movimientos = this.MovimientosCierres(lista_movimientos);
            tabla_movimientos.setWidthPercentage(100);
            documento.add(tabla_movimientos);
            documento.add(Chunk.NEWLINE);
            documento.add(Chunk.NEWLINE);
            documento.add(Chunk.NEWLINE);
            //documento.newPage();

            ///insertr grafico
            String url = rb.getString("ruta") + "/exportar/migracion/" + "informe" + periodo + ".jpg";
            Image imgGraf = Image.getInstance(url);
            imgGraf.scaleToFit(500, 500);
            documento.add(imgGraf);

            documento.close();

        } catch (Exception e) {
            generado = false;
            System.out.println("error al generar pdf : " + e.toString());
            e.printStackTrace();
        }

        return generado;
    }

    //metodo modificado por edgar para java 7
    public static void crearImgGrafico(JFreeChart chart,
            String aFileName,
            int width,
            int height,
            double quality)
            throws FileNotFoundException, IOException {

        BufferedImage img = draw(chart, width, height);

        FileOutputStream fos = new FileOutputStream(aFileName);

        //JPEGImageEncoder encoder2 = JPEGCodec.createJPEGEncoder(fos);
        //JPEGEncodeParam param2 = encoder2.getDefaultJPEGEncodeParam(img);
        
        JPEGImageWriter imageWriter = (JPEGImageWriter) ImageIO.getImageWritersBySuffix("jpeg").next();
        ImageOutputStream ios = ImageIO.createImageOutputStream(fos);
        imageWriter.setOutput(ios);

       // param2.setQuality((float) quality, true);
        
        JPEGImageWriteParam jpegParams = (JPEGImageWriteParam) imageWriter.getDefaultWriteParam();
        jpegParams.setCompressionMode(JPEGImageWriteParam.MODE_EXPLICIT);
        jpegParams.setCompressionQuality((float) quality);

        
        IIOMetadata imageMetaData = imageWriter.getDefaultImageMetadata(new ImageTypeSpecifier(img), null);
        
        //encoder2.encode(img, param2);
        
        imageWriter.write(imageMetaData, new IIOImage(img, null, null), null);
        ios.close();
        imageWriter.dispose();

        fos.close();

    }

    public static BufferedImage draw(JFreeChart chart, int width, int height) {

        BufferedImage img = new BufferedImage(width, height,BufferedImage.TYPE_INT_RGB);

        Graphics2D g2 = img.createGraphics();

        chart.draw(g2, new Rectangle2D.Double(0, 0, width, height));

        g2.dispose();

        return img;

    }

    /**
     * Creates a chart.
     *
     * @param dataset the dataset.
     *
     * @return A chart.
     */
    /*  public static JFreeChart createChart(PieDataset dataset) {

     JFreeChart chart = ChartFactory.createPie3DChart(
     "Grafico Informe Mensual",  // chart title
     dataset,             // data
     true,               // include legend
     true,
     true
     );

     PiePlot plot = (PiePlot) chart.getPlot();
     // plot.set
     //  plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 12));
     plot.setNoDataMessage("No Exste informacion para este periodo");
     plot.setCircular(false);


     plot.setStartAngle(270);
     //   plot.setDirection(Rotation.ANTICLOCKWISE);
     plot.setForegroundAlpha(0.60f);
     plot.setInteriorGap(0.33);
     // add the chart to a panel...
     final ChartPanel chartPanel = new ChartPanel(chart);
     chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
     return chart;

     }*/
    /**
     * Creates a sample dataset.
     *
     * @return A sample dataset.
     */
    public static PieDataset createDataset(ArrayList<MovimientosCaptaciones> lista_movimientos) {
        DefaultPieDataset dataset = new DefaultPieDataset();

        //aqui hacer calculo por inversionista
        for (int i = 0; i < lista_movimientos.size(); i++) {
            MovimientosCaptaciones movimeinto = lista_movimientos.get(i);
            dataset.setValue(movimeinto.getNombre_inversionista(), movimeinto.getSaldo_final());
        }
        return dataset;
    }

    /**
     * Creates a chart.
     *
     * @param dataset the dataset.
     *
     * @return A chart.
     */
    public static JFreeChart createChartBarra(DefaultCategoryDataset dataset) {

        JFreeChart chart = ChartFactory.createBarChart3D("Grafico Informe Mensual", "", "Saldo", dataset, PlotOrientation.HORIZONTAL, true, true, false);

        CategoryPlot plot = chart.getCategoryPlot();
        plot.setNoDataMessage("No Exste informacion para este periodo");
        BarRenderer render = (BarRenderer) plot.getRenderer();
        render.setSeriesPaint(0, new Color(0, 153, 0));
        render.setSeriesPaint(1, new Color(0, 153, 0));
        chart.setBackgroundPaint(new Color(204, 255, 229));

        CategoryAxis domainAxis = plot.getDomainAxis();

        java.awt.Font font2 = new java.awt.Font("Dialog", Font.NORMAL, 8);
        domainAxis.setTickLabelFont(font2);
        domainAxis.setLabelFont(font2);
        CategoryLabelPosition leftPositionSpec = new CategoryLabelPosition(
                RectangleAnchor.LEFT, TextBlockAnchor.CENTER_LEFT,
                CategoryLabelWidthType.RANGE, 0.4f);

        domainAxis.setCategoryLabelPositions(new CategoryLabelPositions(
                new CategoryLabelPosition(), new CategoryLabelPosition(),
                leftPositionSpec, new CategoryLabelPosition()));

        // set the range axis to display integers only...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

        return chart;

    }

    /**
     * Creates a sample dataset.
     *
     * @return A sample dataset.
     */
    public static DefaultCategoryDataset createDatasetBarra(ArrayList<MovimientosCaptaciones> lista_movimientos) {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        for (int i = 0; i < lista_movimientos.size(); i++) {
            MovimientosCaptaciones movimeinto = lista_movimientos.get(i);
            dataset.setValue(movimeinto.getSaldo_final(), "Inversionistas", movimeinto.getNombre_inversionista());
        }
        return dataset;
    }

    /**
     * ************************************ DATOS generales**************************************************
     */
    protected PdfPTable Datosgenerales(String periodo) throws DocumentException {

        String fecha = Util.getFechahoy();
        PdfPTable tabla_temp = new PdfPTable(2);
        //double vlr=this.Total_pagado(lista);
        double vlr = 10;
        float[] medidaCeldas = {0.280f, 0.720f};
        tabla_temp.setWidths(medidaCeldas);

        Font fuente = new Font(Font.HELVETICA, 9);
        Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();

        // fila 1
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("INFORME MENSUAL", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase("", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase("PERIODO", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase(periodo, fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase("FECHA DE GENERACION", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setColspan(3);
        celda_temp.setPhrase(new Phrase(fecha.substring(8, 10) + " de " + Utility.NombreMes(Integer.parseInt(fecha.substring(5, 7))) + " de " + fecha.substring(0, 4), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

        return tabla_temp;
    }

    /**
     * ************************************ Movimientos**************************************************
     */
    protected PdfPTable MovimientosCierres(ArrayList<MovimientosCaptaciones> lista_movimientos) throws DocumentException {
        this.saldo_final = 0;

        double vlr = 0, ip = 0;
        PdfPTable tabla_temp = new PdfPTable(6);
        try {
            double costo_mensual = 0;

            java.awt.Color fondo = new java.awt.Color(0x33, 0x66, 0x33);
            java.awt.Color color_fuente_header = new java.awt.Color(0xFF, 0xFF, 0xFF);
            float[] medidaCeldas = {0.400f, 0.150f, 0.100f, 0.100f, 0.100f, 0.150f};
            tabla_temp.setWidths(medidaCeldas);

            Font fuente = new Font(Font.TIMES_ROMAN, 8);
            Font fuenteB = new Font(Font.TIMES_ROMAN, 8, Font.BOLD);
            Font fuente_header = new Font(Font.TIMES_ROMAN, 8, Font.BOLD);
            fuente_header.setColor(color_fuente_header);
            PdfPCell celda_temp = new PdfPCell();
            celda_temp = new PdfPCell();

            // columna 1
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Inversionista", fuente_header));
            celda_temp.setBackgroundColor(fondo);

            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Saldo Final", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Porcentaje Total", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Tasa", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Promedio Ponderado", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Tipo Inversionista", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            for (int i = 0; i < lista_movimientos.size(); i++) {
                MovimientosCaptaciones movimiento = (MovimientosCaptaciones) lista_movimientos.get(i);
                this.saldo_final = this.saldo_final + movimiento.getSaldo_final();
                costo_mensual = costo_mensual + movimiento.getPromedio_ponderado();

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase(movimiento.getNombre_inversionista(), fuente));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("$" + Util.customFormat(movimiento.getSaldo_final()), fuente));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
                tabla_temp.addCell(celda_temp);

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase(movimiento.getPorcentaje_total() + " %", fuente));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase(movimiento.getTasa_ea() + " %", fuente));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("" + Util.redondear(movimiento.getPromedio_ponderado(), 2) + " %", fuente));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase(movimiento.getInversionista().getTipo_inversionista(), fuente));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);

            }

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(" ", fuente_header));
            celda_temp.setColspan(5);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            // fila 1 totales
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Total", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("" + Util.customFormat(this.saldo_final), fuenteB));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("100%", fuenteB));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("C.Mensual", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("" + Util.redondear(costo_mensual, 2) + " %", fuenteB));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("", fuente_header));
            celda_temp.setColspan(3);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            double ea = ((Math.pow(((costo_mensual / 100) + 1), 12)) - 1) * 100;

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("EA", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("" + Util.redondear(ea, 2) + " %", fuenteB));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

        } catch (Exception e) {

            System.out.println("error al generar pdf : " + e.toString());
            e.printStackTrace();
        }
        return tabla_temp;

    }

    /**
     * consulta el ultimo movimientos de una subcuenta para recalcular interes y
     * retornarlo
     *
     * @param dstrct distrito del usuario en sesion
     * @return double con los resultados obtenidos
     * @throws Exception
     */
    public double getSaldoTotal(Inversionista inversionista) throws Exception {
        double total_saldo = 0;

        ArrayList<MovimientosCaptaciones> saldo_subcuentas = new ArrayList<MovimientosCaptaciones>();
        ArrayList<MovimientosCaptaciones> cuentas_inversionistas = consultarSubcuentasMovimientosInversionista(inversionista.getDstrct(), inversionista.getNit());// consultar ultimo movimientos del mes

        for (int i = 0; i < cuentas_inversionistas.size(); i++) {
            ArrayList<MovimientosCaptaciones> movimientos_subcuenta
                    = consultarUltimosMovimientos(inversionista.getDstrct(), cuentas_inversionistas.get(i).getNit(), String.valueOf(cuentas_inversionistas.get(i).getSubcuenta()), "N");// consultar ultimo movimiento

            Inversionista subcuenta = consultarSubcuentaInversionista(inversionista.getDstrct(), inversionista.getNit(), cuentas_inversionistas.get(i).getSubcuenta());

            MovimientosCaptaciones movimiento = new MovimientosCaptaciones();
            movimiento = this.calculosMovimientoFinal(subcuenta, movimientos_subcuenta);
            total_saldo = total_saldo + movimiento.getSaldo_final();
        }

        return total_saldo;
    }

    public MovimientosCaptaciones calculosMovimientoFinal(Inversionista inversionista, ArrayList<MovimientosCaptaciones> lista_movimientos) throws Exception {
        int i = lista_movimientos.size() - 1;
        CaptacionInversionistaService cis = new CaptacionInversionistaService(this.dao.getDatabaseName());
        MovimientosCaptaciones movimiento = new MovimientosCaptaciones();

        movimiento.setSubcuenta(inversionista.getSubcuenta());
        movimiento.setFecha(Util.getFechahoy());

        movimiento.setTasa_ea(inversionista.getTasa_ea());
        movimiento.setNit(inversionista.getNit());
        movimiento.setSubcuenta(inversionista.getSubcuenta());
        movimiento.setNombre_subcuenta(lista_movimientos.get(i).getNombre_subcuenta());

        movimiento.setSaldo_inicial(lista_movimientos.get(i).getSaldo_final());
        movimiento.setNombre_subcuenta(lista_movimientos.get(i).getNombre_subcuenta());

        if (inversionista.getTipo_interes().equals("C")) {
            movimiento.setValor_intereses(this.getInteresesHoy(inversionista, lista_movimientos.get(i).getFecha(), movimiento));
            //subtotal
            movimiento.setSubtotal(movimiento.getSaldo_inicial() + movimiento.getValor_intereses() - movimiento.getValor_retefuente() - movimiento.getValor_reteica());
            //saldo final
            movimiento.setSaldo_final(movimiento.getSubtotal());
        } else {///interes simple

            if (lista_movimientos.get(i).getRetiro() == 0) {///calcular  base de intereses
                movimiento.setBase_intereses(lista_movimientos.get(i).getSaldo_final() - lista_movimientos.get(i).getIntereses_acomulados());//*
            } else {
                movimiento.setBase_intereses(lista_movimientos.get(i).getSaldo_final());
            }
            movimiento.setValor_intereses(this.getInteresesHoy(inversionista, lista_movimientos.get(i).getFecha(), movimiento)); ///calcular  intereses

            if (lista_movimientos.get(i).getRetiro() >= lista_movimientos.get(i).getIntereses_acomulados()) {///calcular intereses acomulados
                movimiento.setIntereses_acomulados(movimiento.getValor_intereses());
            } else {
                movimiento.setIntereses_acomulados(lista_movimientos.get(i).getIntereses_acomulados() + movimiento.getValor_intereses());
            }

            movimiento.setSaldo_final(movimiento.getBase_intereses() + movimiento.getIntereses_acomulados() - movimiento.getValor_retefuente()
                    + movimiento.getValor_reteica() + movimiento.getConsignacion() - movimiento.getRetiro());
        }

        return movimiento;
    }

    public MovimientosCaptaciones calculosMovimientoRetiroRI(Inversionista inversionista, MovimientosCaptaciones movimiento_cierre) throws Exception {
        //int i = lista_movimientos.size() - 1;
        CaptacionInversionistaService cis = new CaptacionInversionistaService(this.dao.getDatabaseName());
        MovimientosCaptaciones movimiento_ri = new MovimientosCaptaciones();

        movimiento_ri.setSubcuenta(inversionista.getSubcuenta());
        movimiento_ri.setFecha(movimiento_cierre.getFecha());

        movimiento_ri.setTasa_ea(inversionista.getTasa_ea());
        movimiento_ri.setNit(inversionista.getNit());
        movimiento_ri.setSubcuenta(inversionista.getSubcuenta());
        movimiento_ri.setNombre_subcuenta(movimiento_cierre.getNombre_subcuenta());

        movimiento_ri.setSaldo_inicial(movimiento_cierre.getSaldo_final());

        if (inversionista.getTipo_interes().equals("C")) {
            // movimiento_ri.setValor_intereses(this.getInteresesHoy(inversionista, lista_movimientos.get(i).getFecha(), movimiento));
            //subtotal
            movimiento_ri.setSubtotal(movimiento_ri.getSaldo_inicial() + movimiento_ri.getValor_intereses() - movimiento_ri.getValor_retefuente() - movimiento_ri.getValor_reteica());
            //saldo final

            movimiento_ri.setRetiro(this.getValorRetiroRI(movimiento_cierre) + (movimiento_cierre.getValor_intereses() - this.getRetefuenteRI(inversionista, movimiento_cierre) - this.getReteicaRI(inversionista, movimiento_cierre)));//setiar con el valor total de la consulta - (interes ala fecha de causacion -retenciones)

            movimiento_ri.setSaldo_final(movimiento_ri.getSubtotal() - movimiento_ri.getRetiro());

        } else {///interes simple
            movimiento_ri.setRetiro(this.getValorRetiroRI(movimiento_cierre) + (movimiento_cierre.getValor_intereses() - this.getRetefuenteRI(inversionista, movimiento_cierre) - this.getReteicaRI(inversionista, movimiento_cierre)));//setiar con el valor total de la consulta - (interes ala fecha de causacion -retenciones)

            if (movimiento_cierre.getRetiro() == 0) {///calcular  base de intereses
                movimiento_ri.setBase_intereses(movimiento_cierre.getSaldo_final() - movimiento_cierre.getIntereses_acomulados());//*
            } else {
                movimiento_ri.setBase_intereses(movimiento_ri.getSaldo_final());
            }
            movimiento_ri.setValor_intereses(this.getInteresesHoy(inversionista, movimiento_cierre.getFecha(), movimiento_ri)); ///calcular  intereses

            if (movimiento_cierre.getRetiro() >= movimiento_cierre.getIntereses_acomulados()) {///calcular intereses acomulados
                movimiento_ri.setIntereses_acomulados(movimiento_ri.getValor_intereses());
            } else {
                movimiento_ri.setIntereses_acomulados(movimiento_cierre.getIntereses_acomulados() + movimiento_ri.getValor_intereses());
            }

            movimiento_ri.setSaldo_final(movimiento_ri.getBase_intereses() + movimiento_ri.getIntereses_acomulados() - movimiento_ri.getValor_retefuente()
                    + movimiento_ri.getValor_reteica() + movimiento_ri.getConsignacion() - movimiento_ri.getRetiro());
        }

        return movimiento_ri;
    }

    public double getInteresesHoy(Inversionista subcuenta, String fecha_movimiento_anterior, MovimientosCaptaciones movimiento_actual) {
        Date fechaInicial = Util.ConvertiraDate1(fecha_movimiento_anterior);//movimiento anterior
        Date fechaFinal = new Date();
        double intereses = 0;
        long numDias = 0;

        if (subcuenta.getDias_calendario() == 365)//validacion base dias  intereses
        {
            long diff = fechaFinal.getTime() - fechaInicial.getTime();
            numDias = diff / (1000 * 60 * 60 * 24);
        } else {
            numDias = Util.dias360(fechaInicial, fechaFinal);
        }

        if (subcuenta.getTipo_interes().equals("C")) {

            double exponente = ((double) numDias / subcuenta.getAno_base());
            double base = (1 + (subcuenta.getTasa_ea() / 100));
            intereses = movimiento_actual.getSaldo_inicial() * (Math.pow(base, exponente)) - movimiento_actual.getSaldo_inicial();
        } else {
            if (subcuenta.getTipo_interes().equals("S")) {
                double tasa_diaria = (subcuenta.getTasa_diaria() / 100);
                intereses = movimiento_actual.getBase_intereses() * numDias * tasa_diaria;
            }

        }
        return intereses;
    }

    private static void poneImagen(Document document, String archivo) {
        RandomAccessFileOrArray ra = null;
        boolean resultado = false;
        try {
     //      ra = new RandomAccessFileOrArray(archivo,true );
            // getTiffImage(ra, 1);
            //Image img= TiffImage.getTiffImage(ra, 1);
            Image img = Image.getInstance(archivo);
            img.scaleToFit(800, 430);

            if (img != null) {
                //Se valida la escala de la im�gen
                if (img.getScaledWidth() > 600 || img.getScaledHeight() > 800) {
                    //Se reduce la escala de la im�gen
                    img.scaleToFit(615, 825);
                }
                //Se indica la posicion donde se colocara la im�gen en el PDF
                img.setAbsolutePosition(0, 0);

                //Se inserta la im�gen en el PDF
                document.add((Element) img);

            }
            //ra.close();
        } catch (Exception e) {
            System.out.println("Error al agregar imagen al PDF " + e.getMessage());
        }
    }

    private static void poneSelloAgua(PdfWriter writer) {
        try {

            com.lowagie.text.pdf.PdfContentByte cb = writer.getDirectContent();
            //Se crea un templete para asignar la marca de agua
            com.lowagie.text.pdf.PdfTemplate template = cb.createTemplate(700, 300);

            template.beginText();
               //Inicializamos los valores para el templete
            //Se define el tipo de letra, color y tama�o
            com.lowagie.text.pdf.BaseFont bf = com.lowagie.text.pdf.BaseFont.createFont(com.lowagie.text.pdf.BaseFont.HELVETICA, com.lowagie.text.pdf.BaseFont.CP1252, com.lowagie.text.pdf.BaseFont.NOT_EMBEDDED);
            Color c = new Color(220, 220, 220);
            template.setColorFill(c);
            template.setFontAndSize(bf, 6);

            template.setTextMatrix(0, 0);
            //Se define el texto que se agregara como marca de agua
            template.showText("TEXTO MARCA AGUA");
            template.endText();

               //Se asigna el templete
            //Se asignan los valores para el texto de marca de agua
            // Se asigna el grado de inclinacion y la posicion donde aparecer� el texto
            //                                                     x    y
            cb.addTemplate(template, 1, 1, -1, 1, 50, 500);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean enviarCorreo(String from, String to, String copia, String asunto, String msg, String ruta, String archivo) {
        boolean enviado = false;
        try {

            /**
             * *********************** Envio de Correo************************
             */
            Email2 emailData = null;
            String ahora = new java.util.Date().toString();
            emailData = new Email2();
            emailData.setEmailId(12345);
            emailData.setRecstatus("A");//A
            emailData.setEmailcode("emailcode");
            emailData.setEmailfrom(from);
            emailData.setEmailto(to);
            emailData.setEmailcopyto(copia);
            emailData.setEmailHiddencopyto("");
            emailData.setEmailsubject(asunto);//"WebServiceMultiple_Fintra2" );
            emailData.setEmailbody(msg);
            emailData.setLastupdat(new Timestamp(System.currentTimeMillis()));
            emailData.setSenderName("sender name");
            emailData.setRemarks("remark2");
            emailData.setTipo("E");
            emailData.setRutaArchivo(ruta);
            emailData.setNombreArchivo(archivo);

            // EmailSendingEngineService emailSendingEngineService=new EmailSendingEngineService("smtpbar.une.net.co","fintravalores@geotechsa.com","fintra21");
            EmailSendingEngineService emailSendingEngineService = new EmailSendingEngineService();
            enviado = emailSendingEngineService.send(emailData);
            emailSendingEngineService = null;//091206
            /**
             * ***********************Fin Envio de Correo************************
             */
        } catch (Exception e) {

            System.out.println("Error  " + e.getMessage());
        }

        return enviado;
    }

    public String guardarDatosTransferencia(MovimientosCaptaciones movimiento) throws Exception {
        return dao.guardarDatosTransferencia(movimiento);
    }

    public void eliminarCuentasTransferencias(MovimientosCaptaciones movimiento) throws Exception {
        dao.eliminarCuentasTransferencias(movimiento);
    }

    public boolean getPagoIntereses(MovimientosCaptaciones movimeinto) throws Exception {
        return dao.getPagoIntereses(movimeinto);
    }

    public double getValorRetiroRI(MovimientosCaptaciones movimeinto) throws Exception {
        return dao.getValorRetiroRI(movimeinto);
    }

    public boolean ExisteDatosTrans(MovimientosCaptaciones movimeinto) throws Exception {
        return dao.ExisteDatosTrans(movimeinto);
    }

    public String actualizarDatosPagosAutomaticos(Inversionista inversionista) throws Exception {

        return dao.actualizarDatosPagosAutomaticos(inversionista);
    }

    public double getSaldoInicialMes(Inversionista subcuenta) throws Exception {
        return dao.getSaldoInicialMes(subcuenta);
    }

    /**
     * Obtiene los movimeintos iniciales
     *
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> consultarMovimientosIniciales(String dstrct, String nit, String subcuenta) throws Exception {
        return dao.consultarMovimientosIniciales(dstrct, nit, subcuenta);
    }

    public ArrayList<MovimientosCaptaciones> getInformeXInversionista(ArrayList<MovimientosCaptaciones> lista) throws Exception {
        ArrayList<MovimientosCaptaciones> lista_final = new ArrayList<MovimientosCaptaciones>();
        ArrayList<MovimientosCaptaciones> lista_movimientos = new ArrayList<MovimientosCaptaciones>();

        lista_movimientos = this.cloneList(lista);
        for (int i = 0; i < lista_movimientos.size(); i++) {
            double saldo_final = 0;

            if (this.ExisteEnLista(lista_final, lista_movimientos.get(i).getNit()) == false) {
                for (int j = 0; j < lista_movimientos.size(); j++) {

                    if (lista_movimientos.get(j).getNit().equals(lista_movimientos.get(i).getNit())) {
                        saldo_final = saldo_final + lista_movimientos.get(j).getSaldo_final();
                    }
                }
                lista_movimientos.get(i).setSaldo_final(saldo_final);
                lista_final.add(lista_movimientos.get(i));

            }
        }

        return lista_final;
    }

    public boolean ExisteEnLista(ArrayList<MovimientosCaptaciones> lista_movimientos, String nit) throws Exception {
        boolean sw = false;
        for (int j = 0; j < lista_movimientos.size(); j++) {

            if (lista_movimientos.get(j).getNit().equals(nit)) {
                sw = true;
                return sw;
            }
        }

        return sw;
    }

    /**
     * Obtiene los inversionistas y su clasificacion
     *
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<Inversionista> reportesInversionistas(String dstrct) throws Exception {

        return dao.reportesInversionistas(dstrct);
    }

    /**
     * Obtiene los inversionistas y su clasificacion
     *
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public BeanGeneral clasificacionInversionistas(String dstrct) throws Exception {
        return dao.clasificacionInversionistas(dstrct);
    }

    /**
     * Obtiene los inversionistas y su clasificacion
     *
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public BeanGeneral clasificacionInversionistasMes(String dstrct, String periodo) throws Exception {
        return dao.clasificacionInversionistasMes(dstrct, periodo);
    }

    /**
     * *********************************************informe pdf captaciones ***********************************************************
     */
    public boolean exportarInformeConsolidadoPdf(Usuario user, ArrayList<Inversionista> lista_inversionista) throws Exception {
        String url_logo = "fintrapdf.gif";
        String userlogin = user.getLogin();

        boolean generado = true;
        String directorio = "";
        String documento_detalle = "";
        ResourceBundle rb = null;
        try {
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            directorio = this.directorioArchivo(userlogin, "informeConsolidado", "pdf");
            Font fuente = new Font(Font.HELVETICA, 8);
            Font fuenteB = new Font(Font.HELVETICA, 8, Font.BOLD);
            Document documento = null;
            documento = this.createDoc("H");
            PdfWriter Pdfriter = PdfWriter.getInstance(documento, new FileOutputStream(directorio));

            documento.setFooter(this.getMyFooter());

            documento.open();
            documento.newPage();
            Image img = Image.getInstance(rb.getString("ruta") + "/images/" + url_logo);
            img.scaleToFit(130, 130);
            documento.add(img);

            documento.add(Chunk.NEWLINE);
            documento.add(new Paragraph(" 802.022.016-1", fuenteB));
            documento.add(Chunk.NEWLINE);

            /*----------------------------------DatosGenerales--------------------------------------*/
            Paragraph p;
            p = new Paragraph("Inversionistas ", fuenteB);
            p.setAlignment(Element.ALIGN_LEFT);
            documento.add(p);
            documento.add(Chunk.NEWLINE);
            /*----------------------------------lista_inversionistas--------------------------------------*/
            PdfPTable tabla_movimientos = this.ListadoInversionista(lista_inversionista);
            tabla_movimientos.setWidthPercentage(100);
            documento.add(tabla_movimientos);
            documento.add(Chunk.NEWLINE);
            documento.add(Chunk.NEWLINE);
            documento.add(Chunk.NEWLINE);

            PdfPTable tabla_datos = this.Clasificacion(this.clasificacionInversionistas(user.getDstrct()));
            tabla_datos.setWidthPercentage(50);

            documento.add(tabla_datos);
            documento.add(Chunk.NEWLINE);
            documento.add(Chunk.NEWLINE);
            //documento.newPage();

            documento.close();

        } catch (Exception e) {
            generado = false;
            System.out.println("error al generar pdf : " + e.toString());
            e.printStackTrace();
        }

        return generado;
    }

    /**
     * ************************************ Movimientos**************************************************
     */
    protected PdfPTable ListadoInversionista(ArrayList<Inversionista> lista_inversionistas) throws DocumentException {

        double vlr = 0, ip = 0;
        PdfPTable tabla_temp = new PdfPTable(6);
        try {
            double costo_mensual = 0;

            java.awt.Color fondo = new java.awt.Color(0x33, 0x66, 0x33);
            java.awt.Color color_fuente_header = new java.awt.Color(0xFF, 0xFF, 0xFF);
            float[] medidaCeldas = {0.100f, 0.430f, 0.110f, 0.120f, 0.140f, 0.100f};
            tabla_temp.setWidths(medidaCeldas);

            Font fuente = new Font(Font.TIMES_ROMAN, 8);
            Font fuenteB = new Font(Font.TIMES_ROMAN, 8, Font.BOLD);
            Font fuente_header = new Font(Font.TIMES_ROMAN, 8, Font.BOLD);
            fuente_header.setColor(color_fuente_header);
            PdfPCell celda_temp = new PdfPCell();
            celda_temp = new PdfPCell();

            // columna 1
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Nit", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Inversionista", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Saldo", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Tipo Inversionista", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Parentesco", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Computa", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            for (int i = 0; i < lista_inversionistas.size(); i++) {
                Inversionista inversionista = (Inversionista) lista_inversionistas.get(i);

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase(inversionista.getNit(), fuente));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase(inversionista.getNombre_subcuenta(), fuente));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("$ " + Util.customFormat(inversionista.getSaldo_total()), fuente));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase(inversionista.getTipo_inversionista(), fuente));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase(inversionista.getTipo_parentesco(), fuente));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase(inversionista.getComputa(), fuente));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);

            }

        } catch (Exception e) {

            System.out.println("error al generar pdf : " + e.toString());
            e.printStackTrace();
        }
        return tabla_temp;

    }

    /**
     * ************************************ Clasificacion**************************************************
     */
    protected PdfPTable Clasificacion(BeanGeneral info) throws DocumentException {
          //totales

        PdfPTable tabla_temp = new PdfPTable(2);
        try {

            java.awt.Color fondo = new java.awt.Color(0x33, 0x66, 0x33);
            java.awt.Color color_fuente_header = new java.awt.Color(0xFF, 0xFF, 0xFF);

            float[] medidaCeldas = {0.500f, 0.500F};
            tabla_temp.setWidths(medidaCeldas);

            Font fuente = new Font(Font.TIMES_ROMAN, 9);
            Font fuenteB = new Font(Font.TIMES_ROMAN, 9, Font.BOLD);
            Font fuente_header = new Font(Font.TIMES_ROMAN, 9, Font.BOLD);
            fuente_header.setColor(color_fuente_header);
            PdfPCell celda_temp = new PdfPCell();
            celda_temp = new PdfPCell();

            // columna 1
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Resumen", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setColspan(2);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Socios", fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(info.getValor_03(), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Particulares", fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(info.getValor_04(), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Familiares no Computan", fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(info.getValor_01(), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Familiares Computan", fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(info.getValor_02(), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Total Inversionistas", fuenteB));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(info.getValor_05(), fuenteB));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);
            tabla_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);

        } catch (Exception e) {

            System.out.println("error al generar pdf : " + e.toString());
            e.printStackTrace();
        }
        return tabla_temp;

    }

    /* Genera el xls del einforme consolidado de inversionistas
     * @param user Usuario que lo genera
     * @param listafacts Lista de facturas
     * @throws Exception Cuando hay un error
     */
    public boolean exportarInformeConsolidadoXls(Usuario user, ArrayList<Inversionista> lista_inversionista) throws Exception {

        String userlogin = user.getLogin();

        boolean generado = true;
        String directorio = "";
        ResourceBundle rb = null;
        try {
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");

            directorio = this.directorioArchivo(userlogin, "informeConsolidado", "xls");
            ExcelApplication excel = this.instanciar("Informe");
            excel.combinarCeldas(0, 0, 2, 4);
            excel.combinarCeldas(4, 4, 4, 5);
            fila = fila + 5;
            String ruta_img = rb.getString("ruta") + "/images/fintrapdf.gif";
            excel.insertaImagen(ruta_img, 0, 0, 10, 200, 0, 0, 1, 2);

            excel.setDataCell(fila, 0, "LISTADO INVERSIONISTAS");
            excel.setCellStyle(fila, 0, excel.getStyle("estilo2"));
            ListadoInversionistaXLS(lista_inversionista);
            ClasificacionXls(this.clasificacionInversionistas(user.getDstrct()));

            fila = 0;
            excel.saveToFile(directorio);
        } catch (Exception e) {
            generado = false;
            throw new Exception("Error al generar el archivo xls: " + e.toString());

        }

        return generado;
    }

    /**
     * Crea un tabla con los datos de todos los inversionistas
     *
     * @throws DocumentException
     * @author JPINEDO-FINTRA
     * @date 12/04/2011
     * @version 1.0
     */
    protected void ListadoInversionistaXLS(ArrayList<Inversionista> lista_inversionista) throws DocumentException, Exception {
        fila = fila + 2;
        int col = 0;
        /*---------RECORRER CADA DOCUMETO-----------*/

        excel.cambiarAnchoColumna(0, 6000);

        // fila 4
        excel.setDataCell(fila, col, "Nit");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));

        col = col + 1;
        excel.setDataCell(fila, col, "Inversionista");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 20000);

        col++;
        excel.setDataCell(fila, col, "Saldo");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 7000);

        col++;
        excel.setDataCell(fila, col, "Tipo Inversionista");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 7000);

        col++;
        excel.setDataCell(fila, col, "Parentesco");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 7000);

        col++;
        excel.setDataCell(fila, col, "Computa");
        excel.setCellStyle(fila, col, excel.getStyle("estilo2"));
        excel.cambiarAnchoColumna(col, 7000);
        fila = fila + 1;
        for (int i = 0; i < lista_inversionista.size(); i++) {
            col = 0;
            Inversionista inversionista = (Inversionista) lista_inversionista.get(i);
            excel.setDataCell(fila, col, inversionista.getNit());
            excel.setCellStyle(fila, col, excel.getStyle("estilo1"));

            col = col + 1;

            excel.setDataCell(fila, col, inversionista.getNombre_subcuenta());
            excel.setCellStyle(fila, col, excel.getStyle("estilo1"));
            col++;

            excel.setDataCell(fila, col, inversionista.getSaldo_total());
            excel.setCellStyle(fila, col, excel.getStyle("numero"));
            col++;

            excel.setDataCell(fila, col, inversionista.getTipo_inversionista());
            excel.setCellStyle(fila, col, excel.getStyle("estilo1"));
            col++;
            excel.setDataCell(fila, col, inversionista.getTipo_parentesco());
            excel.setCellStyle(fila, col, excel.getStyle("estilo1"));

            col++;
            excel.setDataCell(fila, col, inversionista.getComputa());
            excel.setCellStyle(fila, col, excel.getStyle("estilo1"));
            fila = fila + 1;

        }

    }

    /**
     * Crea un tabla con los datos de todos los inversionistas
     *
     * @throws DocumentException
     * @author JPINEDO-FINTRA
     * @date 12/04/2011
     * @version 1.0
     */
    protected void ClasificacionXls(BeanGeneral info) throws DocumentException, Exception {
        fila = fila + 2;

        excel.setDataCell(fila, 4, "Resumen ");
        excel.combinarCeldas(fila, 4, fila, 5);
        excel.setCellStyle(fila, 4, excel.getStyle("estilo2"));

        fila++;
        excel.setDataCell(fila, 4, "Socios");
        excel.setCellStyle(fila, 4, excel.getStyle("estilo2"));
        excel.setDataCell(fila, 5, info.getValor_02());
        excel.setCellStyle(fila, 5, excel.getStyle("estilo1"));

        fila++;
        excel.setDataCell(fila, 4, "Particulares");
        excel.setCellStyle(fila, 4, excel.getStyle("estilo2"));
        excel.setDataCell(fila, 5, info.getValor_03());
        excel.setCellStyle(fila, 5, excel.getStyle("estilo1"));

        fila++;
        excel.setDataCell(fila, 4, "Familiares Socios No Computan");
        excel.setCellStyle(fila, 4, excel.getStyle("estilo2"));
        excel.setDataCell(fila, 5, info.getValor_01());
        excel.setCellStyle(fila, 5, excel.getStyle("estilo1"));

        fila++;
        excel.setDataCell(fila, 4, "Familiares Socios Computan");
        excel.setCellStyle(fila, 4, excel.getStyle("estilo2"));
        excel.setDataCell(fila, 5, info.getValor_02());
        excel.setCellStyle(fila, 5, excel.getStyle("estilo1"));

        fila++;
        excel.setDataCell(fila, 4, "Total Inversionistas");
        excel.setCellStyle(fila, 4, excel.getStyle("estilo2"));
        excel.setDataCell(fila, 5, info.getValor_05());
        excel.setCellStyle(fila, 5, excel.getStyle("estilo1"));

    }

    @Override
    public void onStartPage(PdfWriter writer, Document document) {
        try {
            ResourceBundle rb = null;
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");

            poneImagen(document, rb.getString("ruta") + "/images/marca_agua.gif");
        } catch (Exception ex) {
        }
    }

    public String obtenerUltimaFechaCausacion(String dstrct, String nit, int subcuenta) throws Exception {
        return dao.obtenerUltimaFechaCausacion(dstrct, nit, subcuenta);
    }

    public ArrayList<MovimientosCaptaciones> getInformeDetalleInversionista(String dstrct, String periodo, String nit) throws Exception {
        return dao.getInformeDetalleInversionista(dstrct, periodo, nit);
    }

    public double calcularRetefuente(MovimientosCaptaciones movimiento, Inversionista inversionista) throws SQLException {
        Tipo_impuestoService TimpuestoSvc = new Tipo_impuestoService(this.dao.getDatabaseName());
        Tipo_impuesto impuestos_rfte = TimpuestoSvc.buscarImpuestoxCodigo(inversionista.getDstrct(), inversionista.getRetefuente());

        double retefuente = 0;
        retefuente = movimiento.getValor_intereses() * (Math.abs(impuestos_rfte.getPorcentaje1()) / 100);

        return retefuente;

    }

    public double calcularReteIca(MovimientosCaptaciones movimiento, Inversionista inversionista) throws SQLException {
        Tipo_impuestoService TimpuestoSvc = new Tipo_impuestoService(this.dao.getDatabaseName());
        Tipo_impuesto impuestos_rica = TimpuestoSvc.buscarImpuestoxCodigo(inversionista.getDstrct(), inversionista.getReteica());

        double reteica = 0;
        reteica = movimiento.getValor_intereses() * (Math.abs(impuestos_rica.getPorcentaje1()) / 100);

        return reteica;

    }

}
