/***************************************
* Nombre Clase ............. PrestamoService.java
* Descripci�n  .. . . . . .  Ofrece los servicios para los prestamos
* Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
* Fecha . . . . . . . . . .  09/02/2006
* versi�n . . . . . . . . .  1.0
* Copyright ...Transportes Sanchez Polo S.A.
*******************************************/


package com.tsp.operation.model.services;



import java.io.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.PrestamoDAO;
import javax.servlet.*;
import com.tsp.exceptions.*;
import java.sql.*;


public class PrestamoService {
    
    PrestamoDAO PrestamoDataAccess;
    
    List        tipoPrestamo;
    List        terceros;
    List        periodos;
    List        beneficiarios;
    List        conceptos;
    List        clasificaciones;
    List        equipos;
    
    
    List         listaPrestamos;
    List         listaAmortizaciones;
    Prestamo     prestamo;
    Prestamo     totalesPr;
    Amortizacion amortizacion;
    Amortizacion totalesAm;
    boolean      Aprobador = false;
    List         listaPrestamosMigracion;
    
    Vector prestamos;
    Vector anticipos;
    
    private PrestamoDAO list;//TMOLINA 2008-09-13
    
    public PrestamoService() {
        PrestamoDataAccess  = new PrestamoDAO();
        list                = new PrestamoDAO();//TMOLINA 2008-09-13
        prestamo            = new Prestamo();
        tipoPrestamo        = new LinkedList();
        terceros            = new LinkedList();
        periodos            = new LinkedList();
        beneficiarios       = new LinkedList();
        conceptos           = new LinkedList();
    }
    
    
    
    
        
   
     /**
     * M�todo que carga las lista
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/

    public void loadList() throws Exception{
        try{
               loadTerceros();
               loadTipoPrestamos();
               loadPeriodos();
               loadConceptosPrestamos();
               loadClasificacionesPrestamos();
               loadEquipos();
        }catch(Exception e){
            System.out.println("erro en service"+e.toString()+"__"+e.getMessage());
             throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
    
    /**
     * M�todo que carga el listado de Conceptos de Pagos
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/

    public void loadConceptosPrestamos() throws Exception{
        try{
              this.conceptos = this.PrestamoDataAccess.getTablas( this.PrestamoDataAccess.TABLA_CONCEPTO_PRESTAMO);
        }catch(Exception e){
             throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
    
    /**
     * M�todo que carga el listado de Clasificaciones de los Prestamos
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/

    public void loadClasificacionesPrestamos() throws Exception{
        try{
              this.clasificaciones = this.PrestamoDataAccess.getTablas( this.PrestamoDataAccess.TABLA_CLASIFICACION_PRESTAMO);
        }catch(Exception e){
             throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
     /**
     * M�todo que carga el listado de Terceros
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/

    public void loadTerceros() throws Exception{
        try{
              this.terceros = this.PrestamoDataAccess.getTablas( this.PrestamoDataAccess.TABLA_TERCEROS );
        }catch(Exception e){
             throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
     /**
     * M�todo que carga el listado de Tipo de Prestamos
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/
    public void loadTipoPrestamos() throws Exception{
        try{
              this.tipoPrestamo  = this.PrestamoDataAccess.getTablas( this.PrestamoDataAccess.TABLA_TIPO_PRESTAMOS );
        }catch(Exception e){
             throw new Exception( e.getMessage() );
        }
    }
    
    
    
    /**
     * M�todo que carga el listado de Periodos
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/
    public void loadPeriodos() throws Exception{
        try{
              this.periodos = this.PrestamoDataAccess.getTablas( this.PrestamoDataAccess.TABLA_PERIODOS_PAGO );
        }catch(Exception e){
             throw new Exception( e.getMessage() );
        }
    }
    
    
    /**
     * M�todo que carga el listado de Proveedores
     * @autor....... fvillacob
     * @param agencia, agencia del usuario
     * @throws...... Exception
     * @version..... 1.0.
     **/
    public void loadBeneficiarios( String agencia ) throws Exception{
        try{
              this.beneficiarios = this.PrestamoDataAccess.getProveedores( agencia );
        }catch(Exception e){
             throw new Exception( e.getMessage() );
        }
    }
    
    
        
    /**
     * M�todo que carga el listado de equpos de prestamos
     * @autor....... mfontalvo
     * @throws...... Exception
     * @version..... 1.0.
     **/

    public void loadEquipos() throws Exception{
        try{
              this.equipos = this.PrestamoDataAccess.getTablas( this.PrestamoDataAccess.TABLA_EQUIPOS_PRESTAMO);
        }catch(Exception e){
             throw new Exception( e.getMessage() );
        }
    }
    
    
    
     /**
     * M�todo que guarda el prestamo
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... Prestamo pr, String user
     * @version..... 1.0.
     **/

    public String save(Prestamo pr, String user){
        String msj = " El prestamo para " + pr.getBeneficiarioName() + " por la suma de  $ "+  UtilFinanzas.customFormat2( pr.getMonto() ) +" ha sido guardado...";
        try{
             
             if ( pr.getTipoPrestamo().equalsIgnoreCase("EQ") ){
                 double cuota_financiacion = FuncionesFinancieras2.cuotaFija(pr.getMonto(), pr.getTasa(), pr.getCuotas() );
                 pr.setCuotaFinanciacion( Util.roundByDecimal(cuota_financiacion,0) );
             }
            
             this.PrestamoDataAccess.save(pr, user);
        }catch(Exception e){
            msj = e.getMessage();
        }
        return msj;
    }
    
    
    
    
    /**
     * M�todo que guarda la amortizacion de un prestamo
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... Prestamo pr, String user
     * @version..... 1.0.
     **/
    public void saveAmortizacion(Prestamo pt, String user)throws Exception{
        try{
             this.PrestamoDataAccess.grabarAmortizaciones(pt , user);
        }catch(Exception e){
             throw new Exception( e.getMessage() );
        }
    }
    
    
    /**
     * Reliquidacion de Amortizaciones Modificadas
     * @param pt, Objeto prestamo que almacena en una lista las amortizaciones 
     * @param usuario, usuario que graba las amortizaciones
     * @throws Exception.
     */
    public void reliquidacionAmortizaciones ( Prestamo pt , String usuario ) throws Exception{
        try{
             this.PrestamoDataAccess.reliquidacionAmortizaciones( pt , usuario );
        }catch(Exception e){
             throw new Exception( e.getMessage() );
        }        
    }
    
    
    
    /**
     * M�todo que busca el valor actual del serial de prestamo
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/
    public int getSerial()throws Exception{
        int serial = 0;
        try{
             serial = this.PrestamoDataAccess.getSerial();
        }catch(Exception e){
             throw new Exception( e.getMessage() );
        }
        return serial;
    }
    
    
    /**
     * Metodo para obtener el listado de prestamos
     * @autor mfontalvo
     * @param where resticciones para el listado de prestamos
     * @throws Exception.
     */    
    public void listaPrestamos( String where) throws Exception{
        try{
              this.listaPrestamos = this.PrestamoDataAccess.obtenerPrestamos(where);
        }catch(Exception e){
             throw new Exception( e.getMessage() );
        }
    }    
    

    /**
     * Metodo para consultar un prestamo de la base de datos
     * @autor mfontalvo
     * @param distrito, istrito del prestamo
     * @param id, id del prestamo
     * @throws Exception.
     */    
    public void searchPrestamo( String distrito, int id ) throws Exception{
        try{
            String where = " WHERE p.dstrct = '"+ distrito +"' and id=" + id +" ";  
            List tmp = this.PrestamoDataAccess.obtenerPrestamos(where);
            if (!tmp.isEmpty())
                prestamo = (Prestamo) tmp.get(0);
            else
                prestamo = null;
        }catch(Exception e){
             throw new Exception( e.getMessage() );
        }
    }    

    
    
    
    
    /**
     * Metodo para obtener el listado de amorizaciones
     * @autor mfontalvo
     * @param where resticciones para el listado de amortizaiones
     * @throws Exception.
     */    
    public void listaAmortizaciones( String where ) throws Exception{
        try{
              this.listaAmortizaciones = this.PrestamoDataAccess.obtenerAmortizaciones(where);
        }catch(Exception e){
             throw new Exception( e.getMessage() );
        }
    }
    
    
    /**
     * Metodo para consultar un prestamo de la base de datos
     * @autor mfontalvo
     * @param distrito, distrito del prestamo
     * @param prestamo, prestamo a consultar
     * @throws Exception.
     */    
    public void searchAmortizacionPorPrestamo( String distrito, int prestamo ) throws Exception{
        try{
            String where = " WHERE a.dstrct = '"+ distrito +"' and a.prestamo=" + prestamo +" ";  
            this.listaAmortizaciones  (where);
            List tmp = this.getListaAmortizaciones();
            if (!tmp.isEmpty())
                amortizacion = (Amortizacion) tmp.get(0);
            else
                amortizacion = null;
        }catch(Exception e){
             throw new Exception( e.getMessage() );
        }
    }   
    
    /**
     * Metodo para extraer la amortizaciones transferidas en un
     * rango de fecha.
     * @autor mfontalvo
     * @param distrito, distrito del prestamo
     * @param fechaInicial, Rango inicial para la fecha de transferencia
     * @param fechafinal, Rango final para la fecha de transferencia
     * @param tercero, tercero a consultar
     * @throws Exception.
     */    
    public void searchAmortizacionTransferidas( String distrito, String fechaInicial, String fechaFinal, String tercero ) throws Exception{
        try{
            String where = "  , fin.prestamo b " +
                           "  WHERE  a.dstrct = '"+ distrito +"' "+
                           "     and a.fecha_transferencia between '" + fechaInicial + "' and '"+ fechaFinal +"' " +
                           "     and a.tercero  = '"+ tercero +"' " +
                           "     and b.dstrct = a.dstrct      " +
                           "     and b.id     = a.prestamo    " +
                           "     and b.clasificacion = '01'   " ;  
            this.listaAmortizaciones  (where);
            listaAmortizaciones = this.getListaAmortizaciones();

        }catch(Exception e){
             throw new Exception( e.getMessage() );
        }
    }     
    
    
    
    /**
     * Metodo para consultar los prestamos de equipos pendientes por migrar
     * @autor mfontalvo
     * @param distrito, distrito del prestamo
     * @throws Exception.
     */    
    public void searchPrestamosEQPendientesPorMigrar( String distrito ) throws Exception{
        try{
            String where = " WHERE p.dstrct = '"+ distrito +"' and p.tipoprestamo = 'EQ' and p.fecha_migracion = '0099-01-01 00:00:00' and p.aprobado = 'S'";  
            this.listaPrestamosMigracion = this.PrestamoDataAccess.obtenerPrestamos(where);
        }catch(Exception e){
             throw new Exception( e.getMessage() );
        }
    } 
    
    
    /**
     * Metodo para consultar los prestamos de equipos pendientes por migrar
     * @autor mfontalvo
     * @param distrito, distrito del prestamo
     * @throws Exception.
     */    
    public void searchPrestamosEQPendientesPorMigrar( Vector idPrestamo ) throws Exception{
        try{
            String ids = "";
            for (int i = 0; i < idPrestamo.size(); i++ ){
                ids +=  (!ids.equals("")?",":"") + "'"+ idPrestamo.get(i) +"'";
            }
            
            String where = " WHERE p.tipoprestamo = 'EQ' and p.fecha_migracion = '0099-01-01 00:00:00' and p.aprobado = 'S' AND p.id IN ( "+ ids +" ) ";  
            this.listaPrestamosMigracion = this.PrestamoDataAccess.obtenerPrestamos(where);
        }catch(Exception e){
             throw new Exception( e.getMessage() );
        }
    }
    
    
    /**
     * Metodo para consultar los prestamos de equipos pendientes por migrar
     * @autor mfontalvo
     * @param distrito, distrito del prestamo
     * @throws Exception.
     */    
    public void searchPrestamosEQMigrados( String tercero, String finicial, String ffinal ) throws Exception{
        try{
            String where = " WHERE p.tipoprestamo = 'EQ' and p.fecha_migracion != '0099-01-01 00:00:00' p.tercero = '"+ tercero +"' and p.creation_date between '"+ finicial +"' and '"+ ffinal +"' ";  
            this.listaPrestamos = this.PrestamoDataAccess.obtenerPrestamos(where);
        }catch(Exception e){
             throw new Exception( e.getMessage() );
        }
    }    
    
    /**
     * Metodo que actualiza el estado de aprobado de un prestamo y genera las amortizaciones 
     * del mismo prestamo
     * @autor mfontalvo
     * @param usuario, usuario que aprueba el prestamo
     * @param pt, Prestamo a aprobar
     * @throws Exception.
     */    
    public void aprobarPrestamo ( Prestamo pt, String usuario ) throws Exception{
        try{
              this.PrestamoDataAccess.aprobarPrestamo(usuario, pt.getDistrito(), pt.getId() );
              this.PrestamoDataAccess.grabarAmortizaciones(pt, usuario);
        }catch(Exception e){
             throw new Exception( e.getMessage() );
        }
    }     

    

    /**
     * Metodo que actualiza el el interes de un prestamo
     * @autor mfontalvo
     * @param pt, datos del prestamo
     * @throws Exception.
     */
    public void actualizarInteresPrestamo (Prestamo pt, String usuario)throws Exception{
        try{
            this.PrestamoDataAccess.actualizarInteresPrestamo(pt.getIntereses(), usuario, pt.getDistrito(), pt.getId());
        }catch(Exception e){
             throw new Exception( e.getMessage() );
        }        
    }
    
    
    /**
     * Metodo que actualiza el numero de cuotas de un prestamo
     * @autor mfontalvo
     * @param pt, datos del prestamo
     * @throws Exception.
     */
    public void actualizarCuotasPrestamo (Prestamo pt, String usuario)throws Exception{
        try{
            this.PrestamoDataAccess.actualizarCuotasPrestamo(pt.getCuotas(), usuario, pt.getDistrito(), pt.getId());
        }catch(Exception e){
             throw new Exception( e.getMessage() );
        }        
    }    
    
    /**
     * Metodo que actualiza el estado de aprobado de un prestamo y genera las amortizaciones 
     * del mismo prestamo
     * @autor mfontalvo
     * @param distrito, dstrito del prestamo
     * @param id, id del prestamo
     * @throws Exception.
     */    
    public void eliminarPrestamo ( String distrito, int id ) throws Exception{
        try{
	   this.PrestamoDataAccess.eliminarPrestamo( distrito, id );
        }catch(Exception e){
             throw new Exception( e.getMessage() );
        }
    }     

        
    
    /**
     * Busca la amortizaciones tranferidas y sin confirmacion de mims, luego
     * busca en mims los datos de esas amortizaciones y al final Actualiza los 
     * datos de amortizacion que estan en mism en sot  
     * @autor mfontalvo
     * @param distrito, distrito del prestamo
     * @param fechaInicial, Rango inicial para la fecha de transferencia
     * @param fechafinal, Rango final para la fecha de transferencia
     * @param tercero, tercero a consultar
     * @param usuario, usuario de actualizacion de prestamos
     * @throws Exception.
     */    
    public void actualizarAmortizacionesTransferidas ( String distrito, String fechaInicial, String fechaFinal, String tercero, String usuario ) throws Exception{
        try{
            
            searchAmortizacionTransferidas(distrito, fechaInicial, fechaFinal, tercero);
            if (!listaAmortizaciones.isEmpty()){
                listaAmortizaciones = this.PrestamoDataAccess.verificacionMims(listaAmortizaciones );
                this.PrestamoDataAccess.actualizarAmortizaciones( listaAmortizaciones, usuario );
            }
        }catch (Exception ex){
            throw new Exception( ex.getMessage() );
        }
    }

    
    /**
     * Metodo para generarTotales
     * @autor mfontalvo.
     * @return Prestamo, prestamo que alamacena los totales
     * @throws Exception.
     */
    public Prestamo generarTotalesPrestamo () throws Exception {
        totalesPr = null;
        if ( listaPrestamos!=null ) {
            totalesPr = new Prestamo();
            Iterator it = listaPrestamos.iterator();
            while (it.hasNext()){
                Prestamo pt = (Prestamo) it.next();
                
                totalesPr.setMonto            
                    ( totalesPr.getMonto() + pt.getMonto());
                totalesPr.setIntereses        
                    ( totalesPr.getIntereses() + pt.getIntereses() );
                totalesPr.setCapitalDescontado
                    ( totalesPr.getCapitalDescontado() + pt.getCapitalDescontado() );
                totalesPr.setInteresDescontado
                    ( totalesPr.getInteresDescontado() + pt.getInteresDescontado() );
                totalesPr.setValorDescontado
                    ( totalesPr.getValorDescontado() + pt.getValorDescontado() );
                totalesPr.setValorPagado
                    ( totalesPr.getValorPagado()     + pt.getValorPagado() );
                totalesPr.setValorMigrado
                    ( totalesPr.getValorMigrado()    + pt.getValorMigrado() );
                totalesPr.setValorRegistrado
                    ( totalesPr.getValorRegistrado() + pt.getValorRegistrado() );
                totalesPr.setSaldo_prestamo
                    ( totalesPr.getSaldo_prestamo()  + pt.getSaldo_prestamo() );
            }
        }
        return totalesPr;
    }
        
    
    /**
     * Metodo para generarTotales
     * @autor mfontalvo.
     * @return Amortizacion, amortizacion que almacena los totales
     * @throws Exception.
     */
    public Amortizacion generarTotalesAmortizaciones () throws Exception {
        totalesAm = null;
        if (listaAmortizaciones!=null){
            totalesAm = new Amortizacion();
            Iterator it = listaAmortizaciones.iterator();
            while (it.hasNext()){
                Amortizacion am = (Amortizacion) it.next();
                
                totalesAm.setCapital    
                    ( totalesAm.getCapital() + am.getCapital() );
                totalesAm.setInteres
                    ( totalesAm.getInteres() + am.getInteres() );
                totalesAm.setTotalAPagar    
                    ( totalesAm.getTotalAPagar() + am.getTotalAPagar() );
                totalesAm.setValorDescuento  
                    ( totalesAm.getValorDescuento() + am.getValorDescuento() );
                totalesAm.setValorPagoTercero
                    ( totalesAm.getValorPagoTercero() + am.getValorPagoTercero());
                
            }
            
        }
        return totalesAm;
    }
    
    
    
    /**
     * Actualizacion datos de migracion de prestamos
     * @autor mfontalvo.
     * @throws Exception.
     */
    public String updateFechaMigracionPrestamo( String dstrct, int id, String factura_inicial, String factura_financiacion, String factura_monitoreo, String factura_inicial_monitoreo ) throws Exception{
        return this.PrestamoDataAccess.updateFechaMigracionPrestamo(dstrct, id, factura_inicial, factura_financiacion, factura_monitoreo, factura_inicial_monitoreo);
    }
    
    /**
     * Metodo para obtener el listado de prestamos
     * @autor mfontalvo
     * @param where resticciones para el listado de prestamos
     * @return Lista de prestamos.
     * @throws Exception.
     */
    public Vector obtenerPrestamosLiquidacion ( String beneficiario ) throws Exception{
        return this.PrestamoDataAccess.obtenerPrestamosLiquidacion( beneficiario );
    }
    
    /**
     * Verifica si un usuario es aprobador 
     * @autor mfontalvo
     * @param distrito, distrito del usuario
     * @param usuario, usuario a verificar
     * @throws Exception.
     */    
    public void isUsuarioAprobadorPrestamo (String distrito, String usuario) throws Exception{
        try{
            Aprobador = this.PrestamoDataAccess.isUsuarioAprobadorPrestamo(distrito, usuario);
        }catch (Exception ex){
            throw new Exception( ex.getMessage() );
        }
    }
    
    
    
    
    
        
    /**
     * Metodo para guardar las amortizaciones de un prestamo en la base de datos
     * @autor mfontalvo
     * @param pt, Objeto prestamo que alamacena en una lista las amortizaciones 
     * @param usuario, usuario que graba las amortizaciones
     * @throws Exception.
     */
    public String updatePrestamoSQL ( Prestamo pt , String usuario) throws Exception{
        return this.PrestamoDataAccess.updatePrestamoSQL(pt, usuario);
    }
    
    
     /**
     * Metodo para guardar las amortizaciones de un prestamo en la base de datos
     * @autor mfontalvo
     * @param pt, Objeto prestamo que alamacena en una lista las amortizaciones 
     * @param usuario, usuario que graba las amortizaciones
     * @throws Exception.
     */
    public String grabarAmortizacionesSQL ( Prestamo pt , String usuario) throws Exception{
        return this.PrestamoDataAccess.grabarAmortizacionesSQL(pt, usuario);
    }
    
    /**
     * Metodo para verificar si un prestamo tiene prestamos tipo
     * abonos prestamos.
     * @autor mfontalvo
     * @param where resticciones para el listado de prestamos
     * @return Lista de prestamos.
     * @throws Exception.
     */
    public boolean existePrestamosTipoAbonos ( String beneficiario ) throws Exception{
        return this.PrestamoDataAccess.existePrestamosTipoAbonos( beneficiario );
    }
    
    
    
    
    
    /**
     * Metodo para extraer los pagos hechos en un rango de fechas relacionados 
     * a los prestamos de abonos tranferencias
     * @autor mfontalvo
     * @param fecha inicial de pago
     * @param fecha final de pago
     * @return Lista de cuotas.
     * @throws Exception.
     */
    public void getPagosPrestamosAbonosTransferencias ( String fecha_inicial, String fecha_final) throws Exception{
        prestamos = this.PrestamoDataAccess.getPagosPrestamosAbonosTransferencias(fecha_inicial, fecha_final);
    }
    
    
    
    /**
     * Getter for property prestamo.
     * @return Value of property prestamo.
     */
    public Prestamo getPrestamo() {
        return prestamo;
    }    
    
    /**
     * Setter for property prestamo.
     * @param prestamo New value of property prestamo.
     */
    public void setPrestamo(Prestamo prestamo) {
        this.prestamo = prestamo;
    }    
    
    
    
    
    
    
    /**
     * Getter for property terceros.
     * @return Value of property terceros.
     */
    public java.util.List getTerceros() {
        return terceros;
    }    
    
    /**
     * Setter for property terceros.
     * @param terceros New value of property terceros.
     */
    public void setTerceros(java.util.List terceros) {
        this.terceros = terceros;
    }    
    
    
    
    
    
    /**
     * Getter for property tipoPrestamo.
     * @return Value of property tipoPrestamo.
     */
    public java.util.List getTipoPrestamo() {
        return tipoPrestamo;
    }    
    
    /**
     * Setter for property tipoPrestamo.
     * @param tipoPrestamo New value of property tipoPrestamo.
     */
    public void setTipoPrestamo(java.util.List tipoPrestamo) {
        this.tipoPrestamo = tipoPrestamo;
    }    
    
    
    
    
    
    
    /**
     * Getter for property periodos.
     * @return Value of property periodos.
     */
    public java.util.List getPeriodos() {
        return periodos;
    }    
    
    /**
     * Setter for property periodos.
     * @param periodos New value of property periodos.
     */
    public void setPeriodos(java.util.List periodos) {
        this.periodos = periodos;
    }    
    
    
    
    
    
    
    /**
     * Getter for property beneficiarios.
     * @return Value of property beneficiarios.
     */
    public java.util.List getBeneficiarios() {
        return beneficiarios;
    }    
    
    /**
     * Setter for property beneficiarios.
     * @param beneficiarios New value of property beneficiarios.
     */
    public void setBeneficiarios(java.util.List beneficiarios) {
        this.beneficiarios = beneficiarios;
    }    
    
    /**
     * Getter for property listaPrestamo.
     * @return Value of property listaPrestamo.
     */
    public java.util.List getListaPrestamos() {
        return listaPrestamos;
    }
    
    /**
     * Setter for property listaPrestamo.
     * @param listaPrestamo New value of property listaPrestamo.
     */
    public void setListaPrestamos(java.util.List listaPrestamos) {
        this.listaPrestamos = listaPrestamos;
    }
    
    /**
     * Getter for property conceptos.
     * @return Value of property conceptos.
     */
    public java.util.List getConceptos() {
        return conceptos;
    }
    
    /**
     * Setter for property conceptos.
     * @param conceptos New value of property conceptos.
     */
    public void setConceptos(java.util.List conceptos) {
        this.conceptos = conceptos;
    }

    /**
     * Getter for property listaAmortizaciones.
     * @return Value of property listaAmortizaciones.
     */
    public java.util.List getListaAmortizaciones() {
        return listaAmortizaciones;
    }
    
    /**
     * Setter for property listaAmortizaciones.
     * @param listaAmortizaciones New value of property listaAmortizaciones.
     */
    public void setListaAmortizaciones(java.util.List listaAmortizaciones) {
        this.listaAmortizaciones = listaAmortizaciones;
    }
    
    /**
     * Getter for property amortizacion.
     * @return Value of property amortizacion.
     */
    public com.tsp.operation.model.beans.Amortizacion getAmortizacion() {
        return amortizacion;
    }
    
    /**
     * Setter for property amortizacion.
     * @param amortizacion New value of property amortizacion.
     */
    public void setAmortizacion(com.tsp.operation.model.beans.Amortizacion amortizacion) {
        this.amortizacion = amortizacion;
    }
    
    /**
     * Getter for property Aprobador.
     * @return Value of property Aprobador.
     */
    public boolean isAprobador() {
        return Aprobador;
    }    

    /**
     * Setter for property Aprobador.
     * @param Aprobador New value of property Aprobador.
     */
    public void setAprobador(boolean Aprobador) {
        this.Aprobador = Aprobador;
    }    
    
    
    
    
    /**
     * Getter for property clasificaciones.
     * @return Value of property clasificaciones.
     */
    public java.util.List getClasificaciones() {
        return clasificaciones;
    }
    
    /**
     * Setter for property clasificaciones.
     * @param clasificaciones New value of property clasificaciones.
     */
    public void setClasificaciones(java.util.List clasificaciones) {
        this.clasificaciones = clasificaciones;
    }
    
    /**
     * Getter for property totalesPr.
     * @return Value of property totalesPr.
     */
    public com.tsp.operation.model.beans.Prestamo getTotalesPr() {
        return totalesPr;
    }    

    /**
     * Setter for property totalesPr.
     * @param totalesPr New value of property totalesPr.
     */
    public void setTotalesPr(com.tsp.operation.model.beans.Prestamo totalesPr) {
        this.totalesPr = totalesPr;
    }    
    
    /**
     * Getter for property totalesAm.
     * @return Value of property totalesAm.
     */
    public com.tsp.operation.model.beans.Amortizacion getTotalesAm() {
        return totalesAm;
    }
    
    /**
     * Setter for property totalesAm.
     * @param totalesAm New value of property totalesAm.
     */
    public void setTotalesAm(com.tsp.operation.model.beans.Amortizacion totalesAm) {
        this.totalesAm = totalesAm;
    }
    
    /**
     * Getter for property equipos.
     * @return Value of property equipos.
     */
    public java.util.List getEquipos() {
        return equipos;
    }
    
    /**
     * Setter for property equipos.
     * @param equipos New value of property equipos.
     */
    public void setEquipos(java.util.List equipos) {
        this.equipos = equipos;
    }
    
    /**
     * Getter for property prestamos.
     * @return Value of property prestamos.
     */
    public java.util.Vector getPrestamos() {
        return prestamos;
    }
    
    /**
     * Setter for property prestamos.
     * @param prestamos New value of property prestamos.
     */
    public void setPrestamos(java.util.Vector prestamos) {
        this.prestamos = prestamos;
    }
    
    /**
     * Getter for property anticipos.
     * @return Value of property anticipos.
     */
    public java.util.Vector getAnticipos() {
        return anticipos;
    }
    
    /**
     * Setter for property anticipos.
     * @param anticipos New value of property anticipos.
     */
    public void setAnticipos(java.util.Vector anticipos) {
        this.anticipos = anticipos;
    }
    
    /**
     * Getter for property listaPrestamosMigracion.
     * @return Value of property listaPrestamosMigracion.
     */
    public java.util.List getListaPrestamosMigracion() {
        return listaPrestamosMigracion;
    }
    
    /**
     * Setter for property listaPrestamosMigracion.
     * @param listaPrestamosMigracion New value of property listaPrestamosMigracion.
     */
    public void setListaPrestamosMigracion(java.util.List listaPrestamosMigracion) {
        this.listaPrestamosMigracion = listaPrestamosMigracion;
    }
    
    /**
     * Listado de Prestamos a Transferir, los que estan aprobados unicamente.
     * @autor TMolina 2008-09-13
     */
    public void getPrestamosAprobados()throws ServletException, InformationException, Exception{
        try{ 
            list.ListadoPrestamoAprobados();
         }catch(SQLException e){
            throw new SQLException (e.getMessage ());
         }
    }
    
    /**
     * Listado de Prestamos a Transferir, los que estan aprobados unicamente.
     * @autor TMolina 2008-09-13
     */
    public java.util.Vector getListPrestamo(){
        return  list.getDatosPrestamo();
    }
    
    /**
    * Actualiza los valores de comision y 4 x mil de un determinado prestamo
    * @param codP Codigo del Prestamo
    * @param com comision
    * @param nomB Banco
    * @param codB sucursal
    * @param cuatroxmil Valor cuatro x mil
    * @autor TMolina 2008-09-13
    * @throws ServletException, InformationException, Exception
    */
    public void updateValoresPrestamo(String codP, String com, String nomB, String codB, String cuatroxmil)throws ServletException, InformationException,Exception{
        try{ 
            list.updateValoresPrestamo(codP,com,nomB,codB,cuatroxmil);  
         }
         catch(SQLException e){
            throw new SQLException (e.getMessage ());
         }
    }
    
    /**
    * Atualiza el prestamo para indicar que fue transferido al beneficiario.
    * @param cod Codigo del Prestamo
    * @autor tmolina 2008-09-13
    * @throws ServletException, InformationException, Exception
    */
    public void upPrestamo(String cod)throws ServletException, InformationException,Exception{
        try{ 
            list.upPrestamo(cod);  
         }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
    /**
    * Inicializa la lista de los nits.
    * @autor tmolina 2008-09-13
    */
    public void inicializarNits(){
        list.inicializarNits();
    }
    
      /** 
      * Metodo: save, permite crear el query para insertar el egreso, la cxp y la cxc correspondiente.
      * @param cod: codigo del prestamo
      * @param user: usuario en sesion.
      * @autor : Tmolina 2008-09-13
      * @version : 1.0
      * @throws Exception
      */
    public String save (String cod, String user) throws Exception {
        try{
            return list.save (cod, user);
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception("ERROR EN SAVE DOCS PRESTAMO " + e.getMessage());
        }
    }
    
    /**
     * Metodo para obtener el listado de prestamos
     * @autor tmolina
     * @param beneficiario beneficiario del prestamo
     * @return Lista de prestamos.
     * @throws Exception.
     */
    public Vector obtenerPrestamosLiquidacionOrderBy ( String beneficiario, String fecha_ultimo_anticipo ) throws Exception{
        return this.PrestamoDataAccess.obtenerPrestamosLiquidacionOrderBy( beneficiario, fecha_ultimo_anticipo );
    }
    
    /**
     * Metodo para guardar las amortizaciones de un prestamo en la base de datos
     * @autor tmolina
     * @param pt, Objeto prestamo que alamacena en una lista las amortizaciones 
     * @param usuario, usuario que graba las amortizaciones
     * @param fecha_ultima_liquidacion, fecha de la ultima liquidacion
     * @throws Exception.
     */
    public String grabarAmortizacionesII ( Prestamo pt , String usuario, String fecha_ultima_liquidacion) throws Exception{
        return this.PrestamoDataAccess.grabarAmortizacionesII(pt, usuario, fecha_ultima_liquidacion);
    }
}
