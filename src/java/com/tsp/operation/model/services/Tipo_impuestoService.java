/********************************************************************
 *      Nombre Clase.................   Tipo_impuestoService.java
 *      Descripci�n..................   Service del archivo tipo_de_impuesto
 *      Autor........................   Ing. Juan Manuel Escand�n
 *      Fecha........................   30 de septiembre de 2005, 09:10 AM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.sql.*;
/**
 *
 * @author  JuanM
 */
public class Tipo_impuestoService {
    private Tipo_impuestoDAO   TIDataAccess;
    private List               ListaTI;
    private Tipo_impuesto      Datos;
    
    private String tipoI = "";
    private String fagencias = "";
    private String fconcepto = "";
    
    /** Creates a new instance of Tipo_impuestoService */
    public Tipo_impuestoService() {
        TIDataAccess = new Tipo_impuestoDAO();
        ListaTI = new LinkedList();
    }    
    public Tipo_impuestoService(String dataBaseName) {
        TIDataAccess = new Tipo_impuestoDAO(dataBaseName);
        ListaTI = new LinkedList();
    }    
   
    
    /**
     * Ingresa un registro en el archivotipo_de_impuesto de tipo ICA.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param concepto Concepto
     * @param descripcion Descripci�n
     * @param fecha_vigencia Fecha de vigencia
     * @param porcentaje1 Porcentaje 1
     * @param porcentaje2 Porcentaje 2
     * @param cod_cuenta_contable C�digo de la cuenta contable
     * @param agencia C�digo de la agencia
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void INSERTRICA( String dstrct, String codigo_impuesto, String tipo_impuesto, String concepto, String descripcion, String fecha_vigencia, double porcentaje1, String cod_cuenta_contable, String agencia, String login, String signo )  throws Exception {
        try{
            TIDataAccess.INSERTRICA(dstrct, codigo_impuesto, tipo_impuesto, concepto, descripcion, fecha_vigencia, porcentaje1, cod_cuenta_contable, agencia, login, signo);
        }
        catch(Exception e){
            throw new Exception("Error en INSERTRICA [Tipo_impuestoService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Ingresa un registro en el archivotipo_de_impuesto de tipo RFTE.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param concepto Concepto
     * @param fecha_vigencia Fecha de vigencia
     * @param porcentaje1 Porcentaje 1
     * @param porcentaje2 Porcentaje 2
     * @param cod_cuenta_contable C�digo de la cuenta contable
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void INSERTRFTE( String dstrct, String codigo_impuesto, String tipo_impuesto, String concepto, String descripcion, String fecha_vigencia, double porcentaje1, String cod_cuenta_contable, String login, String signo )  throws Exception {
        try{
            TIDataAccess.INSERTRFTE(dstrct, codigo_impuesto, tipo_impuesto, concepto, descripcion, fecha_vigencia, porcentaje1, cod_cuenta_contable, login, signo);
        }
        catch(Exception e){
            throw new Exception("Error en INSERTRFTE [Tipo_proveedorService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Actualiza el estado de un registro en el archivotipo_de_impuesto de tipo RICA.
     * @autor Ing. Juan Manuel Escand�n
     * @param reg_status Estado del registro
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @param concepto Concepto
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void UPDATEESTADOIVA(String reg_status, String dstrct, String codigo_impuesto,  String fecha_vigencia, String login ) throws Exception {
        try{
            TIDataAccess.UPDATEESTADOIVA(reg_status, dstrct, codigo_impuesto, fecha_vigencia, login);
        }
        catch(Exception e){
            throw new Exception("Error en UPDATEESTADOIVA [Tipo_impuestoService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Actualiza el estado de un registro en el archivotipo_de_impuesto de tipo RICA.
     * @autor Ing. Juan Manuel Escand�n
     * @param reg_status Estado del registro
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @param concepto Concepto
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void UPDATEESTADORICA(String reg_status, String dstrct, String codigo_impuesto,  String fecha_vigencia, String agencia, String concepto, String login ) throws Exception {
        try{
            TIDataAccess.UPDATEESTADORICA(reg_status, dstrct, codigo_impuesto, fecha_vigencia, agencia, concepto, login);
        }
        catch(Exception e){
            throw new Exception("Error en UPDATEESTADORICA [Tipo_impuestoService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Actualiza el estado de un registro en el archivotipo_de_impuesto de tipo RFTE.
     * @autor Ing. Juan Manuel Escand�n
     * @param reg_status Estado del registro
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @param concepto Concepto
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void UPDATEESTADORFTE(String reg_status, String dstrct, String codigo_impuesto,  String fecha_vigencia, String concepto, String login ) throws Exception {
        try{
            TIDataAccess.UPDATEESTADORFTE(reg_status, dstrct, codigo_impuesto, fecha_vigencia, concepto, login);
        }
        catch(Exception e){
            throw new Exception("Error en UPDATEESTADORFTE [Tipo_impuestoService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Elimina un registro en el archivotipo_de_impuesto de tipo IVA.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void DELETEIVA(String dstrct, String codigo_impuesto, String fecha_vigencia ) throws Exception {
        try{
            TIDataAccess.DELETEIVA(dstrct, codigo_impuesto, fecha_vigencia);
        }
        catch(Exception e){
            throw new Exception("Error en DELETEIVA [Tipo_impuestoService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Elimina un registro en el archivotipo_de_impuesto de tipo RICA.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @param agencia C�digo de la agencia
     * @param concepto Concepto
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void DELETERICA(String dstrct, String codigo_impuesto,  String fecha_vigencia, String agencia, String concepto ) throws Exception {
        try{
            TIDataAccess.DELETERICA(dstrct, codigo_impuesto, fecha_vigencia, agencia, concepto);
        }
        catch(Exception e){
            throw new Exception("Error en DELETERICA [Tipo_proveedorService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Elimina un registro en el archivotipo_de_impuesto de tipo RFTE.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void DELETERFTE(String dstrct, String codigo_impuesto,  String fecha_vigencia,  String concepto ) throws Exception {
        try{
            TIDataAccess.DELETERFTE(dstrct, codigo_impuesto, fecha_vigencia, concepto);
        }
        catch(Exception e){
            throw new Exception("Error en DELETERFTE [Tipo_impuestoService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Elimina un registro en el archivo tipo_de_impuesto de tipo IVA.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param tp Tipo de impuestp
     * @param ag C�digo de la agencia
     * @param concepto Concepto
     * @returns Lista de los registro obtenidos del archivo tipo_de_impuesto
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void LIST(String tp, String ag, String concepto) throws Exception {
        try{
            this.ReiniciarLista();
            this.ListaTI = TIDataAccess.LIST(tp,ag,concepto);
        }
        catch(Exception e){
            throw new Exception("Error en LIST [Tipo_impuestoService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Obtiene los registros del archivo tipo_de_impuesto por el tipo.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param codigo  C�digo del impuesto
     * @param fecha Fecha de vigencia
     * @param concepto Concepto
     * @param agencia C�digo de la agencia
     * @returns Lista de los registro obtenidos del archivo tipo_de_impuesto
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void SEARCH_TI(String dstrct, String codigo, String fecha, String concepto, String agencia ) throws Exception {
        try{
            this.ReiniciarDato();
            this.Datos = TIDataAccess.SEARCH_TI(dstrct, codigo, fecha, concepto, agencia);
        }
        catch(Exception e){
            throw new Exception("Error en SEARCH [Tipo_impuestoService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Obtiene los registros del archivo tipo_de_impuesto mediante filtros especificos.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param tipo Tipo de impuesto
     * @param codigo  C�digo del impuesto
     * @returns Lista de los registro obtenidos del archivo tipo_de_impuesto
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void SEARCH(String dstrct, String tipo, String codigo ) throws Exception {
        try{
            this.ReiniciarDato();
            this.Datos = TIDataAccess.SEARCH(dstrct,tipo,codigo);
        }
        catch(Exception e){
            throw new Exception("Error en SEARCH [Tipo_impuestoService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Actualiza un registro en el archivotipo_de_impuesto de tipo RICA.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param tipo Tipo de impuesto
     * @param codigo Nuevo c�digo del impuesto
     * @param fechav Nueva fecha de vigencia
     * @param concepto Nuevo concepto
     * @param descripcion Descripci�n
     * @param porcentaje1 Porcentaje 1
     * @param porcentaje2 Porcentaje 2
     * @param cuenta C�digo de la cuenta contable
     * @param codigoi C�digo del impuesto
     * @param fv Fecha de vigencia
     * @param concept Concepto
     * @param agencia C�digo de la agencia
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void UPDATERICA(String tipo, String codigo, String concepto, String descripcion, String fechav, String porcentaje1, String cuenta, String ag, String codigoi, String fv, String concep, String agencia, String login, String signo  ) throws Exception {
        try{
            TIDataAccess.UPDATERICA(tipo, codigo, concepto, descripcion, fechav, porcentaje1, cuenta, ag, codigoi, fv, concep, agencia, login, signo);
        }
        catch(Exception e){
            throw new Exception("Error en UPDATERICA [Tipo_impuestoService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Actualiza un registro en el archivotipo_de_impuesto de tipo RFTE.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param tipo Tipo de impuesto
     * @param codigo Nuevo c�digo del impuesto
     * @param fechav Nueva fecha de vigencia
     * @param concepto Nuevo concepto
     * @param descripcion Descripci�n
     * @param porcentaje1 Porcentaje 1
     * @param porcentaje2 Porcentaje 2
     * @param cuenta C�digo de la cuenta contable
     * @param codigoi C�digo del impuesto
     * @param fv Fecha de vigencia
     * @param concept Concepto
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void UPDATERFTE(String tipo, String codigo, String concepto, String fechav, String porcentaje1, String porcentaje2, String cuenta, String codigoi, String fv, String concep, String login, String signo  ) throws Exception {
        try{
            TIDataAccess.UPDATERFTE(tipo, codigo, concepto, fechav, porcentaje1, porcentaje2, cuenta, codigoi, fv, concep, login, signo);
        }
        catch(Exception e){
            throw new Exception("Error en UPDATEIVA [Tipo_impuestoService]...\n"+e.getMessage());
        }
    }
    
    public void ReiniciarDato(){
        this.Datos = null;
    }
    public void ReiniciarLista(){
        this.ListaTI = null;
    }
    
    public Tipo_impuesto getDato(){
        return this.Datos;
    }
    
    public List getList(){
        return this.ListaTI;
    }
    
    
      /**
     * Este m�todo genera un objeto tipo Tipo_Impuesto dado los parametros dados
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @param tipoImpuesto Tipo de impuesto 
     * @param codigoImpuesto C�digo del impuesto
     * @param fecha Fecha de vigencia
     * @autor Ing. David Lamadrid
     * @version 1.0
     */
    public Tipo_impuesto buscarImpuestoPorCodigos(String tipoImpuesto, String codigoImpuesto, String distrito,String agencia) throws SQLException {
        this.Datos = TIDataAccess.buscarImpuestoPorCodigos(tipoImpuesto, codigoImpuesto, distrito, agencia );
        return Datos;
    }
    
     /*
     *Metodo que Retorna un Vector con los tipos de Impuestos en una fecha Determinada
     *David lamadrid
     *@params String tipoImpuesto,String fecha
     *@Exceptions SQLException
    */


    public Vector buscarImpuestoPorTipo(String tipoImpuesto,String fecha,String distrito,String agencia) throws SQLException {
        return TIDataAccess.buscarImpuestoPorTipo(tipoImpuesto, fecha,distrito,agencia); 
    }
     
    /**
     * Actualiza el estado de un registro en el archivotipo_de_impuesto de tipo RIVA.
     * @autor Ing. Tito Andr�s Maturana
     * @param reg_status Estado del registro
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void UPDATEESTADORIVA(String reg_status, String dstrct, String codigo_impuesto,  String fecha_vigencia, String login ) throws SQLException {
        this.TIDataAccess.UPDATEESTADORIVA(reg_status, dstrct, codigo_impuesto, fecha_vigencia, login);
    }
    
    /**
     * Actualiza un registro en el archivotipo_de_impuesto de tipo IVA.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param tipo Tipo de impuesto
     * @param codigo Nuevo c�digo del impuesto
     * @param fechav Nueva fecha de vigencia
     * @param concepto Nuevo concepto
     * @param porcentaje1 Porcentaje 1
     * @param cuenta C�digo de la cuenta contable
     * @param codigoi C�digo del impuesto
     * @param fv Fecha de vigencia
     * @param concept Concepto
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void UPDATERIVA(String tipo, String codigo, String concepto, String fechav, String porcentaje1, String cuenta, String codigoi, String fv, String concep, String login, String signo  ) throws SQLException {
        this.TIDataAccess.UPDATERIVA(tipo, codigo, concepto, fechav, porcentaje1, cuenta, codigoi, fv, concep, login, signo);
    }
    
    /**
     * Elimina un registro en el archivotipo_de_impuesto de tipo RIVA.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void DELETERIVA(String dstrct, String codigo_impuesto, String fecha_vigencia ) throws SQLException {
        this.TIDataAccess.DELETERIVA(dstrct, codigo_impuesto, fecha_vigencia);
    }
    
    /**
     * Verfica si existe un registro en el archivotipo_de_impuesto de tipo IVA.
     * @autor Ing. Tito Andr�s Maturana
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @return true si existe, false si no
     * @version 1.0
     */
    public boolean EXISTEIVA(String dstrct, String codigo_impuesto, String fecha_vigencia ) throws SQLException {
        return this.TIDataAccess.EXISTEIVA(dstrct, codigo_impuesto, fecha_vigencia);
    }
    
    /**
     * Verifica si existe un registro en el archivotipo_de_impuesto de tipo RFTE.
     * @autor Ing. Tito Andr�s Maturana
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @param tipo RFTE/RFTECL
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @return true si existe, false si no
     * @version 1.0
     */
    public boolean EXISTERFTE(String dstrct, String codigo_impuesto,  String fecha_vigencia,  String concepto, String tipo ) throws SQLException {
        return this.TIDataAccess.EXISTERFTE(dstrct, codigo_impuesto, fecha_vigencia, concepto, tipo);
    }
    
    /**
     * Verifica si existe un registro en el archivotipo_de_impuesto de tipo RICA.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @param agencia C�digo de la agencia
     * @param concepto Concepto
     * @param tipo RICA/RICACL
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @return true si existe, false si no
     * @version 1.0
     */
    public boolean EXISTERICA(String dstrct, String codigo_impuesto,  String fecha_vigencia, String agencia, String concepto, String tipo ) throws SQLException {
        return this.TIDataAccess.EXISTERICA(dstrct, codigo_impuesto, fecha_vigencia, agencia, concepto, tipo);
    }
    
    /**
     * Verifica si existe un registro en el archivo tipo_de_impuesto de tipo RIVA.
     * @autor Ing. Tito Andr�s Maturana
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @return true si existe, false si no
     * @version 1.0
     */
    public boolean EXISTERIVA(String dstrct, String codigo_impuesto, String fecha_vigencia ) throws SQLException {
        return this.TIDataAccess.EXISTERIVA(dstrct, codigo_impuesto, fecha_vigencia);
    }
    /**
     * Este m�todo genera un boolean true con si existe un impuesto dado el codig ,y la fecha de actualizacion y false si no existe
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @param codigoImpuesto C�digo del impuesto
     * @param fecha Fecha de vigencia
     * @autor Ing. David Lamadrid
     * @version 1.0
     */
    public boolean  existeImpuestoPorCodigo(String codigoImpuesto, String tipoImpuesto, String fecha, String agencia) throws SQLException {
        return TIDataAccess.existeImpuestoPorCodigo(codigoImpuesto,tipoImpuesto,fecha,agencia); 
    }
    
    
    
   
    
    /**
     * Este m�todo genera un vector con los tipos de impuestos
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @autor Ing. David Lamadrid
     * @version 1.0
     */
    public Vector vTiposImpuestos() throws SQLException {
        Vector v = new Vector();
        v.add("IVA");
        v.add("RIVA");
        v.add("RICA");
        v.add("RFTE");
        return v;
    }
    
         /*
     *Metodo que Retorna un Vector con los tipos de Impuestos en una fecha Determinada
     *Jose de la rosa
     *@params String tipoImpuesto,String fecha
     *@Exceptions SQLException
    */
    public Vector buscarImpuestoCliente(String tipoImpuesto,String fecha,String distrito,String agencia) throws SQLException {
        return TIDataAccess.buscarImpuestoCliente (tipoImpuesto, fecha,distrito,agencia); 
    }
    
      /*
     *Metodo que busca los codigos del rica de una agencia del a�o en curso
     *Diogenes Bastidas
     *@params distrito, agencia, fecha
     *@Exceptions SQLException
    */
    public void buscarRicaAgencia(String distrito, String agencia, String fecha) throws SQLException {
        TIDataAccess.buscarRicaAgencia(distrito, agencia, fecha);
    }
    
    public Vector getImpuestos() {
        return TIDataAccess.getImpuestos();
    }
    
    public void setImpuestos(Vector impuestos) {
        TIDataAccess.setImpuestos(impuestos);
    }
    
     /*
     *Busca un impuesto x codigo
     *@autorAndres maturana
     *@params String tipoImpuesto,String fecha
     *@Exceptions SQLException
     */
    public Tipo_impuesto buscarImpuestoxCodigo(String dstrct, String codigo) throws SQLException {
        return this.TIDataAccess.buscarImpuestoxCodigo(dstrct, codigo);
    }
     /**
     * Ingresa un registro en el archivotipo_de_impuesto de tipo RIVA.
     * @autor Ing. Tito Andr�s Maturana
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param concepto Concepto
     * @param fecha_vigencia Fecha de vigencia
     * @param porcentaje1 Porcentaje 1
     * @param cod_cuenta_contable C�digo de la cuenta contable
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void INSERTRIVA(String dstrct, String codigo_impuesto , String tipo_impuesto, String concepto, String fecha_vigencia, double porcentaje1, String cod_cuenta_contable, String login, String signo) throws SQLException {
        TIDataAccess.INSERTRIVA(dstrct, codigo_impuesto, tipo_impuesto, concepto, fecha_vigencia, porcentaje1, cod_cuenta_contable, login, signo);
    }
    /**
     * Actualiza un registro en el archivotipo_de_impuesto de tipo IVA.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param tipo Tipo de impuesto
     * @param codigo Nuevo c�digo del impuesto
     * @param fechav Nueva fecha de vigencia
     * @param concepto Nuevo concepto
     * @param porcentaje1 Porcentaje 1
     * @param porcentaje2 Porcentaje 2
     * @param cuenta C�digo de la cuenta contable
     * @param codigoi C�digo del impuesto
     * @param fv Fecha de vigencia
     * @param concept Concepto
     * @param signo 1/-1
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void UPDATEIVA(String tipo, String codigo, String concepto, String fechav, String porcentaje1, String porcentaje2, String cuenta, String descripcion, String codigoi, String fv, String concep, String login, String signo  ) throws Exception {
        try{
            TIDataAccess.UPDATEIVA(tipo, codigo, concepto, fechav, porcentaje1, porcentaje2, cuenta, descripcion, codigoi, fv, concep, login, signo);
        }
        catch(Exception e){
            throw new Exception("Error en UPDATEIVA [Tipo_impuestoService]...\n"+e.getMessage());
        }
    }

    /**
     * Verfica si existe un registro en el archivotipo_de_impuesto de tipo IVA.
     * @autor Ing. Tito Andr�s Maturana
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @return true si existe, false si no
     * @version 1.0
     */
    public boolean EXISTEIVA(String dstrct, String codigo_impuesto, String fecha_vigencia, String concepto ) throws SQLException {
        return this.TIDataAccess.EXISTEIVA(dstrct, codigo_impuesto, fecha_vigencia, concepto);
    }
     /**
     * Actualiza el estado de un registro en el archivotipo_de_impuesto de tipo RICA.
     * @autor Ing. Juan Manuel Escand�n
     * @param reg_status Estado del registro
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @param concepto Concepto
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void UPDATEESTADOIVA(String reg_status, String dstrct, String codigo_impuesto,  String fecha_vigencia, String login, String concepto ) throws Exception {
        try{
            TIDataAccess.UPDATEESTADOIVA(reg_status, dstrct, codigo_impuesto, fecha_vigencia, login, concepto);
        }
        catch(Exception e){
            throw new Exception("Error en UPDATEESTADOIVA [Tipo_impuestoService]...\n"+e.getMessage());
        }
    }
     /**
     * Ingresa un registro en el archivotipo_de_impuesto de tipo IVA.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param concepto Concepto
     * @param fecha_vigencia Fecha de vigencia
     * @param porcentaje1 Porcentaje 1
     * @param porcentaje2 Porcentaje 2
     * @param cod_cuenta_contable C�digo de la cuenta contable
     * @param signo 1/-1
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void INSERTIVA(String dstrct, String codigo_impuesto , String tipo_impuesto, String concepto, String fecha_vigencia, double porcentaje1, double porcentaje2, String cod_cuenta_contable, String login, String descripcion, String signo) throws Exception {
        try{
            TIDataAccess.INSERTIVA(dstrct, codigo_impuesto, tipo_impuesto, concepto, fecha_vigencia, porcentaje1, porcentaje2, cod_cuenta_contable, login, descripcion, signo);
        }
        catch(Exception e){
            throw new Exception("Error en INSERTIVA [Tipo_proveedorService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Getter for property tipoI.
     * @return Value of property tipoI.
     */
    public java.lang.String getTipoI() {
        return tipoI;
    }
    
    /**
     * Setter for property tipoI.
     * @param tipoI New value of property tipoI.
     */
    public void setTipoI(java.lang.String tipoI) {
        this.tipoI = tipoI;
    }
    
    /**
     * Getter for property fagencias.
     * @return Value of property fagencias.
     */
    public java.lang.String getFagencias() {
        return fagencias;
    }
    
    /**
     * Setter for property fagencias.
     * @param fagencias New value of property fagencias.
     */
    public void setFagencias(java.lang.String fagencias) {
        this.fagencias = fagencias;
    }
    
    /**
     * Getter for property fconcepto.
     * @return Value of property fconcepto.
     */
    public java.lang.String getFconcepto() {
        return fconcepto;
    }
    
    /**
     * Setter for property fconcepto.
     * @param fconcepto New value of property fconcepto.
     */
    public void setFconcepto(java.lang.String fconcepto) {
        this.fconcepto = fconcepto;
    }
    
}
