/***************************************
    * Nombre Clase ............. ArchivoMovimientoService.java
    * Descripci�n  .. . . . . .  ofrece los servicios para generar el archivo de movimiento
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  14/03/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/



package com.tsp.operation.model.services;


import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.ArchivoMovimiento;
import com.tsp.operation.model.DAOS.ArchivoMovimientoDAO;



public class ArchivoMovimientoService {
    
    private ArchivoMovimientoDAO  dao;
    private List                  listCheques;
    
    private String                distrito;
    private String                banco;
    private String                sucursal;
    private String                corrida;
    
    
    private boolean              isProceso;
    private boolean              activoBtn;
    
    
    
    public ArchivoMovimientoService() {
        dao         = new ArchivoMovimientoDAO();
        listCheques = new LinkedList();
        isProceso   = false;
        activoBtn   = true;
    }
    public ArchivoMovimientoService(String dataBaseName) {
        dao         = new ArchivoMovimientoDAO(dataBaseName);
        listCheques = new LinkedList();
        isProceso   = false;
        activoBtn   = true;
    }
    
  
    
    
    
    
     
   /**
   * M�todo que  busca los cheques de la corrida
   * @autor.......fvillacob
   * @version.....1.0.
   **/ 
    public void searchCheques(String distrito, String banco, String sucursal, String corrida)throws Exception{
        this.listCheques = new LinkedList();
        this.distrito = distrito;
        this.banco    = banco;
        this.sucursal = sucursal;
        this.corrida  = corrida;
        try{
            this.listCheques = dao.searchCheques(distrito, banco, sucursal, corrida);
            formatear();
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        
    }
    
    
    
    
    /**
   * M�todo que  formatea la longitud de los campos
   * @autor.......fvillacob
   * @version.....1.0.
   **/ 
    public void formatear()throws Exception{
       try{
            for(int i=0;i<this.listCheques.size();i++){
                ArchivoMovimiento  am = (ArchivoMovimiento)this.listCheques.get(i);
                am.formatearLongitud();
            }
       }catch(Exception e){
            throw new Exception(e.getMessage());
       }
    }
    
    
    
    
    
    
   /**
   * M�todo que  determina si existe migrada ya la transferencia
   * @autor.......fvillacob
   * @version.....1.0.
   **/ 
    public boolean existeTransferencia()throws Exception{
        boolean estado = false;
        try{ 
             estado = this.dao.existeTransferencia( distrito, banco, sucursal, corrida);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return estado;
    }
    
    
    
    
    
    /**
   * M�todo que  inserta la transferencia migrada
   * @autor.......fvillacob
   * @version.....1.0.
   **/ 
    public void insertTransferencia(String user)throws Exception{
        try{ 
             this.dao.insertTransferencia( distrito, banco, sucursal, corrida, user);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
       
    }
    
    
    
    
    
    
    
    /**
     * Getter for property listCheques.
     * @return Value of property listCheques.
     */
    public java.util.List getListCheques() {
        return listCheques;
    }    
    
    /**
     * Setter for property listCheques.
     * @param listCheques New value of property listCheques.
     */
    public void setListCheques(java.util.List listCheques) {
        this.listCheques = listCheques;
    }    
    
    
    
    
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    
    
    
    
    /**
     * Getter for property banco.
     * @return Value of property banco.
     */
    public java.lang.String getBanco() {
        return banco;
    }
    
    /**
     * Setter for property banco.
     * @param banco New value of property banco.
     */
    public void setBanco(java.lang.String banco) {
        this.banco = banco;
    }
    
    
    
    
    
    
    /**
     * Getter for property sucursal.
     * @return Value of property sucursal.
     */
    public java.lang.String getSucursal() {
        return sucursal;
    }
    
    /**
     * Setter for property sucursal.
     * @param sucursal New value of property sucursal.
     */
    public void setSucursal(java.lang.String sucursal) {
        this.sucursal = sucursal;
    }
    
    
    
    
    
    
    
    /**
     * Getter for property corrida.
     * @return Value of property corrida.
     */
    public java.lang.String getCorrida() {
        return corrida;
    }
    
    /**
     * Setter for property corrida.
     * @param corrida New value of property corrida.
     */
    public void setCorrida(java.lang.String corrida) {
        this.corrida = corrida;
    }
    
    
    
    
    
    /**
     * Getter for property isProceso.
     * @return Value of property isProceso.
     */
    public boolean isIsProceso() {
        return isProceso;
    }
    
    /**
     * Setter for property isProceso.
     * @param isProceso New value of property isProceso.
     */
    public void setIsProceso(boolean isProceso) {
        this.isProceso = isProceso;
    }
    
    
    
    
    
    
    /**
     * Getter for property activoBtn.
     * @return Value of property activoBtn.
     */
    public boolean isActivoBtn() {
        return activoBtn;
    }
    
    /**
     * Setter for property activoBtn.
     * @param activoBtn New value of property activoBtn.
     */
    public void setActivoBtn(boolean activoBtn) {
        this.activoBtn = activoBtn;
    }
    
}
