/**********************************************************************
* Nombre ......................DescuentoEquipoService.java            *
* Descripci�n..................Clase Service de descuento de equipos  *
* Autor........................Armando Oviedo                         *
* Fecha Creaci�n...............06/12/2005                             *
* Modificado por...............LREALES                                *
* Fecha Modificaci�n...........22/05/2006                             *
* Versi�n......................1.0                                    *
* Coyright.....................Transportes Sanchez Polo S.A.          *
**********************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;

public class DescuentoEquipoService {
    
    private DescuentoEquipoDAO dedao;
    
    /** Creates a new instance of DescuentoEquipoService */
    public DescuentoEquipoService(){
        
        dedao = new DescuentoEquipoDAO();
        
    }
    
    /** Funcion publica que obtiene el metodo setDE del DAO */
    public void setDE( DescuentoEquipo de ){
        
        dedao.setDE(de);
        
    }
    
    /** Funcion publica que obtiene el metodo existeDE del DAO */
    public boolean existeDE() throws SQLException{
        
        return dedao.existeDE();
        
    }
    
    /** Funcion publica que obtiene el metodo existeDEAnulado del DAO */
    public boolean existeDEAnulado() throws SQLException{
        
        return dedao.existeDEAnulado();
        
    }
    
    /** Funcion publica que obtiene el metodo getDescuentoEquipo del DAO */
    public DescuentoEquipo getDescuentoEquipo(){
        
        return dedao.getDescuentoEquipo();
        
    }
    
    /** Funcion publica que obtiene el metodo BuscarDescuentoEquipo del DAO */
    public void BuscarDescuentoEquipo() throws SQLException{
        
        dedao.BuscarDescuentoEquipo();
        
    }
    
    /** Funcion publica que obtiene el metodo addDE del DAO */ 
    public void addDE() throws SQLException{
        
        boolean existe = existeDEAnulado();
        
        if(!existe){
            dedao.addDE();
        } else{
            dedao.addDEUPD();
        }
        
    }
    
    /** Funcion publica que obtiene el metodo updateDE del DAO */
    public void updateDE() throws SQLException{
        
        dedao.updateDE();
        
    }
    
    /** Funcion publica que obtiene el metodo deleteDE del DAO */
    public void deleteDE() throws SQLException{
        
        dedao.deleteDE();
        
    }
    
    /** Funcion publica que obtiene el metodo BuscarTodosDescuentos del DAO */
    public void BuscarTodosDescuentos() throws SQLException{  
        
        dedao.BuscarTodosDescuentos();
        
    }
    
    /** Funcion publica que obtiene el metodo getTodosDescuentos del DAO */
    public Vector getTodosDescuentos(){
        
        return dedao.getTodosDescuentos();
        
    }
    
}