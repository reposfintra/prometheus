/********************************************************************
 *      Nombre Clase.................   JspService.java    
 *      Descripci�n..................   Service  de JspDAO
 *      Autor........................   Rodrigo Salazar
 *      Fecha........................   10.07.2005
 *      Versi�n......................   1.1
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/


package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;


public class JspService {
    private JspDAO jspd;
    private String varSeparadorJS = "~";
    
    /** Creates a new instance of JspService */
    public JspService() {
        jspd = new JspDAO();
    }
    
    /**
     * Obtiene la propiedad jsp de la clase JspDAO
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public Jsp getJsp() {
        return jspd.getJsp();
    }
    
    /**
     * Obtiene la propiedad jsps de la clase JspDAO
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public Vector getJsps() {
        return jspd.getJsps();
    }
    
    /**
     * Establece el valor de la propiedad jsps de la clase JspDAO
     * @param jsps Valor de la propiedad jsp
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public void setJsps(Vector jspds) {
        jspd.setJsps(jspds);
    }
    
    /**
     * Inserta un nuevi registro en la tabla jsp
     * @autor Rodrigo Salazar
     * @param tipo Objeto a insertar
     * @throws SQLException
     * @version 1.0
     */
    public void insertJsp(Jsp tipo) throws SQLException{
        try{
            //////System.out.println("Service"+tipo.getDescripcion());
            jspd.setJsp(tipo);
            jspd.insertJsp();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public boolean existJsp(String codigo) throws SQLException{
        return jspd.existJsp(codigo);
    }
    
    /**
     * Busca un registro en la tabla jsp
     * @autor Rodrigo Salazar
     * @param usuario C�digo de la p�gina JSP
     * @throws SQLException
     * @version 1.0
     */
    public void serchJsp(String codigo)throws SQLException{
        jspd.searchJsp(codigo);
    }
    
    /**
     * Lista todos los registros q no est�n anulados
     * @autor Rodrigo Salazar
     * @throws SQLException
     * @version 1.0
     */
    public void listJsp()throws SQLException{
        jspd.listJsp();
    }
    
    /**
     * Anula un registro en la tabla jsp
     * @autor Rodrigo Salazar
     * @param usuario Login del usuario
     * @param cod C�digo de la p�gina JSP a anular
     * @throws SQLException
     * @version 1.0
     */
    public void anularJsp(String usuario, String codigo)throws SQLException{
        jspd.anularJsp(usuario ,codigo);
    }
    
    /**
     * Busca un registro en la tabla jsp
     * @autor Rodrigo Salazar
     * @param cod C�digo de la p�gina JSP
     * @param oag Nombre de la p�gina JSP
     * @throws SQLException
     * @version 1.0
     */
    public Vector searchDetalleJsps(String codigo, String pagina ) throws SQLException{
        try{
            return jspd.searchDetalleJsps(codigo , pagina);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Modifica un registro en la tabla jsp
     * @autor Rodrigo Salazar
     * @param jsp Objeto a modificar
     * @throws SQLException
     * @version 1.0
     */
    public void modificarJsp(Jsp jsp) throws SQLException {
        try{
            jspd.setJsp(jsp);
            jspd.modificarJsp();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Lista todos los registros de la tabla jsp que no est�n anulados
     * y los alamacena en un vector
     * @autor Rodrigo Salazar
     * @throws SQLException
     * @version 1.0
     */
    public Vector listarJsp() throws SQLException{
        try{
            return jspd.listarJsp();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Getter for property varSeparadorJS.
     * @return Value of property varSeparadorJS.
     */
    public java.lang.String getVarSeparadorJS() {
        return varSeparadorJS;
    }
    
    /**
     * Setter for property varSeparadorJS.
     * @param varSeparadorJS New value of property varSeparadorJS.
     */
    public void setVarSeparadorJS(java.lang.String varSeparadorJS) {
        this.varSeparadorJS = varSeparadorJS;
    }
    
    /**
     * Genera una cadena de caracteres de la declaraci�n de
     * un arreglo en lenguaje JavaScript a partir del listado de
     * p�ginas JSP.
     * @autor Tito Andr�s Maturana
     * @throws SQLException
     * @version 1.0
     */
    public String GenerarJSCampos() throws SQLException{
        
        String var = "\n var CamposJSP = [ ";
        
        Vector jsps = this.listarJsp();
        Object[] jsps_aux = jsps.toArray();
        for(int i=0; i<jsps_aux.length; i++){
            for(int j=0; j<jsps_aux.length; j++){
                Jsp p = (Jsp) jsps_aux[i];
                Jsp p2 = (Jsp) jsps_aux[j];
                if( p.getNombre().compareTo(p2.getNombre())>0 ){
                    Jsp temp = p;
                    jsps_aux[i] = p2;
                    jsps_aux[j] = temp;
                }
            }
        }
        
        for(int i=0; i<jsps_aux.length; i++){
            Jsp jsp = (Jsp) jsps.elementAt(i);
            var += "\n '" + jsp.getCodigo() + varSeparadorJS + jsp.getNombre() + "',";
        }
        
        var = var.substring(0,var.length()-1) + "];";
        //////System.out.println("-----------------------> var: " + var);
        
        return var;
    }

    /**
     * Genera una cadena de caracteres de la declaraci�n de
     * un arreglo en lenguaje JavaScript a partir del listado de
     * la rutas de las p�ginas JSP.
     * @autor Tito Andr�s Maturana
     * @throws SQLException
     * @version 1.0
     */
    public String GenerarJSCamposPaths() throws SQLException{
        String var = "\n var CamposJSPURL = [ ";
        
        Vector jsps = this.listarJsp();
        Object[] jsps_aux = jsps.toArray();
        for(int i=0; i<jsps_aux.length; i++){
            for(int j=0; j<jsps_aux.length; j++){
                Jsp p = (Jsp) jsps_aux[i];
                Jsp p2 = (Jsp) jsps_aux[j];
                if( p.getNombre().compareTo(p2.getNombre())>0 ){
                    Jsp temp = p;
                    jsps_aux[i] = p2;
                    jsps_aux[j] = temp;
                }
            }
        }
        
        for(int i=0; i<jsps_aux.length; i++){
            Jsp jsp = (Jsp) jsps.elementAt(i);
            var += "\n '" + jsp.getCodigo() + varSeparadorJS + jsp.getRuta() + "',";
        }
        
        var = var.substring(0,var.length()-1) + "];";
        //////System.out.println("-----------------------> var: " + var);
        
        return var;
    }
    
    /**
     * Muestra el c�digo fuente de una vista JSP.
     * @autor Tito Andr�s Maturana
     * @param url Ruta completa del archivo
     * @throws SQLException
     * @version 1.0
     */
    public String obtenerCodigoJSP(String url) throws Exception{
        
        BufferedReader input = new BufferedReader(new FileReader(url));
        
        String linea = "";
        String out = "";
        
        while ((linea=input.readLine())!=null){            
            out += linea + "\n";
        }
        
        return out;
    }
    
    /**
     * Muestra el c�digo fuente de una vista JSP.
     * @autor Tito Andr�s Maturana
     * @param url Ruta completa del archivo
     * @throws SQLException
     * @version 1.0
     */
    public String obtenerCodigoJSPResaltado(String url) throws Exception{
        
        BufferedReader input = new BufferedReader(new FileReader(url));
        
        
        org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
        logger.info("URL: " + url);
        //this.validarJSP(url);
        
        input = new BufferedReader(new FileReader(url));
        
        String linea = "";
        String out = "";
        int ultcorte = 0;
        
        while ((linea=input.readLine())!=null){
            String ln = linea;
            String ln2 = "";
            
            //logger.info("LN: " + ln);
            
            ln2 = linea;// this.convertirLineaCampos(linea);
            
            ////////System.out.println("..........ENTRO: " + linea + "\n..........SALIO:" + ln2);
            //if( ln2.length()==0){
            if( ln2.compareTo(linea)==0 ){
                out += linea.replaceAll("<","&lt;").replaceAll(">","&gt;").replaceAll(" ","&nbsp;") + "<br>";                
            } else {                
                out += ln2 + "<br>";
                ////////System.out.println("...LN: " + ln2);
            }
           
        }    
        
        input.close();
        return out;
    }
    
    /**
     * Muestra el c�digo fuente de una vista JSP en con los campos en 
     * distintos colores.
     * @autor Tito Andr�s Maturana
     * @param url Ruta completa del archivo
     * @throws SQLException
     * @version 1.0
     */
    private String convertirLineaCampos(String ln){
        
        org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
        
        //logger.info("LN: " + ln);
        
        String ln2 = "";
        
        if ( ln.indexOf("<input")!=-1 || ln.indexOf("<img")!=-1 || 
                    ln.indexOf("<select")!=-1 || ln.indexOf("<a")!=-1 ){
            
            String color = "";
            String tipocampo = "";
            int campo = 0;
            int incrcampo = 0;
            
                        
            int[] vec = {ln.indexOf("<input"), ln.indexOf("<input:"), ln.indexOf("<img"),
                    ln.indexOf("<select"), ln.indexOf("<a")};  
                    
            for( int i=0; i<vec.length-1; i++ ){
                for( int j=i+1; j<vec.length-1; j++ ){
                    if( vec[j]!=-1 && ( vec[j]< vec[i] || vec[i]==-1 ) ){
			int temp = vec[i];
			vec[i] = vec[j];
			vec[j] = temp;
                    }
                }                
            }       
                    
            logger.info("..... " + vec[0] + ", " + vec[1] + ", " + vec[2] + ", " + vec[3] + ", " + vec[4]);
            
            int ocurr = vec[0];
            
            if ( ln.indexOf("<input:")!=-1 && ln.indexOf("<input:")==ocurr ){
                campo = ln.indexOf("<input:");
                String lnaux = ln.substring(campo);
                int tip = lnaux.indexOf(" ");
                tipocampo = lnaux.substring(7, tip);
                incrcampo+=6;
                ////////System.out.println("..............(input:) tipo: " + tipocampo);
            } else if(ln.indexOf("<input")!=-1 && ln.indexOf("<input")==ocurr){
                campo = ln.indexOf("<input");
                incrcampo+=6;
            } else if(ln.indexOf("<img")!=-1 && ln.indexOf("<img")==ocurr){
                campo = ln.indexOf("<img");
                tipocampo = "imagen";
                incrcampo+=4;
            } else if(ln.indexOf("<select")!=-1 && ln.indexOf("<select")==ocurr){
                campo = ln.indexOf("<select");
                tipocampo = "select";
                incrcampo+=7;
            } else if(ln.indexOf("<a")!=-1 && ln.indexOf("<a")==ocurr){
                campo = ln.indexOf("<a");
                tipocampo = "link";
                incrcampo+=2;
            }
            
            String str = ln.substring(campo);  
            
            String str2 = "";
            
            int corte = -1;
            String subln = "";

            for( int i=0; i < str.length(); i++){
                if(i!=0 && str.charAt(i) == '>' && str.charAt(i-1) != '%' ){                    
                    str2 = str.substring(0, i+=1);
                    subln = str2;                    
                    str2 = str2.replaceAll(">","&gt;").replaceAll("<","&lt;").replaceAll(" ","&nbsp;")
                    + "</font>" ;
                    corte = i;
                    break;
                }
            }
            
            
            int tc = subln.indexOf("type=");

            if( tc!=-1 ){
                tipocampo = subln.substring(tc + 5);
                if( tipocampo.startsWith("'") ){
                    tc = tipocampo.substring(1).indexOf("'");
                    tipocampo = tipocampo.substring(1, tc+=1);
                } else if ( tipocampo.startsWith("\"") ){
                    tc = tipocampo.substring(1).indexOf("\"");
                    tipocampo = tipocampo.substring(1, tc+=1);
                } else {
                    tc = tipocampo.indexOf(" ");
                    tipocampo = tipocampo.substring(0, tc+=1);
                }
            } else if ( tipocampo.length()==0 ){
                tipocampo = "text";
            }

            if( tipocampo.matches("select") ){
                color = "color:#003399;";
            } else if ( tipocampo.matches("text") ){
                color = "color:#CC0000;";
            } else if ( tipocampo.matches("checkbox") ){
                color = "color:#CC33CC;";
            } else if ( tipocampo.matches("button") ){
                color = "color:#996600;";
            } else if ( tipocampo.matches("imagen") ){
                color = "color:#FF3366;";
            } else if ( tipocampo.matches("link") ){
                color = "color:#33CC99;";
            }
            
            String font = "<font style=\"font-family:Verdana; font-size:11px;  " + color + "\">";//font-weight:bold            
            
            ln2 = ln.substring(0, campo).replaceAll("<","&lt;").replaceAll(">","&gt;").replaceAll(" ","&nbsp;") 
                    + font + ( ( str2.length()>0) ? str2 + this.convertirLineaCampos(str.substring(corte)) : "" );
            
            return ln2;    
        } else {                        
            return ln.replaceAll("<","&lt;").replaceAll(">","&gt;").replaceAll(" ","&nbsp;");
        }
    }
    
    /**
     * Corrige cuando un tag se cierra en m�s de una l�nea.
     * @autor Tito Andr�s Maturana
     * @param input Lee las l�neas del archivo.
     * @param ln L�nea a reestructurar
     * @throws SQLException
     * @version 1.0
     */
    private String validarJSP(String url) throws Exception{      
        
        BufferedReader input = new BufferedReader(new FileReader(url));      
        
        String jspok = "";
        String linea = "";
        Vector nlineas = new Vector();
        
        while ((linea=input.readLine())!=null){
            String ln = linea;
            
            if ( linea.indexOf("<input")!=-1 || linea.indexOf("<img")!=-1 || 
                    linea.indexOf("<select")!=-1 || linea.indexOf("<a")!=-1 ){
                                
                    int campo = 0;
                    int incrcampo = 0;


                    int[] vec = {ln.indexOf("<input"), ln.indexOf("<input"), ln.indexOf("<img"),
                    ln.indexOf("<select"), ln.indexOf("<a")};

                    for( int i=0; i<vec.length-1; i++ ){
                        for( int j=i+1; j<vec.length-1; j++ ){
                            if( vec[j]!=-1 && ( vec[j]< vec[i] || vec[i]==-1 ) ){
                                int temp = vec[i];
                                vec[i] = vec[j];
                                vec[j] = temp;
                            }
                        }
                    }

                    int ocurr = vec[0];

                    if ( ln.indexOf("<input:")!=-1 && ln.indexOf("<input:")==ocurr ){
                        campo = ln.indexOf("<input:");
                        incrcampo+=6;
                    } else if(ln.indexOf("<input")!=-1 && ln.indexOf("<input")==ocurr){
                        campo = ln.indexOf("<input");
                    } else if(ln.indexOf("<img")!=-1 && ln.indexOf("<img")==ocurr){
                        campo = ln.indexOf("<img");
                    } else if(ln.indexOf("<select")!=-1 && ln.indexOf("<select")==ocurr){
                        campo = ln.indexOf("<select");
                    } else if(ln.indexOf("<a")!=-1 && ln.indexOf("<a")==ocurr){
                        campo = ln.indexOf("<a");
                    }
                    
                    int corte = -1;

                    for( int i=0; i < ln.length(); i++){
                        if(i!=0 && ln.charAt(i) == '>' && ln.charAt(i-1) != '%' ){                            
                            corte = i;
                            break;
                        }
                    }
                    
                    if( corte == -1 ){
                        ln = this.validarJSPR(input, ln);
                        ////////System.out.println("................se llamo tu idea: " + ln);
                    }
                    
                    ////////System.out.println("............Linea: " + ln);                    
            }
            nlineas.add(ln);
        }
        
        ////////System.out.println("................... ARCHIVO REESTRUCTURADO: ");
        /*Se reecribe el archivo*/
        FileWriter fw = new FileWriter(url);
        BufferedWriter bffw = new BufferedWriter(fw);
        PrintWriter pntw = new PrintWriter(bffw);
        
        for(int i=0; i<nlineas.size(); i++){
            String str = (String) nlineas.elementAt(i);
            ////////System.out.println(str);
            pntw.println(str);
        }
        
        pntw.close();
        
        return jspok;
    }
    
    
    /**
     * Corrige cuando un tag se cierra en m�s de una l�nea.
     * @autor Tito Andr�s Maturana
     * @param input Lee las l�neas del archivo.
     * @param ln L�nea a reestructurar
     * @throws SQLException
     * @version 1.0
     */
    private String validarJSPR(BufferedReader input, String ln)
            throws Exception{                       
        
        int corte = -1;
        
        for( int i=0; i < ln.length(); i++){
            if(i!=0 && ln.charAt(i) == '>' && ln.charAt(i-1) != '%' ){                
                corte = i;
                break;
            }
        }
        
        if( corte == -1 ){
            String str = ln + input.readLine();
            return this.validarJSPR(input, str);
        } else {
            return ln;
        }
    }
    
    /**
     * Verifica la existencia de un registro en la tabla jsp
     * @autor Ing. Andr�s Maturana
     * @param codigo C�digo de la p�gina JSP
     * @throws SQLException
     * @version 1.0
     */
    public String existeJsp(String codigo) throws SQLException{
        return jspd.existeJsp(codigo);
    }
}
