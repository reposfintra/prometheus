/***************************************
    * Nombre Clase ............. AdicionAjustesPlanillasServices.java
    * Descripci�n  .. . . . . .  ofrece los servicios para adicionar extrafletes a la planilla
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  02/12/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/




package com.tsp.operation.model.services;



import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.AdicionAjustesPlanillasDAO;


public class AdicionAjustesPlanillasServices {
    
    
    
    
    private  AdicionAjustesPlanillasDAO   AdicionAjustesPlanillasDataAccess;
    private  Ajuste                       ajuste;
    private  List                         listaExtrafletes;
    private  List                         listaMonedas;
    private  final  String                TSP       = "890103161";
    private  final  String                PLANILLA  = "001";
   
    
    
    public AdicionAjustesPlanillasServices() {        
        AdicionAjustesPlanillasDataAccess  =  new  AdicionAjustesPlanillasDAO();
        ajuste                             =  null;   
        listaExtrafletes                   =  new LinkedList();
        listaMonedas                       =  new LinkedList();
    }
    public AdicionAjustesPlanillasServices(String dataBaseName) {        
        AdicionAjustesPlanillasDataAccess  =  new  AdicionAjustesPlanillasDAO(dataBaseName);
        ajuste                             =  null;   
        listaExtrafletes                   =  new LinkedList();
        listaMonedas                       =  new LinkedList();
    }
    
    
    
    
    
    
    
    
      /**
       * M�todo que carga los codigos de Extrafletes
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
    public void loadExtrafletes() throws Exception{
        try{
            listaExtrafletes = this.AdicionAjustesPlanillasDataAccess.loadExtrafletes();
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
      /**
       * M�todo que carga las Monedas
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
    public void loadMonedas() throws Exception{
        try{
            listaMonedas = this.AdicionAjustesPlanillasDataAccess.loadMonedas();
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
    
    
    
     /**
       * M�todo que obtiene datos de la planilla
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
    public void datosPlanillas(String distrito, String oc)throws Exception{
        ajuste    =  null;
        try{
             ajuste             = this.AdicionAjustesPlanillasDataAccess.searchPlanilla(distrito, oc);
             if( ajuste!= null ){
             
              // Moneda Local:
                 ajuste.setMonedaLocal( this.AdicionAjustesPlanillasDataAccess.getMonedaLocal(distrito) );

              // Movimientos:
                 List  movimientos  = loadMovimientos(distrito, oc);
                 ajuste.setMovimientos( movimientos );

              // Valor Planilla Moneda Local:
                 ajuste.setVlrOCML(  this.convert( ajuste.getValor(), ajuste.getMoneda() , ajuste.getMonedaLocal() , ajuste.getMonedaLocal() )       );

              // Valor Real planilla Moneda Local:
                 ajuste.setVlrPlanilla     ( this.getValorPlanilla(  movimientos, ajuste.getValor(), ajuste.getMoneda(),  ajuste.getMonedaLocal()  ) );

              // Saldo:
                 ajuste.setNeto( ajuste.getVlrPlanilla() +  this.getValorSaldo( movimientos ) );
                 
                 
              // Valor Real planilla MOC:
              //   ajuste.setVlrPlanillaMonOC( this.convert( ajuste.getVlrPlanilla(), ajuste.getMonedaLocal(), ajuste.getMoneda(), ajuste.getMonedaLocal() ) );
            
            }  
             
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
     /**
       * M�todo que carga los Movimientos de la planilla
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
    public List loadMovimientos(String distrito, String oc) throws Exception{
        List movimientos = new LinkedList();
        try{
            
            movimientos = this.AdicionAjustesPlanillasDataAccess.loadMovPlanilla(distrito, oc);
            movimientos = calcularPorcentaje( movimientos );
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return movimientos;
    }
    
    
    
    
    
    
    
    /**
       * M�todo que agrega el ajuste a la planilla en movpla
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
    public String  agregarAjuste( String cia, String user, String agencia, String concepto, double valor, String moneda)throws Exception{
        String  msj = "";
        try{
            
            String dstrct              = cia;
            String agency_id           = agencia;
            String document_type       = PLANILLA;
            String concept_code        = concepto;
            String pla_owner           = this.ajuste.getPropietario();
            String planilla            = this.ajuste.getPlanilla();
            String supplier            = this.ajuste.getPlaca();
            
            
            Hashtable  HAjuste         = this.getAjusteLista(concepto);
            
            String applicated_ind      = "N";    
            String application_ind     = (String)HAjuste.get("aplica"); 
            String ind_vlr             = "V";  
            
            if(  application_ind == null  || application_ind.trim().equals("")  )
                 application_ind = "V";
            
                       
            double vlr_for       = valor;
                   vlr_for       = (  !moneda.equals("PES") &&  !moneda.equals("BOL")  )? Util.roundByDecimal( vlr_for,  2 ) :  (int)Math.round( vlr_for ) ;
            double vlr_disc      = vlr_for; 
            double vlr           = vlr_for; 
            String currency      = moneda;  
                       
                        
            //convertimos monedas, para el valor local: 
            String monedaLocal         =  ajuste.getMonedaLocal();
            if( !moneda.equals(monedaLocal) ){                 
                String       fecha =  Utility.getHoy("-");
                TasaService  svc   =  new  TasaService();
                Tasa         obj   =  svc.buscarValorTasa(monedaLocal,  moneda , monedaLocal , fecha);
                if( obj!=null ){                    
                    vlr       = valor * obj.getValor_tasa();                    
                    vlr       = (  !monedaLocal.equals("PES") &&  !monedaLocal.equals("BOL")  )? Util.roundByDecimal( vlr,  2  ) :  (int)Math.round( vlr ) ;                     
                    vlr_disc  = vlr; 
                }
            }    
            
            
            String creation_user       = user;      
            String proveedor_anticipo  = TSP; 
            String base                = ajuste.getBase();                                     
                                      
                        
         // Insertamos:
            msj = this.AdicionAjustesPlanillasDataAccess.adicionarAjuste(dstrct, agency_id, document_type, concept_code, pla_owner, 
                                                                         planilla, supplier, applicated_ind, application_ind, ind_vlr, 
                                                                         vlr_disc, vlr, vlr_for, currency, creation_user, proveedor_anticipo, base);
            
        }catch(Exception e){
            msj =  e.getMessage() ;
        }
        return msj;
    }
    
    
    
    
    
     /**
       * M�todo que busca el ajuste aplicado
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
    public Hashtable getAjusteLista(String code){
           Hashtable  ajuste = null;
           for(int i=0; i<this.listaExtrafletes.size();i++){
               Hashtable aj  = (Hashtable)listaExtrafletes.get(i);
               String    cod = (String) aj.get("codigo");
               if( cod.equals( code )  ){
                   ajuste = aj;
                   break;
               }
           }           
           return ajuste;
    }
    
    
    
    
    
    
    
     /**
       * M�todo que asigna valor local a los movimientos de porcentaje
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
    public List  calcularPorcentaje(List lista)throws Exception{        
        double vlr    = 0;      
        for(int i=0;i<lista.size();i++){
              MoviOC  mov = (MoviOC )lista.get(i);
              if( mov.getAplica().equals("V") &&  mov.getIndicador_valor().equals("V") )
                   vlr   += mov.getVlrReal();
        }        
        
        for(int i=0;i<lista.size();i++){
              MoviOC  mov = (MoviOC )lista.get(i);
              if( mov.getIndicador_valor().equals("P") ){
                   mov.setVlrReal  (  ( vlr   * mov.getValor() ) / 100  );
                   mov.setMoneda( ajuste.getMonedaLocal() );    
              }
        } 
        return lista;
    }
    
    
    
    
    
    
     /**
       * M�todo que busca el valor de la planilla en moneda Local
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
    public double getValorPlanilla(List lista, double vlrOC, String monedaOC, String monedaLocal)throws Exception{
        double vlr=0;
        try{
            
            for(int i=0;i<lista.size();i++){
                 MoviOC  mov = (MoviOC )lista.get(i);
                 if( mov.getAplica().equals("V") )
                      vlr   += mov.getVlrReal();
            } 
            
            if( monedaOC.equals(monedaLocal) )
                vlr += vlrOC;
            else{
                 String       fecha =  Utility.getHoy("-");
                 TasaService  svc   =  new  TasaService();
                 Tasa         obj   =  svc.buscarValorTasa(monedaLocal,  monedaOC , monedaLocal , fecha);
                 if( obj!=null )
                     vlr      +=  vlrOC * obj.getValor_tasa();  
            }
            
            
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return vlr;
    }
    
    
    
    
    
    /**
       * M�todo que busca el valor aplica a saldo
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
    public double getValorSaldo(List lista)throws Exception{
        double vlr=0;
        try{
            
            for(int i=0;i<lista.size();i++){
                 MoviOC  mov = (MoviOC )lista.get(i);
                 if( mov.getAplica().equals("S") )
                      vlr   += mov.getVlrReal();
            } 
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return vlr;
    }
    
    
    
    
    
    public double convert( double valor, String moneda, String monedaDestino , String monedaLocal)throws Exception{
        double vlr = valor;
        try{
            
            if( !moneda.equals( monedaDestino) ){
                 
                 String       fecha =  Utility.getHoy("-");
                 TasaService  svc   =  new  TasaService();
                 Tasa         obj   =  svc.buscarValorTasa(monedaLocal,  moneda , monedaDestino , fecha);
                 if( obj!=null )
                     vlr      +=  valor * obj.getValor_tasa(); 
            }
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        
        return vlr;        
    }
    
    
    
    
    
    
    /**
     * Getter for property ajuste.
     * @return Value of property ajuste.
     */
    public com.tsp.operation.model.beans.Ajuste getAjuste() {
        return ajuste;
    }    
    
    /**
     * Setter for property ajuste.
     * @param ajuste New value of property ajuste.
     */
    public void setAjuste(com.tsp.operation.model.beans.Ajuste ajuste) {
        this.ajuste = ajuste;
    }    
    
    /**
     * Getter for property listaExtrafletes.
     * @return Value of property listaExtrafletes.
     */
    public java.util.List getListaExtrafletes() {
        return listaExtrafletes;
    }    
    
    /**
     * Setter for property listaExtrafletes.
     * @param listaExtrafletes New value of property listaExtrafletes.
     */
    public void setListaExtrafletes(java.util.List listaExtrafletes) {
        this.listaExtrafletes = listaExtrafletes;
    }
    
    /**
     * Getter for property listaMonedas.
     * @return Value of property listaMonedas.
     */
    public java.util.List getListaMonedas() {
        return listaMonedas;
    }
    
    /**
     * Setter for property listaMonedas.
     * @param listaMonedas New value of property listaMonedas.
     */
    public void setListaMonedas(java.util.List listaMonedas) {
        this.listaMonedas = listaMonedas;
    }
    
}
