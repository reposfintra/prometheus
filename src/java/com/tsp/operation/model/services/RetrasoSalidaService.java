/*
 * RetrasoSalidaService.java
 *
 * Created on 15 de diciembre de 2005, 04:08 PM
 */

package com.tsp.operation.model.services;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  dlamadrid
 */
public class RetrasoSalidaService
{
    RetrasoSalidaDAO dao;
    /** Creates a new instance of RetrasoSalidaService */
    public RetrasoSalidaService ()
    {
        dao=new RetrasoSalidaDAO ();
    }
    
    public void insertarRetrazo () throws Exception
    {
        dao.insertarRetrazo ();
    }
    
    /**
     * Getter for property retraso.
     * @return Value of property retraso.
     */
    public com.tsp.operation.model.beans.RetrasoSalida getRetraso ()
    {
        return dao.getRetraso ();
    }
    
    /**
     * Setter for property retraso.
     * @param retraso New value of property retraso.
     */
    public void setRetraso (com.tsp.operation.model.beans.RetrasoSalida retraso)
    {
        dao.setRetraso (retraso);
    }
    
}
