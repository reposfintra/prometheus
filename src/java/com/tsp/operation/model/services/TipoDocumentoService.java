/*
 * TipoDocumentoService.java
 *
 * Created on 30 de marzo de 2005, 08:56 AM
 */

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  Administrador
 */
public class TipoDocumentoService {
    
    private TipoDocumentoDAO tdoc;
    
    private Vector tipodoc;
    
    /** Creates a new instance of TipoDocumentoService */
    public TipoDocumentoService() {
        tdoc = new TipoDocumentoDAO();
    }
    
    public TipoDocumentoService(String dataBaseName) {
        tdoc = new TipoDocumentoDAO(dataBaseName);
    }
    
    public Vector listarTDocumentos() throws SQLException{
        try{
            return tdoc.listarTDocumentos();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public String getDescripcion(String id) throws SQLException{
        return tdoc.getDescripcion(id);
    }
    
    public String getId(String desc) throws SQLException{
        return tdoc.getId(desc);
    }
    /**
     * Metodo ListarTipoDocumentos, lista los documentos de la tabla gen,
     * @autor : Ing. Diogenes Antonio Bastidas Morales
     * @see   : listarTDocumentos - TipoDocumentoDAO
     * @version : 1.0
     */
     
    public void ListarTipoDocumentos()throws SQLException{
        this.tipodoc = tdoc.listarTDocumentos();
    }
    
    /**
     * Metodo obtenerTipoDocumentos, obtiene el vector
     * @autor : Ing. Diogenes Antonio Bastidas Morales
     * @see   : obtenerTDocumentos - TipoDocumentoDAO
     * @version : 1.0
     */
    public Vector obtenerTipoDocumentos()throws SQLException{
        return this.tipodoc;
    }
}
