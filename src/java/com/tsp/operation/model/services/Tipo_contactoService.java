/*
 * Nombre        Tipo_contactoService.java
 * Autor         Ing. Rodrigo Salazar
 * Fecha         22 de junio de 2005, 02:42 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

public class Tipo_contactoService {

    private Tipo_contactoDAO tipo_contacto;
    
    /** Creates a new instance of Tipo_contactoService */
    public Tipo_contactoService() {
        tipo_contacto = new Tipo_contactoDAO();
    }
    
    /**
     * Getter for property tipo_contactos.
     * @return Value of property tipo_contactos
     */
    public Vector getTipo_contactos() {
        return tipo_contacto.getTipo_contactos();
    }
    
    /**
     * Setter for property tipo_contactos.
     * @param ub New value of property tipo_contactos.
     */
    public void setTipo_contactos(Vector tipo_contactos) {
        tipo_contacto.setTipo_contactos(tipo_contactos);
    }
    
    /**
     * Metodo listarTContacto, obtienen los tipo de contactos
     *  registrados en el sistema
     * @autor  Ing. Rodrigo Salazar
     * @see  listarTContactos()
     * @version  1.0
     */
    public Vector listarTContactos () throws SQLException{
        try{
            return tipo_contacto.listarTContactos();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
}
