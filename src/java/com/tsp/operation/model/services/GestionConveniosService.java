/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.GestionConveniosDAO;
import com.tsp.operation.model.beans.AfiliadoConvenio;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.CmbGeneralScBeans;
import com.tsp.operation.model.beans.Convenio;
import java.util.ArrayList;
//darrieta
import com.tsp.operation.model.beans.ConveniosRemesas;
import com.tsp.operation.model.beans.Deducciones;
import com.tsp.operation.model.beans.Propietario;

/**
 *
 * @author maltamiranda
 */
public class GestionConveniosService {

    GestionConveniosDAO gcd;

    public GestionConveniosService() {
        gcd = new GestionConveniosDAO();
    }

    public GestionConveniosService(String dataBaseName) {
        gcd = new GestionConveniosDAO(dataBaseName);
    }
    
    public ArrayList getConvenios( String tipo)throws Exception{
        return gcd.getConvenios(tipo);
    }

    public ArrayList getPrefijos(String bd)throws Exception{
        return gcd.getPrefijos(bd);
    }
    
    public ArrayList getAgencias ()throws Exception{
        return gcd.getAgencias();
    }
    
    public ArrayList getTipo_documento(String bd)throws Exception{
        return gcd.getTipo_documento(bd);
    }

     public void insertar_convenio(String bd, Convenio cnv, String redescuento,String tipo)throws Exception {
        gcd.insertar_convenio(bd, cnv,redescuento,tipo);
    }

    public Convenio buscar_convenio(String bd, String id_convenio)throws Exception {
        return gcd.buscar_convenio(bd, id_convenio);
    }
    
    public Deducciones buscar_deducciones_convenio(String bd, String id_convenio)throws Exception {
        return gcd.buscar_deducciones_convenio(bd, id_convenio);
    }

    public void actualizar_convenio(String bd, Convenio cnv,String tipo)throws Exception {
        gcd.actualizar_convenio(bd, cnv, tipo);
    }

    public ArrayList<Convenio> listarConvenios(String bd) throws Exception{
        ArrayList<Convenio> lista = null;
        try {
            lista = gcd.listarConvenios(bd);
        }
        catch (Exception e) {
            throw new Exception("Error al buscar datos: "+e.toString());
        }
        return lista;
    }

    public ArrayList<String> buscarDatosTercero(String columna,String cadena) throws Exception{
        ArrayList<String> lista = null;
        try {
            lista = gcd.buscarDatosTercero(columna, cadena);
        }
        catch (Exception e) {
            throw new Exception("Error al buscar datos: "+e.toString());
        }
        return lista;
    }

     public String buscarNombreTercero(String nit) throws Exception {
          String nombre = null;
        try {
            nombre = gcd.buscarNombreTercero(nit);
        }
        catch (Exception e) {
            throw new Exception("Error al buscar datos: "+e.toString());
        }
        return nombre;

     }

    public ArrayList<String> buscarDatosCiudad(String columna,String cadena) throws Exception{
        ArrayList<String> lista = null;
        try {
            lista = gcd.buscarDatosCiudad(columna, cadena);
        }
        catch (Exception e) {
            throw new Exception("Error al buscar datos: "+e.toString());
        }
        return lista;
    }

    public String datosCodigoUsuario(String codusuario) throws Exception{
        String datos = "";
        try {
            datos = gcd.datosCodigoUsuario(codusuario);
        }
        catch (Exception e) {
            throw new Exception("error: "+e.toString());
        }
        return datos;
    }

    public boolean existeCuenta(String cuenta) throws Exception{
        boolean sw= false;
        try {
            sw = gcd.existeCuenta(cuenta);
        }
        catch (Exception e) {
            throw new Exception("error: "+e.toString());
        }
        return sw;
    }

    public ArrayList<String> buscarDatosAfil(String columna,String cadena) throws Exception{
        ArrayList<String> lista = null;
        try {
            lista = gcd.buscarDatosAfil(columna, cadena);
        }
        catch (Exception e) {
            throw new Exception("Error al buscar datos: "+e.toString());
        }
        return lista;
    }

    public ArrayList<String> datosConveniosSectores(String nitAfil) throws Exception{
        ArrayList<String> lista = null;
        try {
            lista = gcd.datosConveniosSectores(nitAfil);
        }
        catch (Exception e) {
            throw new Exception("Error al buscar datos: "+e.toString());
        }
        return lista;
    }

    public ArrayList<AfiliadoConvenio> buscarConvenios(String nit) throws Exception {
        ArrayList<AfiliadoConvenio> lista = null;
        try {
            lista = gcd.buscarConvenios(nit);
        }
        catch (Exception e) {
            throw new Exception("Error al buscar datos: "+e.toString());
        }
        return lista;

    }

    public String nombreConvenio(String id_convenio) throws Exception{
        String datos = "";
        try {
            datos = gcd.nombreConvenio(id_convenio);
        }
        catch (Exception e) {
            throw new Exception("error: "+e.toString());
        }
        return datos;
    }

    public String nombreSector(String cod_sector) throws Exception{
        String datos = "";
        try {
            datos = gcd.nombreSector(cod_sector);
        }
        catch (Exception e) {
            throw new Exception("error: "+e.toString());
        }
        return datos;
    }

    public String nombreSubSector(String cod_sector,String cod_subsector) throws Exception{
        String datos = "";
        try {
            datos = gcd.nombreSubSector(cod_sector,cod_subsector);
        }
        catch (Exception e) {
            throw new Exception("error: "+e.toString());
        }
        return datos;
    }

    public void asignarUsuario(String codusuario,ArrayList<String> convs,String user) throws Exception{
        try {
            gcd.asignarUsuario(codusuario, convs, user);
        }
        catch (Exception e) {
            throw new Exception("error: "+e.toString());
        }
    }

    public ArrayList<String> asignacionesUsuarioAfil(String codusuario, String nitafil) throws Exception{
        ArrayList<String> lista = null;
        try {
            lista = gcd.asignacionesUsuarioAfil(codusuario, nitafil);
        }
        catch (Exception e) {
            throw new Exception("Error al buscar datos: "+e.toString());
        }
        return lista;
    }

    public ArrayList<String> listarBancos() throws Exception{
        ArrayList<String> lista = null;
        try {
            lista = gcd.listarBancos();
        }
        catch (Exception e) {
            throw new Exception("Error al buscar datos: "+e.toString());
        }
        return lista;
    }

    //darrieta
    public ArrayList<String> buscarBancos(String campo, String valor) throws Exception{
        return gcd.buscarBancos(campo, valor);
    }

    public ConveniosRemesas buscarRemesa(ConveniosRemesas datosRemesa) throws Exception{
        return gcd.buscarRemesa(datosRemesa);
    }

    public Propietario buscarProvConvenio(int idProvConvenio) throws Exception{
        return gcd.buscarProvConvenio(idProvConvenio);
    }

    public ArrayList getPrefijosCXC(String bd)throws Exception{
        return gcd.getPrefijosCXC(bd);
    }

     public boolean isAvalTercero(String id_convenio) throws Exception{
        boolean sw= false;
        try {
            sw = gcd.isAvalTercero(id_convenio);
        }
        catch (Exception e) {
            throw new Exception("error: "+e.toString());
        }
        return sw;
    }

     /**
     * Busca dato en tablagen
     * @param dato table_type a buscar
     * @return ArrayList
     * @throws Exception cuando hay error
     */
    public ArrayList busquedaGeneral(String dato) throws Exception{
        ArrayList cadena= new ArrayList ();
        try {
            cadena = gcd.busquedaGeneral(dato);

        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: "+e.toString());
        }
        return cadena;
    }

    public ArrayList<String> buscarConveniosTipo(String tipo, boolean cat) throws Exception{
        return gcd.buscarConveniosTipo(tipo, cat);
    }

    /**
     * Busca la informacion de los formulario de un tipo de convenio
     * @param tipoconv tipo de convenio
     * @param cat indicador de ley mipyme
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<BeanGeneral> obtenerForm(String tipoconv, boolean cat) throws Exception{
        return gcd.obtenerForm(tipoconv, cat);
    }

    /**
     * Busca la informacion de los montos de un convenio
     * @param id_convenio
     * @return BeanGeneral con los resultados obtenidos
     * @throws Exception
     */
    public BeanGeneral obtenerMontos(String id_convenio) throws Exception{
        return gcd.obtenerMontos(id_convenio);
    }
    
     /**
     * Busca el la tasa de un proveedor
     * @param convenio
     * @param proveedor
     * @return nombre alterno sector
     * @throws Exception
     */
      public double getTasaProveedor(String convenio, String proveedor) throws Exception {
             return gcd.getTasaProveedor(convenio, proveedor);
      }

       public ArrayList<String> buscarProveedores(String columna,String cadena) throws Exception{
        ArrayList<String> lista = null;
        try {
            lista = gcd.buscarProveedores(columna, cadena);
        }
        catch (Exception e) {
            throw new Exception("Error al buscar datos: "+e.toString());
        }
        return lista;
    }

       
    /**
     * Busca el redescuento de un convenio
     * @param id_convenio
     * @return boolean redescuento
     * @throws Exception
     */
    public boolean tieneRedescuento (String id_convenio) throws Exception {
        return gcd.tieneRedescuento(id_convenio);
    }
    
    /**
     * Busca el nombre alterno del sector
     * @param cod_sector
     * @return nombre alterno sector
     * @throws Exception
     */
    public String nombreAlternoSector(String cod_sector) throws Exception{
        String datos = "";
        try {
            datos = gcd.nombreAlternoSector(cod_sector);
        }
        catch (Exception e) {
            throw new Exception("error: "+e.toString());
        }
        return datos;
    }

    /**
     * Busca el nombre alterno del subsector
     * @param cod_sector
     * @param cod_subsector
     * @return nombre alterno subsector
     * @throws Exception
     */
    public String nombreAlternoSubSector(String cod_sector,String cod_subsector) throws Exception{
        String datos = "";
        try {
            datos = gcd.nombreAlternoSubSector(cod_sector,cod_subsector);
        }
        catch (Exception e) {
            throw new Exception("error: "+e.toString());
        }
        return datos;
    }
    /**
     * Busca el tipo de un convenio
     * @param id_convenio
     * @return string tipo
     * @throws Exception
     */
    public String getTipoConvenio(String id_convenio) throws Exception {
        return gcd.getTipoConvenio(id_convenio);
    }

     public Convenio buscar_convenio(String cod_neg)throws Exception {
        return gcd.buscar_convenio(cod_neg);
    }
     
      public Propietario buscarProvConvenio(String nit_proveedor,String id_convenio,String cod_sector,String cod_subsector) throws Exception{
        return gcd.buscarProvConvenio(nit_proveedor, id_convenio, cod_sector, cod_subsector);
    }


         public ArrayList buscar_convenios_reactivacion(String tipo) throws Exception {
        return gcd.buscar_convenios_reactivacion(tipo);
    }
         
    public String valida_remesa(String id_convenio,String titulo) throws Exception {
           return gcd.valida_remesa(id_convenio, titulo);
    }
    public ArrayList getFiducias()throws Exception{
        return gcd.getFiducias();
    }
    
    public ArrayList getEntidadRedescuento() throws Exception {
        return gcd.getEntidadRedescuento();
    }

    public String getEntidadRedescuentoNom(String id) throws Exception {
        return gcd.getEntidadRedescuentoNom(id);
    }

    public String getRedescuentoConvenio(String convenio) throws Exception {
        return gcd.getRedescuentoConvenio(convenio);
    }

    public ArrayList<CmbGeneralScBeans> GetComboGenerico(String Query, String IdCmb, String DescripcionCmb, String wuereCmb)throws Exception{
       return gcd.GetComboGenerico(Query, IdCmb, DescripcionCmb, wuereCmb);
    }   
}
