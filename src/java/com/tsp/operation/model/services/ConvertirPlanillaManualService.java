/*********************************************************************************
 * Nombre clase :      ConvertirPlanillaManualService.java                       *
 * Descripcion :       Service del ConvertirPlanillaManualService.java           *
 * Autor :             LREALES                                                   *
 * Fecha :             29 de marzo de 2006, 10:35 AM                             *
 * Version :           1.0                                                       *
 * Copyright :         Fintravalores S.A.                                   *
 *********************************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;

public class ConvertirPlanillaManualService {
    
    private ConvertirPlanillaManualDAO planillaman;
    
    /** Creates a new instance of ConvertirPlanillaManualService */
    public ConvertirPlanillaManualService () {
        
        planillaman = new ConvertirPlanillaManualDAO ();
        
    }
    
    /** Funcion publica que obtiene el metodo getVectorPlanilla del DAO */
    public Vector getVectorPlanilla () throws SQLException {
        
        return planillaman.getPlaManual();
        
    }    
    
    /** Funcion publica que obtiene el metodo getIngresoPlanilla del DAO */
    public Ingreso_Trafico getIngresoPlanilla () throws SQLException {
       
        return planillaman.getIngTra();
    }
    
    /** Funcion publica que obtiene el metodo listaPlanillasManuales del DAO */
    public void listaPlanillasManuales () throws SQLException {
        
        planillaman.listaPlanillasManuales ();
        
    }
    
    /** Funcion publica que obtiene el metodo existePlanilla del DAO */
    public boolean existePlanilla ( String numplaman ) throws SQLException {
        
        planillaman.existePlanilla ( numplaman );
        return planillaman.existePlanilla( numplaman );
        
    }
    
    /** Funcion publica que obtiene el metodo existePlanillaOriginal del DAO */
    public boolean existePlanillaOriginal ( String numplaori ) throws SQLException {
        
        planillaman.existePlanillaOriginal ( numplaori );
        return planillaman.existePlanillaOriginal( numplaori );
        
    }
    
    /** Funcion publica que obtiene el metodo existeRepMovTra del DAO */
    public boolean existeRepMovTra ( String numplaori ) throws SQLException {
        
        planillaman.existeRepMovTra ( numplaori );
        return planillaman.existeRepMovTra( numplaori );
        
    }
    
    /** Funcion publica que obtiene el metodo Update_Ingreso_Trafico del DAO */
    public String Update_Ingreso_Trafico ( String numplaori, String pto_control_ultreporte, String nompto_control_ultreporte, String fecha_ult_reporte, String pto_control_proxreporte, String nompto_control_proxreporte, String fecha_prox_reporte, String creation_date, String creation_user, String zona, String nomzona, String ult_observacion ) throws SQLException {
        
        return planillaman.Update_Ingreso_Trafico( numplaori, pto_control_ultreporte, nompto_control_ultreporte, fecha_ult_reporte, pto_control_proxreporte, nompto_control_proxreporte, fecha_prox_reporte, creation_date, creation_user, zona, nomzona, ult_observacion );
        
    }
    
    /** Funcion publica que obtiene el metodo Update_Trafico del DAO */
    public String Update_Trafico ( String numplaori, String pto_control_ultreporte, String fecha_ult_reporte, String pto_control_proxreporte, String fecha_prox_reporte, String creation_date, String creation_user, String zona, String ult_observacion ) throws SQLException {
        
        return planillaman.Update_Trafico( numplaori, pto_control_ultreporte, fecha_ult_reporte, pto_control_proxreporte, fecha_prox_reporte, creation_date, creation_user, zona, ult_observacion );
        
    }
    
    /** Funcion publica que obtiene el metodo Update_Rep_Mov_Trafico del DAO */
    public String Update_Rep_Mov_Trafico ( String fecrep, String numplaori, String numplaman ) throws SQLException {
        
       return planillaman.Update_Rep_Mov_Trafico( fecrep, numplaori, numplaman );
        
    }
    
    /** Funcion publica que obtiene el metodo Delete_Planilla_Manual_Ingreso_Trafico del DAO */
    public String Delete_Planilla_Manual_Ingreso_Trafico ( String numplaman ) throws SQLException {
        
        return planillaman.Delete_Planilla_Manual_Ingreso_Trafico( numplaman );
        
    }
    
    /** Funcion publica que obtiene el metodo Delete_Planilla_Manual_Trafico del DAO */
    public String Delete_Planilla_Manual_Trafico ( String numplaman ) throws SQLException {
        
        return planillaman.Delete_Planilla_Manual_Trafico( numplaman );
        
    }
    
    /** Funcion publica que obtiene el metodo Delete_Planilla_Manual_Rep_Mov_Trafico del DAO */
    public String Delete_Planilla_Manual_Rep_Mov_Trafico ( String numplaman ) throws SQLException {
        
        return planillaman.Delete_Planilla_Manual_Rep_Mov_Trafico( numplaman );
        
    }
    
    /** Funcion publica que obtiene el metodo Anular_Planilla_Manual_Despacho_Manual del DAO */
    public String Anular_Planilla_Manual_Despacho_Manual ( String numplaman, String usuario ) throws SQLException {
        
        return planillaman.Anular_Planilla_Manual_Despacho_Manual( numplaman, usuario );
        
    }
    
    /** Funcion publica que obtiene el metodo PlacaPlanillaManual del DAO */
    public String PlacaPlanillaManual ( String numplaman ) throws SQLException {
        
        planillaman.PlacaPlanillaManual( numplaman );
        return planillaman.PlacaPlanillaManual( numplaman );
        
    }
    
    /** Funcion publica que obtiene el metodo PlacaPlanillaOriginal del DAO */
    public String PlacaPlanillaOriginal ( String numplaori ) throws SQLException {
        
        planillaman.PlacaPlanillaOriginal( numplaori );
        return planillaman.PlacaPlanillaOriginal( numplaori );
        
    }
    
    /** Funcion publica que obtiene el metodo RutaPlanillaManual del DAO */
    public String RutaPlanillaManual ( String numplaman ) throws SQLException {
        
        planillaman.RutaPlanillaManual( numplaman );
        return planillaman.RutaPlanillaManual( numplaman );
        
    }
    
    /** Funcion publica que obtiene el metodo RutaPlanillaOriginal del DAO */
    public String RutaPlanillaOriginal ( String numplaori ) throws SQLException {
        
        planillaman.RutaPlanillaOriginal( numplaori );
        return planillaman.RutaPlanillaOriginal( numplaori );
        
    }
    
    /** Funcion publica que obtiene el metodo ClientePlanilla del DAO */
    public String ClientePlanilla ( String numpla ) throws SQLException {
        
        planillaman.ClientePlanilla( numpla );
        return planillaman.ClientePlanilla( numpla );
        
    }
    
    /** Funcion publica que obtiene el metodo getClientes del DAO */
    public Vector getVectorClientes () throws SQLException {
        
        return planillaman.getClientes();
        
    } 
        
    /*******************************************/
    /** Funcion publica que obtiene el metodo RutaPlanillaIngresoTrafico del DAO */
    public String RutaPlanillaIngresoTrafico ( String numplaori ) throws SQLException {
        
        planillaman.RutaPlanillaIngresoTrafico( numplaori );
        return planillaman.RutaPlanillaIngresoTrafico( numplaori );
        
    }
    /*******************************************/
    
    /** Funcion publica que obtiene el metodo buscarPlanilla del DAO */
    public void buscarPlanilla ( String numpla ) throws SQLException {
        
        planillaman.buscarPlanilla ( numpla );
        
    }
    
    /** Funcion publica que obtiene el metodo buscarRepMovTra del DAO */
    public void buscarRepMovTra ( String numpla ) throws SQLException {
        
        planillaman.buscarRepMovTra ( numpla );
        
    }
    public void buscarRepMovTra2 ( String numpla, String numplaori ) throws SQLException {
        
        planillaman.buscarRepMovTra2 ( numpla, numplaori );
        
    }
    
    /** Funcion publica que obtiene el metodo Update_Rep_Mov_Trafico_2 del DAO */
    public String Update_Rep_Mov_Trafico_2 ( String numplaori, String numplaman, String reg_status, String last_update, String user_update ) throws SQLException {
        
        planillaman.Update_Rep_Mov_Trafico_2 ( numplaori, numplaman, reg_status, last_update, user_update );
        return planillaman.Update_Rep_Mov_Trafico_2 ( numplaori, numplaman, reg_status, last_update, user_update );
        
    }
    
    /** Funcion publica que obtiene el metodo Update_Rep_Mov_Trafico_Minuto del DAO */
    public String Update_Rep_Mov_Trafico_Minuto ( String numpla, String fecrep, int minuto ) throws SQLException {
        
        planillaman.Update_Rep_Mov_Trafico_Minuto( numpla, fecrep, minuto );
        return planillaman.Update_Rep_Mov_Trafico_Minuto( numpla, fecrep, minuto );
        
    }
    
    /** Funcion publica que obtiene el metodo existePlanillaEnTrafico del DAO */
    public boolean existePlanillaEnTrafico ( String numplaori ) throws SQLException {
        
        planillaman.existePlanillaEnTrafico ( numplaori );
        return planillaman.existePlanillaEnTrafico ( numplaori );
        
    }
    
    /** Funcion publica que obtiene el metodo ClientePlanilla_Caso_2 del DAO */
    public String ClientePlanilla_Caso_2 ( String numpla ) throws SQLException {
        
        planillaman.ClientePlanilla_Caso_2( numpla );
        return planillaman.ClientePlanilla_Caso_2( numpla );
        
    }
    
    /** Funcion publica que obtiene el metodo PlacaPlanilla_Caso_2 del DAO */
    public String PlacaPlanilla_Caso_2 ( String numplaori ) throws SQLException {
        
        planillaman.PlacaPlanilla_Caso_2( numplaori );
        return planillaman.PlacaPlanilla_Caso_2( numplaori );
        
    }
    
}