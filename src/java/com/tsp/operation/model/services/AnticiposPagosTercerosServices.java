  /***************************************
  * Nombre Clase ............. AnticiposPagosTercerosServices.java
  * Descripci?n  .. . . . . .  Sevicio para anticipos pago a terceros
  * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
  * Fecha . . . . . . . . . .  04/08/2006
  * versi?n . . . . . . . . .  1.0
  * Copyright ...............  Transportes Sanchez Polo S.A.
  *****************************************************************/

package com.tsp.operation.model.services;


import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import javax.servlet.*;
import javax.servlet.http.*;






import com.tsp.operation.model.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.beans.Usuario;

import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import com.tsp.util.Util;

//librerias pdf
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.Image;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Rectangle;
import com.mchange.v2.c3p0.C3P0Registry;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;


import java.sql.*;

public class AnticiposPagosTercerosServices {


    AnticiposPagosTercerosDAO  AnticiposPagosTercerosDataAccess;
    List               listAgencias;
    List               listBancos;
    List               tipoCta;

    String             TABLA_FORMATO_BANCOS = "FBANCO";
    String             TABLA_CTA            = "TCUENTA";

    List               listCTATercero;
    List               listPorAprobar;
    List               listPorTransferir;

    String             distrito      = "";
    String             proveedor     = "";
    String             nameProveedor = "";

    String            ckAgencia_ ;
    String            Agencia_;
    String            ckPropietario_;
    String            Propietario_;
    String            ckPlanilla_;
    String            Planilla_;
    String            ckPlaca_;
    String            Placa_;

    String            ckConductor_;
    String            Conductor_;

    List               listParaReversar;
private Movpla     movpla;

    List               listRepetidosPorTransferir;//20101013

    /**
     * M�todos que revierte un prontopago
     * @autor.......jbarros
     * @throws      Exception
     * @version.....1.0.
     **/
    public java.util.List Listar_OC_Fec(String sec)throws Exception{
        boolean bool=false;
        try{
            listParaReversar = this.AnticiposPagosTercerosDataAccess.Listar_OC_Fec(sec);
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return listParaReversar;
    }


    /**
     * M�todos que busca un movpla por la secuencia y la planilla
     * @autor.......jbarros
     * @throws      Exception
     * @version.....1.0.
     **/
    public Movpla searchMovPla(String planilla,String sec)throws Exception{
        boolean bool=false;
        try{
            movpla = this.AnticiposPagosTercerosDataAccess.searchMovPla(planilla,sec);
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return movpla;
    }


    /**
     * M�todos que elimina el extracto y el extracto detalle por medio de la secuencia
     * @autor.......jbarros
     * @throws      Exception
     * @version.....1.0.
     **/
    public String Eliminar_Extracto_ExtractoDetalle(String sec)throws Exception{
        String sql="";
        try{
            sql = this.AnticiposPagosTercerosDataAccess.Eliminar_Extracto_ExtractoDetalle(sec);
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return sql;
    }




    public AnticiposPagosTercerosServices() {
        AnticiposPagosTercerosDataAccess  =  new  AnticiposPagosTercerosDAO();
        listAgencias                      =  new  LinkedList();
        listBancos                        =  new  LinkedList();
        tipoCta                           =  new  LinkedList();
        listCTATercero                    =  new  LinkedList();
        ini();
    }
    public AnticiposPagosTercerosServices(String dataBaseName) {
        AnticiposPagosTercerosDataAccess  =  new  AnticiposPagosTercerosDAO(dataBaseName);
        listAgencias                      =  new  LinkedList();
        listBancos                        =  new  LinkedList();
        tipoCta                           =  new  LinkedList();
        listCTATercero                    =  new  LinkedList();
        ini();
    }




    public void ini(){
        listPorAprobar     =  new LinkedList();
        listPorTransferir  =  new LinkedList();
        listCTATercero     =  new  LinkedList();
        distrito           = "";
        proveedor          = "";

        Agencia_           = "";
        Propietario_       = "";
        Planilla_          = "";
        Placa_             = "";
        Conductor_         = "";

    }



     /**
     * M�todos que carga las agencias
     * @autor.......fvillacob
     * @throws      Exception
     * @version.....1.0.
     **/
    public void loadAgencias()throws Exception{
        try{
            this.listAgencias = this.AnticiposPagosTercerosDataAccess.searchAgencias();
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }




    /**
     * M�todos que carga los bancos
     * @autor.......fvillacob
     * @throws      Exception
     * @version.....1.0.
     **/
    public void loadBancos()throws Exception{
        try{
            this.listBancos =  this.AnticiposPagosTercerosDataAccess.getTablaGen(TABLA_FORMATO_BANCOS);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }




    /**
     * M�todos que busca el nit del tercero asociado al usuario
     * @autor.......fvillacob
     * @throws      Exception
     * @version.....1.0.
     **/
    public String getProveedorUser( String login)throws Exception{
        String  nit = "";
        try{
            nit =  this.AnticiposPagosTercerosDataAccess.getTerceroUser(login);
        }catch(Exception e){
            throw new Exception( e.getMessage());
        }
        return  nit;
    }




    /**
     * M�todos que carga los tipos de cuentas
     * @autor.......fvillacob
     * @throws      Exception
     * @version.....1.0.
     **/
    public void loadTipoCTA()throws Exception{
        try{
            this.tipoCta =  this.AnticiposPagosTercerosDataAccess.getTablaGen(TABLA_CTA);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }




     /**
     * M�todos que busca info del banco en tabla general dep del tipo
     * @autor.......fvillacob
     * @throws      Exception
     * @version.....1.0.
     **/
    public Hashtable  getInfoBanco(String tipo, String codigo)throws Exception{
        Hashtable  info =  null;
        try{
              info = this.AnticiposPagosTercerosDataAccess.getInfoBanco(tipo, codigo);
        }catch(Exception e){
            throw new Exception( "getInfoBanco: " + e.getMessage());
        }
        return info;
    }




    /**
     * M�todos que busca banco del  nit
     * @autor.......fvillacob
     * @throws      Exception
     * @version.....1.0.
     **/
    public Hashtable  getBancoNIT(String nit)throws Exception{
        Hashtable  info =  null;
        try{
              info = this.AnticiposPagosTercerosDataAccess.getBancoNIT(nit);
        }catch(Exception e){
            throw new Exception( " getBancoNIT: " + e.getMessage());
        }
        return info;
    }





    /**
     * M�todos que busca los anticipos por aprobar para un proveedor y filtros
     * @autor.......fvillacob
     * @throws      Exception
     * @version.....1.0.
     **/
    public void getAnticipos_por_Aprobar(   String distrito,        String proveedor,
                                            String ckAgencia,       String Agencia ,
                                            String ckPropietario,   String Propietario,
                                            String ckPlanilla,      String Planilla,
                                            String ckPlaca ,        String Placa,
                                            String ckConductor,     String Conductor
                                    ) throws Exception{

         listPorAprobar    =  new LinkedList();
         try{
             this.distrito           =  distrito;
             this.proveedor          =  proveedor;
             this.nameProveedor      = this.AnticiposPagosTercerosDataAccess.getNamePropietario(proveedor);

             Agencia_           = Agencia;
             Propietario_       = Propietario;
             Planilla_          = Planilla;
             Placa_             = Placa;

             ckAgencia_         = ckAgencia;
             ckPropietario_     = ckPropietario;
             ckPlanilla_        = ckPlanilla;
             ckPlaca_           = ckPlaca;

             ckConductor_       =  ckConductor;
             Conductor_         =  Conductor;


             listPorAprobar = this.AnticiposPagosTercerosDataAccess.searchAnticipos(distrito, proveedor, ckAgencia, Agencia, ckPropietario, Propietario, ckPlanilla, Planilla, ckPlaca, Placa , ckConductor, Conductor );

         }catch(Exception e){
            throw new Exception(e.getMessage());
         }
    }







    /**
     * M�todos que refresca la vista de aprobacion
     * @autor.......fvillacob
     * @throws      Exception
     * @version.....1.0.
     **/
    public void refrescarAnticipos_por_Aprobar() throws Exception{
         listPorAprobar    =  new LinkedList();
         try{
             listPorAprobar = this.AnticiposPagosTercerosDataAccess.searchAnticipos(distrito,
                                                                                    proveedor,
                                                                                    ckAgencia_,
                                                                                    Agencia_,
                                                                                    ckPropietario_,
                                                                                    Propietario_,
                                                                                    ckPlanilla_,
                                                                                    Planilla_,
                                                                                    ckPlaca_,
                                                                                    Placa_ ,
                                                                                    ckConductor_,
                                                                                    Conductor_
                                                                                    );
         }catch(Exception e){
            throw new Exception(e.getMessage());
         }
    }







    /**
     * M�todos que busca los anticipos por transferir para un proveedor
     * @autor.......fvillacob
     * @throws      Exception
     * @version.....1.0.
     **/
    public void getAnticipos_por_Transferir(   String distrito, String proveedor,String Banco,String Nocuenta, String propietario, Usuario usuario ) throws Exception{
         listPorTransferir       =  new LinkedList();
         this.distrito           =  distrito;
         this.proveedor          =  proveedor;
         this.nameProveedor      = this.AnticiposPagosTercerosDataAccess.getNamePropietario(proveedor);

         try{
             listPorTransferir = this.AnticiposPagosTercerosDataAccess.searchAnticipos_transferir(distrito, proveedor,Banco,Nocuenta, propietario, usuario );
         }catch(Exception e){
            throw new Exception(e.getMessage());
         }
    }




    /**
     * M�todos que busca nombre del nit
     * @autor.......fvillacob
     * @throws      Exception
     * @version.....1.0.
     **/
    public String getNameNIT(String nit)throws Exception{
        String name="";
        try{
            name = this.AnticiposPagosTercerosDataAccess.getNameNit(nit);
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return name;
    }



    /**
     * M�todos que refresca la vista de transferencia
     * @autor.......fvillacob
     * @throws      Exception
     * @version.....1.0.
     **/
    public void refrescarAnticipos_por_Transferir(String Banco,String Ncuenta, String propietario,Usuario user) throws Exception{
         listPorTransferir       =  new LinkedList();
         try{
               listPorTransferir = this.AnticiposPagosTercerosDataAccess.searchAnticipos_transferir(this.distrito, this.proveedor,Banco,Ncuenta, propietario, user);
         }catch(Exception e){
            throw new Exception(e.getMessage());
         }
    }




    /**
     * M�todos que busca los anticipos por transferir para un proveedor
     * @param nit
     * @param sec
     * @param anticipo
     * @param Banco
     * @return 
     * @autor.......fvillacob
     * @throws      Exception
     * @version.....1.0.
     **/
    public String asignarCTA( String nit, String sec, String anticipo,String Banco ) throws Exception{
         try{
             return this.AnticiposPagosTercerosDataAccess.asignatCta(nit, sec, anticipo,Banco);
         }catch(Exception e){
            throw new Exception(e.getMessage());
         }
        
    }



    /**
     * M�todos que asigna porcentaje de descuento para el anticipo
     * @param nit
     * @param anticipo
     * @param valor
     * @param Banco
     * @return 
     * @autor.......fvillacob
     * @throws      Exception
     * @version.....1.0.
     **/
    public String asignarDescuento( String nit,  String anticipo, double valor,String Banco ) throws Exception{
         try{
             this.AnticiposPagosTercerosDataAccess.asignarDescuento(nit, valor);
             return this.AnticiposPagosTercerosDataAccess.aplicarValores(nit, anticipo,Banco);             
         }catch(Exception e){
            throw new Exception(e.getMessage());
         }
         
         
    }

    public String aplicarValores2( String nit,  String anticipo ) throws Exception{
         try{
             return this.AnticiposPagosTercerosDataAccess.aplicarValores2(nit, anticipo);
         }catch(Exception e){
            throw new Exception(e.getMessage());
         }
    }

    public String aplicarValores( String nit,  String anticipo,String Banco ) throws Exception{
         try{
             return this.AnticiposPagosTercerosDataAccess.aplicarValores(nit, anticipo,Banco);
         }catch(Exception e){
            throw new Exception(e.getMessage());
         }
    }


    /**
     * M�todos que busca los bancos asociados al propietario
     * @autor.......fvillacob
     * @throws      Exception
     * @version.....1.0.
     **/
     public List getListaCuentas( String nit, String tipo )throws Exception{
         System.out.println("in getListaCuentas .............");
        List listaCta = new LinkedList();
        try{
            listaCta =  AnticiposPagosTercerosDataAccess.getListCuentas(nit, tipo );
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return listaCta;
    }





     /**
     * M�todos que busca los bancos del tercero
     * @autor.......fvillacob
     * @throws      Exception
     * @version.....1.0.
     **/
     public void  searchListaCuentasTercero( String nit )throws Exception{
         System.out.println("in getListaCuentas 2.............");
        this.listCTATercero  =  new LinkedList();
        try{
            listCTATercero  =  this.AnticiposPagosTercerosDataAccess.getTablaGen(nit);
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }


     public  String  cuenta_a_transferir(List lista)throws Exception{
        String sql = "";
        try{
            sql=this.AnticiposPagosTercerosDataAccess.cuenta_a_transferir(lista);
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return sql;
    }




    /**
     * M�todos que selecciona los registros marcados
     * @autor.......fvillacob
     * @throws      Exception
     * @version.....1.0.
     **/
    public List  getSeleccion(String[] ids, String modo  ) throws Exception{
        List seleccion = new LinkedList();
        List lista     = (modo.equals("AP"))?this.listPorAprobar: this.listPorTransferir;

        try{
            if( ids!= null && lista!=null)
                for(int i=0;i<ids.length;i++){
                    int id = Integer.parseInt( ids[i] );
                    for(int j=0;j<lista.size();j++){
                        AnticiposTerceros anticipo =(AnticiposTerceros)lista.get(j);
                        if(  anticipo.getId() == id ){
                             seleccion.add(anticipo);
                             lista.remove(j);
                             break;
                        }
                    }
                }


              this.listPorAprobar    = (  modo.equals("AP") )?lista : this.listPorAprobar;
              this.listPorTransferir = ( !modo.equals("AP") )?lista : this.listPorTransferir;


        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return seleccion;
    }






    /**
     * M�todos que agrupa por SQL los anticipos a transferir
     * @autor.......fvillacob
     * @throws      Exception
     * @version.....1.0.
     **/
    public List getAprupar(String[] anticipos)throws Exception{
        List aux = new LinkedList();
        try{
            aux = this.AnticiposPagosTercerosDataAccess.agrupar(anticipos);

        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return aux;
    }




        /**
     * M�todos lista  los anticipos a transferir
     * @autor.......jpinedo
     * @throws      Exception
     * @version.....1.0.
     **/
    public List getAnticipos(String[] anticipos)throws Exception{
        List aux = new LinkedList();
        try{
            aux = this.AnticiposPagosTercerosDataAccess.getAnticipos(anticipos);

        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return aux;
    }





    /**
     * M�todos que busca la serie de transferencia
     * @autor.......fvillacob
     * @throws      Exception
     * @version.....1.0.
     **/
    public String getSerie()throws Exception{
        String number="";
        try{
             number = this.AnticiposPagosTercerosDataAccess.getSerieTransferencia();
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return number;
    }






     /**
     * M�todos que aprueba los anticipos
     * @autor.......fvillacob
     * @throws      Exception
     * @version.....1.0.
     **/
    public String aprobar(List lista, String user)throws Exception{
        String sql = "";
        try{
            sql = this.AnticiposPagosTercerosDataAccess.aprobar(lista, user );
        }catch(Exception e){
            throw new Exception( " aprobar " + e.getMessage() );
        }
        return sql;
    }




    /**
     * M�todos que transfiere los anticipos
     * @autor.......fvillacob
     * @throws      Exception
     * @version.....1.0.
     **/
    public String transferir(List lista, String user)throws Exception{
        String sql = "";
        try{
            sql = this.AnticiposPagosTercerosDataAccess.transferir(lista, user );
        }catch(Exception e){
            throw new Exception( " transferir " + e.getMessage() );
        }
        return sql;
    }




    /**
     * Getter for property listAgencias.
     * @return Value of property listAgencias.
     */
    public java.util.List getListAgencias() {
        return listAgencias;
    }

    /**
     * Setter for property listAgencias.
     * @param listAgencias New value of property listAgencias.
     */
    public void setListAgencias(java.util.List listAgencias) {
        this.listAgencias = listAgencias;
    }




    /**
     * Getter for property listPorAprobar.
     * @return Value of property listPorAprobar.
     */
    public java.util.List getListPorAprobar() {
        return listPorAprobar;
    }

    /**
     * Setter for property listPorAprobar.
     * @param listPorAprobar New value of property listPorAprobar.
     */
    public void setListPorAprobar(java.util.List listPorAprobar) {
        this.listPorAprobar = listPorAprobar;
    }





    /**
     * Getter for property listPorTransferir.
     * @return Value of property listPorTransferir.
     */
    public java.util.List getListPorTransferir() {
        return listPorTransferir;
    }

    /**
     * Setter for property listPorTransferir.
     * @param listPorTransferir New value of property listPorTransferir.
     */
    public void setListPorTransferir(java.util.List listPorTransferir) {
        this.listPorTransferir = listPorTransferir;
    }

    /**
     * Getter for property listBancos.
     * @return Value of property listBancos.
     */
    public java.util.List getListBancos() {
        return listBancos;
    }

    /**
     * Setter for property listBancos.
     * @param listBancos New value of property listBancos.
     */
    public void setListBancos(java.util.List listBancos) {
        this.listBancos = listBancos;
    }

    /**
     * Getter for property tipoCta.
     * @return Value of property tipoCta.
     */
    public java.util.List getTipoCta() {
        return tipoCta;
    }

    /**
     * Setter for property tipoCta.
     * @param tipoCta New value of property tipoCta.
     */
    public void setTipoCta(java.util.List tipoCta) {
        this.tipoCta = tipoCta;
    }

    /**
     * Getter for property proveedor.
     * @return Value of property proveedor.
     */
    public java.lang.String getProveedor() {
        return proveedor;
    }

    /**
     * Setter for property proveedor.
     * @param proveedor New value of property proveedor.
     */
    public void setProveedor(java.lang.String proveedor) {
        this.proveedor = proveedor;
    }

    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }

    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }

    /**
     * Getter for property nameProveedor.
     * @return Value of property nameProveedor.
     */
    public java.lang.String getNameProveedor() {
        return nameProveedor;
    }

    /**
     * Setter for property nameProveedor.
     * @param nameProveedor New value of property nameProveedor.
     */
    public void setNameProveedor(java.lang.String nameProveedor) {
        this.nameProveedor = nameProveedor;
    }

    /**
     * Getter for property listCTATercero.
     * @return Value of property listCTATercero.
     */
    public java.util.List getListCTATercero() {
        return listCTATercero;
    }

    /**
     * Setter for property listCTATercero.
     * @param listCTATercero New value of property listCTATercero.
     */
    public void setListCTATercero(java.util.List listCTATercero) {
        this.listCTATercero = listCTATercero;
    }

     /**
     * M�todos que permite trasladar el anticipo para la tabla de terceros
     * @autor.......fvillacob
     * @throws      Exception
     * @version.....1.0.
     **/
    public String InsertAnticipo( Movpla mov , Proveedor beneficiarioPP,int secuencia,String Complemento)throws Exception{
        try{
            return  this.AnticiposPagosTercerosDataAccess.InsertAnticipo(mov, beneficiarioPP,secuencia,Complemento);
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }

     /**
     * M�todos que busca un reporte entre un rango de fechas
     * @autor.......jbarros
     * @throws      Exception
     * @version.....1.0.
     **/
    public java.util.List getReporte_CumplidosFF(String fec1,String fec2)throws Exception{
          return this.AnticiposPagosTercerosDataAccess.search_ReporteCumplidos(fec1, fec2 );
    }

       /**
     * M�todos que selecciona los los nitpro y secuencias
     * @autor.......jbarros
     * @throws      Exception
     * @version.....1.0.
     **/
    public List  Listar_NitPro_Secuencia(String ids) throws Exception{
        List seleccion = new LinkedList();
        try{
            seleccion = AnticiposPagosTercerosDataAccess.Listar_NitPro_Secuencia(ids);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return seleccion;
    }
    /**
     * M�todos que permite trasladar el anticipo para la tabla de terceros
     * @autor.......fvillacob
     * @throws      Exception
     * @version.....1.0.
     **/
    public void  pasarAnticipos( ImpresionCheque cheque , String agencia)throws Exception{
        try{

            String  sql = this.AnticiposPagosTercerosDataAccess.InsertAnticipo(cheque,agencia);
            TransaccionService  svc =  new  TransaccionService(this.AnticiposPagosTercerosDataAccess.getDatabaseName());

            //svc.crearStatement();
            svc.getSt().addBatch(sql);
            svc.execute();


        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }

     /**
       * M�todo que la cuenta de banco girador, el nombre del banco del cliente y el tipo de cuenta
       * @autor.......jbarros
       * @throws......Exception
       * @version.....1.0.
       **/
      public  List Bancos_AnticipoPT(String distr, String nit, String sec) throws Exception{
          try{
            return  this.AnticiposPagosTercerosDataAccess.Bancos_AnticipoPT(distr, nit, sec);
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
      }

        /**
       * M�todo que carga el porcentage de descuento de un cliente
       * @autor.......jbarros
       * @throws......Exception
       * @version.....1.0.
       **/
    public double  descuento( String nit )throws Exception{
        try{
            return  this.AnticiposPagosTercerosDataAccess.descuento(nit);
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }

    /**
     * M�todo que extrae los propietarios que tiene prestamos pendientes pos liquidar
     * @autor.......mfontalvo
     * @throws......Exception
     * @version.....1.0.
     **/
    public TreeMap getPropietariosConPrestamos() throws Exception{
        return this.AnticiposPagosTercerosDataAccess.getPropietariosConPrestamos();
    }

    /**
     * M�todo que extrae los propietarios que tiene prestamos pendientes pos liquidar
     * @autor.......mfontalvo
     * @throws......Exception
     * @version.....1.0.
     **/
    public Vector getPlanillasAbonosPrestamos( String beneficiario ) throws Exception{
        return this.AnticiposPagosTercerosDataAccess.getPlanillasAbonosPrestamos( beneficiario );
    }

    /**
     * Metodo para actualizar los anticipos que se utilizaron en la liquidacion
     * @autor mfontalvo
     * @param ant, Anticipo para actualizar
     * @param usuario, usuario que hizo la liquidacion
     * @throws Exception.
     */
    public String updatePlanillaPrestamosSQL ( AnticiposTerceros ant, String usuario ) throws Exception{
        return this.AnticiposPagosTercerosDataAccess.updatePlanillaPrestamosSQL( ant, usuario );
    }

      /**
     * M�todo que inserta del movpla  registros a la tabla de anticipos para terceros
     * @autor....... mfontalvo
     * @throws...... Exception
     * @parameter... Movpla mov
     * @version..... 1.0.
     **/
    public synchronized String  InsertAnticipo(AnticiposTerceros ant, Usuario usuario )throws Exception {
        return this.AnticiposPagosTercerosDataAccess.InsertAnticipo( ant, usuario );
    }

    /**
     * M�todo que extrae los propietarios que tiene prestamos pendientes pos liquidar
     * @autor.......mfontalvo
     * @throws......Exception
     * @version.....1.0.
     **/
    public Vector getTransferenciasPorID( String ids ) throws Exception{
        return this.AnticiposPagosTercerosDataAccess.getTransferenciasPorID( ids );
    }

    public boolean acordeConExtractoDetalle(String idAnticipo)  throws Exception{
        return this.AnticiposPagosTercerosDataAccess.acordeConExtractoDetalle(idAnticipo);
    }

    public List getApruparF(String[] anticipos)throws Exception{
        List aux = new LinkedList();
        try{
            aux = this.AnticiposPagosTercerosDataAccess.agruparF(anticipos);

        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return aux;
    }

    public List getApruparFGas(String[] anticipos,String comisionador,String banc)throws Exception{
        List aux = new LinkedList();
        try{
            aux = this.AnticiposPagosTercerosDataAccess.agruparFGas(anticipos,comisionador,banc);

        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return aux;
    }

    /**
     * Agrupa los pagos de acuerdo al banco, sucursal, cliente, cuenta, valor a pagar.
     * @autor tmolina
     **/
    public List groupByPrestamos(String[] anticipos)throws Exception{
        List aux = new LinkedList();
        try{
            aux = this.AnticiposPagosTercerosDataAccess.groupByPrestamos(anticipos);
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return aux;
    }

     /**
     * M�todo que busca las planillas tipo Abono prestamo ordenadas por fecha de anticipo
     * @autor   Tmolina
     * @throws  Exception
     * @version 15-may-2009
     **/
    public Vector getPlanillasAbonosPrestamosOrdenadas( String beneficiario ) throws Exception{
        return this.AnticiposPagosTercerosDataAccess.getPlanillasAbonosPrestamosOrdenadas( beneficiario );
    }

    //--ADD 16 Junio 2009

    public List getApruparN(String[] anticipos,String cxp[] )throws Exception{
        List aux = new LinkedList();
        try{
            aux = this.AnticiposPagosTercerosDataAccess.agruparN(anticipos,cxp);
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return aux;
    }
    /**
     * Actualiza n bancos de n anticipos.
     * @param lista ArrayList con los datos
     * @return Mensaje confirmando la actualizacion o diciendo que no se hizo
     * @since 2010-05-13
     * @author rhonalf
     */
    public String updateBank(ArrayList lista){
        String mens = "Actualizacion completa";
        try {
            if(lista.size()>0){
                this.AnticiposPagosTercerosDataAccess.updateBank(lista);
            }
            else{
                mens = "No se enviaron filas para modificar...";
            }
        }
        catch (Exception e) {
            mens = "No se pudo hacer la actualizacion...";
            System.out.println("Error al actualizar bancos de transferencias: "+e.toString());
            e.printStackTrace();
        }
        return mens;
    }
    public ArrayList ObtenerAnticiposNoTransferidos(String login)throws Exception{
        return this.AnticiposPagosTercerosDataAccess.ObtenerAnticiposNoTransferidos(this.AnticiposPagosTercerosDataAccess.getTerceroUser(login));
    }

    public void AnularAnticipos(String login,String [] ids,String [] obs)throws Exception{
        this.AnticiposPagosTercerosDataAccess.AnularAnticipos(login,ids,obs);
    }

    public String obtainAnticiposSemiRepetidos(List ants)throws Exception{//20101014 se hallan los semirepetidos para la lista
        try{
            String respuesta="0";
            if (ants!=null && ants.size()>0){
                listRepetidosPorTransferir= this.AnticiposPagosTercerosDataAccess.obtainAnticiposSemiRepetidos(ants);
            }else{
                listRepetidosPorTransferir=null;
            }
            if (listRepetidosPorTransferir!=null && listRepetidosPorTransferir.size()>0){            respuesta=""+listRepetidosPorTransferir.size();        }//20101014
            return respuesta;
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("errror en obtainAnticiposSemiRepetidos"+e.toString());
            throw new Exception( e.getMessage() );
        }
    }

    public String esAnticipoSemiRepetido(String idx)throws Exception{//20101013 se evalua si es semirepetido
        try{
            String respuesta="";
            for (int i=0 ; i<listRepetidosPorTransferir.size();i++){//se recorren los repetidos
                if (idx.equals(listRepetidosPorTransferir.get(i))){//si es igual a 1 repetido
                    respuesta="S";
                }
            }
            return respuesta;
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("errror en esAnticipoSemiRepetido"+e.toString());
            throw new Exception( e.getMessage() );
        }
    }

  /**jpinedo
     * devuelve una lista con los anticipos  trasferido en una misma hora filtrado por propietario
     * @param nit, y fecha
     * @return Listado con los datos encontrados
     * @throws Exception Cuando hay un error
     */
    public List getGrupoFecTransferencia(List va,String nit,String fecha_desm) throws Exception{
        List tem =new LinkedList();
        try {

            for(int i=0;i<va.size();i++)
            {
                AnticiposTerceros  ant = (AnticiposTerceros)va.get(i);
                if(ant.getPla_owner().equals(nit) && ant.getFecha_transferencia().equals(fecha_desm))
                {
                    tem.add(ant);
                }
                ant=null;
            }


        }
        catch (Exception e) {
            throw new Exception("Error en agurpando negocios  "+e.toString());
        }
        return tem;
    }


       public double Total_pagado( List lista )
       {

           double vlr=0;
            AnticiposTerceros anticipo = new AnticiposTerceros();
           for (int i=0;i<lista.size();i++)
           {
               anticipo = (AnticiposTerceros)lista.get(i);
               vlr=vlr+ anticipo.getVlrConsignar();
           }
           return vlr;
    }


   public double Total_pagado_X_Secuencia( List lista, int secuencia )
       {

           double vlr=0;
            AnticiposTerceros anticipo = new AnticiposTerceros();
           for (int i=0;i<lista.size();i++)
           {

               anticipo = (AnticiposTerceros)lista.get(i);
               if(anticipo.getSecuencia()==secuencia)
               {
               vlr=vlr+ anticipo.getVlrConsignar();
               }
           }
           return vlr;
    }

        public List FiltrarXPlanillas(List va) throws Exception{
       List tem =new LinkedList();
        try {

           // boolean sw = this.ValidarLiquidacion(va);

           /* if(sw);
            {*/
                for(int i=0;i<va.size();i++)
                {
                    AnticiposTerceros  ant = (AnticiposTerceros)va.get(i);

                    if((ant.getPlanilla().substring(0, 1).equals("E"))&& va.size()>=1)
                    {
                        ant.setVlrConsignar(this.Total_pagado_X_Secuencia(va,ant.getSecuencia()));
                        tem.add(ant);
                    }
                    ant=null;
                }
            //}
        }
        catch (Exception e)
        {
            throw new Exception("Error en filtrandoxplanillas  "+e.toString());
        }
        return tem;
    }


       public boolean  ValidarLiquidacion( List lista )
       {

           boolean  sw=false;
            AnticiposTerceros anticipo = new AnticiposTerceros();
           for (int i=0;i<lista.size();i++)
           {

               anticipo = (AnticiposTerceros)lista.get(i);
               if((anticipo.getPlanilla().substring(0, 1).equals("E"))&& lista.size()>1)
               {
                  sw = true;

               }

               i=10000;

           }
           return sw;
    }







      public String directorioArchivo(String user, String cons, String extension) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user.toUpperCase();
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
            ruta = ruta + "/" + cons +/* "_" + fmt.format(new Date()) +*/ "." + extension;
        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }

          public String directorioArchivo(String user) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user.toUpperCase();
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }

        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;

    }



 /***********************************************soporte pdf anticipos ************************************************************/
public boolean exportarReportePdf(String userlogin,List lista,String nit,String fecha) throws Exception {
        String  url_logo="fintrapdf.gif";
        NitSot pr= new NitSot();
        NitDAO nitDAO = new NitDAO();
        pr =nitDAO.searchNit(nit);
            lista=this.getGrupoFecTransferencia(lista, nit, fecha);
            double  vlr=this.Total_pagado(lista);


           boolean generado = true;
            String directorio = "";String documento_detalle="";
            ResourceBundle rb = null;
        try {
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            directorio = this.directorioArchivo(userlogin, "ReportePago"+pr.getNombre(), "pdf");
            Font fuente = new Font(Font.HELVETICA, 9);
            Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
            Document documento = null;
            documento = this.createDoc();
            PdfWriter.getInstance(documento, new FileOutputStream(directorio));


            documento.setFooter(this.getMyFooter());

            documento.open();
            documento.newPage();
            Image img= Image.getInstance(rb.getString("ruta")+"/images/"+url_logo);
            img.scaleToFit(200, 200);
            documento.add(img);
            documento.add(Chunk.NEWLINE);
            documento.add(new Paragraph("802.022.016-1",fuenteB));
            documento.add(Chunk.NEWLINE);
            Paragraph p= new Paragraph("REPORTE DE PAGOS",fuenteB);
            p.setAlignment(Element.ALIGN_CENTER);
            documento.add(p);
            documento.add(Chunk.NEWLINE);

           /*----------------------------------datos_propietarios--------------------------------------*/
            PdfPTable tabla_datos_pro = this.DatosPropietario(pr,lista);
            tabla_datos_pro.setWidthPercentage(100);
            documento.add(tabla_datos_pro);
            documento.add(Chunk.NEWLINE);
            documento.add(Chunk.NEWLINE);

           /*----------------------------------lista_trnaferencias--------------------------------------*/
            p= new Paragraph("DETALLE DE PAGOS",fuenteB);
            p.setAlignment(Element.ALIGN_LEFT);
            documento.add(p);
            documento.add(Chunk.NEWLINE);
            PdfPTable tabla_negocios = this.Trasferencias(lista);
            tabla_negocios.setWidthPercentage(100);
            documento.add(tabla_negocios);
            documento.add(Chunk.NEWLINE);

            PdfPTable tabla_total = this.Total(vlr);
            tabla_total.setWidthPercentage(100);
            documento.add(tabla_total);
            documento.add(Chunk.NEWLINE);documento.add(Chunk.NEWLINE);
            documento.close();
            //Runtime.getRuntime().exec("/home/dev1/Sitios/fintravalores-copia/fintravalores/build/web/exportar/migracion/JPINEDO/ReportePago.pdf");


        }
   catch (Exception e)
        {
            generado = false;
            System.out.println("error al generar pdf : " + e.toString());
            e.printStackTrace();
        }
        System.out.println("fin elaboracion pdf solicitud de aval");
        //Envio de Correo

        return generado;
    }
   /*********************************************************************************************************************/



 private Document createDoc() {

            Rectangle pageSize = new Rectangle(842,595);
          // Document doct=new Document(PageSize, 30, 30, 25, 25);

        Document doc = new Document(pageSize, 60, 40, 40, 40);//
        doc.addAuthor ("Fintravalores S.A");
        doc.addSubject ("Trasferencias");

        return doc;
    }


 /************************************** DATOS propietarios***************************************************/
  protected PdfPTable DatosPropietario(NitSot pr,List  lista) throws DocumentException {
        AnticiposTerceros anticipo = ( AnticiposTerceros)lista.get(0);
        String fecha=anticipo.getFecha_transferencia();
        PdfPTable tabla_temp = new PdfPTable(2);




        double vlr=this.Total_pagado(lista);
      float[] medidaCeldas = {0.200f, 0.800f};
        tabla_temp.setWidths(medidaCeldas);

        Font fuente = new Font(Font.HELVETICA, 9);
        Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();

        // fila 1
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("PROPIETARIO", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase(pr.getNombre(), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);


        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase("NIT", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase(pr.getCedula(), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);




        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase("FECHA DE PAGO", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setColspan(3);
        celda_temp.setPhrase(new Phrase(fecha.substring(8, 10)+" de "+Utility.NombreMes(Integer.parseInt(fecha.substring(5, 7)))+" de "+fecha.substring(0, 4), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);


        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase("TOTAL PAGADO", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setColspan(3);
        celda_temp.setPhrase(new Phrase(Utility.customFormat(vlr), fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

          return tabla_temp;
    }







  /************************************** trasferencias***************************************************/
  protected PdfPTable Trasferencias( List  lista) throws DocumentException {

        double vlr=0,ip=0;
         PdfPTable tabla_temp = new PdfPTable(9);
       try
       {

       AnticiposTerceros anticipo = new AnticiposTerceros();


         if(this.ValidarLiquidacion(lista))
        {
          lista=this.FiltrarXPlanillas(lista);
        }



        float[] medidaCeldas = {0.05f, 0.100f, 0.100f, 0.120f, 0.25f,0.130f,0.105f,0.100f,0.100f};
        tabla_temp.setWidths(medidaCeldas);

        Font fuente = new Font(Font.TIMES_ROMAN, 9);
        Font fuenteB = new Font(Font.TIMES_ROMAN, 9, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();
        celda_temp = new PdfPCell();

        // columna 1
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("N?", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Doc. Interno", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Planilla", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Nit", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Titular", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Banco", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Tipo Cta", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Cuenta", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);


         celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Valor", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

       for (int i=0;i<lista.size();i++)
       {
            anticipo = ( AnticiposTerceros)lista.get(i);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(""+(i+1), fuenteB));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(anticipo.getFactura_mims(), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);


            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase( anticipo.getPlanilla()  , fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);


            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase( anticipo.getNit_cuenta()  , fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(anticipo.getNombre_cuenta(), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(anticipo.getBanco(), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(anticipo.getTipo_cuenta(), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(anticipo.getCuenta(), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(Util.customFormat(anticipo.getVlrConsignar() ), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);



        }
      }
        catch (Exception e)
        {

            System.out.println("error al generar pdf : " + e.toString());
            e.printStackTrace();
        }
        return tabla_temp;

    }





  /************************************** tabla total***************************************************/
  protected PdfPTable Total( double vlr  ) throws DocumentException {


        PdfPTable tabla_temp = new PdfPTable(3);
        float[] medidaCeldas = {0.750f, 0.150f,0.100f};
        tabla_temp.setWidths(medidaCeldas);

        Font fuente = new Font(Font.HELVETICA, 9);
        Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(" ", fuenteB));
        //celda_temp.setColspan(2);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);


        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("TOTAL PAGADO", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(Utility.customFormat(vlr), fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        return tabla_temp;
    }

        public HeaderFooter getMyFooter() throws ServletException {
        try {
             Phrase p = new Phrase();
            Font fuente = new Font(Font.HELVETICA, 9);
            Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
             p.clear();


             p.add(new Paragraph("Carrera 53 # 79 - 01 Local 205 Barranquilla, Colombia\n",fuenteB));
             p.add(new Paragraph("PBX: 57 5 3679900 FAX: 57 5 3679906\n",fuenteB));
             p.add(new Paragraph("www.fintravalores.com\n",fuenteB));
             HeaderFooter header = new HeaderFooter(p, false);
             header.setBorder(Rectangle.NO_BORDER);
             header.setAlignment(Paragraph.ALIGN_CENTER);

             return header;
         } catch (Exception ex) {
            throw new ServletException("Header Error");
         }
     }







         public boolean envia_correo(String from,String to,String copia,String asunto,String msg,String ruta,String archivo)
    {
     boolean enviado=false;
     try
       {


         /************************* Envio de Correo*************************/
           Email2 emailData = null;
           String ahora = new java.util.Date().toString();
            emailData=new Email2();
            emailData.setEmailId(12345);
            emailData.setRecstatus("A");//A
            emailData.setEmailcode( "emailcode");
            emailData.setEmailfrom(from);
            emailData.setEmailto(to );
            emailData.setEmailcopyto(copia);
            emailData.setEmailHiddencopyto("");
            emailData.setEmailsubject(asunto);//"WebServiceMultiple_Fintra2" );
            emailData.setEmailbody(msg);
            emailData.setLastupdat(new Timestamp(System.currentTimeMillis()) );
            emailData.setSenderName( "sender name" );
            emailData.setRemarks("remark2");
            emailData.setTipo("E");
            emailData.setRutaArchivo(ruta);
            emailData.setNombreArchivo(archivo);


           // EmailSendingEngineService emailSendingEngineService=new EmailSendingEngineService("smtpbar.une.net.co","fintravalores@geotechsa.com","fintra21");
              EmailSendingEngineService emailSendingEngineService=new EmailSendingEngineService("smtpout.secureserver.net","jpinedo@fintravalores.com","123456");
           enviado= emailSendingEngineService.send(emailData);
            emailSendingEngineService=null;//091206
            /*************************Fin Envio de Correo*************************/
           }
           catch(Exception e)
           {

               System.out.println("Error  "+e.getMessage());
           }

     return enviado;
    }


         
             /**
     * Obtiene el valor de la secuencia del archivo para un banco
     * @param banco Codigo del banco
     * @param usuario codigo del usuario en sesion
     * @return valor de la secuencia del archivo
     * @throws Exception 
     */
    public String obtenerSecuenciaBanco(String banco, String usuario) throws Exception{
        return AnticiposPagosTercerosDataAccess.obtenerSecuenciaBanco(banco, usuario);
    }
    
    public String obtenerInfoTextoEfecty(String[] negocios) throws Exception{
        return AnticiposPagosTercerosDataAccess.obtenerInfoTextoEfecty(negocios);
    }

    public ArrayList<AnticiposTerceros> obtenerUtimasTrasferencias(String cuenta, String nit_cuenta) throws Exception {
        return AnticiposPagosTercerosDataAccess.ultimasTransferencias(cuenta,nit_cuenta);
    }

    public String updatePesoAjuste(AnticiposTerceros trans, int peso, Usuario user) throws Exception {
        return AnticiposPagosTercerosDataAccess.updatePesoAnticipo(trans, peso, user);
    }
    
     public String buscarPermisoTransferencia(Usuario user) throws Exception {
        return AnticiposPagosTercerosDataAccess.buscarPermisoTransferencia(user);
    }
     
        /**
     * M�todos que agrupa por SQL los anticipos a transferir para cuando es transferencia
     * @autor.......EGONZALEZ
     * @throws      Exception
     * @version.....1.0.
     **/
    public List getApruparTransferencias(String[] anticipos)throws Exception{
        List aux = new LinkedList();
        try{
            aux = this.AnticiposPagosTercerosDataAccess.agruparTransferencia(anticipos);

        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return aux;
    }
    
    
     public String updatePesoAjusteTrasferencias(AnticiposTerceros trans, int peso, Usuario user) throws Exception {
        return AnticiposPagosTercerosDataAccess.updatePesoAnticipoTransferencia(trans, peso, user);
    }



}
