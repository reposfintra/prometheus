/**
 * Nombre        SJCostoService.java
 * Descripci�n
 * Autor         Mario Fontalvo Solano
 * Fecha         3 de mayo de 2006, 03:14 PM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.SJCostoDAO;
import com.tsp.operation.model.beans.SJCosto;
import com.tsp.operation.model.beans.SJRuta;
import com.tsp.operation.model.beans.StdJob;
import java.util.*;

public class SJCostoService {
    
    
    private SJCostoDAO dataAccess;
    private StdJob     estandar;  
    private Vector     listado;
    private Vector     listarutas;
    private TreeMap    indicesIniciales;
    
    
    /** Crea una nueva instancia de  SJCostoService */
    public SJCostoService() {
        dataAccess = new SJCostoDAO();
    }
    
    
     /**
      * M�todo para extraer los costos de un estandar
      * @autor mfontalvo
      * @throws Exception.
      * @param distrito, distrito del estandar.
      * @param stdjob, estanadar a consultar.
      * @version 1.0.
      **/    
    public void obtenerCostos (String distrito, String stdjob) throws Exception{
        try{
            listado = dataAccess.getCostos(distrito, stdjob);
        }catch (Exception ex){
            throw new Exception ( " SJCostoService.obtenerCostos() ...\n " + ex.getMessage() );
        }
    }
    
     /**
      * M�todo para extraer inicializar los indices de los costos de 
      * un estandar almacenado en la lista,
      * @autor mfontalvo
      * @throws Exception.
      * @version 1.0.
      **/     
    public void inicializarIndices () throws Exception{
        try{
            if (listado!=null){
                TreeMap tii = new TreeMap();
                TreeMap tis = new TreeMap();
                for (int i = 0; i< listado.size() ; i++){
                    SJCosto sj = (SJCosto) listado.get(i);
                    String ii = (String) tii.get( sj.getCodOrigen() );
                    if (ii == null){
                        tii.put( sj.getCodOrigen() , String.valueOf(i) );
                        tis.put( sj.getCodOrigen() , String.valueOf(i) );
                    }
                    else{
                        String  is  = (String ) tis.get( sj.getCodOrigen() );
                        SJCosto sja = (SJCosto) listado.get( Integer.parseInt(is) );
                        sja.setIndiceSig( i );
                        tis.put( sj.getCodOrigen() , String.valueOf(i) );
                    }
                }
                indicesIniciales = tii;
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ( " SJCostoService.inicializarIndices() ...\n " + ex.getMessage() );
        }
    }
    

    /**
     * M�todo para generar las rutas de los viajes 
     * un estandar almacenado en la lista,
     * @autor mfontalvo
     * @throws Exception.
     * @version 1.0.
     **/  
    public void generarRutas () throws Exception{
        if (estandar!=null){
            generarRutas(estandar.getorigin_code(), estandar.getdestination_code());
        }
    }
    
    
    
    /**
     * M�todo para generar las rutas de los viajes 
     * un estandar almacenado en la lista,
     * @autor mfontalvo
     * @param origen, punto de partida
     * @param destino, punto de llegada
     * @throws Exception.
     * @version 1.0.
     **/     
    public void generarRutas (String origen, String destino) throws Exception{
        try{
            listarutas = null;
            if (listado!=null && indicesIniciales!=null){
                String ii = (String) indicesIniciales.get(origen);
                if (ii== null) return;
                
                // seteo de la ruta inicial para comenzar el proceso
                listarutas = new Vector();

                SJRuta ruta = new SJRuta();
                ruta.setOrigen ( origen  );
                ruta.setDestino( destino );
                ruta.setUltimo ( origen  );
                listarutas.add(ruta);
                
                for (int i=0; i<listarutas.size(); ){
                    loadRutas(listarutas, i);
                    if (  ((SJRuta) listarutas.get(i)).isFinalizado() ){
                        i++;
                    }
                }
                
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ( " SJCostoService.generarRutas() ...\n " + ex.getMessage() );
        }
    }    
    
  
    
    private int obtenerPuntoInicial (String ciudad){
        int punto = -1;
        if (indicesIniciales!=null){
            String ii = (String) indicesIniciales.get(ciudad);
            if ( ii!=null ) punto = Integer.parseInt(ii);
        }
        return punto;
    }
    
    private void loadSiguientes (Vector rutas, int pInicial){
        if (pInicial!=-1){
            SJCosto sj = (SJCosto) listado.get(pInicial);
            rutas.add( sj );
            loadSiguientes ( rutas, sj.getIndiceSig() );
        }
    }
    
    private void loadRutas(Vector rutas, int indice){
        if (indice>=0 && indice<rutas.size()){
            SJRuta  ruta = (SJRuta) rutas.get(indice);
            if (ruta!=null && !ruta.isFinalizado()){
                Vector tmp = new Vector();
                loadSiguientes(tmp, obtenerPuntoInicial( ruta.getUltimo() )  );
                if (!tmp.isEmpty()){
                    for (int i=0; i<tmp.size(); i++){
                        SJRuta  nuevo = (SJRuta ) ruta.clone();
                        SJCosto next  = (SJCosto) tmp.get(i);
                        nuevo.addNodo(next);
                        if ( i==0 )
                            rutas.set( indice, nuevo);
                        else
                            rutas.add( indice+i, nuevo);
                    }
                }
                else{
                    ruta.setFinalizado(true);
                }
            }
        }
    }
    
    
    public SJRuta obtenerRutaMinima(){
        int    pos   = -1;
        double valor = 0;
        for (int i = 0; listarutas!=null && i< listarutas.size(); i++){
            SJRuta r = (SJRuta) listarutas.get(i);
            if ( r.isExitoso() ){
                if ( pos==-1 || valor > r.getCostoTotal() ){
                    pos   = i;
                    valor = r.getCostoTotal();
                }
            }
        }
        if (pos!=-1)
            return (SJRuta) listarutas.get(pos);
        else 
            return null;
    }
    
    /**
     * Getter for property listado.
     * @return Value of property listado.
     */
    public Vector getListado() {
        return listado;
    }    
    
    /**
     * Setter for property listado.
     * @param listado New value of property listado.
     */
    public void setListado(Vector listado) {
        this.listado = listado;
    }    
    
    /**
     * Getter for property indicesIniciales.
     * @return Value of property indicesIniciales.
     */
    public java.util.TreeMap getIndicesIniciales() {
        return indicesIniciales;
    }    
    
    /**
     * Setter for property indicesIniciales.
     * @param indicesIniciales New value of property indicesIniciales.
     */
    public void setIndicesIniciales(java.util.TreeMap indicesIniciales) {
        this.indicesIniciales = indicesIniciales;
    }
    
    /**
     * Getter for property listarutas.
     * @return Value of property listarutas.
     */
    public java.util.Vector getListarutas() {
        return listarutas;
    }
    
    /**
     * Setter for property listarutas.
     * @param listarutas New value of property listarutas.
     */
    public void setListarutas(java.util.Vector listarutas) {
        this.listarutas = listarutas;
    }
    
    /**
     * Getter for property estandar.
     * @return Value of property estandar.
     */
    public com.tsp.operation.model.beans.StdJob getEstandar() {
        return estandar;
    }
    
    /**
     * Setter for property estandar.
     * @param estandar New value of property estandar.
     */
    public void setEstandar(com.tsp.operation.model.beans.StdJob estandar) {
        this.estandar = estandar;
    }
    
}
