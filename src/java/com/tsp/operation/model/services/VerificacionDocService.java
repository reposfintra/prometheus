/*
 * VerificacionDocService.java
 *
 * Created on 3 de octubre de 2005, 08:26 AM
 */

package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  dbastidas
 */
public class VerificacionDocService {
    private VerificacionDocDAO veridocdao;
    /** Creates a new instance of VerificacionDocService */
    public VerificacionDocService() {
        veridocdao = new  VerificacionDocDAO();
    }
    
    public void VerificarDocumento(VerificacionDoc documen ) throws SQLException {
        try{
            veridocdao.setDocumentos(documen); 
            veridocdao.VerificarDocumento();
        }catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
    
   public void cargarDocConductor(String ced)throws SQLException {
        try{
            veridocdao.cargarDocConductor(ced);
        }catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
    public void cargarDocPlaca(String pla)throws SQLException {
        try{
            veridocdao.cargarDocPlaca(pla);
        }catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
   
    public VerificacionDoc ObtDocumentos(){
        return veridocdao.ObtDocumentos();
    }
    public void tieneDocumentosConductor(String cedula, String estado  ) throws SQLException {
        try{
            veridocdao.tieneDocumentosConductor(cedula, estado);
        }catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
   }
   public void tieneDocumentosPlaca(String placa, String estado  ) throws SQLException {
        try{
            veridocdao.tieneDocumentosPlaca(placa, estado);
        }catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
   }
    public void Verificar(VerificacionDoc documen,String estado ) throws SQLException {
        try{
            veridocdao.setDocumentos(documen); 
            veridocdao.Verificar(estado);
        }catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
    
    public void AgregarDocumentos( String act  ) throws SQLException {
        try{
            veridocdao.AgregarDocumentos(act);
        }catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
    
   
}
