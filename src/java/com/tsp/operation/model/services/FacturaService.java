/***********************************************************************************
 * Nombre clase : ............... FacturaService.java                            *
 * Descripcion :................. Clase que instancia los metodos de DespachoManualDAO  *
 *                                con la BD.                                            *
 * Autor :....................... Ing. Andres Martinez                                  *
 * Fecha :....................... 15 de noviembre de 2005, 11:02 AM                     *
 * Version :..................... 1.0                                                   *
 * Copyright :................... Fintravalores S.A.                               *
 ***********************************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

public class FacturaService {
    private FacturaDAO dao;
    private String incluirCumplidas;

    /** Creates a new instance of DespachoManualService */
    public FacturaService() {
        this.dao= new FacturaDAO();
    }
    public FacturaService(String dataBaseName) {
        this.dao= new FacturaDAO(dataBaseName);
    }
    public Vector getRemesas(){
        return dao.getRemesas();
    }
    public void  setRemesas(Vector a){
        dao.setRemesas(a);
    }
    public void buscarRemesas(String codcli, String cumplidas,String dstrct)throws SQLException{
        try {
            dao.buscarRemesas(codcli,cumplidas,dstrct);
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public void buscarItemPdf(String factura)throws SQLException{
        try {
            dao.buscarItemPdf(factura);
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    public void buscarTodasRemesas(String codcli, String cumplidas,String dstrct)throws SQLException{
        try {
            dao.buscarTodasRemesas(codcli,cumplidas,dstrct);
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public boolean buscarCliente(String codcli,String agencia)throws SQLException{
        boolean cli= false;
        try {
            cli= dao.buscarCliente(codcli,agencia);
        }catch(SQLException e){
            e.printStackTrace();
        }
        return cli;
    }

    public double valorConvertido(String a,double b,String c) throws SQLException{
        double valor=0;
        try {
            valor= dao.valorConvertido(a,b,c);
        }catch(SQLException e){
            e.printStackTrace();
        }
        return valor;
    }

    public double valorTasa(String a,String b) throws SQLException{
        double valor=0;
        try {
            valor= dao.valorTasa(a,b);

        }catch(SQLException e){
            e.printStackTrace();
        }
        return valor;
    }

    public void buscarFacturaDetalles(String a,String b) throws SQLException{

        try {
            dao.buscarFacturaDetalles(a,b);

        }catch(SQLException e){
            e.printStackTrace();
        }

    }
    public double valorTasaVal(String a,String b,String c, String d, String e) throws SQLException{
        double valor=0;
        try {
            valor= dao.valorTasaVal(a,b,c,d,e);

        }catch(SQLException f){
            f.printStackTrace();
        }
        return valor;
    }

    public  void  buscarMoneda(String a) throws SQLException{

        try {
            dao.buscarMoneda(a);

        }catch(SQLException e){
            e.printStackTrace();
        }

    }

    public  void buscarFacturas(String distrito,String cod, String query, String agencia) throws SQLException{

        try {
            dao.buscarFacturas(distrito, cod, query,agencia);

        }catch(SQLException e){
            e.printStackTrace();
        }

    }
    public  void buscarFacturasImpresion1(String distrito,String cod,String fecha1, String fecha2) throws SQLException{

        try {
            dao.buscarFacturasImpresion1(distrito, cod, fecha1, fecha2);

        }catch(SQLException e){
            e.printStackTrace();
        }

    }
    public  void buscarFacturasImpresion2(String distrito,String fecha1, String fecha2) throws SQLException{

        try {
            dao.buscarFacturasImpresion2(distrito, fecha1, fecha2);

        }catch(SQLException e){
            e.printStackTrace();
        }

    }

    public Vector getRemesasFacturar(){
        return dao.getRemesasFacturar();
    }
    public void setRemesasFacturar(Vector a){
        dao.setRemesasFacturar(a);
    }


    public Vector getRemesasModificadas(){
        return dao.getRemesasModificadas();
    }
    public void setRemesasModificadas(Vector a){
        dao.setRemesasModificadas(a);
    }
    public factura getFactu(){
        return dao.getFactu();
    }
    public void  setFactu(factura a){
        dao.setFactu(a);
    }
    /**
     * Getter for property monedaLocal.
     * @return Value of property monedaLocal.
     */
    public java.lang.String getMonedaLocal() {
        return dao.getMonedaLocal();
    }

    /**
     * Setter for property monedaLocal.
     * @param monedaLocal New value of property monedaLocal.
     */
    public void setMonedaLocal(java.lang.String monedaLocal) {
        dao.setMonedaLocal(monedaLocal);
    }

    public void updateFechaImpresion(String f){

        try {
            dao.updateFechaImpresion(f);

        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public void escribirArchivo(factura ingreso, Vector items, String nombreArchivo, String user) {
        try{
            dao.escribirArchivo(ingreso, items, nombreArchivo, user);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public factura leerArchivo(String nombreArchivo, String user) throws SQLException{
        factura f= new factura();
        try{
            f= dao.leerArchivo(nombreArchivo, user);
        }
        catch(Exception e){
            f=null;
            e.printStackTrace();
        }
        return f;
    }
    public Vector leerArchivoItems(String nombreArchivo,String user) throws SQLException{
        Vector f = new Vector();
        try{
            f= dao.leerArchivoItems(nombreArchivo, user);
        }
        catch(Exception e){
            f=null;
            e.printStackTrace();
        }
        return f;
    }

    /**
     * Getter for property vFacturas.
     * @return Value of property vFacturas.
     */
    public java.util.Vector getVFacturas() {
        return dao.getVFacturas();
    }

    /**
     * Setter for property vFacturas.
     * @param vFacturas New value of property vFacturas.
     */
    public void setVFacturas(java.util.Vector vFacturas) {
        dao.setVFacturas(vFacturas);
    }

    public boolean statusaFacturaDetalles(String distrito,String tipodocu,String docu)throws SQLException{
        boolean h=true;
        try {
            h=dao.statusaFacturaDetalles(distrito, tipodocu, docu);

        }catch(SQLException e){
            e.printStackTrace();
        }
        return h;
    }
    public boolean statusaFactura(String distrito,String tipodocu,String docu)throws SQLException{
        boolean h=true;
        try {
            h=dao.statusaFactura(distrito, tipodocu, docu);

        }catch(SQLException e){
            e.printStackTrace();
        }
        return h;
    }

    public Vector buscarDocumentos(String tipo)throws SQLException{
        Vector h=new Vector();
        try {
            h=dao.buscarDocumentos(tipo);

        }catch(SQLException e){
            e.printStackTrace();
        }
        return h;
    }
    /**
     * Metodo : funcion que guarda un objeto tipo Ingreso detalle en un Archivo
     * @autor : Ing. Andres Martinez
     * @param : objeto ingreso, un vector de ingreso, nombre del archivo, el usuario
     * @version : 1.0
     */
    public void borrarArchivo(String nombreArchivo, String user)throws IOException{
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String path = rb.getString("ruta");
        String Ruta  = path + "/exportar/migracion/" + user+"/";
        File f = new File(Ruta + nombreArchivo );
        if(f.exists()){
            f.delete();
        }
    }
    /**
     * Getter for property vtipodoc.
     * @return Value of property vtipodoc.
     */
    public java.util.Vector getVtipodoc() {
        return dao.getVtipodoc();
    }

    /**
     * Setter for property vtipodoc.
     * @param vtipodoc New value of property vtipodoc.
     */
    public void setVtipodoc(java.util.Vector vtipodoc) {
        dao.setVtipodoc(vtipodoc);
    }
    public java.util.Vector getVDocumentos() {
        return dao.getVDocumentos();
    }
    public void setVDocumentos(java.util.Vector a) {
        dao.setVDocumentos(a);
    }
    public void buscartelementos()throws SQLException {

        dao.buscartelementos();
    }
    public void buscartunidades()throws SQLException{
        dao.buscartunidades();
    }

    /**
     * Getter for property tunidades.
     * @return Value of property tunidades.
     */
    public java.util.Vector getTunidades() {
        return dao.getTunidades();
    }
    /**
     * Getter for property telementos.
     * @return Value of property telementos.
     */
    public java.util.Vector getTelementos() {
        return dao.getTelementos();
    }

    public String[] buscarplancuentas(String a)throws SQLException{
        String h[]={"",""};
        try {
            h=dao.buscarplancuentas(a);

        }catch(SQLException e){
            e.printStackTrace();
        }
        return h;

    }
    public String buscarsubledger(String a,String id)throws SQLException{
        String h="";
        try {
            h=dao. buscarsubledger(a, id);

        }catch(SQLException e){
            e.printStackTrace();
        }
        return h;

    }
    /**
     * Getter for property todasRemesas.
     * @return Value of property todasRemesas.
     */
    public java.util.Vector getTodasRemesas() {
        return dao.getTodasRemesas();
    }

    public TreeMap listarMonedas() throws SQLException{
        return dao.listarMonedas();
    }

    /**
     * Getter for property todasRemesas.
     * @return Value of property todasRemesas.
     */
    public java.util.Vector costoReembolsable(String remesa) throws SQLException {
        return dao.costoReembolsable(remesa);
    }

    public String buscarAgenciaCobro(String codcli)throws SQLException{
        return dao.buscarAgenciaCobro( codcli);
    }

    public void buscarItemPdf( String factura, String distrito )throws SQLException{
        try {
            dao.buscarItemPdf( factura, distrito );
        }catch(SQLException e){
            e.printStackTrace();
        }
    }



    public void updateFechaImpresion( String f , String distrito ){

        try {
            dao.updateFechaImpresion( f, distrito );

        }catch(SQLException e){
            e.printStackTrace();
        }
    }




    public  void buscarFacturas(String distrito,String cod, String query) throws SQLException{

        try {
            dao.buscarFacturas(distrito, cod, query);

        }catch(SQLException e){
            e.printStackTrace();
        }

    }

    /**
     * Getter for property incluirCumplidas.
     * @return Value of property incluirCumplidas.
     */
    public java.lang.String getIncluirCumplidas() {
        return incluirCumplidas;
    }

    /**
     * Setter for property incluirCumplidas.
     * @param incluirCumplidas New value of property incluirCumplidas.
     */
    public void setIncluirCumplidas(java.lang.String incluirCumplidas) {
        this.incluirCumplidas = incluirCumplidas;
    }

    public void buscarItemsCarbon( String factura, String distrito )throws SQLException{
        try {
            dao.buscarItemsCarbon( factura, distrito );
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    //jose de la rosa 2007-01-22
    public String anularFactura(String distrito,String tipodocu,String docu, String login)throws SQLException{
        return dao.anularFactura( distrito, tipodocu, docu, login);
    }

    //jose de la rosa 2007-01-22
    public String anularTransaccion( String distrito, String tipodocu, String docu, int trans)throws SQLException{
        return dao.anularTransaccion( distrito, tipodocu, docu, trans);
    }

    /**
     * Metodo existeRemesa busca la remesa
     * @autor Ing. ivan  gomez
     * @param codcli C�digo del cliente pagador y kla remesa
     * @throws SQLException si ocurre un error en la cone�i�n con la Base de Datos
     * @version 1.0.
     */
    public boolean existeRemesa(String codcli,String remesa)throws SQLException{
        return dao.existeRemesa(codcli, remesa);
    }

    public Vector getStandar(){
        return dao.getStandar();
    }

    /**
     * Metodo buscarCiudad_Cliente busca la ciudad de un cliente en la tabla usuarios
     * @autor Ing. Juan M. Escand�n P.
     * @param  idusuario - login del usuario
     * @seee   FacturaDAO - buscarCiudad_Cliente
     * @throws SQLException si ocurre un error en la cone�i�n con la Base de Datos
     * @version 1.0.
     */
    public String buscarCiudad_Cliente( String idusuario )throws SQLException{
        return dao.buscarCiudad_Cliente(idusuario);
    }
    /**
     * Metodo calcularTotal calcula el total de las remesas a facturar
     * @autor Ing. Ivan Gomez
     * @throws SQLException si ocurre un error en la cone�i�n con la Base de Datos
     * @version 1.0.
     */
    public void calcularTotal()throws SQLException{

        double total_fac = 0;
        for(int j=0;j<dao.getRemesasFacturar().size();j++){
            factura_detalle fd = (factura_detalle)dao.getRemesasFacturar().get(j);
            total_fac += fd.getValor_item();
        }
        dao.getFactu().setValor_factura(total_fac);
    }

    /**
     * Metodo calcularTotal calcula el total de las remesas a facturar
     * @autor Ing. Ivan Gomez
     * @throws SQLException si ocurre un error en la cone�i�n con la Base de Datos
     * @version 1.0.
     */
    public void calcularTotalme()throws SQLException{

        double total_fac = 0;
        for(int j=0;j<dao.getRemesasFacturar().size();j++){
            factura_detalle fd = (factura_detalle)dao.getRemesasFacturar().get(j);
            total_fac += fd.getValor_itemme();
        }
        dao.getFactu().setValor_facturame(total_fac);
    }




    public void ReportePagosClientes(String fechaI,String fechaF )throws Exception{
        this.dao.ReportePagosClientes(fechaI,fechaF);

    }
    public java.util.Vector getVector() {
        return dao.getVector();
    }
    /**
     * Metodo : Funcion que retorna facturas de acuerdo a los filtros de fecha, y de estado de impresion
     * @autor : Ing. Juan M. Escandon P
     * @param : String distrito, String query, String agencia
     * @version : 1.0
     */
    public void buscarFacturasRangoFechas(String distrito, String query, String agencia) throws SQLException{
        try {
            dao.buscarFacturasRangoFechas(distrito, query, agencia);

        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public void ReportePagosClientes(String fechaI,String fechaF, String codcli)throws Exception{
        this.dao.ReportePagosClientes(fechaI,fechaF,codcli);

    }
    /**
     * Metodo inicialisa la Lista de Docuemntos por pagar en factura .
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param codcli C�digo del cliente
     * @param agc C�digo de la agencia de facturaci�n del cliente
     * @param agc_duena C�digo de la agencia due�a
     * @param agc_cobro C�digo de la agencia de cobro
     * @throws SQLException si ocurre un error en la cone�i�n con la Base de Datos
     * @version 1.0.
     */
     public void facturasNoPagadas(String cliente, String agc, String fecha, String dstrct, String agc_duena, String agc_cobro,String prov,String cccli,String vto,String cfact,String hc,String lim1,String lim2, String codigocliente, String convenio) throws SQLException {//20100629
        dao.facturasNoPagadas(cliente, agc, fecha, dstrct, agc_duena, agc_cobro,prov,cccli,vto,cfact,hc,lim1,lim2,codigocliente,convenio);//20100629
     }

    public String ingresarFactura(factura factu,String distrito,String usuario,String base) throws Exception{
        String a="";
        try {
            a=dao.ingresarFactura(factu, distrito, usuario, base);

        }catch(Exception e){
            e.printStackTrace();
            throw new Exception("ERROR EN ingresarFactura " + e.getMessage());
        }
        return a;
    }




    public String ingresarFacturaDetalle(factura factu,String distrito,String usuario,String base,String f) throws Exception{
        String h="";
        try {
            h=dao.ingresarFacturaDetalle(factu, distrito, usuario, base,f);

        }catch(Exception e){
            e.printStackTrace();
            throw new Exception("ERROR EN ingresarFacturaDetalle " + e.getMessage());
        }
        return h;
    }





    /**
     * Getter for property vtipodoc.
     * @return Value of property vtipodoc.
     */
    public String getNumfac() {
        return dao.getNumfac();
    }

    /**
     * Metodo : Funcion que permite obtener informacion adicional, acerca de la descripcion de las Facturas de Carbon
     * @autor : Ing. Juan M. Escandon
     * @param : Distrito , Factura
     * @version : 1.0
     */
    public factura buscarDescripcionCarbon( String distrito, factura fac ) throws SQLException{
        try {
            fac =  dao.buscarDescripcionCarbon(distrito, fac );
        }catch(SQLException e){
            e.printStackTrace();
        }
        return fac;
    }//Jescandon 10-04-07

    public String modificarFactura(factura factu,String distrito,String usuario,String codfac) throws SQLException{
        String a="";
        try {
            a=dao.modificarFactura(factu, distrito, usuario,codfac);

        }catch(SQLException e){
            e.printStackTrace();
        }
        return a;
    }
    public String borrarFacturaDetalles(String distrito,String tipodocu,String docu)throws SQLException{
        String h="";
        try {
            h=dao.borrarFacturaDetalles(distrito, tipodocu, docu);

        }catch(SQLException e){
            e.printStackTrace();
        }
        return h;
    }

    /**
     * buscarFacturaCambio
     * * descripcion: metodo para buscar informacion de una factura para el programa  cambiode agencia y formato
     * @autor : Ing. Ivan Gomez
     * @version : 1.0
     * @return :obj factura
     */
     public void buscarFacturaCambio(String dstrct,String factura)throws SQLException{
         dao.buscarFacturaCambio(dstrct, factura);
     }


     /**
     * Metodo updateImpresion, permite actualizar los parametros de impresion
     * @autor : Ing. Ivan Dario Gomez
     * @param : String distrito, String factura, String agencia_impresion, String formato
     * @version : 1.0
     */
    public void updateImpresion(String distrito, String factura, String agencia_impresion, String formato) throws SQLException {
        dao.updateImpresion(distrito, factura, agencia_impresion, formato);
    }

     /**
     * buscarDatosRemesa
     * * descripcion: metodo para buscar informacion de una remesa
     * @autor : Ing. Ivan Gomez
     * @version : 1.0
     * @return :obj factura
     */
    public Vector buscarDatosRemesa(Vector vector)throws SQLException{
        return dao.buscarDatosRemesa(vector);
    }


    /**
     * buscarMovrem
     * * descripcion: metodo para buscar informacion de una remesa
     * @autor : Ing. Ivan Gomez
     * @param : String numero de remesa y String tipo de busqueda
     *          tipo :  "TOTAL" para traer los valores totales de la remesa
     *                  "MOVIMIENTOS" para traer solo la sumatoria de los movimientos
     * @version : 1.0
     * @return :obj factura
     */
    public Movrem buscarMovrem(String dstrct,String numrem, String tipo)throws SQLException{
        return dao.buscarMovrem( dstrct ,numrem,  tipo);
    }


    public Vector getRemesasEliminadas(){
        return dao.getRemesasEliminadas();
    }
    public void setRemesasEliminadas(Vector a){
        dao.setRemesasEliminadas(a);
    }

    /* public static void main(String a []){
        FacturaService fac = new FacturaService();
        ////System.out.println("Inicia");
        try{
            Movrem obj =  fac.buscarMovrem("A1103807", "TOTAL");
            if(obj!=null){
                System.out.println("Pesoreal:     "+ obj.getPesoreal());
                System.out.println("getQty_value: "+ obj.getQty_value());
                System.out.println("getVlrrem:    "+ obj.getVlrrem()  );
                System.out.println("getVlrrem2:   "+ obj.getVlrrem2() );
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        ////System.out.println("Fin");
    }*/

    public void facturasCorficolombiana(String cliente, String agc, String fecha, String dstrct, String agc_duena, String agc_cobro,String prov,String cccli,String vto,String cfact,String hc,String lim1,String lim2, String nit_fiducia) throws SQLException {
        dao.facturasCorficolombiana(cliente, agc, fecha, dstrct, agc_duena, agc_cobro,prov,cccli,vto,cfact,hc,lim1,lim2, nit_fiducia);
    }

    public Vector getVectorFactura()throws SQLException{
        return dao.getVectorFactura();
    }

    public void inicializarNits(){
        dao.inicializarNits();
    }

    public ArrayList<String> ingresarNewFactura(factura factu, String documento, String usuario) throws Exception{
        ArrayList<String> sql =null;
        try {
            sql = dao.ingresarNewFactura(factu, documento, usuario);

        }catch(Exception e){
            e.printStackTrace();
            throw new Exception("ERROR EN ingresarNewFactura " + e.getMessage());
        }
        return sql;
    }

    public java.util.TreeMap getPrefijosCxcs()throws Exception    {//20100628
        return  dao.getPrefijosCxcs();
     }
     public java.util.TreeMap getHcs()throws Exception    {//20100628
        return  dao.getHcs();
     }

     public void facturasEndoso(String cliente, String fecha, String dstrct,String prov,String cccli,String cfact,String lim1,String lim2, String factura, String tercero) throws SQLException {
        dao.facturasEndoso(cliente, fecha, dstrct,prov,cccli,cfact,lim1,lim2, factura,tercero);
    }
      
    public String cargarListas(String tipo, String filtro){
        return dao.cargarListas(tipo, filtro);
    }
}
