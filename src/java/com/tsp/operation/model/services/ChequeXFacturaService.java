/*******************************************************************
 * Nombre Clase ............. ChequeXFacturaService.java
 * Descripci�n  .. . . . . .  Genera el Cheque para una factura
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  27/12/2005
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 ******************************************************************/



package com.tsp.operation.model.services;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.ImpresionChequeService;
import com.tsp.operation.model.DAOS.ChequeXFacturaDAO;
import com.tsp.operation.model.DAOS.ChequeFacturasCorridaDAO;
import com.tsp.operation.model.DAOS.PrechequeDAO;
import com.tsp.operation.model.DAOS.SeriesChequesDAO;
import com.tsp.operation.model.SeriesDAO;
import javax.servlet.*;
import javax.servlet.http.*;




public class ChequeXFacturaService {
    
    private  ChequeXFacturaDAO        ChequeXFacturaDataAccess;
    private  ChequeFacturasCorridaDAO ChequeFacturasCorridaDataAccess;
    private  ImpresionChequeService   ImpSvc;
    private  SeriesChequesDAO         SeriesChequesDataAccess;//jose de la rosa
    
    
    private  List                listProveedores;
    private  List                listDocumentos;
    private  List                listFacturasPro;
    
    
    private  String              RTFE = "RFTE";
    private  String              RICA = "RICA";
    private  String              RIVA = "RIVA";
    private  String              IVA  = "IVA";
    
    private Vector vector;
    
    // Parametros form.
    private  Usuario             usuario;
    private  String              proveedor;
    private  String              tipoFactura;
    private  String[]            vecFacturas;
    private  List                facturasCheque;
    
    
    private  List                listCheques;
    private  List                seriesCheques;
    private  Cheque              cheque;
    private  boolean             isProcess;
    
    //operez 24 Febrero
    private PrechequeDAO PrechequeDataAccess;
    private Vector precheques;
    private Precheque precheque;
    private String agencia_impresion;
    
        
    public ChequeXFacturaService() {
        PrechequeDataAccess             = new PrechequeDAO();
        ChequeXFacturaDataAccess        = new ChequeXFacturaDAO();
        ChequeFacturasCorridaDataAccess = new ChequeFacturasCorridaDAO();
        ImpSvc                          = new ImpresionChequeService();
        listProveedores                 = new LinkedList();
        listDocumentos                  = new LinkedList();
        listFacturasPro                 = new LinkedList();
        listCheques                     = new LinkedList();
        seriesCheques                   = new LinkedList();
        usuario                         = new Usuario();
        cheque                          = null;
        isProcess                       = false;
        precheques                      = new Vector();
        precheque                       = new Precheque();
        SeriesChequesDataAccess         = new SeriesChequesDAO();//jose 2007-04-21
        resetDatos();
    }    
    public ChequeXFacturaService(String dataBaseName) {
        PrechequeDataAccess             = new PrechequeDAO(dataBaseName);
        ChequeXFacturaDataAccess        = new ChequeXFacturaDAO(dataBaseName);
        ChequeFacturasCorridaDataAccess = new ChequeFacturasCorridaDAO(dataBaseName);
        SeriesChequesDataAccess         = new SeriesChequesDAO(dataBaseName);//jose 2007-04-21
        ImpSvc                          = new ImpresionChequeService(dataBaseName);
        listProveedores                 = new LinkedList();
        listDocumentos                  = new LinkedList();
        listFacturasPro                 = new LinkedList();
        listCheques                     = new LinkedList();
        seriesCheques                   = new LinkedList();
        usuario                         = new Usuario();
        cheque                          = null;
        isProcess                       = false;
        precheques                      = new Vector();
        precheque                       = new Precheque();
        resetDatos();
    }    
    
    
    public void resetDatos(){
        String vacio    = "";
        proveedor       = vacio;
        tipoFactura     = vacio;
        facturasCheque  = new LinkedList();
        listFacturasPro = new LinkedList();
        vecFacturas     = null;
        isProcess       = false;
        
    }
    
    
    
    
    public void resetCheque(){
        this.cheque        = null;
        this.listCheques   = new LinkedList();
        this.seriesCheques = new LinkedList();
    }
    
    
    
    /**
     * M�todos que captura valores del request enviado por el formulario
     * @autor.......fvillacob
     * @version.....1.0.
     **/
    public void loadRequest( HttpServletRequest request){
        this.proveedor    = reset( request.getParameter("provee")     );
        this.tipoFactura  = reset( request.getParameter("tipoFactura")   );
        this.vecFacturas  = request.getParameterValues("facturasCheques");
        loadFacturasCheque(this.vecFacturas);
    }
    
    
    
    /**
     * M�todos que setea valores nulos
     * @autor.......fvillacob
     * @version.....1.0.
     **/
    public String reset(String val){
        if(val==null)
            val = "";
        return val;
    }
    
    
    
    /**
     * M�todos que captura las facturas involucradas en el cheque
     * @autor.......fvillacob
     * @version.....1.0.
     **/
    public void loadFacturasCheque(String[] vec){
        facturasCheque = new LinkedList();
        if(vec!=null){
            for(int i=0;i<vec.length;i++){
                String noFactura = vec[i];
                for(int j=0;j<this.listFacturasPro.size();j++){
                    FacturasCheques factura = (FacturasCheques)listFacturasPro.get(j);
                    if(factura.getDocumento().equals(noFactura))
                        facturasCheque.add(factura);
                }
            }
        }
    }
    
    
    
    
    
    
    
    
    /**
     * M�todos que busca los proveedores del usuario, con respecto a su distrito y agencia
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  void searchProveedores()throws Exception{
        this.listProveedores = new LinkedList();
        try{
            this.listProveedores = this.ChequeXFacturaDataAccess.getProveedores( this.usuario.getDstrct(), this.usuario.getId_agencia() );
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
    /**
     * M�todos que busca los proveedores del usuario, con respecto a su distrito y agencia
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  void searchDocumentos()throws Exception{
        this.listDocumentos = new LinkedList();
        try{
            this.listDocumentos = this.ChequeXFacturaDataAccess.getDocumentos( this.usuario.getDstrct() );
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
    /**
     * M�todos que busca las facturas del proveedor
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  void searchFacturasProveedor()throws Exception{
        this.listFacturasPro = new LinkedList();
        try{
            this.listFacturasPro = this.ChequeXFacturaDataAccess.getFacturasProveedor( this.usuario.getDstrct(), this.proveedor, this.tipoFactura, this.usuario.getId_agencia());
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
   
    
    /**
     * M�todo que obtiene el ultimo procesado de la serie
     * @autor.......fvillacob
     * @version.....1.0.
     **/
    public int getSerieProcesada(String banco, String sucursal){
        int last = 0;
        for(int i=0;i<this.seriesCheques.size();i++){
            Hashtable serie = (Hashtable)this.seriesCheques.get(i);
            String bancoSerie    = (String) serie.get("banco");
            String sucursalSerie = (String) serie.get("sucursal");
            
            if (  banco.equals( bancoSerie )   &&    sucursal.equals( sucursalSerie )  ){
                int procesado = Integer.parseInt(  (String) serie.get("procesado") );
                last          = procesado;
                break;
            }
            
        }
        return last;
    }
    
    
    
    
    /**
     * M�todo que actualiza el ultimo procesado de la serie
     * @autor.......fvillacob
     * @version.....1.0.
     **/
    public void updateProcesada(String banco, String sucursal, int last){
        
        for(int i=0;i<this.seriesCheques.size();i++){
            Hashtable serie         = (Hashtable)this.seriesCheques.get(i);
            String    bancoSerie    = (String) serie.get("banco");
            String    sucursalSerie = (String) serie.get("sucursal");
            
            if (  banco.equals( bancoSerie )   &&    sucursal.equals( sucursalSerie )  ){
                serie.put("procesado", String.valueOf( last )   );
                break;
            }
            
        }
        
    }
    
    
    
    
    
    
    /**
     * M�todo que retorna el cheque a imprimir
     * @autor.......fvillacob
     * @version.....1.0.
     **/
    public void loadNextCheque(){
        this.cheque = null;
        if(this.listCheques!=null && this.listCheques.size()>0){
            this.cheque = (Cheque)listCheques.get(0);
            listCheques.remove(0);
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * M�todos que busca los bancos en las facturas
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public List getBancos()throws Exception{
        List bancos = new LinkedList();
        try{
            
            for(int i=0; i<facturasCheque.size();i++){
                FacturasCheques factura = (FacturasCheques) facturasCheque.get(i);
                String bancoFactura     = factura.getBanco();
                int  sw = 0;
                
                for(int j=0; j<bancos.size();j++){
                    String bk = (String)bancos.get(j);
                    if(bk.equals(bancoFactura) ){
                        sw=1;
                        break;
                    }
                }
                
                if( sw==0 )
                    bancos.add(bancoFactura);
            }
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return bancos;
    }
    
    
    
    /**
     * M�todos que busca las sucursales de los bancos en las facturas
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public List getSucursales(String banco)throws Exception{
        List lista = new LinkedList();
        try{
            
            for(int i=0; i<facturasCheque.size();i++){
                FacturasCheques factura = (FacturasCheques) facturasCheque.get(i);
                String bancoFactura     = factura.getBanco();
                if( bancoFactura.equals(  banco )   ){
                    String sucursalFactura  = factura.getSucursal();
                    int    sw  = 0;
                    
                    for(int j=0; j<lista.size();j++){
                        String suc = (String)lista.get(j);
                        if( suc.equals(sucursalFactura) ){
                            sw = 1;
                            break;
                        }
                    }
                    
                    if( sw==0 )
                        lista.add(sucursalFactura);
                    
                }
            }
            
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return lista;
    }
    
    
    
    /**
     * M�todos que busca las monedas de las las sucursales de los bancos en las facturas
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public List getMonedas(String banco, String sucursal)throws Exception{
        List lista = new LinkedList();
        try{
            
            for(int i=0; i<facturasCheque.size();i++){
                FacturasCheques factura = (FacturasCheques) facturasCheque.get(i);
                String bancoFactura     = factura.getBanco();
                String sucursalFactura  = factura.getSucursal();
                
                if( bancoFactura.equals(  banco )  &&    sucursalFactura.equals( sucursal )    ){
                    
                    String monedaBancoFactura  = factura.getMonedaBanco();
                    int    sw  = 0;
                    
                    for(int j=0; j<lista.size();j++){
                        String money = (String)lista.get(j);
                        if( money.equals( monedaBancoFactura ) ){
                            sw = 1;
                            break;
                        }
                    }
                    
                    if( sw==0 )
                        lista.add(monedaBancoFactura);
                    
                }
            }
            
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return lista;
    }
    
    
    
    
    /**
     * M�todos que busca las facturas con la misma  monedas de las las sucursales de los bancos en las facturas
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public List getFacturas(String banco, String sucursal, String moneda)throws Exception{
        List lista = new LinkedList();
        try{
            
            for(int i=0; i<facturasCheque.size();i++){
                FacturasCheques factura = (FacturasCheques) facturasCheque.get(i);
                String bancoFactura     = factura.getBanco();
                String sucursalFactura  = factura.getSucursal();
                String monedaFactura    = factura.getMonedaBanco();
                if( bancoFactura.equals(  banco )  &&    sucursalFactura.equals( sucursal )  &&  monedaFactura.equals(moneda)    ){
                    lista.add(factura);
                }
            }
            
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return lista;
    }
    
    
    /**
     * M�todos que calcula valor de la factura a partir de sus items.
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public FacturasCheques  calcularVlrFactura(FacturasCheques factura)throws Exception{
        try{
            
            double  vlrFactura  =  this.ChequeXFacturaDataAccess.getVlrItemFactura( factura );
            factura.setVlrFactura   ( vlrFactura );
            
        }catch(Exception e){
            throw new Exception( " calcularVlrFactura " + e.getMessage() );
        }
        
        return factura;
    }
            
    /**
     * M�todos que actualiza el cheque en las tablas: Egreso, Egresodet, cxp_doc
     * @autor.......fvillacob
     * @parameter   double vlrPagar
     * @throws......Exception
     * @version.....1.0.
     **/
    
    public String updateCheque(double vlrPagar)throws Exception{
        String sql = "";
        try{
            
            if(cheque!=null){
                
                
                // CHEQUE:
                sql += this.ChequeXFacturaDataAccess.insertEgreso    ( cheque,  vlrPagar,  this.usuario.getLogin() );
                
                
                // DETALLE:
                List listFacturas =  cheque.getFacturas();
                
                if(vlrPagar== cheque.getMonto()){   // CANCELACION:
                    for(int i=0;i<listFacturas.size();i++){
                        FacturasCheques    factura = (FacturasCheques) listFacturas.get(i);
                        factura.setTipo_pago("C");
                        double saldo  =    factura.getVlrPagar();
                        String item   =    String.valueOf( i+ 1 );
                        sql += this.ChequeXFacturaDataAccess.insertEgresoDet(cheque, factura, item ,  saldo, this.usuario.getLogin() );
                        sql += this.ChequeXFacturaDataAccess.updateCXP_DOC   (cheque.getNumero(), factura, saldo,  this.usuario);
                    }
                }
                else{                               // ABONO:
                    for(int i=0;i<listFacturas.size();i++){
                        FacturasCheques    factura = (FacturasCheques) listFacturas.get(i);
                        double saldo  =    factura.getVlrPagar();
                        String item   =    String.valueOf( i+ 1 );
                        if(saldo>=vlrPagar){
                            factura.setTipo_pago("A");
                            sql += this.ChequeXFacturaDataAccess.insertEgresoDet(cheque, factura, item , vlrPagar,  this.usuario.getLogin() );
                            sql += this.ChequeXFacturaDataAccess.updateCXP_DOC   (cheque.getNumero(), factura, vlrPagar,  this.usuario );
                            break;
                        }
                        else{
                            factura.setTipo_pago("C");
                            sql += this.ChequeXFacturaDataAccess.insertEgresoDet(cheque, factura, item , vlrPagar,  this.usuario.getLogin() );
                            sql += this.ChequeXFacturaDataAccess.updateCXP_DOC   (cheque.getNumero(), factura, saldo, this.usuario );
                            vlrPagar = vlrPagar - saldo;
                        }
                    }
                }
                
                
                
                // SERIE:
                
                Series  serie  = cheque.getSerie();
                int     tope   = Integer.parseInt( serie.getSerial_fished_no() );
                int     ultimo = cheque.getLastNumber();
                
                if(tope > ultimo)
                    sql += this.ChequeFacturasCorridaDataAccess.ActualizarSeries(cheque, this.usuario.getLogin() );    // series
                if(tope <= ultimo )
                    sql += this.ChequeFacturasCorridaDataAccess.FinalizarSeries(cheque,  this.usuario.getLogin() );      // series
                
                
                
            }
            else
                sql = "El cheque no existe, ya fu� impreso. Favor generar otro cheque.";
            
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return sql;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //--------------------------------------------------------------------------
    // SET :
    
    
    
    /**
     * M�todos que setea el usuario
     * @autor.......fvillacob
     * @version.....1.0.
     **/
    public void setUsuario(Usuario user){
        this.usuario  = user;
    }
    
    
    public void setProveedor(String val ){
        this.proveedor = val;
    }
    
    
    public void setTipoFactura(String val){
        this.tipoFactura = val;
    }
    
    
    
    public void setListFacturasPro(List listFacturasPro) {
        this.listFacturasPro = listFacturasPro;
    }
    
    
    public void setFacturasCheque(List facturasCheque) {
        this.facturasCheque = facturasCheque;
    }
    
    
    
    
    
    
    //--------------------------------------------------------------------------
    // GET :
    
    /**
     * M�todos que devuelve la lista de proveddores de la agencia del usuario
     * @autor.......fvillacob
     * @version.....1.0.
     **/
    public List getProveedores(){
        return this.listProveedores;
    }
    
    public List getDocumentos(){
        return this.listDocumentos;
    }
    
    public Cheque getCheque(){
        return this.cheque;
    }
    
    
    public String getProveedor(){
        return this.proveedor;
    }
    
    public String getTipoFactura(){
        return this.tipoFactura;
    }
    
    
    public List getListFacturasPro() {
        return listFacturasPro;
    }
    
    public List getFacturasCheque() {
        return facturasCheque;
    }
    
    
    
    /**
     * Getter for property isProcess.
     * @return Value of property isProcess.
     */
    public boolean isIsProcess() {
        return isProcess;
    }
    
    /**
     * Setter for property isProcess.
     * @param isProcess New value of property isProcess.
     */
    public void setIsProcess(boolean isProcess) {
        this.isProcess = isProcess;
    }
    
    
    
    
    /**
     * Getter for property listCheques.
     * @return Value of property listCheques.
     */
    public java.util.List getListCheques() {
        return listCheques;
    }
    
    /**
     * Setter for property listCheques.
     * @param listCheques New value of property listCheques.
     */
    public void setListCheques(java.util.List listCheques) {
        this.listCheques = listCheques;
    }
    
    
    /**
     * M�todos que busca los proveedores del usuario, con respecto a su distrito y agencia
     * @autor.......Osvaldo P�rez Ferrer
     * @throws......Exception
     * @version.....1.0.
     **/
    public void searchProveedores( String nombre )throws Exception{
        this.listProveedores = new LinkedList();
        try{
            this.listProveedores = this.ChequeXFacturaDataAccess.getProveedores( nombre, this.usuario.getDstrct(), this.usuario.getId_agencia() );
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    /**
     * M�todos que calcula impuestos y descuestos
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public FacturasCheques  calcularImpuestos(FacturasCheques factura)throws Exception{
        try{
            
            cheque = new Cheque();
            
            double  vlrRTFE     =  this.ChequeXFacturaDataAccess.getVlrImpuestosFactura( factura, this.RTFE );
            double  vlrRICA     =  this.ChequeXFacturaDataAccess.getVlrImpuestosFactura( factura, this.RICA );
            double  vlrRIVA     =  this.ChequeXFacturaDataAccess.getVlrImpuestosFactura( factura, this.RIVA );
            double  vlrIVA      =  this.ChequeXFacturaDataAccess.getVlrImpuestosFactura( factura, this.IVA  );
            
            factura.setVlrDescuentos( vlrRICA + vlrRIVA  +  vlrIVA );
            factura.setVlrRetencion( vlrRTFE  );
            
            //Para mostrar en el cheque
            factura.setValor_rtfe( vlrRTFE );
            factura.setValor_rica( vlrRICA );
            factura.setValor_riva( vlrRIVA );
            factura.setValor_iva(  vlrIVA );
            
        }catch(Exception e){
            throw new Exception( "calcularImpuestos " + e.getMessage() );
        }
        
        return factura;
    }
    
    /**
     * M�todo que busca la moneda local
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    
    public String getMonedaLocal(String distrito)throws Exception{
        return ChequeXFacturaDataAccess.getMonedaLocal(distrito);
    }
    
    
    /**
     * Retorna el sql de anulacion del egreso
     * @autor Ing. Andr�s Maturana
     * @throws Exception
     * @version 1.0
     **/
    public String anularCheque(String dstrct, String banco, String sucursal, String cheque, String usuario)throws Exception{
        return this.ChequeXFacturaDataAccess.anularCheque(dstrct, banco, sucursal, cheque, usuario);
    }
    
    /**
     * Elimina la corrida de la fra relacionada a un cheque
     * @autor Ing. Andr�s Maturana
     * @throws Exception
     * @version 1.0
     **/
    public String deleteCorridaChequeCxP(String dstrct, String banco, String sucursal, String cheque)throws Exception{
        return this.ChequeXFacturaDataAccess.deleteCorridaChequeCxP(dstrct, banco, sucursal, cheque);
    }
    
    /**
     * Actualiza la corrida y el cheque de la fra relacionada a un cheque
     * @autor Ing. Andr�s Maturana
     * @throws Exception
     * @version 1.0
     **/
    public String updateCxP_DocCheque(String dstrct, String banco, String sucursal, String cheque)throws Exception{
        return this.ChequeXFacturaDataAccess.updateCxP_DocCheque(dstrct, banco, sucursal, cheque);
    }
    
    /**
     * Obtiene un egreso
     * @autor Ing. Andr�s Maturana
     * @throws Exception
     * @version 1.0
     **/
    public void getCheque(String dstrct, String banco, String sucursal, String cheque)throws Exception{
        this.ChequeXFacturaDataAccess.getCheque(dstrct, banco, sucursal, cheque);
    }
    
    /**
     * Getter for property egreso.
     * @return Value of property egreso.
     */
    public com.tsp.operation.model.beans.Egreso getEgreso() {
        return this.ChequeXFacturaDataAccess.getEgreso();
    }
    
    /**
     * Setter for property egreso.
     * @param egreso New value of property egreso.
     */
    public void setEgreso(com.tsp.operation.model.beans.Egreso egreso) {
        this.ChequeXFacturaDataAccess.setEgreso(egreso);
    }
    
   
    
    /**
     * Establece si el cheque digitado existe y fue impreso
     * @autor Ing. Andr�s Maturana
     * @throws Exception
     * @version 1.0
     **/
    public boolean chequeImpreso(String dstrct, String banco, String sucursal, String cheque)throws Exception{
        return this.ChequeXFacturaDataAccess.chequeImpreso(dstrct, banco, sucursal, cheque);
    }
    
     /**
     * M�todos que busca la factura
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String formarCheque()throws Exception{
        
        String  msj      = "";
        resetCheque();
        
        try{
            
            String distrito     = this.usuario.getDstrct();
            String monedaLocal  = this.ChequeXFacturaDataAccess.getMonedaLocal( distrito ) ;
            
            
            Hashtable  datoProveedor = this.ChequeXFacturaDataAccess.getBancoProveedor( distrito, this.proveedor );
            if(datoProveedor!=null){
                
                // BANCOS
                List listbancos = getBancos();
                if( listbancos.size()>0 ){
                    for(int i=0;i<listbancos.size();i++){
                        String banco = (String)listbancos.get(i);
                        
                        
                        // Esquema de Impresion:
                        List  listaEsquema   = ImpSvc.getEsquema( distrito, banco );
                        
                        if(listaEsquema!=null && listaEsquema.size()>0){
                            
                            
                            //SUCURSAL
                            List listSucursal = getSucursales(banco);
                            if( listSucursal.size()>0 ){
                                for(int j=0;j<listSucursal.size();j++){
                                    String sucursal = (String)listSucursal.get(j);
                                    
                                    Series serie =  this.ChequeXFacturaDataAccess.getSeries( distrito, banco, sucursal );
                                    if(serie!=null){
                                        
                                        
                                        // Buscamos en memoria la serie de ese banco, si ha sido procesada
                                        int lastNumberSerie  = serie.getLast_number();
                                        int serieProcesada   = getSerieProcesada(   banco, sucursal );
                                        if( serieProcesada > 0 )
                                            lastNumberSerie = serieProcesada;
                                        
                                        int contSerie        = 0;
                                        int numeroCheque     = lastNumberSerie  + contSerie;
                                        
                                        
                                        // MONEDA DE PAGO:
                                        List listMonedas = getMonedas(banco, sucursal);
                                        if( listMonedas.size()>0 ){
                                            for(int l=0;l<listMonedas.size();l++){
                                                String monedaBanco = (String) listMonedas.get(l);
                                                
                                                double netoCheque  = 0;
                                                String swMsj       = "";
                                                
                                                
                                                //FACTURAS:
                                                List listFacturas  =  getFacturas(banco, sucursal, monedaBanco);
                                                for(int k=0; k<listFacturas.size(); k++){
                                                    FacturasCheques factura = (FacturasCheques)listFacturas.get(k);
                                                    
                                                    factura.setMonedaLocal(monedaLocal );
                                                    factura = calcularImpuestos   ( factura );
                                                    factura = calcularVlrFactura  ( factura );
                                                    factura = this.convertirMoneda( factura, monedaBanco, monedaLocal);
                                                    swMsj  += factura.getMsj();
                                                    
                                                    double  vlrNeto_me  =   factura.getVlrFactura() + factura.getVlrDescuentos() +  factura.getVlrRetencion();
                                                    factura.setVlrNetoPago( vlrNeto_me );
                                                    
                                                    // Documentos relacionados:
                                                    List docRel =  this.ChequeXFacturaDataAccess.getFacturasRelacionadas( factura.getDstrct(), factura.getTipo_documento(), factura.getDocumento(), factura.getProveedor() );
                                                    for(int m=0;m<docRel.size();m++){
                                                        FacturasCheques factRel = (FacturasCheques)docRel.get(m);
                                                        
                                                        if( factRel.getTipoObjecto().equals( this.ChequeXFacturaDataAccess.TIPO_FACTURA  ) ){
                                                            factRel = calcularImpuestos    ( factRel );
                                                            factRel = calcularVlrFactura   ( factRel );
                                                            factRel = convertirMoneda(factRel, monedaBanco, monedaLocal);
                                                            swMsj  += factura.getMsj();
                                                            double  vlrNeto_rel  =   factRel.getVlrFactura() + factRel.getVlrDescuentos() +  factRel.getVlrRetencion();
                                                            factRel.setVlrNetoPago( vlrNeto_rel);
                                                        }
                                                        else{
                                                            factRel = convertirMoneda(factRel, monedaBanco, monedaLocal);
                                                            swMsj  += factura.getMsj();
                                                            factRel.setVlrNetoPago( factRel.getVlrPagar() );
                                                            factRel.setVlrFactura( factRel.getVlrPagar() );
                                                        }
                                                        
                                                    }
                                                    factura.setDocumnetosRelacionados( docRel );
                                                    
                                                    // Neto cheque:
                                                    netoCheque += factura.getVlrPagar();
                                                }
                                                
                                                
                                                
                                                
                                                // CHEQUE :
                                                
                                                if ( Integer.parseInt(serie.getSerial_fished_no())  >= numeroCheque){
                                                    
                                                    netoCheque  =  (  monedaBanco.equals("PES") ||  monedaBanco.equals("BOL")  )? (int)  Math.round(  netoCheque ) :  Util.roundByDecimal(netoCheque, 2);
                                                    
                                                    
                                                    Cheque cheque = new Cheque();
                                                    cheque.setDistrito         ( distrito );
                                                    cheque.setBanco            ( banco    );
                                                    cheque.setSucursal         ( sucursal );
                                                    cheque.setCuentaBanco      ( serie.getCuenta()             );
                                                    cheque.setAgencia          ( this.usuario.getId_agencia()  );
                                                    cheque.setMoneda           ( monedaBanco  );
                                                    cheque.setMonedaLocal      ( monedaLocal  );
                                                    cheque.setMonto            ( netoCheque   );
                                                    cheque.setVlrPagar         ( netoCheque   );
                                                    cheque.setSerie            ( serie        );
                                                    cheque.setEsquema          ( listaEsquema );
                                                    
                                                    cheque.setNumero           ( serie.getPrefijo() +  numeroCheque    );
                                                    cheque.setLastNumber       ( numeroCheque                          );
                                                    cheque.setBeneficiario     ( (String)datoProveedor.get("nit")      );//Este es el proveedor
                                                    cheque.setNombre           ( (String)datoProveedor.get("nombre")   );
                                                    cheque.setNit_beneficiario ( (String)datoProveedor.get("nit_beneficiario") );//Osvaldo
                                                    cheque.setNom_beneficiario ( (String)datoProveedor.get("beneficiario")   );//Osvaldo
                                                    cheque.setNitProveedor     ( cheque.getBeneficiario());
                                                    cheque.setAno              ( Util.getFechaActual_String(1)         );
                                                    cheque.setMes              ( Util.getFechaActual_String(3)         );
                                                    cheque.setDia              ( Util.getFechaActual_String(5)         );
                                                    
                                                    
                                                    // Facturas del Cheque
                                                    cheque.setFacturas( listFacturas );
                                                    
                                                    if( !cheque.getNom_beneficiario().equals("") ){//Osvaldo
                                                        
                                                        if(netoCheque<=0  || !swMsj.equals("")  ){
                                                            if( !swMsj.equals("") )  cheque.setComentario( swMsj );
                                                            else                     cheque.setComentario( "No se puede imprimir el cheque, su valor no es positivo" );
                                                            msj += cheque.getComentario();
                                                        }
                                                        else{
                                                            
                                                            // Serie procesada
                                                            Hashtable procesada = new  Hashtable();
                                                            procesada.put("banco",     banco    );
                                                            procesada.put("sucursal",  sucursal );
                                                            procesada.put("procesado", String.valueOf( numeroCheque ) );
                                                            
                                                            if( serieProcesada == 0 )
                                                                this.seriesCheques.add( procesada  );
                                                            else
                                                                this.updateProcesada(banco, sucursal, numeroCheque  );
                                                            
                                                            contSerie++;
                                                            numeroCheque    =  lastNumberSerie  + contSerie;
                                                            
                                                            this.listCheques.add( cheque );
                                                            
                                                        }
                                                        
                                                    } else{//Osvaldo
                                                       msj += "El Nit Beneficiario: "+cheque.getNit_beneficiario()+" no presenta nombre"; 
                                                    }
                                                    
                                                    
                                                } else{
                                                    msj += " La serie para el banco " + banco +" "+ sucursal +" ha llegado a su tope maximo" ;
                                                    break;
                                                }
                                                
                                                
                                            }
                                            
                                        }else
                                            msj += "La factura no presenta moneda de banco para pago";
                                        
                                        
                                    }else
                                        msj += "No hay serie definida para el banco " + banco +" "+ sucursal;
                                    
                                    
                                }
                                
                            }else
                                msj += "El banco " + banco +" no presenta sucursal en la factura";
                            
                        }
                        else
                            msj += "No hay esquema de impresi�n para el banco "+ banco;
                        
                    }
                    
                }else
                    msj += "Las facturas no presentan bancos para pago";
            }
            else
                msj += "El proveedor no presenta informaci�n";
            
            
            
            
        }catch(Exception e){
            throw new Exception( " formarCheque " + e.getMessage() );
        }
        return msj;
    }
    //24 Febrero de 2007
    /*Jescandon 15-02-07*/
    public String obtenerNumeroChqComp( String distrito, String banco, String sucursal  ) throws Exception {
        String numChk = "";
        try{
            Series serie =  this.ChequeXFacturaDataAccess.getSeries( distrito, banco, sucursal );
            if(serie!=null){
                // Buscamos en memoria la serie de ese banco, si ha sido procesada
                int lastNumberSerie  = serie.getLast_number();
                int serieProcesada   = getSerieProcesada(   banco, sucursal );
                if( serieProcesada > 0 )
                    lastNumberSerie = serieProcesada;
                
                int contSerie        = 0;
                int numeroCheque     = lastNumberSerie  + contSerie;
                
                numChk               = serie.getPrefijo() +  numeroCheque;
            }
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( " ObtenerNumeroChq " + e.getMessage() );
        }
        return numChk;
    }
    
     public String obtenerNumeroChq( String distrito, String banco, String sucursal  ) throws Exception {
        String numChk = "";
        try{
            Series serie =  this.ChequeXFacturaDataAccess.getSeries( distrito, banco, sucursal );
            if(serie!=null){
                // Buscamos en memoria la serie de ese banco, si ha sido procesada
                int lastNumberSerie  = serie.getLast_number();
                int serieProcesada   = getSerieProcesada(   banco, sucursal );
                if( serieProcesada > 0 )
                    lastNumberSerie = serieProcesada;
                
                int contSerie        = 0;
                int numeroCheque     = lastNumberSerie  + contSerie;
                
                numChk               = String.valueOf(numeroCheque);
            }
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( " ObtenerNumeroChq " + e.getMessage() );
        }
        return numChk;
    }
    
    
    public Cheque getEgreso( String distrito, String banco, String sucursal, String chk )  throws Exception{
        try{
            return this.ChequeXFacturaDataAccess.getEgreso(distrito, banco, sucursal, chk);
        }catch(Exception e){
            throw new Exception( " getEgreso " + e.getMessage() );
        }
    }
    
    public String insertEgreso( Cheque chk, String user, String numchk )  throws Exception{
        try{
            return this.ChequeXFacturaDataAccess.insertEgreso(chk, user, numchk);
        }catch(Exception e){
            throw new Exception( " insertEgreso " + e.getMessage() );
        }
    }
    
    public String insertEgresoDet( Cheque chk, String user, String numchk )  throws Exception{
        try{
            return this.ChequeXFacturaDataAccess.insertEgresoDet(chk, user, numchk);
        }catch(Exception e){
            throw new Exception( " insertEgresoDet " + e.getMessage() );
        }
    }
  
     /***********************************************************
     * Metodo que guarda el precheque y el detalle 
     * @autor.......Ing. Osvaldo P�rez Ferrer
     * @parameter   double vlrPagar
     * @throws......Exception
     * @version.....1.0.
     ***********************************************************/
    
    public ArrayList<String> guardarPreCheque(double vlrPagar)throws Exception{
        ArrayList<String> sql = new ArrayList<String>();
        try{
            
            if(cheque!=null){
                
                // PRECHEQUE:
                Precheque pre = new Precheque();
                
                SeriesDAO sd = new SeriesDAO(this.ChequeXFacturaDataAccess.getDatabaseName());
                String[] id = sd.obtenerSerie("PRECHQ");
                
                if( id!=null && id.length==2 ){
                }
            
                pre.setId( id[1] );
                pre.setDstrct( cheque.getDistrito() );
                pre.setBanco( cheque.getBanco() );
                pre.setSucursal( cheque.getSucursal() );
                pre.setBeneficiario( cheque.getNit_beneficiario() );
                pre.setProveedor( cheque.getNitProveedor() );
                pre.setAgencia( this.agencia_impresion );
                pre.setValor( vlrPagar );
                pre.setMoneda( cheque.getMoneda() );
                pre.setCreation_user( this.usuario.getLogin() );
                pre.setBase( this.usuario.getBase() );
                pre.setCheque( "" );
                pre.setTipo_documento( "FAP" ); //Mod TMolina 2008-08-20
                            
                sql.add(this.PrechequeDataAccess.insertPrecheque( pre ));
                
                // DETALLE:
                List listFacturas =  cheque.getFacturas();
                
                if(vlrPagar== cheque.getMonto()){   // CANCELACION:
                    for(int i=0;i<listFacturas.size();i++){
                        FacturasCheques    factura = (FacturasCheques) listFacturas.get(i);
                        pre.setTipo_pago("C");
                        double saldo  =    factura.getVlrPagar();
                        String item   =    String.valueOf( i+ 1 );
                
                        pre.setDocumento( factura.getDocumento() );
                        pre.setItem( item );
                        pre.setValor( saldo );
                        
                        sql.add(this.PrechequeDataAccess.insertPrechequeDetalle( pre ));

                    }
                }
                else{                               // ABONO:
                    for(int i=0;i<listFacturas.size();i++){
                        FacturasCheques    factura = (FacturasCheques) listFacturas.get(i);
                        double saldo  =    factura.getVlrPagar();
                        String item   =    String.valueOf( i+ 1 );
                        pre.setDocumento( factura.getDocumento() );
                        pre.setItem( item );
                        
                        if(saldo>=vlrPagar){
                            pre.setTipo_pago("A");
                            pre.setValor( vlrPagar );
                            sql.add(this.PrechequeDataAccess.insertPrechequeDetalle( pre ));
                            break;
                        }
                        else{
                            pre.setTipo_pago("C");
                            pre.setValor( saldo );

                            sql.add(this.PrechequeDataAccess.insertPrechequeDetalle( pre ));
                            vlrPagar = vlrPagar - saldo;
                        }
                    }
                }
                
                // SERIE:                
                
                sd.actualizarSerie("OPEREZ", "PRECHQ");  
                precheque = pre;
                
            }
            else
                sql.add("El cheque no existe, ya fu� impreso. Favor generar otro cheque.");
            
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return sql;
    }
    
    /**
     * M�todos metodo que agrega un precheque a la lista de cheques
     * @autor.......Ing. Osvaldo P�rez Ferrer
     * @throws......Exception
     * @version.....1.0.
     **/
    public String formarPrecheque()throws Exception{
        
        String  msj      = "";
        resetCheque();
        
        try{
            
            String distrito     = this.usuario.getDstrct();
            String monedaLocal  = this.ChequeXFacturaDataAccess.getMonedaLocal( distrito ) ;
            
            
            Hashtable  datoProveedor = this.ChequeXFacturaDataAccess.getBancoProveedor( distrito, this.proveedor );
            if(datoProveedor!=null){
                
                // BANCOS
                List listbancos = getBancos();
                if( listbancos.size()>0 ){
                    for(int i=0;i<listbancos.size();i++){
                        String banco = (String)listbancos.get(i);
                        
                        
                        //SUCURSAL
                        List listSucursal = getSucursales(banco);
                        if( listSucursal.size()>0 ){
                            for(int j=0;j<listSucursal.size();j++){
                                String sucursal = (String)listSucursal.get(j);
                                
                                // MONEDA DE PAGO:
                                List listMonedas = getMonedas(banco, sucursal);
                                if( listMonedas.size()>0 ){
                                    for(int l=0;l<listMonedas.size();l++){
                                        String monedaBanco = (String) listMonedas.get(l);
                                        
                                        double netoCheque  = 0;
                                        String swMsj       = "";
                                        
                                        
                                        //FACTURAS:
                                        List listFacturas  =  getFacturas(banco, sucursal, monedaBanco);
                                        for(int k=0; k<listFacturas.size(); k++){
                                            FacturasCheques factura = (FacturasCheques)listFacturas.get(k);
                                            
                                            factura.setMonedaLocal(monedaLocal );
                                            factura = calcularImpuestos   ( factura );
                                            factura = calcularVlrFactura  ( factura );
                                            factura = this.convertirMoneda( factura, monedaBanco, monedaLocal);
                                            swMsj  += factura.getMsj();
                                            
                                            double  vlrNeto_me  =   factura.getVlrFactura() + factura.getVlrDescuentos() +  factura.getVlrRetencion();
                                            factura.setVlrNetoPago( vlrNeto_me );
                                            
                                            // Documentos relacionados:
                                            List docRel =  this.ChequeXFacturaDataAccess.getFacturasRelacionadas( factura.getDstrct(), factura.getTipo_documento(), factura.getDocumento(), factura.getProveedor() );
                                            for(int m=0;m<docRel.size();m++){
                                                FacturasCheques factRel = (FacturasCheques)docRel.get(m);
                                                
                                                if( factRel.getTipoObjecto().equals( this.ChequeXFacturaDataAccess.TIPO_FACTURA  ) ){
                                                    factRel = calcularImpuestos    ( factRel );
                                                    factRel = calcularVlrFactura   ( factRel );
                                                    factRel = convertirMoneda(factRel, monedaBanco, monedaLocal);
                                                    swMsj  += factura.getMsj();
                                                    double  vlrNeto_rel  =   factRel.getVlrFactura() + factRel.getVlrDescuentos() +  factRel.getVlrRetencion();
                                                    factRel.setVlrNetoPago( vlrNeto_rel);
                                                }
                                                else{
                                                    factRel = convertirMoneda(factRel, monedaBanco, monedaLocal);
                                                    swMsj  += factura.getMsj();
                                                    factRel.setVlrNetoPago( factRel.getVlrPagar() );
                                                    factRel.setVlrFactura( factRel.getVlrPagar() );
                                                }
                                                
                                            }
                                            factura.setDocumnetosRelacionados( docRel );
                                            
                                            // Neto cheque:
                                            netoCheque += factura.getVlrPagar();
                                        }
                                        
                                                                                                                        
                                        // CHEQUE :
                                        
                                        
                                        netoCheque  =  (  monedaBanco.equals("PES") ||  monedaBanco.equals("BOL")  )? (double)  Math.round(  netoCheque ) :  Util.roundByDecimal(netoCheque, 2); //Cambie int por double - 6 Sept - 2008
                                        
                                        
                                        Cheque cheque = new Cheque();
                                        cheque.setDistrito         ( distrito );
                                        cheque.setBanco            ( banco    );
                                        cheque.setSucursal         ( sucursal );
                                     
                                        cheque.setMoneda           ( monedaBanco  );
                                        cheque.setMonedaLocal      ( monedaLocal  );
                                        cheque.setMonto            ( netoCheque   );
                                        cheque.setVlrPagar         ( netoCheque   );

                                        cheque.setBeneficiario     ( (String)datoProveedor.get("nit")      );//Este es el proveedor
                                        cheque.setNombre           ( (String)datoProveedor.get("nombre")   );
                                        cheque.setNit_beneficiario( (String)datoProveedor.get("nit_beneficiario") );//Osvaldo
                                        cheque.setNom_beneficiario( (String)datoProveedor.get("beneficiario")   );//Osvaldo
                                        cheque.setNitProveedor     ( cheque.getBeneficiario());
                                        
                                        
                                        
                                        // Facturas del Cheque
                                        cheque.setFacturas( listFacturas );
                                        
                                        if( !cheque.getNom_beneficiario().equals("") ){//Osvaldo
                                            
                                            if(netoCheque<=0  || !swMsj.equals("")  ){
                                                if( !swMsj.equals("") )  cheque.setComentario( swMsj );
                                                else                     cheque.setComentario( "No se puede generar el cheque, su valor no es positivo" );
                                                msj += cheque.getComentario();
                                            }
                                            else{
                                                
                                                // Serie procesada
                                                Hashtable procesada = new  Hashtable();
                                                procesada.put("banco",     banco    );
                                                procesada.put("sucursal",  sucursal );
 
                                                
                                                this.listCheques.add( cheque );
                                                
                                            }
                                            
                                        } else{//Osvaldo
                                            msj += "El Nit Beneficiario: "+cheque.getNit_beneficiario()+" no presenta nombre";
                                        }
                                        
                                    }
                                    
                                }else
                                    msj += "La factura no presenta moneda de banco para pago";
                                
                            }
                            
                        }else
                            msj += "El banco " + banco +" no presenta sucursal en la factura";
                        
                    }
                    
                }else
                    msj += "Las facturas no presentan bancos para pago";
            }
            else
                msj += "El proveedor no presenta informaci�n";
            
            
            
            
        }catch(Exception e){
            throw new Exception( " formarCheque " + e.getMessage() );
        }
        return msj;
    }
    
    public void loadPrecheques() throws Exception{        
        this.precheques = PrechequeDataAccess.getPrecheques( this.usuario.getId_agencia(), this.usuario.getDstrct() );
        
    }
    
    
    
    /**
     * Getter for property precheques.
     * @return Value of property precheques.
     */
    public java.util.Vector getPrecheques() {
        return precheques;
    }
    
    /**
     * Setter for property precheques.
     * @param precheques New value of property precheques.
     */
    public void setPrecheques(java.util.Vector precheques) {
        this.precheques = precheques;
    }
    
    
   
    
    /**
     * Getter for property vecFacturas.
     * @return Value of property vecFacturas.
     */
    public java.lang.String[] getVecFacturas() {
        return this.vecFacturas;
    }
    
    /**
     * Setter for property vecFacturas.
     * @param vecFacturas New value of property vecFacturas.
     */
    public void setVecFacturas(java.lang.String[] vecFacturas) {
        this.vecFacturas = vecFacturas;
    }
    
    /**
     * Getter for property precheque.
     * @return Value of property precheque.
     */
    public com.tsp.operation.model.beans.Precheque getPrecheque() {
        return precheque;
    }
    
    /**
     * Setter for property precheque.
     * @param precheque New value of property precheque.
     */
    public void setPrecheque(com.tsp.operation.model.beans.Precheque precheque) {
        this.precheque = precheque;
    }
    
    /**
     * M�todos que busca las facturas del proveedor
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void searchFacturasProveedorFromPrecheque( Precheque p )throws Exception{
        this.listFacturasPro = new LinkedList();
        try{//Mod TMolina 2008-08-20
            this.listFacturasPro = this.ChequeXFacturaDataAccess.getFacturasProveedorFromPrecheque( p.getId(), p.getDstrct(), p.getProveedor(), "FAP", this.usuario.getId_agencia() );
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    public void anularPrecheque( String id ) throws Exception{
        this.PrechequeDataAccess.anularPrecheque(id, this.usuario.getLogin() );
    }
    
    /**
     * Getter for property agencia_impresion.
     * @return Value of property agencia_impresion.
     */
    public java.lang.String getAgencia_impresion() {
        return agencia_impresion;
    }
    
    /**
     * Setter for property agencia_impresion.
     * @param agencia_impresion New value of property agencia_impresion.
     */
    public void setAgencia_impresion(java.lang.String agencia_impresion) {
        this.agencia_impresion = agencia_impresion;
    }
    
    public String anularChequeDetalles(String dstrct, String banco, String sucursal, String cheque, String usuario)throws Exception{
        return this.ChequeXFacturaDataAccess.anularChequeDetalles(dstrct, banco, sucursal, cheque, usuario);
    }
   
        
    /*********************************************************************
     * Metodo obtenerChequesReimprimibles, obtiene una lista con datos de
     *      cheques que se pueden reimprimir, del banco y sucursal dados
     * @param: banco, banco del cheque
     * @param: sucursal, sucursal del cheque
     * @param: ini, rango inicial de cheque
     * @param: fin, rango final de cheque
     *********************************************************************/
    public void obtenerChequesReimprimibles( String dstrct, String banco, String sucursal, String ini, String fin ) throws Exception{
        this.vector = this.ChequeXFacturaDataAccess.obtenerChequesReimprimibles(dstrct, banco, sucursal, ini, fin);
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return vector;
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        this.vector = vector;
    }
    
    /*********************************************************************
     * Metodo marcarReimpreso, deshabilita el cheque para reimpresion
     * @author: Ing. Osvaldo P�rez Ferrer
     *********************************************************************/
    public void marcarReimpreso( Cheque c ) throws Exception{
        this.ChequeXFacturaDataAccess.marcarReimpreso( c, this.usuario.getLogin() );
    }
     /**
     * M�todos que conviete moneda factura a la del banco
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public FacturasCheques convertirMoneda(FacturasCheques factura, String monedaBanco, String monedaLocal)throws Exception{
        
        try{
            
            String  hoy         = Util.getFechaActual_String(4);
            
            // Todos en moneda extranjera:
            double  vlrDesc     =  factura.getVlrDescuentos();
            double  vlrReten    =  factura.getVlrRetencion();
            double  vlrFactura  =  factura.getVlrFactura();
            double  vlrPagar    =  factura.getVlr_saldo_me();
            
            double vlr_iva      =  factura.getValor_iva();
            double vlr_riva     =  factura.getValor_riva();
            double vlr_rica     =  factura.getValor_rica();
            double vlr_rfte     =  factura.getValor_rtfe();
            
            String moneda       =  factura.getMoneda();
            double tasa         =  1;
            
            
            if( !moneda.equals( monedaBanco  ) ){
                
                TasaService  svc  =  new  TasaService(this.ChequeXFacturaDataAccess.getDatabaseName());
                Tasa         obj  = svc.buscarValorTasa(monedaLocal,  moneda , monedaBanco , hoy);
                if(obj!=null){
                    tasa        = obj.getValor_tasa();                    
                    
                    if( monedaBanco.equals("DOL") ){
                        vlrFactura *= tasa;
                        vlrReten   *= tasa;
                        vlrDesc    *= tasa;
                        vlrPagar   *= tasa;                        
                        
                        vlr_iva    *= tasa;
                        vlr_riva   *= tasa;
                        vlr_rica   *= tasa;
                        vlr_rfte   *= tasa;
                        
                    }else{
                        vlrFactura = Math.round( vlrFactura * tasa );
                        vlrReten   = Math.round( vlrReten * tasa );
                        vlrDesc    = Math.round( vlrDesc * tasa );
                        vlrPagar   = Math.round( vlrPagar * tasa );
                        
                        vlr_iva    = Math.round( vlr_iva * tasa );
                        vlr_riva   = Math.round( vlr_riva * tasa );
                        vlr_rica   = Math.round( vlr_rica * tasa );
                        vlr_rfte   = Math.round( vlr_rfte * tasa );
                    }
                    
                }
                else
                    factura.setMsj( "No hay tasa de conversi�n de " + moneda + " a " + monedaBanco +" para la factura " + factura.getDocumento() );
            }
            
            factura.setVlrFactura   ( vlrFactura );
            factura.setVlrDescuentos( vlrDesc    );
            factura.setVlrRetencion( vlrReten   );
            factura.setVlrPagar     ( vlrPagar   );
            
            factura.setValor_iva( vlr_iva );
            factura.setValor_riva( vlr_riva );
            factura.setValor_rica( vlr_rica );
            factura.setValor_rtfe( vlr_rfte );
            
            factura.setTasa         ( tasa );
            
            
        }catch(Exception e){
            throw new Exception( "convertirMoneda " + e.getMessage());
        }
        return factura;
    }
     public String formarPrechequesToCheque( Vector seleccionados ) throws Exception{
        
        Vector v = this.getPrecheques();
        String msj = "";
        
        Vector bancos = new Vector();
        
        //Obtenemos distintos bancos y sucursales de los precheques
        for( int i=0; i<v.size(); i++ ){
            Precheque p = (Precheque) v.get(i);
            if( seleccionados.contains( p.getId() ) ){
                if( !bancos.contains( p.getBanco()+"_"+p.getSucursal()) ){
                    bancos.add( p.getBanco()+"_"+p.getSucursal() );
                }
            }
        }
        
        int cont = 0;
        
        for( int j=0; j<bancos.size(); j++ ){
            String banco = (String)bancos.get(j);
            for( int i=0; i<v.size(); i++ ){
                Precheque p = (Precheque) v.get(i);
                if( seleccionados.contains( p.getId() ) ){                    
                    if( banco.equals( p.getBanco()+"_"+p.getSucursal() ) ){                        
                        this.setProveedor( p.getProveedor() );
                        this.setTipoFactura("FAP");//Mod TMolina 2008-08-20
                        this.setVecFacturas( p.getFacturas().split(",") );
                        this.searchFacturasProveedorFromPrecheque( p );
                        this.loadFacturasCheque( this.getVecFacturas() );
                        this.setPrecheque( p );
                        
                        String m = this.prechequeToCheque( p, cont );
                        msj += !m.equals("")? m+"<br>" : m;
                        cont++;
                    }
                }
            }
            cont = 0;
        }
        return msj;
    }
     
     public Series obtenerSerieActiva( String distrito, String banco, String sucursal ) throws Exception {
        Series serie = null;
        try{
            serie   =  this.ChequeXFacturaDataAccess.getSeries( distrito, banco, sucursal );
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( "ObtenerSerieActiva " + e.getMessage() );
        }
        return serie;
    }
    
    
    
    public String ActualizarSerie( String distrito, String banco, String sucursal, Cheque cheque  ) throws Exception {
        String sql = "";
        try{
            Series serie =  this.ChequeXFacturaDataAccess.getSeries( distrito, banco, sucursal );
            if(serie!=null){
                // Buscamos en memoria la serie de ese banco, si ha sido procesada
                int lastNumberSerie  = serie.getLast_number();
                int serieProcesada   = getSerieProcesada(   banco, sucursal );
                if( serieProcesada > 0 )
                    lastNumberSerie = serieProcesada;
                
                int contSerie        = 0;
                int numeroCheque     = lastNumberSerie  + contSerie;

            }
            // serie  = cheque.getSerie();
            int     tope   = Integer.parseInt( serie.getSerial_fished_no() );
            int     ultimo = cheque.getLastNumber();
            
            if(tope > ultimo)
                sql += this.ChequeFacturasCorridaDataAccess.ActualizarSeries( cheque, cheque.getUsuario_impresion() );    // series
            if(tope <= ultimo )
                sql += this.ChequeFacturasCorridaDataAccess.FinalizarSeries(  cheque, cheque.getUsuario_impresion() );      // series
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( " ActualizarSerie " + e.getMessage() );
        }
        return sql;
    }
    
    /**
     * Getter for property facturas.
     * @return Value of property facturas.
     */
    public java.util.Vector getFacturas() {
        return this.ChequeXFacturaDataAccess.getFacturas();
    }
    
    /**
     * Setter for property facturas.
     * @param facturas New value of property facturas.
     */
    public void setFacturas(java.util.Vector facturas) {
        this.ChequeXFacturaDataAccess.setFacturas(facturas);
    }
    
    /**
     * Actualiza la corrida y el cheque de la fra relacionada a un cheque
     * @autor Ing. Andr�s Maturana
     * @throws Exception
     * @version 1.0
     **/
    public void obtenerCxP_DocCheque(String dstrct, String banco, String sucursal, String cheque)throws Exception{
        this.ChequeXFacturaDataAccess.obtenerCxP_DocCheque(dstrct, banco, sucursal, cheque);
    }
    
    /**
     * Actualiza los saldos y abonos de la fra relacionada a un cheque
     * @autor Ing. Andr�s Maturana
     * @throws Exception
     * @version 1.0
     **/
    public String updateSaldosCxP_DocCheque(String dstrct, String proveedor, String tipo_documento, String documento, double vlr, double vlr_for)throws Exception{
        return this.ChequeXFacturaDataAccess.updateSaldosCxP_DocCheque(dstrct, proveedor, tipo_documento, documento, vlr, vlr_for);
    }
    
    public void removePrecheque( String id ){
        
        if( this.precheques != null && this.precheques.size() >0  ){            
            for( int i=0; i<this.precheques.size(); i++ ){
                Precheque pre = (Precheque) this.precheques.get(i);
                if( pre.getId().equals(id) ){
                    this.precheques.remove(i);
                    break;
                }
            }            
        }
        
    }
    
    /**
     * Inserta en anulacion egreso
     * @autor Ing. Juan M Escandon
     * @throws Exception
     * @version 1.0
     **/
    public String insertAnulacion_egreso(
    String dstrct,
    String banco,
    String sucursal,
    String cheque,
    String login,
    String causa,
    String observacion,
    String trecuperacion,
    String base, String clasificacion) throws Exception{
        
        return this.ChequeXFacturaDataAccess.insertAnulacion_egreso(dstrct, banco, sucursal, cheque, login, causa, observacion, trecuperacion, base, clasificacion );
    }
    
     /**
     * Inserta en anulacion egreso
     * @autor Ing. Andr�s Maturana
     * @throws Exception
     * @version 1.0
     **/
    public String insertAnulacion_egreso(
    String dstrct,
    String banco,
    String sucursal,
    String cheque,
    String login,
    String causa,
    String observacion,
    String trecuperacion,
    String base) throws Exception{
        
        return this.ChequeXFacturaDataAccess.insertAnulacion_egreso(dstrct, banco, sucursal, cheque, login, causa, observacion, trecuperacion, base);
    }

       
    /**
     * Establece si el cheque digitado pertenece a un pago a Fra
     * @autor Ing. Andr�s Maturana
     * @throws Exception
     * @version 1.0
     **/
    public boolean isChequeCXP(String dstrct, String banco, String sucursal, String cheque)throws Exception{
        return this.ChequeXFacturaDataAccess.isChequeCXP(dstrct, banco, sucursal, cheque);
    }
    
     public void formarChequesReimprimibles( String[] cheques, String banco, String sucursal )throws Exception{
        
        String moneda_local  = this.ChequeXFacturaDataAccess.getMonedaLocal( usuario.getDstrct() ) ;
        
        for( int i=0; i<cheques.length; i++ ){
            
            Cheque c = new Cheque();
            
            c.setNumero( cheques[i] );
            c = this.ChequeXFacturaDataAccess.obtenerFacturasCheque( c, moneda_local, banco, sucursal );
            
            // Esquema de Impresion:
            List  listaEsquema        = ImpSvc.getEsquemaCMS( c.getBanco() );
            List  listaEsquemaLaser   = ImpSvc.getEsquemaCMS( "LASER "+c.getBanco() );
            
            if( listaEsquema!=null && listaEsquema.size()>0 ){                
                c.setEsquema          ( listaEsquema );
                c.setEsquemaLaser     ( listaEsquemaLaser );
                this.listCheques.add( c );
            }else{                
                c.setComentario("No hay esquema de impresi�n para el banco "+c.getBanco());
            }
            
        }
        
    }
   
    
         /**
     * M�todos genera el cheque correspondiente al precheque dado
     * @autor.......Ing. Osvaldo P�rez Ferrer
     * @throws......Exception
     * @version.....1.0.
     **/
    public String prechequeToCheque( Precheque pre, int contSerie )throws Exception{
        
        String  msj      = "";
        //resetCheque();
        
        try{
            
            //ImpresionChequeService ImpSvc         = new ImpresionChequeService();
            //AMATURANA 20.04.2007
            //SeriesChequesDAO seriesChequeDAO = new SeriesChequesDAO();
            
            
            String distrito = pre.getDstrct();
            String banco    = pre.getBanco();
            String sucursal = pre.getSucursal();
            
            String monedaLocal  = this.ChequeXFacturaDataAccess.getMonedaLocal( distrito ) ;
            
            Hashtable  datoProveedor = this.ChequeXFacturaDataAccess.getBancoProveedor( distrito, this.proveedor );
            if(datoProveedor!=null){
                                                
                // Esquema de Impresion:
                List  listaEsquema        = ImpSvc.getEsquemaCMS( banco );
                List  listaEsquemaLaser   = ImpSvc.getEsquemaCMS( "LASER "+banco );
                
                if(listaEsquema!=null && listaEsquema.size()>0){
                    //logger.info("?usuario en sesi�n: " + this.usuario.getLogin());
                    //Series serie =  this.ChequeXFacturaDataAccess.getSeries( distrito, banco, sucursal );
                    //AMATURANA 20.04.2007
                    Series serie = SeriesChequesDataAccess.getSeries( 
                        distrito, 
                        banco, 
                        sucursal, 
                        "CXP",  
                        usuario.getId_agencia(), 
                        usuario.getLogin() );
                    if(serie!=null){
                        
                        int lastNumberSerie  = serie.getLast_number();
                        int numeroCheque     = lastNumberSerie  + contSerie;
                                                
                        String monedaBanco = pre.getMoneda();
                        
                        double netoCheque  = 0;
                        String swMsj       = "";                        
                        
                        //FACTURAS:
                        List listFacturas  =  getFacturas(banco, sucursal, monedaBanco);
                        for(int k=0; k<listFacturas.size(); k++){
                            FacturasCheques factura = (FacturasCheques)listFacturas.get(k);
                            
                            factura.setMonedaLocal(monedaLocal );
                            factura = calcularImpuestos   ( factura );
                            factura = calcularVlrFactura  ( factura );
                            factura = this.convertirMoneda( factura, monedaBanco, monedaLocal);
                            swMsj  += factura.getMsj();
                            
                            double  vlrNeto_me  =   factura.getVlrFactura() + factura.getVlrDescuentos() +  factura.getVlrRetencion();
                            
                            double abono = pre.getItemEspecifico( factura.getDocumento() ).getValor();
                            factura.setVlr_total_abonos_me( abono );
                            factura.setVlrNetoPago( vlrNeto_me );
                            
                            // Documentos relacionados:
                            List docRel =  this.ChequeXFacturaDataAccess.getFacturasRelacionadas( factura.getDstrct(), factura.getTipo_documento(), factura.getDocumento(), factura.getProveedor() );
                            for(int m=0;m<docRel.size();m++){
                                FacturasCheques factRel = (FacturasCheques)docRel.get(m);
                                
                                if( factRel.getTipoObjecto().equals( this.ChequeXFacturaDataAccess.TIPO_FACTURA  ) ){
                                    factRel = calcularImpuestos    ( factRel );
                                    factRel = calcularVlrFactura   ( factRel );
                                    factRel = convertirMoneda(factRel, monedaBanco, monedaLocal);
                                    swMsj  += factura.getMsj();
                                    double  vlrNeto_rel  =   factRel.getVlrFactura() + factRel.getVlrDescuentos() +  factRel.getVlrRetencion();
                                    factRel.setVlrNetoPago( vlrNeto_rel);
                                }
                                else{
                                    factRel = convertirMoneda(factRel, monedaBanco, monedaLocal);
                                    swMsj  += factura.getMsj();
                                    factRel.setVlrNetoPago( factRel.getVlrPagar() );
                                    factRel.setVlrFactura( factRel.getVlrPagar() );
                                }
                                
                            }
                            factura.setDocumnetosRelacionados( docRel );
                            
                            // Neto cheque:
                            netoCheque += factura.getVlrPagar();
                        }
                                                                                                
                        // CHEQUE :
                        
                        if ( Integer.parseInt(serie.getSerial_fished_no())  >= numeroCheque){
                            //TMolina Mod cambie int por double 09-Sept -2008
                            netoCheque  =  (  monedaBanco.equals("PES") ||  monedaBanco.equals("BOL")  )? (double)  Math.round(  netoCheque ) :  Util.roundByDecimal(netoCheque, 2);
                            
                            
                            Cheque cheque = new Cheque();
                            cheque.setDistrito         ( distrito );
                            cheque.setBanco            ( banco    );
                            cheque.setSucursal         ( sucursal );
                            cheque.setCuentaBanco      ( serie.getCuenta()             );
                            cheque.setAgencia          ( this.usuario.getId_agencia()  );
                            cheque.setMoneda           ( monedaBanco  );
                            cheque.setMonedaLocal      ( monedaLocal  );
                            cheque.setMonto            ( netoCheque  );
                            cheque.setVlrPagar         ( pre.getValor() );
                            cheque.setSerie            ( serie        );
                            cheque.setEsquema          ( listaEsquema );
                            cheque.setEsquemaLaser     ( listaEsquemaLaser );
                            
                            cheque.setNumero           ( serie.getPrefijo() +  numeroCheque    );
                            cheque.setLastNumber       ( numeroCheque                          );
                            cheque.setBeneficiario     ( (String)datoProveedor.get("nit")      );//Este es el proveedor
                            cheque.setNombre           ( (String)datoProveedor.get("nombre")   );
                            cheque.setNit_beneficiario( (String)datoProveedor.get("nit_beneficiario") );//Osvaldo
                            cheque.setNom_beneficiario( (String)datoProveedor.get("beneficiario")   );//Osvaldo
                            cheque.setNitProveedor     ( cheque.getBeneficiario());
                            cheque.setAno              ( Util.getFechaActual_String(1)         );
                            cheque.setMes              ( Util.getFechaActual_String(3)         );
                            cheque.setDia              ( Util.getFechaActual_String(5)         );
                            cheque.setId_precheque     ( pre.getId() );
                            
                            // Facturas del Cheque
                            cheque.setFacturas( listFacturas );
                            
                            if( !cheque.getNom_beneficiario().equals("") ){//Osvaldo
                                
                                if(netoCheque<=0  || !swMsj.equals("")  ){
                                    if( !swMsj.equals("") )  cheque.setComentario( swMsj );
                                    else                     cheque.setComentario( "No se puede imprimir el cheque, su valor no es positivo" );
                                    msj += cheque.getComentario();
                                }
                                else{                                                                       
                                    
                                    this.listCheques.add( cheque );
                                    
                                }
                                
                            } else{//Osvaldo
                                msj += "El Nit Beneficiario: "+cheque.getNit_beneficiario()+" no presenta nombre";
                            }
                            
                            
                        } else{
                            msj += " La serie para el banco " + banco +" "+ sucursal +" ha llegado a su tope maximo" ;                            
                        }                                                
                        
                    }else
                        msj += "No hay serie definida para el banco " + banco +" "+ sucursal + " para Pago a Proveedores o la serie no est�" +
                                " asignada al usuario en sesi�n " + usuario.getLogin() + ".";
                }
                else
                    msj += "No hay esquema de impresi�n para el banco "+ banco;
            }
            else
                msj += "El proveedor no presenta informaci�n";
            
            
            
            
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( " formarCheque " + e.getMessage() );
        }
        return msj;
    }
    
    //jdelarosa 19/04/2007
    /**
     * M�todos genera el cheque correspondiente al precheque dado
     * @autor.......Ing. Jose de la rosa
     * @throws......Exception
     * @version.....1.0.
     **/
    public String updateTransferenciaCheque(double vlrPagar)throws Exception{
        String sql = "";
        try{
            
            if(cheque!=null){
                // CHEQUE:
                sql += this.ChequeXFacturaDataAccess.insertEgreso    ( cheque,  vlrPagar,  this.usuario.getLogin(), "TR" );
                                
                // DETALLE:
                List listFacturas =  cheque.getFacturas();
                
                if(vlrPagar== cheque.getMonto()){   // CANCELACION:
                    for(int i=0;i<listFacturas.size();i++){
                        FacturasCheques    factura = (FacturasCheques) listFacturas.get(i);
                        factura.setTipo_pago("C");
                        double saldo  =    factura.getVlrPagar();
                        String item   =    String.valueOf( i+ 1 );
                        sql += this.ChequeXFacturaDataAccess.insertEgresoDet(cheque, factura, item ,  saldo, this.usuario.getLogin(), "TR" );
                        sql += this.ChequeXFacturaDataAccess.updateCXP_DOC   (cheque.getNumero(), factura, saldo,  this.usuario );
                        sql += this.ChequeXFacturaDataAccess.updateBancoCXP_DOC(cheque, factura, this.usuario.getLogin() );
                        
                    }
                }
                else{                               // ABONO:
                    for(int i=0;i<listFacturas.size();i++){
                        FacturasCheques    factura = (FacturasCheques) listFacturas.get(i);
                        double saldo  =    factura.getVlrPagar();
                        String item   =    String.valueOf( i+ 1 );
                        if(saldo>=vlrPagar){
                            factura.setTipo_pago("A");
                            sql += this.ChequeXFacturaDataAccess.insertEgresoDet(cheque, factura, item , vlrPagar,  this.usuario.getLogin(), "TR" );
                            sql += this.ChequeXFacturaDataAccess.updateCXP_DOC   (cheque.getNumero(), factura, vlrPagar, this.usuario );
                            sql += this.ChequeXFacturaDataAccess.updateBancoCXP_DOC(cheque, factura, this.usuario.getLogin() );
                            break;
                        }
                        else{
                            factura.setTipo_pago("C");
                            sql += this.ChequeXFacturaDataAccess.insertEgresoDet(cheque, factura, item , vlrPagar,  this.usuario.getLogin(), "TR" );
                            sql += this.ChequeXFacturaDataAccess.updateCXP_DOC   (cheque.getNumero(), factura, saldo, this.usuario );
                            sql += this.ChequeXFacturaDataAccess.updateBancoCXP_DOC(cheque, factura, this.usuario.getLogin() );
                            vlrPagar = vlrPagar - saldo;
                        }
                    }
                }
                
                // SERIE:
                
                Series  serie  = cheque.getSerie();
                cheque.setLastNumber( serie.getLast_number() );
                sql += this.SeriesChequesDataAccess.updateSerieSQL( String.valueOf( serie.getId() ), this.usuario.getLogin() );
                
            }
            else
                sql = "El cheque no existe, ya fu� impreso. Favor generar otro cheque.";
            
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
        return sql;
    }
    //jdelarosa 19/04/2007
    /**
     * M�todos genera el cheque correspondiente al precheque dado
     * @autor.......Ing. jose de la rosa
     * @throws......Exception
     * @version.....1.0.
     **/
    public String numeroCheque( Cheque cheque, String distrito, String banco, String sucursal)throws Exception{
        String  msj      = "";
        try{
            Series serie =  this.SeriesChequesDataAccess.getSeries( distrito, banco, sucursal, "CXP",  this.usuario.getId_agencia(), this.usuario.getLogin() );
            if(serie!=null){
                if ( Integer.parseInt(serie.getSerial_fished_no()) >= serie.getLast_number() ){
                    cheque.setSerie(serie);
                    cheque.setNumero( serie.getLast_number()+"" );
                }
                else
                    msj += " La serie para el banco " + banco +" "+ sucursal +" ha llegado a su tope maximo" ;
            }else
                msj += "No hay serie definida para el banco " + banco +" "+ sucursal;
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return msj;
    }
    
    
    public String obtenerNumeroChqComp( String distrito, String banco, String sucursal, String concepto,  String agencia, String usuario  ) throws Exception {
        String numChk = "";
        try{
            //System.out.println(distrito+"B"+banco+"S"+sucursal+"C"+concepto+"A"+agencia+"U"+usuario);
            Series serie =  this.SeriesChequesDataAccess.getSeries(distrito, banco, sucursal, concepto, agencia, usuario );
            if( serie != null ){
                // Buscamos en memoria la serie de ese banco, si ha sido procesada
                int lastNumberSerie  = serie.getLast_number();
                int serieProcesada   = getSerieProcesada(   banco, sucursal );
                if( serieProcesada > 0 )
                    lastNumberSerie = serieProcesada;
                
                int contSerie        = 0;
                int numeroCheque     = lastNumberSerie  + contSerie;
                
                numChk               = serie.getPrefijo() +  numeroCheque;
            }
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( " ObtenerNumeroChq " + e.getMessage() );
        }
        return numChk;
    }
    
    public Series obtenerSerieConcepto( String distrito, String banco, String sucursal, String concepto,  String agencia, String usuario  ) throws Exception {
        Series serie = null;
        try{
            serie =  this.SeriesChequesDataAccess.getSeries(distrito, banco, sucursal, concepto, agencia, usuario );
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( " obtenerSerieConcepto " + e.getMessage() );
        }
        return serie;
    }
    
    public String ActualizarSerieConcepto( String id , String usuario  ) throws Exception {
        String sql = "";
        try{
            sql =  this.SeriesChequesDataAccess.updateSerieSQL( id, usuario );
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( " ActualizarSerieConcepto " + e.getMessage() );
        }
        return sql;
    }
    
    /**
     * Retorna el sql de anulacion del egreso
     * @autor Ing. Juan M.Escandon
     * @throws Exception
     * @version 1.0
     **/
    public String anularEgreso(String dstrct, String banco, String sucursal, String cheque, String usuario)throws Exception{
        return this.ChequeXFacturaDataAccess.anularEgreso(dstrct, banco, sucursal, cheque, usuario);
    }
    
    
    public ArrayList<String> updateChequePreCheque(double vlrPagar, double intereses)throws Exception{
        ArrayList<String> sql = new ArrayList<String>();
        try{
            
            if(cheque!=null){
                Precheque upd = new Precheque();
                // CHEQUE:
                sql.add(this.ChequeXFacturaDataAccess.insertEgreso    ( cheque,  vlrPagar,  this.usuario.getLogin() ));
                
                upd.setTipo_documento("010");
                upd.setCheque(cheque.getNumero());
                upd.setUsuario_impresion( this.usuario.getLogin() );
                upd.setId( cheque.getId_precheque() );
                upd.setProveedor( cheque.getBeneficiario() );
                upd.setDstrct( cheque.getDistrito() );
                sql.add(this.PrechequeDataAccess.updatePrecheque( upd ));
                
                // DETALLE:
                List listFacturas =  cheque.getFacturas();
                
                if(vlrPagar== cheque.getMonto()){   // CANCELACION:
                    for(int i=0;i<listFacturas.size();i++){
                        FacturasCheques    factura = (FacturasCheques) listFacturas.get(i);
                        factura.setTipo_pago("C");
                        double saldo  =    factura.getVlrPagar();
                        String item   =    String.valueOf( i+ 1 );
                        sql.add(this.ChequeXFacturaDataAccess.insertEgresoDet(cheque, factura, item ,  saldo, this.usuario.getLogin() ));
                        sql.add(this.ChequeXFacturaDataAccess.updateCXP_DOC   (cheque.getNumero(), factura, saldo, this.usuario ));
                        
                        
                        upd.setDocumento( factura.getDocumento() );
                        sql.add(this.PrechequeDataAccess.updatePrechequeDetalle( upd ));
                        
                        
                    }
                }
                else{                               // ABONO:
                    for(int i=0;i<listFacturas.size();i++){
                        FacturasCheques    factura = (FacturasCheques) listFacturas.get(i);
                        double saldo  =    factura.getVlrPagar();
                        String item   =    String.valueOf( i+ 1 );
                        if(saldo>=vlrPagar){
                            factura.setTipo_pago("A");
                            sql.add(this.ChequeXFacturaDataAccess.insertEgresoDet(cheque, factura, item , vlrPagar,  this.usuario.getLogin() ));
                            if(intereses>0){
                                vlrPagar=vlrPagar-intereses;
                                sql.add(this.ChequeXFacturaDataAccess.updateCXP_DOC   (cheque.getNumero(), factura, vlrPagar,  this.usuario ));
                            }else{
                                sql.add(this.ChequeXFacturaDataAccess.updateCXP_DOC   (cheque.getNumero(), factura, vlrPagar,  this.usuario ));
                            }
                            
                            upd.setDocumento( factura.getDocumento() );
                            sql.add(this.PrechequeDataAccess.updatePrechequeDetalle( upd ));
                            
                            break;
                        }
                        else{
                            factura.setTipo_pago("C");
                            sql.add(this.ChequeXFacturaDataAccess.insertEgresoDet(cheque, factura, item , vlrPagar,  this.usuario.getLogin() ));
                            sql.add(this.ChequeXFacturaDataAccess.updateCXP_DOC   (cheque.getNumero(), factura, saldo,  this.usuario ));
                            vlrPagar = vlrPagar - saldo;
                            
                            upd.setDocumento( factura.getDocumento() );
                            sql.add(this.PrechequeDataAccess.updatePrechequeDetalle( upd ));
                            
                        }
                    }
                }
                
                // SERIE:
                //AMATURANA 21.04.2007
                SeriesChequesDAO seriesChequeDAO = new SeriesChequesDAO(this.ChequeXFacturaDataAccess.getDatabaseName());
                Series  serie  = cheque.getSerie();
                int     tope   = Integer.parseInt( serie.getSerial_fished_no() );
                int     ultimo = cheque.getLastNumber();
                
                String[] listSql = seriesChequeDAO.updateSerieSQL(String.valueOf(serie.getId()), usuario.getLogin()).split(";");//AMATURANA 21.04.2007
                for(int i=0; i <listSql.length; i++){
                    sql.add(listSql[i]);
                }
                /*if(tope > ultimo)
                    sql += this.ChequeFacturasCorridaDataAccess.ActualizarSeries(cheque, this.usuario.getLogin() );    // series                    
                if(tope <= ultimo )
                    sql += this.ChequeFacturasCorridaDataAccess.FinalizarSeries(cheque,  this.usuario.getLogin() );      // series
                */
                
                
            }
            //else
            //    sql = "El cheque no existe, ya fu� impreso. Favor generar otro cheque.";
            
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return sql;
    }
       
}
