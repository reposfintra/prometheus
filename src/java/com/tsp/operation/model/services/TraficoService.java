/*
 * TraficoService.java
 *
 * Created on 5 de septiembre de 2005, 03:51 PM
 */

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
/**
 *
 * @author  Administrador
 */
public class TraficoService {
    
    private TraficoDAO traficoDao;
    private FiltroUsuariosDAO filtros;
    private ConfiguracionUsuarioDAO configuracion;
    private ConfTraficoUDAO vista;
    private List    campos;
    private List    campos1;
    private List    lista;
    private List    lista1;
    private String  varCamposJS;
    private String  varSeparadorJS = "~";
    private Vector  camposTrafico;
    private Vector  colTrafico;
    private String conf="";
    private String var3="";
    private String var1="";
    private String linea="";
    
    private Vector configActual;
    private boolean clasifCAtual_Asc = true;
    
    /** Creates a new instance of TraficoService */
    public TraficoService () {
        traficoDao = new TraficoDAO ();
        filtros = new FiltroUsuariosDAO ();
        configuracion = new ConfiguracionUsuarioDAO ();
        vista = new ConfTraficoUDAO ();
    }
    
    
    public void  listarTrafico () throws SQLException {
        try {
            traficoDao.listarTrafico ();
        }
        catch (SQLException e) {
            throw new SQLException (e.getMessage ());
        }
    }
    
    public Vector obtVecTrafico ()throws SQLException {
        return traficoDao.obtVecTrafico ();
    }
    
    //Metodo que retorna el Vector con los Campos de Trafico
    //Para el Cabesote de la tabla
    public Vector obtCamposTrafico ()throws SQLException {
        return this.camposTrafico;
    }
    
    public String obtConf () {
        //conf="select oid,planilla,placa,nomcond,nomorigen,nomdestino,fecha_ult_reporte,fecha_prox_reporte,nompto_control_proxreporte,caravana,nomzona,ult_observacion";
        Vector v = this.vAtributos ();
        conf="select oid";
        for(int i=0;i< v.size ();i++) {
            Vector fila =(Vector)v.elementAt (i);
            conf= conf+","+fila.elementAt (1);
        }
        ////System.out.println("Configuracion  ="+conf);
        return conf+",cedcon";
    }
    
    public String obtVar1 () {
        // var1="planilla-placa-nomcond-nomorigen-nomdestino-fecha_despacho-fecha_ult_reporte-fecha_prox_reporte-nompto_control_proxreporte-caravana-ult_observacion-nomzona-user_update";
        Vector v = this.vAtributos ();
        var1="";
        for(int i=0;i< v.size ();i++) {
            Vector fila =(Vector)v.elementAt (i);
            var1= var1+"-"+fila.elementAt (1);
        }
        return var1;
    }
    
    public String obtVar3 () {
        //  var3="-PLANILLA-10-PLACA-12-CONDUCTOR-30-ORIGEN-30-DESTINO-30-FECHA DESPACHO-30-FECHA ULT REPORTE-30-FECHA PROX REPORTE-30-PROXIMO REPORTE-10-CARAVANA-10-OBSERVACION-10-USUARIO-10-ZONA-10";
        Vector v = this.vAtributos ();
        var3="";
        for(int i=0;i< v.size ();i++) {
            Vector fila =(Vector)v.elementAt (i);
            var3= var3+"-"+fila.elementAt (0)+"-"+fila.elementAt (2);
        }
        return var3;
    }
    
    public String obtlinea () {
        // linea="-planilla-placa-nomcond-nomorigen-nomdestino-fecha_ult_reporte-fecha_prox_reporte-nompto_control_proxreporte-caravana-nomzona-ult_observacion";
        linea="";
        Vector v = this.vAtributos ();
        for(int i=0;i< v.size ();i++) {
            Vector fila =(Vector)v.elementAt (i);
            linea= linea+"-"+fila.elementAt (1);
        }
        return linea+"-";
    }
    
    public Vector obtColTrafico ()throws SQLException {
        return this.colTrafico;
    }
    public void listarCamposTrafico (String linea)throws SQLException {
        ////System.out.println ("entra en listarCampos Trafico");
        camposTrafico = new Vector ();
        camposTrafico = traficoDao.obtenerCampos (linea);
        ////System.out.println ("sale en listarCampos Trafico");
        // return camposTrafico;
    }
    
    public void listarColTrafico (String linea)throws SQLException {
        ////System.out.println ("entra en listarCampos Trafico");
        colTrafico = new Vector ();
        colTrafico = traficoDao.obtenerCampos (linea);
        ////System.out.println ("sale en listarCampos Trafico");
        // return camposTrafico;
    }
    
    public void listarColtrafico2 (Vector v) {
        colTrafico =null;
        colTrafico = new Vector ();
        for( int i=0;i <v.size ();i++) {
            if((i %2) == 0) {
                String nombre =""+v.elementAt (i);
                nombre = nomAtributo2 (nombre);
                String alineacion=nomAtributo3 (""+v.elementAt (i));
                Vector fila = new  Vector ();
                fila.add (""+v.elementAt (i));
                fila.add (""+v.elementAt (i+1));
                fila.add (nombre);
                fila.add (alineacion);
                colTrafico.add (fila);
            }
            
        }
    }
    
    public Vector generarColumnas (Vector v1,Vector v2) {
        Vector v3= new Vector ();
        
        for(int x=0;x<v1.size ();x++) {
            String nombre =""+ v1.elementAt (x);
            for (int i=0;i<v2.size ();i++) {
                Vector fila = (Vector)v2.elementAt (i);
                if(nombre.equals (""+fila.elementAt (2))) {
                    Vector fila1= new Vector ();
                    fila1.add (""+fila.elementAt (0));
                    fila1.add (""+fila.elementAt (2));
                    fila1.add (""+fila.elementAt (1));
                    fila1.add (""+fila.elementAt (3));
                    v3.add (fila1);
                }
            }
        }
        return v3;
    }
    
    public List searchCampos () throws SQLException {
        // try{
        //create();
        Vector v1= new Vector ();
        // Vector fila = new Vector();
        v1= this.vAtributos ();
        lista = new LinkedList ();
        lista1 = new LinkedList ();
        for (int i=0;i<v1.size ();i++) {
            Vector fila = (Vector)v1.elementAt (i);
            lista.add (""+fila.elementAt (0));
            lista1.add (""+fila.elementAt (1));
        }
        
        this.campos  = this.lista;
        
        this.campos1  = this.lista1;
        
        GenerarJSCampos ();
        /* }catch(SQLException e){ throw new SQLException(e.getMessage()); }*/
        return campos;
    }
    
    public Vector obtNombresCampos (Vector v) {
        Vector v1=new Vector ();
        String nomCampo="";
        Vector atributos = this.vAtributos ();
        for (int i=0;i<v.size ();i++) {
            nomCampo=""+v.elementAt (i);
            for(int x=0;x <atributos.size ();x++) {
                Vector fila = (Vector)atributos.elementAt (x);
                if (nomCampo.equals (""+fila.elementAt (1)))
                    v1.add (""+fila.elementAt (0));
            }
        }
        return v1;
    }
    
    public void actualizarCol (String nombre,String valor) {
        Vector v = vAtributos ();
        for (int i=0;i<v.size ();i++) {
            Vector fila = (Vector)v.elementAt (i);
            // if (nombre.equals(""+fila.elementAt(1)))
            //  filaeleme
        }
    }
    
    public Vector vAtributos () {
        Vector atributos= new Vector ();
        Vector valores;
        valores =new Vector ();
        valores.add ("T D");
        valores.add ("tipo_despacho");
        valores.add ("5");
        valores.add ("center");
        atributos.add (valores);
        
        valores=null;
        valores =new Vector ();
        valores.add ("Planilla");
        valores.add ("planilla");
        valores.add ("10");
        valores.add ("center");
        atributos.add (valores);
        
        valores=null;
        valores=new Vector ();
        valores.add ("Placa");
        valores.add ("placa");
        valores.add ("12");
        valores.add ("center");
        atributos.add (valores);
        
        valores=null;
        valores=new Vector ();
        valores.add ("Conductor");
        valores.add ("nomcond");
        valores.add ("30");
        valores.add ("left");
        atributos.add (valores);
        
        valores=null;
        valores=new Vector ();
        valores.add ("Celular");
        valores.add ("cel_cond");
        valores.add ("30");
        valores.add ("left");
        atributos.add (valores);
        
        valores=null;
        valores=new Vector ();
        valores.add ("Origen");
        valores.add ("nomorigen");
        valores.add ("30");
        valores.add ("left");
        atributos.add (valores);
        
        valores=null;
        valores=new Vector ();
        valores.add ("Destino");
        valores.add ("nomdestino");
        valores.add ("30");
        valores.add ("left");
        atributos.add (valores);
        
        valores=null;
        valores=new Vector ();
        valores.add ("Cliente");
        valores.add ("cliente");
        valores.add ("50");
        valores.add ("left");
        atributos.add (valores);
        
        valores=null;
        valores=new Vector ();
        valores.add ("Despacho");
        valores.add ("fecha_despacho");
        valores.add ("30");
        valores.add ("center");
        atributos.add (valores);
        
        valores=null;
        valores=new Vector ();
        valores.add ("Salida");
        valores.add ("fecha_salida");
        valores.add ("30");
        valores.add ("center");
        atributos.add (valores);
        
        valores=null;
        valores=new Vector ();
        valores.add ("Puesto");
        valores.add ("nompto_control_ultreporte");
        valores.add ("25");
        valores.add ("center");
        atributos.add (valores);
        
        valores=null;
        valores=new Vector ();
        valores.add ("HorFec.");
        valores.add ("fecha_ult_reporte");
        valores.add ("30");
        valores.add ("center");
        atributos.add (valores);
        
        valores=null;
        valores=new Vector ();
        valores.add ("S.Puesto");
        valores.add ("nompto_control_proxreporte");
        valores.add ("30");
        valores.add ("left");
        atributos.add (valores);
        
        valores=null;
        valores=new Vector ();
        valores.add ("S.HorFec.");
        valores.add ("fecha_prox_reporte");
        valores.add ("30");
        valores.add ("center");
        atributos.add (valores);
        
        valores=null;
        valores=new Vector ();
        valores.add ("Tiempo");
        valores.add ("demora");
        valores.add ("5");
        valores.add ("center");
        atributos.add (valores);
        
        valores=null;
        valores=new Vector ();
        valores.add ("C");
        valores.add ("caravana");
        valores.add ("0");
        valores.add ("center");
        atributos.add (valores);
        
        valores=null;
        valores=new Vector ();
        valores.add ("E");
        valores.add ("escolta");
        valores.add ("0");
        valores.add ("center");
        atributos.add (valores);
        
        valores=null;
        valores=new Vector ();
        valores.add ("Observacion");
        valores.add ("ult_observacion");
        valores.add ("10");
        valores.add ("left");
        atributos.add (valores);
        
        valores=null;
        valores=new Vector ();
        valores.add ("Zona");
        valores.add ("nomzona");
        valores.add ("0");
        valores.add ("left");
        atributos.add (valores);
        
        valores=null;
        valores=new Vector ();
        valores.add ("Modificado por");
        valores.add ("user_update");
        valores.add ("10");
        valores.add ("left");
        atributos.add (valores);
        
        
        valores=null;
        valores=new Vector ();
        valores.add ("EIntermedias");
        valores.add ("neintermedias");
        valores.add ("30");
        valores.add ("left");
        atributos.add (valores);
        
        
        valores=null;
        valores=new Vector ();
        return atributos;
    }
    
    public String nomAtributo (String atributo) {
        Vector v = new Vector ();
        v= this.vAtributos ();
        String nombre="";
        for(int i=0;i<v.size ();i++) {
            Vector fila =(Vector)v.elementAt (i);
            if (atributo.equals (""+fila.elementAt (1)))
                nombre=""+fila.elementAt (0);
        }
        return nombre;
    }
    
    public String nomAtributo2 (String atributo) {
        Vector v = new Vector ();
        v= this.vAtributos ();
        String nombre="";
        for(int i=0;i<v.size ();i++) {
            Vector fila =(Vector)v.elementAt (i);
            if (atributo.equals (""+fila.elementAt (0)))
                nombre=""+fila.elementAt (1);
        }
        return nombre;
    }
    
    public String nomAtributo3 (String atributo) {
        Vector v = new Vector ();
        v= this.vAtributos ();
        String nombre="";
        for(int i=0;i<v.size ();i++) {
            Vector fila =(Vector)v.elementAt (i);
            if (atributo.equals (""+fila.elementAt (0)))
                nombre=""+fila.elementAt (3);
        }
        return nombre;
    }
    
    public String generarVariable1 (String []campos) {
        String var ="\n var var1= [";
        String nombre="";
        for (int i =0;i<campos.length;i++) {
            nombre=nomAtributo (campos[i]);
            var +="\n '"+ campos[i]+ varSeparadorJS+nombre+"',";
        }
        var = var.substring (0,var.length ()-1) + "];";
        return var;
    }
    
    public String generarVariable2 (String []campos) {
        String var ="\n var var2= [";
        String nombre="";
        for (int i =0;i<campos.length;i++) {
            nombre=nomAtributo (campos[i]);
            var +="\n '"+ campos[i]+ varSeparadorJS+nombre+"',";
        }
        var = var.substring (0,var.length ()-1) + "];";
        return var;
    }
    
    
    public void GenerarJSCampos () {
        String var = "\n var CamposJS = [ ";
        if (campos!=null)
            for (int i=0;i<campos.size ();i++) {
                //   Usuario usu = (Usuario)  campos.get(i);
                var += "\n '"+ campos1.get (i) + varSeparadorJS + campos.get (i) +"',";
            }
        var = var.substring (0,var.length ()-1) + "];";
        varCamposJS = var;
    }
    
    public List getCampos ()
    { return this.campos;       }
    public String getVarJSCampo ()
    { return this.varCamposJS;  }
    public String getVarJSSeparador ()
    { return "\n var SeparadorJS = '" + this.varSeparadorJS + "';";  }
    
    
    
    
    /**
    * Este m�todo retorna  un Vector con los campos dada un String con el nombre de los campos
    * separados por -
    * @param String linea
    */
    public  Vector obtenerCampos (String linea) {
        this.camposTrafico= traficoDao.obtenerCampos (linea);
        return camposTrafico;
    }
    
    /**
    * Este m�todo retorna el select de la consulta de ingreso_trafico dado un Vector de String de campos 
    * que van a ser seleccionados
    * @see TraficoDAO
    * @param String String []campo
    * @throws SQLException si un error de acceso a la base de datos ocurre.
    */
    public String getConfiguracion (String []campo) {
        String configuracion= traficoDao.getConfiguracion (campo);
        return configuracion;
    }
    
    /**
    * Este m�todo retorna el oreder by de la consulta de ingreso_trafico dado un Vector de String de campos y el orden
    * que van a ser seleccionados
    * @param String []campo,String orden
    * @see TraficoDAO
    * @throws SQLException si un error de acceso a la base de datos ocurre.
    */
    public String getClasificacion (String []campo,String orden) {
        String clasificacion = traficoDao.getClasificacion (campo,orden);
        return clasificacion;
    }
    
    public String getConsulta (String configuracion,String filtro,String clasificacion) {
        String consulta = traficoDao.getConsulta (configuracion,filtro,clasificacion);
        return consulta;
    }
    
    /**
    * Este m�todo que genera el Vector de ingreso trafico dado la consulta y el Vector de campos que van a ser mostrados
    *
    * @param String consulta,Vector campos
    * @see TraficoDAO
    * @throws SQLException si un error de acceso a la base de datos ocurre.
    */
    public void  generarTabla (String consulta,Vector campos) throws SQLException {
        try {
            traficoDao.generarTabla (consulta,campos);
        }
        catch (SQLException e) {
            throw new SQLException (e.getMessage ());
        }
    }
    
    public Vector obdtVectorCampo () {
        return traficoDao.obtVectorCampo ();
    }
    
    /**
    * Este m�todo que retorna un Vector con la informacion de ingreso trafico dadas las zonas
    * que tiene asignadas un usuario
    * @param String campo,String validacion
    * @throws SQLException si un error de acceso a la base de datos ocurre.
    * @see TraficoDAO
    */
    public Vector generarCampos (String campo,String validacion)throws SQLException {
        Vector v=new Vector ();
        try {
            v=traficoDao.generarCampos (campo,validacion);
        }
        catch (SQLException e) {
            throw new SQLException (e.getMessage ());
        }
        return v;
    }
    
        
    public  Vector obtenerVectorFiltros (String []linea) {
        Vector campos = new Vector ();
        for(int i=0;i<linea.length;i++) {
            StringTokenizer separados = new StringTokenizer (linea[i], "|");
            
            // Agrego cada uno de los campos al vector y luego lo retorno
            Vector fila  = new Vector ();
            while (separados.hasMoreTokens ()) {
                fila.addElement (separados.nextToken ());
            }
            if( fila.size()==1){//Added by Andr�s Maturana
                fila.addElement ("");
            }
            campos.add (fila);
        }
        return campos;
    }
    
    
    public Vector ordenarVectorFiltro (Vector filtro) {
        Vector v= new Vector ();
        for (int i=0;i<filtro.size ();i++) {
            Vector fila =(Vector)filtro.elementAt (i);
            String f =""+fila.elementAt (0);
            ////System.out.println (" f:"+f);
            for(int x=0;x<filtro.size ();x++) {
                Vector fila1 =(Vector)filtro.elementAt (x);
                String f2 =""+fila1.elementAt (1);
                String f1 =""+fila1.elementAt (0);
                ////System.out.println (" f1:"+f1);
                if(f.equals (f1)) {
                    if (existeCampo ( v,f1,f2)==false) {
                        v.add (fila1);
                    }
                }
            }
        }
        return v;
    }
    
    public boolean existeCampo (Vector v,String f1,String f2) {
        boolean n=false;
        for (int i=0;i<v.size ();i++) {
            Vector fila=(Vector)v.elementAt (i);
            String c1=""+fila.elementAt (0);
            String c2=""+fila.elementAt (1);
            if (f1.equals (c1) && f2.equals (c2)) {
                n=true;
            }
        }
        return n;
    }
    
    public String filtro (Vector v) {
        String filtro="";
        String temp="0";
        for(int i=0;i<v.size ();i++) {
            Vector fila1 = (Vector)v.elementAt (i);
            String f1 = ""+fila1.elementAt (0);
            String f11= ""+fila1.elementAt (1);
            if (i< (v.size ()-1)) {
                Vector fila2 = (Vector)v.elementAt (i+1);
                String f2 =""+ fila2.elementAt (0);
                if(f1.equals (""+f2)) {
                    if(temp.equals ("0")) {
                        filtro=filtro+" ("+ f1 +" = '"+f11+"' or";
                        temp="1";
                    }
                    else
                        filtro=filtro+" "+ f1 +" = '"+f11+"' or";
                }
                else {
                    if(temp.equals ("1")) {
                        filtro=filtro+" "+ f1 +" = '"+f11+"') and";
                        temp="0";
                    }
                    else {
                        filtro=filtro+" "+ f1 +" = '"+f11+"' and";
                        temp="0";
                        
                    }
                    
                }
            }
            else {
                if(temp.equals ("1"))
                    filtro=filtro+" "+ f1 +" = '"+f11+"') ";
                else
                    filtro=filtro+" "+ f1 +" = '"+f11+"' ";
            }
        }
        return filtro;
    }
    
    /**
    * Este m�todo retorna  un long con la difirencia entre dos fechas en formato SQL
    * @see TraficoDAO
    * @param java.sql.Timestamp fecha1(fecha inicial), java.sql.Timestamp fecha2(fechaFinal)
    */
    public long getDiferencia (java.sql.Timestamp fecha1, java.sql.Timestamp fecha2) {
        //  java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp.parse(;
        
        Map resultadoMap = new HashMap ();
        
        //los milisegundos
        long diferenciaMils = fecha1.getTime () - fecha2.getTime ();
        
        //obtenemos los segundos
        long segundos = diferenciaMils / 1000;
        
        //obtenemos las horas
        long horas = segundos / 3600;
        
        //restamos las horas para continuar con minutos
        //segundos -= horas*3600;
        
        //igual que el paso anterior
        long minutos = segundos /60;
        segundos -= minutos*60;
        
        //ponemos los resultados en un mapa :-)
        resultadoMap.put ("horas",Long.toString (horas));
        resultadoMap.put ("minutos",Long.toString (minutos));
        resultadoMap.put ("segundos",Long.toString (segundos));
        return minutos;
        
    }
    
    public String generarAlarma (long minutos,int i) {
        String alarma="";
        //////System.out.println ("minutos de atrazo"+minutos);
        if (minutos<=0) {
            alarma="";
            if((i%2)!=0)
                alarma="#F7F5F4";
            else
                alarma="#EEEEF2";
        }
        else if (minutos <=30 && minutos > 0)
            alarma="#FFFFBB";//amarillo
        else if (minutos >30 && minutos <= 3000)
            alarma="#FFBF80";//naranja
        else
            alarma="#FF9F9F";
        return alarma;
    }
    
    /**
    * metodo que retorna un Vector con las zonas asignadas a un usuario en la tabla zonausuarios
    * @param String idUsuario
    * @ see TraficoDAO
    * @throws SQLException si un error de acceso a la base de datos ocurre.
    */
    public void  generarVZonas (String idUsuario) throws SQLException {
        traficoDao.gennerarVZonas (idUsuario);
    }
    
    /**
     * metodo que retorna un Vector con la descripcion de la zonas y el codigo de zonas de un usuario de trafico en una hora determinada
     * @param String user
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void gennerarVZonasTurno (String user) throws SQLException {
        this.traficoDao.gennerarVZonasTurno (user);
    }
    
    /**
    * Este m�todo que actualiza los registros de ingreso_trafico cambiando a zona de seguridad
    * aquellos que esten retrasados mas de una hora
    * @param String filtro,java.sql.Timestamp fechaActual
    * @throws SQLException si un error de acceso a la base de datos ocurre.
    * @see TraficoDAO
    */
    public void actualizarZonas (String filtro,java.sql.Timestamp fechaActual) throws SQLException {
        try {
            traficoDao.actualizarZonas ( filtro,fechaActual);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    /**
    * Este m�todo que retorna  el nombre de la zona de un registro en ingrso trafico dado la planilla
    * que tiene asignadas un usuario
    * @param String campo(Planilla),Connection con
    * @throws SQLException si un error de acceso a la base de datos ocurre.
    * @see TraficoDAO
    */
    public String  encontrarZona (String campo,Connection con) throws SQLException {
        return traficoDao.encontrarZona (campo,con);
    }
    
    /**
    * Este m�todo que retorna  el numero de la planilla dado el oid del registro
    * @see TraficoDAO
    * @param String id
    * @throws SQLException si un error de acceso a la base de datos ocurre.
    */
    public String  getNumeroPlanilla (String id) throws SQLException {
            return traficoDao.getNumeroPlanilla (id);
    }
    
    public Vector obdtVectorZonas () {
        return traficoDao.obtVectorZonas ();
    }
    
    public String validacion (Vector zonas) {
        String validacion="";
        for (int i=0;i<zonas.size ();i++) {
            if (i< zonas.size ()-1)
                validacion=validacion +" zona='"+zonas.elementAt (i)+"' or ";
            else
                validacion=validacion + "zona='"+zonas.elementAt (i)+"' ";
        }
        validacion= "("+validacion+")";
        return validacion;
    }
    
    public Vector obtVecFiltrosU () {
        return filtros.obtVecFiltrosU ();
    }
    
    public String codificar (String cadena) {
        String cad="";
        String temp="";
        for (int i=0 ;i<cadena.length ();i++) {
            temp=cadena.charAt (i)+"";
            if(temp.equals ("'"))
                cad = cad+ "!";
            else
                cad=cad+cadena.charAt (i);
        }
        return cad;
    }
    
    public String decodificar (String cadena) {
        String cad="";
        String temp="";
        for (int i=0 ;i<cadena.length ();i++) {
            temp=cadena.charAt (i)+"";
            if(temp.equals ("!"))
                cad = cad+ "'";
            else
                cad=cad+cadena.charAt (i);
        }
        return cad;
    }
    
    /**
     * Este m�todo se encarga de listar los filtros creados por el usuario en el programa de control trafico
     * @author  dlamadrid
     * @version 1.0
     * @param ConfTraficoU c
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void  generarFiltrosU (String campo) throws SQLException {
        try {
            ////System.out.println ("Login de usuario antes de generar filtros "+campo);
            filtros.generarFiltrosU (campo);
        }
        catch (SQLException e) {
            throw new SQLException (e.getMessage ());
        }
    }
    
    /**
     * Este m�todo se encarga de encontrar un filtros creados dado el codigo del filtro
     * @author  ing David lamadrid
     * @version 1.0
     * @param String id
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void  generarFiltrosC (String id) throws SQLException {
        try {
            ////System.out.println ("oid antes de generar filtros "+id);
            filtros.generarFiltrosC (id);
        }
        catch (SQLException e) {
            throw new SQLException (e.getMessage ());
        }
    }
    
    /**
     * Este m�todo se encarga de elminar un filtro dado el codigo del mismo
     * @author  ing David lamadrid
     * @version 1.0
     * @param String id
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void eliminararFiltro (String campo) throws SQLException {
            filtros.eliminarFiltro (campo);
    }
    
    /**
     * Este m�todo se encarga de insertar un filtro con objeto tipo FiltrosUsuarios
     * @author  ing David lamadrid
     * @version 1.0
     * @param String id
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void  insertarFiltro (FiltrosUsuarios u) throws SQLException {
            filtros.insertarFiltro (u);
    }
    
   /**
     * Obtiene el un registro del archivo ingreso trafico
     * @autor Andr�s Maturana De La Cruz
     * @param numpla Numero de la planilla
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @version 1.0
     */
    public void obtenerIngreso_Trafico (String numpla) throws SQLException {
        this.traficoDao.obtenerIngreso_Trafico (numpla);
    }
    
    public String obtenerNumeroCaravana (String numpla) throws SQLException {
        return this.traficoDao.obtenerNumeroCaravana (numpla);
    }
    
    public void obtenerZonasPorUsuario (String id) throws SQLException {
        this.traficoDao.obtenerZonasPorUsuario (id);
    }
    
    
    /**
     * Este m�todo retorna un String con los codigos de zonas a los que puede acceder un usuario en una fecha y hora determindas del registro de turnos
     * @param String user
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public String zonasDeTurnos (String user) throws SQLException {
        return  this.traficoDao.zonasDeTurnos (user);
    }
    
    
    /**
     * metodo que retorna un Vector con la descripcion de la zonas y el codigo de zonas de un usuario de trafico en una hora determinada
     * @param String user
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public Vector vDeTurnos (String user) throws SQLException {
        return this.traficoDao.vDeTurnos (user);
    }
    
    /**
     * Getter for property zonas.
     * @return Value of property zonas.
     */
    public java.util.TreeMap getZonas () {
        return this.traficoDao.getZonas ();
    }
    
    /**
     * Setter for property zonas.
     * @param zonas New value of property zonas.
     */
    public void setZonas (java.util.TreeMap zonas) {
        this.traficoDao.setZonas (zonas);
    }
    
    /**
     * Este m�todo se encarga de insertar una configuracion hecha por el usuario en el programa de control trafico
     * @author  ing David lamadrid
     * @version 1.0
     * @param String id
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void insertarConfiguracion (ConfiguracionUsuario c) throws SQLException {
        this.configuracion.insertarConfiguracion (c);
    }
    
    /**
     * Este m�todo se encarga de eliminar  una configuracion dado el codigo de esta
     * @author  ing David lamadrid
     * @version 1.0
     * @param String id
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void eliminarConfiguracion (String id) throws SQLException {
        this.configuracion.eliminarConfiguracion (id);
    }
    
    /**
     * Este m�todo se encarga de listar la configuracion hecha por el usuario previamente al uso del programa de control trafico
     * @author  ing David lamadrid
     * @version 1.0
     * @param String usuario
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void listarConfiguracion (String usuario) throws SQLException {
        this.configuracion.listarConfiguracion (usuario);
    }
    
    /**
     * Este m�todo se encarga de cargar un TreeMap con las configuraciones realizadas por el usuario en el programa de control trafico
     * @author  ing David lamadrid
     * @version 1.0
     * @param String usuario
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void tConfiguracionesPorUsuario (String usuario) throws SQLException {
        this.configuracion.tConfiguracionesPorUsuario (usuario);
    }
    
    public java.util.TreeMap getTConf () {
        return this.configuracion.getTConf ();
    }
    
    /**
     * Este m�todo se encarga de listar la configuracion hecha por el usuario dado el codigo de la configuracion
     * @author  ing David lamadrid
     * @version 1.0
     * @param String id
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public ConfiguracionUsuario listarConfiguracionPorId (String id) throws SQLException {
        return this.configuracion.listarConfiguracionPorId (id);
    }
    /**
     * Setter for property tConf.
     * @param tConf New value of property tConf.
     */
    public void setTConf (java.util.TreeMap tConf) {
        this.configuracion.setTConf (tConf);
    }
    
    /**
     * Este m�todo se encarga de registrar un regitro en la tabla conf_trafico_u
     *cargando un objeto tipo ConfTraficoU
     * @author  dlamadrid
     * @version 1.0
     * @param ConfTraficoU c
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void insertarConfiguracion (ConfTraficoU c) throws SQLException {
        this.vista.insertarConfiguracion (c);
    }
    
    /**
     * Este m�todo se encarga de retornar tru si existe un registro en la tabla conf_trafico_u dado el usuario y false si no existe
     * @author  dlamadrid
     * @version 1.0
     * @param String usuario
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public boolean existeConfiguracion (String usuario) throws SQLException {
        return     this.vista.existeConfiguracion (usuario);
    }
    
    /**
     * Este m�todo se encarga de actualizar un registro en la tabla conf_trafico_u seteado un objeto ConfTraficoU
     * @author  dlamadrid
     * @version 1.0
     * @param ConfTraficoU c
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void actualizarConfiguracion (ConfTraficoU c) throws SQLException {
        this.vista.actualizarConfiguracion (c);
    }
    
    /**
     * Este m�todo se encarga de listar los  registros en la tabla conf_trafico_u dado un usuario
     * @author  dlamadrid
     * @version 1.0
     * @param String usuario
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public ConfTraficoU listarConfiguracionPorUsuario (String usuario) throws SQLException {
        return  this.vista.listarConfiguracionPorUsuario (usuario);
    }
    
    
    public String  obtenerVia (String numpla) throws SQLException {
        return traficoDao.obtenerVia (numpla);
    }
    
    public String  obtenerPPC (String numpla) throws SQLException {
        return traficoDao.obtenerPPC (numpla);
    }
    
    public String  obtenerSecuencia (String via) throws SQLException {
        return traficoDao.obtenerSecuencia (via);
    }
    
    public boolean  validarVia (String ppc,String secuencia) throws SQLException {
        return traficoDao.validarVia (ppc, secuencia);
    }
    /***************************************************************************
     ***************************************************************************
     **                                                                       **
     **    Los siguientes metodos y/o atributos fueron agregados de la        **
     **    aplicaci�n SOT. Alejandro Payares, 13.12.05                        **
     **                                                                       **
     ***************************************************************************
     ***************************************************************************/
    
    /**
     * Este m�todo genera los registros que hacen parte de los movimientos de
     * trafico de una OC/Planilla asi como las observaciones
     * @param numpla Numero de planilla/OC a la que se quiere consultar trafico
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void traficoSearch (String numpla )throws SQLException {
        traficoDao.traficoSearch (numpla);
    }
    
     /**
     * Este m�todo genera los registros que hacen parte de los movimientos de
     * las observaciones de trafico de una OC/Planilla.
     * @param numpla Numero de planilla/OC a la que se quiere consultar trafico
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public List getTraficoList () {
        return traficoDao.getTraficoList ();
    }
    
    /**
     * Getter for getTraficoObSearch
     * @return 
     */
    public void traficoObSearch (String numpla )throws SQLException {
        traficoDao.traficoObSearch (numpla);
    }
    
    /**
     * Getter for getTraficoObList.
     * @return List
     */
    public List getTraficoObList () {
        return traficoDao.getTraficoObList ();
    }
    
    
    /*********************************************
     *  METODOS POR: ANDRES MATURANA DE LA CRUZ  *
     *********************************************/
    
    /**
     * Obtiene las configuraciones de pantalla de un usuario
     * @param usuario Login del usuario
     * @author  Ing. Andr�s Maturana De La Cruz
     * @version 1.0
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public void camposConfiguracionesUsuario (String usuario) throws SQLException {
        this.configuracion.camposConfiguracionesUsuario(usuario);
    }
    
    /**
     * Genera una variable en Java Script de los campos de cada una de las
     * configuraciones de pantalla de un usuario.
     * @param usuario Login del usuario
     * @author  Ing. Andr�s Maturana De La Cruz
     * @version 1.0
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public String jsCamposConfig(String usuario) throws SQLException {
        this.camposConfiguracionesUsuario(usuario);
        Vector flds = this.configuracion.getVConf();
        String arrays = "";
        for( int i=0; i<flds.size(); i++){
            Vector ln = (Vector) flds.elementAt(i);
            String id = (String) ln.elementAt(0);
            String linea = (String) ln.elementAt(1);
            linea=linea.replaceAll("-tipo_despacho","");
            linea=linea.replaceAll("-planilla","");
            linea=linea.replaceAll("-placa","");
            String[] campos = linea.split("-");
            String c = "";
            for( int j=1; j<campos.length; j++){
                c += "\n'" + campos[j] + "~" + this.nomAtributo(campos[j]) + "',";
            }
            if( c.length()>0 )
                c = c.substring(0, c.length() - 1);
            String var = "\n var var" + id + " = [" + c + "];";
            arrays += var;
        }
        
        //////System.out.println("................. VBLES: " + arrays);
        
        return arrays;
    }
    
    /**
     * Genera una variable en Java Script de los campos determinados
     * @param var Campos de la vista
     * @author  Ing. Andr�s Maturana De La Cruz
     * @version 1.0
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public String jsCampos(String var) throws SQLException {
        String arrays = "\n var jsCampos = ";
        
        String[] campos = var.split("-");
        String c = "";
        for( int j=1; j<campos.length; j++){
            c += "\n'" + campos[j] + "~" + this.nomAtributo(campos[j]) + "',";
        }
        
        if( c.length()>0 )
            c = c.substring(0, c.length() - 1);
        arrays += "[" + c + "];";
        
        //////System.out.println("................. VBLE: " + arrays);
        
        return arrays;
    }
    
    /**
     * Genera una variable en Java Script de los campos que pertenecen 
     * a la configuraci�n actual de pantalla.
     * @author  Ing. Andr�s Maturana De La Cruz
     * @param var Variable que contiene la configuraci�n que se est� aplicando
     * @version 1.0
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public String jsConfigActual(String var){
        String arrays = "\n var jsConfig = ";
        
        String[] campos = var.split("-");
        String c = "";
        for( int j=1; j<campos.length; j++){
            c += "\n'" + campos[j] + "~" + this.nomAtributo(campos[j]) + "',";
        }
        
        if( c.length()>0 )
            c = c.substring(0, c.length() - 1);
        arrays += "[" + c + "];";
        
        //////System.out.println("................. VBLE: " + arrays);
        
        return arrays;
    }
    
    /**
     * Genera una variable en Java Script de los campos que pertenecen
     * a la clasificaci�n aplicada a la vista de control de trafico por un usuario.
     * @param order Variable que contiene la clasificaci�n que se est� aplicando
     * @author  Ing. Andr�s Maturana De La Cruz
     * @version 1.0
     * @ Modificado por: Karen Reales.
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public String jsClasificPrevia(String order){
        String conf = "\n jsClasif = [";
        String orden2 = order;
        order = order.replaceAll("ASC", "");
        order = order.replaceAll("DESC", "");
        if( order!=null && order.length()!=0 ){
            order = order.trim().substring(9);
            String campos = order;
            /*int spc = campos.indexOf(" ");
            campos = campos.substring(0, spc);*/
            StringTokenizer token =  new StringTokenizer(campos, ",");
            String c = "";
            if( token.countTokens()!=0 ){
                while( token.hasMoreTokens()){
                    String aux = token.nextToken();
                    aux = aux.replaceAll(" ","");
                    ////System.out.println("Encontrado_"+aux+"_");
                    
                    c += "\n'" + aux + "~" + this.nomAtributo(aux) + "',";
                }
                if( c.length()>0 )
                    c = c.substring(0, c.length() - 1);
            } else {
                c += "\n'" + campos + "~" + this.nomAtributo(campos) + "',";
            }
            
            conf += c + "];";
            //////System.out.println("....................... CLASIF PREVIA: " + conf);
            if( orden2.indexOf("DESC")!=-1 ){
                this.setClasifCAtual_Asc(false);
            } else {
                this.setClasifCAtual_Asc(true);
            }
            //////System.out.println("....................... ORDEN DE LA CLASIF: " + this.isClasifCAtual_Asc());
        } else {
            conf = "\n jsClasif = []; "; 
        }
        
        return conf;
    }
    
    /**
     * Getter for property configActual.
     * @return Value of property configActual.
     */
    public java.util.Vector getConfigActual() {
        return configActual;
    }
    
    /**
     * Setter for property configActual.
     * @param configActual New value of property configActual.
     */
    public void setConfigActual(java.util.Vector configActual) {
        this.configActual = configActual;
    }
    
    /**
     * Getter for property clasifCAtual_Asc.
     * @return Value of property clasifCAtual_Asc.
     */
    public boolean isClasifCAtual_Asc() {
        return clasifCAtual_Asc;
    }
    
    /**
     * Setter for property clasifCAtual_Asc.
     * @param clasifCAtual_Asc New value of property clasifCAtual_Asc.
     */
    public void setClasifCAtual_Asc(boolean clasifCAtual_Asc) {
        this.clasifCAtual_Asc = clasifCAtual_Asc;
    }
    
    /**
     * Getter for property ingreso.
     * @return Value of property ingreso.
     */
    public com.tsp.operation.model.beans.Ingreso_Trafico getIngreso() {
        return this.traficoDao.getIngreso();
    }
    
    /**
     * Setter for property ingreso.
     * @param ingreso New value of property ingreso.
     */
    public void setIngreso(com.tsp.operation.model.beans.Ingreso_Trafico ingreso) {
        this.traficoDao.setIngreso(ingreso);
    }
    
    /**
     * Obtiene el un registro del archivo ingreso trafico
     * @autor Andr�s Maturana De La Cruz
     * @param numpla Numero de la planilla
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @version 1.0
     */
    public void obtenerTrafico (String numpla) throws SQLException {
        this.traficoDao.obtenerTrafico (numpla);
    }
    
    /**
     * Obtiene los escoltas asociados a una planilla
     * @autor Andr�s Maturana De La Cruz
     * @param numpla Numero de la planilla
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @version 1.0
     */
    public String obtenerEscoltasVehiculo(String numpla) throws SQLException {
        return this.traficoDao.obtenerEscoltasVehiculo(numpla);
    }
    
    /**
     * Obtiene los escoltas asociados a una planilla
     * @autor Andr�s Maturana De La Cruz
     * @param numpla Numero de la planilla
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @version 1.0
     */
    public String obtenerEscoltasCaravana(String numpla) throws SQLException {
        return this.traficoDao.obtenerEscoltasCaravana(numpla);
    }
    
    /**
     * Obtiene las caravanas asociadas a una planilla
     * @autor Andr�s Maturana De La Cruz
     * @param numpla Numero de la planilla
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @version 1.0
     */
    public String obtenerCaravanas(String numpla) throws SQLException {
        return this.traficoDao.obtenerCaravanas(numpla);
    }
    
  
      /******************************************************************************/
    
     /**
     * Genera una variable de los campos que pertenecen a los filtros actuales.
     * @author  Ing. Andr�s Maturana De La Cruz
     * @param var Variable que contiene los filtros que se est�n aplicando
     * @version 1.0
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public String filtroActual(String var) throws Exception{
        
        String salida = "";
        try{
        if( var!=null && var.length()!=0){
            String[] campos = var.split("SEPARADOR");
            String campo2 = "";
            if( campos.length != 0){
                for( int j=0; j<campos.length; j++){
                    campo2 = campos[j].trim().replaceAll(" = ", "|").replaceAll("\\(","").replaceAll("\\)","");//.replaceAll(" ","");
                    if( !campo2.startsWith("zona")) salida = salida + "<option value='" + campo2 + "' SELECTED>" + campo2 + "</option>";
                }
            } else {
                campo2 = var.trim().replaceAll(" = ", "|").replaceAll("\\(","").replaceAll("\\)","");
                if( !campo2.startsWith("zona"))salida = salida + "<option value='" + campo2 + "' SELECTED>" + campo2 + "</option>";
            }
        }

        }catch(Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage()); 
        } 
        return salida;
    }
    
    /**
     * Genera un una variable en Java Script por cada una de los filtros almecenado s por el usuario.
     * @author  Ing. Andr�s Maturana De La Cruz
     * @version 1.0
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     */
    public String jsFiltros(){
        String js = "";
        String js2 = "";
        Vector fil = obtVecFiltrosU();
        if( fil!=null ){
            for(int i=0; i<fil.size(); i++){
                Vector vec = (Vector) fil.elementAt(i);
                String nomfil = (String) vec.elementAt(0);
                String campos = (String) vec.elementAt(5);
                
                /* Variable JScript con los filtros */
                String var = "\n var" + nomfil + " = [";
                String var2 = "\n str" + nomfil + " = \" ";
                StringTokenizer token =  new StringTokenizer(campos, ",");
                String c = "";
                String c2 = "";
                while( token.hasMoreTokens()){
                    String aux = token.nextToken().replaceAll("=","|");
                    c += "\n'" + aux + "~" +aux + "',";
                    c2 += "<option value='" + aux + "' SELECTED>" + aux + "</option>";
                }
                
                if( c.length()>0 )
                    c = c.substring(0, c.length() - 1);
                
                var += c + "];";
                var2 += c2 + "\";";
                js += var;
                js += var2;
            }
        }
        
        return js;
    }
    
    /**
     * Elimina todas las configuraciones de un determinado usuario
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param login Login del usuario
     * @throws SQLException
     * @version 1.0
     */
    public void eliminarConfiguraciones(String login) throws SQLException {
        this.vista.eliminarConfiguraciones(login);
    }
    /////////////////////////// AMATURANA 11.08.2006
    
    /**
     * Getter for property registros.
     * @return Value of property registros.
     */
    public java.util.Vector getRegistros() {
        return this.traficoDao.getRegistros();
    }
    
    /**
     * Setter for property registros.
     * @param registros New value of property registros.
     */
    public void setRegistros(java.util.Vector registros) {
        this.traficoDao.setRegistros(registros);
    }
    
    /**
     * Obtiene el un registro del archivo ingreso trafico pertenecientes a una placa.
     * @autor Andr�s Maturana De La Cruz
     * @param numpla Numero de la planilla
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @version 1.0
     */
    public void obtenerIngreso_TraficoPlaca(String placa) throws SQLException {
        this.traficoDao.obtenerIngreso_TraficoPlaca(placa);
    }
       
    /**
     * Obtiene el un registro del archivo ingreso trafico pertenecientes a un despacho manual.
     * @autor Andr�s Maturana De La Cruz
     * @param numpla Numero de la planilla
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @version 1.0
     */
    public void reporteDespachoManual(String dstrct )throws SQLException {
        this.traficoDao.reporteDespachoManual(dstrct);
    }
    
    
    
       /**
   *Metodo para obtener reporte detallado de trafico
   *@param planillas numeros de planillas separadas por comas.
   */
  public void getReporteDetalladoTraf(String planillas) throws SQLException{
      traficoDao.getReporteDetalladoTraf( planillas );
  }
  
  /**
   * Getter for property traficoDetallado.
   * @return Value of property traficoDetallado.
   */
  public java.util.List getTraficoDetallado() {
      return traficoDao.getTraficoDetallado();
  }
  
  /**
   * Setter for property traficoDetallado.
   * @param traficoDetallado New value of property traficoDetallado.
   */
  public void setTraficoDetallado(java.util.List traficoDetallado) {
      traficoDao.setTraficoDetallado( traficoDetallado );
  }
   /**
     * Actualiza ingreso trafico y cambia el color de la fila.
     * @autor Karen Reales
     * @param Numpla, Color de la letra
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @version 1.0
     */
    public void cambiarColor(String numpla, String color )throws SQLException {
        traficoDao.cambiarColor(numpla, color);
    }
    
    /**
     * Obtiene el un registro del archivo ingreso trafico pertenecientes a un despacho manual.
     * @autor Andr�s Maturana De La Cruz
     * @param numpla Numero de la planilla
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @version 1.0
     */
    public void reporteDespachoManual( String dstrct, String fechai, String fechaf  )throws SQLException {
        this.traficoDao.reporteDespachoManual(dstrct, fechai + " 00:00:00", fechaf + " 23:59:59");
    }
}
