 /**************************************************************************
 * Nombre clase: ReportePODService.java                               *
 * Autor: Ing. Ivan DArio Gomez Vanegas                                    *
 * Fecha: Created on 2 de septiembre de 2006, 10:05 AM                        *
 * Versi�n: Java 1.0                                                       *
 * Copyright: Fintravalores S.A. S.A.                                 *
 ***************************************************************************/




package com.tsp.operation.model.services;
import java.util.*;
import com.tsp.operation.model.DAOS.*;

public class ReportePODService {
     private ReportePODDAO dao;   
     private List reporte;
    
    /** Creates a new instance of ReportePODService */
    public ReportePODService() {
       dao = new ReportePODDAO();       
    }
    
     public void searchReporte(String fecini, String fecfin, String cliente) throws Exception {
        try{
            this.reporte = dao.searchReporte( fecini,  fecfin,  cliente); 
        }catch(Exception e){
            throw new Exception("Error en searchReporte ReportePODDAO]...\n"+e.getMessage());
        }
    }
    
     /**
      * Getter for property reporte.
      * @return Value of property reporte.
      */
     public java.util.List getReporte() {
         return reporte;
     }
     
     /**
      * Setter for property reporte.
      * @param reporte New value of property reporte.
      */
     public void setReporte(java.util.List reporte) {
         this.reporte = reporte;
     }
     
}
