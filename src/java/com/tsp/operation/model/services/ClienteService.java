/*
 * ClienteService.java
 *
 * Created on 13 de noviembre de 2004, 9:49
 */

package com.tsp.operation.model.services;

import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;
import com.tsp.operation.model.DAOS.*;
/**
*
 * @author  HENRY
 */
public class ClienteService {
    private TreeMap TlistaCMC = null;
    private List listaC;   
    private ClienteDAO clienteDao;
    private BeanGeneral BeanCliente;
    private Cliente clienteUnidad;
    private TreeMap treemap;
    private String cod;//TMaturana 19.01.05
    private String cedula = "";
    private Vector vectorsCargaSop;
    /** Creates a new instance of ClienteService */
    public ClienteService() {
        clienteDao = new ClienteDAO();
        this.BeanCliente = new BeanGeneral();
    }
    public ClienteService(String dataBaseName) {
        clienteDao = new ClienteDAO(dataBaseName);
        this.BeanCliente = new BeanGeneral();
    }
    
    
    /**
     * M�todo para agregar una demora al archivo de demoras. 
     * @autor Tito Andr�s Maturana
     * @param demora Instancia de la clase Demora.
     * @see com.tsp.operation.model.DAOS.DemorasDAO#agregarDemora(Demora)
     * @throws SQLException
     * @version 1.0
     */
    public void agregarCliente(BeanGeneral BeanCliente) throws java.sql.SQLException{
        this.clienteDao.agregarCliente(BeanCliente);        
    }
    
    
    public void agregarSoporteCliente(String llave,String codigo,String districto)throws SQLException{
        this.clienteDao.agregarSoporteCliente(llave,codigo,districto);        
    }
    
    
    public void searchCliente(String nomcod)throws SQLException{
        clienteDao.searchCliente(nomcod.toUpperCase());
    }
    public Cliente getCliente() throws SQLException{
        return clienteDao.getCliente();
    }
    public void setCliente(Cliente cli) {
        clienteDao.setCliente(cli);
    }
   
    
    public void updateCliente()throws SQLException{
        clienteDao.updateCliente();
    }
    
    public Vector getClientes() throws SQLException {
        return clienteDao.listarClientes();
    }
    
     public Vector getClientes2() throws SQLException {
        return clienteDao.listarClientes2();  
    }
    
    public java.util.Vector getClientesVec() {
        return clienteDao.getClientes();
    }
    
    /**
     * Metodo searchGeneralClientes, busca los clientes por nombre o codigo     
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @see : searchGeneralClientes  -  ClienteDAO
     * @param : String codigo o el nombre a buscar
     * @version : 1.0
     */
    public void searchGeneralClientes(String codnom)throws SQLException {      
        clienteDao.searchGeneralClientes(codnom);
    }    
     /**
     * Metodo existeCliente, busca un cliente por el codigo     
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : String codigo 
     * @see : existeCliente  -  ClienteDAO
     * @return : retorna true si el cliente existe en la Bd o false si no
     * @version : 1.0
     */
    public boolean existeCliente(String codcli)throws SQLException{   
        return clienteDao.existeCliente(codcli);
    }
    ///metodo nuevo by RARP
     public boolean existeNitCliente(String codcli,String nit)throws SQLException{   
        return clienteDao.existeNitCliente(codcli,nit);
    }
    ///End NM
    public String cargarSoporteCampoCliente (String [] soportes) throws Exception {  
        return clienteDao.cargarSoporteCampoCliente (soportes);
    }
    
    
     public boolean existeClienteIngresarEstadoLLeno(String codcli)throws SQLException{   
        return clienteDao.existeClienteIngresarEstadoVacio(codcli);
    }
      public boolean existeClienteIngresarEstadoVacio(String codcli)throws SQLException{   
        return clienteDao.existeClienteIngresarEstadoVacio(codcli);
    }
    //David 23.11.05
    /**
     * Instancia un objeto <code>Vector</code> con los clientes con un nombre dado
     * @autor Ing. David Lamadrid
     * @see com.tsp.operation.model.services#getClientes()
     * @return 
     * @version 1.0
     */
    public void vClientes(String nombre) throws SQLException{
           clienteDao.vClientes (nombre);
      }
    
     /**
     * Obtiene un objeto <code>TreeMap</code> con los nombres de locs clientes
     * y sus respectivos c�digos
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @see com.tsp.operation.model.services#getClientes()
     * @return <code>TreeMap</code> con los nombres de locs clientes
     * y sus respectivos c�digos
     * @version 1.0
     */
    public TreeMap listarClientes() throws SQLException{
        Vector vec = this.getClientes();
        TreeMap tr = new TreeMap();
        
        for( int i=0; i<vec.size(); i++){
            Cliente cli = (Cliente) vec.elementAt(i);
            tr.put(cli.getNomcli(), cli.getCodcli());        
        }
        
        return tr;
    }
    
    public TreeMap listarClientes2() throws SQLException{
        Vector vec = this.getClientes2();
        TreeMap tr = new TreeMap();
        
        for( int i=0; i<vec.size(); i++){
            Cliente cli = (Cliente) vec.elementAt(i);
            tr.put(cli.getNomcli(), cli.getCodcli());        
        }
        
        return tr;
    }
    
    
    ///
    public TreeMap listar(Usuario loggedUser) throws SQLException {
        return clienteDao.listar(loggedUser);
    }
    
    public TreeMap getCbxCliente(){
        return clienteDao.getCbxCliente();
    }
    
    public void clienteSearch() throws SQLException {
        clienteDao.clienteSearch();
    }
    
    public java.util.LinkedList getClientesSot() {
        return clienteDao.getClientesSot();
    }
    
    public String notasClienteSearch(String codCliente) throws SQLException {
        return clienteDao.notasClienteSearch(codCliente);
    }
    
    //TMaturana 19.01.06
    /**
     * Getter for property treemap.
     * @return Value of property treemap.
     */
    public java.util.TreeMap getTreemap() {
        return treemap;
    }
    
    /**
     * Setter for property treemap.
     * @param treemap New value of property treemap.
     */
    public void setTreemap(java.util.TreeMap treemap) {
        this.treemap = treemap;
    }
    
    /**
     * Setea la propiedad treemap de tipo <code>TreeMap</code> con los nombres de locs clientes
     * y sus respectivos c�digos
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @see com.tsp.operation.model.services#getClientes()
     * @return <code>TreeMap</code> con los nombres de locs clientes
     * y sus respectivos c�digos
     * @version 1.0
     */
    public void setTreeMapClientes() throws SQLException{
        this.treemap = this.listarClientes();
    }
    
    public void setTreeMapClientes2() throws SQLException{
        this.treemap = this.listarClientes2();
    }
      
    /**
    * Metodo datosCliente , Metodo que retorna el nombre y el codigo del cliente
    * @autor : Ing. jose de la rosa
    * @param : String codigo del cliente
    * @version : 1.0
    */    
    public void datosCliente(String codcli)throws SQLException{
        clienteDao.datosCliente (codcli);
    }
     public void consultarCliente(String nombre) throws SQLException {
        clienteDao.consultarCliente(nombre);
    }
      public void consultarClienteFactura(String nombre) throws SQLException {
        clienteDao.consultarClienteFactura(nombre);
    }
    public void consultarClienteFen(String nombre,String afil) throws SQLException {
           clienteDao.consultarClienteFen(nombre,afil); 
    }
      public void consultarClienteAll(String afil) throws SQLException {
           clienteDao.consultarClienteAll(afil); 
    }
      
       public void ClientesList(String codcli) throws SQLException {
           clienteDao.ClientesList(codcli); 
    }

 /**
     * Metodo searchAccount_Code_C, busca el codigo de cuenta contable de una planilla
     * @autor : Ing. Juan Manuel Escandon Perez
     * @see : searchAccount_Code_C  -  ClienteDAO
     * @param : String numpla
     * @version : 1.0
     */
    public void searchAccount_Code_C( String numpla )throws SQLException {      
        clienteDao.searchAccount_Code_C(numpla);
    } 
    
      //Ivan Gomez 27 julio 2006
     public void BuscarCliente(String codCli) throws SQLException {
       this.clienteUnidad = clienteDao.BuscarCliente(codCli);
    }
      /**
     * Getter for property clienteUnidad.
     * @return Value of property clienteUnidad.
     */
    public com.tsp.operation.model.beans.Cliente getClienteUnidad() {
        return clienteUnidad;
    }
    
    /**
     * Setter for property clienteUnidad.
     * @param clienteUnidad New value of property clienteUnidad.
     */
    public void setClienteUnidad(com.tsp.operation.model.beans.Cliente clienteUnidad) {
        this.clienteUnidad = clienteUnidad;
    }
     /**
     * Metodo searchNit, busca el nit del cliente de una planilla
     * @autor : Ing. Ivan DArio Gomez
     * @see : searchNit  -  ClienteDAO
     * @param : String numpla
     * @version : 1.0
     */
    public void searchNit( String numpla )throws SQLException {      
        clienteDao.searchNit(numpla);
    } 
     /**
    * Metodo datosCliente , Metodo que busca la informacion del cliente
    * @autor : Ing. Diogenes Bastidas
    * @param : String codigo del cliente
    * @version : 1.0
    */    
    public void informacionCliente(String codcli)throws SQLException{
        clienteDao.informacionCliente(codcli);
    }

     //FILYS FERNANDEZ 30 DE OCTUBRE 2006
     public void buscarClienteModificar(String codCli) throws Exception {
       clienteDao.buscarClienteModificar(codCli);
    }
     public void BusquedaCLiente(String codCli,String nit,String nombre,String agencia,String pais ) throws Exception {
       clienteDao.BusquedaCLiente(codCli,nit,nombre,agencia,pais );
    } 
    public java.util.Vector getVector() {
        return clienteDao.getVector();
    }
     public void modificarCliente (BeanGeneral BCliente) throws Exception{
        try{
             clienteDao.modificarCliente(BCliente);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    } 
     public void eliminarCliente (String  codCLi) throws Exception{
        try{
            clienteDao.eliminarCliente(codCLi);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }  
      public void eliminarClienteTemporalmente (String  codCLi) throws Exception{
        try{
            clienteDao.eliminarClienteTemporalmente(codCLi);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    public TreeMap listarCMC() throws SQLException{
        clienteDao.listarCMC();
        // mfontalvo 2006-01-16
        TlistaCMC = clienteDao.getCbxCMC();
        return TlistaCMC;
    }
     public TreeMap getCbxCMC(){
        return clienteDao.getCbxCMC();
    }
      public TreeMap listarBase() throws SQLException{
        clienteDao.listarBase();
        // mfontalvo 2006-01-16
        TlistaCMC = clienteDao.getCbxCMC();
        return TlistaCMC;
    }
      
     public TreeMap listarUca() throws SQLException{
        clienteDao.listarUca();
        // mfontalvo 2006-01-16
        TlistaCMC = clienteDao.getCbxCMC();
        return TlistaCMC;
    }  

/********************************************************
 Entregado a Fily 12 Febrero 2007
*********************************************************/

  public java.util.Vector getVectorCod() {
        return clienteDao.getVectorCod();
    }
    public void CodCliente()throws Exception{   
        this.setCod( clienteDao.CodCliente());
    }
    
    /*incremento de coidigo cliente */
   public void IncrementoDeCodigo(String codigo) throws Exception{
        try{
             clienteDao.IncrementoDeCodigo(codigo);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    } 
   
   /**
    * Getter for property cod.
    * @return Value of property cod.
    */
   public java.lang.String getCod() {
       return cod;
   }
   
   /**
    * Setter for property cod.
    * @param cod New value of property cod.
    */
   public void setCod(java.lang.String cod) {
       this.cod = cod;
   }
   
    public List listarAgecniaFacturacion() throws Exception {
        try{
           this.ReiniciarLista();
           this.listaC = clienteDao.listarAgecniaFacturacion();
       }
       catch(Exception e){
            throw new Exception("Error en ListarAgencias [CiudadService]...\n"+e.getMessage());
        }
        return this.listaC;
    }
     public void ReiniciarLista(){
        this.listaC = null;
    }
    public void  BusquedaCedCLiente(String agencia ) throws Exception {
        this.setCedula(clienteDao.BusquedaCLiente(agencia ));  
    }
    
    public void  BusquedaSoporteAD() throws Exception {
        clienteDao.BusquedaSoporteAD();  
    }
    
    public void  cargarSoporte (String [] soportes) throws Exception {
        clienteDao.cargarSoporte (soportes);  
    }
    
    /**
     * Getter for property cedula.
     * @return Value of property cedula.
     */
    public java.lang.String getCedula() {
        return cedula;
    }
    
    /**
     * Setter for property cedula.
     * @param cedula New value of property cedula.
     */
    public void setCedula(java.lang.String cedula) {
        this.cedula = cedula;
    }
    
    
    public void setVectorsSoporte(java.util.Vector soporte) {
        this.clienteDao.setVectorsCargaSop( soporte);
    }
     public java.util.Vector getVectorsSoporte() {
        return clienteDao.getVectorsSoporte();
    }
     
     //funcion que carga los soportes
     public java.util.Vector getVectorsCargaSop() {
        return clienteDao.getVectorsCargaSop();
    } 
    public void setVectorsCargaSop(java.util.Vector soporte) {
        this.clienteDao.setVectorsCargaSop( soporte);
    } 
    public java.lang.String getMensaje() {
        return clienteDao.getMensaje();
    }
    
    public String agregarClienteFen(BeanGeneral BeanCliente) throws java.sql.SQLException{
        return this.clienteDao.agregarClienteFen(BeanCliente);
    }
     
     public void UpdateClienteFen(BeanGeneral BeanCliente) throws java.sql.SQLException{
        this.clienteDao.UpdateClienteFen(BeanCliente);         
    }
    
     public void DelClienteFen(BeanGeneral BeanCliente) throws java.sql.SQLException{
        this.clienteDao.DelClienteFen(BeanCliente);          
    }
     
    public String Validar(String a,String b,String c)throws Exception
    {
        String res="";
        try{
            res=clienteDao.Validar(a,b,c); 
        }
        catch(Exception e){
            throw new Exception (e.getMessage ());
        }
        return res;
    }
    
     public String Validar_2(String a,String b)throws Exception
    {
        String res="";
        try{
            res=clienteDao.Validar_2(a,b);
        }
        catch(Exception e){
            throw new Exception (e.getMessage ());
        }
        return res;
    }
     
      public String Validar_3(String a,String b)throws Exception
    {
        String res="";
        try{
            res=clienteDao.Validar_3(a,b);
        }
        catch(Exception e){
            throw new Exception (e.getMessage ());
        }
        return res;
    }
    
     public void agregarNitFen(BeanGeneral BeanCliente) throws java.sql.SQLException{
        this.clienteDao.agregarNitFen(BeanCliente);        
    }
     
    public void relsave_2(BeanGeneral bg)throws Exception
    {
        try{ 
            clienteDao.UPrel(bg); 
         }
            catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }

    public void add_codeudor(Codeudor cd) throws Exception{
             clienteDao.add_codeudor(cd);
    }



    
     /**
     * Consulta si existe un cliente de acuerdo al nit
     * @param nit
     * @return true si existe o false si no
     * @throws Exception 
     */
    public boolean existeClienteXNit(String nit) throws Exception{
        return clienteDao.existeClienteXNit(nit);
    }
    
    public BeanGeneral buscarClienteXnit(String nit) throws java.sql.SQLException{
        return clienteDao.buscarClienteXnit(nit);          
       
    }
    
    public boolean  actualizarClienteXnit(String nit, String codigo, String name, String direccion, String telefono, String celular,String barrio,String codciu,String cooddpto, String email ,  String observaciones, Usuario user) throws java.sql.SQLException, Exception{
        return clienteDao.actualizarClienteXnit(nit, codigo, name, direccion, telefono, celular, barrio, codciu, cooddpto, email,observaciones, user);
       
    }

    public String buscarNegocios(String nit) {
     return clienteDao.bucarNegocios(nit);
    }
    
    
    public boolean actualizarSolicitud(String num_sol,String ext_email,String ext_corr,String nit) throws Exception {
     return clienteDao.actualizarSolicitud(num_sol,ext_email,ext_corr,nit);
    }
    
    public JsonObject buscarEmail(String email) {
     return clienteDao.bucarEmail(email);
    }


    
}
