/*
 * MigracionDiscrepanciaService.java
 *
 * Created on 14 de octubre de 2005, 10:39 AM
 */

package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  dbastidas
 */
public class MigracionDiscrepanciaService {
    private MigracionDiscrepanciaDAO migdisdao;
    /** Creates a new instance of MigracionDiscrepanciaService */
    public MigracionDiscrepanciaService() {
        migdisdao = new MigracionDiscrepanciaDAO();
    }
    
    public Vector MigrarDiscrepancia() throws SQLException {
        try{
            return migdisdao.MigrarDiscrepancia();
        }catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
    
    public void marcarMigracion(String fecha, String usuario) throws SQLException {
        try{
           migdisdao.marcarMigracion(fecha, usuario);
        }catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
}
