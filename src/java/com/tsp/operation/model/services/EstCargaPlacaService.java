/******************************************************************
* Nombre                        EstCargaPlacaService.java
* Descripci�n                   Clase Service para la tabla est_carga_placa
* Autor                         ricardo rosero
* Fecha                         13/01/2006
* Versi�n                       1.0
* Coyright                      Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;

/**
 *
 * @author  Ricardo Rosero
 */
public class EstCargaPlacaService {
    // Atributos
    private Usuario usuario;
    
    private EstCargaPlacaDAO dao;
        
    // Funciones
    
    /**
    * EstCargaPlaca_Service
    * @autor: ....... Ing. Ricardo Rosero
    * @version ...... 1.0
    */   
    public EstCargaPlacaService() {
        dao = new EstCargaPlacaDAO();
    }
    
    /**
    * getEstCargaPlaca
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @see    ....... getEstCargaPlaca EstCargaPlacaDAO
    * @version ...... 1.0
    */   
    public EstCargaPlaca getEstCargaPlaca()throws SQLException{
        return dao.getEstCargaPlaca();
    }
    
    /**
    * insertar
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @see    ....... insertar2 EstCargaPlacaDAO
    * @version ...... 1.0
    */   
    public void insertar() throws SQLException{
        try{
            dao.insertar2();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
            
        }
    }
    
    /**
    * eliminarEstCargaPlaca
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @see    ....... delete EstCargaPlacaDAO
    * @version ...... 1.0
    */   
    public void eliminarEstCargaPlaca()throws SQLException{
        try{
            dao.deleteTabla();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
    * setUser
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ Usuario usu
    * @version ...... 1.0
    */   
    public void setUser(Usuario usu){
        this.usuario = usu;
    }
    
    /**
    * getUser
    * @autor: ....... Ing. Ricardo Rosero
    * @version ...... 1.0
    */   
    public Usuario getUser(){
        return this.usuario;
    }
    
    /**
    * obtenerInforme
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @see    ....... obtenerInforme EstCargaPlacaDAO
    * @version ...... 1.0
    */   
    public Vector obtenerInforme() throws SQLException{
        return dao.obtenerInforme();
    }
    
    /**
    * setVector
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ Vector vec
    * @see    ....... setEcpVec EstCargaPlacaDAO
    * @version ...... 1.0
    */   
    public void setVector(Vector vec){
        dao.setEcpVec(vec);
    }
    
    /**
    * getVector
    * @autor: ....... Ing. Ricardo Rosero
    * @see    ....... getEcpVec EstCargaPlacaDAO
    * @version ...... 1.0
    */   
    public Vector getVector(){
        return dao.getEcpVec();
    }
}