/*
 * Nombre        PerfilService.java
 * Autor         Ing. Sandra M. Escalante G.
 * Fecha         10 de marzo de 2005, 03:48 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
/**
 *
 * @author  Administrador
 */
public class PerfilService {
    
    private PerfilDAO perfil;
    
    /** Creates a new instance of PerfilService */
    public PerfilService() {
        perfil = new PerfilDAO();
    }
    
    /**
     * Getter for property perfil.
     * @return Value of property perfil.
     */
    public Perfil getPerfil()throws SQLException{
        return perfil.getPerfil();
    }
    
    /**
     * Metodo <tt>agregarPerfil()</tt>, registra un nuevo perfil en el sistema
     * @autor : Ing. Sandra Escalante
     * @see setPerfil(Perfil p), agregarPerfil() 
     * @version : 1.0
     */
    public void agregarPerfil(Perfil p)throws SQLException{
        try{
            perfil.setPerfil(p);
            perfil.agregarPerfil();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo <tt>agregarPerfilOpcion</tt>, registra una nueva relacion perfil-opcion en el sistema
     * @autor : Ing. Sandra Escalante
     * @see agregarPerfilOpcion (String idperfil, int idopcion, String us)
     * @version : 1.0
     */
    public void agregarPerfilOpcion(String idperfil, int idopcion, String us) throws SQLException{
        try{
            perfil.agregarPerfilOpcion(idperfil, idopcion, us);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo <tt>anularPerfil()</tt>, anula un perfil de la tabla
     * @autor : Ing. Sandra Escalante
     * @see setPerfil (Perfil p), anularPerfil()
     * @version : 1.0
     */
    public void anularPerfil( Perfil p )throws SQLException{
        try{
            perfil.setPerfil(p);
            perfil.anularPerfil();
        }catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
     /**
     * Metodo <tt>anularPerfilopcion()</tt>, anula un registro perfil-opcion de la tabla
     * @autor : Ing. Sandra Escalante
     * @see anularPerfilOpcion( String uu, String idprf, int idop )
     * @version : 1.0
     */
    public void anularPerfilOpcion( String uu, String idprf, int idop )throws SQLException{
        try{
            perfil.anularPerfilOpcion(uu, idprf, idop);
        }catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo <tt>updatePerfil()</tt>, actualiza los datos de un perfil en la tabla
     * @autor : Ing. Sandra Escalante     
     * @see perfil.setPerfil(Perfil p), perfil.updatePerfil();
     * @version : 1.0
     */
    public void updatePerfil(Perfil p)throws SQLException{
        try{
            perfil.setPerfil(p);
            perfil.updatePerfil();
        }catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo <tt>obtenerPerfilxPropietario()</tt>, obtiene un perfil dado un codigo
     * @autor : Ing. Sandra Escalante
     * @see obtenerPerfilxPropietario( String idp )
     * @version : 1.0
     */
    public void obtenerPerfilxPropietario( String idp )throws SQLException{
        try{
            perfil.obtenerPerfilxPropietario(idp);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo <tt>buscarPerfilesxPropietarioenDetalle</tt>, obtiene los perfiles dado unos parametros de busqueda
     * @autor : Ing. Sandra Escalante
     * @see buscarPerfilesxPropietarioenDetalle(String idp, String nom)
     * @version : 1.0
     */
    public void buscarPerfilesxPropietarioenDetalle(String idp, String nom)throws SQLException{
        try{
            perfil.buscarPerfilesxPropietarioenDetalle(idp, nom);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo <tt>existePerfilxPropietario()</tt>, verifica la existencia de un perfil en la tabla
     * @autor : Ing. Sandra Escalante
     * @see existePerfilxPropietario( String idp )
     * @version : 1.0
     */
    public boolean existePerfilxPropietario( String idp ) throws SQLException {
        return perfil.existePerfilxPropietario(idp);
    }
    
    /**
     * Metodo <tt>existePerfilxPropietarioAnulado()</tt>, verifica la existencia de un perfil anulado en la tabla
     * @autor : Ing. Sandra Escalante
     * @see existePerfilxPropietarioAnulado( String idp )   
     * @version : 1.0
     */
    public boolean existePerfilxPropietarioAnulado( String idp ) throws SQLException {
        return perfil.existePerfilxPropietarioAnulado(idp);
    }
    
    /**
     * Metodo <tt>existePerfilOpcionAnulado()</tt>, verifica la existencia de la relacion
     *  perfil-opcion anulada en la tabla
     * @autor : Ing. Sandra Escalante
     * @see existePerfilOpcionAnulado( String idprf, int idop )
     * @version : 1.0
     */
    public boolean existePerfilOpcionAnulado( String idprf, int idop ) throws SQLException {
        return perfil.existePerfilOpcionAnulado(idprf, idop);
    }
    
    /**
     * Metodo <tt>estaPerfilopcion()</tt>, verifica la existencia de la relacion perfil-opcion en la tabla
     * @autor : Ing. Sandra Escalante
     * @see estaPerfilOpcion( String idprf, int idop )
     * @version : 1.0
     */
    public boolean estaPerfilOpcion( String idprf, int idop ) throws SQLException {
        return perfil.estaPerfilOpcion(idprf, idop);
    }
    
    /**
     * Metodo <tt>estaPOActivo</tt>, verifica si un registro perfil-opcion se encuentra activo (rec_status != A)
     * @autor : Ing. Sandra Escalante
     * @see estaPOActivo( String idprf, int idop)
     * @version : 1.0
     */
    public boolean estaPOActivo( String idprf, int idop) throws SQLException {
        return perfil.estaPOActivo(idprf, idop);
    }
    
    /**
     * Metodo <tt>activarPerfil</tt>, activa un perfil (rec-status='') que habia sido anulado anteriormente 
     * @autor : Ing. Sandra Escalante     
     * @see perfil.setPerfil(prf), perfil.activarPerfil()
     * @version : 1.0
     */
    public void activarPerfil( Perfil prf )throws SQLException{
        try{
            perfil.setPerfil(prf);
            perfil.activarPerfil();
        }catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo <tt>activarPerfilOpcion()</tt>, activa una relacion perfil-opcion (rec_status='') anulada con anterioridad
     * @autor : Ing. Sandra Escalante
     * @see activarPerfilOpcion(String uu, String idprf, int idop)
     * @version : 1.0
     */
    public void activarPerfilOpcion(String uu, String idprf, int idop)throws SQLException{
        try{
            perfil.activarPerfilOpcion(uu, idprf, idop);
        }catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Getter for property vperfiles.
     * @return Value of property vperfiles.
     */
    public Vector getVPerfil(){
        return perfil.getVPerfil();
    }
    
    /**
     * Metodo <tt>listarPerfilesxpropietario</tt>, obtiene los perfiles del sistema
     * @autor : Ing. Sandra Escalante
     * @see listarPerfilesxPropietario()
     * @version : 1.0
     */   
    public void listarPerfilesxPropietario()throws SQLException{
        try{
            perfil.listarPerfilesxPropietario();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo <tt>listarPOxPerfil</tt>, obtiene los regitros perfil-opcion anulados dado un perfil
     * @autor : Ing. Sandra Escalante
     * @see listarPOxPerfil( String prf )
     * @version : 1.0
     */
    public void listarPOxPerfil( String prf )throws SQLException{
        try{
            perfil.listarPOxPerfil(prf);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo <tt>listarPerfilOpcion</tt>, obtiene los regitros perfil-opcion dado un perfil
     * @autor : Ing. Sandra Escalante
     * @see listarPerfilOpcion( String prf )
     * @version : 1.0
     */
    public void listarPerfilOpcion( String prf )throws SQLException{
        try{
            perfil.listarPerfilOpcion(prf);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo <tt>listarPOxOpcion</tt>, obtiene los registros perfil-opcion donde la opcion sea la dada
     * @autor : Ing. Sandra Escalante
     * @see listarPOxOpcion( int opcion )
     * @version : 1.0
     */
    public void listarPOxOpcion( int opcion )throws SQLException{
        try{
            perfil.listarPOxOpcion(opcion);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Getter for property opsactxprf.
     * @return Value of property opsactxprf.
     */
    public java.util.Vector getOpsactxprf() {
        return perfil.getOpsactxprf();
    }
    
    /**
     * Metodo <tt>opcionesActivasxPerfil</tt>, obtiene una lista de las opciones activas
     *  de una perfil (rec_status='')
     * @autor : Ing. Sandra M Escalante G
     * @see opcionesActivasxPerfil(String prf) 
     * @version : 1.0
     */
    public void opcionesActivasxPerfil(String prf) throws SQLException {
        perfil.opcionesActivasxPerfil(prf);
    }
    /**
     * M�todo que obtiene los perfiles de un usuario dado
     * @autor David Pi�a Lopez
     * @throws SQLException
     * @version 1.0
     * @param idusuario El login del usuario
     */
    public void getPerfilesUsuario(String idusuario)throws SQLException{
        perfil.getPerfilesUsuario( idusuario );
    }
    public void getPerfilesUsuario(String idusuario, String distrito, String proyecto)throws SQLException{
        perfil.getPerfilesUsuario( idusuario, distrito, proyecto );
    }
    public void setVPerfil( Vector p )throws SQLException{
        perfil.setVPerfil( p );
    }
    public void getCompania(String usuario) throws SQLException {
        perfil.getCompania(usuario);
    }
}//end class