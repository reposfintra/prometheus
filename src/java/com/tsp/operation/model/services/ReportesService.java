/*
 * ReportesService.java
 *
 * Created on 31 de julio de 2005, 11:29
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.util.*;


/**
 *
 * @author  mario
 */
public class ReportesService {
    
    private ReportesDAO RptDataAccess;
    private List    Viajes;
    private String Ano;
    private String Mes;
    
    private TreeMap xlsViajes;
    
    /** Creates a new instance of ReportesService */
    public ReportesService() {
        RptDataAccess = new ReportesDAO();
    }
    
    public void Presupuestado_VS_Ejecutado(String Distrito, String Agencia, String Cliente, String Estandar, String Ano, String Mes) throws Exception{
        try{
            Viajes = RptDataAccess.loadViajesPt(Distrito, Agencia, Cliente, Estandar, Ano, Mes);
            Viajes = RptDataAccess.loadViajesEj(Distrito, Agencia, Cliente, Estandar, Ano, Mes, Viajes);
            this.Ano = Ano;
            this.Mes = Mes;
            GenerarCalculos();
        }catch (Exception ex){
            throw new Exception("Error en la rutina Presupuestado_VS_Ejecutado en [ReportesService] ....\n"+ ex.getMessage());
        }
        
    }
    
    
    public void GenerarCalculos(){
        if (Viajes!=null){
            for(int i = 0; i<Viajes.size() ; i++){
                ViajesAgencia ag = (ViajesAgencia) Viajes.get(i);
                for(int j = 0; j< ag.getListadoClientes().size() ; j++){
                    ViajesCliente cl = (ViajesCliente) ag.getListadoClientes().get(j);
                    for(int k = 0; k< cl.getListaStandar().size() ; k++){
                        ViajesStandar st = (ViajesStandar) cl.getListaStandar().get(k);
                        st.setAno(Ano);
                        st.setMes(Mes);
                        st.Calcular();
                    }
                    cl.setAno(Ano);
                    cl.setMes(Mes);
                    cl.Calcular();
                }
                ag.setAno(Ano);
                ag.setMes(Mes);
                ag.Calcular();
            }
        }
    }
    
    public void VentasPresupuestadas(String Ano, String Mes)throws Exception{
        try{
            xlsViajes = RptDataAccess.BuscarVentas(Ano, Mes);
        }catch (Exception ex ){
            throw new Exception("Error en la rutina VentasPresupuestadas en [ReportesService] ....\n"+ ex.getMessage());
        }
    }
    
    public void PresupuestoCostosOperativos(String Distrito, String Agencia, String Cliente, String Estandar, String Ano, String Mes) throws Exception{
        try{
            Viajes = RptDataAccess.loadCostosPt(Distrito, Agencia, Cliente, Estandar, Ano, Mes);
           // Viajes = RptDataAccess.loadViajesEj(Distrito, Agencia, Cliente, Estandar, Ano, Mes, Viajes);
            this.Ano = Ano;
            this.Mes = Mes;
            GenerarCalculos();
        }catch (Exception ex){
            throw new Exception("Error en la rutina PresupuestoCostosOperativos en [ReportesService] ....\n"+ ex.getMessage());
        }
        
    }
        
    
    public List getListado(){
        return Viajes;
    }
    public TreeMap getListadoXLS(){
        return xlsViajes;
    }    
    public String getAno(){
        return Ano;
    }
    public String getMes(){
        return Mes;
    }
    
    
    
}
