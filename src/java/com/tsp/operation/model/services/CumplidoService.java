/************************************************************************************
 * Nombre clase: CumplidoService.java                                               *
 * Descripci�n: Clase que maneja los servicios al model relacionados con cumplido.  *
 * Autor: Ing. Rodrigo Salazar                                                      *
 * Fecha: 20 de septiembre de 2005, 02:17 PM                                        *
 * Versi�n: Java 1.0                                                                *
 * Copyright: Fintravalores S.A. S.A.                                          *
 ***********************************************************************************/

package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  R.SALAZAR
 */
public class CumplidoService {
    
    CumplidoDAO cumpdao;
    // mfontalvo 2006-01-17
    List listaDocumentos = null;
    List lista = null;
    
    /** Creates a new instance of CumplidoService */
    public CumplidoService() {
        cumpdao = new CumplidoDAO();
    }
    
    /**
     * Metodo: Instancia de la funcion insertarCumplido del DAO
     * @autor : Ing. Rodrigo Salazar
     * @see InsertarCumplido - CumplidoDAO
     * @param : objeto Cumplido
     * @version : 1.0
     */
    public void InsertarCumplido(Cumplido var)throws SQLException{
        
        try{
            cumpdao.insertarCumplidos(var);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo: Instancia de la funcion listarCumplidos del DAO
     * @autor : Ing. Rodrigo Salazar
     * @see listarCumplidos - CumplidoDAO
     * @param :
     * @version : 1.0
     */
    public Vector listarCumplidos()throws SQLException{
        try{
            return cumpdao.listarCumplidos();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo: Instancia de la funcion listarCumplidos del DAO
     * @autor : Ing. Rodrigo Salazar
     * @see listarCumplidos - CumplidoDAO
     * @param : objeto Cumplido
     * @version : 1.0
     */
    public void listarCumplidos(Cumplido var)throws SQLException{
        try{
            cumpdao.insertarCumplidos(var);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo: Instancia de la funcion buscarCumplido del DAO
     * @autor : Ing. Rodrigo Salazar
     * @see buscarCumplido - CumplidoDAO
     * @param : tipo de documento, codigo del documento
     * @version : 1.0
     */
    public Cumplido buscarCumplido(String tipo, String cod)throws SQLException{
        Cumplido c = null;
        try{
            return cumpdao.buscarCumplido(tipo,cod);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    //jose 25/10/2005
    /**
     * Metodo: Instancia de la funcion listarCumplidosMigrados del DAO
     * @autor : Ing. Jose de la Rosa
     * @see listarCumplidosMigrados - CumplidoDAO
     * @param : fecha
     * @version : 1.0
     */
    public Vector listarCumplidosMigrados(String fecha)throws SQLException{
        try{
            return cumpdao.listarCumplidosMigrados(fecha);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo: Instancia de la funcion updateCumplido del DAO
     * @autor : Ing. Rodrigo Salazar
     * @see updateCumplido - CumplidoDAO
     * @param : fecha, usuario, tipo documento, codigo documento
     * @version : 1.0
     */
    public void updateCumplido(String fecha, String usuario, String tipo_doc, String cod_doc)throws SQLException{
        this.cumpdao.updateCumplido(fecha, usuario, tipo_doc, cod_doc);
    }
    
    /**
     * Metodo: Instancia de la funcion getCumplido del DAO
     * @autor : Ing. Rodrigo Salazar
     * @see getCumplido - CumplidoDAO
     * @param :
     * @version : 1.0
     */
    public Cumplido getCumplido( )throws SQLException{
        return cumpdao.getCumplido();
    }
    
    /**
     * Metodo: Instancia de la funcion setCumplido del DAO
     * @autor : Ing. Rodrigo Salazar
     * @see setCumplido - CumplidoDAO
     * @param :
     * @version : 1.0
     */
    public void setCumplido(Cumplido cum) {
        cumpdao.setCumplido(cum);
    }
    
    /**
     * Metodo: Instancia de la funcion estaCumplida del DAO
     * @autor : Ing. Karen Reales
     * @see estaCumplida - CumplidoDAO
     * @param : numero de la planilla
     * @version : 1.0
     */
    public boolean estaCumplida(String numpla)throws SQLException{
        return cumpdao.estaCumplida(numpla);
    }
    
    /**
     * Metodo: Instancia de la funcion insertarControl_proceso del DAO
     * @autor : Ing. Jose de la rosa
     * @see insertarControl_proceso - CumplidoDAO
     * @param : objeto Cumplido
     * @version : 1.0
     */
    
    
public String insertarControl_proceso(Cumplido var)throws SQLException{
        
        try{
            return cumpdao.insertarControl_proceso(var);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo: listarControlSoporte, lista los soportes de los clientes dada una remesa
     * @autor : Ing. Jose de La Rosa
     * @see listarControlSoporte - CumplidoDAO
     * @param : remesa
     * @version : 1.0
     */
    public void listarControlSoporte(String numrem)throws SQLException{
        try{
            cumpdao.listarControlSoporte(numrem);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }  
    
        /**
     * Metodo: setCumplidos, permite obtener un vector de registros de cumplido.
     * @autor : Ing. Jose de la rosa
     * @see setCumplidos - CumplidoDAO
     * @param : vector
     * @version : 1.0
     */
    public void setCumplidos (Vector cump) {
        cumpdao.setCumplidos (cump);
    }
    
    /**
     * Metodo: getCumplidos, permite retornar un vector de registros de cumplido.
     * @autor : Ing. Jose de la rosa
     * @see getCumplidos - CumplidoDAO
     * @param :
     * @version : 1.0
     */
    public Vector getCumplidos () {
        return cumpdao.getCumplidos ();
    }
    
     /**
     * Metodo, para busca el listado tipo de documentos soporte de una remesa
     * @autor mfontalvo
     * @fecha 2006-01-17
     * @see com.tsp.operation.model.DAOS.CumplidoDAO.ListadoDocumentosSoporteRemesa
     * @param remesa a buscar
     * @param estado de los tipo de documentos soportes
     * @param tipo de fechas (fisicas o logicas)
     * @version  1.0
     */
    
    public void listarSoportesRemesa(String remesa, String estado, String tipo)throws Exception{
        try{
            listaDocumentos = cumpdao.ListadoDocumentosSoporteRemesa(remesa, estado, tipo);
        }
        catch(Exception e){
            throw new Exception("Error en listarCumplidosMigrados [CumplidoService] ...\n" + e.getMessage());
        }
    }   
    
    /**
     * Metodo,para actulizar la fecha y agencia de envio de los soportes de 
     * una remesa
     * @autor mfontalvo
     * @fecha 2006-01-18
     * @see com.tsp.operation.model.DAOS.CumplidoDAO.ActualizarEnvio
     * @param Remesa, que se va a actualizar
     * @param Soporte, que se va a actualizar
     * @param editAGE, indica si se puede modificar o no la agencia
     * @param Agencia, nueva agencia de envio
     * @param editFEF, indica si se puede modificar o no la fecha de envio fisico
     * @param fechaEnvioFisico, nueva fecha de envio fisico 
     * @param editFRF, indica si se puede modificar o no la fecha de recibido fisico
     * @param fechaRecibidoFisico, nueva fecha de recibido fisico 
     * @param editFEL, indica si se puede modificar o no la fecha de envio logico
     * @param fechaEnvioLogico, nueva fecha de envio logico  
     * @param editFRL, indica si se puede modificar o no la fecha de recibido logico
     * @param fechaRecibidoLogico, nueva fecha de recibido logico
     * @param Usuario, que registra el envio
     * @version  1.0
     * @see cumpdao.ActualizarFechas.
     * @throws Exception.
     */
    
    public void ActualizarFechas (
                String Remesa, 
                String Soporte, 
                boolean editAGE, String Agencia,
                boolean editFEF, String fechaEnvioFisico, 
                boolean editFRF, String fechaRecibidoFisico, 
                boolean editFEL, String fechaEnvioLogico, 
                boolean editFRL, String fechaRecibidoLogico, 
                String Usuario)throws Exception{
        try{
            cumpdao.ActualizarFechas(
                Remesa, 
                Soporte, 
                editAGE, Agencia, 
                editFEF, fechaEnvioFisico, 
                editFRF, fechaRecibidoFisico, 
                editFEL, fechaEnvioLogico, 
                editFRL, fechaRecibidoLogico, 
                Usuario  
            );
        }
        catch(Exception e){
            throw new Exception("Error en ActualizarFechas [CumplidoService] ...\n" + e.getMessage());
        }
    }     
    
    /**
     * Obtiene el valor de la propiedad listaDocumentos.
     * @autor mfontalvo
     * @fecha 2006-01-17
     * @return Valor de la propiedad listaDocumentos.
     */
    public List getListaDocumentos() {
        return listaDocumentos;
    }
    /**
     * Metodo : Metodo que genera la lista con los datos para generar el reporte de Cumplido 
     * @autor : Ing. Juan Manuel Escandon Perez
     * @see ReporteCumplido - CumplidoDAO
     * @param : String fecha Inicial, String fecha Final, String agencia del cumplido
     * @version : 1.0
     */
    public List ReporteCumplido(String fechainicial, String fechaFinal, String agencia, String usuario)throws Exception{
        try{
            return cumpdao.ReporteCumplido(fechainicial, fechaFinal, agencia, usuario);
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    /**
     * Metodo: existeControlSoporte, valida si una remesa y un documento de soporte existen en la tabla
     * @autor : Ing. Jose de la rosa
     * @param : numero de la remesa y los documentos de soporte
     * @version : 1.0
     */
    public boolean existeControlSoporte(String distrito, String numrem, String soporte)throws SQLException{
        return cumpdao.existeControlSoporte(distrito, numrem, soporte);
    }  
    /**
     * Metodo: existeControlSoporte, valida si una remesa y un documento de soporte existen en la tabla
     * @autor : Ing. Juan Escandon
     * @param : numero de la remesa y los documentos de soporte
     * @version : 1.0
     */
    public void ReporteCumplidos()throws Exception{
        
        try{
            String fechainicial = cumpdao.FechaProceso("cumplido");
            
            
            lista = cumpdao.ListaCumplidos(fechainicial);
            for( int i = 0; i <= lista.size(); i++ ){
                Cumplido objeto = (Cumplido)lista.get(i);
                Cumplido aux    =  cumpdao.SearchPlarem(objeto.getCod_doc());
                if( aux.getEstado()!= "A" ){
                    objeto.setCod_doc(aux.getCod_doc());
                    objeto.setTipo_doc("002");
                    cumpdao.InsertCumplido(objeto);                    
                }
            }
            cumpdao.ActualizarFechaProceso("cumplido");
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    
    /**
     * Metodo, para busca el listado tipo de documentos soporte de una remesa
     * @autor mfontalvo
     * @fecha 2006-01-17
     * @see com.tsp.operation.model.DAOS.CumplidoDAO.ListadoDocumentosSoporteRemesa
     * @param remesa a buscar
     * @param estado de los tipo de documentos soportes
     * @param tipo de fechas (fisicas o logicas)
     * @version  1.0
     */

    public void listarSoportesRemesa(String Agencia, String fecIni, String fecFin, String remesa, String estado, String tipo)throws Exception{
        try{
            listaDocumentos = cumpdao.ListadoDocumentosSoporteRemesa(Agencia, fecIni, fecFin, remesa, estado, tipo);
        }
        catch(Exception e){
            throw new Exception("Error en listarCumplidosMigrados [CumplidoService] ...\n" + e.getMessage());
        }
    }
    
    
    public double maxCantidadPlanilla( String distrito, String numpla, String numrem ) throws SQLException{
        return cumpdao.maxCantidadPlanilla ( distrito, numpla, numrem );
    }
    
    public double maxCantidadRemesa( String planilla, String distrito ) throws SQLException{
        return cumpdao.maxCantidadRemesa ( planilla, distrito );
    }
    
    /**
     * Metodo: Instancia de la funcion insertarCumplido del DAO
     * @autor : Ing. Rodrigo Salazar
     * @see InsertarCumplido - CumplidoDAO
     * @param : objeto Cumplido
     * @version : 1.0
     */
    public String insertCumplido(Cumplido var)throws SQLException{
        
        try{
            return cumpdao.insertCumplidos(var);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public double maxCantidadCumplida( String planilla, String distrito ) throws SQLException{
        return cumpdao.maxCantidadCumplida(planilla, distrito);
    }
    
        /**
     * Metodo: updateCumplidos, permite actualizar un cumplido
     * @autor : Ing. Rodrigo Salazar
     * @param : objeto Cumplido
     * @version : 1.0
     */
    public String updateCumplidos(Cumplido var) throws SQLException{
       return cumpdao.updateCumplidos(var);
    }
     /**
     * Metodo: Tiene Cumplido
     * @autor : Ing. Karen reales
     * @param : Placa
     * @version : 1.0
     */
    public boolean tieneCumplido(String placa) throws SQLException{
        return cumpdao.tieneCumplido(placa);
    }
}
 /*****************************************************
 Entregado a karen 13 Febrero 2007
  *****************************************************/
