/********************************************************************
 *  Nombre Clase.................   RelacionGrupoSubgrupoPlacaService.java
 *  Descripci�n..................   Service del DAO de la tabla XXXXXXXXX
 *  Autor........................   Ing. Leonardo Parodi
 *  Fecha........................   16.01.2006
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  EQUIPO12
 */
public class RelacionGrupoSubgrupoPlacaService {
        
        RelacionGrupoSubgrupoPlacaDAO dao;
        /** Creates a new instance of RelacionGrupoSubgrupoPlacaService */
        public RelacionGrupoSubgrupoPlacaService() {
                dao = new RelacionGrupoSubgrupoPlacaDAO();
        }
        
        /**
         * Metodo InsertRelacion , Metodo que llama a InsertRelacion en el DAO
         * @autor : Ing. Leonardo Parody
         * @return : void
         * @version : 1.0
         */
        
        public void InsertRelacion(RelacionGrupoSubgrupoPlaca relacion) throws SQLException{
                dao.setRelacionGrupoSubgrupoPlaca(relacion);
                dao.InsertRelacion();
        }
        
        /**
         * Metodo UpdateRelacion , Metodo que llama a UpdateRelacion en el DAO
         * @autor : Ing. Leonardo Parody
         * @return : void
         * @version : 1.0
         */
        
        public void UpdateRelacion(RelacionGrupoSubgrupoPlaca relacion, String placa, String subworkgroup, String grupo) throws SQLException{
                dao.setRelacionGrupoSubgrupoPlaca(relacion);
                dao.UpdateRelacion(placa, subworkgroup, grupo);
        }
        
        /**
         * Metodo ExisteRelacion , Metodo que llama a ExisteRelacion en el DAO
         * @autor : Ing. Leonardo Parody
         * @return : void
         * @version : 1.0
         */
        
        public boolean ExisteRelacion(String placa, String subworkgroup, String grupo) throws SQLException{
                return dao.ExisteRelacion(placa, subworkgroup, grupo);
        }
        
        /**
         * Metodo ConsultaRelacion , Metodo que llama a ConsultaRelacion en el DAO
         * @autor : Ing. Leonardo Parody
         * @return : void
         * @version : 1.0
         */
        
        public List ConsultaRelacion(String placa, String subworkgroup, String grupo) throws SQLException{
                return dao.ConsultaRelacion(placa, subworkgroup, grupo);
        }
        
        /**
         * Metodo ConsultaInsertRelacion , Metodo que llama a ConsultaRelacion en el DAO
         * @autor : Ing. Leonardo Parody
         * @return : void
         * @version : 1.0
         */
        
        public List ConsultaInsertRelacion(String placa, String grupo, String subworkgroup) throws SQLException{
                return dao.ConsultaInsertRelacion(placa, subworkgroup, grupo);
        }
        
         /**
         * Metodo AnularRelacion , Metodo que llama a ConsultaRelacion en el DAO
         * @autor : Ing. Leonardo Parody
         * @return : void
         * @version : 1.0
         */
        
        public void AnularRelacion(String placa, String subworkgroup, String grupo) throws SQLException{
               dao.AnularRelacion(placa, subworkgroup,grupo);
        }
        
        /**
         * Metodo EliminarRelacion , Metodo que llama a EliminarRelacion en el DAO
         * @autor : Ing. Leonardo Parody
         * @return : void
         * @version : 1.0
         */
        
        public void EliminarRelacion(RelacionGrupoSubgrupoPlaca relacion) throws SQLException{
                dao.setRelacionGrupoSubgrupoPlaca(relacion);
                dao.EliminarRelacion();
        }
        
}
