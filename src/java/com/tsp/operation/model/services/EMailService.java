/*
 * EMailService.java
 *
 * Created on 7 de diciembre de 2005, 04:47 PM
 */

package com.tsp.operation.model.services;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
/**
 *
 * @author  dbastidas
 */
public class EMailService {
    
    private EMailDAO emaild;
    /** Creates a new instance of EMailService */
    public EMailService() {
        emaild = new EMailDAO();
    }
    public EMailService(String dataBaseName) {
        emaild = new EMailDAO(dataBaseName);
    }
    
    public  void  saveMail(Email  mail)throws Exception{
        try{
            emaild.saveMail(mail);
        }
        catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
}
