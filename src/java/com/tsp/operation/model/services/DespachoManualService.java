
/***********************************************************************************
 * Nombre clase : ............... DespachoManualService.java                            *
 * Descripcion :................. Clase que instancia los metodos de DespachoManualDAO  *
 *                                con la BD.                                            *
 * Autor :....................... Ing. David Lamadrid                                   *
 * Fecha :....................... 15 de noviembre de 2005, 11:02 AM                     *
 * Version :..................... 1.0                                                   *
 * Copyright :................... Fintravalores S.A.                               *
 ***********************************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
/**
 *
 * @author  dlamadrid
 */
public class DespachoManualService
{
    
    private DespachoManualDAO dao;
    
    /** Creates a new instance of DespachoManualService */
    public DespachoManualService ()
    {
        this.dao= new DespachoManualDAO ();
    }
    
    
    /**
     * Metodo insertarDespacho (), recibe un objeto tipo DespachoManual seteado y realiza una transaccion
     * para insertar en las tablas de despacho_maunal ,trafico e ingreso_trafcico,
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public boolean insertarDespacho () throws SQLException
    {
        return dao.insertarDespacho();
    }
    
    /**
     * Metodo: BuscarPuestosControl()
     * Descripcion:busca los puestos de control referentes a una ruta
     * parametros: String ruta
     * Autor: Andres Martinez
     */
     public void BuscarPuestosControl(String ruta) throws SQLException{
        dao.BuscarPuestosControl(ruta);
     }
     /**
     * Metodo: BuscarPuestoControl()
     * Descripcion:busca los puestos de control referentes a una ruta
     * parametros: String ruta
     * Autor: Andres Martinez
     */
     public String  BuscarPuestoControl(String numpla) throws SQLException{
        return dao.BuscarPuestoControl(numpla);
     }
    /**
     * Metodo eliminarDespacho, Metodo que permite la eliminacion de un despacho_manual en la bd
     * @autor : Ing. David Lamadrid
     * @param : String distrito,String placa,String fechaDespacho
     * @version : 1.0
     */
    public void eliminarDespacho (String distrito,String placa,String fechaDespacho,String planilla) throws SQLException
    {
        dao.eliminarDespacho (distrito, placa, fechaDespacho,planilla);
    }
    
    /**
     * Metodo existeDespacho, Metodo que permite la verificacion del la existencia de un despacho_manual en la bd
     * @autor : Ing. David Lamadrid
     * @param : String distrito,String placa,String fechaDespacho
     * @version : 1.0
     */
    public boolean existeDespacho (String distrito,String placa,String fechaDespacho ) throws SQLException
    {
        return dao.existeDespacho (distrito, placa, fechaDespacho);
    }
    
    /**
     * Metodo listaDespacho  , Metodo que setea un vector de objetos tipo DespahoManual de regitros
     * de despacho_manual en la base de daTOS
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public void listaDespacho () throws SQLException
    {
       
        dao.listaDespacho ();
       
    }
    
    /**
     * Metodo listaDespacho  , Metodo que setea un vector de objetos tipo DespahoManual de regitros
     * de despacho_manual en la base de daTOS
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public Vector listaPuestos () throws SQLException
    {
        return dao.getPc();
    }
    
     /**
     * Metodo listaJustificaciones  , Metodo que setea un vector de objetos tipo DespahoManual de regitros
     * de despacho_manual en la base de daTOS
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public Vector listaJustificaciones () throws SQLException
    {
     
        return dao.getJus();
    }
    /**
     * Getter for property despacho.
     * @return Value of property despacho.
     */
    public com.tsp.operation.model.beans.DespachoManual getDespacho ()
    {
        return dao.getDespacho ();
    }
    
    /**
     * Setter for property despacho.
     * @param despacho New value of property despacho.
     */
    public void setDespacho (com.tsp.operation.model.beans.DespachoManual despacho)
    {
        dao.setDespacho (despacho);
    }
    
    /**
     * Setter for property vDespacho.
     * @param vDespacho New value of property vDespacho.
     */
    public void setVDespacho (java.util.Vector vDespacho)
    {
        dao.setVDespacho (vDespacho);
    }
    
    /**
     * Getter for property vDespacho.
     * @return Value of property vDespacho.
     */
    public java.util.Vector getVDespacho ()
    { 
        return dao.getVDespacho (); 
    }
    
    /**
     * Metodo despachosPorPlaca , Metodo que setea un vector de  objetos tipo DespahoManual dada la placa
     * @autor : Ing. David Lamadrid
     * @param : String placa
     * @version : 1.0
     */
    public void despachosPorPlaca (String placa) throws SQLException
    {
        dao.despachosPorPlaca (placa);
    }
    
    /**
     * Metodo despachosPorRuta , Metodo que setea un vector de  objetos tipo DespahoManual dada la ruta
     * @autor : Ing. David Lamadrid
     * @param : String ruta
     * @version : 1.0
     */
    public void despachosPorRuta (String ruta) throws SQLException
    {
        
        dao.despachosPorRuta (ruta);
    }
    
    /**
     * Metodo despachosPorCliente , Metodo que setea un vector de  objetos tipo DespahoManual dado el codigo del cliente
     * @autor : Ing. David Lamadrid
     * @param : String cliente
     * @version : 1.0
     */
    public void despachosPorCliente (String cliente) throws SQLException
    {
        
        dao.despachosPorCliente (cliente);
    }
    
    /**
     * Metodo despachoPorCodigo , Metodo que setea un objeto tipo DespahoManual dada la llave primaria
     * de despacho manual en la base de daTOS
     * @autor : Ing. David Lamadrid
     * @param : Connection con
     * @version : 1.0
     */
    public void despachoPorCodigo (String distrito,String placa,String fechaDespacho ) throws SQLException
    {
        dao.despachoPorCodigo (distrito, placa, fechaDespacho);
    }
    
    /**
     * Metodo actualizarDespacho, Metodo que permite la actualizacion de un despacho_manual en la bd
     * @autor : Ing. David Lamadrid
     * @param : String d,String p,String f,String conductor,String ruta,String cliente,String carga
     * @version : 1.0
     */
    public boolean actualizarDespacho () throws SQLException
    {
        return dao.actualizarDespacho();
    }
    
    /*************************************************************************************/
    
    /**
     * Metodo: obtenerNombreZona
     * Descripcion: busca el metodo obtenerNombreZona() en el DAO.
     * parametros: String zona
     * Autor: LREALES
     */
     public String  obtenerNombreZona( String zona ) throws SQLException {
         
        return dao.obtenerNombreZona( zona );
        
     }
    
     /**
     * Metodo: buscarNombreConductor
     * Descripcion: busca el metodo buscarNombreConductor() en el DAO.
     * parametros: String planilla
     * Autor: LREALES
     */
     public String  buscarNombreConductor( String planilla ) throws SQLException {
         
         return dao.buscarNombreConductor( planilla );
        
     }
     
     /**
     * Metodo: buscarNombreCliente
     * Descripcion: busca el metodo buscarNombreCliente() en el DAO.
     * parametros: String planilla
     * Autor: LREALES
     */
     public String  buscarNombreCliente( String planilla ) throws SQLException {
         
         return dao.buscarNombreCliente( planilla );
        
     }
     
}
