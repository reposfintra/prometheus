/***********************************************************************************
 * Nombre clase :  UsuarioAprobacionService.java         
 * Descripcion : Clase que maneja los Servicios               
 *               asignados a Model relacionados con el      
 *               programa de Usuaraio aprobacion
 * Autor :    Ing. Diogenes Antonio Bastidas Morales        
 * Fecha :     13 de enero de 2006, 06:10 PM           
 * Version :   1.0                                            
 * Copyright : Fintravalores S.A.                        
 ***********************************************************************************/
 



package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;


public class UsuarioAprobacionService {
    private UsuarioAprobacionDAO usuaprod;
    /** Creates a new instance of UsuarioAprobacionService */
    public UsuarioAprobacionService() {
        usuaprod = new UsuarioAprobacionDAO();
    }
    public UsuarioAprobacionService(String dataBaseName) {
        usuaprod = new UsuarioAprobacionDAO(dataBaseName);
    }
    
    /**
     * Metodo obtVecTablaAprovacion, retorna el vector de las tabla 
     *        que puede aprovar el usuario 
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: obtVecTablaAprovacion - UsuarioAprobacionDAO
     * @version : 1.0
     */
    public Vector obtVecTablaAprovacion (){
        return usuaprod.obtVecTablaAprovacion();
    }
    
     /**
     * Metodo listarTablaUsuAprobacion, lista tabla usuario de aprovacion                  
     * @param: login
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: listarTablaUsuAprobacion - UsuarioAprobacionDAO
     * @version : 1.0
     */ 
    public void listarTablaUsuAprobacion( String login ) throws SQLException {
        usuaprod.listarTablaUsuAprobacion(login);
    }
    
     /**
     * Metodo insertarUsuario, lista tabla usuario de aprovacion                  
     * @param: Objeto Usuario aprobacion
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: insertarUsuario - UsuarioAprobacionDAO
     * @version : 1.0
     */ 
    public void insertarUsuario(UsuarioAprobacion usuapro ) throws SQLException {
        usuaprod.setUsuarioAprobacion(usuapro);
        usuaprod.insertarUsuario();
    }
    
    /**
     * Metodo listarUsuariosAprobacion, lista tabla usuario de aprovacion                  
     * @param: agencia,tabla,usuario
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: listarUsuariosAprobacion - UsuarioAprobacionDAO     
     * @version : 1.0
     */ 
    public void listarUsuariosAprobacion( String agencia, String tabla, String usuario ) throws SQLException {
        usuaprod.listarUsuariosAprobacion(agencia, tabla, usuario);
    }
    
    /**
     * Metodo anularUsuarioAprobacion, anula un registro
     * de la tabla usuario_aprobacion                  
     * @param: agencia,tabla,usuario
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: anularUsuarioAprobacion - UsuarioAprobacionDAO   
     * @version : 1.0
     */ 
    public void eliminarUsuarioAprobacion(UsuarioAprobacion usuapro) throws SQLException {
        usuaprod.setUsuarioAprobacion(usuapro);
        usuaprod.eliminarUsuarioAprobacion();
    }
     
    /**
     * Metodo buscarUsuariosAprobacion, busca el objeto tipo                   
     * UsuarioAprobacion
     * @param: agencia,tabla,usuario
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: anularUsuarioAprobacion - UsuarioAprobacionDAO   
     * @version : 1.0
     */ 
    public void buscarUsuarioAprobacion( String agencia, String tabla, String usuario ) throws SQLException {
        usuaprod.buscarUsuarioAprobacion(agencia, tabla, usuario);
    }
    
    /**
     * Metodo getUsuarioAprobacion,obtiene el objeto del usuario aprobacion                  
     * @param: 
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: anularUsuarioAprobacion - UsuarioAprobacionDAO   
     * @version : 1.0
     */ 
    public UsuarioAprobacion getUsuarioAprobacion(){
        return usuaprod.getUsuarioAprobacion();
    }
    
     /**
     * Metodo modificarUsuario, modifica los usuario de aprovacion                  
     * @param: objeto Usuario Aprobacion
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: modificarUsuario - UsuarioAprobacionDAO
     * @version : 1.0
     */ 
    public void modificarUsuario( UsuarioAprobacion usuapro) throws SQLException {
        usuaprod.setUsuarioAprobacion(usuapro);
        usuaprod.modificarUsuario();
    }
      
    /**
     * Metodo existeUsuariosAprobacion, busca el objeto tipo                   
     * UsuarioAprobacion
     * @param: agencia,tabla,usuario
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */ 
    public boolean existeUsuariosAprobacion( String agencia, String tabla, String usuario ) throws SQLException {
        return usuaprod.existeUsuariosAprobacion(agencia, tabla, usuario);
    }
    
}
