/***************************************************************************
 * Nombre clase : ............... ReporteTarjetaService.java               *
 * Descripcion :................. Clase que maneja los Servicios           *
 *                                asignados a Model relacionados con el    *
 *                                programa de RegistroTarjeta              *
 * Autor :....................... Ing. Juan Manuel Escandon Perez          *
 * Fecha :........................ 6 de diciembre de 2005, 08:38 AM        *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.sql.*;
/**
 *
 * @author  JuanM
 */
public class RegistroTarjetaService {
    /*
     *Declaracion de atributos
     */
    private RegistroTarjetaDAO  RTDataAccess;
    private List                ListaRT;
    private RegistroTarjeta     Datos;
    /** Creates a new instance of RegistroTarjetaService */
    public RegistroTarjetaService() {
        RTDataAccess    = new RegistroTarjetaDAO();
        ListaRT         = new LinkedList();
        Datos           = new RegistroTarjeta();
    }
    
    /**
     * Metodo Insert, a�ade un nuevo registro a la tabla RegistroTarjeta
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String dstrct, String num_tarjeta , String placa, String cedula, String creation_user, String user_update
     * @see   : Insert - RegistroTarjetaDAO
     * @version : 1.0
     */
    public void Insert(String dstrct, String num_tarjeta , String placa, String cedula, String creation_user, String user_update) throws Exception {
        try{
            RTDataAccess.INSERT(dstrct, num_tarjeta, placa, cedula, creation_user, user_update);
        }
        catch(Exception e){
            throw new Exception("Error en Insert [RegistroTarjetaService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Metodo UpdateEstado, modifica el campo reg_status en la tabla RegistroTarjeta
     * para un reg_status y una placa, std_job_no correspondiente
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String reg_status, String placa , String std_job
     * @see   : UpdateEstado - FlotaDAO
     * @version : 1.0
     */
    public void UpdateEstado(String reg_status, String numtarjeta,  String cedula, String fecha_creacion , String usuario) throws Exception {
        try{
            RTDataAccess.CESTADO(reg_status, numtarjeta, cedula, fecha_creacion, usuario);
        }
        catch(Exception e){
            throw new Exception("Error en Update [RegistroTarjetaService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Metodo Delete, elimina un registro de la tabla RegistroTarjeta
     * para un num_tarjeta, cedula  y una fecha_creacion correspondiente
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String num_tarjeta , String std_job, String cedula
     * @see   : Delete - FlotaDAO
     * @version : 1.0
     */
    public void Delete(String num_tarjeta,  String cedula, String fecha_creacion ) throws Exception {
        try{
            RTDataAccess.DELETE(num_tarjeta, cedula, fecha_creacion);
        }
        catch(Exception e){
            throw new Exception("Error en Delete [RegistroTarjetaService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Metodo List, lista todos los registros  no anulados de la tabla registrotarjeta
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String placa
     * @see   : ListxPlaca - FlotaDAO
     * @version : 1.0
     */
    public void List() throws Exception {
        try{
            ReiniciarLista();
            ListaRT = RTDataAccess.LIST();
        }
        catch(Exception e){
            throw new Exception("Error en List [RegistroTarjetaService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Metodo List, lista los registros la tabla registrotarjeta de acuerdo a los parametros de busqueda
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String placa
     * @see   : ListxPlaca - FlotaDAO
     * @version : 1.0
     */
    public void ListREG( String numtarjeta, String cedula, String placa, String fechacreacion ) throws Exception {
        try{
            ReiniciarLista();
            ListaRT = RTDataAccess.LISTREG(numtarjeta, cedula, placa, fechacreacion);
        }
        catch(Exception e){
            throw new Exception("Error en ListREG [RegistroTarjetaService]...\n"+e.getMessage());
        }
    }
    
    
    
    /**
     * Metodo Buscar, retorna un true si encuentra un registro dentro de la
     * tabla RegistroTarjeta, para un num_tarjeta, y una fecha_creacion correspondiente
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String num_tarjeta, String cedula, String fecha_creacion
     * @see   : Buscar - RegistroTarjetaDAO
     * @version : 1.0
     */
    public boolean Buscar( String num_tarjeta, String cedula, String fecha_creacion ) throws SQLException {
        try{
            return RTDataAccess.BUSCAR(num_tarjeta, cedula, fecha_creacion);
        }
        catch(Exception e){
            throw new SQLException("Error en Buscar [RegistroTarjetaService]...\n"+e.getMessage());
        }
    }
    
     /**
     * Metodo Buscar, retorna un true si encuentra un registro dentro de la
     * tabla RegistroTarjeta, para un num_tarjeta, y una fecha_creacion correspondiente
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String num_tarjeta, String cedula, String fecha_creacion, String placa
     * @see   : Buscar - RegistroTarjetaDAO
     * @version : 1.0
     */
    public boolean buscarAll( String num_tarjeta, String cedula, String fecha_creacion, String placa ) throws SQLException {
        try{
            return RTDataAccess.buscarAll(num_tarjeta, cedula, fecha_creacion, placa);
        }
        catch(Exception e){
            throw new SQLException("Error en Buscar [RegistroTarjetaService]...\n"+e.getMessage());
        }
    }
    
    
    /**
     * Metodo Buscar, retorna un true si encuentra un registro dentro de la
     * tabla TarjetaIntel(tablagen), para un num_tarjeta.
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String num_tarjeta
     * @see   : Buscar - RegistroTarjetaDAO
     * @version : 1.0
     */
    public boolean BuscarTarjeta( String num_tarjeta ) throws SQLException {
        try{
            return RTDataAccess.BUSCARTARJETA(num_tarjeta);
        }
        catch(Exception e){
            throw new SQLException("Error en Buscar [RegistroTarjetaService]...\n"+e.getMessage());
        }
    }
    
    
    /**
     * Metodo Buscar, retorna un true si encuentra un registro dentro de la
     * tabla RegistroTarjeta, para un num_tarjeta, y una fecha_creacion correspondiente
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String num_tarjeta, String cedula, String fecha_creacion
     * @see   : Buscar - RegistroTarjetaDAO
     * @version : 1.0
     */
    public void Search( String num_tarjeta, String cedula, String fecha_creacion ) throws SQLException {
        try{
            this.ReiniciarDato();
            this.Datos = RTDataAccess.SEARCH(num_tarjeta, cedula, fecha_creacion);
        }
        catch(Exception e){
            throw new SQLException("Error en Buscar [RegistroTarjetaService]...\n"+e.getMessage());
        }
    }
    
    
    /**
     * Metodo Update, modifica un registro en la tabla RegistroTarjeta
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String nnum_tarjeta, String nplaca, String ncedula, String ncreation_user, String nuser_update, String num_tarjeta, String cedula, String fecha_creacion
     * @see   : Update - FlotaDAO
     * @version : 1.0
     */
    public void Update( String nnum_tarjeta, String nplaca, String ncedula, String ncreation_user, String nuser_update, String num_tarjeta, String cedula, String fecha_creacion, String nfecha ) throws Exception {
        try{
            RTDataAccess.UPDATE(nnum_tarjeta, nplaca, ncedula, ncreation_user, nuser_update, num_tarjeta, cedula, fecha_creacion, nfecha);
        }catch(Exception e){
            throw new Exception("Error en Update [RegistroTarjetaService]...\n"+e.getMessage());
        }
    }
    
    
    public void ReiniciarDato(){
        this.Datos = null;
    }
    public void ReiniciarLista(){
        this.ListaRT = new LinkedList();
    }
    
    public RegistroTarjeta getDato(){
        return this.Datos;
    }
    
    public List getList(){
        return this.ListaRT;
    }
    
}
