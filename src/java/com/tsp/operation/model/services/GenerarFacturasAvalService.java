/*
 * Nombre        GenerarFacturasAvalService.java
 * Descripci�n   Clase para el acceso a los datos  del programa generar facturas de aval
 * Autor         Iris vargas
 * Fecha         28 de abril de 2012, 12:08 PM
 * Versi�n       1.0
 * Coyright      Geotech S.A.
 */
package com.tsp.operation.model.services;

import com.tsp.finanzas.contab.model.DAO.ComprobantesDAO;
import com.tsp.finanzas.contab.model.DAO.ContabilizacionNegociosDAO;
import com.tsp.finanzas.contab.model.DAO.MovAuxiliarDAO;
import com.tsp.finanzas.contab.model.beans.Comprobantes;
import com.tsp.operation.model.DAOS.GenerarFacturasAvalDAO;
import com.tsp.operation.model.beans.BeanGeneral;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

/**
 * Clase para el acceso a los datos de generacion de facturas de aval
 * @author Iris Vargas
 */
public class GenerarFacturasAvalService {

    GenerarFacturasAvalDAO dao;
    private ComprobantesDAO comproDAO;
    private ContabilizacionNegociosDAO contdao;
    private MovAuxiliarDAO movDAO;

    public GenerarFacturasAvalService() {
        dao = new GenerarFacturasAvalDAO();
        movDAO         = new  MovAuxiliarDAO();
        comproDAO      = new  ComprobantesDAO();
        contdao            = new  ContabilizacionNegociosDAO();
    }
    public GenerarFacturasAvalService(String dataBaseName) {
        dao = new GenerarFacturasAvalDAO(dataBaseName);
        movDAO         = new  MovAuxiliarDAO(dataBaseName);
        comproDAO      = new  ComprobantesDAO(dataBaseName);
        contdao            = new  ContabilizacionNegociosDAO(dataBaseName);
    }

    /**
     * Sql con el listado de convenios segun campo anombre
     * @return sql con el listado de convenios segun campo anombre
     * @param anombre  indica si el convenio esta o no anombre de un tercero
     * @throws Exception cuando hay error
     */
    public ArrayList<String> buscarConveniosAnombre(boolean anombre) throws Exception {
        ArrayList<String> lista;
        try {
            lista = dao.buscarConveniosAnombre(anombre);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return lista;
    }

     /**
     * Sql con el listado de convenios segun el avalista
     * @return sql con el listado de convenios segun el avalista
     * @param avalista  indica el nit del avalista
     * @throws Exception cuando hay error
     */
    public ArrayList<String> buscarConveniosAvalista(String avalista) throws Exception {
        ArrayList<String> lista;
        try {
            lista = dao.buscarConveniosAvalista(avalista);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return lista;
    }

    /**
     * Sql con el listado de afiliados segun convenio
     * @return sql con el listado de afiliados segun convenio
     * @param convenio
     * @throws Exception cuando hay error
     */
    public ArrayList<String> buscarAfiliadosConvenio(int convenio) throws Exception {
        ArrayList<String> lista;
        try {
            lista = dao.buscarAfiliadosConvenio(convenio);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return lista;
    }

    /**
     * Sql con el listado de Avalistas
     * @return sql con el listado de Avalistas
     * @throws Exception cuando hay error
     */
    public ArrayList<String> buscarAvalistas() throws Exception {
        ArrayList<String> lista;
        try {
            lista = dao.buscarAvalistas();

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return lista;
    }

    /**
     * Sql con el listado de negocios a generar cxc segun filtros
     * @return sql con el listado de negocios a generar cxc segun filtros
     * @param fechaini
     * @param fechafin
     * @param convenio
     * @param anombre
     * @param afiliado
     * @throws Exception cuando hay error
     */
    public ArrayList<BeanGeneral> buscarNegocios(String fechaini, String fechafin, int convenio, boolean anombre, String afiliado) throws Exception {
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
        try {
            lista = dao.buscarNegocios(fechaini, fechafin, convenio, anombre, afiliado);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return lista;
    }

     /**
     * Sql con el listado de negocios a generar cxp segun filtros
     * @return sql con el listado de negocios a generar cxp segun filtros
     * @param fechaini
     * @param fechafin
     * @param convenio
     * @throws Exception cuando hay error
     */
    public ArrayList<BeanGeneral> buscarNegocios(String fechaini, String fechafin, int convenio) throws Exception {
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
        try {
            lista = dao.buscarNegocios(fechaini, fechafin, convenio);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return lista;
    }

     /**
     * Sql con el listado de afiliados
     * @return sql con el listado  de afiliados
     * @param fechaini
     * @param fechafin
     * @param convenio
     * @throws Exception cuando hay error
     */
    public ArrayList<String> buscarAfiliados(String fechaini, String fechafin, int convenio) throws Exception {
        ArrayList<String> lista = new ArrayList<String>();
        try {
            lista = dao.buscarAfiliados(fechaini, fechafin, convenio);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return lista;
    }
   
    public Comprobantes buscarNegocio(String negocio, String dstrct) throws java.lang.Exception {
        return dao.buscarNegocio(negocio, dstrct);

    }

    public String getAccountDES(String oc) throws Exception {
        return dao.getAccountDES(oc);
    }

    /**
     * M�todo que inserta el comprobante y comprodet
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String insertar(Comprobantes comprobante, String user) throws Exception {
        String error = "";
        try {
            error = evaluarComprobante(comprobante);
            if (error.equals("")) {
                List items = comprobante.getItems();
                List aux = new LinkedList();
                int sw = 0;
                // Validamos los items:
                if (items != null) {
                    for (int j = 0; j < items.size(); j++) {
                        Comprobantes comprodet = (Comprobantes) items.get(j);
                        String evaDet = evaluarComprobante(comprodet);
                        if (evaDet.equals("")) {
                            aux.add(comprodet);
                        } else {
                            error = "Error item " + j + " " + evaDet;   // Guardamos el item
                            sw = 1;
                            break;
                        }
                    }
                }
                if (sw == 0) {
                    items = aux;  // Items q cumplen para ser insertados
                    if (items != null && items.size() > 0) {
                        // Tomamos la secuencia:
                        int sec = comproDAO.getGrupoTransaccion();
                        comprobante.setGrupo_transaccion(sec);
                        error = contdao.InsertComprobante(comprobante, user);
                        error += contdao.InsertComprobanteDetalle2(items, sec, user);

                    } else {
                        error = "Error No presenta items validos";
                    }
                }
            } else {
                error = "Error" + error;
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(" INSERTAR COMPROBANTE NEGOCIOS : " + e.getMessage());
        }
        return error;
    }

    /**
     * M�todo que evalua datos v�lidos del comprobante y comprodet
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String evaluarComprobante(Comprobantes comprobante) throws Exception {
        String msj = "";
        try {
            //System.out.println("Evalua");
            // Campos comunes:
            if (comprobante.getDstrct().trim().equals("")) {
                msj += " No presenta distrito";
            }
            if (comprobante.getTipodoc().trim().equals("")) {
                msj += " No presenta tipo de documento";
            }
            if (comprobante.getNumdoc().trim().equals("")) {
                msj += " No presenta n�mero de documento";
            }
            if (comprobante.getPeriodo().trim().equals("")) {
                msj += " No presenta periodo de contabilizaci�n";
            }
            if (comprobante.getDetalle().trim().equals("")) {
                msj += " No presenta una descripci�n o detalle";
            }
            if (comprobante.getBase().trim().equals("")) {
                msj += " No presenta base";
            }


            String tipo = comprobante.getTipo();

            if (tipo.equals("C")) {

                if (comprobante.getTotal_debito() != comprobante.getTotal_credito()) {
                    msj += " Est� descuadrado en valores debitos y cr�ditos";
                }
                if (comprobante.getSucursal().trim().equals("")) {
                    msj += " No presenta sucursal";
                }
                if (comprobante.getFechadoc().trim().equals("")) {
                    msj += " No presenta fecha de documento";
                }
                if (comprobante.getTercero().trim().equals("")) {
                    msj += " No presenta tercero";
                }
                if (comprobante.getMoneda().trim().equals("")) {
                    msj += " No presenta moneda";
                }
                if (comprobante.getAprobador().trim().equals("")) {
                    msj += " No presenta aprobador";
                }

            } else {

                if (comprobante.getCuenta().trim().equals("")) {
                    msj += " No presenta cuenta";
                } else {
                    String cta = comprobante.getCuenta();
                    Hashtable cuenta = movDAO.getCuenta(comprobante.getDstrct(), cta);

                    if (cuenta != null) {

                        String requiereAux = (String) cuenta.get("auxiliar");
                        String requiereTercero = (String) cuenta.get("tercero");

                        // Tercero.
                        if (requiereTercero.equals("S") && comprobante.getTercero().trim().equals("")) {
                            msj += " La cuenta " + cta + " requiere tercero  y el item no presenta tercero";
                        }

                        // Auxiliar.
                        if (requiereAux.equals("S")) {

                            if (comprobante.getAuxiliar().trim().equals("")) {
                                msj += " La cuenta " + cta + " requiere auxiliar y el item no presenta auxiliar";
                            }

                            if (!contdao.existeCuentaSubledger(comprobante.getDstrct(), cta, comprobante.getAuxiliar())) {
                                msj += "La cuenta " + cta + " no tiene el subledger " + comprobante.getAuxiliar() + " asociado";
                            }
                        }

                    } else {
                        msj += " La cuenta " + cta + " no existe o no est� activa";
                    }

                }

            }

            System.out.println("Msg" + msj);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
        return msj;
    }

     public String ingresarCXCComisionAval(BeanGeneral bg) throws SQLException {
         return dao.ingresarCXCComisionAval(bg);
     }

     public String ingresarDetalleCXCComisionAval(BeanGeneral bg, String item) throws SQLException {
         return dao.ingresarDetalleCXCComisionAval(bg, item);
     }

     public String ingresarCXPAvalista(BeanGeneral bg) throws SQLException {
         return dao.ingresarCXPAvalista(bg);
     }

         public String ingresarDetalleCXPAvalista(BeanGeneral bg) throws SQLException {
              return dao.ingresarDetalleCXPAvalista(bg);
     }
         
}
