 /********************************************************************
 *      Nombre Clase.................   DemorasService.java    
 *      Descripci�n..................   Service de DemorasDAO
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   05.08.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;


public class DemorasService {
    
    private DemorasDAO demorasDAO;
    private boolean ejecutoProceso=false;
    private Vector planillasReporte;
    private Hashtable PlanillasSelec = new Hashtable();
    
    /** Creates a new instance of DemorasService */
    public DemorasService() {
        this.demorasDAO = new DemorasDAO();
    }
    
    /**
     * M�todo para agregar una demora al archivo de demoras. 
     * @autor Tito Andr�s Maturana
     * @param demora Instancia de la clase Demora.
     * @see com.tsp.operation.model.DAOS.DemorasDAO#agregarDemora(Demora)
     * @throws SQLException
     * @version 1.0
     */
    public void agregarDemora(Demora demora) throws java.sql.SQLException{
        this.demorasDAO.agregarDemora(demora);        
    }
    
    /**
     * M�todo para finalizar una demora dentro del archivo de demoras. 
     * @autor Tito Andr�s Maturana
     * @param demora Instancia de la clase Demora.
     * @see com.tsp.operation.model.DAOS.DemorasDAO#finalizarDemora(Demora)
     * @throws SQLException
     * @version 1.0
     */
    public void finalizarDemora(Demora demora) throws java.sql.SQLException{        
        this.demorasDAO.finalizarDemora(demora);        
    }
    
    /**
     * Verifica la existencia de una planilla en el archivo planilla. 
     * @autor Tito Andr�s Maturana
     * @param numpla N�mero de la planilla.
     * @see com.tsp.operation.model.DAOS.DemorasDAO#existePlanilla(String)
     * @throws SQLException
     * @return true si existe o false sino.
     * @version 1.0
     */
    public boolean existePlanilla(String numpla) throws java.sql.SQLException{        
        return this.demorasDAO.existePlanilla(numpla);        
    }
    
    /**
     * Verifica la existencia de una caravana en el archivo de caravanas. 
     * @autor Tito Andr�s Maturana
     * @param numcaravana N�mero de la caravana.
     * @see com.tsp.operation.model.DAOS.DemorasDAO#existeCaravana(Integer)
     * @return true si existe o false sino.
     * @throws SQLException
     * @version 1.0
     */
    public boolean existeCaravana(int numcaravana) throws java.sql.SQLException{        
        return this.demorasDAO.existeCaravana(numcaravana);        
    }
    
    /**
     * Obtiene la ultima planilla de un veh�culo. 
     * @autor Tito Andr�s Maturana
     * @param numcaravana N�mero de la caravana.
     * @see com.tsp.operation.model.DAOS.DemorasDAO#ultimaPlanilla(String)
     * @return N�mero de la �ltima planilla
     * @throws SQLException
     * @version 1.0
     */
    public String ultimaPlanillaVehiculo(String plaveh) throws java.sql.SQLException{        
        return this.demorasDAO.ultimaPlanilla(plaveh);        
    }
        
    /**
     * Obtiene los numeros de las planillas de los veh�vulos
     * que conforman una caravana. 
     * @autor Tito Andr�s Maturana
     * @param numcaravana N�mero de la caravana.
     * @see com.tsp.operation.model.DAOS.DemorasDAO#vechiculosCaravana(Integer)
     * @return Vector con los numeros de planilla que conforman la caravana
     * @throws SQLException
     * @version 1.0
     */
    public Vector planillasCaravana(int numcaravana) throws java.sql.SQLException{        
        return this.demorasDAO.vechiculosCaravana(numcaravana);        
    }
    
    /**
     *  Los siguientes m�todos deben ser reemplazados x los propios ens sus respectivos 
     *  Service.
     */
    public TreeMap tramosPlanilla() throws java.sql.SQLException{        
        TreeMap tramos = new TreeMap();
        tramos.put("Origen1 - Destino1", "O1D1");
        tramos.put("Origen2 - Destino2", "O2D2");
        tramos.put("Origen3 - Destino3", "O3D3");
        tramos.put("Origen4 - Destino4", "O4D4");
        tramos.put("Origen5 - Destino5", "O5D5");
        return tramos;
    }
    
    public TreeMap tramosCaravana() throws java.sql.SQLException{        
        TreeMap tramos = new TreeMap();
        tramos.put("Origen1 - Destino1", "O1D1");
        tramos.put("Origen2 - Destino2", "O2D2");
        tramos.put("Origen3 - Destino3", "O3D3");
        tramos.put("Origen4 - Destino4", "O4D4");
        tramos.put("Origen5 - Destino5", "O5D5");
        return tramos;
    }
    
    public TreeMap codigosJerarquia() throws java.sql.SQLException{        
        TreeMap jer = new TreeMap();
        jer.put("Causa 1", "CAU1");
        jer.put("Causa 2", "CAU2");
        jer.put("Causa 3", "CAU3");
        jer.put("Causa 4", "CAU4");
        jer.put("Causa 5", "CAU5");
        jer.put("Causa 6", "CAU6");
        return jer;
    }
    
    public Vector planillasTramo(){
        Vector vec = new Vector();
        vec.add("CP3000");
        vec.add("CP3595");
        vec.add("CP1100");
        vec.add("CP2200");
        return vec;
    }
   
    /**
     * Obtiene una demora del archivo de demoras
     * que conforman una caravana. 
     * @autor Tito Andr�s Maturana
     * @param numpla N�mero de la planilla.
     * @see com.tsp.operation.model.DAOS.DemorasDAO#obtenerDemora(String)
     * @return La �ltima demora de una planilla
     * @throws SQLException
     * @version 1.0
     */
    public Demora obtenerDemora(String numpla) throws java.sql.SQLException{        
        return this.demorasDAO.obtenerDemora(numpla);        
    }
    
    /**
     * Verifica que la planilla se encuentre registrada en la tabla
     * ingreso_trafico
     * @autor Tito Andr�s Maturana
     * @param numpla N�mero de la planilla.
     * @see com.tsp.operation.model.DAOS.DemorasDAO#planillaEnIngresoTrafico(String)
     * @throws SQLException
     * @return
     */
    public boolean planillaEnIngresoTrafico(String numpla)throws SQLException{
        return this.demorasDAO.planillaEnIngresoTrafico(numpla);
    }
    
    /**
     * Verifica la existendstrct de una demora.
     * @autor Tito Andr�s Maturana
     * @param numpla N�mero de la planilla.
     * @param fecdem Fecha de la demora.
     * @param duracion Duraci�n de la demora.
     * @see com.tsp.operation.model.DAOS.DemorasDAO#existeDemora(String, String)
     * @throws SQLException
     * @return true si existe, false sino.
     */
    public boolean existeDemora(String numpla, String fecdem, int duracion)throws SQLException{
        return this.demorasDAO.existeDemora(numpla, fecdem, duracion);
    }
    
  
    
    /**
     * Obtiene un vector con las demoras de una planilla
     * @autor Tito Andr�s Maturana
     * @param numpla N�mero de la planilla.
     * @return Arreglo de demoras.
     * @see com.tsp.operation.model.DAOS.DemorasDAO#listarDemorasPlanilla(String)
     * @throws SQLException
     * @version 1.0
     */
    public Vector listarDemorasPlanilla(String numpla)throws SQLException{
        return this.demorasDAO.listarDemorasPlanilla(numpla);
    }
    
    /**
     * Obtiene una demora del archivo de demoras por el codigo
     * @autor Tito Andr�s Maturana
     * @param id C�digo de la demora.
     * @see com.tsp.operation.model.DAOS.DemorasDAO#obtenerDemoraID(String)
     * @return La demora de una planilla con el id especificado
     * @throws SQLException
     * @version 1.0
     */
    public Demora obtenerDemoraID(String id) throws SQLException{
        return this.demorasDAO.obtenerDemoraID(id);
    }
    
    /**
     * Getter for property ejecutoProceso.
     * @return Value of property ejecutoProceso.
     */
    public boolean isEjecutoProceso() {
        return ejecutoProceso;
    }
    
    /**
     * Setter for property ejecutoProceso.
     * @param ejecutoProceso New value of property ejecutoProceso.
     */
    public void setEjecutoProceso(boolean ejecutoProceso) {
        this.ejecutoProceso = ejecutoProceso;
    }
    
    /**
     * Getter for property planillasReporte.
     * @return Value of property planillasReporte.
     */
    public java.util.Vector getPlanillasReporte() {
        return planillasReporte;
    }
    
    /**
     * Setter for property planillasReporte.
     * @param planillasReporte New value of property planillasReporte.
     */
    public void setPlanillasReporte(java.util.Vector planillasReporte) {
        this.planillasReporte = planillasReporte;
    }
    
    /**
     * Getter for property PlanillasSelec.
     * @return Value of property PlanillasSelec.
     */
    public java.util.Hashtable getPlanillasSelec() {
        return PlanillasSelec;
    }
    
    /**
     * Setter for property PlanillasSelec.
     * @param PlanillasSelec New value of property PlanillasSelec.
     */
    public void setPlanillasSelec(java.util.Hashtable PlanillasSelec) {
        this.PlanillasSelec = PlanillasSelec;
    }
    
    /**
     * M�todo para agregar una demora al archivo de demoras. 
     * @autor Tito Andr�s Maturana
     * @param demora Instandstrct de la clase Demora.
     * @param sw Devolver cadena
     * @throws SQLException
     * @version 1.0
     */
    public String agregarDemora(Demora demora, boolean sw)throws SQLException{
        return this.demorasDAO.agregarDemora( demora, sw);
    }
    
    /**
     * Actualiza el campo duracion en la tabla ingreso_trafico en la
     * planilla correspondiente.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param numpla N�mero de la planilla.
     * @param duracion Duracion de la demora.
     * @param itraf Registro del archivo ingreso_trafico
     * @throws SQLException
     */
    public String actualizarIngresoTrafico(String numpla, Integer duracion, boolean SW)throws SQLException{
        return this.demorasDAO.actualizarIngresoTrafico(numpla, duracion, SW);
    }
    /**
     * Actualiza el campo de observacion en la tabla ingreso_trafico en la
     * planilla correspondiente siempre y cuando tenda el reg_status iguala a "D".
     * @autor Ing. Andr�s Maturana De La Cruz.
     * @param numpla N�mero de la planilla.
     * @param observacion Observacion de la demora.
     * @param itraf Registro del archivo ingreso_trafico
     * @throws SQLException
     */
    public String actualizarIngresoTrafico(String numpla, String observacion)throws SQLException{
        return this.demorasDAO.actualizarIngresoTrafico(numpla, observacion);
    }
    
     /**
     * Verifica la existencia de una planilla en el archivo planilla. 
     * @autor Andr�s Maturana
     * @param numpla N�mero de la planilla.
     * @see com.tsp.operation.model.DAOS.DemorasDAO#existePlanilla(String)
     * @throws SQLException
     * @return true si existe o false sino.
     * @version 1.0
     */
    public boolean existeDespachoManual(String numpla) throws java.sql.SQLException{        
        return this.demorasDAO.existeDespachoManual(numpla);        
    }

    
}
