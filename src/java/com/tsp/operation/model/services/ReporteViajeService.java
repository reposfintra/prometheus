
/***************************************************************************
 * Nombre clase : ............... ReporteEgresoService.java                *
 * Descripcion :................. Clase que maneja los Servicios           *
 *                                asignados a Model relacionados con el    *
 *                                programa de generacion del reporte de    *
 *                                viajes                                   *
 * Autor :....................... Ing. Juan Manuel Escandon Perez          *
 * Fecha :........................ 16 de noviembre de 2005, 11:19 AM       *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.util.*;

public class ReporteViajeService {
    private ReporteViajeDAO    RVDataAccess;
    private List               ListaRV;
    private ReporteViaje       Datos;
    /** Creates a new instance of ReporteViajeService */
    public ReporteViajeService() {
        RVDataAccess = new ReporteViajeDAO();
        ListaRV = new LinkedList();
        Datos = new ReporteViaje();
    }
    
    
    /**
     * Metodo ListReporteViaje, lista todos los viajes correspondientes para uan placa
     * entre rango un rango de fechas
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String placa , String fecha inicial , String fecha final
     * @see   : listReporteViaje - ReporteViajeDAO
     * @version : 1.0
     */
    public void listReporteViaje(String placa, String fechaInicial, String fechaFinal) throws Exception {
        try{
            this.ReiniciarListaRV();
            this.ListaRV = RVDataAccess.listReporteViaje(placa, fechaInicial, fechaFinal);
        }
        catch(Exception e){
            throw new Exception("Error en listReporteViaje [ReporteViajeService]...\n"+e.getMessage());
        }
    }
    
    
     /**
     * Metodo ReiniciarDato, le asigna el valor null al atributo Datos,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public void ReiniciarDato(){
        this.Datos = null;
    }
    
    /**
     * Metodo ReiniciarListaRE, le asigna el valor null al atributo ListaRV,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public void ReiniciarListaRV(){
        this.ListaRV = null;
    }
    
   
    /**
     * Metodo getDato, retorna el valor del atributo Datos,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public ReporteViaje getDato(){
        return this.Datos;
    }
    
    /**
     * Metodo getListRE, retorna el valor del atributo ListaRV,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public List getListRV(){
        return this.ListaRV;
    }
    
}
