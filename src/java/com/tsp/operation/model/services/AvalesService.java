/*
 * AvalesService.java
 * Created on 1 de mayo de 2009, 15:30
 */
package com.tsp.operation.model.services;
import com.tsp.operation.model.DAOS.AvalesDAO;
import java.util.ArrayList;
// @author  Fintra
public class AvalesService { 
    AvalesDAO avalesDAO;
    public AvalesService() {
        avalesDAO = new AvalesDAO();
    }    
    public AvalesService(String dataBaseName) {
        avalesDAO = new AvalesDAO(dataBaseName);
    }    
    
    public ArrayList searchAvales(String fechaIni, String fechaFin,String numAval) throws Exception
    {
        return avalesDAO.searchAvales(fechaIni, fechaFin,numAval);
    }
    public ArrayList getAvales() throws Exception
    {
        return avalesDAO.getAvales();
    }
    public String aprobarAvales(String[] avales,String userx) throws Exception
    {
        return avalesDAO.aprobarAvales(avales, userx);
    }
}
