/******************************************************************
* Nombre ......................ClasificacionPlacaService.java
* Descripci�n..................Clase Service para Clasificaci�n de placas
* Autor........................Armando Oviedo
* Fecha........................17/01/2006
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.services;

import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
/**
 *
 * @author  Armando Oviedo
 */
public class ClasificacionPlacaService {
    
    private ClasificacionPlacaDAO clasificacionPlacaDAO;
    
    /** Creates a new instance of ClasificacionPlacaService */
    public ClasificacionPlacaService() {
        clasificacionPlacaDAO = new ClasificacionPlacaDAO();
    }
    
    /**
     * M�todo que actualiza y coloca una clasificacion a la tabla placa
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     * @see.........clasificacionPlacaDAO
     **/ 
    public void actualizarPlaca() throws SQLException{
        clasificacionPlacaDAO.actualizarPlaca();
    }
}
