/********************************************************************
 *      Nombre Clase.................   ImpoExpoService.java
 *      Descripci�n..................   Manejo de del DAO ImpoExpoDAO
 *      Autor........................   David Pi�a Lopez
 *      Fecha........................   1 de agosto de 2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 */
package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  David
 */
public class ImpoExpoService {
    ImpoExpoDAO dao;
    
    /** Creates a new instance of ImpoExpoService */
    public ImpoExpoService() {
        dao = new ImpoExpoDAO();
    }
    public java.util.Vector getVector() {
        return dao.getVector();
    }
    public void setVector(java.util.Vector vector) {
        dao.setVector( vector );
    }
     /**
     *M�todo que obtiene las importaciones o exportaciones de un cliente dado
     *@param cliente El codigo del cliente
     *@param tipo el tipo documento
     *@autor: David Pi�a     
     *@throws: En caso de que un error de base de datos ocurra.     
     */
    public void obtenerImpoExpoCliente( String distrito, String cliente, String tipo, String codigo, String pendiente, String sin_cdr, String inicio, String fin ) throws SQLException{
        dao.obtenerImpoExpoCliente( distrito, cliente, tipo, codigo, pendiente, sin_cdr, inicio, fin );
    }
    /**
     *M�todo que obtiene las informacion para el reporte de importaciones o exportaciones
     *de materia prima.
     *@param cliente El codigo del cliente
     *@param tipo el tipo documento
     *@autor: David Pi�a     
     *@throws: En caso de que un error de base de datos ocurra.     
     */
    public void obtenerImpoExpoRemesas( String dstrct, String tipo, String documento ) throws SQLException{
        dao.obtenerImpoExpoRemesas( dstrct, tipo, documento );
    }
    /**
     *M�todo que obtiene el valor total de la remesa en la factura detallada
     *@param dstrct Distrito
     *@param cliente El codigo del cliente
     *@param tipo el tipo documento
     *@return el valor de la sumatoria
     *@autor: David Pi�a     
     *@throws: En caso de que un error de base de datos ocurra.     
     */
    public double obtenerSumatoriaFacturasRemesa( String dstrct, String tipo, String documento ) throws SQLException{
        return dao.obtenerSumatoriaFacturasRemesa( dstrct, tipo, documento );
    }
    /**
     *M�todo que actualiza el usuario y fecha de generacion de impo_expo
     *@param dstrct Distrito
     *@param cliente El codigo del cliente
     *@param tipo el tipo documento     
     *@param usuario El usuario de generacion
     *@autor: David Pi�a     
     *@throws: En caso de que un error de base de datos ocurra.     
     */
    public void actualizarGeneracion( String dstrct, String tipo, String documento, String usuario ) throws SQLException{
        dao.actualizarGeneracion( dstrct, tipo, documento, usuario );
    }
    
     /**
     *M�todo que obtiene las importaciones o exportaciones de un cliente dado
     *@param cliente El codigo del cliente
     *@param tipo el tipo documento
     *@autor Ing. Andr�s Maturana     
     *@throws En caso de que un error de base de datos ocurra.     
     */
    public void obtenerRegistroCliente( String cliente, String dstrct ) throws SQLException{
        dao.obtenerRegistroCliente(cliente, dstrct);
    }
    
    /**
     *M�todo que obtiene las informacion para el reporte exportaciones
     *de materia prima.
     *@param dstrct Distrito
     *@param cliente El codigo del cliente
     *@param tipo el tipo documento
     *@autor Ing. Andr�s Maturana
     *@throws En caso de que un error de base de datos ocurra.     
     */
    public void obtenerExpoCliente( String dstrct, String tipo, String documento ) throws SQLException{
        dao.obtenerExpoCliente(dstrct, tipo, documento);
    }
    /**********************************************************
     * Metodo para obtener las importaciones que no se les ha 
     *        grabado fecha de llegada o salida CDR
     *@autor: Ing. Osvaldo P�rez
     *@throws: En caso de que un error de base de datos ocurra.          
     **********************************************************/
    public void obtenerImpoSinFecha( String dstrct ) throws SQLException{
        dao.obtenerImpoSinFecha( dstrct );
    }
    
     /**********************************************************
     * Metodo para obtener todas las importaciones o exportaciones
     *@param: String dstrct distrito del usuario
     *@param: String tipo 'IMP' � 'EXP'
     *@autor: Ing. Osvaldo P�rez
     *@throws: En caso de que un error de base de datos ocurra.
     **********************************************************/
    public void obtenerTodasImpoExpo( String dstrct, String tipo ) throws SQLException{
        dao.obtenerTodasImpoExpo(dstrct, tipo);
    }

     /**********************************************************
     * Metodo para obtener la importacion con el codigo dado
     *@param: String dstrct distrito del usuario
     *@param: String tipo 'IMP' � 'EXP'
     *@autor: Ing. Osvaldo P�rez
     *@throws: En caso de que un error de base de datos ocurra.
     **********************************************************/
    public void obtenerImportacion( String dstrct, String codigo ) throws SQLException{
        dao.obtenerImportacion(dstrct, codigo);
    }
    
    /**
     * Getter for property imp.
     * @return Value of property imp.
     */
    public com.tsp.operation.model.beans.ImpoExpo getImp() {
        return dao.getImp();
    }
    
    /**
     * Setter for property imp.
     * @param imp New value of property imp.
     */
    public void setImp(com.tsp.operation.model.beans.ImpoExpo imp) {
        dao.setImp(imp);
    }
    
     /**********************************************************
     * Metodo para grabnar las fechas CDR de la importacion
     *@param: String dstrct distrito del usuario
     *@param: String tipo 'IMP' � 'EXP'
     *@param: String llegada fecha de llegada al centro de distribucion
     *@param: String salida fecha de salida del centro de distribucion
     *@autor: Ing. Osvaldo P�rez
     *@throws: En caso de que un error de base de datos ocurra.
     **********************************************************/
    public void grabarFechasCDR( String dstrct, String codigo, String llegada, String salida ) throws SQLException{
        dao.grabarFechasCDR(dstrct, codigo, llegada, salida);
    }
    
    public Vector reporteImportacion( String impo, String usuario, String dstrct) throws Exception{
        return dao.reporteImportacion(impo, usuario, dstrct);
    }
    
    /**
     *M�todo verifica si existe o no un documento de exportaci�n.
     *@param dstrct Distrito
     *@param doc N�mero del documento de exportaci�n.
     *@autor Ing. Andr�s Maturana
     *@throws En caso de que un error de base de datos ocurra.     
     */
    public boolean existeDocExpo( String dstrct, String doc ) throws SQLException{
        return dao.existeDocExpo(dstrct, doc);
    }
    
    
}
