/***********************************************************************************
 * Nombre clase : ............... DespachoManualService.java                            *
 * Descripcion :................. Clase que instancia los metodos de CXPDocDAO          *
 *                                con la BD.                                            *
 * Autor :....................... Ing. David Lamadrid                                   *
 * Fecha :....................... 15 de noviembre de 2005, 11:02 AM                     *
 * Version :..................... 1.0                                                   *
 * Copyright :................... Fintravalores S.A.                               *
 ***********************************************************************************/
package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.sql.*;
import java.io.*;

public class CXPDocService {

    public CXPDocDAO dao = new CXPDocDAO();
    /** Creates a new instance of CXPDocService */
    public boolean enproceso;
    private Vector agencias;
    private List documentos;
    private List listHC;
    private Vector facturas;
    private Vector facturas_ret;

    /** Creates a new instance of CXPDocService */
    public CXPDocService() {
        this.enproceso = false;
    }
    public CXPDocService(String dataBaseName) {
        dao = new CXPDocDAO(dataBaseName);
        this.enproceso = false;
    }

    /**
     * Getter for property enproceso.
     * @return Value of property enproceso.
     */
    public boolean getEnproceso() {
        return enproceso;
    }

    /**
     * Setter for property enproceso.
     * @param enproceso New value of property enproceso.
     */
    public void setEnproceso() {
        this.enproceso = (enproceso == false) ? true : false;
    }

    /**
     * Metodo: getNumero, convierte un String en en double.
     * @autor : Ing. david Lamadrid
     * @param : String
     * @version : 1.0
     */
    public double getNumero(String numero) {
        double n = 0;
        String cadena = "";
        if (!numero.equals("")) {
            String[] campos = numero.split(",");
            for (int i = 0; i < campos.length; i++) {
                cadena = cadena + campos[i];
            }
//            n = Double.parseDouble(cadena);

            try {
                n = Double.parseDouble(cadena);
            } catch (java.lang.NumberFormatException e) {
                n = 0;
            }
        } else {
            n = 0;
        }
        return n;
    }

    /*Anular*/
    /* ANULAR FACTURA */
    /**
     * Metodo:          ExisteCXP_Items_Doc
     * Descriocion :    Funcion publica que obtiene el metodo ExisteCXP_Items_Doc del DAO
     * @autor :         LREALES
     * @param :         distrito, proveedor, tipo de documento y documento
     * @return:         boo ( retorna un boolean )
     * @version :       1.0
     */
    public boolean ExisteCXP_Items_Doc(String distrito, String proveedor, String tipo_documento, String documento) throws SQLException {
        return dao.ExisteCXP_Items_Doc(distrito, proveedor, tipo_documento, documento);

    }

    /**
     * Metodo:          ExisteCXP_Imp_Doc
     * Descriocion :    Funcion publica que obtiene el metodo ExisteCXP_Imp_Doc del DAO
     * @autor :         LREALES
     * @param :         distrito, proveedor, tipo de documento y documento
     * @return:         boo ( retorna un boolean )
     * @version :       1.0
     */
    public boolean ExisteCXP_Imp_Doc(String distrito, String proveedor, String tipo_documento, String documento) throws SQLException {
        return dao.ExisteCXP_Imp_Doc(distrito, proveedor, tipo_documento, documento);

    }

    /**
     * Metodo:          ExisteCXP_Imp_Item
     * Descriocion :    Funcion publica que obtiene el metodo ExisteCXP_Imp_Item del DAO
     * @autor :         LREALES
     * @param :         distrito, proveedor, tipo de documento y documento
     * @return:         boo ( retorna un boolean )
     * @version :       1.0
     */
    public boolean ExisteCXP_Imp_Item(String distrito, String proveedor, String tipo_documento, String documento) throws SQLException {
        return dao.ExisteCXP_Imp_Item(distrito, proveedor, tipo_documento, documento);

    }

    /**
     * Metodo:          AnularCXP_Items_Doc
     * Descriocion :    Funcion publica que obtiene el metodo AnularCXP_Items_Doc del DAO
     * @autor :         LREALES
     * @param :         cXP_Doc ( documento de cuentas por pagar )
     * @return:         -
     * @version :       1.0
     */
    public void AnularCXP_Items_Doc(CXP_Doc cXP_Doc) throws SQLException {

        dao.AnularCXP_Items_Doc(cXP_Doc);

    }

    /**
     * Metodo:          AnularCXP_Imp_Doc
     * Descriocion :    Funcion publica que obtiene el metodo AnularCXP_Imp_Doc del DAO
     * @autor :         LREALES
     * @param :         cXP_Doc ( documento de cuentas por pagar )
     * @return:         -
     * @version :       1.0
     */
    public void AnularCXP_Imp_Doc(CXP_Doc cXP_Doc) throws SQLException {

        dao.AnularCXP_Imp_Doc(cXP_Doc);

    }

    /**
     * Metodo:          AnularCXP_Imp_Item
     * Descriocion :    Funcion publica que obtiene el metodo AnularCXP_Imp_Item del DAO
     * @autor :         LREALES
     * @param :         cXP_Doc ( documento de cuentas por pagar )
     * @return:         -
     * @version :       1.0
     */
    public void AnularCXP_Imp_Item(CXP_Doc cXP_Doc) throws SQLException {

        dao.AnularCXP_Imp_Item(cXP_Doc);

    }
    /*Fin Anular*/

    /**
     * Metodo que instancia un Objeto tipo CXDOC en CXPDocDAO
     * @autor.......David Lamadrid
     * @param.......CXPDoc factura(objeto de CXPDoc)
     * @see.........CXPDocDAO.class
     * @throws......Clase no encontrada.
     * @version.....1.0.
     * @return.......
     */
    public void setFactura(com.tsp.operation.model.beans.CXP_Doc factura) {
        dao.setFactura(factura);
    }

    /**
     * Metodo que retorna un boolean para verificar la existencia de un Documento por pagar en el sistema
     * retorna true si existe y false si no existe.
     * @autor.......David Lamadrid
     * @param.......String dis(Distrito),String proveedor,String tipoDoc,String docuemnto.
     * @see.........CXPDocDAO.class
     * @throws.......
     * @version.....1.0.
     * @return......boolean.
     */
    public boolean existeDoc(String dis, String proveedor, String tipoDoc, String documento) throws SQLException {
        return dao.existeDoc(dis, proveedor, tipoDoc, documento);
    }

    /**
     * Metodo UpdateFactura: Actualiza los campos de la factua
     * @autor.......Ivan Dario Gomez
     * @param.......String distrito, String codigo del proveedor y numero de la factura
     * @see.........
     * @throws.......Exception
     * @version.....1.0.
     */
    public void UpdateFactura(CXP_Doc doc, Vector vItems, Vector vImpuestosDoc, String agencia) throws Exception {
        dao.UpdateFactura(doc, vItems, vImpuestosDoc, agencia);

    }

    /**
     * Metodo que Inserta un Documento por pagar en el Sistema .
     * @autor.......David Lamadrid
     * @param.......CXDoc doc(Objeto tipo CXPDoc), Vector vItems(Vector de objetos CxPItem),Vector vImpuestos(Vector de Objetos CXPImpDoc).
     * @see.........CXPDocDAO.class
     * @throws......Existe Documento en el Sistema.
     * @version.....1.0.
     * @return.......
     */
    public void insertarCXPDoc(CXP_Doc doc, Vector vItems, Vector vImpuestosDoc, String agencia) throws Exception {
        dao.insertarCXPDoc(doc, vItems, vImpuestosDoc, agencia);
    }

    /**
     * Metodo que guarda un objeto tipo CXP_Doc en un Archivo
     * @autor.......David Lamadrid
     * @param.......CXP_Doc factura
     * @see........
     * @throws.......IOException
     * @version.....1.0.
     * @return......Vector vImpuestos.
     */
    public void escribirArchivo(CXP_Doc factura, Vector items, String maxfila, String nombreArchivo, String user) throws IOException {
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String path = rb.getString("ruta");
        String Ruta = path + "/exportar/migracion/" + user;
        String RutaA = Ruta + "/" + nombreArchivo;

        File archivo = new File(Ruta);
        archivo.mkdirs();
        File archivoR = new File(RutaA);

        FileOutputStream salidaArchivoR = new FileOutputStream(archivoR);

        ObjectOutputStream salidaObjetoR = new ObjectOutputStream(salidaArchivoR);

        salidaObjetoR.writeObject(factura);
        salidaObjetoR.writeObject(items);
        salidaObjetoR.writeObject(maxfila);

        salidaObjetoR.close();


    }

    /**
     * Metodo: BorrarArchivo, permite obtener un objeto de registros de CXP_doc.
     * @autor : Ing. DAvid Lamidrid
     * @param : String Nombre del archivo, String Usuario
     * @version : 1.0
     */
    public boolean BorrarArchivo(String nombreArchivo, String user) throws IOException, ClassNotFoundException {
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String path = rb.getString("ruta");
        String Ruta = path + "/exportar/migracion/" + user + "/" + nombreArchivo;
        File archivo = new File(Ruta);
        return archivo.delete();
    }

    /**
     * m�todo obtenerNumeroFXP , Busca la factura por proveedor
     * @param String Proveedor
     * @throws
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public void obtenerNumeroFXP(String proveedor) throws SQLException {
        dao.obtenerNumeroFXP(proveedor);
    }

    /**
     * Metodo que Lee  un objeto tipo CXP_Doc en un Archivo
     * @autor.......David Lamadrid
     * @param.......String nombreArchivo,String user;
     * @see........
     * @throws.......IOException
     * @version.....1.0.
     * @return......Objeto tipo CXP_Doc.
     */
    public CXP_Doc leerArchivo(String nombreArchivo, String user) throws IOException, ClassNotFoundException {
        CXP_Doc factura;
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String path = rb.getString("ruta");
        String Ruta = path + "/exportar/migracion/" + user + "/" + nombreArchivo;
        File archivo = new File(Ruta);
        FileInputStream entradaArchivo = new FileInputStream(archivo);
        ObjectInputStream entradaObjeto = new ObjectInputStream(entradaArchivo);
        try {
            factura = (CXP_Doc) entradaObjeto.readObject();
        } catch (ClassNotFoundException e) {
            factura = null;
            //System.out.println("Error no se ha encontrado la clase");
        }
        entradaObjeto.close();
        return factura;
    }

    /**
     * Metodo que Lee  un objeto tipo Vector en un Archivo
     * @autor.......David Lamadrid
     * @param.......String nombreArchivo,String user;
     * @see........
     * @throws.......IOException
     * @version.....1.0.
     * @return......Objeto tipo Vector con los Items de una factura.
     */
    public Vector leerArchivoItems(String nombreArchivo, String user) throws IOException, ClassNotFoundException {
        Vector items;
        CXP_Doc factura;
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String path = rb.getString("ruta");
        String Ruta = path + "/exportar/migracion/" + user + "/" + nombreArchivo;
        File archivo = new File(Ruta);
        FileInputStream entradaArchivo = new FileInputStream(archivo);
        ObjectInputStream entradaObjeto = new ObjectInputStream(entradaArchivo);
        try {
            factura = (CXP_Doc) entradaObjeto.readObject();
            items = (Vector) entradaObjeto.readObject();
        } catch (ClassNotFoundException e) {
            items = null;
            //System.out.println("Error no se ha encontrado la clase");
        }
        entradaObjeto.close();
        return items;
    }

    /**
     * Metodo BuscarFactura: busca el encabezado de la factura
     * @autor.......Ivan Dario Gomez
     * @param.......String distrito, String codigo del proveedor y numero de la factura
     * @see.........BuscarFactura - CXPDocDAO
     * @throws.......Exception
     * @version.....1.0.
     * @return......Objeto tipo CXP_Doc.
     */
    public CXP_Doc BuscarFactura(String dstrct, String proveedor, String documento, String tipo_doc) throws Exception {
        dao.BuscarFactura(dstrct, proveedor, documento, tipo_doc);
        return dao.getFactura();
    }

    /**
     * Metodo BuscarItemFactura: busca los Item de la factura
     * @autor.......Ivan Dario Gomez
     * @param.......String distrito, String codigo del proveedor y numero de la factura
     * @see.........BuscarItemFactura - CXPDocDAO
     * @throws.......Exception
     * @version.....1.0.
     * @return......Vector de Item.
     */
    public Vector BuscarItemFactura(CXP_Doc factura, Vector vTipImpuestos) throws Exception {
        return dao.BuscarItemFactura(factura, vTipImpuestos);

    }

    /**
     * Metodo:          ConsultarCXP_Doc
     * Descriocion :    Funcion publica que obtiene el metodo ConsultarCXP_Doc del DAO
     * @autor :         Leonardo Parody
     * @modificado por: LREALES - mayo del 2006
     * @param :         distrito y documento
     * @return:         cXP_Doc ( retorna un Objeto )
     * @version :       1.0
     */
    public CXP_Doc ConsultarCXP_Doc(String documento, String distrito) throws SQLException {
        return dao.ConsultarCXP_Doc(documento, distrito);

    }

    /**
     * Metodo reporteFacturasXLS, obtiene las facturas creadas segun parametros ingresados
     * @autor  Ing. Jose de la Rosa
     *          modificado Ing Sandra Escalante
     * @see reporteFacturasXLS(String dstrct, String agencia, String fchi, String fchf)
     * @param  distrito y agencia del usuario en sesion (String), fecha inicial y final
     *          de realizacion del proceso
     * @return List
     * @version 2.0
     */
    public void reporteFacturasXLS(String dstrct, String agencia, String fchi, String fchf) throws SQLException {
        try {
            dao.reporteFacturasXLS(dstrct, agencia, fchi, fchf);
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }

    /**
     * Getter for property facturas.
     * @return Value of property facturas.
     */
    public List getFacturas() throws SQLException {
        return dao.getFacturas();
    }

    public Vector getVectorFacturas() {
        return dao.vectorFacturas();
    }
    /* public void sumarNumeroObsAutorizador(CXPItemDoc item) throws SQLException {
    dao.actualizarNumeroObsAutorizador("SUMAR",item.getDstrct(), item.getProveedor(), item.getTipo_documento(), item.getDocumento());
    }
    public void restarNumeroObsAutorizador(CXPItemDoc item) throws SQLException {
    dao.actualizarNumeroObsAutorizador("RESTAR",item.getDstrct(), item.getProveedor(), item.getTipo_documento(), item.getDocumento());
    }
    public void sumarNumeroObsPagadorRegistrador(CXPItemDoc item,String usuario) throws SQLException {
    dao.actualizarNumeroObsPagadorRegistrador("SUMAR",usuario,item.getDstrct(), item.getProveedor(), item.getTipo_documento(), item.getDocumento());
    }
    public void autorizarCuentaPorPagar(CXP_Doc cuenta) throws SQLException {
    dao.autorizarCuentaPorPagar(cuenta);
    }*/

    /**
     * Metodo que retorna un Vector de  Objetos tipo CXDOC en CXPDocDAO
     * @autor.......David Lamadrid
     * @param.......ninguno.
     * @see.........CXPDocDAO.class
     * @throws......Clase no encontrada.
     * @version.....1.0.
     * @return.......Vector vCXPDoc.
     */
    public java.util.Vector getVecCxp_doc() {
        return dao.getVecCxp_doc();
    }

    /**
     * Metodo que setea un Vector de  Objetos tipo CXDOC en CXPDocDAO
     * @autor.......David Lamadrid
     * @param.......Vector vecCxp_doc(Vector de objetos de CXPDoc)
     * @see.........CXPDocDAO.class
     * @throws......Clase no encontrada.
     * @version.....1.0.
     * @return......Vector de Objetos CXPDoc
     */
    public void setVecCxp_doc(java.util.Vector vecCxp_doc) {
        dao.setVecCxp_doc(vecCxp_doc);
    }

    /**
     * Metodo que retorna un Objeto tipo CXDOC en CXPDocDAO
     * @autor.......David Lamadrid
     * @param.......ninguno
     * @see.........CXPDocDAO.class
     * @throws......Clase no encontrada.
     * @version.....1.0.
     * @return......CXPDoc.
     */
    public com.tsp.operation.model.beans.CXP_Doc getFactura() {
        return dao.getFactura();
    }

    /**
     * Metodo que retorna un String con fecha generada de una fecha inicial y un numero de dias
     * @autor.......David Lamadrid
     * @param.......ninguno
     * @see.........CXPDocDAO.class
     * @throws.......
     * @version.....1.0.
     * @return......String fechaFinal.
     */
    public String fechaFinal(String fechai, int n) {
        return dao.fechaFinal(fechai, n);
    }

    /**
     * Metodo que devuele el valor de cambio dado dos monedas .
     * @autor.......David Lamadrid
     * @param.......String dis(Distrito),String proveedor,String tipoDoc,String docuemnto.
     * @see.........CXPDocDAO.class
     * @throws.......
     * @version.....1.0.
     * @return......float valor.
     */
    public float buscarValor(String moneda1, String moneda2, String fecha, float valor) throws SQLException {
        //System.out.println("entra al buscar valor en servicio ");
        return dao.buscarValor(moneda1, moneda2, fecha, valor);
    }

    /**
     * Metodo inicialisa la Lista de Docuemntos por pagar en CXPDocDAO .
     * @autor.......David Lamadrid
     * @param.......ninguna
     * @see.........CXPDocDAO.class
     * @throws......No Existen Documentos en el Sistema.
     * @version.....1.0.
     * @return.......
     */
    public void listaDoc() throws SQLException {
        dao.listaDoc();
    }

    /**
     * Metodo que retorna un String con Informacion de la Diferencia entre dos fechas.
     * @autor.......David Lamadrid
     * @param.......ninguna
     * @see.........CXPDocDAO.class
     * @throws......Fecha Invalida.
     * @version.....1.0.
     * @return......String resultado(Diferencia de fechas)
     */
    public String getDiferencia(java.util.Date fecha1, java.util.Date fecha2) {
        java.util.Date fechaMayor = null;
        java.util.Date fechaMenor = null;
        Map resultadoMap = new HashMap();

        /* Verificamos cual es la mayor de las dos fechas, para no tener sorpresas al momento
         * de realizar la resta.
         */
        if (fecha1.compareTo(fecha2) > 0) {
            fechaMayor = fecha1;
            fechaMenor = fecha2;
        } else {
            fechaMayor = fecha2;
            fechaMenor = fecha1;
        }

        //los milisegundos
        long diferenciaMils = fechaMayor.getTime() - fechaMenor.getTime();

        //obtenemos los segundos
        long segundos = diferenciaMils / 1000;

        //obtenemos las horas
        long horas = segundos / 3600;

        //restamos las horas para continuar con minutos
        segundos -= horas * 3600;

        //igual que el paso anterior
        long minutos = segundos / 60;
        segundos -= minutos * 60;

        //ponemos los resultados en un mapa :-)
        resultadoMap.put("horas", Long.toString(horas));
        resultadoMap.put("minutos", Long.toString(minutos));
        resultadoMap.put("segundos", Long.toString(segundos));
        String resultado = "";
        resultado = " Horas " + horas + " minutos " + minutos + " segundos" + segundos;
        //return resultadoMap;
        return resultado;
    }

    /**
     * Metodo que retorna un long con el numero de dias que existe entre una fecha inicial y una fecha final.
     * @autor.......David Lamadrid
     * @param.......fecha1 fecha Inicial,fecha2 Fecha Final type Timestamp.
     * @see.........CXPDocDAO.class
     * @throws......Fecha Invalida.
     * @version.....1.0.
     * @return......long resultado (numero de dias)
     */
    public long getDiasDiferencia(java.sql.Timestamp fecha1, java.sql.Timestamp fecha2) {
        long resultado = 0;
        resultado = ((fecha1.getTime() - fecha2.getTime()) / 1000) / 86400;
        return resultado;
    }

    /**
     * Metodo que retorna un Vector con los nombres de proveedores dado un String[] con los proveedores
     * sepearados por |;
     * @autor.......David Lamadrid
     * @param.......String []linea(lista de proveedores) .
     * @see.........CXPDocDAO.class
     * @throws......String[] linea invalido.
     * @version.....1.0.
     * @return......Vector campos(Vector de String con informacion de los Proveedores)
     */
    public Vector obtenerComboProveedores(String[] linea) {
        Vector campos = new Vector();
        for (int i = 0; i < linea.length; i++) {
            StringTokenizer separados = new StringTokenizer(linea[i], "|");

            // Agrego cada uno de los campos al vector y luego lo retorno
            Vector fila = new Vector();
            while (separados.hasMoreTokens()) {
                fila.addElement(separados.nextToken());
            }
            campos.add(fila);
        }
        return campos;
    }

    /**
     * Metodo que retorna un Vector con los nombres de proveedores dado un String con los proveedores
     * sepearados por |;
     * @autor.......David Lamadrid
     * @param.......String linea(lista de proveedores) .
     * @see.........CXPDocDAO.class
     * @throws......String[] linea invalido.
     * @version.....1.0.
     * @return......Vector campos(Vector de String con informacion de los Proveedores)
     */
    public Vector obtenerComboProveedores1(String linea) {
        Vector campos = new Vector();
        String campo = "";
        int pos = 0;
        //System.out.println("linea antes del while  "+linea);
        while (linea.length() > 1) {
            pos = linea.indexOf("|");
            campo = linea.substring(0, pos);
            //System.out.println("campo " + campo);
            campos.add(campo);
            linea = linea.substring(pos + 1, linea.length());
            //System.out.println("linea "+linea);
        }
        return campos;
    }

    /**
     * Metodo que retorna el nombre de un documento dado el tipo de docuemnto
     * @autor.......David Lamadrid
     * @param.......String tipo.
     * @see.........CXPDocDAO.class
     * @throws......no existe tipo de docuemto.
     * @version.....1.0.
     * @return......String nombreDocumento
     */
    public String nombreDocumento(String tipo) throws SQLException {
        return dao.nombreDocumento(tipo);
    }

    /**
     * Metodo que setea un Vector de objetos CXPDoc daodo el proveedor ,banco y suscursal
     * @autor.......David Lamadrid
     * @param.......String id,String banco,String sucursal.
     * @see.........CXPDocDAO.class
     * @throws......No Existen Objetos con los paramaetros de entrada
     * @version.....1.0.
     * @return.......
     */
    public void listaDocPorProveedor(String id, String banco, String sucursal) throws SQLException {
        dao.listaDocPorProveedor(id, banco, sucursal);
    }

    /**
     * Metodo que guarda un objeto tipo CXP_Doc en un Archivo
     * @autor.......David Lamadrid
     * @param.......CXP_Doc factura
     * @see........
     * @throws.......IOException
     * @version.....1.0.
     * @return......Vector vImpuestos.
     */
    public void escribirArchivo(CXP_Doc factura, Vector items, String nombreArchivo, String user) throws IOException {
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String path = rb.getString("ruta");
        String Ruta = path + "/exportar/migracion/" + user + "/" + nombreArchivo;
        File archivo = new File(Ruta);
        FileOutputStream salidaArchivo = new FileOutputStream(archivo);
        ObjectOutputStream salidaObjeto = new ObjectOutputStream(salidaArchivo);
        salidaObjeto.writeObject(factura);
        salidaObjeto.writeObject(items);
        salidaObjeto.close();
        archivo.mkdirs();
    }

    /**
     * Metodo: obtenerCamposPlanilla, permite obtener los campos de un planilla
     * @autor : Ing. Jose de la rosa
     * @param : numero de planilla.
     * @see obtenerCamposPlanilla - CXPDocDAO
     * @version : 1.0
     */
    public void obtenerCamposPlanilla(String planilla) throws SQLException {
        try {
            dao.obtenerCamposPlanilla(planilla);
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }

    /**
     * Metodo ingresarCXP_doc, permite ingrear una factura
     * recibe por parametro
     * @autor : Ing. Jose de la rosa
     * @param :
     * @see ingresarCXP_doc - CXPDocDAO
     * @version : 1.0
     */
    public void ingresarCXP_doc(CXP_Doc acuerdo) throws SQLException {
        try {
            dao.setCXP_doc(acuerdo);
            dao.ingresarCXP_doc();
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }

    /**
     * Metodo: getCXP_doc, permite retornar un objeto de registros de CXP_doc.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @see getCXP_doc - CXPDocDAO
     * @version : 1.0
     */
    public CXP_Doc getCXP_doc() {
        return dao.getCXP_doc();
    }

    /**
     * Metodo: setCXP_doc, permite obtener un objeto de registros de CXP_doc.
     * @autor : Ing. Jose de la rosa
     * @param : objeto
     * @see setCXP_doc - CXPDocDAO
     * @version : 1.0
     */
    public void setCXP_doc(CXP_Doc acuerdo) {
        dao.setCXP_doc(acuerdo);
    }

    public String formatearNumero(String n) {
        // para estar seguros que vamos a trabajar con un string
        String valor = "" + n;
        String res = "";
        int i = 0;
        int j = 0;
        for (i = valor.length() - 1, j = 0; i >= 0; i--, j++) {
            if (j % 3 == 0 && j != 0) {
                res += ",";
            }
            res += valor.charAt(i);
        }
        //ahora nos toca invertir el numero;
        String aux = "";
        for (i = res.length() - 1; i >= 0; i--) {
            aux += res.charAt(i);
        }
        return aux;
    }

    public java.util.Vector getVFacturas() {
        return dao.getVFacturas();
    }
    private TreeMap codCliAre = new TreeMap();

    //juan  02-05-2006
    public TreeMap llenarCodCliAre() {
        this.codCliAre.put("C", "C");
        return codCliAre;
    }

    /**
     * Getter for property codCliAre.
     * @return Value of property codCliAre.
     */
    public java.util.TreeMap getCodCliAre() {
        return codCliAre;
    }

    /**
     * Setter for property codCliAre.
     * @param codCliAre New value of property codCliAre.
     */
    public void setCodCliAre(java.util.TreeMap codCliAre) {
        this.codCliAre = codCliAre;
    }

    /**
     * Metodo inicialisa la Lista de Docuemntos por pagar en CXPDocDAO .
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param proveedor NIT del proveedor
     * @agc C�digo de la agencia del usuario
     * @throws SQLException si ocurre un error en la cone�i�n con la Base de Datos
     * @version 1.0.
     */
    public void facturasNoPagadas(String proveedor, String agc) throws SQLException {
        dao.facturasNoPagadas(proveedor, agc);
    }

    /**
     * Metodo:          ExisteCXP_Doc
     * Descriocion :    Funcion publica que obtiene el metodo ExisteCXP_Doc del DAO
     * @autor :         Leonardo Parody
     * @modificado por: LREALES - mayo del 2006
     * @param :         distrito, proveedor, tipo de documento y documento
     * @return:         boo ( retorna un boolean )
     * @version :       1.0
     */
    public boolean ExisteCXP_Doc(String distrito, String proveedor, String tipo_documento, String documento) throws SQLException {
        return dao.ExisteCXP_Doc(distrito, proveedor, tipo_documento, documento);

    }

    /**
     * Metodo:          ConsultarCXP_Doc
     * Descriocion :    Funcion publica que obtiene el metodo ConsultarCXP_Doc del DAO
     * @autor :         Leonardo Parody
     * @modificado por: LREALES - mayo del 2006
     * @param :         distrito, proveedor, tipo de documento y documento
     * @return:         cXP_Doc ( retorna un Objeto )
     * @version :       1.0
     */
    public CXP_Doc ConsultarCXP_Doc(String distrito, String proveedor, String tipo_documento, String documento) throws SQLException {
        return dao.ConsultarCXP_Doc(distrito, proveedor, tipo_documento, documento);

    }

    /**
     * Metodo:          AnularCXP_Doc
     * Descriocion :    Funcion publica que obtiene el metodo AnularCXP_Doc del DAO
     * @autor :         Leonardo Parody
     * @modificado por: LREALES - mayo del 2006
     * @param :         cXP_Doc ( documento de cuentas por pagar )
     * @return:         nada.
     * @version :       1.0
     */
    public void AnularCXP_Doc(CXP_Doc cXP_Doc) throws SQLException {

        dao.AnularCXP_Doc(cXP_Doc);

    }

    /**
     * Metodo:          SQL_Nombre_Proveedor
     * Descriocion :    Funcion publica que obtiene el metodo SQL_Nombre_Proveedor del DAO
     * @autor :         LREALES
     * @param :         distrito y proveedor
     * @return:         nombre ( retorna un String )
     * @version :       1.0
     */
    public String SQL_Nombre_Proveedor(String distrito, String proveedor) throws SQLException {
        return dao.SQL_Nombre_Proveedor(distrito, proveedor);

    }
    //************************* 30 junio ***************************
    //LREALES

    /**
     * Metodo: getVector_items, permite retornar un vector de registros de items.
     * @autor : LREALES
     * @param : -
     * @version : 1.0
     */
    public Vector getVector_items() throws SQLException {

        return dao.getVector_items();

    }

    /**
     * Metodo:          ConsultarCXP_Items_Doc
     * Descriocion :    Funcion publica que obtiene el metodo ConsultarCXP_Items_Doc del DAO
     * @autor :         LREALES - junio del 2006
     * @param :         distrito, proveedor, tipo_documento y documento.
     * @return:         -
     * @version :       1.0
     */
    public void ConsultarCXP_Items_Doc(String distrito, String proveedor, String tipo_documento, String documento) throws SQLException {

        dao.ConsultarCXP_Items_Doc(distrito, proveedor, tipo_documento, documento);

    }

    /**
     * Metodo: getVector_tblcon, permite retornar un vector de registros de conceptos.
     * @autor : LREALES
     * @param : -
     * @version : 1.0
     */
    public Vector getVector_tblcon() throws SQLException {

        return dao.getVector_tblcon();

    }

    /**
     * Metodo:          ConsultarTblCon
     * Descriocion :    Funcion publica que obtiene el metodo ConsultarTblCon del DAO
     * @autor :         LREALES - junio del 2006
     * @param :         un Hashtable.
     * @return:         -
     * @version :       1.0
     */
    public void ConsultarTblCon(Hashtable ht_items) throws SQLException {

        dao.ConsultarTblCon(ht_items);

    }

    /**
     * Metodo: getVector_movpla, permite retornar un vector de registros del movimiento de la planilla.
     * @autor : LREALES
     * @param : -
     * @version : 1.0
     */
    public Vector getVector_movpla() throws SQLException {

        return dao.getVector_movpla();

    }

    /**
     * Metodo:          ConsultarMovPla
     * Descriocion :    Funcion publica que obtiene el metodo ConsultarMovPla del DAO
     * @autor :         LREALES - junio del 2006
     * @param :         un Hashtable.
     * @return:         -
     * @version :       1.0
     */
    public void ConsultarMovPla(Hashtable ht_items) throws SQLException {

        dao.ConsultarMovPla(ht_items);

    }

    /**
     * Metodo:          DuplicarMovPla
     * Descriocion :    Funcion publica que obtiene el metodo DuplicarMovPla del DAO
     * @autor :         LREALES - junio del 2006
     * @param :         un Hashtable.
     * @return:         -
     * @version :       1.0
     */
    public void DuplicarMovPla(Hashtable ht_movpla, String vlr_disc_htd, String vlr_htd, String vlr_for_htd) throws SQLException {

        dao.DuplicarMovPla(ht_movpla, vlr_disc_htd, vlr_htd, vlr_for_htd);

    }

    /**
     * Metodo:          AnularMovPla
     * Descriocion :    Funcion publica que obtiene el metodo AnularMovPla del DAO
     * @autor :         LREALES - junio del 2006
     * @param :         vlr_disc_ht, vlr_ht, vlr_for_ht, user_update_ht, user_anul_ht, concept_code_ht, planilla_ht, pla_owner_ht, y creation_date_ht.
     * @return:         -
     * @version :       1.0
     */
    public void AnularMovPla(String vlr_disc_ht, String vlr_ht, String vlr_for_ht, String user_update_ht, String user_anul_ht, String concept_code_ht, String planilla_ht, String pla_owner_ht, String creation_date_ht) throws SQLException {

        dao.AnularMovPla(vlr_disc_ht, vlr_ht, vlr_for_ht, user_update_ht, user_anul_ht, concept_code_ht, planilla_ht, pla_owner_ht, creation_date_ht);

    }

    //Ivan 22 julio 2006
    /**
     * Metodo buscarCuenta , para buscar los datos de la cuenta
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : String dstrct String cuenta
     * @see     : buscarCuenta - GrabarComprobanteDAO
     * @throws  : SQLException
     * @version : 1.0
     */
    public Hashtable buscarCuenta(String dstrct, String cuenta) throws SQLException {
        try {
            return dao.buscarCuenta(dstrct, cuenta);
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }

    /**
     * Metodo ExisteABC , para verificar si existe el codigo ABC
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @throws  : SQLException
     * @version : 1.0
     */
    public boolean ExisteABC(String codigo) throws SQLException {
        try {
            return dao.ExisteABC(codigo);
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }

    /**
     * Getter for property agencias.
     * @return Value of property agencias.
     */
    public java.util.Vector getAgencias() {
        return agencias;
    }

    /**
     * Setter for property agencias.
     * @param agencias New value of property agencias.
     */
    public void setAgencias(java.util.Vector agencias) {
        this.agencias = agencias;
    }

    /**
     * Metodo obtenerAgencias , Metodo para buscar las agencias
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @throws  : SQLException
     * @version : 1.0
     */
    public void obtenerAgencias() throws SQLException {
        try {
            this.agencias = dao.obtenerAgencias();
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
    //Ivan Gomez

    /**
     * Metodo  BuscarFactObsPagador, recibe el nombre del usuario en session,
     * permite buscar las facturas con observacion para el Usuario pagador
     * @autor : Ing. Ivan Dario Gomez Vanegas
     * @param : String identificaci�n del usuario y el distrito
     * @see   : BuscarFactObsPagador - CXPDocDAO
     * @version : 1.0
     */
    public void BuscarFactObsPagador(String idusuario, String Dstrct, String Agencia, String Fecini, String Fecfin) throws SQLException {
        dao.BuscarFactObsPagador(idusuario, Dstrct, Agencia, Fecini, Fecfin);
    }

    /**
     * Metodo actualizarNumeroObsAutorizador, Actualiza el numero de la observaci�n del Autorizador
     *        mandando el Parametro Sumar 
     * @autor   : Ing. Henry A. Osorio Gonz�lez
     * @param   : recibe el beans CXPItemDoc
     * @seee    : actualizarNumeroObsAutorizador - CXPDocDAO 
     * @version : 1.0
     */
    public void sumarNumeroObsAutorizador(CXPItemDoc item) throws SQLException {
        dao.actualizarNumeroObsAutorizador("SUMAR", item.getDstrct(), item.getProveedor(), item.getTipo_documento(), item.getDocumento());
    }

    /**
     * Metodo  buscarFacturasxUsuario, recibe el nombre del usuario en session,
     * permite buscar las facturas a autorizar o autorizadas por el usuario autorizador en session
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : String identificaci�n del usuario, distrito, Agencia, fecha inicial, 
     *          fecha final y boolean para saber si es el listado de autorizadas o no autorizadas
     * @see   : buscarFacturasxUsuario - CXPDocDAO
     * @version : 1.0
     */
    public void buscarFacturasxUsuario(String idusuario, String Dstrct, String Agencia, String Fecini, String Fecfin, boolean Autorizadas) throws SQLException {
        dao.buscarFacturasxUsuario(idusuario, Dstrct, Agencia, Fecini, Fecfin, Autorizadas);
    }

    /**
     * Metodo sumarNumeroObsPagadorRegistrador, Actualiza el numero de la observaci�n del Registrador
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : String opcion, String usuario, String distrito, String proveedor, String tipo de documento, String documento
     * @see   : actualizarNumeroObsPagadorRegistrador - CXPDocDAO
     * @version : 1.0
     */
    public void sumarNumeroObsPagadorRegistrador(CXPItemDoc item) throws SQLException {
        dao.actualizarNumeroObsPagadorRegistrador("SUMAR", item.getDstrct(), item.getProveedor(), item.getTipo_documento(), item.getDocumento());
    }

    /**
     * Metodo   : autorizarCuentaPorPagar, permite autorizar el pago de la factura que
     *            recibe por parametro
     * @autor   : Ing. Henry A. Osorio Gonz�lez
     * @param   : CXP_Doc la factura a autorizar
     * @see     : autorizarCuentaPorPagar - CXPDocDAO
     * @version : 1.0
     */
    public void autorizarCuentaPorPagar(CXP_Doc cuenta) throws SQLException {
        dao.autorizarCuentaPorPagar(cuenta);
    }

    /**
     * Metodo obtenerAgencias , Metodo para buscar las agencias
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : String distrito , String proveedor , String tipo de documento
     * @see     : BuscarDocumentos - CXPDocDAO
     * @throws  : SQLException
     * @version : 1.0
     */
    public void BuscarDocumentos(String dstrct, String proveedor, String tipoDoc) throws SQLException {
        try {
            this.documentos = dao.BuscarDocumentos(dstrct, proveedor, tipoDoc);
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }

    /**
     * Getter for property documentos.
     * @return Value of property documentos.
     */
    public java.util.List getDocumentos() {
        return documentos;
    }

    /**
     * Setter for property documentos.
     * @param documentos New value of property documentos.
     */
    public void setDocumentos(java.util.List documentos) {
        this.documentos = documentos;
    }

    /**
     * Metodo TieneNotasDEB_CRE , Metodo para verificar si una factura
     * tiene notas debitos o notas creditos                        
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @throws  : SQLException
     * @version : 1.0
     */
    public boolean TieneNotasDEB_CRE(String factura) throws SQLException {
        return dao.TieneNotasDEB_CRE(factura);
    }

    /**
     * Metodo BuscarSaldoDocRel: busca el saldo del documento relacionado
     * @autor.......Ivan Dario Gomez
     * @param.......String distrito, String codigo del proveedor y numero de la factura
     * @see.........BuscarSaldoDocRel - CXPDocDAO
     * @throws.......Exception
     * @version.....1.0.
     * @return......double
     */
    public double BuscarSaldoDocRel(String dstrct, String proveedor, String documento, String tipo_doc) throws Exception {
        return dao.BuscarSaldoDocRel(dstrct, proveedor, documento, tipo_doc);

    }

    //AMATURANA
    /**
     * Muestra el detalle de la factura
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public CXP_Doc detalleFactura(String dstrct, String proveedor, String documento, String tipo_doc) throws Exception {
        dao.detalleFactura(dstrct, proveedor, documento, tipo_doc);
        return dao.getFactura();
    }

    /**
     * Metodo BuscarHC , Metodo para buscar los hc
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @throws  : SQLException
     * @version : 1.0
     */
    public void BuscarHC() throws SQLException {
        this.listHC = dao.BuscarHC();
    }

    /**
     * Getter for property listHC.
     * @return Value of property listHC.
     */
    public List getListHC() {
        return listHC;
    }

    /**
     * Setter for property listHC.
     * @param listHC New value of property listHC.
     */
    public void setListHC(java.util.List listHC) {
        this.listHC = listHC;
    }

    /**
     * Metodo para buscar los docs relacionados a una factura
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public List buscarDocsRelacionados(
            String dstrct,
            String proveedor,
            String doc) throws SQLException {
        return dao.buscarDocsRelacionados(dstrct, proveedor, doc);
    }

    /**
     * Insertar
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public String insertarCXPDocMigracion(CXP_Doc doc) throws SQLException {
        return dao.insertarCXPDocMigracion(doc);
    }

    /**
     * Insertar item
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public String insertarItemCXPDocMigracion(CXPItemDoc item) throws SQLException {
        return dao.insertarItemCXPDocMigracion(item);
    }

    /**
     * Verifica si existe la factura sin importar su estado
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public boolean existeCxPDoc(CXP_Doc doc) throws SQLException {
        return dao.existeCxPDoc(doc);
    }

    /**
     * Insertar el impuesto del item
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public String insertarImpuestoItemCXPDocMigracion(CXPImpItem impItem) throws SQLException {
        return dao.insertarImpuestoItemCXPDocMigracion(impItem);
    }

    /**
     * Insertar los impuestos del documento
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public String insertarImpuestoCXPDocMigracion(CXPImpDoc impDoc) throws SQLException {
        return dao.insertarImpuestoCXPDocMigracion(impDoc);
    }

    /**
     * Obtiene los egresos generados para la factura determinada
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct Distrito
     * @param nit Nit del proveedor
     * @param tipodoc Tipo de Documento
     * @param doc Documento
     * @throws SQLException
     * @version 1.0
     */
    public void getEgresosFactura(String dstrct, String nit, String tipodoc, String doc) throws SQLException {
        dao.getEgresosFactura(dstrct, nit, tipodoc, doc);
    }

    /**
     * Metodo existeEnPrecheque , Metodo PARA SABER SI UNA FACTURA ESTA DISPONIBLE PARA MODIFICAR,
     * SI ESTA EN PRECHEQUE_DETALLE Y EL REG_STATUS ='' NO SE PUEDE MODIFICAR LA FACTURA
     * tiene notas debitos o notas creditos
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : String dstrct,String proveedor, String tipo_doc,String factura
     * @throws  : SQLException
     * @version : 1.0
     */
    public boolean existeEnPrecheque(String dstrct, String proveedor, String tipo_doc, String factura) throws SQLException {
        return dao.existeEnPrecheque(dstrct, proveedor, tipo_doc, factura);
    }

    /**
     * Obtiene los egresos pendientes ( precheques) generados para la factura determinada
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct Distrito
     * @param nit Nit del proveedor
     * @param tipodoc Tipo de Documento
     * @param doc Documento
     * @throws SQLException
     * @version 1.0
     */
    public void getEgresosPendientesFactura(String dstrct, String nit, String tipodoc, String doc) throws SQLException {
        dao.getEgresosPendientesFactura(dstrct, nit, tipodoc, doc);
    }

    /**
     * Getter for property info.
     * @return Value of property info.
     */
    public java.util.Vector getInfo() {
        return this.dao.getInfo();
    }

    /**
     * Setter for property info.
     * @param info New value of property info.
     */
    public void setInfo(java.util.Vector info) {
        this.setInfo(info);
    }

    /**
     * Metodo:          CuentaModuloCXP 
     * Descripcion :    M�todo que permite verificar si existe o no una cuenta para el modulo 1.
     * @autor :        Ivan Gomez
     * @param:          distrito y numero de la cuenta.
     * @return:         un switch tipo boolean
     * @throws:         SQLException
     * @version :       1.0
     */
    public boolean CuentaModuloCXP(String distrito, String cuenta) throws SQLException {
        return dao.CuentaModuloCXP(distrito, cuenta);
    }

    /**
     * Muestra el detalle de la factura
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public Vector detalleItemFactura(CXP_Doc factura, Vector vTipImpuestos) throws SQLException {
        dao.detalleItemFactura(factura, vTipImpuestos);
        //return dao.getVecCxp_doc();
        return dao.getInfo();
    }

    /**
     * Obtiene el nit de un cliente de una remesa para facturar
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param numrem N�mero de la remesa
     * @throws SQLException
     * @version 1.0
     */
    public String getNitClienteRemesa(String numrem) throws SQLException {
        return this.dao.getNitClienteRemesa(numrem);
    }

    /**
     * Obtiene la unidad de negocios mas los 3 ultimos digitos del codigo del cliente
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param numrem N�mero de la remesa
     * @throws SQLException
     * @version 1.0
     */
    public String getParteCuentaRemesa(String numrem) throws SQLException {
        return this.dao.getParteCuentaRemesa(numrem);
    }

    /**
     * Obtiene la 1ra remesa no anulada de la oc
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param oc N�mero de la planilla
     * @throws SQLException
     * @version 1.0
     */
    public String obtenerOt(String oc) throws SQLException {
        return dao.obtenerOt(oc);
    }

    /**
     * Metodo  que nos arroja la informacion de las facturas Asociadas a ua Factura recurrente
     * @autor.......Ing. Juan M. Escandon P
     * @param.......distrito, proveedor, tipo de documento y documento
     * @see.........CXPDocDAO.class
     * @version.....1.0.
     * @return.......
     */
    public void consultarFacturasAsociadasRxp(String distrito, String proveedor, String tipo_documento, String documento) throws Exception {
        this.facturas = dao.consultarFacturasAsociadasRxp(distrito, proveedor, tipo_documento, documento);
    }

    public Vector getFacturasAsociadasRxp() {
        return this.facturas;
    }

    /**
     * Setter for property facturas.
     * @param facturas New value of property facturas.
     */
    public void setFacturas(java.util.Vector facturas) {
        this.facturas = facturas;
    }

    /**
     * Metodo updateBanco, permite actualizar el banco de una factura
     * recibe por parametro
     * @autor : Ing. Ivan Dario Gomez
     * @param : String distrito, String proveedor, String tipo_doc, String documento, String banco,String sucursal,String moneda
     * @version : 1.0
     */
    public void updateBanco(String distrito, String proveedor, String tipo_doc, String documento, String banco, String sucursal, String fecha_vencimiento) throws SQLException {
        dao.updateBanco(distrito, proveedor, tipo_doc, documento, banco, sucursal, fecha_vencimiento);
    }

    /**
     * Metodo que Inserta un Documento por pagar en el Sistema --  Modificado para que retorne el SQL .
     * @autor.......David Lamadrid
     * @param.......CXDoc doc(Objeto tipo CXPDoc), Vector vItems(Vector de objetos CxPItem),Vector vImpuestos(Vector de Objetos CXPImpDoc).
     * @see.........CXPDocDAO.class
     * @throws......Existe Documento en el Sistema.
     * @version.....1.0.
     * @return.......
     * @Modificado jbarros
     */
    public ArrayList<String> insertarCXPDoc_SQL(CXP_Doc doc, Vector vItems, Vector vImpuestosDoc, String agencia, String tipo) throws Exception {
        return dao.insertarCXPDoc_SQL(doc, vItems, vImpuestosDoc, agencia,tipo);
    }

    /**
     * Setter for property facturas_ret.
     * @param facturas_ret New value of property facturas_ret.
     */
    public void setFacturas_ret(java.util.Vector facturas_ret) {
        this.facturas_ret = facturas_ret;
    }

    /**
     * Getter for property facturas_ret.
     * @return Value of property facturas_ret.
     */
    public java.util.Vector getFacturas_ret() {
        return facturas_ret;
    }

    /**
     * Obtiene la unidad de negocios mas los 3 ultimos digitos del codigo del cliente
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct Distrito
     * @param nit Nit del proveedor
     * @param doc Numero de la factura
     * @param apl Inidica si aplica retenci�n al pago
     * @throws SQLException
     * @version 1.0
     */
    public String aplicaRetencionPago(String dstrct, String nit, String doc, String apl) throws SQLException {
        return this.dao.aplicaRetencionPago(dstrct, nit, doc, apl);
    }

    /**
     * Obtiene el propietario de una placa
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param numpla N�mero de la planilla
     * @throws SQLException
     * @version 1.0
     */
    public String propietarioPlacaPlanilla(String numpla) throws SQLException {
        return dao.propietarioPlacaPlanilla(numpla);
    }

    /**
     * Obtiene el propietario de una placa
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param numpla N�mero de la planilla
     * @throws SQLException
     * @version 1.0
     */
    public boolean existeAgenciaContable(String agecont) throws SQLException {
        return dao.existeAgenciaContable(agecont);
    }

    /**
     * Obtiene el propietario de una placa
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param numpla N�mero de la planilla
     * @throws SQLException
     * @version 1.0
     */
    public boolean existeCuentaContable(String dstrct, String cuenta) throws SQLException {
        return dao.existeCuentaContable(dstrct, cuenta);
    }

    /**
     * Verifica si existe la relaci�n OT-OC
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param oc N�mero de la planilla
     * @param ot N�mero de la remesa
     * @throws SQLException
     * @version 1.0
     */
    public boolean relacionOtOc(String oc, String ot) throws SQLException {
        return dao.relacionOtOc(oc, ot);
    }

    /**
     * Verifica si existe la OT
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param ot N�mero de la remesa
     * @throws SQLException
     * @version 1.0
     */
    public boolean existeOT(String ot) throws SQLException {
        return dao.existeOT(ot);
    }

    /**
     * Verifica si existe la OC
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param oc N�mero de la planilla
     * @throws SQLException
     * @version 1.0
     */
    public boolean existeOC(String oc) throws SQLException {
        return dao.existeOC(oc);
    }

    /**
     * Verifica si existe la placa
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param placa Placa
     * @throws SQLException
     * @version 1.0
     */
    public boolean existePlaca(String placa) throws SQLException {
        return dao.existePlaca(placa);
    }

    /**
     * Verifica si existe la relaci�n Placa-OC
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param oc N�mero de la planilla
     * @param placa N�mero de la placa
     * @throws SQLException
     * @version 1.0
     */
    public boolean relacionPlacaOc(String oc, String plaveh) throws SQLException {
        return dao.relacionPlacaOc(oc, plaveh);
    }

    //AMATURANA
    /**
     * Metodo obtenerAgencias , Metodo para buscar las agencias
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public void consultarDocumento(
            String dstrct,
            String fra,
            String tipo_doc,
            String numpla,
            String agc,
            String banco,
            String sucursal,
            String nit,
            String fechai,
            String fechaf,
            String pagada,
            String placa,
            String ret_pago) throws SQLException {
        dao.consultarDocumento(dstrct, fra, tipo_doc, numpla, agc, banco, sucursal, nit, fechai, fechaf, pagada, placa, ret_pago);
    }

    public void consultarDocumento(
            String dstrct,
            String fra,
            String tipo_doc,
            String numpla,
            String agc,
            String banco,
            String sucursal,
            String nit,
            String fechai,
            String fechaf,
            String pagada,
            String placa,
            String ret_pago, String iniciox, String hc, String usuario) throws SQLException, Exception {
        dao.consultarDocumento(dstrct, fra, tipo_doc, numpla, agc, banco, sucursal, nit, fechai, fechaf, pagada, placa, ret_pago, iniciox, hc, usuario);
    }

    /**
     * Metodo: BuscarFactura, busca los datos de la factura
     * @autor : Ing. Ivan Gomez
     * @param :String distrito, String codigo del proveedor y numero de la factura
     * @version : 1.0
     */
    public String BuscarClaseDocumento(String dstrct, String proveedor, String documento, String tipo_doc) throws SQLException {
        return dao.BuscarClaseDocumento(dstrct, proveedor, documento, tipo_doc);
    }

    public int updateBancoXProveedor(String distrito, String proveedor, String banco, String sucursal, boolean act_proveedor) throws SQLException {
        return dao.updateBancoXProveedor(distrito, proveedor, banco, sucursal, act_proveedor);
    }

    /**
     * Obtiene los documentos relacionados a una factura
     * @param proveedor nit del proveedor
     * @param tipo_documento Tipo de documento a buacar (ND o NC)
     * @param documento_relacionado numero del documento que se va a buscar
     * @param tipo_documento_relacionado tipo de documento que se va a buscar
     * @return ArrayList con los resultados obtenidos
     * @throws Exception 
     */
    public ArrayList<CXP_Doc> buscarDocsRelacionados(String dstrct, String proveedor, String tipo_documento, String documento_relacionado, String tipo_documento_relacionado) throws Exception {
        return dao.buscarDocsRelacionados(dstrct, proveedor, tipo_documento, documento_relacionado, tipo_documento_relacionado);
    }

    public String autorizarFacturas(String usuario, String dstrct, String proveedor, String tipo_documento, String documento) throws SQLException, Exception {
        return dao.autorizarFactura(usuario, dstrct, proveedor, tipo_documento, documento);
    }

    public ArrayList<CXP_Doc> buscar_facturas_no_aprobadas(String usuario, String fechaInicial, String fechaFinal, String agencia) throws SQLException {
        return dao.buscar_facturas_no_aprobadas(usuario, fechaInicial, fechaFinal, agencia);
    }
    
    public ArrayList<CXP_Doc> buscarFacturasAutorizadas(String usuario, String fechaInicial, String fechaFinal, String agencia) throws SQLException {
        return dao.buscarFacturasAutorizadas(usuario, fechaInicial, fechaFinal, agencia);
    }
    
    public void ActualizarSaldosDocumento(CXP_Doc cXP_Doc) throws SQLException {
        dao.ActualizarSaldosDocumento(cXP_Doc);
    }

    /**
     * Acosta Jesid, Agosto 2013
     * @param prex prefijo del codigo
     */
    public String getSecuenca_CXP_Doc(String prex) {
        try {
            return dao.generar_Id_CXP(prex);
        } catch(Exception e ) {
            return "";
}
    }
    
}