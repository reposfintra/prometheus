/*
 * Codigo_discrepanciaService.java
 *
 * Created on 28 de junio de 2005, 01:12 AM
 */

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  Jose
 */
public class Causas_AnulacionService {
    
    private Causas_AnulacionDAO causasDAO;
    /** Creates a new instance of Codigo_discrepanciaService */
    public Causas_AnulacionService() {
        causasDAO = new  Causas_AnulacionDAO();
    }
    
    /**
     * Getter for property causa_anulacion.
     * @return Value of property causa_anulacion.
     */
    public com.tsp.operation.model.beans.Causas_Anulacion getCausa_anulacion() {
        return causasDAO.getCausa_anulacion();
    }
    
    /**
     * Setter for property causa_anulacion.
     * @param causa_anulacion New value of property causa_anulacion.
     */
    public void setCausa_anulacion(com.tsp.operation.model.beans.Causas_Anulacion causa_anulacion) {
        causasDAO.setCausa_anulacion(causa_anulacion);
    }
    
    /**
     * Getter for property causas_anulacione.
     * @return Value of property causas_anulacione.
     */
    public java.util.Vector getCausas_anulacion() {
        return causasDAO.getCausas_anulacion();
    }
    
    /**
     * Setter for property causas_anulacione.
     * @param causas_anulacione New value of property causas_anulacione.
     */
    public void setCausas_anulacion(java.util.Vector causas_anulacione) {
        causasDAO.setCausas_anulacion(causas_anulacione );
    }
    
    
    public void insert() throws SQLException{
        causasDAO.insert();
    }
    
    public void search(String cod)throws SQLException{
        causasDAO.search(cod);
    }
    
    public boolean exist(String cod) throws SQLException{
        return causasDAO.exist(cod);
    }
    
    public void list()throws SQLException{
        causasDAO.list();
        
    }
    
    public void update() throws SQLException{
        causasDAO.update();
    }
    
    public void anular() throws SQLException{
        causasDAO.anular();
    }
    public void consultar(String codigo, String desc)throws SQLException{
        causasDAO.consultar(codigo, desc);
    }
    public java.util.TreeMap getTcausasa() {
        return causasDAO.getTcausasa();
    }    
  
    /**
     * Setter for property tcausasa.
     * @param tcausasa New value of property tcausasa.
     */
    public void setTcausasa(java.util.TreeMap tcausasa) {
        causasDAO.setTcausasa(tcausasa);
    }
    
    public void llenarTree()throws SQLException{
        causasDAO.llenarTree();
    }
    
}
