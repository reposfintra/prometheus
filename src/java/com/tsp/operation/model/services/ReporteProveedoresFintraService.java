/******************************************************************************
 * Nombre clase :      ReporteProveedoresFintraService.java                   *
 * Descripcion :       Service del ReporteProveedoresFintraService.java       *
 * Autor :             LREALES                                                *
 * Fecha :             12 de septiembre de 2006, 12:00 PM                     *
 * Version :           1.0                                                    *
 * Copyright :         Fintravalores S.A.                                *
 ******************************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;

public class ReporteProveedoresFintraService {
    
    private ReporteProveedoresFintraDAO reporte;
    
    /** Creates a new instance of ReporteProveedoresFintraService */
    public ReporteProveedoresFintraService() {
        
        reporte = new ReporteProveedoresFintraDAO ();
        
    }
    
    /** Funcion publica que obtiene el metodo getVector del DAO */
    public Vector getVectorReporte () throws SQLException {
        
        return reporte.getVector();
        
    }
    
    /** Funcion publica que obtiene el metodo getVector_nits del DAO */
    public Vector getVectorNits () throws SQLException {
        
        return reporte.getVector_nits();
        
    }
    
    /** Funcion publica que obtiene el metodo reporteEspecifico del DAO */
    public void reporteEspecifico ( String nit_prov, String fecini, String fecfin ) throws SQLException {
        
        reporte.reporteEspecifico ( nit_prov, fecini, fecfin );
        
    }
     
    /** Funcion publica que obtiene el metodo reporteTodos del DAO */
    public void reporteTodos ( String fecini, String fecfin ) throws SQLException {
        
        reporte.reporteTodos ( fecini, fecfin );
        
    }
         
    /** Funcion publica que obtiene el metodo proveedores del DAO */
    public void proveedores ( String nom_prov ) throws SQLException {
        
        reporte.proveedores ( nom_prov );
        
    }
    
}