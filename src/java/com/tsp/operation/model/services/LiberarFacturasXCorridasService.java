/***************************************
 * Nombre Clase ............. LiberarFacturasXCorridasService.java
 * Descripci�n  .. . . . . .  Sevicios para liberar las facturas de la corrida
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  19/04/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.    *******************************************/



package com.tsp.operation.model.services;


import com.tsp.operation.model.DAOS.LiberarFacturasXCorridasDAO;
import java.util.*;


public class LiberarFacturasXCorridasService {
    
    LiberarFacturasXCorridasDAO   LiberarFacturasXCorridasDataAccess;
    
    
    public LiberarFacturasXCorridasService() {
        LiberarFacturasXCorridasDataAccess  = new  LiberarFacturasXCorridasDAO();
    }
    public LiberarFacturasXCorridasService(String dataBaseName) {
        LiberarFacturasXCorridasDataAccess  = new  LiberarFacturasXCorridasDAO(dataBaseName);
    }
    
    
    
    
    
    /**
     * M�todo que  permiten obtener el listado de facturas para liberar
     * @autor.......fvillacob
     * @version.....1.0.
     **/
    public List getFacturas(String distrito, String corrida, String user)throws Exception{
        List lista = new LinkedList();
        try{
            lista = this.LiberarFacturasXCorridasDataAccess.getFacturasParaLiberar(distrito, corrida, user);
            
        }catch(Exception e){
            throw new Exception( " getFacturas : " + e.getMessage() );
        }
        return lista;
    }
    
    
    
    
    /**
     * M�todo que  permiten obtener el sql para ejecutarlo
     * @autor.......fvillacob
     * @version.....1.0.
     **/
    public String getSQL( String distrito, String corrida, String proveedor, String tipo_documento, String documento )throws Exception{
        String sql = "";
        try{
            sql = this.LiberarFacturasXCorridasDataAccess.obtenerSQLLiberacion(distrito, corrida, proveedor, tipo_documento, documento);
        }catch(Exception e){
            throw new Exception( " getSQL : " + e.getMessage() );
        }
        return sql;
    }
        
    
    
    
    /**
     * M�todo que  permiten obtener las corridas a liberar
     * @autor.......fvillacob
     * @version.....1.0.
     **/
    public String[] getCorridas( String distrito )throws Exception{
        String[] runes = null;
        try{
            runes  = this.LiberarFacturasXCorridasDataAccess.getCorridasParaLiberar( distrito );
        }catch(Exception e){
            throw new Exception( " getCorridas : " + e.getMessage() );
        }
        return runes;
    }
    
    
    
    
    public void getCorridasParaLiberar(String distrito, String login) throws Exception{
        this.LiberarFacturasXCorridasDataAccess.getCorridasParaLiberar(distrito, login);
    }
     
    
    
    
    
    /**
     * Getter for property treemap.
     * @return Value of property treemap.
     */
    public java.util.TreeMap getTreemap() {
        return this.LiberarFacturasXCorridasDataAccess.getTreemap();
    }
    
    
    /**
     * Setter for property treemap.
     * @param treemap New value of property treemap.
     */
    public void setTreemap(java.util.TreeMap treemap) {
        this.LiberarFacturasXCorridasDataAccess.setTreemap(treemap);
    }
    
    
    
}
