/***************************************************************************
 * Nombre clase : ............... FlotaService.java                        *
 * Descripcion :................. Clase que maneja los Servicios           *
 *                                asignados a Model relacionados con el    *
 *                                programa de Flota                        *
 * Autor :....................... Ing. Juan Manuel Escandon Perez          *
 * Fecha :........................ 6 de diciembre de 2005, 08:38 AM        *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.model.services; 
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.sql.*;
/**
 *
 * @author  JuanM
 */
public class FlotaService {
    /*
     *Declaracion de atributos
     */
    private FlotaDAO           FDataAccess;
    private List               ListaF;
    private Flota              Datos;
    /** Creates a new instance of FlotaService */
    public FlotaService() {
        FDataAccess     = new FlotaDAO();
        ListaF          = new LinkedList();
        Datos           = new Flota();
    }
    
    /**
     * Metodo Insert, a�ade un nuevo registro a la tabla Flota
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String distrito, String base, String placa , String std_job , String usuario
     * @see   : Insert - FlotaDAO
     * @version : 1.0
     */
    public void Insert(String dstrct, String base , String std_job, String placa, String usuario) throws Exception {
        try{
            FDataAccess.Insert(dstrct, base, std_job, placa, usuario);
        }
        catch(Exception e){
            throw new Exception("Error en Insert [FlotaService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Metodo UpdateEstado, modifica el campo reg_status en la tabla flota
     * para un std_job y una placa correspondiente
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String reg_status, String placa , String std_job
     * @see   : UpdateEstado - FlotaDAO
     * @version : 1.0
     */
    public void UpdateEstado(String reg_status, String std_job,  String placa , String usuario) throws Exception {
        try{
            FDataAccess.UpdateEstado(reg_status, std_job, placa, usuario);
        }
        catch(Exception e){
            throw new Exception("Error en Update [FlotaService]...\n"+e.getMessage());
        }
    }
    
     /**
     * Metodo Delete, elimina un registro de la tabla flota 
     * para un std_job y una placa correspondiente
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String placa , String std_job
     * @see   : Delete - FlotaDAO
     * @version : 1.0
     */
    public void Delete(String std_job,  String placa ) throws Exception {
        try{
            FDataAccess.Delete(std_job, placa);
        }
        catch(Exception e){
            throw new Exception("Error en Delete [FlotaService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Metodo ListxPlaca, lista toda la flota correspondiente para una 
     * placa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String placa
     * @see   : ListxPlaca - FlotaDAO
     * @version : 1.0
     */
    public List ListxPlaca(String placa) throws Exception {
        try{
            ReiniciarLista();
            return ListaF = FDataAccess.ListxPlaca(placa);
        }
        catch(Exception e){
            throw new Exception("Error en ListxPlaca [FlotaService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Metodo ListxStd, lista toda la flota correspondiente para un
     * std_job
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String std_job
     * @see   : ListxStd - FlotaDAO
     * @version : 1.0
     */
    public List  ListxStd(String std_job) throws Exception {
        try{
            ReiniciarLista();
            return ListaF = FDataAccess.ListxStd(std_job);
        }
        catch(Exception e){
            throw new Exception("Error en ListxStd [FlotaService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Metodo Buscar, retorna un true si encuentra un registro dentro de la 
     * tabla flota, para un std_job y una placa correspondiente
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String placa, String std_job
     * @see   : Buscar - FlotaDAO
     * @version : 1.0
     */
    public boolean Buscar( String std_job_no, String placa ) throws SQLException {
        try{
            return FDataAccess.Buscar(std_job_no, placa);
        }
        catch(Exception e){
            throw new SQLException("Error en Buscar [FlotaService]...\n"+e.getMessage());
        }
    }
    
     /**
     * Metodo List, lista toda la flota correspondiente para esta std_job 
     * y / o placa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String placa , String std_job 
     * @see   : List - FlotaDAO
     * @version : 1.0
     */
    public List  List(String placa, String std_job) throws Exception {
        try{
            ReiniciarLista();
            return ListaF = FDataAccess.List(placa, std_job);
        }
        catch(Exception e){
            throw new Exception("Error en List [FlotaService]...\n"+e.getMessage());
        }
    }
    
    
    public void ReiniciarDato(){
        this.Datos = null;
    }
    public void ReiniciarLista(){
        this.ListaF = new LinkedList();
    }
    
    public Flota getDato(){
        return this.Datos;
    }
    
    public List getList(){
        return this.ListaF;
    }
    
    /**
     * Metodo: existPlaca, permite buscar una flota que se encuentre dentro de un rango de fecha
     * @autor : Ing. Jose de la rosa
     * @see existPlaca - FlotalDAO     
     * @param : la placa, el distrito, la fecha actual
     * @version : 1.0
     */
    public Flota existPlaca (String placa, String distrito, String fecha )throws SQLException{
        try{
            return FDataAccess.existPlaca(placa, distrito, fecha);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
     /**
     * Metodo: existStandar, permite buscar un Acuerdo especial dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @see existStandar - FlotalDAO     
     * @param : el standar, el distrito
     * @version : 1.0
     */    
    public boolean existStandar (String standar, String distrito, String flota) throws SQLException{
        try{
            return FDataAccess.existStandar(standar, distrito, flota);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }    
}
