/*
 * ClienteService.java
 *
 * Created on 13 de noviembre de 2004, 9:49
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;
import com.tsp.operation.model.DAOS.*;
/**
 *
 * @author  fily
 */
public class TransitoService {
    private TreeMap TlistaCMC = null;
    private List listaC;   
    private TransitoDAO Transitodao;
    private BeanGeneral Bean;
    private Cliente clienteUnidad;
    private TreeMap treemap;
    private Vector vector;
    /** Creates a new instance of ClienteService */
    public TransitoService() {
        Transitodao = new TransitoDAO();
        this.Bean = new BeanGeneral(); 
    }
     public java.util.Vector getVector() {
         return Transitodao.getVector(); 
     }      
     
     public void  BusquedaMinifiesto(String FechaI,String FechaF ) throws Exception {
       Transitodao. BusquedaMinifiesto(FechaI,FechaF ); 
    } 
     public void   BusquedaVehiculos(String FechaI,String FechaF ) throws Exception {
       Transitodao. BusquedaVehiculos(FechaI,FechaF ); 
    } 
    public void   BusquedaConductores(String FechaI,String FechaF ) throws Exception {
       Transitodao. BusquedaConductores(FechaI,FechaF ); 
    }
    public void   BusquedaEmpresas(String FechaI,String FechaF ) throws Exception {
       Transitodao. BusquedaEmpresas(FechaI,FechaF ); 
    }
     public void   BusquedaMercancias(String FechaI,String FechaF ) throws Exception {
       Transitodao. BusquedaMercancias(FechaI,FechaF ); 
    }
 
}
