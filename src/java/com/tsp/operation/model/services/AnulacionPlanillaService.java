/***************************************************************************
 * Nombre clase : ............... AnulacionPlanillaService.java            *
 * Descripcion :................. Clase que maneja los Servicios           *
 *                                asignados a Model relacionados con el    *
 *                                programa de generacion del reporte de    *
 *                                planillas anuladas                       *
 * Autor :....................... Ing. Juan Manuel Escandon Perez          *
 * Fecha :........................ 23 de noviembre de 2005, 02:08 PM       *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.util.*;

public class AnulacionPlanillaService {
    private AnulacionPlanillaDAO    PADataAccess;
    private List                    ListaPA;
    private AnulacionPlanilla       Datos;
    /** Creates a new instance of AnulacionPlanillaService */
    public AnulacionPlanillaService() {
        PADataAccess    = new AnulacionPlanillaDAO();
        ListaPA         = new LinkedList();
        Datos           = new AnulacionPlanilla();
    }
    
    
    /**
     * Metodo listAnulacionPlanillas, lista todas las planillas que se encuentran anuladas 
     * entre un rango de fechas como parametros de busqueda y una agencia como paramentros
     * adicionales
     * @autor : Ing. Juan Manuel Escandon Perez
     * @see   : listAnulacionPlanillas - AnulacionPlanillaDAO
     * @param : String fecha inicial , String fecha final, String agencia, String usuario
     * @version : 1.0
     */
    public void listAnulacionPlanillas( String fechaInicial, String fechaFinal,String agencia, String usuario ) throws Exception {
        try{
            this.ReiniciarListaPA();
            this.ListaPA = PADataAccess.listAnulacionPlanillas(fechaInicial, fechaFinal, agencia, usuario);
            //System.out.println("LISTA " + this.ListaPA.size() );
        }
        catch(Exception e){
            e.printStackTrace();
            throw new Exception("Error en listAnulacionPlanillas [AnulacionPlanillaService]...\n"+e.getMessage());
        }
    }
    
      /**
     * Metodo ReiniciarDato, le asigna el valor null al atributo Datos,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public void ReiniciarDato(){
        this.Datos = null;
    }
    
    /**
     * Metodo ReiniciarListaPA le asigna el valor null al atributo ListaPa,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public void ReiniciarListaPA(){
        this.ListaPA = null;
    }
    
    
    /**
     * Metodo getDato, retorna el valor del atributo Datos,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public AnulacionPlanilla getDato(){
        return this.Datos;
    }
    
    /**
     * Metodo getListPA, retorna el valor del atributo ListaPA,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public List getListPA(){
        return this.ListaPA;
    }
    
}
