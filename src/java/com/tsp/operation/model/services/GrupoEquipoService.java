/*
* Nombre        GrupoEquipoService.java
* Descripci�n   Clase que presta los servicios para el acceso a los datos de grupo_equipo
* Autor         Alejandro Payares
* Fecha         24 de noviembre de 2005, 02:45 PM
* Versi�n       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.GrupoEquipoDAO;

import java.sql.SQLException;
import java.util.LinkedList;

/**
 * Clase que presta los servicios para el acceso a los datos de grupo_equipo
 * @author  Alejandro Payares
 */
public class GrupoEquipoService {
    
    GrupoEquipoDAO dao;
    
    /** Creates a new instance of GrupoEquipoService */
    public GrupoEquipoService() {
        dao = new GrupoEquipoDAO();
    }

    /**
     * Carga los grupos de equipos en memoria.
     * @return Lista con los grupos de equipos.
     * @throws SQLException Si un error de acceso a la base de datos ocurre.
     * @author Alejandro Payares
     */
    public LinkedList getGruposEquipos() throws SQLException {
        return dao.getGruposEquipos();
    }
}
