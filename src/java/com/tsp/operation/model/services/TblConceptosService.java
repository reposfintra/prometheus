/**
 * Nombre        TblConceptosService.java
 * Descripci�n   Service de manipulacion de datos de la tabla tblcon
 * Autor         Mario Fontalvo Solano
 * Fecha         8 de marzo de 2006, 03:52 PM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.TblConceptosDAO;
import com.tsp.operation.model.beans.TblConceptos;
import java.sql.SQLException;
import java.util.*;

public class TblConceptosService {
    
    
    // declaracion de variable
    TblConceptosDAO ConceptosDataAccess;
    Vector          vector;
    Vector          vectorCG;
    Vector          vectorCE;
    TblConceptos    concepto;
    TreeMap         cmbConceptos;
    TreeMap         cmbClases;
    
    
    
    /** Crea una nueva instancia de  TblConceptosService */
    public TblConceptosService() {
        ConceptosDataAccess = new TblConceptosDAO();
    }
    
    
    
    /**
     * Metodo que obtiene una lista de conceptos no anulados de la base de datos
     * @autor mfontalvo
     * @throws Exception.
     */
    public void obtenerConceptos() throws Exception {
        try{
            vector = ConceptosDataAccess.obtenerConceptos();
        }catch (Exception ex){
            throw new Exception("Error en obtenerListado [TblConceptosService]  ...\n" + ex.getMessage());
        }
    }
    
    
    /**
     * Metodo que obtiene todos los conceptos de la base de datos
     * @autor mfontalvo
     * @throws Exception.
     */    
    public void obtenerTodosLosConceptos() throws Exception {
        try{
            vector = ConceptosDataAccess.obtenerTodosLosConceptos();
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en obtenerTodosLosConceptos [TblConceptosService]  ...\n" + ex.getMessage());
        }
    }
    
    
    /**
     * Metodo que obtiene los conceptos generales
     * @autor mfontalvo
     * @throws Exception.
     */    
    public void obtenerConceptosGenerales() throws Exception {
        try{
            vectorCG = ConceptosDataAccess.obtenerConceptosGenerales();
        }catch (Exception ex){
            throw new Exception("Error en obtenerConceptosGenerales [TblConceptosService]  ...\n" + ex.getMessage());
        }
    }
    
    /**
     * Metodo que obtiene los conceptos especificos
     * @param concept_class, clase general de concepto
     * @autor mfontalvo
     * @throws Exception.
     */    
    public void obtenerConceptosEspecificos(String concept_class) throws Exception {
        try{
            vectorCE = ConceptosDataAccess.obtenerConceptosEspecificos( concept_class );
        }catch (Exception ex){
            throw new Exception("Error en obtenerConceptosEspecificos [TblConceptosService]  ...\n" + ex.getMessage());
        }
    }
    
    
    /**
     * Metodo que obtiene un concepto de la base de datos
     * @param dstrct, distrito
     * @param concept_code, Codigo de concepto
     * @autor mfontalvo
     * @throws Exception.
     */     
    public void buscarConceptos(String dstrct, String concept_code) throws Exception {
        try{
            concepto = ConceptosDataAccess.obtenerConceptos(dstrct, concept_code);
        }catch (Exception ex){
            throw new Exception("Error en obtenerListado [TblConceptosService]  ...\n" + ex.getMessage());
        }
    }
    
    
    /**
     * Metodo que toma el listado de conceptos y lo convierte a un treemap
     * @param concept_class, clase general de concepto
     * @autor mfontalvo
     * @throws Exception.
     */     
    public void cargarConceptos(Vector conceptos) throws Exception{
        try{
            if (conceptos!=null && !conceptos.isEmpty()){
                Iterator it = conceptos.iterator();
                cmbConceptos = new TreeMap();
                while (it.hasNext()){
                    TblConceptos concepto = (TblConceptos) it.next();
                    cmbConceptos.put("[" + concepto.getConcept_code() + "]  "+ concepto.getConcept_desc(), concepto.getConcept_code());
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error cargarConceptos [TblConceptosService] ...\n " + ex.getMessage() );
        }
    }
    
    
    /**
     * Metodo que ingresa un concepto a la base de datos
     * @autor mfontalvo
     * @param concepto, Concepto a ingresar
     * @throws Exception.
     */
    public void insertConcepto( TblConceptos concepto ) throws SQLException {
        try{
            if (concepto!=null){
                ConceptosDataAccess.insertConcepto(concepto);
            }
        } catch (SQLException ex){
            ex.printStackTrace();
            throw new SQLException( "Error insertConcepto [TblConceptosService] ...\n" + ex.getMessage() );
        }
    }
    
    /**
     * Metodo que actualiza un concepto a la base de datos
     * @autor mfontalvo
     * @param concepto, Concepto a actualizar
     * @throws Exception.
     */    
    public void updateConcepto( TblConceptos concepto ) throws Exception {
        try{
            if (concepto!=null){
                ConceptosDataAccess.updateConcepto(concepto);
            }
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error updateConcepto [TblConceptosService] ...\n" + ex.getMessage() );
        }
    }
    
    
    /**
     * Metodo que actuliza el estado de una lista de conceptos de la base de datos
     * @autor mfontalvo
     * @param conceptos, Vector de conceptos
     * @throws Exception.
     */    
    public void updateEstado( Vector conceptos ) throws Exception {
        try{
            if (conceptos!=null){
                ConceptosDataAccess.updateEstado(conceptos);
            }
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error updateEstado [TblConceptosService] ...\n" + ex.getMessage() );
        }
    }
    
    
    /**
     * Metodo que elimina un grupo de conceptos de la base de datos
     * @autor mfontalvo
     * @param conceptos, Vector de conceptos
     * @throws Exception.
     */      
    public void deleteConceptos( Vector conceptos ) throws Exception {
        try{
            if (conceptos!=null){
                ConceptosDataAccess.deleteConceptos(conceptos);
            }
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error deleteConceptos [TblConceptosService] ...\n" + ex.getMessage() );
        }
    }
    
    
    /**
     * Getter for property concepto.
     * @return Value of property concepto.
     */
    public com.tsp.operation.model.beans.TblConceptos getConcepto() {
        return concepto;
    }
    
    /**
     * Setter for property concepto.
     * @param concepto New value of property concepto.
     */
    public void setConcepto(com.tsp.operation.model.beans.TblConceptos concepto) {
        this.concepto = concepto;
    }
    
    /**
     * Getter for property lista.
     * @return Value of property lista.
     */
    public java.util.Vector getVector() {
        return vector;
    }
    
    /**
     * Setter for property lista.
     * @param lista New value of property lista.
     */
    public void setVector(java.util.Vector vector) {
        this.vector = vector;
    }
    
    /**
     * Getter for property cmbConceptos.
     * @return Value of property cmbConceptos.
     */
    public java.util.TreeMap getCmbConceptos() {
        return cmbConceptos;
    }
    
    /**
     * Setter for property cmbConceptos.
     * @param cmbConceptos New value of property cmbConceptos.
     */
    public void setCmbConceptos(java.util.TreeMap cmbConceptos) {
        this.cmbConceptos = cmbConceptos;
    }
    
    /**
     * Getter for property cmbClases.
     * @return Value of property cmbClases.
     */
    public java.util.TreeMap getCmbClases() {
        return cmbClases;
    }
    
    /**
     * Setter for property cmbClases.
     * @param cmbClases New value of property cmbClases.
     */
    public void setCmbClases(java.util.TreeMap cmbClases) {
        this.cmbClases = cmbClases;
    }
    
    /**
     * Getter for property vectorCG.
     * @return Value of property vectorCG.
     */
    public java.util.Vector getVectorCG() {
        return vectorCG;
    }
    
    /**
     * Setter for property vectorCG.
     * @param vectorCG New value of property vectorCG.
     */
    public void setVectorCG(java.util.Vector vectorCG) {
        this.vectorCG = vectorCG;
    }
    
    /**
     * Getter for property vectorCE.
     * @return Value of property vectorCE.
     */
    public java.util.Vector getVectorCE() {
        return vectorCE;
    }
    
    /**
     * Setter for property vectorCE.
     * @param vectorCE New value of property vectorCE.
     */
    public void setVectorCE(java.util.Vector vectorCE) {
        this.vectorCE = vectorCE;
    }
     /* Metodo que actualiza un concepto a la base de datos
     * @autor hosorio
     * @param concepto, Concepto a actualizar descripcion y cuenta
     * @throws Exception.
     */    
    public void updateConcepto2( String desc, String account, String codigo ) throws SQLException {
         ConceptosDataAccess.update2(desc, account, codigo);
    }
    public void anular(String distrito, String codigo)throws SQLException { 
        ConceptosDataAccess.anular(distrito,codigo);
    }
    /**
      * Determina si un concepto y cual es su estado
      * @autor Ing. Andr�s Maturana D.
      * @param concepto C�digo del concepto
      * @param distrito C�digo del distrito
      * @throws Exception.
      */  
    public String existeConcepto( String distrito, String codigo ) throws SQLException {
        return ConceptosDataAccess.existeConcepto(distrito, codigo);
    }
    
}
