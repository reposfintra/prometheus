/******************************************************************************
 * Nombre clase :                    HojaControlViajeService.java             *
 * Descripcion :                     Clase que maneja los Servicios           *
 *                                   asignados a Model relacionados con el    *
 *                                   programa de generacion del reporte de    *
 *                                   remesas sin documentos relacionados      *
 * Autor :                           Ing. Juan Manuel Escandon Perez          *
 * Fecha :                           29 de diciembre de 2005, 02:49 PM        *
 * Version :                         1.0                                      *
 * Copyright :                       Fintravalores S.A.                  *
 *****************************************************************************/
package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.*;
import java.util.*;


public class HojaControlViajeService {
    private HojaControlViaje hvc;
    private HojaControlViajeDAO hvcDAO;
    /** Creates a new instance of HojaControlViajeService */
    public HojaControlViajeService() {
        hvc = new HojaControlViaje();
        hvcDAO = new HojaControlViajeDAO();
    }
    
    
    public void Impresion(String numpla ) throws Exception {
        try{
            this.ReiniciarDato();
            this.hvc = hvcDAO.ControlViaje(numpla);
            
        }
        catch(Exception e){
            throw new Exception("Error en Impresion [HojaControlViajeService]...\n"+e.getMessage());
        }
    }
    
    public HojaControlViaje getDato(){
        return this.hvc;
    }
    
    public void setDato( HojaControlViaje hvc ){
        this.hvc = hvc;
    }
    
    public void ReiniciarDato(){
        this.hvc = null;
    }
    
}
