/********************************************************************
 *      Nombre Clase.................   DocumentoService.java
 *      Descripci�n..................   Service de la tabla tbldoc
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   11.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.sql.*;

/**
 *
 * @author  Andres
 */
public class DocumentoService {
    
    private DocumentoDAO doc;
    
    private String  varSeparadorJS = "~";
    private String  varCamposJS;
    
    /** Creates a new instance of DocumentoService */
    public DocumentoService() {
        this.doc = new DocumentoDAO();
    }
    public DocumentoService(String dataBaseName) {
        this.doc = new DocumentoDAO(dataBaseName);
    }
    
    /**
     * Inserta un nuevo registro.
     * @autor Tito Andr�s Maturana
     * @param Objeto de la clase Documento a insertar
     * @throws SQLException
     * @version 1.0
     */
    public void insertarDocumento(Documento doc) throws SQLException{
        this.doc.setDoc(doc);
        this.doc.insertarDocumento();
    }
    
    /**
     * Actualiza un registro.
     * @autor Tito Andr�s Maturana
     * @param Objeto de la clase Documento a actualizar
     * @throws SQLException
     * @version 1.0
     */
    public void actualizarDocumento(Documento doc) throws SQLException{
        this.doc.setDoc(doc);
        this.doc.actualizarDocumento();
    }
    
    /**
     * Anula un registro. Establece el valor del reg_status en 'A'
     * @autor Tito Andr�s Maturana
     * @param cia Distrito
     * @param codigo doctype Codigo del documento
     * @throws SQLException
     * @version 1.0
     */
    public void anularDocumento(String cia, String doctype) throws SQLException{
        this.doc.anularDocumento(cia, doctype);
    }
    
    /**
     * Obtiene un registro.
     * @autor Tito Andr�s Maturana
     * @param cia Distrito
     * @param codigo doctype Codigo del documento
     * @throws SQLException
     * @version 1.0
     */
    public void obtenerDocumento(String cia, String doctype) throws SQLException{
        this.doc.obtenerDocumento(cia, doctype);
    }
    
    /**
     * Obtiene la propiedad doc.
     * @autor Tito Andr�s Maturana
     * @throws SQLException
     * @version 1.0
     */
    public Documento getDocumento() throws SQLException{
        return this.doc.getDoc();
    }
    
    /**
     * Establece la propiedad doc.
     * @autor Tito Andr�s Maturana
     * @param doc Nuevo valor de la propiedad doc
     * @throws SQLException
     * @version 1.0
     */
    public void setDocumento(Documento doc) throws SQLException{
        this.doc.setDoc(doc);
    }
    
    /**
     * Obtiene la propiedad docs.
     * @autor Tito Andr�s Maturana
     * @throws SQLException
     * @version 1.0
     */
    public Vector obtenerDocumentos() throws SQLException{
        this.doc.obtenerDocumentos();
        return this.doc.getDocs();
    }
    
    /**
     * Obtiene una lista de registrode del mismo distrito
     * @autor Tito Andr�s Maturana
     * @param cia Distrito
     * @throws SQLException
     * @version 1.0
     */
    public Vector obtenerDocumentos(String distrito) throws SQLException{
        this.doc.obtenerDocumentos(distrito);
        return this.doc.getDocs();
    }
    
    /**
     * Genera una cadena de caracteres de la declaraci�n de
     * un arreglo en lenguaje JavaScript a partir del listado de
     * documentos de un distrito.
     * @autor Tito Andr�s Maturana
     * @param distrito Distrito de los documentos.
     * @throws SQLException
     * @version 1.0
     */
    public void GenerarJSCampos(String distrito) throws SQLException{
        String var = "\n var CamposJS = [ ";
        Vector docs = this.obtenerDocumentos(distrito);
        if (docs!=null)
            for (int i=0; i<docs.size(); i++){
                Documento doc = (Documento) docs.elementAt(i);
                var += "\n '" + doc.getC_document_type() + varSeparadorJS + doc.getC_document_name() +"',";
            }
        var = var.substring(0,var.length()-1) + "];";
        varCamposJS = var;
    }
    
    
    /**
     * Obtiene una cadena de caracteres de la declaraci�n de
     * un arreglo en lenguaje JavaScript a partir del listado de
     * documentos de un distrito.
     * @autor Tito Andr�s Maturana
     * @param distrito Distrito de los documentos.
     * @throws SQLException
     * @version 1.0
     */
    public String obtenerJSCampos(String distrito) throws SQLException{
        this.GenerarJSCampos(distrito);
        return this.getVarCamposJS();
    }
    
    
    /**
     * Getter for property varCamposJS.
     * @return Value of property varCamposJS.
     */
    public java.lang.String getVarCamposJS() {
        return varCamposJS;
    }
    
    /**
     * Setter for property varCamposJS.
     * @param varCamposJS New value of property varCamposJS.
     */
    public void setVarCamposJS(java.lang.String varCamposJS) {
        this.varCamposJS = varCamposJS;
    }
    
    /**
     * Getter for property varSeparadorJS.
     * @return Value of property varSeparadorJS.
     */
    public java.lang.String getVarSeparadorJS() {
        return varSeparadorJS;
    }
    
    /**
     * Setter for property varSeparadorJS.
     * @param varSeparadorJS New value of property varSeparadorJS.
     */
    public void setVarSeparadorJS(java.lang.String varSeparadorJS) {
        this.varSeparadorJS = varSeparadorJS;
    }
    
    
     public TreeMap obtenerDocumentosT() throws SQLException{
        TreeMap documentos = new TreeMap();
        
        Vector vdocumentos = this.listarTDocumentosPorAplicacion();
        
        for(int i=0; i< vdocumentos.size(); i++){
                Documento doc = ( Documento)vdocumentos.elementAt(i);
               // if( banc.getBanco().matches(banco) )
               documentos.put(doc.getC_document_name (),doc.getC_document_type ());
        } 
        return documentos;    
    }
     
    //David 23.12.05
     
     
      /**
     * Este m�todo genera un Vector con los tipos de socumentos de la bd
     * @param String agencia 
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public Vector listarTDocumentos()throws SQLException{
        return  doc.listarTDocumentos();
    }
    
    
    /**
     * Este m�todo genera un Vector con los tipos de documento pertenecientes a CXPDOC
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public Vector listarTDocumentosPorAplicacion()throws SQLException{
        return doc.listarTDocumentosPorAplicacion();
    }
    
    /**
     * Este m�todo genera un TreeMap con los tipos de documento pertenecientes a CXPDOC
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public TreeMap obtenerDocumentosCXP() throws SQLException{
        TreeMap documentos = new TreeMap();
        
        Vector vdocumentos = this.listarTDocumentosPorAplicacion();
        
        for(int i=0; i< vdocumentos.size(); i++){
            Documento d = (Documento)vdocumentos.elementAt(i);
            // if( banc.getBanco().matches(banco) )
            documentos.put(d.getC_document_name(),d.getC_document_type());
        }
        return documentos;
    }
    
}
