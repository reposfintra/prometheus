/*
 * PreanticipoService.java
 *
 * Created on 11 de abril de 2008, 09:29 AM
 */

package com.tsp.operation.model.services;

import java.util.ArrayList;
import com.tsp.operation.model.DAOS.*;

import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.Font;
import com.lowagie.text.Document;

import com.lowagie.text.pdf.PdfWriter;

import java.io.FileOutputStream;

import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;

import com.lowagie.text.Rectangle;
import com.lowagie.text.Image;

import java.util.ResourceBundle;

import java.util.Calendar;

/**
 *
 * @author  imorales
 */
public class PreanticipoService {
    
    int n;
    BaseFont bf;
    Font fuente_normal    = new Font(bf, 9);
    Font fuente_negrita = new Font(bf, 9);
    
    ResourceBundle rb;
    Document document;
    	
    PreanticipoDAO preanticipoDAO;

    /** Creates a new instance of PreanticipoService */
    public PreanticipoService()  throws Exception{
        n=0;
        preanticipoDAO=new PreanticipoDAO();
        bf = BaseFont.createFont( BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED );
        fuente_negrita.setStyle( com.lowagie.text.Font.BOLD );
    }
    public PreanticipoService(String dataBaseName)  throws Exception{
        n=0;
        preanticipoDAO=new PreanticipoDAO(dataBaseName);   
        bf = BaseFont.createFont( BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED );
        fuente_negrita.setStyle( com.lowagie.text.Font.BOLD );
    }
    public void generarPdf(ArrayList preanticipos,String user){
        try{
            
            //String conjuntoClientes =getConjuntoClientes(clientes);
            //ArrayList extractoClientes=extractosClientesDAO.getExtractosClientes(conjuntoClientes);

            iniciar();
        
            this.escribirPreanticipos(preanticipos, user );
            document.close();
            //System.out.println("TERMINO");
        }catch (Exception e){
            System.out.println("error al generarPdf" +e.toString()+"...."+e.getMessage());      
        }
    }
    
    public int getN(){
        return n;
    }
    
    public void iniciar() throws Exception {
        
        rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        
        n=n+1;
        //System.out.println("n"+n);
        String ruta = rb.getString("ruta")+"/pdf/rotu"+n+".pdf";
        
        document            = new Document();
        
        PdfWriter.getInstance(document, new FileOutputStream( ruta ));
        
        //page    = document.getPageSize();
        
        document.open();
        document.setMargins(0,0,0,0);
    }
    
    public void escribirPreanticipos( ArrayList preanticipos,String user) throws Exception{
        try {
            //float[] anchos = {0.05f,1.0f,0.05f};//porcentajes de ancho de columnas de una tabla
            //System.out.println("n3"+n);
            
            //document.setPageSize(new Rectangle(850,500));
            
            boolean newpag=document.newPage();//nueva pag
            //System.out.println("newpag"+newpag);
            
            int cantidad_columnas=1;
            
            //String nit_actual="";  
            int i=0;
            PdfPTable tableimg ;//se crea una tabla 
            PdfPTable tablefirma ;//se crea una tabla 
            PdfPTable table = new PdfPTable(cantidad_columnas);//se crea una tabla 
            table.setWidthPercentage(100);//uso de 100% horizontal
            //System.out.println("extractoClientes.size"+extractoClientes.size());
            
            String[] preanticip=new String[2];
            //String niti="";
            PdfPCell cell_vacia,cell;
            
            cell_vacia    = new PdfPCell();//se crea una celda vacia triple
            cell_vacia.setColspan(1);//
            cell_vacia.setBorderWidthTop(0);//borde
            cell_vacia.setBorderWidthBottom(0);//borde
            cell_vacia.setBorderWidthLeft(0);//borde
            cell_vacia.setBorderWidthRight(0);//borde
            cell_vacia.setPhrase  ( new Phrase("", fuente_normal ) );//       
            //String[] client=new String[4];
            
            //PdfPTable table_detalle = new PdfPTable(cantidad_columnas);//se crea una tabla 
            
            //String tipodoc="";
            Calendar ahora=Calendar.getInstance();
            int dia=ahora.get(Calendar.DAY_OF_MONTH);
            int mes=ahora.get(Calendar.MONTH);
            int ano=ahora.get(Calendar.YEAR);
            while (i<preanticipos.size()){
                
                preanticip=(String[])preanticipos.get(i);
                //System.out.println("niti"+niti+"dato0:"+datosi[0]);
                                       
                    //document.add(table_detalle);                    
                    //table_detalle = new PdfPTable(cantidad_columnas);//se crea una tabla 
                    
                    table = new PdfPTable(cantidad_columnas);//se crea una tabla 
                    document.newPage();//nueva pag
                    
                    cell_vacia.setColspan(cantidad_columnas);//
                    table.addCell(cell_vacia);//se mete la celda  
                    table.addCell(cell_vacia);//se mete la celda  
                    
                    tableimg = new PdfPTable(5);//se crea una tabla 
                    
                    cell_vacia.setColspan(5);
                    tableimg.addCell(cell_vacia);//se mete la celda  
                    tableimg.addCell(cell_vacia);//se mete la celda  
                    tableimg.addCell(cell_vacia);//se mete la celda  
                    tableimg.addCell(cell_vacia);//se mete la celda  
                    tableimg.addCell(cell_vacia);//se mete la celda  
                    cell_vacia.setColspan(4);
                    
                    cell= new PdfPCell();//se crea una celda 
                    rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");  
                    Image jpg = Image.getInstance(rb.getString("ruta")+"/images/fintra.gif");//se crea una imagen  
                    //Image jpg = Image.getInstance("C:/Tomcat5/webapps/fintravalores/images/fintra.gif");//se crea una imagen                       
                    jpg.scaleToFit(35,25);//se reduce la imagen                       
                    cell.setImage(jpg);//se mete la imagen en la celda
                    
                    cell.setBorderWidthTop(0);//borde
                    cell.setBorderWidthBottom(0);//borde
                    cell.setBorderWidthLeft(0);//borde
                    cell.setBorderWidthRight(0);//borde

                    tableimg.addCell(cell);//se mete la celda de la imagen
                    tableimg.addCell(cell_vacia);//se mete la celda  
                    cell_vacia.setColspan(1);
                    
                    document.add(tableimg); 
                    
                    table.addCell(cell_vacia);//se mete la celda  
                    table.addCell(cell_vacia);//se mete la celda 
                    
                    cell= new PdfPCell();//se crea una celda 
                    cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                    cell.setColspan(cantidad_columnas);//triple
                    cell.setBorderWidthTop(0);//borde
                    cell.setBorderWidthBottom(0);//borde
                    cell.setBorderWidthLeft(0);//borde
                    cell.setBorderWidthRight(0);//borde
                    
                    cell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                    cell.setPhrase  ( new Phrase("RECIBO DE CAJA NUMERO "+preanticip[5], fuente_negrita ) );//                                         
                    table.addCell(cell);//se mete la celda                      
                    cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                    
                    table.addCell(cell_vacia);//se mete la celda 
                    table.addCell(cell_vacia);//se mete la celda 
                    table.addCell(cell_vacia);//se mete la celda 
                    
                    cell.setPhrase  ( new Phrase("CIUDAD : "+"CARTAGENA", fuente_normal ) );//                                         
                    table.addCell(cell);//se mete la celda  
                    
                    //client=    extractosClientesDAO.getDatosCliente(datosi[0]);       
                    
                    cell.setPhrase  ( new Phrase("DIA : "+dia, fuente_normal ) );//                                         
                    table.addCell(cell);//se mete la celda  
                    
                    cell.setPhrase  ( new Phrase("MES : "+getNombreMes(mes), fuente_normal ) );//                                         
                    table.addCell(cell);//se mete la celda  
                    
                    cell.setPhrase  ( new Phrase("A�O : "+ano, fuente_normal ) );//                                         
                    table.addCell(cell);//se mete la celda  
                    
                    //cell.setPhrase  ( new Phrase("CODIGO : "+"12345", fuente_normal ) );//                                         
                    //table.addCell(cell);//se mete la celda  
                    
                    cell.setPhrase  ( new Phrase("PAGADO A : "+preanticip[0], fuente_normal ) );//                                         
                    table.addCell(cell);//se mete la celda  
                    
                    cell.setPhrase  ( new Phrase("VALOR : $ "+com.tsp.util.Util.customFormat(Double.parseDouble(preanticip[1])), fuente_normal ) );//                                         
                    table.addCell(cell);//se mete la celda  
                    
                    cell.setPhrase  ( new Phrase("CONCEPTO : "+"PREANTICIPO", fuente_normal ) );//                                         
                    table.addCell(cell);//se mete la celda  
                    
                    cell.setPhrase  ( new Phrase("APROBADOR : "+user, fuente_normal ) );//                                         
                    table.addCell(cell);//se mete la celda  
                    
                    document.add(table);
                    
                    tablefirma=new PdfPTable(3);
                    
                    cell_vacia.setColspan(3);
                    tablefirma.addCell(cell_vacia);//se mete la celda  
                    tablefirma.addCell(cell_vacia);//se mete la celda  
                    tablefirma.addCell(cell_vacia);//se mete la celda  
                    tablefirma.addCell(cell_vacia);//se mete la celda  
                    tablefirma.addCell(cell_vacia);//se mete la celda  
                    tablefirma.addCell(cell_vacia);//se mete la celda  
                    tablefirma.addCell(cell_vacia);//se mete la celda  
                    tablefirma.addCell(cell_vacia);//se mete la celda         
                    
                    cell_vacia.setColspan(1);
                    
                    cell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                    cell.setPhrase  ( new Phrase("_____________________________", fuente_normal ) );//                                         
                    tablefirma.addCell(cell);//se mete la celda                      
                    cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                    
                    tablefirma.addCell(cell_vacia);//se mete la celda  
                    
                    
                    
                    cell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                    cell.setPhrase  ( new Phrase("_____________________________", fuente_normal ) );//                                         
                    tablefirma.addCell(cell);//se mete la celda                      
                    cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                    
                    tablefirma.addCell(cell_vacia);//se mete la celda  
                    tablefirma.addCell(cell_vacia);//se mete la celda  
                    tablefirma.addCell(cell_vacia);//se mete la celda  
                    
                    cell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                    cell.setPhrase  ( new Phrase("Nombre de recibido", fuente_normal ) );//                                         
                    tablefirma.addCell(cell);//se mete la celda                      
                    cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                    
                    tablefirma.addCell(cell_vacia);//se mete la celda  
                    
                    cell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                    cell.setPhrase  ( new Phrase("Firma de recibido", fuente_normal ) );//                                         
                    tablefirma.addCell(cell);//se mete la celda                      
                    cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                                        
                    document.add(tablefirma);
                
                    //table = new PdfPTable(cantidad_columnas);//se crea una tabla de 
                
                    //table.addCell(cell_vacia);//se mete la celda vacia 
                    
                    //document.add(table);
                    
                
                
    
                //document.add(table);
                        
                i++;
            }
            
            //document.add(table_detalle);          
            
            newpag=document.newPage();
            //System.out.println("newpag"+newpag);
            
        }catch (Exception ex){
                System.out.println("ex en escribirPreanticipos"+ex.toString()+"__"+ex.getMessage());
            ex.printStackTrace();
        }
    }
    
    public String getNombreMes(int mes){
        mes++;
        String m="";
        if (mes==1){m="Enero";}
        if (mes==2){m="Febrero";}
        if (mes==3){m="Marzo";}
        if (mes==4){m="Abril";}
        if (mes==5){m="Mayo";}
        if (mes==6){m="Junio";}
        if (mes==7){m="Julio";}
        if (mes==8){m="Agosto";}
        if (mes==9){m="Septiembre";}
        if (mes==10){m="Octubre";}
        if (mes==11){m="Noviembre";}
        if (mes==12){m="Diciembre";}
        return m;
        
    }
            
    public ArrayList getPreanticiposImprimibles(String[] preanticipos) throws Exception{
        ArrayList preanticiposimprimibles=new ArrayList();
        preanticiposimprimibles=preanticipoDAO.getPreanticiposImprimibles(preanticipos);
        
        return preanticiposimprimibles;
    }
}
