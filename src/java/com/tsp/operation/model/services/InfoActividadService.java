/***********************************************************************************
 * Nombre clase : ............... InfoActividadService.java                        *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.                                       *
 * Autor :....................... Ing. Diogenes Antonio Bastidas Morales           *
 * Fecha :........................ 2 de septiembre de 2005, 06:34 PM               *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/

package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;


public class InfoActividadService {
    private InfoActividadDAO infoactd;
    
    /** Creates a new instance of InfoActividadService */
    public InfoActividadService() {
        infoactd = new InfoActividadDAO();
    }
    /**
     * Metodo obtInfoActividad, obtiene  objeto de tipo Infoactividad
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: obtInfoActividad - InfoActividadDAO
     * @version : 1.0
     */
    public InfoActividad obtInfoActividad(){
        return infoactd.obtInfoActividad();
    }
    /**
     * Metodo obtInfoActividad, obtiene  objeto de tipo Infoactividad
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: obtVecInfoActividad - InfoActividadDAO
     * @version : 1.0
     */
    public Vector obtVecInfoActividad (){
        return infoactd.obtVecInfoActividad();
    }
    /**
     * Metodo insertarInfoActividad, ingresa un registro en la tabla infoactividad
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: obtVecInfoActividad - InfoActividadDAO
     * @version : 1.0
     */
    public void insertarInfoActividad(InfoActividad infact) throws SQLException {
        try{
            infoactd.setActividad(infact);
            infoactd.insertarInfoActividad();
        }catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo modificarInfoActividad, modifica un registro en la tabla infoactividad
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: modificarInfoActividad - InfoActividadDAO
     * @version : 1.0
     */
    public void modificarInfoActividad(InfoActividad infact) throws SQLException {
        try{
            
            infoactd.setActividad(infact);
            infoactd.modificarInfoActividad();
            
        }catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo buscarInfoActividad, retorna true o false si tiene informacion la actividad                  
     * @param:compa�ia, codigo actividad, cod cliente, conmpa�ia
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: buscarInfoActividad - InfoActividadDAO
     * @version : 1.0
     */ 
    public boolean buscarInfoActividad(String codact, String codcli ,String cia, String numpla, String numrem) throws SQLException {
        try{
            return infoactd.buscarInfoActividad(codact, codcli, cia, numpla, numrem );
        }catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo anularInfoActividad, anula actividades
     * @param:compa�ia, codigo actividad decripcion
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: anularInfoActividad - InfoActividadDAO
     * @version : 1.0 
     */ 
    public void anularInfoActividad(InfoActividad infact) throws SQLException {
        try{           
           infoactd.setActividad(infact);
           infoactd.anularInfoActividad(); 
        }catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo:  existeInfoActividadAnulada, retorna true o false si la infoactividad esta anulada.
     * @param:codigo actividad, codigo cliente, compa�ia
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: existeInfoActividadAnulada - InfoActividadDAO
     * @version : 1.0
     */ 
    public boolean existeActividadInfoAnulada(String codact, String codcli ,String cia, String numrem ) throws SQLException {
        try{
           return infoactd.existeInfoActividadAnulada(codact, codcli, cia, numrem);
        }catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo:  actualizarActPlanilla, actualiza la actividad en la planilla.
     * @param: numpla, cod actividad
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: actualizarActPlanilla - InfoActividadDAO
     * @version : 1.0
     */ 
    public void actualizarActPlanilla(String numpla, String act) throws SQLException {
        try{
            infoactd.actualizarActPlanilla(numpla, act);
            
        }catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
    
    
     /* Metodo fecha_ult_act_Registrada, retorna la fecha de cierre de la ultima actividad registrada      
     * @param:compa�ia,  cod cliente, numpla, numremesa
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */ 
    public String fecha_ult_act_Registrada(String codcli ,String cia, String numpla, String numrem) throws SQLException {
        return infoactd.fecha_ult_act_Registrada(codcli, cia, numpla, numrem);
    }
}
