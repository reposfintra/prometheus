/**********
 * Nombre clase :            IngresoService.java
 * Descripcion :             Clase que maneja los Service
 * Autor :                   Ing. Diogenes Antonio Bastidas Morales
 * Fecha :                   10 de mayo de 2006, 09:54 AM
 * Version :                 1.0
 * Copyright :               Fintravalores S.A.
 **********/

package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*; 

import java.io.*;
import java.sql.*;
import java.util.*;


public class IngresoService {
    private IngresoDAO ingresodao;
    /** Creates a new instance of IngresoService */
    
    private boolean temporal   = false;;
    private String nro_ing     = "";
     
    private boolean tempmis    = false;
    private String nro_ingmis  = "";
    
    
    public IngresoService() {
        ingresodao = new IngresoDAO();
    }

    public IngresoService(String dataBaseName) {
        ingresodao = new IngresoDAO(dataBaseName);
    }
   
    
    
    /**
     * Metodo insertarIngreso, ingresa un registro en la tabla ingreso
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    
    public String insertarIngreso(Ingreso ing) throws Exception {
        try{
            ingresodao.setIngreso(ing);
            return ingresodao.insertarIngreso();
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * Metodo buscarSerie, busca el numero del ingreso
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales 
     * @version : 1.0
     */
    public String buscarSerie(String tipo) throws Exception {
        try{
            return ingresodao.buscarSerie(tipo);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * Metodo buscarIngreso, mopdifica un registro en la tabla ingreso
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarIngreso(String dstrct, String tipodoc,String num_ingreso) throws Exception {
        try{
            ingresodao.buscarIngreso(dstrct, tipodoc, num_ingreso);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * Getter for property ingreso.
     * @return Value of property ingreso.
     */
    public com.tsp.operation.model.beans.Ingreso getIngreso() {
        return ingresodao.getIngreso();
    }
    
     /**
     * Metodo setIngreso, agrega valores al objeto
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void setIngreso(Ingreso ing) throws Exception {
        try{
            ingresodao.setIngreso(ing);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    /**
     * Metodo modificarIngreso, mopdifica un registro en la tabla ingreso
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void modificarIngreso(Ingreso ing) throws Exception {
        try{
            ingresodao.setIngreso(ing);
            ingresodao.modificarIngreso();
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
     /**
     * Metodo buscarIngresosCliente, busca los ingresos del cliente
     * @param: codigo del cliente, fecha inicial, fecha final
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarIngresosCliente(String dstrct, String tipo,String codcli,String fecini, String fecfin, String estado ) throws Exception {
        try{
            ingresodao.buscarIngresosCliente(dstrct, tipo,codcli, fecini, fecfin, estado);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * Getter for property listadoingreso.
     * @return Value of property listadoingreso.
     */
    public java.util.LinkedList getListadoingreso() {
        return ingresodao.getListadoingreso();
    }
    
    /**
     * Metodo buscarIngresos_NroFactura, busca los ingresos del cliente, deacuerdo a un numero de factura
     * @param: distito, numero de factura
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarIngresos_NroFactura(String dstrct, String factura ) throws Exception {
        try{
            ingresodao.buscarIngresos_NroFactura(dstrct, factura);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * Metodo buscarIngreso, busca el ingreso miscelaneo
     * @param:disrtito, numero ingreso
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarIngresoMiscelaneo(String dstrct,String tipodoc, String num_ingreso) throws Exception {
        try{
            ingresodao.buscarIngresoMiscelaneo(dstrct, tipodoc, num_ingreso);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    public void busquedaIngresoMiscelaneo(String dstrct, String tipo, String identificacion, String fecha1, String fecha2 ) throws Exception {
        ingresodao.busquedaIngresoMiscelaneo(dstrct, tipo,identificacion, fecha1, fecha2);
    }
    
    /**
     * Metodo tieneItemsIngreso, verifica si tiene items un ingreso
     * @param: distrito, tipodocumento,nroingreso
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean tieneItemsIngreso(String dstrct, String tipodocumento, String nroingreso ) throws Exception {
        try{
            return ingresodao.tieneItemsIngreso(dstrct, tipodocumento, nroingreso);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * Metodo anularIngreso, anula el registro y coloca el
     * @param: objeto ingreso
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public String anularIngreso(Ingreso ing) throws Exception {
        try{
            ingresodao.setIngreso(ing);
            return ingresodao.anularIngreso();
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    /**
     * M�todo que busca los ingresos de acuuerdo a unos filtros
     * @autor.......Jose de la rosa
     * @throws......Exception
     * @version.....1.0.
     **/
    
    public void consultaIngresos(String distrito, String cliente, String periodo, String tipo_ingreso, String fec_ini, String fec_fin, String tipo_doc, String doc, String estado, String no_ingreso)throws Exception{
        try{
            ingresodao.consultaIngresos(distrito, cliente, periodo, tipo_ingreso, fec_ini, fec_fin, tipo_doc, doc, estado, no_ingreso);
        }catch(Exception e){
            e.printStackTrace();            
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * Getter for property temporal.
     * @return Value of property temporal.
     */
    public boolean isTemporal() {
        return temporal;
    }    
    
    /**
     * Setter for property temporal.
     * @param temporal New value of property temporal.
     */
    public void setTemporal(boolean temporal) {
        this.temporal = temporal;
    }
    
    /**
     * Getter for property nro_ing.
     * @return Value of property nro_ing.
     */
    public String getNro_ing() {
        return nro_ing;
    }
    
    /**
     * Setter for property nro_ing.
     * @param nro_ing New value of property nro_ing.
     */
    public void setNro_ing(String nro_ing) {
        this.nro_ing = nro_ing;
    }
    
    /**
     * Getter for property tempmis.
     * @return Value of property tempmis.
     */
    public boolean isTempmis() {
        return tempmis;
    }
    
    /**
     * Setter for property tempmis.
     * @param tempmis New value of property tempmis.
     */
    public void setTempmis(boolean tempmis) {
        this.tempmis = tempmis;
    }
    
    /**
     * Getter for property nro_ingmis.
     * @return Value of property nro_ingmis.
     */
    public String getNro_ingmis() {
        return nro_ingmis;
    }
    
    /**
     * Setter for property nro_ingmis.
     * @param nro_ingmis New value of property nro_ingmis.
     */
    public void setNro_ingmis(String nro_ingmis) {
        this.nro_ingmis = nro_ingmis;
    }
    
        /**
     * M�todo que busca las facturas asociadas a un ingreso
     * @autor.......Jose de la rosa
     * @throws......Exception
     * @version.....1.0.
     **/
    public void consultaFacturasIngresos(String distrito, String factura )throws Exception{
        try{
            ingresodao.consultaFacturasIngresos(distrito, factura);
        }catch(Exception e){
            e.printStackTrace();            
            throw new Exception(e.getMessage());
        }
    }
     public void update_Fecimp_Ingresoo( String tipodocumento, String nroingreso) throws Exception {
        try{
            ingresodao.update_Fecimp_Ingreso( tipodocumento, nroingreso );
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
     
      /**
     * Metodo anularIngresoContabilizado, modifica un registro en la tabla ingreso
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public String anularIngresoContabilizado( Ingreso ing ) throws Exception {
        ingresodao.setIngreso(ing);
        return ingresodao.anularIngresoContabilizado();
    }

     /**
     * Metodo tieneItemsIngreso, verifica si tiene items un ingreso
     * @param: distrito, tipodocumento,nroingreso
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public double saldoIngresoRI(String dstrct, String tipodocumento, String nroingreso ) throws SQLException {
        return ingresodao.saldoIngresoRI( dstrct, tipodocumento, nroingreso );
    }
    
    /**
     * Metodo tieneSubIngreso, verifica si un ingreso tiene sub ingresos asociados
     * @param: distrito, tipodocumento,nroingreso
     * @autor : Ing. Jdelarosa
     * @version : 1.0
     */
    public boolean tieneSubIngreso(String dstrct, String tipodocumento, String nroingreso ) throws SQLException {
        return ingresodao.tieneSubIngreso( dstrct, tipodocumento, nroingreso );
    }    
    
    public String insertarNewIngreso(Ingreso ing) throws Exception {
        try{
            ingresodao.setIngreso(ing);
            return ingresodao.insertarNewIngreso();
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    public String incrementarSerie(String tipo) throws Exception {
        try{
            return ingresodao.incrementarSerie(tipo);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
     /**
     * Metodo buscarIngresos, busca los ingresos
     * @param: fecha inicial, fecha final
     * @autor : Ing. Iris Vargas
     * @version : 1.0
     */
    public void buscarIngresos(String dstrct, String tipo,String banco,String fecini, String fecfin, String num_ingreso, String factura ) throws Exception {
        try{
            ingresodao.buscarIngresos(dstrct, tipo, banco, fecini, fecfin, num_ingreso, factura);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
        
}
