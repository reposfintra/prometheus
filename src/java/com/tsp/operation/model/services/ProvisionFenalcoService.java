/*
 * ProvisionFenalcoService.java
 * Created on 17 de junio de 2008, 04:25 PM
 */

package com.tsp.operation.model.services;
import com.tsp.operation.model.DAOS.*;
import java.util.Vector;
import com.tsp.operation.model.beans.*;

/**
 *
 * @author  jemartinez
 */
public class ProvisionFenalcoService {

    ProvisionFenalcoDAO provisionFenalcoDAO;
    private String[] infoTitulosFenalco = null;
    private String[] infoTitulosFintra = null;

    private int num_avales = 0;
    private String[] avales = null;

    public ProvisionFenalcoService() {
        provisionFenalcoDAO = new ProvisionFenalcoDAO();
        try{
            
            
            
        }catch(java.lang.Exception e){
            System.out.println("Service: "+e.toString());
        }
    }

    /**
     * Retorna la lista de avales de
     */
    public String[] getAvalesService(){
        try{
            if (avales==null){
                avales = provisionFenalcoDAO.getAvales();//lista de avales
            }
        }catch(Exception e){
            System.out.println("errore en getAvalesService:"+e.toString());
            e.printStackTrace();
        }
        return avales;
    }

    public void setBeanTitulosService(BeanGeneral beanGeneral){
        provisionFenalcoDAO.setBeanTitulos(beanGeneral);
    }

    public BeanGeneral getBeanTitulosService(){
        return provisionFenalcoDAO.getBeanTitulos();
    }

    //info de fenalco
    private String[] getInfoTitulosService(int tipo) throws java.lang.Exception{
        return provisionFenalcoDAO.getInfoTitulos(tipo);
    }

    //info de fintra
    public String[] getInfoTitulosFenalco() throws java.lang.Exception{
        try{
            if (infoTitulosFenalco==null){
                infoTitulosFenalco = this.getInfoTitulosService(1);
            }
        }catch(Exception e){
            System.out.println("errore en getInfoTitulosFenalco:"+e.toString());
            e.printStackTrace();
        }
        return this.infoTitulosFenalco;
    }

    public String[] getInfoTitulosFintra() throws java.lang.Exception{
        try{
            if (infoTitulosFintra==null){
                infoTitulosFintra = this.getInfoTitulosService(2);
            }
        }catch(Exception e){
            System.out.println("errore en getInfoTitulosFintra:"+e.toString());
            e.printStackTrace();
        }
        return this.infoTitulosFintra;
    }

    public String[] getInfoTitulosService(int tipo, int limit, int offset) throws java.lang.Exception{
        return provisionFenalcoDAO.getInfoTitulos(tipo, limit, offset);
    }

    public String setEstadoProvisionService(String[] noFactura, String noDoc)  throws java.lang.Exception{
        return provisionFenalcoDAO.setEstadoProvision(noFactura, noDoc);
    }

    public int getCountProvisionService(int tipo, String noAval) throws java.lang.Exception{
        return provisionFenalcoDAO.getCountProvisionXAval(tipo, noAval);
    }

    private String[] getAvales() throws java.lang.Exception{
        return provisionFenalcoDAO.getAvales();
    }

    public void setCxpItemsDocService(String[] titulos) throws java.lang.Exception{
        provisionFenalcoDAO.insertarCxpItemsDoc(titulos);
    }

    public void setCxpDocService(String titulo) throws java.lang.Exception{
        provisionFenalcoDAO.insertarCxpDoc(titulo);
    }

    public String obtenerInformacionProveedorService() throws java.lang.Exception{
        return provisionFenalcoDAO.obtenerInformacionProveedor();
    }

    public String obtenerValorCxpDocService(String noFactura) throws java.lang.Exception{
        return provisionFenalcoDAO.obtenerValorCxpDoc(noFactura);
    }

    public void actualizarValorCxpDocService(String noFactura, String  valor) throws java.lang.Exception{
        provisionFenalcoDAO.actualizarValorCxpDoc(noFactura,valor);
    }

    public String[] obtenerListadoFenalcoSinConcordar() throws java.lang.Exception{
        return provisionFenalcoDAO.obtenerListadoFenalcoSinConcordar();
    }
}
