/*
 * AsignacionUsuariosClientesService.java
 *
 * Created on 16 de septiembre de 2005, 11:00 AM
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.General;
import com.tsp.operation.model.DAOS.AsignacionClientesUsuariosDAO;
import java.util.*;
/**
 *
 * @author  mfontalvo
 */
public class AsignacionUsuariosClientesService {
    
    
    private AsignacionClientesUsuariosDAO AsigUC ;
    private List ListaTipos;
    private List ListaPerfiles;
    private List ListaClientes;
    private List ListaUsuarios;
    
    private List ListaPerfilUsuario;
    private List ListaPerfilCliente;
    
    private String TipoRelacion;
    private String Perfil;
    
    
    /** Creates a new instance of AsignacionUsuariosClientesService */
    public AsignacionUsuariosClientesService() {
        AsigUC = new AsignacionClientesUsuariosDAO();
    }
    
    public void Init()throws Exception{
        try{
            createTables();
            buscarTipos();
            buscarPerfiles();
            buscarUsuarios();
            buscarClientes();
            
            if (ListaPerfiles!=null && ListaPerfiles.size()>0){
                Perfil = ((General) ListaPerfiles.get(0)).getCodigo();
                buscarPerfilClientes(Perfil);
                buscarPerfilUsuarios(Perfil);
                buscarRelacionPerfil(Perfil);
            }
        }catch (Exception ex){
            throw new Exception("Error en Init [AsignacionUsuariosClientesService] ....\n"+ex.getMessage());
        }
    }
    
    public void createTables()throws Exception{
         AsigUC.EXECUTE_UPDATE(AsigUC.CREATE_PERFIL_USUARIOS, null, null);
         AsigUC.EXECUTE_UPDATE(AsigUC.CREATE_PERFIL_CLIENTES, null, null);
         AsigUC.EXECUTE_UPDATE(AsigUC.CREATE_RELACION_PERFIL, null, null);
    }
    
    public void upUsuariosClientes(String []Usuarios, String []Clientes, String Tipo, String Perfil, String Usuario)throws Exception{
        try{
            AsigUC.upUsuariosClientes(Usuarios, Clientes, Tipo, Perfil, Usuario);
        }catch (Exception ex){
            throw new Exception("Error en upUsuariosClientes [AsignacionUsuariosClientesService] ... \n"+ex.getMessage());
        }
    }

    
    public void buscarRelacionPerfil(String Perfil)throws Exception{
        try{
            String [] params = { Perfil };
            List tmp = AsigUC.EXECUTE_QUERY(AsigUC.SELECT_RELACION_PERFIL,  params );
            if (tmp!=null && tmp.size()>0) 
                TipoRelacion = ((General) tmp.get(0)).getDescripcion();
            else
                TipoRelacion = "";
        }catch (Exception ex){
            throw new Exception("Error en buscarTiposUsuarios [AsignacionUsuariosClientesService] ... \n"+ex.getMessage());
        }
    }    
    public void buscarTipos()throws Exception{
        try{
            ListaTipos = AsigUC.EXECUTE_QUERY(AsigUC.SELECT_TIPOS,  null );
        }catch (Exception ex){
            throw new Exception("Error en buscarTiposUsuarios [AsignacionUsuariosClientesService] ... \n"+ex.getMessage());
        }
    }
    public void buscarPerfiles()throws Exception{
        try{
            ListaPerfiles = AsigUC.EXECUTE_QUERY(AsigUC.SELECT_PERFILES,  null );
        }catch (Exception ex){
            throw new Exception("Error en buscarPerfiles [AsignacionUsuariosClientesService] ... \n"+ex.getMessage());
        }
    }    
    public void buscarClientes()throws Exception{
        try{
            ListaClientes = AsigUC.EXECUTE_QUERY(AsigUC.SELECT_CLIENTES,  null );
        }catch (Exception ex){
            throw new Exception("Error en buscarClientes [AsignacionUsuariosClientesService] ... \n"+ex.getMessage());
        }
    }
    public void buscarUsuarios()throws Exception{
        try{
            ListaUsuarios = AsigUC.EXECUTE_QUERY(AsigUC.SELECT_USUARIOS,  null );
        }catch (Exception ex){
            throw new Exception("Error en buscarClientes [AsignacionUsuariosClientesService] ... \n"+ex.getMessage());
        }
    }
    
    public void buscarPerfilUsuarios(String Perfil)throws Exception{
        try{
            String [] params = { Perfil };
            ListaPerfilUsuario = AsigUC.EXECUTE_QUERY(AsigUC.SELECT_PERFIL_USUARIO,  params );
        }catch (Exception ex){
            throw new Exception("Error en buscarPerfilUsuarios [AsignacionUsuariosClientesService] ... \n"+ex.getMessage());
        }
    }    
    public void buscarPerfilClientes(String Perfil)throws Exception{
        try{
            String [] params = { Perfil };
            ListaPerfilCliente = AsigUC.EXECUTE_QUERY(AsigUC.SELECT_PERFIL_CLIENTE,  params );
        }catch (Exception ex){
            throw new Exception("Error en buscarPerfilUsuarios [AsignacionUsuariosClientesService] ... \n"+ex.getMessage());
        }
    }
    
    
    public void setPerfil(String value){
        Perfil = value;
    }
    
    public String getPeril(){
        return Perfil;
    }
    public String getTipoRelacion(){
        return TipoRelacion;
    }
    public List getListaTipos(){
        return ListaTipos;
    }
    public List getListaPerfiles(){
        return ListaPerfiles;
    }
    public List getListaUsuarios(){
        return ListaUsuarios;
    }
    public List getListaClientes(){
        return ListaClientes;
    }
    public List getListaPerfilUsuarios(){
        return ListaPerfilUsuario;
    }
    public List getListaPerfilClientes(){
        return ListaPerfilCliente;
    }
    
    
}
