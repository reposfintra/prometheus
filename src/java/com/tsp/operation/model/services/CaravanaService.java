/*
 * CaravanaService.java
 *
 * Created on 02 de agosto de 2005, 14:34
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.VistaCaravana;
import com.tsp.operation.model.beans.Caravana;
import com.tsp.operation.model.beans.Via;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.ViaService;
import com.tsp.util.Util;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  Henry
 */
public class CaravanaService {
    
    CaravanaDAO dao;
    ViaService viaService;
    public Vector vectorRutas;
    
    public CaravanaService() {
        dao = new CaravanaDAO();
        viaService  = new ViaService();
        vectorRutas = new Vector();
    }
    
    public String getFechaActual(){
        return Util.getFechaActual_String(4);
    }
    public int getNoCaravana() throws SQLException{
        return dao.getNextNumeroCaravana();
    }
    public void getVehiculosDespachados(String fecini, String fecfin, String ag) throws SQLException {
        dao.getVehiculosDespachados(fecini, fecfin, ag);
    }
    public void getVehiculosCaravana(String numcaravana) throws SQLException {
        dao.getVehiculosCaravana(numcaravana);
        //Cargando las rutas de la planilla
        Vector caravanas = dao.getVectorCaravanaActual();
        for (int i=0; i<caravanas.size(); i++){
            VistaCaravana vis  = (VistaCaravana) caravanas.elementAt(i);
            String ruta = vis.getRuta_pla();
            this.cargarVectorPCXRutaPla(ruta);
        }            
    }
    /*public void eliminarPosicionVectorModificarCaravana(int pos){
        dao.eliminarPosicionVectorModificarCaravana(pos);
    }*/
    public Vector getVectorVehiculos(){
        return dao.getVectorVehiculos();
    }
    public void modificarVectorVehiculos(int pos, boolean val){
        dao.modificarVectorVehiculos(pos,val);
    }
    public void insertarCaravana(Caravana c) throws SQLException {
        dao.insertarCaravana(c);
    }
    public Vector getVectorCaravanaActual(){
        return dao.getVectorCaravanaActual();
    }
    public void agregarVectorCaravanaActual(VistaCaravana vc){
        dao.agregarVectorCaravanaActual(vc);
    }
    public void eliminarPosicionVectorCaravanaActual(int pos){
        dao.eliminarPosicionVectorCaravanaActual(pos);
    }
    public void getVehiculosXPlanilla(String numPla) throws SQLException{
        dao.getVehiculosXPlanilla(numPla);
    }
    public VistaCaravana getVehiculoPlanilla(){
        return dao.getVehiculoPlanilla();
    }
    public boolean existaPlanilla(String numpla) throws SQLException{
        return dao.existePlanilla(numpla);
    }
    public boolean existaPlanillaCaravana(String numpla) throws SQLException{
        return dao.existePlanillaCaravana(numpla);
    }
    public boolean existaVehiculoCaravana(VistaCaravana vista){
        return dao.existeVehiculoCaravana(vista);
    }
    public String getNumeroCaravanaXPlanilla(String numpla) throws SQLException{
        return dao.getNumeroCaravanaXPlanilla(numpla);
    }
    public void agregarVectorExcluido(VistaCaravana vc){
        dao.agregarVectorExcluidos(vc);
    }
    public void eliminarPosicionVectorExcluido(int pos){
        dao.eliminarPosicionVectorExcluidos(pos);
    }
    public Vector getVectorExcluidos(){
        return dao.getVectorExcluidos();
    }
    public void setNumCaravana(int num){
        dao.setNumCaravana(num);
    }
    public int getNumCaravana(){
        return dao.getNumCaravana();
    }
    public void setRazonVectorExcluido(int pos, String razon){
        dao.setRazonVectorExcluido(pos, razon);
    }
    public void modificarCaravana(Caravana c) throws SQLException{
        dao.modificarCaravana(c);
    }
    public void limpiarVectorActualPlanillas(){
        dao.limpiarVectorActualPlanillas();
    }
    public void borrarPosicionMayorUnoVectorActualPlanillas(){
        dao.borrarPosicionMayorUnoVectorActualPlanillas();
    }
    public Vector getInfoClienteCaravana(String codcli, String std_job_no) throws SQLException{
        return dao.getInfoClienteCaravana(codcli, std_job_no);
    }
    //HOY
    public Vector getVectorListarCaravana(){
        return dao.getVectorListarCaravana();
    }
    public void getVectorCaravanas(String car, boolean fin) throws SQLException {
        dao.getVectorCaravanas(car,fin);
    }
    public void getVectorCaravanasGeneradas() throws SQLException {
        dao.getVectorCaravanasGeneradas();
    }
    public boolean validarOrigenDestino(VistaCaravana v, String via1) {
        /*String ori = v.getOripla ();
        String des = v.getDespla ();
        return dao.validarOrigenDestino (ori, des);*/
        return validarTramosIguales(via1, v.getVia());
    }
    public Vector getVectorPlanillasCaravana(int numCar) throws SQLException{
        return dao.getVectorPlanillasCaravana(numCar);
    }
    public String getFechaInicioCaravana(int num) throws SQLException {
        return dao.getFechaInicioCaravana(num);
    }
    public void finalizarCaravana(int numcaravana) throws SQLException {
        dao.finalizarCaravana(numcaravana);
    }
    public String getFechaFinCaravana(int num) throws SQLException {
        return dao.getFechaFinCaravana(num);
    }
    public void limpiarVectorPlanillasExcluidas(){
        dao.limpiarVectorPlanillasExcluidas();
    }
    public void anularCaravana(int num) throws SQLException{
        dao.anularCaravana(num);
    }
    public void actualizarCaravanaIngresoTrafico(String planilla, String valor) throws SQLException{
        dao.actualizarCaravanaIngresoTrafico(planilla, valor);
    }
    
    public boolean validarTramosIguales(String via1, String via2){
        //llenar en vector 1 la via 1
        Vector vec1 = new Vector();
        int cont = 0;
        for(int i=0;i<via1.length();i++){
            cont++;
            if(cont==2){
                String codciu = via1.substring(i-1,i+1);
                vec1.add(codciu);
                cont = 0;
            }
        }
        //llenar en vector 2 la via 2
        Vector vec2 = new Vector();
        cont = 0;
        for(int i=0;i<via2.length();i++){
            cont++;
            if(cont==2){
                String codciu = via2.substring(i-1,i+1);
                vec2.add(codciu);
                cont = 0;
            }
        }
        // comienza la validaci�n
        int pos_origen = 0;
        int j = 0;
        boolean no_secuencia = true;
        for(int i=0; i< vec1.size(); i++){
            if( vec1.get(i).equals( vec2.get(0) ) )
                pos_origen = 1;
            if( pos_origen == 1 && no_secuencia == true && j != vec2.size() ){
                if( !vec2.get(j).equals(vec1.get(i) ) )
                    no_secuencia = false;
                j++;
            }
        }
        if( ( pos_origen == 1 && no_secuencia == false ) || ( pos_origen == 0 ) )
            return false;
        else
            return true;
    }
    
    /**
     * Metodo: modificarDestinoOrigenCaravana, permite modificar el origen y el destino de una caravana con el numero de la planilla
     * @autor : Ing. Jose de la rosa
     * @param : numero de planillla, el codigo de origen y el codigo de destino de la caravana
     * @version : 1.0
     */
    public void modificarDestinoOrigenCaravana(String numpla, String origen, String destino) throws SQLException {
        dao.modificarDestinoOrigenCaravana(numpla, origen, destino);
    }
    
    /***********************************************
     * METODOS POR ANDRES MATURANA DE LA CRUZ
     ***********************************************/
    
    /**
     * Busca una planilla dada en los registros almacenados para determinar si existe o no
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param numpla. El numero de la planilla
     * @throws SQLException si existe un error en la conculta.
     * @return <tt>true</tt> si existe la planilla  � <tt>false</tt> si no existe
     */
    
    public boolean existePlanillaManual(String numpla) throws SQLException {
        return dao.existePlanillaManual(numpla);
    }
    
    /**
     * Busca los datos de un vehiculo filtrados por el numero de la planilla manual
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param numpla. Numero de la planilla
     * @param fecfin Fecha final de despachos
     * @throws SQLException si existe un error en la conculta.
     * @return un vector de objetos <tt>VistaCaravana</tt>
     */
    public void getVehiculoXPlanillaManual(String numPla) throws SQLException {
        dao.getVehiculoXPlanillaManual(numPla);
    }
    /**
     * Obtiene la ruta de las planillas agregadas
     * @autor Ing. Henry A. Osorio Gonzalez
     * @param String viaPlanilla
     * @return un vector de String
     */
    public void cargarVectorPCXRutaPla(String via) throws SQLException {
        Vector vias = viaService.obtenerPCVia(via);
        boolean esta = false;
        for (int i=0; i<vias.size(); i++){
            Via v = (Via) vias.elementAt(i);
            for (int j=0; j<vectorRutas.size(); j++) {
                Via v2 = (Via) vectorRutas.elementAt(j);
                if (v2.getCodigo().equals(v.getCodigo())) {
                    esta = true;
                }
            }
            if (esta==false){
                vectorRutas.addElement(v);
            }
            esta = false;
        }
    }
    
    /**
     * Getter for property vectorRutas.
     * @return Value of property vectorRutas.
     */
    public java.util.Vector getVectorRutas() {
        return vectorRutas;
    }
    
    /**
     * Setter for property vectorRutas.
     * @param vectorRutas New value of property vectorRutas.
     */
    public void setVectorRutas(java.util.Vector vectorRutas) {
        this.vectorRutas = vectorRutas;
    }
    public boolean existaPlanillaEnCaravana(String numpla) throws SQLException{
        return dao.existePlanillaEnCaravana(numpla);
    }
    
     /**
     * Busca si una  planilla dada esta en  una caravana �  ha sido asignada a un escolta, si esta en una caravana
     * se buscan todas las planillas que  la acompa�an en la caravana y se verifican que todas tengan como 
     * ultimo reporte de movimiento trafico "Entrega", si es asi se liberan los escoltas relacionados con la caravana
     * o si es una asignaci�n sencilla se libera automaticamente el escolta asignado a esta.     *
     * @autor Ing. Henry A. Osorio Gonzalez
     * @param numpla. El numero de la planilla
     * @throws SQLException si existe un error en la conculta.
     */
     public void finalizarCaravanaEscoltaTrafico(String numpla) throws SQLException {
         RepMovTraficoService repMovTraficoService = new RepMovTraficoService();
         EscoltaVehiculoService escoltaVehiculoSVC = new EscoltaVehiculoService();
         int numC = Integer.parseInt(this.getNumeroCaravanaXPlanilla(numpla));
         //Es una asignaci�n sencilla
         if (numC==0) {
              Vector placas = escoltaVehiculoSVC.getPlacaEscoltaAsignada(numpla);
              for (int i=0; i<placas.size(); i++) {
              	escoltaVehiculoSVC.liberarEscoltas(placas.elementAt(i).toString(),numpla);    
              }              
              escoltaVehiculoSVC.actualizarEscoltaIngresoTrafico(numpla,"");
         } 
         else {
             //La planilla esta en una caravana
            Vector caravana = this.getVectorPlanillasCaravana(numC);
            boolean[] entrega = new boolean[caravana.size()];
            for (int i=0; i<caravana.size(); i++) {
                entrega[i] = repMovTraficoService.tieneReporteEntraga((String) caravana.elementAt(i));
            }
            int sw = 0;
            for (int i=0; i<entrega.length; i++){
                if (entrega[i]==false)
                    sw = 1;
            }
            if (sw==0) {
                /*Obtenfo los vehiculos escoltas de esa caravana*/
                escoltaVehiculoSVC.searchEscoltasCaravana(numC);
                Vector placas = escoltaVehiculoSVC.getPlacasEscolta();
                /*Actualizando las planillas de ingreso trafico*/
                for (int i=0; i<caravana.size(); i++) {
                    String plani = (String) caravana.elementAt(i);
                    escoltaVehiculoSVC.actualizarEscoltaIngresoTrafico(plani, "");                   
                    this.actualizarCaravanaIngresoTrafico(plani,"");
                    escoltaVehiculoSVC.actualizarEscoltaIngresoTrafico(plani,"");
                }                
                 /*Liberando escoltas de la caravana*/
                for(int j=0; j<placas.size(); j++){
                    String placa = (String) placas.elementAt(j);
                    escoltaVehiculoSVC.liberarEscoltasCaravana(placa,numC);
                }
                /*Finalizando la caravana*/
                this.finalizarCaravana(numC);
            }
         }
     }
}