/*
 *
 * luigi
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.util.*;
import java.sql.*;
/**
 *
 * @author  Ing Luis Eduardo Frieri
 */
public class VetoService {
    
    private VetoDAO VetoDataAccess;
    /** Creates a new instance of CarroceriaService */
    public VetoService() {
        VetoDataAccess = new VetoDAO();
    }
    public VetoService(String dataBaseName) {
        VetoDataAccess = new VetoDAO(dataBaseName);
    }
    
    public void insertarVeto( String dstrct, String usuario, String documento, String base) throws SQLException{
         try{
            VetoDataAccess.insertarVeto(dstrct, usuario, documento, base);
         }catch(SQLException e){
         throw new SQLException("Error : insertarVeto /n" + e.getMessage()+"" + e.getErrorCode());
         }
    }
    
    public void insertarVetoNuevo( String dstrct, String usuario, String documento, String base, String causa, String obs, String evento, String fuente, String tipo) throws SQLException{
         try{
            VetoDataAccess.insertarVetoNuevo(dstrct, usuario, documento, base, causa, obs, evento, fuente, tipo);
         }catch(SQLException e){
         throw new SQLException("Error : insertarVetoNuevo /n" + e.getMessage()+"" + e.getErrorCode());
         }
    }
    
    public void buscarCausaVeto(String ced, String clase) throws SQLException {
        VetoDataAccess.buscarCausaVeto(ced, clase);
    }
    
    public Veto obtenerVeto(){
        return VetoDataAccess.obtVeto();
    }
    
    public void ModificarVeto(String doc, String usu){
        try{
            VetoDataAccess.ModificarVeto(doc, usu);
        }catch(SQLException e){
            e.printStackTrace();
        } 
    }
    
}
