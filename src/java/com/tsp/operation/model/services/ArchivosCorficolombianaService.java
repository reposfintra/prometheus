/*
 * ArchivosCorficolombianaService.java
 *
 * Created on 27 de octubre de 2008, 12:13
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.ArchivosCorficolombianaDAO;
import java.util.ArrayList;
import com.tsp.util.LogWriter;
import java.io.PrintWriter;
/**
 *
 * @author  navi
 */
public class ArchivosCorficolombianaService {
    ArchivosCorficolombianaDAO archivosCorficolombianaDAO;
    /** Creates a new instance of ArchivosCorficolombianaService */
    public ArchivosCorficolombianaService() {
        archivosCorficolombianaDAO=new ArchivosCorficolombianaDAO();
    }
    
    public String generarArchivo(String fecini,String fecfin,PrintWriter writer,String op) throws Exception{
        try{
            String [] consultas=new String[12];
            ArrayList registros=new ArrayList();
            //System.out.println("antes de getR"+fecini+"_"+fecfin);
            registros=archivosCorficolombianaDAO.getRegistros(fecini,fecfin,op);
            //System.out.println("despues de getR");
            for (int i=0 ; i<registros.size(); i++){
                //System.out.println("reg:"+(String ) registros.get(i));
                writer.println((String)registros.get(i));
            }
            
        }catch(Exception e){
            System.out.println("errorcito:"+e.toString()+"__"+e.getMessage());
        }
        return "";
    }
    
}
