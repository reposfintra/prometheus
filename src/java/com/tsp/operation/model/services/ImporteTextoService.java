/*
 * ImporteTextoService.java
 * Created on 24 de marzo de 2009, 16:14
 */
package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.ImporteTextoDAO;
import java.util.ArrayList;
import java.util.List;
import java.io.ByteArrayInputStream;
import java.util.Dictionary;
import com.tsp.util.Utility;
import java.util.ResourceBundle;
import com.tsp.operation.model.beans.Imagen;

/** * @author  Fintra */
public class ImporteTextoService {
    ImporteTextoDAO importeTextoDAO;
    private  boolean                     process;    //20100706
    public ImporteTextoService() {
        process        = false;//20100706
        importeTextoDAO=new ImporteTextoDAO();
    }
    
    public List searchArchivos(String documento        ,String loginx) throws Exception{        
        try{            
             ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
             //String ruta  = rb.getString("ruta")+ "/images/multiservicios/"+loginx+"/";
             String ruta  = rb.getString("ruta")+ "/images/multiservicios/tem/"+loginx+"/";//20100707
             //System.out.println("en service doc:"+documento+" y ruta:"+ruta);
             return  this.importeTextoDAO.searcArchivos(documento,(""+ruta ),loginx);    
        }catch(Exception e){ 
            System.out.println("hubo error en imptextservice :"+e.toString()+"__"+e.getMessage());
            throw new Exception( e.getMessage());
        }
    }

    public String getESTADO(){
        return this.importeTextoDAO.getESTADO();
    }

    public boolean isProcess() {//20100706
        return process;
    }

    public void setProcess(boolean process) {//20100706
        this.process = process;
    }

}

