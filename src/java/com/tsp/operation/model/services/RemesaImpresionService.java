/******************************************************************************
 * Nombre clase :                    RemesaImpresionService.java              *
 * Descripcion :                     Clase que maneja los Servicios           *
 *                                   asignados a Model relacionados con el    *
 *                                   programa de generacion del reporte de    *
 *                                   remesas sin documentos relacionados      *
 * Autor :                           Ing. Juan Manuel Escandon Perez          *
 * Fecha :                           6 de enero de 2006, 04:00 PM             *
 * Version :                         1.0                                      *
 * Copyright :                       Fintravalores S.A.                  *
 *****************************************************************************/
package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.util.*;

public class RemesaImpresionService {
    private RemesaImpresionDAO DataAccess;
    private DatosRemesaImpPDF  Datos;
    private List ListaPlaAsociadas;
    
    /** Creates a new instance of RemesaImpresionService */
    public RemesaImpresionService() {
        DataAccess = new RemesaImpresionDAO();
        Datos = new DatosRemesaImpPDF();
        ListaPlaAsociadas = new LinkedList();
    }
    
    
    /**
     * Metodo DatosRemesa, permite buscar los datos correspondiente a una remesa para su impresion.
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String numrem
     * @see   : DatosRemesa - RemesaImpresionDAO
     * @version : 1.0
     */
    public void DatosRemesa(String numrem) throws Exception {
        try{
            this.Datos = DataAccess.DatosRemesa(numrem);
        }
        catch(Exception e){
            throw new Exception("Error en DatosRemesa [RemesaImpresionService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Metodo asigna null al objeto de acceso a datos
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : 
     * @see   : 
     * @version : 1.0
     */
    public void ReiniciarDato(){
        this.Datos = null;
    }
    
    /**
     * Metodo getDato obtiene el objeto del Dao
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : 
     * @see   : 
     * @version : 1.0
     */
    public DatosRemesaImpPDF getDato(){
        return this.Datos;
    }
    
    /**
      * Metodo SearchPlanilla_Pendiente, indica si una remesa tiene asociadas planillas pendientes ( reg_status = 'P' )
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String numrem
     * @see   : SearchPlanilla_Pendiente - RemesaImpresionDAO
     * @version : 1.0
     */
    public boolean SearchPlanilla_Pendiente(String numrem) throws Exception {
        try{
            return DataAccess.SearchPlanilla_Pendiente(numrem);
        }
        catch(Exception e){
            throw new Exception("Error en SearchPlanilla_Pendiente [RemesaImpresionService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Metodo asigna null al objeto de acceso a ListaPlaAsociadas
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param :
     * @see   :
     * @version : 1.0
     */
    public void ReiniciarLista(){
        this.ListaPlaAsociadas = null;
    }
    
    /**
     * Metodo getList obtiene el objeto ListaPlaAsociadas
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param :
     * @see   :
     * @version : 1.0
     */
    public List getList(){
        return this.ListaPlaAsociadas;
    }


/**
     * Metodo ListaPlanillaAsociadas, permite buscar los datos correspondiente a una remesa para su impresion.
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String numrem
     * @see   : ListaPlanillaAsociadas - RemesaImpresionDAO
     * @version : 1.0
     */
    public void ListaPlanillaAsociadas(String numrem) throws Exception {
        try{
            this.ReiniciarLista();
            ListaPlaAsociadas = DataAccess.ListaPlanillaAsociadas(numrem);
        }
        catch(Exception e){
            throw new Exception("Error en ListaPlanillaAsociadas [RemesaImpresionService]...\n"+e.getMessage());
        }
    }
    
     /**
     * Metodo DatosRemesaAsociadas, permite buscar los datos correspondiente a una remesa y una planilla para su impresion.
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String numrem
     * @see   : DatosRemesaAsociadas - RemesaImpresionDAO
     * @version : 1.0
     */
    public void DatosRemesaAsociadas(String numpla, String numrem) throws Exception {
        try{
            this.Datos = DataAccess.DatosRemesaAsociadas(numpla, numrem);
        }
        catch(Exception e){
            throw new Exception("Error en DatosRemesaAsociadas [RemesaImpresionService]...\n"+e.getMessage());
        }
    }

    
}
