/*
 * RepMovTraficoService.java
 *
 * Created on 12 de septiembre de 2005, 11:32 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;
/**
 *
 * @author Armando Oviedo
 */
public class RepMovTraficoService {
    
    private RepMovTraficoDAO rmtdao;
    private TreeMap causas;
    private Vector planillasReportes;
    private String Planilla;
    public Vector  reporteVaradosCliente;
    
    /** Creates a new instance of RepMovTraficoService */
    public RepMovTraficoService() {
        rmtdao = new RepMovTraficoDAO();
    }
    
    public void setUltRMT(RepMovTrafico ultrmt){
        rmtdao.setUltRMT(ultrmt);
    }
    
    public RepMovTrafico getUltRMT(){
        return rmtdao.getUltRMT();
    }
    
    public RepMovTrafico getProxRMT(){
        return rmtdao.getProxRMT();
    }
    
    public void setProxRMT(RepMovTrafico proxrmt){
        rmtdao.setProxRMT(proxrmt);
    }
    
    public void setDatosPlanillaRMT(DatosPlanillaRMT dp){
        rmtdao.setDatosPlanillaRMT(dp);
    }
    
    public void SearchLastCreatedReport() throws SQLException{
        rmtdao.SearchLastCreatedReport();
    }
    public String addRMT(RepMovTrafico rmt) throws SQLException{
        String sql = "";
        try{
            rmtdao.setRMT(rmt);
            boolean existe = rmtdao.existeReporte();
            if(!existe)
                sql =rmtdao.addRMT();
            else
                sql=rmtdao.updIfExists();
        }
        catch(SQLException ex){
            throw new SQLException(ex.getMessage());
        }
        return sql;
    }
    
    public boolean existeReporte() throws SQLException{
        return rmtdao.existeReporte();
    }
    
    public String getFecharep(){
        GregorianCalendar g = new GregorianCalendar();
        String fecharep = String.valueOf(g.get(Calendar.YEAR));
        int MES = g.get(Calendar.MONTH)+1;
        String mes="0";
        if(MES<10){
            mes = mes+MES;
        }
        else{
            mes = String.valueOf(MES);
        }
        fecharep = fecharep +"-"+ mes +"-"+ String.valueOf(g.get(Calendar.DATE));
        return fecharep;
    }
    
    public void getNextPC() throws SQLException{
        rmtdao.getNextPC();
    }
    
    public void setLastCreatedReport() throws SQLException{
        rmtdao.setLastCreatedReport();
    }
    
    public String getCodNextPC(){
        return rmtdao.getCodNextPC();
    }
    
    public String getHora(){
        String hora="";
        GregorianCalendar g = new GregorianCalendar();
        String h = String.valueOf(g.get(Calendar.HOUR_OF_DAY));
        if(h.length()<2){
            h = "0"+h;
        }
        String m = String.valueOf(g.get(Calendar.MINUTE));
        if(m.length()<2){
            m = "0"+m;
        }
        hora = h+":"+m;
        return hora;
    }
    
    public Vector getTiposubicacion() throws SQLException{
        return rmtdao.getTiposubicacion();
    }
    
    public Vector getUbicaciones() throws SQLException{
        return rmtdao.getUbicaciones();
    }
    
    public void BuscarTiposubicacion() throws SQLException{
        rmtdao.BuscarTiposubicacion();
    }
    
    public String getNombreCiudad(String codciu) throws SQLException{
        return rmtdao.getNombreCiudad(codciu);
    }
    
    public boolean esDespachoManual(String numpla) throws SQLException{
        return rmtdao.esDespachoManual(numpla);
    }
    
    public void BuscarPuestosControl() throws SQLException{
        rmtdao.BuscarPuestosControl();
    }
    
    public void BuscarUbicaciones(String codtipoub) throws SQLException{
        if(codtipoub.equalsIgnoreCase("PC")){
            BuscarPuestosControl();
        }
        else if(codtipoub.equalsIgnoreCase("CIU")){
            BuscarCiudades();
        }
        else{
            rmtdao.getOtrasUbicaciones(codtipoub);
        }
    }
    
    public void BuscarPlanilla(String numpla) throws SQLException{
        rmtdao.BuscarPlanilla(numpla);
    }
    
    public void BuscarPlanillaDM(String numpla) throws SQLException{
        rmtdao.BuscarPlanillaDM(numpla);
    }
    
    public double getDuracionTramo(String origen, String destino) throws SQLException{
        return rmtdao.getDuracionTramo(origen, destino);
    }
    
    public DatosPlanillaRMT getDatosPlanilla() throws SQLException{
        return rmtdao.getDatosPlanillaRMT();
    }
    
    public void BuscarReportesPlanilla(String numpla) throws SQLException{
        rmtdao.BuscarReportesPlanilla(numpla);
    }
    
    public Vector getReportesPlanilla() throws SQLException{
        return rmtdao.getReportesPlanilla();
    }
    
    
    public void delReport(RepMovTrafico rmt) throws SQLException{
        try{
            rmtdao.setRMT(rmt);
            rmtdao.delReport();
        }
        catch(SQLException ex){
            ex.getMessage();
        }
    }
    
    public RepMovTrafico getLastCreatedReport() throws SQLException{
        return rmtdao.getLastCreatedReport();
    }
    
    public void BuscarReporteMovTraf(String numpla, String fecha) throws SQLException{
        rmtdao.BuscarReporteMovTraf(numpla, fecha);
    }
    
    public RepMovTrafico getReporteMovTraf(){
        return rmtdao.getReporteMovTraf();
    }
    
    public String getDescripcionTUbicacion(String cod) throws SQLException{
        return rmtdao.getNombreTipoUbicacion(cod);
    }
    
    public String getDescripcionUbicacion(String cod) throws SQLException{
        return rmtdao.getNombreUbicacion(cod);
    }
    
    public boolean esFechaAnteriorAExistente(String fecha) throws SQLException{
        return rmtdao.esFechaAnteriorAExistente(fecha);
    }
    public DatosPlanillaRMT getDatosPlanillaRMT(){
        return rmtdao.getDatosPlanillaRMT();
    }
    
    public String getZona(String codciu) throws SQLException{
        return rmtdao.getZona(codciu);
    }
    
    //Jose 27/10/2005
    public boolean planillaConReporteEntrga(String numpla) throws SQLException{
        return rmtdao.planillaConReporteEntrga(numpla);
    }
    
    //Tito
    public Vector getReportesPlanilla(String numpla) throws SQLException{
        return rmtdao.getReportesPlanilla(numpla);
    }
    
    
    /**
     * M�todo que buscar y llena un vector de ciudades
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void BuscarCiudades() throws SQLException{
        rmtdao.BuscarCiudades();
    }
    
    /**
     * M�todo que buscar y llena un vector de ciudades
     * @autor.......Jose de la rosa
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void BuscarTodasCiudades() throws SQLException{
        rmtdao.BuscarTodasCiudades();
    }
    
    /**
     * M�todo que actualiza las tablas de ingreso tr�fico y tr�fico despu�s de una inserci�n o modificaci�n
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     **/
    public String actualizarIngresoTrafico() throws SQLException{
        return rmtdao.actualizarIngresoTrafico();
    }
    /**
     * M�todo que setea un objeto de reporte de movimiento de tr�fico con el ultimo encontrado
     * @autor.......Karen Reales
     * @param.......N�mero de planilla
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void BuscarReporteMovTraf(String numpla) throws SQLException{
        rmtdao.BuscarReporteMovTraf(numpla);
    }
    /**
     * M�todo que busca un grupo de planilla que pertenece a una caravana
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void BuscarPlanillasCaravana() throws SQLException{
        rmtdao.BuscarPlanillasCaravana();
    }
    
    /**
     * Getter for property caravana.
     * @return Value of property caravana.
     */
    public java.util.Vector getCaravana() {
        return rmtdao.getCaravana();
    }
    
    /**
     * Setter for property caravana.
     * @param caravana New value of property caravana.
     */
    public void setCaravana(java.util.Vector caravana) {
        rmtdao.setCaravana(caravana);
    }
    /**
     * M�todo que actualiza los campos  de ultimo reporte en ingreso trafico y trafico
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.
     **/
    public String actualizarUltPC() throws SQLException{
        return rmtdao.actualizarUltPC();
    }
    
    /**
     * M�todo que verifica si existe una planilla en ingreso_trafico
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.
     **/
    public boolean estaPlanilla(String numpla) throws SQLException{
        return rmtdao.estaPlanilla(numpla);
    }
    /*fin*/
    
    //Diogenes 16.12.05
    /**
     * Metodo  sumarDemoraAct, modificar el campo demora, sumando la demora con
     * la demora de la actividad.
     * @param: demora
     * see: sumarDemoraAct(int demora) - RepMovTraficoDAO
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void sumarDemoraAct(double demora, String numpla) throws SQLException {
        rmtdao.sumarDemoraAct(demora,numpla);
    }
    /**
     * Funcion que retorna un treemap de una lista de clasificacion de observaciones
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.
     **/
    public TreeMap getListaClasificacion() throws SQLException{
        com.tsp.operation.model.services.TblGeneralService tblgensvc = new  com.tsp.operation.model.services.TblGeneralService();
        tblgensvc.buscarLista("CLO", "REP_TRA");
        return tblgensvc.getLista_des();
    }
    
    /**
     * Funcion que retorna un treemap de una lista de causas
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void searchListaCausas(String tipo) throws SQLException{
        com.tsp.operation.model.services.TblGeneralService tblgensvc = new  com.tsp.operation.model.services.TblGeneralService();
        tblgensvc.buscarLista("CS", tipo);
        causas= tblgensvc.getLista_des();
    }
    
    /**
     * Getter for property causas.
     * @return Value of property causas.
     */
    public java.util.TreeMap getCausas() {
        return causas;
    }
    
    /**
     * Setter for property causas.
     * @param causas New value of property causas.
     */
    public void setCausas(java.util.TreeMap causas) {
        this.causas = causas;
    }    
   
    
    /**
     * Busca los clientes destinatarios de una planilla
     * @autor Ing. Karen Reales
     * @param Numero de planilla
     * @throws SQLException
     * @version 1.0.
     **/
    public void buscarClientes(String numpla) throws SQLException{
        rmtdao.buscarClientes(numpla);
    }
    
    /**
     * Getter for property clientes.
     * @return Value of property clientes.
     */
    public java.util.Vector getClientes() {
        return rmtdao.getClientes();
    }
    
    /**
     * Setter for property clientes.
     * @param clientes New value of property clientes.
     */
    public void setClientes(java.util.Vector clientes) {
        rmtdao.setClientes(clientes);
    }
    /*fin*/
    
    /**
     * Realiza todas las consultas pertinentes para el reporte de generacion
     * @autor Ing. Tito Andr�s Maturana D.
     * @param fechaI Fecha inicial del per�odo
     * @param fechaF Fecha final del per�odo
     * @throws SQLException
     * @see com.tsp.operation.model.DAOS.RepMovTraficoDAO.java#planillasRegistradasPeriodo(String, String)
     * @see com.tsp.operation.model.DAOS.RepMovTraficoDAO.java#puestosControlPlanilla(String)
     * @see com.tsp.operation.model.DAOS.RepMovTraficoDAO.java#puestoControlRegistrado(String, String, String, String)
     * @see com.tsp.operation.model.DAOS.RepMovTraficoDAO.java#ultimoRegistroPCPlanilla(String, String)
     * @see com.tsp.operation.model.DAOS.RepMovTraficoDAO.java#agregarDuracionTramo(String)
     * @see com.tsp.operation.model.DAOS.RepMovTraficoDAO.java# usuariosTurno(String)
     * @see com.tsp.operation.model.DAOS.RepMovTraficoDAO.java# ingresarRegistroFaltante(ReporteGeneracion, String)
     * @version 1.0.
     **/
    public void iniciarReporteGeneracion(String login, String fechaI, String fechaF) throws SQLException{
        rmtdao.iniciarReporteGeneracion(login, fechaI + " 00:00:00", fechaF + " 23:59:59.999");
        Vector numplas = rmtdao.planillasPeriodo(fechaI + " 00:00:00", fechaF + " 23:59:59.999");
        
        for( int i=0; i<numplas.size(); i++){
            String numpla = (String) numplas.elementAt(i);
            Vector tramos = puestosControlPlanilla(numpla);
            Object[] trms = tramos.toArray();
            
            for( int j=1; j<(trms.length-1); j++){
                String pc0 = (String) trms[j-1];
                String pc = (String) trms[j];
                boolean registrado = rmtdao.puestoControlRegistrado(numpla, pc);
                String fecha_anterior = "";
                
                if(!registrado){
                    //double duracion = rmtdao.getDuracionTramo(pc0, pc);
                    String zonapc = rmtdao.zonaPC(pc);
                    
                    if( j!=1 ){
                        fecha_anterior = rmtdao.ultimoRegistroPCPlanilla(numpla, pc0);
                    } else {
                        fecha_anterior = rmtdao.fechaSalida(numpla);
                    }
                    
                    if( fecha_anterior.length()!=0){
                        //double duracion = rmtdao.getDuracionTramo(pc0, pc);
                        String nfecha = rmtdao.agregarDuracionTramo(fecha_anterior, pc0, pc);
                        Vector users = rmtdao.usuariosTurno(nfecha);
                        ReporteGeneracion gen = new ReporteGeneracion();
                        gen.setFecha(nfecha);
                        gen.setPc(pc);
                        gen.setPlanilla(numpla);
                        gen.setZona(zonapc);
                        if( users.size()==0 ){
                            gen.setLogin("NR");
                            rmtdao.ingresarRegistroFaltante(gen, login);
                        } else {
                            boolean sw = false;
                            for( int k=0; k<users.size(); k++){
                                Vector arg = (Vector) users.elementAt(k);
                                String user = (String) arg.elementAt(0);
                                String[] zonas = ((String) arg.elementAt(1)).split(",");
                                for(int x=0; x<zonas.length; x++){
                                    if( zonas[x].compareTo(zonapc)==0 ){
                                        gen.setLogin(user);
                                        rmtdao.ingresarRegistroFaltante(gen, login);
                                        sw = true;
                                    }
                                }
                            }
                            
                            if(!sw){
                                gen.setLogin("NR");
                                rmtdao.ingresarRegistroFaltante(gen, login);
                            }
                        }
                        
                    }
                    
                    
                }
            }
            
        }
        
    }
    
    /**
     * Obtiene el tramo de una planilla
     * @autor Ing. Tito Andr�s Maturana
     * @param numpla N�mero de la planilla
     * @see com.tsp.operation.model.DAOS.RepMovTraficoDAO.java#obtenerInformacionTramo(String)
     * @returns Arreglo con los distintod puntos de la ruta de un planilla
     * @throws SQLException
     * @version 1.0
     **/
    public Vector puestosControlPlanilla(String numpla) throws SQLException {
        String tr = this.rmtdao.obtenerInformacionTramo(numpla);
        Vector pcs = new Vector();
        if( tr!=null ){
            for( int i=0; i<(tr.length()); i+=2 ){
                String pc = String.valueOf(tr.charAt(i)).concat(String.valueOf(tr.charAt(i+1)));
                pcs.add(pc);
            }
        }
        return pcs;
    }
    
    
    /**
     * Obtiene los detalles del reporte de generacion
     * @autor Ing. Tito Andr�s Maturana D.
     * @throws SQLException
     * @see com.tsp.operation.model.DAOS.RepMovTraficoDAO.java#obtenerReporteGeneracion()
     * @version 1.0.
     **/
    public Vector obtenerReporteGeneracion() throws SQLException{
        return rmtdao.detalleReporteDeGeneracion();
    }
    
    /**
     * Obtiene el total de registros, registros no grabados, registros grabadps, y porcentaje de registros grabados por usuario
     * @autor Ing. Tito Andr�s Maturana D.
     * @throws SQLException
     * @return Los totales por usuario
     * @see com.tsp.operation.model.DAOS.RepMovTraficoDAO.java#totalesReporteDeGeneracion()
     * @version 1.0.
     **/
    public Vector totalesReporteDeGeneracion() throws SQLException{
        return rmtdao.totalesReporteDeGeneracion();
    }
    
    /**
     * Obtiene todos los registros de las planilla en un per�odo cuya diferencia entre la fecha
     * del reporte y la fecha del reporte planeado exceda los 30 mins.
     * @autor Ing. Tito Andr�s Maturana D.
     * @param fechaI Fecha inicial del per�odo
     * @param fechaF Fecha final del per�odo
     * @throws SQLException
     * @return Arreglo con los registros
     * @see com.tsp.operation.model.DAOS.RepMovTraficoDAO.java#reporteCoherencia(String, String)
     * @version 1.0.
     **/
    public Vector reporteCoherencia(String fechaI, String fechaF) throws SQLException{
        return rmtdao.reporteCoherencia(fechaI + " 00:00:00", fechaF + " 23:59:59.999");
    }
    
    /**
     * Actualiza el registro en ingreso trafico y trafico para registrarlo como varado
     * @autor Ing. Karen Reales.
     * @throws SQLException
     * @version 1.0.
     **/
    public void registrarVarado() throws SQLException{
        rmtdao.registrarVarado();
    }
    /**
     * Actualiza el registro en ingreso trafico y trafico para quitar la marca de varado
     * @autor Ing. Karen Reales.
     * @throws SQLException
     * @version 1.0.
     **/
    public void registrarReinicio() throws SQLException{
        rmtdao.registrarReinicio();
    }
    /**
     * M�todo que setea un Reporte de movimiento de tr�fico
     * @autor.......Armando Oviedo
     * @param.......Objeto RepMovTrafico
     * @version.....1.0.
     **/
    public void setRMT(RepMovTrafico rmt){
        rmtdao.setRmt(rmt);
    }
    
    /**
     * Obtiene los registros de los vehiculos varados y demorados del archivo de reporte
     * movimiento tr�fico
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws <code>SQLException</code>
     * @version 1.0
     */
    public void reporteVehiculosDemoradosPlaca() throws SQLException{
        rmtdao.reporteVehiculosDemoradosPlaca();
    }
    /**
     * Registra en la tabla de entrega_destinatarios cuando el reporte es de entrega
     * parcial o entrega final
     * @autor Ing. Karen Reales.
     * @throws SQLException
     * @version 1.0.
     **/
    public String registrarDestinatarios(Remesa rem) throws SQLException{
        return rmtdao.registrarDestinatarios(rem);
    }
    /**
     * Obtiene las distintas planillas en un puesto de control dado
     * @autor Ing. Karen Reales.
     * @throws SQLException
     * @version 1.0.
     **/
    public void buscarPlanillas(String numpla, String pco) throws SQLException{
        rmtdao.buscarPlanillas(numpla, pco);
    }
    /**
     * Getter for property planillas.
     * @return Value of property planillas.
     */
    public java.util.Vector getPlanillas() {
        return rmtdao.getPlanillas();
    }
    
    /**
     * Setter for property planillas.
     * @param planillas New value of property planillas.
     */
    public void setPlanillas(java.util.Vector planillas) {
        rmtdao.setPlanillas(planillas);
    }
    
    /**
     * Getter for property planillasReportes.
     * @return Value of property planillasReportes.
     */
    public java.util.Vector getPlanillasReportes() {
        return planillasReportes;
    }
    
    
    
    /**
     * Getter for property planillasReportes.
     * @return Value of property planillasReportes.
     */
    public void setPlanillasReportes(Vector planillasReportes) {
        this.planillasReportes=planillasReportes;
    }
    
    /**
     * M�todo que busca una planilla en tr�fico, ingreso tr�fico y en planilla la carga en un objeto DatosPlanillaRMT
     * @autor.......Karen Reales
     * @param.......numero de planilla
     * @throws......SQLException
     * @version.....1.0.
     **/
    public DatosPlanillaRMT obtenerPlanilla(String numpla) throws SQLException{
        return rmtdao.obtenerPlanilla(numpla);
    }
    
    
    
    /**
     * M�todo que actualiza las tablas de ingreso tr�fico y tr�fico despu�s de una inserci�n o modificaci�n
     * @autor.......Jose de la rosa
     * @throws......SQLException
     * @version.....1.0.
     **/
    public String actualizarDatosTrafico(double duracion) throws SQLException{
        return rmtdao.actualizarDatosTrafico(duracion);
    }
    
    /**
     * M�todo que retorna los datos de la planilla q su remesa es de aduana
     * @autor.......Jose de la rosa
     * @param.......Numero de la planilla
     * @throws......SQLException
     * @version.....1.0.
     * @return.......Vector de remesas: numero de la remesa, origen de la remesa
     **/
    public Vector planillaRemesasConAduana(String numpla) throws SQLException{
        return rmtdao.planillaRemesasConAduana(numpla);
    }
    
    /**
     * M�todo que retorna si los el pais de origen de la remesa es diferente al pais origen del PC
     * @autor.......Jose de la rosa
     * @param.......codigo de la ciudad de origen de la remesa, codigo de ciudad del pc
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public boolean remesasInternacionales(String orirem, String ciupc) throws SQLException{
        return rmtdao.remesasInternacionales(orirem, ciupc);
    }
    
    /**
     * M�todo que retorna si la ciudad de PC no es de frontera
     * @autor.......Jose de la rosa
     * @param.......codigo de ciudad del pc
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public boolean pcNoEsFrontera(String ciupc) throws SQLException{
        return rmtdao.pcNoEsFrontera(ciupc);
    }
    
    /**
     * M�todo que modifica la aduana en 'N' de la remesa
     * @autor.......Jose de la rosa
     * @param.......numero de la remesa
     * @throws......SQLException
     * @version.....1.0.
     * @return.......
     **/
    public String actualizacionAduana(String numrem) throws SQLException{
        return rmtdao.actualizacionAduana(numrem);
    }
    
    /**
     * M�todo que modifica la aduana en 'N' de la remesa
     * @autor.......Jose de la rosa
     * @param.......numero de la remesa
     * @throws......SQLException
     * @version.....1.0.
     * @return.......
     **/
    public String actualizacionProxFecha(String fecha, String numpla, String observacion) throws SQLException{
        return rmtdao.actualizacionProxFecha(fecha, numpla, observacion);
    }
    /**
     * M�todo que modifica la la observacion de ingreso trafico
     * @autor.......Jose de la rosa
     * @param.......numero de la remesa
     * @throws......SQLException
     * @version.....1.0.
     * @return.......
     **/
    public String actualizacionObservacion(String observacion, String numpla) throws SQLException{
        return rmtdao.actualizacionObservacion(observacion, numpla);
    }
    public void updReport(RepMovTrafico rmt, String olddate,boolean IsUltimo) throws SQLException{
        try{
            String zona = getZona(rmt.getUbicacion_procedencia());
            rmt.setZona(zona);
            rmtdao.setRMT(rmt);
            rmtdao.updReport(olddate, IsUltimo);
        }
        catch(SQLException ex){
            ex.getMessage();
        }
    }
    /**
     * Setter for property Planilla.
     * @param Planilla New value of property Planilla.
     */
    public void setPlanilla(java.lang.String Planilla) {
        this.Planilla = Planilla;
    }
    
    /**
     * Getter for property Planilla.
     * @return Value of property Planilla.
     */
    public java.lang.String getPlanilla() {
        return Planilla;
    }
    /**
     * M�todo que obtiene la fecha del reporte anterior
     * @autor.......Ivan Gomez
     * @see ........ getFECHA_ANTERIOR - RepMovTraficoDAO
     * @throws......SQLException
     * @version.....1.0.
     **/
    public String getFechaAnterior() throws SQLException{
        return rmtdao.getFECHA_ANTERIOR();
    }
    /**
     * M�todo que obtiene la fecha del Ultimo reporte
     * @autor.......Ivan Gomez
     * @see ........ getFECHA_ULTIMO - RepMovTraficoDAO
     * @throws......SQLException
     * @version.....1.0.
     **/
    public String getFechaUltimo() throws SQLException{
        return rmtdao.getFECHA_ULTIMO();
    }
    /**
     * M�todo para saber si es un solo registro
     * @autor.......Ivan Gomez
     * @see ........ isUnSoloRegistro - RepMovTraficoDAO
     * @throws......SQLException
     * @version.....1.0.
     **/
    public boolean isUnSoloRegistro() throws SQLException{
        return rmtdao.isUnSoloRegistro();
    }
    public String[] BuscarRegStatus(String numpla)throws SQLException{
        return rmtdao.BuscarStatus(numpla);
    }
    /**
     * M�todo que retorna true si existe un una via de una planilla
     * @autor.......Jose de la rosa
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public boolean existeVia(String origen, String destino, String secuencia) throws SQLException{
        return rmtdao.existeVia(origen, destino, secuencia);
    }
    public String cambiarViaPlanilla(boolean sw) throws SQLException{
        return rmtdao.cambiarViaPlanilla(sw);
    }
    /**
     * M�todo que retorna si la planilla ya tiene un gegistro en esa hora
     * @autor.......Jose de la rosa
     * @param.......fecha a evaluar
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean.
     **/
    public boolean existePlanillaConFecha(String numpla, String fecha) throws SQLException{
        return rmtdao.existePlanillaConFecha(numpla, fecha);
    }
    /**
     * M�todo que modifica la placa, el propietario y el conductor de una planilla
     * @autor.......Jose de la rosa
     * @param.......numero de la remesa
     * @throws......SQLException
     * @version.....1.0.
     * @return.......
     **/
    public String actualizacionPlacaConductor(String cedcon, String nomcon, String cedprop, String nomprop, String placa, String planilla) throws SQLException{
        return rmtdao.actualizacionPlacaConductor(cedcon, nomcon, cedprop, nomprop, placa, planilla);
    }
    /**
     * Busca todos los reportes de las planillas que no se reportaron en el PC correspondiente, o siguient en la ruta.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param fecha_ini Fecha inicial del per�odo
     * @param Fecha_fin Fecha final del reporte
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void reportePuestosControl(String fecha_ini, String fecha_fin) throws SQLException{
        rmtdao.reportePuestosControl(fecha_ini, fecha_fin);
    }
    /**
     * M�todo que actualiza las tablas de ingreso tr�fico para marcar los despachos
     *de cadena.
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.
     **/
    public String marcarCadena(String numpla) throws SQLException{
        return rmtdao.marcarCadena(numpla);
    }
    /**
     * Obtiene las distintas planillas en un puesto de control dado
     * @autor Ing. Karen Reales.
     * @throws SQLException
     * @version 1.0.
     **/
    public void buscarPlanillasReinicio(String numpla, String pco) throws SQLException{
        rmtdao.buscarPlanillasReinicio(numpla, pco);
    }
    /**
     * M�todo que actualiza las tablas de ingreso tr�fico para desmarcar los despachos
     *de cadena.
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.
     **/
    public String desmarcarCadena(String numpla) throws SQLException{
        return rmtdao.desmarcarCadena(numpla);
    }
    /********** RepMovTraficoService *************/
    /**
     * M�todo que actualiza las zonas de ingreso tr�fico y tr�fico
     * @autor.......Jose de la rosa
     * @throws......SQLException
     * @version.....1.0.
     **/
    public String actualizarZonaTrafico() throws SQLException{
        return rmtdao.actualizarZonaTrafico();
    }
    /**
     * M�todo que retorna true si el tipo de documento es una detencion
     * @autor.......Ivan DArio Gomez
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public boolean isDetencion(String tipoReporte) throws SQLException{
        return rmtdao.isDetencion(tipoReporte);
    }
    /**
     * M�todo obtenerUrbanoUrbano, que obtiene las planillas cuya zona = 000
     * y esten en campo urbano = 'S'
     * @autor.......Diogenes Bastidas
     * @throws......SQLException
     * @version.....1.0.
     **/
    public Vector obtenerUrbanoUrbano() throws SQLException{
        return rmtdao.obtenerUrbanoUrbano();
        
    }
    
    /**
     * M�todo obtenerUrbanoNoAplica, que obtiene las planillas cuya zona = 000
     * y esten en campo urbano = 'N'
     * @autor.......Diogenes Bastidas
     * @throws......SQLException
     * @version.....1.0.
     **/
    public Vector obtenerUrbanoNoAplica() throws SQLException{
        return rmtdao.obtenerUrbanoNoAplica();
    }
    
    /**
     * M�todo obtenerVacios, que obtiene las planillas que esten vacias
     * @autor.......Diogenes Bastidas
     * @throws......SQLException
     * @version.....1.0.
     **/
    public Vector obtenerVacios() throws SQLException{
        
        return rmtdao.obtenerVacios();
        
    }
    
    /**
     * eliminarIngresoTrafico, elimina de la tabla ingreso trafico
     * @autor Ing. Diogenes Bastidas
     * @param numero de la planilla
     * @throws SQLException
     * @version 1.0
     **/
    public void eliminarIngresoTrafico(String numpla) throws SQLException{
        rmtdao.eliminarIngresoTrafico(numpla);
    }
    public String actualizacionProxFecha(String fecha, String numpla, String observacion, String zona) throws SQLException{
        return rmtdao.actualizacionProxFecha(fecha, numpla, observacion, zona);
    }
    /**
     * M�todo que obtiene laa planillas anuladas en MIMS y que se encuentren en ingreso trafico
     * @autor.......Jose De La Rosa
     * @throws......SQLException
     * @version.....1.0.
     **/
    public Vector planillaAnuladasMimsIngresoTrafico( ) throws SQLException{
        return rmtdao.planillaAnuladasMimsIngresoTrafico();
    }
    /**
     * M�todo que actualiza los campos  de ultimo reporte en ingreso trafico y trafico
     * @autor.......Jose de la rosa
     * @throws......SQLException
     * @version.....1.0.
     **/
    public String actualizarUltimotPC() throws SQLException{
        return rmtdao.actualizarUltimotPC();
    }
    /**
     * M�todo que actualiza las tablas de ingreso tr�fico y tr�fico despu�s de una inserci�n o modificaci�n
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     **/
    public String actualizarTablaIngresoTrafico() throws SQLException{
        return rmtdao.actualizarTablaIngresoTrafico();
    }
    /**
     * M�todo que retorna el nombre de un puesto de control
     * @autor.......Armando Oviedo
     * @param.......c�digo pc
     * @throws......SQLException
     * @version.....1.0.
     * @return.......datos del puesto de control
     **/
    public Vector getDatosCiudad(String codciu) throws SQLException{
        return rmtdao.getDatosCiudad( codciu );
    }
    /**
     * M�todo obtenerSJSinTrafico, que obtiene las planillas cuyas  remesas
     * el  standar esta entablaben como estandar sin trafico
     * @autor.......Diogenes Bastidas
     * @throws......SQLException
     * @version.....1.0.
     **/
    public Vector obtenerSJSintrafico() throws SQLException{
        return rmtdao.obtenerSJSinTrafico();
    }
    /**
     * M�todo que actualiza los campos  de ultimo reporte en ingreso trafico y trafico cuando
     * el tipo de reporte es llegada
     * @autor.......David Pina Lopez
     * @throws......SQLException
     * @version.....1.0.
     **/
    public String actualizarUltimotPCLlegada() throws SQLException{
        return rmtdao.actualizarUltimotPCLlegada();
    }
    /* Getter for property rmt.
     * @return Value of property rmt.
     */
    public RepMovTrafico getRmt() {
        return rmtdao.getRmt();
    }
    /**
     * M�todo que actualiza los campos  de ultimo reporte en ingreso trafico y trafico cuando
     * el tipo de reporte es llegada
     * @autor.......David Pina Lopez
     * @throws......SQLException
     * @version.....1.0.
     **/
    public String actualizacionProxFechaRep(String fecha, String numpla, String observacion, String zona, String tiporep) throws SQLException{
        return rmtdao.actualizacionProxFechaRep( fecha, numpla, observacion, zona, tiporep );
    }
    /**
     * M�todo que retorna true o false si la planilla ha tenido algun reporte de trafico.
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public boolean tieneReporte(String planilla) throws SQLException{
        return rmtdao.tieneReporte(planilla);
    }
    
    /**
     * verifica si una planilla tiene Reporte de Entrega
     * @autor Ing. Henry A. Osorio Gonzalez
     * @param planilla
     * @throws SQLException
     * @version 1.0.
     **/
    public boolean tieneReporteEntraga(String planilla) throws SQLException{
        return rmtdao.tieneReporteEntraga(planilla);
    }
    /**
     * Obtiene los registros dentro de un per�odo dado, teniendo en cuaneta el campo 'creation_date'
     * @autor Ing. Tito Andr�s Maturana D.
     * @param fechaI Fecha inicial del per�odo
     * @param fechaF Fecha final del per�odo
     * @throws SQLException
     * @see com.tsp.operation.model.DAOS.RepMovTraficoDAO.java#detalleReporteDeOportunidad(String, String)
     * @version 1.0.
     **/
    public Vector detalleReporteDeOportunidad(String fechaI, String fechaF) throws SQLException{
        return rmtdao.detalleReporteDeOportunidad(fechaI + " 00:00:00", fechaF + " 23:59:59.999");
    }
    
 
    
    
    //AMATURANA 31.10.2006
    /**
     * Obtiene los registros dentro de un per�odo dado, teniendo en cuaneta el campo 'creation_date'
     * @autor Ing. Tito Andr�s Maturana D.
     * @param fechaI Fecha inicial del per�odo
     * @param fechaF Fecha final del per�odo
     * @throws SQLException
     * @see com.tsp.operation.model.DAOS.RepMovTraficoDAO.java#detalleReporteDeOportunidad(String, String)
     * @version 1.0.
     **/
    public Vector detalleReporteDeOportunidadCMSA(String fechaI, String fechaF) throws SQLException{
        return rmtdao.detalleReporteDeOportunidadCMSA(fechaI + " 00:00:00", fechaF + " 23:59:59.999");
    }
    
    /**
     * Obtiene las estad�sticas de registros oportunos y inoportunos a partir de los detalles
     * @autor Ing. Tito Andr�s Maturana D.
     * @param detall Arreglo con los registros del archivo de rep-mov_trafico en un per�odo
     * @throws SQLException
     * @see #detalleReporteDeOportunidad(String, String)
     * @version 1.0.
     **/
    public Vector reporteDeOportunidad(Vector det){
        Vector rep = new Vector();
        
        for(int i=0; i<det.size(); i++){
            
            Vector vec = (Vector) det.elementAt(i);
            RepMovTrafico rmt = (RepMovTrafico) vec.elementAt(0);
            String usuario = (String) vec.elementAt(1);
            
            int c = 0; //Cantidad de registros
            int coport = 0; //Cantidad de registros oportunos
            int cinoport = 0; //Cantidad de registros inoportunos
            for( int j=i; j<det.size(); j++){
                Vector vec1 = (Vector) det.elementAt(j);
                RepMovTrafico rmt1 = (RepMovTrafico) vec1.elementAt(0);
                
                
                if( rmt.getCreation_user().compareTo(rmt1.getCreation_user())==0 ){
                    c++;
                    long mm = com.tsp.util.Util.minutosTranscurridos(rmt1.getLast_update(), rmt1.getFechaReporte());
                    
                    if( mm<-60 || mm>60 ){
                        cinoport++;
                    } else {
                        coport++;
                    }
                    
                    
                } else {
                    break;
                }
            }
            i+=(c-1);
            RepGral obj = new RepGral();
            obj.setUsuario(usuario);
            obj.setLogin(rmt.getCreation_user());
            obj.setRegistros(String.valueOf(c));
            obj.setOportunos(String.valueOf(coport));
            obj.setInoportunos(String.valueOf(cinoport));
            obj.setPorcent_opor(coport != 0 ? String.valueOf(com.tsp.util.Util.redondear(((double) coport/ (double) c)*100 , 1)) : "0");
            obj.setPorcent_inop(cinoport != 0 ? String.valueOf(com.tsp.util.Util.redondear(((double) cinoport/(double) c)*100 , 1)) : "0");
            rep.add(obj);
        }
        return rep;
    }
    
    /**
     * Obtiene los registros de los vehiculos varados y demorados del archivo de reporte
     * movimiento tr�fico
     * @autor Ing. Tito Andr�s Maturana
     * @param plaveh Placa del veh�culo
     * @param cliente C�digo del cliente
     * @param ori C�digo de la ciudad de origen
     * @param dest C�digo de la ciudad de destino
     * @param agencia C�digo de la agencia
     * @throws <code>SQLException</code>
     * @see com.tsp.operation.model.DAOS.RepMovTraficoDAO.java#reporteVehiculosDemoradosPlaca(String, String, String, String, String)
     * @version 1.0
     */
    public void reporteVehiculosDemoradosPlaca(String plaveh, String cliente, String ori, String dest, String agencia, String fechai, String fechaf, String tiprep, String zona) throws SQLException{
        rmtdao.reporteVehiculosDemoradosPlaca(plaveh, cliente, ori, dest, agencia, fechai, fechaf, tiprep, zona);
    }
    
     /**********************************************************************
     *Metodo para obtener el reporte de vahiculos varados del cliente dado
     *            en el periodo dado
     *@param: String cliente cliente al cual se le van a consultar los varados
     *@param: String inicio fecha inicial
     *@param: String fin fecha final
     *@param: String dstrct distrito
     *@return: Vector de Hashtable con el reporte
     *@throws: Exception en caso de un error
     ************************************************************************/
    public Vector reporteVaradosCliente( String cliente, String inicio, String fin, String dstrct) throws Exception{
       return rmtdao.reporteVaradosCliente(cliente, inicio, fin, dstrct );
    }
    
         /**
     * M�todo que retorna true o false si la planilla tiene entrega.
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public boolean tieneEntrega(String placa) throws SQLException{
        return rmtdao.tieneEntrega(placa);
    }
    
      /**
     * M�todo que actualiza en la tabla tr�fico 
     * @autor.......Ivan DArio Gomez
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void updateTrafico(RepMovTrafico rmt) throws SQLException{
        rmtdao.updateTrafico(rmt);
    }
    
    /**
     * Determina si se puede o no crear/modificar un plan de viaje por un usuario
     * diferente a Tr�fico.
     * @autor Ing. Andr�s Maturana
     * @param numpla N�mero de la planilla
     * @throws <code>SQLException</code>
     * @version 1.0
     */
    public boolean permitirPlanViaje(String numpla) throws SQLException {
        String salida = rmtdao.reporteSalida(numpla);
        if( salida.length()!=0 ){
            int reps = rmtdao.reportesPosteriores(numpla, salida);
            if( reps >= 1 ){
                return false;
            }
        }
        
        return true;
    }
      /**
     * Obtiene los registros de los vehiculos varados y demorados del archivo de reporte
     * movimiento tr�fico
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws <code>SQLException</code>
     * @version 1.0
     */
    public void reporteVehiculosDemoradosPlacaMod(String plaveh, String cliente, String ori, String dest, String agencia, String fechai, String fechaf, String tiprep, String zona) throws SQLException{
        rmtdao.reporteVehiculosDemoradosPlacaMod(plaveh, cliente, ori, dest, agencia, fechai, fechaf, tiprep, zona);
    }
    
    /**
     * M�todo que retorna el nombre DEL TPO DE REPORTE
     * @autor.......iVAN gOMEZ
     * @param.......String tipo_reporte
     * @throws......SQLException
     * @version.....1.0.
     * @return.......Nombre del reporte
     **/
    public String getNombreReporte(String tipo_reporte)throws SQLException{
        return rmtdao.getNombreReporte(tipo_reporte);
    }

}
