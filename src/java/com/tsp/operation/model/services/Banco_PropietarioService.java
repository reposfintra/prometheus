/*
 * ClienteService.java
 *
 * Created on 13 de noviembre de 2004, 9:49
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;
import com.tsp.operation.model.DAOS.*;
/**
 *
 * @author  HENRY
 */
public class Banco_PropietarioService {
    private Propietario_bancoDAO PbancoDAO;
    private Banco_Propietario BancoPo;
 
    
    /** Creates a new instance of ClienteService */
    public Banco_PropietarioService() {
        PbancoDAO = new Propietario_bancoDAO();
        this.BancoPo = new Banco_Propietario();
    }
    public Banco_PropietarioService(String dataBaseName) {
        PbancoDAO = new Propietario_bancoDAO(dataBaseName);
        this.BancoPo = new Banco_Propietario();
    }
    
    
    /**
     * M�todo para agregar una demora al archivo de demoras. 
     * @autor Tito Andr�s Maturana
     * @param demora Instancia de la clase Demora.
     * @see com.tsp.operation.model.DAOS.DemorasDAO#agregarDemora(Demora)
     * @throws SQLException
     * @version 1.0
     */
    public void agregarBancoPropietario(Banco_Propietario BancoPo)throws SQLException{
        this.PbancoDAO.agregarBancoPropietario(BancoPo);        
    }
     public boolean existeBancoP(String Propietario)throws SQLException{  
        return PbancoDAO.existeBancoP(Propietario);
    }
     public void BusquedaBancoPro(String Propietario,String HC,String Banco ) throws Exception {
         this.PbancoDAO.BusquedaBancoPro(Propietario, HC, Banco);
     }
     public java.util.Vector getVector() {
        return PbancoDAO.getVector();
    }
     public void BusquedaDatosBancoPro(String Propietario,String HC,String Distrito ) throws Exception {
         this.PbancoDAO.BusquedaDatosBancoPro(Propietario,HC,Distrito ) ;
     }
     public void ModificarBancoPropietario(Banco_Propietario BancoPo)throws SQLException{
         this.PbancoDAO.ModificarBancoPropietario(BancoPo);  
     }
     public void AnularBancoPro(String distrito,String Propietario,String hc) throws Exception {
         this.PbancoDAO.AnularBancoPro(distrito,Propietario,hc);
     }
  
}
