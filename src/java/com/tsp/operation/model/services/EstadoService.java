/*
 * EstadoService.java
 *
 * Created on 1 de marzo de 2005, 01:09 PM
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;


/**
 *
 * @author  DIBASMO
 */
public class EstadoService {
    
    private EstadoDAO estadod; 
    
    /** Creates a new instance of EstadoService */
    public EstadoService() {
        estadod = new EstadoDAO();
    }
    public EstadoService(String dataBaseName) {
        estadod = new EstadoDAO(dataBaseName);
    }
    
    public void insertarPais(Estado estado)throws SQLException {
       try{
           estadod.setEstado(estado);
           estadod.insertarEstado();
       }
       catch(SQLException e){
           throw new SQLException(e.getMessage());
       }
   }
    public boolean existeEstado(String cod_pais, String cod_depto) throws SQLException {
       return estadod.existeEstado(cod_pais,cod_depto); 
   }
    public boolean existeEstadoAnulado(String cod_pais, String cod_depto) throws SQLException {
       return estadod.existeEstadoAnulado(cod_pais,cod_depto); 
   }
   public boolean existeEstadonom(String nom_pais, String nom_depto) throws SQLException {
       return estadod.existeEstadonom(nom_pais, nom_depto);
   }   
    public List obtenerEstados() throws SQLException {
       List Estados = null;
       Estados = estadod.obtenerEstados();
       return Estados;
    }   
    public Vector obtEstados() throws SQLException {
       Vector VecEstados = null;
       VecEstados = estadod.obtEstados();
       return VecEstados;
    }   
    public void Estados (String pais) throws SQLException {
       try{
           estadod.Estados(pais);
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
   }
    public void listarEstado (String pais) throws SQLException {
       try{
           estadod.listarEstado(pais);
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
   }
    public void listarEstadoGeneral ( ) throws SQLException {
       try{
           estadod.listarEstadoGeneral();
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
   }
    public Estado obtenerestado()throws SQLException{
       return estadod.obtenerEstado();
    }
   
    public void buscarestado (String cod_pais, String cod_depto) throws SQLException {
       try{
           estadod.buscarestado(cod_pais, cod_depto);     
       }
       catch(SQLException e){
           throw new SQLException ( e.getMessage()); 
       }       
    }
    public void buscarestadoXnom (String nom_pais, String nom_depto, String zona) throws SQLException {
       try{
           estadod.buscarestadosXnom(nom_pais, nom_depto, zona );   
       }
       catch(SQLException e){
           throw new SQLException ( e.getMessage()); 
       }       
   }
    public void modificarestado (Estado estado) throws SQLException {
       try{
           estadod.setEstado(estado);
           estadod.modificarestado();
       }
       catch(SQLException e){
           throw new SQLException(e.getMessage());
       }
   }
    public void Activarestado (Estado estado) throws SQLException {
       try{
           estadod.setEstado(estado);
           estadod.Activarestado();
       }
       catch(SQLException e){
           throw new SQLException(e.getMessage());
       }
   }
    public void anularestado(Estado estado)throws SQLException {
       try{
           estadod.setEstado(estado);
           estadod.anularestado();
       }
       catch(SQLException e){
           throw new SQLException(e.getMessage());
       }
   }
   public boolean existeEstadosxPais( String cpais ) throws SQLException {
       return estadod.existeEstadosxPais( cpais ); 
   }   
   /**
     * Metodo <tt>buscarEstadosxNombre</tt>, obtiene los estados segun paramtros de busqueda
     * @autor : Ing. Sandra Escalante
     * @param codigo del pais, frase nombre de estado (String)     
     * @see buscarEstadosxNombre (String pais, String frase)
     * @version : 1.0
     */
    public void buscarEstadosxNombre (String pais, String frase) throws SQLException {
     try{
           estadod.buscarEstadosxNombre(pais,frase);     
       }
       catch(SQLException e){
           throw new SQLException ( e.getMessage()); 
       }       
    }
    
}
