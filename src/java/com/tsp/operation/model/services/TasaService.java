/*
 * TasaService.java
 *
 * Created on 28 de junio de 2005, 11:44 AM
 */

package com.tsp.operation.model.services;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.util.Utility;
/**
 *
 * @author  DIOGENES
 */
public class TasaService {
    private TasaDAO tasa_d;
    private Tasa tasa;
    private String   msj ;    // Obtiene el comentario de la validación.

    /** Creates a new instance of TasaService */
    public TasaService() {
        tasa_d = new TasaDAO();
    }
    public TasaService(String dataBaseName) {
        tasa_d = new TasaDAO(dataBaseName);
    }
    
    
 

 public void reset(){
        msj = "";
 }
    public Vector obtVecTasa(){
        return tasa_d.obtVecTasa();
    }
    
    public Tasa obtenerTasa(){
        return tasa_d.obtenerTasa();
    }
    public void insertarTasa(Tasa tasa) throws SQLException {
        try{
            tasa_d.setTasa(tasa);
            tasa_d.insertarTasa();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public boolean existeTasa(String moneda1, String moneda2, String fecha, String cia) throws SQLException {
        try{
            return tasa_d.existeTasa(moneda1, moneda2, fecha, cia);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public boolean existeTasaAnulado(String moneda1, String moneda2, String fecha, String cia) throws SQLException {
        try{
            return tasa_d.existeTasaAnulado(moneda1, moneda2, fecha, cia);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void listarTasa() throws SQLException {
        try{
            tasa_d.listarTasa();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void buscarTasa(String moneda1, String moneda2, String fecha, String cia ) throws SQLException {
        try{
            tasa_d.buscarTasa(moneda1, moneda2, fecha, cia);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void modificarTasa(Tasa tasa) throws SQLException {
        try{
            tasa_d.setTasa(tasa);
            tasa_d.modificarTasa();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void anularTasa(Tasa tasa) throws SQLException {
        try{
            tasa_d.setTasa(tasa);
            tasa_d.anularTasa();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void activarTasa(Tasa tasa) throws SQLException {
        try{
            tasa_d.setTasa(tasa);
            tasa_d.activarTasa();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void BuscarTasaX(String moneda1, String moneda2, String fecha, String cia) throws SQLException {
        try{
            tasa_d.BuscarTasaX(moneda1, moneda2, fecha, cia);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    //kreales
    public double buscarValor(String moneda1, String moneda2, String fecha, float valor ) throws SQLException {
        return tasa_d.buscarValor(moneda1, moneda2, fecha, valor);
    }
    /**
     * Metodo buscarTasa, busca el valor de la tasa de cambio dada dos monedas,
     * obteniendo un objeto que tiene la fecha, el valor de la tasa y valor calculado
     * @param: Moneda1 moneda inicial
     * @param: Moneda2 moneda final
     * @param: fecha Fecha del cambio
     * @param: valor Valor a cambiar
     * @autor : Ing. Diogenes Bastidas.
     * @version : 1.0
     */
    public Tasa buscarValorTasa(String moneda_local, String moneda1, String moneda2, String fecha ) throws Exception {
        tasa = null;
        //verifico si las monedas son iguales
        if(moneda1.equals(moneda2)){
            ////System.out.println("Monedas Iguales");
            tasa = new Tasa();
            tasa.setFecha(fecha);
            tasa.setValor_tasa1(1);
            tasa.setValor_tasa2(1);
            tasa.setValor_tasa(1);
        }
        else{
            try{
                ////System.out.println("Monedas diferentes");
                //busco la tasa para realizar conversion
                tasa = tasa_d.buscarDatosTasa(moneda1, moneda2, fecha);
                if (tasa == null){
                    //verifico si las monedas son diferentes a la moneda local para realizar un
                    //puente para la convercion,es decir de la moneda 1 a la moneda local y de la moneda local a moneda 2
                    if( !moneda1.equals(moneda_local) && !moneda2.equals(moneda_local)){

                        //realizo la primera busqueda moneda 1 -> moneda local
                        Tasa tasa1 = tasa_d.buscarDatosTasa(moneda1, moneda_local, fecha);
                        if(tasa1!=null){

                            //realizo la segunda busqueda moneda local -> moneda 2
                            Tasa tasa2 = tasa_d.buscarDatosTasa(moneda_local, moneda2, tasa1.getFecha());
                            if(tasa2 != null){
                                if(tasa1.getFecha().equals(tasa2.getFecha())){
                                    tasa = new Tasa();
                                    tasa.setFecha(tasa1.getFecha());
                                    tasa.setValor_tasa1(tasa1.getValor_tasa1());
                                    tasa.setValor_tasa2(tasa2.getValor_tasa2());
                                    tasa.setValor_tasa(tasa1.getValor_tasa1()*tasa2.getValor_tasa2());
                                    tasa.setDiferencia(tasa1.getDiferencia());
                                }

                            }
                        }
                    }
                }
            }catch(Exception e){
                throw new Exception(e.getMessage());
            }
        }
        return tasa;
    }
    
    public static void main(String a []){
        TasaService prog = new TasaService();
        ////System.out.println("Inicia");
        try{
            Tasa obj =  prog.buscarValorTasa("PES", "BOL", "DOL", "2006-04-25");
            if(obj!=null){
                
                ////System.out.println("Valor valor 1 "+ obj.getValor_tasa1());
                ////System.out.println(obj.getValor_tasa() * 1000 + "DOL");
                ////System.out.println("");
                ////System.out.println((obj.getValor_tasa1() * 1000) + "PES");
                ////System.out.println((obj.getValor_tasa1() * 1000) * obj.getValor_tasa2() + "DOL" );
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        ////System.out.println("Fin");
    }
    /**
       * Método que verifica que exista tasa de conversión para el dia de hoy
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
    public  boolean  isTasaHoy(String distrito) throws Exception{
        reset();
        boolean estado = false;
        try{
            String  newLinea  = "<BR>";
            String  title     = "No es posible realizar el proceso:" + newLinea;
            String  error     = "- No hay tasa de conversión de moneda para ";
            
            
            String hoy    =  Utility.getHoy("-");
            String DOL    = "DOL";
            String BOL    = "BOL";
            String PES    = "PES";
            
            
            ChequeXFacturaDAO dao    =  new ChequeXFacturaDAO(this.tasa_d.getDatabaseName());
            String     monedaLocal   =  dao.getMonedaLocal(distrito);
            
            
            TasaService  svc  =  new  TasaService(this.tasa_d.getDatabaseName());
                
         // PESO
            if( monedaLocal.equals(PES)   ){
                
                Tasa   obj1  =  svc.buscarValorTasa(monedaLocal,  PES , DOL , hoy);
                Tasa   obj2  =  svc.buscarValorTasa(monedaLocal,  PES , BOL , hoy);
                if( obj1!= null   &&   obj2!=null)
                    estado = true;
                else{
                    if ( obj1==null )  msj += error + PES +" -> " +  DOL + newLinea;
                    if ( obj2==null )  msj += error + PES +" -> " +  BOL + newLinea;                    
                }
                
            }
            
         // BOLIVAR
            if( monedaLocal.equals(BOL)   ){
                
                Tasa   obj1  =  svc.buscarValorTasa(monedaLocal,  BOL , DOL , hoy);
                Tasa   obj2  =  svc.buscarValorTasa(monedaLocal,  BOL , PES , hoy);
                if( obj1!= null   &&   obj2!=null)
                    estado = true;
                else{
                    if ( obj1==null )  msj += error + BOL +" -> " +  DOL + newLinea;
                    if ( obj2==null )  msj += error + BOL +" -> " +  PES + newLinea;                    
                }
                
            }   
            
            
         // DOLAR
            if( monedaLocal.equals(DOL)  ){
                
                Tasa   obj1  =  svc.buscarValorTasa(monedaLocal,  DOL , BOL , hoy);
                Tasa   obj2  =  svc.buscarValorTasa(monedaLocal,  DOL , PES , hoy);
                if( obj1!= null   &&   obj2!=null)
                    estado = true;
                else{
                    if ( obj1==null )  msj += error + DOL +" -> " +  BOL + newLinea;
                    if ( obj2==null )  msj += error + DOL +" -> " +  PES + newLinea;                    
                }
                
                
            }
            
            
            if( !msj.equals("") )
                msj =  title + msj;
            
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return estado;
        
    }
    


   /**
       * Método que verifica que exista tasa de conversión para la fecha
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
    public  boolean  isTasaFecha(String distrito, String fecha) throws Exception{
        reset();
        boolean estado = false;
        try{
            String  newLinea  = "<BR>";
            String  title     = "No es posible realizar el proceso:" + newLinea;
            String  error     = "- No hay tasa ["+  fecha  +"] de conversión de moneda para ";
            
            
            String DOL    = "DOL";
            String BOL    = "BOL";
            String PES    = "PES";
            
            
            ChequeXFacturaDAO dao    =  new ChequeXFacturaDAO();
            String     monedaLocal   =  dao.getMonedaLocal(distrito);
            
            
            TasaService  svc  =  new  TasaService();
                
         // PESO
            if( monedaLocal.equals(PES)   ){
                
                Tasa   obj1  =  svc.buscarValorTasa(monedaLocal,  PES , DOL , fecha);
                Tasa   obj2  =  svc.buscarValorTasa(monedaLocal,  PES , BOL , fecha);
                if( obj1!= null   &&   obj2!=null)
                    estado = true;
                else{
                    if ( obj1==null )  msj += error + PES +" -> " +  DOL + newLinea;
                    if ( obj2==null )  msj += error + PES +" -> " +  BOL + newLinea;                    
                }
                
            }
            
         // BOLIVAR
            if( monedaLocal.equals(BOL)   ){
                
                Tasa   obj1  =  svc.buscarValorTasa(monedaLocal,  BOL , DOL , fecha);
                Tasa   obj2  =  svc.buscarValorTasa(monedaLocal,  BOL , PES , fecha);
                if( obj1!= null   &&   obj2!=null)
                    estado = true;
                else{
                    if ( obj1==null )  msj += error + BOL +" -> " +  DOL + newLinea;
                    if ( obj2==null )  msj += error + BOL +" -> " +  PES + newLinea;                    
                }
                
            }   
            
            
         // DOLAR
            if( monedaLocal.equals(DOL)  ){
                
                Tasa   obj1  =  svc.buscarValorTasa(monedaLocal,  DOL , BOL , fecha);
                Tasa   obj2  =  svc.buscarValorTasa(monedaLocal,  DOL , PES , fecha);
                if( obj1!= null   &&   obj2!=null)
                    estado = true;
                else{
                    if ( obj1==null )  msj += error + DOL +" -> " +  BOL + newLinea;
                    if ( obj2==null )  msj += error + DOL +" -> " +  PES + newLinea;                    
                }
                
                
            }
            
            
            if( !msj.equals("") )
                msj =  title + msj;
            
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return estado;
        
    }



/**
     * Getter for property msj.
     * @return Value of property msj.
     */
    public java.lang.String getMsj() {
        return msj;
    }
    
    /**
     * Setter for property msj.
     * @param msj New value of property msj.
     */
    public void setMsj(java.lang.String msj) {
        this.msj = msj;
    }
}
