/*
 * ResponsableService.java
 *
 * Created on 28 de octubre de 2005, 04:33 PM
 */

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  Jose
 */
public class ResponsableService {
    private ResponsableDAO responsable;
    /** Creates a new instance of ResponsableService */
    public ResponsableService() {
        responsable = new ResponsableDAO();
    }
    private Codigo_discrepanciaDAO codigo_discrepancia;
    /** Creates a new instance of Codigo_discrepanciaService */
    
    public Responsable getResponsable( )throws SQLException{
        return responsable.getResponsable();
    }
    
    public Vector getResponsables()throws SQLException{
        return responsable.getResponsables();
    }
    
    public void setResponsables(Vector Responsables)throws SQLException{
        responsable.setResponsables(Responsables);
    }
    
    public void insertResponsable(Responsable user) throws SQLException{
        try{
            responsable.setResponsable(user);
            responsable.insertResponsable();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public boolean existResponsable(String cod) throws SQLException{
        try{
            return responsable.existResponsable(cod);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public void serchResponsable(String cod)throws SQLException{
        try{        
            responsable.searchResponsable(cod);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public void listResponsable()throws SQLException{
        try{         
            responsable.listResponsable();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public void updateResponsable(Responsable user)throws SQLException{
        try{        
            responsable.setResponsable(user);
            responsable.updateResponsable();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }

    public void anularResponsable(Responsable res)throws SQLException{
        try{
            responsable.setResponsable(res);
            responsable.anularResponsable();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public void searchDetalleResponsables(String cod, String desc) throws SQLException{
        try{
            responsable.searchDetalleResponsables(cod, desc);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
}
