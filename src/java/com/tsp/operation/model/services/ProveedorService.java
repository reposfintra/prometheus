/*
 * ProveedorService.java
 *
 * Created on 30 de septiembre de 2005, 08:22 AM
 */

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  Andres
 */
public class ProveedorService {
    private ProveedorDAO proveedor;

    /** Creates a new instance of ProveedorService */
    public ProveedorService() {
        proveedor = new ProveedorDAO();
    }
    public ProveedorService(String dataBaseName) {
        proveedor = new ProveedorDAO(dataBaseName);
    }


    /**
     * Inserta un nuevo registro en el archivo proveedor.
     * @autor Ing. Tito Andr�s Maturana D.
     * @param prov Instancia de <code>Proveedor</code>
     * @see com.tsp.operation.model.DAOS.ProveedorDAO#setProveedor(Proveedor)
     * @see com.tsp.operation.model.DAOS.ProveedorDAO#insertarProveedor()
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void agregarProveedor(Proveedor prov) throws java.sql.SQLException{
        this.proveedor.setProveedor(prov);
        this.proveedor.insertarProveedor();
    }

    /**
     * Actualiza nuevo registro en el archivo proveedor y guarda historial de .
     * @autor Ing. Enrique De Lavalle.
     * @param prov Instancia de <code>Proveedor</code>
     * @see com.tsp.operation.model.DAOS.ProveedorDAO#setProveedor(Proveedor)
     * @see com.tsp.operation.model.DAOS.ProveedorDAO#actualizarProveedor()
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void actualizarProveedor(Proveedor prov,String agency, String branch_code, String bank_account) throws java.sql.SQLException{
        this.proveedor.setProveedor(prov);
        this.proveedor.actualizarProveedor(agency, branch_code, bank_account);
    }

    /**
     * Actualiza nuevo registro en el archivo proveedor.
     * @autor Ing. Tito Andr�s Maturana D.
     * @param prov Instancia de <code>Proveedor</code>
     * @see com.tsp.operation.model.DAOS.ProveedorDAO#setProveedor(Proveedor)
     * @see com.tsp.operation.model.DAOS.ProveedorDAO#actualizarProveedor()
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void actualizarProveedor(Proveedor prov) throws java.sql.SQLException{
        this.proveedor.setProveedor(prov);
        this.proveedor.actualizarProveedor();
    }

    /**
     * Anula un registro en el archivo proveedor.
     * @autor Ing. Tito Andr�s Maturana D.
     * @param nit Identificaci�n del proveedor
     * @param cia C�digo del distrito
     * @see com.tsp.operation.model.DAOS.ProveedorDAO#anularProveedor(String, String)
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void anularProveedor(String nit, String cia) throws java.sql.SQLException{
        this.proveedor.anularProveedor(nit,  cia);
    }

    /**
     * Obtiene un nuevo registro del archivo proveedor.
     * @autor Ing. Tito Andr�s Maturana D.
     * @param nit Identificaci�n del proveedor
     * @param cia C�digo del distrito
     * @see com.tsp.operation.model.DAOS.ProveedorDAO#obtenerProveedor(String, String)
     * @see com.tsp.operation.model.DAOS.ProveedorDAO#getProveedor()
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public Proveedor obtenerProveedor(String nit, String cia) throws java.sql.SQLException{
        this.proveedor.obtenerProveedor(nit,  cia);
        return this.proveedor.getProveedor();
    }

    /**
     * Obtiene los registros del archivo proveedor.
     * @autor Ing. Tito Andr�s Maturana D.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public Vector obtenerProveedores() throws java.sql.SQLException{
        this.proveedor.obtenerProveedores();
        return this.proveedor.getProveedores();
    }

    public void setProveedor(Proveedor prov){
        this.proveedor.setProveedor(prov);
    }

    public Proveedor getProveedor(){
        return this.proveedor.getProveedor();
    }
    //tito 10.11.05
    public void eliminarProveedor(String nit, String cia) throws java.sql.SQLException{//10.11.2005
        this.proveedor.eliminarProveedor(nit,  cia);
    }


    /**
     * Este m�todo retorna un objeto Proveedor por un nit dado
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public Proveedor obtenerProveedorPorNit(String nit) throws SQLException {
        return proveedor.obtenerProveedorPorNit(nit);
    }

   public void obtenerProveedoresPorNit(String nit) throws SQLException {
        proveedor.obtenerProveedoresPorNit(nit);
    }

      /**
     * Getter for property proveedores.
     * @return Value of property proveedores.
     */
    public java.util.Vector getProveedores() {
        return proveedor.getProveedores ();
    }

    //Jose 23.01.2006
    /**
     * Metodo: searchProveedores, Este m�todo obtiene una lista de proveedores dado unos parametros
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @params String nit, String nombre
     * @see searchProveedores - ProveedorDAO
     * @autor : Ing. Jose de la rosa
     * @version : 1.0
     */
    public Vector searchProveedores (String nit, String nombre) throws java.sql.SQLException {
        try{
            return this.proveedor.searchProveedores (nit,nombre);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
     /**
     * Obtiene los nitpro q no estan registrado en el archivo nit y proveedor
     * @autor Ing. Andr�s Maturana D.
     * @param fechai Fecha inicial del per�odo
     * @param fechaf Fecha final del per�odo
     * @param cia C�digo del distrito
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void reporteVerificacionNitpro(String fechai, String fechaf, String cia) throws SQLException {
        this.proveedor.reporteVerificacionNitpro(fechai + " 00:00:00", fechaf + " 23:59:59", cia);
    }

    /**
     * Obtiene los nitpro q no estan registrado en el archivo nit y proveedor
     * @autor Ing. Andr�s Maturana D.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void reporteVerificacionNitpro(){
        Vector vec0 = this.proveedor.getProveedores();
        Vector vec1 = new Vector();

        //org.apache.log4j.Logger logger = org.apache.log4j.Logger.getL

        for( int i=0; i<vec0.size(); i++){
            RepGral obj0 = (RepGral) vec0.elementAt(i);

            if( i == 0 ){
                vec1.add(obj0);
            } else {
                boolean exist = false;
                for( int j=0; j<vec1.size(); j++){
                    RepGral obj1 = (RepGral) vec1.elementAt(j);
                    if( obj0.getNitpro().compareTo(obj1.getNitpro())==0 ){
                        exist = true;
                    }
                }

                if( !exist ){
                    vec1.add(obj0);
                }
            }
        }

        this.proveedor.setProveedores(vec1);
    }


    /**
     * aprueba el  proveedor.
     * @autor Ing. Diogenes Bastidas
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void aprobarProveedor(Proveedor prov) throws SQLException {
        this.proveedor.setProveedor(prov);
        this.proveedor.aprobarProveedor();
    }

     /**
     * Este m�todo retorna el E-Mail de de un proovedo por un nit dado
     * @autor : Ing. Julio Barros
     * @version : 1.0
     */
    public String obtenerEMailPorNit(String nit) throws SQLException {
        return proveedor.obtenerEMailPorNit(nit);
    }

    /**
     * Este m�todo retorna el nit afiliafo de de un proovedor por un nit dado
     * @autor : Ing. Julio Barros
     * @version : 1.0
     */
    public String obtenerDatAfi(String nit) throws SQLException {
        return proveedor.obtenerDatAfi(nit);
    }

     /**
     * Actualiza el campo ret_pago para un proveedor
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct Distrito
     * @param nit Nit del proveedor
     * @param rp S o N si aplica retencion al pago
     * @version : 1.0
     */
    public void aplicaRetencionPago(String dstrct, String nit, String rp) throws SQLException {
        this.proveedor.aplicaRetencionPago(dstrct, nit, rp);
    }

     /**
     * Getter for property proveedores_list.
     * @return Value of property proveedores_list.
     */
    public java.util.List getProveedores_list() {
        return proveedor.getProveedores_list();
    }

    /**
     * Setter for property proveedores_list.
     * @param proveedores_list New value of property proveedores_list.
     */
    public void setProveedores_list(java.util.List proveedores_list) {
        this.proveedor.setProveedores_list(proveedores_list);
    }

    //----------20100817-----------//
    public void insertConvs(String nitprov,ArrayList lista1) throws Exception{
        System.out.println("nitp "+nitprov+" lista "+lista1.size());
        try {
            this.proveedor.insertConvs(nitprov, lista1);
        }
        catch (Exception e) {
            throw new Exception("error: "+e.toString());
        }
    }

    public ArrayList buscarConvs() throws Exception{
        ArrayList lista = null;
        try {
            lista = proveedor.buscarConvs();
        }
        catch (Exception e) {
            throw new Exception("error: "+e.toString());
        }
        return lista;
    }

    public ArrayList buscarConvs(String nit) throws Exception{
        ArrayList lista = null;
        try {
            lista = proveedor.buscarConvs(nit);
        }
        catch (Exception e) {
            throw new Exception("error: "+e.toString());
        }
        return lista;
    }

    public ArrayList buscarSects() throws Exception{
        ArrayList lista = null;
        try {
            lista = proveedor.buscarSects();
        }
        catch (Exception e) {
            throw new Exception("error: "+e.toString());
        }
        return lista;
    }

    public ArrayList buscarSects(String nit) throws Exception{
        ArrayList lista = null;
        try {
            lista = proveedor.buscarSects(nit);
        }
        catch (Exception e) {
            throw new Exception("error: "+e.toString());
        }
        return lista;
    }

    public String buscarSubSects(String idsect) throws Exception{
        ArrayList lista = null;
        String cadena = "<option selected='selected' value=''>...</option>";
        try {
            lista = proveedor.buscarSubSects(idsect);
            if(lista!=null && lista.size()>0){
                String[] split = null;
                for (int i = 0; i < lista.size(); i++) {
                    split = ((String)(lista.get(i))).split(";_;");
                    cadena += "<option value='"+split[0]+"'>"+split[1]+"</option>";
                }
            }
        }
        catch (Exception e) {
            throw new Exception("error: "+e.toString());
        }
        return cadena;
    }

    public String buscarDats(String idconv) throws Exception{
        String cadena = "0;_;0";
        try {
            cadena = proveedor.buscarDats(idconv);
        }
        catch (Exception e) {
            throw new Exception("error: "+e.toString());
        }
        return cadena;
    }

    public ArrayList listaAfils() throws Exception{
        ArrayList lista = null;
        try {
            lista = proveedor.listaAfils();
        }
        catch (Exception e) {
            throw new Exception("error: "+e.toString());
        }
        return lista;
    }

    public String datosAfilS(String idconv) throws Exception{
        String lista = "";
        try {
            lista = proveedor.datosAfilS(idconv);
        }
        catch (Exception e) {
            throw new Exception("error: "+e.toString());
        }
        return lista;
    }

    public Proveedor buscarNitAfiliado(String nit) throws Exception{
        return proveedor.buscarNitAfiliado(nit);
    }

    public Vector searchProveedores2 (String nombre) throws java.sql.SQLException {
        try{
            return this.proveedor.searchProveedores2 (nombre);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }


    public ArrayList searchConds(String nit) throws Exception{
        ArrayList lista = null;
        try {
            lista = this.proveedor.searchConds(nit);
        }
        catch (Exception e) {
            throw new Exception("Error en service: "+e.toString());
        }
        return lista;
    }

    public ArrayList searchRanks(String nit) throws Exception{
        ArrayList lista = null;
        try {
            lista = this.proveedor.searchRanks(nit);
        }
        catch (Exception e) {
            throw new Exception("Error en service: "+e.toString());
        }
        return lista;
    }



    public ArrayList<BeanGeneral> buscarDeps(String codpais) throws Exception{
        ArrayList<BeanGeneral> lista = null;
        try{
            lista = this.proveedor.buscarDeps(codpais);
        }
        catch (Exception e){
            throw new Exception("Error en service: "+e.toString());
        }
        return lista;
    }

    public ArrayList<BeanGeneral> buscaCius(String codpais, String codpt) throws Exception{
        ArrayList<BeanGeneral> lista = null;
        try{
            lista = this.proveedor.buscaCius(codpais,codpt);
        }
        catch (Exception e){
            throw new Exception("Error en service: "+e.toString());
        }
        return lista;
    }

    public ArrayList buscarTiposProveedor() throws Exception {
        ArrayList lista = null;
        try {
            lista = this.proveedor.cargarTipos_proveedor();
        }
        catch (Exception e) {
            throw new Exception("Error en service: "+e.toString());
        }
        return lista;
    }
}
