/*
 * PaisService.java
 *
 * Created on 24 de febrero de 2005, 10:42 AM
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  DIBASMO
 */
public class PaisService {
    
    private PaisDAO paisd; 
    private TreeMap paises = new TreeMap();
    
    /** Creates a new instance of PaisService */
    public PaisService() {
        paisd = new PaisDAO(); 
    }
     
    public PaisService(String dataBaseName) {
        paisd = new PaisDAO(dataBaseName); 
    }
     
   public void insertarPais(Pais pais)throws SQLException {
       try{
           paisd.setPais(pais);
           paisd.insertarPais();
       }
       catch(SQLException e){
           throw new SQLException(e.getMessage());
       }
   }
   public boolean existPais(String cod) throws SQLException {
       return paisd.existPais(cod); 
   }   
   public boolean existPaisAnulado(String cod) throws SQLException {
       return paisd.existPaisAnulado(cod); 
   }   
   public boolean existPaisnom(String nom) throws SQLException {
       return paisd.existPaisnom(nom); 
   }   
   public List obtenerpaises( ) throws SQLException {
       List Paises = null;
       Paises = paisd.obtenerpaises();
       return Paises;
   }   
   public void listarpaises () throws SQLException {
       try{
           paisd.listarpaises();
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
   }
   public void paises () throws SQLException {
       try{
           paisd.paises();
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
   }
   public Pais obtenerpais()throws SQLException{
       return paisd.obtenerpais();
   }   
   public Vector obtpaises()throws SQLException{
       return paisd.obtpaises();
   }   
   public void buscarpais(String cod) throws SQLException {
       try{
           paisd.buscarpais(cod);     
       }
       catch(SQLException e){
           throw new SQLException ( e.getMessage()); 
       }       
   }
   public void buscarpaisnombre(String nom) throws SQLException {
       try{
           paisd.buscarpaisnombre(nom);     
       }
       catch(SQLException e){
           throw new SQLException ( e.getMessage()); 
       }       
   }
   public void modificarpais(Pais pais)throws SQLException {
       try{
           paisd.setPais(pais);
           paisd.modificarpais();
       }
       catch(SQLException e){
           throw new SQLException(e.getMessage());
       }
   }
   public void Activarpais(Pais pais)throws SQLException {
       try{
           paisd.setPais(pais);
           paisd.Activarpais();
       }
       catch(SQLException e){
           throw new SQLException(e.getMessage());
       }
   }
   public void anularpais(Pais pais)throws SQLException {
       try{
           paisd.setPais(pais);
           paisd.anularpais();
       }
       catch(SQLException e){
           throw new SQLException(e.getMessage());
       }
   }
   
   public boolean existePaises() throws SQLException {
       return paisd.existePaises(); 
   }   
   /**
     *Metodo que busca la lista de paises y los almacena en un treemap
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void listarPaises()throws SQLException{
        paisd.listarPaises();
    }
     /**
     * Getter for property listpais.
     * @return Value of property listpais.
     */
    public java.util.TreeMap getListpais() {
        return paisd.getListpais();
    }
    /**
     * Setter for property listpais.
     * @param listpais New value of property listpais.
     */
    public void setListpais(java.util.TreeMap listpais) {
        paisd.setListpais(listpais);
    }
    /**
    * Carga un <code>TreeMap</code> con los pa�ses almacenados
    * @autor Ing. Andr�s Maturana De La Cruz
    * @throws SQLException
    * @version 1.0
    */
   public void loadPaises() throws SQLException {
       this.listarpaises();
       
       if( this.obtenerpaises()!=null ){
           Object[] lst  = this.obtenerpaises().toArray();
           this.paises = new TreeMap();
           
           for( int i=0; i<lst.length; i++){
               Pais pa = (Pais) lst[i];
               this.paises.put(pa.getCountry_name(), pa.getCountry_code());
           }
       }
   }
   
   /**
    * Getter for property paises.
    * @return Value of property paises.
    */
   public java.util.TreeMap getPaises() {
       return paises;
   }
   
   /**
    * Setter for property paises.
    * @param paises New value of property paises.
    */
   public void setPaises(java.util.TreeMap paises) {
       this.paises = paises;
   }
}
