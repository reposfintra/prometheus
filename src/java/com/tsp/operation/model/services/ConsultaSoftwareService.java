    /***************************************
    * Nombre Clase ............. ConsultaSoftwareService.java
    * Descripci�n  .. . . . . .  Sevicio para la consulta de software
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  14/01/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/


package com.tsp.operation.model.services;



import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import javax.servlet.*;
import javax.servlet.http.*;


public class ConsultaSoftwareService {
    
    private RevisionSoftwareDAO  RevisionSoftwareDataAccess;
    private String   equipo;
    private String   sitio;
    private List     listProgramas;
    
    
    
    public ConsultaSoftwareService() {
        RevisionSoftwareDataAccess  = new RevisionSoftwareDAO();
        reset();
    }
    
    
    
    /**
     * M�todos que setea las variables
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public void reset(){
        equipo        = "";
        sitio         = "";
        listProgramas =  new LinkedList();
    }
    
    
    
    
    /**
     * M�todos que busca los dif. programas del sitio
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/ 
    public void searchPrograms()throws Exception{
        this.listProgramas = new LinkedList();
        try{
            this.listProgramas = this.RevisionSoftwareDataAccess.searchProgram(this.equipo, this.sitio);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    
    
    
    /**
     * M�todos que devuelve un programa
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/ 
    public Programa getPrograma(String code)throws Exception{
        Programa programa = new Programa();
        for(int i=0;i<this.listProgramas.size();i++){
             Programa  obj = (Programa) listProgramas.get(i);
             if(obj.getPrograma().equals( code ) ){
                 programa = obj;
                 break;
             }
        }        
        return programa;
    }
    
    
    
    
    
    
    // SET :
    /**
     * M�todos que setea los parametros
     * @autor.......fvillacob
     * @version.....1.0.     
     **/ 
    
    
    /**
     * M�todos que setea el valor del equipo
     * @autor.......fvillacob
     * @parameter... String val
     * @version.....1.0.     
     **/ 
    public void setEquipo(String val){
        this.equipo = val;
    }
    
    
    /**
     * M�todos que setea el valor del sitio
     * @autor.......fvillacob
     * @parameter... String val
     * @version.....1.0.     
     **/ 
    public void setSitio(String val){
        this.sitio = val;
    }
    
    
    
    
    
    
    
    
    // GET :
    /**
     * M�todos que devuelve los valores de los parametros
     * @autor.......fvillacob
     * @version.....1.0.     
     **/     
    
    
    /**
     * M�todos que devuelve el valor del equipo
     * @autor.......fvillacob
     * @version.....1.0.     
     **/ 
    public String getEquipo(){
        return this.equipo;
    }
    
    
    /**
     * M�todos que devuelve el valor del sitio
     * @autor.......fvillacob
     * @version.....1.0.     
     **/
    public String getSitio(){
        return this.sitio ;
    }
        
    
    /**
     * M�todos que devuelve el valor del programa
     * @autor.......fvillacob
     * @version.....1.0.     
     **/
    public List getProgramas(){
        return this.listProgramas ;
    }
    
    
}
