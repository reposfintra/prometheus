/********************************************************************
 *  Nombre Clase.................   ReportePlacaFotoService.java
 *  Descripci�n..................   Service del DAO de la tabla (placa, fotos)
 *  Autor........................   Ing. Leonardo Parodi
 *  Fecha........................   06.01.2005
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.text.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

public class ReportePlacaFotoService {
    
    ReportePlacaFotosDAO dao;
    
    /** Creates a new instance of ReportePlacaFotoService */
    public ReportePlacaFotoService() {
        dao = new ReportePlacaFotosDAO();
    }
    
    /**
     * Metodo ReportePlacaFoto , Metodo llama a ReportePlacaFoto en el DAO
     * @autor : Ing. Leonardo Parody
     * @return : List
     * @version : 1.0
     */
    
    public List ReportePlacaFoto(String fechaI, String fechaF, String agencia, String dstrct) throws Exception{
        return dao.ReportePlacaFoto(fechaI, fechaF, agencia, dstrct);
    }
    
    /**
     * Metodo ReporteConductorFoto , Metodo que llama a ReporteConductorFoto en el DAO
     * @autor : Ing. Leonardo Parody
     * @return : List
     * @version : 1.0
     */
    
    public List ReporteConductorFoto(String fechaI, String fechaF, String agencia, String dstrct) throws Exception{
        return dao.ReporteConductorFoto(fechaI, fechaF, agencia,dstrct);
    }
    
    
    //**************************************< Diogenes Bastidas>**********************************
    
    public Vector obtenerDespachos(String dstrct,String agencia, String fecha1, String fecha2) throws Exception{
        return dao.obtenerDespachos(dstrct, agencia, fecha1, fecha2);
    }
    
    public String tienePernoctacion(String fecha_salida, String fecha_pos_llegada)throws Exception{
        String estado ="NO";
        //System.out.println("Fecha_Salida "+fecha_salida+" Fecha pos llegada "+fecha_pos_llegada);
        if(!fecha_pos_llegada.equals("")){
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            java.util.Date fec_salida = fmt.parse(fecha_salida);
            
            java.util.Date fec_pos_llegada = fmt.parse(fecha_pos_llegada);
            long dif = (fec_pos_llegada.getTime() - fec_salida.getTime()) / (60*60*1000);
            
            if(dif>=24){
                estado="SI";
            }else{
                if(fecha_salida.substring(0,10).equals( fecha_pos_llegada.substring(0,10) ) ){
                    long val_lleg = fec_pos_llegada.getHours();
                    long val_sal = fec_salida.getHours();
                    if( ( val_lleg > 20 || val_lleg<5 ) || (val_sal > 20 || val_sal<5) ){
                        estado = "SI";
                    }
                }
                else{
                    estado = "SI";
                }
            }
           
        }
        return estado;
    }
    
    
  
}
