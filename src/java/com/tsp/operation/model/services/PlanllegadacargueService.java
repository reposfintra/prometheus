/*
 * PlanllegadacargueService.java
 *
 * Created on 19 de septiembre de 2006, 10:39 AM
 */

package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;

public class PlanllegadacargueService {
    private PlanllegadacargueDAO planllegada;
    
    /** Creates a new instance of PlanllegadacargueService */
    public PlanllegadacargueService() {
        planllegada = new PlanllegadacargueDAO();
    }
    
    /**
     * Metodo insertPlanllegadaCargue, ingresa un registro en la tabla actividad
     * @param: objeto plan_llegada_cargue 
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void insertarPlanllegadaCargue ( Plan_llegada_cargue llegada_cargue ) throws Exception{
        planllegada.insertarPlanllegadaCargue(llegada_cargue);
    }
    
    
    /**
     * Metodo modificarPlanllegadaCargue, ingresa un registro en la tabla actividad
     * @param: objeto tipo 
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void modificarPlanllegadaCargue ( Plan_llegada_cargue llegada_cargue ) throws Exception{
        planllegada.modificarPlanllegadaCargue(llegada_cargue);
    }
    
    
      /**
     * Metodo buscarInfoTransporte, busca la informacion del transporte
     * @param: objeto tipo Plan_llegada_cargue, cod cliente y nro transporte
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public String buscarInfoTransporte ( String cliente, String nrotrans ) throws Exception{
        return planllegada.buscarInfoTransporte(cliente, nrotrans);
    }
    
     public void ReporteViajesDescargues (String fecini, String fecfin ) throws SQLException{
         planllegada.ReporteViajesDescargues(fecini, fecfin);
    }    
    
     public java.util.Vector getvectorValores() {
        return planllegada.getvectorValores();
    } 
}
