/********************************************************************
 *      Nombre Clase.................   AplicacionService.java    
 *      Descripci�n..................   Service de la tabla tblapp    
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

public class ConceptoService {
        
        private ConceptoDAO concepto;
        
        /** Creates a new instance of ConceptoService */
        public ConceptoService() {
                this.concepto = new ConceptoDAO();
        }
        
        /**
         * Obtiene un objeto de la clase Concepto
         * @autor Tito Andr�s Maturana
         * @version 1.0
         */
        public Concepto getConcepto(){
                return this.concepto.getConcepto();
        }
        
        /**
         * Instancia el objeto de la clase Concepto
         * @autor Tito Andr�s Maturana
         * @version 1.0
         */
        public void setConcepto(Concepto con){
                this.concepto.setConcepto(con);
        }
        
        /**
         * Obtiene un vector de objetos de la clase Concepto
         * @autor Tito Andr�s Maturana
         * @version 1.0
         */        
        public Vector getConceptos(){
                return this.concepto.getConceptos();
        }
        
        /**
         * Instancia un vector de objetos de la clase Concepto
         * @autor Tito Andr�s Maturana
         * @version 1.0
         */
        public void setConceptos(Vector obj){
                this.concepto.setConceptos(obj);
        }
        
        
        /**
         * Inserta un nuevo registro
         * @autor Tito Andr�s Maturana
         * @param con Objeto Concepto a insertar
         * @version 1.0
         * @throws SQLException
         */
        public void ingresarConcepto(Concepto con) throws SQLException{
                this.concepto.setConcepto(con);
                this.concepto.insertar();
        }
        
        /**
         * Obtiene un nuevo registro
         * @autor Tito Andr�s Maturana
         * @param tabla Nombre de la tabla
         * @param descripcion Descripci�n del concepto
         * @version 1.0
         * @throws SQLException
         */
        public Concepto obtenerConcepto(String tabla, String descripcion) throws SQLException{
                this.concepto.obtener(tabla, descripcion);
                return this.concepto.getConcepto();
        }
        
        
        /**
         * Actualiza un registro
         * @autor Tito Andr�s Maturana
         * @param con Objeto Concepto a insertar
         * @throws SQLException
         * @version 1.0
         */
        public void actualizarConcepto(Concepto con) throws SQLException{
                this.concepto.setConcepto(con);
                this.concepto.actualizar();
        }
        
        /**
         * Lista los registros
         * @autor Tito Andr�s Maturana
         * @throws SQLException
         * @version 1.0
         */
        public Vector listarConceptos() throws SQLException{
                this.concepto.listar();
                return this.concepto.getConceptos();
        }
        
}
