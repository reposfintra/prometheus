 /***************************************
    * Nombre Clase ............. ExtractosService.java
    * Descripci�n  .. . . . . .  Generamos Los Extractos  en un rango de fecha determinado
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  25/10/2005
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/


package com.tsp.operation.model.services;



import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.Corrida;
import com.tsp.operation.model.DAOS.ExtractosDAO;
import java.util.*;




public class ExtractosService {
    
    private ExtractosDAO  ExtractosDataAccess;
    
    
    private List        distritos;
    private List        bancos;
    private List        sucursales;
    private List        proveedores;
    private List        tipoProveedores;
    private Hashtable   filtros;
    private Hashtable   datosFiltros;   
    
    
    private Usuario     usuario;
    private boolean     inProceso;     
    
    private String      distritoCorrida;
    private String[]    bancosCorrida;
    private String[]    sucursalesCorrida;
    private String[]    proveedoresCorrida;
    private String      fechaIniCorrida;
    private String      fechaFinCorrida;
    private String      tipoProveedorCorrida;
    
    private String      fechaIniViaje;
    private String      fechaFinViaje;
    
    private String      tipoViaje;
    
    
    private List        facturasAdicionales;
    
    
    private List        placas;
    private String[]    placasFiltro;
    
    
    private String      tipoPago = "B";
    
    
    public ExtractosService() {
        ExtractosDataAccess = new ExtractosDAO();
        inProceso           = false;
        usuario             = new Usuario();
        reset();
    }
    public ExtractosService(String dataBaseName) {
        ExtractosDataAccess = new ExtractosDAO(dataBaseName);
        inProceso           = false;
        usuario             = new Usuario();
        reset();
    }
    
    
    
    
    
    /**
     * M�todo que  permiten manejar estado de boton : activar
     * @autor.......fvillacob
     * @version.....1.0.
     **/
    
    public void activarProcesos(){
       this.inProceso = true;
    }
    
    
    
     /**
     * M�todo que  permiten manejar estado de boton : desactivar
     * @autor.......fvillacob
     * @version.....1.0.
     **/
    public void desactivarProcesos(){
       this.inProceso = false;
    }
     
        
    
    /**
     * M�todo que  permiten manejar estado de boton 
     * @autor.......fvillacob
     * @version.....1.0.
     **/
    public boolean  isActivoProcesos(){
        return   this.inProceso;
    }
    
    
    
    
    
    
     /**
     * M�todo que  setea atributos
     * @autor.......fvillacob
     * @version.....1.0.
     **/
    public void reset(){
        filtros         = new Hashtable();
        datosFiltros    = new Hashtable();
        distritos       = new LinkedList();
        bancos          = new LinkedList();
        sucursales      = new LinkedList();
        proveedores     = new LinkedList();
        tipoProveedores = new LinkedList();
        placas          = new LinkedList(); 
        desactivarProcesos();
    }
    
    
    
    
    
    /**
     * M�todo que busca la informaci�n de la corrida
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public Hashtable infoCorrida(String distrito, String corrida)throws Exception{
           Hashtable info = null;
           try{
                 info = this.ExtractosDataAccess.infoCorrida(distrito, corrida);
           }catch(Exception e){
               throw new Exception( e.getMessage() );
           }           
           return info;
    }
    
    
    
    
    
   
     /**
     * M�todo que  verifica que exista la corrida para el distrito
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String existeCorrida(String distrito, String corrida, String user )throws Exception{
           String msj = "";
           try{
               msj = this.ExtractosDataAccess.validarCorrida(distrito, corrida, user );
           }catch(Exception e){
               throw new Exception( e.getMessage() );
           }           
           return msj;
    }
    
    
    
    
    
     /**
     * M�todo que  inserta informaci�n de la corrida
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String insertInfoCorrida(
                                     String distrito,
                                     String corrida,
                                     String tpago, 
                                     String tviaje,
                                     String fechacumini,
                                     String fechacumfin,
                                     String bancos,
                                     String fproveedor,
                                     String proveedores,
                                     String placas,
                                     String fechavenini,
                                     String fechavenfin,
                                     String usuario,
                                     String cheque_cero
                                   )throws Exception{
           String sql = "";
           try{
               sql = this.ExtractosDataAccess.insertInfoCorrida( distrito, corrida , tpago,tviaje, fechacumini, fechacumfin, bancos, fproveedor, proveedores, placas, fechavenini, fechavenfin , usuario, cheque_cero  );
           }catch(Exception e){
               throw new Exception( e.getMessage() );
           }           
           return sql;
    }
    
    
    
    
    
    
    
    
    
    
    
     /**
     * M�todo que  habilita los datos de acuerdo a los filtros seleccionados:
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void formarFiltros(  String ckDistrito, String ckBanco,  String ckProveedores,   String ckFecha, String  filtroProveedor, String ckViaje, String ckPlacas ) throws Exception{
        try{
                reset();        
               
               // if(ckDistrito  != null){
                    searchDistritos();
               // }
                if(ckBanco      != null) {
                    searchBanco();
                }
                if(ckProveedores!= null){
                    if ( filtroProveedor !=null  ){
                        if ( filtroProveedor.equals("LISTADO") )   searchProveedores();
                        if ( filtroProveedor.equals("HC") )        searchTipoProveedores();                                     
                    }
                    
                }
                
                filtros.put("distrito",        resetCK(ckDistrito      ) );
                filtros.put("banco",           resetCK(ckBanco         ) );
                filtros.put("proveedor",       resetCK(ckProveedores   ) );
                filtros.put("filtroProveedor", filtroProveedor           );
                filtros.put("fecha",           resetCK(ckFecha         ) );
                filtros.put("viaje",           resetCK(ckViaje         ) );
                filtros.put("placa",           resetCK(ckPlacas        ) );
                
                
                
                datosFiltros.put("distrito",          this.getDistritos()       );
                datosFiltros.put("banco",             this.getBancos()          );
                datosFiltros.put("sucursales",        this.getSucursales()      );
                datosFiltros.put("proveedores",       this.getProveedores()     );
                datosFiltros.put("tipoProveedores",   this.getTipoProveedores() );
                
                
                
        }catch(Exception e){
            throw new Exception( " formarFiltros: " + e.getMessage());
        }
        
    }
    
    
    
    
    /**
     * M�todo que  setea estado de los checkbox
     * @autor.......fvillacob
     * @version.....1.0.
     **/
    public String resetCK(String val){
        return  ( val==null )?"OFF" : "ON";
    }
    
    
    
    
    
    /**
     * M�todo que busca nombre del nit
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String searchName(String nit) throws Exception{
        String  name = null;
        try{
               name =  this.ExtractosDataAccess.getNombre(nit);
        }catch(Exception e){ 
            throw new Exception( " searchName() " + e.getMessage());
        }
        return name;
    }
    
    
    
    
    /**
     * M�todo que busca la placa
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String searchPlaca(String placa) throws Exception{
        String  name = null;
        try{
               name =  this.ExtractosDataAccess.existePlaca( placa );
        }catch(Exception e){ 
            throw new Exception( " searchPlaca() " + e.getMessage());
        }
        return name;
    }
    
    
    
    
    
     /**
     * M�todo que carga la lista de distritos
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void searchDistritos() throws Exception{
        try{
             this.distritos.add(this.usuario.getDstrct());
          // this.distritos  = this.ExtractosDataAccess.getDistritos( this.usuario.getId_agencia() , this.usuario.getDstrct() ); 
        }catch(Exception e){ 
            throw new Exception( " searchDistritos() " + e.getMessage());
        }
    }
    
    
    
    
    /**
     * M�todo que carga la lista de bancos
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void searchBanco() throws Exception{
        try{
           this.bancos = this.ExtractosDataAccess.getBancos(  this.usuario.getDstrct(),  this.usuario.getId_agencia() );
        }catch(Exception e){ 
            throw new Exception( " searchBanco() " + e.getMessage());
        }
    }
        
    
    
    /**
     * M�todo que carga la lista de proveedores
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void searchProveedores() throws Exception{
        try{
           this.proveedores = this.ExtractosDataAccess.getProveedores(  this.usuario.getDstrct(),  this.usuario.getId_agencia() );
        }catch(Exception e){ 
            throw new Exception( " searchProveedores() " + e.getMessage());
        }
    }
   
    
    
    
    /**
     * M�todo que carga la lista de Tipos de proveedores
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void searchTipoProveedores() throws Exception{
        try{
           this.tipoProveedores   = this.ExtractosDataAccess.getTipoProveedores();
        }catch(Exception e){ 
            throw new Exception( " searchTipoProveedores() " + e.getMessage());
        }
    }
    
    
    
    /**
     * M�todo que carga la lista de bancos
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void searchSucursales(String[] bancos) throws Exception{
        try{
           String cedenabancos = this.getToString(bancos);

           this.sucursales     = this.ExtractosDataAccess.getSucursales(  this.usuario.getDstrct(), cedenabancos,  this.usuario.getId_agencia() );
        }catch(Exception e){ 
            throw new Exception( " searchBanco() " + e.getMessage());
        }
    }
        
    
    
    
    /**
     * M�todo que carga la lista de placas
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void searchPlacas(String[] nits) throws Exception{
        try{
           String cedenaNits = this.getToString(nits);
           this.placas       = this.ExtractosDataAccess.getPlacas( cedenaNits );
           
        }catch(Exception e){ 
            throw new Exception( " searchPlacas() " + e.getMessage());
        }
    }
    
    
    
    
    
    /**
     * M�todo que devuelve  el vector en cadena
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String getToString(String[] val){
        String  cadena="";
        if(val!=null){
            for(int i=0; i<val.length;i++){
                String valor = val[i].trim();
                if( !cadena.equals("")) 
                     cadena+=",";
                cadena += "'" + valor + "'";
            }
                
        }
        return cadena;
    }
    
    
    
    
    
    
    
        
    
    
    /**
     * M�todo que busca las facturas para la corrida
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public List getFacturas(  String SQL, 
                              String distrito, 
                              String filtrosViajes, 
                              String filtrosFacturasOPs, 
                              String filtrosFacturasTipo4, 
                              String filtrosHC, 
                              String filtroPlacas,                               
                              String fileUrl, 
                              String tipoPago               
                              
                            ) throws Exception{
        List lista = new LinkedList();
        try{
            lista            = this.ExtractosDataAccess.getFacturasSQL( SQL, distrito,  filtrosViajes, filtrosFacturasOPs,    filtrosFacturasTipo4,  filtrosHC, filtroPlacas, fileUrl,  tipoPago  );
        }catch(Exception e){
            throw new Exception( " getFacturas " + e.getMessage() );
        }
        return lista;
    }
    
    
    
    
    /**
     * M�todo que busca las facturas para la corrida, se usan en agregar facturas a la corrida
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void getFacturas_Adicionales(String distrito, String filtros ) throws Exception{
        facturasAdicionales = new LinkedList();
        try{
            facturasAdicionales  = this.ExtractosDataAccess.getFacturas( distrito,  filtros );
        }catch(Exception e){
            throw new Exception( " getFacturas_Adicionales " + e.getMessage() );
        }
    }
    
    
     
    /**
   * M�todo que carga  de la lista general las facturas que se han seleccionados
   * @autor.......fvillacob
   * @throws......Exception
   * @version.....1.0.
   **/ 
   public List loadSeleccion(String[] facturas)throws Exception{
       List seleccion = new LinkedList();
       try{
        
           if(facturas!=null)
                 for(int i=0;i<facturas.length;i++){
                     int id = Integer.parseInt(  facturas[i]  );
                     
                     for(int j=0;j<this.facturasAdicionales.size();j++){                         
                           Corrida  factura  = (Corrida) facturasAdicionales.get(j);                           
                           if (  factura.getId()== id ){                               
                                 seleccion.add(factura); 
                                 facturasAdicionales.remove(j);
                                 break;
                           }
                     }
                     
                 }
           
       }catch(Exception e){
           throw new Exception( " loadSeleccion " + e.getMessage() );
       }
       return  seleccion;
   }
   
   
   
    
    
    
    
    
    
    
    
    
     /**
     * M�todo que obtiene el numero de corrida a asignar
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String getNoCorrida(String distrito) throws Exception{
        String corrida = "1";
        try{            
                corrida = this.ExtractosDataAccess.getNoCorrida( distrito );         
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return corrida;
    }
    
    
    
    
    
    /**
     * M�todo que inserta en la tabla corrida las facturas obtenidas
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String insert(Corrida  factura, String corrida, String user, String fecha ) throws Exception{
        String sqlInsert = "";
        try{            
           sqlInsert = this.ExtractosDataAccess.insertExtractos( factura, corrida,  user, fecha );           
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return sqlInsert;
    }
    
    
    
    
    
     /**
     * M�todo que actualiza el campo corrida a las facturas
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String updateFacturas(Corrida  factura, String corrida, String user) throws Exception{
        String sqlUpdate = "";
        try{            
            sqlUpdate = this.ExtractosDataAccess.updateFacturas ( factura, corrida, user );           
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return sqlUpdate;
    }
    
    
    
   
    
    
    
     // SET :    
     /**
     * M�todo que setea valores de atributos
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    
    public void setUsuario(Usuario val){
        this.usuario = val;
    }
    
    
    
    
    
    
    
   // GET :    
     /**
     * M�todo que devuelven valores de atributos
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
   
    public List getDistritos(){
        return this.distritos;
    }    
    
    public List getBancos(){
        return this.bancos;
    }
    
    public List getSucursales(){
        return this.sucursales;
    }
    
    public List getProveedores(){
        return this.proveedores;
    }
        
    public Hashtable getFiltros(){
        return this.filtros;
    }
    
    public Hashtable getDatosFiltros(){
        return this.datosFiltros;
    }
    
  
           
    public String getOficinaPPal(){
        return this.ExtractosDataAccess.OFICINA_PPAL;
    }
    
    
    
    /**
     * Getter for property bancosCorrida.
     * @return Value of property bancosCorrida.
     */
    public java.lang.String[] getBancosCorrida() {
        return this.bancosCorrida;
    }    
        
    
    /**
     * Setter for property bancosCorrida.
     * @param bancosCorrida New value of property bancosCorrida.
     */
    public void setBancosCorrida(java.lang.String[] bancosCorrida) {
        this.bancosCorrida = bancosCorrida;
    }
    
    /**
     * Getter for property distritoCorrida.
     * @return Value of property distritoCorrida.
     */
    public java.lang.String getDistritoCorrida() {
        return distritoCorrida;
    }
    
    /**
     * Setter for property distritoCorrida.
     * @param distritoCorrida New value of property distritoCorrida.
     */
    public void setDistritoCorrida(java.lang.String distritoCorrida) {
        this.distritoCorrida = distritoCorrida;
    }
    
    /**
     * Getter for property proveedoresCorrida.
     * @return Value of property proveedoresCorrida.
     */
    public java.lang.String[] getProveedoresCorrida() {
        return this.proveedoresCorrida;
    }
    
    /**
     * Setter for property proveedoresCorrida.
     * @param proveedoresCorrida New value of property proveedoresCorrida.
     */
    public void setProveedoresCorrida(java.lang.String[] proveedoresCorrida) {
        this.proveedoresCorrida = proveedoresCorrida;
    }
    
    /**
     * Getter for property sucursalesCorrida.
     * @return Value of property sucursalesCorrida.
     */
    public java.lang.String[] getSucursalesCorrida() {
        return this.sucursalesCorrida;
    }
    
    /**
     * Setter for property sucursalesCorrida.
     * @param sucursalesCorrida New value of property sucursalesCorrida.
     */
    public void setSucursalesCorrida(java.lang.String[] sucursalesCorrida) {
        this.sucursalesCorrida = sucursalesCorrida;
    }
    
    /**
     * Getter for property fechaIniCorrida.
     * @return Value of property fechaIniCorrida.
     */
    public java.lang.String getFechaIniCorrida() {
        return fechaIniCorrida;
    }
    
    /**
     * Setter for property fechaIniCorrida.
     * @param fechaIniCorrida New value of property fechaIniCorrida.
     */
    public void setFechaIniCorrida(java.lang.String fechaIniCorrida) {
        this.fechaIniCorrida = fechaIniCorrida;
    }
    
    /**
     * Getter for property fechaFinCorrida.
     * @return Value of property fechaFinCorrida.
     */
    public java.lang.String getFechaFinCorrida() {
        return fechaFinCorrida;
    }
    
    /**
     * Setter for property fechaFinCorrida.
     * @param fechaFinCorrida New value of property fechaFinCorrida.
     */
    public void setFechaFinCorrida(java.lang.String fechaFinCorrida) {
        this.fechaFinCorrida = fechaFinCorrida;
    }
    
    
    
    
    
    /**
     * Getter for property tipoProveedores.
     * @return Value of property tipoProveedores.
     */
    public java.util.List getTipoProveedores() {
        return tipoProveedores;
    }
    
    /**
     * Setter for property tipoProveedores.
     * @param tipoProveedores New value of property tipoProveedores.
     */
    public void setTipoProveedores(java.util.List tipoProveedores) {
        this.tipoProveedores = tipoProveedores;
    }
    
    
    
    /**
     * Getter for property tipoProveedorCorrida.
     * @return Value of property tipoProveedorCorrida.
     */
    public java.lang.String getTipoProveedorCorrida() {
        return tipoProveedorCorrida;
    }
    
    /**
     * Setter for property tipoProveedorCorrida.
     * @param tipoProveedorCorrida New value of property tipoProveedorCorrida.
     */
    public void setTipoProveedorCorrida(java.lang.String tipoProveedorCorrida) {
        this.tipoProveedorCorrida = tipoProveedorCorrida;
    }
    
    /**
     * Getter for property fechaIniViaje.
     * @return Value of property fechaIniViaje.
     */
    public java.lang.String getFechaIniViaje() {
        return fechaIniViaje;
    }
    
    /**
     * Setter for property fechaIniViaje.
     * @param fechaIniViaje New value of property fechaIniViaje.
     */
    public void setFechaIniViaje(java.lang.String fechaIniViaje) {
        this.fechaIniViaje = fechaIniViaje;
    }
    
    
    
    /**
     * Getter for property fechaFinViaje.
     * @return Value of property fechaFinViaje.
     */
    public java.lang.String getFechaFinViaje() {
        return fechaFinViaje;
    }
    
    /**
     * Setter for property fechaFinViaje.
     * @param fechaFinViaje New value of property fechaFinViaje.
     */
    public void setFechaFinViaje(java.lang.String fechaFinViaje) {
        this.fechaFinViaje = fechaFinViaje;
    }
    
    
    
    
    /**
     * Getter for property facturasAdicionales.
     * @return Value of property facturasAdicionales.
     */
    public java.util.List getFacturasAdicionales() {
        return facturasAdicionales;
    }
    
    /**
     * Setter for property facturasAdicionales.
     * @param facturasAdicionales New value of property facturasAdicionales.
     */
    public void setFacturasAdicionales(java.util.List facturasAdicionales) {
        this.facturasAdicionales = facturasAdicionales;
    }
    
    /**
     * Getter for property tipoViaje.
     * @return Value of property tipoViaje.
     */
    public java.lang.String getTipoViaje() {
        return tipoViaje;
    }
    
    /**
     * Setter for property tipoViaje.
     * @param tipoViaje New value of property tipoViaje.
     */
    public void setTipoViaje(java.lang.String tipoViaje) {
        this.tipoViaje = tipoViaje;
    }
    
    /**
     * Getter for property placas.
     * @return Value of property placas.
     */
    public java.util.List getPlacas() {
        return placas;
    }
    
    /**
     * Setter for property placas.
     * @param placas New value of property placas.
     */
    public void setPlacas(java.util.List placas) {
        this.placas = placas;
    }
    
    /**
     * Getter for property placasFiltro.
     * @return Value of property placasFiltro.
     */
    public java.lang.String[] getPlacasFiltro() {
        return this.placasFiltro;
    }    
    
    /**
     * Setter for property placasFiltro.
     * @param placasFiltro New value of property placasFiltro.
     */
    public void setPlacasFiltro(java.lang.String[] placasFiltro) {
        this.placasFiltro = placasFiltro;
    }    
    
    /**
     * Getter for property tipoPago.
     * @return Value of property tipoPago.
     */
    public java.lang.String getTipoPago() {
        return tipoPago;
    }
    
    /**
     * Setter for property tipoPago.
     * @param tipoPago New value of property tipoPago.
     */
    public void setTipoPago(java.lang.String tipoPago) {
        this.tipoPago = tipoPago;
    }
    
}
