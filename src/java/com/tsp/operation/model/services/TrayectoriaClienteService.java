/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.services;
import com.tsp.operation.model.DAOS.SiniestrosDAO;
import com.tsp.operation.model.DAOS.TrayectoriaClienteDAO;
import com.tsp.operation.model.beans.Siniestros;
import com.tsp.operation.model.beans.TrayectoriaCliente;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
/**
 *
 * @author jpinedo
 */
public class TrayectoriaClienteService {
    
    TrayectoriaClienteDAO trayectoriacldao;
  
    public TrayectoriaClienteService(){
      trayectoriacldao = new TrayectoriaClienteDAO();
    } 
     
    public TrayectoriaClienteService(String dataBaseName){
      trayectoriacldao = new TrayectoriaClienteDAO(dataBaseName);
    } 


    /**
     * M�todo que lista siniestros enviados
     * @autor.......jpinedo
     * @throws......Exception
     * @version.....1.0.
     **/
    public  ArrayList< TrayectoriaCliente> ListarTrayectoriaCliente( String id) throws Exception{
        ArrayList lista = new ArrayList();
        try
        {
           lista   = trayectoriacldao.ListarTrayectoriaCliente(id);
        }catch(Exception e){
            throw new Exception( "listarsiniestros " + e.getMessage());
        }

        return lista;

    }


}
