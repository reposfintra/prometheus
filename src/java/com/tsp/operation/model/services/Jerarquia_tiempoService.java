/*
 * Jerarquia_tiempoService.java
 *
 * Created on 26 de junio de 2005, 01:28 PM
 */

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  Jose
 */
public class Jerarquia_tiempoService {
    private Jerarquia_tiempoDAO jerarquia_tiempo;
    
    
    /** Creates a new instance of Jerarquia_tiempoService */
    public Jerarquia_tiempoService() {
        jerarquia_tiempo = new Jerarquia_tiempoDAO();
    }
    public Jerarquia_tiempo getJerarquia_tiempo( )throws SQLException{
        return jerarquia_tiempo.getJerarquia_tiempo();
    }
    
    public Vector getJerarquia_tiempos() {
        return jerarquia_tiempo.getJerarquia_tiempos();
    }
    
    public void setJerarquia_tiempos(Vector Jerarquia_tiempos) {
        jerarquia_tiempo.setJerarquia_tiempos(Jerarquia_tiempos);
    }
    
    public void insertJerarquia_tiempo(Jerarquia_tiempo user) throws SQLException{
        try{
            jerarquia_tiempo.setJerarquia_tiempo(user);
            jerarquia_tiempo.insertJerarquia_tiempo();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public boolean existJerarquia_tiempo(String codact, String codres, String coddem) throws SQLException{
        return jerarquia_tiempo.existJerarquia_tiempo(codact,codres,coddem);
    }
    
    public void serchJerarquia_tiempo(String codact, String codres, String coddem)throws SQLException{
        jerarquia_tiempo.searchJerarquia_tiempo(codact,codres,coddem);
    }
    
    public void listJerarquia_tiempo()throws SQLException{
        jerarquia_tiempo.listJerarquia_tiempo();
    }
    
    public void updateJerarquia_tiempo(String ncodact, String ncodres, String ncoddem, String usu,String codact, String codres, String coddem, String base)throws SQLException{
        jerarquia_tiempo.updateJerarquia_tiempo(ncodact, ncodres, ncoddem, usu, codact, codres, coddem, base);
    }
    public void anularJerarquia_tiempo(Jerarquia_tiempo jer)throws SQLException{
            try{
            jerarquia_tiempo.setJerarquia_tiempo(jer);
            jerarquia_tiempo.anularJerarquia_tiempo();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }  
    }
    public Vector searchDetalleJerarquia_tiempos(String codact, String codres, String coddem) throws SQLException{
        try{
            return jerarquia_tiempo.searchDetalleJerarquia_tiempos(codact, codres, coddem);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    //Diogenes 16.12.05
    /**
     * Metodo  buscarResponsablaAct, lista los responsables de la de una actividad
     * @param: codigo de la actividad
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: buscarResponsablaAct(String codact) - Jerarquia_tiempoDAO
     * @version : 1.0
     */
    public void buscarResponsablaAct(String codact)throws SQLException{
        try{
            jerarquia_tiempo.buscarResponsablaAct(codact);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
     /**
     * Metodo  buscarCausaAct, lista la causas de la de una actividad
     * @param: codigo de la actividad
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: buscarCausaAct(String codact) - Jerarquia_tiempoDAO
     * @version : 1.0
     */
     public void buscarCausaAct(String codact)throws SQLException{
        try{
            jerarquia_tiempo.buscarCausaAct(codact);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
     
    /**
     * Getter for property vecres.
     * @return Value of property vecres.
     */
    public java.util.Vector getVecres() {
        return jerarquia_tiempo.getVecres();
    }
    
    /**
     * Getter for property veccausa.
     * @return Value of property veccausa.
     */
    public java.util.Vector getVeccausa() {
        return jerarquia_tiempo.getVeccausa();
    }

}
