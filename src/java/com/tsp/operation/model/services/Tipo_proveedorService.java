/*
 * Tipo_proveedorService.java
 *
 * Created on 22 de septiembre de 2005, 08:57 PM
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.*;
import java.util.*;
/**
 *
 * @author  Jm
 */
public class Tipo_proveedorService {
    private Tipo_proveedorDAO  TPDataAccess;
    private List               ListaTP;
    private Tipo_proveedor     Datos;
    
    /** Creates a new instance of Tipo_proveedorService */
    public Tipo_proveedorService() {
        TPDataAccess = new Tipo_proveedorDAO();
        ListaTP = new LinkedList();
      //  Datos = new Tipo_proveedor();
    }
    
    public void INSERT(String dstrct, String codigo_proveedor , String descripcion, String usuario) throws Exception {
        try{
            TPDataAccess.INSERT_TP(dstrct, codigo_proveedor, descripcion, usuario);
        }
        catch(Exception e){
            throw new Exception("Error en INSERT [Tipo_proveedorService]...\n"+e.getMessage());
        }
    }
    
    public void UPDATEESTADO_TP(String reg_status, String codigo_proveedor, String descripcion,  String usuario ) throws Exception {
        try{
            TPDataAccess.UPDATEESTADO_TP(reg_status, codigo_proveedor, descripcion, usuario);
        }
        catch(Exception e){
            throw new Exception("Error en UPDATEESTADO [Tipo_proveedorService]...\n"+e.getMessage());
        }
    }
    
    public void SEARCH_TP(String codigo_proveedor, String descripcion ) throws Exception {
        try{
            this.ReiniciarDato();
            this.Datos = TPDataAccess.SEARCH_TP(codigo_proveedor, descripcion);
        }
        catch(Exception e){
            throw new Exception("Error en SEARCH [Tipo_proveedorService]...\n"+e.getMessage());
        }
    }
    
    public boolean EXISTE_TP(String codigo_proveedor, String descripcion ) throws Exception {
        try{
            return TPDataAccess.EXISTE_TP(codigo_proveedor, descripcion);
        }
        catch(Exception e){
            throw new Exception("Error en EXISTE [Tipo_proveedorService]...\n"+e.getMessage());
        }
    }
    
    public void DELETE_TP(String codigo_proveedor, String descripcion ) throws Exception {
        try{
            TPDataAccess.DELETE_TP(codigo_proveedor, descripcion);
        }
        catch(Exception e){
            throw new Exception("Error en DELETE [Tipo_proveedorService]...\n"+e.getMessage());
        }
    }
    
    public void LIST() throws Exception {
        try{
            this.ReiniciarLista();
            this.ListaTP = TPDataAccess.LIST();
        }
        catch(Exception e){
            throw new Exception("Error en LIST [Tipo_proveedorService]...\n"+e.getMessage());
        }
    }
    
    public void UPDATE_TP(String ncodigo_proveedor, String ndescripcion, String usuario, String codigo_proveedor, String descripcion ) throws Exception {
        try{
            TPDataAccess.UPDATE_TP(ncodigo_proveedor, ndescripcion, usuario, codigo_proveedor, descripcion);
        }
        catch(Exception e){
            throw new Exception("Error en UPDATE [Tipo_proveedorService]...\n"+e.getMessage());
        }
    }
    
    
    public void ReiniciarDato(){
        this.Datos = null;
    }
    public void ReiniciarLista(){
        this.ListaTP = null;
    }
    
    public Tipo_proveedor getDato(){
        return this.Datos;
    }
    
    public List getList(){
        return this.ListaTP;
    }
    
}
