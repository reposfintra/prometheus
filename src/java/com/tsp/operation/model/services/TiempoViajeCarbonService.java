/***********************************************************************************
 * Nombre clase : ............... TiempoViajeCarbonService.java                    *
 * Descripcion :................. Clase que maneja los Servicios                   *
 *                                asignados a Model relacionados con el            *
 *                                programa de TiempoViajeCarbon                    *
 * Autor :....................... Ing. Diogenes Antonio Bastidas Morales           *
 * Fecha :........................ 21 de noviembre de 2005, 08:29 AM               *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/
 

package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  dbastidas
 */
public class TiempoViajeCarbonService {
    
    private TiempoViajeCarbonDAO tiempoviaje;
    
    /** Creates a new instance of TiempoViajeCarbonService */
    public TiempoViajeCarbonService() {
        tiempoviaje = new TiempoViajeCarbonDAO();
    }
    
     /**
     * Metodo tiempoViajeCarbonXLS, lista la planillas con los vehiculos de carbon que esten dentro ,
     * de un rango de fechas 
     * @param: fecha inicial, fecha final 
     * @see:  tiempoViajeCarbonXLS - TiempoViajeCarbonDAO
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public List tiempoViajeCarbonXLS(String fecini, String fecfin) throws SQLException{
        try{
            return tiempoviaje.tiempoViajeCarbonXLS(fecini, fecfin);
        }
        catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    } 
    
     /**
     * Metodo  buscarSJ, lista los stanadres de carbon
     * @param: 
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public Vector buscarSJ( ) throws Exception{
        try{
            return tiempoviaje.buscarSJ();
        }catch(Exception e){
           throw new Exception(e.getMessage());
        }
    }
}
