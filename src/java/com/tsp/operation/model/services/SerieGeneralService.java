/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import java.sql.*;
import com.tsp.operation.model.DAOS.*;





/**
 *
 * @author Alvaro
 */
public class SerieGeneralService { 


    private SerieGeneralDAO serieGeneralDao;



    /** Creates a new instance of ClienteService */
    public SerieGeneralService() {
        serieGeneralDao = new SerieGeneralDAO();
    }
    public SerieGeneralService(String dataBaseName) {
        serieGeneralDao = new SerieGeneralDAO(dataBaseName);
    }

    public SerieGeneral getSerie(String dstrct,String agency_id, String document_type)throws SQLException{
        return serieGeneralDao.getSerie(dstrct,agency_id,document_type);
    }

    public void setSerie(String dstrct,String agency_id, String document_type)throws SQLException{
        serieGeneralDao.setSerie(dstrct,agency_id,document_type);
    }


    public SerieGeneral getSerie(String dstrct,String agency_id, String document_type,String dataBaseName)throws SQLException{
        return serieGeneralDao.getSerie(dstrct,agency_id,document_type, dataBaseName);
    }




    public void setSerie(String dstrct,String agency_id, String document_type,String dataBaseName)throws SQLException{
        serieGeneralDao.setSerie(dstrct,agency_id,document_type, dataBaseName);
    }



}
