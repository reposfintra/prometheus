/*Created on 15 de septiembre de 2005, 02:00 PM
 */

package com.tsp.operation.model.services;

/**@author  fvillacob
 */


import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;


public class ProyectoService {
    
       private ProyectoDAO menu;    
       private List    perfiles;
       private List    usuarios;
       private List    perfilusuarios;
       private List    usuariosperfil;
       private String  varSeparadorJS = "~";
       private String  varUsuariosJS;
       private String  varPerfilJS;
       private String  varPerfilUsuariosJS;
       private String  varUsuariosPerfilJS;
       
       private String PARAM1;
       private String PARAM2;

   
      public ProyectoService() {
            menu           = new ProyectoDAO();
            perfiles       = null;
            usuarios       = null;
            perfilusuarios = null;
            usuariosperfil = null;
            PARAM1         = "1";
            PARAM2         = "2";
            create();
      }
      
      
      
    
    public void create(){
        try{ //this.menu.create();
        }
        catch(Exception e){}
    }
    
    
    public List searchProyectos() throws SQLException {
        try{
           create();
           this.perfiles = this.menu.getProyectos(); 
           GenerarJSProyectos();
        }catch(SQLException e){ throw new SQLException(e.getMessage()); }
        return perfiles;
    }
    
    
     public void GenerarJSProyectos(){
        String var = "\n var GruposJS = [ ";
        if (perfiles!=null)
            for (int i=0;i<perfiles.size();i++){
                Perfil per = (Perfil)  perfiles.get(i);
                var += "\n '"+ per.getId() + varSeparadorJS + per.getNombre() +"',";
            }
        var = var.substring(0,var.length()-1) + "];";
        varPerfilJS = var;
    }
     
    
     
    
    public List searchUsuarios() throws SQLException {
        try{
           create();
           this.usuarios  = this.menu.getUsuarios(); 
           GenerarJSUsuarios();
        }catch(SQLException e){ throw new SQLException(e.getMessage()); }
        return usuarios;
    }
    
    
     public void GenerarJSUsuarios(){
        String var = "\n var UsuariosJS = [ ";
        if (usuarios!=null)
            for (int i=0;i<usuarios.size();i++){
                Usuario usu = (Usuario)  usuarios.get(i);
                var += "\n '"+ usu.getLogin() + varSeparadorJS + usu.getNombre() +"',";
            }
        var = var.substring(0,var.length()-1) + "];";
        varUsuariosJS = var;
    }
    
     
     
    
     public List searchProyectosUsuarios() throws SQLException {
        try{
           create();
           this.perfilusuarios  = this.menu.getProyectoUsuarios(PARAM1); 
           GenerarJSProyectoUsuarios();
        }catch(SQLException e){ throw new SQLException( " searchProyectosUsuarios(): " +e.getMessage()); }
        return perfilusuarios;
    }
     
     
     public void GenerarJSProyectoUsuarios(){
        String var = "\n var PerfilUsuariosJS = '";
        if (perfilusuarios!=null)
            for (int i=0;i<perfilusuarios.size();i++){
                PerfilUsuario usu = (PerfilUsuario)  perfilusuarios.get(i);
                var +=  usu.getPerfil() +"-"+ usu.getUsuarios() +"|";
            }
        var += "';";
        varPerfilUsuariosJS = var;
    }
     
     
     
    
     
     public List searchUsuariosProyectos() throws SQLException {
        try{
           create();
           this.usuariosperfil  = this.menu.getProyectoUsuarios(PARAM2);
           GenerarJSUsuariosProyectos();
        }catch(SQLException e){ throw new SQLException(e.getMessage()); }
        return  usuariosperfil;
    }
     
     
     
   public void GenerarJSUsuariosProyectos(){
        String var = "\n var UsuariosPerfilJS = '";
        if (usuariosperfil!=null)
            for (int i=0;i<usuariosperfil.size();i++){
                PerfilUsuario usu = (PerfilUsuario)  usuariosperfil.get(i);
                var +=  usu.getUsuarios() +"-"+ usu.getPerfil()+ "-"+ usu.getPerName() +"|";
            }
        var += "';";
        varUsuariosPerfilJS = var;
    }  
     
   
   
    
   
    
    
    
   public void Insert (String []ListaUsuarios, String []ListaGrupos, String user)throws Exception{
        try{
            create();
            this.menu.Insert(ListaGrupos,ListaUsuarios, user );
        }catch (Exception ex){
            throw new Exception ("Error en Insert [MenuService] ... \n" + ex.getMessage());
        }
    }
    
  
    public List getPerfiles()           { return this.perfiles;       }
    public List getUsuarios()           { return this.usuarios;       }
    
    
    public String getVarJSUsuario()          { return this.varUsuariosJS;  }
    public String getVarJSProyectos()        { return this.varPerfilJS;    }
    public String getVarJSSeparador()        { return "\n var SeparadorJS = '" + this.varSeparadorJS + "';";  }
    public String getVarJSProyectoUsuario()  { return this.varPerfilUsuariosJS;  }
    
    
    
    
    /*
    public String getVarJSUsuarioPer()  { return this.varUsuariosPerfilJS;  }
    
    */
    
    
}
