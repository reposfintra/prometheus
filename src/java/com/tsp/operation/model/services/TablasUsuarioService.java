/***********************************************************************************
 * Nombre clase : ............... TablasUsuarioService.java                            *
 * Descripcion :................. Clase que instancia los metodos de TablasUsuarioDAO  *
 *                                con la BD.                                            *
 * Autor :....................... Ing. David Lamadrid                                   *
 * Fecha :....................... 4 de diciembre de 2005, 11:02 AM                     *
 * Version :..................... 1.0                                                   *
 * Copyright :................... Fintravalores S.A.                               *
 ***********************************************************************************/

package com.tsp.operation.model.services;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
/**
 *
 * @author  dlamadrid
 */
public class TablasUsuarioService {
    TablasUsuarioDAO dao;
    /** Creates a new instance of TablasUsuarioService */
    public TablasUsuarioService () {
        dao= new TablasUsuarioDAO ();
    }
    
    /**
     * inserta un registro en la tabla tablas_usuario
     * @autor David Lamadrid
     * @throws SQLException
     * @see TablasUsuarioDAO
     * @version 1.0
     */
    public void insertar () throws SQLException {
        dao.insertar ();
    }
    
    /**
     * elimina un registro en la tabla tablas_usuario dada la llave primaria del registro
     * @autor David Lamadrid
     * @throws SQLException
     * @see TablasUsuarioDAO
     * @version 1.0
     */
    public void eliminar (int codigo) throws SQLException {
        dao.eliminar (codigo);
    }
    
    /**
     * Lista los registros de tablas_usuario por usuario
     * @autor David Lamadrid
     * @throws SQLException
     * @see TablasUsuarioDAO
     * @version 1.0
     */
    public void listarPorU (String codigo) throws SQLException {
        dao.listarPorU (codigo);
    }
    
    /**
     * Lista todos los registros del la tabla tablas_usuario
     * @autor David Lamadrid
     * @throws SQLException
     * @see TablasUsuarioDAO
     * @version 1.0
     */
    public void listarTodo () throws SQLException {
        dao.listarTodo ();
    }
    
    /**
     * Metodo que retorna false si no exite el registro y true si un usuario ya tiene una tabla asignada con el mismo nombre
     * @autor David Lamadrid
     * @throws SQLException
     *@see TablasUsuarioDAO
     * @version 1.0
     */
    public boolean existePorU (String nombre,String usuario) throws SQLException {
        return dao.existePorU (nombre,usuario);
    }
    
    /**
     * Getter for property vTablas.
     * @see TablasUsuarioDAO
     * @return Value of property vTablas.
     */
    public java.util.Vector getVTablas () {
        return  dao.getVTablas ();
    }
    
    /**
     * Setter for property vTablas.
     *@see TablasUsuarioDAO
     * @param vTablas New value of property vTablas.
     */
    public void setVTablas (java.util.Vector vTablas) {
        dao.setVTablas (vTablas);
    }
    
    /**
     * Getter for property tabla.
     *@see TablasUsuarioDAO
     * @return Value of property tabla.
     */
    public com.tsp.operation.model.beans.TablasUsuario getTabla () {
        return dao.getTabla ();
    }
    
    /**
     * Setter for property tabla.
     *@see TablasUsuarioDAO
     * @param tabla New value of property tabla.
     */
    public void setTabla (com.tsp.operation.model.beans.TablasUsuario tabla) {
        dao.setTabla (tabla);
    }
    
    /**
     * Getter for property tTablas.
     *@see TablasUsuarioDAO
     * @return Value of property tTablas.
     */
    public java.util.TreeMap getTTablas () {
        return dao.getTTablas ();
    }
    
    /**
     * Setter for property tTablas.
     * @param tTablas New value of property tTablas.
     */
    public void setTTablas (java.util.TreeMap tTablas) {
        dao.setTTablas (tTablas);
    }
    
    /**
     * metodo que retorna true si existe una tabla en la bd
     * @autor David Lamadrid
     * @throws SQLException
     * @param String nombre
     * @see TablasUsuarioDAO
     * @version 1.0
     */
    public  boolean existeTabla (String nombre ) throws SQLException {
        return   dao.existeTabla (nombre);
    }
    
    /**
     * metodo que carga un TreeMap con las tablas asignadas por el uusario
     * @autor David Lamadrid
     * @throws SQLException
     * @param String usuario
     * @see TablasUsuarioDAO
     * @version 1.0
     */
    public void tTablasPorUsuario (String usuario) throws SQLException {
        dao.tTablasPorUsuario (usuario);
    }
    
    /**
     * Getter for property tTabla.
     * @return Value of property tTabla.
     */
    public java.util.TreeMap getTTabla () {
        return dao.getTTabla ();
        
    }
    
    /**
     * Setter for property tTabla.
     * @param tTabla New value of property tTabla.
     */
    public void setTTabla (java.util.TreeMap tTabla) {
        dao.setTTabla (tTabla);
    }
    
    public void buscarTablas (String nombre ) throws SQLException{
        dao.buscarTablas (nombre);
    }
    
    /**
     * Getter for property vNombreTablas.
     *@see TablasUsuarioDAO 
     * @return Value of property vNombreTablas.
     */
    public java.util.Vector getVNombreTablas () {
        return dao.getVNombreTablas ();
    }
    
    /**
     * Setter for property vNombreTablas.
     *@see TablasUsuarioDAO 
     * @param vNombreTablas New value of property vNombreTablas.
     */
    public void setVNombreTablas (java.util.Vector vNombreTablas) {
        dao.setVNombreTablas (vNombreTablas);
    }
    
}
