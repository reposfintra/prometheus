package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.BancosDAO;
import com.tsp.operation.model.beans.Bancos;
import java.util.ArrayList;

/**
 * Service para la tabla de bancos
 * @author darrieta - geotech
 */
public class BancosService {
    
    BancosDAO dao;

    public BancosService() {
        dao = new BancosDAO();
    }
    public BancosService(String dataBaseName) {
        dao = new BancosDAO(dataBaseName);
    }
    
    /**
     * Obtiene los registros no anulados de la tabla bancos
     * @return ArrayList<Bancos> con los resultados obtenidos
     * @throws Exception 
     */
    public ArrayList<Bancos> obtenerBancosNit() throws Exception {
        return dao.obtenerBancosNit();
    }
    

    /**
     * Obtiene los registros de la tabla bancos de acuerdo a la nacionalidad
     * @param nacionales true si se quiere buscar bancos nacionales o false si se quiere buscar extranjeros
     * @return ArrayList<Bancos> con los resultados obtenidos
     * @throws Exception 
     */
    public ArrayList<Bancos> obtenerBancosNacionalidad(boolean nacionales) throws Exception {
        return dao.obtenerBancosNacionalidad(nacionales);
    }
    
    /**
     * Obtiene un banco de acuerdo a su nit
     * @param nit
     * @return bean con los datos del banco
     * @throws Exception 
     */
    public Bancos obtenerBancoXNit(String nit) throws Exception {
        return dao.obtenerBancoXNit(nit);
    }
    
    /**
     * Obtiene los registros no anulados de la tabla bancos
     * @return ArrayList<Bancos> con los resultados obtenidos
     * @throws Exception 
     */
    public ArrayList<Bancos> obtenerBancos() throws Exception {
        return dao.obtenerBancos();
    }
}
