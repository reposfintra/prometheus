/*************************************************************
 * Nombre      ............... ImportarService.java
 * Descripcion ............... Clase general de importaciones
 * Autor       ............... mfontalvo
 * Fecha       ............... Octubre - 01 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.ImportacionDAO;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.Evento;

import java.util.*;
import java.io.*;


/**
 * Service de Importaciones
 */
public class ImportacionService {
    
    /**
     * Objeto de enlace a la base de datos
     */    
    ImportacionDAO ImportDataAccess;
    
    /**
     * Listado general de retorno donde se manipulan listas de Parametros de
     * Importacion
     */    
    List    Listado;
    
    /**
     * Elemento especifico de retorno don se alamacena un solo elemnto de parametros de
     * importacion
     */    
    ImportacionParametros dt;
    
    /**
     * Errores generados por el proceso de importacion
     */    
    TreeMap errores;
    
    /**
     * Listado de tablas generales
     */
    TreeMap tablas;
    
    /** Creates a new instance of ImportacionService */
    public ImportacionService() {
        ImportDataAccess = new ImportacionDAO();
    }
    public ImportacionService(String dataBaseName) {
        ImportDataAccess = new ImportacionDAO(dataBaseName);
    }
    
    /**
     * Procedimiento para agragar una tabla de importacion en las siguientes
     * tablas
     *  - importacion_parametros
     *  - importacion_estrucutura
     * @autor mfontalvo.
     * @see ImportDataAccess.agregarEstructura(String [], String [], String [] , String [], String [], String , String , String ).
     * @param descripcion deascripcion de la tabla
     * @param campos Array de campos a ingresar
     * @param estructura Array de definiciones de Importacion
     * @param def Arreglo de valores que indica si el campo posee un valor default o no de migracion
     * @param tdef Arreglo de valores que indica si el tipo de valores default que posee un campo.
     * @param vdef valor default de la variable, este se implementará si en el momento de la
     * importacion este campo viene vacio
     * @param tabla nombre de la tabla de importacion
     * @param usuario usuario que registra la tabla de importacion
     * @throws Exception .
     */
    
    public void agregarEstructura   (String [] campos, String [] estructura, String [] def, String [] tdef, String [] vdef , String [] alias, String [] validaciones, String inserciones[], String []update, String []obs_v, String []obs_i, String formato, String tabla, String descripcion, String usuario) throws Exception{
        try{
            ImportDataAccess.agregarEstructura(campos, estructura, def, tdef, vdef, alias, validaciones, inserciones, update, obs_v, obs_i, formato, tabla, descripcion, usuario);
        }catch (Exception ex){
            throw new Exception ("Error en agregarEstructura [ImportacionService]....\n" + ex.getMessage());
        }
    }

    /**
     * Procedimiento para agragar una tabla de importacion en las siguientes
     * tablas
     *  - importacion_parametros
     *  - importacion_estrucutura
     * @autor mfontalvo.
     * @see ImportDataAccess.agregarEstructura(String [], String [], String [] , String [], String [], String , String , String ).
     * @param descripcion deascripcion de la tabla
     * @param campos Array de campos a ingresar
     * @param estructura Array de definiciones de Importacion
     * @param def Arreglo de valores que indica si el campo posee un valor default o no de migracion
     * @param tdef Arreglo de valores que indica si el tipo de valores default que posee un campo.
     * @param vdef valor default de la variable, este se implementará si en el momento de la
     * importacion este campo viene vacio
     * @param tabla nombre de la tabla de importacion
     * @param usuario usuario que registra la tabla de importacion
     * @throws Exception .
     */
    public void modificarEstructura (String [] campos, String [] estructura, String [] def, String tdef[], String []vdef, String [] alias, String [] validaciones, String []inserciones, String []update, String []obs_v, String []obs_i, String formato, String tabla, String descripcion, String usuario) throws Exception{
        try{
            ImportDataAccess.modificarEstructura(campos, estructura, def, tdef, vdef , alias, validaciones, inserciones, update, obs_v, obs_i, formato, tabla, descripcion, usuario );
        }catch (Exception ex){
            throw new Exception ("Error en modificarEstructura [ImportacionService]....\n" + ex.getMessage());
        }
    }    
    
    
 
    /**
     * Metodo para agregar en importacion_parametros datos completos
     * @autor mfontalvo.
     * @param tabla nombre de la tabla
     * @param descripcion descripcion de la tabla de importacion
     * @param ctab parametro de construccion del archivo y toma los siguientes valores
     *  -   A: nombre del archivo
     *  -   T: nombre de la tabla
     * @param fcon parametro de fecha de construccion del archivo y toma los siguientes valores
     *  - A: fecha del Archivo
     *  - S: fecha actual
     *  - N: no aplicar fecha
     * @param adat parametro que indica la actualizacion de datos del archivo de migracion
     *    -   A: adicionar datos al momento de subirlos
     *    -   E: eliminar los datos actuales antes de subir el archivo
     * @param bpna parametro de busqueda por nombre de archivo
     *    - I: inicia por el nombre del archivo
     *    - C: nombre completo
     * @param usuario usuario que registra el parametro de importacion
     * @throws Exception .
     * @see agregarParametros (String, String, String, String , String, String, String ).
     */
    
    public void agregarParametros (String formato, String tabla, String descripcion, String ctab, String fcon, String adat, String bpna, String ins, String upd, String usuario) throws Exception{
        try{
            ImportDataAccess.agregarParametros(formato, tabla, descripcion, ctab, fcon, adat, bpna, ins, upd, usuario);
        }catch (Exception ex){
            throw new Exception ("Error en agregarParametros [ImportacionService]....\n" + ex.getMessage());
        }
    }      
    

    /**
     * Metodo para modificar los parametros de importacion
     * @autor mfontalvo.
     * @see agregarParametros (String, String, String, String , String, String, String ).
     * @param titulo indica si el archivo de importacion pose o no titulo (S/N)
     *
     * @param evento Nombre del evento que manipulará la importacion, en caso de no definirlo
     * se encargara de procesarlo por default el mismo programa en general
     * @param tabla nombre de la tabla
     * @param descripcion descripcion de la tabla de importacion
     * @param ctab parametro de construccion del archivo y toma los siguientes valores
     *  -   A: nombre del archivo
     *  -   T: nombre de la tabla
     * @param fcon parametro de fecha de construccion del archivo y toma los siguientes valores
     *  - A: fecha del Archivo
     *  - S: fecha actual
     *  - N: no aplicar fecha
     * @param adat parametro que indica la actualizacion de datos del archivo de migracion
     *    -   A: adicionar datos al momento de subirlos
     *    -   E: eliminar los datos actuales antes de subir el archivo
     * @param bpna parametro de busqueda por nombre de archivo
     *    - I: inicia por el nombre del archivo
     *    - C: nombre completo
     * @param usuario usuario que registra el parametro de importacion
     * @throws Exception .
     */
   
    
    public void modificarParametros (String formato, String tabla, String descripcion, String ctab, String fcon, String adat, String bpna, String titulo, String evento, String ins, String upd, String usuario) throws Exception{
        try{
            ImportDataAccess.modificarParametros(formato, tabla, descripcion, ctab, fcon, adat, bpna, titulo, evento, ins, upd, usuario);
        }catch (Exception ex){
            throw new Exception ("Error en modificarParametros [ImportacionService]....\n" + ex.getMessage());
        }
    }      
    
    
    /**
     * rutina que elimina una tabla de
     * - importacion_parametros
     * - importacion_estructura
     * @autor mfontalvo.
     * parametros de entrada un array de string donde
     * vienen todas las tablas que se deben borrar
     * @param tablas Arreglo de tablas a eliminar
     * @throws Exception .
     */
    
    public void eliminar (String [] formatos) throws Exception{
        try{
            ImportDataAccess.eliminarEstructura( formatos );
        }catch (Exception ex){
            throw new Exception ("Error en eliminar [ImportacionService]....\n" + ex.getMessage());
        }
    }     
    
    
    /**
     * procedimiento que busca un listado general de las tablas que tienen
     * definidas parametros y estructuras
     * @autor mfontalvo.
     * @throws Exception .
     */ 
    
    public void listadoGeneral () throws Exception {
        try{
            Listado = ImportDataAccess.Listado();
        }catch (Exception ex){
            throw new Exception ("Error en listadoGeneral [ImportacionService]...\n" + ex.getMessage());
        }
    }
    
    
    /**
     * procedimiento para buscar los datos de una tabla especifica
     * @param tabla nombre de la tabla
     * @autor mfontalvo.
     * @throws Exception .
     */
    
    public void Buscar ( String formato ) throws Exception {
        try{
            dt = ImportDataAccess.Buscar( formato );
            if (dt!=null){
                List key = ImportDataAccess.obtenerLLavesPrimarias( dt.getTabla() );
                if ( dt != null && key != null ) {
                    for (int i = 0; dt.getListaCampos() != null && i < dt.getListaCampos().size(); i++){
                        ImportacionEstructura est = (ImportacionEstructura) dt.getListaCampos().get(i);
                        for (int j = 0; key != null && j < key.size() ; j++ ){
                            String k = (String) key.get(j);
                            if (est.getCampo().equalsIgnoreCase(k)){
                                est.setPrimaryKey(true);
                                break;
                            }
                        }
                    }

                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ("Error en listadoGeneral [ImportacionService]...\n" + ex.getMessage());
        }
    }   
    

    /**
     * Procedimiento para buscar las tablas que contiene la base datos
     * @autor mfontalvo.
     * @throws Exception .
     */
    
    public void ListarTablas (  ) throws Exception {
        try{
            tablas = ImportDataAccess.ListarTablas();
        }catch (Exception ex){
            throw new Exception ("Error en ListarTablas [ImportacionService]...\n" + ex.getMessage());
        }
    }     
    
    
    /**
     * Procedimiento para incluir una tabla de la base de datos como una tabla
     * de importacion
     * @autor mfontalvo.
     * @param ntabla nombre de la tabla a incluir
     * @param usuario usuario del proceso
     * @throws Exception
     * @return devuelve si se pudo incluir o no en la tabla de migracion
     */    
    public boolean Incluir ( String formato, String ntabla, String usuario) throws Exception {
        try{
            return ImportDataAccess.Incluir(formato, ntabla, usuario);
        }catch (Exception ex){
            throw new Exception ("Error en importar [ImportacionService]...\n" + ex.getMessage());
        }
    }  
    
    /**
     * Procedimineto para actualizar la tabla de migracion contra los cambios hechos 
     * en la base de datos.
     * @autor mfontalvo.
     * @param usuario usuario del proceso
     * @throws Exception
     */    
    public void Recargar ( String usuario ) throws Exception {
        try{
            
            ImportacionParametros viejo = getDatos();
            eliminar( new String [] { viejo.getFormato() } );
            if ( ImportDataAccess.Incluir(viejo.getFormato(), viejo.getTabla(), usuario) ) {
                
                Buscar(viejo.getFormato());
                ImportacionParametros nuevo = getDatos();
                nuevo.setDescripcion( viejo.getDescripcion() );
                
                
                // recorremos todos los campos nuevos para actulizarlo
                for (int i = 0 ; i < nuevo.getListaCampos().size(); i++ ){
                    ImportacionEstructura impen = 
                        (ImportacionEstructura) nuevo.getListaCampos().get(i);
                    
                    // busco la informacion del campo en la version anterior
                    for (int j = 0; j < viejo.getListaCampos().size(); j++){
                        ImportacionEstructura impev = 
                           (ImportacionEstructura) viejo.getListaCampos().get(j);
                        if (impen.getCampo().equals( impev.getCampo())  ){
                            
                            impen.setDefault         ( impev.getDefault   () );
                            impen.setVariable        ( impev.getVariable  () );
                            impen.setConstante       ( impev.getConstante () );
                            impen.setAlias           ( impev.getAlias     () );
                            impen.setInsercion       ( impev.getInsercion () );
                            impen.setValidacion      ( impev.getValidacion() );
                            impen.setAplicaUpdate    ( impev.isAplicaUpdate());
                            impen.setObs_validacion  ( impev.getObs_validacion() );
                            impen.setObs_insercion   ( impev.getObs_insercion () );
                            nuevo.getListaCampos().set(i, impen);
                            break;
                            
                        }
                    }// end for
                }// end for
                
                // modificacion de los paamtros de importacion
                modificarParametros(
                    viejo.getFormato(), viejo.getTabla(), viejo.getDescripcion(), viejo.getCTAB(), viejo.getFCON(), 
                    viejo.getADAT(), viejo.getBPNA(), viejo.getTitulo(), viejo.getEvento(), viejo.getInsert(), viejo.getUpdate(), usuario
                );
                    
                
            }
            
            
        }catch (Exception ex){
            throw new Exception ("Error en importar [ImportacionService]...\n" + ex.getMessage());
        }
    }    
    

    
    
    /**
     * Procedimento para realizar el proceso de imporcacion dado
     * el listado de datos y el archivo
     * @autor mfontalvo.
     * @param listado listado de datos
     * @param archivo archivo de migracion
     * @param constantes TreeMap de constantes de importacion
     * @throws Exception .
     * @see setearListaImportacion (List, ImportacionParametros, TreeMap, int[])
     * @see generarPosiciones(ImportacionParametros, String[])
     */
    public void importarInsert ( List listado, File archivo, TreeMap constantes, Usuario usuario) throws Exception {
        try{
            
            
            // dependiendo del nombre del  archivo en este procedimiento
            // devuelve la estructura del insert para la tabla mas apropiada
            // para importar, en caso contrario, arroja un excepcion indicando
            // que no se encontro ninguna tabla
                        
            ImportacionParametros tabParams = ImportDataAccess.prepararInsert(archivo, usuario.getLogin());
            
            
            // si la tabla contiene titulo se elimina
            // el primer registro.
            
            String [] titulos    = null;
            int    [] posTitulos = null;
            if (tabParams.getTitulo().equals("S")){
                titulos = (String []) listado.get(0);
                posTitulos = this.generarPosicionesInsert(tabParams.getListaCampos(), titulos);
                listado.remove(0);
            }              
            
            
            // seteo de paramtros de importacion para los campos
            listado = setearListaImportacionInsert(listado, tabParams, constantes, posTitulos);
            
            
            
            // ejecutamos el proceso general
            errores = ImportDataAccess.ImportarInsert(listado, tabParams );
            
            
            
            // proceso del evento especial
            if (!tabParams.getEvento().trim().equals("")){
                try{
                    Evento event = (Evento) com.tsp.util.Util.runClass(tabParams.getEvento().trim());
                    event.setUsuario(usuario);
                    event.start();
                }catch ( Exception ex){
                    throw new Exception ("Error del evento.... \n" + ex.getMessage());
                }                
            }
            
            
            
        }catch (Exception ex){
            throw new Exception ("Error en importarInsert [ImportacionService]...\n" + ex.getMessage());
        }
    }   
    
    
    
    /**
     * Procedimento para realizar el proceso de imporcacion dado
     * el listado de datos y el archivo
     * @autor mfontalvo.
     * @param listado listado de datos
     * @param archivo archivo de migracion
     * @param constantes TreeMap de constantes de importacion
     * @throws Exception .
     * @see setearListaImportacion (List, ImportacionParametros, TreeMap, int[])
     * @see generarPosiciones(ImportacionParametros, String[])
     */
    public void importarUpdate ( List listado, File archivo, Usuario usuario) throws Exception {
        try{
            
            
            
            // si la tabla contiene titulo se elimina
            // el primer registro.
            
            String [] titulos    = null;
            int    [] posTitulos = null;
            titulos = (String []) listado.get(0);
            listado.remove(0);
            
            
            // dependiendo del nombre del  archivo en este procedimiento
            // devuelve la estructura del insert para la tabla mas apropiada
            // para importar, en caso contrario, arroja un excepcion indicando
            // que no se encontro ninguna tabla
                        
            ImportacionParametros tabParams = ImportDataAccess.prepararUpdate(archivo.getName(), titulos, usuario.getLogin());
            posTitulos = generarPosicionesUpdate(tabParams.getListaCampos());
            // seteo de paramtros de importacion para los campos
            listado = setearListaImportacionUpdate(listado, tabParams, posTitulos);
            
            
            
            // ejecutamos el proceso general
            errores = ImportDataAccess.ImportarUpdate(listado, tabParams );
            
        }catch (Exception ex){
            throw new Exception ("Error en importarUpdate [ImportacionService]...\n" + ex.getMessage());
        }
    }   
    
    
    /**
     * Setea la lista de importacion segun lis parametros de importacion
     * @autor mfontalvo.
     * @param listado lista a importar
     * @param tabParams parametros de importacion en el momento
     * @param constantes constantes de importacion.
     * @param pos  Posiciones de loas titulos de cabecera del archivo de importacion
     * @throws Exception.
     * @return retorna la lista de importacion modificada.
     * @see setearParametros(int [], String[], ParametrosImportacion, TreeMap)
     */    
    public List setearListaImportacionInsert (List listado, ImportacionParametros tabParams, TreeMap constantes, int[] pos) throws Exception {
        try{
            if (listado!=null){
                for (int i=0; i< listado.size(); i++){
                    String []datos = (String []) listado.get(i);
                    Object []d     = setearParametrosInsert(pos, datos, tabParams, constantes);
                    listado.set(i, d);
                }
            }
            
        }catch (Exception ex){
            throw new Exception ("Error en setearListaImportacion [ImportacionService] .... \n" + ex.getMessage());
        }
        return listado;
    }

    

    /**
     * Setea la lista de importacion segun lis parametros de importacion
     * @autor mfontalvo.
     * @param listado lista a importar
     * @param tabParams parametros de importacion en el momento
     * @param constantes constantes de importacion.
     * @param pos  Posiciones de loas titulos de cabecera del archivo de importacion
     * @throws Exception.
     * @return retorna la lista de importacion modificada.
     * @see setearParametros(int [], String[], ParametrosImportacion, TreeMap)
     */    
    public List setearListaImportacionUpdate (List listado, ImportacionParametros tabParams, int[] pos) throws Exception {
        try{
            if (listado!=null){
                
                int[] posInv = this.invertirPosiciones(pos);
                for (int i=0; i< listado.size(); i++){
                    String []datos = (String []) listado.get(i);
                    
                    
                    String ndatos  [] = new String [ pos.length ]; 
                    for (int j=0; j<pos.length ; j++){
                        ndatos [j] = (pos[j]<datos.length ? datos[pos[j]] : "" );
                    }
                    
                    String dt     [] = new String [ tabParams.getListaCampos().size() ]; 
                    for  ( int j = 0; j < tabParams.getListaCampos().size(); j++ ){
                        ImportacionEstructura est = (ImportacionEstructura) tabParams.getListaCampos().get(j);
                        if ( est.isSeleccionado())
                            dt[j] = ndatos [ posInv[ Integer.parseInt(est.getSecuencia())] ];
                        else
                            dt[j] = est.getConstante();
                    }
                    
                    
                    String status = "";
                    // querys de insercion, es decir, se completan los datos que hacen falta
                    for  ( int j = 0; j < tabParams.getListaCampos().size(); j++ ){
                        ImportacionEstructura est = (ImportacionEstructura) tabParams.getListaCampos().get(j);
                        if (est.isSeleccionado() && !est.getInsercion().equals("")){
                            String []d = this.obtenerDato(tabParams.getListaCampos(), est, dt, "INSERCION");
                            if (!d[0].equals("ok"))
                               status += d[0];
                            else if ( d[1]!=null ){
                                ndatos  [ posInv[ Integer.parseInt(est.getSecuencia()) ]] = d[1];
                                //ndatos1 [ pos   [ Integer.parseInt(est.getSecuencia()) ]] = d[1];
                                dt      [j] = d[1];
                            }
                                
                        }
                    }                    
                    
                    // validacion de variables
                    for  ( int j = 0; j < tabParams.getListaCampos().size(); j++ ){
                        ImportacionEstructura est = (ImportacionEstructura) tabParams.getListaCampos().get(j);
                        if (est.isSeleccionado() && !est.getValidacion().equals("")){
                            String []d = this.obtenerDato(tabParams.getListaCampos(), est, dt, "VALIDACION");
                            if (!d[0].equals("ok"))
                               status += d[0];
                        }
                    }                        
                    
                    if (status.equals("")) status = "ok" ;
                    
                    listado.set(i, new Object [] { status, ndatos } );
                }
                
            }
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ("Error en setearListaImportacionUpdate [ImportacionService] .... \n" + ex.getMessage());
        }
        return listado;
    }
    
    public int[] invertirPosiciones ( int []pos) {
        int posIn [] = null;
        if(pos!=null){
            int max = -1;
            for (int i = 0; i<pos.length;i++){
                if (pos[i] > max)
                    max = pos[i];
            }
            max++;
            
            posIn = new int[max];            
            for (int i = 0; i<pos.length;i++){
                posIn [pos[i]] = i;
            }
        }        
        return  posIn;
    }
    
    
    /**
     * Procedimiento que actualiza los datos segun sea los paraamtros de importacion
     * @autor mfontalvo.
     * @param pos  Posiciones de loas titulos de cabecera del archivo de importacion
     * @param datos datos a actualizar
     * @param param parametros de importacion
     * @param constantes contasntes de importacion
     * @throws Exception.
     * @return arreglo que alamacena el estado de los datos y los datos ya modificados
     */    
    public Object[] setearParametrosInsert (int []pos, String []datos, ImportacionParametros param, TreeMap constantes) throws Exception {
        String status = "";
        String [] ndatos = null;
        try{
            if (param!=null){
                
                // cuadre de la cantidad de columnas
                ndatos = new String[param.getListaCampos().size()];
                
                ////////////////////////////////////////////////////////////////
                if (pos==null){
                    if (datos.length>param.getListaCampos().size()){
                        for (int i = 0; i < param.getListaCampos().size() ;  i++)
                            ndatos[i] = datos[i];
                    }
                    else if ( datos.length < param.getListaCampos().size() ){
                        for (int i = 0; i < param.getListaCampos().size() ;  i++)
                            ndatos[i] = (i<datos.length?datos[i]:"");
                    }
                    else
                        ndatos = datos;
                }
                ////////////////////////////////////////////////////////////////
                else{
                    
                    for (int i = 0; i < pos.length; i++){
                        if (pos[i]!=-1 && pos[i]<datos.length)
                            ndatos[i] = datos[pos[i]];
                        else
                            ndatos[i] = "";
                    }
                }
                
                // seteo de variables de default.
                for  ( int i = 0; i < ndatos.length ; i++ ){
                    ImportacionEstructura est = (ImportacionEstructura) param.getListaCampos().get(i);
                    if ( (ndatos[i]==null || ndatos[i].equals("")) && est.getDefault().equals("S")){
                        constantes.put("_CONST_", est.getConstante());
                        String valor = (String) constantes.get(est.getVariable());
                        if (valor!=null && !valor.equals(""))
                            ndatos[i] = valor;
                    }
                }
                                
                // seteo de variables de insercion
                for  ( int i = 0; i < ndatos.length ; i++ ){
                    ImportacionEstructura est = (ImportacionEstructura) param.getListaCampos().get(i);
                    //if ((ndatos[i]==null || ndatos[i].equals("")) && est.getDefault().equals("N") && !est.getInsercion().equals("")){
                    if (!est.getInsercion().equals("")){
                        String []d = this.obtenerDato(param.getListaCampos(), est, ndatos, "INSERCION");
                        if (d[0].equals("ok"))
                            ndatos[i] = d[1];
                        else 
                            status += d[0];
                    }
                }        
                                
                // validacion de variables
                for  ( int i = 0; i < ndatos.length ; i++ ){
                    ImportacionEstructura est = (ImportacionEstructura) param.getListaCampos().get(i);
                    if (!est.getValidacion().equals("")){
                        String []d = this.obtenerDato(param.getListaCampos(), est, ndatos, "VALIDACION");
                        if (!d[0].equals("ok"))
                           status += d[0];
                    }
                }        
            }
            
            if (status.equals("")) status = "ok";
                        
        }catch (Exception ex){
            throw new Exception ("Error en setearParametros [ImportacionDAO] ...\n" + ex.getMessage());
        }
        return new Object[] { status , ndatos };
    }
        
    /**
     * Funcion para generar las posiciones en que se encuentra la cabecera
     * del archivo de importacion
     * @autor mfontalvo
     * @fecha 2006-01-31
     * @param cols listado de columnas de las tablas
     * @param titulos arreglo de strings que contiene la cabecera del
     * archivo de excel
     * @throws Exception.
     */

    public int[] generarPosicionesInsert(List cols, String []titulos) throws Exception {
        int pos [] = new int [cols.size()];
        try{
            // inicializando las posiciones
            for (int i = 0; i < pos.length; pos[i++] = -1);
            
            String campos = "";
            boolean estado = true;
            
            
            // busqueda de posiciones
            for (int i = 0 ; i< cols.size(); i++){
                ImportacionEstructura est = (ImportacionEstructura) cols.get(i);
                for (int j = 0; j < titulos.length; j++){
                    if (titulos[j].trim().equalsIgnoreCase( est.getAlias().trim()  )){
                        pos[i] = j;
                        break;
                    }
                }
                if (pos[i]==-1 && est.getDefault().equals("N") && est.getInsercion().equals("") ){
                    campos += est.getCampo() + ", ";
                    estado = false;
                } 
            }
          
            
            if (!estado)
                throw new Exception("No se  encontraron las siguientes columnas " + campos + " y son necesariamente requeridos en el formato.");
            
        }catch ( Exception ex){
            throw new Exception(
                "Error en generar las posiciones de cabecera del archivo " +
                "importacion ....\n" + ex.getMessage()
            );
        }
        return pos;
    }
    
    
    /**
     * Funcion para generar las posiciones en que se encuentra la cabecera
     * del archivo de importacion
     * @autor mfontalvo
     * @fecha 2006-01-31
     * @param cols listado de columnas de las tablas
     * @param titulos arreglo de strings que contiene la cabecera del
     * archivo de excel
     * @throws Exception.
     */

    public int[] generarPosicionesUpdate(List cols) throws Exception {
        int pos [] = new int [cols.size()];
        int cpos[] = null;
        try{
            
            // inicializando las posiciones            
            for (int i = 0; i < pos.length; pos[i++] = -1);
            
            String campos = "";
            boolean estado = true;
            
            // busqueda de posiciones para campos que no son llaves
            int p = 0;
            for (int i = 0 ; i< cols.size(); i++){
                ImportacionEstructura est = (ImportacionEstructura) cols.get(i);
                if (est.isSeleccionado() && !est.isPrimaryKey()){
                    pos[p++] = Integer.parseInt( est.getSecuencia() ) ;                   
                }
            }
            
            // busqueda de posiciones para campos que  son llaves
            for (int i = 0 ; i< cols.size(); i++){
                ImportacionEstructura est = (ImportacionEstructura) cols.get(i);
                if (est.isSeleccionado() && est.isPrimaryKey()){
                    pos[p++] = Integer.parseInt( est.getSecuencia() ) ;      
                }
            }    
            
            cpos = new int[ p ];
            for (int i=0;i< cpos.length ; cpos[i] = pos[i++] );
                        
        }catch ( Exception ex){
            throw new Exception(
                "Error en generar las posiciones de cabecera del archivo " +
                "importacion ....\n" + ex.getMessage()
            );
        }
        return cpos;
    }    
    
    
    
    
    
    /**
     * Metodo que setea las variables de un sql definido para un campo
     * ya sea de validacion o insercion
     * @autor mfontalvo
     * @param otrosCampos, Lista de campos de la misma tabla
     * @param est, campo que se va a validar
     * @param []datos, datos que contienen la informacion de los campos
     * @param tipo, inidica el tipo de consulta (Validacion, Insercion)
     * @throws Exception.
     * @return arreglo, la primera posicion indica el estado de la operacion
     * y el segundo el resulta de la consulta ejecutada.
     */
    
    private String[] obtenerDato (List otrosCampos, ImportacionEstructura est, String []datos, String tipo){
        String sql    = (tipo.equalsIgnoreCase("INSERCION")?est.getInsercion():est.getValidacion());
        String dato   = null;
        String status = "ok";
        if (!sql.equals("")){
            sql += " ";
            for (int i = 0; i < otrosCampos.size() ; i++){
                ImportacionEstructura e = (ImportacionEstructura) otrosCampos.get(i);           
                String campo = "formato." + e.getCampo() + " |formato." + e.getCampo() + "=" ;
                sql = sql.replaceAll(campo , " '" + (i<datos.length?datos[i]:"") + "'" );
            }
            try {
                //System.out.println("SQL " + sql );
                dato = ImportDataAccess.obtenerDato(sql);
                if (dato==null){
                    
                    if (tipo.equalsIgnoreCase("INSERCION") && !est.getObs_insercion().equalsIgnoreCase("NOT MESSAGE")){
                        if (est.getObs_insercion().trim().equals(""))
                            status = "[Insercion-"+ est.getAlias() + "] no permitio continuar con el proceso. ";
                        else 
                            status = "ERROR: " + est.getObs_insercion();                       
                    } else if (tipo.equalsIgnoreCase("VALIDACION") && !est.getObs_validacion().equalsIgnoreCase("NOT MESSAGE")) { 
                        if (est.getObs_validacion().trim().equals(""))
                            status = "[Validacion-"+ est.getAlias() + "] no permitio continuar con el proceso. ";
                        else
                            status = "ERROR: " + est.getObs_validacion();                        
                    }
                }
            } catch (Exception ex){
                status = "["+ tipo +"], Estructura SQL mal definida para el campo " + est.getAlias() + ". ";
            }
        }
        return new String [] { status , dato };
    }
    
    
    
    
    
    /**
     * Metodo de verifiacion de permiso del usuario sobre un formato
     * autor mfontalvo
     * @throws Exception.
     * @return msg, mensaje que indica el estado inicial del proceso.
     */
    public String usuarioValido(String archivo, String usuario, String tipo) throws Exception{
        return ImportDataAccess.usuarioValido(archivo, usuario, tipo);
    }
    
    /**
     * funciones para devolver datos generales de importacion
     * @return Listado de parametros de Importacion.
     * @autor mfontalvo
     */
    
    public List getListado(){
        return this.Listado;
    }
    
    /**
     * Procedimineto para obtener el objeto parametros de importacion 
     * seteado en el momneto
     * @autor mfontalvo.
     * @return devuelve el objeto parametros de importacion
     */    
    public ImportacionParametros getDatos(){
        return this.dt;
    }
    
    /**
     * Procedimiento para reinicializar los parametros de importacion
     * @autor mfontalvo.
     */
    
    public void ResetDato(){
        dt = null;
    }
    
    /**
     * Retorna errores producidos por el proceso de importacion
     * @autor mfontalvo.
     * @return Treemap de errores.
     * @autor mfontalvo
     */    
    public TreeMap getErrores(){
        return this.errores;
    }
      
    /**
     * Retorna el listado de tablas generales de la base de datos
     * @autor mfontalvo.
     * @return Treemap de tablas genrales.
     * @autor mfontalvo
     */    
    public TreeMap getTablas(){
        return this.tablas;
    }    
    
    
    
}
