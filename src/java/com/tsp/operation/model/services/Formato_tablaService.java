/****************************************************************************************************
 * Nombre clase: Formato_tablaService.java                                                          *
 * Descripci�n: Clase que maneja los servicios al model relacionados con los formatos de tabla.     *
 * Autor: Ing. Jose de la rosa                                                                      *
 * Fecha: 25 de noviembre de 2006, 11:13 AM                                                         *
 * Versi�n: Java 1.0                                                                                *
 * Copyright: Fintravalores S.A. S.A.                                                          *
 ***************************************************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.util.Util;

public class Formato_tablaService {
    private Formato_tablaDAO ftabla;
    //operez
    private Vector formatos;
    private Vector vector;
    private Vector datos;
    private String[] titulos;
    private String[] campos;
    
    /** Creates a new instance of Formato_tablaService */
    public Formato_tablaService() {
        ftabla = new Formato_tablaDAO();
    }
    public Formato_tablaService(String dataBaseName) {
        ftabla = new Formato_tablaDAO(dataBaseName);
    }
    
    public Formato_tabla getFtabla() {
        return this.ftabla.getFtabla();
    }
    
    public void setFtabla(Formato_tabla ftabla) {
        this.ftabla.setFtabla( ftabla );
    }
    
    public Vector getVecftabla() {
        return this.ftabla.getVecftabla();
    }
    public void setVecftabla(Vector Vecftabla) {
        this.ftabla.setVecftabla( Vecftabla );
    }
    
    public Vector getVecformato() {
        return this.ftabla.getVecformato();
    }
    
    public Vector getVecftablaMod() {
        return this.ftabla.getVecftablaMod();
    }
    /**
     * Setter for property VecftablaMod.
     * @param VecftablaMod New value of property VecftablaMod.
     */
    public void setVecftablaMod (Vector VecftablaMod) {
        this.ftabla.setVecftablaMod(VecftablaMod);
    }
    
    public void resetVecftablaMod () {
        this.ftabla.resetVecftablaMod();
    }
    
    /**
     * Metodo: insertFormato_tabla, permite ingresar un registro a la tabla formato_tabla.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String insertFormato_tabla(Formato_tabla ftabla) throws SQLException{
        setFtabla(ftabla);
        return this.ftabla.insertFormato_tabla();
    }
    
    /**
     * Metodo: existeTabla, permite buscar si existe el nombre de la tabla en la BD
     * @autor : Ing. Jose de la rosa
     * @param : nombre de la tabla.
     * @version : 1.0
     */
    public void existeTabla(String nombre_tabla) throws Exception{
        try{
            this.ftabla.existeTabla( nombre_tabla );
        }catch(Exception ex){
            ex.getMessage();
        }
    }
    
    /**
     * Metodo: updateTabla, permite actualizar el campo referencia en tablagen.
     * @autor : Ing. Jose de la rosa
     * @param : nombre de la tabla, codigo de la tabla, referencia.
     * @version : 1.0
     */
    public String updateTabla(String table_type, String table_code, String referencia) throws SQLException{
        return this.ftabla.updateTabla( table_type, table_code, referencia );
    }
    
    /**
     * Metodo: formatoTabla, permite todos los formatos en tablagen
     * @autor : Ing. Jose de la rosa
     * @param : numero del estandar, el tipo de acuerdo.
     * @version : 1.0
     */
    public void formatoTabla( String table_type ) throws SQLException{
        try{
            this.ftabla.formatoTabla( table_type );
        }catch(Exception ex){
            ex.getMessage();
        }
    }
    
    /**
     * Metodo: formatoTablaModificar, permite todos los formatos en tablagen que se pueden modificar
     * @autor : Ing. Jose de la rosa
     * @param : numero del estandar, el tipo de acuerdo.
     * @version : 1.0
     */
    public void formatoTablaModificar( String table_type ) throws SQLException{
        try{
            this.ftabla.formatoTablaModificar( table_type );
        }catch(Exception ex){
            ex.getMessage();
        }
    }
    
    /**
     * Metodo: existeFormatoTabla, verifica si existe el formato en la tabla
     * @autor : Ing. Jose de la rosa
     * @param : distrito, formato.
     * @version : 1.0
     */
    public boolean existeFormatoTabla( String distrito, String formato, String tabla, String campo ) throws SQLException{
        return this.ftabla.existeFormatoTabla( distrito, formato, tabla, campo );
    }
    
    /**
     * Metodo: existeTablaModificar, permite buscar si existe el nombre de la tabla en la BD
     * @autor : Ing. Jose de la rosa
     * @param : nombre de la tabla.
     * @version : 1.0
     */
    public void existeTablaModificar(String distrito, String formato ) throws SQLException{
        try{
            this.ftabla.existeTablaModificar( distrito, formato );
        }catch(Exception ex){
            ex.getMessage();
        }
    }
    
    /**
     * Metodo: deleteTabla, permite eliminar todos los datos de la tabla.
     * @autor : Ing. Jose de la rosa
     * @param : nombre de la tabla, codigo de la tabla, referencia.
     * @version : 1.0
     */
    public String deleteTabla( String distrito, String formato, String tabla ) throws SQLException{
        return this.ftabla.deleteTabla( distrito, formato, tabla );
    }
    
    /**
     * Metodo: anularTabla, permite anular todos los datos de la tabla.
     * @autor : Ing. Jose de la rosa
     * @param : nombre de la tabla, codigo de la tabla, referencia.
     * @version : 1.0
     */
    public String anularTabla( String distrito, String formato, String tabla ) throws SQLException{
        return this.ftabla.anularTabla( distrito, formato, tabla );
    }
    
    /**
     * Metodo: converTipoCampo, permite buscar el valor de conversion del tipo de formato
     * @autor : Ing. Jose de la rosa
     * @param : nombre tabla, tipo de campo
     * @version : 1.0
     */
    public String converTipoCampo( String table_type, String tipo_campo ) throws SQLException{
        return this.ftabla.converTipoCampo( table_type, tipo_campo );
    }
    
    /**
     * Metodo: cargar campos para el formulario de acuerdo  a una tabla y formato
     * @autor : Ing. Diogenes Bastidas
     * @param : nombre tabla
     * @version : 1.0
     */
    public void camposFormulario( String formato ) throws SQLException{
        ftabla.camposFormulario(formato );
    }
    
    public Vector getVecCampos() {
        return ftabla.getVecCampos();
    }
    public void setVecCampos(Vector VecCampos) {
        ftabla.setVecCampos(VecCampos);
    }
    
    /**
     * Metodo: cargar campos para el formulario de busqueda
     * @autor : Ing. Diogenes Bastidas
     * @param : nombre tabla
     * @version : 1.0
     */
    public void camposFormularioBusqueda( String formato ) throws SQLException{
        ftabla.camposFormularioBusqueda(formato);
    }
    
    /**
     * Getter for property vecbusqueda.
     * @return Value of property vecbusqueda.
     */
    public Vector getVecbusqueda() {
        return ftabla.getVecbusqueda();
    }
    
    /**
     * Setter for property vecbusqueda.
     * @param vecbusqueda New value of property vecbusqueda.
     */
    public void setVecbusqueda(Vector vecbusqueda) {
        this.ftabla.setVecbusqueda(vecbusqueda);
    }
    
    /**
     * Metodo: formatoTabla, permite todos los formatos que tengan estructura
     * @autor : Ing. Diogenes Bastidas
     * @param : nombre tabla
     * @version : 1.0
     */
    public void formatoDefinidos( String table_type ) throws SQLException{
        ftabla.formatoDefinidos(table_type);
    }
    
    /**
     * Getter for property fordefinidos.
     * @return Value of property fordefinidos.
     */
    public Vector getFordefinidos() {
        return ftabla.getFordefinidos();
    }
    
    /**
     * Metodo: obtenerCamposTabla, obtiene los campos de una tabla
     * @autor : Ing. Osvaldo P�rez Ferrer
     * @param : String distrito
     * @param : String tabla, tabla de la cual se desean obtener los campos
     * @param : String formato, formato al que pertenece la tabla
     * @version : 1.0
     */
    public void obtenerCamposTabla( String distrito, String formato ) throws SQLException{
        this.vector = ftabla.obtenerCamposTabla( distrito ,formato );
    }
    
    /**
     * Metodo: obtenerFormatosTablas, obtiene los distintos formatos y tablas existentes
     * @autor : Ing. Osvaldo P�rez Ferrer
     * @param : distrito
     * @version : 1.0
     */
    public void obtenerFormatosTablas( String distrito ) throws SQLException{
        this.formatos = ftabla.obtenerFormatosTablas( distrito );
    }
    
    /**
     * Getter for property formatos.
     * @return Value of property formatos.
     */
    public java.util.Vector getFormatos() {
        return formatos;
    }
    
    /**
     * Setter for property formatos.
     * @param formatos New value of property formatos.
     */
    public void setFormatos(java.util.Vector formatos) {
        this.formatos = formatos;
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return vector;
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        this.vector = vector;
    }
    
    public void obtenerDatosTabla( String dstrct, String conditions, String campos ) throws Exception{
        this.datos = ftabla.obtenerDatosTabla(dstrct, conditions, campos);
    }
    
    /**
     * Getter for property titulos.
     * @return Value of property titulos.
     */
    public java.lang.String[] getTitulos() {
        return this.titulos;
    }
    
    /**
     * Setter for property titulos.
     * @param titulos New value of property titulos.
     */
    public void setTitulos(java.lang.String[] titulos) {
        this.titulos = titulos;
    }
    
    /**
     * Getter for property campos.
     * @return Value of property campos.
     */
    public java.lang.String[] getCampos() {
        return this.campos;
    }
    
    /**
     * Setter for property campos.
     * @param campos New value of property campos.
     */
    public void setCampos(java.lang.String[] campos) {
        this.campos = campos;
    }
    /*
    public double calcularVlrFor( Movpla m, String moneda_local ) throws Exception{
     
        //Se calcula el vlr a partir de vlr_for
        double value = 0;
     
        Tasa t = hayTasa( moneda_local,m.getCurrency(), moneda_local );
        if( t != null ){
            return t.getValor_tasa() * m.getVlr_for();
        }
        return 0;
     
     
    }
     */
    public Tasa hayTasa(  String moneda_local, String currency, String desired  ) throws Exception{
        TasaService ts = new TasaService();
        Calendar c = Calendar.getInstance();
        String hoy = c.get(1)+"-"+( c.get(2)+1 )+"-"+c.get(5);
        return  ts.buscarValorTasa( moneda_local, currency, desired, hoy );
    }
    
    
    public String insertIntoMovpla( Vector datos, Usuario u, String moneda, String formato ) throws Exception{
        
        String mens = "";
        String curr = "";
        
        for( int i=0; i<datos.size(); i++ ){
            
            Movpla m = new Movpla();
            Hashtable h = (Hashtable)datos.get(i);
            
            String oid = (String) h.get("oid");
            curr = (String)h.get("moneda");
            
            Tasa t = hayTasa( moneda, curr, moneda );
            ////System.out.println("moneda= "+moneda+", curr= "+ curr);
            if( t!=null ){
                ////System.out.println("HAY TASA");
                m.setDstrct             ( nvl( (u.getDstrct() ) ) ) ;
                m.setAgency_id          ( nvl( ((String)h.get("agencia") ) ) );
                m.setDocument_type      ( nvl( ("001") ) );
                m.setConcept_code       (  ( ((String)h.get("concepto") ) ) );
                
                m.setPlanilla           ( nvl( ((String)h.get("numpla") ) ) );
                m.setSupplier           ( nvl( ((String)h.get("placa") ) ) );
                m.setApplicated_ind     ("S");
                m.setApplication_ind    ("S");
                m.setInd_vlr            ("V");
                m.setVlr_for            (Float.parseFloat((String)h.get("valor") ) );
                ////System.out.println("tasa: "+t.getValor_tasa()+", valor= "+m.getVlr_for());
                m.setValor              (t.getValor_tasa() * m.getVlr_for());
                m.setCurrency           ( nvl( (curr) ) );
                m.setProveedor_anticipo( nvl( ("890103161") ) );//Nit TSP
                m.setCreation_user      ( nvl( (u.getLogin() ) ) );
                
                m = ftabla.buscarDatosAdicionales( (String)h.get("numpla"), m );
                
                String mens2 = this.validarMovpla(m);
                
                if( mens2.equals("") ){
                    mens += ftabla.insertMovPla(m, oid);
                    //Se graba la discrepancia
                    if( formato.equals("DIS") ){
                        ftabla.insertarDiscrepancia(m, h);
                    }
                    //Se graba la sancion
                    if( formato.equals("SAN") ){
                        ftabla.insertarSancion(h, m);
                    }
                }else{
                    mens += "\nNo se pudo insertar el registro para la planilla "+m.getPlanilla()+", "+mens2;
                }
                
            }else{
                ////System.out.println("NO HAY TASA");
                mens += "\nNo se insert� el registro "+m.getConcept_code()+
                ", planilla "+m.getPlanilla()+
                ", pla_owner "+m.getPla_owner()+
                ", creation_date "+Util.getFechaActual_String(4)+"...No hay tasa de conversion de "+moneda+" a "+curr;
            }
        }
        return mens;
    }
    
    
    
    public String insert( Vector datos, String formato, Usuario u, String moneda ) throws Exception{
        
        String tabla_destino = ftabla.formatoGrabaEn( formato );
        String mens = "";
        
        ////System.out.println( tabla_destino );
        
        if( !tabla_destino.equals("error") ){
            
            if( tabla_destino.equals("movpla") ){
                mens = insertIntoMovpla(datos, u, moneda, formato);
            }else if( tabla_destino.equals("cxp") ){
                mens = insertIntoCxpRxp(datos, u, moneda, "cxp", formato);
            }else if( tabla_destino.equals("recurrente") ){
                mens = insertIntoCxpRxp(datos, u, moneda, "rxp", formato);
            }
            
        }
        return mens;
    }
    
    public String nvl( String value ){
        return value!=null? value : "";
    }
    
    /**
     * Getter for property datos.
     * @return Value of property datos.
     */
    public java.util.Vector getDatos() {
        return datos;
    }
    
    /**
     * Setter for property datos.
     * @param datos New value of property datos.
     */
    public void setDatos(java.util.Vector datos) {
        this.datos = datos;
    }
    
    /****************************************************************
     * Metodo formatoGrabaEn, obtiene la tabla en la que el formato
     *      dado, debe migrar registros(facturas)
     * @param: String formato, formato de tabla del que se desea migrar
     * @throws: SQLException, en caso de error de BD
     ****************************************************************/
    public String formatoGrabaEn( String formato ) throws SQLException{
        return ftabla.formatoGrabaEn(formato);
    }
    
    
    public String validarMovpla( Movpla m ){
        
        String mens = "";
        boolean prov = true;
        
        if( m.getCausa()!=null && m.getCausa().equals("OK") ){
            
            if( !( m.getPla_owner() != null && !m.getPla_owner().equals("") ) ){
                mens += "No se encuentra informacion del proveedor, ";
                prov = false;
            }
            if( !( m.getBranch_code() != null && !m.getBranch_code().equals("") ) && prov){
                mens += "el proveedor no presenta banco, ";
            }
            if( !( m.getBank_account_no() != null && !m.getBank_account_no().equals("") ) && prov){
                mens += "el proveedor no presenta sucursal, ";
            }
            
        }else{
            mens +=" La planilla o la placa correspondiente no existe ";
        }
        
        return mens;
    }
    
    public String getDescripcion(String formato, String placa, String planilla){
        
        String desc = "";
        if( formato.equals("COM") ) desc = "DESCUENTO COMBUSTIBLE "+ placa;
        
        else if( formato.equals("LLA") ) desc = "DESCUENTO LLANTAS "+ placa;
        
        else if( formato.equals("COE") ) desc = "ACOMPA�AMIENTO PLANILLA "+ planilla;
        return desc;
    }
    
     /**
     * Metodo: estaTabla, permite buscar si existe el nombre de la tabla en la BD
     * @autor : Ing. Jose de la rosa
     * @param : nombre de la tabla.
     * @version : 1.0
     */
    public boolean estaTabla (String nombre_tabla) throws SQLException{
        return ftabla.estaTabla( nombre_tabla );
    }
    
        /**
     * Metodo: estaCampo, permite buscar si existe el nombre del campo en la tabla
     * @autor : Ing. Jose de la rosa
     * @param : nombre de la tabla, nombre campo.
     * @version : 1.0
     */
    public boolean estaCampo (String nombre_tabla, String campo_val) throws SQLException{
        return ftabla.estaCampo( nombre_tabla, campo_val );
    }
    
     public String validarCXP( CXP_Doc c ){
        
        String mens = "";
        boolean p = true;
        if( c.getProveedor().equals("") ){
            mens += " La placa no presenta propietario,";
            p     = false;
        }
        if( ( c.getBanco().equals("") || c.getSucursal().equals("") ) && p==true ){
            mens += " El propietario no presenta Banco o sucursal,";
        }
        if( c.getAgencia().equals("") && p==true ){
            mens += " El banco no presenta agencia, ";
        }
        
        return mens;
        
    }
    public String insertIntoCxpRxp( Vector datos, Usuario u, String moneda, String tabla, String formato ) throws Exception{
        
        String mens = "";
        String curr = "";
        float valor = 0;
        String descripcion = "";
        
        for( int i=0; i<datos.size(); i++ ){
            
            CXP_Doc bean = null;
            
            Hashtable h = (Hashtable)datos.get(i);
            
            String oid = (String) h.get("oid");
            curr = (String)h.get( "moneda" );//Esta moneda se convierte a la del banco
            
            String placa = (String)h.get( "placa" );
            valor = Float.parseFloat((String)h.get("valor"));
            String planilla = nvl( (String)h.get( "numpla" ) );
            
            String emp_servicio = null;
            
            //Si el formato es COEXPORT, se obtiene el nit de la empresa de servicio
            if( formato.equals("COE" ) ){ emp_servicio = (String)h.get( "emp_servicio" ); }
            
            bean = ftabla.buscarDatosAdicionales( placa, bean, emp_servicio );
            ////System.out.println("placa "+placa);
            if( bean!=null ){
                ////System.out.println("bean no es nulo");
                String validar = validarCXP(bean);
                if( validar.equals("") ){
                    ////System.out.println("valid� OK");
                    Tasa t1 = hayTasa( moneda, curr, bean.getMoneda_banco() );//para convertir de la moneda de factura_migracion a la del banco
                    Tasa t2 = hayTasa( moneda, bean.getMoneda_banco(), moneda );//para convertir de la moneda del banco a la del distrito
                    ////System.out.println("curr= "+curr+", moneda_banco= "+ bean.getMoneda_banco());
                    ////System.out.println("tasa: "+t1.getValor_tasa()+", valor= "+valor);
                    
                    ////System.out.println("\nmoneda_banco= "+bean.getMoneda_banco()+", moneda= "+ moneda);
                    ////System.out.println("tasa: "+t2.getValor_tasa()+", valor= "+valor);
                    
                    if( t1!=null ){
                        if( t2!=null ){
                            bean.setDstrct( u.getDstrct() );
                            bean.setTipo_documento("010");
                            bean.setMoneda( bean.getMoneda_banco() );
                            
                            String[] doc = ftabla.obtenerSerie();
                            
                            if( doc!=null ){
                                
                                int signo = -1;
                                
                                if( formato.equals("COE") ){ signo = 1; }
                                descripcion = getDescripcion(formato, placa, planilla);
                                bean.setDocumento( doc[0]+doc[1] );
                                bean.setDescripcion( descripcion );
                                
                                if( !bean.getMoneda().trim().equals("PES") ){
                                    bean.setVlr_neto_me( Util.roundByDecimal( valor * t1.getValor_tasa(), 2 ) * signo );
                                    bean.setVlr_saldo_me( Util.roundByDecimal( valor * t1.getValor_tasa(), 2 ) * signo );
                                }else{
                                    bean.setVlr_neto_me( Math.round( valor * t1.getValor_tasa() ) * signo );
                                    bean.setVlr_saldo_me( Math.round( valor * t1.getValor_tasa() ) * signo );
                                }
                                
                                bean.setVlr_neto( Math.round( bean.getVlr_neto_me() * t2.getValor_tasa() ) );
                                bean.setVlr_saldo( Math.round( bean.getVlr_neto_me() * t2.getValor_tasa() ) );
                                
                                bean.setTasa( t2.getValor_tasa() );
                                bean.setCreation_user( u.getLogin() );
                                
                                OpService op = new OpService();
                                
                                bean.setAprobador( op.getABC( bean.getAgencia() ) );//Aqui va el codigo_abc, para el item
                                
                                String codigo_cuenta = ftabla.getCodigoCuenta( (String)h.get("dstrct"), bean.getProveedor(),
                                                       (String)h.get("concepto"), (String)h.get( "numpla" ) );
                                
                                if( codigo_cuenta!=null && !codigo_cuenta.equals("") ){
                                    
                                    bean.setCorrida( codigo_cuenta ); //Aqui va el codigo de cuenta para el item
                                    bean.setPeriodo( planilla );//Aqui va la planilla para el item
                                    if( tabla.equals("rxp") ){
                                        bean.setNum_cuotas( Integer.parseInt( (String)h.get("nro_cuota") ) );
                                        bean.setFrecuencia("S");
                                    }
                                    
                                    mens += ftabla.insertCxpRxp(bean, tabla, oid);
                                }else{
                                    mens += "\nNo se insert� el registro, no se encontro en codigo de cuenta para proveedor "+
                                    bean.getProveedor()+", concepto "+(String)h.get("concepto")+" ,planilla "+planilla;
                                }
                            }else{
                                mens += "\nNo se puede continuar la migracion, no se encontr� la serie FACMIGR";
                                break;
                            }
                        }else{
                            mens += "\nNo se insert� el registro distrito"+bean.getDstrct()+
                            ", proveedor "+bean.getProveedor()+
                            ", tipo_documento "+bean.getTipo_documento()+"...No hay tasa de conversion de "+bean.getMoneda()+" a "+moneda;
                        }
                    }else{
                        mens += "\nNo se insert� el registro distrito"+bean.getDstrct()+
                        ", proveedor "+bean.getProveedor()+
                        ", tipo_documento "+bean.getTipo_documento()+"...No hay tasa de conversion de "+curr+" a "+bean.getMoneda();
                    }
                }else{
                    mens += validar!=null? "\nNo se insert� el registro para la placa "+placa+" "+validar : "";
                }
            }else{
                if( formato.equals("COE" ) ) mens += "\nNo se insert� el registro para la empresa de servicio "+emp_servicio+", no presenta informacion ";                
                else mens += "\nNo se insert� el registro para la placa "+placa+", no presenta informacion ";
            }
        }
        return mens;
    }
}
