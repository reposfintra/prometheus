/*
    Document   : Modulo de Compras y Servicios.
    Created on : 10/02/2017, 11:00:00 AM
    Author     : hcuello
*/

package com.tsp.operation.model.services;

import java.util.ArrayList;
import com.tsp.operation.model.DAOS.ComprasProcesoDAO;
import com.tsp.operation.model.beans.ComprasProcesoBeans;
import com.tsp.operation.model.beans.Usuario;
import java.util.List;
import java.util.ResourceBundle;

public class ComprasProcesoService {
    ComprasProcesoDAO lpr; // = new RequisicionesDAO();
    
    public ComprasProcesoService(String dataBaseName) {
        this.lpr = new ComprasProcesoDAO(dataBaseName);
    }

    public ArrayList<ComprasProcesoBeans> ComprasProcesoBeans(String Query, String NmLogin, String id_sol, String accione, String codSol)throws Exception{
       return lpr.GetComprasProcesoListado(Query, NmLogin, id_sol, accione, codSol);
    }
    
    public ArrayList<ComprasProcesoBeans> GetInsumosOCS(String Query, String NmLogin)throws Exception{
       return lpr.GetInsumosOCS(Query, NmLogin);
    }    

    public ArrayList<ComprasProcesoBeans> GetInsumosxApu(String Query, String NmLogin, String id_sol, String accione)throws Exception{
       return lpr.GetInsumosxApu(Query, NmLogin, id_sol, accione);
    }
    
    public ArrayList<ComprasProcesoBeans> GetInsumosCatalogo(String Query)throws Exception{
       return lpr.GetInsumosCatalogo(Query);
    }

    public ArrayList<ComprasProcesoBeans> GetInsumosOCSEdit(String Query, String NmLogin, String OdnCmpra)throws Exception{
       return lpr.GetInsumosOCSEdit(Query, NmLogin, OdnCmpra);
    }    
    
    public ArrayList<ComprasProcesoBeans> GetInsumosDespacho(String Query, String NmLogin, String OdnCmpra)throws Exception{
       return lpr.GetInsumosDespacho(Query, NmLogin, OdnCmpra);
    }    
    
    public ArrayList<ComprasProcesoBeans> GetOrdenCompra(String Query, String OdnCmpra)throws Exception{
       return lpr.GetOrdenCompra(Query, OdnCmpra);
    }    

}
