/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.CiclosFacturacionDAO;
import com.tsp.operation.model.beans.BeanGeneral;
import java.util.ArrayList;

/**
 *
 * @author Ing. Rhonalf Martinez (rhonalf) <rhonaldomaster@gmail.com>
 * @version 0.1
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class CiclosFacturacionService {

    private CiclosFacturacionDAO cdao;

    public CiclosFacturacionService(){
        cdao = new CiclosFacturacionDAO();
    }

    /**
     * Busca los ciclos de facturaci&oacute;n para un a&ntilde;o
     * @param anio A&ntilde;o a buscar
     * @return Listado con los datos
     * @throws Exception Cuando hay error
     */
    public ArrayList<BeanGeneral> buscarCiclosAnio(String anio) throws Exception{
        ArrayList<BeanGeneral> lista = null;
        try {
            lista = cdao.buscarCiclosAnio(anio);
        }
        catch (Exception e) {
            System.out.println("Error: "+e.toString());
            throw e;
        }
        return lista;
    }

    /**
     * Busca si existen datos de ciclos para el a&ntilde;o
     * @param anio A&ntilde;o a buscar
     * @return Si existen o no datos
     * @throws Exception Cuando hay error
     */
    public boolean existeDatosAnio(String anio) throws Exception{
        boolean existen = false;
        try {
            existen = cdao.existeDatosAnio(anio);
        }
        catch (Exception e) {
            System.out.println("Error: "+e.toString());
            throw e;
        }
        return existen;
    }

    /**
     * Genera sql para insertar datos en la tabla
     * @param anio A&ntilde;o de los ciclos
     * @param bean Objeto con los datos
     * @return Cadena con el codigo generado
     * @throws Exception Cuando hay error
     */
    public String insertar(String anio, BeanGeneral bean) throws Exception{
        String sql = "";
        try {
            sql = cdao.insertar(anio, bean);
        }
        catch (Exception e) {
            System.out.println("Error: "+e.toString());
            throw e;
        }
        return sql;
    }

    /**
     * Genera el codigo para eliminar los datos de ciclos de un a&ntilde;o
     * @param anio A&ntilde;o a eliminar
     * @return Cadena con el codigo generado
     * @throws Exception Cuando hay error
     */
    public String eliminar(String anio) throws Exception{
        String sql = "";
        try {
            sql = cdao.eliminar(anio);
        }
        catch (Exception e) {
            System.out.println("Error: "+e.toString());
            throw e;
        }
        return sql;
    }

    /**
     * Ejecuta el codigo que se le indique
     * @param sql Codigo a ejecutar
     * @return Numero de filas afectadas
     * @throws Exception Cuando hay error
     */
    public int ejecutarSQL(String sql) throws Exception{
        int rows = 0;
        try {
            rows = cdao.ejecutarSQL(sql);
        }
        catch (Exception e) {
            System.out.println("Error: "+e.toString());
            throw e;
        }
        return rows;
    }
}