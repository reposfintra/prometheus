/***********************************************************************************
 * Nombre clase : ............... RetroactivoService.java                          *
 * Descripcion :................. Clase que maneja los Servicios                   *
 *                                asignados a Model relacionados con el            *
 *                                programa de RetroactivoService                   *
 * Autor :....................... Ing. Diogenes Antonio Bastidas Morales           *
 * Fecha :........................ 6 de febrero de 2006, 08:33 AM                  *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/

package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;

public class RetroactivoService {
    
    private RetroactivoDAO retactivo;
    /** Creates a new instance of RetroactivoService */
    public RetroactivoService() {
        retactivo = new RetroactivoDAO(); 
    }
    
    /**
     * Metodo setRetroactivo, setea el objeto de tipo retroactivo
     * @param: objeo retroactivo
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public Retroactivo getRetroactivo(){
        return retactivo.getRetroactivo();
    }
    
    /**
     * Metodo insertarRetroactivo, ingresa un registro en la tabla retroactividad
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void insertarRetroactivo(Retroactivo recact) throws SQLException {
        retactivo.setRetroactivo(recact);
        retactivo.insertarRetroactivo();
    }
    
    /**
     * Metodo modificarRetroactivo, modifica un registro en la tabla retroactivo
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void modificarRetroactivo(Retroactivo recact) throws SQLException {
        retactivo.setRetroactivo(recact);
        retactivo.modificarRetroactivo();        
    }
    
    /**
     * Metodo anularRetroactivo, modifica un registro en la tabla retroactivo
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void anularRetroactivo(Retroactivo recact) throws SQLException {
        retactivo.setRetroactivo(recact);
        retactivo.anularRetroactivo();
    }
    
    /**
     * Metodo existeRetroactivo, verifica si existe un registro
     * @param: standar, ruta, fecha1, fecha2
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean existeRetroactivo(String stdjob, String ruta, String fecha1, String fecha2 ) throws SQLException {
        return retactivo.existeRetroactivo(stdjob, ruta, fecha1, fecha2);
    }
    
    /**
     * Metodo listarRetroactivo, buscar el registro en retroactivo
     * @param: standar, ruta, fecha1, fecha2
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void listarRetroactivo(String stdjob, String ruta ) throws SQLException {
        retactivo.listarRetroactivo(stdjob, ruta);
    }
    
    /**
     * Metodo buscarRetroactivo, buscar el registro en retroactivo
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarRetroactivo(String stdjob, String ruta, String fecha1, String fecha2 ) throws SQLException {
        retactivo.buscarRetroactivo(stdjob, ruta, fecha1, fecha2);
    }
    
    /**
     * Metodo existeRetroactivoAnulado, verifica si existe un registro anulado
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean existeRetroactivoAnulado(String stdjob, String ruta, String fecha1, String fecha2 ) throws SQLException {
        return retactivo.existeRetroactivoAnulado(stdjob, ruta, fecha1, fecha2);
    }
    
     /**
     * Metodo getRetroactivos, obtien la lista de retroactivos
     * @param: objeo retroactivo
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public Vector getRetroactivos(){
        return retactivo.getRetroactivos();
    }
    
}
