/***************************************
    * Nombre Clase ............. ResumenPrestamoService.java
    * Descripci�n  .. . . . . .  Brinda servicios para el resumen del prestamo
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  04/03/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/



package com.tsp.operation.model.services;



import java.io.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;



public class ResumenPrestamoService {
    
   
    
    private ResumenPrestamoDAO  ResumenPtmDAO;
    private List                listaPrestamos;
    
    
    public ResumenPrestamoService() {
        ResumenPtmDAO  = new ResumenPrestamoDAO();
        listaPrestamos = new LinkedList();
    }
    
    
    
    
    
    /**
     * M�todo que busca el resumen de los prestamos perteneciente a un tercero
     * @parameter   String  idTercero
     * @Exception   Exception
     * @version     1.0.     
     **/ 
    public void searchPrestamos(String distrito,String  idTercero, String clasificacion)throws Exception{
        try{
            this.listaPrestamos  = this.ResumenPtmDAO.getAcumuladoPrestamos(distrito, idTercero, clasificacion);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    
    
    
    
    
    
    
    
    // SET Y GET :
    
    
    /**
     * Getter for property listaPrestamos.
     * @return Value of property listaPrestamos.
     */
    public java.util.List getListaPrestamos() {
        return listaPrestamos;
    }    
    
    /**
     * Setter for property listaPrestamos.
     * @param listaPrestamos New value of property listaPrestamos.
     */
    public void setListaPrestamos(java.util.List listaPrestamos) {
        this.listaPrestamos = listaPrestamos;
    }    
    
    
    
    
    
    
    
}
