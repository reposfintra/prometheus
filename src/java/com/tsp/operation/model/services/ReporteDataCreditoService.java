/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.ReporteDataCreditoDAO;
import java.util.ArrayList;

/** 
 *
 * @author maltamiranda
 */
public class ReporteDataCreditoService {

    public String getClientes(String cl,String nomselect,String def)throws Exception    {
        ReporteDataCreditoDAO rdcd=new ReporteDataCreditoDAO();
        return  rdcd.getClientes(cl, nomselect,def);
    }

    public String getAfiliados(String cl,String nomselect,String def)throws Exception    {
        ReporteDataCreditoDAO rdcd=new ReporteDataCreditoDAO();
        return  rdcd.getAfiliados(cl, nomselect,def);
    }

    public ArrayList buscar_negocios(String idclie, String negocio, String proveedor, String rangoi, String rangof,int columna,String fechaReporte)throws Exception {
        ReporteDataCreditoDAO rdcd=new ReporteDataCreditoDAO();
        return rdcd.buscar_negocios(idclie, negocio, proveedor, rangoi, rangof, columna, fechaReporte);
    }

    public ArrayList agregar_negocios_acumulado(String idclie, String negocio, String proveedor, String rangoi, String rangof, int columna, String[] obligaciones, String usuario, String fechaReporte)throws Exception {
        ReporteDataCreditoDAO rdcd=new ReporteDataCreditoDAO();
        rdcd.agregar_negocios_acumulado(obligaciones, usuario);
        return rdcd.buscar_negocios(idclie, negocio, proveedor, rangoi, rangof, columna, fechaReporte);
    }

    public ArrayList buscar_acumulados(int columna)throws Exception {
        ReporteDataCreditoDAO rdcd=new ReporteDataCreditoDAO();
        return rdcd.buscar_acumulados(columna);
    }

    public ArrayList eliminar_negocios_acumulado(String idclie, String negocio, String rangoi, String rangof, int columna, String[] obligaciones)throws Exception{
        ReporteDataCreditoDAO rdcd=new ReporteDataCreditoDAO();
        rdcd.eliminar_negocios_acumulado(obligaciones);
        return rdcd.buscar_acumulados(columna);
    }

    public void limpiar_negocios_acumulado()throws Exception {
        ReporteDataCreditoDAO rdcd=new ReporteDataCreditoDAO();
        rdcd.limpiar_negocios_acumulado();
    }

    public String generarReporte()throws Exception {
        ReporteDataCreditoDAO rdcd=new ReporteDataCreditoDAO();
        //"Archivo generado, porfavor verifique en el directorio de archivos";
        return rdcd.generarReporte();
    }

}
