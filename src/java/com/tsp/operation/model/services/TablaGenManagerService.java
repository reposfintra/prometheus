
/*
 * Nombre        TablaGenManagerService.java
 * Descripci�n   Clase DAO para tabla general
 * Autor         Alejandro Payares
 * Fecha         14 de diciembre de 2005, 05:51 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */
package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.TablaGenManagerDAO;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.TablaGen;

/**
 *
 * @author  Alejandro Payares
 */
public class TablaGenManagerService {

    private LinkedList rin ;
    private LinkedList piso ;
    private LinkedList modelo ;
    private LinkedList titulo ;
    private LinkedList clase ;
    private LinkedList tipo ;
    private LinkedList clasificacion;
    //Diogenes 05-02-2006
    private LinkedList tblclasificacion;
    private LinkedList tblhandlecode;
    private LinkedList tblBanco;

    //Jose
    private LinkedList con_proveedor;
    private LinkedList grupo;
    private LinkedList resact;
    private LinkedList causa;
    private LinkedList general;
    //16-06-2006
    private LinkedList tipoDocumentos;
    private LinkedList usuarios;

    private LinkedList tipoIngresos;

    //Osvaldo
    private LinkedList datos2;


    /**
     * El objeto que permite el acceso a los datos
     */
    private TablaGenManagerDAO dao;

    //Alejandro Payares 05-05-2006
    //variable para guardar datos temporales
    private Hashtable datos;

    //Luis Eduardo frieri 19-01-2007
    // guarda la tabla THCODE de tablagen
    private LinkedList listaHc;
    private TreeMap hcs;
    private TreeMap fpago;
    private LinkedList grupoid;

    /** Creates a new instance of TablaGenManagerService */
    public TablaGenManagerService() {
        dao = new TablaGenManagerDAO();
    }
    
    public TablaGenManagerService(String dataBaseName) {
        dao = new TablaGenManagerDAO(dataBaseName);
    }

    /**
     * Busca las tablas registradas en tabla gen y las carga en la lista de tablas
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void buscarTablas() throws SQLException{
        dao.buscarTablas();
    }
     /**
     * Busca las tablas registradas en tabla gen segun el perfil y las carga en la lista de tablas
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void buscarTablasPerfiles(String perfil) throws SQLException{
        dao.buscarTablasPerfiles(perfil);
    }
    /**
     * M�todo buscarClasificacion  realiza una busqueda en la tabla general dato y relaciona la informacion
     * con la tabla TPERSONA
     * @autor.......Diogenes Bastidas Morales
     * @see.........buscarRegistros     -    TblGeneralDAO
     * @version.....2.0.
     **/
    public void buscarClasificacionPro()throws SQLException {
        dao.buscarRegistros("TPERSONA");
        this.tblclasificacion = dao.obtenerTablas();
    }
    /**
     * M�todo buscarHandle_code  realiza una busqueda en la tabla general dato y relaciona la informacion
     * con la tabla THCODE
     * @autor.......Diogenes Bastidas Morales
     * @see.........buscarRegistros     -    TblGeneralDAO
     * @version.....2.0.
     **/
    public void buscarHandle_code()throws SQLException {
        dao.buscarRegistros("THCODE");
        this.tblhandlecode = dao.obtenerTablas();
    }
    /**
     * Getter for property tblclasificacion.
     * @return Value of property tblclasificacion.
     */
    public java.util.LinkedList getTblclasificacion() {
        return tblclasificacion;
    }

    /**
     * Getter for property tblhandlecode.
     * @return Value of property tblhandlecode.
     */
    public java.util.LinkedList getTblhandlecode() {
        return tblhandlecode;
    }

    /**
     * Devuelve la lista de las tablas cargadas por el m�todo obtenerTablas()
     * @return La lista de tablas
     */
    public LinkedList obtenerTablas(){
        return dao.obtenerTablas();
    }

    /**
     * Devuelve la lista de las tablas cargadas por el m�todo obtenerTablas()
     * @return La lista de tablas
     */
    public LinkedList obtenerTablasPerfiles(){
        return dao.obtenerTablasPerfiles();
    }

    /**
     * busca los registros de tablagen cuyo table_type sea igual al dado
     * @param table_type el tipo de la tabla o el nombre de la sub-tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void buscarRegistros(String table_type) throws SQLException {
        dao.buscarRegistros(table_type);
    }

    /**
     * Agrega una tabla nueva en tablagen
     * @param tipo el tipo o nombre de la tabla
     * @param descripcion una corta descripci�n del uso de la tabla
     * @param user el usuario que crea la tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @throws InformationException si la tabla a agregar ya existe
     */
    public void agregarTabla(String tipo, String descripcion, String user, String obs)throws SQLException, com.tsp.exceptions.InformationException {
        dao.agregarTabla(tipo, descripcion, user, obs);
    }

    /**
     * Agregae un nuevo registro a una subtabla de tablagen
     * @param tipo el tipo o nombre de la tabla
     * @param codigo el codigo del registro
     * @param referencia la referencia del registro
     * @param descripcion la descripci�n del registro
     * @param user el usuario que crea la tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @throws InformationException si el registro a agregar corresponde a uno ya existente
     */
    public void agregarRegistro(String tipo, String codigo, String referencia, String descripcion, String user, String dato)throws SQLException, com.tsp.exceptions.InformationException {
        dao.agregarRegistro(tipo, codigo, referencia, descripcion, user, dato);
    }

    /**
     * perimte obtener un registro completo que coincida con el oid dado
     * @param oid el identificador �nico del registro
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @return un bean tipo <CODE>TablaGen</CODE> correspondiente al registro
     */
    public TablaGen obtenerRegistro(String oid)throws SQLException{
        return dao.obtenerRegistro(oid);
    }

    /**
     * Permite modificar un registro de una subtabla de tablagen.
     * @param oid el identificador �nico del registro
     * @param tipo el tipo o nombre de la tabla
     * @param codigo el codigo del registro
     * @param referencia la referencia del registro
     * @param descripcion la descripci�n del registro
     * @param user el usuario que edita el registro
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @throws InformationException Si el nuevo nombre del registro, la referencia y la descripci�n
     * corresponden a un registro ya existente.
     */
    public void editarRegistro(String oid,String tipo, String codigo, String referencia, String descripcion, String user, String dato)throws SQLException, com.tsp.exceptions.InformationException {
        dao.editarRegistro(oid, tipo, codigo, referencia, descripcion, user, dato);
    }

    /**
     * Permite eliminar uno o m�s registros
     * @param oids Los identificadores �nicos de los registros a eliminar.
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void eliminarRegistros(String [] oids)throws SQLException {
        dao.eliminarRegistros(oids);
    }

    /**
     * Permite eliminar una o m�s tablas
     * @param tablas los nombre o tipos de las tablas
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void eliminarTablas(String [] tablas)throws SQLException {
        dao.eliminarTablas(tablas);
    }

    /**
     * Permite modificar una tabla dentro de tablgen, es decir su nombre y su descripci�n.
     * Cuando se modifica el nombre, tambi�n son modificados los registros que pertencen a
     * esa tabla.
     * @param tipo el tipo o nombre de la tabla
     * @param descripcion la descripci�n de la tabla
     * @param tipoViejo el nombre antiguo de la tabla
     * @param user el usuario que modifica la tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @throws InformationException Si el nuevo nombre de la tabla corresponde a una ya existente.
     */
    public void editarTabla(String tipo, String descripcion, String tipoViejo, String user, String obs)throws SQLException, com.tsp.exceptions.InformationException{
        dao.editarTabla(tipo, descripcion, tipoViejo, user, obs);
    }

    /**
     * Obtiene el registro correspondiente a una tabla sin sus registros, es decir
     * el nombre de la tabla y su descripci�n
     * @param type el nombre de la tabla, o el tipo
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @return Un bean <CODE>TablaGen</CODE> con los datos de la tabla
     */
    public TablaGen obtenerTabla(String type)throws SQLException{
        return dao.obtenerTabla(type);
    }

    /**
     * M�todo que realiza una busqueda en la tabla general dato y relaciona la informacion
     * con la tabla general mediante un codigo de tabla
     * @autor.......Henry A. Osorio Gonzalez
     * @see.........buscarDatosTablasGenerales     -    TblGeneralDAO
     * @version.....1.0.
     **/
    public void buscarDatosTablasGenerales(String codTabla,String codigo, String programa)throws SQLException {
        dao.buscarDatosTablasGenerales(codTabla, codigo, programa);
    }

    /**
     * M�todo que retorna un Vector de TreeMaps
     * @autor.......Henry A. Osorio Gonzalez
     * @return.......un vector de treemaps, los cuales contienen los datos de la tabla geenral
     * @see.........getVectorTablasGenerales     -    TblGeneralDAO
     * @version.....1.0.
     **/
    public Vector getVectorTablasGenerales() {
        return dao.getVectorTablasGenerales();
    }

    /**
     * M�todo que realiza una busqueda en la tabla general dato y relaciona la informacion
     * con la tabla general mediante un codigo de tabla
     * @autor.......Juan Manuel Escandon Perez
     * @see.........DatosTablasGenerales     -    TblGeneralDAO
     * @version.....2.0.
     **/
    public void DatosTablasGenerales(String codTabla,String codigo, String programa)throws SQLException {
        dao.DatosTablasGenerales(codTabla, codigo, programa);
    }
    /**
     * M�todo que realiza una busqueda en la tabla general dato y relaciona la informacion
     * con la tabla general mediante un codigo de tabla
     * @autor.......Jose de la rosa
     * @see.........obtenerInfoTablaGen     -    TblGeneralDAO
     * @version.....1.0.
     **/
    public LinkedList obtenerInfoTablaGen(String type, String programa)throws SQLException{
        return dao.obtenerInfoTablaGen(type, programa);
    }


    /**
     * M�todo que obtiene los valores de la tabla general con el nombre de tabla TBLRIN
     * @autor.......Jose de la rosa
     * @see.........buscarRines     -    TblGeneralDAO
     * @version.....2.0.
     **/
    public void buscarRines()throws SQLException {
        dao.buscarRegistros("TBLRIN");
        this.rin = dao.obtenerTablas();
    }

    /**
     * M�todo que obtiene los valores de la tabla general con el nombre de tabla TBLPISO
     * @autor.......Jose de la rosa
     * @see.........buscarPiso     -    TblGeneralDAO
     * @version.....2.0.
     **/
    public void buscarPiso()throws SQLException {
        dao.buscarRegistros("TBLPISO");
        this.piso = dao.obtenerTablas();
    }

    /**
     * M�todo que obtiene los valores de la tabla general con el nombre de tabla TBLMODELO
     * @autor.......Jose de la rosa
     * @see.........buscarModelo     -    TblGeneralDAO
     * @version.....2.0.
     **/
    public void buscarModelo()throws SQLException {
        dao.buscarRegistros("TBLMODELO");
        this.modelo = dao.obtenerTablas();
    }

    /**
     * M�todo que obtiene los valores de la tabla general con el nombre de tabla TBLTITU
     * @autor.......Jose de la rosa
     * @see.........buscarModelo     -    TblGeneralDAO
     * @version.....2.0.
     **/
    public void buscarTitulo()throws SQLException {
        dao.buscarRegistros("TBLTITU");
        this.titulo = dao.obtenerTablas();
    }

    /**
     * M�todo que obtiene los valores de la tabla general con el nombre de tabla PLACA y con los  valores
     * de la clase y el tipo obtenido: T - trailer, P - cabezotes y O - otros.
     * @autor.......Jose de la rosa
     * @see.........buscarModelo     -    TblGeneralDAO
     * @version.....2.0.
     **/
    public void buscarClase(String tipo)throws SQLException {
        dao.buscarRegistrosConReferencia("EC","PLACA",tipo);
        this.clase = dao.obtenerTablas();
    }

    /**
     * M�todo que obtiene los valores de la tabla general con el nombre de tabla PLACA y con los  valores
     * de la tipo y el tipo obtenido: T - trailer, P - cabezotes y O - otros.
     * @autor.......Jose de la rosa
     * @see.........buscarModelo     -    TblGeneralDAO
     * @version.....2.0.
     **/
    public void buscarTipo(String tipo)throws SQLException {
        dao.buscarRegistrosConReferencia("ET","PLACA",tipo);
        this.tipo = dao.obtenerTablas();
    }

    /**
     * M�todo que obtiene los valores de la tabla general con el nombre de tabla clase equipo CLAEQUI
     * @autor.......Jose de la rosa
     * @see.........buscarClasificacion     -    TblGeneralDAO
     * @version.....2.0.
     **/
    public void buscarClasificacion()throws SQLException {
        dao.buscarRegistros("CLAEQUI");
        this.clasificacion = dao.obtenerTablas();
    }

    /**
     * Getter for property clasificacion.
     * @return Value of property clasificacion.
     */
    public java.util.LinkedList getClasificacion() {
        return clasificacion;
    }

    /**
     * Getter for property modelo.
     * @return Value of property modelo.
     */
    public java.util.LinkedList getModelo() {
        return modelo;
    }

    /**
     * Getter for property titulo.
     * @return Value of property titulo.
     */
    public java.util.LinkedList getTitulo() {
        return titulo;
    }

    /**
     * Getter for property rin.
     * @return Value of property rin.
     */
    public java.util.LinkedList getPiso() {
        return piso;
    }

    /**
     * Getter for property rin.
     * @return Value of property rin.
     */
    public java.util.LinkedList getRin() {
        return rin;
    }

    /**
     * Getter for property clase.
     * @return Value of property clase.
     */
    public java.util.LinkedList getClase() {
        return clase;
    }

    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.util.LinkedList getTipo() {
        return tipo;
    }
    //JOSE 2006-03-07
    /**
     * M�todo que obtiene los valores de la tabla general con el nombre de tabla TBLGRUPO
     * @autor.......Jose de la rosa
     * @see.........buscarGrupos     -    TblGeneralDAO
     * @version.....2.0.
     **/
    public void buscarGrupos()throws SQLException {
        dao.buscarRegistros("TBLGRUPO");
        this.grupo = dao.obtenerTablas();
    }

    /**
     * Getter for property grupo.
     * @return Value of property grupo.
     */
    public java.util.LinkedList getGrupo() {
        return grupo;
    }
    /**
     * M�todo que obtiene los valores de la tabla general con el nombre de tabla CBANCO
     * @autor.......Diogenes Bastidas
     * @see.........buscarClasificacion     -    TblGeneralDAO
     * @version.....2.0.
     **/
    public void buscarBanco()throws SQLException {
        dao.buscarRegistros("CBANCO");
        this.tblBanco = dao.obtenerTablas();
    }

    /**
     * Getter for property tblBanco.
     * @return Value of property tblBanco.
     */
    public LinkedList getTblBanco() {
        return tblBanco;
    }
    /**
     * M�todo que obtiene los valores de la tabla general con el nombre de tabla PLACA y con los  valores
     * de la tipo y el tipo obtenido: T - trailer, P - cabezotes y O - otros.
     * @autor.......Jose de la rosa
     * @see.........buscarGrupo     -    TblGeneralDAO
     * @version.....2.0.
     **/
    public void buscarGrupo(String cliente)throws SQLException {
        dao.obtenerInfoTablaGen("EMAIL_GRUP",cliente);
        this.grupo = dao.obtenerTablas();
    }

    /**
     * obtenerTreeMapLista, permite generar un objeto TreeMap dada una lista.
     * @autor Ing. Henry Osorio
     * @param: Lista LinkedList
     * @see: obtenerTreeMapLista - PlacaDAO
     * @version 1.0
     * @throws.
     */
    public TreeMap obtenerTreeMapLista(LinkedList tblmod){
        TreeMap modelos = new TreeMap();
        for(int i = 0; i<tblmod.size(); i++){
            TablaGen tblm = (TablaGen) tblmod.get(i);
            modelos.put(tblm.getDescripcion(),tblm.getTable_code());
        }
        return modelos;
    }
    /**
     * Verifica si una fecha esta registrada como d�a feriado.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param fecha Fecha a verificar
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @return <code>true</code> si es feriado, <code>false</code> si no lo es
     */
    public boolean isFeriado(String fecha)throws SQLException{
        return dao.isFeriado(fecha);
    }
    //jose2006-03-14
    /**
     * busca los registros de tablagen cuyo table_type sea igual al dado
     * @param table_type el tipo de la tabla o el nombre de la sub-tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void buscarDatos(String table_type, String table_code )throws SQLException {
        dao.buscarDatos(table_type, table_code);
    }

    /**
     * Getter for property tblgen.
     * @return Value of property tblgen.
     */
    public TablaGen getTblgen() {
        return dao.getTblgen();
    }
    /**
     * M�todo que obtiene los valores de la tabla general con el nombre de tabla responsable actividad
     * @autor.......Diogenes Bastidas Morales
     * @see.........buscarResponsableActividad     -    TblGeneralDAO
     * @version.....1.0.
     **/
    public void buscarResponsableActividad()throws SQLException {
        dao.buscarRegistros("RESACT");
        this.resact = dao.obtenerTablas();
    }

    /**
     * M�todo que obtiene los valores de la tabla general con el nombre de tabla causa demoras
     * @autor.......Diogenes Bastidas Morales
     * @see.........buscarResponsableActividad     -    TblGeneralDAO
     * @version.....1.0.
     **/
    public void buscarCausaDemora()throws SQLException {
        dao.buscarRegistros("CAUDEM");
        this.causa = dao.obtenerTablas();
    }

    /**
     * Getter for property resact.
     * @return Value of property resact.
     */
    public java.util.LinkedList getResact() {
        return resact;
    }

    /**
     * Setter for property resact.
     * @param resact New value of property resact.
     */
    public void setResact(java.util.LinkedList resact) {
        this.resact = resact;
    }

    /**
     * Getter for property causa.
     * @return Value of property causa.
     */
    public java.util.LinkedList getCausa() {
        return causa;
    }

    /**
     * Setter for property causa.
     * @param causa New value of property causa.
     */
    public void setCausa(java.util.LinkedList causa) {
        this.causa = causa;
    }
    /**
     * Permite obtener un dato almacendao en la tabla de datos
     * @return Value of property dato.
     * @autor Alejandro Payares
     */
    public java.lang.Object getDato(String llave) {
        return datos.get(llave);
    }

    /**
     * Permite guardar un dato asociado a un nombre
     * @param dato New value of property dato.
     * @autor Alejandro Payares
     */
    public void setDato(String llave,java.lang.Object dato) {
        if ( datos == null ) {
            datos = new Hashtable();
        }
        this.datos.put(llave,dato);
    }
    /**
     * Setter for property tblgen.
     */
    public void setTblgen(TablaGen tblgen) {
        this.dao.setTblgen(tblgen);
    }
    /**
     * M�todo que busca en la tabla general y busca la informacion relacionada con una
     * referencia
     * @param.......String codigo de la tabla y String referencia
     * @see.........buscarReferencia     -    TblGeneralDAO
     * @autor.......Ing. Juan Manuel Escandon Perez
     * @version.....2.0.
     **/
    public void buscarReferencias(String tabla , String referencia)throws SQLException {
        dao.buscarReferencia(tabla, referencia);
        this.general = dao.obtenerTablas();
    }


    /**
     * Getter for property general.
     * @return Value of property general.
     */
    public java.util.LinkedList getReferencias() {
        return general;
    }

    /**
     * M�todo que obtiene los tipo de documentos del despacho
     * @autor.......Diogenes Bastidas Morales
     * @see.........obtenerInfoTablaGen     -    TblGeneralDAO
     * @version.....1.0.
     **/
    public void buscarTipodocumentos()throws Exception{
        try{
            this.tipoDocumentos =dao.obtenerInfoTablaGen("TDOC", "DESPACHO");
        }catch(Exception e){
            e.getStackTrace();
        }
    }
    /**
     * Getter for property tipoDocumentos.
     * @return Value of property tipoDocumentos.
     */
    public java.util.LinkedList getTipoDocumentos() {
        return tipoDocumentos;
    }

    /**
     * Setter for property tipoDocumentos.
     * @param tipoDocumentos New value of property tipoDocumentos.
     */
    public void setTipoDocumentos(java.util.LinkedList tipoDocumentos) {
        this.tipoDocumentos = tipoDocumentos;
    }
    /**
     * M�todo que obtiene los usuarios de TUSUARIOS en TABLAGEN
     * @autor.......LREALES
     * @see.........obtenerInfoTablaGen - TblGeneralDAO
     * @version.....1.0.
     **/
    public void buscarUsuarios()throws Exception{

        try {

            this.usuarios = dao.obtenerInfoTablaGen( "TUSUARIOS", "REP_FAC" );

        } catch( Exception e ){

            e.getStackTrace();

        }

    }

    /**
     * Getter for property usuarios.
     * @return Value of property usuarios.
     */
    public java.util.LinkedList getUsuarios() {

        return usuarios;

    }

    /**
     * Setter for property usuarios.
     * @param tipoDocumentos New value of property usuarios.
     */
    public void setUsuarios( java.util.LinkedList usuarios ) {

        this.usuarios = usuarios;

    }
    //TMATURANA
    private TreeMap treemap = new TreeMap();


    /**
     * Setter for property treemap.
     * @param treemap New value of property treemap.
     */
    public void setTreemap(java.util.TreeMap treemap) {
        this.treemap = treemap;
    }

    /**
     * M�todo que obtiene los usuarios de TUSUARIOS en TABLAGEN
     * y los carga en un treemap
     * @autor Ing. Andr�s Maturana
     * @version 1.0
     **/
    public void loadTUsuariosFactura() throws Exception{

        this.buscarUsuarios();
        this.treemap = new TreeMap();

        for( int i=0; i<this.usuarios.size(); i++){

            TablaGen t = (TablaGen) this.usuarios.get(i);
            this.treemap.put(t.getDescripcion(), t.getTable_code());

        }
    }
    /**
     * Getter for property treemap.
     * @return Value of property treemap.
     */
    public java.util.TreeMap getTreemap() {
        return treemap;
    }
    public TablaGen obtenerInformacionDato(String table_type, String code)throws SQLException {
        return dao.obtenerInformacionDato(table_type, code);
    }
    /**
     * busca los registros de tablagen cuyo table_type sea igual al dado y
     * reg_status != 'A'
     *@author Osvaldo P�rez Ferrer
     * @param table_type el tipo de la tabla o el nombre de la sub-tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void buscarRegistrosNoAnulados(String table_type)throws SQLException {
        dao.buscarRegistrosNoAnulados(table_type);
    }

    /**
     * verifica si existe un registro con tabla_type = type y table_code
     * @author Osvaldo P�rez Ferrer
     * @param table_type el tipo de la tabla o el nombre de la sub-tabla
     * @param code codigo o table_code
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @return 0 si el registro no existe
     *         1 si existe
     *         -1 si existe anulado
     */
    public int existeRegistroCode(String type, String code)throws SQLException{
        return dao.existeRegistroCode(type, code);
    }

    /**
     * verifica si existe un registro con tabla_type = type y dato
     * @author Osvaldo P�rez Ferrer
     * @param table_type el tipo de la tabla o el nombre de la sub-tabla
     * @param code codigo o table_code
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @return 0 si el registro no existe
     *         1 si existe
     *         -1 si existe anulado
     */
    public int existeRegistroDato(String type, String dato)throws SQLException{
        return dao.existeRegistroDato(type, dato);
    }

    /**
     * obtiene el registro, con tabla_code dado, o con dato dado
     * @author Osvaldo P�rez Ferrer
     * @param table_type el tipo de la tabla o el nombre de la sub-tabla
     * @param code codigo o table_code
     * @param dato dato
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void obtenerRegistro(String type, String code, String dato)throws SQLException {
        dao.obtenerRegistro(type,code,dato);
    }

    /**
     * Permite modificar un registro de una subtabla de tablagen, incluido el reg_status.
     * @param oid el identificador �nico del registro
     * @param tipo el tipo o nombre de la tabla
     * @param codigo el codigo del registro
     * @param referencia la referencia del registro
     * @param descripcion la descripci�n del registro
     * @param user el usuario que edita el registro
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @throws InformationException Si el nuevo nombre del registro, la referencia y la descripci�n
     * corresponden a un registro ya existente.
     */
    public void actualizarRegistro(String oid,String tipo, String codigo, String referencia, String descripcion, String user, String dato)throws SQLException, com.tsp.exceptions.InformationException {
        dao.actualizarRegistro(oid, tipo, codigo, referencia, descripcion, user, dato);
    }

    /**
     * busca los registros de tablagen cuyo table_type sea igual al dado
     * @param table_type el tipo de la tabla o el nombre de la sub-tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void buscarRegistrosOrdenados(String table_type)throws SQLException {
        dao.buscarRegistrosOrdenados(table_type);
    }
    /**
     * @author : Osvaldo P�rez
     * busca los registros de tablagen que sean equipos propios, con la
     * descripcion de la clase de equipo, proveniente de la tablagen CLAEQUI
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void obtenerEquipos() throws SQLException{
        dao.obtenerEquipos();
    }
    /**
     * @author : Osvaldo P�rez
     * busca los registros de tablagen que sean equipos propios, con la
     * descripcion de la clase de equipo, proveniente de la tablagen CLAEQUI
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void obtenerEquipos(String clase) throws SQLException{
        dao.obtenerEquipos(clase);
    }
    
    /**
     * busca los registros de tablagen cuyo table_type sea igual al dado
     * @author : Osvaldo P�rez Ferrer
     * @param table_type el tipo de la tabla o el nombre de la sub-tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void buscarRegistroByDesc(String table_type, String table_code)throws SQLException {
        dao.buscarRegistroByDesc(table_type, table_code);
    }
    
    /**
     * busca los registros de tablagen cuyo table_type sea igual al dado
     * @author : Osvaldo P�rez Ferrer
     * @param table_type el tipo de la tabla o el nombre de la sub-tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void buscarRegistrosOrdenadosByDesc(String table_type)throws SQLException {
        dao.buscarRegistrosOrdenadosByDesc(table_type);
    }


     /**
     * busca los registros de tablagen cuyo table_type sea igual a "THCODE"
     * @author : Luis Eduardo Frieri.
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void buscarHc()throws SQLException {
       dao.buscarRegistrosOrdenadosByDesc("THCODE");
       this.setListaHc(dao.obtenerTablas());
    }




/**
     * Getter for property tipoIngresos.
     * @return Value of property tipoIngresos.
     */
    public java.util.LinkedList getTipoIngresos() {
        return tipoIngresos;
    }

    /**
     * Setter for property tipoIngresos.
     * @param tipoIngresos New value of property tipoIngresos.
     */
    public void setTipoIngresos(java.util.LinkedList tipoIngresos) {
        this.tipoIngresos = tipoIngresos;
    }

    public void buscarTipoIngreso()throws SQLException{
        dao.buscarRegistros("TIPOING");
        this.tipoIngresos = dao.obtenerTablas();
    }

    /**
     * Getter for property datos2.
     * @return Value of property datos2.
     */
    public java.util.LinkedList getDatos2() {
        return datos2;
    }

    /**
     * Setter for property datos2.
     * @param datos2 New value of property datos2.
     */
    public void setDatos2(java.util.LinkedList datos2) {
        this.datos2 = datos2;
    }

    /**
     * Getter for property listaHc.
     * @return Value of property listaHc.
     */
    public java.util.LinkedList getListaHc() {
        return listaHc;
    }

    /**
     * Setter for property listaHc.
     * @param listaHc New value of property listaHc.
     */
    public void setListaHc(java.util.LinkedList listaHc) {
        this.listaHc = listaHc;
    }

    /**
     * Getter for property hcs.
     * @return Value of property hcs.
     */
    public java.util.TreeMap getHcs() {
        this.hcs = dao.getHcs();
        return hcs;
    }

    /**
     * Setter for property hcs.
     * @param hcs New value of property hcs.
     */
    public void setHcs(java.util.TreeMap hcs) {
        this.hcs = hcs;
    }

    public void listarHc(){
        try{
        dao.listarHc();
        } catch( Exception e ){

            e.getStackTrace();

        }
    }

    /**
     * Getter for property fpago.
     * @return Value of property fpago.
     */
    public java.util.TreeMap getFpago() {
        this.fpago = dao.getFpago();
        return fpago;
    }

    /**
     * Setter for property fpago.
     * @param fpago New value of property fpago.
     */
    public void setFpago(java.util.TreeMap fpago) {
        this.fpago = fpago;
    }

    public void listarFpago(){
        try{
        dao.listarFpago();
        } catch( Exception e ){

            e.getStackTrace();

        }
    }

     /**
     * busca los registros de tablagen cuyo table_type sea igual al dado y la secuencia del dato
     * @author : Jose de la rosa
     * @param table_type el tipo de la tabla o el nombre de la sub-tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public String buscarCampoTablagenDato( String table_type, String table_code, String secuencia )throws SQLException {
        return dao.buscarCampoTablagenDato(table_type, table_code, secuencia);
    }

     /**
     * Obtiene el valor de la leyenda en el campo dato
     * @author Ing. Andr�s Maturana De La Cruz
     * @param tabla Table Type
     * @param codigo Table Code
     * @param leyenda Leyenda
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public String obtenerLeyendaDato(String tabla, String codigo, String leyenda)throws SQLException {
        return dao.obtenerLeyendaDato(tabla, codigo, leyenda);
    }

    /**
     * busca los registros de tablagen cuyo table_type sea igual a "THCODE"
     * @author : Luis Eduardo Frieri.
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     */
    public void buscarCon_Proveedor()throws SQLException {
       dao.buscarRegistrosOrdenadosByDesc("CONPROV");
       this.setCon_proveedor(dao.obtenerTablas());
    }

    /**
     * Getter for property con_proveedor.
     * @return Value of property con_proveedor.
     */
    public java.util.LinkedList getCon_proveedor() {
        return con_proveedor;
    }

    /**
     * Setter for property con_proveedor.
     * @param con_proveedor New value of property con_proveedor.
     */
    public void setCon_proveedor(java.util.LinkedList con_proveedor) {
        this.con_proveedor = con_proveedor;
    }

      //luigi
    /**
     * M�todo que obtiene los valores de la tabla general con el nombre de tabla GRUPOEQ
     * @autor.......Ing luis frieri
     **/
    public void buscarGruposEq()throws SQLException {
        dao.buscarRegistros("GRUPOEQ");
        this.grupoid = dao.obtenerTablas();
    }

    /**
     * Getter for property grupoid.
     * @return Value of property grupoid.
     */
    public java.util.LinkedList getGrupoid() {
        return grupoid;
    }

    /**
     * Setter for property grupoid.
     * @param grupoid New value of property grupoid.
     */
    public void setGrupoid(java.util.LinkedList grupoid) {
        this.grupoid = grupoid;
    }
     /**
      * Obtienene al aprobador de las facturas para la facturaci�n autom�tica
      * @autor Ing. Andr�s Maturana De La Cruz
      * @param id Almacenadora
      * @throws SQLException si algun error ocurre con el acceso a la base de datos.
      * @return aprobador de la factura
      */
    public String aprobadorCXPAuto(String id)throws SQLException{
        return this.dao.aprobadorCXPAuto(id);
    }
      /**
     * M�todo obtiene el aprobador de las facturas de anticipo a terceros
     * con la tabla general mediante un codigo de tabla
     * @autor.......Enrique De Lavalle Rizo
     * @see.........obtenerInfoTablaGen     -    TblGeneralDAO
     * @version.....1.0.
     **/
    public String obtenerProveedor()throws SQLException{
        return dao.buscarAprobadorFactura();
    }

     //jose de la rosa 2007-03-02
    public void buscarTipoIngreso(String programa)throws SQLException{
       this.tipoIngresos = obtenerInfoTablaGen("TIPOING",  programa);
    }

    /**
     * M�todo que realiza una busqueda en la tabla general dato y relaciona la informacion
     * con la tabla general mediante un codigo de tabla
     * @autor.......Jdelarosa
     * @see.........tablasGeneralesPrograma     -    TblGeneralDAO
     * @version.....2.0.
     **/
    public void tablasGeneralesPrograma(String codTabla, String programa)throws SQLException {
        setTablas( obtenerInfoTablaGen(codTabla,  programa) );
    }


    /**
     * Getter for property tablas.
     * @return Value of property tablas.
     */
    public java.util.LinkedList getTablas() {
        return dao.getTablas();
    }

    /**
     * Setter for property tablas.
     * @param tablas New value of property tablas.
     */
    public void setTablas(java.util.LinkedList tablas) {
        dao.setTablas(tablas);
    }


    /**
     * Obtiene el registro correspondiente a una tabla.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param tbltype el nombre de la tabla, o el tipo
     * @param tblcode C�digo de la tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @return Un bean <CODE>TablaGen</CODE> con los datos de la tabla
     */
    public TablaGen obtenerTablaGen(String tbltype, String tblcode)throws SQLException{
        return this.dao.obtenerTablaGen(tbltype, tblcode);
    }

    /**
     * Obtiene el registro correspondiente a una tabla a partor de una descripci�n.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param tbltype el nombre de la tabla, o el tipo
     * @param desc Descripci�n de la tabla
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @return Un bean <CODE>TablaGen</CODE> con los datos de la tabla
     */
    public TablaGen obtenerTablaGenByDesc(String tbltype, String desc)throws SQLException{
        return this.dao.obtenerTablaGenByDesc(tbltype, desc);
    }


     /**
     * @autor.......Luis Eduardo Frieri
     * @version.....1.0.
     **/
    public TreeMap BuscarDatosTreemap(String type)throws SQLException{
        TreeMap t = dao.BuscarDatosTreemap(type);
        return t;
    }

     /**
     * M�todo que obtiene los tipo de documentos del despacho
     * @autor.......Diogenes Bastidas Morales
     * @see.........obtenerInfoTablaGen     -    TblGeneralDAO
     * @version.....1.0.
     **/
    public void buscarCodigosABC ()throws Exception{
        try{
            dao.buscarRegistrosOrdenadosByDesc("ABC");
            setTipoDocumentos( dao.obtenerTablas() );
        }catch(Exception e){
            e.getStackTrace();
        }
    }

    /**
     * Carga un <code>TreeMap</code> con los registros de una tablagen
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param tbltype el nombre de la tabla, o el tipo
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @return Un bean <CODE>TreeMap</CODE> con los datos de la tabla
     */
    public TreeMap loadTableType(String table_type) throws Exception{
        this.buscarRegistros(table_type);
        List tbls = this.getTablas();
        TreeMap tm = new TreeMap();
        Iterator it = tbls.listIterator();

        while ( it.hasNext()){
            TablaGen tbl = (TablaGen) it.next();
            tm.put( tbl.getDescripcion(), tbl.getTable_code());
        }

        return tm;
    }

    //ffernandez 18/04/2007
     public void listarTODOHc(){
        try{
            dao.listarTODOHc();
        } catch( Exception e ){
            e.getStackTrace();

        }
    }

      public java.util.TreeMap getThcs() {
        return dao.getThcs();
    }

       public ArrayList obtenerRegistroDato(String type, String dato) throws SQLException,Exception {
        return dao.obtenerRegistroDato(type, dato);
    }

       public String getDato(String table_type, String table_code) throws Exception{
        String dato = "";
        try {
            dato = dao.getDato(table_type, table_code);
        }
        catch (Exception e) {
            throw new Exception("Error eal obtener dato: "+e.toString());
        }
        return dato;
    }

       
}
