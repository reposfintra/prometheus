/***********************************************************************************
 * Nombre clase : ............... DespachoManualService.java                            *
 * Descripcion :................. Clase que instancia los metodos de RXPDocDAO          *
 *                                con la BD.                                            *
 * Autor :....................... Ing. David Lamadrid                                   *
 * Fecha :....................... 15 de noviembre de 2005, 11:02 AM                     *
 * Version :..................... 1.0                                                   *
 * Copyright :................... Fintravalores S.A.                               *
 ***********************************************************************************/
package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.sql.*;
import java.io.*;
/**
 *
 * @author  dlamadrid
 */
//logger
import org.apache.log4j.Logger;

public class RXPDocService {
    
    Logger log = Logger.getLogger(this.getClass());
        
    public RXPDocDAO dao = new RXPDocDAO();
    /** Creates a new instance of CXPDocService */
    public boolean enproceso;
    //juan
    private TreeMap codCliAre = new TreeMap() ;
    
    //Ivan Dario Gomez
    private String[] codigoCuenta = {"-","--","---","---","---"};
    
    private TreeMap tipos = new TreeMap();
    
    /** Creates a new instance of CXPDocService */
    public RXPDocService() {
        this.enproceso = false;
    }
    
    /**
     * Getter for property enproceso.
     * @return Value of property enproceso.
     */
    public boolean getEnproceso() {
        return enproceso;
    }
    
    /**
     * Setter for property enproceso.
     * @param enproceso New value of property enproceso.
     */
    public void setEnproceso() {
        this.enproceso = (enproceso == false)?true:false;
    }    
    
    /**
     * Getter for property facturas.
     * @return Value of property facturas.
     */
    public List getFacturas()throws SQLException{
        return dao.getFacturas();
    }        
    
    /**
     * Metodo que instancia un Objeto tipo CXDOC en RXPDocDAO
     * @autor.......David Lamadrid
     * @param.......CXPDoc factura(objeto de CXPDoc)
     * @see.........RXPDocDAO.class
     * @throws......Clase no encontrada.
     * @version.....1.0.
     * @return.......
     */
    public void setFactura(com.tsp.operation.model.beans.CXP_Doc factura) {
        dao.setFactura(factura);
    }
    
    /**
     * Metodo que retorna un Vector de  Objetos tipo CXDOC en RXPDocDAO
     * @autor.......David Lamadrid
     * @param.......ninguno.
     * @see.........RXPDocDAO.class
     * @throws......Clase no encontrada.
     * @version.....1.0.
     * @return.......Vector vCXPDoc.
     */
    public java.util.Vector getVecCxp_doc() {
        return dao.getVecCxp_doc();
    }
    
    /**
     * Metodo que setea un Vector de  Objetos tipo CXDOC en RXPDocDAO
     * @autor.......David Lamadrid
     * @param.......Vector vecCxp_doc(Vector de objetos de CXPDoc)
     * @see.........RXPDocDAO.class
     * @throws......Clase no encontrada.
     * @version.....1.0.
     * @return......Vector de Objetos CXPDoc
     */
    public void setVecCxp_doc(java.util.Vector vecCxp_doc) {
        dao.setVecCxp_doc(vecCxp_doc);
    }
    
    /**
     * Metodo que retorna un Objeto tipo CXDOC en RXPDocDAO
     * @autor.......David Lamadrid
     * @param.......ninguno
     * @see.........RXPDocDAO.class
     * @throws......Clase no encontrada.
     * @version.....1.0.
     * @return......CXPDoc.
     */
    public com.tsp.operation.model.beans.CXP_Doc getFactura() {
        return dao.getFactura();
    }
    
    /**
     * Metodo que retorna un String con fecha generada de una fecha inicial y un numero de dias
     * @autor.......David Lamadrid
     * @param.......ninguno
     * @see.........RXPDocDAO.class
     * @throws.......
     * @version.....1.0.
     * @return......String fechaFinal.
     */
    public  String fechaFinal(String fechai,int n) {
        return dao.fechaFinal(fechai,n);
    }
    
    /**
     * Metodo que retorna un boolean para verificar la existencia de un Documento por pagar en el sistema
     * retorna true si existe y false si no existe.
     * @autor.......David Lamadrid
     * @param.......String dis(Distrito),String proveedor,String tipoDoc,String docuemnto.
     * @see.........RXPDocDAO.class
     * @throws.......
     * @version.....1.0.
     * @return......boolean.
     */
    public boolean existeDoc(String dis,String proveedor,String tipoDoc,String documento ) throws SQLException {
        return dao.existeDoc(dis, proveedor , tipoDoc , documento );
    }    
    
    /**
     * Metodo que Inserta un Documento por pagar en el Sistema .
     * @autor.......David Lamadrid
     * @param.......CXDoc doc(Objeto tipo CXPDoc), Vector vItems(Vector de objetos CxPItem),Vector vImpuestos(Vector de Objetos CXPImpDoc).
     * @see.........RXPDocDAO.class
     * @throws......Existe Documento en el Sistema.
     * @version.....1.0.
     * @return.......
     */
    public void insertarCXPDoc( CXP_Doc doc,Vector vItems,Vector vImpuestosDoc,String agencia ) throws Exception {
        dao.insertarCXPDoc( doc, vItems, vImpuestosDoc, agencia ); 
    }    
    
    /**
     * Metodo que retorna un String con Informacion de la Diferencia entre dos fechas.
     * @autor.......David Lamadrid
     * @param.......ninguna
     * @see.........RXPDocDAO.class
     * @throws......Fecha Invalida.
     * @version.....1.0.
     * @return......String resultado(Diferencia de fechas)
     */
    public  String  getDiferencia(java.util.Date fecha1, java.util.Date fecha2) {
        java.util.Date fechaMayor = null;
        java.util.Date fechaMenor = null;
        Map resultadoMap = new HashMap();
        
                /* Verificamos cual es la mayor de las dos fechas, para no tener sorpresas al momento
                 * de realizar la resta.
                 */
        if (fecha1.compareTo(fecha2) > 0) {
            fechaMayor = fecha1;
            fechaMenor = fecha2;
        }else {
            fechaMayor = fecha2;
            fechaMenor = fecha1;
        }
        
        //los milisegundos
        long diferenciaMils = fechaMayor.getTime() - fechaMenor.getTime();
        
        //obtenemos los segundos
        long segundos = diferenciaMils / 1000;
        
        //obtenemos las horas
        long horas = segundos / 3600;
        
        //restamos las horas para continuar con minutos
        segundos -= horas*3600;
        
        //igual que el paso anterior
        long minutos = segundos /60;
        segundos -= minutos*60;
        
        //ponemos los resultados en un mapa :-)
        resultadoMap.put("horas",Long.toString(horas));
        resultadoMap.put("minutos",Long.toString(minutos));
        resultadoMap.put("segundos",Long.toString(segundos));
        String resultado="";
        resultado= " Horas "+horas+" minutos "+minutos +" segundos"+segundos;
        //return resultadoMap;
        return resultado;
    }
    
    
    /**
     * Metodo que retorna un long con el numero de dias que existe entre una fecha inicial y una fecha final.
     * @autor.......David Lamadrid
     * @param.......fecha1 fecha Inicial,fecha2 Fecha Final type Timestamp.
     * @see.........RXPDocDAO.class
     * @throws......Fecha Invalida.
     * @version.....1.0.
     * @return......long resultado (numero de dias)
     */
    public long getDiasDiferencia(java.sql.Timestamp fecha1,java.sql.Timestamp fecha2 ) {
        long resultado =0;
        resultado = ((fecha1.getTime()-fecha2.getTime())/1000)/86400;
        return resultado;
    }
    
    /**
     * Metodo que retorna un Vector con los nombres de proveedores dado un String[] con los proveedores
     * sepearados por |;
     * @autor.......David Lamadrid
     * @param.......String []linea(lista de proveedores) .
     * @see.........RXPDocDAO.class
     * @throws......String[] linea invalido.
     * @version.....1.0.
     * @return......Vector campos(Vector de String con informacion de los Proveedores)
     */
    public  Vector obtenerComboProveedores(String []linea) {
        Vector campos = new Vector();
        for(int i=0;i<linea.length;i++) {
            StringTokenizer separados = new StringTokenizer(linea[i], "|");
            
            // Agrego cada uno de los campos al vector y luego lo retorno
            Vector fila  = new Vector();
            while (separados.hasMoreTokens()) {
                fila.addElement(separados.nextToken());
            }
            campos.add(fila);
        }
        return campos;
    }
    
    /**
     * Metodo que retorna un Vector con los nombres de proveedores dado un String con los proveedores
     * sepearados por |;
     * @autor.......David Lamadrid
     * @param.......String linea(lista de proveedores) .
     * @see.........RXPDocDAO.class
     * @throws......String[] linea invalido.
     * @version.....1.0.
     * @return......Vector campos(Vector de String con informacion de los Proveedores)
     */
    public  Vector obtenerComboProveedores1(String linea) {
        Vector campos = new Vector();
        String campo="";
        int pos=0;
        ////System.out.println("linea antes del while  "+linea);
        while (linea.length()>1) {
            pos=linea.indexOf("|");
            campo = linea.substring(0,pos);
            ////System.out.println("campo " + campo);
            campos.add(campo);
            linea = linea.substring(pos+1,linea.length());
            ////System.out.println("linea "+linea);
        }
        return campos;
    }
    
    //********************Mofificado Juan 02-05-2006
    /**
     * Metodo que guarda un objeto tipo CXP_Doc en un Archivo
     * @autor.......David Lamadrid
     * @param.......CXP_Doc factura
     * @see........
     * @throws.......IOException
     * @version.....1.0.
     * @return......Vector vImpuestos.
     */
    public void escribirArchivo(CXP_Doc factura,Vector items,String maxfila,String nombreArchivo,String user)throws IOException{
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String path = rb.getString("ruta");
        String Ruta  = path + "/exportar/migracion/" + user;
        String RutaA = Ruta + "/"+nombreArchivo;
        
        File archivo = new File(Ruta);
        archivo.mkdirs();
        File archivoR =  new File(RutaA);
        
        FileOutputStream  salidaArchivoR =   new FileOutputStream(archivoR);
        
        ObjectOutputStream salidaObjetoR =   new ObjectOutputStream(salidaArchivoR);
        
        salidaObjetoR.writeObject(factura);
        salidaObjetoR.writeObject(items);
        salidaObjetoR.writeObject(maxfila);
        
        salidaObjetoR.close();
        
        
    }
    
    /**
     * Metodo que Lee  un objeto tipo CXP_Doc en un Archivo
     * @autor.......David Lamadrid
     * @param.......String nombreArchivo,String user;
     * @see........
     * @throws.......IOException
     * @version.....1.0.
     * @return......Objeto tipo CXP_Doc.
     */
    public CXP_Doc leerArchivo(String nombreArchivo,String user)throws IOException,ClassNotFoundException{
        CXP_Doc factura;
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String path = rb.getString("ruta");
        String Ruta  = path + "/exportar/migracion/" + user + "/"+nombreArchivo;
        log.info("LEYENDO CABECERA: " + Ruta);
        File archivo = new File(Ruta);
        FileInputStream entradaArchivo = new FileInputStream(archivo);
        ObjectInputStream entradaObjeto= new ObjectInputStream(entradaArchivo);
        try{
            factura = (CXP_Doc) entradaObjeto.readObject();
        }
        catch(ClassNotFoundException e){
            factura=null;
            ////System.out.println("Error no se ha encontrado la clase");
        }
        entradaObjeto.close();
        return factura;
    }
    
    /**
     * Metodo que Lee  un objeto tipo Vector en un Archivo
     * @autor.......David Lamadrid
     * @param.......String nombreArchivo,String user;
     * @see........
     * @throws.......IOException
     * @version.....1.0.
     * @return......Objeto tipo Vector con los Items de una factura.
     */
    public Vector leerArchivoItems(String nombreArchivo,String user)throws IOException,ClassNotFoundException{
        Vector items;
        CXP_Doc factura;
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String path = rb.getString("ruta");
        String Ruta  = path + "/exportar/migracion/" + user + "/"+nombreArchivo;
        File archivo = new File(Ruta);
        log.info("LEYENDO ITEMS: " + Ruta);
        FileInputStream entradaArchivo = new FileInputStream(archivo);
        ObjectInputStream entradaObjeto= new ObjectInputStream(entradaArchivo);
        try{
            factura = (CXP_Doc) entradaObjeto.readObject();
            items = (Vector) entradaObjeto.readObject();
        }
        catch(ClassNotFoundException e){
            items=null;
            ////System.out.println("Error no se ha encontrado la clase");
        }
        entradaObjeto.close();
        return items;
    }
    
    /**
     * Metodo que Lee  un objeto tipo Vector en un Archivo
     * @autor.......David Lamadrid
     * @param.......String nombreArchivo,String user;
     * @see........
     * @throws.......IOException
     * @version.....1.0.
     * @return......Objeto tipo Vector con los Items de una factura.
     */
    public String leerArchivoMaxfila(String nombreArchivo,String user)throws IOException,ClassNotFoundException{
        String maxfila ="1";
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String path = rb.getString("ruta");
        String Ruta  = path + "/exportar/migracion/" + user + "/"+nombreArchivo;
        File archivo = new File(Ruta);
        FileInputStream entradaArchivo = new FileInputStream(archivo);
        ObjectInputStream entradaObjeto= new ObjectInputStream(entradaArchivo);
        try{
            maxfila = (String) entradaObjeto.readObject();
        }
        catch(ClassNotFoundException e){
            maxfila="1";
            ////System.out.println("Error no se ha encontrado la clase");
        }
        entradaObjeto.close();
        return maxfila;
    }    
    
    /**
     * Metodo: getCXP_doc, permite retornar un objeto de registros de CXP_doc.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @see getCXP_doc - RXPDocDAO
     * @version : 1.0
     */
    public CXP_Doc getCXP_doc( ){
        return dao.getCXP_doc();
    }
    
    /**
     * Metodo: setCXP_doc, permite obtener un objeto de registros de CXP_doc.
     * @autor : Ing. Jose de la rosa
     * @param : objeto
     * @see setCXP_doc - RXPDocDAO
     * @version : 1.0
     */
    
    public void setCXP_doc(CXP_Doc acuerdo){
        dao.setCXP_doc(acuerdo);
    }
    /**
     * Metodo: getNumero, convierte un String en en double.
     * @autor : Ing. david Lamadrid
     * @param : String
     * @version : 1.0
     */
    public double getNumero(String numero){
        double n=0;
        String cadena="";
        if(!numero.equals("")){
            String[] campos= numero.split(",");
            for(int i=0;i<campos.length;i++){
                cadena = cadena+campos[i];
            }
            n= Double.parseDouble(cadena);
            
            try {
                n= Double.parseDouble(cadena);
            }
            catch(java.lang.NumberFormatException e) {
                n=0;
            }
        }
        else{
            n=0;
        }
        return n;
    }
    
    public String formatearNumero(String n){
        // para estar seguros que vamos a trabajar con un string
        String valor = ""+ n ;
        String res = "";
        int i=0;
        int j=0;
        for( i=valor.length()-1,j = 0; i >= 0; i--,j++ ){
            if ( j % 3 == 0 && j != 0 ){
                res += ",";
            }
            res += valor.charAt(i);
        }
        //ahora nos toca invertir el numero;
        String  aux = "";
        for( i=res.length()-1; i >= 0; i-- ){
            aux += res.charAt(i);
        }
        return aux;
    }
    
    public java.util.Vector getVFacturas() {
        return dao.getVFacturas();
    }
    /**
     * m�todo obtenerNumeroFXP , Busca la factura por proveedor
     * @param String Proveedor
     * @throws
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public void obtenerNumeroFXP(String proveedor)throws SQLException{
        dao.obtenerNumeroFXP(proveedor);
    }
    
    /*
    /**
     * Este m�todo genera un Treemap con los Bancos pertenecientes a una agencia
     * @param String agencia
     * @throws
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    /* public TreeMap obtenerNombresBancosPorAgencia(String agencia) throws SQLException{
        TreeMap bancos = new TreeMap();
     
        Vector banks = this.obtenerBancosPorAgencia(agencia);
     
        for(int i=0; i<banks.size(); i++){
                Banco banc = (Banco) banks.elementAt(i);
                bancos.put(banc.getBanco(), banc.getBanco());
        }
     
        return bancos;
    }*/
    
    //juan  02-05-2006
    public TreeMap llenarCodCliAre(){
        this.codCliAre.put("C","C");
        return codCliAre;
    }
    
    /**
     * Getter for property codCliAre.
     * @return Value of property codCliAre.
     */
    public java.util.TreeMap getCodCliAre() {
        return codCliAre;
    }
    
    /**
     * Setter for property codCliAre.
     * @param codCliAre New value of property codCliAre.
     */
    public void setCodCliAre(java.util.TreeMap codCliAre) {
        this.codCliAre = codCliAre;
    }
    /**
     * Metodo: BorrarArchivo, permite obtener un objeto de registros de CXP_doc.
     * @autor : Ing. DAvid Lamidrid
     * @param : String Nombre del archivo, String Usuario
     * @version : 1.0
     */
    public boolean BorrarArchivo(String nombreArchivo,String user)throws IOException,ClassNotFoundException{
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String path = rb.getString("ruta");
        String Ruta  = path + "/exportar/migracion/" + user + "/"+nombreArchivo;
        File archivo = new File(Ruta);
        return archivo.delete();
    }
    
    
    /**
     * Metodo BuscarFactura: busca el encabezado de la factura
     * @autor.......Ivan Dario Gomez
     * @param.......String distrito, String codigo del proveedor y numero de la factura
     * @see.........BuscarFactura - RXPDocDAO
     * @throws.......Exception
     * @version.....1.0.
     * @return......Objeto tipo CXP_Doc.
     */
    public CXP_Doc BuscarFactura(String dstrct, String proveedor, String documento)throws Exception{
        dao.BuscarFactura( dstrct,  proveedor,  documento);
        return dao.getFactura();
    }
    /**
     * Metodo BuscarItemFactura: busca los Item de la factura
     * @autor.......Ivan Dario Gomez
     * @param.......String distrito, String codigo del proveedor y numero de la factura
     * @see.........BuscarItemFactura - RXPDocDAO
     * @throws.......Exception
     * @version.....1.0.
     * @return......Vector de Item.
     */
    public Vector BuscarItemFactura(CXP_Doc factura,Vector vTipImpuestos)throws Exception{
        return dao.BuscarItemFactura(factura, vTipImpuestos); 
        
    }
    
    
    /**
     * Metodo UpdateFactura: Actualiza los campos de la factua
     * @autor.......Ivan Dario Gomez
     * @param.......String distrito, String codigo del proveedor y numero de la factura
     * @see.........
     * @throws.......Exception
     * @version.....1.0.
     */
    public void UpdateFactura( CXP_Doc doc,Vector vItems,Vector vImpuestosDoc,String agencia )throws Exception{
         dao.UpdateFactura(  doc, vItems, vImpuestosDoc, agencia );   
        
    }
    
    /**
     * Getter for property codigoCuenta.
     * @return Value of property codigoCuenta.
     */
    public java.lang.String[] getCodigoCuenta() {
        return this.codigoCuenta;
    }
    
    /**
     * Setter for property codigoCuenta.
     * @param codigoCuenta New value of property codigoCuenta.
     */
    public void setCodigoCuenta(String Dato, int pos) {
        this.codigoCuenta[pos] = Dato;
    }
    
    /**
     * Metodo: getVecCxpItemsDoc, Medoto para getar el vector de item
     * @autor : Ing.David Lamadrid
     * @param :
     * @see getVecCxpItemsDoc - CXPItemDocDAO
     * @version : 1.0
     */ 
    public java.util.Vector getVecRxpItemsDoc() {
        return dao.getVecRxpItemsDoc();
    }
    /**
     * Metodo: setVecCxpItemsDoc, Medoto para setar el vector de item
     * @autor : Ing.David Lamadrid
     * @param :
     * @see setVecCxpItemsDoc - CXPItemDocDAO
     * @version : 1.0
     */ 
    public void setVecRxpItemsDoc(java.util.Vector vecCxpItemsDoc) {        
        dao.setVecRxpItemsDoc(vecCxpItemsDoc);
    }
    
    
    /********************************************************************
     *                  ANULACION DE FACTURAS RECURRENTES               *
     ********************************************************************/
    
    /**
     * Metodo:          ExisteCXP_Doc
     * Descriocion :    Funcion publica que obtiene el metodo ExisteCXP_Doc del DAO
     * @autor :         Leonardo Parody
     * @modificado por: LREALES - mayo del 2006
     * @param :         distrito, proveedor, tipo de documento y documento
     * @return:         boo ( retorna un boolean )
     * @version :       1.0
     */
    public boolean ExisteCXP_Doc ( String distrito, String proveedor, String tipo_documento, String documento ) throws SQLException {
        
        dao.ExisteCXP_Doc ( distrito, proveedor, tipo_documento, documento );
        return dao.ExisteCXP_Doc ( distrito, proveedor, tipo_documento, documento );
        
    }
    
    /**
     * Metodo:          ConsultarCXP_Doc
     * Descriocion :    Funcion publica que obtiene el metodo ConsultarCXP_Doc del DAO
     * @autor :         Leonardo Parody
     * @modificado por: LREALES - mayo del 2006
     * @param :         distrito, proveedor, tipo de documento y documento
     * @return:         cXP_Doc ( retorna un Objeto )
     * @version :       1.0
     */
    public CXP_Doc ConsultarCXP_Doc ( String distrito, String proveedor, String tipo_documento, String documento ) throws SQLException {
        
        dao.ConsultarCXP_Doc( distrito, proveedor, tipo_documento, documento );
        return dao.ConsultarCXP_Doc( distrito, proveedor, tipo_documento, documento );
        
    }
    
    /**
     * Metodo:          AnularCXP_Doc
     * Descriocion :    Funcion publica que obtiene el metodo AnularCXP_Doc del DAO
     * @autor :         Leonardo Parody
     * @modificado por: LREALES - mayo del 2006
     * @param :         cXP_Doc ( documento de cuentas por pagar )
     * @return:         nada.
     * @version :       1.0
     */
    public void AnularCXP_Doc ( CXP_Doc cXP_Doc ) throws SQLException {
        
        dao.AnularCXP_Doc( cXP_Doc );
        
    }
    
    /**
     * Metodo:          SQL_Nombre_Proveedor
     * Descriocion :    Funcion publica que obtiene el metodo SQL_Nombre_Proveedor del DAO
     * @autor :         LREALES
     * @param :         distrito y proveedor
     * @return:         nombre ( retorna un String )
     * @version :       1.0
     */
    public String SQL_Nombre_Proveedor ( String distrito, String proveedor ) throws SQLException {
        
        dao.SQL_Nombre_Proveedor ( distrito, proveedor );
        return dao.SQL_Nombre_Proveedor ( distrito, proveedor );
        
    }   
    
    
    /**
     * Muestra el detalle de la factura
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public CXP_Doc detalleFactura(String dstrct, String proveedor, String documento,String tipo_doc)throws Exception{
        dao.detalleFactura( dstrct,  proveedor,  documento, tipo_doc); 
        return dao.getFactura();
    }
    
     /**
     * Muestra el detalle de la factura
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public Vector detalleItemFactura(CXP_Doc factura, Vector vTipImpuestos)throws SQLException{
        dao.detalleItemFactura(factura, vTipImpuestos); 
        return dao.getVecCxp_doc();
    }
    
     /**
     * Metodo buscarFacturasPlanillas , Metodo para buscar las facturas de una planilla
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @throws  : SQLException
     * @version : 1.0
     */
    public void buscarFacturasPlanillas(String dstrct,
    String fra,
    String tipo_doc,
    String agc,
    String banco,
    String sucursal,
    String nit,
    String fechai,
    String fechaf,
    String pagada,
    String planilla) throws SQLException{
        dao.buscarFacturasPlanillas( dstrct,fra, tipo_doc, agc,banco, sucursal, nit, fechai, fechaf, pagada, planilla);
    }
    
    /**
     * Obtiene el consecutivo
     * @autor   Ing. Andres Maturana
     * @throws  SQLException
     * @version 1.0
     */
    public String obtenerConsecutivo() throws SQLException{
        return dao.obtenerConsecutivo();
    }
    /**************************************************************
     * metodo anularRecurrentes, recibe un vector con los datos de 
     *        las facturas que se deben anular, cada posicion del 
     *        vector representa una factura, de esta forma:
     *        DSTRCT-_-PROVEEDOR-_-DOCUMENTO-_-TIPO_DOCUMENTO
     *        ej: TSP-_-22565923-_-FR00049-_-010
     * @param: String[] facturas
     * @param: String usuario
     * @throws: en caso de un error de BD
     **************************************************************/
    public int anularRecurrentes( String[] facturas, String usuario ) throws Exception{
        
        int cont=0;
        
        for( int i=0; i<facturas.length; i++ ){
                        
            String factura[] = facturas[i].split("-_-");
            
            if( factura.length == 4 ){
                
                CXP_Doc c = new CXP_Doc();
                
                c.setUser_update( usuario );
                c.setDstrct( factura[0] );
                c.setProveedor( factura[1] );
                c.setDocumento( factura[2] );
                c.setTipo_documento( factura[3] );
                
                dao.AnularCXP_Doc( c );
                cont++;
            }
            
        }
        return cont;
                
    }
     /***************************************************
     * metodo aplazarRecurrente, actualiza la fecha de la
     *        ultima transferencia para aplazar la generacion 
     *        de la proxima factura(cuota)
     * @author: Ing: Osvaldo P�rez Ferrer
     * @throws: en casi de un error de BD
     ***************************************************/
    public void aplazarRecurrente ( CXP_Doc r ) throws Exception{
        dao.aplazarRecurrente(r);
    }
    
     /**
     * Metodo obtenerAgencias , Metodo para buscar las agencias
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public void consultarDocumento(
            String dstrct,
            String fra,
            String tipo_doc,
            String agc,
            String banco,
            String sucursal,
            String nit,
            String fechai,
            String fechaf,
            String pagada,
            String anuladas,
            String tipo) throws SQLException{
                dao.consultarDocumento(dstrct, fra, tipo_doc, agc, banco, sucursal, nit, fechai, fechaf, pagada, anuladas, tipo);  
    }
    
    /**
     * Getter for property tipos.
     * @return Value of property tipos.
     */
    public java.util.TreeMap getTipos() {
        return tipos;
    }    
    
    /**
     * Setter for property tipos.
     * @param tipos New value of property tipos.
     */
    public void setTipos(java.util.TreeMap tipos) {
        this.tipos = tipos;
    }
    
     /**
     * Obtener todos los conceptos
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public void cargarConceptos (String dstrct ) throws Exception{
        this.dao.cargarConceptos(dstrct);
    }
    
    /**
     * Getter for property treemap.
     * @return Value of property treemap.
     */
    public java.util.TreeMap getTreemap() {
        return dao.getTreemap();
    }
    
    /**
     * Setter for property treemap.
     * @param treemap New value of property treemap.
     */
    public void setTreemap(java.util.TreeMap treemap) {
        dao.setTreemap(treemap);
    }
    
    /**
     * Consultar RxP filtrando por concepto
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct Distrito
     * @param nit Nit del proveedor
     * @param fechai Fecha de inicio del per�odo
     * @param fechaf Fecha de fin del per�odo
     * @param conc C�digo del concepto
     * @param pendiente Si el saldo en me es igual a 0
     * @throws SQLException
     * @version 1.0
     */
    public void consultarRxPConcepto (String dstrct, String nit, String fechai, String fechaf, String conc, String pendiente ) throws Exception{
        dao.consultarRxPConcepto(dstrct, nit, fechai, fechaf, conc, pendiente);
    }
    
    
     /**
     * Metodo updateBanco, permite actualizar el banco de una factura
     * recibe por parametro
     * @autor : Ing. Ivan Dario Gomez
     * @param : String distrito, String proveedor, String tipo_doc, String documento, String banco,String sucursal,String moneda
     * @version : 1.0
     */
    public void updateBanco(String distrito, String proveedor, String tipo_doc, String documento, String banco,String sucursal) throws SQLException {
        dao.updateBanco(distrito, proveedor, tipo_doc, documento, banco, sucursal); 
    }
    
}//Fin de la Clase
