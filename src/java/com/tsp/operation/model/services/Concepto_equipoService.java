/************************************************************************
* Nombre ....................Concepto_equipoService.java                *
* Descripci�n................Clase que maneja los servicios al model    *
*                            relacionados con los conceptos de equipos. *
* Autor......................Ing. Jose de la rosa                       *
* Fecha Creaci�n.............14 de diciembre de 2005, 10:22 PM          *
* Modificado por.............LREALES                                    *
* Fecha Modificaci�n.........5 de junio de 2006, 10:13 AM               *
* Versi�n....................1.0                                        *
* Coyright...................Transportes Sanchez Polo S.A.              *
************************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

public class Concepto_equipoService {
    
    private Concepto_equipoDAO concepto_equipo;    
    
    /** Creates a new instance of Concepto_equipoService */
    public Concepto_equipoService () {
        concepto_equipo = new Concepto_equipoDAO();
    }
    
    /**
     * Metodo: getConcepto_equipo, permite retornar un objeto de registros de concepto equipo.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */    
    public Concepto_equipo getConcepto_equipo( )throws SQLException{
        return concepto_equipo.getConcepto_equipo();
    }
    
    /**
     * Metodo: getConcepto_equipoes, permite retornar un vector de registros de concepto equipo.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */    
    public Vector getConcepto_equipos()throws SQLException{
        return concepto_equipo.getConcepto_equipos();
    }
    
    /**
     * Metodo: setConcepto_equipo, permite obtener un objeto de registros de concepto equipo.
     * @autor : Ing. Jose de la rosa
     * @param : objeto
     * @version : 1.0
     */
    public void setConcepto_equipo (Concepto_equipo acuerdo)throws SQLException {
        concepto_equipo.setConcepto_equipo(acuerdo);
    }
    
    /**
     * Metodo: setConcepto_equipoes, permite obtener un vector de registros de concepto equipo.
     * @autor : Ing. Jose de la rosa
     * @param : vector
     * @version : 1.0
     */    
    public void setConcepto_equipos(Vector Concepto_equipos)throws SQLException{
        concepto_equipo.setConcepto_equipos(Concepto_equipos);
    }
    
    /**
    * Metodo insertConcepto_equipo, ingresa los acuerdos especiales (Concepto_equipo).
    * @autor : Ing. Jose de la rosa
    * @modificado por: LREALES
    * @see insertarConcepto_equipo - Concepto_equipoDAO
    * @param : Concepto_equipo
    * @version : 1.0
    */   
    public void insertConcepto_equipo( Concepto_equipo user ) throws SQLException{
        
        concepto_equipo.setConcepto_equipo( user );
        concepto_equipo.insertConcepto_equipo();
        
    }    
    
    /**
    * Metodo existConcepto_equipo, obtiene la informacion de los concepto equipos dado unos parametros
    * (standar, codigo).
    * @autor : Ing. Jose de la rosa
    * @modificado por: LREALES
    * @see existConcepto_equipo - Concepto_equipoDAO
    * @param : el codigo
    * @version : 1.0
    */       
    public boolean existConcepto_equipo( String codigo ) throws SQLException{
        
        return concepto_equipo.existConcepto_equipo( codigo );
       
    }    
    
    /**
    * Metodo searchConcepto_equipo, permite buscar un concepto equipo dado unos parametros
    * (standar, codigo).
    * @autor : Ing. Jose de la rosa
    * @modificado por: LREALES
    * @see searchConcepto_equipo - Concepto_equipoDAO
    * @param : el codigo
    * @version : 1.0
    */      
    public void searchConcepto_equipo( String codigo )throws SQLException{
               
        concepto_equipo.searchConcepto_equipo( codigo );
        
    }
    
    /**
    * Metodo listConcepto_equipo, lista la informacion de los conceptos equipos
    * @autor : Ing. Jose de la rosa
    * @modificado por: LREALES
    * @see listConcepto_equipo - Concepto_equipoDAO
    * @param :
    * @version : 1.0
    */    
    public void listConcepto_equipo()throws SQLException{
               
        concepto_equipo.listConcepto_equipo();
        
    }
    
    /**
    * Metodo updateConcepto_equipo, permite actualizar un concepto equipo dados unos parametros
    * @autor : Ing. Jose de la rosa
    * @modificado por: LREALES
    * @see updateConcepto_equipo - Concepto_equipoDAO
    * @param : 
    * @version : 1.0
    */     
    public void updateConcepto_equipo( Concepto_equipo res )throws SQLException{
              
        concepto_equipo.setConcepto_equipo( res );
        concepto_equipo.updateConcepto_equipo();
        
    }

    /**
    * Metodo anularConcepto_equipo, permite anular un concepto equipo de la bd
    * (Concepto_equipo).
    * @autor : Ing. Jose de la rosa
    * @modificado por: LREALES
    * @see anularConcepto_equipo - Concepto_equipoDAO
    * @param : Concepto_equipo
    * @version : 1.0
    */      
    public void anularConcepto_equipo( Concepto_equipo res )throws SQLException{
        
        concepto_equipo.setConcepto_equipo( res );
        concepto_equipo.anularConcepto_equipo();
        
    }
    
    /**
     * Metodo: searchDetalleConcepto_equipo, permite listar los conceptos equipos por detalles dados unos parametros.
     * (standar, codigo).
     * @autor : Ing. Jose de la rosa
     * @modificado por: LREALES
     * @see anularConcepto_equipo - Concepto_equipoDAO
     * @param : el codigo y la descripcion del equipo.
     * @version : 1.0
     */       
    public void searchDetalleConcepto_equipo ( String codigo, String descripcion ) throws SQLException{
        
        concepto_equipo.searchDetalleConcepto_equipo( codigo, descripcion );
        
    }    
    
    /**************************************************************************/
    
    /**
     * Metodo: getOtro_vector, permite retornar un vector de registros de concepto equipo.
     * @autor : LREALES
     * @param : -
     * @version : 1.0
     */    
    public Vector getOtro_vector()throws SQLException{
        
        return concepto_equipo.getOtro_vector();
        
    }
    
    /**
    * Metodo existeCodigoEnTBLCON, verifica si existe el concepto de equipos
    * dado unos parametros (distrito, codigo).
    * @autor : LREALES
    * @see existeCodigoEnTBLCON - Concepto_equipoDAO
    * @param : el distrito y el codigo
    * @version : 1.0
    */       
    public boolean existeCodigoEnTBLCON( String distrito, String codigo ) throws SQLException{
        
        return concepto_equipo.existeCodigoEnTBLCON( distrito, codigo );
       
    }    
    
    /**
    * Metodo searchDetallesTBLCON, permite buscar todos los conceptos equipos.
    * @autor : LREALES
    * @see searchDetallesTBLCON - Concepto_equipoDAO
    * @param : -
    * @version : 1.0
    */      
    public void searchDetallesTBLCON()throws SQLException{
               
        concepto_equipo.searchDetallesTBLCON();
        
    }
    
}