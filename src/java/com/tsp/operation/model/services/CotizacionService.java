/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.services;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
/**
 *
 * @author Rhonalf
 */
public class CotizacionService {
    private CotizacionDAO cdao;
    private ElectricaribeOfertaDAO edao;
    public CotizacionService() {
        cdao = new CotizacionDAO();
        edao = new ElectricaribeOfertaDAO();
    }
    public CotizacionService(String dataBaseName) {
        cdao = new CotizacionDAO(dataBaseName);
        edao = new ElectricaribeOfertaDAO(dataBaseName);
    }

    public String getConsecutivo() throws Exception{
	  return cdao.getConsecutivo();
    }

    public void aprobarOrden(ArrayList codigos){
        int cant = 0;
        try{
            cant = codigos.size();
            for(int i=0;i<cant;i++){
                cdao.aprobarOrden((String)codigos.get(i));
            }
        }
        catch(Exception e){
            System.out.println("Error en la aprobacion: "+e.getMessage());
            e.printStackTrace();
        }
    }

    public ArrayList ver_Aprobadas(){
        ArrayList list = new ArrayList();
        try{
          list = cdao.ver_Aprobadas();
        }
        catch(Exception e){
            System.out.println("Error en ver cotizaciones aprobadas: "+e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList verPend(){
        ArrayList list = new ArrayList();
        try{
          list = cdao.verPendientes();
        }
        catch(Exception e){
            System.out.println("Error en ver no pendientes: "+e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList verTodas(){
        ArrayList list = new ArrayList();
        try{
          list = cdao.verTodas();
        }
        catch(Exception e){
            System.out.println("Error en ver no pendientes: "+e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList verDetalles(String consecutivo) {
        ArrayList list = new ArrayList();
        try{
            list = cdao.detallesConsec(consecutivo);
        }
        catch(Exception e){
            System.out.println("Error en ver detalles: "+e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public void insertDetsPlusOrden(ArrayList lista,String fec,String user) {
        String accion="";
        Cotizacion cot = null;
        ClientesVerService csr = new ClientesVerService(this.cdao.getDatabaseName());
        String estado="";
        try{
            int numdatos = lista.size();
            if(numdatos>0){
                cot = (Cotizacion)lista.get(0);
                accion = cot.getAccion();
                estado = csr.estadoAccion(accion);
                cdao.insertDetPlusOrden(lista, user, fec);
                System.out.println("Insercion de datos de cotizacion para la accion "+accion);
                if(buscarBorrador(accion)) {
                    cdao.actualizarEstado(accion,user);
                    System.out.println("Pasando de borrador a cotizacion definitiva datos de cotizacion para la accion "+accion);
                }

            }
            else {
                 System.out.println("Problema en la insercion: el service no ha recibido ningun dato y por lo tanto no hay modificaciones");
            }

        }
        catch(Exception e){
            System.out.println("Error en la insercion: "+e.getMessage());
            e.printStackTrace();
        }
    }


    public TreeMap datosMs() {
        TreeMap rsp = null;
        try{
            rsp = cdao.datosMs();
        }
        catch(Exception e){
            System.out.println("Error en buscar datos ms: "+e.getMessage());
            e.printStackTrace();
        }
        return rsp;
    }

    public String datosAccion(String idaccion){
        String resp = "";
        try{
            resp = cdao.datosAccion(idaccion);
        }
        catch(Exception e){
            System.out.println("Error en buscar datos accion: "+e.getMessage());
            e.printStackTrace();
        }
        return resp;
    }

    public ArrayList buscar(String param, String filtro){
        ArrayList list = new ArrayList();
        try{
            list = cdao.buscar(filtro, param);
        }
        catch(Exception e){
            System.out.println("Error en buscar datos por consecutivo: "+e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList buscarDets(String param, String filtro){
        ArrayList list = new ArrayList();
        try{
            list = cdao.buscarDets(filtro, param);
        }
        catch(Exception e){
            System.out.println("Error en buscar datos por consecutivo dets: "+e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public void anularOrden(ArrayList codigos,String usuario){
        int cant = 0;
        try{
            cant = codigos.size();
            for(int i=0;i<cant;i++){
                cdao.anularCotizacion((String)codigos.get(i),usuario);
            }
        }
        catch(Exception e){
            System.out.println("Error en la anulacion: "+e.getMessage());
            e.printStackTrace();
        }
    }

    public boolean existeCotizacion(String idaccion){
        boolean ex=false;
        try{
            ex = cdao.existeCotizacion(idaccion);
        }
        catch(Exception e){
            System.out.println("Error en la busqueda de existencia: "+e.getMessage());
            e.printStackTrace();
        }
        return ex;
    }

    //added 24-oct-2009
    public ArrayList getValoresAccion(String idaccion){
        ArrayList resp = null;
        try{
            resp = cdao.getvaloresAccion(idaccion);
        }
        catch(Exception e){
            System.out.println("Error en buscar datos de la accion: "+e.getMessage());
            e.printStackTrace();
        }
        return resp;
    }

    public String getConsecutivo(String idaccion){
        String resp = "";
         try{
            resp = cdao.getConsecutivo(idaccion);
        }
        catch(Exception e){
            System.out.println("Error en buscar datos accion: "+e.getMessage());
            e.printStackTrace();
        }
        return resp;
    }

    //added 26-oct-2010
    public void valorizarAIU(double pa,double pi,double pu,double a,double i,double u,String user,String accion) {
         try{
            cdao.valorizarAIU(pa, pi, pu, a, i, u, user, accion);
        }
        catch(Exception e){
            System.out.println("Error en actualizar datos accion aiu: "+e.getMessage());
            e.printStackTrace();
        }
    }

    //added 27-oct-2010
    public boolean hayValoresAiu(String idaccion){
        boolean resp = false;
        try{
            resp = cdao.hayValoresAiu(idaccion);
        }
        catch(Exception e){
            System.out.println("Error en verificar datos accion aiu: "+e.getMessage());
            e.printStackTrace();
        }
        return resp;
    }
    
    public double[] getPorcentajesAIU(String idaccion){//091029
        double[] resp = null;
        try{
            resp = cdao.getPorcentajesAIU(idaccion);
        }
        catch(Exception e){
            System.out.println("Error en getPorcentajesAIU: "+e.toString()+"__"+e.getMessage());
            e.printStackTrace();
        }
        return resp;
    }

    //091222
    public void anularRegistro(ArrayList regs,String usuario) {
        int longitud = regs.size();
        try{
            if(longitud>0) {
                cdao.anularRegistro(regs, usuario);
            }
            else{
                System.out.println("Problema en anular registro: no se enviaron datos al service");
            }
        }
        catch(Exception e){
            System.out.println("Error en anular registro: "+e.getMessage());
            e.printStackTrace();
        }
    }


    public void guardarBorrador(ArrayList lista,String fec,String user) {
        try{
            int numdatos = lista.size();
            if(numdatos>0){
                cdao.guardarBorrador(lista, user, fec);
            }
            else {
                System.out.println("No se enviaron datos para colocar como borrador");
            }

        }
        catch(Exception e){
            System.out.println("Error en el guardado de borrador: "+e.getMessage());
            e.printStackTrace();
        }
    }


    public void borrarDetalles(String accion){
        try{
           cdao.borrarDetalles(accion);
        }
        catch(Exception e){
            System.out.println("Error en anular detalles de cotizacion: "+e.getMessage());
            e.printStackTrace();
        }
    }

    public boolean buscarBorrador(String accion){
        boolean hay = false;
        try{
            hay = cdao.buscarBorrador(accion);
        }
        catch(Exception e){
            System.out.println("Error buscando borradores: "+e.getMessage());
            e.printStackTrace();
        }
        return hay;
    }

    public String nombreContratista(String idaccion){
        String ncont="";
        try{
           ncont = cdao.nombreContratista(idaccion);
        }
        catch(Exception e){
            System.out.println("Error en buscar nombre contratista: "+e.getMessage());
            e.printStackTrace();
        }
        return ncont;
    }


}
