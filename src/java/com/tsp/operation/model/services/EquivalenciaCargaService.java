/************************************************************************************
 * Nombre clase: EquivalenciaCargaService.java                                      *
 * Descripci�n: Clase que maneja los servicios al model relacionados con los        *
 *              equivalencia de carga                                               *
 * Autor: Ing. Jose de la rosa                                                      *
 * Fecha: 12 de enero de 2007, 11:18 AM                                             *
 * Versi�n: Java 1.0                                                                *
 * Copyright: Fintravalores S.A. S.A.                                          *
 ***********************************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  EQUIPO26
 */
public class EquivalenciaCargaService {
    
    private EquivalenciaCargaDAO equivalencia;
    
    /** Creates a new instance of EquivalenciaCargaService */
    public EquivalenciaCargaService() {
        this.equivalencia = new EquivalenciaCargaDAO ();
    }
    
    /**
     * Getter for property equivalencia.
     * @return Value of property equivalencia.
     */
    public EquivalenciaCarga getEquivalencia() {
        return this.equivalencia.getEquivalencia();
    }
    
    /**
     * Setter for property equivalencia.
     * @param equivalencia New value of property equivalencia.
     */
    public void setEquivalencia(EquivalenciaCarga equivalencia) {
        this.equivalencia.setEquivalencia(equivalencia);
    }
    
    /**
     * Getter for property Vecequivalencia.
     * @return Value of property Vecequivalencia.
     */
    public Vector getVecequivalencia() {
        return this.equivalencia.getVecequivalencia();
    }
    
    /**
     * Setter for property Vecequivalencia.
     * @param Vecequivalencia New value of property Vecequivalencia.
     */
    public void setVecequivalencia(Vector Vecequivalencia) {
        this.equivalencia.setVecequivalencia(Vecequivalencia);
    }
    
    /**
     * Metodo: insertEquivanlenciaCarga, permite ingresar un registro a la tabla equivalencia_carga.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void insertEquivalenciaCarga ( EquivalenciaCarga equivalencia ) throws SQLException{
        setEquivalencia(equivalencia);
        this.equivalencia.insertEquivalenciaCarga();
    }
    
    /**
     * Metodo: searchEquivalenciaCarga, permite buscar una equivalencia de carga dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : codigo y distrito
     * @version : 1.0
     */
    public void searchEquivalenciaCarga ( String codigo, String distrito )throws SQLException{
        this.equivalencia.searchEquivalenciaCarga(codigo, distrito);
    }
    
    /**
     * Metodo: existEquivalenciaCarga, permite buscar una equivalencia de carga dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : codigo y distrito.
     * @version : 1.0
     */
    public boolean existEquivalenciaCarga ( String codigo, String distrito ) throws SQLException{
        return this.equivalencia.existEquivalenciaCarga(codigo, distrito);
    }
    
    /**
     * Metodo: listEquivalenciaCarga, permite listar todas los esquemas de equivalencia
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void listEquivalenciaCarga (String distrito, String codigo, String car_legal, String tipo_carga)throws SQLException{
        this.equivalencia.listEquivalenciaCarga(distrito, codigo, car_legal, tipo_carga);
    }
    
    /**
     * Metodo: updateEquivalenciaCarga, permite actualizar un esquema de equivalencia dados unos parametros.
     * @autor : Ing. Jose de la rosa
     * @param : 
     * @version : 1.0
     */
    public void updateEquivalenciaCarga ( EquivalenciaCarga equivalencia ) throws SQLException{
        setEquivalencia(equivalencia);
        this.equivalencia.updateEquivalenciaCarga();
    }
    
     /**
     * Metodo: anularEquivalenciaCarga, permite anular un esquema de equivalencia dados unos parametros.
     * @autor : Ing. Jose de la rosa
     * @param : 
     * @version : 1.0
     */
    public void anularEquivalenciaCarga ( EquivalenciaCarga equivalencia ) throws SQLException{
        setEquivalencia(equivalencia);
        this.equivalencia.anularEquivalenciaCarga();
    }
}
