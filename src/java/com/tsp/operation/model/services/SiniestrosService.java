/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.SiniestrosDAO;
import com.tsp.operation.model.beans.Siniestros;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author jpinedo
 */
public class SiniestrosService {


        SiniestrosDAO fenalcoDao;
        
        public SiniestrosService() {
            fenalcoDao = new SiniestrosDAO();
        }
        public SiniestrosService(String dataBaseName) {
            fenalcoDao = new SiniestrosDAO(dataBaseName);
        }



     public boolean  Validar_Siniestros_Activos(String id) throws Exception
     {
         boolean estado=false;
         boolean estado_s=fenalcoDao.Validar_Siniestros_Activos_1(id);        
         if(estado_s)
         {
           estado=estado_s;
         }
         else
         {
             estado_s=fenalcoDao.Validar_Siniestros_Activos_2(id);
             if(estado_s)
             {
                 estado=estado_s;
             }
             else
             {
                 estado=fenalcoDao.Validar_Siniestros_Activos_3(id);
             }
         }
     return estado;
     }



       /**
     * M�todo que lista siniestros enviados
     * @autor.......jpinedo
     * @throws......Exception
     * @version.....1.0.
     **/
    public  ArrayList< Siniestros> ListarSiniestro( String id) throws Exception{
        ArrayList lista = new ArrayList();
        try
        {
           lista   = fenalcoDao.ListarSiniestro(id);
        }catch(Exception e){
            throw new Exception( "listarsiniestros " + e.getMessage());
        }

        return lista;

    }
    
    /**
     * M�todo que consulta si una cedula esta reportada y trae el motivo del reporte
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String  cedulaReportada( String cedula) throws Exception{
        String reporte= "";
                try
        {
           reporte   = fenalcoDao.cedulaReportada(cedula);
        }catch(Exception e){
            throw new Exception( "cedulaReportada " + e.getMessage());
        }
        return reporte;
    }

    /**
     * M�todo que consulta si una cedula esta reportada y trae el motivo del reporte
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String  cuentaReportada( String cuenta,String banco) throws Exception{
        String reporte= "";
                try
        {
           reporte   = fenalcoDao.cuentaReportada(cuenta, banco);
        }catch(Exception e){
            throw new Exception( "cedulaReportada " + e.getMessage());
        }
        return reporte;
    }


}
