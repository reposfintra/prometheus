/*
 * ProcesoCorficolombianaService.java
 *
 * Created on 28 de julio de 2008, 10:56 AM
 */
package com.tsp.operation.model.services;

import com.tsp.finanzas.contab.model.DAO.ComprobantesDAO;
import com.tsp.finanzas.contab.model.DAO.ContabilizacionNegociosDAO;
import com.tsp.finanzas.contab.model.DAO.MovAuxiliarDAO;
import com.tsp.finanzas.contab.model.beans.Comprobantes;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.Convenio;
import com.tsp.operation.model.beans.Usuario;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author  tmolina
 */
public class ProcesoCorficolombianaService {

    ProcesoCorficolombianaDAO procesoCorficolombianaDAO;
    private ComprobantesDAO             comproDAO;
    private ContabilizacionNegociosDAO  dao;
    private  MovAuxiliarDAO              movDAO;
    /** Creates a new instance of ProcesoCorficolombianaService */
    
//    public ProcesoCorficolombianaService() {
//        procesoCorficolombianaDAO = new ProcesoCorficolombianaDAO();
//        dao            = new  ContabilizacionNegociosDAO();
//        comproDAO      = new  ComprobantesDAO();
//        movDAO         = new  MovAuxiliarDAO();
//    }
//    
    public ProcesoCorficolombianaService(String dataBaseName) {
         procesoCorficolombianaDAO = new ProcesoCorficolombianaDAO(dataBaseName);
         dao            = new  ContabilizacionNegociosDAO(dataBaseName);
         comproDAO      = new  ComprobantesDAO(dataBaseName);
         movDAO         = new  MovAuxiliarDAO(dataBaseName);
    }
    

    public String endozarFacturas(String factura) throws java.lang.Exception {
        return procesoCorficolombianaDAO.endozarFacturas(factura);
    }

    public Comprobantes buscarNegocio(String negocio, String dstrct) throws java.lang.Exception {
        return procesoCorficolombianaDAO.buscarNegocio(negocio, dstrct);
    }

    public boolean existeComprobante(String negocio) throws java.lang.Exception {
        return procesoCorficolombianaDAO.existeComprobante(negocio);
    }

    /**
     * M�todo que inserta el comprobante y comprodet en caso de error se devuelve la lista vacia
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     * 
     **/
    public  ArrayList<String>  insertar(Comprobantes comprobante, String user) throws Exception {
        String error="";
        ArrayList<String> sql=new ArrayList<>();
        try {
            error = evaluarComprobante(comprobante);
            if (error.equals("")) {
                List items = comprobante.getItems();
                List aux = new LinkedList();
                int sw = 0;
                // Validamos los items:
                if (items != null) {
                    for (int j = 0; j < items.size(); j++) {
                        Comprobantes comprodet = (Comprobantes) items.get(j);
                        String evaDet = evaluarComprobante(comprodet);
                        if (evaDet.equals("")) {
                            aux.add(comprodet);
                        } else {
                            error="Error item "+j+" "+evaDet;   // Guardamos el item
                            sw = 1;
                            break;
                        }
                    }
                }
                if (sw == 0) {
                    items = aux;  // Items q cumplen para ser insertados
                    if (items != null && items.size() > 0) {
                        // Tomamos la secuencia:
                        int sec = comproDAO.getGrupoTransaccion();
                        comprobante.setGrupo_transaccion(sec);
                        sql.add(dao.InsertComprobante(comprobante, user));
                        ArrayList listaQuerys = dao.InsertComprobanteDetalle2(items, sec, user);
                        Iterator i = listaQuerys.iterator();
                        while (i.hasNext()) {
                            String query = (String) i.next();
                            sql.add(query);
                        }

                    } else {
                        error="Error No presenta items validos";
                    }
                }
            }else{
                error="Error"+error;
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(" INSERTAR COMPROBANTE NEGOCIOS : " + e.getMessage());
        }
         return sql;
    }

    /**
     * M�todo que evalua datos v�lidos del comprobante y comprodet
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String evaluarComprobante(Comprobantes comprobante) throws Exception {
        String msj = "";
        try {
            //System.out.println("Evalua");
            // Campos comunes:
            if (comprobante.getDstrct().trim().equals("")) {
                msj += " No presenta distrito";
            }
            if (comprobante.getTipodoc().trim().equals("")) {
                msj += " No presenta tipo de documento";
            }
            if (comprobante.getNumdoc().trim().equals("")) {
                msj += " No presenta n�mero de documento";
            }
            if (comprobante.getPeriodo().trim().equals("")) {
                msj += " No presenta periodo de contabilizaci�n";
            }
            if (comprobante.getDetalle().trim().equals("")) {
                msj += " No presenta una descripci�n o detalle";
            }
            if (comprobante.getBase().trim().equals("")) {
                msj += " No presenta base";
            }


            String tipo = comprobante.getTipo();

            if (tipo.equals("C")) {

                if (comprobante.getTotal_debito() != comprobante.getTotal_credito()) {
                    msj += " Est� descuadrado en valores debitos y cr�ditos";
                }
                if (comprobante.getSucursal().trim().equals("")) {
                    msj += " No presenta sucursal";
                }
                if (comprobante.getFechadoc().trim().equals("")) {
                    msj += " No presenta fecha de documento";
                }
                if (comprobante.getTercero().trim().equals("")) {
                    msj += " No presenta tercero";
                }
                if (comprobante.getMoneda().trim().equals("")) {
                    msj += " No presenta moneda";
                }
                if (comprobante.getAprobador().trim().equals("")) {
                    msj += " No presenta aprobador";
                }

            } else {

                if (comprobante.getCuenta().trim().equals("")) {
                    msj += " No presenta cuenta";
                } else {
                    String cta = comprobante.getCuenta();
                    Hashtable cuenta = movDAO.getCuenta(comprobante.getDstrct(), cta);

                    if (cuenta != null) {

                        String requiereAux = (String) cuenta.get("auxiliar");
                        String requiereTercero = (String) cuenta.get("tercero");

                        // Tercero.
                        if (requiereTercero.equals("S") && comprobante.getTercero().trim().equals("")) {
                            msj += " La cuenta " + cta + " requiere tercero  y el item no presenta tercero";
                        }

                        // Auxiliar.
                        if (requiereAux.equals("S")) {

                            if (comprobante.getAuxiliar().trim().equals("")) {
                                msj += " La cuenta " + cta + " requiere auxiliar y el item no presenta auxiliar";
                            }

                            if (!dao.existeCuentaSubledger(comprobante.getDstrct(), cta, comprobante.getAuxiliar())) {
                                msj += "La cuenta " + cta + " no tiene el subledger " + comprobante.getAuxiliar() + " asociado";
                            }
                        }

                    } else {
                        msj += " La cuenta " + cta + " no existe o no est� activa";
                    }

                }

            }

            System.out.println("Msg" + msj);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
        return msj;
    }

    public String activarProcesoIntereses(String prefix_dif_fid, String dif_fid, String dif, String negocio, String usuario) throws java.lang.Exception {
        return procesoCorficolombianaDAO.activarProcesoIntereses(prefix_dif_fid, dif_fid, dif, negocio, usuario);
    }

     public String anularProcesoIntereses( String dif, String negocio, String usuario) throws java.lang.Exception {
        return procesoCorficolombianaDAO.anularProcesoIntereses( dif, negocio, usuario);
    }

    public ArrayList<String> comprobantesReversion(String negocio, Convenio conv, Usuario usuario, String nit_fiducia) throws java.lang.Exception {
         return procesoCorficolombianaDAO.comprobantesReversion(negocio, conv, usuario, nit_fiducia);
     }


}
