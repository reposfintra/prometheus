/***************************************
 * Nombre Clase ............. OpService.java
 * Descripci�n  .. . . . . .  Permite Generar las OPs
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  18/10/2005
 * versi�n . . . . . . . . .  1.0
 * Copyright ...Transportes Sanchez Polo S.A.
 *******************************************/



package com.tsp.operation.model.services;



import java.io.*;
import java.util.*;
import com.tsp.util.Util;
import com.tsp.util.Utility;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
// Excel
import com.tsp.operation.model.beans.POIWrite;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;


//Log de Errores:
import com.tsp.util.LogWriter;



public class OpService {
    
    private Object objeto;
    private OpDAO              OpDataAccess;
    private List               listaOP;
    private LiquidarOCService  LiqSvc;
    private POIWrite           Excel;
    // Excel:
    private HSSFCellStyle      texto;
    private HSSFCellStyle      numero;
    private HSSFCellStyle      titulo;
    private HSSFCellStyle      cabecera;
    
    private int                fila;
    private int                columna;
    private int                filaItem;
    private int                columnaItem;
    private int                swfila;
    private int                swfilaItem;
    private String             hojaOP;
    private String             hojaITEM;
    
    private List OCsinP;      // sescalante
    private List hc;          // Ivan Gomez
    
    
    private LogWriter logWriter; 
    
   
    private  FileWriter        fw;
    private  BufferedWriter    bf;
    private  PrintWriter       linea;
    
    private String  TIPO_CREDITO     = "035";
    private String  TIPO_DEBITO      = "036";
    
    
    
    public  final String  PRODENECIA_OP = "GRABACION";
    
    
    
    
    public OpService() {
        OpDataAccess = new OpDAO();
        LiqSvc       = new  LiquidarOCService();
        listaOP      = null;
        OCsinP       = null; //sescalante
    }
    public OpService(String dataBaseName) {
        OpDataAccess = new OpDAO(dataBaseName);
        LiqSvc       = new  LiquidarOCService();
        listaOP      = null;
        OCsinP       = null; //sescalante
    }
    
    
    
    
    /**
     * M�todo que setea el url para generar el archivo de Error
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void initFile(String url)throws Exception{
        try{            
             this.fw         = new FileWriter    ( url      );
             this.bf         = new BufferedWriter( this.fw  );
             this.linea      = new PrintWriter   ( this.bf  );
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
    public void cerrar(){
         this.linea.close();  
    }
    
    
    
    /**
     * M�todo generateOP, genera una OP
     * @autor   fvillacob
     * @param   Fecha Inicial, Fecha Final (String)
     * @throws  Exception
     * @version 1.0.
     **/
    public void generateOP(String fechaIni, String fechaFin,String HC, String distrito) throws Exception{
        try{
            
         //obtienen OCs para generar OP
            listaOP      = this.OpDataAccess.getOP(fechaIni,fechaFin, HC, distrito);  
            
        }catch(Exception e){
            throw new Exception( " generateOP: " +e.getMessage());
        }
    }
    
    
    
    
    
    /**
     * M�todo getValorMoneda, realiza el proceso de convesion de un valor de una moneda a otra
     *  y define el valor de la tasa
     * @autor   fvillacob
     * @param   moneda original del valor (String), valor (double), moneda a convertir (string), fecha de creacion dela tasa (String), distrito de la OP (String)
     * @return  double
     * @throws  Exception
     * @version 1.0.
     **/
    public double getValorMoneda(String moneda, double valorMoneda, String monedaTo, String fecha, String dstrct)throws Exception{
        double valor=0;
        try{
            valor      = this.OpDataAccess.getValorMoneda(moneda, valorMoneda, monedaTo, fecha, dstrct);
        }catch(Exception e){ throw new Exception(e.getMessage());}
        return valor;
    }
    
    
    
    
    /**
     * M�todo getOP, instacia listaOP
     * @autor   fvillacob
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    public  List getOP(){
        return this.listaOP;
    }
    
    
    
    
    
    
    
    
    /**
     * M�todo insertList, registra la OP y define el archivo excel para control de generacion de OP
     * @autor   fvillacob
     * @param lista de OP (List), usuario, ruta (String)
     * @throws  Exception
     * @version 1.0.
     **/
    public String  insertOP(OP  op, Usuario user) throws Exception{
              
            String comentario = "";
                        
            try{
                                                 
                     String planilla =  op.getOc();
                     String dis      =  op.getDstrct();
                     
                     if( op.getComentario().equals("") ){
                            
                           TransaccionService tService = new TransaccionService(user.getBd());
                           String       procedencia    = "OP";  
                            
                            
                           String SQL = "";  // SQL a ejecutar
                           int    sw  = 0;   // Control de Error.
                         
                           
                        // GENERAMOS SQL  A EJECUTAR:   
                           try{
                               
                                // Cabecera OP:   
                                   SQL  += OpDataAccess.insertOP(op, user.getLogin());       // Cabecera.


                                 // Items:
                                    List items  = op.getItem();
                                    if( items!=null  &&  items.size()>0  )
                                        for( int i=0; i<items.size(); i++ ){
                                            OPItems item = (OPItems) items.get(i);                                                
                                            SQL += OpDataAccess.insertItem( item, user.getLogin()); // Detalles


                                             // Impuestos de Detalle
                                                List impuestos = item.getImpuestos();
                                                if( impuestos!=null && impuestos.size()>0)
                                                    for(int j=0; j<impuestos.size(); j++){
                                                          OPImpuestos imp  = (OPImpuestos)impuestos.get(j);                                    

                                                          double vlr_imp      = 0;
                                                          double vlr_me_imp   = 0;

                                                          if(imp.getTipo().equals("RFTE") ){
                                                             vlr_imp     =  item.getVlrReteFuente();
                                                             vlr_me_imp  =  item.getVlrReteFuente_ME();
                                                         }
                                                         if(imp.getTipo().equals("RICA") ){
                                                            vlr_imp     = item.getVlrReteIca();
                                                            vlr_me_imp  = item.getVlrReteIca_ME();
                                                         }
                                                         SQL += OpDataAccess.insertImpItem( item.getDstrct(), item.getProveedor(), item.getTipo_documento(), item.getDocumento(), item.getItem(), imp.getCodigo(), imp.getPorcentaje(), vlr_imp , vlr_me_imp , user.getLogin(), item.getBase());

                                                    }

                                        }


                                     // Movpla, Planilla: actualizar  con la factura.                                   
                                        SQL += OpDataAccess.updateFactura( op, procedencia );
                                        

                           }catch(Exception kk){
                                sw=1;
                                comentario += "ERROR AL CREAR SQL. OC: "+  op.getOc() + " CAUSA : "+ kk.getMessage() ;
                           }
                               
                           
                           
                            
                            
                         // EJECUTAMOS EL SQL:
                            try{       
                                
                                  tService.crearStatement();
                                  tService.getSt().addBatch(SQL);  // Cabecera, Detalle, Impuesto detalle
                                  tService.execute();
                                  
                               // Impuestos por cabecera:
                                  try{
                                      
                                       SQL = OpDataAccess.searchImpByDoc(op, user.getLogin());
                                       tService.crearStatement();
                                       tService.getSt().addBatch(SQL);
                                       tService.execute();
                                       
                                   }catch(Exception exSQL){
                                       comentario += "ERROR IMPUESTOS CABECERA "+ exSQL.getMessage() ;           
                                   }
                                  
                                  
                            }catch(Exception exSQL){
                                   sw=1;
                                   comentario += "ERROR AL INSERTAR OP. OC: "+  op.getOc() + " CAUSA : "+ exSQL.getMessage() ; 
                            }     
                                
                                
                           
                            if(  sw == 0)
                                 comentario = "OC : " + op.getOc() +" PROPIETARIO: "+   op.getProveedor()   + " OP : " + op.getDocumento() + " EXITOSO." ;
                            
                            else
                             // Eliminamos reg. de descuentos  
                              this.OpDataAccess.deleteDescMmto(dis, planilla);
                           
                                                           
                            
                     }else{
                          comentario += "OP NO GENERADA. OC : "+ op.getOc() + " CAUSA : "+ op.getComentario();
                       // Eliminamos reg. de descuentos    
                           this.OpDataAccess.deleteDescMmto(dis, planilla);
                     }
                      
                      
                      
            
            }catch(Exception e){
                throw new Exception( " insertOP: " + e.getMessage() );
            }
            
            
             return comentario;
            
    }
    
    
    
    
    
    
    
    
    /**
     * M�todo insertList, registra la OP y define el archivo excel para control de generacion de OP
     * @autor   fvillacob
     * @param lista de OP (List), usuario, ruta (String)
     * @throws  Exception
     * @version 1.0.
     **/
    public void insertList(List lista, Usuario user, String url, String urlError, String procedencia) throws Exception{
                  
            try{
                
                       if( procedencia==null || procedencia.equals("") )
                           procedencia = "OP";
                
                       
                    // Excel para mostrar las OP generadas:
                        this.Excel  = new POIWrite(url);
                        this.configXLS();

                     // TXT de Errores producidos:
                        initFile(urlError);
                        linea.println("INICIO PROCESOS FACTURACION DE OPS   ");
                        linea.println("ERRORES ENCONTRADOS                  ");
                        linea.println("__________________________________________________________________________________________________________");
                 
                        
                     // GENERAMOS SQL:                            
                        
                        if(lista!=null  && lista.size()>0){
                            
                            TransaccionService tService = new TransaccionService(user.getBd());
                            
                            Iterator it = lista.iterator();
                            while(it.hasNext()){
                                OP     op   = (OP)it.next();
                                String msj  = op.getComentario();

                                if(msj.equals("") ){
                                        
                                       String sql  = OpDataAccess.insertOP(op, user.getLogin());
                                      //Grabamos la OP al xls
                                        this.addOPExcel(op);

                                        //ITEMS DE LA OP
                                        List items  = op.getItem();
                                        if( items!=null && items.size()>0 ){
                                            for(int i=0;i<items.size();i++){
                                                OPItems item = (OPItems) items.get(i);
                                                
                                                // Insertamos el Item:
                                                sql += OpDataAccess.insertItem( item, user.getLogin()); 

                                                // Grabamos al archivo  el item de la op
                                                addItemOPExcel(item );
                                                incItem();

                                                // Insertamos sus Impuestos
                                                List impuestos = item.getImpuestos();
                                                if(impuestos!=null && impuestos.size()>0){
                                                    for(int j=0;j<impuestos.size();j++){
                                                        OPImpuestos imp  = (OPImpuestos)impuestos.get(j);                                    

                                                        double vlr_imp      = 0;
                                                        double vlr_me_imp   = 0;

                                                        if(imp.getTipo().equals("RFTE") ){
                                                            vlr_imp     =  item.getVlrReteFuente();
                                                            vlr_me_imp  =  item.getVlrReteFuente_ME();
                                                        }
                                                        if(imp.getTipo().equals("RICA") ){
                                                            vlr_imp     = item.getVlrReteIca();
                                                            vlr_me_imp  = item.getVlrReteIca_ME();
                                                        }

                                                        sql += OpDataAccess.insertImpItem( item.getDstrct(), item.getProveedor(), item.getTipo_documento(), item.getDocumento(), item.getItem(), imp.getCodigo(), imp.getPorcentaje(), vlr_imp , vlr_me_imp , user.getLogin(), item.getBase());
                                                    }
                                                }
                                            }
                                        }
                                        
                                     // ACTUALIZAR PLANILLA, MOVPLA: la asignamos el numero de factura op correspondiente                                      
                                        sql += OpDataAccess.updateFactura( op, procedencia );
                                       
                                  
                                            
                                     // AFECTAMOS SALDO FACTURA RELACIONADA:
                                        if( procedencia.equals("ADICION") ){
                                            if( op.getTipo_documento().equals( TIPO_CREDITO  ) ||   op.getTipo_documento().equals( TIPO_DEBITO )    ){                                                
                                                sql += OpDataAccess.UpdateCXP_Saldos( op, user.getLogin() );                                                
                                            }
                                        }
                                        
                                        
                                        
                                    // EJECUTAMOS SQL
                                        
                                        int sw = 0;
                                        //1. Cabecera, Items, Impuestos de Items, update de planilla:
                                        try{
                                    
                                            tService.crearStatement();
                                            tService.getSt().addBatch(sql);
                                            tService.execute();

                                        }catch(Exception exSQL){
                                            linea.println( "ERROR EJECUTANDO SQL FACTURA  PARA LA OC : " + op.getOc()  );
                                            linea.println( exSQL.getMessage()     );
                                            sw=1;
                                        }   
                                        
                                        
                                        
                                       // 2. Impuiestos de cabecera:
                                          if(sw==0){
                                               sql = OpDataAccess.searchImpByDoc(op, user.getLogin());
                                               try{
                                    
                                                   tService.crearStatement();
                                                   tService.getSt().addBatch(sql);
                                                   tService.execute();

                                                }catch(Exception exSQL){
                                                    linea.println( "ERROR NO GRAVE EJECUTANDO SQL IMP FACTURA  PARA LA OC : " + op.getOc()  );
                                                    linea.println( exSQL.getMessage()     );
                                                }  
                                          }
                                        
                                        
                                        
                                        
                                        
                                        // Incremento fila de Excel
                                        inc();
                                }   
                                else
                                    linea.println("OC : "+ op.getOc() + " CAUSA : "+ op.getComentario() );

                            }


                        }else{
                            sinDatos();
                            linea.println("Lista de planillas vacia....");
                        }
                        
                        
                        
            
            }catch(Exception e){
                linea.println("ERROR EJECUCION : ");
                linea.println( e.getMessage()     );
                throw new Exception( " insertar Ops: " + e.getMessage() );
            }
            finally{
                 linea.println("__________________________________________________________________________________________________________");
                 linea.println("FIN ERRORES                       ");
                 Excel.cerrarLibro();
                 cerrar();
            }
            
            
            
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // EXCEL:
    
    /**
     * M�todo que envia mensaje al usuario que no hay datos
     * @autor   fvillacob
     * @throws  Exception
     * @version 1.0.
     **/
    public void sinDatos() throws Exception{
        try{
            this.Excel.obtenerHoja( hojaOP   );
            Excel.adicionarCelda( this.fila +1 , this.columna, "No hay planillas con datos correctos y validos para generar OP...",     this.texto);
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
    /**
     * M�todo que configura parametros de Excel
     * @autor   fvillacob
     * @throws  Exception
     * @version 1.0.
     **/
    public void configXLS() throws Exception{
        try{
            this.hojaOP       = "OP";
            this.hojaITEM     = "ITEM";
            this.Excel.obtenerHoja( hojaOP   );
            this.Excel.obtenerHoja( hojaITEM );
            createStyle();
            
            this.swfila        = 1;
            this.swfilaItem    = 1;
            this.fila          = 0;
            this.columna       = 0;
            header();
            
            this.filaItem      = 0;
            this.columnaItem   = 0;
            this.headerItem();
            
            inc();
            incItem();
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    /**
     * M�todo que Define estilo del archivo xls
     * @autor   fvillacob
     * @throws  Exception
     * @version 1.0.
     **/
    public void createStyle()throws Exception{
        try{
            this.texto     = Excel.nuevoEstilo("Book Antiqua", 9,  false , false, "text"        , Excel.NONE , Excel.NONE , Excel.NONE);
            this.cabecera  = Excel.nuevoEstilo("Book Antiqua", 12, true ,  false, "text"        , Excel.NONE , Excel.NONE , Excel.NONE);
            this.numero    = Excel.nuevoEstilo("Book Antiqua", 9,  false , false, ""            , Excel.NONE , Excel.NONE , Excel.NONE);
            this.titulo    = Excel.nuevoEstilo("Book Antiqua", 9,  true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.LIGHT_ORANGE.index , HSSFCellStyle.ALIGN_CENTER);
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
    /**
     * M�todo que establece titulos xls
     * @autor   fvillacob
     * @throws  Exception
     * @version 1.0.
     **/
    public void header()throws Exception{
        try{
            
            this.Excel.obtenerHoja(hojaOP);
            String fecha =  Util.getFechaActual_String(6);
            
            Excel.adicionarCelda( 0, 0, "TRANSPORTES SANCHEZ POLO",     this.cabecera);
            Excel.adicionarCelda( 1, 0, "GENERACION DE OPs",            this.cabecera);
            Excel.adicionarCelda( 2, 0, fecha,                          this.cabecera);
            
            this.fila    = 4;
            this.columna = 0;
            
            Excel.adicionarCelda( this.fila, this.columna, "DISTRITO",          this.titulo); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, "PLANILLA",          this.titulo); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, "ORIGEN",            this.titulo); columna+=1;            
            Excel.adicionarCelda( this.fila, this.columna, "PLACA",             this.titulo); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, "PROVEEDOR",         this.titulo); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, "FECHA CUMPLIDO",    this.titulo); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, "BANCO",             this.titulo); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, "SUCURSAL",          this.titulo); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, "OP",                this.titulo); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, "DESCRIPCION",       this.titulo); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, "FECHA FACTURA",     this.titulo); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, "FECHA VENCIMIENTO", this.titulo); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, "VALOR OC",          this.titulo); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, "VALOR MOV",         this.titulo); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, "VALOR NETO",        this.titulo); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, "VALOR NETO ME",     this.titulo); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, "MONEDA",            this.titulo);
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
    /**
     * M�todo que define titulo de item
     * @autor   fvillacob
     * @throws  Exception
     * @version 1.0.
     **/
    public void headerItem()throws Exception{
        try{
            
            this.Excel.obtenerHoja(hojaITEM);
            Excel.adicionarCelda( this.filaItem, this.columnaItem, "DISTRITO",          this.titulo); columnaItem+=1;
            Excel.adicionarCelda( this.filaItem, this.columnaItem, "OP",                this.titulo); columnaItem+=1;
            Excel.adicionarCelda( this.filaItem, this.columnaItem, "ITEM",              this.titulo); columnaItem+=1;
            Excel.adicionarCelda( this.filaItem, this.columnaItem, "DESCRIPCION",       this.titulo); columnaItem+=1;
            Excel.adicionarCelda( this.filaItem, this.columnaItem, "ASIGNADOR",         this.titulo); columnaItem+=1;
            Excel.adicionarCelda( this.filaItem, this.columnaItem, "INDICADOR",         this.titulo); columnaItem+=1;
            Excel.adicionarCelda( this.filaItem, this.columnaItem, "VALOR",             this.titulo); columnaItem+=1;
            Excel.adicionarCelda( this.filaItem, this.columnaItem, "VALOR ME",          this.titulo); columnaItem+=1;
            Excel.adicionarCelda( this.filaItem, this.columnaItem, "CUENTA",            this.titulo); columnaItem+=1;
            Excel.adicionarCelda( this.filaItem, this.columnaItem, "ABC",               this.titulo); columnaItem+=1;
            Excel.adicionarCelda( this.filaItem, this.columnaItem, "RFTE",              this.titulo); columnaItem+=1;
            Excel.adicionarCelda( this.filaItem, this.columnaItem, "RICA",              this.titulo); columnaItem+=1;
            Excel.adicionarCelda( this.filaItem, this.columnaItem, "NETO",              this.titulo);
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
    
    /**
     * M�todo que Adiciona fila al archivo xls
     * @autor   fvillacob
     * @throws  Exception
     * @version 1.0.
     **/
    public void  addOPExcel(OP  op)throws Exception{
        try{
            
            if(fila>=64000){
                fila    = 0;
                columna = 0;
                this.swfila++;
                hojaOP = "OP"+ String.valueOf( this.swfila );
                header();
            }
            
            this.Excel.obtenerHoja(hojaOP);
            Excel.adicionarCelda( this.fila, this.columna, op.getDstrct(),            this.texto); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, op.getOc(),                this.texto); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, op.getOrigenOC(),          this.texto); columna+=1;            
            Excel.adicionarCelda( this.fila, this.columna, op.getPlaca(),             this.texto); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, op.getProveedor(),         this.texto); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, op.getFechaCumplido(),     this.texto); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, op.getBanco(),             this.texto); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, op.getSucursal(),          this.texto); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, op.getDocumento(),         this.texto); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, op.getDescripcion(),       this.texto); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, op.getFecha_documento(),   this.texto); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, op.getFecha_vencimiento(), this.texto); columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, op.getVlrOcNeto(),         this.numero);columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, op.getVlrMov(),            this.numero);columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, op.getVlrNeto(),           this.numero);columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, op.getVlr_neto_me(),       this.numero);columna+=1;
            Excel.adicionarCelda( this.fila, this.columna, op.getMoneda(),            this.texto);
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
    /**
     * M�todo que Adiciona fila de item
     * @autor   fvillacob
     * @throws  Exception
     * @version 1.0.
     **/
    public void  addItemOPExcel(OPItems item )throws Exception{
        try{
            
            if(filaItem>=64000){
                filaItem    = 0;
                columnaItem = 0;
                this.swfilaItem++;
                hojaITEM    ="ITEM" + String.valueOf( this.swfilaItem );
                headerItem();
            }
            
            this.Excel.obtenerHoja(hojaITEM);
            Excel.adicionarCelda( this.filaItem, this.columnaItem,   item.getDstrct()       ,  this.texto); columnaItem+=1;
            Excel.adicionarCelda( this.filaItem, this.columnaItem,   item.getDocumento()    ,  this.texto); columnaItem+=1;
            Excel.adicionarCelda( this.filaItem, this.columnaItem,   item.getItem()         ,  this.texto); columnaItem+=1;
            Excel.adicionarCelda( this.filaItem, this.columnaItem,   item.getDescconcepto() ,  this.texto); columnaItem+=1;//sescalante
            Excel.adicionarCelda( this.filaItem, this.columnaItem,   item.getAsignador()    ,  this.texto); columnaItem+=1;
            Excel.adicionarCelda( this.filaItem, this.columnaItem,   item.getIndicador()    ,  this.texto); columnaItem+=1;
            Excel.adicionarCelda( this.filaItem, this.columnaItem,   item.getVlr()          ,  this.numero); columnaItem+=1;
            Excel.adicionarCelda( this.filaItem, this.columnaItem,   item.getVlr_me()       ,  this.numero); columnaItem+=1;
            Excel.adicionarCelda( this.filaItem, this.columnaItem,   item.getCodigo_cuenta(),  this.texto); columnaItem+=1;
            Excel.adicionarCelda( this.filaItem, this.columnaItem,   item.getCodigo_abc()   ,  this.texto); columnaItem+=1;
            Excel.adicionarCelda( this.filaItem, this.columnaItem,   item.getVlrReteFuente(),  this.numero); columnaItem+=1;
            Excel.adicionarCelda( this.filaItem, this.columnaItem,   item.getVlrReteIca()   ,  this.numero); columnaItem+=1;
            Excel.adicionarCelda( this.filaItem, this.columnaItem,   (  item.getVlr()   -  item.getVlrReteFuente() -  item.getVlrReteIca() ) ,  this.numero);
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    /**
     * M�todo que incrementa fila para la hoja de op
     * @autor   fvillacob
     * @version 1.0.
     **/
    public void inc(){
        this.fila++;
        this.columna=0;
    }
    
    
    /**
     * M�todo que incrementa fila para la hoja de item de la op
     * @autor   fvillacob
     * @version 1.0.
     **/
    public void incItem(){
        this.filaItem++;
        this.columnaItem=0;
    }
    
    /**
     * M�todo getOPsinProvRegistrado, obtiene las planillas cuyo nitpro no se encuentra registrado en proveedor
     * @autor   fvillacob
     * @see getOPsinProvRegistrado(fechaIni, fechaFin)
     * @param   Fecha inicial, fecha final (String)
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    private List provs = null;
     public List getOPsinProvRegistrado(String fechaIni, String fechaFin) throws Exception{
         List lista = this.OpDataAccess.getOPsinProvRegistrado(fechaIni, fechaFin);
          if (lista != null && lista.size()> 0){
                //exclusion de proveedores repetidos
                this.setProvs();
                provs = this.getExcluirRepetidos(lista);
            }
         return lista;
     }
     
     /**
     * M�todo getExcluirRepetidos, excluye proveedores repetidos de la lista
     * @autor sesclanate     
     * @param lista de planillas y proveedores no registrados en sistemas
     * @return List
     * @throws Exception
     * @version 1.0.
     **/
    public List getExcluirRepetidos(List lista)throws Exception{
        List listProv = new LinkedList();
        try{
            
            if(lista!=null && lista.size()>0){
                for(int i=0;i<lista.size();i++){
                    Planilla p  = (Planilla)lista.get(i);
                    String prov  = p.getNitpro();
                    int    sw  = 0;
                    for(int j=0; j < listProv.size(); j++){
                        Planilla pl  = (Planilla)listProv.get(j);
                        if( pl.getNitpro().equals(prov) ) {
                            sw = 1;
                            break;
                        }
                    }
                    if(sw==0)
                        listProv.add(p);
                }
            }
            
        }catch( Exception e){
            throw new Exception(" getExcluirRepetidos: "+e.getMessage());
        }
        return listProv;
    }
    
    /**
     * Getter for property provs.
     * @return Value of property provs.
     */
    public java.util.List getProvs() {
        return provs;
    }
    
    /**
     * Setter for property provs.
     * @param provs New value of property provs.
     */
    public void setProvs() {
        this.provs = new LinkedList();
    }
     /**
     * Metodo searchHC, Busca los datos de la tabla THCODE en la tabla general
     * @autor : Ing. Ivan Gomez   
     * @see : buscarListaHC  -   OpDAO
     * @version : 1.0
     */         
    public void searchHC() throws Exception {
        this.hc = OpDataAccess.buscarListaHC();  
             
    }
    
    /**
     * Getter for property hc.
     * @return Value of property hc.
     */
    public List getHc() {
        return hc;
    }
    
    /**
     * M�todo getABC, devuelve el valor ABC para la agencia
     * @autor   Osvaldo P�rez Ferrer
     * @param   agencia (String)
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    public String getABC(String agencia) throws Exception{
        return OpDataAccess.getABC(agencia);
    }
    
    /**
     * Getter for property objeto.
     * @return Value of property objeto.
     */
    public java.lang.Object getObjeto() {
        return objeto;
    }
    
    /**
     * Setter for property objeto.
     * @param objeto New value of property objeto.
     */
    public void setObjeto(java.lang.Object objeto) {
        this.objeto = objeto;
    }
    
    
    public String cambiarPropietarioOP( String new_proveedor, String usuario ) throws Exception{
        
        CXP_Doc c = new CXP_Doc();
        
        String mens = "";
        
        try{
            if( this.objeto != null ){
                Hashtable h = (Hashtable)objeto;
                
                c.setDstrct(            (String) h.get("dstrct") );
                c.setDocumento(         (String) h.get("documento") );
                c.setTipo_documento(    (String) h.get("tipo_documento") );
                c.setProveedor(         (String) h.get("proveedor") );                
                                
                CXPDocDAO d = new CXPDocDAO();
                
                c = d.ConsultarCXP_Doc(c.getDstrct(),c.getProveedor(),c.getTipo_documento(),c.getDocumento());
                
                c.setPlanilla(      (String)h.get("numpla") );
                c.setBase(          (String) h.get("base") );
                c.setCreation_user( usuario );
                                
                mens = OpDataAccess.cambiarPropietarioOP( c, new_proveedor );                                                               
            }
        }catch (Exception ex){
            mens = "No se pudo realizar el cambio de propietario, error en los datos";
            ex.printStackTrace();
            //throw new Exception("ERROR OpService.cambiarPropietarioOP "+ex.getMessage());
        }
        return mens;
    }
    
     /*********************************************************
     * metodo buscarOPPlanilla, busca la op de la planilla dada,
     *        setea un Hashtable en un Object del service
     * @author: Ing. Osvaldo P�rez Ferrer
     * @param: numero de planilla
     * @throws: en caso de un error de BD
     *********************************************************/
    public void buscarOPPlanilla( String planilla ) throws Exception{
        this.setObjeto( OpDataAccess.buscarOPPlanilla(planilla) );
    }
    
}
