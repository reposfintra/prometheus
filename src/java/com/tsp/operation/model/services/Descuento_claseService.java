/****************************************************************************************************
 * Nombre clase: Descuento_claseService.java                                                        *
 * Descripci�n: Clase que maneja los servicios al model relacionados con los                        *
 *              descuentos por clase por equipo.                                                    *
 * Autor: Ing. Jose de la rosa                                                                      *
 * Fecha: 7 de diciembre de 2005, 11:39 AM                                                          *
 * Versi�n: Java 1.0                                                                                *
 * Copyright: Fintravalores S.A. S.A.                                                          *
 ***************************************************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

public class Descuento_claseService {
    
    private Descuento_claseDAO descuento_clase;
    /** Creates a new instance of Descuento_claseService */
    public Descuento_claseService () {
        descuento_clase = new Descuento_claseDAO();
    }
    /**
     * Metodo: getDescuento_clase, permite retornar un objeto de registros de Acuerdo clase.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */    
    public Descuento_clase getDescuento_clase( )throws SQLException{
        return descuento_clase.getDescuento_clase();
    }
    
    /**
     * Metodo: getDescuento_clasees, permite retornar un vector de registros de Acuerdo clase.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */    
    public Vector getDescuento_clases()throws SQLException{
        return descuento_clase.getDescuento_clases();
    }
    
    /**
     * Metodo: setDescuento_clase, permite obtener un objeto de registros de Acuerdo clase.
     * @autor : Ing. Jose de la rosa
     * @param : objeto
     * @version : 1.0
     */
    public void setDescuento_clase (Descuento_clase descuento)throws SQLException {
        descuento_clase.setDescuento_clase(descuento);
    }
    
    /**
     * Metodo: setDescuento_clasees, permite obtener un vector de registros de Acuerdo clase.
     * @autor : Ing. Jose de la rosa
     * @param : vector
     * @version : 1.0
     */    
    public void setDescuento_clases(Vector descuento_clasees)throws SQLException{
        descuento_clase.setDescuento_clases(descuento_clasees);
    }
    
    /**
    * Metodo insertDescuento_clase, ingresa los acuerdos clasees (Descuento_clase).
    * @autor : Ing. Jose de la rosa
    * @see insertarDescuento_clase - Descuento_claseDAO
    * @param : Descuento_clase
    * @version : 1.0
    */    
    public void insertDescuento_clase(Descuento_clase user) throws SQLException{
        try{
            descuento_clase.setDescuento_clase(user);
            descuento_clase.insertDescuento_clase();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }    
    
    /**
    * Metodo existDescuento_clase, obtiene la informacion de los acuerdos clasees dado unos parametros
    * (standar, codigo).
    * @autor : Ing. Jose de la rosa
    * @see existDescuento_clase - Descuento_claseDAO
    * @param : clase del equipo
    * @version : 1.0
    */       
    public boolean existDescuento_clase(String clase, String distrito, String concepto) throws SQLException{
        try{
            return descuento_clase.existDescuento_clase(clase, distrito, concepto);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo: existeClase, permite buscar un descuento clase dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : clase del eqipo y la frecuencia.
     * @version : 1.0
     */
    public void existeClase (String clase, String frecuencia, String distrito, String placa, String cliente) throws SQLException{
        try{
            descuento_clase.existeClase(clase, frecuencia, distrito, placa, cliente);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }    
    /**
    * Metodo searchDescuento_clase, permite buscar un acuerdo clase dado unos parametros
    * (standar, codigo).
    * @autor : Ing. Jose de la rosa
    * @see searchDescuento_clase - Descuento_claseDAO
    * @param : numero del standar y el codigo de concepto
    * @version : 1.0
    */      
    public void searchDescuento_clase(String clase, String distrito, String concepto)throws SQLException{
        try{        
            descuento_clase.searchDescuento_clase(clase, distrito, concepto);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
    * Metodo listDescuento_clase, lista la informacion de los acuerdos clasees
    * @autor : Ing. Jose de la rosa
    * @see listdescuento_clase - Descuento_claseDAO
    * @param :
    * @version : 1.0
    */    
    public void listDescuento_clase(String distrito)throws SQLException{
        try{         
            descuento_clase.listDescuento_clase(distrito);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
    * Metodo updateDescuento_clase, permite actualizar un acuerdo clase dados unos parametros
    * @autor : Ing. Jose de la rosa
    * @see updateDescuento_clase - Descuento_claseDAO
    * @param : 
    * @version : 1.0
    */     
    public void updateDescuento_clase(Descuento_clase res)throws SQLException{
        try{       
            descuento_clase.setDescuento_clase(res);
            descuento_clase.updateDescuento_clase();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }

    /**
    * Metodo anularDescuento_clase, permite anular un acuerdo clase de la bd
    * (Descuento_clase).
    * @autor : Ing. Jose de la rosa
    * @see anularDescuento_clase - Descuento_claseDAO
    * @param : Descuento_clase
    * @version : 1.0
    */      
    public void anularDescuento_clase(Descuento_clase res)throws SQLException{
        try{
            descuento_clase.setDescuento_clase(res);
            descuento_clase.anularDescuento_clase();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo: searchDetalleDescuento_clase, permite listar los acuerdos clases por detalles dados unos parametros.
     * (standar, codigo).
     * @autor : Ing. Jose de la rosa
     * @see anularDescuento_clase - Descuento_claseDAO
     * @param : numero del standar y el codigo de concepto.
     * @version : 1.0
     */       
    public void searchDetalleDescuento_clase (String clase, String codigo, String frecuencia, String mes, String distrito) throws SQLException{
        try{
            descuento_clase.searchDetalleDescuento_clase(clase, codigo, frecuencia, mes, distrito);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
 
}
