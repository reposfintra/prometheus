/*Created on 13 de julio de 2005, 10:54 AM*/

package com.tsp.operation.model.services;

/**@author  fvillacob */



import java.io.*;
import java.util.*;
import com.tsp.operation.model.beans.*;



public class DirectorioService {
    
    private ListadoArchivos directorio;
    private String          url;
    private String          raiz;
    private String          ruta;
    
    
    public DirectorioService() {
        directorio = new ListadoArchivos();
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        ruta = rb.getString("ruta");
        ////System.out.println("Esta es la ruta del directorio "+ruta);
        url        = ruta + "/exportar/migracion/";//"/usr/local/tomcat/webapps/sot/informes/";
        raiz       = "/exportar/migracion/";
    }
    
    
    
    public void create(String user) throws Exception{
        try{ ListadoArchivos.create( this.url + user); }
        catch(Exception e){ throw new Exception(e.getMessage());}
    }
    
    
    
    public void setDirectorio(ListadoArchivos dir){  this.directorio = dir; }
    public ListadoArchivos getDirectorio()        { return this.directorio; }
    
    public String getUrl    () { return this.url   ; }
    public String getRaiz   () { return this.raiz  ; }
    
    
    public Archivo getArchivo(int id){
        List list = this.directorio.getListado();
        Archivo file = null;
        if(list!=null && list.size()>0){
            Iterator it = list.iterator();
            while(it.hasNext()){
               Archivo arc = (Archivo) it.next();
               if(arc.get_Id()==id){
                   file = arc;
                   break;
               }
            }
        }
        return file;
    }
    
    public void delete(String ruta) throws Exception{
       try{directorio.delete(ruta);}
       catch (Exception e){ throw new Exception ( e.getMessage());}
    }
    
    /**
     * @return the ruta
     */
    public String getRuta() {
        return ruta;
    }
    
    
    
    
}
