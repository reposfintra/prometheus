/***************************************
 * Nombre Clase  CaptacionesFintraService.java
 * Descripci�n   Permite Operar las captaciones
 * Autor         JULIO ERNESTO BARROS RUEDA
 * Fecha         06/06/2007
 * versi�n       1.0
 * Copyright     Fintra Valores S.A.
 *******************************************/


package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.util.Utility;
import com.tsp.util.Util;
import java.util.*;
//import java.io.*;
    

public class CaptacionesFintraService {

    CaptacionesFintraDAO   CaptacionesFintraDataAccess;
    
    
    Propietario     Propietario      = new Propietario(); 
    Vector          Captaciones      = new Vector();
    Vector          CaptacionesM     = new Vector();
    
   
    
    public CaptacionesFintraService() {
        CaptacionesFintraDataAccess = new CaptacionesFintraDAO();
    }
    public CaptacionesFintraService(String dataBaseName) {
        CaptacionesFintraDataAccess = new CaptacionesFintraDAO(dataBaseName);
    }
    
    /*
     * Metodo que retorna los datos de un propietario de Captaciones
     *
     */
    public void buscarPropietario(String propietario) throws Exception {
        try{
            Propietario = CaptacionesFintraDataAccess.buscarPropietario(propietario);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
     /**
     * M�todos que permite trasladar el anticipo para la tabla de terceros
     * @autor.......fvillacob
     * @throws      Exception
     * @version.....1.0.
     **/
    public String InsertAnticipo( Captacion captacion)throws Exception{
        try{
            return  this.CaptacionesFintraDataAccess.InsertAnticipo(captacion);
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    /*
     * Metodo que ingresa la captacion
     *
     */
    public String Registrar_Operacion(Captacion captacion) throws Exception {
        try{
            return CaptacionesFintraDataAccess.Registrar_Operacion(captacion); 
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    public void MovimientosProveedorCaptaciones(String nit,String fechai,String fechaf)throws Exception{
      try{
            Captaciones = CaptacionesFintraDataAccess.MovimientosProveedorCaptaciones(nit,fechai,fechaf); 
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }  
    }
    
    
    public void ProveedoresCaptacionesMesAMes(String fechai,String fechaf)throws Exception{
      try{
            CaptacionesM = CaptacionesFintraDataAccess.ProveedoresCaptacionesMesAMes(fechai,fechaf); 
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }  
    } 
    
    /*
     * Metodo que retorna el consecutivo requrido
     *
     *
     */
    public String ConsecutivoCaptaciones(String operacion) throws Exception {
        String consecutivo = "";
        try{
            consecutivo = CaptacionesFintraDataAccess.ConsecutivoCaptaciones(operacion); 
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        return consecutivo;
    }
    
    
    /**
     * Getter for property Propietario.
     * @return Value of property Propietario.
     */
    public com.tsp.operation.model.beans.Propietario getPropietario() {
        return Propietario;
    }    
    
    /**
     * Setter for property Propietario.
     * @param Propietario New value of property Propietario.
     */
    public void setPropietario(com.tsp.operation.model.beans.Propietario Propietario) {
        this.Propietario = Propietario;
    }
    
    /**
     * Getter for property Captaciones.
     * @return Value of property Captaciones.
     */
    public java.util.Vector getCaptaciones() {
        return Captaciones;
    }
    
    /**
     * Setter for property Captaciones.
     * @param Captaciones New value of property Captaciones.
     */
    public void setCaptaciones(java.util.Vector Captaciones) {
        this.Captaciones = Captaciones;
    }
    
    /**
     * Getter for property CaptacionesM.
     * @return Value of property CaptacionesM.
     */
    public java.util.Vector getCaptacionesM() {
        return CaptacionesM;
    }
    
    /**
     * Setter for property CaptacionesM.
     * @param CaptacionesM New value of property CaptacionesM.
     */
    public void setCaptacionesM(java.util.Vector CaptacionesM) {
        this.CaptacionesM = CaptacionesM;
    }
    
}
