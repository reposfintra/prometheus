/*
 * CambioDestinoService.java
 *
 * Created on 24 de abril de 2007, 11:52 AM
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.CambioDestinoDAO;
import com.tsp.operation.model.beans.Remesa2;
import com.tsp.operation.model.beans.Movrem;
import com.tsp.operation.model.beans.Tasa;
import com.tsp.util.*;
import java.util.*;
import java.sql.*;

public class CambioDestinoService {
    
    
    
    CambioDestinoDAO dao;    
    Remesa2          remesa ;
    Remesa2          remesaRemplazo ;
    
    TreeMap origenes;
    TreeMap destinos;
    TreeMap estandares;
    TreeMap ciudades;
    
    Vector remitentes;
    Vector destinatarios;
    
    TasaService    tasaSvc;
    FacturaService facturaSvc;

    public CambioDestinoService() {
        dao        = new CambioDestinoDAO();
        tasaSvc    = new TasaService();
        facturaSvc = new FacturaService();
    }
    public CambioDestinoService(String dataBaseName) {
        dao        = new CambioDestinoDAO(dataBaseName);
        tasaSvc    = new TasaService(dataBaseName);
        facturaSvc = new FacturaService(dataBaseName);
    }
    
    
    /**
     * Metodo para extraer los datos de la remesa
     * @autor mfontalvo
     * @param dstrct, distrito de la remesa 
     * @param numrem, numero de la remesa
     * @throws Exception.
     * @return bean de Remesa2
     */
    public void obtenerRemesa(String dstrct, String numrem) throws Exception{
        remesa = dao.obtenerRemesa(dstrct, numrem);
        
        // busqueda de otros movimientos de la remesa
        Movrem mov = facturaSvc.buscarMovrem(dstrct, numrem, "TOTAL");
        if (mov!=null){
            remesa.setVlrrem    ( mov.getVlrrem()     );
            remesa.setVlrrem2   ( mov.getVlrrem2()    );
            remesa.setQty_value ( mov.getQty_value()  );
            remesa.setPesoreal  ( mov.getPesoreal()   );
            remesa.setTasa      ( mov.getTasa()       );
        }
        
    }
    
    

    
    /**
     * Metodo para extraer los datos de un estandar, en el formato de Remesa2
     * @autor mfontalvo
     * @param dstrct, distrito de la remesa 
     * @param estandar, numero del estandar
     * @param cliente,l codigo del cliente
     * @throws Exception.
     * @return bean de Remesa2
     */
    public void obtenerRemesaRemplazo(String dstrct, String estandar, String cliente) throws Exception{
        remesaRemplazo = dao.obtenerRemesaRemplazo(dstrct, estandar, cliente);
        
        if (remesaRemplazo!=null){
            
            remesaRemplazo.setCia   ( remesa.getCia()    );
            remesaRemplazo.setNumrem( remesa.getNumrem() );
            
            if( remesaRemplazo.getCurrency().equals("PES") ){
                remesaRemplazo.setTasa(1);
                remesaRemplazo.setVlrrem2( remesaRemplazo.getVlrrem() );
            } else {
                int decimales = (remesaRemplazo.getCurrency().equalsIgnoreCase("DOL")?2:0);
                if (remesa.getCurrency().equals(remesaRemplazo.getCurrency())){
                    remesaRemplazo.setTasa   ( remesa.getTasa() );
                    remesaRemplazo.setVlrrem2( Util.roundByDecimal(remesaRemplazo.getVlrrem() * remesa.getTasa(), decimales) );
                } else {
                    Tasa   t      = tasaSvc.buscarValorTasa("PES", remesaRemplazo.getCurrency(), "PES", remesa.getFecrem() );
                    double tasa   = (t!=null? t.getValor_tasa() : 0);                    
                    remesaRemplazo.setTasa( tasa );
                    remesaRemplazo.setVlrrem2( Util.roundByDecimal(remesaRemplazo.getVlrrem() * tasa, decimales) );                    
                }
            }
        }
    }
       
    /**
     * Metodo para extraer los origenes apartir de un cliente
     * @autor mfontalvo
     * @param dstrct, distrito de los estandares
     * @param cliente, codigo de cliente 
     * @throws Exception.
     * @return bean de TreeMap 
     */
    public void obtenerOrigenes(String dstrct, String cliente) throws Exception{
        origenes = dao.obtenerOrigenes(dstrct, cliente);
    }
    
    /**
     * Metodo para extraer los destino apartir de un cliente
     * @autor mfontalvo
     * @param dstrct, distrito de los estandares
     * @param cliente, codigo de cliente 
     * @param origen, codigo del origen
     * @throws Exception.
     * @return bean de TreeMap 
     */
    public void obtenerDestinos(String dstrct, String cliente, String origen) throws Exception{
        destinos = dao.obtenerDestinos(dstrct, cliente, origen);
    }
    
    /**
     * Metodo para extraer los destino apartir de un cliente
     * @autor mfontalvo
     * @param dstrct, distrito de los estandares
     * @param cliente, codigo de cliente 
     * @param origen, codigo del origen
     * @throws Exception.
     * @return bean de TreeMap 
     */
    public void obtenerEstandares(String dstrct, String cliente, String origen, String destino) throws Exception{
        estandares = dao.obtenerEstandares(dstrct, cliente, origen, destino);
    }
    
    
    /**
     * Metodo para extraer las ciudades
     * @autor mfontalvo
     * @throws Exception.
     * @return bean de TreeMap 
     */
    public void obtenerCiudades() throws Exception{
        ciudades = dao.obtenerCiudades();
    }
        
    
    
    /**
     * Metodo para extraer los remitentes y destinatarios.
     * @autor mfontalvo
     * @param ciudad, codigo de la ciudad
     * @param cliente, clodigo del cliente
     * @throws Exception.
     * @return bean de TreeMap 
     */
    public void obtenerRemitentes(String ciudad, String cliente) throws Exception{
        remitentes = dao.obtenerREMIDEST(ciudad, cliente, "RE");
    }
        
    
    /**
     * Metodo para extraer los remitentes y destinatarios.
     * @autor mfontalvo
     * @param ciudad, codigo de la ciudad
     * @param cliente, clodigo del cliente
     * @throws Exception.
     * @return bean de TreeMap 
     */
    public void obtenerDestinatarios(String ciudad, String cliente) throws Exception{
        destinatarios = dao.obtenerREMIDEST(ciudad, cliente, "DE");
    }
    
    
    /**
     * Metodo para guardar el cambio de destino de la remesa.
     * @autor mfontalvo
     * @param ro, remesa original
     * @param ra, remesa actual
     * @throws Exception.
     */
    public void saveCambioDestino(Remesa2 ro, Remesa2 ra) throws Exception {
        dao.saveCambioDestino(ro, ra);
    }
    /**
     * Getter for property remesa.
     * @return Value of property remesa.
     */
    public com.tsp.operation.model.beans.Remesa2 getRemesa() {
        return remesa;
    }    
    
    /**
     * Setter for property remesa.
     * @param remesa New value of property remesa.
     */
    public void setRemesa(com.tsp.operation.model.beans.Remesa2 remesa) {
        this.remesa = remesa;
    }
    
    /**
     * Getter for property origenes.
     * @return Value of property origenes.
     */
    public java.util.TreeMap getOrigenes() {
        return origenes;
    }
    
    /**
     * Setter for property origenes.
     * @param origenes New value of property origenes.
     */
    public void setOrigenes(java.util.TreeMap origenes) {
        this.origenes = origenes;
    }
    
    /**
     * Getter for property destinos.
     * @return Value of property destinos.
     */
    public java.util.TreeMap getDestinos() {
        return destinos;
    }
    
    /**
     * Setter for property destinos.
     * @param destinos New value of property destinos.
     */
    public void setDestinos(java.util.TreeMap destinos) {
        this.destinos = destinos;
    }
    
    /**
     * Getter for property estandares.
     * @return Value of property estandares.
     */
    public java.util.TreeMap getEstandares() {
        return estandares;
    }
    
    /**
     * Setter for property estandares.
     * @param estandares New value of property estandares.
     */
    public void setEstandares(java.util.TreeMap estandares) {
        this.estandares = estandares;
    }
    
    /**
     * Getter for property remesaRemplazo.
     * @return Value of property remesaRemplazo.
     */
    public com.tsp.operation.model.beans.Remesa2 getRemesaRemplazo() {
        return remesaRemplazo;
    }
    
    /**
     * Setter for property remesaRemplazo.
     * @param remesaRemplazo New value of property remesaRemplazo.
     */
    public void setRemesaRemplazo(com.tsp.operation.model.beans.Remesa2 remesaRemplazo) {
        this.remesaRemplazo = remesaRemplazo;
    }
    
    /**
     * Getter for property ciudades.
     * @return Value of property ciudades.
     */
    public java.util.TreeMap getCiudades() {
        return ciudades;
    }
    
    /**
     * Setter for property ciudades.
     * @param ciudades New value of property ciudades.
     */
    public void setCiudades(java.util.TreeMap ciudades) {
        this.ciudades = ciudades;
    }
    
    /**
     * Getter for property remitentes.
     * @return Value of property remitentes.
     */
    public java.util.Vector getRemitentes() {
        return remitentes;
    }
    
    /**
     * Setter for property remitentes.
     * @param remitentes New value of property remitentes.
     */
    public void setRemitentes(java.util.Vector remitentes) {
        this.remitentes = remitentes;
    }
    
    /**
     * Getter for property destinatarios.
     * @return Value of property destinatarios.
     */
    public java.util.Vector getDestinatarios() {
        return destinatarios;
    }
    
    /**
     * Setter for property destinatarios.
     * @param destinatarios New value of property destinatarios.
     */
    public void setDestinatarios(java.util.Vector destinatarios) {
        this.destinatarios = destinatarios;
    }
    
    public String actualizarValor(Remesa2 ro) throws SQLException {
        return dao.actualizarValor(ro);
    }
        
    
}
