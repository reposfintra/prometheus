/************************************************************************************
 * Nombre clase : CorridaService.java                                               *
 * Autor : Ing. Henry A.Osorio Gonz�lez                                             *
 * Modificado Ing Sandra Escalante Greco                                            *
 * Fecha : 28 de Noviembre de 2005, 4:02 PM                                         *
 * Version : 1.0                                                                    *
 * Copyright : Fintravalores S.A.                                              *
 ************************************************************************************/
package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.TransaccionService;
import java.util.*; 
import java.sql.*;

public class CorridaService {
    
    public CorridaDAO dao;
    public  Paginacion   paginacion = new Paginacion();
    public String corrida = "";
    public String propietario = "";
    
    
    public CorridaService() {       
        dao = new CorridaDAO();
    }
    
    public CorridaService(String dataBaseName) {       
        dao = new CorridaDAO(dataBaseName);
    }
    
    /**
     * Metodo buscarCorridas, Busca todas las corridas del sistema segun distrito y agencia
     * @autor Ing Henry Osorio
     * @modif Ing. Sandra Escalante Greco
     * @param : String dstrct, String agencia
     * @see :buscarCorridas(dstrct, agencia) 
     * @version : 2.0
     */
    public void buscarCorridas(String dstrct, String agencia, boolean MantenerPaginacion) throws SQLException {
        dao.buscarCorridas(dstrct, agencia);
    }
    
     /**
     * Metodo buscarCorridaBancos, busca los bancos de una corridas segun parametros de busqueda
     * @autor : Ing. Ivan Dario Gomez   
     * @param : String banco, String sucursal, String corrida, String distrito
     * @see   : buscarCorridaBancos - CorridaDAO
     * @version : 1.0
     */
    public void buscarCorridaBancos(String dstrct, String agencia,String Numcorrida, boolean MantenerPaginacion) throws SQLException {
        dao.buscarCorridaBancos(dstrct, agencia,Numcorrida);  
    }
    
     /**
     * Metodo buscarBancoProveedor, Busca todos los Proveedores de un banco que tenga facturas en una corrida segun distrito y agencia
     *                        si la agencia es OP muestra todos los bancos
     * @autor : Ing Ivan Dario Gomez 
     * @param : String dstrct, String agencia, String numero de corrida, String Banco y String Sucursal
     * @see   : buscarBancoProveedor - CorridaDAO
     * @version : 2.0
     */
     public void buscarBancoProveedor(String dstrct, String agencia,String Numcorrida,String banco, String sucursal, boolean MantenerPaginacion) throws SQLException {
        dao.buscarBancoProveedor(dstrct, agencia,Numcorrida,banco,sucursal);  
    }
    
      /**
     * Metodo buscarBancoProveedor, Busca todos los Proveedores de un banco que tenga facturas en una corrida segun distrito y agencia
     *                        si la agencia es OP muestra todos los bancos
     * @autor : Ing Ivan Dario Gomez 
     * @param : String dstrct, String agencia, String numero de corrida, String Banco y String Sucursal
     * @see   : buscarBancoProveedor - CorridaDAO
     * @version : 2.0
     */
     public void buscarProveedorFactura(String dstrct, String agencia,String Numcorrida,String banco, String sucursal,String NitPro, boolean MantenerPaginacion) throws SQLException {
        dao.buscarProveedorFactura(dstrct, agencia,Numcorrida,banco,sucursal,NitPro);  
    }
     
     
    /**
     * Metodo autorizarCorrida, autoriza el pago de una corrida
     * @autor : Ing. Ivan Dario Gomez Vanegas
     * @param : String usuario, String [] corridas seleccionas para autorizar,  lista de corridas que no se autorizan 
     * @see : autorizarCorrida - CorridaDAO
     * @version : 2.0
     */
    public void autorizarCorrida(String user, String[] CorridasSelec, List CorridasNoSelec) throws SQLException {
        dao.autorizarCorrida(user, CorridasSelec, CorridasNoSelec);  
    }
    
     /**
     * Metodo autorizarBanco, autoriza el pago de las facturas de un banco
     * @autor : Ing. Ivan Dario Gomez Vanegas
     * @param : String usuario, Lista de  Bancos seleccionas para autorizar,  lista de bancos que no se autorizan 
     * @see : autorizarBanco - CorridaDAO
     * @version : 2.0
     */
    public void autorizarBanco(String user, List BancosSelec, List BancosNoSelec) throws SQLException {
        dao.autorizarBanco(user, BancosSelec, BancosNoSelec);   
    }
    
    
     /**
     * Metodo autorizarProveedor, autoriza el pago de las facturas de un banco
     * @autor : Ing. Ivan Dario Gomez Vanegas
     * @param : String distrito ,String usuario, Lista de  Bancos seleccionas para autorizar,  lista de bancos que no se autorizan 
     * @see : autorizarBanco - CorridaDAO
     * @version : 2.0
     */
    public void autorizarProveedor(String dstrct,String user, List ProveedorSelec, List ProveedorNoSelec) throws SQLException {
        dao.autorizarProveedor(dstrct, user, ProveedorSelec, ProveedorNoSelec);    
    }
    
    
    /**
     * Metodo autorizarFactura, autoriza el pago de las facturas de un proveedor
     * @param dstrct
     * @param user
     * @param FacturaSelec
     * @param FacturaNoSelec
     * @throws java.sql.SQLException
     * @autor : Ing. Ivan Dario Gomez Vanegas 
     * @see : autorizarBanco - CorridaDAO
     * @version : 2.0
     */
    public void autorizarFactura(String dstrct,String user, List FacturaSelec, List FacturaNoSelec) throws SQLException {
        dao.autorizarFactura(dstrct, user, FacturaSelec, FacturaNoSelec);     
    } 
    
     /**
     * Metodo getVectorCorridas, obtiene el valor del vector vecCorridas
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @return Vector
     * @see : getVectorCorridas()
     * @version : 1.0
     */
    public Vector getVectorCorridas() {
        return dao.getVectorCorridas();
    }
    
    /**
     * Metodo buscarListaCorridas, busca las corridas segun parametros de busqueda
     * @autor : Ing. Sandra Escalante    
     * @param : String banco, String sucursal, String corrida, String distrito
     * @see buscarListaCorridas(corr, dstrct)
     * @version : 1.0
     */
    public void buscarListaCorridas(String corr, String dstrct, boolean MantenerPaginacion) throws SQLException {
        dao.buscarListaCorridas(corr, dstrct);  
        paginacion.setConfiguracion("Corrida&accion=Search&cmd=paginacion&tipo=proveedor", 15, 15);
        paginacion.setLista(this.getListado());
        if (!MantenerPaginacion) paginacion.setVista_Actual(1);
    }
    
     /**
     * Metodo getListado, obtiene la variable listado
     * @autor  Ing. Sandra Escalante
     * @see getListado()
     * @return Vector
     * @version  1.0
     */
    public List getListado() {
        return dao.getListado();
    }
    
    /**
     * Metodo buscarFacturasxCP, busca las facturas correspondientes a una corrida y proveedor dado
     * @autor : Ing. Sandra Escalante    
     * @param : String nro de la corrida, String codigo del proveedor
     * @see buscarFacturasxCP(String corrida, String prov)
     * @version : 1.0
     */
    public void buscarFacturasxCP(String corrida, String prov, String dstrct) throws SQLException {
        dao.buscarFacturasxCP(corrida, prov, dstrct);
    }        
     /**
     * Metodo busquedaXTransferencia, busca las corridas dependiendo de la corrida
     * @autor : Ing. fily steven fernandez    
     * @param : String nro de la corrida
     * @see busquedaXTransferencia(String corrida)
     * @version : 1.0
     */
    public void busquedaXCorridas(String corridas) throws Exception {
        dao.busquedaXCorridas(corridas);
    }
    
    public void busquedaXHiloCorridas(String corridas) throws Exception {
        dao.busquedaXHiloCorridas(corridas); 
    }
    /**
     * Metodo busquedaXPropietario, busca las corridas dependiendo de la propietario
     * @autor : Ing. fily steven fernandez    
     * @param : String nro de la corrida
     * @see busquedaXPropietario(String propietario)
     * @version : 1.0
     */
    public void busquedaXPropietario(String propietario) throws Exception {
        dao.busquedaXPropietario(propietario);
    }
    
    /**
     * Metodo busquedaXTransferencias, busca las corridas dependiendo de la propietario y la 
     * corrida
     * @autor : Ing. fily steven fernandez    
     * @param : String nro de la corrida
     * @see busquedaXTransferencias(String propietario, String Corridas)
     * @version : 1.0
     */
    public void busquedaXTransferencias(String propietario, String corridas) throws Exception {
        dao.busquedaXTransferencias(propietario, corridas);
    }
    /**
     * Metodo SumaValorCorridas, suma total del campo valor por el propietario
     * corrida
     * @autor : Ing. fily steven fernandez    
     * @param : String nro de la corrida
     * @see SumaValorCorridas(String propietario)
     * @version : 1.0
     */
    public void SumaValorCorridas(String propietario) throws Exception {
        dao.SumaValorCorridas(propietario);
    }
    
    public java.util.Vector getVector() {
        return dao.getVector();
    }
     public java.util.Vector getVectorValor() {
        return dao.getVectorValor();
    }
     
     /**
      * Getter for property corrida.
      * @return Value of property corrida.
      */
     public java.lang.String getCorrida() {
         return corrida;
     }
     
     /**
      * Setter for property corrida.
      * @param corrida New value of property corrida.
      */
     public void setCorrida(java.lang.String corrida) {
         this.corrida = corrida;
     }
     
     /**
      * Getter for property propietario.
      * @return Value of property propietario.
      */
     public java.lang.String getPropietario() {
         return propietario;
     }
     
     /**
      * Setter for property propietario.
      * @param propietario New value of property propietario.
      */
     public void setPropietario(java.lang.String propietario) {
         this.propietario = propietario;
     }
     
     public void cxpro(String Propietario, String Corridas ) throws Exception {
         dao.cxpro(Propietario,Corridas);
     }
      /**
     * Metodo buscarCorridas, Busca todas las corridas del sistema segun distrito y agencia
     * @autor Ing jdelarosa
     * @modif 
     * @param : String dstrct, String agencia
     * @see :buscarCorridas(dstrct, agencia) 
     * @version : 2.0
     */
    public void buscarCorridasUsuarios(String usuario, String dstrct, String agencia) throws SQLException {
        dao.buscarCorridasUsuarios(dstrct, agencia, usuario);
    }
    
    
    /**
     * Metodo excluirValoresMenores, excluye los las facturas que tengan el valor menor al digitado
     * @autor  Ing. Ivan Dario Gomez
     * @param String dstrct, String corrida, double valor
     * @version  1.0
     */
    public void excluirValoresMenores(String dstrct, String corrida, double valor) throws SQLException {
        String SQL = dao.excluirValoresMenores(dstrct, corrida, valor);
        TransaccionService tService = new TransaccionService(this.dao.getDatabaseName());
        tService.crearStatement();
        tService.getSt().addBatch(SQL);  
        tService.execute();
        
    }
     
}
