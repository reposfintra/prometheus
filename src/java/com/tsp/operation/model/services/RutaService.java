/*
 * RutaService.java
 *
 * Created on 24 de junio de 2005, 11:03 AM
 */

package com.tsp.operation.model.services;

import java.util.*;
import java.sql.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;

/**
 *
 * @author  Jcuesta
 */
public class RutaService {
    
    RutaDAO Rutadao;
    
    /** Creates a new instance of RutaService */
    public RutaService() {
        Rutadao = new RutaDAO();
    }
    public void agregarRuta()throws SQLException{
        Rutadao.agregarRuta();
    }
    
    public void anularRuta()throws SQLException{
        Rutadao.anularRuta();
    }
    
    public void listarRutas()throws SQLException{
        Rutadao.listarRutas();
    }
    
    public void buscarRuta(String cia, String origen, String destino, String secuencia)throws SQLException{
        Rutadao.buscarRuta(cia, origen, destino, secuencia);
    }
    
    public void modificarRuta()throws SQLException{
        Rutadao.modificarRuta();
    }
    
    public boolean existeRuta(String cia, String origen, String destino)throws SQLException{
        return Rutadao.existeRuta(cia, origen, destino);
    }
    public void consultarRutas()throws SQLException{
        Rutadao.consultarRutas();
    }
    
    /**
     * Getter for property Ruta.
     * @return Value of property Ruta.
     */
    public com.tsp.operation.model.beans.Ruta getRuta() {
        return Rutadao.getRuta();
    }
    
    /**
     * Setter for property Ruta.
     * @param Ruta New value of property Ruta.
     */
    public void setRuta(com.tsp.operation.model.beans.Ruta Ruta) {
        Rutadao.setRuta(Ruta);
    }
    
    /**
     * Getter for property Rutas.
     * @return Value of property Rutas.
     */
    public java.util.Vector getRutas() {
        return Rutadao.getRutas();
    }
    
    /**
     * Setter for property Rutas.
     * @param Rutas New value of property Rutas.
     */
    public void setRutas(java.util.Vector Rutas) {
        Rutadao.setRutas( Rutas);
    }
    public String ultimaRuta(String cia, String origen, String destino)throws SQLException{
        return Rutadao.ultimaRuta(cia, origen, destino);
    }
    
    /**
    * Metodo vClientes , Metodo que retorna un Vector de Vectores con las rutas dado un String con la ciudad de origen de la ruta
    * @autor : Ing. David Lamadrid
    * @param : String nombre
    * @version : 1.0
    */
    public void vClientes(String nombre) throws SQLException{
        Rutadao.vClientes(nombre);
    }
    
    public java.util.Vector getVRutas(){
        return Rutadao.getVRutas();
    }
    
    public void setVRutas(java.util.Vector vRutas){
        Rutadao.setVRutas(vRutas);
    }
    /**
     * metodo que obtine las vias vias con origen y destino definido
     * y verifica que no se repita la ruta para una via
     * @autor Ing. SEscalante
     * @see verificarRuta(String origen, String destino, String nueva_ruta)
     * @param origen y destino de la via (String), ruta (String)
     * @return boolean
     * @version 1.0
     */
    public boolean verificarRuta(String origen, String destino, String nueva_ruta) throws Exception{
        return Rutadao.verificarRuta(origen, destino, nueva_ruta);
    }
    /**
     * M�todo que obtiene las ruta de una via
     * @autor Ing. David Pi�a Lopez
     * @throws SQLException
     * @param origen Origen de la via
     * @version 1.0
     */
    public void getRutaOrigen( String origen ) throws Exception{
        Rutadao.getRutaOrigen( origen );
    }
    /**
     * M�todo que obtiene las ruta de una via
     * @autor Ing. David Pi�a Lopez
     * @throws SQLException
     * @param origen Origen de la via
     * @version 1.0
     */
    public void armarVias( String via ) throws Exception{
        Rutadao.armarVias( via );
    }
    
    
/**
     * metodo que obtine las vias vias con origen y destino definido
     * y verifica que no se repita la ruta para una via
     * @autor Ing. Jose de la rosa
     * @see verificarRuta(String origen, String destino, String nueva_ruta)
     * @param origen y destino de la via (String), ruta (String)
     * @return boolean
     * @version 1.0
     */
    public boolean verificarRutaSecuencia(String origen, String destino, String nueva_ruta) throws Exception{
        return Rutadao.verificarRutaSecuencia(origen, destino, nueva_ruta);
    }
    
    
    
}
