/*********************************************************************************
 * Nombre clase :      ConsultarModificarFacturaClienteService.java              *
 * Descripcion :       Service del ConsultarModificarFacturaClienteService.java  *
 * Autor :             LREALES                                                   *
 * Fecha :             30 de noviembre de 2006, 04:00 PM                         *
 * Version :           1.0                                                       *
 * Copyright :         Fintravalores S.A.                                   *
 *********************************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;

public class ConsultarModificarFacturaClienteService {
    
    private ConsultarModificarFacturaClienteDAO dao;
    
    /** Creates a new instance of ConsultarModificarFacturaClienteService */
    public ConsultarModificarFacturaClienteService() {
        
        dao = new ConsultarModificarFacturaClienteDAO ();
        
    }
    
    /** Funcion publica que obtiene el metodo getVector del DAO */
    public Vector getVectorReporte () throws Exception {
        
        return dao.getVector();
        
    }    
    
    /** Funcion publica que obtiene el metodo consultarFactura del DAO */
    public void consultarFactura ( String distrito, String factura ) throws Exception {
        
        dao.consultarFactura ( distrito, factura );
        
    }
    
    /** Funcion publica que obtiene el metodo actualizarFactura del DAO */
    public void actualizarFactura ( String distrito, String factura, String nueva_fecha_probable_pago, String usuario ) throws Exception {
        
        dao.actualizarFactura ( distrito, factura, nueva_fecha_probable_pago, usuario );
        
    }
    
}