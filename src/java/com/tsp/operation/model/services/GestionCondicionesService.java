/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * GestionCondicionesService.java :
 * Copyright 2010 Rhonalf Martinez Villa <rhonaldomaster@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.tsp.operation.model.services;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author rhonalf
 */
public class GestionCondicionesService {

    private GestionCondicionesDAO gdao;

    public GestionCondicionesService(){
        gdao = new GestionCondicionesDAO();
    }
    public GestionCondicionesService(String dataBaseName){
        gdao = new GestionCondicionesDAO(dataBaseName);
    }

    /**
     * Busca los tipos de titulo valor
     * @return Listado con los resultados encontrados
     * @throws Exception Cuando hay error
     */
    public ArrayList<String> titulosValor() throws Exception{
        ArrayList<String> lista = null;
        try {
            lista = gdao.titulosValor();
        }
        catch (Exception e) {
            throw new Exception("Error en titulosValor en GestionCondicionesService.java: "+e.toString());
        }
        return lista;
    }

    /**
     * Busca los tipos de plazo de cuota
     * @return Listado con los resultados encontrados
     * @throws Exception Cuando hay error
     */
    public ArrayList<String> plazoCuota() throws Exception{
        ArrayList<String> lista = null;
        try {
            lista = gdao.plazoCuota();
        }
        catch (Exception e) {
            throw new Exception("Error en plazoCuota en GestionCondicionesService.java: "+e.toString());
        }
        return lista;
    }

    /**
     * Busca datos de convenios del afiliado
     * @param nitprov Nit del afiliado
     * @return Listado con los datos encontrados
     * @throws Exception Cuando hay error
     */
    public ArrayList<BeanGeneral> conveniosProv(String nitprov) throws Exception{
        ArrayList<BeanGeneral> lista = null;
        try {
            lista = gdao.conveniosProv(nitprov);
        }
        catch (Exception e) {
            throw new Exception("Error en conveniosProv en GestionCondicionesService.java: "+e.toString());
        }
        return lista;
    }

    /**
     * Busca el nombre de un sector
     * @param id_sector El codigo del sector
     * @return Cadena con el nombre
     * @throws Exception Cuando hay error
     */
    public String nomSector(String id_sector) throws Exception{
        String nombre = "";
        try {
            nombre = gdao.nomSector(id_sector);
        }
        catch (Exception e) {
            throw new Exception("Error en nomSector en GestionCondicionesService.java: "+e.toString());
        }
        return nombre;
    }

    /**
     * Busca el nombre de un subsector
     * @param id_sector El codigo del subsector
     * @return Cadena con el nombre
     * @throws Exception Cuando hay error
     */
    public String nomSubSector(String id_sector,String id_subsector) throws Exception{
        String nombre = "";
        try {
            nombre = gdao.nomSubSector(id_sector,id_subsector);
        }
        catch (Exception e) {
            throw new Exception("Error en nomSubSector en GestionCondicionesService.java: "+e.toString());
        }
        return nombre;
    }

    /**
     * Busca el nombre de un tipo de titulo valor
     * @param id_titulo Codigo del titulo
     * @return Cadena con el nombre
     * @throws Exception Cuando hay error
     */
    public String nomTipoTitulo(String id_titulo) throws Exception{
        String nombre = "";
        try {
            nombre = gdao.nomTipoTitulo(id_titulo);
        }
        catch (Exception e) {
            throw new Exception("Error en nomTipoTitulo en GestionCondicionesService.java: "+e.toString());
        }
        return nombre;
    }

    /**
     * Actualiza los rangos de una condicion
     * @param listarangos Daros a actualizar
     * @throws Exception Cuando hay error
     */
    public void modificarRangos(ArrayList<String> listarangos,String cod_cond,String user) throws Exception{
        try {
            gdao.modificarRangos(listarangos,cod_cond,user);
        }
        catch (Exception e) {
            throw new Exception("Error en modificarRangos en GestionCondicionesService.java: "+e.toString());
        }
    }

    /**
     * Inserta los rangos de una condicion
     * @param listarangos Datos a actualizar
     * @throws Exception Cuando hay error
     */
    public void insertarRangos(ArrayList<String> listarangos) throws Exception{
        try {
            gdao.insertarRangos(listarangos);
        }
        catch (Exception e) {
            throw new Exception("Error en insertarRangos en GestionCondicionesService.java: "+e.toString());
        }
    }

    /**
     * Busca proveedores dependiendo de un filtro
     * @param dato El dato a buscar
     * @param filtro Filtro a aplicar
     * @return Listado de datos que coinciden con el dato y el filtro
     * @throws Exception Cuando hay error
     */
    public ArrayList<Proveedor> buscarProv(String dato,String filtro) throws Exception{
        ArrayList<Proveedor> lista = null;
        try {
            lista = gdao.buscarProv(dato, filtro);
        }
        catch (Exception e) {
            throw new Exception("Error en buscarProv en GestionCondicionesService.java: "+e.toString());
        }
        return lista;
    }

    /**
     * Busca los rangos asociados con las condiciones
     * @param conv codigo del convenio
     * @param tipotit tipo de titulo valor
     * @param plazoprimer plazo del primer titulo
     * @param prop si es propietario o no
     * @return Lista con los datos encontrados
     * @throws Exception Cuando hay error
     */
    public ArrayList<BeanGeneral> rangosConvenio(String conv,String tipotit,String plazoprimer,boolean prop) throws Exception{
        ArrayList<BeanGeneral> lista = null;
        try {
            lista = gdao.rangosConvenio(conv, tipotit, plazoprimer, prop);
        }
        catch (Exception e) {
            throw new Exception("Error en rangosConvenio en GestionCondicionesService.java: "+e.toString());
        }
        return lista;
    }

    /**
     * Busca los el codigo de las condiciones
     * @param conv codigo del convenio
     * @param tipotit tipo de titulo valor
     * @param plazoprimer plazo del primer titulo
     * @param prop si es propietario o no
     * @return Cadena con el dato encontrado
     * @throws Exception Cuando hay error
     */
    public String codigoCond(String conv,String tipotit,String plazoprimer,boolean prop) throws Exception{
        String cod = "";
        try {
            cod = gdao.codigoCond(conv, tipotit, plazoprimer, prop);
        }
        catch (Exception e) {
            throw new Exception("Error en codigoCond en GestionCondicionesService.java: "+e.toString());
        }
        return cod;
    }

    /**
     * Busca el codigo de la relacion convenio-sector-subsector
     * @param id_convenio Codigo del convenio
     * @param cod_sector Codigo del sector
     * @param cod_subsector Codigo del subsector
     * @return Cadena con el codigo obtenido
     * @throws Exception Cuando hay error
     */
    public String codigoCv(String id_convenio,String cod_sector,String cod_subsector) throws Exception{
        String cod = "";
        try {
            cod = gdao.codigoCv(id_convenio, cod_sector, cod_subsector);
        }
        catch (Exception e) {
            throw new Exception("Error en codigoCv en GestionCondicionesService.java: "+e.toString());
        }
        return cod;
    }

    public void insertHeaderConv(String codcv,String tipotit,String plazoprimer,boolean prop,String user) throws Exception{
        try {
            gdao.insertHeaderConv(codcv, tipotit, plazoprimer, prop, user);
        }
        catch (Exception e) {
            throw new Exception("Error en insertHeaderConv en GestionCondicionesService.java: "+e.toString());
        }
    }

     /**
     * Busca la informacion de los convenios, sector y subsector para un afiliado y un usuario
     * @param nitprov nit del proveedor
     * @param idUsuario id del usuario
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<BeanGeneral> obtenerConveniosProvUsuario(String nitprov, String idUsuario) throws Exception{
        return gdao.obtenerConveniosProvUsuario(nitprov, idUsuario);
    }

    public ArrayList<String> titulosValorConvenio(String idConvenio) throws Exception{
        return gdao.titulosValorConvenio(idConvenio);
    }
    public ArrayList<Proveedor> buscarAfiliadosUsuario(String idUsuario) throws Exception{
        return gdao.buscarAfiliadosUsuario(idUsuario);
    }
    public ArrayList<BeanGeneral> obtenerBancos() throws Exception{
        return gdao.obtenerBancos();
    }
    public double buscarPorcentajeAval(int idProvConvenio, String tipoTitulo, int plazo, boolean propietario, int numCuotas) throws Exception {
        return gdao.buscarPorcentajeAval(idProvConvenio, tipoTitulo, plazo, propietario, numCuotas);
    }
    public ArrayList buscarRangosAval(int idProvConvenio, String tipoTitulo, int plazo, boolean propietario) throws Exception {
        return gdao.buscarRangosAval(idProvConvenio, tipoTitulo, plazo, propietario);
    }
    
    
    /**
     * Busca la informacion de los convenios, sector y subsector para un afiliado y un usuario
     * @param nitprov nit del proveedor
     * @param idUsuario id del usuario
     * @param idconvenio id del convenio
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<BeanGeneral> obtenerSectoresProvUsuario(String nitprov, String idUsuario, String idconvenio) throws Exception{
        return gdao.obtenerSectoresProvUsuario(nitprov, idUsuario, idconvenio);
    }

    /**
     *
     * @param numero_solicitud
     * @return
     * @throws java.lang.Exception
     */
    public JsonObject getInfoPlanDePagos(int numero_solicitud) throws Exception {
         return gdao.getInfoPlanDePagos(numero_solicitud);
    }

    public JsonArray getDetallePlanDePagos(int numero_solicitud) throws Exception{
        return gdao.getDetallePlanDePagos(numero_solicitud);
    }

    public JsonObject getTotalDetallePlanDePagos(int numero_solicitud) throws Exception {
       return gdao.getTotalDetallePlanDePagos(numero_solicitud);
    }

}
