/******************************************************************
* Nombre ......................TblGeneralDatoService.java
* Descripci�n..................Clase Service para Tabla General Dato
* Autor........................Ing. Armando Oviedo
* Fecha........................21/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  Ing. Armando Oviedo
 */
public class TblGeneralDatoService {
    
    private TblGeneralDatoDAO tgddao;
    /** Creates a new instance of TblGeneralDatoService */
    public TblGeneralDatoService() {
        tgddao = new TblGeneralDatoDAO();
    }
    
    /**
     * M�todo que setea un objeto TblGeneralDato
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @see.........TblGeneralDatoDAO
     * @param.......TblGeneralDato tgd
     **/     
    public void setTablaGeneralDato(TblGeneralDato tgd){
        this.tgddao.setTablaGeneralDato(tgd);
    }
    
    /**
     * M�todo que retorna un objeto TblGeneralDato
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @see.........TblGeneralDatoDAO
     * @return.......TblGeneralDato tgd
     **/     
    public TblGeneralDato getTablaGeneralDato(){
        return tgddao.getTablaGeneralDato();
    }
    
    /**
     * M�todo que busca y setea un objeto TblGeneralDato
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @see.........TblGeneralDatoDAO
     * @param.......TblGeneralDato de cargado con el set
     **/ 
    public void buscarTablaGeneralDato() throws SQLException{
        this.tgddao.buscarTablaGeneralDato();
    }
    
    /**
     * M�todo que retorna un boolean si existe el TblGeneralDato o no
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @see.........TblGeneralDatoDAO
     * @param.......c�digo de la tabla general dato cargado con el set
     **/     
    public boolean existe() throws SQLException{
        return this.tgddao.existe();
    }
    
    /**
     * M�todo que retorna un boolean si existe un item TblGeneralDato, previamente anulado
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @see.........TblGeneralDatoDAO
     * @param.......c�digo de tabla general dato cargado con el set
     **/   
    public boolean existeAnulado() throws SQLException{
        return this.tgddao.existeAnulado();
    }
    
    /**
     * M�todo que actualiza el reg_status de un TblGeneralDato anulado
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @see.........TblGeneralDatoDAO
     * @version.....1.0.     
     **/ 
    public void addTablaGeneralDatoAnulado() throws SQLException{
        this.tgddao.addTablaGeneralDatoAnulado();
    }
    
    /**
     * M�todo que agrega un TblGeneralDato
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @see.........TblGeneralDatoDAO
     * @version.....1.0.     
     **/ 
    public void addTablaGeneralDato() throws SQLException{        
        if(!existeAnulado()){
            this.tgddao.addTablaGeneralDato();
        }
        else{
            this.tgddao.addTablaGeneralDatoAnulado();
        }        
    }
    
    /**
     * M�todo que modifica un TblGeneralDato
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @see.........TblGeneralDatoDAO
     * @version.....1.0.     
     **/ 
    public void update() throws SQLException{
        this.tgddao.update();
    }
    
    /**
     * M�todo que elimina un TblGeneralDato
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @see.........TblGeneralDatoDAO
     * @version.....1.0.     
     **/ 
    public void delete() throws SQLException{
        this.tgddao.delete();
    }
    
    /**
     * M�todo que setea un vector que contiene todos los objetos de la tabla
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @see.........TblGeneralDatoDAO
     * @version.....1.0.     
     **/ 
    public void buscarTodosTablaGeneralDato() throws SQLException{    
        this.tgddao.buscarTodosTablaGeneralDato();
    }
    
    /**
     * M�todo que guetea todos los Tabla General Dato
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     * @see.........TblGeneralDatoDAO
     * @return......Vector de objetos TblGeneralDato
     **/     
    public Vector getTodosTablaGeneralDato(){
        return this.tgddao.getTodosTablaGeneralDato();
    }
    
    /**
     * M�todo getTiposDatos, que guetea un treemap que contiene tipos de datos de una BD
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.          
     * @see.........TblGeneralDatoDao
     * @return......TreeMap de tipos de datos
     **/     
    public java.util.TreeMap getTiposDatos(){        
        TreeMap causas = new TreeMap();
        try{            
            causas.put("numeric", "numeric");
            causas.put("varchar", "varchar");
            causas.put("serial", "serial");
            causas.put("timestamp", "timestamp");
            causas.put("char", "char");
            causas.put("text", "text");
            causas.put("int", "int");
            causas.put("bit", "bit");
            causas.put("bool", "bool");
            causas.put("float", "float");
            causas.put("time", "time");
            causas.put("varbit", "varbit");
            causas.put("moneda", "moneda");
        }
        catch(Exception ex){
           ex.printStackTrace();
        }
        return causas;
    }
    
    /**
     * M�todo que guetea una secuencia dado el c�digo
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.      
     * @see.........TblGeneralDatoDao    
     * @return......Secuencia
     **/     
    public int getSequencyNumber(String codtabla) throws SQLException{
        return this.tgddao.getSequencyNumber(codtabla);
    }
    
    /**
     * M�todo que setea un Vector que contiene objetos tipo TblGeneralDato
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @see.........TblGeneralDatoDao
     * @version.....1.0.
     */     
    public void buscarItemsCodigoTabla(String codtabla) throws SQLException{
        this.tgddao.buscarItemsCodigoTabla(codtabla);
    }
    
    /**
     * M�todo que reordena las secuencias que han sido modificadas
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @param.......String secuencias[], String codtabla
     * @see.........TblGeneralDatoDao
     * @version.....1.0.
     */ 
    public void reordenarSecuenciasModificadas(String secuencias[], String codtabla) throws SQLException{
        this.tgddao.reordenarSecuenciasModificadas(secuencias,codtabla);
    }
    
    /**
     * M�todo que reordena las secuencias si alguna ha sido eliminada
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @param.......String codtabla
     * @see.........TblGeneralDatoDao
     * @version.....1.0.
     */ 
    public void recalcularSecuenciasEliminadas(String codtabla) throws SQLException{
        this.tgddao.recalcularSecuenciasEliminadas(codtabla);
    }
    
     /**
     * buscarLeyendaTable_type, busca las leyendas dependiendo al cod tabletype
     * @autor       Diogenes Bastidas Morales       
     * @throws      SQLException
     * @version     1.0.
     * @see         buscarLeyendaTable_type -  TblGeneralDatoDAO
     * @param       codigo tablatype
     **/ 
    public Vector buscarLeyendaTable_type(String tabletype) throws SQLException{
        return tgddao.buscarLeyendaTable_type(tabletype);

    }
    
}
