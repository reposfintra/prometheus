/********************************************************************
 *  Nombre Clase.................   ReporteRetroactivoService.java
 *  Descripci�n..................   Service del DAO del reporte de retroactivos
 *  Autor........................   Ing. Tito Andr�s Maturana
 *                                  Ing. Leonardo Parodi
 *  Fecha........................   14.12.2005
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;


public class ReporteRetroactivoService {
    
    public ReporteRetroactivoDAO dao;
    
    /** Creates a new instance of CXPDocService */
    public ReporteRetroactivoService() {
        dao = new ReporteRetroactivoDAO();
    }    
    
    /**
     * Realiza la consulta del reporte de retroactivos y los almacena en una
     * la tabala 'reportes_retroactivos' en ele schema 'tem'.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param fechaI Fecha inicial del reporte
     * @param fechaF Fecha final del reporte
     * @see com.tsp.operation.model.DAOS.ReporteRetroactivoDAO#reporteRetroactivoStart(String, String)
     * @version 1.0
     * @throws SQLException Si se presenta un error con la Base de Datos
     */
    public void reporteRetroactivoStart(String fechaI, String fechaF)throws SQLException{
        dao.reporteRetroactivoStart(fechaI, fechaF);
    }
    
    /**
     * Obtiene el valor del retroactivo de la ruta determinada
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param ruta Ruta del estandar
     * @see com.tsp.operation.model.DAOS.ReporteRetroactivoDAO#obtenerValorRetroactivo(String)
     * @version 1.0
     * @throws SQLException Si se presenta un error con la Base de Datos
     */
    public float obtenerValorRetroactivo(String ruta)throws SQLException{
        return dao.obtenerValorRetroactivo(ruta);
    }
    
    /**
     * Realiza la consulta de la obtenci�n de los viajes 'directos'
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @version 1.0
     * @see com.tsp.operation.model.DAOS.ReporteRetroactivoDAO#clasificadosDirectos()
     * @throws SQLException Si se presenta un error con la Base de Datos
     */
    public Vector clasificadosDirectos() throws SQLException{
        Vector vec = dao.obtenerClasificadosDirectos();
        Vector directos = new Vector();
        
        String ult_prop = "";
        String ult_placa = "";
        String ult_nit = "";
        double total = 0;
        double subttl = 0;
        int ttl_vjs = 0;
        double ttl_tons = 0;
        
        float retro_jagua = this.obtenerValorRetroactivo("JAGUA");
        float retro_loma = this.obtenerValorRetroactivo("LOMA");
        float retro_carbosan = this.obtenerValorRetroactivo("CARBOSAN");
        
        Hashtable ht = new Hashtable();
        
        ht.put("retro_jagua", String.valueOf(retro_jagua));
        ht.put("retro_loma", String.valueOf(retro_loma));
        ht.put("retro_carbosan", String.valueOf(retro_carbosan));
        
        directos.add(ht);
        
        if( vec.size()>0 ){
            ReporteRetroactivo ln = (ReporteRetroactivo) vec.elementAt(0);
            ult_prop = ln.getPropietario();
            ult_placa = ln.getPlaca();
            ult_nit = ln.getNit();
            ht = new Hashtable();
            ht.put("placa", ult_placa);
            ht.put("propietario", ult_prop);
            ht.put("nit", ult_nit);
            
            //ht.put("",);
            
            for(int i=0; i<vec.size(); i++){
                ln = new ReporteRetroactivo();
                ln = (ReporteRetroactivo) vec.elementAt(i);         
                
                if ( !( ult_prop.matches(ln.getPropietario()) && ult_placa.matches(ln.getPlaca())) ){
                    ht.put("ttl_vjs", String.valueOf(ttl_vjs));
                    ht.put("ttl_tons", String.valueOf(ttl_tons));
                    ht.put("total", String.valueOf(total));
                    directos.add(ht);
                    ht = new Hashtable();
                    
                    total = 0;
                    ttl_vjs = 0;
                    ttl_tons = 0;
                    
                    ult_prop = ln.getPropietario();
                    ult_placa = ln.getPlaca();
                    ult_nit = ln.getNit();
                    
                    ht.put("placa", ult_placa);
                    ht.put("propietario", ult_prop);
                    ht.put("nit", ult_nit);
                }
                
                if( ln.getRuta().matches("LOMA") ){
                    subttl = com.tsp.util.Util.redondear(ln.getToneladas()*retro_loma, 2);
                    ttl_vjs += ln.getViajes();
                    ttl_tons += ln.getToneladas();
                    total += subttl;
                    
                    ht.put("viajes_loma", String.valueOf(ln.getViajes()));
                    ht.put("tons_loma", String.valueOf(ln.getToneladas()));
                    ht.put("ttl_loma", String.valueOf(subttl));
                } else if( ln.getRuta().matches("JAGUA") ){
                    subttl = com.tsp.util.Util.redondear(ln.getToneladas()*retro_jagua, 2);
                    ttl_vjs += ln.getViajes();
                    ttl_tons += ln.getToneladas();
                    total += subttl;
                    
                    ht.put("viajes_jagua", String.valueOf(ln.getViajes()));
                    ht.put("tons_jagua", String.valueOf(ln.getToneladas()));
                    ht.put("ttl_jagua", String.valueOf(subttl));
                } else if( ln.getRuta().matches("CARBOSAN") ){
                    subttl = com.tsp.util.Util.redondear(ln.getToneladas()*retro_carbosan, 2);
                    ttl_vjs += ln.getViajes();
                    ttl_tons += ln.getToneladas();
                    total += subttl;
                    
                    ht.put("viajes_carbosan", String.valueOf(ln.getViajes()));
                    ht.put("tons_carbosan", String.valueOf(ln.getToneladas()));
                    ht.put("ttl_carbosan", String.valueOf(subttl));
                }
            }
            
            ht.put("ttl_vjs", String.valueOf(ttl_vjs));
            ht.put("ttl_tons", String.valueOf(ttl_tons));
            ht.put("total", String.valueOf(total));
            
            directos.add(ht);
        }        
        
        return directos;
    }
    
    /**
     * Realiza la consulta de la obtenci�n de los viajes 'intermediarios'
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @version 1.0
     * @see com.tsp.operation.model.DAOS.ReporteRetroactivoDAO#obtenerClasificadosIntermediarios()
     * @throws SQLException Si se presenta un error con la Base de Datos
     */
    public Vector clasificadosIntermediarios() throws SQLException{
        Vector vec = dao.obtenerClasificadosIntermediarios();
        Vector intermedios = new Vector();
        
        String ult_prop = "";
        String ult_placa = "";
        String ult_nit = "";
        double total = 0;
        double subttl = 0;
        int ttl_vjs = 0;
        double ttl_tons = 0;
        
        float retro_jagua = this.obtenerValorRetroactivo("JAGUA");
        float retro_loma = this.obtenerValorRetroactivo("LOMA");
        float retro_carbosan = this.obtenerValorRetroactivo("CARBOSAN");
        
        Hashtable ht = new Hashtable();
        
        ht.put("retro_jagua", String.valueOf(retro_jagua));
        ht.put("retro_loma", String.valueOf(retro_loma));
        ht.put("retro_carbosan", String.valueOf(retro_carbosan));
        
        intermedios.add(ht);
        
        if( vec.size()>0 ){
            ReporteRetroactivo ln = (ReporteRetroactivo) vec.elementAt(0);
            ult_prop = ln.getPropietario();
            ult_placa = ln.getPlaca();
            ult_nit = ln.getNit();
            ht = new Hashtable();
            ht.put("placa", ult_placa);
            ht.put("propietario", ult_prop);
            ht.put("nit", ult_nit);
            
            //ht.put("",);
            
            for(int i=0; i<vec.size(); i++){
                ln = new ReporteRetroactivo();
                ln = (ReporteRetroactivo) vec.elementAt(i);         
                
                if ( !( ult_prop.matches(ln.getPropietario()) && ult_placa.matches(ln.getPlaca())) ){
                    ht.put("ttl_vjs", String.valueOf(ttl_vjs));
                    ht.put("ttl_tons", String.valueOf(ttl_tons));
                    ht.put("total", String.valueOf(total));
                    intermedios.add(ht);
                    ht = new Hashtable();
                    
                    total = 0;
                    ttl_vjs = 0;
                    ttl_tons = 0;
                    
                    ult_prop = ln.getPropietario();
                    ult_placa = ln.getPlaca();
                    ult_nit = ln.getNit();
                    
                    ht.put("placa", ult_placa);
                    ht.put("propietario", ult_prop);
                    ht.put("nit", ult_nit);
                }
                
                if( ln.getRuta().matches("LOMA") ){
                    subttl = com.tsp.util.Util.redondear(ln.getToneladas()*retro_loma, 2);
                    ttl_vjs += ln.getViajes();
                    ttl_tons += ln.getToneladas();
                    total += subttl;
                    
                    ht.put("viajes_loma", String.valueOf(ln.getViajes()));
                    ht.put("tons_loma", String.valueOf(ln.getToneladas()));
                    ht.put("ttl_loma", String.valueOf(subttl));
                } else if( ln.getRuta().matches("JAGUA") ){
                    subttl = com.tsp.util.Util.redondear(ln.getToneladas()*retro_jagua, 2);
                    ttl_vjs += ln.getViajes();
                    ttl_tons += ln.getToneladas();
                    total += subttl;
                    
                    ht.put("viajes_jagua", String.valueOf(ln.getViajes()));
                    ht.put("tons_jagua", String.valueOf(ln.getToneladas()));
                    ht.put("ttl_jagua", String.valueOf(subttl));
                } else if( ln.getRuta().matches("CARBOSAN") ){
                    subttl = com.tsp.util.Util.redondear(ln.getToneladas()*retro_carbosan, 2);
                    ttl_vjs += ln.getViajes();
                    ttl_tons += ln.getToneladas();
                    total += subttl;
                    
                    ht.put("viajes_carbosan", String.valueOf(ln.getViajes()));
                    ht.put("tons_carbosan", String.valueOf(ln.getToneladas()));
                    ht.put("ttl_carbosan", String.valueOf(subttl));
                }
            }
            
            ht.put("ttl_vjs", String.valueOf(ttl_vjs));
            ht.put("ttl_tons", String.valueOf(ttl_tons));
            ht.put("total", String.valueOf(total));
            
            intermedios.add(ht);
        }        
        
        return intermedios;
    }
    
    /**
     * Realiza la consulta de la obtenci�n de los viajes unicos y repetidos
     * @autor Ing. Leonardo Parodi
     * @see com.tsp.operation.model.DAOS.ReporteRetroactivoDAO#reporteRetroactivoUnicosRepetidos()
     * @version 1.0
     * @throws SQLException Si se presenta un error con la Base de Datos
     */
    public void reporteRetroactivoUnicosRepetidos() throws SQLException{
            dao.reporteRetroactivoUnicosRepetidos();
    }
    
    /**
     * Retorna el valor de la propiedad Unicos
     * @autor Ing. Leonardo Parodi
     * @see com.tsp.operation.model.DAOS.ReporteRetroactivoDAO#Unicos()
     * @version 1.0
     */
    public Vector Unicos(){
            return dao.getUnicos();
    }
    
    /**
     * Retorna el valor de la propiedad Repetidos
     * @autor Ing. Leonardo Parodi
     * @see com.tsp.operation.model.DAOS.ReporteRetroactivoDAO#getRepetidos()
     * @version 1.0
     */
    public Vector Repetidos(){
            return dao.getRepetidos();
    }

}
