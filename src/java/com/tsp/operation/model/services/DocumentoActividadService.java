/********************************************************************
 *      Nombre Clase.................   DocumentoActividadService.java    
 *      Descripci�n..................   Service de la tabla tblcat_doc    
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   19.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  Andres
 */
public class DocumentoActividadService {
        private DocumentoActividadDAO act_doc;
        private String varSeparadorJS = "~";
        private String  varCamposJS;
        private String docselected = "";
        
        /** Creates a new instance of DocumentoActividadService */
        public DocumentoActividadService() {
                this.act_doc = new DocumentoActividadDAO();
        }
        
        /**
         * Obtiene una lista registros por documento
         * @autor Tito Andr�s Maturana
         * @param distrito Distrito
         * @param documento documento C�digo del documento
         * @throws SQLException
         * @version 1.0
         */
        public Vector obtenerActividadesDocumento(String distrito, String doc)
                throws SQLException{
                        
                this.act_doc.obtenerActividadesDocumento(distrito, doc);
                return this.act_doc.getAct_docs();
        }
        
        /**
         * Genera una cadena de caracteres de la declaraci�n de
         * un arreglo en lenguaje JavaScript a partir del listado de
         * documentod de un distrito.
         * @autor Tito Andr�s Maturana
         * @param distrito Distrito de las aplicaciones.
         * @param doc C�digo del documento.
         * @throws SQLException
         * @version 1.0
         */
        public void GenerarJSCampos(String distrito, String doc) 
                throws SQLException{
                        
                TBLActividadesDAO actDAO = new TBLActividadesDAO();
                DocumentoDAO docDAO = new DocumentoDAO();
                String var = "\n var CamposJSACT = [ ";
                Vector acts = this.obtenerActividadesDocumento(distrito, doc);
                if (acts!=null)
                        for (int i=0; i<acts.size(); i++){
                                DocumentoActividad act_doc = (DocumentoActividad) acts.elementAt(i);
                                TBLActividades act = actDAO.ConsultarTBLActividad(act_doc.getActividad(), act_doc.getDistrito());
                                var += "\n '" + act.getCodigo() + varSeparadorJS + act.getDescripcion() +"',";
                        }
                var = var.substring(0,var.length()-1) + "];";
                var += "\n var CamposJSDoc = [ ";
                docDAO.obtenerDocumento(distrito, this.getDocselected());                
                Documento docc = docDAO.getDoc();
                var += "\n '" + docc.getC_document_type() + varSeparadorJS + docc.getC_document_name() +"',";
                var = var.substring(0,var.length()-1) + "];";
                docDAO.setDoc(null);
                
                this.varCamposJS = var;
        }
        
        /**
         * Inserta un nuevo registro.
         * @autor Tito Andr�s Maturana
         * @param obj Objeto de la clase DocumentoActividad a ingresar
         * @throws SQLException
         * @version 1.0
         */
        public void ingresarDocumentoActividad(DocumentoActividad obj)
                throws SQLException{
                
                this.act_doc.setAct_doc(obj);
                this.act_doc.ingresarActividadeDocumento();
        }
        
        /**
         * Actualiza un registro.
         * @autor Tito Andr�s Maturana
         * @param obj Objeto de la clase DocumentoActividad a actualizar
         * @throws SQLException
         * @version 1.0
         */
        public void actualizarDocumentoActividad(DocumentoActividad obj)
                throws SQLException{
                
                this.act_doc.setAct_doc(obj);
                this.act_doc.updateActividadeDocumento();
        }
        
        /**
         * Estblece si un registro a existe
         * @autor Tito Andr�s Maturana
         * @param distrito Distrito
         * @param documento documento C�digo del documento
         * @param documento actividad C�digo de la actividad
         * @throws SQLException
         * @version 1.0
         */
        public boolean existeActividadDocumento(String cia, String doc, String act)
                throws SQLException{
        
                return this.act_doc.existeActividadDocumento(cia, doc, act);
        }
        
        /**
         * Obtiene un registro
         * @autor Tito Andr�s Maturana
         * @param distrito Distrito
         * @param documento documento C�digo del documento
         * @param documento actividad C�digo de la actividad
         * @throws SQLException
         * @version 1.0
         */
        public DocumentoActividad obtenerActividadDocumento(String cia, String doc, String act)
                throws SQLException{
                
                this.act_doc.obtenerActividadDocumento(cia, doc, act);
                return this.act_doc.getAct_doc();
        }
        
        
        /**
         * Getter for property varCamposJS.
         * @return Value of property varCamposJS.
         */
        public java.lang.String getVarCamposJS() {
                return varCamposJS;
        }
        
        /**
         * Setter for property varCamposJS.
         * @param varCamposJS New value of property varCamposJS.
         */
        public void setVarCamposJS(java.lang.String varCamposJS) {
                this.varCamposJS = varCamposJS;
        }
        
        /**
         * Getter for property docselected.
         * @return Value of property docselected.
         */
        public java.lang.String getDocselected() {
                return docselected;
        }        
        
        /**
         * Setter for property docselected.
         * @param docselected New value of property docselected.
         */
        public void setDocselected(java.lang.String docselected) {
                this.docselected = docselected;
        }
        
}
