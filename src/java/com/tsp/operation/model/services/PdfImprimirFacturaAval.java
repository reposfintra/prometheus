/*************************************************************************
 * Nombre Clase ............. PdfImprimirFacturaAval.java
 * Descripci�n .. . . . . . . FV- 210 Impresion de Factura de Aval
 * Autor . . . . . . . . . .  Ing. Jose Avila
 * Fecha . . . . . . . . . .  27/07/2012
 * versi�n . . . . . . . . .  1.0
 * Copyright................  Fintra S.A.
************************************************************************/
package com.tsp.operation.model.services;

import com.aspose.cells.Column;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.html.HtmlTags;
import com.lowagie.text.*;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.html.simpleparser.StyleSheet;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.DAOS.NegociosDAO;
import com.tsp.operation.model.beans.*;
import java.util.*;
import java.io.*;

import java.awt.Color;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;


/**
 *
 * @author Ing. Jose Avila
 */
public class PdfImprimirFacturaAval {
    
    int diay = 0;
    int mesy = 0;
    int anoy = 0;
    int enum_letras = 0;
    String ecc_representante = "";
    String erepresentada = "";
    String ecc_deudor = "";
    String ecc_codeudor = "";
    String eswcodeudor = "";
    Document document;
    int n = 0;
    int contador = 0;
    int cantidad_de_letras = 7;
    PdfPTable table;
    float[] widths = {0.12f, 0.17f, 0.1f, 0.27f, 0.17f, 0.17f};
    float[] widthsCabecera = {0.2f, 0.3f, 0.15f, 0.35f};
    BaseFont bf;
    Font fuente_normal = new Font(bf, 9);
    Font fuente_normal_grande = new Font(bf, 20);
    Font fuente_negrita_grande = new Font(bf, 20f);
    Font fuente_negrita8 = new Font(bf, 9);
    Font fuente_normal_media = new Font(bf, 14);//jemartinez
    Font fuente_negrita_media = new Font(bf, 14);//jemartinez
    PdfWriter writer;
    Rectangle page;
    PdfPCell linea_blanco;
    SimpleDateFormat fmt = null;
    String ruta = "";

    //Variables de datos
    String orden = "";
    Model model;
    File pdf;
    Model modelito;
    BeanGeneral bg;
    String x = "xxx";
    Phrase texto_principal;
    String no_aprobacion = "", ciudad = "", fecha_otorgamiento = "", fecha_vencimiento = "", consecutivo = "", digito = "", nombre = "", cedula = "", ciudad_cedula = "", representado = "", nombre_codeudor = "", cedula_codeudor = "", dia_pagare = "", mes_pagare = "", ano_pagare = "", empresa = "", plata_letra = "", plata_numero = "", empresa_facultada = "", ciudad_pago = "", cedula_deudor = "", cedula_firma_codeudor = "";
    String fecha_otorgamiento_inicial = "";//mar14d8
    java.util.Calendar Fecha_Negocio;
    java.util.Calendar Fecha_Temp;
    ResourceBundle rb;
    Phrase frase;
    String fecha_facturacion = "9999-00-00";
    //Phrase frase2;
    
    private Font cNormal    = new Font(Font.getStyleValue("Calibri"),4);
    
    
     SerieGeneral serie= new SerieGeneral(); 
     SerieGeneralService serieGeneralService;
     Usuario usuario;  
     NegociosGenService Negociossvc;
     String nf="";

    /**
     * Creates a new instance of PdfImprimirFacturaAval
     */
    public PdfImprimirFacturaAval() throws Exception {
        bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        fuente_negrita_grande.setStyle(com.lowagie.text.Font.BOLD);
        fuente_negrita8.setStyle(com.lowagie.text.Font.BOLD);
        fuente_negrita_media.setStyle(com.lowagie.text.Font.BOLD); //jemartinez
        serieGeneralService = new SerieGeneralService() ;
        Negociossvc = new NegociosGenService();
    }
    public PdfImprimirFacturaAval(String dataBaseName) throws Exception {
        bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        fuente_negrita_grande.setStyle(com.lowagie.text.Font.BOLD);
        fuente_negrita8.setStyle(com.lowagie.text.Font.BOLD);
        fuente_negrita_media.setStyle(com.lowagie.text.Font.BOLD); //jemartinez
        serieGeneralService = new SerieGeneralService(dataBaseName);
        Negociossvc = new NegociosGenService(dataBaseName);
    }

  

    public void generarPdfFacturaAval( Negocios negocio, SolicitudPersona sp,  NitSot objnit, SolicitudAval aval, TablaGen tbconceptos, Usuario usuario, Convenio convenio) {
        try {


            try {
              
                this.usuario=usuario;
                bf = BaseFont.createFont("Helvetica", "Cp1252", false);
                rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                ruta = (new StringBuilder()).append(rb.getString("ruta")).append("/pdf/FacturaAval" + ".pdf").toString();

                Document documento = new Document();
                PdfWriter.getInstance(documento, new FileOutputStream(ruta));
                com.lowagie.text.Rectangle page = documento.getPageSize();
                

                 
                //documento.setFooter(this.getMyFooter());

                documento.open();
                documento.newPage();

                if (negocio.getFecha_factura_aval() != null  &&  !negocio.getFecha_factura_aval().equals(""))
                   this.fecha_facturacion = negocio.getFecha_factura_aval().substring(0 , 10);
                else {
                    Date date = new Date();
                    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
                    this.fecha_facturacion=s.format(date);
                    //fecha_facturacion =s.format(sumaDias(date, 30));// 30 dias a la fecha
                    Negociossvc.ActualizaFechaFacturaAval(negocio.getCod_negocio(), fecha_facturacion);
                }

                //this.fecha_facturacion = negocio.getFecha_liquidacion().substring(0, 10);
               /* if (!convenio.isMediador_aval()) {
                    if (negocio.getEstado().equals("V") || negocio.getEstado().equals("A") || negocio.getEstado().equals("T")) {
                        this.fecha_facturacion = negocio.getFecha_ap().substring(0, 10);
                    }
                }*/

          
            
           /*----------------------------------Creacion de la tabla Contenedora 1--------------------------------------*/
            PdfPTable tabla_datos = this.TablaContenedoraHeader(negocio);
            tabla_datos.setWidthPercentage(100);
            documento.add(tabla_datos);
            documento.add(Chunk.NEWLINE);
            documento.add(Chunk.NEWLINE);
            
            /*----------------------------------Creacion de la tabla Contenedora Separa al Detalle--------------------------------------*/
            PdfPTable tabla_datosSepDetalle1 = this.TablaContenedoraSeparaDetalle();
            tabla_datosSepDetalle1.setWidthPercentage(100);
            documento.add(tabla_datosSepDetalle1);
            documento.add(Chunk.NEWLINE);
            documento.add(Chunk.NEWLINE);
            
                
            /*----------------------------------Creacion de la tabla Contenedora 2--------------------------------------*/
            PdfPTable tabla_datos2 = this.TablaContenedora(sp, objnit, aval, negocio);
            tabla_datos2.setWidthPercentage(100);
            documento.add(tabla_datos2);
            documento.add(Chunk.NEWLINE);
            documento.add(Chunk.NEWLINE);
            
             /*----------------------------------Creacion de la tabla Contenedora Separa al Detalle--------------------------------------*/
            PdfPTable tabla_datosSepDetalle = this.TablaContenedoraSeparaDetalle();
            tabla_datosSepDetalle.setWidthPercentage(100);
            documento.add(tabla_datosSepDetalle);
            documento.add(Chunk.NEWLINE);
            documento.add(Chunk.NEWLINE);
            
             /*----------------------------------Creacion de la tabla Contenedora Titulos del Detalle--------------------------------------*/
            PdfPTable tabla_datosTituDetalle = this.TablaContenedoraTitulosDetalle();
            tabla_datosTituDetalle.setWidthPercentage(100);
            documento.add(tabla_datosTituDetalle);
            documento.add(Chunk.NEWLINE);
            documento.add(Chunk.NEWLINE);
            
            
           /*----------------------------------Creacion de la tabla Contenedora Detalle--------------------------------------*/
            PdfPTable tabla_datosDetalle = this.TablaContenedoraDetalle(negocio, tbconceptos);
            tabla_datosDetalle.setWidthPercentage(100);
            documento.add(tabla_datosDetalle);
            documento.add(Chunk.NEWLINE);
            documento.add(Chunk.NEWLINE);
            
            
            /*----------------------------------Creacion del parrafo final--------------------------------------*/
            
            Font fuente = new Font(Font.TIMES_ROMAN, 7);
            com.lowagie.text.Paragraph p= new com.lowagie.text.Paragraph("Por haber recibido a satisfacci�n la prestaci�n del servicio contratado, aceptamos esta factura en la forma y t�rminos del libramiento. La presente factura de venta tiene car�cter de titulo valor y se rige por la ley 1231 de julio 17 / 2008.",fuente);
            p.setAlignment(com.lowagie.text.Paragraph.ALIGN_LEFT);
            documento.add(p);
          
            documento.add(Chunk.NEWLINE);
                
            if (usuario.getId_agencia().equals("OP")) {

                    p = new com.lowagie.text.Paragraph("FENALCO es una entidad sin �nimo de lucro de car�cter gremial, no sujeta a las retenciones en la fuente (seg�n numeral 1 y 3 del Art�culo 19 del E.T., Ley 633 del 29 de diciembre de 2000), no somos contribuyentes  del impuesto de Industria y Comercio seg�n lo dispuesto por el Art. 65 del acuerdo 015 de 2001 del concejo distrital de Barranquilla. (Estatuto Tributario del Distrito Especial, Industrial y Portuario de Barranquilla. ", fuente);

                } else {

                    p = new com.lowagie.text.Paragraph("FENALCO es una entidad sin �nimo de lucro de car�cter gremial, no sujeta a las retenciones en la fuente (seg�n numeral 1 y 3 del Art�culo 19 del E.T., Ley 633 del 29 de diciembre de 2000), no somos contribuyentes  del impuesto de Industria y Comercio seg�n lo dispuesto por el Art. 95 del acuerdo 041 de 2006 del concejo distrital de Cartagena  D. T. y C. (Estatuto Tributario del Distrito Tur�stico y Cultural de Cartagena). ", fuente);
                }

            p.setAlignment(com.lowagie.text.Element.ALIGN_LEFT);
            documento.add(p);
            
            documento.add(Chunk.NEWLINE);
            p = new com.lowagie.text.Paragraph("NO SOMOS RETENEDORES DEL IMPUESTO A LAS VENTAS", fuente);
            p.setAlignment(com.lowagie.text.Element.ALIGN_LEFT);
            documento.add(p);
            
            documento.add(Chunk.NEWLINE);
            p = new com.lowagie.text.Paragraph("Observaciones", fuente);
            p.setAlignment(com.lowagie.text.Element.ALIGN_LEFT);
            documento.add(p);

            
                
            
           /*----------------------------------Creacion de la tabla Observaciones --------------------------------------*/ 
            PdfPTable tabla_temp = new PdfPTable(1);

            tabla_temp.setWidthPercentage(100);
            documento.add(tabla_temp);
            documento.add(Chunk.NEWLINE);

            PdfPCell celda_temp = new PdfPCell();

            // fila 1
            celda_temp = new PdfPCell();
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            
            // agrego tabla 1
            celda_temp.setBorderWidthTop(0.5f);
            celda_temp.setBorderWidthLeft(0.5f);
            celda_temp.setBorderWidthRight(0.5f);
            celda_temp.setBorderWidthBottom(0.5f);
           
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("\n\n\n\n\n\n\n\n",fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);
            documento.add(tabla_temp);

        
            /*----------------------------------Creacion del Firma final--------------------------------------*/
            
            com.lowagie.text.Paragraph firma= new com.lowagie.text.Paragraph("\n\n\n__________________________________________\nFIRMA DE ACEPTACI�N\nC.C. No. ________________ de ________________",fuente);
            firma.setAlignment(com.lowagie.text.Paragraph.ALIGN_RIGHT);
            documento.add(firma);
            
            

            documento.close();
          
            
             } catch (Exception e) {  
                 e.printStackTrace();                System.out.println("ERROR en generarPdfNuevoPagare" + e.toString() + "__" + e.getMessage());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("error en servicillo__" + ex.toString() + "__" + ex.getMessage());
        }
    }
    
    
   
   
    
    
  /************************************** CREACION TABLA CONTENEDORA 1 PRINCIPAL ***************************************************/
    protected PdfPTable TablaContenedoraHeader(Negocios negocio) throws DocumentException, SQLException, BadElementException, MalformedURLException, IOException, Exception {

        String fecha = Util.getFechahoy();
        PdfPTable tabla_temp = new PdfPTable(4);

        float[] medidaCeldas = {0.200f, 0.350f, 0.200f, 0.250f};
        tabla_temp.setWidths(medidaCeldas);
        
        Font fuente = new Font(Font.HELVETICA, 9);
        Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();



        // fila 1
        celda_temp = new PdfPCell();
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.addElement(this.tablaLogo());// agrego tabla del logo
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        tabla_temp.addCell(celda_temp);
 
        
        celda_temp = new PdfPCell();
        celda_temp.addElement(this.tablaFenalco());// agrego tabla 2 Fenalco
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        tabla_temp.addCell(celda_temp);
        
        celda_temp = new PdfPCell();
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.addElement(this.tablaResponsable());// agrego tabla 3 responsable
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        tabla_temp.addCell(celda_temp);
        
        celda_temp = new PdfPCell();
        celda_temp.addElement(this.tablaFactura(negocio)); // agrego tabla 4 factura
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        tabla_temp.addCell(celda_temp);



        return tabla_temp;
    }

 
    
    
    
    
 /************************************** DATOS TABLA 1 LOGO ***************************************************/
    protected PdfPTable tablaLogo() throws DocumentException, SQLException, BadElementException, MalformedURLException, IOException {


        String fecha = Util.getFechahoy();
        PdfPTable tabla_temp = new PdfPTable(1);
       
        java.awt.Color color_fuente_header = new java.awt.Color(0xFF, 0xFF, 0xFF);

        Font fuente_header = new Font(Font.TIMES_ROMAN, 9, Font.BOLD);
        fuente_header.setColor(color_fuente_header);

        Font fuente = new Font(Font.TIMES_ROMAN, 9);
        Font fuenteB = new Font(Font.TIMES_ROMAN, 9, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();
        
        //validamos por la agencia el logo que debe aparecer 
        String url_logo = "";
        if (usuario.getId_agencia().equals("OP")) {
            url_logo = "fenalco_atlantico.jpg";
        } else {
            url_logo = "fenalco_bolivar.jpg";
        }
            com.lowagie.text.Image img = com.lowagie.text.Image.getInstance(rb.getString("ruta") + "/images/" + url_logo);
            img.scaleToFit(60, 60);
            img.setBorder(1);
           


        celda_temp = new PdfPCell();
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setPhrase(new Phrase(" ", fuente_header));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda_temp.addElement(img);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(40);
            
        return tabla_temp;
    }
    
     /************************************** DATOS TABLA 2 FENALCO***************************************************/
 
        protected PdfPTable tablaFenalco() throws DocumentException, SQLException {


        String fecha = Util.getFechahoy();
        PdfPTable tabla_temp = new PdfPTable(1);
       
        java.awt.Color color_fuente_header = new java.awt.Color(0xAA, 0xAA, 0xAA);

  
        float[] medidaCeldas = {0.1000f};
        tabla_temp.setWidths(medidaCeldas);

        Font fuente = new Font(Font.TIMES_ROMAN, 6);
        Font fuenteB = new Font(Font.TIMES_ROMAN, 6, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();


        celda_temp = new PdfPCell();
            //Encabezado para fenalco.            
            if (usuario.getId_agencia().equals("OP")) {
                celda_temp.setPhrase(new Phrase("FEDERACION NACIONAL DE COMERCIANTES\n FENALCO ATLANTICO ", fuente));
            } else {
                celda_temp.setPhrase(new Phrase("FEDERACION NACIONAL DE COMERCIANTES\n FENALCO BOLIVAR ", fuente));
            }

        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        
        
        celda_temp = new PdfPCell();
        
            //Informacion genera de la oficina
            if (usuario.getId_agencia().equals("OP")) {
                celda_temp.setPhrase(new Phrase("\nNIT.: 890.100.985-8 R�GIMEN COM�N\n CALLE 72 # 61-07  PBX: 3531448 A.A. 150\n"
                        + "EMAIL: facturacion@fenalcoatlantico.com.co\n"
                        + "BARRANQUILLA - COLOMBIA ", fuente));
            } else {
                celda_temp.setPhrase(new Phrase("\nNIT.: 890.480.024-4 R�GIMEN COM�N\n Centro, Edif. Banco del Estado, P 14, La Matuna.\n PBX: 6641056, 6646028\n"
                        + "EMAIL: facturacion@fenalcobolivar.com\n"
                        + "CARTAGENA - COLOMBIA ", fuente));
            }
       
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        return tabla_temp;
    }
    
  
    /************************************** DATOS TABLA 2 FENALCO***************************************************/
        protected PdfPTable tablaResponsable() throws DocumentException, SQLException {


        String fecha = Util.getFechahoy();
        PdfPTable tabla_temp = new PdfPTable(1);
       
        java.awt.Color color_fuente_header = new java.awt.Color(0xAA, 0xAA, 0xAA);

  
        float[] medidaCeldas = {0.1000f};
        tabla_temp.setWidths(medidaCeldas);

        Font fuente = new Font(Font.TIMES_ROMAN, 6);
        Font fuenteB = new Font(Font.TIMES_ROMAN, 6, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();


        celda_temp = new PdfPCell();
        //Actividad economica
            if (usuario.getId_agencia().equals("OP")) {
                celda_temp.setPhrase(new Phrase("Responsable\nIVA R�gimen Com�n\nActividad Econ�mica 9411\n Resolucion DIAN No. 20000170190\n Fecha: 17/07/2014" + "\nNumeracion Autorizada Del 4841 al 100000", fuente));
            } else {
                celda_temp.setPhrase(new Phrase("Responsable\nIVA R�gimen Com�n\nActividad Econ�mica 9411\n Resolucion DIAN No. 60000091959\n Fecha: 26/05/2015" + "\nFactura Por Computador\n Numeracion Autorizada Del 750 al 100000", fuente));
            } 
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        
        
       
        return tabla_temp;
    }
    
  
    /************************************** DATOS TABLA FAACTURA VENTA***************************************************/
        protected PdfPTable tablaFactura(Negocios negocio) throws DocumentException, SQLException, Exception {
        
  

            nf = Negociossvc.getNumeroFacVentaAval(negocio.getCod_negocio());
            if (nf == null || nf.equals("")) {
                serie = serieGeneralService.getSerie(usuario.getCia(), usuario.getId_agencia(), "FAF");
                nf = serie.getUltimo_prefijo_numero();
                serieGeneralService.setSerie(usuario.getCia(), usuario.getId_agencia(), "FAF"); //Incrementa el numero de ingreso
                Negociossvc.actualizaNumeroFacVentaAval(nf, negocio.getCod_negocio());
            }

            

     
        PdfPTable tabla_temp = new PdfPTable(1);      
 

  
        float[] medidaCeldas = {0.1000f};
        tabla_temp.setWidths(medidaCeldas);

        Font fuente = new Font(Font.TIMES_ROMAN, 6);
        Font fuenteB = new Font(Font.TIMES_ROMAN, 6, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();


        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("FACTURA DE VENTA\n No. "+nf, fuenteB));
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        
        
       
        return tabla_temp;
    }
    
   /************************************** DIBUJO LA TABLA CONTENEDORA 2 ***************************************************/
    protected PdfPTable TablaContenedora(SolicitudPersona sp, NitSot objnit, SolicitudAval aval, Negocios negocio) throws DocumentException, SQLException, Exception {

        String fecha = Util.getFechahoy();
        PdfPTable tabla_temp = new PdfPTable(4);

        float[] medidaCeldas = {0.350f, 0.190f, 0.270f, 0.190f};
        tabla_temp.setWidths(medidaCeldas);
        
        Font fuente = new Font(Font.HELVETICA, 9);
        Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();



        // fila 1
        celda_temp = new PdfPCell();
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.addElement(this.tabla1(sp, objnit));// agrego tabla 1
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0.5f);
        celda_temp.setBorderWidthRight(0.5f);
        celda_temp.setBorderWidthBottom(0.5f); 
        tabla_temp.addCell(celda_temp);
 
        
        celda_temp = new PdfPCell();
        celda_temp.addElement(this.tabla2(aval, negocio));// agrego tabla 2
        celda_temp.setHorizontalAlignment(PdfPCell.RIGHT);
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0.5f);
        celda_temp.setBorderWidthRight(0.5f);
        celda_temp.setBorderWidthBottom(0.5f); 
        tabla_temp.addCell(celda_temp);
        
        celda_temp = new PdfPCell();
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.addElement(this.tabla3(negocio));// agrego tabla 3
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0.5f);
        celda_temp.setBorderWidthRight(0.5f);
        celda_temp.setBorderWidthBottom(0.5f); 
        tabla_temp.addCell(celda_temp);
        
        celda_temp = new PdfPCell();
        celda_temp.addElement(this.tabla4(negocio)); // agrego tabla 4
        celda_temp.setHorizontalAlignment(PdfPCell.RIGHT);
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0.5f);
        celda_temp.setBorderWidthRight(0.5f);
        celda_temp.setBorderWidthBottom(0.5f); 
        tabla_temp.addCell(celda_temp);



        return tabla_temp;
    }

    
    
 /************************************** DATOS TABLA 1***************************************************/
    protected PdfPTable tabla1( SolicitudPersona sp, NitSot objnit) throws DocumentException, SQLException {



        PdfPTable tabla_temp = new PdfPTable(1);
       
        java.awt.Color color_fuente_header = new java.awt.Color(0xAA, 0xAA, 0xAA);

        Font fuente_header = new Font(Font.TIMES_ROMAN, 7, Font.BOLD);
            fuente_header.setColor(color_fuente_header);
            
        Font fuente = new Font(Font.TIMES_ROMAN, 6);
        Font fuenteB = new Font(Font.HELVETICA, 6, Font.BOLD);

        float[] medidaCeldas = {0.1000f};
        tabla_temp.setWidths(medidaCeldas);

        PdfPCell celda_temp = new PdfPCell();

        celda_temp = new PdfPCell();
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setPhrase(new Phrase(sp.getNombre()+"\n"+sp.getTipoId()+". "+sp.getIdentificacion()+"\n"+sp.getDireccion()+"\n"+sp.getTelefono()+"", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
            
        return tabla_temp;
    }

    
    
        protected PdfPTable tabla2(SolicitudAval aval, Negocios negocio) throws DocumentException, SQLException {

          
        
                String anio = fecha_facturacion.substring(0, 4);//a�o
                String dia = fecha_facturacion.substring(8, 10);//dia
                String mes = fecha_facturacion.substring(5, 7);//mes
             
                
       // String fecha = Util.getFechahoy();
        PdfPTable tabla_temp = new PdfPTable(1);
       
        java.awt.Color color_fuente_header = new java.awt.Color(0xAA, 0xAA, 0xAA);

        Font fuente_header = new Font(Font.TIMES_ROMAN, 8, Font.BOLD);
        fuente_header.setColor(color_fuente_header);
        
        Font fuente = new Font(Font.TIMES_ROMAN, 6);
        Font fuenteB = new Font(Font.HELVETICA, 6, Font.BOLD);

        float[] medidaCeldas = {0.1000f};
        tabla_temp.setWidths(medidaCeldas);


        PdfPCell celda_temp = new PdfPCell();

        //fila 1
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("FECHA FACTURA", fuente));
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(dia+"  "+mes+"  "+anio+"  ", fuente));
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
       
             
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("DIA "+"MES  "+" A�O", fuente));
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        
        
        // fila 2
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("", fuente));
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        return tabla_temp;
    }
    
        
        
        
        
        
        
        
        
        
            
        protected PdfPTable tabla3(Negocios negocio) throws DocumentException, SQLException {


            
 

        //String fecha = "30-07-2012"; //aval.getCreationDate().substring(0, 10);
        String anio = fecha_facturacion.substring(0, 4);//a�o
        String dia = fecha_facturacion.substring(8, 10);//dia
        String mes = fecha_facturacion.substring(5, 7);//mes

                
        
        //String fecha = Util.getFechahoy();
        PdfPTable tabla_temp = new PdfPTable(6);
       
        java.awt.Color color_fuente_header = new java.awt.Color(0xAA, 0xAA, 0xAA);

        Font fuente_header = new Font(Font.TIMES_ROMAN, 6, Font.BOLD);
        fuente_header.setColor(color_fuente_header);

        Font fuente = new Font(Font.TIMES_ROMAN, 6);
        Font fuenteB = new Font(Font.HELVETICA, 6, Font.BOLD);

        float[] medidaCeldas = {166.6f,166.6f,166.6f,166.6f,166.6f,166.6f};
        tabla_temp.setWidths(medidaCeldas);

     
        PdfPCell celda_temp = new PdfPCell();

        
        
        // fila 1
             
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("PERIODO DE FACTURACI�N ", fuente));
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda_temp.setColspan(6);
        tabla_temp.addCell(celda_temp);
        
       // fila 2
             
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("\n\n    "+dia+"           "+mes+"           "+anio, fuente));
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setColspan(3);
        tabla_temp.addCell(celda_temp);
        
        
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("\n\n    "+dia+"           "+mes+"           "+anio, fuente));
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(1f);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setColspan(3);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        
        
       
        
         // fila 3
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("DIA ", fuente));
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("MES ", fuente));
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0.5f);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("A�O ", fuente));
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0.5f);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        
        
         celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("DIA ", fuente));
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(1f);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("MES ", fuente));
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0.5f);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        
        
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("A�O ", fuente));
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0.5f);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        
        
        
        
        
        
        return tabla_temp;
    }
    
    
        
        protected PdfPTable tabla4(Negocios negocio) throws DocumentException, SQLException, Exception {

         ArrayList<DocumentosNegAceptado> itemsLiquidacion = new ArrayList();
         
         itemsLiquidacion =  Negociossvc.buscarDetallesNegocio(negocio.getCod_negocio());
          
         
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        String fecha_vencimiento =  s.format(sumaDias(s.parse(fecha_facturacion), 30)); //itemsLiquidacion.get(0).getFecha();
        String anio = fecha_vencimiento.substring(0, 4);//a�o
        String dia = fecha_vencimiento.substring(8, 10);//dia
        String mes = fecha_vencimiento.substring(5, 7);//mes

        

       // String fecha = Util.getFechahoy();
        PdfPTable tabla_temp = new PdfPTable(3);
       
        java.awt.Color color_fuente_header = new java.awt.Color(0xAA, 0xAA, 0xAA);

        Font fuente_header = new Font(Font.TIMES_ROMAN, 6, Font.BOLD);
        fuente_header.setColor(color_fuente_header);
        
         Font fuente = new Font(Font.TIMES_ROMAN, 6);
        Font fuenteB = new Font(Font.HELVETICA, 6, Font.BOLD);

        float[] medidaCeldas = {333.3f,333.3f,333.3f};
        tabla_temp.setWidths(medidaCeldas);

        PdfPCell celda_temp = new PdfPCell();     
        
        // fila 1
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("PAGUE ANTES DE ", fuente ));
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda_temp.setColspan(3);
        tabla_temp.addCell(celda_temp);
        
       // fila 2
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("\n\n"+dia, fuente));
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("\n\n"+mes, fuente));
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
              
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("\n\n"+anio, fuente));
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        
     
        
        
        
         // fila 3
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("DIA ", fuente));
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("MES ", fuente));
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0.5f);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("A�O ", fuente));
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0.5f);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        
        
      

        
        return tabla_temp;
    }
    
    
 /************************************** DIBUJO LA TABLA CONTENEDORA SEPARA AL DETALLE ***************************************************/
    protected PdfPTable TablaContenedoraSeparaDetalle() throws DocumentException, SQLException {

        String fecha = Util.getFechahoy();
        PdfPTable tabla_temp = new PdfPTable(4);

        float[] medidaCeldas = {0.350f, 0.190f, 0.270f, 0.190f};
        tabla_temp.setWidths(medidaCeldas);
        
        Font fuente = new Font(Font.HELVETICA, 9);
        Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();



        // fila 1
        celda_temp = new PdfPCell();
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
      //  celda_temp.addElement(this.tabla1());// agrego tabla 1
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        tabla_temp.addCell(celda_temp);
 
        
        celda_temp = new PdfPCell();
       // celda_temp.addElement(this.tabla2());// agrego tabla 2
        celda_temp.setHorizontalAlignment(PdfPCell.RIGHT);
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        tabla_temp.addCell(celda_temp);
        
        celda_temp = new PdfPCell();
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
       // celda_temp.addElement(this.tabla3());// agrego tabla 3
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        tabla_temp.addCell(celda_temp);
        
        celda_temp = new PdfPCell();
      //  celda_temp.addElement(this.tabla4()); // agrego tabla 4
        celda_temp.setHorizontalAlignment(PdfPCell.RIGHT);
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        tabla_temp.addCell(celda_temp);



        return tabla_temp;
    }
    
 /************************************** DIBUJO LA TABLA CONTENEDORA TITULOS DE LOS DETALLE ***************************************************/
    protected PdfPTable TablaContenedoraTitulosDetalle() throws DocumentException, SQLException {

  
        PdfPTable tabla_temp = new PdfPTable(2);

        float[] medidaCeldas = {0.600f, 0.400f};
        tabla_temp.setWidths(medidaCeldas);
        
        Font fuente = new Font(Font.HELVETICA, 9);
        Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();



        // fila 1
        celda_temp = new PdfPCell();
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.addElement(this.tablaTituloIzquierda());// agrego tabla titulo izquierda
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0.5f);
        celda_temp.setBorderWidthRight(0.5f);
        celda_temp.setBorderWidthBottom(0.5f); 
        tabla_temp.addCell(celda_temp);
 
        
        celda_temp = new PdfPCell();
        celda_temp.addElement(this.tablaTituloDerecha());// agrego tabla titulo derecha
        celda_temp.setHorizontalAlignment(PdfPCell.RIGHT);
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0.5f);
        celda_temp.setBorderWidthRight(0.5f);
        celda_temp.setBorderWidthBottom(0.5f); 
        tabla_temp.addCell(celda_temp);
        
      

        return tabla_temp;
    }
    
 
      
 /************************************** DATOS TABLA TITULO DE LA IZQUIERDA ***************************************************/
    protected PdfPTable tablaTituloIzquierda() throws DocumentException, SQLException {


  
        PdfPTable tabla_temp = new PdfPTable(1);
       
        java.awt.Color color_fuente_header = new java.awt.Color(0xAA, 0xAA, 0xAA);

        Font fuente_header = new Font(Font.TIMES_ROMAN, 6, Font.BOLD);
        fuente_header.setColor(color_fuente_header);
        
        Font fuente = new Font(Font.TIMES_ROMAN, 6);
        Font fuenteB = new Font(Font.HELVETICA, 6, Font.BOLD);

        float[] medidaCeldas = {0.250f};
        tabla_temp.setWidths(medidaCeldas);

        
         PdfPCell celda_temp = new PdfPCell();
          
        // fila 1
        celda_temp = new PdfPCell();
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setPhrase(new Phrase("RESUMEN CONCEPTOS FACTURADOS DEL MES", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
            
        return tabla_temp;
        
        
        
    }

  
      
 /************************************** DATOS TABLA TITULO DE LA DERECHA ***************************************************/
    protected PdfPTable tablaTituloDerecha() throws DocumentException, SQLException {


        String fecha = Util.getFechahoy();
        PdfPTable tabla_temp = new PdfPTable(1);
       
        java.awt.Color color_fuente_header = new java.awt.Color(0xAA, 0xAA, 0xAA);

        Font fuente_header = new Font(Font.TIMES_ROMAN, 6, Font.BOLD);
        fuente_header.setColor(color_fuente_header);
        
        Font fuente = new Font(Font.TIMES_ROMAN, 6);
        Font fuenteB = new Font(Font.HELVETICA, 6, Font.BOLD);

        float[] medidaCeldas = {0.250f};
        tabla_temp.setWidths(medidaCeldas);

        
         PdfPCell celda_temp = new PdfPCell();
          
        // fila 1
        celda_temp = new PdfPCell();
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setPhrase(new Phrase("MOVIMIENTO CUENTA HASTA FECHA DE CORTE", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
            
        return tabla_temp;
        
        
        
    }

  
    
    /************************************** DIBUJO LA TABLA CONTENEDORA DETALLE ***************************************************/
    protected PdfPTable TablaContenedoraDetalle(Negocios negocio,  TablaGen tbconceptos) throws DocumentException, SQLException, Exception {

        String fecha = Util.getFechahoy();
        PdfPTable tabla_temp = new PdfPTable(4);

        float[] medidaCeldas = {0.600f, 0.600f, 0.400f, 0.400f};
        tabla_temp.setWidths(medidaCeldas);
        
        Font fuente = new Font(Font.HELVETICA, 9);
        Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();

        
        // fila 1
        celda_temp = new PdfPCell();
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.addElement(this.tablaResumen(negocio, tbconceptos));// agrego tabla 1 resumen
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0.5f);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0.5f); 
        tabla_temp.addCell(celda_temp);
 
        
        celda_temp = new PdfPCell();
        celda_temp.addElement(this.tablaResumen2(negocio));// agrego tabla 2 resuman
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0.5f);
        celda_temp.setBorderWidthBottom(0.5f); 
        tabla_temp.addCell(celda_temp);
        
        celda_temp = new PdfPCell();
        celda_temp.addElement(this.tablaMovimiento(negocio));// agrego tabla 1 movimiento
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0.5f);
        celda_temp.setBorderWidthRight(0.5f);
        celda_temp.setBorderWidthBottom(0.5f); 
        celda_temp.setColspan(2);
        tabla_temp.addCell(celda_temp);
        
        
       

        return tabla_temp;
    }

   
  

    
 /************************************** DATOS TABLA 1 RESUMEN ***************************************************/
    protected PdfPTable tablaResumen(Negocios negocio,  TablaGen tbconceptos) throws DocumentException, SQLException {




        String codigo ="";
        String concepto="";
        if(negocio != null){
            codigo = negocio.getCnd_aval();
            concepto = negocio.getConcepto();
        }
        
        String fecha = Util.getFechahoy();
        PdfPTable tabla_temp = new PdfPTable(1);
       
        java.awt.Color color_fuente_header = new java.awt.Color(0xAA, 0xAA, 0xAA);

        Font fuente_header = new Font(Font.TIMES_ROMAN, 5, Font.BOLD);
        fuente_header.setColor(color_fuente_header);
        
        Font fuente = new Font(Font.TIMES_ROMAN, 5);
        Font fuenteB = new Font(Font.HELVETICA, 5, Font.BOLD);

        float[] medidaCeldas = {0.250f};
        tabla_temp.setWidths(medidaCeldas);

        
         PdfPCell celda_temp = new PdfPCell();
          
        // fila 1
        celda_temp = new PdfPCell();
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0);
        celda_temp.setPhrase(new Phrase("\nC�digo "+"    "+"Concepto", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        
        // fila 2
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("\n"+tbconceptos.getDato()+"         "+" "+tbconceptos.getDescripcion(), fuente));
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        
        // fila 3
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", fuente));
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
     
            
        return tabla_temp;
        
        
        
    }

  
         
 /************************************** DATOS TABLA RESUMEN 2 ***************************************************/
    protected PdfPTable tablaResumen2(Negocios negocio) throws DocumentException, SQLException, Exception {

        String ValorLetra = "";
        String subtotal = "";
        double vlr_sin_iva = 0;
        double vlr_iva = 0;
        double vlraval = 0;
        if (negocio != null) {
            String subtot = negocio.getvalor_aval();
            subtotal = String.valueOf(Util.customFormat(Double.parseDouble(subtot)));

            vlraval = Double.parseDouble(negocio.getvalor_aval());
            if(negocio.isFinanciaAval())
            {
            NegociosDAO negdao = new NegociosDAO(usuario.getBd());

            vlraval = negdao.buscarNegocioAval(negocio.getCod_negocio()).getVr_negocio();
            subtot=""+vlraval;
            }

            vlr_sin_iva = (vlraval / 1.16);
            vlr_iva = (vlraval - vlr_sin_iva);

            RMCantidadEnLetras c = new RMCantidadEnLetras();
            String[] a;
            a = c.getTexto(Math.round(vlraval));

            String res = "";
            for (int i = 0; i < a.length; i++) {
                res = res + ((String) a[i]).replace("-", "");
            }
            ValorLetra = res.trim();//Valor Total en Letras

        }
        

        String fecha = Util.getFechahoy();
        PdfPTable tabla_temp = new PdfPTable(2);
       
        java.awt.Color color_fuente_header = new java.awt.Color(0xAA, 0xAA, 0xAA);

        Font fuente_header = new Font(Font.TIMES_ROMAN, 5, Font.BOLD);
        fuente_header.setColor(color_fuente_header);
        
        Font fuente = new Font(Font.TIMES_ROMAN, 5);
        Font fuenteB = new Font(Font.HELVETICA, 5, Font.BOLD);

        float[] medidaCeldas = {0.125f, 0.125f};
        tabla_temp.setWidths(medidaCeldas);

        
         PdfPCell celda_temp = new PdfPCell();
      
        
        // fila 1
        celda_temp = new PdfPCell();
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0);
        celda_temp.setPhrase(new Phrase("\nCant"+"      " +"Valor", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        
        
        // fila 2
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("\nIVA"+"             "+" Total", fuente));
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        
        // fila 1.1
        celda_temp = new PdfPCell();
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0);
        celda_temp.setPhrase(new Phrase("\n1"+"           " +Util.customFormat(Math.round(vlr_sin_iva)), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        
         // fila 2.1
        celda_temp = new PdfPCell();
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0);
        celda_temp.setPhrase(new Phrase("\n"+Util.customFormat( Math.round(vlr_iva))+"           " +Util.customFormat(Math.round(vlraval)), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        
        // fila 4
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", fuente));
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
     
            
        return tabla_temp;
    }

    
    
    
     
 /************************************** DATOS TABLA 1 MOVIMIENTO ***************************************************/
    protected PdfPTable tablaMovimiento(Negocios negocio) throws DocumentException, SQLException, Exception {

        String ValorLetra ="";
        String subtotal ="";
         double vlr_sin_iva=0;
          double vlr_iva=0;
          double vlraval=0;
          
          vlraval = Double.parseDouble(negocio.getvalor_aval());
     
           if (negocio.isFinanciaAval()){
                Negocios negAval=Negociossvc.buscarNegocioAval(negocio.getCod_negocio());/// se busca el negocio de aval del negocio original
                vlraval=negAval.getVr_negocio();
           }
           
           
           
          
          
          
          if(negocio !=null){
         String subtot = negocio.getvalor_aval(); 
         subtotal = String.valueOf(Util.customFormat(Double.parseDouble(subtot)));
              
       
       
         vlr_sin_iva=(vlraval/1.16);
          vlr_iva=(vlraval-vlr_sin_iva);
        
         RMCantidadEnLetras c = new RMCantidadEnLetras();
                        String[] a;
                        a = c.getTexto(Math.round(vlraval));

                        String res = "";
                        for (int i = 0; i < a.length; i++) {
                            res = res + ((String) a[i]).replace("-", "");
                        }
                        ValorLetra = res.trim();//Valor Total en Letras

        }   
        
        String fecha = Util.getFechahoy();
        PdfPTable tabla_temp = new PdfPTable(1);
       
        java.awt.Color color_fuente_header = new java.awt.Color(0xAA, 0xAA, 0xAA);

        Font fuente_header = new Font(Font.TIMES_ROMAN, 7, Font.BOLD);
        fuente_header.setColor(color_fuente_header);
        
        Font fuente = new Font(Font.TIMES_ROMAN, 7);
        Font fuente2 = new Font(Font.TIMES_ROMAN, 6);
        Font fuenteB = new Font(Font.HELVETICA, 7, Font.BOLD);

        float[] medidaCeldas = {0.250f};
        tabla_temp.setWidths(medidaCeldas);

        
         PdfPCell celda_temp = new PdfPCell();
          
        
        // fila 1
        celda_temp = new PdfPCell();
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0);
        celda_temp.setPhrase(new Phrase("\nSUBTOTAL "+"                      "+Util.customFormat(Math.round(vlr_sin_iva)), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        
        // fila 2
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("IVA 16%  "+"                          "+Util.customFormat( Math.round(vlr_iva)), fuente));
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        
        // fila 3
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("TOTAL FACTURA  "+"         "+Util.customFormat(Math.round(vlraval)), fuente));
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        
        // fila 4
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Valor en letras:"+" "+ValorLetra, fuenteB));
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        
        // fila 5
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("\n\nNOTAS: \n1. El no pago oportuno de �sta factura causar� inter�s por mora a la tasa m�xima permitida por la ley y suspensi�n del servicio."
        +"\n2. El pago de su saldo vencido no lo exonera de los intereses moratorios."
        +"\n3. El no recibo de esta factura no lo exonera del pago.", fuente2));
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        // fila 4
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("\n\n\n\n\n\n\n\n\n\n\n\n", fuente));
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0); 
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
     
            
        return tabla_temp;
        
        
        
    }

    private static Date sumaDias(Date fecha, int dias) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(fecha);
        cal.add(Calendar.DAY_OF_YEAR, dias);
        return cal.getTime();
    }

   /*     public HeaderFooter getMyFooter() throws ServletException {
        try {



            Phrase p = new Phrase();
            Font fuente = new Font(Font.HELVETICA, 6);
            Font fuenteB = new Font(Font.HELVETICA, 6, Font.BOLD);
            p.clear();

            p.add(new com.lowagie.text.Paragraph("Numero de Resolucion 20000142992\n", fuente));
            p.add(new com.lowagie.text.Paragraph("Fecha de expedicion\n", fuente));
            HeaderFooter header = new HeaderFooter(p, false);
            header.setBorder(Rectangle.NO_BORDER);
            header.setAlignment(com.lowagie.text.Paragraph.ALIGN_LEFT);

            return header;
        } catch (Exception ex) {
            throw new ServletException("Header Error");
        }
    }
*/
  
    
}
