/*
 * Codigo_demoraService.java
 *
 * Created on 26 de junio de 2005, 01:16 PM
 */

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  Jose
 */
public class Codigo_demoraService {
    private Codigo_demoraDAO codigo_demora;
    private TreeMap cdemoras;
    
    /** Creates a new instance of Codigo_demoraService */
    public Codigo_demoraService() {
        codigo_demora = new Codigo_demoraDAO();
    }
    public Codigo_demora getCodigo_demora( )throws SQLException{
        return codigo_demora.getCodigo_demora();
    }
    
    public Vector getCodigo_demoras() {
        return codigo_demora.getCodigo_demoras();
    }
    
    public void setCodigo_demoras(Vector Codigo_demoras) {
        codigo_demora.setCodigo_demoras(Codigo_demoras);
    }
    
    public void insertCodigo_demora(Codigo_demora user) throws SQLException{
        try{
            codigo_demora.setCodigo_demora(user);
            codigo_demora.insertCodigo_demora();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public boolean existCodigo_demora(String cod) throws SQLException{
        return codigo_demora.existCodigo_demora(cod);
    }
    
    public void serchCodigo_demora(String cod)throws SQLException{
        codigo_demora.searchCodigo_demora(cod);
    }
    
    public void listCodigo_demora()throws SQLException{
        codigo_demora.listCodigo_demora();
    }
    
    public void updateCodigo_demora(String cod,String desc,String usu, String base)throws SQLException{
        codigo_demora.updateCodigo_demora(cod, desc, usu, base);
    }
    public void anularCodigo_demora(Codigo_demora dem)throws SQLException{
            try{
            codigo_demora.setCodigo_demora(dem);
            codigo_demora.anularCodigo_demora();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }  
    }
    public void searchDetalleCodigo_demoras(String cod, String desc, String base) throws SQLException{
        try{
            codigo_demora.searchDetalleCodigo_demoras(cod, desc, base);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
        public Vector listarCodigo_demora() throws SQLException{
        try{
            return codigo_demora.listarCodigo_demora();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
        
    /**
     * Carga un objeto TreeMap con los c�digos de demora
     * @autor Andr�s Maturana De La Cruz
     * @version 1.1
     * @throws SQLException si ocurre una excepci�n con la BD
     **/
    public void searchCDemoras() throws SQLException{
        cdemoras = new TreeMap();
        Vector demoras = this.listarCodigo_demora();
        Codigo_demora cod_demora = new Codigo_demora();
        //////System.out.println("hey dude: " + demoras.size());
        for( int i=0; i<demoras.size(); i++){
            cod_demora = (Codigo_demora) demoras.elementAt(i);            
            cdemoras.put(cod_demora.getDescripcion(),  cod_demora.getCodigo());
        }        
    }
    
    public String obtenerCDemora(String cod) throws SQLException{              
        return this.codigo_demora.obtenerCodigo_demora(cod);
    }

    /**
     * Getter for property cdemoras.
     * @return Value of property cdemoras.
     */
    public java.util.TreeMap getCdemoras() {
        return cdemoras;
    }
    
    /**
     * Setter for property cdemoras.
     * @param cdemoras New value of property cdemoras.
     */
    public void setCdemoras(java.util.TreeMap cdemoras) {
        this.cdemoras = cdemoras;
    }
    
}
