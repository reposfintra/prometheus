/***************************************
    * Nombre Clase ............. ChequeFacturasCorridaService.java
    * Descripci�n  .. . . . . .  Generamos Los Cheques de las facturas aprobada para pago dentro de una corrida.
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  07/03/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/


package com.tsp.operation.model.services;


import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.ImpresionChequeService;
import com.tsp.operation.model.DAOS.ChequeFacturasCorridaDAO;
import com.tsp.operation.model.DAOS.SeriesChequesDAO;
import com.tsp.operation.model.DAOS.ChequeXFacturaDAO;
import javax.servlet.*;
import javax.servlet.http.*;

	





public class ChequeFacturasCorridaService {    
    
    private  ChequeFacturasCorridaDAO  ChequeDataAccess;   
    private  ChequeXFacturaDAO         ChequeXFacturaDataAccess;
    private  List                      facturasAprobadas;
    private  double                    Total;
    private  List                      facturasSeleccionada;
    private  List                      listCheques;
    private  Hashtable                 bloque;
    private  Hashtable                 bloquePrint;
    private  String                    distrito;
    private  String                    agencia;
    private  boolean                   estadoBtnRegresar;
    
    private  String corrida            = "";
    private  String banco              = "";
    private  String sucursal           = "";
    private  String nit                = "";
    private  Usuario                   usuario;
    private  int                       idSerie;
    
    
    private  int                       CANTIDAD_FILTROS;
    public   String[]                  TITULOS;
    public   String[]                  TAMANO_TITULOS;
    private  String                    IMG_EXPAND;
    private  String                    IMG_COLAPSE;
    private  List                      vistaFiltros;
    private  List                      listBloques;
    private  String                    ON;
    private  String                    OFF;
    private  List                      seriesCheques;    
    
    
    private  String                    CK_ON;
    private  String                    CK_OFF;    
    
    
    private  int                       cantAprobadas;
    
    private  String                     monedaLocal;
    
    private  List                       listBancos;
    private  List                       listSucursal;
    
    
    
    
    public ChequeFacturasCorridaService() {
        ChequeDataAccess          = new ChequeFacturasCorridaDAO();
        ChequeXFacturaDataAccess  = new ChequeXFacturaDAO();
        reset();
    }
    public ChequeFacturasCorridaService(String dataBaseName) {
        ChequeDataAccess          = new ChequeFacturasCorridaDAO(dataBaseName);
        ChequeXFacturaDataAccess  = new ChequeXFacturaDAO(dataBaseName);
        reset();
    }
    
      
    
    
   /**
   * M�todo que  setea la lista de facturas aprobadas y total
   * @autor.......fvillacob
   * @version.....1.0.
   **/ 
    public void reset(){
        bloque               = null;
        bloquePrint          = null;
        estadoBtnRegresar    = true;
        facturasAprobadas    = new LinkedList();
        facturasSeleccionada = new LinkedList();
        listCheques          = new LinkedList();
        seriesCheques        = new LinkedList();
        Total                = 0; 
        cantAprobadas        = 0;
        listBancos           = new LinkedList();
        listSucursal         = new LinkedList();
    }
    
    
    
    
   /**
   * M�todo que  activa variables de filtros
   * @autor.......fvillacob
   * @version.....1.0.
   **/ 
    public void activarFiltro(){
         ON                = "1";
         OFF               = "0";
         CK_ON             = "checked";
         CK_OFF            = "";
         
         IMG_EXPAND        = "expand.gif";
         IMG_COLAPSE       = "collapse.gif";
         CANTIDAD_FILTROS  = 4;
         TITULOS           =  new String[CANTIDAD_FILTROS];
             TITULOS[0]    =  "CORRIDA";
             TITULOS[1]    =  "BANCO";
             TITULOS[2]    =  "SUCURSAL";
             TITULOS[3]    =  "PROVEEDOR";
             
        TAMANO_TITULOS     =  new String[CANTIDAD_FILTROS];
             TAMANO_TITULOS[0] = "10%";
             TAMANO_TITULOS[1] = "10%";
             TAMANO_TITULOS[2] = "15%";
             TAMANO_TITULOS[3] = "25%";
             
         vistaFiltros      =  new LinkedList();
         listBloques       =  new LinkedList();
    }
    
    
   //------------------------------------------------------------------------------------------------
   // BUSQUEDA DE FACTURAS Y FORMACION DE BLOQUES :
   //------------------------------------------------------------------------------------------------
    
    /**
     * M�todo que busca las facturas aprobadas para pago, agrupadas  por: Corrida, banco, Sucursal, Proveedor
     teniendo en cuenta distrito y agencia del usuario.
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void buscarFacturasAprobadas(String distrito, String agencia) throws Exception{
        try{
            
            String PRE_CORRIDA   = "BL_CORRIDA";
            String PRE_BANCO     = "BL_BANCO";
            String PRE_SUCURSAL  = "BL_SUCURSAL";
            String PRE_PROVEEDOR = "BL_PROVEEDOR";
            String SEPARADOR     = "_";
            
            
            this.distrito = distrito;
            this.agencia  = agencia;
            reset();
            activarFiltro();
            int    cont        = 1;
            
            monedaLocal        = this.ChequeXFacturaDataAccess.getMonedaLocal( this.distrito ) ;
            ChequeXFacturaService svc = new ChequeXFacturaService();
            
            
            //--- Corridas:
            List listCorridas = this.ChequeDataAccess.getCorridasConFacturasPago(distrito, agencia);
            for(int i=0; i<listCorridas.size();i++){
                String corrida     = (String)listCorridas.get(i);
                
                
                Hashtable  bloqueLevel0        =  new Hashtable();
                bloqueLevel0.put("titulo", corrida);
                bloqueLevel0.put("padre",  "");
                bloqueLevel0.put("id",     PRE_CORRIDA + SEPARADOR + String.valueOf(i) );
                bloqueLevel0.put("selection",  "");
                
                
                List listLevel0     = new LinkedList();
                double totalLevel0  = 0;
                
                //--- Bancos:
                List listBancos = this.ChequeDataAccess.getBancosConFacturasPago(distrito, corrida, agencia);
                for(int j=0; j<listBancos.size();j++){
                    String  banco      = (String)listBancos.get(j);
                    
                    
                    Hashtable  bloqueLevel1 =  new Hashtable();
                    bloqueLevel1.put("titulo", banco);
                    bloqueLevel1.put("padre",  PRE_CORRIDA + SEPARADOR + String.valueOf(i)              );
                    bloqueLevel1.put("id",     PRE_BANCO   + SEPARADOR + String.valueOf(i) + SEPARADOR +  String.valueOf(j) );
                    bloqueLevel1.put("selection",  "");
                    
                    List listLevel1    = new LinkedList();
                    double totalLevel1 = 0;
                    
                    //--- Sucursales:
                    List listSucursales = this.ChequeDataAccess.getSucursalesBancosConFacturasPago(distrito, corrida, banco,  agencia);
                    for(int k=0; k<listSucursales.size();k++){
                        String sucursal     = (String)listSucursales.get(k);
                        
                        
                        Hashtable  bloqueLevel2 =  new Hashtable();
                        bloqueLevel2.put("titulo", sucursal);
                        bloqueLevel2.put("padre",  PRE_BANCO    + SEPARADOR + String.valueOf(i) + SEPARADOR +  String.valueOf(j)  );
                        bloqueLevel2.put("id",     PRE_SUCURSAL + SEPARADOR + String.valueOf(i) + SEPARADOR +  String.valueOf(j)  + SEPARADOR + String.valueOf(k));
                        bloqueLevel2.put("selection",  "");
                        
                        List listLevel2     = new LinkedList();
                        double totalLevel2  = 0;
                        
                        //--- Proveedores:
                        List listProveedores = this.ChequeDataAccess.getBeneficiariosFacturasCorrida(distrito, corrida, banco, sucursal,  agencia );
                        
                        for(int l=0; l<listProveedores.size();l++){
                            Hashtable  datoProveedor     = ( Hashtable ) listProveedores.get(l);
                            String     proveedor         = ( String    ) datoProveedor.get("nit");
                            String     nombre            = ( String    ) datoProveedor.get("nombre");
                            String     beneficiario      = ( String    ) datoProveedor.get("beneficiario");//Osvaldo
                            String     nit_beneficiario  = ( String    ) datoProveedor.get("nit_beneficiario");//Osvaldo
                            
                            Hashtable  bloqueLevel3 =  new Hashtable();
                            bloqueLevel3.put("beneficiario", beneficiario);//Osvaldo - Nombre del beneficiario
                            bloqueLevel3.put("nit_beneficiario", nit_beneficiario);//Osvaldo - Nit del beneficiario
                            bloqueLevel3.put("titulo", nombre);
                            bloqueLevel3.put("padre",  PRE_SUCURSAL   + SEPARADOR + String.valueOf(i) + SEPARADOR +  String.valueOf(j)  + SEPARADOR + String.valueOf(k)  );
                            bloqueLevel3.put("id",     PRE_PROVEEDOR  + SEPARADOR + String.valueOf(i) + SEPARADOR +  String.valueOf(j)  + SEPARADOR + String.valueOf(k)  +SEPARADOR+  String.valueOf(l));
                            bloqueLevel3.put("selection",  "");
                            double totalLevel3       = 0;
                            
                            //--- Facturas:
                            List listFacturas  =  this.ChequeDataAccess.getFacturasAprobadasBeneficiarios(distrito, corrida, banco, sucursal, proveedor, agencia );
                            List listBLFactura = new LinkedList();
                            for(int m=0; m<listFacturas.size();m++){
                                FacturasCheques  factura  = (FacturasCheques) listFacturas.get(m);
                                Total += factura.getVlr_saldo();
                                factura.setIdObject( cont );
                                factura.setSelected( false );
                                factura.setDatoProveedor( datoProveedor );
                                
                                
                                //Cambio de moneda:
                                String     monedaBanco   = factura.getMonedaBanco();
                                factura = svc.convertirMoneda( factura , monedaBanco, monedaLocal);
                                factura.setMonedaLocal( monedaLocal );
                                
                                //informacion del bloque al cual pertenece:
                                Hashtable  blFactura = new  Hashtable();
                                blFactura.put("corrida"    , corrida  );
                                blFactura.put("banco"      , banco    );
                                blFactura.put("sucursal"   , sucursal );
                                blFactura.put("proveedor"  , proveedor);
                                blFactura.put("factura"    , factura.getDocumento()   );
                                blFactura.put("descripcion", factura.getDescripcion() );
                                blFactura.put("saldo"      , String.valueOf( factura.getVlrPagar() )   );
                                blFactura.put("padre"      , PRE_PROVEEDOR + SEPARADOR + String.valueOf(i) + SEPARADOR +  String.valueOf(j)  + SEPARADOR +  String.valueOf(k)  + SEPARADOR +  String.valueOf(l)  );
                                blFactura.put("id"         , String.valueOf( factura.getIdObject() )    );
                                blFactura.put("ck"         , CK_ON );
                                blFactura.put("level"     , "4"   );
                                blFactura.put("selection",  "");
                                
                                factura.infoBloque = blFactura;
                                
                                listBLFactura.add   (  blFactura   );
                                this.listBloques.add(  blFactura   );
                                
                                cont++;
                                cantAprobadas++;
                                
                                // Totales de bloques:
                                totalLevel3 += factura.getVlrPagar();
                                totalLevel2 += factura.getVlrPagar();
                                totalLevel1 += factura.getVlrPagar();
                                totalLevel0 += factura.getVlrPagar();
                                
                                facturasAprobadas.add( factura );
                            }
                            
                            
                            bloqueLevel3.put("items"     , listBLFactura                 );
                            bloqueLevel3.put("total"     , String.valueOf( totalLevel3 ) );
                            bloqueLevel3.put("img"       , this.IMG_EXPAND               );
                            bloqueLevel3.put("estado"    , this.ON                       );
                            bloqueLevel3.put("hijos"     , this.OFF                      );
                            bloqueLevel3.put("llave"     , corrida + banco + sucursal + proveedor );
                            bloqueLevel3.put("level"     , "3"   );
                            bloqueLevel3.put("ck"        , CK_ON );
                            bloqueLevel3.put("fila"      , String.valueOf(i)             );
                            
                            this.listBloques.add( bloqueLevel3 );
                            listLevel2.add( bloqueLevel3 );
                            
                        }
                        
                        bloqueLevel2.put("items"     , listLevel2                    );
                        bloqueLevel2.put("total"     ,String.valueOf( totalLevel2 )  );
                        bloqueLevel2.put("img"       , this.IMG_COLAPSE              );
                        bloqueLevel2.put("estado"    , this.ON                       );
                        bloqueLevel2.put("hijos"     , this.ON                       );
                        bloqueLevel2.put("llave"     , corrida + banco + sucursal    );
                        bloqueLevel2.put("level"     , "2"   );
                        bloqueLevel2.put("ck"        , CK_ON );
                        bloqueLevel2.put("fila"      , String.valueOf(i)             );
                        
                        this.listBloques.add( bloqueLevel2 );
                        listLevel1.add( bloqueLevel2 );
                    }
                    
                    bloqueLevel1.put("items"     , listLevel1                    );
                    bloqueLevel1.put("total"     ,String.valueOf( totalLevel1 )  );
                    bloqueLevel1.put("img"       , this.IMG_COLAPSE              );
                    bloqueLevel1.put("estado"    , this.ON                       );
                    bloqueLevel1.put("hijos"     , this.ON                       );
                    bloqueLevel1.put("llave"     , corrida + banco               );
                    bloqueLevel1.put("level"     , "1"    );
                    bloqueLevel1.put("ck"        , CK_ON  );
                    bloqueLevel1.put("fila"      , String.valueOf(i)             );
                    
                    this.listBloques.add( bloqueLevel1 );
                    listLevel0.add( bloqueLevel1 );
                }
                
                bloqueLevel0.put("items"     , listLevel0                    );
                bloqueLevel0.put("total"     , String.valueOf( totalLevel0 ) );
                bloqueLevel0.put("img"       , this.IMG_COLAPSE              );
                bloqueLevel0.put("estado"    , this.ON                       );
                bloqueLevel0.put("hijos"     , this.ON                       );
                bloqueLevel0.put("llave"     , corrida                       );
                bloqueLevel0.put("level"     , "0"    );
                bloqueLevel0.put("ck"        , CK_ON  );
                bloqueLevel0.put("fila"      , String.valueOf(i)             );
                
                this.listBloques.add( bloqueLevel0 );
                vistaFiltros.add( bloqueLevel0 );
            }
            
        }catch(Exception e){
            throw new Exception( " buscarFacturasAprobadas " + e.getMessage() );
        }
        
    }
   
   
   
   //------------------------------------------------------------------------------------------------
   // SELECCION DE CHECKBOX :
   //------------------------------------------------------------------------------------------------
   
   
   
   /**
   * M�todo que busca los hijos del padre
   * @autor.......fvillacob
   * @version.....1.0.
   **/
   public List getHijos(String padre)throws Exception{
       List listHijos = new LinkedList();
       try{
           
            for (int i=0;i<this.listBloques.size();i++){
                 Hashtable  bloque     =  (Hashtable)this.listBloques.get(i);
                 String     padreBL    =  (String)bloque.get("padre");
                 if( padreBL.equals(padre)  )
                     listHijos.add(bloque);
            }
           
       }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
       return listHijos;
   }
   
   
   
   /**
   * M�todo que selecciona los hijos del id
   * @autor.......fvillacob
   * @version.....1.0.
   **/
   public void seleccionar(String id, String ck)throws Exception{
       try{
           
              List listHijos    = getHijos(id);                 
              for (int j=0;j< listHijos.size();j++){
                  Hashtable  bloqueH  =  (Hashtable)listHijos.get(j); 
                  
                  bloqueH.put("ck" , ck );                  
                  String     level    =  (String)bloqueH.get("level");
                  if( !level.equals("4")){
                       String     idBL     =  (String)bloqueH.get("id");
                       seleccionar(idBL,ck);                      
                  }                  
              }            
           
       }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
   }
   
   
   
    /**
   * M�todo que maneja la selecciona de bloques
   * @autor.......fvillacob
   * @version.....1.0.
   **/
   public void filtroSeleccion(String id, String ck)throws Exception{
       try{
           
           for (int i=0;i<this.listBloques.size();i++){
                 Hashtable  bloque     =  (Hashtable)this.listBloques.get(i);
                 String     idBL       =  (String)bloque.get("id");
                 if( idBL.equals(id)   ){
                   
                     bloque.put("ck" , ck );
                     String  level     =  (String)bloque.get("level");
                     if(!level.equals("4") )
                         seleccionar( id, ck );
                 }   
           }
           
           verificacionLoad();
           
       }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
       
   }
   
   
   
   
   
    /**
   * M�todo que permite verificar los bloques llenados, activando sus ck y color de texto 
   * @autor.......fvillacob
   * @version.....1.0.
   **/
   public void verificacionLoad()throws Exception{
       try{
           String color    = "#749585";
           String color_2  = "#745585";
           
           cantAprobadas = 0;
           
           for (int i=0;i<this.listBloques.size();i++){
                 Hashtable  bloque    =  (Hashtable)this.listBloques.get(i);
                 String     id        =  (String)bloque.get("id");
                 String     level     =  (String)bloque.get("level");
                 if(!level.equals("4") ){
                      
                         int contON      = 0;
                         List listHijos   = getHijos(id); 
                         for (int j=0;j< listHijos.size();j++){
                               Hashtable  bloqueH   =  (Hashtable)listHijos.get(j); 
                               String     ckBloqueH =  (String)bloqueH.get("ck");                               
                               if( ckBloqueH.equals(  this.CK_ON )  )
                                   contON++;
                         }
                         
                         String estado   = ( contON==0)?color:( ( contON< listHijos.size() )? color_2 :"" ); 
                         String estadoCk = ( contON==listHijos.size() )?this.CK_ON: this.CK_OFF;
                         bloque.put("selection", estado   );
                         bloque.put("ck",        estadoCk );
                 }
                 else{
                     String     ckBloque =  (String)bloque.get("ck");
                     if ( ckBloque.equals(  this.CK_OFF )   )
                          bloque.put("selection",color   );
                     else{
                         bloque.put("selection", "" );  
                         cantAprobadas++;   
                     }
                 }
                 
           }
           
       }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
       
   }
   
   
   
   
   
   
   
   //------------------------------------------------------------------------------------------------
   // EXPANDIR  COLAPSAR :
   //------------------------------------------------------------------------------------------------
   
   
   /**
   * M�todo que maneja los filtros en los bloques de la vista
   * @autor.......fvillacob
   * @version.....1.0.
   **/
   public void filtro(String llave) throws Exception{
      try{
           for (int i=0;i<this.listBloques.size();i++){
               Hashtable  bloque      = (Hashtable)listBloques.get(i);
               String     levelBloque = (String)bloque.get("level");
               if( !levelBloque.equals("4") ){
                   String     llaveBloque = (String)bloque.get("llave");
                   if(llaveBloque.equals(llave) ){ 
                         String newImg         =  (  ((String)bloque.get("img"))  .equals( this.IMG_COLAPSE)  )? this.IMG_EXPAND  :  this.IMG_COLAPSE ;
                         String newEstadoHijo  =  (  ((String)bloque.get("hijos")).equals( this.OFF)          )? this.ON          :  this.OFF         ;
                         bloque.put("img"       , newImg         );
                         bloque.put("hijos"     , newEstadoHijo  );
                       break;
                   }    
               }
           }
      }catch(Exception e){
           throw new Exception( e.getMessage() );
      }
   }
   
   
   
   
   
   
   //------------------------------------------------------------------------------------------------
   //  FORMACION DE LOS CHEQUES A PARTIR DE LAS FACTURAS SELECCIONADAS:
   //------------------------------------------------------------------------------------------------
   
   
   
   
   
    /**
   * M�todo que permite verificar los bloques llenados, activando sus ck y color de texto 
   * @autor.......fvillacob
   * @version.....1.0.
   **/
   public void activarSeleccion()throws Exception{
       try{
          
           for (int i=0;i<this.listBloques.size();i++){
                 Hashtable  bloque    =  (Hashtable)this.listBloques.get(i);                 
                 String     level     =  (String)bloque.get("level");
                 if( level.equals("4") ){                     
                        String     ckBloque  =  (String)bloque.get("ck");
                        String     id        =  (String)bloque.get("id");
                        boolean    estado    =  ( ckBloque.equals(  this.CK_ON ) )?true:false;
                        activar( Integer.parseInt(id), estado);
                 }
           }
           
       }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
       
   }
   
   
   
   
   /**
   * M�todo que activa la seleccion del objeto
   * @autor.......fvillacob
   * @version.....1.0.
   **/
   private void activar(int id, boolean estado)throws Exception{
      try{ 
          for(int i=0;i<this.facturasAprobadas.size();i++){
                FacturasCheques  factura  = (FacturasCheques) this.facturasAprobadas.get(i);
                if(factura.getIdObject()==id){
                    factura.setSelected( estado );
                    break;   
                }
          }
      }catch(Exception e){
           throw new Exception( e.getMessage() );
      }
   }
   
   
   
   
   /**
   * M�todo en el cual se llena la lista de facturas seleccionadas
   * @autor.......fvillacob
   * @version.....1.0.
   **/
   public void loadFacturasSeleccionadas()throws Exception{
       try{
              this.facturasSeleccionada = new LinkedList();
              activarSeleccion();      
              for(int i=0;i<this.facturasAprobadas.size();i++){
                  FacturasCheques  factura  = (FacturasCheques)this.facturasAprobadas.get(i);
                  if (  factura.isSelected() )
                        this.facturasSeleccionada.add( factura );
              }
       }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
   }
   
   
   
   /**
   * M�todo que obtiene el ultimo procesado de la serie
   * @autor.......fvillacob
   * @version.....1.0.
   **/
   public int getSerieProcesada(String banco, String sucursal){
       int last = -1;
       for(int i=0;i<this.seriesCheques.size();i++){
           Hashtable serie = (Hashtable)this.seriesCheques.get(i);
           String bancoSerie    = (String) serie.get("banco");
           String sucursalSerie = (String) serie.get("sucursal");
           
           if (  banco.equals( bancoSerie )   &&    sucursal.equals( sucursalSerie )  ){
                 int procesado = Integer.parseInt(  (String) serie.get("procesado") );
                 last          = procesado;
                 break;
           }
           
       }
       return last;
   }
   
   
   
   
  /**
   * M�todo que actualiza el ultimo procesado de la serie
   * @autor.......fvillacob
   * @version.....1.0.
   **/
   public void updateProcesada(String banco, String sucursal, int last){
     
       for(int i=0;i<this.seriesCheques.size();i++){
           Hashtable serie         = (Hashtable)this.seriesCheques.get(i);
           String    bancoSerie    = (String) serie.get("banco");
           String    sucursalSerie = (String) serie.get("sucursal");
           
           if (  banco.equals( bancoSerie )   &&    sucursal.equals( sucursalSerie )  ){
                 serie.put("procesado", String.valueOf( last )   );
                 break;
           }
           
       }
       
   }
   
   
   
   
   
   /**
   * M�todo que retorna el bloque de cheque a imprimir
   * @autor.......fvillacob
   * @version.....1.0.
   **/
   public void loadNextBloqueCheques(){
       this.bloque = null;
       if(this.listCheques!=null && this.listCheques.size()>0){
           this.bloque = (Hashtable)listCheques.get(0);
           listCheques.remove(0);
       }
   }
   
   
   
   /**
   * Carga en Memoria  el bloque de cheque a imprimir
   * @autor.......fvillacob
   * @version.....1.0.
   **/
   public void copyBloque(){
       this.bloquePrint =  new Hashtable();
       this.bloquePrint.put("corrida",      (String) this.bloque.get("corrida")    );
       this.bloquePrint.put("banco",        (String) this.bloque.get("banco")      );
       this.bloquePrint.put("sucursal",     (String) this.bloque.get("sucursal")   );
       this.bloquePrint.put("tamanoPage",   (String) this.bloque.get("tamanoPage") );
       this.bloquePrint.put("cuenta",       (String) this.bloque.get("cuenta")     );
       this.bloquePrint.put("neto",         (String) this.bloque.get("neto")       );
       this.bloquePrint.put("cheques",      (List)   this.bloque.get("cheques")    );
   }
  
   
   
   
   
   
  /**
   * M�todo que genera el sql para actualizar los cheques en la bd. Egreso, egresodet y serie
   * @autor.......fvillacob
   * @throws......Exception
   * @version.....1.0.
   **/ 
   public String updateBloqueCheque(String user)throws Exception{
       String sql = "";
       
       if(this.bloque!=null){
           
          
           List   listaCheques  = (List) bloque.get("cheques");
           
           
           for(int i=0;i<listaCheques.size();i++){
               Cheque  cheque = (Cheque)listaCheques.get(i);
               
               // Cheque:
               sql += ChequeXFacturaDataAccess.insertEgreso( cheque, cheque.getMonto() , user );                          // egreso
               
               // Facturas:
               List listaDetalle = cheque.getFacturas();
               for(int j=0;j<listaDetalle.size();j++){
                   FacturasCheques    factura = (FacturasCheques) listaDetalle.get(j);
                   factura.setTipo_pago("C");
                   double vlr    =    factura.getVlrPagar();
                   String item   =    String.valueOf( j+ 1 );
                   sql += this.ChequeXFacturaDataAccess.insertEgresoDet (cheque, factura, item ,  vlr, user        );  // egresodet
                   sql += this.ChequeDataAccess.updateCXP_DOC           (cheque.getNumero(), factura, vlr,  user   );  // cxp_doc : actualizamos saldo y abono
                   sql += this.ChequeDataAccess.updateCorrida           (cheque.getNumero(), factura, user         );  // corrida : actualizamos campo cheque y fecha impresi�n
                   
               }
               
               
              // Serie:               
               Series  serie  = cheque.getSerie();
               int     tope   = Integer.parseInt( serie.getSerial_fished_no() );
               int     ultimo = cheque.getLastNumber();

               if(tope > ultimo)
                   sql += this.ChequeDataAccess.ActualizarSeriesCorridaCero(cheque, user );    // series
               if(tope <= ultimo )    
                   sql += this.ChequeDataAccess.FinalizarSeriesCorridasCero(cheque, user);      // series
               
           }
           
       }
       return sql;
   }
   
   
   
   
   
   
   
   
   
   
    
   /**
   * M�todo que busca las corridas seleccionadas
   * @autor.......fvillacob
   * @version.....1.0.
   **/
   public List corridas_FacturasSelect(){
       List lista = new LinkedList();
       for(int i=0;i<this.facturasSeleccionada.size();i++){
           FacturasCheques  factura  = (FacturasCheques)this.facturasSeleccionada.get(i);
           Hashtable        info     = factura.getInfoBloque();
           String           corrida  = (String)info.get("corrida");
           int              sw       = 0;
           for(int j=0;j<lista.size();j++){
               String codeCor = (String)lista.get(j);
               if(codeCor.equals(corrida)){
                   sw=1;
                   break;
               }
           }
           if(sw==0)
               lista.add(corrida);
       }
       return lista;
   }
    
    
   /**
   * M�todo que busca los bancos de cada corrida seleccionada
   * @autor.......fvillacob
   * @version.....1.0.
   **/
   public List bancosCorridas_FacturasSelect(String  noCorrida){
       List lista = new LinkedList();
       for(int i=0;i<this.facturasSeleccionada.size();i++){
           FacturasCheques  factura  = (FacturasCheques)this.facturasSeleccionada.get(i);
           Hashtable        info     = factura.getInfoBloque();
           String           corrida  = (String)info.get("corrida");
           if(corrida.equals(noCorrida)){
               String   banco  =  (String)info.get("banco");
               int sw = 0;
               for(int j=0;j<lista.size();j++){
                   String codeBan = (String)lista.get(j);
                   if(codeBan.equals(banco)){
                       sw=1;
                       break;
                   }
               }
               if(sw==0)
                   lista.add(banco);
           }
       }
       return lista;
   }
   
   
   /**
   * M�todo que busca las sucursales de los bancos de cada corrida seleccionada
   * @autor.......fvillacob
   * @version.....1.0.
   **/
   public List sucursalesBancosCorridas_FacturasSelect(String  noCorrida, String codeBanco){
       List lista = new LinkedList();
       for(int i=0;i<this.facturasSeleccionada.size();i++){
           FacturasCheques  factura  = (FacturasCheques)this.facturasSeleccionada.get(i);
           Hashtable        info     = factura.getInfoBloque();
           String           corrida  = (String)info.get("corrida");
           String           banco    = (String)info.get("banco");
           if(corrida.equals(noCorrida)   &&   banco.equals(codeBanco) ){
               String   sucursal  =  (String)info.get("sucursal");
               int sw = 0;
               for(int j=0;j<lista.size();j++){
                   String codeSuc = (String)lista.get(j);
                   if(codeSuc.equals(sucursal)){
                       sw=1;
                       break;
                   }
               }
               if(sw==0)
                   lista.add(sucursal);
           }
       }
       return lista;
   }
   
   
   
   /**
   * M�todo que busca los proveedores de las sucursales de los bancos de cada corrida seleccionada
   * @autor.......fvillacob
   * @version.....1.0.
   **/
    public List proveedorSucursalesBancosCorridas_FacturasSelect(String  noCorrida, String codeBanco, String codeSucursal){
       List lista = new LinkedList();
       for(int i=0;i<this.facturasSeleccionada.size();i++){
           FacturasCheques  factura  = (FacturasCheques)this.facturasSeleccionada.get(i);
           Hashtable        info     = factura.getInfoBloque();
           String           corrida  = (String)info.get("corrida");
           String           banco    = (String)info.get("banco");
           String           sucursal = (String)info.get("sucursal");
           if(corrida.equals(noCorrida)   &&   banco.equals(codeBanco)  &&  sucursal.equals( codeSucursal ) ){
               String   proveedor  =  (String)info.get("proveedor");
               int sw = 0;
               for(int j=0;j<lista.size();j++){
                   String codeProve = (String)lista.get(j);
                   if(codeProve.equals(proveedor)){
                       sw=1;
                       break;
                   }
               }
               if(sw==0)
                   lista.add(proveedor);
           }
       }
       return lista;
   }
    
    
    
    
    
   /**
   * M�todo que busca los proveedores de las sucursales de los bancos de cada corrida seleccionada
   * @autor.......fvillacob
   * @version.....1.0.
   **/
    public List facturasProveedorSucursalesBancosCorridas_FacturasSelect(String  noCorrida, String codeBanco, String codeSucursal, String  nit){
       List lista = new LinkedList();
       for(int i=0;i<this.facturasSeleccionada.size();i++){
           FacturasCheques  factura    = (FacturasCheques)this.facturasSeleccionada.get(i);
           Hashtable        info       =  factura.getInfoBloque();
           String           corrida    =  (String)info.get("corrida");
           String           banco      =  (String)info.get("banco");
           String           sucursal   =  (String)info.get("sucursal");
           String           proveedor  =  (String)info.get("proveedor");
           if(corrida.equals(noCorrida)   &&   banco.equals(codeBanco)  &&  sucursal.equals( codeSucursal )  &&   proveedor.equals(nit)     ){
               lista.add(factura);
           }
       }
       return lista;
   }
    
   
   
   
   
   
   
    
    /**
   * M�todo que busca los bancos dependiendo la agencia
   * @autor.......fvillacob
   * @version.....1.0.
   **/
    public void loadBancos(String distrito, String agencia)throws Exception{
        try{
            this.listBancos  =  this.ChequeDataAccess.loadBancos(distrito, agencia);
        }catch(Exception e){
            throw new Exception ( e.getMessage() );
        }
    }
   
    
   
   /**
   * M�todo que busca las  sucursales de los bancos dependiendo la agencia
   * @autor.......fvillacob
   * @version.....1.0.
   **/
    public void loadSucursales(String distrito, String agencia)throws Exception{
        try{
            this.listSucursal = this.ChequeDataAccess.loadSucursales(distrito, agencia);
        }catch(Exception e){
            throw new Exception ( e.getMessage() );
        }
    }
    
    
   
    
   /**
   * M�todo que genera la variable js de sucursales
   * @autor.......fvillacob
   * @version.....1.0.
   **/
    public String getSucursalesJS()throws Exception{
        String js = " var sucursales = '";
        for(int i=0;i<this.listSucursal.size();i++){
            Hashtable  hc =  (Hashtable)this.listSucursal.get(i);
              String  banco    =  (String) hc.get("banco");
              String  sucursal =  (String) hc.get("sucursal");
              if( i<this.listSucursal.size() )
                  js +="||";
              js += banco + "|" + sucursal;
        }
        js += "';";
        
        return js;
    }
    
    
    
   
   // SET   AND  GET :   
   
   /**
    * Getter for property facturasAprobadas.
    * @return Value of property proveedoresFacturas.
    */
   public List getFacturas() {
       return facturasAprobadas;
   }   
    
   
   
   /**
    * Setter for property facturasAprobadas.
    * @param proveedoresFacturas New value of property proveedoresFacturas.
    */
   public void setFacturas(List list) {
       this.facturasAprobadas = list;
   }   
   
  
   
   
   
   
   
   /**
    * Getter for property Total.
    * @return Value of property Total.
    */
   public double getTotal() {
       return Total;
   }   

   /**
    * Setter for property Total.
    * @param Total New value of property Total.
    */
   public void setTotal(double Total) {
       this.Total = Total;
   }
   
   
   
   
   
   
   
   /**
    * Getter for property TAMANO_TITULOS.
    * @return Value of property TAMANO_TITULOS.
    */
   public java.lang.String[] getTAMANO_TITULOS() {
       return this.TAMANO_TITULOS;
   }   
   
   /**
    * Setter for property TAMANO_TITULOS.
    * @param TAMANO_TITULOS New value of property TAMANO_TITULOS.
    */
   public void setTAMANO_TITULOS(java.lang.String[] TAMANO_TITULOS) {
       this.TAMANO_TITULOS = TAMANO_TITULOS;
   }   
   
   
   
   
   
   
   
   /**
    * Getter for property TITULOS.
    * @return Value of property TITULOS.
    */
   public java.lang.String[] getTITULOS() {
       return this.TITULOS;
   }   
   
   /**
    * Setter for property TITULOS.
    * @param TITULOS New value of property TITULOS.
    */
   public void setTITULOS(java.lang.String[] TITULOS) {
       this.TITULOS = TITULOS;
   }   
   
   
   
   
   
   
   /**
    * Getter for property CANTIDAD_FILTROS.
    * @return Value of property CANTIDAD_FILTROS.
    */
   public int getCANTIDAD_FILTROS() {
       return CANTIDAD_FILTROS;
   }
   
   /**
    * Setter for property CANTIDAD_FILTROS.
    * @param CANTIDAD_FILTROS New value of property CANTIDAD_FILTROS.
    */
   public void setCANTIDAD_FILTROS(int CANTIDAD_FILTROS) {
       this.CANTIDAD_FILTROS = CANTIDAD_FILTROS;
   }
   
   
   
   
   /**
    * Getter for property vistaFiltros.
    * @return Value of property vistaFiltros.
    */
   public java.util.List getVistaFiltros() {
       return vistaFiltros;
   }
   
   /**
    * Setter for property vistaFiltros.
    * @param vistaFiltros New value of property vistaFiltros.
    */
   public void setVistaFiltros(java.util.List vistaFiltros) {
       this.vistaFiltros = vistaFiltros;
   }
   
   
   
   
   
   
   /**
    * Getter for property ON.
    * @return Value of property ON.
    */
   public java.lang.String getON() {
       return ON;
   }
   
   /**
    * Setter for property ON.
    * @param ON New value of property ON.
    */
   public void setON(java.lang.String ON) {
       this.ON = ON;
   }
   
   
   
   
   
   
   
   /**
    * Getter for property facturasSeleccionada.
    * @return Value of property facturasSeleccionada.
    */
   public java.util.List getFacturasSeleccionada() {
       return facturasSeleccionada;
   }
   
   /**
    * Setter for property facturasSeleccionada.
    * @param facturasSeleccionada New value of property facturasSeleccionada.
    */
   public void setFacturasSeleccionada(java.util.List facturasSeleccionada) {
       this.facturasSeleccionada = facturasSeleccionada;
   }
   
   
   
   
   
   
   /**
    * Getter for property distrito.
    * @return Value of property distrito.
    */
   public java.lang.String getDistrito() {
       return distrito;
   }
   
   /**
    * Setter for property distrito.
    * @param distrito New value of property distrito.
    */
   public void setDistrito(java.lang.String distrito) {
       this.distrito = distrito;
   }
   
   
   
   
   
   
   /**
    * Getter for property agencia.
    * @return Value of property agencia.
    */
   public java.lang.String getAgencia() {
       return agencia;
   }
   
   
   /**
    * Setter for property agencia.
    * @param agencia New value of property agencia.
    */
   public void setAgencia(java.lang.String agencia) {
       this.agencia = agencia;
   }
   
   
   
   
   
   /**
    * Getter for property listCheques.
    * @return Value of property listCheques.
    */
   public java.util.List getListCheques() {
       return listCheques;
   }   
   
   /**
    * Setter for property listCheques.
    * @param listCheques New value of property listCheques.
    */
   public void setListCheques(java.util.List listCheques) {
       this.listCheques = listCheques;
   }   
   
   
   
   
   
   
   /**
    * Getter for property bloque.
    * @return Value of property bloque.
    */
   public java.util.Hashtable getBloque() {
       return bloque;
   }   
   
   /**
    * Setter for property bloque.
    * @param bloque New value of property bloque.
    */
   public void setBloque(java.util.Hashtable bloque) {
       this.bloque = bloque;
   }   
   
   
   
   
   
   /**
    * Getter for property estadoBtnRegresar.
    * @return Value of property estadoBtnRegresar.
    */
   public boolean isEstadoBtnRegresar() {
       return estadoBtnRegresar;
   }
   
   /**
    * Setter for property estadoBtnRegresar.
    * @param estadoBtnRegresar New value of property estadoBtnRegresar.
    */
   public void setEstadoBtnRegresar(boolean estadoBtnRegresar) {
       this.estadoBtnRegresar = estadoBtnRegresar;
   }
   
   
   
   
   
   
   /**
    * Getter for property bloquePrint.
    * @return Value of property bloquePrint.
    */
   public java.util.Hashtable getBloquePrint() {
       return bloquePrint;
   }
   
   /**
    * Setter for property bloquePrint.
    * @param bloquePrint New value of property bloquePrint.
    */
   public void setBloquePrint(java.util.Hashtable bloquePrint) {
       this.bloquePrint = bloquePrint;
   }
   
   
   
   
   /**
    * Getter for property cantAprobadas.
    * @return Value of property cantAprobadas.
    */
   public int getCantAprobadas() {
       return cantAprobadas;
   }
   
   /**
    * Setter for property cantAprobadas.
    * @param cantAprobadas New value of property cantAprobadas.
    */
   public void setCantAprobadas(int cantAprobadas) {
       this.cantAprobadas = cantAprobadas;
   }
   
   /**
    * Getter for property monedaLocal.
    * @return Value of property monedaLocal.
    */
   public java.lang.String getMonedaLocal() {
       return monedaLocal;
   }
   
   /**
    * Setter for property monedaLocal.
    * @param monedaLocal New value of property monedaLocal.
    */
   public void setMonedaLocal(java.lang.String monedaLocal) {
       this.monedaLocal = monedaLocal;
   }
   /*Operez Modificado 12 enero 2007*/
    
    /**
     * M�todo que busca las facturas aprobadas para pago, agrupadas  por: Corrida, banco, Sucursal, Proveedor
     * teniendo en cuenta distrito y agencia del usuario.
     * @autor.......Osvaldo P�rez Ferrer
     * @throws......Exception
     * @version.....1.0.
     **/
    public void buscarFacturasAprobadasFiltro(String distrito, String agencia) throws Exception{
        try{
            
            String PRE_CORRIDA   = "BL_CORRIDA";
            String PRE_BANCO     = "BL_BANCO";
            String PRE_SUCURSAL  = "BL_SUCURSAL";
            String PRE_PROVEEDOR = "BL_PROVEEDOR";
            String SEPARADOR     = "_";
            
            
            this.distrito = distrito;
            this.agencia  = agencia;
            reset();
            activarFiltro();
            int    cont        = 1;
            
            monedaLocal        = this.ChequeXFacturaDataAccess.getMonedaLocal( this.distrito ) ;
            ChequeXFacturaService svc = new ChequeXFacturaService();
            
            
            if( corrida != null && corrida.length() > 0 ){                                    
                    
                    Hashtable  bloqueLevel0        =  new Hashtable();
                    bloqueLevel0.put("titulo", corrida);
                    bloqueLevel0.put("padre",  "");
                    bloqueLevel0.put("id",     PRE_CORRIDA + SEPARADOR + String.valueOf(0) );
                    bloqueLevel0.put("selection",  "");
                    
                    
                    List listLevel0     = new LinkedList();
                    double totalLevel0  = 0;
                    
                    //--- Bancos:
                    //List listBancos = this.ChequeDataAccess.getBancosConFacturasPago(distrito, corrida, banco, agencia);
                    // for(int j=0; j<listBancos.size();j++){
                    //    String  banco      = (String)listBancos.get(j);
                        
                        
                        Hashtable  bloqueLevel1 =  new Hashtable();
                        bloqueLevel1.put("titulo", banco);
                        bloqueLevel1.put("padre",  PRE_CORRIDA + SEPARADOR + String.valueOf(0)              );
                        bloqueLevel1.put("id",     PRE_BANCO   + SEPARADOR + String.valueOf(0) + SEPARADOR +  String.valueOf(0) ); // String.valueOf(j) );
                        bloqueLevel1.put("selection",  "");
                        
                        List listLevel1    = new LinkedList();
                        double totalLevel1 = 0;
                        
                        //--- Sucursales:
                       // List listSucursales = this.ChequeDataAccess.getSucursalesBancosConFacturasPago(distrito,  corrida, banco, sucursal, agencia);
                       // for(int k=0; k<listSucursales.size();k++){
                          //  String sucursal     = (String)listSucursales.get(k);
                            
                            
                            Hashtable  bloqueLevel2 =  new Hashtable();
                            bloqueLevel2.put("titulo", sucursal);
                            bloqueLevel2.put("padre",  PRE_BANCO    + SEPARADOR + String.valueOf(0) + SEPARADOR +  String.valueOf(0)  ); //+  String.valueOf(j)  );
                            bloqueLevel2.put("id",     PRE_SUCURSAL + SEPARADOR + String.valueOf(0) + SEPARADOR +  String.valueOf(0)  + SEPARADOR + String.valueOf(0)); //String.valueOf(j)  + SEPARADOR + String.valueOf(k));
                            bloqueLevel2.put("selection",  "");
                            
                            List listLevel2     = new LinkedList();
                            double totalLevel2  = 0;
                            
                            //--- Proveedores:
                            List listProveedores = this.ChequeDataAccess.getBeneficiariosFacturasCorrida2(distrito, corrida, banco, sucursal, this.nit, agencia );
                            
                            for(int l=0; l<listProveedores.size();l++){
                                
                                Hashtable  datoProveedor     = ( Hashtable ) listProveedores.get(l);
                                String     proveedor         = ( String    ) datoProveedor.get("nit");
                                String     nombre            = ( String    ) datoProveedor.get("nombre");
                                String     beneficiario      = ( String    ) datoProveedor.get("beneficiario");//Osvaldo
                                String     nit_beneficiario  = ( String    ) datoProveedor.get("nit_beneficiario");//Osvaldo
                                                                
                                
                                Hashtable  bloqueLevel3 =  new Hashtable();
                                bloqueLevel3.put("beneficiario", beneficiario);//Osvaldo - Nombre del beneficiario
                                bloqueLevel3.put("nit_beneficiario", nit_beneficiario);//Osvaldo - Nit del beneficiario
                                bloqueLevel3.put("titulo", nombre);
                                bloqueLevel3.put("padre",  PRE_SUCURSAL   + SEPARADOR + String.valueOf(0) + SEPARADOR +  String.valueOf(0)  + SEPARADOR + String.valueOf(0)  ); //String.valueOf(j)  + SEPARADOR + String.valueOf(k)  );
                                bloqueLevel3.put("id",     PRE_PROVEEDOR  + SEPARADOR + String.valueOf(0) + SEPARADOR +  String.valueOf(0)  + SEPARADOR + String.valueOf(0)  +SEPARADOR+  String.valueOf(l)); //String.valueOf(j)  + SEPARADOR + String.valueOf(k)  +SEPARADOR+  String.valueOf(l));
                                bloqueLevel3.put("selection",  "");
                                double totalLevel3       = 0;
                                
                                //--- Facturas:
                                List listFacturas  =  this.ChequeDataAccess.getFacturasAprobadasBeneficiarios(distrito, corrida, banco, sucursal, proveedor, agencia );
                                List listBLFactura = new LinkedList();
                                for(int m=0; m<listFacturas.size();m++){
                                    FacturasCheques  factura  = (FacturasCheques) listFacturas.get(m);
                                    Total += factura.getVlr_saldo();
                                    factura.setIdObject( cont );
                                    factura.setSelected( false );
                                    factura.setDatoProveedor( datoProveedor );
                                    
                                    
                                    //Cambio de moneda:
                                    String     monedaBanco   = factura.getMonedaBanco();
                                    factura = svc.convertirMoneda( factura , monedaBanco, monedaLocal);
                                    factura.setMonedaLocal( monedaLocal );
                                    
                                    //informacion del bloque al cual pertenece:
                                    Hashtable  blFactura = new  Hashtable();
                                    blFactura.put("corrida"    , corrida  );
                                    blFactura.put("banco"      , banco    );
                                    blFactura.put("sucursal"   , sucursal );
                                    blFactura.put("proveedor"  , proveedor);
                                    blFactura.put("factura"    , factura.getDocumento()   );
                                    blFactura.put("descripcion", factura.getDescripcion() );
                                    blFactura.put("saldo"      , String.valueOf( factura.getVlrPagar() )   );
                                    blFactura.put("padre"      , PRE_PROVEEDOR + SEPARADOR + String.valueOf(0) + SEPARADOR +  String.valueOf(0)  + SEPARADOR +  String.valueOf(0)  + SEPARADOR +  String.valueOf(l)  ); //String.valueOf(j)  + SEPARADOR +  String.valueOf(k)  + SEPARADOR +  String.valueOf(l)  );
                                    blFactura.put("id"         , String.valueOf( factura.getIdObject() )    );
                                    blFactura.put("ck"         , CK_ON );
                                    blFactura.put("level"     , "4"   );
                                    blFactura.put("selection",  "");
                                    
                                    factura.infoBloque = blFactura;
                                    
                                    listBLFactura.add   (  blFactura   );
                                    this.listBloques.add(  blFactura   );
                                    
                                    cont++;
                                    cantAprobadas++;
                                    
                                    // Totales de bloques:
                                    totalLevel3 += factura.getVlrPagar();
                                    totalLevel2 += factura.getVlrPagar();
                                    totalLevel1 += factura.getVlrPagar();
                                    totalLevel0 += factura.getVlrPagar();
                                    
                                    facturasAprobadas.add( factura );
                                }
                                
                                
                                bloqueLevel3.put("items"     , listBLFactura                 );
                                bloqueLevel3.put("total"     , String.valueOf( totalLevel3 ) );
                                bloqueLevel3.put("img"       , this.IMG_EXPAND               );
                                bloqueLevel3.put("estado"    , this.ON                       );
                                bloqueLevel3.put("hijos"     , this.OFF                      );
                                bloqueLevel3.put("llave"     , corrida + banco + sucursal + proveedor );
                                bloqueLevel3.put("level"     , "3"   );
                                bloqueLevel3.put("ck"        , CK_ON );
                                bloqueLevel3.put("fila"      , String.valueOf(0)             );
                                
                                if( listBLFactura.size()>0  ){
                        this.listBloques.add( bloqueLevel3 );
                        listLevel2.add( bloqueLevel3 );
                    }
                                
                            }
                            
                            bloqueLevel2.put("items"     , listLevel2                    );
                            bloqueLevel2.put("total"     ,String.valueOf( totalLevel2 )  );
                            bloqueLevel2.put("img"       , this.IMG_COLAPSE              );
                            bloqueLevel2.put("estado"    , this.ON                       );
                            bloqueLevel2.put("hijos"     , this.ON                       );
                            bloqueLevel2.put("llave"     , corrida + banco + sucursal    );
                            bloqueLevel2.put("level"     , "2"   );
                            bloqueLevel2.put("ck"        , CK_ON );
                            bloqueLevel2.put("fila"      , String.valueOf(0)             );
                            
                            this.listBloques.add( bloqueLevel2 );
                            listLevel1.add( bloqueLevel2 );
                     //   }
                        
                        bloqueLevel1.put("items"     , listLevel1                    );
                        bloqueLevel1.put("total"     ,String.valueOf( totalLevel1 )  );
                        bloqueLevel1.put("img"       , this.IMG_COLAPSE              );
                        bloqueLevel1.put("estado"    , this.ON                       );
                        bloqueLevel1.put("hijos"     , this.ON                       );
                        bloqueLevel1.put("llave"     , corrida + banco               );
                        bloqueLevel1.put("level"     , "1"    );
                        bloqueLevel1.put("ck"        , CK_ON  );
                        bloqueLevel1.put("fila"      , String.valueOf(0)             );
                        
                        this.listBloques.add( bloqueLevel1 );
                        listLevel0.add( bloqueLevel1 );
                  //  }
                    
                    bloqueLevel0.put("items"     , listLevel0                    );
                    bloqueLevel0.put("total"     , String.valueOf( totalLevel0 ) );
                    bloqueLevel0.put("img"       , this.IMG_COLAPSE              );
                    bloqueLevel0.put("estado"    , this.ON                       );
                    bloqueLevel0.put("hijos"     , this.ON                       );
                    bloqueLevel0.put("llave"     , corrida                       );
                    bloqueLevel0.put("level"     , "0"    );
                    bloqueLevel0.put("ck"        , CK_ON  );
                    bloqueLevel0.put("fila"      , String.valueOf(0)             );
                    
                    this.listBloques.add( bloqueLevel0 );
                    vistaFiltros.add( bloqueLevel0 );
               // }
            }
            
        }catch(Exception e){
            throw new Exception( " buscarFacturasAprobadas " + e.getMessage() );
        }
        
    }
     /**
     * M�todo que verifica si existe la corrida dada
         teniendo en cuenta distrito y agencia del usuario.
     * @autor.......Osvaldo P�rez Ferrer
     * @throws......Exception
     * @version.....1.0.
     **/
    public  boolean existeCorridasConFacturasPago( String distrito, String agencia) throws Exception{
        return ChequeDataAccess.existeCorridasConFacturasPago( this.corrida, distrito, agencia, this.banco, this.sucursal, this.nit );
    }
     
    /**
     * M�todos que setea valores nulos
     * @autor.......Osvaldo
     * @version.....1.0.
     **/
    public String reset(String val){
        return (val!=null)? val : "";
    }
    
  
    /**
     * Getter for property corrida.
     * @return Value of property corrida.
     */
    public java.lang.String getCorrida() {
        return corrida;
    }
    
    /**
     * Setter for property corrida.
     * @param corrida New value of property corrida.
     */
    public void setCorrida(java.lang.String corrida) {
        this.corrida = corrida;
    }
    
    
    public void loadRequest( HttpServletRequest request ){
        String corrida   = reset( request.getParameter("corrida")     );
        String banco     = reset( request.getParameter("banco")       );
        String sucursal  = reset( request.getParameter("sucursal")    );
        String nit       = reset( request.getParameter("nit")    );
        
        this.corrida     = corrida.equals("")?  this.corrida  : corrida;        
        this.banco       = banco.equals("")?    this.banco    : banco;
        this.sucursal    = sucursal.equals("")? this.sucursal : sucursal;
        this.nit         = nit.equals("")?      this.nit      : nit; 
    }
    
    
    
    /**
     * Getter for property banco.
     * @return Value of property banco.
     */
    public java.lang.String getBanco() {
        return banco;
    }    
    
    /**
     * Setter for property banco.
     * @param banco New value of property banco.
     */
    public void setBanco(java.lang.String banco) {
        this.banco = banco;
    }    
   
    /**
     * Getter for property sucursal.
     * @return Value of property sucursal.
     */
    public java.lang.String getSucursal() {
        return sucursal;
    }
    
    /**
     * Setter for property sucursal.
     * @param sucursal New value of property sucursal.
     */
    public void setSucursal(java.lang.String sucursal) {
        this.sucursal = sucursal;
    }
    
    /**
     * Getter for property nit.
     * @return Value of property nit.
     */
    public java.lang.String getNit() {
        return nit;
    }
    
    /**
     * Setter for property nit.
     * @param nit New value of property nit.
     */
    public void setNit(java.lang.String nit) {
        this.nit = nit;
    }
    
    /**
     * Getter for property listBancos.
     * @return Value of property listBancos.
     */
    public java.util.List getListBancos() {
        return listBancos;
    }
    
    /**
     * Setter for property listBancos.
     * @param listBancos New value of property listBancos.
     */
    public void setListBancos(java.util.List listBancos) {
        this.listBancos = listBancos;
    }
    
    /**
     * Getter for property listSucursal.
     * @return Value of property listSucursal.
     */
    public java.util.List getListSucursal() {
        return listSucursal;
    }
    
    /**
     * Setter for property listSucursal.
     * @param listSucursal New value of property listSucursal.
     */
    public void setListSucursal(java.util.List listSucursal) {
        this.listSucursal = listSucursal;
    }
    
    /**
     * M�todo que verifica si las corridas permiten factura
     *        con valores en 0
     * @autor.......Jose de la rosa
     * @throws......Exception
     * @version.....1.0.
     **/
    public  boolean permiteCorridasConFacturasCero( String distrito, String corrida ) throws Exception{
        return ChequeDataAccess.permiteCorridasConFacturasCero( distrito, corrida );
    }
    
    /**
     * M�todo que forma los cheques a partir de las facturas seleccionadas
     * @autor      Fernell villacob
     * @modificado Osvaldo Perez 2007-03-31
     * @version 1.0.
     **/
    public String formarCheques() throws Exception{
        this.listCheques   = new LinkedList();
        this.seriesCheques = new LinkedList();
        String  msj        = "";
        boolean facCero    = false;
        
        try{
            
            if(this.facturasSeleccionada.size()>0){
                
                
                // CORRIDAS:
                List listCorridas  = this.corridas_FacturasSelect();
                for(int i=0;i<listCorridas.size();i++){
                    String  corrida = (String)listCorridas.get(i);
                    
                    // insertar funcion de ----
                    facCero = permiteCorridasConFacturasCero( this.distrito, corrida);
                    
                    //BANCOS
                    List listBancos = this.bancosCorridas_FacturasSelect(corrida);
                    for(int j=0;j<listBancos.size();j++){
                        String banco  = (String)listBancos.get(j);
                        
                        ImpresionChequeService ImpSvc          = new ImpresionChequeService(ChequeDataAccess.getDatabaseName());
                        
                        List               listaEsquema        = ImpSvc.getEsquemaCMS( banco);//Osvaldo
                        List               listaEsquemaLaser   = ImpSvc.getEsquema( this.distrito, "LASER "+banco); 
                        
                        if(listaEsquema!=null && listaEsquema.size()>0){
                            
                            // SUCURSALES:
                            List  listSucursales = this.sucursalesBancosCorridas_FacturasSelect(corrida, banco);
                            for(int k=0;k<listSucursales.size();k++){
                                String sucursal  = (String)listSucursales.get(k);
                                
                                /****** Jose 2007-03-30 *****/
                                //se verifica si la sumatoria de las facturas del banco del proveedor es 0
                                double netoCh = 0;
                                boolean swCero = false;
                                
                                //PROVEEDORES:
                                List  listProveedores  = this.proveedorSucursalesBancosCorridas_FacturasSelect(corrida, banco, sucursal);
                                if ( listProveedores.size()>0  ){
                                    for(int l=0;l<listProveedores.size();l++){
                                        String     proveedor       = (String)listProveedores.get(l);
                                        Hashtable  datosProveedor  = this.ChequeXFacturaDataAccess.getBancoProveedor( this.distrito, proveedor);
                                        if( datosProveedor!=null  ){
                                            netoCh = 0;
                                            List  listFacturas  = this.facturasProveedorSucursalesBancosCorridas_FacturasSelect(corrida, banco, sucursal, proveedor );
                                            for(int m=0;m<listFacturas.size();m++){
                                                FacturasCheques  factura  = (FacturasCheques)listFacturas.get(m);
                                                netoCh += factura.getVlrPagar();
                                            }
                                            if( netoCh == 0 )
                                                swCero = true;
                                        }
                                    }
                                }
                                
                                SeriesChequesDAO seriesChequeDAO = new SeriesChequesDAO();//Osvaldo
                                
                                Series serie =  null;
                                Series serieCero =  null;
                                if( swCero && facCero ){
                                    serieCero =  this.ChequeXFacturaDataAccess.getSeries( this.distrito, "BANCO_CERO", "SUCURSAL_CERO" );
                                }
                                
                                //serie =  this.ChequeXFacturaDataAccess.getSeries( this.distrito, banco, sucursal );
                                
                                serie = seriesChequeDAO.getSeries( distrito, banco, sucursal, "CXP", usuario.getId_agencia(), usuario.getLogin() );//Osvaldo
                                
                                if( ( serie!=null && !swCero ) || ( swCero && facCero && serieCero != null && serie!=null ) ){
                                    
                                    int lastNumberSerie  = 0;
                                    int serieProcesada   = 0;
                                    int contSerie        = 0;
                                    int numeroCheque     = 0;
                                    
                                    int lastNumberSerieCero  = 0;
                                    int serieProcesadaCero   = 0;
                                    int contSerieCero        = 0;
                                    int numeroChequeCero     = 0;
                                    boolean swesCero = false;
                                    
                                    if( swCero && facCero ){
                                        // Buscamos en memoria la serie de ese banco, si ha sido procesada
                                        lastNumberSerieCero  = serieCero.getLast_number();
                                        serieProcesadaCero   = getSerieProcesada( "BANCO_CERO", "SUCURSAL_CERO" );
                                        if( serieProcesadaCero > -1 )
                                            lastNumberSerieCero = serieProcesadaCero;
                                        
                                        contSerieCero        = 0;
                                        numeroChequeCero     = lastNumberSerieCero;
                                    }
                                    
                                    // Buscamos en memoria la serie de ese banco, si ha sido procesada
                                    lastNumberSerie  = serie.getLast_number();
                                    serieProcesada   = getSerieProcesada( banco, sucursal );
                                    if( serieProcesada > -1 )
                                        lastNumberSerie = serieProcesada;

                                    contSerie        = 0;
                                    numeroCheque     = lastNumberSerie;
                                    
                                    
                                    /****** Jose 2007-03-30 *****/
                                    
                                    String pageBanco     = this.ChequeDataAccess.pageBanco(banco);
                                    String pageBancoCMS  = this.ChequeDataAccess.pageBanco(banco+"_CMS");//Osvaldo 30-Marzo-2007
                                    
                                    //if( !pageBancoCMS.equals("")  ){//Osvaldo 30-Marzo-2007
                                        
                                        
                                        //PROVEEDORES:
                                        //listProveedores  = this.proveedorSucursalesBancosCorridas_FacturasSelect(corrida, banco, sucursal);
                                        
                                        if ( listProveedores.size()>0  ){
                                            
                                            
                                            // Bloque de Cheques:
                                            Hashtable   bloqueCheques = new Hashtable();
                                            bloqueCheques.put("corrida",    corrida  );
                                            bloqueCheques.put("banco",      banco    );
                                            bloqueCheques.put("sucursal",   sucursal );
                                            bloqueCheques.put("tamanoPage", pageBanco );
                                            bloqueCheques.put("pageBancoCMS", pageBancoCMS );//Osvaldo 30-Marzo-2007
                                            bloqueCheques.put("cuenta",     serie.getCuenta() );
                                            
                                            boolean swCheques = false;
                                            
                                            double netoBloqueCheques  = 0;
                                            List   listaChequesBloque = new LinkedList();
                                            
                                            for(int l=0;l<listProveedores.size();l++){
                                                String     proveedor       = (String)listProveedores.get(l);
                                                
                                                Hashtable  datosProveedor  = this.ChequeXFacturaDataAccess.getBancoProveedor( this.distrito, proveedor);
                                                
                                                
                                                if( datosProveedor!=null  ){
                                                    
                                                    
                                                    String monedaBanco  = (String)datosProveedor.get("moneda");
                                                    double netoCheque   = 0;
                                                    String swMsj        = "";
                                                    
                                                    // FACTURA
                                                    List  listFacturas  = this.facturasProveedorSucursalesBancosCorridas_FacturasSelect(corrida, banco, sucursal, proveedor );
                                                    
                                                    for(int m=0;m<listFacturas.size();m++){
                                                        FacturasCheques  factura  = (FacturasCheques)listFacturas.get(m);
                                                        
                                                        ChequeXFacturaService svc = new ChequeXFacturaService();
                                                        factura      = svc.calcularImpuestos   ( factura );
                                                        factura      = svc.calcularVlrFactura  ( factura );
                                                        monedaBanco  = factura.getMonedaBanco();
                                                        factura      = svc.convertirMoneda     ( factura , monedaBanco, monedaLocal);
                                                        swMsj       += factura.getMsj();
                                                        
                                                        double vlr_neto_me  = factura.getVlrFactura()  + factura.getVlrDescuentos()  + factura.getVlrRetencion();
                                                        factura.setVlrNetoPago( vlr_neto_me );
                                                        
                                                        
                                                        // Documentos relacionados:
                                                        List docRel =  this.ChequeXFacturaDataAccess.getFacturasRelacionadas( factura.getDstrct(), factura.getTipo_documento(), factura.getDocumento(), factura.getProveedor() );
                                                        for(int n=0;n<docRel.size();n++){
                                                            FacturasCheques factRel = (FacturasCheques)docRel.get(n);
                                                            
                                                            if( factRel.getTipoObjecto().equals( this.ChequeXFacturaDataAccess.TIPO_FACTURA  ) ){
                                                                factRel = svc.calcularImpuestos    ( factRel );
                                                                factRel = svc.calcularVlrFactura   ( factRel );
                                                                factRel = svc.convertirMoneda(factRel, monedaBanco, monedaLocal);
                                                                swMsj  += factura.getMsj();
                                                                double  vlrNeto_rel  =   factRel.getVlrFactura() + factRel.getVlrDescuentos() +  factRel.getVlrRetencion();
                                                                factRel.setVlrNetoPago( vlrNeto_rel);
                                                            }
                                                            else{
                                                                factRel = svc.convertirMoneda(factRel, monedaBanco, monedaLocal);
                                                                swMsj  += factura.getMsj();
                                                                factRel.setVlrNetoPago( factRel.getVlrPagar() );
                                                                factRel.setVlrFactura( factRel.getVlrPagar() );
                                                            }
                                                        }
                                                        factura.setDocumnetosRelacionados( docRel );
                                                        
                                                        
                                                        // Neto cheque:
                                                        netoCheque += factura.getVlrPagar();
                                                    }
                                                    
                                                    
                                                    
                                                    
                                                    // CHEQUE:
                                                    
                                                    if ( Integer.parseInt(serie.getSerial_fished_no())  >= numeroCheque || (Integer.parseInt(serieCero.getSerial_fished_no())  >= numeroChequeCero && swCero && facCero ) ){
                                                        
                                                        // Redondeamos valor del cheque para moneda  peso y bolivar
                                                        netoCheque  =  (  monedaBanco.equals("PES") ||  monedaBanco.equals("BOL")  )? (int)  Math.round(  netoCheque ) :  Util.roundByDecimal(netoCheque, 2);
                                                        
                                                        int cheq = ( netoCheque == 0 )?numeroChequeCero:numeroCheque;
                                                        String ser  = ( netoCheque == 0 )?serieCero.getPrefijo():serie.getPrefijo();
                                                        String cuen = ( netoCheque == 0 )?serieCero.getCuenta():serie.getCuenta();
                                                        
                                                        Cheque cheque = new Cheque();
                                                        cheque.setDistrito    ( (String)datosProveedor.get("distrito")  );
                                                        cheque.setCorrida     ( corrida                                 );
                                                        cheque.setBanco       ( banco                                   );
                                                        cheque.setSucursal    ( sucursal                                );
                                                        cheque.setCuentaBanco ( serie.getCuenta()                       );
                                                        cheque.setAgencia     ( this.agencia                            );
                                                        cheque.setMoneda      ( monedaBanco                             );
                                                        cheque.setMonedaLocal ( monedaLocal                             );
                                                        cheque.setMonto       ( netoCheque                              );
                                                        cheque.setVlrPagar    ( netoCheque                              );
                                                        cheque.setSerie       ( ( netoCheque == 0 )?serieCero:serie     );
                                                        cheque.setEsquema     ( listaEsquema                            );
                                                        cheque.setEsquemaLaser( listaEsquemaLaser );
                                                        
                                                        cheque.setNumero      ( ser + cheq                              );
                                                        cheque.setLastNumber  ( cheq                                    );
                                                        
                                                        //cheque.setBeneficiario( (String)datosProveedor.get("nit")     );
                                                        //cheque.setNombre      ( (String)datosProveedor.get("nombre")  );
                                                        cheque.setNitProveedor( (String)datosProveedor.get("nit")     );
                                                        
                                                        cheque.setBeneficiario( (String)datosProveedor.get("nit_beneficiario")     );//Osvaldo
                                                        cheque.setNombre      ( (String)datosProveedor.get("beneficiario")  );//Osvaldo
                                                        
                                                        cheque.setNit_beneficiario( (String)datosProveedor.get("nit_beneficiario")     );//Osvaldo
                                                        cheque.setNom_beneficiario      ( (String)datosProveedor.get("beneficiario")  );//Osvaldo
                                                        
                                                        cheque.setAno         ( Util.getFechaActual_String(1)         );
                                                        cheque.setMes         ( Util.getFechaActual_String(3)         );
                                                        cheque.setDia         ( Util.getFechaActual_String(5)         );
                                                        
                                                        // Facturas del Cheque
                                                        cheque.setFacturas( listFacturas );
                                                        
                                                        if( !cheque.getNombre().equals("") ){//Osvaldo
                                                            
                                                            if( ( netoCheque<=0 && !facCero || netoCheque<0 && facCero )   ||  !swMsj.equals("")  ){// jose
                                                                if( !swMsj.equals("") )  msj+= swMsj ;
                                                                else                     msj+= "No se puede imprimir el cheque para ["+ (String)datosProveedor.get("nit") +"] "+ (String)datosProveedor.get("nombre")  +" su valor no es positivo";
                                                            }else{
                                                                netoBloqueCheques += netoCheque;
                                                                swCheques = true;
                                                                listaChequesBloque.add( cheque );
                                                                
                                                                if( netoCheque == 0 ){
                                                                    contSerieCero++;
                                                                    numeroChequeCero    =  lastNumberSerieCero  + contSerieCero;
                                                                    swesCero = true;
                                                                }
                                                                else{
                                                                    contSerie++;
                                                                    numeroCheque        =  lastNumberSerie  + contSerie;
                                                                }
                                                            }
                                                            
                                                        }else{//Osvaldo
                                                            msj += "El Nit Beneficiario: "+cheque.getBeneficiario()+" no presenta nombre";
                                                        }
                                                    }
                                                    else{
                                                        msj += " La serie para el banco " + banco +" "+ sucursal +" ha llegado a su tope maximo" ;
                                                        break;
                                                    }
                                                    
                                                    
                                                    
                                                    
                                                }
                                                else
                                                    msj += "El proveedor " +  proveedor +"no presenta informaci�n completa con bancos";
                                                
                                                
                                            }
                                            
                                            
                                            
                                            bloqueCheques.put("neto",    String.valueOf( netoBloqueCheques ) );
                                            bloqueCheques.put("cheques", listaChequesBloque );
                                            
                                            if( swCheques )   // Si el bloque contiene cheques
                                                listCheques.add( bloqueCheques );
                                            
                                            
                                        }else
                                            msj+= "No hay proveedores para "+ banco +" "+ sucursal;
                                        
                                        numeroCheque = swesCero? numeroChequeCero : numeroCheque;
                                        banco = swesCero? "BANCO_CERO" : banco;
                                        sucursal = swesCero? "SUCURSAL_CERO" : sucursal;
                                        
                                        // Serie procesada
                                        Hashtable procesada = new  Hashtable();
                                        procesada.put("banco",     banco    );
                                        procesada.put("sucursal",  sucursal );
                                        procesada.put("procesado", String.valueOf( numeroCheque ) );
                                        
                                        if( serieProcesada == -1 )
                                            this.seriesCheques.add( procesada  );
                                        else
                                            this.updateProcesada(banco, sucursal, numeroCheque  );
                                        
                              /*          
                                    }
                                    else
                                        msj+= " El banco " + banco +" para el distrito " + this.distrito + " no tiene tama�o de papel definido en tabla general";
                               **/
                                }
                                else
                                    if(!swCero)
                                        msj += "No hay serie definida para el banco " + banco +" "+ sucursal + " para Pago a Proveedores o la serie no est� asignada al usuario en sesi�n " + usuario.getLogin();
                                    else
                                        if( !facCero )
                                            msj+= " La corrida no puede imprimir valores en 0 ";
                                        else
                                            msj+= " No existe serie para cheques con valor 0 del distrito " + this.distrito;
                            }
                        }
                        else
                            msj = " No hay esquema de Impresi�n definido para el banco " + banco +" del distrito " + this.distrito;
                        
                    }
                }
            }
            else
                msj = "Deber� seleccionar las facturas que desea pagar.";

            
            if( listCheques.size()==0)
                msj = "NO SE PUDO GENERAR BLOQUE DE CHEQUES :<br>" + msj;
            
            
            
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( "formarCheques:" + e.getMessage());
        }
        return  msj;
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public com.tsp.operation.model.beans.Usuario getUsuario() {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(com.tsp.operation.model.beans.Usuario usuario) {
        this.usuario = usuario;
    }
    
    /**
     * Getter for property idSerie.
     * @return Value of property idSerie.
     */
    public int getIdSerie() {
        return idSerie;
    }
    
    /**
     * Setter for property idSerie.
     * @param idSerie New value of property idSerie.
     */
    public void setIdSerie(int idSerie) {
        this.idSerie = idSerie;
    }
    
}
