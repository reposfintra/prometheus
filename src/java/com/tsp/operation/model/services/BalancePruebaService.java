/*
 * BalancePruebaService.java
 * Modificado: Ing. Iv�n Devia
 *
 * Created on 31 de julio de 2005, 11:29
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.util.*;



public class BalancePruebaService {
    
//    private BalancePruebaDAO        BPData;
//    private BalancePruebas          BPruebas;
//    private String                  Ano;
//    private int                     Mes;
//    private List                    ListaBalance;
//    private BPElementosGasto        BPElementos;
//    private BPUnidadesNegocio       BPUnidades;
//    private BPElementosGasto        BPElementosA;
//    private BPUnidadesNegocio       BPUnidadesA;
//    private BPCuentasNumericas      BPCuentasN;
//    private BPCuentasAlpha          BPCuentasA;
//    private List                    ListaIngreso;
    
    
    /** Creates a new instance of ReportesService */
    public BalancePruebaService() {
//        BPData          = new BalancePruebaDAO();
//        BPruebas        = new BalancePruebas();
//        BPElementos     = new BPElementosGasto();
//        BPUnidades      = new BPUnidadesNegocio();
//        BPElementosA    = new BPElementosGasto();
//        BPUnidadesA     = new BPUnidadesNegocio();
//        BPCuentasN      = new BPCuentasNumericas();
//        BPCuentasA      = new BPCuentasAlpha();
    }
    
//    public void TipoBalance( String distrito, int mes, String anio ) throws Exception{
//        try{
//            
//            ListaBalance = BPData.ListBalance(distrito, mes, anio);
//            
//            /*
//            for( int i=0;i<ListaBalance.size();i++ ){
//                //System.out.println( ((BPCuentas)(ListaBalance.get(i))).getElemento() );
//            }*/
//            
//            List ListaElementos = this.buscarDistintosElementos(ListaBalance);
//            //System.gc();
//            
//            /*Totales Variables Tipo Balance*/
//            double TotalSaldoAntB        = 0.0;
//            double TotalMDebB            = 0.0;
//            double TotalMCreB            = 0.0;
//            double TotalSaldoAcB         = 0.0;
//            
//            //System.gc();
//            for ( int i = 0; i < ListaElementos.size(); i++ ){
//                
//                /*Totales Variables Elementos*/
//                double TotalSaldoAntE        = 0.0;
//                double TotalMDebE            = 0.0;
//                double TotalMCreE            = 0.0;
//                double TotalSaldoAcE         = 0.0;
//                //System.gc();
//                
//                BPCuentas bp = (BPCuentas)ListaElementos.get(i);
//                BPElementos = new BPElementosGasto();
//                BPElementos.setCodigo(bp.getElemento());
//                //Llenar descripcion
//                
//                //BPCuentasN.addElementos(bp);
//                List DistUnidades   = this.buscarDistintasUnidades(ListaBalance,bp.getElemento());
//                //System.gc();
//                for ( int j = 0; j < DistUnidades.size(); j++ ){
//                    
//                    /*Totales Variable Unidades*/
//                    double TotalSaldoAntU        = 0.0;
//                    double TotalMDebU            = 0.0;
//                    double TotalMCreU            = 0.0;
//                    double TotalSaldoAcU         = 0.0;
//                    
//                    BPCuentas bpu = (BPCuentas)DistUnidades.get(j);
//                    BPUnidades  = new BPUnidadesNegocio();
//                    BPUnidades.setCodigo(bpu.getUnidad());
//                    //Llenar Descripcion
//                    
//                    List ListCuentas     = this.buscarCuentas(ListaBalance, bp.getElemento(), bpu.getUnidad());
//                    //System.gc();
//                    for( int k = 0; k < ListCuentas.size(); k++ ){
//                        
//                        BPCuentas bpc  = (BPCuentas)ListCuentas.get(k);
//                        bpc.CalcularSaldos(mes);
//                        /*Saldos Totales*/
//                        
//                        TotalSaldoAntU  += bpc.getSaldoAnterior();
//                        TotalMDebU      += bpc.getMovDebito();
//                        TotalMCreU      += bpc.getMovCredito();
//                        TotalSaldoAcU   += bpc.getSaldoActual();
//                        BPUnidades.addCuentas(bpc);
//                        //System.gc();
//                    }//Fin Cuentas
//                    
//                    
//                    BPUnidades.setMovCredito(TotalMCreU);
//                    BPUnidades.setMovDebito(TotalMDebU);
//                    BPUnidades.setSaldoActual(TotalSaldoAcU);
//                    BPUnidades.setSaldoAnterior(TotalSaldoAntU);
//                    //System.gc();
//                    /*Total Unidades*/
//                    TotalSaldoAntE        += TotalSaldoAntU;
//                    TotalMDebE            += TotalMDebU;
//                    TotalMCreE            += TotalMCreU;
//                    TotalSaldoAcE         += TotalSaldoAcU;
//                    //System.gc();
//                    BPElementos.addUnidades(BPUnidades);
//                }//Fin de Cuentas
//                
//                
//                BPElementos.setSaldoActual(TotalSaldoAcE);
//                BPElementos.setSaldoAnterior(TotalSaldoAntE);
//                BPElementos.setMovCredito(TotalMCreE);
//                BPElementos.setMovDebito(TotalMDebE);
//                //System.gc();
//                TotalSaldoAntB        += TotalSaldoAntE ;
//                TotalMDebB            += TotalMDebE;
//                TotalMCreB            += TotalMCreE;
//                TotalSaldoAcB         += TotalSaldoAcE;
//                //System.gc();
//                BPCuentasN.addElementos(BPElementos);
//                //System.gc();
//            }//Fin Elementos
//            
//            //System.gc();
//            
//            BPCuentasN.setSaldoActual(TotalSaldoAcB);
//            BPCuentasN.setSaldoAnterior(TotalSaldoAntB);
//            BPCuentasN.setMovCredito(TotalMCreB);
//            BPCuentasN.setMovDebito(TotalMDebB);
//            
//            BPruebas.setBpn(BPCuentasN);
//            //System.gc();
//            
//            //Falta Cuentas Alpha
//            this.Ano = anio;
//            this.Mes = mes;
//            // GenerarCalculos();
//            
//        }catch (Exception ex){
//            throw new Exception("___Error en la rutina BalancePruebas en [BalancePruebaService] ....\n"+ ex.getMessage());
//        }
//        //System.gc();
//        
//    }
//    
//    public void TipoIngresos( String distrito, int mes, String anio ) throws Exception{
//        try{
//            
//            ListaIngreso = BPData.ListIngreso(distrito, mes, anio);
//            
//            List ListaElementos = this.buscarDistintosElementos(ListaIngreso);
//            
//            /*Totales Variables Tipo Ingresos*/
//            double TotalSaldoAntI        = 0.0;
//            double TotalMDebI            = 0.0;
//            double TotalMCreI            = 0.0;
//            double TotalSaldoAcI         = 0.0;
//            
//            //System.gc();
//            for ( int i = 0; i < ListaElementos.size(); i++ ){
//                
//                
//                
//                /*Totales Variables Elementos*/
//                double TotalSaldoAntEA        = 0.0;
//                double TotalMDebEA            = 0.0;
//                double TotalMCreEA            = 0.0;
//                double TotalSaldoAcEA         = 0.0;
//                
//                
//                BPCuentas bp = (BPCuentas)ListaElementos.get(i);
//                BPElementosA = new BPElementosGasto();
//                BPElementosA.setCodigo(bp.getElemento());
//                BPElementosA.setDescripcionElemento(bp.getDescripcionElemento());
//                //Llenar descripcion
//                ////System.out.println("paso4a");
//                //BPCuentasN.addElementos(bp);
//                List DistUnidades   = this.buscarDistintasUnidades(ListaIngreso,bp.getElemento());
//                //System.gc();
//                for ( int j = 0; j < DistUnidades.size(); j++ ){
//                    ////System.out.println("paso5a");
//                    /*Totales Variable Unidades*/
//                    double TotalSaldoAntUA        = 0.0;
//                    double TotalMDebUA            = 0.0;
//                    double TotalMCreUA            = 0.0;
//                    double TotalSaldoAcUA         = 0.0;
//                    
//                    BPCuentas bpu = (BPCuentas)DistUnidades.get(j);
//                    BPUnidadesA  = new BPUnidadesNegocio();
//                    BPUnidadesA.setCodigo(bpu.getUnidad());
//                    BPUnidadesA.setDescripcionUnidad(bpu.getDescripcionUnidad());
//                    //Llenar Descripcion
//                    
//                    List ListCuentas     = this.buscarCuentas(ListaIngreso, bp.getElemento(),
//                    bpu.getUnidad());
//                    //System.gc();
//                    for( int k = 0; k < ListCuentas.size(); k++ ){
//                        ////System.out.println("paso6a");
//                        BPCuentas bpc  = (BPCuentas)ListCuentas.get(k);
//                        bpc.CalcularSaldos(mes);
//                        /*Saldos Totales*/
//                        
//                        TotalSaldoAntUA  += bpc.getSaldoAnterior();
//                        TotalMDebUA      += bpc.getMovDebito();
//                        TotalMCreUA      += bpc.getMovCredito();
//                        TotalSaldoAcUA   += bpc.getSaldoActual();
//                        BPUnidadesA.addCuentas(bpc);
//                        //System.gc();
//                        
//                    }//Fin Cuentas
//                    
//                    //System.gc();
//                    BPUnidadesA.setMovCredito(TotalMCreUA);
//                    BPUnidadesA.setMovDebito(TotalMDebUA);
//                    BPUnidadesA.setSaldoActual(TotalSaldoAcUA);
//                    BPUnidadesA.setSaldoAnterior(TotalSaldoAntUA);
//                    //System.gc();
//                    
//                    /*Total Unidades*/
//                    TotalSaldoAntEA        += TotalSaldoAntUA;
//                    TotalMDebEA            += TotalMDebUA;
//                    TotalMCreEA            += TotalMCreUA;
//                    TotalSaldoAcEA         += TotalSaldoAcUA;
//                    
//                    BPElementosA.addUnidades(BPUnidadesA);
//                    //System.gc();
//                }//Fin de Cuentas
//                
//                
//                BPElementosA.setSaldoActual(TotalSaldoAcEA);
//                BPElementosA.setSaldoAnterior(TotalSaldoAntEA);
//                BPElementosA.setMovCredito(TotalMCreEA);
//                BPElementosA.setMovDebito(TotalMDebEA);
//                //System.gc();
//                TotalSaldoAntI        += TotalSaldoAntEA;
//                TotalMDebI            += TotalMDebEA;
//                TotalMCreI            += TotalMCreEA;
//                TotalSaldoAcI         += TotalSaldoAcEA;
//                
//                BPCuentasA.addElementos(BPElementosA);
//                //System.gc();
//                
//            }//Fin Elementos
//            
//            //System.gc();
//            
//            BPCuentasA.setSaldoActual(TotalSaldoAcI);
//            BPCuentasA.setSaldoAnterior(TotalSaldoAntI);
//            BPCuentasA.setMovCredito(TotalMCreI);
//            BPCuentasA.setMovDebito(TotalMDebI);
//            
//            BPruebas.setBpa(BPCuentasA);
//            //System.gc();
//            //Falta Cuentas Alpha
//            this.Ano = anio;
//            this.Mes = mes;
//            // GenerarCalculos();
//            
//            //System.gc();
//        }catch (Exception ex){
//            throw new Exception("Error en la rutina BalancePruebas en [BalancePruebaService] ....\n"+
//            ex.getMessage());
//        }
//        //System.gc();
//        
//    }
//    
//    
//    
//    public void GenerarCalculos(){
//        if (BPruebas!=null){
//            List listaElementos  = BPruebas.getBpn().getElementoGasto();
//            /*Elementos gasto*/
//            for(int i = 0; i<listaElementos.size() ; i++){
//                BPElementosGasto bpe = (BPElementosGasto) listaElementos.get(i);
//                
//                /*Unidades*/
//                for(int j = 0; j< bpe.getUnidadesNegocio().size() ; j++){
//                    BPUnidadesNegocio bpu = (BPUnidadesNegocio) bpe.getUnidadesNegocio().get(j);
//                    /*Cuentas*/
//                    //System.gc();
//                    for(int k = 0; k< bpu.getCuentas().size() ; k++){
//                        BPCuentas bpc = (BPCuentas) bpu.getCuentas().get(k);
//                        bpc.setAno(Ano);
//                        bpc.CalcularSaldos(Mes);
//                        //System.gc();
//                    }
//                    bpu.setAno(Ano);
//                    bpu.CalcularSaldos(Mes);
//                    //System.gc();
//                }
//                bpe.setAno(Ano);
//                bpe.CalcularSaldos(Mes);
//                //System.gc();
//            }
//            BPruebas.getBpn().CalcularSaldos(Mes);//Faltan Totales
//            //System.gc();
//        }
//    }
//
//    
//    
//    public BalancePruebas getBalance(){
//        return BPruebas;
//    }
//    
//    public String getAno(){
//        return Ano;
//    }
//    public int getMes(){
//        return Mes;
//    }
//    
//    
//    
//    /**/
//    public List buscarDistintosElementos(List lista){
//        List DistintoElementos = new LinkedList();
//        Vector v = new Vector();
//        for(int i=0;i<lista.size();i++){
//            BPCuentas item = (BPCuentas) lista.get(i);
//            //boolean sw = true;
//            if( !v.contains(item.getElemento()) ){
//                DistintoElementos.add(item);
//                v.add(item.getElemento());
//            }
//            //System.gc();
//            
//        }
//        //System.gc();
//        return DistintoElementos;
//        
//    }
//    
//    public List buscarDistintasUnidades(List lista, String Elemento){
//        List DistintoUnidades = new LinkedList();
//        Vector v = new Vector();
//        for(int i=0;i<lista.size();i++){
//            BPCuentas item = (BPCuentas) lista.get(i);
//            
//            if(item.getElemento().equals(Elemento)){                                
//                if( !v.contains(item.getUnidad()) ){
//                   DistintoUnidades.add(item);
//                   v.add(item.getUnidad());
//                }        
//                //System.gc();        
//            } 
//        }
//        //System.gc();
//        return DistintoUnidades;
//        
//    }
//    
//    public List buscarCuentas(List lista, String Elemento,String Unidad){
//        List Cuentas = new LinkedList();
//        for(int i=0;i<lista.size();i++){
//            BPCuentas item = (BPCuentas) lista.get(i);
//            if(item.getElemento().equals(Elemento) && item.getUnidad().equals(Unidad)){
//                Cuentas.add(item);
//            }
//            //System.gc();
//        }
//        //System.gc();
//        return Cuentas;
//        
//    }
//    
//    /**
//     * Getter for property BPCuentasN.
//     * @return Value of property BPCuentasN.
//     */
//    public com.tsp.operation.model.beans.BPCuentasNumericas getBPCuentasN() {
//        return BPCuentasN;
//    }
//    
//    /**
//     * Setter for property BPCuentasN.
//     * @param BPCuentasN New value of property BPCuentasN.
//     */
//    public void setBPCuentasN(com.tsp.operation.model.beans.BPCuentasNumericas BPCuentasN) {
//        this.BPCuentasN = BPCuentasN;
//    }
//    /*
//    public static void main(String[]kasl)throws Exception{
//        BalancePruebaService b = new BalancePruebaService();
//        
//        
//        long ini = 0;
//        long fin = 0;
//        
//        ini = System.currentTimeMillis();
//        
//        //b.TipoBalance("FINV", 1, "2007");
//        b.TipoIngresos("FINV", 9, "2006");
//        
//        fin = System.currentTimeMillis();
//        
//        //System.out.println( (fin - ini)/1000 +" seconds" );
//        
//        //b.buscarDistintosElementos(new LinkedList());
//    }*/
}
