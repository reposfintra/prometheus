/****************************************************************************************************
 * Nombre clase: DescuentoEtapaService.java                                                         *
 * Descripci�n: Clase que maneja los servicios al model relacionados con los dsecuentos de equipos. *
 * Autor: Ing. Jose de la rosa                                                                      *
 * Fecha: 5 de julio de 2006, 10:11 AM                                                              *
 * Versi�n: Java 1.0                                                                                *
 * Copyright: Fintravalores S.A. S.A.                                                          *
 ***************************************************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;

/**
 *
 * @author  EQUIPO13
 */
public class DescuentoEtapaService {
    
    private Model  model;
    
    
    /** Creates a new instance of DescuentoEtapaService */
    public DescuentoEtapaService() {
        model = new Model();
    }
    
    public static void main(String a [])throws Exception{
        DescuentoEtapaService  x = new DescuentoEtapaService();
        List ja = x.procesoDescuento("646835", "FINV", "R39884",50000, "HOSORIO", "COL", "BQ", "PES", "CONSULTA");
    }
    
    public List procesoDescuento(String numpla, String distrito, String trailer, float valor, String usuario, String base, String agencia, String moneda_local, String consulta ) throws SQLException{
        try{
            
            OPItems  item = null;
            List listaOP = new LinkedList();
            String fecha_actual = Util.getFechaActual_String(6);
            String mes = fecha_actual.substring(0,7);
            float total = 0;
            Descuento des = null;
            Movpla mo = null;
            Vector consultas = new Vector();
            //obtnien los trailer de una planilla
            List lista = model.planillaService.obtenerCamposTrailer(trailer, numpla);
            if( lista != null && lista.size() > 0 ){
                Iterator it = lista.iterator();
                while(it.hasNext()){
                    Planilla planilla = (Planilla) it.next();
                    //obtiene los trailer q existan en la tabla clase equipo
                    model.tablaGenService.buscarDatos("EQPROPIO",trailer);
                    TablaGen equipo = model.tablaGenService.getTblgen();
                    if ( equipo != null ){
                        //verifica el la tabla descuento si existe la planilla en el a�o y mes de proceso
                        //if(!model.descuentoService.existDescuentoPlanillaFecha ( planilla.getNumpla (), mes)){
                        if( model.FlotaSvc.existStandar( planilla.getSj(), distrito, planilla.getPlaveh() ) ){
                            //Se verifica si en el acuerdo especia existe el standar job y el tipo es CABEZOTE
                             model.acuerdo_especialService.existStandarTipo( planilla.getSj(),"CABEZOTE",distrito, "S" );
                            Vector ac =  model.acuerdo_especialService.getAcuerdo_especiales();
                            if( ac !=null && ac.size() > 0 ){
                                for(int i = 0; i < ac.size(); i++){
                                    Acuerdo_especial a = (Acuerdo_especial) ac.get(i);
                                    total = (int)Math.round( ( valor * ( (float) a.getPorcentaje_descuento() / 100 ) ) );
                                    if( total > 0){
                                        
                                        // Ingreso del descuento
                                        des = new Descuento();
                                        des.setNum_planilla(planilla.getNumpla());
                                        des.setPlaca_cabezote(planilla.getPlaveh());
                                        des.setPlaca_trailer(planilla.getPlatlr());
                                        des.setStd_job_no( planilla.getSj() );
                                        des.setValor( total );
                                        des.setFecha_proceso( mes );
                                        des.setClase_equipo( equipo.getReferencia() );
                                        des.setTipo_acuerdo( a.getTipo_acuerdo() );
                                        des.setCodigo_concepto( a.getCodigo_concepto() );
                                        des.setProveedor( planilla.getNitpro() );
                                        des.setNum_equipo( equipo.getDato() );
                                        des.setUsuario_creacion( usuario );
                                        des.setUsuario_modificacion( usuario );
                                        des.setBase(base);
                                        des.setDistrito(distrito);
                                        
                                        //Ingreso MovPla
                                        mo = new Movpla();
                                        mo.setDstrct( distrito );
                                        mo.setAgency_id( agencia );
                                        mo.setDocument_type( "001" );
                                        mo.setConcept_code( a.getCodigo_concepto() );
                                        mo.setPla_owner( planilla.getNitpro() );
                                        mo.setPlanilla( numpla );
                                        mo.setSupplier( planilla.getPlaveh() );
                                        mo.setAp_ind("S");
                                        mo.setVlr( total );
                                        mo.setCurrency( moneda_local );
                                        mo.setCreation_user( usuario );
                                        mo.setInd_vlr("V");
                                        mo.setVlr_for( total );
                                        mo.setCreation_date( "now()" );
                                        mo.setSucursal( "" );
                                        mo.setBanco( "" );
                                        mo.setCuenta( "" );
                                        mo.setCantidad( 0 );
                                        mo.setReanticipo( "N" );
                                        mo.setFecha_migracion( "now()" );
                                        
                                        if( consulta.equals("CONSULTA") ){
                                            //adiciono a la lista de el objeto de OP
                                            OpDAO dOP  =  new OpDAO();
                                            String abc = "";
                                            try{
                                                abc = dOP.getABC( agencia );
                                                model.concepto_equipoService.searchConcepto_equipo( mo.getConcept_code() );
                                            }catch(Exception ex){
                                                ex.getMessage();
                                            }
                                            item = new OPItems();
                                            item.setDstrct        ( distrito );
                                            item.setDescripcion   ( mo.getConcept_code() );
                                            item.setVlr           ( mo.getVlr()* -1 );   // Negativo para la consulta
                                            item.setIndicador     ( "V" );
                                            item.setAsignador     ( "S" );
                                            item.setMoneda        ( moneda_local );
                                            item.setAgencia       ( agencia );
                                            item.setCodigo_cuenta( model.descuentoService.obtenerCodigoCuenta(distrito, planilla.getNitpro(), mo.getConcept_code(), planilla.getNumpla() ) );
                                            item.setCodigo_abc    ( abc );
                                            item.setCheque        ( "" );//ivan Gomez
                                            item.setConcepto      ( mo.getConcept_code() );
                                            item.setDescconcepto  ( model.concepto_equipoService.getConcepto_equipo().getDescripcion()  );
                                            item.setTipo_cuenta   ( "Q" );
                                            item.setBase          ( base );
                                            listaOP.add( item );
                                        }
                                        
                                        if( !model.descuentoService.existDescuentoMantenimento( distrito, planilla.getNumpla(),  a.getCodigo_concepto() ) &&
                                        !model.descuentoService.existDescuentoMovPlanilla(planilla.getNumpla(),  a.getCodigo_concepto(), planilla.getNitpro()) && !consulta.equals("CONSULTA") ){
                                            
                                            consultas.add( model.descuentoService.insertDescuento(des) );
                                            //consultas.add( model.movplaService.insertMovPla( mo, base ) );
                                        }
                                    }
                                }
                            }
                            else{
                                //Se verifica si en la tabla clase de equipo existe el equipo y el tipo sea VIAJE
                                model.descuento_claseService.existeClase( equipo.getReferencia() ,"VIAJE", distrito, planilla.getPlaveh(), planilla.getClientes());
                                Vector descV = model.descuento_claseService.getDescuento_clases();
                                if( descV != null && descV.size() > 0 ){
                                    for(int i = 0; i < descV.size(); i++){
                                        Descuento_clase d = (Descuento_clase) descV.get(i);
                                        total = (int)Math.round( ( valor * ( (float) d.getPorcentaje_descuento() / 100 ) )) ;
                                        if( total > 0){
                                            
                                            // Ingreso del descuento
                                            des = new Descuento();
                                            des.setNum_planilla(planilla.getNumpla());
                                            des.setPlaca_cabezote(planilla.getPlaveh());
                                            des.setPlaca_trailer(planilla.getPlatlr());
                                            des.setStd_job_no( planilla.getSj() );
                                            des.setValor( total );
                                            des.setFecha_proceso( mes );
                                            des.setClase_equipo( equipo.getReferencia() );
                                            des.setTipo_acuerdo( "CABEZOTE" );
                                            des.setCodigo_concepto( d.getCodigo_concepto() );
                                            des.setProveedor( planilla.getNitpro() );
                                            des.setNum_equipo( equipo.getDato() );
                                            des.setUsuario_creacion( usuario );
                                            des.setUsuario_modificacion( usuario );
                                            des.setBase(base);
                                            des.setDistrito(distrito);
                                            consultas.add( model.descuentoService.insertDescuento(des) );
                                            
                                            //Ingreso MovPla
                                            mo = new Movpla();
                                            mo.setDstrct( distrito );
                                            mo.setAgency_id( agencia );
                                            mo.setDocument_type( "001" );
                                            mo.setConcept_code( d.getCodigo_concepto() );
                                            mo.setPla_owner( planilla.getNitpro() );
                                            mo.setPlanilla( numpla );
                                            mo.setSupplier( planilla.getPlaveh() );
                                            mo.setAp_ind("S");
                                            mo.setVlr( total );
                                            mo.setCurrency( moneda_local );
                                            mo.setProveedor("890103161");
                                            mo.setCreation_user( usuario );
                                            mo.setInd_vlr("V");
                                            mo.setVlr_for( total );
                                            mo.setCreation_date( "now()" );
                                            mo.setSucursal( "" );
                                            mo.setBanco( "" );
                                            mo.setCuenta( "" );
                                            mo.setCantidad( 0 );
                                            mo.setReanticipo( "N" );
                                            mo.setFecha_migracion( "now()" );
                                            
                                            if( consulta.equals("CONSULTA") ){
                                                //adiciono a la lista de el objeto de OP
                                                OpDAO dOP  =  new OpDAO();
                                                String abc = "";
                                                try{
                                                    abc = dOP.getABC( agencia );
                                                    model.concepto_equipoService.searchConcepto_equipo( mo.getConcept_code() );
                                                }catch(Exception ex){
                                                    ex.getMessage();
                                                }
                                                item = new OPItems();
                                                item.setDstrct        ( distrito );
                                                item.setDescripcion   ( mo.getConcept_code() );
                                                item.setVlr           ( mo.getVlr()* -1 );   // Negativo para la consulta
                                                item.setIndicador     ( "V" );
                                                item.setAsignador     ( "S" );
                                                item.setMoneda        ( moneda_local );
                                                item.setAgencia       ( agencia );
                                                item.setCodigo_cuenta( model.descuentoService.obtenerCodigoCuenta(distrito, planilla.getNitpro(), mo.getConcept_code(), planilla.getNumpla() ) );
                                                item.setCodigo_abc    ( abc );
                                                item.setCheque        ( "" );//ivan Gomez
                                                item.setConcepto      ( mo.getConcept_code() );
                                                item.setDescconcepto  ( model.concepto_equipoService.getConcepto_equipo().getDescripcion()  );
                                                item.setTipo_cuenta   ( "Q" );
                                                item.setBase          ( base );
                                                listaOP.add( item );
                                            }
                                            
                                            if( !model.descuentoService.existDescuentoMantenimento( distrito, planilla.getNumpla(),  d.getCodigo_concepto() ) &&
                                            !model.descuentoService.existDescuentoMovPlanilla( planilla.getNumpla(),  d.getCodigo_concepto(), planilla.getNitpro() ) && !consulta.equals("CONSULTA") ){
                                                
                                                consultas.add( model.descuentoService.insertDescuento(des) );
                                                //consultas.add( model.movplaService.insertMovPla( mo, base ) );
                                            }
                                        }
                                        
                                    }
                                }
                                else{
                                    //Se verifica si en la tabla clase de equipo existe el equipo y el tipo sea MES
                                    model.descuento_claseService.existeClase( equipo.getReferencia() ,"MES", distrito, planilla.getPlaveh(), planilla.getClientes());
                                    Vector descM = model.descuento_claseService.getDescuento_clases();
                                    if( descM != null && descM.size() >0 ){
                                        for(int i = 0; i < descM.size(); i++){
                                            Descuento_clase d = (Descuento_clase) descM.get(i);
                                            total = (int)Math.round( ( valor * ( (float) d.getPorcentaje_descuento() / 100 ) ));
                                            if( total > 0){
                                                
                                                // Ingreso del descuento
                                                des = new Descuento();
                                                des.setNum_planilla(planilla.getNumpla());
                                                des.setPlaca_cabezote(planilla.getPlaveh());
                                                des.setPlaca_trailer(planilla.getPlatlr());
                                                des.setStd_job_no( planilla.getSj() );
                                                des.setValor( total );
                                                des.setFecha_proceso( mes );
                                                des.setClase_equipo( equipo.getReferencia() );
                                                des.setTipo_acuerdo( "CABEZOTE" );
                                                des.setCodigo_concepto( d.getCodigo_concepto() );
                                                des.setProveedor( planilla.getNitpro() );
                                                des.setNum_equipo( equipo.getDato() );
                                                des.setUsuario_creacion( usuario );
                                                des.setUsuario_modificacion( usuario );
                                                des.setBase(base);
                                                des.setDistrito(distrito);
                                                
                                                //Ingreso MovPla
                                                mo = new Movpla();
                                                mo.setDstrct( distrito );
                                                mo.setAgency_id( agencia );
                                                mo.setDocument_type( "001" );
                                                mo.setConcept_code( d.getCodigo_concepto() );
                                                mo.setPla_owner( planilla.getNitpro() );
                                                mo.setPlanilla( numpla );
                                                mo.setSupplier( planilla.getPlaveh() );
                                                mo.setAp_ind("S");
                                                mo.setVlr( total );
                                                mo.setCurrency( moneda_local );
                                                mo.setProveedor("890103161");
                                                mo.setCreation_user( usuario );
                                                mo.setInd_vlr("V");
                                                mo.setVlr_for( total );
                                                mo.setCreation_date( "now()" );
                                                mo.setSucursal( "" );
                                                mo.setBanco( "" );
                                                mo.setCuenta( "" );
                                                mo.setCantidad( 0 );
                                                mo.setReanticipo( "N" );
                                                mo.setFecha_migracion( "now()" );
                                                
                                                if( consulta.equals("CONSULTA") ){
                                                    //adiciono a la lista de el objeto de OP
                                                    OpDAO dOP  =  new OpDAO();
                                                    String abc = "";
                                                    try{
                                                        abc = dOP.getABC( agencia );
                                                        model.concepto_equipoService.searchConcepto_equipo( mo.getConcept_code() );
                                                    }catch(Exception ex){
                                                        ex.getMessage();
                                                    }
                                                    item = new OPItems();
                                                    item.setDstrct        ( distrito );
                                                    item.setDescripcion   ( mo.getConcept_code() );
                                                    item.setVlr           ( mo.getVlr()* -1 );   // Negativo para la consulta
                                                    item.setIndicador     ( "V" );
                                                    item.setAsignador     ( "S" );
                                                    item.setMoneda        ( moneda_local );
                                                    item.setAgencia       ( agencia );
                                                    item.setCodigo_cuenta( model.descuentoService.obtenerCodigoCuenta(distrito, planilla.getNitpro(), mo.getConcept_code(), planilla.getNumpla() ) );
                                                    item.setCodigo_abc    ( abc );
                                                    item.setCheque        ( "" );//ivan Gomez
                                                    item.setConcepto      ( mo.getConcept_code() );
                                                    item.setDescconcepto  ( model.concepto_equipoService.getConcepto_equipo().getDescripcion()  );
                                                    item.setTipo_cuenta   ( "Q" );
                                                    item.setBase          ( base );
                                                    listaOP.add( item );
                                                }
                                                
                                                if( !model.descuentoService.existDescuentoMantenimento( distrito, planilla.getNumpla(),  d.getCodigo_concepto() ) &&
                                                !model.descuentoService.existDescuentoMovPlanilla( planilla.getNumpla(),  d.getCodigo_concepto(), planilla.getNitpro() ) && !consulta.equals("CONSULTA") ){
                                                    
                                                    consultas.add( model.descuentoService.insertDescuento(des) );
                                                    //consultas.add( model.movplaService.insertMovPla( mo, base ) );
                                                }
                                            }
                                            
                                        }
                                    }
                                }
                            }
                            
                        }
                        else{
                            //Se verifica si en la tabla flota existe la placa del cabezote.
                            Flota f = model.FlotaSvc.existPlaca( planilla.getPlaveh(), distrito, String.valueOf( planilla.getFechapla() ) );
                            if( f != null ) {
                                //Se verifica si en el acuerdo especia existe el standar job y el tipo es CABEZOTE
                                model.acuerdo_especialService.existStandarTipo( f.getPlaca(), "CABEZOTE",distrito,"P");
                                Vector ac =  model.acuerdo_especialService.getAcuerdo_especiales();
                                if( ac != null && ac.size() > 0 ){
                                    for(int i = 0; i < ac.size(); i++){
                                        Acuerdo_especial a = (Acuerdo_especial) ac.get(i);
                                        total = (int)Math.round( ( valor * ( (float) a.getPorcentaje_descuento() / 100 ) )) ;
                                        if( total > 0){
                                            
                                            // Ingreso del descuento
                                            des = new Descuento();
                                            des.setNum_planilla(planilla.getNumpla());
                                            des.setPlaca_cabezote(planilla.getPlaveh());
                                            des.setPlaca_trailer(planilla.getPlatlr());
                                            des.setStd_job_no( planilla.getSj() );
                                            des.setValor( total );
                                            des.setFecha_proceso( mes );
                                            des.setClase_equipo( "CABEZOTE" );
                                            des.setTipo_acuerdo( a.getTipo_acuerdo() );
                                            des.setCodigo_concepto( a.getCodigo_concepto() );
                                            des.setProveedor( planilla.getNitpro() );
                                            des.setNum_equipo( equipo.getDato() );
                                            des.setUsuario_creacion( usuario );
                                            des.setUsuario_modificacion( usuario );
                                            des.setBase(base);
                                            des.setDistrito(distrito);
                                            
                                            //Ingreso MovPla
                                            mo = new Movpla();
                                            mo.setDstrct( distrito );
                                            mo.setAgency_id( agencia );
                                            mo.setDocument_type( "001" );
                                            mo.setConcept_code( a.getCodigo_concepto() );
                                            mo.setPla_owner( planilla.getNitpro() );
                                            mo.setPlanilla( numpla );
                                            mo.setSupplier( planilla.getPlaveh() );
                                            mo.setAp_ind("S");
                                            mo.setVlr( total );
                                            mo.setCurrency( moneda_local );
                                            mo.setProveedor("890103161");
                                            mo.setCreation_user( usuario );
                                            mo.setInd_vlr("V");
                                            mo.setVlr_for( total );
                                            mo.setCreation_date( "now()" );
                                            mo.setSucursal( "" );
                                            mo.setBanco( "" );
                                            mo.setCuenta( "" );
                                            mo.setCantidad( 0 );
                                            mo.setReanticipo( "N" );
                                            mo.setFecha_migracion( "now()" );
                                            
                                            if( consulta.equals("CONSULTA") ){
                                                //adiciono a la lista de el objeto de OP
                                                OpDAO dOP  =  new OpDAO();
                                                String abc = "";
                                                try{
                                                    abc = dOP.getABC( agencia );
                                                    model.concepto_equipoService.searchConcepto_equipo( mo.getConcept_code() );
                                                }catch(Exception ex){
                                                    ex.getMessage();
                                                }
                                                item = new OPItems();
                                                item.setDstrct        ( distrito );
                                                item.setDescripcion   ( mo.getConcept_code() );
                                                item.setVlr           ( mo.getVlr()* -1 );   // Negativo para la consulta
                                                item.setIndicador     ( "V" );
                                                item.setAsignador     ( "S" );
                                                item.setMoneda        ( moneda_local );
                                                item.setAgencia       ( agencia );
                                                item.setCodigo_cuenta( model.descuentoService.obtenerCodigoCuenta(distrito, planilla.getNitpro(), mo.getConcept_code(), planilla.getNumpla() ) );
                                                item.setCodigo_abc    ( abc );
                                                item.setCheque        ( "" );//ivan Gomez
                                                item.setConcepto      ( mo.getConcept_code() );
                                                item.setDescconcepto  ( model.concepto_equipoService.getConcepto_equipo().getDescripcion()  );
                                                item.setTipo_cuenta   ( "Q" );
                                                item.setBase          ( base );
                                                listaOP.add( item );
                                            }
                                            
                                            if( !model.descuentoService.existDescuentoMantenimento( distrito, planilla.getNumpla(),  a.getCodigo_concepto() ) &&
                                            !model.descuentoService.existDescuentoMovPlanilla( planilla.getNumpla(),  a.getCodigo_concepto(), planilla.getNitpro() ) && !consulta.equals("CONSULTA") ){
                                                
                                                consultas.add( model.descuentoService.insertDescuento(des) );
                                                //consultas.add( model.movplaService.insertMovPla( mo, base ) );
                                            }
                                        }
                                    }
                                }
                                else{
                                    //Se verifica si en la tabla clase de equipo existe el equipo y el tipo sea VIAJE
                                    model.descuento_claseService.existeClase( equipo.getReferencia() ,"VIAJE", distrito, planilla.getPlaveh(), planilla.getClientes());
                                    Vector descV = model.descuento_claseService.getDescuento_clases();
                                    if( descV != null && descV.size() > 0 ){
                                        for(int i = 0; i < descV.size(); i++){
                                            Descuento_clase d = (Descuento_clase) descV.get(i);
                                            total = (int)Math.round( ( valor * ( (float) d.getPorcentaje_descuento() / 100 ) )) ;
                                            if( total > 0){
                                                // Ingreso del descuento
                                                des = new Descuento();
                                                des.setNum_planilla(planilla.getNumpla());
                                                des.setPlaca_cabezote(planilla.getPlaveh());
                                                des.setPlaca_trailer(planilla.getPlatlr());
                                                des.setStd_job_no( planilla.getSj() );
                                                des.setValor( total );
                                                des.setFecha_proceso( mes );
                                                des.setClase_equipo( equipo.getReferencia() );
                                                des.setTipo_acuerdo( "CABEZOTE" );
                                                des.setCodigo_concepto( d.getCodigo_concepto() );
                                                des.setProveedor( planilla.getNitpro() );
                                                des.setNum_equipo( equipo.getDato() );
                                                des.setUsuario_creacion( usuario );
                                                des.setUsuario_modificacion( usuario );
                                                des.setBase(base);
                                                des.setDistrito(distrito);
                                                
                                                //Ingreso MovPla
                                                mo = new Movpla();
                                                mo.setDstrct( distrito );
                                                mo.setAgency_id( agencia );
                                                mo.setDocument_type( "001" );
                                                mo.setConcept_code( d.getCodigo_concepto() );
                                                mo.setPla_owner( planilla.getNitpro() );
                                                mo.setPlanilla( numpla );
                                                mo.setSupplier( planilla.getPlaveh() );
                                                mo.setAp_ind("S");
                                                mo.setVlr( total );
                                                mo.setCurrency( moneda_local );
                                                mo.setProveedor("890103161");
                                                mo.setCreation_user( usuario );
                                                mo.setInd_vlr("V");
                                                mo.setVlr_for( total );
                                                mo.setCreation_date( "now()" );
                                                mo.setSucursal( "" );
                                                mo.setBanco( "" );
                                                mo.setCuenta( "" );
                                                mo.setCantidad( 0 );
                                                mo.setReanticipo( "N" );
                                                mo.setFecha_migracion( "now()" );
                                                
                                                if( consulta.equals("CONSULTA") ){
                                                    //adiciono a la lista de el objeto de OP
                                                    OpDAO dOP  =  new OpDAO();
                                                    String abc = "";
                                                    try{
                                                        abc = dOP.getABC( agencia );
                                                        model.concepto_equipoService.searchConcepto_equipo( mo.getConcept_code() );
                                                    }catch(Exception ex){
                                                        ex.getMessage();
                                                    }
                                                    item = new OPItems();
                                                    item.setDstrct        ( distrito );
                                                    item.setDescripcion   ( mo.getConcept_code() );
                                                    item.setVlr           ( mo.getVlr()* -1 );   // Negativo para la consulta
                                                    item.setIndicador     ( "V" );
                                                    item.setAsignador     ( "S" );
                                                    item.setMoneda        ( moneda_local );
                                                    item.setAgencia       ( agencia );
                                                    item.setCodigo_cuenta( model.descuentoService.obtenerCodigoCuenta(distrito, planilla.getNitpro(), mo.getConcept_code(), planilla.getNumpla() ) );
                                                    item.setCodigo_abc    ( abc );
                                                    item.setCheque        ( "" );//ivan Gomez
                                                    item.setConcepto      ( mo.getConcept_code() );
                                                    item.setDescconcepto  ( model.concepto_equipoService.getConcepto_equipo().getDescripcion()  );
                                                    item.setTipo_cuenta   ( "Q" );
                                                    item.setBase          ( base );
                                                    listaOP.add( item );
                                                }
                                                
                                                if( !model.descuentoService.existDescuentoMantenimento( distrito, planilla.getNumpla(),  d.getCodigo_concepto() ) &&
                                                !model.descuentoService.existDescuentoMovPlanilla( planilla.getNumpla(),  d.getCodigo_concepto(), planilla.getNitpro() ) && !consulta.equals("CONSULTA") ){
                                                    
                                                    consultas.add( model.descuentoService.insertDescuento(des) );
                                                    //consultas.add( model.movplaService.insertMovPla( mo, base ) );
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        //Se verifica si en la tabla clase de equipo existe el equipo y el tipo sea MES
                                        model.descuento_claseService.existeClase( equipo.getReferencia() ,"MES", distrito, planilla.getPlaveh(), planilla.getClientes());
                                        Vector descM = model.descuento_claseService.getDescuento_clases();
                                        if( descM != null && descM.size() >0 ){
                                            for(int i = 0; i < descM.size(); i++){
                                                Descuento_clase d = (Descuento_clase) descM.get(i);
                                                total = (int)Math.round( ( valor * ( (float) d.getPorcentaje_descuento() / 100 ) ) );
                                                if( total > 0){
                                                    
                                                    // Ingreso del descuento
                                                    des = new Descuento();
                                                    des.setNum_planilla(planilla.getNumpla());
                                                    des.setPlaca_cabezote(planilla.getPlaveh());
                                                    des.setPlaca_trailer(planilla.getPlatlr());
                                                    des.setStd_job_no( planilla.getSj() );
                                                    des.setValor( total );
                                                    des.setFecha_proceso( mes );
                                                    des.setClase_equipo( equipo.getReferencia() );
                                                    des.setTipo_acuerdo( "CABEZOTE" );
                                                    des.setCodigo_concepto( d.getCodigo_concepto() );
                                                    des.setProveedor( planilla.getNitpro() );
                                                    des.setNum_equipo( equipo.getDato() );
                                                    des.setUsuario_creacion( usuario );
                                                    des.setUsuario_modificacion( usuario );
                                                    des.setBase(base);
                                                    des.setDistrito(distrito);
                                                    
                                                    //Ingreso MovPla
                                                    mo = new Movpla();
                                                    mo.setDstrct( distrito );
                                                    mo.setAgency_id( agencia );
                                                    mo.setDocument_type( "001" );
                                                    mo.setConcept_code( d.getCodigo_concepto() );
                                                    mo.setPla_owner( planilla.getNitpro() );
                                                    mo.setPlanilla( numpla );
                                                    mo.setSupplier( planilla.getPlaveh() );
                                                    mo.setAp_ind("S");
                                                    mo.setVlr( total );
                                                    mo.setCurrency( moneda_local );
                                                    mo.setProveedor("890103161");
                                                    mo.setCreation_user( usuario );
                                                    mo.setInd_vlr("V");
                                                    mo.setVlr_for( total );
                                                    mo.setCreation_date( "now()" );
                                                    mo.setSucursal( "" );
                                                    mo.setBanco( "" );
                                                    mo.setCuenta( "" );
                                                    mo.setCantidad( 0 );
                                                    mo.setReanticipo( "N" );
                                                    mo.setFecha_migracion( "now()" );
                                                    
                                                    if( consulta.equals("CONSULTA") ){
                                                        //adiciono a la lista de el objeto de OP
                                                        OpDAO dOP  =  new OpDAO();
                                                        String abc = "";
                                                        try{
                                                            abc = dOP.getABC( agencia );
                                                            model.concepto_equipoService.searchConcepto_equipo( mo.getConcept_code() );
                                                        }catch(Exception ex){
                                                            ex.getMessage();
                                                        }
                                                        item = new OPItems();
                                                        item.setDstrct        ( distrito );
                                                        item.setDescripcion   ( mo.getConcept_code() );
                                                        item.setVlr           ( mo.getVlr()* -1 );   // Negativo para la consulta
                                                        item.setIndicador     ( "V" );
                                                        item.setAsignador     ( "S" );
                                                        item.setMoneda        ( moneda_local );
                                                        item.setAgencia       ( agencia );
                                                        item.setCodigo_cuenta( model.descuentoService.obtenerCodigoCuenta(distrito, planilla.getNitpro(), mo.getConcept_code(), planilla.getNumpla() ) );
                                                        item.setCodigo_abc    ( abc );
                                                        item.setCheque        ( "" );//ivan Gomez
                                                        item.setConcepto      ( mo.getConcept_code() );
                                                        item.setDescconcepto  ( model.concepto_equipoService.getConcepto_equipo().getDescripcion()  );
                                                        item.setTipo_cuenta   ( "Q" );
                                                        item.setBase          ( base );
                                                        listaOP.add( item );
                                                    }
                                                    
                                                    if( !model.descuentoService.existDescuentoMantenimento( distrito, planilla.getNumpla(),  d.getCodigo_concepto() ) &&
                                                    !model.descuentoService.existDescuentoMovPlanilla( planilla.getNumpla(),  d.getCodigo_concepto(), planilla.getNitpro() ) && !consulta.equals("CONSULTA") ){
                                                        
                                                        consultas.add( model.descuentoService.insertDescuento(des) );
                                                        //consultas.add( model.movplaService.insertMovPla( mo, base ) );
                                                    }
                                                }
                                                
                                            }
                                        }
                                    }
                                }
                            }
                            else{
                                //Se verifica si en el acuerdo especia existe el standar job y el tipo es CLIENTE
                                model.acuerdo_especialService.existStandarTipo( planilla.getSj(), "CLIENTE", distrito , "S");
                                Vector ac =  model.acuerdo_especialService.getAcuerdo_especiales();
                                if( ac != null && ac.size() > 0 ){
                                    for(int i = 0; i < ac.size(); i++){
                                        Acuerdo_especial a = (Acuerdo_especial) ac.get(i);
                                        total = (int)Math.round( ( valor * ( (float) a.getPorcentaje_descuento() / 100 ) )) ;
                                        if( total > 0){
                                            
                                            // Ingreso del descuento
                                            des = new Descuento();
                                            des.setNum_planilla(planilla.getNumpla());
                                            des.setPlaca_cabezote(planilla.getPlaveh());
                                            des.setPlaca_trailer(planilla.getPlatlr());
                                            des.setStd_job_no( planilla.getSj() );
                                            des.setValor( total );
                                            des.setFecha_proceso( mes );
                                            des.setClase_equipo( "CLIENTE" );
                                            des.setTipo_acuerdo( a.getTipo_acuerdo() );
                                            des.setCodigo_concepto( a.getCodigo_concepto() );
                                            des.setProveedor( planilla.getNitpro() );
                                            des.setNum_equipo( equipo.getDato() );
                                            des.setUsuario_creacion( usuario );
                                            des.setUsuario_modificacion( usuario );
                                            des.setBase(base);
                                            des.setDistrito(distrito);
                                            
                                            //Ingreso MovPla
                                            mo = new Movpla();
                                            mo.setDstrct( distrito );
                                            mo.setAgency_id( agencia );
                                            mo.setDocument_type( "001" );
                                            mo.setConcept_code( a.getCodigo_concepto() );
                                            mo.setPla_owner( planilla.getNitpro() );
                                            mo.setPlanilla( numpla );
                                            mo.setSupplier( planilla.getPlaveh() );
                                            mo.setAp_ind("S");
                                            mo.setVlr( total );
                                            mo.setCurrency( moneda_local );
                                            mo.setProveedor("890103161");
                                            mo.setCreation_user( usuario );
                                            mo.setInd_vlr("V");
                                            mo.setVlr_for( total );
                                            mo.setCreation_date( "now()" );
                                            mo.setSucursal( "" );
                                            mo.setBanco( "" );
                                            mo.setCuenta( "" );
                                            mo.setCantidad( 0 );
                                            mo.setReanticipo( "N" );
                                            mo.setFecha_migracion( "now()" );
                                            
                                            if( consulta.equals("CONSULTA") ){
                                                //adiciono a la lista de el objeto de OP
                                                OpDAO dOP  =  new OpDAO();
                                                String abc = "";
                                                try{
                                                    abc = dOP.getABC( agencia );
                                                    model.concepto_equipoService.searchConcepto_equipo( mo.getConcept_code() );
                                                }catch(Exception ex){
                                                    ex.getMessage();
                                                }
                                                item = new OPItems();
                                                item.setDstrct        ( distrito );
                                                item.setDescripcion   ( mo.getConcept_code() );
                                                item.setVlr           ( mo.getVlr()* -1 );   // Negativo para la consulta
                                                item.setIndicador     ( "V" );
                                                item.setAsignador     ( "S" );
                                                item.setMoneda        ( moneda_local );
                                                item.setAgencia       ( agencia );
                                                item.setCodigo_cuenta( model.descuentoService.obtenerCodigoCuenta(distrito, planilla.getNitpro(), mo.getConcept_code(), planilla.getNumpla() ) );
                                                item.setCodigo_abc    ( abc );
                                                item.setCheque        ( "" );//ivan Gomez
                                                item.setConcepto      ( mo.getConcept_code() );
                                                item.setDescconcepto  ( model.concepto_equipoService.getConcepto_equipo().getDescripcion()  );
                                                item.setTipo_cuenta   ( "Q" );
                                                item.setBase          ( base );
                                                listaOP.add( item );
                                            }
                                            
                                            if( !model.descuentoService.existDescuentoMantenimento( distrito, planilla.getNumpla(),  a.getCodigo_concepto() ) &&
                                            !model.descuentoService.existDescuentoMovPlanilla( planilla.getNumpla(),  a.getCodigo_concepto(), planilla.getNitpro() ) && !consulta.equals("CONSULTA") ){
                                                
                                                consultas.add( model.descuentoService.insertDescuento(des) );
                                                //consultas.add( model.movplaService.insertMovPla( mo, base ) );
                                            }
                                        }
                                    }
                                }
                                else{
                                    //Se verifica si en la tabla clase de equipo existe el equipo y el tipo sea VIAJE
                                    model.descuento_claseService.existeClase( equipo.getReferencia() ,"VIAJE", distrito, "", planilla.getClientes());
                                    Vector descV = model.descuento_claseService.getDescuento_clases();
                                    if( descV != null && descV.size() > 0 ){
                                        for(int i = 0; i < descV.size(); i++){
                                            Descuento_clase d = (Descuento_clase) descV.get(i);
                                            total = (int)Math.round( ( valor * ( (float) d.getPorcentaje_descuento() / 100 ) ) );
                                            if( total > 0){
                                                
                                                // Ingreso del descuento
                                                des = new Descuento();
                                                des.setNum_planilla(planilla.getNumpla());
                                                des.setPlaca_cabezote(planilla.getPlaveh());
                                                des.setPlaca_trailer(planilla.getPlatlr());
                                                des.setStd_job_no( planilla.getSj() );
                                                des.setValor( total );
                                                des.setFecha_proceso( mes );
                                                des.setClase_equipo( equipo.getReferencia() );
                                                des.setTipo_acuerdo( "CABEZOTE" );
                                                des.setCodigo_concepto( d.getCodigo_concepto() );
                                                des.setProveedor( planilla.getNitpro() );
                                                des.setNum_equipo( equipo.getDato() );
                                                des.setUsuario_creacion( usuario );
                                                des.setUsuario_modificacion( usuario );
                                                des.setBase(base);
                                                des.setDistrito(distrito);
                                                
                                                //Ingreso MovPla
                                                mo = new Movpla();
                                                mo.setDstrct( distrito );
                                                mo.setAgency_id( agencia );
                                                mo.setDocument_type( "001" );
                                                mo.setConcept_code( d.getCodigo_concepto() );
                                                mo.setPla_owner( planilla.getNitpro() );
                                                mo.setPlanilla( numpla );
                                                mo.setSupplier( planilla.getPlaveh() );
                                                mo.setAp_ind("S");
                                                mo.setVlr( total );
                                                mo.setCurrency( moneda_local );
                                                mo.setProveedor("890103161");
                                                mo.setCreation_user( usuario );
                                                mo.setInd_vlr("V");
                                                mo.setVlr_for( total );
                                                mo.setCreation_date( "now()" );
                                                mo.setSucursal( "" );
                                                mo.setBanco( "" );
                                                mo.setCuenta( "" );
                                                mo.setCantidad( 0 );
                                                mo.setReanticipo( "N" );
                                                mo.setFecha_migracion( "now()" );
                                                
                                                if( consulta.equals("CONSULTA") ){
                                                    //adiciono a la lista de el objeto de OP
                                                    OpDAO dOP  =  new OpDAO();
                                                    String abc = "";
                                                    try{
                                                        abc = dOP.getABC( agencia );
                                                        model.concepto_equipoService.searchConcepto_equipo( mo.getConcept_code() );
                                                    }catch(Exception ex){
                                                        ex.getMessage();
                                                    }
                                                    item = new OPItems();
                                                    item.setDstrct        ( distrito );
                                                    item.setDescripcion   ( mo.getConcept_code() );
                                                    item.setVlr           ( mo.getVlr()* -1 );   // Negativo para la consulta
                                                    item.setIndicador     ( "V" );
                                                    item.setAsignador     ( "S" );
                                                    item.setMoneda        ( moneda_local );
                                                    item.setAgencia       ( agencia );
                                                    item.setCodigo_cuenta( model.descuentoService.obtenerCodigoCuenta(distrito, planilla.getNitpro(), mo.getConcept_code(), planilla.getNumpla() ) );
                                                    item.setCodigo_abc    ( abc );
                                                    item.setCheque        ( "" );//ivan Gomez
                                                    item.setConcepto      ( mo.getConcept_code() );
                                                    item.setDescconcepto  ( model.concepto_equipoService.getConcepto_equipo().getDescripcion()  );
                                                    item.setTipo_cuenta   ( "Q" );
                                                    item.setBase          ( base );
                                                    listaOP.add( item );
                                                }
                                                if( !model.descuentoService.existDescuentoMantenimento( distrito, planilla.getNumpla(),  d.getCodigo_concepto() ) &&
                                                !model.descuentoService.existDescuentoMovPlanilla( planilla.getNumpla(),  d.getCodigo_concepto(), planilla.getNitpro() ) && !consulta.equals("CONSULTA") ){
                                                    consultas.add( model.descuentoService.insertDescuento(des) );
                                                    //consultas.add( model.movplaService.insertMovPla( mo, base ) );
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        //Se verifica si en la tabla clase de equipo existe el equipo y el tipo sea MES
                                        model.descuento_claseService.existeClase( equipo.getReferencia() ,"MES", distrito, planilla.getPlaveh(), planilla.getClientes());
                                        Vector descM = model.descuento_claseService.getDescuento_clases();
                                        if( descM != null && descM.size() > 0 ){
                                            for(int i = 0; i < descM.size(); i++){
                                                Descuento_clase d = (Descuento_clase) descM.get(i);
                                                total = (int)Math.round( ( valor * ( (float) d.getPorcentaje_descuento() / 100 ) )) ;
                                                if( total > 0){
                                                    
                                                    // Ingreso del descuento
                                                    des = new Descuento();
                                                    des.setNum_planilla(planilla.getNumpla());
                                                    des.setPlaca_cabezote(planilla.getPlaveh());
                                                    des.setPlaca_trailer(planilla.getPlatlr());
                                                    des.setStd_job_no( planilla.getSj() );
                                                    des.setValor( total );
                                                    des.setFecha_proceso( mes );
                                                    des.setClase_equipo( equipo.getReferencia() );
                                                    des.setTipo_acuerdo( "CABEZOTE" );
                                                    des.setCodigo_concepto( d.getCodigo_concepto() );
                                                    des.setProveedor( planilla.getNitpro() );
                                                    des.setNum_equipo( equipo.getDato() );
                                                    des.setUsuario_creacion( usuario );
                                                    des.setUsuario_modificacion( usuario );
                                                    des.setBase(base);
                                                    des.setDistrito(distrito);
                                                    
                                                    //Ingreso MovPla
                                                    mo = new Movpla();
                                                    mo.setDstrct( distrito );
                                                    mo.setAgency_id( agencia );
                                                    mo.setDocument_type( "001" );
                                                    mo.setConcept_code( d.getCodigo_concepto() );
                                                    mo.setPla_owner( planilla.getNitpro() );
                                                    mo.setPlanilla( numpla );
                                                    mo.setSupplier( planilla.getPlaveh() );
                                                    mo.setAp_ind("S");
                                                    mo.setVlr( total );
                                                    mo.setCurrency( moneda_local );
                                                    mo.setProveedor("890103161");
                                                    mo.setCreation_user( usuario );
                                                    mo.setInd_vlr("V");
                                                    mo.setVlr_for( total );
                                                    mo.setCreation_date( "now()" );
                                                    mo.setSucursal( "" );
                                                    mo.setBanco( "" );
                                                    mo.setCuenta( "" );
                                                    mo.setCantidad( 0 );
                                                    mo.setReanticipo( "N" );
                                                    mo.setFecha_migracion( "now()" );
                                                    
                                                    if( consulta.equals("CONSULTA") ){
                                                        //adiciono a la lista de el objeto de OP
                                                        OpDAO dOP  =  new OpDAO();
                                                        String abc = "";
                                                        try{
                                                            abc = dOP.getABC( agencia );
                                                            model.concepto_equipoService.searchConcepto_equipo( mo.getConcept_code() );
                                                        }catch(Exception ex){
                                                            ex.getMessage();
                                                        }
                                                        item = new OPItems();
                                                        item.setDstrct        ( distrito );
                                                        item.setDescripcion   ( mo.getConcept_code() );
                                                        item.setVlr           ( mo.getVlr() * -1 );   // Negativo para la consulta
                                                        item.setIndicador     ( "V" );
                                                        item.setAsignador     ( "S" );
                                                        item.setMoneda        ( moneda_local );
                                                        item.setAgencia       ( agencia );
                                                        item.setCodigo_cuenta ( model.descuentoService.obtenerCodigoCuenta(distrito, planilla.getNitpro(), mo.getConcept_code(), planilla.getNumpla() ) );
                                                        item.setCodigo_abc    ( abc );
                                                        item.setCheque        ( "" );//ivan Gomez
                                                        item.setConcepto      ( mo.getConcept_code() );
                                                        item.setDescconcepto  ( model.concepto_equipoService.getConcepto_equipo().getDescripcion()  );
                                                        item.setTipo_cuenta   ( "Q" );
                                                        item.setBase          ( base );
                                                        listaOP.add( item );
                                                    }
                                                    
                                                    if( !model.descuentoService.existDescuentoMantenimento( distrito, planilla.getNumpla(),  d.getCodigo_concepto() ) &&
                                                    !model.descuentoService.existDescuentoMovPlanilla( planilla.getNumpla(),  d.getCodigo_concepto(), planilla.getNitpro() ) && !consulta.equals("CONSULTA") ){
                                                        
                                                        consultas.add( model.descuentoService.insertDescuento(des) );
                                                        //consultas.add( model.movplaService.insertMovPla( mo, base ) );
                                                    }
                                                }
                                                
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        // valido si existen campos en el movpla
                        if( !consulta.equals("CONSULTA") ){
                            model.despachoService.insertar(consultas);
                            listaOP = null;
                            consultas.clear();
                        }
                    }
                    
                }
                
            }
            return listaOP;
        }
        catch(SQLException e){
            e.printStackTrace();
            throw new SQLException(e.getMessage());
        }
    }
    
    
}
