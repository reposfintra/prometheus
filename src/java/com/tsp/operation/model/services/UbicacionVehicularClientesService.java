/***************************************************************************
 * Nombre clase: UbicacionVehicularClientesService.java                    *           *
 * Descripci�n: Clase que maneja los servicios para el reporte Ubicacion   *
 *              Vahicular Clientes                                         *
 * Autor: Ing. Ivan DArio Gomez Vanegas                                    *
 * Fecha: Created on 1 de octubre de 2005, 08:20 AM                        *
 * Versi�n: Java 1.0                                                       *
 * Copyright: Fintravalores S.A. S.A.                                 *
 ***************************************************************************/
 

package com.tsp.operation.model.services;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.UbicacionVehicularClientesDAO;



public class UbicacionVehicularClientesService {
    private UbicacionVehicularClientesDAO UbicacionDataAccess;
    
    /** Creates a new instance of UbicacionVehicularClientesService */
    public UbicacionVehicularClientesService() {
        UbicacionDataAccess = new UbicacionVehicularClientesDAO();
    }
    
       /**
   * Este m�todo genera los registros que hacen parte del reporte de ubicaci�n
   * vehicular y los guarda en una lista.
   * @param ubicVehArgs Criterios de b�squeda. Son los siguientes:
   *        ubicVehArgs[0] = C�digo del cliente.
   *        ubicVehArgs[1] = Fecha Inicial
   *        ubicVehArgs[2] = Fecha Final
   *        ubicVehArgs[3] = Tipos de Viaje (NA, RM, RC, RE, DM, DC)
   * @param user Objeto con los datos de un usuario destinatario
   *             Para cualquier otro usuario, este atributo no tiene efecto alguno.
   * @see     : UbicacionVehicularSearch - UbicacionVehicularClientesDAO
   * @throws SQLException si un error de acceso a la base de datos ocurre.
   */
     public void UbicacionVehicularSearch(String [] ubicVehArgs, Usuario user)throws SQLException{
        try{
            UbicacionDataAccess.UbicacionVehicularSearch(ubicVehArgs, user);
        }
        catch(SQLException e){
            throw new SQLException("Error en UbicacionVehicularSearch [UbicacionVehicularClientesService]...\n"+e.getMessage());
        }
        
    }
     
     /**
     * Devuelve un array de tipo String [] donde se encuentran almacenados los nombres
     * de los campos del reporte de informaci�n al cliente.
     * @see     : obtenerCamposDeReporte - UbicacionVehicularClientesDAO
     * @return El array con los nombres de los campos del reporte de informaci�n al cliente.
     */
     public String [] obtenerCamposDeReporte(){
        return UbicacionDataAccess.obtenerCamposDeReporte();
    }
     
     
     /**
     * retorna los datos del reporte ubicacion Vehicular Clientes
     * @see     : obtenerDatosReporte - UbicacionVehicularClientesDAO
     * @return El array con los nombres de los campos del reporte de informaci�n al cliente.
     */
    public LinkedList obtenerDatosReporte(){
        return UbicacionDataAccess.obtenerDatosReporte();
    }

    /**
     * Este m�todo permite obtener los titulos del encabezado del reporte de
     * informaci�n al cliente. El Hashtable que es devuelto contiene los titulos de todos
     * los campos del reporte y la clave de cada titulo es su respectivo nombre
     * de campo en la Base de datos, para obtener los nombre de los campos del
     * reporte utilize el metodo <CODE>String [] obtenerCamposDeReporte()</CODE>.
     * @throws SQLException Si algun problema ocurre en el acceso a la Base de Datos.
     * @return Una tabla con los titulos de los campos del reporte de informaci�n al cliente.
     * @see     : obtenerTitulosDeReporte - UbicacionVehicularClientesDAO
     */
      public Hashtable obtenerTitulosDeReporte() throws SQLException{
        return UbicacionDataAccess.obtenerTitulosDeReporte();
      }


     /**
   * Retorna las devoluciones asociadas a una planilla en el reporte
   * de ubicaci�n vehicular.
   * @param numPlanilla N�mero de la planilla.
   * @see     : UbicacionVehicularReturn - UbicacionVehicularClientesDAO
   * @throws SQLException Si un error de acceso a la base de datos ocurre.
   */
    public void UbicacionVehicularReturn(String numPlanilla) throws SQLException {
        try{
            UbicacionDataAccess.UbicacionVehicularReturn(numPlanilla);
        }
        catch(SQLException e){
            throw new SQLException("Error en UbicacionVehicularReturn [UbicacionVehicularClientesService]...\n"+e.getMessage());
        } 
     }

   /**
   * Getter for property devoluciones.
   * @return Value of property devoluciones.
   */
   public LinkedList getDevoluciones() {
    return UbicacionDataAccess.getDevoluciones();
   }
  
}
