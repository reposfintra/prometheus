/************************************************************************************
 * Nombre clase : ............... MigracionProveedorEscoltaService.java             *
 * Descripcion :................. Clase que maneja los Servicios                    *
 *                                asignados al Model relacionados con el            *
 *                                programa de Migracion de Proveedores de Escoltas  * 
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                      *
 * Fecha :....................... 29 de Noviembre de 2005, 10:02 PM                 *
 * Version :..................... 1.0                                               *
 * Copyright :................... Fintravalores S.A.                           *
 ************************************************************************************/
package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;

public class MigracionProveedorEscoltaService {
    
    public MigracionProveedorEscoltaDAO dao = new MigracionProveedorEscoltaDAO();
    
    public MigracionProveedorEscoltaService() {
    }   
      /**
     * Metodo searchServiciosEscoltas, busca los servicios prestados por  vehiculos 
     * escoltas  entre dos fechas
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : String fecha inicial, String fecha final
     * @version : 1.0
     */
    public Vector searchServiciosEscoltasAVehiculos(String feci, String fecf) throws SQLException {        
        return dao.searchServiciosEscoltasAVehiculos(feci, fecf);
    }   
      /**
     * Metodo searchServiciosEscoltasACaravana, busca los servicios prestados por  vehiculos 
     * escoltas  a una caravana entre dos fechas
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : String fecha inicial, String fecha final
     * @version : 1.0
     */
    public Vector searchServiciosEscoltasACaravana(String feci, String fecf) throws SQLException { 
        return dao.searchServiciosEscoltasACaravana(feci,fecf);
    }    
}
