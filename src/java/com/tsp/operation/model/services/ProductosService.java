/*
 * ProductosService.java
 *
 * Created on 27 de julio de 2009, 8:39
 */

package com.tsp.operation.model.services;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
/**
 *
 * @author  Rhonalf
 */
public class ProductosService {
    private ProductosDAO prdao;
    private ArrayList resultado;
    /** Creates a new instance of ProductosService */
    public ProductosService() {
        prdao = new ProductosDAO();
    }
    
    public ArrayList listarTodos() throws Exception{
        ArrayList rx = new ArrayList();
        try{
            rx = prdao.listarTodos();
        }
        catch(Exception e){
            System.out.println("Error en listar todos: "+e.toString());
            e.printStackTrace();
        }
        return rx;
    }
    
    public ArrayList listarProovedoresPara(int cod_prod) throws Exception{
        ArrayList rx2 = new ArrayList();
        try{
            rx2 = prdao.listarProovedoresPara(cod_prod);
        }
        catch(Exception e){
            System.out.println("Error en listar para: "+e.toString());
        }
        return rx2;
    }
    
    public ArrayList verTodos() throws Exception{
        ArrayList rx3 = new ArrayList();
        try{
            rx3 = prdao.verTodos();
        }
        catch(Exception e){
            System.out.println("Error en ver todos: "+e.toString());
            e.printStackTrace();
        }
        return rx3;
    }
    
    public void insertarProducto(String descripcion,double precio) throws Exception{
        try{
            prdao.insertarProducto(descripcion,precio);
        }
        catch(Exception ex){
            System.out.println("Error en insertar producto: "+ex.toString()+" "+ex.getMessage());
        }
    }
    
    public String contarProductos(){
        String rsp="";
        try{
            rsp = prdao.contarProductos();
        }
        catch(Exception e){
            System.out.println("Error en contar: "+e.toString());
        }
        return rsp;
    }
    
    public void anularProducto(String codigo) throws Exception{
         try{
            prdao.anularProducto(codigo);
        }
        catch(Exception ex){
            System.out.println("Error en anular producto: "+ex.toString());
        }
    }
    
    private void setResult(ArrayList r){
        this.resultado = r;
    }
    
    public ArrayList getResultado(){
        return this.resultado;
    }
    
    public void buscarPor(int filtro,String texto) throws Exception{
        ArrayList rx4 = new ArrayList();
        try{
            rx4 = prdao.buscarPor(filtro,texto);
            setResult(rx4);
        }
        catch(Exception e){
            System.out.println("Error en buscar por: "+e.toString());
            e.printStackTrace();
        }
    }
    
}
