/*Created on 28 de junio de 2005, 02:57 PM
 */

package com.tsp.operation.model.services;

/**@author  fvillacob*/



import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.util.*;
import java.sql.SQLException;



public class TiempoViajeService {
    
    private TiempoViajeDAO  dao;
    private Vector            ots;
    public  Paginacion      page;
    
    private List            ciudades;
    private String          type;
    private String          days;
    private String          to;

    
    
    public TiempoViajeService() {
        dao = new TiempoViajeDAO();
        ots              = null;
        ciudades         = null;       
       
    }
    
    
    public void paginar(){ 
        
    }
    
    
    public void searchOTs(String tipo, String fechai,String fechaf,String cliente) throws Exception {      
      try{  
          this.ots = this.dao.searchOTs(tipo,fechai,fechaf,cliente);
      }
      catch(Exception e){ throw new Exception ( e.getMessage());}
    }
    
    
    public Vector getOTs(){ return this.ots; }
    
    
    
    
    
    public List getCiuades(){ return this.ciudades; }
    
    
    public String getTipo   (){ return this.type ; }
    public String getDias   (){ return this.days ; }
    public String getLLegada(){ return this.to   ; }
    
     
    public Vector searchClientes(String agencia)throws Exception{
        return dao.searchClientes(agencia);
    }
    
}
