/*
 * ClienteService.java
 *
 * Created on 13 de noviembre de 2004, 9:49
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;
import com.tsp.operation.model.DAOS.*;
/**
 *
 * @author  ffernandez
 */
public class Control_excepcionService {
    
    private Control_ExcepcionDAO ControlDAO;
    private BeanGeneral BeanControl;
    private Cliente clienteUnidad;
    private TreeMap treemap; //TMaturana 19.01.05
    /** Creates a new instance of ClienteService */
    public Control_excepcionService() {
        ControlDAO = new Control_ExcepcionDAO();
        this.BeanControl = new BeanGeneral();
    }
    public Control_excepcionService(String dataBaseName) {
        ControlDAO = new Control_ExcepcionDAO(dataBaseName);
        this.BeanControl = new BeanGeneral();
    }
       
    public boolean existeControl(String codcli,String Origen, String Destino, String agencia) throws Exception {
       return ControlDAO.existeControl(codcli,Origen,Destino,agencia );
    }
  
    public void agregarControlE(BeanGeneral BeanControl) throws java.sql.SQLException{
        this.ControlDAO.agregarControlE(BeanControl);        
    }
     public void bucarControlExcepcion(String codigo, String origen, String  destino, String agencia) throws Exception {
       ControlDAO.bucarControlExcepcion(codigo,origen,destino,agencia);
    }
     public java.util.Vector getVector() {
        return ControlDAO.getVector(); 
    } 
     
      public void modificarCotrolExcepcion (BeanGeneral CE) throws Exception{
        try{
             ControlDAO.modificarCotrolExcepcion(CE);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
        
    } 
       public void buscarControl(String codigo,String origen,String destino, String agencia) throws Exception {
            ControlDAO.buscarControl(codigo,origen,destino,agencia); 
    }
    
     public void eliminarControl (String codigo,String origen,String destino, String agencia) throws Exception{
        try{
            ControlDAO.eliminarControl(codigo,origen,destino,agencia);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }  
}
