/*
* Nombre        ConciliacionService.java
* Descripci�n   Clase que maneja los servicios para la conciliacion de placas
* Autor         Sescalante
* Fecha         5 de abril de 2006, 09:41 AM
* Versi�n       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/

package com.tsp.operation.model.services;
import com.tsp.operation.model.DAOS.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @modificado  Ivan Dario Gomez
 */
public class ConciliacionService {
    private ConciliacionDAO dao;   
    /** Creates a new instance of ConciliacionService */
    public ConciliacionService() {
        dao = new ConciliacionDAO();
    }
    
    
     /**
     * M�todo compararconPlacas, realiza la conciliacion de las placas del archivo contra las 
     *                           que estan en la tabla plainlla
     * @autor       Sescalante
     * @modificado  Ivan Gomez Vanegas
     * @param       la linea leida del archivo csv, y la fecha de corte
     * @see         compararconPlacas - ConciliacionDAO
     * @throws      Exception
     * @version     1.0.
     **/
    public String compararconPlacas(String line, String fch)throws SQLException{
        return dao.compararconPlacas(line, fch);
    }
    
    
    /**
     * M�todo enviarCorreo, realiza el proceso de envio de correo
     * @autor       Sescalante
     * @modificado  Ivan Gomez Vanegas
     * @param       un strin con las placas, la fecha, y la ruta
     * @see         enviarCorreo - ConciliacionDAO
     * @throws      Exception
     * @version     1.0.
     **/

    public void enviarCorreo(String placas, String fecha, String ruta)throws SQLException{
        dao.enviarCorreo(placas, fecha, ruta); 
    }
}
