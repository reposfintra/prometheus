/*********************************************************************************
 * Nombre clase :      ReporteFacturaDestinatarioService.java                    *
 * Descripcion :       Service del ReporteFacturaDestinatarioService.java        *
 * Autor :             LREALES                                                   *
 * Fecha :             03 de abril de 2006, 12:45 PM                             *
 * Version :           1.0                                                       *
 * Copyright :         Fintravalores S.A.                                   *
 *********************************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;

public class ReporteFacturaDestinatarioService {
    
    private ReporteFacturaDestinatarioDAO reporte;
    
    /** Creates a new instance of ReporteFacturaDestinatarioService */
    public ReporteFacturaDestinatarioService () {
        
        reporte = new ReporteFacturaDestinatarioDAO ();
        
    }
    
    /** Funcion publica que obtiene el metodo getVectorPlanilla del DAO */
    public Vector getVectorPlanilla () throws SQLException {
        
        return reporte.getPlanilla();
        
    }    
    
    /** Funcion publica que obtiene el metodo getVectorRemesa del DAO */
    public Vector getVectorRemesa () throws SQLException {
        
        return reporte.getRemesa();
        
    }
    
    /** Funcion publica que obtiene el metodo getIngresoPlanilla del DAO */
    public HojaReportes getHojaReportes () throws SQLException {
       
        return reporte.getHojRep();
        
    }
    
    /** Funcion publica que obtiene el metodo listaPlanilla del DAO */
    public void listaPlanilla ( String numpla ) throws SQLException {
        
        reporte.listaPlanilla ( numpla );
        
    }
    
    /** Funcion publica que obtiene el metodo listaRemesa del DAO */
    public void listaRemesa ( String numrem ) throws SQLException {
        
        reporte.listaRemesa ( numrem );
        
    }
    
    /** Funcion publica que obtiene el metodo existePlanilla del DAO */
    public boolean existePlanilla ( String numpla ) throws SQLException {
        
        reporte.existePlanilla ( numpla );
        return reporte.existePlanilla( numpla );
        
    }
    
    /** Funcion publica que obtiene el metodo existeRemesa del DAO */
    public boolean existeRemesa ( String numrem ) throws SQLException {
        
        reporte.existeRemesa ( numrem );
        return reporte.existeRemesa( numrem );
        
    }
        
}