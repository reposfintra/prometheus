/*
 * AgenciaService.java
 *
 * Created on 13 de noviembre de 2004, 9:49
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;
import com.tsp.operation.model.DAOS.*;
/**
 *
 * @author  AMENDEZ
 */
public class AgenciaService {
    
    private AgenciaDAO agenciaDataAccess;
    private TreeMap listaAgencias = null;
    //tmaturana 27.01.06
    private TreeMap agencia = new TreeMap();
    private List listaC;
    
    /** Creates a new instance of CarroceriaService */
    public AgenciaService() {
        agenciaDataAccess = new AgenciaDAO();
    }
    public AgenciaService(String dataBaseName) {
        agenciaDataAccess = new AgenciaDAO(dataBaseName);
    }
    
    
    
    public TreeMap searchAgencia() throws SQLException{
        agenciaDataAccess.searchAgencia();
        return agenciaDataAccess.getCbxAgencia();
    }
    
    public TreeMap searchZona() throws SQLException{
        agenciaDataAccess.searchZona();
        return agenciaDataAccess.getCbxZona();
    }
    
   //ALEJANDRO PAYARES ,8 de noviembre de 2005
    
    public void searchAgenciaDistrito(String distrito)throws SQLException{
        agenciaDataAccess.searchAgenciaDistrito(distrito);
    }
    
    public TreeMap getCbxAgencia(){
        return agenciaDataAccess.getCbxAgencia();
    }
    
    public Vector obtenerAgencias( String distrito ) throws SQLException {
        return agenciaDataAccess.obtenerAgencias(distrito);
    }
    
    public Agencia obtenerAgencia( String idAgencia ) throws SQLException {
        return agenciaDataAccess.obtenerAgencia(idAgencia);
    }
    
    //JUANM
    public Vector obtenerAgenciasdespachador( String despachador ) throws SQLException {
        return agenciaDataAccess.SEARCH_AGENCIAS_DESPACHADOR(despachador);
    }
    public java.util.Vector getAgencias() {
        return agenciaDataAccess.getAgencias();
    }
    
    ///juanm 061005
    public String Agencias() throws SQLException{
        return agenciaDataAccess.Agencias();
    }
    
    /**
     * Setter for property agencias.
     * @param agencias New value of property agencias.
     */
    public void setAgencias(java.util.Vector agencias) {
        agenciaDataAccess.setAgencias(agencias);
    }
    
    public void cargarAgencias() throws SQLException {
        agenciaDataAccess.obtenerAgencias();
    }
    public Vector getVectorAgencias() throws SQLException {
        return agenciaDataAccess.getVectorAgencias();
    }
    
    //jose 03.11.05
         /**
     * obtiene la agencia relacionada al usuario.
     * @parametro: codigo de la ciudad del usuario.
     */
    public String agenciasUsuarios(String agencia) throws SQLException{
        return agenciaDataAccess.agenciasUsuarios(agencia);
    }
      /**
     * obtiene la agencia relacionada a la remesa.
     * @parametro: codigo de la ciudad de la remesa destino.
     */
    public String agenciasRemesas(String agencia) throws SQLException{
        return agenciaDataAccess.agenciasRemesas(agencia);
    }
      /**
     * obtiene la agencia relacionada a la palnilla.
     * @parametro: codigo de la ciudad de la planilla destino.
     */    
    public String agenciasPlanillas(String agencia) throws SQLException{
        return agenciaDataAccess.agenciasPlanillas(agencia);
    }
    
    /**
     * obtiene la lista ordenada de agencias.
     * @param
     */
    public void listarOrdenadas()throws SQLException{
        agenciaDataAccess.listarOrdenadas();
    }
    /**
     * Obtiene el valor de la propiedad listaAgencias
     * @autor mfontalvo
     * @fecha 2006-01-16
     * @return Value of property listaAgencias.
     */
    public java.util.TreeMap getListaAgencias() {
        return listaAgencias;
    }
    
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.util.TreeMap getAgencia() {
        return agencia;
    }
    
    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia(java.util.TreeMap agencia) {
        this.agencia = agencia;
    }
    
    /**
     * Setea la propiedad agencia.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @return Objeto tipo <code>TreeMap</code> con los c�digos y nombres de las agencias.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void loadAgencias() throws SQLException {
        this.agencia = this.listar();
        this.agencia = this.searchAgencia();
    }
    
        /**
     * Metodo que retorna un vector de agencias de las remesas realizadas en una fecha dada.
     * @autor Ing. Karen Reales.
     * @param Codigo del cliente
     * @param Fecha inicial
     * @param Fecha final
     * @return Vector con agencias
     * @throws SQLException si ocurre un error en la conexi�n con la Base de Datos.
     * @version 1.0
     */
    public Vector  search_agencias_cliente( String cliente, String fecini, String fecfin ) throws SQLException {
        return agenciaDataAccess.search_agencias_cliente(cliente, fecini, fecfin);
    }
    public TreeMap listar() throws SQLException{
        agenciaDataAccess.listar();
        // mfontalvo 2006-01-16
        listaAgencias = agenciaDataAccess.getCbxAgencia();
        return listaAgencias;
    }

     public List listarAgenciasXFacturacion() throws Exception {
        try{
           this.ReiniciarLista();
           this.listaC = agenciaDataAccess.listarAgenciasXFacturacion();
       }
       catch(Exception e){
            throw new Exception("Error en ListarAgencias [CiudadService]...\n"+e.getMessage());
        }
        return this.listaC;
    }
     public void ReiniciarLista(){
        this.listaC = null;
    }

}
