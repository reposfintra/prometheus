/*
 * Codigo_discrepanciaService.java
 *
 * Created on 28 de junio de 2005, 01:12 AM
 */

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  Jose
 */
public class Codigo_discrepanciaService {
    private Codigo_discrepanciaDAO codigo_discrepancia;
    /** Creates a new instance of Codigo_discrepanciaService */
    public Codigo_discrepanciaService() {
        codigo_discrepancia = new Codigo_discrepanciaDAO();
    }
    
    public Codigo_discrepancia getCodigo_discrepancia( )throws SQLException{
        return codigo_discrepancia.getCodigo_discrepancia();
    }
    
    public Vector getCodigo_discrepancias() {
        return codigo_discrepancia.getCodigo_discrepancias();
    }
    
    public void setCodigo_discrepancias(Vector Codigo_discrepancias) {
        codigo_discrepancia.setCodigo_discrepancias(Codigo_discrepancias);
    }
    
    public void insertCodigo_discrepancia(Codigo_discrepancia user) throws SQLException{
        try{
            codigo_discrepancia.setCodigo_discrepancia(user);
            codigo_discrepancia.insertCodigo_discrepancia();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public boolean existCodigo_discrepancia(String cod) throws SQLException{
        return codigo_discrepancia.existCodigo_discrepancia(cod);
    }
    
    public void serchCodigo_discrepancia(String cod)throws SQLException{
        codigo_discrepancia.searchCodigo_discrepancia(cod);
    }
    
    public void listCodigo_discrepancia()throws SQLException{
        codigo_discrepancia.listCodigo_discrepancia();
    }
    
    public void updateCodigo_discrepancia(String cod,String desc,String usu)throws SQLException{
        codigo_discrepancia.updateCodigo_discrepancia(cod, desc, usu);
    }
    public void anularCodigo_discrepancia(Codigo_discrepancia unit)throws SQLException{
            try{
            codigo_discrepancia.setCodigo_discrepancia(unit);
            codigo_discrepancia.anularCodigo_discrepancia();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }  
    }
    public void searchDetalleCodigo_discrepancias(String cod, String desc, String base) throws SQLException{
        try{
            codigo_discrepancia.searchDetalleCodigo_discrepancias(cod, desc, base);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public Vector listarCodigo_discrepancias() throws SQLException{
        try{
            return codigo_discrepancia.listarCodigo_discrepancias();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
}
