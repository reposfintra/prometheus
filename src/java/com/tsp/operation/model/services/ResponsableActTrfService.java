/*
 * ResponsableActTrfService.java
 *
 * Created on 10 de septiembre de 2005, 11:24 AM
 */

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  INTEL
 */
public class ResponsableActTrfService {
    private ResponableActTrfDAO resp;
    /** Creates a new instance of ResponsableActTrfService */
    public ResponsableActTrfService() {
        resp = new ResponableActTrfDAO();
    }
    
    public ResponsableActTrf getResponsableActTrf( )throws SQLException{
        return resp.getResponsableActTrf();
    }
    
    public Vector getResponsableActTrfs() {
        return resp.getResponsableActTrfs();
    }
    
    public void setResponsableActTrfs(Vector ResponsableActTrfs) {
        resp.setResponsableActTrfs(ResponsableActTrfs);
    }
    
    public void insertResponsableActTrf(ResponsableActTrf user) throws SQLException{
        try{
            resp.setResponsableActTrf(user);
            resp.insertResponsableActTrf();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public boolean existResponsableActTrf(String cod) throws SQLException{
        return resp.existResponsableActTrf(cod);
    }
    
    public void serchResponsableActTrf(String cod)throws SQLException{
        resp.searchResponsableActTrf(cod);
    }
    
    public void listResponsableActTrf()throws SQLException{
        resp.listResponsableActTrf();
    }
    
    public void updateResponsableActTrf(String cod,String desc,String usu, String base)throws SQLException{
        resp.updateResponsableActTrf(cod, desc, usu, base);
    }
    public void anularResponsableActTrf(ResponsableActTrf dem)throws SQLException{
            try{
            resp.setResponsableActTrf(dem);
            resp.anularResponsableActTrf();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }  
    }
    public Vector searchDetalleResponsableActTrfs(String cod, String desc, String base) throws SQLException{
        try{
            return resp.searchDetalleResponsableActTrfs(cod, desc, base);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
        public Vector listarResponsableActTrf() throws SQLException{
        try{
            return resp.listarResponsableActTrf();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
 
    public TreeMap searchCDemoras() throws SQLException{
        TreeMap tresp = new TreeMap();
        Vector vresp = this.listarResponsableActTrf();
        ResponsableActTrf r = new ResponsableActTrf();
        //////System.out.println("hey dude: " + vresp.size());
        for( int i=0; i<vresp.size(); i++){
            r = (ResponsableActTrf) vresp.elementAt(i);            
            tresp.put(r.getDescripcion(),  r.getCodigo());
        }
        
        return tresp;
    }
    
    public String obtenerCDemora(String cod) throws SQLException{              
        return this.resp.obtenerResponsableActTrf(cod);
    }
    
}
