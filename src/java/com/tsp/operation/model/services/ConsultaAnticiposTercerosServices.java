/***************************************
    * Nombre Clase ............. ConsultaAnticiposTercerosServices.java
    * Descripci�n  .. . . . . .  Sevicio para consulta anticipos pago a terceros
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  22/08/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/



package com.tsp.operation.model.services;



import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import javax.servlet.*;
import javax.servlet.http.*;



public class ConsultaAnticiposTercerosServices {
    
    
    
    AnticiposPagosTercerosDAO  AnticiposPagosTercerosDataAccess;
    List                       lista; 
    
    
    
 // Filtros:
    private String distrito_ ;        
    private String proveedor_;             
    private String ckAgencia_;       
    private String Agencia_ ;               
    private String ckPropietario_;   
    private String Propietario_;             
    private String ckPlanilla_;      
    private String Planilla_;                
    private String ckPlaca_;
    private String Placa_;                  
    private String ckConductor_;     
    private String Conductor_;            
    private String ckReanticipo_;    
    private String reanticipo_;
    
    private String ckFechas_;
    private String fechaIni_;
    private String fechaFin_;
    private String ckLiquidacion_; 
    private String Liquidacion_;
    private String ckTransferencia_; 
    private String Transferencia_; 
    private String ckFactura_; 
    private String Factura_;
    
    
    public ConsultaAnticiposTercerosServices() {
        AnticiposPagosTercerosDataAccess  =  new  AnticiposPagosTercerosDAO();
        reset();
    }
    public ConsultaAnticiposTercerosServices(String dataBaseName ) {
        AnticiposPagosTercerosDataAccess  =  new  AnticiposPagosTercerosDAO(dataBaseName);
        reset();
    }
    
    
    
    
    public void reset(){
        lista      =  new LinkedList();
        
        ckAgencia_      = null;       
        ckPropietario_  = null;   
        ckPlanilla_     = null;      
        ckPlaca_        = null;
        ckConductor_    = null;     
        ckReanticipo_   = null; 
        ckFechas_       = null;
        ckLiquidacion_  = null; 
        ckTransferencia_= null; 
        ckFactura_      = null; 
    
        distrito_        = "";
        Agencia_         = "";
        Propietario_     = "";
        Planilla_        = "";
        Placa_           = ""; 
        Conductor_       = ""; 
        reanticipo_      = "";  
        Liquidacion_     = "";
        Transferencia_   = "";
        Factura_         = "";
        
        fechaIni_       = Utility.getHoy("-");
        fechaFin_       = Utility.getHoy("-");
    }
    
    
    
    
    /**
     * M�todos que busca los anticipos de la consulta
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/
     public void searchAnticipos       (    String distrito,        String proveedor,                                  
                                            String ckAgencia,       String Agencia ,                       
                                            String ckPropietario,   String Propietario,                      
                                            String ckPlanilla,      String Planilla,                     
                                            String ckPlaca ,        String Placa, 
                                            String ckConductor,     String Conductor, 
                                            String ckReanticipo,    String reanticipo,
                                            String ckFechas,        String fechaIni,  String fechaFin,                                           
                                            String ckLiquidacion,   String Liquidacion, 
                                            String ckTransferencia, String Transferencia, 
                                            String ckFactura,       String Factura
                                            
                                      ) throws Exception{
         
         reset();                               
         try{             
            
             distrito_         = distrito;        
             proveedor_        = proveedor;              
             ckAgencia_        = ckAgencia;
             Agencia_          = Agencia;               
             ckPropietario_    = ckPropietario;   
             Propietario_      = Propietario;            
             ckPlanilla_       = ckPlanilla;
             Planilla_         = Planilla;               
             ckPlaca_          = ckPlaca;
             Placa_            = Placa;                  
             ckConductor_      = ckConductor;     
             Conductor_        = Conductor;              
             ckReanticipo_     = ckReanticipo;    
             reanticipo_       = reanticipo;             
             ckFechas_         = ckFechas;
             fechaIni_         = fechaIni;
             fechaFin_         = fechaFin;
             
             ckLiquidacion_    = ckLiquidacion; 
             Liquidacion_      = Liquidacion;
             ckTransferencia_  = ckTransferencia; 
             Transferencia_    = Transferencia; 
             ckFactura_        = ckFactura; 
             Factura_          = Factura;
             
             
             lista = this.AnticiposPagosTercerosDataAccess.consultarAnticipos(distrito, proveedor, ckAgencia, Agencia, ckPropietario, Propietario, ckPlanilla, Planilla, ckPlaca, Placa , ckConductor, Conductor, ckReanticipo, reanticipo, ckFechas, fechaIni, fechaFin,  ckLiquidacion, Liquidacion, ckTransferencia, Transferencia, ckFactura, Factura );             
             
         }catch(Exception e){
            throw new Exception(e.getMessage());
         }                      
    }
    
    
    
    
    
     
     /**
     * M�todos que refresca la busqueda
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/
     public void refresh( ) throws Exception{
         try{ 
             
             lista = this.AnticiposPagosTercerosDataAccess.consultarAnticipos(distrito_, proveedor_, ckAgencia_, Agencia_, ckPropietario_, Propietario_, ckPlanilla_, Planilla_, ckPlaca_, Placa_ , ckConductor_, Conductor_, ckReanticipo_, reanticipo_, ckFechas_, fechaIni_, fechaFin_,  ckLiquidacion_, Liquidacion_, ckTransferencia_, Transferencia_, ckFactura_, Factura_ );             
             
         }catch(Exception e){
            throw new Exception(e.getMessage());
         }                      
    }
    
    
    
    
    
    
    /**
     * Getter for property lista.
     * @return Value of property lista.
     */
    public java.util.List getLista() {
        return lista;
    }    
    
    /**
     * Setter for property lista.
     * @param lista New value of property lista.
     */
    public void setLista(java.util.List lista) {
        this.lista = lista;
    }    
    
    
}
