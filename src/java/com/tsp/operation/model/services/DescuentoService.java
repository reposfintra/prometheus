/****************************************************************************************************
 * Nombre clase: Descuento.java                                                                     *
 * Descripci�n: Clase que maneja los servicios al model relacionados con los descuentos .           *                                                   *
 * Autor: Ing. Jose de la rosa                                                                      *
 * Fecha: 12 de diciembre de 2005, 09:15 AM                                                         *
 * Versi�n: Java 1.0                                                                                *
 * Copyright: Fintravalores S.A. S.A.                                                          *
 ***************************************************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

public class DescuentoService {
    private DescuentoDAO descuento;
    /** Creates a new instance of DescuentoService */
    public DescuentoService () {
        descuento = new DescuentoDAO ();
    }
    /**
     * Metodo: getDescuento, permite retornar un objeto de registros de descuento.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Descuento getDescuento ( )throws SQLException{
        return descuento.getDescuento ();
    }
    
    /**
     * Metodo: getDescuentoes, permite retornar un vector de registros de Acuerdo clase.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Vector getDescuentos ()throws SQLException{
        return descuento.getDescuentos ();
    }
    
    /**
     * Metodo: setDescuento, permite obtener un objeto de registros de descuento.
     * @autor : Ing. Jose de la rosa
     * @param : objeto
     * @version : 1.0
     */
    public void setDescuento (Descuento des)throws SQLException {
        descuento.setDescuento (des);
    }
    
    /**
     * Metodo: setDescuentos, permite obtener un vector de registros de descuento.
     * @autor : Ing. Jose de la rosa
     * @param : vector
     * @version : 1.0
     */
    public void setDescuentos (Vector descuentos)throws SQLException{
        descuento.setDescuentos (descuentos);
    }
    
    /**
     * Metodo insertDescuento, ingresa los descuentos (Descuento).
     * @autor : Ing. Jose de la rosa
     * @see insertarDescuento - DescuentoDAO
     * @param : Descuento
     * @version : 1.0
     */
    public String insertDescuento (Descuento user) throws SQLException{
        try{
            descuento.setDescuento (user);
            return descuento.insertDescuento ();
        }
        catch(SQLException e){
            e.printStackTrace();
            throw new SQLException (e.getMessage ());
        }
    }
    /**
     * Metodo: existDescuento, permite buscar un descuento clase dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : numero de planilla.
     * @see insertarDescuento - DescuentoDAO
     * @version : 1.0
     */
    public boolean existDescuentoPlanillaFecha (String distrito, String numpla, String concepto, String fecha) throws SQLException{
        try{
            return descuento.existDescuentoPlanillaFecha (distrito, numpla, concepto, fecha);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
    /**
     * Metodo: obtenerCodigoCuenta, permite obtener el codigo de la cuenta
     * @autor : Ing. Jose de la rosa
     * @param : numero de planilla.
     * @see obtenerCodigoCuenta - DescuentoDAO
     * @version : 1.0
     */
    public String obtenerCodigoCuenta (String distrito, String proveedor, String concepto, String numpla) throws SQLException{
            try{
            return descuento.obtenerCodigoCuenta (distrito, proveedor, concepto, numpla);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
    /**
     * Metodo: existDescuentoMovPlanilla, permite verificar si existe el registro en MOVPLA
     * @autor : Ing. Jose de la rosa
     * @param : numero de planilla.
     * @version : 1.0
     */
    public boolean existDescuentoMovPlanilla (String numpla, String concepto, String proveedor) throws SQLException{
        try{
            return descuento.existDescuentoMovPlanilla (numpla, concepto, proveedor);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }        
    }
    
    /**
     * Metodo: existDescuentoMantenimento,  permite verificar si existe el registro en DESCUENTO_MANTENIMIENTO
     * @autor : Ing. Jose de la rosa
     * @param : numero de planilla.
     * @version : 1.0
     */
    public boolean existDescuentoMantenimento ( String distrito, String numpla, String concepto ) throws SQLException{
        try{
            return descuento.existDescuentoMantenimento (distrito, numpla, concepto);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }          
    }
    
}
