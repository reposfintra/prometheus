/******************************************************************
* Nombre ......................TblGeneralProgService.java
* Descripci�n..................Clase Service para tablagen_prog (antes tbl_general_prog)
* Autor........................ricardo Rosero
* Fecha........................23/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
* modificado...................29/12/2005
*******************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.sql.*;
import java.util.*;

public class TblGeneralProgService {
    
    private TblGeneralProgDAO tgdao; 
    
    /** Creates a new instance of TblGeneralProgService */
    public TblGeneralProgService() {
        tgdao = new TblGeneralProgDAO();
    }
    
    /**
     * M�todo que setea un objeto TblGeneralProg
     * @autor.......ricardo rosero          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......TblGeneralProg
     **/
    public void setTblGeneralProg(TblGeneralProg tg){
        tgdao.setTG(tg);
    }
    
    /**
     * M�todo que retorna un objeto TblGeneralProg
     * @autor.......ricardo rosero
     * @throws......SQLException
     * @version.....1.0.
     * @return.......TblGeneralProg
     **/     
    public TblGeneralProg getTblGeneralProg(){
        return tgdao.getTblGeneralProg();
    }
    
    /**
     * M�todo que busca y setea un objeto TblGeneralProg
     * @autor.......ricardo rosero
     * @throws......SQLException
     * @version.....1.0.
     * @param.......TblGeneralProg de cargado con el set
     **/ 
    public void BuscarTblGeneralProg() throws SQLException{
        tgdao.buscarTblGeneralProg();
    }
    
    /**
     * @autor.......ricardo rosero
     * @throws......SQLException
     * @version.....1.0.
     **/     
    public boolean existeTblGeneralProg() throws SQLException{
        return tgdao.existeTblGeneralProg();
    }
    
    /**
     * @autor.......ricardo rosero
     * @throws......SQLException
     * @version.....1.0.
     * @param.......c�digo del descuento cargado con el set
     **/   
    public boolean existeTblGeneralProgAnulado() throws SQLException{
        return tgdao.existeTblGeneralProgAnulado();
    }
    
    /**
     * @autor.......ricardo rosero
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void updateTblGeneralProgAnulado() throws SQLException{
        tgdao.updateTblGeneralProgAnulado();
    }
    
    /**
     * @autor.......ricardo rosero
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void addTblGeneralProg() throws SQLException{
        try{
            tgdao.addTblGeneralProg();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    /**
     * @autor.......ricardo rosero
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void updateTblGeneralProg() throws SQLException{        
        tgdao.updateTblGeneralProg();          
    }
    
    /**
     * @autor.......ricardo rosero
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void deleteTblGeneralProg() throws SQLException{
        tgdao.deleteTblGeneralProg();
    }
    
    /**
     * @autor.......ricardo rosero
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void buscarTodosTblGeneralProg() throws SQLException{
        tgdao.buscarTodosTblGeneralProg();
    }
    
    /**
     * @autor.......ricardo rosero        
     * @throws......SQLException
     * @version.....1.0.     
     * @return......Vector de objetos TblGeneralProg
     **/     
    public Vector getTodosTblGeneralProg(){
        return tgdao.getTodosTblGeneralProg();
    }
    
    //Ricardo
    /**
    * listarTodos
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public void listarTodos() throws SQLException {
       try{
           tgdao.listarTodos();
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
    }
    
    /**
    * listarPortable_code
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public void listarPortable_code(String table_type, String table_code) throws SQLException {
       try{
           tgdao.listarPortable_code(table_type, table_code);
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
    }
    
    /**
    * listarTodos2
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public void listarTodos2() throws SQLException {
       try{
           tgdao.listarTodos2();
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
    }
    
    /**
    * listarTodos3
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public void listarTodos3() throws SQLException {
       try{
           tgdao.listarTodos3();
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
    }
    
    /**
    * listarTodos4
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public void listarTodos4() throws SQLException {
       try{
           tgdao.listarTodos4();
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
    }
    
    /**
    * listarTodos5
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public void listarTodos5() throws SQLException {
       try{
           tgdao.listarTodos5();
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
    }
    
    /**
    * listarTodos6
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public void listarTodos6() throws SQLException {
       try{
           tgdao.listarTodos6();
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
    }
    
    /**
    * listarTodos7
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public void listarTodos7() throws SQLException {
       try{
           tgdao.listarTodos7();
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
    }
    
    /**
    * listarTodos8
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public void listarTodos8() throws SQLException {
       try{
           tgdao.listarTodos8();
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
    }
    
    /**
    * obtTblGeneralProg
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ none
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public Vector obtTblGeneralProg() throws SQLException{
       return tgdao.obtTablaGen();
    }
    
    /**
    * existeTblGeneralProg
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ table_type, table_code
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public boolean existeTblGeneralProg( String table_type, String table_code ) throws SQLException{
        boolean existe = false;
        try{
            existe = tgdao.existeTblGeneralProg( table_type, table_code );
        }
        catch(SQLException sqlEx){
            sqlEx.printStackTrace();
        }
        return existe;
    }
    
    /**
    * anularTblGeneralProg
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public void anularTblGeneralProg() throws SQLException{
        tgdao.anularTblGeneralProg(); 
    }
    /**
    * M�todo: buscarEmailsCliente
     *@param cod Codigo del cliente
    * @autor: ....... Ing. David Pi�a Lopez     
    * @throws ....... SQLException
    * @version ...... 1.0
    */
    public void buscarEmailsCliente( String cod )throws SQLException{
        tgdao.buscarEmailsCliente( cod );
    }
    /**
    * M�todo: buscarClientesEmail
    * @autor: ....... Ing. David Pi�a Lopez
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public void buscarClientesEmail()throws SQLException{
        tgdao.buscarClientesEmail();
    }
        /**
    * existeTblGeneralProg
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ table_type, table_code
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public boolean existeTblGeneralProg( String table_type, String table_code, String program ) throws SQLException{
            return tgdao.existeTblGeneralProg( table_type, table_code, program );
    }

    /**
    * M�todo: estaAnuladoTblGeneralProg
    * @autor: ....... Ing. Ricardo Rosero
    * @param: ....... table_type, table_code
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public boolean estaAnuladoTblGeneralProg( String table_type, String table_code, String program ) throws SQLException {
            return tgdao.estaAnuladoTblGeneralProg( table_type, table_code, program );
    }

    /**
    * listarTodos
    * @autor: ....... Ing. Ricardo Rosero
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public void listarTodos(String table_type, String table_code, String program) throws SQLException {
       try{
           tgdao.listarTodos(table_type, table_code, program);
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
    }
    
}
