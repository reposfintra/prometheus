/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.services;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
/**
 *
 * @author Rhonalf
 */
public class MaterialesService {

    private MaterialesDAO mdao;
    
    private ArrayList resultado;
    
    public MaterialesService(){
        mdao = new MaterialesDAO();
    }
public ArrayList getCategorias(String tipo) throws Exception{
        return mdao.getCategorias(tipo);
    }
    public void anularProducto(String codigo,String user){
        try{
            mdao.anularProducto(codigo, user);
        }
        catch(Exception e){
            System.out.println("Error en anular producto: "+e.toString());
        }
    }

public void insertarProducto(String descripcion,double precio,String consec,String tipo,String medida,String categoria,String usuario) {
         try{
             mdao.insertarProducto(descripcion, precio, consec, tipo, medida, categoria, usuario);
         }
         catch(Exception e){
            System.out.println("Error en insertar producto: "+e.toString());
        }
     }

     public String contarProductos(){
         return mdao.contarProductos();
     }

      public ArrayList verTodos(){
         ArrayList res = null;
          try{
             res = mdao.verTodos();
         }
          catch(Exception e){
              System.out.println("Error en listar todos: "+e.toString());
          }
         return res;
      }

      public ArrayList verMateriales(){
         ArrayList res = null;
          try{
             res = mdao.listarMateriales();
         }
          catch(Exception e){
              System.out.println("Error en listar materiales: "+e.toString());
          }
         return res;
      }

      public ArrayList verMano(){
         ArrayList res = null;
          try{
             res = mdao.listarManos();
         }
          catch(Exception e){
              System.out.println("Error en listar manos: "+e.toString());
          }
         return res;
      }

      public ArrayList verOtros(){
         ArrayList res = null;
          try{
             res = mdao.listarOtros();
         }
          catch(Exception e){
              System.out.println("Error en listar otros: "+e.toString());
          }
         return res;
      }

      public ArrayList buscarPor(int filtro,String texto) throws Exception{ 
        ArrayList rx4 = new ArrayList();
        try{
            rx4 = mdao.buscarPor(filtro,texto);
            setResult(rx4);
        }
        catch(Exception e){
            System.out.println("Error en buscar por: "+e.toString());
            e.printStackTrace(); 
        }
        return rx4;
    }
      
    private void setResult(ArrayList r){
        this.resultado = r;
    }
    
    public ArrayList getResultado(){
        return this.resultado;
    }
    
    //091202
    public ArrayList cargaTipoMats() throws Exception {
        ArrayList rx4 = new ArrayList();
        try{
            rx4 = mdao.cargaTipo(1);
            if(rx4.size()<0) throw new Exception("No hay valores para la busqueda solicitada");
        }
        catch(Exception e){
            System.out.println("Error: "+e.toString());
            e.printStackTrace();
        }
        return rx4;
    }

    public ArrayList cargaTipoMano() throws Exception {
        ArrayList rx4 = new ArrayList();
        try{
            rx4 = mdao.cargaTipo(2);
             if(rx4.size()<0) throw new Exception("No hay valores para la busqueda solicitada");
        }
        catch(Exception e){
            System.out.println("Error: "+e.toString());
            e.printStackTrace();
        }
        return rx4;
    }

    public ArrayList cargaTipoOtros() throws Exception {
        ArrayList rx4 = new ArrayList();
        try{
            rx4 = mdao.cargaTipo(3);
            if(rx4.size()<0) throw new Exception("No hay valores para la busqueda solicitada");
        }
        catch(Exception e){
            System.out.println("Error : "+e.toString());
            e.printStackTrace();
        }
        return rx4;
    }
    
    public ArrayList buscarPor(int criterio) throws Exception {
        ArrayList rx4 = new ArrayList();
        try{
            rx4 = mdao.buscarPor(criterio);
            if(rx4.size()<0) throw new Exception("No hay valores para la busqueda solicitada");
        }
        catch(Exception e){
            System.out.println("Error : "+e.toString());
            e.printStackTrace();
        }
        return rx4;
    }
    public ArrayList buscarPorAnul(int filtro,String texto) throws Exception{
        ArrayList rx4 = new ArrayList();
        try{
            rx4 = mdao.buscarPorAnul(filtro,texto);
            setResult(rx4);
        }
        catch(Exception e){
            System.out.println("Error en buscar por anul.: "+e.toString());
            e.printStackTrace();
        }
        return rx4;
    }


}
