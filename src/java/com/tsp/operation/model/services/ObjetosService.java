/*
 * ObjetosServices.java
 *
 * Created on 4 de enero de 2006, 11:15 AM
 */

package com.tsp.operation.model.services;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
/**
 *
 * @author  EQUIPO12
 */
public class ObjetosService {
        private ObjetosDAO dao;
        
        /** Creates a new instance of ObjetosServices */
        public ObjetosService() {
                dao = new ObjetosDAO();
        }
        
        public void escribirArchivoObjeto(Object Objeto, String user, String pagina) throws IOException{
               dao.escribirArchivo(Objeto, user, pagina);
        }
        
        public Object leerArchivoObjeto(String user, String pagina) throws IOException,ClassNotFoundException{
               Object Objeto=null;
               Objeto = dao.leerArchivo(pagina , user);
               return Objeto;
        }
        
}
