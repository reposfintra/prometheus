/***************************************
* Nombre Clase ............. IngresoPrestamoService.java
* Descripci�n  .. . . . . .  Ofrece los servicios para registrar ingresos a los prestamos
* Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
* Fecha . . . . . . . . . .  18/03/2006
* versi�n . . . . . . . . .  1.0
* Copyright ...Transportes Sanchez Polo S.A.
*******************************************/




package com.tsp.operation.model.services;




import java.io.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.IngresoPrestamoDAO;


public class IngresoPrestamoService {
    
   private IngresoPrestamoDAO  dao;
   private Prestamo            prestamo;
   private List                listAmortizacion;
   private String              tercero;
   private String              idPrestamo;
   private String              distrito;
   
    
    public IngresoPrestamoService() {
        dao              = new IngresoPrestamoDAO();
        prestamo         = null;
        listAmortizacion = new LinkedList();
    }
    
    
    
    public void reset(){
         prestamo         = null;
         listAmortizacion = new LinkedList();
    }
    
    
    
     /**
     * Metodo que busca el prestamo
     * @autor fvillacon
     * @param  String distrito,String tercero, String id
     * @throws Exception.
     */    
    public void serachPrestamo(String distrito,String tercero, String prestamo )throws Exception{
        try{
            reset();
            this.tercero     = tercero;
            this.idPrestamo  = prestamo;
            this.distrito    = distrito;
            this.prestamo    = this.dao.getPrestamos(distrito, tercero, prestamo );
            if(this.prestamo!=null){
                listAmortizacion = this.dao.getAmortizaciones(distrito, tercero, prestamo);
                this.prestamo.setAmortizacion( listAmortizacion );
            }
            
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    
    
    
    
    
    /**
     * Metodo que cancela la cuota
     * @autor  fvillacon
     * @param  String[] cuotas
     * @throws Exception.
     */   
    public void  cancelarCuotas(String[] cuotas, String banco, String sucursal, String cheque, String corrida, String user)throws Exception{
        try{
            for(int i=0;i<cuotas.length;i++){
                String       item  = cuotas[i];
                Amortizacion amort = this.getCuota( item );
                if(amort!=null)
                    this.dao.cancelarPagoCuota(amort, banco, sucursal, cheque, corrida, user );
            }
            this.serachPrestamo( distrito, tercero, idPrestamo);
            
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    
    
    
    /**
     * Metodo que devuelve la cuota de la lista
     * @autor fvillacon
     * @param  int item
     * @throws Exception.
     */ 
    public Amortizacion getCuota(String item){
        Amortizacion amort = null;
        for(int i=0;i<listAmortizacion.size();i++){
            Amortizacion amortizacion = (Amortizacion)listAmortizacion.get(i);
            if( amortizacion.getItem().equals(item) ){
                amort = amortizacion;
                listAmortizacion.remove(i);
                break;
            }
        }
        return amort;
    }
    
    
    
    
    
    
    
    
    
    /**
     * Getter for property prestamo.
     * @return Value of property prestamo.
     */
    public com.tsp.operation.model.beans.Prestamo getPrestamo() {
        return prestamo;
    }    
    
    /**
     * Setter for property prestamo.
     * @param prestamo New value of property prestamo.
     */
    public void setPrestamo(com.tsp.operation.model.beans.Prestamo prestamo) {
        this.prestamo = prestamo;
    }    
    
    
    
    
    /**
     * Getter for property listAmortizacion.
     * @return Value of property listAmortizacion.
     */
    public java.util.List getListAmortizacion() {
        return listAmortizacion;
    }    
    
    /**
     * Setter for property listAmortizacion.
     * @param listAmortizacion New value of property listAmortizacion.
     */
    public void setListAmortizacion(java.util.List listAmortizacion) {
        this.listAmortizacion = listAmortizacion;
    }    
    
    
    
    /**
     * Getter for property tercero.
     * @return Value of property tercero.
     */
    public java.lang.String getTercero() {
        return tercero;
    }    
    
    /**
     * Setter for property tercero.
     * @param tercero New value of property tercero.
     */
    public void setTercero(java.lang.String tercero) {
        this.tercero = tercero;
    }    
    
    
    
    
    /**
     * Getter for property idPrestamo.
     * @return Value of property idPrestamo.
     */
    public java.lang.String getIdPrestamo() {
        return idPrestamo;
    }    
    
    /**
     * Setter for property idPrestamo.
     * @param idPrestamo New value of property idPrestamo.
     */
    public void setIdPrestamo(java.lang.String idPrestamo) {
        this.idPrestamo = idPrestamo;
    }    
    
    
    
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
}
