/************************************************************************************
 * Nombre clase: JustificacionService.java                                          *
 * Descripci�n: Clase que maneja los servicios al model relacionados con            *
 *              las Jusificaciones.                                                 *
 * Autor: Ing. Jose de la rosa                                                      *
 * Fecha: 14 de marzo de 2006, 08:38 AM                                             *
 * Versi�n: Java 1.0                                                                *
 * Copyright: Fintravalores S.A. S.A.                                          *
 ***********************************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

public class JustificacionService {
    private JustificacionDAO justificacion;
    
    /** Creates a new instance of JustificacionService */
    public JustificacionService () {
        this.justificacion = new JustificacionDAO ();
    }
    
    /**
     * Metodo: getJustificacion, permite retornar un objeto de registros de justificacion.
     * @autor : Ing. Jose de la rosa
     * @see getJustificacion - JustificacionDAO
     * @param :
     * @version : 1.0
     */
    public Justificacion getJustificacion ( )throws SQLException{
        return justificacion.getJustificacion ();
    }
    
    /**
     * Metodo: getVecJustificacion, permite retornar un vector de registros de justificacion.
     * @autor : Ing. Jose de la rosa
     * @see getVecJustificacion - JustificacionDAO
     * @param :
     * @version : 1.0
     */
    public Vector getVecJustificacion () {
        return justificacion.getVecJustificacion ();
    }
    
     /**
     * Metodo: setJustificacion, permite retornar un objeto de registros de justificacion.
     * @autor : Ing. Jose de la rosa
     * @see setJustificacion - JustificacionDAO
     * @param :
     * @version : 1.0
     */
    public void setJustificacion (Justificacion jus) {
        justificacion.setJustificacion (jus);
    }
    
        /**
     * Metodo: setVecJustificacion, permite obtener un vector de registros de justificacion.
     * @autor : Ing. Jose de la rosa
     * @see setVecJustificacion - JustificacionDAO
     * @param : vector
     * @version : 1.0
     */
    public void setVecJustificacion (Vector vec) {
        justificacion.setVecJustificacion (vec);
    }
    
    /**
    * Metodo: ingresarJustificacion, permite ingresar un registro a la tabla justificacion.
    * (justificacion).
    * @autor : Ing. Jose de la rosa
    * @see ingresarJustificacion - JustificacionDAO
    * @param : objeto de justificacion
    * @version : 1.0
    */
    public String ingresarJustificacion (Justificacion justificaciones) throws java.sql.SQLException{
        this.justificacion.setJustificacion (justificaciones);
        return this.justificacion.ingresarJustificacion ();
    }
    
    /**
     * Metodo: ingresarJustificacion, permite ingresar un registro a la tabla justificacion.
     * @autor : Ing. Jose de la rosa
     * @see listarJustificaciones - JustificacionDAO
     * @param : objeto de justificacion
     * @version : 1.0
     */    
    public void listarJustificaciones(Justificacion justificaciones)throws SQLException{
        this.justificacion.setJustificacion (justificaciones);
        this.justificacion.listarJustificaciones ();
    }
}
