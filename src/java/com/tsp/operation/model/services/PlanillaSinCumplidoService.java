/***************************************************************************
 * Nombre clase : ............... PlanillaSinCumplidoService.java          *
 * Descripcion :................. Clase que maneja los Servicios           *
 *                                asignados a Model relacionados con el    *
 *                                programa de generacion del reporte de    *
 *                                planillas sin cumplido                   *
 * Autor :....................... Ing. David Velasquez Gonzalez   
 * Modified:......................Ing. Enrique De Lavalle                  *
 * Fecha :........................ 24 de noviembre de 2005, 02:40 PM       *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.util.*;

public class PlanillaSinCumplidoService {
    private PlanillaSinCumplidoDAO  PADataAccess;
    private List                    ListaPA;
    private PlanillaSinCumplido     Datos;
    
    /** Creates a new instance of AnulacionPlanillaService */
    public PlanillaSinCumplidoService() {
        PADataAccess    = new PlanillaSinCumplidoDAO();
        ListaPA         = new LinkedList();
        Datos           = new PlanillaSinCumplido();
    }
    
    
    /**
     * Metodo listPlanillaSinCumplido, lista todas las planillas que se encuentran sin cumplido 
     * entre un rango de fechas como parametros de busqueda y un cliente como paramentros
     * adicionales
     * @autor : Ing. David Velasquez Gonzalez
     * @see   : listPlanillaSinCumplido - PlanillaSinCumplidoDAO
     * @param : String fecha inicial , String fecha final, String cliente
     * @version : 1.0
     */
    public void listPlanillaSinCumplido( String fechaInicial, String fechaFinal,String cliente) throws Exception {
        try{
            this.ReiniciarListaPA();
            this.ListaPA = PADataAccess.listPlanillaSinCumplido(fechaInicial, fechaFinal, cliente);
        }
        catch(Exception e){
            throw new Exception("Error en listPlanillasSinCumplido [PlanillaSinCumplidoService]...\n"+e.getMessage());
        }
    }
    
    
    public void ReiniciarDato(){
        this.Datos = null;
    }
    
  
    public void ReiniciarListaPA(){
        this.ListaPA = null;
    }
    
    
    public PlanillaSinCumplido getDato(){
        return this.Datos;
    }
    

    public List getListPA(){
        return this.ListaPA;
    }
    
}
