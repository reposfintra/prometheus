/******************************************************************
* Nombre ......................VehNoRetornadoService.java
* Descripci�n..................Clase DAO para Veh�culos No Retornados
* Autor........................Armando Oviedo
* Fecha........................18/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
/**
 *
 * @author  Armando Oviedo
 */
public class VehNoRetornadoService {
    
    VehNoRetornadoDAO vnrdao;
    Vector causas;
    /** Creates a new instance of VehNoRetornadoService */
    public VehNoRetornadoService() {
        vnrdao = new VehNoRetornadoDAO();
    }
    
    /**
     * Metodo setVehiculoNoRetornado, setea un vehiculo no retornado     
     * @autor : Ing. Armando Oviedo
     * @see:  VehNoRetornadoDAO, VehNoRetornado     
     * @version : 1.0
     */    
    public void setVehiculoNoRetornado(VehNoRetornado vnr){
        this.vnrdao.setVehiculoNoRetornado(vnr);
    }
    
    /**
     * Metodo getVehiculoNoRetornado, getea un objeto vehiculo no retornado
     * @autor.......Ing. Armando Oviedo               
     * @see:  VehNoRetornadoDAO, VehNoRetornado
     * @version.....1.0.
     * @return.......VehNoRetornado de
     **/     
    public VehNoRetornado getVehiculoNoRetornado(){
        return this.vnrdao.getVehiculoNoRetornado();
    }
    
    /**
     * Metodo buscarVehiculoNoRetornado, buscar un objeto de vehiculo no retornado
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @see:  VehNoRetornadoDAO, VehNoRetornado     
     **/ 
    public void buscarVehiculoNoRetornado() throws SQLException{
        this.vnrdao.buscarVehiculoNoRetornado();
    }
    
    /**
     * M�todo existeVehiculoNoRetornado, retorna un boolean si existe el VehNoRetornado o no
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @see:  VehNoRetornadoDAO, VehNoRetornado     
     **/     
    public boolean existeVehiculoNoRetornado() throws SQLException{
        return vnrdao.existeVehiculoNoRetornado();        
    }
    
    /**
     * M�todo existeVehiculoNoRetornadoAnulado, retorna un boolean si existe el VehNoRetornado o no, anulado
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @see:  VehNoRetornadoDAO, VehNoRetornado     
     **/   
    public boolean existeVehiculoNoRetornadoAnulado() throws SQLException{
        return vnrdao.existeVehiculoNoRetornadoAnulado();
    }

    /**
     * M�todo updateVehiculoNoRetornadoAnulado, actualiza el reg_status de un VehNoRetornado anulado
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @see:  VehNoRetornadoDAO, VehNoRetornado     
     * @version.....1.0.     
     **/ 
    public void updateVehiculoNoRetornadoAnulado() throws SQLException{
        this.vnrdao.updateVehiculoNoRetornadoAnulado();
    }
    
    /**
     * M�todo addVehiculoNoRetornado, agrega un VehNoRetornado
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void addVehiculoNoRetornado() throws SQLException{
        if(!existeVehiculoNoRetornadoAnulado()){
            this.addVehiculoNoRetornado();
        }else{            
            this.updateVehiculoNoRetornadoAnulado();
        }
    }
    
    /**
     * M�todo que modifica un VehNoRetornado
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @see:  VehNoRetornadoDAO, VehNoRetornado     
     * @version.....1.0.     
     **/ 
    public void updateVehiculoNoRetornado() throws SQLException{
        this.vnrdao.updateVehiculoNoRetornado();
    }
    
    /**
     * M�todo deleteVehiculoNoRetornado, elimina un VehNoRetornado
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @see:  VehNoRetornadoDAO, VehNoRetornado     
     * @version.....1.0.     
     **/ 
    public void deleteVehiculoNoRetornado() throws SQLException{
        this.vnrdao.deleteVehiculoNoRetornado();
    }
    
    /**
     * M�todo buscarTodosVehiculosNoRetornados, setea un vector que contiene todos los objetos de la tabla
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @see:  VehNoRetornadoDAO, VehNoRetornado     
     * @version.....1.0.     
     **/ 
    public void buscarTodosVehiculosNoRetornados() throws SQLException{
        this.vnrdao.buscarTodosVehiculosNoRetornados();
    }
    
    /**
     * M�todo getTodosVehiculosNoRetornados, que guetea todos los descuentosequipos
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     * @see:  VehNoRetornadoDAO, VehNoRetornado     
     * @return......Vector de objetos VehNoRetornado
     **/     
    public Vector getTodosVehiculosNoRetornados(){
        return this.vnrdao.getTodosVehiculosNoRetornados();
    }
    
    /**
     * M�todo que busca causas de VNR definidas en tablagen
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @see.........VehNoRetornadoDAO
     * @param.......VehNoRetornado de
     **/
    public void buscarCausas() throws SQLException{
        this.vnrdao.buscarCausas();
    }
    
    /**
     * M�todo que busca y setea un objeto VehNoRetornado
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......VehNoRetornado de cargado con el set
     **/
    public void buscarVehiculosNoRetornados() throws SQLException{
        this.vnrdao.buscarVehiculosNoRetornados();
    }
    
    /**
     * M�todo getCausasTblGeneral, que guetea un treemap que contiene todas las causas del no retorno del vehiculo
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     * @see:  TblGeneralDao, TblGeneral
     * @return......TreeMap de objetos Tabla General
     **/     
    public java.util.TreeMap getCausasTblGeneral(){        
        TreeMap causas = new TreeMap();
        try{            
            Vector veccausas = this.vnrdao.getCausas();
            for(int i=0;i<veccausas.size();i++){
                Vector tmp = new Vector();
                tmp = (Vector)(veccausas.elementAt(i));
                causas.put(String.valueOf(tmp.elementAt(1)),String.valueOf(tmp.elementAt(0)));            
            }        
        }
        catch(Exception ex){
           ex.printStackTrace();
        }
        return causas;
    }
}
