/******************************************************************
* Nombre ......................EquipoPropioService.java
* cescripci�n..................Clase Service para equipo propio
* Autor........................Armando Oviedo
* Fecha........................12/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.services; 

import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;

public class EquipoPropioService {
    
    EquipoPropioDAO epdao;
    /** Creates a new instance of EquipoPropioService */
    public EquipoPropioService() {
        epdao = new EquipoPropioDAO();
    }
    
    public void setEP(EquipoPropio ep){
        epdao.setEP(ep);
    }
    
    public EquipoPropio getEquipoPropio(){
        return epdao.getEquipoPropio();
    }
    
    /**
     * M�todo que retorna si existe o no una placa
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean existe
     **/
    public boolean existePlaca() throws SQLException{
        return epdao.existePlaca();
    }
    
    /**
     * M�todo que retorna si existe o no una clase de equipo
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean existe
     **/
    public boolean existeClaseEquipo() throws SQLException{
        return epdao.existeClaseEquipo();
    }
    
    /**
     * M�todo que busca y setea un objeto EquipoPropio
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......EquipoPropio ce cargado con el set
     **/ 
    public void BuscarEquipoPropio() throws SQLException{
        epdao.BuscarEquipoPropio();
    }
    
    /**
     * M�todo que retorna un boolean si existe el EquipoPropio o no
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......c�digo cel cescuento cargado con el set
     **/     
    public boolean existeEP() throws SQLException{
        return epdao.existeEP();
    }
    
    /**
     * M�todo que retorna un boolean si existe el EquipoPropio o no, anulado
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......c�digo cel cescuento cargado con el set
     **/   
    public boolean existeCEAnulado() throws SQLException{
        return epdao.existeCEAnulado();
    }
    
    /**
     * M�todo que actualiza el reg_status ce un EquipoPropio anulado
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void addCEUPD() throws SQLException{
        epdao.addCEUPD();
    }
    
    /**
     * M�todo que agrega un EquipoPropio
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void addCE() throws SQLException{
        epdao.addCE();
    }
    
    /**
     * M�todo que modifica un EquipoPropio
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void updateCE() throws SQLException{
        epdao.updateCE();
    }
    
    /**
     * M�todo que elimina un EquipoPropio
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void DeleteCE() throws SQLException{
        epdao.DeleteCE();
    }
    
    /**
     * M�todo que setea un vector que contiene todos los objetos ce la tabla
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void BuscarTodosEquiposPropios() throws SQLException{ 
        epdao.BuscarTodosEquiposPropios();
    }
    
    /**
     * M�todo que guetea todos los cescuentosequipos
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     * @return......Vector ce objetos EquipoPropio
     **/     
    public Vector getTodosEquiposPropios(){
        return epdao.getTodosEquiposPropios(); 
    }
    
    /**
     * Metodo: searchEquipo, permite buscar los equipos propios
     * @autor : Ing. Jose de la rosa
     * @param : una placa.
     * @version : 1.0
     */    
    public void searchEquipo(String placa)throws SQLException{
        epdao.searchEquipo(placa);
    }
    
}
