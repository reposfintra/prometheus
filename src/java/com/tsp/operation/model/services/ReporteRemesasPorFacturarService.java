/******************************************************************************
 * Nombre clase :      ReporteRemesasPorFacturarService.java                  *
 * Descripcion :       Service del ReporteRemesasPorFacturarService.java      *
 * Autor :             LREALES                                                *
 * Fecha :             17 de julio de 2006, 3:54 PM                           *
 * Version :           1.0                                                    *
 * Copyright :         Fintravalores S.A.                                *
 ******************************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;

public class ReporteRemesasPorFacturarService {
    
    private ReporteRemesasPorFacturarDAO reporte;
    
    /** Creates a new instance of ReporteRemesasPorFacturarService */
    public ReporteRemesasPorFacturarService () {
        
        reporte = new ReporteRemesasPorFacturarDAO ();
        
    }
    
    /** Funcion publica que obtiene el metodo getRemesa del DAO */
    public Vector getVectorRemesa () throws Exception {
        
        return reporte.getRemesa();
        
    }
    
    /** Funcion publica que obtiene el metodo RemesasPorFacturar del DAO */
    public void RemesasPorFacturar ( String distrito, String fechaini, String fechafin, String agencia_duenia, String agencia_facturadora, String anual, String anio ) throws Exception {
        //boolean todas, 
        // MODIFICADO 22 NOVIEMBRE 2006 - LREALES
        reporte.RemesasPorFacturar ( distrito, fechaini, fechafin, agencia_duenia, agencia_facturadora, anual, anio );
        //
        //todas, 
    }
    
    /**
     * Carga un listado desde el a�o actual hasta 1999
     */
    public String getComboAnual(){
        int yy = Util.AnoActual();
        String combo = "";
        
        for( int i = yy; i>= 1999; i--){
            combo += "\n<option value = '" + i + "'>" + i + "</option>";
        }
        
        return combo;
    }
        
}