/*
 * Nombre        UbicacionVehicularPropietarioService.java
 * Descripción   Clase que maneja los servicios del reporte 
 *               Ubicacion Vehicular Propietario
 * Autor         Ivan Dario Gomez Vanegas
 * Fecha         31 de enero de 2006, 9:55
 * Version       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.services;
import java.util.*;
import com.tsp.operation.model.DAOS.*;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Hashtable;
/**
 *
 * @author  Ivan Dario Gomez Vanegas
 */
public class UbicacionVehicularPropietarioService {
    private  UbicacionVehicularPropietarioDAO UbicaVehicuPropDataAccess;
    private  List ListaPlacas;
    private  List ListaPropietario;
    private  String NitPropietario;
    
    /** Creates a new instance of UbicacionVehicularPropietarioService */
    public UbicacionVehicularPropietarioService() {
      UbicaVehicuPropDataAccess = new UbicacionVehicularPropietarioDAO(); 
      ListaPlacas               = new LinkedList();
      NitPropietario            = "";
    }
    
    
   /**
   * Metodo   : BuscarPlacas, retorna una lista de placas del propirtario.
   * @autor   : Ing. Ivan Dario Gomez Vanegas
   * @param   : los nit del propietario
   * @see     : BuscarPlacas - UbicacionVehicularPropietarioDAO 
   * @version : 1.0
   */
    public void BuscarPlacas(String nitPro) throws Exception {
         try{
              this.ListaPlacas = UbicaVehicuPropDataAccess.BuscarPlacas(nitPro); 
         }catch(Exception e){
              throw new Exception("Error en BuscarPlacas [UbicacionVehicularPropietarioService]...\n"+e.getMessage());
         }
    }
    
    
     /**
   * Metodo   : BuscarPlacasADMIN, retorna una lista de placas Para un Administrador.
   * @autor   : Ing. Ivan Dario Gomez Vanegas
   * @param   : 
   * @see     : BuscarPlacasADMIN - UbicacionVehicularPropietarioDAO 
   * @version : 1.0
   */
    public void BuscarPlacasADMIN( ) throws Exception {
         try{
              this.ListaPlacas = UbicaVehicuPropDataAccess.BuscarPlacasADMIN(); 
         }catch(Exception e){
              throw new Exception("Error en BuscarPlacas [UbicacionVehicularPropietarioService]...\n"+e.getMessage());
         }
    }
     /**
   * Metodo   : BuscarPropietarios, retorna una lista de propirtarios.
   * @autor   : Ing. Ivan Dario Gomez Vanegas
   * @param   : 
   * @see     : BuscarPropietarios - UbicacionVehicularPropietarioDAO 
   * @version : 1.0
   */
    public void BuscarPropietarios( ) throws Exception {
         try{
              this.ListaPropietario = UbicaVehicuPropDataAccess.BuscarPropietarios(); 
         }catch(Exception e){
              throw new Exception("Error en BuscarPropietarios [UbicacionVehicularPropietarioService]...\n"+e.getMessage());
         }
    }
    
      /**
   * Metodo   : BuscarNits, retorna una lista con los nits del propietario.
   * @autor   : Ing. Ivan Dario Gomez Vanegas
   * @param   : el id del usuario
   * @see     : BuscarPropietarios - UbicacionVehicularPropietarioDAO 
   * @version : 1.0
   */
    public void BuscarNits(String idUsuario ) throws Exception {
         try{
              this.ListaPropietario = UbicaVehicuPropDataAccess.BuscarNits(idUsuario);  
         }catch(Exception e){
              throw new Exception("Error en BuscarPropietarios [UbicacionVehicularPropietarioService]...\n"+e.getMessage());
         }
    }
    
        /**
   * Metodo   : BuscarNits, retorna una lista con los nits del propietario.
   * @autor   : Ing. Ivan Dario Gomez Vanegas
   * @param   : el id del usuario
   * @see     : BuscarPropietarios - UbicacionVehicularPropietarioDAO 
   * @version : 1.0
   */
    public void BuscarNitsPropietario(String idUsuario ) throws Exception {
         try{
              this.NitPropietario = UbicaVehicuPropDataAccess.BuscarNitsPropietario(idUsuario);  
         }catch(Exception e){
              throw new Exception("Error en BuscarPropietarios [UbicacionVehicularPropietarioService]...\n"+e.getMessage());
         }
    }
    
     public void UpdateNit(String Nits,String idUsuario) throws Exception{
          try{
               UbicaVehicuPropDataAccess.UpdateNit(Nits,idUsuario);  
         }catch(Exception e){
              throw new Exception("Error en UpdateNit [UbicacionVehicularPropietarioService]...\n"+e.getMessage());
         }
     }
     
    /**
   * Metodo   : buscarDatosReporte, Metodo que busca los datos del reporte.
   * @autor   : Ing. Ivan Dario Gomez Vanegas
   * @param   : fecha inicial, fecha final, placa, tipo de viaje, estado del viaje y el usuario
   * @see     : buscarDatosReporte - UbicacionVehicularPropietarioDAO 
   * @version : 1.0
   */
     public void buscarDatosReporte(String Fecini, String Fecfin, String[]Placas,String listaTipoViaje, String estadoViajes, String User )throws SQLException {
          try{ 
               String placas = "";
               for (int i =0; i<Placas.length;i++){
                  placas += "'"+Placas[i].trim()+"'";
                  if (i + 1<Placas.length){
                      placas +=","; 
                  }
               }
               
               
               UbicaVehicuPropDataAccess.buscarDatosReporte(Fecini,Fecfin,placas,listaTipoViaje,estadoViajes,User ); 
         }catch(Exception e){
              throw new SQLException("Error en buscarDatosReporte [UbicacionVehicularPropietarioService]...\n"+e.getMessage());
         }
     }
      /**
   * Metodo   : buscarDatosReportePlaca, Metodo que busca los datos del reporte con una sola placa.
   * @autor   : Ing. Ivan Dario Gomez Vanegas
   * @param   : fecha inicial, fecha final, placa, tipo de viaje, estado del viaje y el usuario
   * @see     : buscarDatosReporte - UbicacionVehicularPropietarioDAO 
   * @version : 1.0
   */
     public void buscarDatosReportePlaca(String Fecini, String Fecfin, String Placa,String listaTipoViaje, String estadoViajes, String User )throws SQLException {
          try{ 
               String PlacaNew = "'";
               PlacaNew += Placa;
               PlacaNew += "'";
               
               UbicaVehicuPropDataAccess.buscarDatosReporte(Fecini,Fecfin,PlacaNew,listaTipoViaje,estadoViajes,User ); 
         }catch(Exception e){
              throw new SQLException("Error en buscarDatosReporte [UbicacionVehicularPropietarioService]...\n"+e.getMessage());
         }
     }
    /**
     * Devuelve un array de tipo String [] donde se encuentran almacenados los nombres
     * de los campos del reporte de información al cliente.
     * @return El array con los nombres de los campos del reporte de ubicación vehicular.
     */
    public String [] obtenerCamposDeReporte(){
        return UbicaVehicuPropDataAccess.obtenerCamposDeReporte();
    }
    
    /**
     * Este método permite obtener los titulos del encabezado del reporte de
     * ubicación. El Hashtable que es devuelto contiene los titulos de todos
     * los campos del reporte y la clave de cada titulo es su respectivo nombre
     * de campo en la Base de datos, para obtener los nombre de los campos del
     * reporte utilize el metodo <CODE>String [] obtenerCamposDeReporte()</CODE>.
     * @throws SQLException Si algun problema ocurre en el acceso a la Base de Datos.
     * @return Una tabla con los titulos de los campos del reporte de ubicación vehicular.
     */
    public Hashtable obtenerTitulosDeReporte() throws SQLException{
        return UbicaVehicuPropDataAccess.obtenerTitulosDeReporte();
    }
   
     /**
     * retorna los datos del reporte ubicacion Vehicular Propietario
     * @see     : obtenerDatosReporte - UbicacionVehicularPropietarioDAO
     * @return El array con los nombres de los campos del reporte ubicacion vehicular Propietario.
     */
    public LinkedList obtenerDatosReporte(){
        return UbicaVehicuPropDataAccess.obtenerDatosReporte();
    } 

    public List getListaPlacas(){return this.ListaPlacas; }
    public List getListaPropietario(){return this.ListaPropietario; }
    public String getNitPropietario(){return this.NitPropietario;}
    
    
    /**
     * Busca la información financiera del viaje.
     * @param numpla La planilla a la cual se le buscará la información financiera.
     * @throws SQLException Si algun error ocurre en la base de datos
     * @autor Alejandro Payares
     */    
    public void buscarInformacionFinanciera(String numpla) throws SQLException {
        UbicaVehicuPropDataAccess.buscarInformacionFinanciera(numpla);
    }
    
    /**
     * Devuelve la información financiera buscada por el método buscarInformacionFinanaciera
     * @return EL vector que contiene las filas con la información de cada factura o pagos realizados
     * para ese viaje.
     * @autor Alejandro Payares
     */    
    public Vector obtenerInformacionFinanciera(){
        return UbicaVehicuPropDataAccess.obtenerInformacionFinanciera();
    }
    
    /**
     * Busca la la fecha y el nombre del conductor de la planilla dada
     * @param numpla el numero de la planilla
     * @throws SQLException Si algun error ocurre en la base de datos
     * @autor Alejandro Payares
     */    
    public void buscarInfoPlanilla(String numpla) throws SQLException {
        UbicaVehicuPropDataAccess.buscarInfoPlanilla(numpla);
    }
    
    /**
     * Devuelve la información buscada por el método buscarInfoPlanilla
     * @return Un Hashtable que contiene la fecha, fecha de despacho y nombre del conductor de la planilla
     * @autor Alejandro Payares
     */    
    public Hashtable getInfoPlanilla(){
        return UbicaVehicuPropDataAccess.getInfoPlanilla();
    }
    
    /**
     * Separa por miles un numero tipo double
     * @param valor El valor a formatear
     * @return El valor formateado.
     */    
    public String separarPorMiles(double valor) throws Exception{
        return UbicaVehicuPropDataAccess.separarPorMiles(valor);
    }
}
