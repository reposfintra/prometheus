/*
 * Tipo_sancionService.java
 *
 * Created on 20 de octubre de 2005, 01:46 PM
 */

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  EQUIPO12
 */
public class Tipo_sancionService {
     private Tipo_sancionDAO tipo_sancion;
    /** Creates a new instance of Tipo_sancionService */
    public Tipo_sancionService() {
        tipo_sancion = new Tipo_sancionDAO();
    }
    public Tipo_sancion getTipo_sancion( )throws SQLException{
        return tipo_sancion.getTipo_sancion();
    }
    
    public Vector getTipo_sanciones() {
        return tipo_sancion.getTipo_sanciones();
    }
    
    public void setTipo_sanciones(Vector tipo_sanciones) {
        tipo_sancion.setTipo_sanciones(tipo_sanciones);
    }
    public void listarTipo_sanciones() throws SQLException{
        try{
            tipo_sancion.listarTipo_sanciones();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }     
    public void listTipo_sanciones() throws SQLException{
        try{
            tipo_sancion.listTipo_sanciones();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }     
}
