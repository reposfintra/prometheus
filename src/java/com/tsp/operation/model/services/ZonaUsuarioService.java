/*
 * ZonaService.java
 *
 * Created on 13 de Julio de 2005, 09:00 AM
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  HENRY
 */
public class ZonaUsuarioService {
    
    private ZonaUsuarioDAO zonaUsuarioDao; 
    
    /** Creates a new instance of ZonaService */
    public ZonaUsuarioService() {
        zonaUsuarioDao = new ZonaUsuarioDAO(); 
    }
     
   public void insertarZonaUsuario(ZonaUsuario zona)throws SQLException {
       try{
           zonaUsuarioDao.setZonaUsuario(zona);
           zonaUsuarioDao.insertarZonaUsuario();
       }
       catch(SQLException e){
           throw new SQLException(e.getMessage());
       }
   }
   public boolean existeZonaUsuario(String cod, String nom) throws SQLException {
       return zonaUsuarioDao.existeZonaUsuario(cod,nom); 
   }         
   public boolean existeZona(String cod) throws SQLException {
       return zonaUsuarioDao.existeZona(cod); 
   }         
   public boolean existeUsuario(String nom) throws SQLException {
       return zonaUsuarioDao.existeUsuario(nom); 
   }         
   
   public List obtenerZonasUsuario( ) throws SQLException {
       List zonas = null;
       zonas = zonaUsuarioDao.obtenerZonas();
       return zonas;
   }   
   public void listarZonas () throws SQLException {
       try{
           zonaUsuarioDao.listarZonas();
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
   }
   public void zonas () throws SQLException {
       try{
           zonaUsuarioDao.zonasUsuarios();
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
   }
   public ZonaUsuario obtenerZona()throws SQLException{
       return zonaUsuarioDao.obtenerZonaUsuario();
   }   
   public Vector obtZonas()throws SQLException{
       return zonaUsuarioDao.obtZonas();
   }   
   public void buscarZonaUsuarios(String cod, String nom) throws SQLException {
       try{
           zonaUsuarioDao.buscarZonaUsuario(cod,nom);     
       }
       catch(SQLException e){
           throw new SQLException ( e.getMessage()); 
       }       
   }
   public void buscarZonaNombre(String nom) throws SQLException {
       try{
           zonaUsuarioDao.buscarZonaUsuarioNombre(nom);     
       }
       catch(SQLException e){
           throw new SQLException ( e.getMessage()); 
       }       
   }
   public void modificarZonaUsuario(ZonaUsuario zona, String nuevoNom)throws SQLException {
       try{
           zonaUsuarioDao.setZonaUsuario(zona);
           zonaUsuarioDao.modificarZonaUsuario(nuevoNom);
       }
       catch(SQLException e){
           throw new SQLException(e.getMessage());
       }
   }   
   public void eliminarZonaUsuario(ZonaUsuario zona)throws SQLException {
       try{
           zonaUsuarioDao.setZonaUsuario(zona);
           zonaUsuarioDao.eliminarZonaUsuario();
       }
       catch(SQLException e){
           throw new SQLException(e.getMessage());
       }
   }
}
