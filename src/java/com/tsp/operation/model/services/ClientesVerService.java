/*
 * creacionCompraCarteraService.java
 *
 * Created on 1 de enero de 2008, 05:58 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package com.tsp.operation.model.services;

import java.util.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.TransaccionService;
import java.sql.*;

/**
 *
 * @author NAVI
 */
public class ClientesVerService {

    ClientesVerDAO cldao;

    public ClientesVerService(){
        cldao = new ClientesVerDAO();
    }
    
    public ClientesVerService (String dataBaseName) {
        cldao = new ClientesVerDAO(dataBaseName);
    }
    
    public String insertarCl(ClienteEca cleca, String usuario) throws Exception {
        String respuesta = "";
        TransaccionService scv = new TransaccionService(this.cldao.getDatabaseName());
        scv.crearStatement();
        try {
            scv.getSt().addBatch(cldao.insertarCl(cleca, usuario));
            scv.getSt().addBatch(cldao.insertarNics(cleca.getId_cliente(), usuario, cleca.getNics()));
            scv.getSt().addBatch(cldao.insertSubcliente(cleca.getId_cliente(), cleca.getId_padre(), usuario));  //PBASSIL
            respuesta = "Cliente creado exitosamente!!";
            scv.execute();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
            respuesta = "Error en la creacion del cliente, porfavor verifique los datos";
            cleca.setId_cliente("");
            cleca.setNics(null);
        } finally {
            scv.closeAll();
        }
        return respuesta;
    }

    public String insertarOf(OfertaElca ofca, String usuario) throws Exception {
        String respuesta = "";
        TransaccionService scv = new TransaccionService(this.cldao.getDatabaseName());
        scv.crearStatement();
        try {
            scv.getSt().addBatch(cldao.insertarOf(ofca, usuario));
            respuesta = "Oferta creada exitosamente!!";
            scv.execute();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
            respuesta = "Error en la creacion de la solicitud, porfavor verifique los datos";
            //ofca.setId_oferta("");//091030
            ofca.setId_solicitud("");//091030
        } finally {
            scv.closeAll();
        }
        return respuesta;
    }

    public String insertarAcc(AccionesEca acceca, String usuario) throws Exception {
        String respuesta = "";
        TransaccionService scv = new TransaccionService(this.cldao.getDatabaseName());
        scv.crearStatement();
        try {
            scv.getSt().addBatch(cldao.insertarAcc(acceca, usuario));
            respuesta = "Accion creada exitosamente!!";
            scv.execute();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
            respuesta = "Error en la creacion de la accion, porfavor verifique los datos";
            acceca.setId_accion("");
        } finally {
            scv.closeAll();
        }
        return respuesta;
    }

    public String delAcc(AccionesEca acceca, String usuario) throws Exception {
        String respuesta = "";
        TransaccionService scv = new TransaccionService(this.cldao.getDatabaseName());
        scv.crearStatement();
        try {
            scv.getSt().addBatch(cldao.delAcc(acceca, usuario));
            respuesta = "Accion eliminada exitosamente!!";
            scv.execute();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
            respuesta = "Error en la eliminacion de la accion";
            acceca.setId_accion("");
        } finally {
            scv.closeAll();
        }
        return respuesta;
    }

    public String updateCl(ClienteEca cleca, String usuario, boolean sw) throws Exception {
        String respuesta = "";
        TransaccionService scv = new TransaccionService(this.cldao.getDatabaseName());
        scv.crearStatement();
        try {
            scv.getSt().addBatch(cldao.updateCl(cleca, usuario, sw));
            scv.getSt().addBatch(cldao.insertarNics(cleca.getId_cliente(), usuario, cleca.getNics()));
            scv.getSt().addBatch(cldao.updatePadre(cleca, usuario));
            respuesta = "Cliente actualizado exitosamente!!";
            scv.execute();
            cleca.setNics(cldao.getNics(cleca.getId_cliente()));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
            respuesta = "Error en la actualizacion del cliente, porfavor verifique los datos";
            cleca.setNics(cldao.getNics(cleca.getId_cliente()));
        } finally {
            scv.closeAll();
        }
        return respuesta;
    }

    public String updateOf(OfertaElca ofca, String usuario) throws Exception {
        String respuesta = "";
        TransaccionService scv = new TransaccionService(this.cldao.getDatabaseName());
        scv.crearStatement();
        try {
            scv.getSt().addBatch(cldao.updateOf(ofca, usuario));
            respuesta = "Oferta actualizada exitosamente!!";
            scv.execute();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
            respuesta = "Error en la actualizacion del cliente, porfavor verifique los datos";
        } finally {
            scv.closeAll();
        }
        return respuesta;
    }

    public String buscarCl(ClienteEca cleca) throws Exception {
        String respuesta = "";
        try {
            respuesta = cldao.buscarCl(cleca);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
            respuesta = "Error en la busqueda del cliente, porfavor verifique los datos";
        }
        return respuesta;
    }

    public String buscarOf(OfertaElca ofca) throws Exception {
        String respuesta = "";
        try {
            respuesta = cldao.buscarOf(ofca);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
            respuesta = "Error en la busqueda de la solicitud, porfavor verifique los datos";
        }
        return respuesta;
    }

    public ArrayList buscarAcc(AccionesEca acceca, String nitprop) throws Exception {
        ArrayList respuesta = null;
        try {
            respuesta = cldao.buscarAcc(acceca, nitprop);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
        }
        return respuesta;
    }

    public String getMails(String opc, String codigo_ejecutivo, String codigo_contratista) throws Exception {
        String respuesta = "";
        try {
            if (opc.equals("1") || opc.equals("3")) {
                respuesta = cldao.buscarMails("opav");
            }
            if (opc.equals("2") || opc.equals("3")) {
                respuesta = respuesta + cldao.buscarMails("ejecutivo_admin");
            }
            respuesta = respuesta + cldao.getMailEjecutivo(codigo_ejecutivo);
            if (!codigo_contratista.equals("")) {
                respuesta = respuesta + ";" + cldao.getMailContratista(codigo_contratista);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
        }
        return respuesta;
    }

    public String getLoginEjecutivo(String codigo_cliente) throws Exception {
        String respuesta = "";
        try {
            respuesta = cldao.getLoginEjecutivo(codigo_cliente);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
        }
        return respuesta;
    }

    public ArrayList getDatos(String param) throws Exception {
        ArrayList arl = cldao.getDatos(param);
        return arl;
    }

    public ArrayList getContratistas() throws Exception {
        ArrayList arl = cldao.getContratistas();
        return arl;
    }

    public ArrayList getEstados() throws Exception {
        ArrayList arl = cldao.getEstado();
        return arl;
    }

    public ArrayList getEjecutivos() throws Exception {
        ArrayList arl = cldao.getEjecutivos();
        return arl;
    }

    public ArrayList getPadres() throws Exception {
        ArrayList arl = cldao.getPadres();
        return arl;
    }

    public boolean isOficial(String id) throws Exception {
        return cldao.isOficial(id);
    }

    public boolean ispermitted(String perfil, String accion) throws Exception {
        return cldao.ispermitted(perfil + accion, accion);
    }

    public String getPerfil(String usuario) throws Exception {
        return cldao.getPerfil(usuario);
    }

    public String getAviso(String id_sol) throws Exception{
        return cldao.getAviso(id_sol);
    }

    public String[] getDatosMensaje(String accion) throws Exception {//091203
        return cldao.getDatosMensaje(accion);
    }
        //100204
    public String estadoAccion(String accion) {
        String estado = "";
        try{
            estado = cldao.estadoAccion(accion);
        }
        catch(Exception e){
            System.out.println("Error buscando estado de la accion: "+e.getMessage());
            e.printStackTrace();
        }
        return estado;
    }

}