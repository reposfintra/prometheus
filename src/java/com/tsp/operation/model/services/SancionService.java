/*
 * SancionService.java
 *
 * Created on 19 de octubre de 2005, 06:04 PM
 */

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  Jose
 */
public class SancionService {
        private SancionDAO sancion;
        /** Creates a new instance of SancionService */
        public SancionService() {
                this.sancion = new SancionDAO();
        }
        public Sancion getSancion( )throws SQLException{
                return sancion.getSancion();
        }
        
        public Vector getSanciones() {
                return sancion.getSanciones();
        }
        public void setSancion(Sancion dis) {
                sancion.setSancion(dis);
        }
        
        public void setSanciones(Vector Sancions) {
                sancion.setSanciones(Sancions);
        }
        public void insertarSancion(Sancion sancion) throws java.sql.SQLException{
                this.sancion.setSancion(sancion);
                this.sancion.insertarSancion();
        }
        public boolean existeSancion(String numpla, String cod_sancion )throws java.sql.SQLException{
                return this.sancion.existeSancion(numpla,cod_sancion );
        }
        public void listSancion()throws SQLException{
                sancion.listSancion();
        }
        public void searchSancion(String numpla, int cod_sancion )throws SQLException{
                sancion.searchSancion(numpla, cod_sancion );
        }
        public void searchDetalleSancion(String numpla, String tipo_sancion)throws SQLException{
                try{
                        sancion.searchDetalleSancion(numpla, tipo_sancion);
                }
                catch(SQLException e){
                        throw new SQLException(e.getMessage());
                }
        }
        public void updateSancion(Sancion sancion)throws SQLException{
                this.sancion.setSancion(sancion);
                this.sancion.updateSancion();
        }
        public void anularSancion(Sancion sancion)throws SQLException{
                this.sancion.setSancion(sancion);
                this.sancion.anularSancion();
        }
        public void listSancionConAprobacion(String fini,String ffin)throws SQLException{
                sancion.listSancionConAprobacion(fini, ffin);
        }
        public void listSancionSinAprobacion()throws SQLException{
                sancion.listSancionSinAprobacion();
        }
        public void listSancionConMigracion(String fini,String ffin)throws SQLException{
                sancion.listSancionConMigracion(fini, ffin);
        }
        public void listSancionSinMigracion()throws SQLException{
                sancion.listSancionSinMigracion();
        }
        public void autorizarSancion(Sancion sancion) throws SQLException{
                this.sancion.setSancion(sancion);
                this.sancion.autorizarSancion();
        }
}
