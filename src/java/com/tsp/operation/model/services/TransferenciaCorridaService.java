/***************************************
    * Nombre Clase ............. TransferenciaCorridaService.java
    * Descripci�n  .. . . . . .  Generamos Los Cheques de las facturas aprobada para pago dentro de una corrida.
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  07/03/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/


package com.tsp.operation.model.services;




import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.TransferenciaCorridaDAO;
import com.tsp.operation.model.DAOS.ChequeFacturasCorridaDAO;
import com.tsp.operation.model.DAOS.ChequeXFacturaDAO;
import com.tsp.operation.model.DAOS.SeriesChequesDAO;



public class TransferenciaCorridaService {
    
    private  TransferenciaCorridaDAO   DAO;
    private  ChequeFacturasCorridaDAO  DAOChequeCorrida;
    private  ChequeXFacturaDAO         DAOCheque;
    private  TasaService               svc;
    
    private  String                    corrida  = "";
    private  String                    banco    = "";
    private  String                    sucursal = "";     
    
    private  String                    distrito;
    private  String                    agencia;
    
    private  List                      listGeneral;
    private  List                      listSeleccion;
    private  List                      listMigracion;
    
    private  String                    transferencia = "";
    private  String                    bancoTR       = "";
    private  String                    sucursalTR    = "";
    private  Series                    serie;
    
    
    private  String                    monedaBanco   =  "";
    private  String                    monedaBancoTR =  "";
    private  String                    monedaLocal   =  "";
    
 // Datos cuenta cliente TSP:
    private String  NIT_TSP                    = "8020220161";
    private String  No_CUENTA_TSP              = "69226034878";
    private String  TIPO_CUENTA_TSP            = "CPAG";        // CA: AHORRO   CC: CORRIENTE
    private String  TIPO_OPERACION_CLIENTE     = "DB";    
    private String  TIPO_TRANSACCION_PROVEEDOR = "CR";
    private String  SECUENCIA_NOTA_ADICIONAL   = "01";
    private String  OBSERVACION                = "TRANSFERENCIA FINTRA";
    private String  NOMBRE                     = "FINTRA";
    private String  HEAD                       ="";
     
    
    
// FORMATOS DEFINIDOS:
    private final String  BANCO_CREDITO = "BANCOLOMBIA";
    
    private final String  BANCO_OCC = "BCO OCCIDENTE";
    
    
    
    
    private SeriesChequesDAO  serieDAO;    
    private final String CONCEPTO_SERIE = "CXP";  // Serie para cheques proveedor
   
    
    
    
    public TransferenciaCorridaService() {
        DAO              = new  TransferenciaCorridaDAO();
        DAOChequeCorrida = new  ChequeFacturasCorridaDAO();
        DAOCheque        = new  ChequeXFacturaDAO();
        serieDAO         = new  SeriesChequesDAO();
        svc              = new  TasaService();
        reset();
    }
    public TransferenciaCorridaService(String dataBaseName) {
        DAO              = new  TransferenciaCorridaDAO(dataBaseName);
        DAOChequeCorrida = new  ChequeFacturasCorridaDAO(dataBaseName);
        DAOCheque        = new  ChequeXFacturaDAO(dataBaseName);
        serieDAO         = new  SeriesChequesDAO(dataBaseName);
        svc              = new  TasaService(dataBaseName);
        reset();
    }
    
    
    
    
    
    public void reset(){
        listSeleccion  = new LinkedList();
        listGeneral    = new LinkedList();
        listMigracion  = new LinkedList();
        
        corrida        =  "";
        banco          =  "";
        sucursal       =  "";         
        transferencia  =  "";
        bancoTR        =  "";
        sucursalTR     =  ""; 
        monedaBancoTR  =  "";
        monedaBanco    =  "";
        monedaLocal    =  "";
        serie          =  null;
    }
    
    
    
    
    
    
    /**
   * M�todo que busca la serie de cheques
   * @autor.......fvillacob
   * @throws......Exception
   * @version.....1.0.
   **/ 
    public void  getSeries(String distrito,  String banco, String sucursal, String agencia, String user ) throws Exception{
         serie  = null;
         try{  
             
             // serie = this.DAOCheque.getSeries(distrito, banco, agenciaBanco); 
                serie = this.serieDAO.getSeries(distrito, banco, sucursal, CONCEPTO_SERIE ,  agencia, user );
                
         }catch(Exception e){
             throw new Exception( e.getMessage() );
         }
    }
    
    
    
    
    /**
   * M�todo que la moneda del banco
   * @autor.......fvillacob
   * @throws......Exception
   * @version.....1.0.
   **/ 
    public String  getMonedaBanco(String distrito,  String banco, String sucursal) throws Exception{
         String moneda = "";
         try{  
              moneda =  this.DAO.getMonedaBanco(distrito , banco     , sucursal      );
         }catch(Exception e){
             throw new Exception( e.getMessage() );
         }
         return moneda;
    }
    
    
    
    
    
    
   /**
   * M�todo que determina si la clase presenta dicho formato para realizar el archivo de transferencia
   * @autor.......fvillacob
   * @throws......Exception
   * @version.....1.0.
   **/ 
    public  boolean  existeFormato(String banco)throws Exception{
        boolean estado = false;
        try{
           /* if ( banco.equals( BANCO_CREDITO ) )  estado = true;
              if ( banco.equals( BANCO_OCC ) )  estado = true;*/
            estado = DAO.existeFormatoBanco(banco); //Mod TMolina 2008-08-28
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return estado;
    }
    
    
    
    /**
   * M�todo que determina si la clase presenta rutina para dicho formato para realizar el archivo de transferencia
   * @autor.......fvillacob
   * @throws......Exception
   * @version.....1.0.
   **/ 
    public  boolean  existeRutinaFormato(String banco)throws Exception{
        boolean estado = false;
        try{
           /* if ( banco.equals( BANCO_CREDITO ) )  estado = true;
              if ( banco.equals( BANCO_OCC ) )  estado = true;*/
            estado = DAO.existeFormatoBanco(banco); //Mod TMolina 2008-08-28
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return estado;
    }
    
    
    
    
    
    
    
    
    
  /**
   * M�todo que busca las facturas aprobadas para pago por transferencia, agrupadas  por: Corrida, banco, Sucursal, Proveedor
     teniendo en cuenta distrito y agencia del usuario.
   * @autor.......fvillacob
   * @throws......Exception
   * @version.....1.0.
   **/ 
   public void buscarFacturasAprobadas(String distrito, String agencia) throws Exception{
       try{
           this.distrito = distrito;
           this.agencia  = agencia;
           reset();
           int contFacturas    = 1;
           
           
           String monedaLocal  =  DAOCheque.getMonedaLocal(distrito);
           
           
     //--- Corridas:
           List listCorridas = this.DAO.getCorridasConFacturasPago(distrito, agencia);
           for(int i=0; i<listCorridas.size();i++){
                   String corrida     = (String)listCorridas.get(i);
              
             //--- Bancos:
                   List listBancos = this.DAO.getBancosConFacturasPago(distrito, corrida, agencia);
                   for(int j=0; j<listBancos.size();j++){
                           String  banco      = (String)listBancos.get(j);
                           
                     //--- Sucursales:
                           List listSucursales = this.DAO.getSucursalesBancosConFacturasPago(distrito, corrida, banco, agencia);
                           for(int k=0; k<listSucursales.size();k++){
                                   String sucursal     = (String)listSucursales.get(k); 
                                   
                             //--- Proveedores:
                                   List listProveedores = this.DAO.getBeneficiariosFacturasCorrida(distrito, corrida, banco, sucursal,  agencia );
                                   for(int l=0; l<listProveedores.size();l++){
                                            Hashtable  datoProveedor = ( Hashtable ) listProveedores.get(l);
                                            String     proveedor     = ( String    ) datoProveedor.get("nit");
                                            String     monedaBanco   = ( String    ) datoProveedor.get("moneda");
                                            
                                      //--- Facturas:
                                            List listFacturas  =  this.DAO.getFacturasAprobadasBeneficiarios(distrito, corrida, banco, sucursal, proveedor, agencia );
                                            for(int m=0; m<listFacturas.size();m++){
                                                    FacturasCheques  factura  = (FacturasCheques) listFacturas.get(m);
                                                    factura.setSelected   ( true ); 
                                                    factura.setMonedaBanco( monedaBanco  );
                                                    factura.setMonedaLocal( monedaLocal  );
                                                    
                                                    factura.setIdObject   ( contFacturas );
                                                    
                                                    listGeneral.add( factura );                                                    
                                                    contFacturas++;
                                            }
                                            
                                   }
                                   
                           }   
                           
                   }
           
           }
           
       }catch(Exception e){
           throw new Exception( " buscarFacturasAprobadas " + e.getMessage() );
       }
        
    }
   
   
   
   /**
   * M�todo que carga  de la lista general las facturas que se han seleccionados
   * @autor.......fvillacob
   * @throws......Exception
   * @version.....1.0.
   **/ 
   public void loadSeleccion()throws Exception{
       this.listSeleccion = this.listGeneral;
       /*
       this.listSeleccion = new LinkedList();
       try{
        
           if(facturas!=null)
                 for(int i=0;i<facturas.length;i++){
                     int id = Integer.parseInt(  facturas[i]  );
                     for(int j=0;j<this.listGeneral.size();j++){
                           FacturasCheques  factura  = (FacturasCheques) listGeneral.get(j);
                           if (  factura.getIdObject()== id ){
                                 listSeleccion.add(factura); 
                                 listGeneral.remove(j);
                                 break;
                           }
                     }
                 }
           
       }catch(Exception e){
           throw new Exception( " loadSeleccion " + e.getMessage() );
       }*/
       
   }
   
   
   
   
   /**
   * M�todo que permite formar los valores para los proveedores
   * @autor.......fvillacob
   * @throws......Exception
   * @version.....1.0.
   **/ 
   public String  formarGrupos(String bancoTR )throws Exception{
        String msjError = "";
        try{
            
            
            
           // FORMATO BANCO CREDITO
              if(this.existeFormato(bancoTR)){ //Mod TMolina 2008-08-28
            
            
                            if( this.listSeleccion.size()>0){
                                
                                 int     secuencia  = 1;
                                 String  fecha      = Util.getFechaActual_String(3) + Util.getFechaActual_String(5) + Util.getFechaActual_String(1).substring(2,4);
                                 this.listMigracion = new LinkedList();                 

                              // Convertir moneda
                                 //TasaService    svc  =  new  TasaService();
                                 String         hoy  = Util.getFechaActual_String(4);           

                             
                             // Codigo transferencia:
                                 String noTransferencia  = Util.getFechaActual_String(6).replaceAll("/|:| ","");
                                 this.transferencia      = noTransferencia;
                                 
                                 
                                 for(int i=0; i<this.listSeleccion.size();i++){
                                       FacturasCheques  factura  = (FacturasCheques) this.listSeleccion.get(i);  
                                       Hashtable  datosTransferencia = new Hashtable();
                                       
                                       factura.setBancoTR      ( bancoTR         );
                                       factura.setSucursalTR   ( sucursalTR      );
                                       factura.setMonedaTR     ( monedaBancoTR   );
                                       factura.setTransferencia( noTransferencia );

                                       datosTransferencia.put("banco_transfer",  factura.getBanco_transfer() );
                                       datosTransferencia.put("no_cuenta",       factura.getNo_cuenta()      );
                                       datosTransferencia.put("tipo_cuenta",     factura.getTipo_cuenta()    );
                                       datosTransferencia.put("nombre_cuenta",   factura.getNombre_cuenta()  );
                                       datosTransferencia.put("cedula_cuenta",   factura.getCedula_cuenta()  ); // Beneficiario
                                       datosTransferencia.put("codigo_banco",    factura.getCodeBancoTransf());
                                       datosTransferencia.put("transferencia",   noTransferencia             );
                                       datosTransferencia.put("proveedor",       factura.getProveedor()      ); // proveedor
                                      
                                       
                                         
                                       double    valor  = factura.getVlrTotalPagar();
                                    // Comparamos moneda banco con el de la transferencia
                                       if( !this.monedaBanco.equals( this.monedaBancoTR )   ){
                                             Tasa   obj  = svc.buscarValorTasa( monedaLocal, monedaBanco , monedaBancoTR , hoy );
                                             if( obj!=null ){
                                                   if( monedaBanco.equals("DOL") )  valor  =  Util.roundByDecimal( valor * obj.getValor_tasa(), 2);
                                                   else                             valor  =  Math.round         ( valor * obj.getValor_tasa()   );                                               
                                             }
                                        }
                                        factura.setValorTR( valor );                                          

                                        
                                        Hashtable existe = getTransferencia(datosTransferencia);
                                        
                                        if( existe==null ){  

                                              // Cheque:
                                                  datosTransferencia.put("distrito",                 this.distrito             );
                                                  datosTransferencia.put("banco",                    bancoTR                   );
                                                  datosTransferencia.put("sucursal",                 sucursalTR                );                                                  
                                                  datosTransferencia.put("nombre",                   factura.getNomProveedor() );
                                                  datosTransferencia.put("agencia",                  this.agencia              );
                                                  datosTransferencia.put("fechaCheque",              hoy                       );   
                                                  datosTransferencia.put("moneda",                   monedaBancoTR             );   
                                                  datosTransferencia.put("base",                     factura.getBase()         ); 
                                                  
                                              // Registro:
                                                  datosTransferencia.put("secuencia",                String.valueOf(secuencia)   ); 
                                                  datosTransferencia.put("secuencia_proveedor",      String.valueOf(1)           ); 
                                                  datosTransferencia.put("fecha",                    fecha                       ); 

                                              // Proveedor:
                                                  datosTransferencia.put("tipo_operacion_proveedor", TIPO_TRANSACCION_PROVEEDOR  );    
                                                  datosTransferencia.put("secuencia_nota",           SECUENCIA_NOTA_ADICIONAL    );
                                                  datosTransferencia.put("observacion",              OBSERVACION   + " PROP: " + factura.getProveedor() );

                                               // Cliente:
                                                  datosTransferencia.put("nit_cliente",               NIT_TSP         );
                                                  datosTransferencia.put("cuenta_cliente",            No_CUENTA_TSP   );
                                                  datosTransferencia.put("tipo_cuenta_cliente",       TIPO_CUENTA_TSP );
                                                  datosTransferencia.put("tipo_operacion_cliente",    TIPO_OPERACION_CLIENTE );


                                                  List listaFact = new LinkedList();
                                                  listaFact.add( factura );

                                                  datosTransferencia.put("facturas",  listaFact                   );
                                                  datosTransferencia.put("valor",     String.valueOf( valor  )    );


                                                  this.listMigracion.add( datosTransferencia );
                                                  secuencia++;

                                          }else{
                                                  List listaFact =  (List)existe.get("facturas");
                                                  listaFact.add(factura);

                                                  double  vlr    =  Double.parseDouble(   (String)existe.get("valor")  );
                                                  existe.put("valor",     String.valueOf( valor  + vlr   ) );
                                          }                      

                                      

                                 }

                            }
            
              }
            
              
              
              
              
           // EXCLUIMOS LOS NEGATIVOS:
              List  aux = new LinkedList();
              int   sec = 1;
              
           // Serie del cheque:  
              String prefijo     =  serie.getPrefijo();
              int    actual      =  serie.getLast_number();
              int    tope        =  Integer.parseInt( serie.getSerial_fished_no() );

              
              for(int i=0; i<this.listMigracion.size();i++){
                  Hashtable  tr          =  (Hashtable)listMigracion.get(i);
                  double     vlr         =  Double.parseDouble(  (String)tr.get("valor")  );
                  String     propietario =  (String)tr.get("proveedor");
                  String     nombre      =  (String)tr.get("nombre");
                  if( vlr >= 0){
                      
                     if( tope >= actual ){ 
                             
                         tr.put("cheque",     String.valueOf( prefijo + String.valueOf( actual )  )  ); 
                         tr.put("secuencia",  String.valueOf( sec )  );
                         tr.put("prefijo",    prefijo                   );
                         tr.put("actual",     String.valueOf( actual )  );
                         aux.add( tr );
                         sec++;
                         actual++;
                         
                     }else{
                          msjError += "No hay serie suficiente para imprimir cheques de transferencias por el banco " + bancoTR + "  " + sucursalTR +", es posible que no se esten pagando todos los propietarios<br>"  ;
                          break;   
                     }   
                     
                  }
                  else           
                      msjError += " El propietario " +  propietario +" "+   nombre  +" se encuentra negativo. <br>";      
                  
              }              
              this.listMigracion = aux;
              
            
        }catch(Exception e){
           throw new Exception( " formarGrupos " + e.getMessage() );
        }
        
        return msjError;
   }
   
   
   
   
   
   /**
   * M�todo que verifica si esos datos de transferencia ya existen en la lista
   * @autor.......fvillacob
   * @throws......Exception
   * @version.....1.0.
   **/ 
   private Hashtable getTransferencia(Hashtable  datoTransferencia)throws Exception{
       Hashtable  transferencia =  null;
       try{
           
           for(int i=0;i<this.listMigracion.size();i++){
               Hashtable  tra = (Hashtable) this.listMigracion.get(i);
               if(   
                   ( (String)tra.get("proveedor") ).     equals( (String)datoTransferencia.get("proveedor")       )   &&
                   ( (String)tra.get("banco_transfer") ).equals( (String)datoTransferencia.get("banco_transfer")  )   &&
                   ( (String)tra.get("no_cuenta") ).     equals( (String)datoTransferencia.get("no_cuenta")       )   &&
                   ( (String)tra.get("tipo_cuenta") ).   equals( (String)datoTransferencia.get("tipo_cuenta")     )   &&
                   ( (String)tra.get("nombre_cuenta") ). equals( (String)datoTransferencia.get("nombre_cuenta")   )   &&
                   ( (String)tra.get("cedula_cuenta") ). equals( (String)datoTransferencia.get("cedula_cuenta")   )  
               ){
                    transferencia = tra;
                    break;
               }
           }
           
       }catch(Exception e){
           throw new Exception( " getTransferencia " + e.getMessage() );
       }
       return transferencia;
   }
   
   
   
   
   
   
   
   
  /**
   * M�todo que permite formatear longitudes de los campos
   * @autor.......fvillacob
   * @throws......Exception
   * @version.....1.0.
   **/ 
   public void formatearLongitud()throws Exception{
       try{
         
           
           for(int i=0;i<this.listMigracion.size();i++){
                 Hashtable  tran = (Hashtable)this.listMigracion.get(i);
                 
                   String secuencia           =  (String)tran.get("secuencia");
                   String secuencia_pro       =  (String)tran.get("secuencia_proveedor");
                   String fecha               =  (String)tran.get("fecha");
                   
                   String nit_cuenta          =  (String)tran.get("cedula_cuenta");
                   String nombre_cuenta       =  (String)tran.get("nombre_cuenta");
                   String code_banco          =  (String)tran.get("codigo_banco");
                   if(code_banco.equals("0007")) {
                     code_banco = "005600078";
                   }
                   String tipo_cuenta         =  (String)tran.get("tipo_cuenta");
                   String numero_cuenta       =  (String)tran.get("no_cuenta");
                   String tipo_trans          =  (String)tran.get("tipo_operacion_proveedor");
                   String valor               =   String.valueOf(  (int)   Double.parseDouble( (String)tran.get("valor") )     );
                   String observacion         =  (String)tran.get("observacion");
                   String nota_adicional      =  (String)tran.get("secuencia_nota");
                   
                   String nit_cli             =  (String)tran.get("nit_cliente");
                   String no_cuenta_cli       =  (String)tran.get("cuenta_cliente");
                   String tipo_cuenta_cli     =  (String)tran.get("tipo_cuenta_cliente");
                   String tipo_operacion_cli  =  (String)tran.get("tipo_operacion_cliente");
                   String cheque              =  (String)tran.get("cheque");   
                 
                 
                 ArchivoMovimiento am = new ArchivoMovimiento();
                   
                     //am.setSecuencia                (secuencia     );
                     //am.setSecuenciaProveedor       (secuencia_pro );
                     //am.setFecha                    (fecha         );
                     am.setCedulaCuenta             (nit_cuenta    );
                     am.setNombreCuenta             (nombre_cuenta );
                     am.setCodigoBancoProveedor     (code_banco    );
                     am.setTipoCuentaProveedor      (tipo_cuenta   );
                     am.setNoCuentaProveedor        (numero_cuenta );
                     am.setTipoTransaccionProveedor (tipo_trans    );
                     am.setValor                    (valor         );
                     am.setCheque                   (cheque);
                     //am.setObservacion              (observacion   );
                     //am.setSecuenciaNotaAdicional   (nota_adicional);
                     //am.setNitCliente               (nit_cli       );
                     //am.setNoCuentaCliente          (no_cuenta_cli );
                     //am.setTipoCuentaCliente        (tipo_cuenta_cli );
                     //am.setTipoOperacionCliente     (tipo_operacion_cli );
                   
                am.formatearLongitud();
                
                tran.put("Registro", am );
           }
           
           
       }catch(Exception e){
           throw new Exception( " formatear " + e.getMessage() );
       }
   }
   
   
   
     
   
   
   
   
   
   
   
   
   
   

   
   
   /**
   * M�todo que crear el sql para actualizar los registros pagados
   * @autor.......fvillacob
   * @throws......Exception
   * @version.....1.0.
   **/
   public String updateRegistros(Hashtable  tran, String user)throws Exception{
       String sql = "";
       try{
           
      // Convertir moneda
          //TasaService    svc  =  new  TasaService();
          String         hoy  = Util.getFechaActual_String(4);  
                        
          
       // Datos de la Transferencia:
       //   for(int i=0;i<this.listMigracion.size();i++){
       //         Hashtable  tran = (Hashtable)this.listMigracion.get(i);
                
                
                
                String noTR                =  (String)tran.get("transferencia");
                String banco               =  (String)tran.get("banco_transfer");
                String sucursal            =  (String)tran.get("suc_transfer");
                String tipo_cuenta         =  (String)tran.get("tipo_cuenta");
                String numero_cuenta       =  (String)tran.get("no_cuenta");
                String nit_cuenta          =  (String)tran.get("cedula_cuenta");
                String nombre_cuenta       =  (String)tran.get("nombre_cuenta");
                double valor               =   Double.parseDouble( (String)tran.get("valor") )    ;
                double vlrLocal            =   valor;
                double tasa                =   1;
                
                String cheque              =  (String)tran.get("cheque");
                int    last                =  Integer.parseInt( (String)tran.get("actual") );
                
             // Validamos existencia del cheque:
                if( this.DAO.existeCheque( distrito, bancoTR, sucursalTR, cheque ) ){
                    String prefijo =  (String)tran.get("prefijo");                    
                    while(true){
                        last ++;
                        cheque = prefijo + last;
                        if( !this.DAO.existeCheque( distrito, bancoTR, sucursalTR, cheque ) )
                            break;
                    }
                }
                
                tran.put("cheque", cheque);
                
                
                
                String moneda              =  (String)tran.get("moneda");
             // convertimos a moneda Local
                if(! moneda.equals(this.monedaLocal) ){
                     Tasa   obj  = svc.buscarValorTasa( monedaLocal, moneda , monedaLocal , hoy );
                     if( obj!=null ){
                         tasa  =  obj.getValor_tasa();
                         if( monedaLocal.equals("DOL") )  vlrLocal  =  Util.roundByDecimal( vlrLocal * tasa, 2);
                         else                             vlrLocal  =  Math.round         ( vlrLocal * tasa   );                                               
                     }
                }
                tran.put("vlrLocal",  String.valueOf(vlrLocal )  );
                tran.put("tasa",      String.valueOf(tasa )      );                
                
                
             // Grabamos el cheque:   
                sql +=  this.DAO.insertEgreso(tran, user); 
                
            //  Actualizamos detalle egreso, cxp y corridas:
                List  facturas  = (List)tran.get("facturas");
                
                for( int j=0; j<facturas.size();j++ ){
                     FacturasCheques  factura  = (FacturasCheques)facturas.get(j);
                     factura.setItem(  Utility.rellenar( String.valueOf(j+1), 3 ) );
                     
                     factura.setValorTRLocal( factura.getValorTR() * tasa );
                     factura.setTasa( tasa );                     
                     
                     sql += this.DAO.insertEgresoDet(  cheque,  factura,  user  );                     
                     sql += this.DAO.updateCorrida  (  cheque , factura,  user  );
                     sql += this.DAO.updateCXP_DOC  (  cheque , factura,  user  );                    
                     
                }
                
                
             // Grabamos la transferencia:
                sql += this.DAO.insertTransferencia( distrito, bancoTR, sucursalTR, noTR, cheque,  banco, tipo_cuenta, numero_cuenta, nit_cuenta, nombre_cuenta, valor, user );
                
                
                
             // Actualizamos serie                
                //sql += this.DAO.updateSerie(distrito, bancoTR, sucursalTR, last + 1 , user );
                
                String id =  String.valueOf( serie.getId() );
                sql      +=  this.serieDAO.updateSerieNoSQL( id, last + 1, user );
                
                
                
                
                
          // }
           
           
          
          
           
           
       }catch(Exception e){
           e.printStackTrace();
           throw new Exception( " updateRegistros " + e.getMessage() );
       }
       return sql;
   }
   
   
   
   
   
   
   
   
   
   
   
   /**
   * M�todo devuelve los registros en la transferencias
   * @autor.......fvillacob
   * @throws......Exception
   * @version.....1.0.
   **/
   public List registrosTransferencias(String distrito, String banco, String sucursal,  String transferencia)throws Exception{
       List lista = new LinkedList();
       try{           
           lista =  this.DAO.getDetalleTransferencia(distrito, banco, sucursal, transferencia);
       }catch(Exception e){
           throw new Exception( " registrosTransferencias " + e.getMessage() );
       }
       return lista;
   }
   
   

   
   
   
   
   
   //-------------------------------------------------------------------------------------
   // SET AND GET
   //-------------------------------------------------------------------------------------
   
   
   
   /**
    * Getter for property listGeneral.
    * @return Value of property listGeneral.
    */
   public java.util.List getListGeneral() {
       return listGeneral;
   }
   
   /**
    * Setter for property listGeneral.
    * @param listGeneral New value of property listGeneral.
    */
   public void setListGeneral(java.util.List listGeneral) {
       this.listGeneral = listGeneral;
   }
   
   
   
   
   
   /**
    * Getter for property listSeleccion.
    * @return Value of property listSeleccion.
    */
   public java.util.List getListSeleccion() {
       return listSeleccion;
   }
   
   /**
    * Setter for property listSeleccion.
    * @param listSeleccion New value of property listSeleccion.
    */
   public void setListSeleccion(java.util.List listSeleccion) {
       this.listSeleccion = listSeleccion;
   }
   
   
   
   
   /**
    * Getter for property listMigracion.
    * @return Value of property listMigracion.
    */
   public java.util.List getListMigracion() {
       return listMigracion;
   }
   
   /**
    * Setter for property listMigracion.
    * @param listMigracion New value of property listMigracion.
    */
   public void setListMigracion(java.util.List listMigracion) {
       this.listMigracion = listMigracion;
   }
   
   /**
    * Getter for property transferencia.
    * @return Value of property transferencia.
    */
   public java.lang.String getTransferencia() {
       return transferencia;
   }
   
   /**
    * Setter for property transferencia.
    * @param transferencia New value of property transferencia.
    */
   public void setTransferencia(java.lang.String transferencia) {
       this.transferencia = transferencia;
   }
   
   public void buscarFacturasAprobadasFiltro(String distrito, String agencia) throws Exception{
        try{
            this.distrito = distrito;
            this.agencia  = agencia;
            reset();
            int contFacturas    = 1;
            
            
            String monedaLocal  =  DAOCheque.getMonedaLocal(distrito);
            
            //--- Bancos:
            List listBancos = this.DAO.getBancosConFacturasPago(distrito, this.corrida, agencia);
            for(int j=0; j<listBancos.size();j++){
                String  banco      = (String)listBancos.get(j);
                
                //--- Sucursales:
                List listSucursales = this.DAO.getSucursalesBancosConFacturasPago(distrito, corrida, banco, agencia);
                for(int k=0; k<listSucursales.size();k++){
                    String sucursal     = (String)listSucursales.get(k);
                    
                    //--- Proveedores:
                    List listProveedores = this.DAO.getBeneficiariosFacturasCorrida(distrito, corrida, banco, sucursal,  agencia );
                    for(int l=0; l<listProveedores.size();l++){
                        Hashtable  datoProveedor = ( Hashtable ) listProveedores.get(l);
                        String     proveedor     = ( String    ) datoProveedor.get("nit");
                        String     monedaBanco   = ( String    ) datoProveedor.get("moneda");
                        
                        //--- Facturas:
                        List listFacturas  =  this.DAO.getFacturasAprobadasBeneficiarios(distrito, corrida, banco, sucursal, proveedor, agencia );
                        for(int m=0; m<listFacturas.size();m++){
                            FacturasCheques  factura  = (FacturasCheques) listFacturas.get(m);
                            factura.setSelected   ( true );
                            factura.setMonedaBanco( monedaBanco  );
                            factura.setMonedaLocal( monedaLocal  );
                            
                            factura.setIdObject   ( contFacturas );
                            
                            listGeneral.add( factura );
                            contFacturas++;
                        }
                        
                    }
                    
                }
                
            }
                        
            
        }catch(Exception e){
            throw new Exception( " buscarFacturasAprobadas " + e.getMessage() );
        }
        
    }
   
   //Implementado por: Ing. Luis Eduardo Frieri.
   
   public void buscarFacturasAprobadasFiltroMod(String distrito, String agencia, String ban, String suc) throws Exception{
        try{
            this.distrito       = distrito;
            this.agencia        = agencia;
            int contFacturas    = 1;
            listGeneral         = new LinkedList();
            
         // Convertir moneda
            //TasaService    svc  =  new  TasaService();
            String         hoy  = Util.getFechaActual_String(4);
            
            
                    String  banco       = ban;                
                    String sucursal     = suc;
                    
               //--- MonedaLocal
                     monedaLocal   =  DAOCheque.getMonedaLocal(distrito);
                     
                     
              //--- Proveedores:
                    List listProveedores = this.DAO.getBeneficiariosFacturasCorrida(distrito, corrida, banco, sucursal,  agencia );
                    
                    
                    for(int l=0; l<listProveedores.size();l++){
                        Hashtable  datoProveedor = ( Hashtable ) listProveedores.get(l);
                        String     proveedor     = ( String    ) datoProveedor.get("nit");
                        
                        //--- Facturas:
                        List listFacturas  =  this.DAO.getFacturasAprobadasBeneficiarios(distrito, corrida, banco, sucursal, proveedor, agencia );
                        for(int m=0; m<listFacturas.size();m++){
                            FacturasCheques  factura  = (FacturasCheques) listFacturas.get(m);
                            factura.setSelected   ( true );
                            factura.setMonedaBanco( monedaBanco  );
                            factura.setMonedaLocal( monedaLocal  );                            
                            factura.setIdObject   ( contFacturas );
                            
                            double  vlr = factura.getVlr_saldo_me() ;  // Valor a pagar 
                            
                         // LLevamos a la moneda banco
                            if( !monedaBanco.equals( factura.getMoneda() ) ){
                                  Tasa   obj  = svc.buscarValorTasa(monedaLocal, factura.getMoneda() , monedaBanco , hoy );
                                  if( obj!=null ){
                                      if( monedaBanco.equals("DOL") )  vlr  =  Util.roundByDecimal( vlr * obj.getValor_tasa(), 2);
                                      else                             vlr  =  Math.round         ( vlr * obj.getValor_tasa()   );                                               
                                  }                                      
                            }
                            factura.setVlrTotalPagar( vlr );
                            
                            
                            listGeneral.add( factura );
                            contFacturas++;
                        }
                        
                    }
                        
            
        }catch(Exception e){
            throw new Exception( " buscarFacturasAprobadasMod " + e.getMessage() );
        }
        
    }
   
  
   
   
   /**
    * Getter for property corrida.
    * @return Value of property corrida.
    */
   public java.lang.String getCorrida() {
       return corrida;
   }
   
   /**
    * Setter for property corrida.
    * @param corrida New value of property corrida.
    */
   public void setCorrida(java.lang.String corrida) {
       this.corrida = corrida;
   }
   
   /**
    * Getter for property bancoTR.
    * @return Value of property bancoTR.
    */
   public java.lang.String getBancoTR() {
       return bancoTR;
   }
   
   /**
    * Setter for property bancoTR.
    * @param bancoTR New value of property bancoTR.
    */
   public void setBancoTR(java.lang.String bancoTR) {
       this.bancoTR = bancoTR;
   }
   
   /**
    * Getter for property sucursalTR.
    * @return Value of property sucursalTR.
    */
   public java.lang.String getSucursalTR() {
       return sucursalTR;
   }
   
   /**
    * Setter for property sucursalTR.
    * @param sucursalTR New value of property sucursalTR.
    */
   public void setSucursalTR(java.lang.String sucursalTR) {
       this.sucursalTR = sucursalTR;
   }
   
   /**
    * Getter for property banco.
    * @return Value of property banco.
    */
   public java.lang.String getBanco() {
       return banco;
   }
   
   /**
    * Setter for property banco.
    * @param banco New value of property banco.
    */
   public void setBanco(java.lang.String banco) {
       this.banco = banco;
   }
   
   /**
    * Getter for property sucursal.
    * @return Value of property sucursal.
    */
   public java.lang.String getSucursal() {
       return sucursal;
   }
   
   /**
    * Setter for property sucursal.
    * @param sucursal New value of property sucursal.
    */
   public void setSucursal(java.lang.String sucursal) {
       this.sucursal = sucursal;
   }
   
   /**
    * Getter for property serie.
    * @return Value of property serie.
    */
   public com.tsp.operation.model.beans.Series getSerie() {
       return serie;
   }
   
   /**
    * Setter for property serie.
    * @param serie New value of property serie.
    */
   public void setSerie(com.tsp.operation.model.beans.Series serie) {
       this.serie = serie;
   }
   
   /**
    * Getter for property monedaBanco.
    * @return Value of property monedaBanco.
    */
   public java.lang.String getMonedaBanco() {
       return monedaBanco;
   }
   
   /**
    * Setter for property monedaBanco.
    * @param monedaBanco New value of property monedaBanco.
    */
   public void setMonedaBanco(java.lang.String monedaBanco) {
       this.monedaBanco = monedaBanco;
   }
   
   /**
    * Getter for property monedaBancoTR.
    * @return Value of property monedaBancoTR.
    */
   public java.lang.String getMonedaBancoTR() {
       return monedaBancoTR;
   }
   
   /**
    * Setter for property monedaBancoTR.
    * @param monedaBancoTR New value of property monedaBancoTR.
    */
   public void setMonedaBancoTR(java.lang.String monedaBancoTR) {
       this.monedaBancoTR = monedaBancoTR;
   }
   
   /**
   * M�todo que permite formatear longitudes de los campos para la generacion del archivo para el BBVA
   * @autor.......Tmolina
   * @throws......Exception
   * @version.....1.0.
   **/ 
   public void formatearLongitudBBVA()throws Exception{     //2008-08-28
       try{
         
           
           for(int i=0;i<this.listMigracion.size();i++){
                 Hashtable  tran = (Hashtable)this.listMigracion.get(i);
               
                   ArchivoMovimiento am = new ArchivoMovimiento();
		   
                   //Tipo ID ==> Nit = 03 - Cedula = 01
		   String tipoID	      = "01";// (String)tran.get("tipodocID");     //Tipo Identificacion
                   String nit_cuenta          =  (String)tran.get("cedula_cuenta"); //Documento
                   // String clasificacion		  =  (String)tran.get("clasificacion"); //Clasificacion del Nit
                   
                   //Hay que agregar un digito de verificacion, solo sera para nit juridico, en otro caso sera '0'
                   nit_cuenta = nit_cuenta+"0";

                   /* Forma de Pago:
                                Abono en cuenta =  1
                                Abono en efectivo = 3 (No implementada)
                        */
                   String formaDePago		  = "1"; 
                   String code_banco = code_banco = (String)tran.get("codigo_banco");

                   //tipo de cuenta.Tipo de pago =1 . Ahorro (CA) = 02. Corriente (CC) = 01. Si Cod banco = 0013 => tipo cuenta = 00
                   String tipo_cuenta  = "";        
                   String numero_cuenta       =  (String)tran.get("no_cuenta");
                   
                   //numero de cuenta BBVA. Si la forma de pago = 1 y el codigo de banco = 0013. Sino se rellena de ceros. La longitud = 16.
                   String numeroCuentaBBVA = "0000000000000000";
                   //numero de cuenta Nacham. Si forma de pago = 1 && codigo de banco diferente "0013". sino se llena con ceros. Lon = 17.
                   String numeroCuentaN = "00000000000000000";
                   
                   //Codigo Oficina Pagadora. Para forma de pago = 3. Para cualquier oficina "9999".
                   String codOficinaPagadora = "0000";
                   
                   if (formaDePago.equals("1")){
                       
                       tipo_cuenta  =  (String)tran.get("tipo_cuenta");
                       if(tipo_cuenta.equals("CC")){
                       tipo_cuenta = "01";
                       }else{
                           if(tipo_cuenta.equals("CA")){
                               tipo_cuenta = "02";
                           }
                       }
                       
                       if(code_banco.equals("0013")){
                           String temp = numero_cuenta; 
                           if(numero_cuenta.length()<10){
                                    temp = am.rellenar(temp, "0", 10, "L");
                           }
                           numeroCuentaBBVA = temp.substring(0,4)+ "00" + tipo_cuenta + am.rellenar(temp.substring(4), "0", 8, "L") ;
                           tipo_cuenta = "00";
                       }else{
                           numeroCuentaN = numero_cuenta;
                       }
                   }else{
                       codOficinaPagadora = "9999";
                   }
		   
                   String [] tem               =  ((String)tran.get("valor")).split ("\\.") ;
	           String valParteEntera       =  tem[0];			   
		   String valParteDecimal      =  tem[1];		   
                   
                   String hoy                  =  Util.getFechaActual_String(8); //Formato AAAAMMDD
                   
                   String nombreBeneficiario   =  (String)tran.get("nombre_cuenta"); //Nombre del beneficiario
                   String direccion1           =   "BOGOTA";
                   String direccion2           =   "";
                   String email                =   "";
                   String concepto1            =   "Fintravalores";
                   
                   am.setTipoID(tipoID);
                   am.setDocumento(nit_cuenta);
                   am.setFormaDePago(formaDePago);
                   am.setCodBanco(code_banco);
                   am.setNoCuentaBBVA(numeroCuentaBBVA);
                   am.setTipCuenta(tipo_cuenta);
                   am.setNoCuenta(numeroCuentaN);
                   am.setValEntera(valParteEntera);
                   am.setValDecimal(valParteDecimal);
                   am.setFecha(hoy);
                   am.setCodOficinaPagadora(codOficinaPagadora);
                   am.setNomBeneficiario(nombreBeneficiario);
                   am.setDireccion1(direccion1);
                   am.setDireccion2(direccion2);
                   am.setEmail(email);
                   am.setConcepto1(concepto1);
                   
                   am.formatearLongitudBBVA();
                
                   tran.put("Registro", am );
           }
           
           
       }catch(Exception e){
           throw new Exception( "ERROR en==> formatearLongitudBBVA " + e.getMessage() );
       }
   } 
   
    public BeanGeneral getDataTransferencia(String tipo_cuenta) throws Exception {
        BeanGeneral bg = DAO.getDataTransferencia(tipo_cuenta);
        return bg;
    }
   
}
