   /***************************************
    * Nombre Clase ............. RevisionSoftwareService.java
    * Descripci�n  .. . . . . .  Sevicio para la revision de software
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  10/01/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/


package com.tsp.operation.model.services;



import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import javax.servlet.*;
import javax.servlet.http.*;



public class RevisionSoftwareService {
    
    private RevisionSoftwareDAO  RevisionSoftwareDataAccess;
    private String   equipo;
    private String   sitio;
    private List     listProgramas;
    
    // Para los combos jsp
    public  Programa programa;
    public  InventarioSoftware archivo;
    private List     listaEquipos;
    private List     listaSitios;
    public  String   SEPERADOR_COLUMN;
    public  String   SEPERADOR_ROW;
    
    
    
    public RevisionSoftwareService() {
        RevisionSoftwareDataAccess  = new RevisionSoftwareDAO();
        this.SEPERADOR_COLUMN       = this.RevisionSoftwareDataAccess.SEPARADOR;
        this.SEPERADOR_ROW          = "|";
        reset();
    }
    
    
    
    /**
     * M�todos que setea las variables
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public void reset(){
        equipo        = "";
        sitio         = "";
        archivo       =  new InventarioSoftware();
        programa      =  new Programa();
        listProgramas =  new LinkedList();
      }
    
    
    
    /**
     * M�todos que recibe los parametros enviados por el request y forma el objeto progranma
     * @autor.......fvillacob
     * @version.....1.0.     
     **/ 
    public void loadRequest(HttpServletRequest request){        
        this.equipo   =  format( request.getParameter("equipo")) ;
        this.sitio    =  format( request.getParameter("sitio") ) ;  
    }
    
    
    
    /**
     * M�todos que formatea valores
     * @autor.......fvillacob
     * @version.....1.0.     
     **/ 
    public String format(String val){
        if(val==null)
            val = "";
        return val;
    }
    
    
    
    
    /**
     * M�todos que carga los equipos y sitios
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/ 
    public void loadDatos()throws Exception{
        try{
            searchEquipos();
            searchSitios();            
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    
    
    
    
    /**
     * M�todos que busca los dif. equipos de la tabla programa
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/ 
    private void searchEquipos()throws Exception{
        this.listaEquipos = new LinkedList();
        try{
            this.listaEquipos = this.RevisionSoftwareDataAccess.searchEquipos();
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
        
    
    /**
     * M�todos que busca los dif. sitios por equipos de la tabla programa
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/ 
    private void searchSitios()throws Exception{
        this.listaSitios = new LinkedList();
        try{
            this.listaSitios = this.RevisionSoftwareDataAccess.searchSitios();
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    
    
    
    /**
     * M�todos que genera la variables js del listado de equipo
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/ 
    public String getEquiposJS()throws Exception{
        String varJs="";
        try{
            if(this.listaEquipos.size()>0){
               varJs = " var equipos ='"; 
               for(int i=0; i<this.listaEquipos.size();i++){
                  String eq = (String)this.listaEquipos.get(i);
                  if(i>0)
                      varJs += this.SEPERADOR_ROW;
                  varJs += eq;
               }
               varJs+="';";
            }            
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return varJs;
    }
    
    
    
    
    /**
     * M�todos que genera la variables js del listado de sitios
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/ 
    public String getSitiosJS()throws Exception{
        String varJs="";
        try{
            if(this.listaSitios.size()>0){
               varJs = " var sitios ='"; 
               
               for(int i=0; i<this.listaSitios.size();i++){
                  String sitio = (String)this.listaSitios.get(i);
                  if(i>0)
                      varJs += this.SEPERADOR_ROW;
                  varJs += sitio;
               }
               
               varJs+="';";
            }            
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return varJs;
    }
    
    
    
    
    
    
    /**
     * M�todos que busca los dif. programas del sitio
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/ 
    public void searchPrograms()throws Exception{
        this.listProgramas = new LinkedList();
        try{
            this.listProgramas = this.RevisionSoftwareDataAccess.searchProgram(this.equipo, this.sitio);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    
    
    /**
     * M�todos que busca los dif. revisiones del programa
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/ 
    public List searchRevisionesPrograma(Programa programa)throws Exception{
        List aux = new LinkedList();
        try{
            aux = this.RevisionSoftwareDataAccess.searchRevision(programa);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return aux;
    }
    
    
    
    
    
    /**
     * M�todos que busca los dif.  files del programa
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/ 
    public List searchFilesPrograma(Programa programa)throws Exception{
        List aux = new LinkedList();
        try{
            aux = this.RevisionSoftwareDataAccess.searchFile(programa);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return aux;
    }
    
    
    
    
    
    
    /**
     * M�todos que busca los dif. revisiones del programa
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/ 
    public List searchRevisionesFile(InventarioSoftware inv)throws Exception{
        List aux = new LinkedList();
        try{
            aux = this.RevisionSoftwareDataAccess.searchRevisionFile(inv);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return aux;
    }
    
    
    
    
    
    
    
    
    
    /**
     * M�todos que guarda la revision
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/ 
    public void saveRevision(Revision rev)throws Exception{
        try{
            this.RevisionSoftwareDataAccess.saveRevision(rev);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    
    
    
    
    
    
    
    // SET:
    /**
     * M�todos que setea el programa
     * @autor.......fvillacob
     * @version.....1.0.     
     **/ 
     public void setPrograma(Programa obj){
        this.programa = obj;
     }
    
    
    
    
    
    
    
    // GET:
    /**
     * M�todos que devuelve los dif. programas del sitio
     * @autor.......fvillacob
     * @version.....1.0.     
     **/ 
    public List getPrograms(){
        return this.listProgramas;
    }
    
    
    public String getEquipo(){
        return this.equipo;
    }
    
    public String getSitios(){
        return this.sitio;
    }
    
    
    public Programa getPrograma(){
        return this.programa;
    }
    
    
    /**
     * M�todos que busca los dif. programas del sitio
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/ 
    public Programa getPrograma(String id)throws Exception{
        Programa object = new Programa();
        try{
            for(int i=0; i<this.listProgramas.size();i++){
                 Programa programa = (Programa)this.listProgramas.get(i);
                 if(programa.getPrograma().equals(id)){
                     object        = programa;
                     break;
                 }
            }
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return object;
    }
    
    
    
}
