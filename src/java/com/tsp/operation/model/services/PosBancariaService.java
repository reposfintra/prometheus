/********************************************************************
 *      Nombre Clase.................   PosBancaria.java    
 *      Descripci�n..................   Service del archivo posicion_bancaria
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   18.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  Andres
 */
public class PosBancariaService {
    
    PosBancariaDAO dao;
    
    /** Creates a new instance of PosBancariaService */
    public PosBancariaService() {
        dao = new PosBancariaDAO();
    }
    
    /**
     * Obtiene un registro del archivo posicion_bancaria.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cia C�digo del Distrito
     * @param agencia C�digo de la agencia
     * @param banco Nombre del banco
     * @param sucursal Nombre de la sucursal del banco
     * fecha Fecha 
     * @see com.tsp.operation.model.DAOS.PosBancariaDAO#obtener(String, String, String, int, int)
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void obtener(String cia, String agencia, String banco, String sucursal, String fecha) throws SQLException{
        dao.obtener(cia, agencia, banco, sucursal, fecha);
    }
    
    /**
     * Verifica la existence de un registro en el archivo posicion_bancaria.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cia C�digo del Distrito
     * @param agencia C�digo de la agencia
     * @param banco Nombre del banco
     * @param sucursal Nombre de la sucursal del banco
     * @param fecha Fecha 
     * @return El reg_status del registro si existe, de lo contrario null
     * @see com.tsp.operation.model.DAOS.PosBancariaDAO#existe(String, String, String, int, int)
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String existe(String cia, String agencia, String banco, String sucursal, String fecha) throws SQLException{
        return dao.existe(cia, agencia, banco, sucursal, fecha);
    }
    
    /**
     * Actualiza un registro en el archivo posicion_bancaria.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @see com.tsp.operation.model.DAOS.PosBancariaDAO#actualizar()
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void actualizar() throws SQLException{
        dao.actualizar();
    }
    
    /**
     * Ingresa un registro en el archivo posicion_bancaria.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @see com.tsp.operation.model.DAOS.PosBancariaDAO#ingresar()
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void ingresar() throws SQLException{
        dao.ingresar();
    }
    
    /**
     * Obtiene una lista de registros del archivo posicion_bancaria.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cia C�digo del Distrito
     * @param agencia C�digo de la agencia
     * @param banco Nombre del banco
     * @param fecha Fecha
     * @see com.tsp.operation.model.DAOS.PosBancariaDAO#listar(String, String, String)
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void listar(String cia, String agencia, String banco, String fecha) throws SQLException{
        dao.listar(cia, agencia, banco, fecha);
    }
    
    /**
     * Getter for property posbanc.
     * @see com.tsp.operation.model.DAOS.PosBancariaDAO#getPosbanc()
     * @return Value of property posbanc.
     */
    public com.tsp.operation.model.beans.PosBancaria getPosbanc() {
        return dao.getPosbanc();
    }
    
    /**
     * Setter for property posbanc.
     * @see com.tsp.operation.model.DAOS.PosBancariaDAO#setPosbanc(posbanc)
     * @param posbanc New value of property posbanc.
     */
    public void setPosbanc(com.tsp.operation.model.beans.PosBancaria posbanc) {
        dao.setPosbanc(posbanc);
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return dao.getVector();
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        dao.setVector(vector);
    }
    
    /**
     * Getter for property corridas.
     * @return Value of property corridas.
     */
    public java.util.Vector getCorridas() {
        return dao.getCorridas();
    }
    
    /**
     * Setter for property corridas.
     * @param corridas New value of property corridas.
     */
    public void setCorridas(java.util.Vector corridas) {
        dao.setCorridas(corridas);
    }
    
    /**
     * Obtiene un arreglo de las ultimas corridas generadas para un banco y una sucursal
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cia C�digo del Distrito
     * @param banco Nombre del banco
     * @param sucursal Nombre de la sucursal del banco
     * @param fecha Fecha del d�a anterior
     * @see com.tsp.operation.model.DAOS.PosBancariaDAO#corridasGeneradas(String, String, String, String)
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void corridaAnterior(String cia, String banco, String sucursal, String  fecha) throws SQLException{
        dao.corridaAnterior(cia, banco, sucursal, fecha);
    }
    
    /**
     * Obtiene ultimo registro del archivo posicion_bancaria para un banco espec�fico.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cia C�digo del Distrito
     * @param agencia C�digo de la agencia
     * @param banco Nombre del banco
     * @param sucursal Nombre de la sucursal del banco
     * @see com.tsp.operation.model.DAOS.PosBancariaDAO#posicionAnterior(String, String, String, String)
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void posicionAnterior(String cia, String agencia, String banco, String sucursal) throws SQLException{
        dao.posicionAnterior(cia, agencia, banco, sucursal);
    }
    
    /**
     * Obtiene un arreglo de las corridas generadas en un per�odo para un banco y una sucursal
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cia C�digo del Distrito
     * @param banco Nombre del banco
     * @param sucursal Nombre de la sucursal del banco
     * @param fechai Fecha inicial del periodo
     * @param fechaf Fecha final del periodo
     * @see com.tsp.operation.model.DAOS.PosBancariaDAO#corridasPeriodo(String, String, String, String, String)
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void corridaPeriodo(String cia, String banco, String sucursal, String fechai, String fechaf) throws SQLException{
        dao.corridaPeriodo(cia, banco, sucursal, fechai, fechaf);
    }
    
}
