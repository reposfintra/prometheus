/*
 * MigracionRemesasAnuladasService.java
 *
 * Created on 21 de julio de 2005, 12:27 PM
 */

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.DAOS.*;
/**
 *
 * @author  Henry
 */
public class MigracionRemesasModificacionOTService {
    
    private MigracionRemesasModificacionOTDAO dao;
    
    /** Creates a new instance of MigracionRemesasAnuladasService */
    public MigracionRemesasModificacionOTService() {
    	dao = new MigracionRemesasModificacionOTDAO();
    }
    public Vector obtenerRemesasModificacionOTs(String fecini, String fecfin) throws SQLException{    	 
    	this.dao.obtenerRemesasModificacionOTs(fecini,fecfin); 
        return dao.getRemesasModificacionOTs();        
    }    
    public String getUltimaFechaProceso() throws IOException{
        return this.dao.getUltimaFechaProceso();
    }
    public void cargarArchivoPropiedades() throws IOException{
        //this.dao.cargarPropiedades();
    }
    public void setUltimaFechaProceso(String fecha) throws IOException{
        try{
        this.dao.setUltimafechaCreacion(fecha);}catch(Exception e){}
    }
    
    /**
     * Metodo obtenerRemesasNF, lista los registros de las remesa no facturadas
     * deacuerdo a un rango de fecha
     * @param: fecha inicio, fecha fin 
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
     public Vector obtenerRemesasNF(String fecini, String fecfin) throws java.sql.SQLException{   
         return dao.obtenerRemesasNF(fecini, fecfin);
     }
}
