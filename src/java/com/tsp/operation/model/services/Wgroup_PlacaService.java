/****************************************************************************
 * Nombre clase :                 Wgroup_PlacaService.java                  *
 * Descripcion :                  Clase que maneja los Servicios            *
 *                                asignados al Model relacionados con el    *
 *                                programa de mantenimiento de la tabla     *
 *                                wgroup_placa                              *
 * Autor : Ing. Juan Manuel Escandon Perez                                  *
 * Fecha : 13 de enero de 2006, 03:45 PM                                    *
 * Version : 1.0                                                            *
 * Copyright : Fintravalores S.A.                                      *
 ****************************************************************************/
package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.util.*;
public class Wgroup_PlacaService {
    private Wgroup_PlacaDAO    WPDataAccess;
    private List               ListaWP;
    private Wgroup_Placa       Datos;
    
    
    /** Creates a new instance of Wgroup_PlacaService */
    public Wgroup_PlacaService() {
        WPDataAccess    =   new Wgroup_PlacaDAO();
        ListaWP         =   new LinkedList();
        Datos           =   new Wgroup_Placa();
    }
    
    
    /**
     * Metodo Insert, permite insertar un registro nuevo a la tabla wgroup_placa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String work_group , String placa, String usuario , String dstrct
     * @see   : Insert - Wgroup_PlacaDAO
     * @version : 1.0
     */
    public void Insert(String wgroup , String placa, String usuario, String dstrct) throws Exception {
        try{
            WPDataAccess.Insert(wgroup, placa, usuario, dstrct);
        }
        catch(Exception e){
            throw new Exception("Error en Insert [Wgroup_PlacaService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Metodo Update, permite modificar un registro en la tabla wgroup_placa dada
     * un work_group y una placa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String nwgroup , String nplaca, String wgroup , String placa
     * @see   : Update - Wgroup_PlacaDAO
     * @version : 1.0
     */
    public void Update(String nwgroup , String nplaca, String wgroup, String placa) throws Exception {
        try{
            WPDataAccess.Update(nwgroup, nplaca, wgroup, placa);
        }
        catch(Exception e){
            throw new Exception("Error en Update [Wgroup_PlacaService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Metodo Search, devuelve un objeto de tipo  Wgroup_Placa, dado
     * un work_group y una placa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String wgroup , String placa
     * @see   : Search - Wgroup_PlacaDAO
     * @version : 1.0
     */
    public void Search(String wgroup, String placa ) throws Exception {
        try{
            this.ReiniciarDato();
            this.Datos = WPDataAccess.Search(wgroup, placa);
        }
        catch(Exception e){
            throw new Exception("Error en Search [Wgroup_PlacaService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Metodo Existe, Busca si existe un registro en la tabla wgroup - placa, dado
     * un work_group y una placa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String wgroup , String placa
     * @see   : Existe - Wgroup_PlacaDAO
     * @version : 1.0
     */
    public boolean Existe(String wgroup, String placa ) throws Exception {
        try{
            return WPDataAccess.Existe(wgroup, placa);
        }
        catch(Exception e){
            throw new Exception("Error en Existe [Wgroup_PlacaService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Metodo Delete, Elimina un registro en la tabla wgroup - placa, dado
     * un work_group y una placa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String wgroup , String placa
     * @see   : Delete - Wgroup_PlacaDAO
     * @version : 1.0
     */
    public void Delete(String wgroup, String placa ) throws Exception {
        try{
            WPDataAccess.Delete(wgroup, placa);
        }
        catch(Exception e){
            throw new Exception("Error en Delete [Wgroup_PlacaService]...\n"+e.getMessage());
        }
    }
    
     /**
     * Metodo Status, Anula o activa un registro de la tabla wgroup - placa, dado
     * un work_group y una placa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String wgroup , String placa
     * @see   : Status - Wgroup_PlacaDAO
     * @version : 1.0
     */
    public void Status(String reg_status, String wgroup, String placa ) throws Exception {
        try{
            WPDataAccess.Status(reg_status, wgroup, placa);
        }
        catch(Exception e){
            throw new Exception("Error en Status [Wgroup_PlacaService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Metodo List, Lista todos los registros de la tabla wgroup - placa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @see   : List - Wgroup_PlacaDAO
     * @version : 1.0
     */
    public void List() throws Exception {
        try{
            this.ReiniciarLista();
            this.ListaWP = WPDataAccess.List();
        }
        catch(Exception e){
            throw new Exception("Error en List [Wgroup_PlacaService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Metodo ReiniciarDato, le asigna el valor null al atributo Datos,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public void ReiniciarDato(){
        this.Datos = null;
    }
    
    /**
     * Metodo ReiniciarLista, le asigna el valor null al atributo ListaWP,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public void ReiniciarLista(){
        this.ListaWP = null;
    }
    
    
    /**
     * Metodo getDato, retorna el valor del atributo Datos,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public Wgroup_Placa getDato(){
        return this.Datos;
    }
    
    /**
     * Metodo getList, retorna el valor del atributo ListaWP
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public List getList(){
        return this.ListaWP;
    }
    
    /* Metodo wGroupXPlaca, obtiene los work_group a los que pertenece una placa
     * @author : Osvaldo P�rez Ferrer
     * @param : placa, placa a la que se le buscar� sus workgroup
     * @throws : en caso de que un error en la base de datos ocurra
     */
    public List wGroupXPlaca(String placa) throws Exception{
        return WPDataAccess.wGroupXPlaca(placa);
    }
}
