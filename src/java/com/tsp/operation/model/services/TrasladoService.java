/*
 * ClienteService.java
 *
 * Created on 13 de noviembre de 2004, 9:49
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;
import com.tsp.operation.model.DAOS.*;
/**
 *
 * @author  ffernandez
 */
public class TrasladoService {
    private TreeMap TlistaCMC = null;
    private TrasladoDAO comprobantesDAO;
    private BeanGeneral comprobante;
    private Cliente clienteUnidad;
    private TreeMap treemap; 

    public TrasladoService() {
        comprobantesDAO = new TrasladoDAO();
        this.comprobante = new BeanGeneral();
    }
    public TrasladoService(String dataBaseName) {
        comprobantesDAO = new TrasladoDAO(dataBaseName);
        this.comprobante = new BeanGeneral();
    }
    
    
    /**
     * M�todo para agregar una demora al archivo de demoras. 
     * @autor Tito Andr�s Maturana
     * @param demora Instancia de la clase Demora.
     * @see com.tsp.operation.model.DAOS.DemorasDAO#agregarDemora(Demora)
     * @throws SQLException
     * @version 1.0
     */
    public void agregarTraslado_bank(BeanGeneral comprobante) throws java.sql.SQLException{
        this.comprobantesDAO.agregarTraslado_bank(comprobante);        
    }
    public void BusquedaTraslado(String fechaI,String fechaF,String banco_origen,String banco_destino )throws Exception{
        this.comprobantesDAO.BusquedaTraslado(fechaI,fechaF,banco_origen,banco_destino );
     }
    public void BusquedaTrasladoModificar(String bancoOri,String bancoDes,String sucursalOri,String sucursalDes,String valor, String fecha ) throws Exception {
        this.comprobantesDAO.BusquedaTrasladoModificar(bancoOri,bancoDes,sucursalOri,sucursalDes,valor,fecha );
     }
    public void modificarTrasladoBancario(BeanGeneral comprobante) throws Exception {
       this.comprobantesDAO.modificarTrasladoBancario(comprobante);
     }
    public String Traslado_cambio_aprobado(String aprobado) throws Exception {
        return this.comprobantesDAO.Traslado_cambio_aprobado(aprobado);
       }
    public boolean existe_traslado_aprobado(String fechaI,String fechaF,String banco_origen,String banco_destino ) throws Exception {
        return this.comprobantesDAO.existe_traslado_aprobado(fechaI,fechaF,banco_origen,banco_destino );
       }
    
    public void Buscar_cuenta_banco(String bancos, String sucursal) throws Exception {
          this.comprobantesDAO.Buscar_cuenta_banco(bancos,sucursal);
       }
    public void modificar_traslado_comprobante(String banco_origen, String  banco_destino, String sucursal_origen, String sucursal_destino,String valor, String comprobante) throws Exception {
          this.comprobantesDAO.modificar_traslado_comprobante(banco_origen,banco_destino,sucursal_origen,sucursal_destino,valor,comprobante);
       }
    
     public void Buscar_traslado_x_id(String id) throws Exception {
        this.comprobantesDAO.Buscar_traslado_x_id(id);
    }
    public void AnularTraslado(BeanGeneral comprobante) throws Exception {
        this.comprobantesDAO.AnularTraslado(comprobante);
     } 
    public void AnularComprobanteYcomprodec(String pk) throws Exception {
        this.comprobantesDAO.AnularComprobanteYcomprodec(pk);
     } 
    
    public void Traslado_buscar_serie() throws Exception {
        this.comprobantesDAO.Traslado_buscar_serie();
     }
    public void agregarComprobante(int grupo_t,String numdoc,String detalle,double totaldebito,double totalcredito, BeanGeneral traslado)throws SQLException{
        this.comprobantesDAO.agregarComprobante(grupo_t,numdoc,detalle,totaldebito,totalcredito, traslado);
    }
    public void agregarComprodect(int grupo_t,String numdoc,String detalle,double totaldebito,double totalcredito, BeanGeneral traslado)throws SQLException{
        this.comprobantesDAO.agregarComprodect(grupo_t,numdoc,detalle,totaldebito,totalcredito,traslado);
    }
    public void Incrementar_tipo_docto(int codigo) throws Exception {
        this.comprobantesDAO.Incrementar_tipo_docto(codigo);
    }
    public void modificarComrpobante(String llave,String Borigen,String Sorigen,String Bdestino,String Sdestino,double totalCredDeb) throws Exception {
          this.comprobantesDAO.modificarComrpobante(llave,Borigen,Sorigen,Bdestino,Sdestino,totalCredDeb);
       }
   
    public void modificarComprodec(String llave,String banco,String sucursal,double totalDebito,double totalCredito,String detallesOri) throws Exception {
          this.comprobantesDAO.modificarComprodec(llave,banco,sucursal,totalDebito,totalCredito,detallesOri) ;
       }
   
   public java.util.Vector getVector() {
        return comprobantesDAO.getVector(); 
    }
   public java.util.Vector getVectorSerie() {
        return comprobantesDAO.getVectorSerie(); 
    }
    public java.util.Vector getVectorBanco() {
        return comprobantesDAO.getVectorBanco();
    }
    public java.util.Vector getVectorTraslado() {
        return comprobantesDAO.getVectorTraslado();
    }   
   
    public void Buscar_DatosBanco(String Banco,String Sucursal) throws Exception {  
        comprobantesDAO.Buscar_DatosBanco(Banco,Sucursal) ;
    }
    
     public java.util.Vector getVectorDatosBanco() {
        return comprobantesDAO.getVectorDatosBanco();
    }
      
    
}
     
     
//Fily 23 Marzo 2007
