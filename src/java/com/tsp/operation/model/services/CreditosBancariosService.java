package com.tsp.operation.model.services;

import com.tsp.finanzas.contab.model.beans.Cuentas;
import com.tsp.operation.model.DAOS.BancosDAO;
import com.tsp.operation.model.DAOS.CreditosBancariosDAO;
import com.tsp.operation.model.beans.Bancos;
import com.tsp.operation.model.beans.Cmc;
import com.tsp.operation.model.beans.CreditoBancario;
import com.tsp.operation.model.beans.CreditoBancarioDetalle;
import com.tsp.operation.model.beans.CupoBanco;
import com.tsp.operation.model.beans.CupoCreditoBancario;
import com.tsp.operation.model.beans.LineaCredito;
import com.tsp.operation.model.beans.PuntosBasicosBanco;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Util;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Service para el programa de manejo de creditos bancarios
 * @author darrieta - geotech
 */
public class CreditosBancariosService {
    
    CreditosBancariosDAO dao;

    public CreditosBancariosService() {
        dao = new CreditosBancariosDAO();
    }
    public CreditosBancariosService(String dataBaseName) {
        dao = new CreditosBancariosDAO(dataBaseName);
    }
    
    /**
     * Obtiene el dtf de la semana
     * @return double con el dtf de la semana o 0 si no existe
     * @throws Exception 
     */
    public double obtenerDTFActual() throws Exception{
        return dao.obtenerDTFActual();
    }
    
    /**
     * Obtiene el dtf de la semana
     * @return double con el dtf de la semana o 0 si no existe
     * @throws Exception 
     */
    public String crearCreditoBancario(CreditoBancario credito) throws Exception{
        return dao.crearCreditoBancario(credito);
    }
    
         
    /**
     * Obtiene los puntos basicos para un banco y linea de credito
     * @throws Exception 
     */
    public double obtenerPuntosBasicos(String banco, String linea) throws Exception{
        return dao.obtenerPuntosBasicos(banco, linea);
    }
    
    /**
     * Obtiene los puntos basicos para un banco y linea de credito
     * @throws Exception 
     */
    public boolean existePuntosBasicos(String banco, String linea) throws Exception{
        return dao.existePuntosBasicos(banco, linea);
    }
    
    /**
     * Obtiene el listado de puntos basicos
     * @throws Exception 
     */
    public ArrayList<PuntosBasicosBanco> listarPuntosBasicos() throws Exception{
        return dao.listarPuntosBasicos();
    }
    
    /**
     * Inserta un registro en la tabla de puntos basicos
     * @param ptoBasico bean con los datos a ingresar
     * @param usuario usuario en sesion
     * @throws Exception 
     */
    public void insertarPuntosBasicos(PuntosBasicosBanco ptoBasico, Usuario usuario) throws Exception{
        dao.insertarPuntosBasicos(ptoBasico, usuario);
    }
        
    /**
     * Edita un registro en la tabla de puntos basicos
     * @param ptoBasico bean con los datos a ingresar
     * @param usuario usuario en sesion
     * @throws Exception 
     */
    public void editarPuntosBasicos(PuntosBasicosBanco ptoBasico, Usuario usuario) throws Exception{
        dao.editarPuntosBasicos(ptoBasico, usuario);
    }
    
    
    /**
     * Obtiene el listado de puntos basicos
     * @throws Exception 
     */
    public ArrayList<CupoBanco> listarCupos() throws Exception{
        return dao.listarCupos();
    }
    
    
    /**
     * Verifica si existe el registro de cupo para un banco y linea
     * @throws Exception 
     */
    public boolean existeCupo(String banco, String linea) throws Exception{
        return dao.existeCupo(banco, linea);
    }
    
    /**
     * Inserta un registro en la tabla de puntos basicos
     * @param cupo bean con los datos a ingresar
     * @param usuario usuario en sesion
     * @throws Exception 
     */
    public void insertarCupo(CupoBanco cupo, Usuario usuario) throws Exception{
        dao.insertarCupo(cupo, usuario);
    }
    
    /**
     * Edita un registro en la tabla de puntos basicos
     * @param cupo bean con los datos a ingresar
     * @param usuario usuario en sesion
     * @throws Exception 
     */
    public void editarCupo(CupoBanco cupo, Usuario usuario) throws Exception{
        dao.editarCupo(cupo, usuario);
    }
    
    /**
     * Calcula la tasa diaria
     * @param dtf valor del DTF
     * @param ptosBasicos valor de los puntos basicos
     * @param periodicidad numero de cuotas al a�o
     */
    public double calcularTasaDiaria(String nit_banco, double dtf, double puntosBasicos, int periodicidad,String tipo_dtf) throws Exception{
        //Se consulta el banco para obtener su base
        BancosDAO bancoDAO = new BancosDAO(this.dao.getDatabaseName());
        Bancos banco = bancoDAO.obtenerBancoXNit(nit_banco);
        
        double tasaDiaria = 0;
        //Tasa EA
        double tea = calcularTasaEA(dtf, puntosBasicos,tipo_dtf);

        //Tasa nominal
        double tn = (Math.pow(tea+1, (double)1/periodicidad) - 1) * periodicidad;
        
        if(banco.getBaseAno()==360){
            //Tasa vencida
            double tv = tn / periodicidad;

            //Tasa diaria
            double dp = 360/periodicidad; //Numero de d�as que tiene el periodo
            tasaDiaria = tv / dp;
        }else if(banco.getBaseAno()==365){
            tasaDiaria = tn / 365;
        }
        
        return tasaDiaria;
    }
    
    /**
     * Calcula la tasa E.A.
     * @param dtf valor del DTF
     * @param ptosBasicos valor de los puntos basicos
     */
    public double calcularTasaEA(double dtf, double puntosBasicos,String tipo_dtf){
        dtf = dtf/100;  double tea=0;
        puntosBasicos = puntosBasicos/100;

        if(tipo_dtf.equals("EA"))
        {
           // dtf=(  Math.pow(( (dtfTA/4)/(1-(dtfTA/4))+1),4))-1
            tea = ((dtf+puntosBasicos));

        }
        else
        {
         //Tasa trimestre vencido
         double ttv = ((dtf+puntosBasicos)/4)/(1-((dtf+puntosBasicos)/4));

        //Tasa EA
         tea = Math.pow(ttv+1, 4) - 1;

        }

        
        return tea;









    }
    
    /**
     * Calcula los intereses causados en un rango de fechas
     * @param tasaDiaria
     * @param fechaInicial
     * @param fechaFinal
     * @param capital
     * @return valor de los intereses
     */
    public double calcularInteresesCausados(String ref,double tasaCobrada,double saldo,double tasaDiaria, double tasaEA,Date fechaInicial, Date fechaFinal, double capital, int baseBanco){
        long numDias = 0;  double intereses=0;
        if(baseBanco==360)
        {
            //Se calcula en numero de dias financieros entre las 2 fechas          
             if(ref.equals("TESORERIA")) //si es tesoreria
             {
                long diff = fechaFinal.getTime() - fechaInicial.getTime();
                numDias =  diff / (1000 * 60 * 60 * 24);
             }
             else
             {
                 numDias = Util.dias360(fechaInicial, fechaFinal);
             }
        }
        else
        {
            long diff = fechaFinal.getTime() - fechaInicial.getTime();
            numDias =  diff / (1000 * 60 * 60 * 24);
        }


       /*calculal intereses **/

        if(baseBanco == 365)//si es base 365
        {
           intereses = saldo * tasaDiaria * numDias;
        }
        else
        {
            intereses = (saldo * Math.pow(1+tasaEA, (double)numDias/360)) - saldo;
            //(capital * Math.pow(1+tasaCobrada, (double)numDias/360)) - saldo;
        }
      


       return intereses;
    }
 
    /**
     * Calcula los intereses en un rango de fechas para un credito factoring
     * @param tasaCobrada tasa cobrada por el banco
     * @param fechaInicial fecha inicial
     * @param fechaFinal fecha final
     * @param saldo saldo a la fecha
     * @return valor de los intereses
     */
    public double calcularInteresesFactoring(double tasaCobrada, Date fechaInicial, Date fechaFinal, double saldo){
        long diff = fechaFinal.getTime() - fechaInicial.getTime();
        long numDias =  diff / (1000 * 60 * 60 * 24);
        double intereses = (saldo * Math.pow(1+tasaCobrada, (double)numDias/360)) - saldo;
        return intereses;
    }
    
        
    /**
     * Consulta los datos para realizar la causacion de los intereses
     * @throws Exception
     */
    public ArrayList<CreditoBancario> consultarDatosCausacion() throws Exception {
        ArrayList<CreditoBancario> creditos = dao.consultarDatosCausacion();
        //Se realiza el calculo de los intereses para cada credito
        for (int i = 0; i < creditos.size(); i++) {
            CreditoBancarioDetalle detalle = creditos.get(i).getDetalles().get(0);
            Date fechaInicial = Util.ConvertiraDate1(detalle.getFecha_inicial());
            //String fechaFin =Util.fechaMasDias(detalle.getFecha_final());
            System.out.println("ffin "+detalle.getFecha_final());
            String fecha = "";
            if (creditos.get(i).getPeriodicidad() == 0) {
                fecha = detalle.getFecha_inicial();
            } else if (creditos.get(i).getPeriodicidad() == 12) {
                if(detalle.getFecha_final()!=null)
                    fecha = Util.fechaMasDias(detalle.getFecha_final(), -30);
                else
                    fecha = detalle.getFecha_inicial();
            } else if (creditos.get(i).getPeriodicidad() == 6) {
                if(detalle.getFecha_final()!=null)
                    fecha = Util.fechaMasDias(detalle.getFecha_final(), -180);
                else
                    fecha = detalle.getFecha_inicial();
            } else if (creditos.get(i).getPeriodicidad() == 4) {
                if(detalle.getFecha_final()!=null)
                    fecha = Util.fechaMasDias(detalle.getFecha_final(), -90);
                else
                    fecha = detalle.getFecha_inicial();
            }
            System.out.println("fecha "+fecha);
            String dtf = dao.obtenerDTFCuota(fecha);
            if(dtf.equals("")){
                fecha = Util.fechaMasDias(fecha, 1);
                dtf = dao.obtenerDTFCuota(fecha);
            }
            detalle.setDtf(Double.parseDouble(dtf));

                if (creditos.get(i).getPeriodicidad() == 0) {//Si es factoring
                    double intereses = calcularInteresesFactoring(creditos.get(i).getTasa_cobrada() / 100, fechaInicial, new Date(), detalle.getSaldo_inicial());
                    detalle.setIntereses(Util.redondear(intereses, 0));
                } else {
                    double tdiaria = calcularTasaDiaria(creditos.get(i).getNit_banco(), detalle.getDtf(), creditos.get(i).getPuntos_basicos(), creditos.get(i).getPeriodicidad(), creditos.get(i).getTipo_dtf());
                    detalle.setTasaEA(calcularTasaEA(detalle.getDtf(), creditos.get(i).getPuntos_basicos(), creditos.get(i).getTipo_dtf()));
                    detalle.setTasaDiaria(tdiaria);
                    double intereses = calcularInteresesCausados(creditos.get(i).getRef_credito(), creditos.get(i).getTasa_cobrada() / 100, detalle.getSaldo_inicial(), tdiaria, detalle.getTasaEA(), fechaInicial, new Date(), detalle.getCapital_inicial(), creditos.get(i).getBaseAno());
                    detalle.setIntereses(Util.redondear(intereses, 0));

                }
            }
        return creditos;
    }
    
    /**
     * Obtiene el dtf de la semana
     * @return double con el dtf de la semana o 0 si no existe
     * @throws Exception 
     */
    public String insertarDetalleCredito(CreditoBancarioDetalle credito) throws Exception{
        return dao.insertarDetalleCredito(credito);
    }
    
    /**
     * Obtiene los creditos agrupados por banco
     * @param tipo tipo de credito N - nacional o E - extranjero
     * @param banco nit del banco
     * @param vigente false para consultar registro con saldo cero o true para saldo > 0
     * @param finicial fecha inicial del rango para buscar la fecha de inicio del credito
     * @param ffinal fecha final del rango para buscar la fecha de inicio del credito
     * @return ArrayList con los resultados obtenidos
     * @throws Exception 
     */
    public ArrayList<CreditoBancario> consultarSaldosBancos(String tipo, String banco, boolean vigente, String finicial, String ffinal) throws Exception{
        return dao.consultarSaldosBancos(tipo, banco, vigente, finicial, ffinal);
    }
    
    /**
     * Obtiene los creditos de un banco
     * @param nitBanco nit del banco a buscar
     * @param vigente false para consultar registro con saldo cero o true para saldo > 0
     * @return ArrayList con los resultados obtenidos
     * @throws Exception 
     */
    public ArrayList<CreditoBancario> consultarCreditosBanco(String nitBanco, boolean vigente) throws Exception{
        return dao.consultarCreditosBanco(nitBanco, vigente);
    }
    
    /**
     * Obtiene los datos de un credito bancario
     * @param dstrct dstrito del usuario en sesion
     * @param nitBanco nit del banco que realizo el credito
     * @param documento numero del credito
     * @return bean CreditoBancario con los datos del credito consultado o null si no existe
     * @throws Exception 
     */
    public CreditoBancario consultarCreditoBancario(String dstrct, String nitBanco, String documento) throws Exception{
        return dao.consultarCreditoBancario(dstrct, nitBanco, documento);
    }
    
    /**
     * Obtiene los movimientos de un credito bancario
     * @param dstrct dstrito del usuario en sesion
     * @param nitBanco nit del banco que realizo el credito
     * @param documento numero del credito
     * @return ArrayList con los datos del credito consultado
     * @throws Exception 
     */
    public ArrayList<CreditoBancarioDetalle> consultarDetallesCreditoBancario(String dstrct, String nitBanco, String documento) throws Exception{
        return dao.consultarDetallesCreditoBancario(dstrct, nitBanco, documento);
    }
    
    
    /**
     * Obtiene los movimientos realizados al credito por fuera del programa
     * @param dstrct dstrito del usuario en sesion
     * @param nitBanco nit del banco que realizo el credito
     * @param documento numero del credito
     * @return ArrayList con los movimientos datos del credito consultado
     * @throws Exception 
     */
    public ArrayList<CreditoBancario> consultarMovimientosExternos(String dstrct, String nitBanco, String documento, int periodicidad) throws Exception{
        ArrayList<CreditoBancario> detalles = dao.consultarEgresosExternos(dstrct, nitBanco, documento);
        if(periodicidad == 0){ //Si es factoring
            detalles.addAll(dao.consultarComprobantesExternos(dstrct, nitBanco, documento));
        }else{
            detalles.addAll(dao.consultarNDExternas(dstrct, nitBanco, documento));
        }
        return detalles;
    }
    
    /**
     * Obtiene el listado de las l�neas de cr�dito
     * @throws Exception 
     */
    public ArrayList<LineaCredito> listarLineasCredito() throws Exception{
        return dao.listarLineasCredito();
    }
    
    public void insertarLineaCredito(LineaCredito linea, Usuario usuario) throws Exception {
        dao.insertarLineaCredito(linea, usuario);
    }

    public boolean existeLineaCredito(String linea) throws Exception {
        return dao.existeLineaCredito(linea);
    }

    public void editarLineaCredito(LineaCredito linea, Usuario usuario) throws Exception {
        dao.editarLineaCredito(linea,usuario);
    }
    
    public ArrayList<CupoCreditoBancario> listarCuposCreditos() throws Exception{
        return dao.listarCuposCreditos();
    }

    public boolean existeCupoCredito(String nombre) throws Exception {
        return dao.existeCupoCredito(nombre);
    }

    public void insertarCupoCredito(CupoCreditoBancario cupo, Usuario usuario) throws Exception {
        dao.insertarCupoCredito(cupo,usuario);
    }

    public void editarCupoCredito(CupoCreditoBancario cupo, Usuario usuario) throws Exception {
        dao.editarCupoCredito(cupo,usuario);
    }
    
    public ArrayList<Cmc> listarCmc() throws Exception{
        return dao.listarCmc();
    }
    
    public ArrayList<Cuentas> listarCuentas(String cuenta) throws Exception{
        return dao.listarCuentas(cuenta);
    }
    
    public ArrayList<Cuentas> obtenerCuentas() throws Exception{
        return dao.obtenerCuentas();
    }
    
    
    public String obtenerDTFCuota(String fecha) throws Exception{
        return dao.obtenerDTFCuota(fecha);
    }

    
    
}
