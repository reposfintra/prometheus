/***************************************
 * Nombre Clase  LiquidarOCService.java
 * Descripci�n   Permite liquidar la planilla
 * Autor         FERNEL VILLACOB DIAZ
 * Fecha         21/12/2005
 * versi�n       1.0
 * Copyright     Transportes Sanchez Polo S.A.
 *******************************************/

package com.tsp.operation.model.services;


import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.Model;
import com.tsp.finanzas.contab.model.DAO.*;
import com.tsp.util.Utility;
import com.tsp.util.Util;
import java.util.*;
import java.io.*;

public class LiquidarOCService {
    
    
    LiquidarOCDAO   LiquidarOCDataAccess;
    OpDAO           opDAO;
    String          oc;
    Liquidacion     liquidacion;
    Planilla        planilla;
    OPImpuestos     IMP_RETEFUENTE;
    OPImpuestos     IMP_RETEIVA;
    OPImpuestos     IMP_RETEICA;
    boolean         enproceso;
    Vector          descuentosMIMS;
    Vector          descuentosPlaca;
    Vector          descuentosPropietario;
    Vector          FacturasCorrida;
    Vector          ocsALiquidar;
    Vector          liquidaciones;
    
    private String                      SEPARADOR  = "<BR>";
    private String                      PESO       = "PES";
    private String                      BOLIVAR    = "BOL";
    private int                         DECIMALES  = 2;
    private String                      VENEZUELA_PLACA  = "VE";
    private String                      DESCUENTOS_MIMS  = "MIMS"; 
    
    
    
    private String  GENERACIONOP        = "GRABACION";
    
    
    
    private  MovAuxiliarDAO              movDAO;
    private  ContabilizacionFacturasDAO  contabDAO;
    
    
    private  int                fec              = Integer.parseInt(Util.getFechaActual_String(8));
    private  int                fechaC           = 20070213;
    private  AdicionOPDAO       AdicionOPDataAccess;
    private  AdicionOPServices  AdicionOPDataServices;
    private  Model         model;
    
    
    
    
    public LiquidarOCService() {
        LiquidarOCDataAccess = new LiquidarOCDAO();
        liquidacion          = new Liquidacion();
        opDAO                = new OpDAO();
        AdicionOPDataAccess  = new AdicionOPDAO();
        oc                   = "";
        enproceso            = false;
        movDAO               = new MovAuxiliarDAO();
        contabDAO            = new ContabilizacionFacturasDAO();
        
    }
    
    
    
    
    
    /**
     * M�todo liquidarOC, realiza el proceso de liquidacion de OC
     * @autor fvillacob
     * @see liquidarOC - liquidarOCDAO
     * @throws Exception
     * @version 1.0.
     **/
    public void liquidarOC(String oc)throws  Exception{
        try{
            planilla  = this.LiquidarOCDataAccess.liquidar(oc);
            List listaOP = this.LiquidarOCDataAccess.getOP(oc);
            listaOP = this.recorrerLista(listaOP,DESCUENTOS_MIMS);            
            
            MarcarVisibles(listaOP);  // ivan Gomez 2006-03-24
            
            liquidacion.setFacturas(listaOP);
        }catch( Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
    
    
    
    /**
     * M�todo MarcarVisibles, realiza el proceso para marcar los movimientos visibles
     * en la jsp
     * @autor Ivan Dario Gomez Vanegas
     * @param List lista de Ops
     * @throws Exception
     * @version 1.0.
     **/
    public void MarcarVisibles(List lista){
        if (lista!=null){
            for(int i=0;i<lista.size();i++){
                OP op = (OP)lista.get(i);
                List listaItem  = op.getItem();
                excluirMovRepetidos(listaItem);
            }
        }
    }
    
    
    /**
     * M�todo buscarDistintosConcepto, metodo que retorna una lista con los distintos conceptos del item
     * @autor Ivan Dario Gomez Vanegas
     * @param List lista de Ops
     * @throws Exception
     * @version 1.0.
     **/
    public List buscarDistintosConcepto(List lista){
        List DistintoConcepto = new LinkedList();
        
        if (lista!=null ){
            for(int i=0;i<lista.size();i++){
                OPItems item = (OPItems) lista.get(i);
                boolean sw = true;
                for(int j=0;j<DistintoConcepto.size();j++){
                    OPItems item2 = (OPItems) DistintoConcepto.get(j);

                    if(item.getDescconcepto().equals(item2.getDescconcepto())){
                        sw = false ;

                        break;
                    }
                }
                if(sw){
                    DistintoConcepto.add(item);//cont +=1;
                }
            }
        }
        return DistintoConcepto;
    }
    
    
    /**
     * M�todo buscarIgualConcepto, metodo que retorna una lista con los item que son iguales al concepto
     * @autor Ivan Dario Gomez Vanegas
     * @param List lista de Ops y el concepto
     * @throws Exception
     * @version 1.0.
     **/
    public List buscarIgualConceptoCheque(List lista, String Concepto,String Cheque){
        List igualconcepto = new LinkedList();
        for(int i=0;i<lista.size();i++){
            OPItems item = (OPItems) lista.get(i);
            if(item.getDescconcepto().equals(Concepto) && item.getCheque().equals(Cheque)){
                igualconcepto.add(item);
            }
            
        }
        return igualconcepto;
    }
    
    
    
    
    public List buscarDistintosCheques(List lista, String Concepto){
        List DistintoCheques = new LinkedList();
        for(int i=0;i<lista.size();i++){
            OPItems item = (OPItems) lista.get(i);
            if(item.getDescconcepto().equals(Concepto)){
                boolean sw = true;
                for(int j=0;j<DistintoCheques.size();j++){
                    OPItems itemDis = (OPItems) DistintoCheques.get(j);
                    if(item.getCheque().equals(itemDis.getCheque())){
                        sw = false;
                        break;
                    }
                }
                if(sw){
                    DistintoCheques.add(item);
                }
            }
        }
        
        
        return DistintoCheques;
        
    }
   
   
    /**
     * M�todo para completar valores de los items
     * @throws Exception
     * @version 1.0.
     **/
    public List completarItems(OP op, List items)throws Exception{
        
        try{
            
            int cont = 2;
            for(int i=0; i<items.size();i++){
                OPItems  item = (OPItems) items.get(i);
                item.setItem    ( Utility.rellenar( String.valueOf(cont) ,3)     );
                
                String descripcion  = item.getDescconcepto();
                String tipoCta      = item.getTipo_cuenta();
                
                if ( tipoCta.equals("Q") ){
                    descripcion += " "+ op.getTrailer();
                    item.setDescconcepto( descripcion  );
                }
                
                
                if( item.getIndicador().toUpperCase().equals("P") )
                    item.setMoneda( op.getMoneda());
                
                item.setProveedor     ( op.getProveedor()        );
                item.setTipo_documento( op.getTipo_documento()   );
                item.setDocumento     ( op.getDocumento()        );
                item.setPlanilla      ( op.getOc()               );
                item.setReteFuente    ( op.getReteFuente()       );
                item.setReteIca       ( op.getReteIca()          );
                item.setReteIva       ( op.getReteIva()          );
                item.setBase          ( op.getBase()             );//sescalante
                
                cont++;
            }
            
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( e.getMessage() );
        }
        
        return items;
    }
    
    /**
     * M�todo que llama al procedimiento de jose para actualizar descuentos de equipos
     * @throws Exception
     * @version 1.0.
     **/
    public List  equipos(OP op, String procedencia)throws Exception{
        List  item = new LinkedList();
        try{
            String distrito    = op.getDstrct();
            String trailer     = op.getTrailer();
            String planilla    = op.getOc();
            String base        = op.getBase();
            String agencia     = op.getAgencia();
            String usuario     = "ADMIN";
            String monedaLocal = op.getMonedaLocal();
            
            
            
            // Pre-Liquidaci�n:
            LiquidarPlanilla  liq = new LiquidarPlanilla();
            liq.Liquidar(distrito, planilla, "MPL");
            double vlrPlanilla    = Util.roundByDecimal(liq.getValorTotalPlanilla(),2);
            
            
            // Llamada al procedimiento de Jose, el cual inserta en movpla:
            this.opDAO.deleteDescMmto(distrito, planilla);
            DescuentoEtapaService  x = new DescuentoEtapaService();
            item = x.procesoDescuento(planilla, distrito, trailer, (float)vlrPlanilla, usuario, base, agencia, monedaLocal, procedencia );
            
            
            
            // Eliminamos los registros de la tabla descuento_mantenimiento
            /*if( procedencia.equals("GRABACION") )
                this.opDAO.deleteDescMmto(distrito, planilla);*/
            
            
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        
        return item;
    }
    
    
    /**
     * M�todo getExcluirRepetidos, excluye oc repetidas en una consulta
     * @autor fvillacob
     * @see liquidarOC - liquidarOCDAO
     * @param lista lista de ocs
     * @return List
     * @throws Exception
     * @version 1.0.
     **/
    public List getExcluirRepetidos(List lista)throws Exception{
        List listOP = new LinkedList();
        try{
            
            if(lista!=null && lista.size()>0){
                for(int i=0;i<lista.size();i++){
                    OP     op  = (OP)lista.get(i);
                    String oc  = op.getOc();
                    int    sw  = 0;
                    for(int j=0; j<listOP.size(); j++){
                        OP  copyOP  = (OP)listOP.get(j);
                        if( copyOP.getOc().equals(oc) ) {
                            sw = 1;
                            break;
                        }
                    }
                    if(sw==0)
                        listOP.add(op);
                }
            }
            
        }catch( Exception e){
            throw new Exception(" getExcluirRepetidos: "+e.getMessage());
        }
        return listOP;
    }
    
    /**
     * M�todo que redondea valores
     * @autor fvillacob
     * @throws Exception
     * @version 1.0.
     **/
    public List redondear(List movimientos ) throws Exception{
        try{
            
            for(int i=0; i<movimientos.size();i++){
                OPItems itemMov = (OPItems) movimientos.get(i);
                String  moneda  = itemMov.getMoneda();
                
                if( moneda.equals(PESO)  ||  moneda.equals(BOLIVAR) ){
                    itemMov.setVlr     ( (int)Math.round(  itemMov.getVlr()    )     );
                    itemMov.setVlr_me  ( (int)Math.round(  itemMov.getVlr_me() )     );
                }else{
                    itemMov.setVlr     ( Util.roundByDecimal( itemMov.getVlr()   , DECIMALES ) );
                    itemMov.setVlr_me  ( Util.roundByDecimal( itemMov.getVlr_me(), DECIMALES ) );
                }
                
            }
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return movimientos;
    }
    
    
    /**
     * M�todo que forma la lista de mov de la oc incluyendo su primer item
     * @autor fvillacob
     * @param op objeto resultado de la busqueda de la informacion para liquidar una oc
     * @return List
     * @throws Exception
     * @version 1.0.
     **/
    public List formatMovimientos(OP  op) throws Exception{
        List lista = new LinkedList();
        
        try{
            
            // Seteamos el valor de la planilla como primer item de la lista 001
            OPItems itemOC =  op.getItemOC();            
            itemOC.setCheque( "" );//ivan Dario Gomez
          
            // Grabamos como primer item en la lista
            lista.add(itemOC);
            
            
            // Recorremos los Movimientos (demas items) para adicionarlos a la lista de resultado
            List movimientos  = op.getItem();
            if(movimientos!=null && movimientos.size()>0){
                
                for(int i=0; i<movimientos.size();i++){
                    OPItems itemMov = (OPItems) movimientos.get(i);
                    lista.add(itemMov);
                }
                
            }
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return lista;
    }
    
    
    /**
     * M�todo calcularPorcentajeItems, calcula el valor del movimiento de planilla Indicador P con respecto a la sumatoria de los movimientos Asigando V
     * @autor...fvillacob
     * @param op objeto resultado de la busqueda de la informacion para liquidar una oc
     * @return OP (orden de pago Objeto)
     * @throws..Exception
     * @version..1.0.
     **/
    public List calcularPorcentajeItems(List lista, String monedaLocal, String monedaBanco, String fecha ) throws Exception{
        try{
            
            double vlrocTotal  = 0;
            if( lista !=null  && lista.size()>0 ){
                
                // Sumatoria de los items ASIGNADOR = V
                for(int i=0;i<lista.size();i++){
                    OPItems  item  = (OPItems) lista.get(i);
                    if( item.getAsignador().toUpperCase().equals("V")){
                        vlrocTotal  += item.getVlr();
                    }
                }
                
                // Calculamos valor item indicador P  = ( sumatoria items asignador V * valor item P ) /100
                for(int i=0;i<lista.size();i++){
                    OPItems  item  = (OPItems) lista.get(i);
                    if( item.getIndicador().toUpperCase().equals("P")){
                        double  vlr      =  (vlrocTotal  * item.getVlr())/100;
                        double  me       =  opDAO.getValorMoneda( monedaLocal, vlr , monedaBanco, fecha, item.getDstrct() );
                        
                        item.setVlr   ( vlr );
                        item.setVlr_me( me  );
                    }
                }
            }
            
        }catch(Exception e){
            throw new Exception( " calcularPorcentajeItems "+ e.getMessage());
        }
        return lista;
    }
    
    
    
    
    /**
     * M�todo calcularTipoValores, calcula el valor de los items Tipo V o S
     * @autor...fvillacob
     * @param lista (lista de items), tipo (tipo de asignador)
     * @return double
     * @throws..Exception
     * @version..1.0.
     **/
    public double calcularTipoValores(List lista, String tipo) throws Exception{
        double vlrTotal  = 0;
        try{
            
            if( lista !=null  && lista.size()>0 ){
                for(int i=0;i<lista.size();i++){
                    OPItems  item  = (OPItems) lista.get(i);
                    if( item.getAsignador().toUpperCase().equals( tipo ))
                        vlrTotal  += item.getVlr();
                }
            }
            
        }catch(Exception e){
            throw new Exception( " calcularTipoValores "+ e.getMessage());
        }
        return vlrTotal;
    }
    
    
    // IMPUESTOS :
    
    /**
     * M�todo calculoImpuesto, calcula valor impuestos a los item
     * @autor...fvillacob
     * @param item
     * @return OPItems
     * @throws..Exception
     * @version..1.0.
     **/
    public OPItems calculoImpuesto( OPItems item , Hashtable datosAgencia ) throws Exception{
        try{
            
            List impuestos = new LinkedList();
            
            if(  item.getAsignador().toUpperCase().equals("V")){   //  V:valor  S: saldo
                
                if( item.getDstrct().equals( opDAO.COLOMBIA )){
                    
                    String  aplica     =  (String)datosAgencia.get("aplica"); 
                    String  pais       =  (String)datosAgencia.get("pais"); 
                    
                    
                    double  vlrImp     = 0;
                    double  vlrImp_ME  = 0;                    
                    String  moneda     =   item.getMoneda();
                                        
                    
                    
                    //RETEFUENTE...Si el proveedor (nitpro OC) es agente retefuente
                    if( item.getReteFuente().equals("S")){
                        
                        if ( pais.toUpperCase().equals("CO") ){
                            
                            vlrImp        =  ( ( item.getVlr()     *  this.IMP_RETEFUENTE.getPorcentaje() ) /100   )  * this.IMP_RETEFUENTE.getSigno();
                            vlrImp_ME     =  ( ( item.getVlr_me()  *  this.IMP_RETEFUENTE.getPorcentaje() ) /100   )  * this.IMP_RETEFUENTE.getSigno();
                            
                            // Redondeo para peso y bolivar
                            if( moneda.equals(PESO)  ||   moneda.equals(BOLIVAR) ){
                                vlrImp     =  (int)Math.round( vlrImp    );
                                vlrImp_ME  =  (int)Math.round( vlrImp_ME );
                            }
                            else{
                                vlrImp     =   Util.roundByDecimal( vlrImp,    DECIMALES  );
                                vlrImp_ME  =   Util.roundByDecimal( vlrImp_ME,  DECIMALES  );
                            }
                            
                            item.setVlrReteFuente   ( vlrImp    );
                            item.setVlrReteFuente_ME( vlrImp_ME );
                            if( vlrImp!=0 ||  vlrImp_ME!=0 )
                                impuestos.add(IMP_RETEFUENTE);
                            
                        }
                    }
                    
                    
                    //RETEICA...Si la ciudad aplica RETEICA
                    if(  aplica.toUpperCase().equals("S") || aplica.toUpperCase().equals("Y")){
                        
                        vlrImp     =  (  ( item.getVlr()     *  this.IMP_RETEICA.getPorcentaje() ) /100 )  * this.IMP_RETEICA.getSigno();
                        vlrImp_ME  =  (  ( item.getVlr_me()  *  this.IMP_RETEICA.getPorcentaje() ) /100 )  * this.IMP_RETEICA.getSigno();
                        
                        // Redondeo para peso y bolivar
                        if( moneda.equals(PESO)  ||   moneda.equals(BOLIVAR) ){
                            vlrImp     =  (int)Math.round( vlrImp    );
                            vlrImp_ME  =  (int)Math.round( vlrImp_ME );
                        }else{
                            vlrImp     =  Util.roundByDecimal( vlrImp,     DECIMALES  );
                            vlrImp_ME  =  Util.roundByDecimal( vlrImp_ME,  DECIMALES  );
                        }                        
                        
                        item.setVlrReteIca    ( vlrImp    );
                        item.setVlrReteIca_ME ( vlrImp_ME );
                        if( vlrImp!=0 ||  vlrImp_ME!=0 )
                            impuestos.add(IMP_RETEICA);                        
                    }
                    
                    
                }
                
            }
            
            //registramos los impuestos del item
            item.setImpuestos(impuestos);
            
        }catch(Exception e){
            throw new Exception( e.getMessage());
        }
        return item;
    }
    
    // NETOS:
    
    /**
     * M�todo calculoNetoLiquidacion, calcula NETO A PAGAR de la OP
     * @autor fvillacob
     * @param listItem lista de items
     * @return double
     * @throws..Exception
     * @version..1.0.
     **/
    public double calculoNetoLiquidacion(List listItem) throws Exception{
        double valor = 0;
        double val   = 0;
        double imp   = 0;
        try{
            if(listItem!=null && listItem.size()>0){
                for(int j=0;j<listItem.size();j++){
                    OPItems  item = (OPItems)listItem.get(j);
                    val    += item.getVlr();
                    imp    += item.getVlrReteFuente() + item.getVlrReteIca();
                }
            }
            valor = val + imp;
        }catch(Exception e){
            throw new Exception( e.getMessage());
        }
        return valor;
    }
    
    
    /**
     * M�todo calculoNetoMELiquidacion, calcula NETO A PAGAR de la OP moneda Extranjera
     * @autor fvillacob
     * @param listItem lista de items
     * @return double
     * @throws..Exception
     * @version..1.0.
     **/
    public double calculoNetoMELiquidacion(List listItem) throws Exception{
        double valor = 0;
        double val   = 0;
        double imp   = 0;
        try{
            if(listItem!=null && listItem.size()>0){
                for(int j=0;j<listItem.size();j++){
                    OPItems  item = (OPItems)listItem.get(j);
                    val    += item.getVlr_me();
                    imp    += item.getVlrReteFuente_ME() + item.getVlrReteIca_ME();
                }
            }
            valor = val + imp;
        }catch(Exception e){
            throw new Exception( e.getMessage());
        }
        return valor;
    }
    
    //  SET  AND  GET :
    
    
    
    /**
     * Metodo: getLiquidacion, permite retornar un objeto de registros de liquidacion.
     * @autor : fvillacob
     * @return Liquidacion
     * @see getLiquidacion - liquidarOCDAO
     * @version : 1.0
     */
    public Liquidacion getLiquidacion(){
        return this.liquidacion;
    }
    
    /**
     * Metodo: setOC, instacia la varible oc
     * @autor : fvillacob
     * @param numero de la oc (String)
     * @version : 1.0
     */
    public void   setOC(String oc){
        this.oc = oc;
    }
    
    /**
     * Metodo: getOC, obteniela el valor de la variable oc
     * @autor : fvillacob
     * @return: String
     * @version : 1.0
     */
    public String getOC() {
        return this.oc;
    }
    
    /**
     * Getter for property pla.
     * @return Value of property pla.
     */
    public com.tsp.operation.model.beans.Planilla getPla() {
        return LiquidarOCDataAccess.getPla();
    }
    
    /**
     * M�todo buscarOC, verifica la existencia de la oc
     * @autor sescalante
     * @see liquidar
     * @throws Exception
     * @version 1.0.
     **/
    public boolean buscarOC(String oc)throws  Exception{
        boolean resp = false;
        try{
            if (this.LiquidarOCDataAccess.liquidar(oc) != null ){
                resp = true;
            }
        }catch (Exception e ){
            throw new Exception( e.getMessage());
        }
        return resp;
    }
    
    /**
     * Getter for property enproceso.
     * @return Value of property enproceso.
     */
    public boolean getEnproceso() {
        return enproceso;
    }
    
    /**
     * Setter for property enproceso.
     * @param enproceso New value of property enproceso.
     */
    public void setEnproceso() {
        this.enproceso = (this.enproceso == false )?true:false;
    }
    
    
    /**
     * BUSQUEDA DE DESCUENTOS DE MIMS PARA UN PROPIETARIO
     * @autor mfontalvo
     * @param proveedor
     * @throws Exception.
     */
    public Vector obtenerAjustesPlanillaMIMS(String planilla) throws Exception{
        try{
            return  LiquidarOCDataAccess.obtenerAjustesPlanillaMIMS( planilla );
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    /**
     * BUSQUEDA DE DESCUENTOS DE MIMS PARA UN PROPIETARIO
     * @autor mfontalvo
     * @param proveedor
     * @throws Exception.
     */
    public Vector obtenerAjustesProveedorMIMS(String proveedor) throws Exception{
        try{
            return LiquidarOCDataAccess.obtenerAjustesProveedorMIMS( proveedor );
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    /**
     * Setear datos para el metodo de liquidacion
     * @return List, lista de items de op para el metodo de liquidacion
     * @throws Exception.
     */
    public List generarDescuentosPlanillasMIMS( String planilla ) throws Exception{
        List lista = null;
        try{
            Vector ajpla = obtenerAjustesPlanillaMIMS( planilla);
            if (ajpla !=null && !ajpla.isEmpty()){
                lista = new LinkedList();
                for (int i=0; i<ajpla.size();i++){
                    DescuentoMIMS desc = (DescuentoMIMS) ajpla.get(i);
                    OPItems item = new OPItems();
                    item.setDstrct       ( desc.getDistrito()     );
                    item.setDescripcion  ( desc.getCodConcepto()  );
                    item.setDescconcepto( desc.getDesConcepto()  );
                    item.setVlr          ( desc.getValor_item()   );
                    item.setAsignador    ( desc.getAsignador()    );
                    item.setIndicador    ( desc.getIndicador()    );
                    item.setMoneda       ( desc.getMoneda()       );
                    item.setCodigo_cuenta( desc.getAccount()      );
                    item.setTipo_cuenta  ( desc.getTipoCuenta()   );
                    item.setAgencia      ( desc.getOrigen() );
                    item.setCodigo_abc   ( "" );
                    item.setCheque       ( desc.getCheque()       );
                    item.setConcepto     ( desc.getCodConcepto()  );
                    lista.add(item);
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        return lista;
    }
    
    /**
     * Metodo de verifiacion de anticipos y renaticipos no impresos
     * @autor mfontalvo
     * @param planilla, numero de planilla
     * @param proveedor, nit
     * @return boolean, true si tiene anticipos no impresos, false lo contrario
     * @throws Exception.
     */
    public boolean verificarAnticipos(String planilla, String proveedor) throws Exception{
        try{
            return LiquidarOCDataAccess.verificarAnticipos(planilla, proveedor);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    
    /**
     * Getter for property descuentosMIMS.
     * @return Value of property descuentosMIMS.
     */
    public java.util.Vector getDescuentosMIMS() {
        return descuentosMIMS;
    }
    
    /**
     * Setter for property descuentosMIMS.
     * @param descuentosMIMS New value of property descuentosMIMS.
     */
    public void setDescuentosMIMS(java.util.Vector descuentosMIMS) {
        this.descuentosMIMS = descuentosMIMS;
    }
    
    /**
     * Getter for property ocsALiquidar.
     * @return Value of property ocsALiquidar.
     */
    public java.util.Vector getOcsALiquidar() {
        return ocsALiquidar;
    }
    
    /**
     * Setter for property ocsALiquidar.
     * @param ocsALiquidar New value of property ocsALiquidar.
     */
    public void setOcsALiquidar(java.util.Vector ocsALiquidar) {
        this.ocsALiquidar = ocsALiquidar;
    }
    
    /**
     * Getter for property liquidaciones.
     * @return Value of property liquidaciones.
     */
    public java.util.Vector getLiquidaciones() {
        return liquidaciones;
    }
    
    /**
     * Setter for property liquidaciones.
     * @param liquidaciones New value of property liquidaciones.
     */
    public void setLiquidaciones(java.util.Vector liquidaciones) {
        this.liquidaciones = liquidaciones;
    }
    
    /**
     * Getter for property descuentosPlaca.
     * @return Value of property descuentosPlaca.
     */
    public java.util.Vector getCorridasNegativas() {
        return FacturasCorrida;
    }
    
    /**
     * Setter for property descuentosPlaca.
     * @param descuentosPlaca New value of property descuentosPlaca.
     */
    public void setDescuentosPlaca(java.util.Vector descuentosPlaca) {
        this.descuentosPlaca = descuentosPlaca;
    }
    
    /**
     * Getter for property descuentosPropietario.
     * @return Value of property descuentosPropietario.
     */
    public java.util.Vector getDescuentosPropietario() {
        return descuentosPropietario;
    }
    
    /**
     * Setter for property descuentosPropietario.
     * @param descuentosPropietario New value of property descuentosPropietario.
     */
    public void setDescuentosPropietario(java.util.Vector descuentosPropietario) {
        this.descuentosPropietario = descuentosPropietario;
    }
    
        /**
     * Getter for property FacturasCorrida.
     * @return Value of property FacturasCorrida.
     */
    public java.util.Vector getFacturasCorrida() {
        return FacturasCorrida;
    }    
    
    /**
     * Setter for property FacturasCorrida.
     * @param FacturasCorrida New value of property FacturasCorrida.
     */
    public void setFacturasCorrida(java.util.Vector FacturasCorrida) {
        this.FacturasCorrida = FacturasCorrida;
    } 
    
     public void buscarDatosALiquidarRep(String planillas, String proveedor) throws Exception {
        try{
            ocsALiquidar = LiquidarOCDataAccess.buscarDatosALiquidarREP(planillas, proveedor);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
     
      /**
      * Metodo para obtener el benficario de de un proveedor
      * @autor mfontalvo
      * @throws Exception.
      */
     public String getBeneficiario(String nit) throws Exception {
        try{
            return  LiquidarOCDataAccess.getBeneficiario(nit);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    } 
     
     
    /**
     * M�todo excluirMovRepetidos, realiza el proceso para exculir los movimientos repetidos
     * @autor Ivan Dario Gomez Vanegas
     * @param List lista de Ops
     * @throws Exception
     * @version 1.0.
     **/
    public void excluirMovRepetidos(List lista){
        
        
        List distinctConcepto =  buscarDistintosConcepto(lista);
        for(int i=0;i<distinctConcepto.size();i++){
            OPItems item = (OPItems) distinctConcepto.get(i);            
            List DistintosCheques   = buscarDistintosCheques(lista,item.getDescconcepto());
            for(int k=0;k< DistintosCheques.size();k++){
                OPItems itemChe = (OPItems) DistintosCheques.get(k);
                List igualConceptoCheque  =  buscarIgualConceptoCheque(lista, itemChe.getDescconcepto(),itemChe.getCheque());
                double suma=0;
                double riva = 0, rica = 0, rfte = 0;
                for(int j=0;j<igualConceptoCheque.size();j++){
                    OPItems itemCon = (OPItems) igualConceptoCheque.get(j);
                    suma += itemCon.getVlr();
                    riva += itemCon.getVlrReteIva();
                    rica += itemCon.getVlrReteIca();
                    rfte += itemCon.getVlrReteFuente();
                }
                if(suma!=0){
                    itemChe.setVisible(true);
                    itemChe.setVlr          (suma);
                    itemChe.setVlrReteIva   (riva);
                    itemChe.setVlrReteIca   (rica);
                    itemChe.setVlrReteFuente(rfte);
                }
            }
        }
    }
    
    /**
     * Metodo para separar los descuentos de propietario
     * general en 2 unos de placa y otros de propietario
     * @autor mfontalvo
     */
    public void separarDescuentosPropietarios() throws Exception{
        try{
            
            descuentosPlaca       = new Vector();
            descuentosPropietario = new Vector();
            if (descuentosMIMS!=null && !descuentosMIMS.isEmpty()){
                for (int i=0 ; i<descuentosMIMS.size() ; i++){
                    DescuentoMIMS desc = (DescuentoMIMS)descuentosMIMS.get(i);
                    if (!desc.getPlaca().equals(desc.getId_mims()))
                        descuentosPlaca.add(desc);
                    else
                        descuentosPropietario.add(desc);
                }
            }
          
              
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }        
    }

    
    
    /**
     * Metodo para excluir descuento de propiertarios y de placas
     * esto depende si el descuento esta relacionado a una planilla
     * en caso de tener planilla 
     * @autor mfontalvo
     * @throws Exception.
     */
    public void filtrarDescuentosPropietarios () throws Exception{
        try{
            
            if (descuentosMIMS!=null && !descuentosMIMS.isEmpty()){
                
                Vector descuentos = this.descuentosMIMS;
                int i = 0;
                while ( i < descuentos.size()){
                    DescuentoMIMS desc = (DescuentoMIMS) descuentos.get(i);
                    boolean remover = false;
                    // si tiene planilla
                    if (!desc.getPlanilla().trim().equals("")){

                        // consulta del estado de la op
                        String estado = LiquidarOCDataAccess.obtenerEstadoOP( desc.getPlanilla() );
                        // en caso de no tener OP se verifica contra la lista de las planillas de la liquidacion.
                        if (  estado.equals("") ){
                            remover = true;

                            if (liquidaciones!=null && !liquidaciones.isEmpty())
                                for (int j=0; j<liquidaciones.size(); j++){                    
                                    Liquidacion liq = (Liquidacion) liquidaciones.get(j);
                                    if ( liq.getOC().equals(desc.getPlanilla())  ){                                    
                                        List fact = liq.getFacturas();
                                        if (fact!=null && !fact.isEmpty()){
                                            OP op = (OP) fact.get(0);
                                            if (!op.getComentario().trim().equals("")){
                                                remover = false;
                                                break;
                                            }
                                        }
                                    }
                                }
                        }
                        else if ( ! (estado.equals("40") || estado.equals("50") ) )
                            remover = true;
                    }     


                    // se retira el descuento si aplico a los criterios
                    if (remover) descuentos.remove(i);                    
                    else i++;
                }
            }
              
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    
    
    /**
     * M�todo buscar datos de planillas a liquidar
     * @autor   mfontalvo
     * @param   parametro de busqueda
     * @param   tipo, tipo de filtro
     * @param   base, indica si debe o no leer las planillas cumplidas
     * @throws  Exception
     * @version 1.0.
     **/    
    public void buscarDatosALiquidar(String parametro, int tipo, String base) throws Exception {
        try{
            ocsALiquidar = LiquidarOCDataAccess.buscarDatosALiquidar(parametro, tipo, base);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    /**
     * M�todo liquidarOC, realiza el proceso de liquidacion de OC,
     * @autor mfontalvo
     * @param procedencia , indica si debe o no tener en cuenta los descuentos de MIMS.
     * @see liquidarOC - liquidarOCDAO
     * @throws Exception
     * @version 1.0.
     **/
    public void liquidarOC(String oc, String procedencia)throws  Exception{
        try{
            liquidacion = new Liquidacion();
            planilla  = this.LiquidarOCDataAccess.liquidar(oc);
            List listaOP = this.LiquidarOCDataAccess.getOP(oc);
            listaOP = this.recorrerLista(listaOP,procedencia);

            MarcarVisibles(listaOP);  // ivan Gomez 2006-03-24

            liquidacion.setFacturas(listaOP);
        }catch( Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * BUSQUEDA DE DESCUENTOS DE MIMS PARA UN PROPIETARIO, BASANDOSE EN
     * LA INFORMACION ACTUAL DE LA LIQUIDACION Q SE ESTA EJECUTANDO
     * @autor mfontalvo
     * @throws Exception.
     * @modificado jbarros
     */
    public void obtenerAjustesProveedorMIMS() throws Exception{
        try{
            if (ocsALiquidar!=null && !ocsALiquidar.isEmpty()){
                    String nitpro = ((Planilla) ocsALiquidar.get(0)).getNitpro();
                    descuentosPropietario = this.obtenerAjustesProveedorWEB( nitpro );
                    FacturasCorrida = this.FacturasProveedorCorrida( nitpro );
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }    

    /**
     * BUSQUEDA DE DESCUENTOS DE WEB PARA UN PROPIETARIO
     * @autor jbarros
     * @param proveedor
     * @throws Exception.
     */
    public Vector FacturasProveedorCorrida(String proveedor) throws Exception{
        try{
            return LiquidarOCDataAccess.FacturasProveedorCorrida( proveedor );
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    /**
     * BUSQUEDA DE DESCUENTOS DE WEB PARA UN PROPIETARIO
     * @autor jbarros
     * @param proveedor
     * @throws Exception.
     */
    public Vector obtenerAjustesProveedorWEB(String proveedor) throws Exception{
        try{
            return LiquidarOCDataAccess.obtenerAjustesProveedorWEB( proveedor );
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    /**
     * Metodo para  liquidar las planillas 
     * segun los parametros definidos
     * @autor mfontalvo.
     * @throws Exception.
     * @Modificado JBarros
     */    
    public void LiquidacionPorBloque(Usuario usuario) throws Exception{
        try{
            if (ocsALiquidar!=null && !ocsALiquidar.isEmpty()){
                liquidaciones = new Vector();
                for (int i=0; i<ocsALiquidar.size();i++){
                    Planilla p = (Planilla) ocsALiquidar.get(i);
                    liquidacion = new Liquidacion();
                    // Averiguo si tiene op con el fin de corre el 
                    // proceso antiguo solo para las oc que no tienen op 
                    planilla  = this.LiquidarOCDataAccess.liquidar(p.getNumpla());
                    p.setReg_status(planilla.getReg_status());
                    p.setClientes  (planilla.getClientes()  );
                    p.setNumrem    (planilla.getNumrem()    );
                    p.setFecdsp    (planilla.getFecdsp()  );
                    List listaOP = this.LiquidarOCDataAccess.getOP(p.getNumpla());
                    if( listaOP != null && listaOP.size()>0 ){
                        OP op = (OP)listaOP.get(0);
                        if (!this.LiquidarOCDataAccess.tieneOP(op.getOc())){
                            liquidarOC(p.getNumpla());                    
                        }else{
                            op.setTieneOp(true);
                            AdicionOPDataAccess = new AdicionOPDAO();
                            AdicionOPDataServices= new AdicionOPServices(); 
                            OpDAO  OPDAO  = new  OpDAO();
                            OP op2 = this.LiquidarOCDataAccess.getOCMovPendientes("FINV", op.getOc());
                            if( op2.getOc()!= null ){
                                    Model  model  = new Model();
                                    //----------- Progama de fernell -------- jbarros 22/02/2007
                                    // Creamos los archivoa a generar.
                                    String file      = model.DirectorioSvc.getUrl() + usuario.getLogin() + "/FACT_ADICIONAL"  +".xls";   // Excel con facturas generadas
                                    String fileError = model.DirectorioSvc.getUrl() + usuario.getLogin() + "/FACT_ADICIONAL"  +".txt";   // Planilla que no se pudo generar facturas por Error
                                    File f = new File(model.DirectorioSvc.getUrl() + usuario.getLogin());
                                    f.mkdir();
                                    String msj="";
                                    // Validamos si la planilla tiene Discrepancia   
                                    if( OPDAO.getDiscrepancia( op2.getOc() ) )
                                         msj = " Presenta discrepancia ";
                                   // Validamos si la placa esta vetada  
                                    if(  OPDAO.getVetoPlaca(  op2.getPlaca()  )  )
                                         msj += "La placa " +  op2.getPlaca()  +" se encuentra vetada ";
                                    op2.setComentario( msj );  
                                    if( msj .equals("") ){
                                        List items =  this.AdicionOPDataAccess.getMovPendientesOC(op2.getDstrct(), op2.getOc(), op2.getProveedor(), op2.getHandle_code());
                                        if (items != null ){
                                            op2.setItem(items);
                                        }
                                    }
                                    AdicionOPDataServices.Liquidar(op2);
                                    List Lista2 =new LinkedList();
                                    Lista2.add(op2);
                                    if (Lista2 != null && Lista2.size() > 0){                
                                        model.OpSvc.insertList(Lista2, usuario ,file , fileError , "ADICION");
                                    }
                                    Lista2= null;
                                    //----------- fin programa fernell
                            }//--fin convertir movimiento en factura
                            /***************************************************************/
                            ChequeXFacturaDAO chequeXfac =new ChequeXFacturaDAO();
                            // trae la lista de los documentos relacionados a la OP
                            List LIFac   = new LinkedList();
                            LIFac = this.LiquidarOCDataAccess.getFacturasRelacionadas( op.getDstrct(), "OP"+op.getOc(),op.getProveedor());
                            CXP_Doc factura = new CXP_Doc();
                            CXPDocService cxpDocService = new CXPDocService();
                            ConsultaExtractoDAO consultaExtractoDAO = new ConsultaExtractoDAO();
                            //metodo que trae una factura determinada
                            factura = cxpDocService.BuscarFactura(op.getDstrct(),op.getProveedor(),"OP"+op.getOc(),"010");
                            // se actualizan los datos de la OP
                            //op.setOc(factura.getDocumento()      );
                            op.setVlrOc(factura.getVlr_total()   );
                            op.setVlrNeto(factura.getVlr_total());
                            op.setVlr_neto(factura.getVlr_total());
                            op.setDocumento( factura.getDocumento() ); 
                            Tipo_impuestoService TimpuestoSvc = new Tipo_impuestoService(); 
                            Vector vTipImp = TimpuestoSvc.vTiposImpuestos();
                            //se cargan los item's de la factura
                            //Vector vItems  = cxpDocService.BuscarItemFactura(factura,vTipImp);
                            Vector vItems  = consultaExtractoDAO.obtenerDetalleFacturas(factura);
                            // Para completar la lista de items con los 
                            // de los docuemntos o facturas asociadas a la OP
                            for(int j=0;j<=LIFac.size()-1;j++){
                                CXP_Doc factura2 = new CXP_Doc();
                                Vector OTi   = new Vector();
                                Vector tupla = new Vector();
                                tupla = (Vector)LIFac.get(j);
                                //cargo las facturas relacionada
                                factura2 = cxpDocService.BuscarFactura(op.getDstrct(),op.getProveedor(),(String)tupla.get(0),(String)tupla.get(1));   
                                OTi  = consultaExtractoDAO.obtenerDetalleFacturas(factura2);
                                for(int k=0;k<=OTi.size()-1;k++){
                                    CXPItemDoc item2 = new CXPItemDoc();
                                    item2 = (CXPItemDoc) OTi.get(k);
                                    vItems.add(item2);
                                }
                                /*   Free mem */
                                factura2= null;OTi= null;tupla= null;
                            }                           
                            List LItems   = new LinkedList();
                            for (int j=0;j<=vItems.size()-1;j++){
                                String temporal="";
                                OPItems  item = new OPItems();
                                CXPItemDoc item2 = new CXPItemDoc();
                                item2 = (CXPItemDoc) vItems.get(j);
                                    item.setDstrct       ( item2.getDstrct() );
                                    item.setDescripcion  ( item2.getDescripcion());
                                    item.setItem         ( ""+(j+1) );
                                    item.setDescconcepto ( item2.getDescripcion());
                                    if( (""+item2.getDescripcion()).length() > 10 )
                                        temporal = (""+item2.getDescripcion()).substring(0,12);
                                    if( op.isTieneOp() && item2.getTipo_documento().equals("035") && temporal.equals("Cruce por PP")){
                                        item.setVlr          ( item2.getVlr()*-1 );
                                    }else{
                                        item.setVlr          ( item2.getVlr() );
                                    }
                                    item.setAsignador    ( "" );
                                    item.setIndicador    ( "" );
                                    item.setMoneda       ( op.getMoneda() );
                                    item.setCodigo_cuenta(  item2.getCodigo_cuenta());  // PL de Ivan.  get_codigocuenta
                                    item.setTipo_cuenta  (  ""  );
                                    item.setAgencia      (  item2.getAgencia() );
                                    item.setFactura      (  factura.getDocumento()  );
                                    item.setCodigo_abc   (  item2.getCodigo_abc()  );  // Codigo ABC               
                                    item.setCheque       (  ""  );  // ivan Gomez
                                    item.setConcepto     (  item2.getConcepto()  );  // Fernel
                                    item.setVlrReteFuente( item2.getVlr_rfte() );
                                    item.setVlrReteIca   ( item2.getVlr_rica() );
                                LItems.add(item);
                            }
                            op.setItem(LItems);
                            liquidacion.setFacturas(listaOP);
                            /********************* Free Mem *******************************/
                            //vItems = null;LItems = null; listaOP = null;
                            //LIFac= null;op= null;op2= null;factura= null;
                            /**************************************************************/
                            /////////////////////////////////////////////////////////////////////////////
                        }
                    }
                    liquidacion.setOC      (p.getNumpla());
                    liquidacion.setPlanilla(p);
                    liquidacion.setBase    (p.getBase());
                    
                    List facturas = liquidacion.getFacturas();
                    if (facturas!=null && !facturas.isEmpty()){
                        OP op = (OP) facturas.get(0);
                        if ( verificarAnticipos( p.getNumpla(), p.getNitpro() ) ){
                            op.setComentario( 
                                op.getComentario() + 
                                (op.getComentario().equals("")?"":"<br>") +  
                                "Esta planilla posee anticipos no impresos, por favor retirela." 
                            );
                        }
                    }
                    liquidaciones.add(liquidacion);                    
                }
                // busca descuentos mims
                this.obtenerAjustesProveedorMIMS();
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
  
    
    
    /**
     * M�todo recorrerLista, realiza el proceso de liquidacion de OC - Fintra
     * @autor fvillacob
     * @see liquidarOC - liquidarOCDAO
     * @param List lista de ocs
     * @return List
     * @throws Exception
     * @version 1.0.
     **/
    public List recorrerLista(List lista, String procedencia)throws  Exception{
        try{
            
            
            if(lista!=null && lista.size()>0 ){
                
                // Excluimos oc que se repiten ya sea por tener varias ots:
                lista = this.getExcluirRepetidos(lista);
                
                for(int i=0;i<lista.size();i++){
                    OP op = (OP)lista.get(i);
                    
                    if( op.getComentario().equals("") ){                    
                    
                     
                           // Equipos(Descuentos de equipos):
                            String proEQ  = (  procedencia.equals(DESCUENTOS_MIMS)  )?"CONSULTA":procedencia;
                            List  item = equipos(op, proEQ);


                            // Movimientos de la oc, de la tabla  movpla
                            String  SQL = "SQL_SEARCH_MOV_PP";
                            List movimientos = opDAO.getItemOP(op, SQL);


                            // Si es proceso consulta, adicionamos a la lista, ya que no hay insert en movpla
                            if( item!=null )
                                movimientos.addAll( item );


                            // completamos valores
                            movimientos = completarItems(op, movimientos );


                            // Adicionamos a la op los mivimientos
                            op.setItem(movimientos);


                            // Realizamos calculos de Liquidacion:
                            op = this.formarliquidar(op, procedencia );


                            
                     }
                    
                 }
                
            }
            
            
        }catch( Exception e){
            e.printStackTrace();
            throw new Exception(" recorrerLista: "+e.getMessage());
        }
        return lista;
    }
    /**
     * M�todo formarOP, realiza el proceso de liquidacion de una OC para la Generaci�n de OPs  TSP.
     * @autor fvillacob
     * @see liquidarOC - liquidarOCDAO
     * @param OP op
     * @return OP
     * @throws Exception
     * @version 1.0.
     **/
    public OP formarOP(OP op)throws  Exception{
        try{
                            
           
              if( op.getComentario().equals("") ){  
                     
                   // Equipos(Descuentos de equipos):
                       List  item = equipos(op, GENERACIONOP);

                   // Movimientos de la oc, de la tabla  movpla
                       String  SQL = "SQL_SEARCH_MOV";
                       List movimientos = opDAO.getItemOP(op, SQL );
                       

                    // Si es proceso consulta, adicionamos a la lista, ya que no hay insert en movpla
                       if( item!=null )
                           movimientos.addAll( item );

                    // completamos valores
                       movimientos = completarItems(op, movimientos );

                    // Adicionamos a la op los mivimientos
                       op.setItem(movimientos);

                    // Realizamos calculos de Liquidacion:
                       op = this.formarliquidar(op, GENERACIONOP );                
             }
            
            
        }catch( Exception e){
            e.printStackTrace();
            throw new Exception(" formarOP: "+e.getMessage());
        }
        return op;
    }
    
    
    
     /**
     * Metodo de verifiacion de anticipos y renaticipos no impresos
     * @autor jbarros
     * @param planilla, numero de planilla
     * @param proveedor, nit
     * @return boolean, true si tiene anticipos no impresos, false lo contrario
     * @throws Exception.
     */
    public boolean verificarAnticiposOP(String planilla, String proveedor) throws Exception{
        try{
            return LiquidarOCDataAccess.verificarAnticiposOP(planilla, proveedor);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    
    
      /**
     * M�todo formarliquidar, complementa la Op en la Liquidacion
     * @autor...fvillacob
     * @param op objeto resultado de la busqueda de la informacion para liquidar una oc
     * @return OP (orden de pago Objeto)
     * @throws..Exception
     * @version..1.0.
     **/
    public OP  formarliquidar(OP op, String  procedencia) throws Exception{
        
        try{
            
            
            
        // Establece si se requiere validaciones o no, dependiendo el programa que llame a la rutina
            boolean  validar = (procedencia.equals( GENERACIONOP ) )?true:false;
            
            
            String planilla = op.getOc();
            
            
            //1. Obtenemos la cantidad cumplida  de la planilla segun la base
            double cantidad = 1;
            if ( !op.getBase().equals("COL")  && !op.getBase().equals("")  )    cantidad = opDAO.buscarCantidadCumplidaCarbon(op.getOc());  //Si es de carbon.
            else                                                                cantidad = opDAO.cantidadCumplida( op.getOc() );
            op.setCantiCump( String.valueOf(cantidad) );
            
            
            //2. Define el valor de la OC, si esta cumplida se recalcula asi vlr = valor Unitario * cantidad
            double valorPlanilla  = op.getVlrOc();
            if( op.getRegStatus().toUpperCase().equals("C") ){
                valorPlanilla   = op.getVlrUnitarioOC() * cantidad;
            }
            op.setVlrOc   ( valorPlanilla  );
            op.setVlrOc_Me( valorPlanilla  );
            
            //3. Establecemos Monedas:
            String monedaLocal    = op.getMonedaLocal();
            String monedaBanco    = op.getMoneda();
            
            
            int    swValor       =  0;
            String msj           = "";
            String fecha         = Utility.getHoy("-");  // HOY
            
            
            
            // Validamos datos
            
            if( validar )
               if( op.getABC().equals("") )
                    msj += "No presenta c�digo ABC para la agencia de la planilla " +  op.getAgenciaOC() ;
       
            
            
            if( valorPlanilla < 0 ){
                msj    += " El valor del item correspondiente a la planilla no es positivo " + SEPARADOR;
                swValor = 1;
            }            
            
            if( op.getProveedor().trim().equals("") ){
                msj    += " No presenta proveedor establecido " + SEPARADOR;
                swValor = 1;
            }
            
             
            if( op.getBanco().trim().equals("")  ){
               msj    += " El proveedor no presenta banco " + SEPARADOR;
               swValor = 1;
            }
            
            
           if( op.getSucursal().trim().equals("")  ){
              msj    += " El  proveedor no presenta sucursal de banco " + SEPARADOR;
              swValor = 1; 
           }
            
            
           if( monedaBanco.trim().equals("") ){
                msj    += " El banco  "+  op.getBanco() +" "+ op.getSucursal() +" del proveedor, no presenta moneda " + SEPARADOR;
                swValor = 1;
           }
            
           if( monedaLocal.trim().equals("") ){
                msj    += " No existe moneda local " + SEPARADOR;
                swValor = 1;
           } 
                   
            
            
                         
           // Rica para el origen:
              Hashtable  datosOrigen    =  opDAO.getDatosAgencia( op.getOrigenOC() );
              String     codigoICA      =  "";
              if( datosOrigen == null ){                              
                    msj    += " El origen "+  op.getOrigenOC()  +" no se encuentra registrado en la tabla de ciudad " + SEPARADOR;
                    swValor = 1;                                
              }else{ 
                           codigoICA      =  (String)datosOrigen.get("codica");
                    String aplicaICA      =  (String)datosOrigen.get("aplica");                              
                    if( aplicaICA.equals("S")  && codigoICA.equals("") ){                                      
                          msj    += " El origen  aplica ICA y no presenta codigo de ICA "+ SEPARADOR;
                          swValor = 1;                                       
                    }
              }
            
            //5. Formar la lista de movimientos, insertamos el valor de la planilla como primer item de los movimientos
            List mov  =  formatMovimientos(op);
                        
            
            //6. Calculos valores de los movimientos
            List listMovimientos  = new LinkedList();
            
            
            if( swValor==0 ){
                
                if(mov!=null && mov.size()>0){
                    
                    // VALORES
                    for(int i=0; i<mov.size();i++){
                        OPItems item      = (OPItems) mov.get(i);
                        String  cta       = item.getCodigo_cuenta();  // Cuenta contable
                        
                        if( !cta.equals("") ){   
                             Hashtable  cuenta =  movDAO.getCuenta( item.getDstrct() , cta  );
                             if( cuenta!= null){
                                     String  auxCTA  = (String) cuenta.get("auxiliar");
                                      
                                      
                                      if( validar)
                                         if( auxCTA.equals("S") ){
                                             if(  item.getAuxiliar().equals("")  )
                                                 msj += "La cuenta " + cta +" requiere auxiliar y el item " +  item.getDescripcion()  +" no presenta";                                      
                                             if( !contabDAO.existeCuentaSubledger( item.getDstrct() , cta , item.getAuxiliar()  )   )
                                                 msj += "La cuenta " + cta + " no tiene el subledger "+ item.getAuxiliar()  +" asociado";
                                         }
                                         
                                      
                                      
                                       if( ! item.getIndicador().toUpperCase().equals("P")){

                                                        double vlrMov     = item.getVlr();
                                                        double vlrMov_ME  = vlrMov;
                                                        String monedaItem = item.getMoneda();

                                                        // convertir moneda movimiento a la del banco VLR_ME
                                                        if( !monedaBanco.equals( item.getMoneda() ))  {
                                                            if(item.getMoneda().trim().equals("")){
                                                                swValor  = 1;
                                                                msj     += "El item "+ item.getDescripcion() +" no presenta tipo de moneda " + SEPARADOR;
                                                            }else{
                                                                vlrMov_ME   = opDAO.getValorMoneda( item.getMoneda(), vlrMov, monedaBanco, fecha, op.getDstrct() );
                                                                if( vlrMov_ME==0  ){
                                                                    swValor  = 1;
                                                                    msj     += "No hay valor de tasa para convertir de ["+ item.getMoneda() + "] a ["+ monedaBanco + "] en la fecha " + fecha  + " para el movimiento " + item.getDescripcion()  + SEPARADOR;
                                                                }
                                                            }
                                                        }

                                                        // convertir moneda banco a la local   VLR_LOCAL
                                                        if( !monedaLocal.equals( monedaBanco )){
                                                            vlrMov     = opDAO.getValorMoneda( monedaBanco, vlrMov_ME, monedaLocal, fecha, op.getDstrct() );
                                                            if( vlrMov==0  ){
                                                                swValor  = 1;
                                                                msj     += "No hay valor de tasa para convertir de ["+ monedaBanco + "] a ["+ monedaLocal + "] en la fecha " + fecha  + " para el movimiento " + item.getDescripcion()  + SEPARADOR;
                                                            }
                                                        }
                                                        else
                                                            vlrMov  = vlrMov_ME;

                                                        item.setVlr_me  ( vlrMov_ME   );
                                                        item.setVlr     ( vlrMov      );
                                                        item.setMoneda  ( monedaBanco  );
                                                        
                                                        
                                       }
                                       
                                                
                                }else
                                   if( validar )
                                       msj += "La cuenta contable " + cta + " no existe o no est� activa.";
                             
                                                
                         
                          }else
                              if( validar )
                                  msj += "El item " + item.getItem() +" correspondiente a " +  item.getDescconcepto() + " [" +  item.getDescripcion() +"] no presenta cuenta contable";
                                  
                        
                        
                          listMovimientos.add(item); 
                        
                        
                    }
                    
                }
                
            }
            
            
            
            
            if( swValor==0 ){
                
                //7. CALCULAMOS VALOR % INDICADOR = P
                listMovimientos =  this.calcularPorcentajeItems(listMovimientos, monedaLocal,monedaBanco, fecha);
                
                
                
                //  Redondeamos valores Items:
                listMovimientos  =  redondear(listMovimientos);
                
                
                
                
                
               //8. IMPUESTOS
                
                //Obtenemos valores retenedor y contibuyente del proveedor (nitpro) de la OC
                  String retenedor       = op.getRetenedor().toUpperCase();
                  String contribuyente   = op.getContribuyente().toUpperCase();
           
               // Placa y pais de la placa de la planilla:
                  String  placa      =  op.getPlaca();
                  String  pais_placa =  this.LiquidarOCDataAccess.paisPlaca(placa);
                
                
                if(  !pais_placa.equals( VENEZUELA_PLACA )  && !monedaBanco.equals( BOLIVAR )     )   // Restringir impuesto Venezuela: cuando pais placa sea venezuela y banco venezuela, No aplicar Descuento.
                    if( !retenedor.equals("S")  ){  // si no es autoretenedor, Aplica Impuestos:                        
            
                          String fechaFINANO    =  Util.getFechaActual_String(1) +"-12-31";
                          String nuevoCodeImp   =  this.LiquidarOCDataAccess.codigoImp_tablaGen(codigoICA);  //Equivalente al codigo en tablagen.
                          IMP_RETEICA           =  opDAO.getImpuestoAgencia ( op.getDstrct(), nuevoCodeImp ,        fechaFINANO, op.getOrigenOC() );
                          IMP_RETEFUENTE        =  opDAO.getImpuestoAgencia ( op.getDstrct(), opDAO.CODERETEFUENTE, fechaFINANO, op.getOrigenOC() ); 
                          
                       // Aplicamos Impuestos a los Items:       
                          for(int i=0; i<listMovimientos.size();i++){
                              OPItems item      = (OPItems) listMovimientos.get(i);
                              item  = this.calculoImpuesto( item, datosOrigen );
                          }
                     
                    }
                  
                  
                
                //9.  Definimos al objeto OP la lista de items
                op.setItem( listMovimientos );
                
                
                //10. Calculamos Valor Neto OC = sumatoria Items tipo asigandor: V-valor
                op.setVlrOcNeto(  this.calcularTipoValores( listMovimientos , "V" ) );
                
                
                //11. Calculamos Valor de los Movimientos = sumatoria Items tipo asignador S-saldo
                op.setVlrMov  (   this.calcularTipoValores( listMovimientos , "S" ) );
                                
                
                //12. NETO DE LA FACTURA : Calculamos y definimos Neto a pagar en la OP = (sumatoria valores de los items) - (sumatoria valores de los impuestos aplicados a los items)
                op.setVlrNeto     ( this.calculoNetoLiquidacion   ( listMovimientos ) );
                op.setVlr_neto_me ( this.calculoNetoMELiquidacion ( listMovimientos ) );
                
                
                // Buscamo Tasa:
                TasaService  svc  =  new  TasaService();
                Tasa         obj  =  svc.buscarValorTasa(monedaLocal,  monedaLocal , monedaBanco , fecha);
                double       tasa = 1;
                if(obj!=null)
                      tasa   = obj.getValor_tasa();
                op.setTasa( tasa  );
                
                                                
                
            }
            
        // Adicionamos comentario:
            op.setComentario( msj );
            
        // Clase de documento Relacionado
            op.setClase_doc_rel("0");
            
            
        }catch(Exception e){
            throw new Exception( " liquidar OC " + e.getMessage());
        }
        return op;
    }
    
    /**
     * Metodo para  liquidar las planillas 
     * segun los parametros definidos
     * @autor mfontalvo.
     * @modificado por Jbarros 22 Marzo 2007
     * @throws Exception.
     */    
    public void LiquidacionPorBloque() throws Exception{
        try{
            if (ocsALiquidar!=null && !ocsALiquidar.isEmpty()){
                liquidaciones = new Vector();
                for (int i=0; i<ocsALiquidar.size();i++){
                    Planilla p = (Planilla) ocsALiquidar.get(i);
                    liquidacion = new Liquidacion();
                    liquidarOC(p.getNumpla());                    
                    liquidacion.setOC      (p.getNumpla());
                    liquidacion.setPlanilla(planilla);
                    liquidacion.setBase    (p.getBase());
                    
                    
                    List facturas = liquidacion.getFacturas();
                    if (facturas!=null && !facturas.isEmpty()){
                        OP op = (OP) facturas.get(0);
                        if ( verificarAnticipos( p.getNumpla(), p.getNitpro() ) ){
                            op.setComentario( 
                                op.getComentario() + 
                                (op.getComentario().equals("")?"":"<br>") +  
                                "Esta planilla posee anticipos no impresos, por favor retirela." 
                            );
                        }
                    }
                    liquidaciones.add(liquidacion);                    
                }
                
                // busca descuentos mims
                this.obtenerAjustesProveedorMIMS();
               
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }    

}
