/************************************************************************************
 * Nombre clase: Esquema_formatoService.java                                        *
 * Descripci�n: Clase que maneja los servicios al model relacionados con los        *
 *              formatos de esquema                                                 *
 * Autor: Ing. Jose de la rosa                                                      *
 * Fecha: 19 de octubre de 2006, 09:09 AM                                           *
 * Versi�n: Java 1.0                                                                *
 * Copyright: Fintravalores S.A. S.A.                                          *
 ***********************************************************************************/


package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  EQUIPO13
 */
public class Esquema_formatoService {
    
    private Esquema_formatoDAO esquema;
    /** Creates a new instance of Esquema_formatoService */
    public Esquema_formatoService () {
        this.esquema = new Esquema_formatoDAO ();
    }
    
    public Esquema_formato getEsquema () {
            return esquema.getEsquema ();
    }
    
    /**
     * Setter for property esquema.
     * @param esquema New value of property esquema.
     */
    public void setEsquema (Esquema_formato esquema) {
        this.esquema.setEsquema (esquema);
    }
    
    /**
     * Getter for property Vecesquema.
     * @return Value of property Vecesquema.
     */
    public Vector getVecesquema () {
       return esquema.getVecesquema ();
    }
    
    /**
     * Setter for property Vecesquema.
     * @param Vecesquema New value of property Vecesquema.
     */
    public void setVecesquema (Vector Vecesquema) {
        esquema.setVecesquema ( Vecesquema );
    }
    
    
    /**
     * Metodo: insertEsquema_formato, permite ingresar un registro a la tabla esquema_formato.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void insertEsquema_formato ( Esquema_formato esquema ) throws SQLException{
        setEsquema (esquema);
        this.esquema.insertEsquema_formato ();
    }
    
    /**
     * Metodo: searchEsquema_formato, permite buscar un formato de esquema dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : codigo del programa y distrito
     * @version : 1.0
     */
    public void searchEsquema_formato ( String codigo, String distrito, String nombre )throws SQLException{
        this.esquema.searchEsquema_formato (codigo, distrito, nombre);
    }
    
    /**
     * Metodo: existEsquema_formato, permite buscar un formato de esquema dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : codigo del programa y distrito.
     * @version : 1.0
     */
    public boolean existEsquema_formato ( String codigo, String distrito, String nombre ) throws SQLException{
        return this.esquema.existEsquema_formato (codigo, distrito, nombre);
    }
    
    /**
     * Metodo: listEsquema_formato, permite listar todas los formatos de descuento
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void listEsquema_formato (String distrito, String codigo, String nombre, String titulo)throws SQLException{
        this.esquema.listEsquema_formato (distrito, codigo, nombre, titulo);
    }
    
        /**
     * Metodo: updateEsquema_formato, permite actualizar un esquema formato dados unos parametros.
     * @autor : Ing. Jose de la rosa
     * @param : 
     * @version : 1.0
     */
    public void updateEsquema_formato ( Esquema_formato esquema ) throws SQLException{
        setEsquema (esquema);
        this.esquema.updateEsquema_formato ();
    }
    
        /**
     * Metodo: anularEsquema_formato, permite anular un esquema formato dados unos parametros.
     * @autor : Ing. Jose de la rosa
     * @param : 
     * @version : 1.0
     */
    public void anularEsquema_formato ( Esquema_formato esquema ) throws SQLException{
        setEsquema (esquema);
        this.esquema.anularEsquema_formato ();
    }
    
    /**
     * Metodo: rangoEsquema_formato, permite buscar si se puede ingresar los valores iniciales y finales dentro de lo rangos existentes.
     * @autor : Ing. Jose de la rosa
     * @param : codigo del programa, distrito, posicion inicial y posicion final.
     * @version : 1.0
     */
    public boolean rangoEsquema_formato ( String distrito, String codigo, String campo, int ini, int fin ) throws SQLException{
        return this.esquema.rangoEsquema_formato (distrito, codigo, campo, ini, fin);
    }
    
    /**
     * Metodo: ordenEsquema_formato, permite buscar si se existe el orden en otro registro
     * @autor : Ing. Jose de la rosa
     * @param : codigo del programa, distrito, posicion inicial y posicion final.
     * @version : 1.0
     */
    public boolean ordenEsquema_formato ( String distrito, String codigo, String campo, int orden ) throws SQLException{
        return this.esquema.ordenEsquema_formato (distrito, codigo, campo, orden);
    }
}
