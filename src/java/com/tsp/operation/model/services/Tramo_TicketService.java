/*
 * Nombre        Tramo_TicketService.java
 * Autor         Ing Jesus Cuestas
 * Modificado    Ing Sandra Escalante
 * Fecha         25 de junio de 2005, 10:12 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.services;

import java.util.*;
import java.sql.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;

public class Tramo_TicketService {
    
    Tramo_TicketDAO tramotdao;
    
    /** Creates a new instance of Tramo_TicketService */
    public Tramo_TicketService() {
        tramotdao = new Tramo_TicketDAO();
    }
    
    /**
     * Getter for property ticket.
     * @return Value of property ticket.
     */
    public com.tsp.operation.model.beans.Tramo_Ticket getTicket() {
        return tramotdao.getTicket();
    }
    
    /**
     * Setter for property ticket.
     * @param ticket New value of property ticket.
     */
    public void setTicket(com.tsp.operation.model.beans.Tramo_Ticket ticket) {
        tramotdao.setTicket(ticket);
    }
    
    /**
     * Getter for property tickets.
     * @return Value of property tickets.
     */
    public java.util.Vector getTickets() {
        return tramotdao.getTickets();
    }
    
    /**
     * Setter for property tickets.
     * @param tickets New value of property tickets.
     */
    public void setTickets(java.util.Vector tickets) {
        tramotdao.setTickets(tickets);
    }
    
    /**
     * Metodo <tt>agregarTramo_Ticket</tt>, registra un ticket asociado a un tramo
     * @autor : Ing. Jesus Cuestas
     * @see agregarTramo_Ticket
     * @version : 1.0
     */
    public void agregarTramo_Ticket()throws SQLException{
        tramotdao.agregarTramo_Ticket();
    }
    
    /**
     * Metodo <tt>anularTramo_Ticket</tt>, anula un tiquete del tramo
     * @autor : Ing. Jesus Cuestas
     * @see anularTramo_Ticket
     * @version : 1.0
     */
    public void anularTramo_Ticket()throws SQLException{
        tramotdao.anularTramo_Ticket();
        
    }
    
    /**
     * Metodo <tt>buscarTramo_Ticket</tt>, obtiene un ticket especifico de un tramo
     * @autor : Ing. Jesus Cuestas
     * @see buscarTramo_Ticket
     * @param distrito, origen, destino del tramo y id del tiquete (String)
     * @version : 1.0
     */
    public void buscarTramo_Ticket(String cia, String origen, String destino, String ticket_id)throws SQLException{
        tramotdao.buscarTramo_Ticket(cia, origen, destino, ticket_id);
    }
    
    /**
     * Metodo <tt>listarTramos_Ticket</tt>, obtiene los tiquetes de un tramo
     * @autor : Ing. Jesus Cuestas
     * @see listarTramos_Ticket
     * @param distrito, origen, destino del tramo
     * @version : 1.0
     */
    public void listarTramos_Ticket(String cia, String origen, String destino)throws SQLException{
        tramotdao.listarTramos_Ticket(cia, origen, destino);
    }
    
    /**
     * Metodo <tt>modificarTramo_Ticket</tt>, modifica un tiquete de un tramo
     * @autor : Ing. Jesus Cuestas
     * @see modificarTramo_Ticket
     * @version : 1.0
     */
    public void modificarTramo_Ticket()throws SQLException{
        tramotdao.modificarTramo_Ticket();
    }
    
    /**
     * Metodo <tt>existeTramo_Ticket</tt>, verifica la existencia de un tiquete en un tramo
     * @autor : Ing. Jesus Cuestas
     * @see existeTramo_Ticket
     * @param distrito, origen, destino del tramo y id del tiquete (String)
     * @return boolean
     * @version : 1.0
     */
    public boolean existeTramo_Ticket(String cia, String origen, String destino, String ticket_id)throws SQLException{
        return tramotdao.existeTramo_Ticket(cia, origen, destino, ticket_id);
    }
    
    /**
     * Metodo <tt>existeIDTicket</tt>, verifica la existencia de un id ticket
     * @autor : Ing. Jesus Cuestas
     * @see existeTicketID
     * @param distrito (String), id ticke (String)
     * @return boolean
     * @version : 1.0
     */
    public boolean existeTicketID(String dstrct, String ticket_id)throws SQLException{
        return tramotdao.existeTicketID(dstrct, ticket_id);
    }
    
    /**
     * Metodo <tt>listarPeajes</tt>, obtiene la lista de peajes del sistema
     * @autor : Ing. Sandra Escalante
     * @see listarPeajes
     * @param distrito (String)
     * @version : 1.0
     */
    public void listarPeajes(String dstrct)throws SQLException{
        tramotdao.listarPeajes(dstrct);
    }
    
    /**
     * Getter for property peajes.
     * @return Value of property peajes.
     */
    public java.util.TreeMap getPeajes() {
        return tramotdao.getPeajes();
    }
}
