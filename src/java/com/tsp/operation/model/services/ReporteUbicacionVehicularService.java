/*********************************************************************
* Nombre        ReporteUbicacionVehicularDAO.java
* Descripci�n   Clase que presta los servicios para el acceso a los datos del reporte de ubicaci�n 
* vehicular de equipos en ruta
* Autor         Ivan Gomez
* Fecha         5 de octubre de 2005, 10:23 AM
* Versi�n       1.0
* Coyright      Transportes Sanchez Polo S.A.
**********************************************************************/


package com.tsp.operation.model.services;

import java.sql.SQLException;

import java.util.LinkedList;
import java.util.Hashtable;

import com.tsp.operation.model.DAOS.ReporteUbicacionVehicularDAO;

/**
 * Clase que presta los servicios para el acceso a los datos del reporte de ubicaci�n 
 * vehicular de equipos en ruta
 * @author  Ivan Gomez
 */
public class ReporteUbicacionVehicularService {
    
    private ReporteUbicacionVehicularDAO dao;
    
    /** Creates a new instance of ReporteUbicacionVehicularService */
    public ReporteUbicacionVehicularService() {
        dao = new ReporteUbicacionVehicularDAO();
    }
    
    /**
     * Metodo: getCiudadesRelacionadas, Busca las ciudades relacionadas con la ciudad pasada como parametro.
     * @autor : Ing. Ivn Gomez
     * @param: codCiudad Codigo de ciudad.
     * @throws SQLException Si un error de acceso a la base de datos ocurre.
     * @version : 1.0
     * @return la lista de ciudades relacionadas
     */
    public LinkedList getCiudadesRelacionadas(String codCiudad)throws SQLException {
        return dao.getCiudadesRelacionadas(codCiudad);
    }
    
    /**
     * Metodo: Este m�todo genera los registros que hacen parte del reporte de ubicaci�n
     * y llegada de equipos en ruta y los guarda en una lista.
     * @autor : Ing. Ivn Gomez
     * @param ubicVehArgs Criterios de b�squeda. Son los siguientes:
     *        ubicVehArgs[0] = C�digo del cliente.
     *        ubicVehArgs[1] = Fecha Inicial
     *        ubicVehArgs[2] = Fecha Final
     *        ubicVehArgs[3] = Tipos de Viaje (NA, RM, RC, RE, DM, DC)
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @version : 1.0
     */
    public void buscarDatosReporte(String [] ubicVehArgs )throws Exception {
        dao.buscarDatosReporte(ubicVehArgs);
    }
    
    /**
     * Metodo: Devuelve un array de tipo String [] donde se encuentran almacenados los nombres
     * de los campos del reporte de informaci�n al cliente.
     * @autor : Ing. Ivan Gomez
     * @return El array con los nombres de los campos del reporte de ubicaci�n vehicular.
     * @version : 1.0
     */
    public String [] obtenerCamposDeReporte(){
        return dao.obtenerCamposDeReporte();
    }
    
    /**
     * Metodo: obtenerTitulosDeReporte ,Este m�todo permite obtener los titulos del encabezado del reporte de
     * ubicaci�n. El Hashtable que es devuelto contiene los titulos de todos
     * los campos del reporte y la clave de cada titulo es su respectivo nombre
     * de campo en la Base de datos, para obtener los nombre de los campos del
     * reporte utilize el metodo <CODE>String [] obtenerCamposDeReporte()</CODE>.
     * @autor : Ing. Ivan Gomez
     * @throws SQLException Si algun problema ocurre en el acceso a la Base de Datos.
     * @return Una tabla con los titulos de los campos del reporte de ubicaci�n vehicular.
     */
    public Hashtable obtenerTitulosDeReporte() throws SQLException{
        return dao.obtenerTitulosDeReporte();
    }
    
    /**
     * Metodo: obtenerDatosReporte,la documentaci�n del mismo metodo en ReporteInformacionAlClienteDAO
     * @autor : Ing. Ivan Gomez
     * @version : 1.0
     */
    public LinkedList obtenerDatosReporte(){
        return dao.obtenerDatosReporte();
    }
}
