/**
 * Nombre        ReversarViajeTraficoService.java
 * Descripci�n
 * Autor         Mario Fontalvo Solano
 * Fecha         24 de noviembre de 2006, 05:00 PM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;

public class ReversarViajeTraficoService {
    private ReversarViajeTraficoDAO rmtdao; 
    /** Crea una nueva instancia de  ReversarViajeTraficoService */
    public ReversarViajeTraficoService() {
        rmtdao = new ReversarViajeTraficoDAO();
    }
    
    
    /**
     * M�todo que retorna true si existe la planilla en ingreso trafico
     * @autor.......Ing. Ivan Gomez
     * @param numpla Numero de la planilla
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public boolean existePlanillaIngresoTrafico( String numpla ) throws SQLException{
        return rmtdao.existePlanillaIngresoTrafico( numpla );
    }
    
    /**
     * M�todo que retorna true si existe la planilla en trafico
     * @autor.......Ing. Ivan Gomez
     * @param numpla Numero de la planilla
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public boolean existePlanillaTrafico( String numpla ) throws SQLException{
        return rmtdao.existePlanillaTrafico( numpla );
    }
    
    
    /**
     * M�todo que retorna true si la ultima planilla en rep_mov_trafico
     * es un tipo de reporte finalizado
     * @autor.......Ing. Ivan Gomez
     * @param numpla Numero de la planilla
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public boolean esPlanillaFinalizada( String numpla ) throws SQLException{
        return rmtdao.esPlanillaFinalizada( numpla );
    }
    
    /**
     * M�todo que obtiene el reporte a copiar en ingreso_trafico
     * @autor.......Ing. Ivan Gomez
     * @param numpla Numero de la planilla
     * @throws......SQLException
     * @version.....1.0.     
     **/
    public void obtenerReporteTrafico( String numpla ) throws SQLException{
        rmtdao.obtenerReporteTrafico( numpla );
    }
    
    /**
     * Getter for property ht.
     * @return Value of property ht.
     */
    public java.util.Hashtable getHt() {
        return rmtdao.getHt();
    }
    
    
     /**
     * M�todo que Obtiene el String de via
     * @autor.......Ing. Ivan Gomez
     * @param numpla Numero de la planilla
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public String obtenerVia( String ruta ) throws SQLException{
        return rmtdao.obtenerVia( ruta );
    }
    
    /**
     * M�todo que insertar en ingreso_trafico
     * @autor.......Ing. Ivan Gomez
     * @param numpla Numero de la planilla
     * @throws......SQLException
     * @version.....1.0.     
     **/
    public void insertarCopiaIngresoTrafico() throws SQLException{
        rmtdao.insertarCopiaIngresoTrafico(); 
    }
    
}
