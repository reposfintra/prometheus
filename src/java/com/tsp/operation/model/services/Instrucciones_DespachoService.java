/************************************************************************
 * Nombre clase: Instrucciones_DespachoDAO.java                         *
 * Descripci�n: Clase que maneja los servicios de la tabla donde se     *
 *              cargan las instrucciones de despacho                    *
 * Autor: Ing. Karen Reales                                             *
 * Fecha: Created on 23 de Enero de 2007, 09:25 AM                      *
 * Versi�n: Java 1.0                                                    *
 * Copyright: Fintravalores S.A. S.A.                              *
 ***********************************************************************/
package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  kreales
 */
public class Instrucciones_DespachoService {
    
    Instrucciones_DespachoDAO instDAO;
    
    /** Creates a new instance of CumplidoService */
    public Instrucciones_DespachoService() {
        instDAO = new Instrucciones_DespachoDAO();
    }
    
    public void ListarOrdenCompra(String dstrct)throws SQLException{
        instDAO.ListarOrdenCompra(dstrct);
    }
    public java.util.Vector getLista() {
        return instDAO.getLista();
    }
    
    /**
     * Setter for property lista.
     * @param lista New value of property lista.
     */
    public void setLista(java.util.Vector lista) {
        instDAO.setLista(lista);
    }
    public void ListarRemesas(String numpla)throws SQLException{
        instDAO.ListarRemesas(numpla);
    }
    /**
     * Getter for property remesas.
     * @return Value of property remesas.
     */
    public java.util.Vector getRemesas() {
        return instDAO.getRemesas();
    }
    
    /**
     * Setter for property remesas.
     * @param remesas New value of property remesas.
     */
    public void setRemesas(java.util.Vector remesas) {
        instDAO.setRemesas(remesas);
    }
    /**
     * Metodo: existeOrdenCompra, COnsulta que retorta true o false si la
     *orden de compra existe.
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public boolean existeOrdenCompra(String dstrct, String ocompra)throws SQLException{
        return instDAO.existeOrdenCompra(dstrct, ocompra);
    }
    
    /**
     * Getter for property inst.
     * @return Value of property inst.
     */
    public com.tsp.operation.model.beans.Instrucciones_Despacho getInst() {
        return instDAO.getInst();
    }
    
    /**
     * Setter for property inst.
     * @param inst New value of property inst.
     */
    public void setInst(com.tsp.operation.model.beans.Instrucciones_Despacho inst) {
        instDAO.setInst(inst);
    }
    /**
     * Metodo: actualizarOCompra,actualiza en la tabla la orden de compra
     *con el numero de la remesa, el de la factura y la fecha de la factura.
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public String actualizarOCompra(Remesa r)throws SQLException{
        return instDAO.actualizarOCompra(r);
    }
    /**
     * Metodo: agregarDocumento, agrega o modifica documentos internos
     *de una remesa
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public String agregarDocumento(Remesa r)throws SQLException{
        return instDAO.agregarDocumento(r);
    }
    /**
     * Metodo: borrarDocumento, borra de la tabla remesadocto un documento
     * interno de una remesa
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public String borrarDocumento(Remesa r)throws SQLException{
        return instDAO.borrarDocumento(r);
    }
    /**
     * Metodo: desmarcarRemesa, actualiza en la tabla la orden de compra
     *con el numero de la remesa, el de la factura y la fecha de la factura en default
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public String desmarcarRemesa(Remesa r)throws SQLException{
        return instDAO.desmarcarRemesa(r);
    }
    /**
     * Metodo: buscarReferencias, busca el vector de referencias que se encuentran
     *relacionadas a una orden de compra.
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public Vector buscarReferencias(Remesa r)throws SQLException{
        return instDAO.buscarReferencias(r);
    }
    /**
     * Metodo: ListarCausas, lista las causas del cargue incompleto
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public void ListarCausas()throws SQLException{
        instDAO.ListarCausas();
    }
    /**
     * Getter for property causas.
     * @return Value of property causas.
     */
    public java.util.Vector getCausas() {
        return instDAO.getCausas();
    }
    
    /**
     * Setter for property causas.
     * @param causas New value of property causas.
     */
    public void setCausas(java.util.Vector causas) {
        instDAO.setCausas(causas);
    }
    /**
     * Metodo: actualizarCantCargue,actualiza la tabla con la cantidad
      *de despacho y la causa si aplica
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public String actualizarCantCargue(Instrucciones_Despacho inst)throws SQLException{
        return instDAO.actualizarCantCargue(inst);
    }
     /**
     * Metodo: actualizarFechaCargue,actualiza la tabla con la fecha real
     *de cargue
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public String actualizarFechaCargue(Remesa rem)throws SQLException{
        return instDAO.actualizarFechaCargue(rem);
    }
    
     /**
     * Metodo: reporteTrakin, Genera el reporte de trakin
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public void reporteTrakin(String ocompra, String factura, String destinatario, String origen, String destino, String fecha1, String fecha2)throws SQLException{
        instDAO.reporteTrakin(ocompra, factura, destinatario, origen, destino, fecha1, fecha2);
    }
    
       /**
     * Metodo: reporteNovedad, Genera el reporte de novedades
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public void reporteNovedad(String condicion, String fecha, String mes)throws SQLException{
        instDAO.reporteNovedad(condicion,fecha,mes);
    }
      /**
     * Metodo: ListarSolucion, lista las posibles soluciones de novedades
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public void ListarSolucion()throws SQLException{
        instDAO.ListarSolucion();
    }
    
    /**
     * Metodo: actualizarNovedada,actualiza en la tabla la orden de compra
     *con la fecha y solucion de la novedad
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public String actualizarNovedad(Instrucciones_Despacho i)throws SQLException{
        return instDAO.actualizarNovedad(i);
    }  
    
      /**
     * Metodo: BuscarNumeroFactura, busca el numero de una factura
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public String BuscarNumeroFactura(String condicion)throws SQLException{
        return instDAO.BuscarNumeroFactura(condicion);
    }
    /**
     * Funcion publica que actualiza la informacion de una entrada planeada especifica.
     */
    public void buscarFactura( String distrito, String tipo_documento, String documento ) throws SQLException {
        instDAO.buscarFactura(distrito, tipo_documento, documento);
    }
    
       /**
     * Metodo: inventario, Genera el reporte de inventario
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public void inventario(String ocompra, String factura, String dstrct)throws SQLException{
        instDAO.inventario(ocompra,factura,dstrct);
    }
         /**
     * Metodo: inventario, Genera el reporte de inventario
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public void detalleInventario(String condicion, String dstrct, String ubicacion, String material)throws SQLException{
        instDAO.detalleInventario(condicion, dstrct, ubicacion, material);
    }
    
         /**
     * Funcion publica que actualiza la informacion de una entrada planeada especifica.
     */
    public void buscarListaFactura( String fecha1, String fecha2 ) throws SQLException {
        instDAO.buscarListaFactura(fecha1,fecha2);
    }
        /**
     * Metodo: buscarRemesa, lista las remesas de una planilla
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public void buscarRemesa(String numrem)throws SQLException{
        instDAO.buscarRemesa(numrem);
    }
    /**
     * Metodo: buscarRemesa, lista las remesas de una planilla
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public void buscarOcompra(String ocompra, String cia)throws SQLException{
        instDAO.buscarOcompra(ocompra,cia);
    }
    
     public String liberarOCompra(String numrem)throws SQLException{
        return instDAO.liberarOCompra(numrem);
    }
    
}
