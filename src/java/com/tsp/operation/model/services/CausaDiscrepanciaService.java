/*
 * CausaDiscrepanciaService.java
 *
 * Created on 13 de octubre de 2005, 04:38 PM
 */

package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  dbastidas
 */
public class CausaDiscrepanciaService {
    private CausaDiscrepaciaDAO causadiscre;
    /** Creates a new instance of CausaDiscrepanciaService */
    public CausaDiscrepanciaService() {
        causadiscre = new CausaDiscrepaciaDAO();
    }
    
    public Vector listarCausas( ) throws SQLException {
        try{
            return causadiscre.listarCausas();
        }catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
    
     public String descripcionCausa(String codigo ) throws SQLException {
         try{
             return causadiscre.descripcionCausa(codigo);
         }catch(SQLException e){
           throw new SQLException(e.getMessage());
         }
     }
}
