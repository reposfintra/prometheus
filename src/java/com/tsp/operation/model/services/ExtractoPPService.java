/***************************************
 * Nombre Clase  ExtractoPPService.java
 * Descripci�n   Permite Revisar los Prontopagos Aprobados
 * Autor         JULIO ERNESTO BARROS RUEDA
 * Fecha         25/11/2006
 * versi�n       1.0
 * Copyright     Transportes Sanchez Polo S.A.
 *******************************************/


package com.tsp.operation.model.services;


import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.util.Utility;
import com.tsp.util.Util;
import java.util.*;
import java.io.*;



public class ExtractoPPService {

    ExtractoPPDAO   ExtractoPPDataAccess;
    
    Vector          Extractos        = new Vector();
    Vector          ExtractoDetalle1 = new Vector();
    Vector          ExtractoDetalle2 = new Vector();
    Vector          ExtractoDetalle0 = new Vector();
    Vector          ExtractoDetalleS = new Vector();
    Extracto        Extracto         = new Extracto(); 
    String          NitProp          = "";
    int             Secuen           = 0;
    Vector          Fechas           = new Vector();
    float           valor            = 0;
    
    public ExtractoPPService(String dataBaseName) {
        ExtractoPPDataAccess = new ExtractoPPDAO(dataBaseName);
    }
    
    
    
    public void buscarExtractosPP(String propietario, int tipo,String fechai,String fechaf) throws Exception {
        try{
            Extractos = ExtractoPPDataAccess.buscarExtractosPP(propietario,tipo,fechai,fechaf);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    public void buscarExtractosPPOC(String propietario,String fechai,String fechaf) throws Exception {
        try{
            NitProp = ExtractoPPDataAccess.buscarExtractoPPOC(propietario,fechai,fechaf);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    public void buscarExtractoPPOC_Fechas(String propietario,String fechai,String fechaf) throws Exception {
        try{
            Fechas = ExtractoPPDataAccess.buscarExtractoPPOC_Fechas(propietario,fechai,fechaf);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    
    public void buscarExtractosPP(String propietario, int tipo) throws Exception {
        try{
            if(Fechas.size()!= 0)
                Extractos = ExtractoPPDataAccess.buscarExtractosPP_Fechas(propietario,tipo,Fechas);
            else 
                Extractos = null;
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    public void buscarExtractoDetallePP(String propietario,String fecha,int tipo) throws Exception {
        try{
            if(tipo==1){
                ExtractoDetalle1 = ExtractoPPDataAccess.buscarExtractoDetallePP(propietario,fecha,tipo);
            }else if(tipo==2){
                ExtractoDetalle2 = ExtractoPPDataAccess.buscarExtractoDetallePP(propietario,fecha,tipo);
            }else {
                ExtractoDetalle0 = ExtractoPPDataAccess.buscarExtractoDetallePP(propietario,fecha,tipo);
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    public void buscarExtractoDetallePP( String secuencia) throws Exception {
        try{
            ExtractoDetalleS = ExtractoPPDataAccess.buscarExtractoDetallePP(secuencia);
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    public void buscarExtractoPP(String propietario,String fecha) throws Exception {
        try{
            Extracto = ExtractoPPDataAccess.buscarExtractoPP(propietario,fecha);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    public void bucar_Secuencia() throws Exception {
        try{
            Secuen = ExtractoPPDataAccess.bucar_Secuencia();
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    
    public float retornar_vlr_pp_e(String fecha) throws Exception {
        try{
            return  ExtractoPPDataAccess.retornar_vlr_pp_e(fecha);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    public float retornar_vlr_pp_ed(String documento,String fecha) throws Exception {
        try{
            return  ExtractoPPDataAccess.retornar_vlr_pp_ed(documento,fecha);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    
    /**
     * Getter for property descuentosPropietario.
     * @return Value of property descuentosPropietario.
     */
    public java.util.Vector getExtractoPP() {
        return Extractos;
    }
    
    /**
     * Getter for property descuentosPropietario.
     * @return Value of property descuentosPropietario.
     */
    public java.util.Vector getExtractoDetallePP1() {
        return ExtractoDetalle1;
    }
    
    /**
     * Getter for property descuentosPropietario.
     * @return Value of property descuentosPropietario.
     */
    public java.util.Vector getExtractoDetallePP2() {
        return ExtractoDetalle2;
    }
    
    /**
     * Getter for property descuentosPropietario.
     * @return Value of property descuentosPropietario.
     */
    public java.util.Vector getExtractoDetallePP0() {
        return ExtractoDetalle0;
    }
    
    /**
     * Getter for property descuentosPropietario.
     * @return Value of property descuentosPropietario.
     */
    public java.util.Vector getExtractoDetallePPS() {
        return ExtractoDetalleS;
    }
    
    /**
     * Getter for property descuentosPropietario.
     * @return Value of property descuentosPropietario.
     */
    public Extracto getExtractoPPU() {
        return Extracto;
    }
    
    /**
     * Getter for property descuentosPropietario.
     * @return Value of property descuentosPropietario.
     */
    public String getNitPropXOC() {
        return NitProp;
    }
    
    /**
     * Getter for property descuentosPropietario.
     * @return Value of property descuentosPropietario.
     */
    public int get_Secuencia() {
        return Secuen;
    }
    
    public void updateExtracto(String secuencia) throws Exception {
        String Banco    ="";
        String Sucursal ="";
        List BancoSucursal;
        try{
            BancoSucursal = ExtractoPPDataAccess.buscarBanco_Sucursal(secuencia);
            for(int i=0;i<BancoSucursal.size();i++){
                Hashtable  Rep = (Hashtable) BancoSucursal.get(i);
		Banco    = ""+Rep.get("banco");
		Sucursal = ""+Rep.get("sucursal");
            }
            //System.out.println("baco y sucursal  "+Banco+"    "+Sucursal);
            ExtractoPPDataAccess.updateExtracto(secuencia,Banco,Sucursal);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }


    
}
