/*
 * ConsultaExtractoService.java
 *
 * Created on 15 de enero de 2007, 10:56 AM
 */

package com.tsp.operation.model.services;


import java.util.*;
import com.tsp.operation.model.beans.Proveedor;
import com.tsp.operation.model.beans.Planilla;
import com.tsp.operation.model.beans.Liquidacion;
import com.tsp.operation.model.beans.CXP_Doc;
import com.tsp.operation.model.beans.CXPItemDoc;
import com.tsp.operation.model.beans.Tasa;
import com.tsp.operation.model.beans.OP;
import com.tsp.operation.model.beans.OPItems;
import com.tsp.operation.model.beans.Planilla;
import com.tsp.operation.model.DAOS.ConsultaExtractoDAO;
import com.tsp.util.Util;

/**
 *
 * @author  equipo
 */
public class ConsultaExtractoService {
    
    
    ConsultaExtractoDAO dao;
    Vector vectorEgresos;
    Vector vectorDetalleEgresos;
    Vector vectorFacturasPendientes;
    Vector vectorFacturasCanceladas;
    Vector vectorPlanillasPendientes;
    Vector vectorLiquidaciones;
    Proveedor proveedor;
    CXP_Doc   factura;
    Planilla  planilla;
    
    
    TasaService tasaSvc;
    LiquidarOCService Liquidacion;
    TreeMap tasas;
    
    
    /** Creates a new instance of ConsultaExtractoService */
    public ConsultaExtractoService() {
        dao         = new ConsultaExtractoDAO();
        Liquidacion = new LiquidarOCService();
        tasaSvc     = new TasaService();
        tasas       = new TreeMap();
    }
    public ConsultaExtractoService(String dataBaseName) {
        dao         = new ConsultaExtractoDAO(dataBaseName);
        Liquidacion = new LiquidarOCService();
        tasaSvc     = new TasaService(dataBaseName);
        tasas       = new TreeMap();
    }
    
    
    
    /**
     * Metodo para obtener los egresos de un propietario a partir de un 
     * periodo dado
     * @param nit, nit del propietario
     * @param finicial, fecha inicial de la consulta
     * @param ffinal, fecha final de la consulta
     * @throws Exception.
     */
    public void obtenerEgresos(String dstrct, String nit, String finicial, String ffinal) throws Exception {
        try{
            vectorEgresos = dao.obtenerEgresos(dstrct, nit, finicial, ffinal); 
        }catch (Exception ex){
            throw new Exception (ex.getMessage());
        }
    }
    
    
    /**
     * Metodo para obtener el detalle de los egresos de un propietario a partir de un 
     * periodo dado
     * @param dstrct, distrito
     * @param banco, banco del egreso
     * @param sucursal, sucursal del egreso
     * @param documento, numero del egreso
     * @throws Exception.
     */
    public void obtenerDetalleEgresos(String dstrct, String banco, String sucursal, String documento) throws Exception {
        try{
            vectorDetalleEgresos = dao.obtenerDetalleEgresos(dstrct, banco, sucursal, documento); 
        }catch (Exception ex){
            throw new Exception (ex.getMessage());
        }
    }
    
    
    
    
    /**
     * Metodo para obtener las facturas pendientes por cancelar a un propietario
     * @param dstrct, distrito
     * @param proveedor, nit del propietario 
     * @throws Exception.
     */
    public void obtenerFacturasPendientes(String dstrct, String proveedor) throws Exception {
        try{
            vectorFacturasPendientes = dao.obtenerFacturasPendientes(dstrct, proveedor); 
            
            // conversiones de las tasas
            int    decimales = (this.proveedor.getCurrency().equals("DOL")?2:0);  
            
            for (int i = 0; vectorFacturasPendientes!=null && i< vectorFacturasPendientes.size(); i++){
                CXP_Doc f = (CXP_Doc) vectorFacturasPendientes.get(i);
                ////System.out.println("FACT " + f.getDocumento());
                if ( !f.getMoneda_dstrct().equals( this.proveedor.getCurrency() )  ){
                    if ( ! f.getMoneda().equals( this.proveedor.getCurrency() ) ){
                        Tasa t = this.obtenerTasa ( f.getMoneda(), this.proveedor.getCurrency(), f.getFecha_documento(), "FINV");
                        double tasa = (t!=null?t.getValor_tasa():0);
                        f.setVlr_saldo ( Util.roundByDecimal( f.getVlr_saldo_me() * tasa, decimales)  );
                        f.setTasa      ( tasa );
                    } else {
                        f.setVlr_saldo( f.getVlr_saldo_me()  );
                    }
                }
            }
        }catch (Exception ex){
            throw new Exception (ex.getMessage());
        }        
    }
       
    
    
   /**
     * Metodo para obtener las facturas de un propietario
     * @param dstrct, distrito
     * @param proveedor, nit del propietario 
     * @param tipo_documento, tipo de docuemnto a consultar
     * @param documento, factura a consultar
     * @throws Exception.
     */
    public void obtenerFactura(String dstrct, String proveedor, String tipo_documento, String documento) throws Exception {
        try {
            factura = dao.obtenerFactura(dstrct, proveedor, tipo_documento, documento);
        } catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }
      
       
    
    /**
     * Metodo para obtener las facturas de un propietario
     * @param CXP
     * @throws Exception.
     * @return Vector de egresos
     */
    public void obtenerDetalleFacturas(CXP_Doc fact) throws Exception {
        try{
            fact.setItems(dao.obtenerDetalleFacturas(fact)); 
        }catch (Exception ex){
            throw new Exception (ex.getMessage());
        }        
    }
    
        
    /**
     * Metodo para obtener las planillas pendientes por cancelar a un propietario
     * @param dstrct, distrito
     * @param proveedor, nit del propietario 
     * @throws Exception.
     */
    public void obtenerPlanillasPendientes(String dstrct, String proveedor) throws Exception {
        try{
            vectorPlanillasPendientes = dao.obtenerPlanillasPendientes(dstrct, proveedor); 
        }catch (Exception ex){
            throw new Exception (ex.getMessage());
        }        
    }    
      
    
    
    
    
    /**
     * Metodo para obtener un planilla
     * @param numpla, numero de la planilla.
     * @throws Exception.
     */
    public void obtenerPlanilla(String numpla) throws Exception {
        try{
            planilla = dao.obtenerPlanilla(numpla);
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }    
    
    /**
     * Metodo para administrar la busqueda de las tasas.
     * @autor mfontalvo
     * @param moneda1, moneda origen
     * @param moneda2, moneda destino
     * @param fecha, fecha en que se consultara la tasa
     * @param cia, distrito
     * @throws Exception.
     * @return Objeto tasa.
     */
    private Tasa obtenerTasa (String moneda1, String moneda2, String fecha, String cia ) throws Exception{
        try {
            String key = moneda1 + " " + moneda2 + " " + fecha + " " +cia;
            Tasa t = (Tasa) tasas.get(key);
            if (t==null){
                if (moneda1.equals(moneda2)){
                    t = new Tasa();
                    t.setValor_tasa(1);
                } else {
                    t = tasaSvc.buscarValorTasa("PES", moneda1, moneda2, fecha);
                }
                tasas.put(key, t);
            }
            return t;
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
    }
    
    
    
    /**
     * Metodo para obtener los egresos de un propietario a partir de un 
     * periodo dado
     * @param nit, nit del propietario
     * @param finicial, fecha inicial de la consulta
     * @param ffinal, fecha final de la consulta
     * @throws Exception.
     */
    public void generarLiquidacionesPendientes() throws Exception {
        try{
            if (vectorPlanillasPendientes!=null && !vectorPlanillasPendientes.isEmpty()){

                String hoy       = Util.dateFormat( new Date() );
                int    decimales = (this.proveedor.getCurrency().equals("DOL")?2:0);   
                
                System.out.println( new Date() );
                System.out.println(vectorPlanillasPendientes.size());
                
                for (int i=0; i<vectorPlanillasPendientes.size();i++){
                    Planilla p = (Planilla) vectorPlanillasPendientes.get(i);
                    Vector items = p.getRemesas();
                    
                    double total = 0;
                    for (int j = 0; items!=null && j < items.size(); j++ ){
                        OPItems  item = (OPItems) items.get(j);
                        Tasa t = this.obtenerTasa(item.getMoneda() , this.proveedor.getCurrency(), hoy, "FINV");
                        double tasa = (t!=null? t.getValor_tasa() : 0);
                        item.setVlr( Util.roundByDecimal( item.getVlr_me() * tasa, decimales ));
                        if ( item.getAsignador().equals("V") ){
                            item.setVlrReteFuente( Util.roundByDecimal( item.getVlr() * Double.parseDouble( item.getPorc_rfte() ) /100.0 , decimales ));
                            item.setVlrReteIca   ( Util.roundByDecimal( item.getVlr() * Double.parseDouble( item.getPorc_rica() ) /100.0, decimales ));
                        }
                        total += item.getVlr() + item.getVlrReteFuente() + item.getVlrReteIca() + item.getVlrReteIva();
                    }
                    p.setVlrPlanilla(total);
                }
                
                System.out.println( new Date() );
                
            }
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
    }    
    
    

    /**
     * metodo para aplicar la misma tasa de la factura a la sus items
     */
    public void aplicarTasaItemsFactura (CXP_Doc fact) throws Exception {
        try{
            
            int    decimales = (this.proveedor.getCurrency().equals("DOL")?2:0);
            Vector items = fact.getItems();            
            
                
            if ( !fact.getMoneda_dstrct().equals( this.proveedor.getCurrency() )  ){
                if ( ! fact.getMoneda().equals( this.proveedor.getCurrency() ) ){
                    for (int j = 0; items!=null && j < items.size(); j++ ){
                        CXPItemDoc item = (CXPItemDoc) items.get(j);                            
                        item.setVlr     ( Util.roundByDecimal( item.getVlr()      * fact.getTasa(), decimales ) );
                        item.setVlr_iva ( Util.roundByDecimal( item.getVlr_iva()  * fact.getTasa(), decimales ) );
                        item.setVlr_riva( Util.roundByDecimal( item.getVlr_riva() * fact.getTasa(), decimales ) );
                        item.setVlr_rica( Util.roundByDecimal( item.getVlr_rica() * fact.getTasa(), decimales ) );
                        item.setVlr_rfte( Util.roundByDecimal( item.getVlr_rfte() * fact.getTasa(), decimales ) );
                    }                        
                } else {
                    for (int j = 0; items!=null && j < items.size(); j++ ){
                        CXPItemDoc item = (CXPItemDoc) items.get(j);                            
                        item.setVlr     ( item.getVlr_me() );
                        item.setVlr_iva ( item.getVlr_iva_me()  );
                        item.setVlr_riva( item.getVlr_riva_me() );
                        item.setVlr_rica( item.getVlr_rica_me() );
                        item.setVlr_rfte( item.getVlr_rfte_me() );
                    }     
                }
            }     
            
        }catch (Exception ex){
            throw new Exception (ex.getMessage());
        }        
    }    

    
    
    /**
     * Getter for property vectorEgresos.
     * @return Value of property vectorEgresos.
     */
    public java.util.Vector getVectorEgresos() {
        return vectorEgresos;
    }
    
    /**
     * Setter for property vectorEgresos.
     * @param vectorEgresos New value of property vectorEgresos.
     */
    public void setVectorEgresos(java.util.Vector vectorEgresos) {
        this.vectorEgresos = vectorEgresos;
    }
    
    /**
     * Getter for property vectorDetalleEgresos.
     * @return Value of property vectorDetalleEgresos.
     */
    public java.util.Vector getVectorDetalleEgresos() {
        return vectorDetalleEgresos;
    }
    
    /**
     * Setter for property vectorDetalleEgresos.
     * @param vectorDetalleEgresos New value of property vectorDetalleEgresos.
     */
    public void setVectorDetalleEgresos(java.util.Vector vectorDetalleEgresos) {
        this.vectorDetalleEgresos = vectorDetalleEgresos;
    }
    
    /**
     * Getter for property vectorFacturasPendientes.
     * @return Value of property vectorFacturasPendientes.
     */
    public java.util.Vector getVectorFacturasPendientes() {
        return vectorFacturasPendientes;
    }
    
    /**
     * Setter for property vectorFacturasPendientes.
     * @param vectorFacturasPendientes New value of property vectorFacturasPendientes.
     */
    public void setVectorFacturasPendientes(java.util.Vector vectorFacturasPendientes) {
        this.vectorFacturasPendientes = vectorFacturasPendientes;
    }
    
    /**
     * Getter for property vectorFacturasCanceladas.
     * @return Value of property vectorFacturasCanceladas.
     */
    public java.util.Vector getVectorFacturasCanceladas() {
        return vectorFacturasCanceladas;
    }
    
    /**
     * Setter for property vectorFacturasCanceladas.
     * @param vectorFacturasCanceladas New value of property vectorFacturasCanceladas.
     */
    public void setVectorFacturasCanceladas(java.util.Vector vectorFacturasCanceladas) {
        this.vectorFacturasCanceladas = vectorFacturasCanceladas;
    }
    
    /**
     * Getter for property proveedor.
     * @return Value of property proveedor.
     */
    public com.tsp.operation.model.beans.Proveedor getProveedor() {
        return proveedor;
    }
    
    /**
     * Setter for property proveedor.
     * @param proveedor New value of property proveedor.
     */
    public void setProveedor(com.tsp.operation.model.beans.Proveedor proveedor) {
        this.proveedor = proveedor;
    }
    
    /**
     * Getter for property vectorPlanillasPendientes.
     * @return Value of property vectorPlanillasPendientes.
     */
    public java.util.Vector getVectorPlanillasPendientes() {
        return vectorPlanillasPendientes;
    }
    
    /**
     * Setter for property vectorPlanillasPendientes.
     * @param vectorPlanillasPendientes New value of property vectorPlanillasPendientes.
     */
    public void setVectorPlanillasPendientes(java.util.Vector vectorPlanillasPendientes) {
        this.vectorPlanillasPendientes = vectorPlanillasPendientes;
    }
    
    /**
     * Getter for property vectorLiquidaciones.
     * @return Value of property vectorLiquidaciones.
     */
    public java.util.Vector getVectorLiquidaciones() {
        return vectorLiquidaciones;
    }
    
    /**
     * Setter for property vectorLiquidaciones.
     * @param vectorLiquidaciones New value of property vectorLiquidaciones.
     */
    public void setVectorLiquidaciones(java.util.Vector vectorLiquidaciones) {
        this.vectorLiquidaciones = vectorLiquidaciones;
    }
    
    /**
     * Getter for property factura.
     * @return Value of property factura.
     */
    public com.tsp.operation.model.beans.CXP_Doc getFactura() {
        return factura;
    }
    
    /**
     * Setter for property factura.
     * @param factura New value of property factura.
     */
    public void setFactura(com.tsp.operation.model.beans.CXP_Doc factura) {
        this.factura = factura;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public com.tsp.operation.model.beans.Planilla getPlanilla() {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(com.tsp.operation.model.beans.Planilla planilla) {
        this.planilla = planilla;
    }
        
}

