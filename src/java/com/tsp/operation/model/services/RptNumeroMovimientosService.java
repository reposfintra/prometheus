/*
 * RptNumeroMovimientosService.java
 *
 * Created on 12 de septiembre de 2005, 11:11
 */

package com.tsp.operation.model.services;
import com.tsp.operation.model.DAOS.*;
import java.util.*;
import java.sql.*;
/**
 *
 * @author  Henry
 */
public class RptNumeroMovimientosService {
    RptNumeroMovimientosPlaDAO dao = new RptNumeroMovimientosPlaDAO();
    /** Creates a new instance of RptNumeroMovimientosService */
    public RptNumeroMovimientosService() {
    }
    public Vector getVectorRpts() {
        return dao.getVectorRpts();
    }
    public void generarRptNumeroMovimienots(String dstrct_code, String fecini, String fecfin) throws SQLException {
        dao.generarRptNumeroMovimienots(dstrct_code, fecini, fecfin);
    }
}
