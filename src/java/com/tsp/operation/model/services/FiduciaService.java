
package com.tsp.operation.model.services;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



import com.tsp.operation.model.beans.*;
import java.util.*;
import java.util.Date;

import java.sql.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.Model;
import com.tsp.util.Util;
import java.text.*;

import com.tsp.util.LogWriter;
import com.aspose.cells.*;
import java.io.*;



/**
 *
 * @author Alvaro
 */
public class FiduciaService {

    private FiduciaDAO fiduciaDao;



    /** Creates a new instance of ConsorcioService */
    public FiduciaService() {
        fiduciaDao = new FiduciaDAO();
    }
    public FiduciaService(String dataBaseName) {
        fiduciaDao = new FiduciaDAO(dataBaseName);
    }
    

    /** Crea una lista de ofertas pendientes de facturar a los clientes */
    public void getFacturaNM()throws SQLException{
        fiduciaDao.getFacturaNM();
        
    }    
    

    /** Crea una lista de PMs */
    public void getFacturaPM(String fiduciaria, String tipo)throws SQLException{
        fiduciaDao.getFacturaPM(fiduciaria, tipo);

    } 

    public List getListaFacturaNM() throws SQLException{
        return fiduciaDao.getListaFacturaNM();
    }

    public String crearCabeceraPM(String distrito, String TIPO_DOCUMENTO_FAC, String SIGLA_PM, String facturaMaestra, String login, LogWriter logWriter)   throws SQLException {
        
        String sql = "";
        return sql;
    } 
    
    
    
    
    public String insertarFacturaCabeceraCxC(FacturaCabeceraCxC cabecera, LogWriter logWriter)throws SQLException{
        return fiduciaDao.insertarFacturaCabeceraCxC(cabecera, logWriter);
    }
    
    public String insertarFacturaDetalleCxC(FacturaDetalleCxC detalle, LogWriter logWriter)throws SQLException{
        return fiduciaDao.insertarFacturaDetalleCxC(detalle,  logWriter);
    }
    
    


    public String insertarFacturaPM(String distrito, String documento, int numeroItems, String fiduciaSeleccionada, String tipoDocumento, String siglaFactura, String usuario, String fechaCreacion,
                                    String cmc , LogWriter logWriter)throws SQLException{
        return fiduciaDao.insertarFacturaPM(distrito, documento, numeroItems, fiduciaSeleccionada, tipoDocumento, siglaFactura, usuario, fechaCreacion, cmc, logWriter);
    }    
    

        
    public String insertarFacturaDetallePM(FacturaDetalleCxC itemDetalle, LogWriter logWriter)throws SQLException{        
        return fiduciaDao.insertarFacturaDetallePM(itemDetalle, logWriter);
    }    
        
 

  public List seleccionaConceptoNM(String distrito,  String documento, int numero_item, String descripcion_concepto, String condicion, String tipoDocumento, String siglaFactura, String usuario, String fechaCreacion,
                                        String cmc , String cuenta_contable,  LogWriter logWriter)throws SQLException{
        return fiduciaDao.seleccionaConceptoNM( distrito,   documento,  numero_item,  descripcion_concepto,  condicion,  tipoDocumento,  siglaFactura,  usuario,  fechaCreacion,
                                         cmc ,  cuenta_contable,  logWriter);
    }     
    
    
    
    
 
    public String insertarIngresoPM(String distrito,  String documento, String tipoDocumento, String tipoDocumentoIngreso, String usuario, String fechaCreacion,
                                    String cuentaIngreso, String cmc , LogWriter logWriter)throws SQLException{
        return fiduciaDao.insertarIngresoPM(distrito,   documento,  tipoDocumento,  tipoDocumentoIngreso,  usuario,  fechaCreacion,
                                            cuentaIngreso,  cmc ,  logWriter);
    }
    
    public String insertarIngresoDetallePM(String distrito,  String documento, String tipoDocumento, String tipoDocumentoIngreso, String usuario, String fechaCreacion,
                                    String cuentaIngreso, LogWriter logWriter)throws SQLException{    
        return fiduciaDao.insertarIngresoDetallePM( distrito,   documento,  tipoDocumento,  tipoDocumentoIngreso,  usuario,  fechaCreacion,
                                     cuentaIngreso,  logWriter) ;  
    }
    
 
    
 
    public String actualizarFacturaNM(String  distrito,  String documento, String tipoDocumento, String usuario, LogWriter logWriter)throws SQLException{    
    
        return fiduciaDao.actualizarFacturaNM( distrito,  documento,  tipoDocumento,  usuario,  logWriter);
    }
    
    public String actualizarFacturaRefNM(String  distrito,  String documento, String tipoDocumento, String usuario, LogWriter logWriter)throws SQLException{    
    
        return fiduciaDao.actualizarFacturaRefNM( distrito,  documento,  tipoDocumento,  usuario,  logWriter);
    }
    
    public String retornarFiducia(String distrito,  String documento, String tipoDocumento, String usuario, LogWriter logWriter) throws SQLException {
        return fiduciaDao.retornarFiducia(distrito, documento, tipoDocumento, usuario, logWriter);
    }
    
    public String fintraaFiducia(String distrito,  String documento, String tipoDocumento, String usuario, String fiducia) throws SQLException {
        return fiduciaDao.fintraaFiducia(distrito, documento, tipoDocumento, fiducia, usuario);
    }
}




