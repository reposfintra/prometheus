/********************************************************************
 *      Nombre Clase.................   IndicadoresDAO.java
 *      Descripci�n..................   Service del archivo tblindicadores
 *      Autor........................   Ing. Tito Andr�s Maturana De La Cruz
 *      Fecha........................   2 de marzo de 2006, 10:10 AM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.services;

/**
 *
 * @author  Ing. Tito Andr�s Maturana De La Cruz
 */

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

public class IndicadoresService {
    private IndicadoresDAO dao;
    private List indicadores;
    private String codeJSOpciones;
    //private List    ListaFolder;
    private List    ListaTraza;
    private String  raiz;
    
    /** Crea una nueva instancia de  IndicadoresService */
    public IndicadoresService() {
        dao = new IndicadoresDAO();
        reiniciar();
        //opcionesMenu = null;
        codeJSOpciones = "";
        raiz = "0";
    }
    
    /**
     * Carga el �rbol
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cia C�digo del distrito
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void load(String cia)  throws Exception {
        searchMenu(cia);
        generateMenu();
    }
    
    /**
     * Busca el �rbol
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cia C�digo del distrito
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void searchMenu(String cia) throws Exception{
        this.indicadores = dao.mostrarContenido(cia);
    }
    
    /**
     * Genera el c�digo Java Script del �rbol
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cia C�digo del distrito
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void generateMenu() throws Exception{
        try{
            if (this.indicadores!=null){
                Iterator it = this.indicadores.iterator();
                String code = "";
                String sw = "";
                int level   = 0;
                while(it.hasNext()){
                    Indicador ind = (Indicador) it.next();
                    code += (ind.getId_folder().equalsIgnoreCase("Y")? this.codeFolder(ind) : this.codeNodo(ind));
                }
                this.codeJSOpciones = code;
            }
        }
        catch(Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * Genera el c�digo Java Script de los nodos de tipo padre
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param tmp Instancia de <code>Indicador</code>
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String codeFolder(Indicador tmp) throws Exception{
        String js = "";
        try{
            String var = (tmp.getCodigo().equals("0")?"menu": "op" +tmp.getCodigo());
            String padreName = (tmp.getCodpadre().equals("0")?"menu": "op" +tmp.getCodpadre());
            
            js=
            "\n\t  " + padreName +".addItem('"+ tmp.getDescripcion() +"','','text','',''); "+
            "\n\t  var " + var +" = null;                               "+
            "\n\t  " + var +"     = new MTMenu();                       "+
            "\n\t  " + padreName +".makeLastSubmenu("+ var +",false);   ";
        }
        catch(Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        return js;
    }
    
    /**
     * Genera el c�digo Java Script de los nodos de tipo hijo
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param tmp Instancia de <code>Indicador</code>
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String codeNodo(Indicador tmp) throws Exception{
        String js = "";
        try{
            String padreName = (tmp.getCodpadre().equals("0")?"menu": "op" + tmp.getCodpadre());
            js =  "\n\t  " + padreName +".addItem('" + tmp.getDescripcion()+"','indicadoresindex.jsp','text','',''); \n";
        }
        catch(Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        return js;
    }
    
    /**
     * Getter for property codeJSOpciones.
     * @return Value of property codeJSOpciones.
     */
    public java.lang.String getCodeJSOpciones() {
        return codeJSOpciones;
    }
    
    /**
     * Setter for property codeJSOpciones.
     * @param codeJSOpciones New value of property codeJSOpciones.
     */
    public void setCodeJSOpciones(java.lang.String codeJSOpciones) {
        this.codeJSOpciones = codeJSOpciones;
    }
    
    /**
     * Elimina un registro en el archivo tblindicadores
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void insertDefault(String cia) throws SQLException{
        dao.insertDefault(cia);
    }
    
    public void reiniciar(){
        //this.ListaFolder = null;
        this.setLista(null);
        this.ListaTraza = null;
        this.raiz = "0";
    }
    
    public void buscarHijos(String cia, String id) throws SQLException{
        dao.buscarHijos(cia, id);
    }
    
    public List mostrarContenido(String cia, String codigo) throws SQLException{
        return dao.mostrarContenido(cia, codigo);
    }
    
    /**
     * Getter for property lista.
     * @return Value of property lista.
     */
    public java.util.List getLista() {
        return dao.getLista();
    }
    
    /**
     * Setter for property lista.
     * @param lista New value of property lista.
     */
    public void setLista(java.util.List lista) {
        dao.setLista(lista);
    }
    
    /**
     * M�todo utilizado para adicionar un elemento a la traza
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @param.......ConceptoPago tmp
     */
    public void addTraza(Indicador tmp){
        if (ListaTraza==null) ListaTraza = new LinkedList();
        ListaTraza.add(tmp);
    }
    
    /**
     * M�todo utilizado para remover un elemento en la traza
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @see.........ConceptoPagoDAO.java
     */
    public void removeTraza(){
        if (ListaTraza!=null && ListaTraza.size()>0) ListaTraza.remove(ListaTraza.size()-1);
    }
    
    public Indicador getUTraza(){
        return ((ListaTraza!=null && ListaTraza.size()>0)? (Indicador) ListaTraza.get(ListaTraza.size()-1):null);
    }
    
    public void actualizarContenido() throws Exception {
        Indicador indc = getUTraza();
        //ListaFolder = dao.mostrarContenido(  ( indc!=null? indc.getCodigo() : "0" ) );
        this.setLista( dao.mostrarContenido(  indc!=null ? indc.getCodigo() : "0" ) );
    }
    
    public String getTrazado(){
        String traza = "/";
        if (ListaTraza!=null){
            for (int i=0;i<ListaTraza.size();i++){
                Indicador cp = (Indicador) ListaTraza.get(i);
                traza += cp.getDescripcion() + "/";
            }
        }
        return traza;
    }
    
    /**
     * Getter for property ListaTraza.
     * @return Value of property ListaTraza.
     */
    public java.util.List getListaTraza() {
        return ListaTraza;
    }
    
    /**
     * Setter for property ListaTraza.
     * @param ListaTraza New value of property ListaTraza.
     */
    public void setListaTraza(java.util.List ListaTraza) {
        this.ListaTraza = ListaTraza;
    }
    
    /**
     * Getter for property raiz.
     * @return Value of property raiz.
     */
    public java.lang.String getRaiz() {
        return raiz;
    }
    
    /**
     * Setter for property raiz.
     * @param raiz New value of property raiz.
     */
    public void setRaiz(java.lang.String raiz) {
        this.raiz = raiz;
    }
    
    /**
     * Inserta un registro en el archivo tblindicadores
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void insertar() throws SQLException{
        dao.insertar();
    }
    
    /**
     * Getter for property indicador.
     * @return Value of property indicador.
     */
    public com.tsp.operation.model.beans.Indicador getIndicador() {
        return dao.getIndicador();
    }
    
    /**
     * Setter for property indicador.
     * @param indicador New value of property indicador.
     */
    public void setIndicador(com.tsp.operation.model.beans.Indicador indicador) {
        dao.setIndicador(indicador);
    }
    
    /**
     * Actualiza un registro en el archivo tblindicadores
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void actualizar() throws SQLException{
        dao.actualizar();
    }
    
    /**
     * Obtiene los indicadores que son del tipo padres
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param con Conexi�n a la Base de Datos
     * @param cia C�digo del Distrito
     * @param id C�digo del padre
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void soloPadres(String cia) throws SQLException{
        dao.setPadres(null);
        dao.soloPadres(null, cia, this.raiz);
    }
    
    
    /**
     * Getter for property padres.
     * @return Value of property padres.
     */
    public java.util.List getPadres() {
        return dao.getPadres();
    }
    
    /**
     * Setter for property padres.
     * @param padres New value of property padres.
     */
    public void setPadres(java.util.List padres) {
        dao.setPadres(padres);
    }
    
    /**
     * Elimina un registro en el archivo tblindicadores
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void delete() throws SQLException{
        Indicador idc = this.getIndicador();
        
        this.buscarHijos(idc.getDistrict(), idc.getCodigo());
        List lst = this.getLista();
        
        if( lst.size()>0 ){
            for( int i=0; i<lst.size(); i++){
                Indicador idc2 = (Indicador) lst.get(i);
                this.setIndicador(idc2);
                this.delete();
            }
        }
        
        this.setIndicador(idc);
        dao.delete();
    }
}
