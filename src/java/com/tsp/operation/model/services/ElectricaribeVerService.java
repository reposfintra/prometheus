/*
 * creacionCompraCarteraService.java
 *
 * Created on 1 de enero de 2008, 05:58 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.tsp.operation.model.services;
import java.util.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;
import java.sql.*;
import com.tsp.operation.model.Model;
import com.tsp.operation.model.threads.HSendMail2; 

/**
 *
 * @author NAVI
 */
public class ElectricaribeVerService {
    private Oferta oferta;
    private ArrayList accords;
    private electricaribeDAO eledao;
    
    private ArrayList accords_modified;
    private ArrayList contratisticas;
    private ArrayList entidades;
    
    public void CargarArrays(String id_orden)
    {   try
        {   accords = eledao.CargarArrayAccords(id_orden);
            oferta  = eledao.CargarOferta(id_orden);
            //.out.println("cargoo");
        }
        catch(Exception e)
        {   System.out.println("assssw"+e.toString());
        }
    }
    
    public ElectricaribeVerService () {
        oferta= new Oferta();
        accords= new ArrayList();
        eledao= new electricaribeDAO();
        try{
            //contratisticas=eledao.getContratistas();
            entidades=eledao.getEntidades(); 
        }catch(Exception e){
            System.out.println("error en ElectricaribeVerService:"+e.toString()+"__"+e.getMessage());
        } 
    }
    
    public ElectricaribeVerService (String dataBaseName) {
        oferta= new Oferta();
        accords= new ArrayList();
        eledao= new electricaribeDAO(dataBaseName);
        try{
            //contratisticas=eledao.getContratistas();
            entidades=eledao.getEntidades(); 
        }catch(Exception e){
            System.out.println("error en ElectricaribeVerService:"+e.toString()+"__"+e.getMessage());
        } 
    }
    
    public ArrayList getAccords()
    {   return accords;
    }

    public Oferta getOferta()
    {   return this.oferta;
    }
    
    public void setAccord(ArrayList accords)
    {   this.accords=accords;
    }
    
    public void addAccord(int index,Accord accord)
    {   if(index<accords.size())
        {        this.accords.remove(index);
        }
        this.accords.add(index, accord);
    }
    
    public void registrarCambioEnAccion(int index,Accord accord){
        int sw=0,i=0;
        if (accords_modified==null){accords_modified=new ArrayList();}
        while (i<accords_modified.size()){
            if (accord.getIdAccion().equals(((Accord)accords_modified.get(i)).getIdAccion())){
                sw=1;                
            }
            i=i+1;
        }
        if (sw==0){
            accords_modified.add(accord);
        }
    }
    
    public void delAccord(int index)
    {   if(index<accords.size())
        {        this.accords.remove(index);
        }
    }
    
    public void setOferta(Oferta oferta) throws Exception
    {   this.oferta=oferta;
    }
    
    public void reset()
    {   oferta= new Oferta();
        accords= new ArrayList();
    }
    
    public String insertarMs(String usuario,String tipo) throws Exception    {    
        //.out.println("insertarMs");
        String respuesta=eledao.insertarMs(oferta ,accords,usuario,tipo);         
        enviarMsg( oferta , accords, usuario,tipo);
        return respuesta;
    }
    
    public java.util.TreeMap getPreMultiservicios()throws Exception
    {
        return  eledao.getPreMultiservicios();
    }
    
    public java.util.TreeMap getClientes()throws Exception
    {
        return  eledao.getClientes();
    }
    
    /*public String enviarMsg(Oferta ofertax ,ArrayList accionesx,String usuariox,String tipo) throws Exception    {  
        try{
            
            if (tipo.equals("EM")){            
                for (int j=0;j<accionesx.size();j++){
                    Accord ac=(Accord)accionesx.get(j);
                    HSendMail2 hSendMail2=new HSendMail2();
                    hSendMail2.start("imorales@fintravalores.com", getEmailContratistaApp(ac.getIdContratista()), "", "", "nueva emergencia", "Se ha creado una emergencia para el cliente "+ofertax.getNombreCliente()+" ( "+ofertax.getIdCliente()+" ) . Descripci�n: "+ofertax.getDetalleInconsistencia()+". ("+ac.getIdContratista()+"). Por favor consultar en http://www.fintra.co",usuariox);            
                }
            }
            if (tipo.equals("CS")){            
                if(!(oferta.getIdOrden()==null) && accords_modified!=null){
                    for (int j=0;j<accords_modified.size();j++){
                        Accord ac=(Accord)accords_modified.get(j);
                        HSendMail2 hSendMail2=new HSendMail2();
                        hSendMail2.start("imorales@fintravalores.com", getEmailContratistaApp(ac.getIdContratista()), "", "", "accion modificada", "Se ha modificado/creado una situaci�n para el cliente "+ofertax.getNombreCliente()+" ( "+ofertax.getIdCliente()+" ) . Descripci�n: "+ac.getAcciones()+". ("+ac.getIdContratista()+"). Por favor consultar en http://www.fintra.co",usuariox);            
                    }
                }else{
                    //.out.println("enviarMsg");
                    String contrati="";
                    int sw=0;           
                    sw=1;
                    HSendMail2 hSendMail2=new HSendMail2();
                    hSendMail2.start("imorales@fintravalores.com", "imorales@fintravalores.com", "", "", "nueva solicitud", "Se ha creado una solicitud para el cliente "+ofertax.getNombreCliente()+" ( "+ofertax.getIdCliente()+" ) . Descripci�n: "+ofertax.getDetalleInconsistencia()+" . Por favor consultar en http://www.fintra.co",usuariox);            
                }
            }

            
        }catch(Exception ee){
            System.out.println("error en envio de msg:"+ee.toString()+"__"+ee.getMessage());
        }
        return "";        
    }*/
    
    public String getCorreoCC(String contratista){
        //.out.println("contratistaaa"+contratista);
        String respuesta="imorales@fintravalores.com";
        if (contratista.equals("CC002")){respuesta="imorales@fintravalores.com";}//SENTEL
        if (contratista.equals("CC008")){respuesta="imorales@fintravalores.com";}//TRACOL
        if (contratista.equals("CC016")){respuesta="imorales@fintravalores.com";}//NORCONTROL
        if (contratista.equals("CC017")){respuesta="imorales@fintravalores.com";}//QIELEC
        if (contratista.equals("CC027")){respuesta="imorales@fintravalores.com";}//ECA
        if (contratista.equals("CC036")){respuesta="imorales@fintravalores.com";}//TRADELCA
        if (contratista.equals("CC038")){respuesta="imorales@fintravalores.com";}//DELELCO
        return respuesta;
    }
    
    public String getEmailContratistaApp(String cod){
       String respuesta="";
       try{
           ArrayList contratis=contratisticas;
           int w=0,sw=0;
           while (w<contratis.size() && sw==0){
               String[] contratistax=(String[])contratis.get(w);
               if (contratistax[0].equals(cod)){
                    respuesta=contratistax[2];
                    sw=1;
               }
               w=w+1;
           }
       }catch(Exception e){
           System.out.println("error en getEmailContratistaApp"+e.toString()+"__"+e.getMessage());
       }
       return respuesta;
       /*       estado[0]=rs.getString("codigo");       estado[1]=rs.getString("estado");                       listEstados.add(estado);               */
      
   }
    
    public String enviarMsg(Oferta ofertax ,ArrayList accionesx,String usuariox,String tipo) throws Exception    {  
        try{
            String[] datos_client=eledao.getDatosClient(ofertax.getIdCliente());
            ofertax.setDirClient(datos_client[7]);
            
            if (tipo.equals("EM")){ 
                if (accionesx.size()>0){
                    Accord ac0=(Accord)accionesx.get(0);
                    HSendMail2 hSendMail20=new HSendMail2();
                    hSendMail20.start("imorales@fintravalores.com", getEmailEntidad("APPLUS"), "", "", 
                    "nueva emergencia", 
                    "APPLUS:  Se ha generado una emergencia con n�mero "+ofertax.getNumOs() +" en el cliente "+ofertax.getNombreCliente()+" "+ofertax.getIdCliente()+" ("+ofertax.getDirClient()+" "+datos_client[5]+") . " +
                    "Observaciones: \n"+ofertax.getDetalleInconsistencia()+". \nFavor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .",usuariox);            
                }
                for (int j=0;j<accionesx.size();j++){
                    Accord ac=(Accord)accionesx.get(j);
                    HSendMail2 hSendMail2=new HSendMail2();
                    hSendMail2.start("imorales@fintravalores.com", getEmailContratistaApp(ac.getIdContratista()), "", "", 
                    "nueva emergencia", 
                    ""+getNombreContratistaApp(ac.getIdContratista())+":  Se ha generado una emergencia con n�mero "+ofertax.getNumOs() +" en el cliente "+ofertax.getNombreCliente()+" "+ofertax.getIdCliente()+" ("+ofertax.getDirClient()+" "+datos_client[5]+") . " +
                    "Observaciones: \n"+ofertax.getDetalleInconsistencia()+". \nFavor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .",usuariox);            
                }
            }
            if (tipo.equals("CS")){            
                if(!(oferta.getIdOrden()==null) && accords_modified!=null){
                    for (int j=0;j<accords_modified.size();j++){
                        Accord ac=(Accord)accords_modified.get(j);
                        HSendMail2 hSendMail2=new HSendMail2();
                        //hSendMail2.start("imorales@fintravalores.com", getEmailContratistaApp(ac.getIdContratista()), "", "", "accion modificada", "Se ha modificado/creado una situaci�n para el cliente "+ofertax.getNombreCliente()+" ( "+ofertax.getIdCliente()+" ) . Descripci�n: "+ac.getAcciones()+". ("+ac.getIdContratista()+"). Por favor consultar en http://www.fintra.co",usuariox);            
                        hSendMail2.start("imorales@fintravalores.com", getEmailContratistaApp(ac.getIdContratista()), "", "", 
                        "accion", 
                        ""+this.getNombreContratistaApp(ac.getIdContratista())+":  Se ha modificado/creado una situaci�n para el cliente "+ofertax.getNombreCliente()+" "+ofertax.getIdCliente()+" ("+ofertax.getDirClient()+" "+datos_client[5]+") . Descripci�n: \n"+ac.getAcciones()+"\nFavor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  . ",usuariox);            
                    }
                }else{
                    //.out.println("");
                    String contrati="";
                    int sw=0;           
                    sw=1;
                    HSendMail2 hSendMail2=new HSendMail2();
                    hSendMail2.start("imorales@fintravalores.com", "imorales@fintravalores.com", "", "", "nueva solicitud eca", "Se ha creado una solicitud "+ofertax.getNumOs()+" para el cliente "+ofertax.getNombreCliente()+" ( "+ofertax.getIdCliente()+" ) . Descripci�n: "+ofertax.getDetalleInconsistencia()+" . Por favor consultar en http://www.fintra.co",usuariox);            
                }
            }

            
        }catch(Exception ee){
            System.out.println("error en envio de msg:"+ee.toString()+"__"+ee.getMessage());
        }
        return "";        
    }
    
    
    public String getNombreContratistaApp(String cod){
       String respuesta="";
       try{
           ArrayList contratis=contratisticas;
           int w=0,sw=0;
           while (w<contratis.size() && sw==0){
               String[] contratistax=(String[])contratis.get(w);
               if (contratistax[0].equals(cod)){
                    respuesta=contratistax[1];
                    sw=1;
               }
               w=w+1;
           }
       }catch(Exception e){
           System.out.println("error en getEmailContratistaApp"+e.toString()+"__"+e.getMessage());
       }
       return respuesta;
      
   }
    
    public String getEmailEntidad(String nombre_entidad){
       String respuesta="";
       try{
           ArrayList entis=entidades;
           int w=0,sw=0;
           while (w<entis.size() && sw==0){
               String[] entidadx=(String[])entis.get(w);
               if (entidadx[0].equals(nombre_entidad)){
                    respuesta=entidadx[2];
                    sw=1;
               }
               w=w+1;
           }
       }catch(Exception e){
           System.out.println("error en getEmailEntidad"+e.toString()+"__"+e.getMessage());
       }
       return respuesta;      
      
   }
    
    public String[] getDatosClient(String idcli) throws Exception{        
        return eledao.getDatosClient(idcli);
    }
    
    
}