/***********************************************************************************
 * Nombre clase : ............... ActividadPlanServices.java                       *
 * Descripcion :................. Clase que maneja los Service                     *
 * Autor :....................... Ing. Diogenes Antonio Bastidas Morales           *
 * Fecha :........................ 12 de septiembre de 2005, 12:42 PM              *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/



package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;


public class ActividadPlanService {
    private ActividadPlanDAO actpland;
    /** Creates a new instance of ActividadPlanServices */
    public ActividadPlanService() {
        actpland = new ActividadPlanDAO();
    }
    
    /**
     * Metodo obtVecActividad, retorna el vector de actividades
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public Vector ObtActividadPlan (){
         return actpland.obtVecActividadPlan();
    }
    /**
     * Metodo BuscarCLientexPlanilla, busca los clientes que tiene la planilla
     * @param: numero de la planilla
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean BuscarCLientexPlanilla(String numpla) throws SQLException {
        try{
            return actpland.BuscarCLientexPlanilla(numpla);
        }catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo buscarInfoplanilla, busca la informacion de la planilla
     * @param: numero de la planilla
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
     public void buscarInfoplanilla(String numpla) throws SQLException {
         actpland.buscarInfoplanilla(numpla);
     }
    /**
     * Metodo getPlan, retorna la informacion de la planilla
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
     public com.tsp.operation.model.beans.Planilla getPlan() {
        return actpland.getPlan();
    }
}
