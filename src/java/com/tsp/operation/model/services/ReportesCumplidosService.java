/*
 * ReportesService.java
 *
 * Created on 03 de octubre de 2005, 11:29
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;


/**
 *
 * @author  LEONARDO PARODY
 */
public class ReportesCumplidosService {
    
    private ReportesCumplidosDAO ReporteDataAccess; 
    
    public ReportesCumplidosService() {
        
        ReporteDataAccess = new ReportesCumplidosDAO(); 
        
    }
     
   public List ObtenerReporte()throws SQLException {
       
       List reporte = null;
       reporte = ReporteDataAccess.ObtenerReporte();
       return reporte;
       
   }
   public void ReportePlaRemPorFecha (String FechaI,String FechaF, String Agencia)throws SQLException{
      
       try{
           ReporteDataAccess.ReportePlaRemPorFecha(FechaI, FechaF, Agencia);
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
       
   }
   
}