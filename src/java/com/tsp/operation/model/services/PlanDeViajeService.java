 /***************************************
    * Nombre Clase ............. PlanDeViajeService.java
    * Descripci�n  .. . . . . .  Controla el Manejo de plan de viaje de la planilla
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  07/10/2005
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/

package com.tsp.operation.model.services;



import java.io.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;




public class PlanDeViajeService {
    
    private PlanDeViajeDAO PlanViajeDataAccess;
    private PlanDeViaje    planViaje;    
    private String         btnSave;
    private String         btnModify;
    private String         btnDelete;    
    private String         ENEBLED;
    private String         DISABLED;
    private List           vias;
    
    
     
     
    
    public PlanDeViajeService() {
        PlanViajeDataAccess = new PlanDeViajeDAO();
        vias                = null;
        init();
    }
    
    
    
    
    /**
     * Metodo que setea variables
     * @autor: ....... Fernel Villacob
     * @version ...... 1.0
     */ 
    private void reset(){
        this.planViaje = null;
        this.vias      = null;
    }
    
    
    
 
       
    
 /**
 * Metodo que inicializa los botones
 * @autor: ....... Fernel Villacob
 * @version ...... 1.0
 */   
    
   public void init(){
        ENEBLED   = "";
        DISABLED  = " visibility='hidden' ";
        btnSave   = DISABLED;
        btnModify = DISABLED;
        btnDelete = DISABLED;
    }
     
   
   
      
    
 /**
 * Metodo que  habilita boton
 * @autor: ....... Fernel Villacob
 * @param ........ String opcion
 * @version ...... 1.0
 */   
      
    public void formatBtn (String opcion){
         init();
         if(opcion.equals("SAVE")){
            this.btnSave = this.ENEBLED;
         }

         if(opcion.equals("SEARCH")){
            this.btnSave   = this.DISABLED;
            this.btnModify = this.DISABLED;
            this.btnDelete = this.DISABLED;
         }

         if(opcion.equals("MODIFY")){
            this.btnModify = this.ENEBLED;
         }
         if(opcion.equals("DELETE")){
            this.btnDelete = this.ENEBLED; 
         }
    }

    
    
    
    
 // SQL:
    
    

   /**
     * Metodo que  busca el plan de viaje Ya creado
     * @autor: ....... Fernel Villacob
     * @param ........ String oc, String distrito
     * @version ...... 1.0
     */   
    public PlanDeViaje search_planViaje(String oc, String distrito) throws Exception{
        reset();
        try{
            this.planViaje = this.PlanViajeDataAccess.search(oc,distrito);
        }catch(Exception e){
            throw new Exception( e.getMessage());
        }
        return this.planViaje;
    }
    
        
    
    
   /**
     * Metodo que  forma el plan de viaje con informacion de la planilla cuando No esta creado
     * @autor: ....... Fernel Villacob
     * @param ........ String oc, String distrito
     * @version ...... 1.0
     */   
    public PlanDeViaje formar_planViaje(String oc, String distrito)throws Exception{
        reset();
        try{
            this.planViaje = this.PlanViajeDataAccess.formarPlanViaje(oc,distrito);
        }catch(Exception e){
            throw new Exception( e.getMessage());
        }
        return this.planViaje;
    }
    
    
    
    /**
     * Metodo que  guarda el Plan de viaje
     * @autor: ....... Fernel Villacob
     * @param ........ PlanDeViaje plan
     * @version ...... 1.0
     */   
    public void save(PlanDeViaje plan)throws Exception{
        try{
           this.PlanViajeDataAccess.save(plan);
        }catch(Exception e){
            throw new Exception( e.getMessage());
        }
    }
    
    
    
    
     
    /**
     * Metodo que  elimina el Plan de viaje
     * @autor: ....... Fernel Villacob
     * @param ........ PlanDeViaje plan
     * @version ...... 1.0
     */  
     public void delete(PlanDeViaje plan)throws Exception{
        try{
           this.PlanViajeDataAccess.delete(plan);
        }catch(Exception e){
            throw new Exception( e.getMessage());
        }
    }
     
    /**
     * Metodo que  actualiza el Plan de viaje
     * @autor: ....... Fernel Villacob
     * @param ........ PlanDeViaje plan
     * @version ...... 1.0
     */  
    public void modify(PlanDeViaje plan)throws Exception{
        try{
           this.PlanViajeDataAccess.modify(plan);
        }catch(Exception e){
            throw new Exception( e.getMessage());
        }
    }    
    
    // GET :
    
     /**
     * Metodo que  devuelve planViaje
     * @autor: ....... Fernel Villacob
     */  
    public PlanDeViaje getPlan() { 
        return this.planViaje; 
    }
    
    
     /**
     * Metodo que  devuelve btnSave
     * @autor: ....... Fernel Villacob
     */
    public String getBtnSave  () { 
        return this.btnSave;  
    }    
    
    /**
     * Metodo que  devuelve btnModify
     * @autor: ....... Fernel Villacob
     */
    public String getBtnModify() { 
        return this.btnModify; 
    }
    
    
     /**
     * Metodo que  devuelve btnDelete
     * @autor: ....... Fernel Villacob
     */
    public String getBtndelete() { 
        return this.btnDelete; 
    }
    
    // SET :
    
    
    /**
     * Metodo que  setea el  el Plan de viaje
     * @autor: ....... Fernel Villacob
     * @param ........ PlanDeViaje obj
     * @version ...... 1.0
     */  
    public void    setPlan(PlanDeViaje obj){ 
        this.planViaje = obj ; 
    }
    
     /**
     * Listado de Vias para esa ruta
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/  
     public List searchUbicacion( String via, String tipo)throws Exception{
         return this.PlanViajeDataAccess.searchUbicacion(via, tipo);
     }
    
}
