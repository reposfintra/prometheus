/******************************************************************************
 * Nombre clase :      ReporteCarbonService.java                              *
 * Descripcion :       Service del ReporteCarbonService.java                  *
 * Autor :             LREALES                                                *
 * Fecha :             28 de octubre de 2006, 08:00 AM                        *
 * Version :           1.0                                                    *
 * Copyright :         Fintravalores S.A.                                *
 ******************************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.util.*;
import java.io.*;
import java.sql.*;
import java.util.*;

public class ReporteCarbonService {
    
    private ReporteCarbonDAO reporte;
    
    /** Creates a new instance of ReporteCarbonService */
    public ReporteCarbonService() {
        
        reporte = new ReporteCarbonDAO ();
        
    }
    
    /** Funcion publica que obtiene el metodo getVector del DAO */
    public Vector getVectorReporte () throws Exception {
        
        return reporte.getVector();
        
    }
    
    /** Funcion publica que obtiene el metodo reporte del DAO */
    public void reporteCarbon ( boolean todos, String nit_proveedor, String fecini, String fecfin ) throws Exception {
        
        reporte.reporte ( todos, nit_proveedor, fecini, fecfin );
        
    }
    
}