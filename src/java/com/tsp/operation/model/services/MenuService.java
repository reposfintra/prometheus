/*
 * Nombre        MenuService.java
 * Autor         Ing. Sandra M. Escalante G.
 * Fecha         26 de abril de 2005, 01:43 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  Administrador
 */
public class MenuService {
    
   private MenuDAO menu;    
   private List    perfiles;
   private List    usuarios;
   private List    perfilusuarios;
   private List    usuariosperfil;
   private String  varSeparadorJS = "~";
   private String  varUsuariosJS;
   private String  varPerfilJS;
   private String  varPerfilUsuariosJS;
   private String  varUsuariosPerfilJS;
   private String  PARAM1;
   private String  PARAM2;
   private Vector opcione;
    
    /** Creates a new instance of MenuService */
    public MenuService() {
        menu           = new MenuDAO();
        perfiles       = null;
        usuarios       = null;
        perfilusuarios = null;
        usuariosperfil = null;
        PARAM1         = "1";
        PARAM2         = "2";
        
    }
    public MenuService(String dataBaseName) {
        menu           = new MenuDAO(dataBaseName);
        perfiles       = null;
        usuarios       = null;
        perfilusuarios = null;
        usuariosperfil = null;
        PARAM1         = "1";
        PARAM2         = "2";
        
    }

     /**
     * Metodo searchPerfiles, obtiene perfiles del sistema
     * @autor  Ing. Fernell Villacob
     * @see  getPerfiles(), GenerarJSPerfiles()
     * @version  1.0
     */
       public List searchPerfiles() throws SQLException {
        try{
           this.perfiles = this.menu.getPerfiles(); 
           generarJSPerfiles();
        }catch(SQLException e){ throw new SQLException(e.getMessage()); }
        return perfiles;
    }
    
    /**
     * Metodo searchUsuarios, obtiene los usuarios del sistema
     * @autor  Ing. Fernell Villacob
     * @see  getUsuarios(), GenerarJSusuarios()
     * @version  1.0
     */
    public List searchUsuarios() throws SQLException {
        try{
           //create();
           this.usuarios  = this.menu.getUsuarios(); 
           generarJSUsuarios();
        }catch(SQLException e){ throw new SQLException(e.getMessage()); }
        return usuarios;
    }
    
    /**
     * Metodo searchPerfilUsuarios, obtiene los perifiles y sus usuarios
     * @autor  Ing. Fernell Villacob
     * @see  getPerfilUsuarios(String parametro), GenerarJSPerfilUsuarios()
     * @version  1.0
     */
     public List searchPerfilUsuarios() throws SQLException {
        try{
           this.perfilusuarios  = this.menu.getPerfilUsuarios(PARAM1); 
           generarJSPerfilUsuarios();
        }catch(SQLException e){ throw new SQLException(e.getMessage()); }
        return perfilusuarios;
    }
     
    /**
     * Metodo searchUsuariosPerfil, obtiene los usuarios por perfil
     * @autor  Ing. Fernell Villacob
     * @see  getPerfilUsuarios(String parametro), GenerarJSUsuariosPerfil()
     * @version  1.0
     */
     public List searchUsuariosPerfil() throws SQLException {
        try{
           this.usuariosperfil  = this.menu.getPerfilUsuarios(PARAM2);
           generarJSUsuariosPerfil();
        }catch(SQLException e){ throw new SQLException(e.getMessage()); 
        }
        return  usuariosperfil;
    }
     
    /**
     * Metodo GenerarJSUsuariosPerfil, genera el script Usuario Perfil
     * @autor  Ing. Fernell Villacob
     * @version  1.0
     */ 
   public void generarJSUsuariosPerfil(){
        String var = "\n var UsuariosPerfilJS = '";
        if (usuariosperfil!=null)
            for (int i=0;i<usuariosperfil.size();i++){
                PerfilUsuario usu = (PerfilUsuario)  usuariosperfil.get(i);
                var +=  usu.getUsuarios() +"-"+ usu.getPerfil()+ "-"+ usu.getPerName() +"|";
            }
        var += "';";
        varUsuariosPerfilJS = var;
    }  
     
   /**
     * Metodo GenerarJSPerfilUsuarios, genera el script Perfil Usuarios
     * @autor  Ing. Fernell Villacob     
     * @version  1.0
     */
    public void generarJSPerfilUsuarios(){
        String var = "\n var PerfilUsuariosJS = '";
        if (perfilusuarios!=null)
            for (int i=0;i<perfilusuarios.size();i++){
                PerfilUsuario usu = (PerfilUsuario)  perfilusuarios.get(i);
                var +=  usu.getPerfil() +"-"+ usu.getUsuarios() +"|";
            }
        var += "';";
        varPerfilUsuariosJS = var;
    }
    
    /**
     * Metodo GenerarJSUsuarios, genera el script de Usuarios
     * @autor  Ing. Fernell Villacob     
     * @version  1.0
     */
    public void generarJSUsuarios(){
        String var = "\n var UsuariosJS = [ ";
        if (usuarios!=null)
            for (int i=0;i<usuarios.size();i++){
                Usuario usu = (Usuario)  usuarios.get(i);
                var += "\n '"+ usu.getLogin() + varSeparadorJS + usu.getNombre() +"',";
            }
        var = var.substring(0,var.length()-1) + "];";
        varUsuariosJS = var;
    }
    
    /**
     * Metodo GenerarJSPerfiles, genera el script de Perfiles
     * @autor  Ing. Fernell Villacob     
     * @version  1.0
     */
    public void generarJSPerfiles(){
        String var = "\n var GruposJS = [ ";
        if (perfiles!=null)
            for (int i=0;i<perfiles.size();i++){
                Perfil per = (Perfil)  perfiles.get(i);
                var += "\n '"+ per.getId() + varSeparadorJS + per.getNombre() +"',";
            }
        var = var.substring(0,var.length()-1) + "];";
        varPerfilJS = var;
    }
    
    
    /**
     * Metodo searchPerfilUsuarios, obtiene los perifiles y sus usuarios
     * @autor  Ing. Fernell Villacob
     * @see  getPerfilUsuarios(String parametro), GenerarJSPerfilUsuarios()
     * @version  1.0
     */
    public void insert (String []ListaUsuarios, String []ListaGrupos, String user)throws Exception{
        try{
            this.menu.insert(ListaGrupos,ListaUsuarios, user );
        }catch (Exception ex){
            throw new Exception ("Error en Insert [MenuService] ... \n" + ex.getMessage());
        }
    }
   
    
    /**
     * Metodo getPerfiles, obtiene la lista de perfiles
     * @autor  Ing. Fernell Villacob     
     * @version  1.0
     */
    public List getPerfiles()  { 
        return this.perfiles;       
    }
    
    /**
     * Metodo getUsuarios, obtiene la lista de usuarios
     * @autor  Ing. Fernell Villacob
     * @version  1.0
     */
    public List getUsuarios() {
        return this.usuarios;       
    }
    
    /**
     * Metodo getVarJSUsuario, obtiene el script de usuarios
     * @autor  Ing. Fernell Villacob
     * @version  1.0
     */
    public String getVarJSUsuario()     { 
        return this.varUsuariosJS;  
    }
    
    
    /**
     * Metodo getVarJSPerfil, obtiene script de perfiles
     * @autor  Ing. Fernell Villacob
     * @version  1.0
     */
    public String getVarJSPerfil()      { 
        return this.varPerfilJS;    
    }
    
   /**
     * Metodo getVarJSPerUsuario, obtiene script de perfiles - usuarios
     * @autor  Ing. Fernell Villacob
     * @version  1.0
     */
    public String getVarJSPerUsuario()  { 
        return this.varPerfilUsuariosJS;  
    }
    
    /**
     * Metodo getVarJSUsuarioPer, obtiene script de perfiles - usuarios
     * @autor  Ing. Fernell Villacob
     * @version  1.0
     */
    public String getVarJSUsuarioPer()  { 
        return this.varUsuariosPerfilJS;  
    }
    
    /**
     * Metodo getVarJSSeparador, obtiene constante varSeparatorJS
     * @autor  Ing. Fernell Villacob
     * @version  1.0
     */
    public String getVarJSSeparador()   { return "\n var SeparadorJS = '" + this.varSeparadorJS + "';";  }
    
    /**
     * Metodo getVMenu, obtiene vector menu
     * @autor  Ing. Sandra M escalante G
     * @see getVMenu()
     * @version  1.0
     */
    public  Vector getVMenu(){
        return menu.getVMenu();
    }
    
    /**
     * Metodo buscarOpcionMenuxIdopcion, busca una opcion dado un codigo
     * @autor  Ing. Sandra M escalante G
     * @see buscarOpcionMenuxIdopcio(int ido)
     * @version  1.0
     */
    public Menu buscarOpcionMenuxIdopcion(int ido) throws SQLException {
        return menu.buscarOpcionMenuxIdopcion(ido);
    }
    
    /**
     * Metodo obtenerPadresRaizMenu, obtiene las opciones (carpetas) en la raiz del menu
     * @autor  Ing. Sandra M escalante G
     * @see obtenerPadresRaizMenu()
     * @version  1.0
     */
    public void obtenerPadresRaizMenu ()throws SQLException{
        try{
            menu.obtenerPadresRaizMenu();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }         
    
    /**
     * Metodo obtenerTOpciones, obtiene todas las opciones del sistema
     * @autor  Ing. Sandra M escalante G
     * @see obtenerTOpciones()
     * @version  1.0
     */
    public void obtenerTOpciones () throws SQLException {
        try{
            menu.obtenerTOpciones();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo nuevaOpcionMenu, ingresa una nueva opcion al sistema
     * @autor  Ing. Sandra M escalante G
     * @see nuevaOpcionMenu(Menu nmenu)
     * @version  1.0
     */
    public int nuevaOpcionMenu(Menu nmenu)throws SQLException{
        try{
            menu.setMenu(nmenu);
            return menu.nuevaOpcionMenu();            
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }   
    }
    
    /**
     * Metodo modificarOpcionMenu, actualiza una opcion
     * @autor  Ing. Sandra M escalante G
     * @see modificarOpcionMenu (Menu nmenu)
     * @version  1.0
     */
     public void modificarOpcionMenu (Menu nmenu)throws SQLException{
        try{
            menu.setMenu(nmenu);
            menu.updateOpcionMenu();            
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }   
    }
     
    /**
     * Metodo anularOpcionMenu, anula una opcion del sistema
     * @autor  Ing. Sandra M escalante G
     * @see anularOpcionMenu( Menu nmenu)
     * @version  1.0
     */ 
    public void anularOpcionMenu ( Menu nmenu )throws SQLException{
        try{
            menu.setMenu(nmenu);
            menu.anularOpcionMenu();
        }catch(SQLException e){
            throw new SQLException(e.getMessage());
        }   
    }
    
    /**
     * Metodo obtenerOpcion, obtiene una opcion dado un codigo
     * @autor  Ing. Sandra M escalante G
     * @see obtenerOpcion ( int ido )
     * @version  1.0
     */
    public void obtenerOpcion(int ido) throws SQLException{
        try{
            menu.obtenerOpcion(ido);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }   
    }
    
    /**
     * Metodo getMenuOpcion, obtiene un objeto Menu ya instanciado
     * @autor  Ing. Sandra M escalante G
     * @see getMenuOpcion()
     * @version  1.0
     */
    public Menu getMenuOpcion () throws SQLException{
        return menu.getMenuOpcion();
    }
    
    /**
     * Metodo obtenerPadreMenuxIDopcion, obtiene la opcion padre de una opcion dada
     * @autor  Ing. Sandra M escalante G
     * @see obtenerPadreMenuxIDpcion( int ido )
     * @version  1.0
     */
    public void obtenerPadreMenuxIDopcion ( int ido )throws SQLException{
        try{
            menu.obtenerPadreMenuxIDopcion(ido);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }

    /**
     * Metodo obtenerHijos, obtiene las opciones (hijas) de una opcion
     * @autor  Ing. Sandra M escalante G
     * @see obtenerHijos( in idpadres)
     * @version  1.0
     */
    public  void obtenerHijos ( int idpadres )throws SQLException{
        try{
            menu.obtenerHijos(idpadres);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo cargarMenuxPerfil, obtiene las opciones por perfil
     * @autor  Ing. Sandra M escalante G
     * @see cargarMenuxPerfil ( String prf )
     * @version  1.0
     */
    public  void cargarMenuxPerfil(String prf)throws SQLException{
        try{
            menu.cargarMenuxPerfil(prf);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo obtenerPadres, obtiene solo las opciones (carpetas)
     * @autor  Ing. Sandra M escalante G
     * @see obtenerPadres ()
     * @version  1.0
     */
    public void obtenerPadres ( )throws SQLException{
        try{
            menu.obtenerPadres();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo cambiarNivelOpcion, modifica el nivel de una opcion
     * @autor  Ing. Sandra M escalante G
     * @see cambiarNivelOpcion ( int opcion, int nivel )
     * @version  1.0
     */
    public void cambiarNivelOpcion ( int opcion, int nivel) throws SQLException{
        try{
           menu.cambiarNivel(opcion, nivel);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }    
     
    /**
     * Metodo getContenidoMenu , Metodo que Busca si existen las paginas de ayuda. De acuerdo 
     * al resultado de la busqueda genera un codigo html especifico
     * @autor : Ing. Henry A. Osorio Gonzalez     
     * @return : Array tipo String de dos posiciones, una que guarda el codigo html de una imagen y
     *           la otra el codigo html de un div.
     * @version : 1.0
     */
    public String [] getContenidoMenu(String base, String pagina, String ruta) {    
        String [] datos = new String[2];        
        //////System.out.println("RUTA: "+pagina);
        int po = ruta.indexOf(".jsp");
        String ruta1 = ruta.substring(0,po)+"HelpD.jsp";
        ////System.out.println(ruta1);
        File  file1 = new File( ruta1 );
        String ruta2 = ruta.substring(0,po)+"HelpF.jsp";
        File  file2 = new File( ruta2 );
        String ruta3 = ruta.substring(0,po)+"HelpE.jsp";
        File  file3 = new File( ruta3 );
        ////System.out.println(file1.exists());
        boolean sw1 = false;
        boolean sw2 = false;
        boolean sw3 = false;
        if(file1.exists()) {
            sw1 = true;
        }
        if(file2.exists()) {
            sw2 = true;
        }
        if(file3.exists()) {
            sw3 = true;
        }        
        int pos = pagina.indexOf(".jsp");
        String desc  =  pagina.substring(0,pos)+"HelpD.jsp";
        String func  =  pagina.substring(0,pos)+"HelpF.jsp";
        String ent   =  pagina.substring(0,pos)+"HelpE.jsp";  
        String image =  "";
        String op1   =  "", op2 = "", op3 = "" ;
        if (sw1 || sw2 || sw3){
            image = "<img src=\""+base+"/images/botones/iconos/help.gif\" width=\"20\" height=\"20\" onMouseOver='showMenu()' align='right'>";
        }
        datos[0] = image;        
        if (sw1) {
            op1   =  "<div opcion='op1' class='fuenteAyuda' url=''>&nbsp;&nbsp;&nbsp; Descripcion  </div> \n";
        }
        if (sw2) {
            op2   =  "<div opcion='op2' class='fuenteAyuda' url=''>&nbsp;&nbsp;&nbsp; Funcionamiento  </div> \n";
        }        
        if (sw3) {
            op3   =  "<div opcion='op3' class='fuenteAyuda' url=''>&nbsp;&nbsp;&nbsp; Entrenamiento </div> \n";
        }
        String div = "<div class=\"skin\" style=\"width:15%;\"  id='menu' onMouseOver='highlight();' \n "+
                          "onClick=\"jumpto('"+desc+"','"+func+"','"+ent+"')\" \n "+
                          "onMouseOut='lowlight()' > \n" +
                     " <hr> \n" +op1+op2+op3+
                     " <hr> \n" +
                     "</div> \n" +
                     "<script> " +
                     "document.body.onclick=hideMenu; " +
                     "</script>";      
        datos[1] = div;
        return datos;
    }
    
    /**
     * Metodo getRealUrl, Recibe una direccion url completa y retorna un anueva direcci�n      
     * @autor : Ing. Henry A. Osorio Gonzalez
     * @return : String Nueva Direcci�n  URL
     *           la otra el codigo html de un div.
     * @version : 1.0
     */ 
   public String getRealUrl(String uri) {       
       int index = uri.indexOf("/",1);
       return uri.substring(index);
   }
   
   /**
     * Getter for property vhijos.
     * @return Value of property vhijos.
     */
    public java.util.Vector getVhijos() {
        return menu.getVhijos();
    }
    
    /**
     * Getter for property menu.
     * @return Value of property menu.
     */
    public com.tsp.operation.model.beans.Menu getMenu() {
        return menu.getMenu();
    }
    public void buscarPerfiles() throws SQLException {
        perfiles = menu.getPerfiles();
    }
    /**
     * Getter for property opcione.
     * @return Value of property opcione.
     */
    public java.util.Vector getOpciones() {
        return opcione;
    }
    
    /**
     * Setter for property opcione.
     * @param opcione New value of property opcione.
     */
    public void setOpciones(java.util.Vector opcione) {
        this.opcione = opcione;
    }
    
    /**
     * Metodo verifica si una opcion con un perfil dado tiene opciones hijas
     * @autor  Ing. Sandra M escalante G
     * @see tieneHijosActivosxPerfil (String perfil, int opcion)
     * @version  1.0
     */
    public boolean tieneHijosActivosxPerfil (String perfil, int opcion) throws SQLException {
        return menu.tieneHijosActivosxPerfil(perfil, opcion);
    }
    
   /**
     * Metodo cargarMenuxUsuario, obtiene las opciones por sumatoria se perfiles de usuario
     * @autor  Ing. Sandra M escalante G
     * @see cargarMenuxusuario ( String u )
     * @version  1.0
     */
    public  void cargarMenuxUsuario(String u)throws SQLException{
        try{
            menu.cargarMenuxUsuario(u);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public String cargarMenuJson(String u, String login, String distrito) throws Exception {
        try {
            return menu.cargarMenuJson(u, login, distrito);
        } catch(SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
    
    public String cargarNombreEmpresa(String distrito) throws Exception {
        try {
            return menu.cargarNombreEmpresa(distrito);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
}
