/********************************************************************
 *  Nombre Clase.................   CuentaValidaService.java
 *  Descripci�n..................   Service del DAO de la tabla cuenta_valida
 *  Autor........................   Ing. Leonardo Parodi
 *  Fecha........................   19.01.2006
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  EQUIPO12
 */
public class CuentaValidaService {
        CuentaValidaDAO dao;
        private TreeMap cuentas = new TreeMap();
        /** Creates a new instance of CuentaValidaService */
        public CuentaValidaService() {
                dao = new CuentaValidaDAO();
        }
        
        /**
         * Metodo InsertCuentaValida , Metodo que llama a InsertCuentaValida en el DAO
         * @autor : Ing. Leonardo Parody
         * @Param : CuentaValida cuenta
         * @return : void
         * @version : 1.0
         */
        
        public void InsertCuentaValida(CuentaValida cuenta) throws SQLException{
                dao.setCuentaValida(cuenta);
                dao.InsertCuentaValida();
        }
        
        /**
         * Metodo InsertCuentaValida , Metodo que llama a UpdateCuentaValida en el DAO
         * @autor : Ing. Leonardo Parody
         * @Param : CuentaValida cuenta
         * @param : String clase
         * @param : String agencia
         * @param : String unidad
         * @param : String cliente_area
         * @param : String elemento
         * @return : void
         * @version : 1.0
         */
        
        public void UpdateCuentaValida(CuentaValida cuenta, String clase, String agencia, String unidad, String cliente_area, String elemento) throws SQLException{
                dao.setCuentaValida(cuenta);
                dao.UpdateCuentaValida(clase, agencia, unidad, cliente_area, elemento);
        }
        
        /**
         * Metodo ExisteCuentaValida , Metodo que llama a ExisteCuentaValida en el DAO
         * @autor : Ing. Leonardo Parody
         * @param : String clase
         * @param : String agencia
         * @param : String unidad
         * @param : String cliente_area
         * @param : String elemento
         * @return : void
         * @version : 1.0
         */
        
        public boolean ExisteCuentaValida(String clase, String agencia, String unidad, String cliente_area, String elemento) throws SQLException{
                return dao.ExisteCuentaValida(clase, agencia, unidad, cliente_area, elemento);
        }
        
        /**
         * Metodo AnularCuentaValida , Metodo que llama a AnularCuentaValida en el DAO
         * @autor : Ing. Leonardo Parody
         * @param : String clase
         * @param : String agencia
         * @param : String unidad
         * @param : String cliente_area
         * @param : String elemento
         * @return : void
         * @version : 1.0
         */
        
        public void AnularCuentaValida(String clase, String agencia, String unidad, String cliente_area, String elemento, String user) throws SQLException{
                dao.AnularCuentaValida(clase, agencia, unidad, cliente_area, elemento, user); 
        }
        
        /**
         * Metodo EliminarCuentaValida , Metodo que llama a AnularCuentaValida en el DAO
         * @autor : Ing. Leonardo Parody
         * @param : String clase
         * @param : String agencia
         * @param : String unidad
         * @param : String cliente_area
         * @param : String elemento
         * @return : void
         * @version : 1.0
         */
        
        public void EliminarCuentaValida(String clase, String agencia, String unidad, String cliente_area, String elemento) throws SQLException{
                dao.EliminarCuentaValida(clase, agencia, unidad, cliente_area, elemento);  
        }
        
        /**
         * Metodo ListCuentaValida , Metodo que llama a InsertCuentaValida en el DAO
         * @autor : Ing. Leonardo Parody
         * @param : String clase
         * @param : String agencia
         * @param : String unidad
         * @param : String cliente_area
         * @param : String elemento
         * @return : List
         * @version : 1.0
         */
        
        public List ListCuentaValida(String clase, String agencia, String cliente_area, String elemento, String unidad) throws SQLException{
                return dao.ConsultaListaCuentaValida( clase,  agencia,  cliente_area,  elemento,  unidad); 
        }
        
        /**
         * Metodo ConsultCuentaValida , Metodo que llama a ConsultCuentaValida en el DAO
         * @autor : Ing. Leonardo Parody
         * @param : String clase
         * @param : String agencia
         * @param : String unidad
         * @param : String cliente_area
         * @param : String elemento
         * @return : CuentaValida
         * @version : 1.0
         */
        
        public CuentaValida ConsultCuentaValida(String clase, String agencia, String unidad, String cliente_area, String elemento) throws SQLException{
                return dao.ConsultCuentaValida(clase, agencia, unidad, cliente_area, elemento);
        }
        /**
         * Getter for property cuentas.
         * @return Value of property cuentas.
         */
        public TreeMap getCuentasTree() {
                return cuentas;
        }
        
        
        /**
         * Setter for property cuentas.
         * @param cuentas New value of property cuentas.
         */
        public void setCuentasTree(TreeMap Cuentas){
                
                this.cuentas = Cuentas;
                
        }
}
