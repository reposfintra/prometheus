/*
 * ConfigurarReporteClientesService.java
 *
 * Created on 1 de septiembre de 2005, 09:05 AM
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.ConfigurarReporteClientesDAO;
import com.tsp.operation.model.beans.Usuario;


/**
 *
 * @author  Alejandro
 */
public class ConfigurarReporteClientesService {
    
    private ConfigurarReporteClientesDAO dao;
    
    /** Creates a new instance of ConfigurarReporteClientesService */
    public ConfigurarReporteClientesService() {
        dao = new ConfigurarReporteClientesDAO();
    }
    
    
    public void generarDatosReporte() throws java.sql.SQLException{
        dao.generarDatosReporte();
    }
    
    /**
     * Getter for property datosReporte.
     * @return Value of property datosReporte.
     */
    public java.util.Vector getDatosReporte() {
        return dao.getDatosReporte();
    }
    
    public void eliminarConfigReporte(String [] datosConfigReportes) throws java.sql.SQLException {
        dao.eliminarConfigReporte(datosConfigReportes);
    }
    
    
     public java.util.TreeMap getUsuarios(){
        return dao.getUsuarios();
    }
     
     public void buscarUsuarios(String tipo, Usuario loggedUser)throws java.sql.SQLException {
         dao.buscarUsuarios(tipo, loggedUser);
     }
     
     public java.util.TreeMap obtenerReportes(){
        return dao.obtenerReportes();
    }
    
    public void buscarReportes() throws java.sql.SQLException{
        dao.buscarReportes();
    }
    
    public void salvarConfigReporte(String [] rptSetup) throws java.sql.SQLException {
        dao.salvarConfigReporte(rptSetup);
    }
}
