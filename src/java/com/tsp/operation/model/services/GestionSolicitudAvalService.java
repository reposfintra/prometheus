/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.services;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.util.ArrayList;
import com.itextpdf.text.DocumentException;
import com.lowagie.text.Chunk;
import com.lowagie.text.Element;
import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.tsp.util.Util;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import com.tsp.operation.model.CompaniaService;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.SQLException;

/**
 *
 * @author rhonalf
 */
public class GestionSolicitudAvalService {

    private GestionSolicitudAvalDAO gsadao;
    private String mensaje="";
    private String comentario="";
    private String estado_neg="";
    private String causal="";
    private String resdeudor="";
    private String nitEmpresa="";

//    public GestionSolicitudAvalService() {
//        gsadao = new GestionSolicitudAvalDAO();
//    }
    public GestionSolicitudAvalService(String dataBaseName) {
        gsadao = new GestionSolicitudAvalDAO(dataBaseName);
    }

    public String getDstrct() {
        return gsadao.getDstrct();
    }

    public void setDstrct(String dstrct) {
        gsadao.setDstrct(dstrct);
    }

    public String getLoginuser() {
        return gsadao.getLoginuser();
    }

    public void setLoginuser(String loginuser) {
        gsadao.setLoginuser(loginuser);
    }

    /**
     * Genera una lista de ciudades para colocar en un objeto select
     * @return ArrayList
     * @throws Exception cuando hay error
     */
    public ArrayList listadoCiudades(String coddept) throws Exception {
        ArrayList cadena = new ArrayList();
        try {
            cadena = gsadao.listadoCiudades(coddept);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar ciudades: " + e.toString());
        }
        return cadena;
    }

    /**
     * Genera una lista de Ocupaciones para colocar en un objeto select
     * @return ArrayList
     * @throws Exception cuando hay error
     */
    public ArrayList listadoOcupaciones(String act) throws Exception {
        ArrayList cadena = new ArrayList();
        try {
            cadena = gsadao.listadoOcupaciones(act);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar Ocupaciones: " + e.toString());
        }
        return cadena;
    }

    /**
     * Genera una lista de departamentos para colocar en un objeto select
     * @return ArrayList
     * @throws Exception cuando hay error
     */
    public ArrayList listadoDeps() throws Exception {
        ArrayList cadena = new ArrayList();
        try {
            cadena = gsadao.listadoDeps();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar ciudades: " + e.toString());
        }
        return cadena;
    }

    public String nombreDept(String dept) throws Exception {
        String cadena = "";
        try {
            cadena = gsadao.nombreDept(dept);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar ciudades: " + e.toString());
        }
        return cadena;
    }

    public String nombreCiudad(String ciu) throws Exception {
        String cadena = "";
        try {
            cadena = gsadao.nombreCiudad(ciu);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar ciudades: " + e.toString());
        }
        return cadena;
    }

    /**
     * Busca dato en tablagen
     * @param dato table_type a buscar
     * @return ArrayList
     * @throws Exception cuando hay error
     */
    public ArrayList busquedaGeneral(String dato) throws Exception {
        ArrayList cadena = new ArrayList();
        try {
            cadena = gsadao.busquedaGeneral(dato);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return cadena;
    }

    public SolicitudAval buscarSolicitud(int num_solicitud) throws Exception {
        SolicitudAval bean = null;
        try {
            bean = gsadao.buscarSolicitud(num_solicitud);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return bean;
    }

    public SolicitudAval buscarSolicitud(String negocio) throws Exception {
        SolicitudAval bean = null;
        try {
            bean = gsadao.buscarSolicitud(negocio);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return bean;
    }

    public ArrayList<SolicitudCuentas> buscarCuentas(int num_solicitud) throws Exception {
        ArrayList<SolicitudCuentas> lista = null;
        try {
            lista = gsadao.buscarCuentas(num_solicitud);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return lista;
    }

    public ArrayList<SolicitudReferencias> buscarReferencias(int num_solicitud, String tipo_per, String tipo_ref) throws Exception {
        ArrayList<SolicitudReferencias> lista = null;
        try {
            lista = gsadao.buscarReferencias(num_solicitud, tipo_per, tipo_ref);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return lista;
    }

    public ArrayList<SolicitudBienes> buscarBienes(int num_solicitud, String tipo_p) throws Exception {
        ArrayList<SolicitudBienes> lista = null;
        try {
            lista = gsadao.buscarBienes(num_solicitud, tipo_p);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return lista;
    }

    public ArrayList<SolicitudVehiculo> buscarVehiculos(int num_solicitud) throws Exception {
        ArrayList<SolicitudVehiculo> lista = null;
        try {
            lista = gsadao.buscarVehiculos(num_solicitud);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return lista;
    }
    
    public ArrayList<SolicitudOblComprar> buscarObligaciones(int num_solicitud) throws Exception {
        ArrayList<SolicitudOblComprar> lista = null;
        try {
            lista = gsadao.buscarObligaciones(num_solicitud);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return lista;
    }    

    public SolicitudEstudiante datosEstudiante(int num_solicitud) throws Exception {
        SolicitudEstudiante bean = null;
        try {
            bean = gsadao.datosEstudiante(num_solicitud);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return bean;
    }

    public SolicitudLaboral datosLaboral(int num_solicitud, String tipo) throws Exception {
        SolicitudLaboral bean = null;
        try {
            bean = gsadao.datosLaboral(num_solicitud, tipo);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return bean;
    }

    public void borrarSolicitud(int num_solicitud) throws Exception {
        try {
            gsadao.borrarSolicitud(num_solicitud);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al borrar datos: " + e.toString());
        }
    }

    public String buscarAfilsUsuario(String id_usuario) throws Exception {
        String cadena = "";
        try {
            ArrayList<String> lista = gsadao.buscarAfilsUsuario(id_usuario);
            String[] dato1 = null;
            for (int i = 0; i < lista.size(); i++) {
                dato1 = (lista.get(i)).split(";_;");
                cadena += "<option value='" + dato1[0] + "'>" + dato1[1] + "</option>";
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return cadena;
    }

    public String buscarCodAfils(String afil) throws Exception {
        String cadena = "";
        try {
            cadena = gsadao.buscarCodAfils(afil);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al buscar codigo: " + e.toString());
        }
        return cadena;
    }

    public String buscarAfilsUsuario(String id_usuario, String dato) throws Exception {
        String cadena = "";
        try {
            ArrayList<String> lista = gsadao.buscarAfilsUsuario(id_usuario);
            String[] dato1 = null;
            for (int i = 0; i < lista.size(); i++) {
                dato1 = (lista.get(i)).split(";_;");
                cadena += "<option value='" + dato1[0] + "' " + (dato1[0].equals(dato) ? "selected" : "") + ">" + dato1[1] + "</option>";
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return cadena;
    }

    public SolicitudPersona buscarPersona(int num_solicitud, String tipo) throws Exception {
        SolicitudPersona persona = null;
        try {
            persona = gsadao.buscarPersona(num_solicitud, tipo);
        } catch (Exception e) {
            throw new Exception("Error en buscar persona: " + e.toString());
        }
        return persona;
    }

    public ArrayList<SolicitudHijos> buscarHijos(int num_solicitud, String tipo) throws Exception {
        ArrayList<SolicitudHijos> hijos = null;
        try {
            hijos = gsadao.buscarHijos(num_solicitud, tipo);
        } catch (Exception e) {
            throw new Exception("Error en buscar hijos: " + e.toString());
        }
        return hijos;
    }

    public SolicitudPersona buscarPersonaId(String id) throws Exception {
        SolicitudPersona persona = null;
        try {
            persona = gsadao.buscarPersonaId(id);
        } catch (Exception e) {
            throw new Exception("Error en buscar persona: " + e.toString());
        }
        return persona;
    }

    public String codigoCliente(String nit) throws Exception {
        String codigo = "";
        try {
            codigo = gsadao.codigoCliente(nit);
        } catch (Exception e) {
            throw new Exception("Error en buscar codigo cliente: " + e.toString());
        }
        return codigo;
    }

    public String infoCliente(String nit) throws Exception {
        String codigo = "";
        try {
            codigo = gsadao.infoCliente(nit);
        } catch (Exception e) {
            throw new Exception("Error en buscar codigo cliente: " + e.toString());
        }
        return codigo;
    }

    public boolean insertarDatos(SolicitudAval datoSolic,ArrayList<SolicitudReferencias> lista_refs,
            ArrayList<SolicitudCuentas> lista_cuentas,ArrayList<SolicitudLaboral> lista_lab,
            ArrayList<SolicitudBienes> lista_bienes, ArrayList<SolicitudVehiculo> lista_veh
            , ArrayList<SolicitudPersona> lista_pers, ArrayList<SolicitudHijos> lista_hij, SolicitudEstudiante estudiante, NegocioTrazabilidad negtraza, SolicitudNegocio solcneg,ArrayList<ObligacionesCompra> ocompraList) throws Exception{
        boolean sw;
        try {
            sw=gsadao.insertarDatos(datoSolic, lista_refs, lista_cuentas,lista_lab,lista_bienes, lista_veh, lista_pers,lista_hij, estudiante, negtraza, solcneg,ocompraList);
        }
        catch (Exception e) {
            throw new Exception("Error en insertarDatosnat: "+e.toString());
        }
        return sw;
    }

    public void insertDocumentos(int num_solicitud, ArrayList<SolicitudDocumentos> lista) throws Exception {
        try {
            gsadao.insertDocumentos(num_solicitud, lista);
        } catch (Exception e) {
            throw new Exception("Error en insertDocs: " + e.toString());
        }
    }

    public void updateDocumentos(int num_solicitud, ArrayList<SolicitudDocumentos> lista) throws Exception {
        try {
            gsadao.updateDocumentos(num_solicitud, lista);
        } catch (Exception e) {
            throw new Exception("Error en updateDocs: " + e.toString());
        }
    }

    public ArrayList<SolicitudDocumentos> buscarDocsSolicitud(int num_solicitud) throws Exception {
        ArrayList<SolicitudDocumentos> lista = null;
        try {
            lista = gsadao.buscarDocsSolicitud(num_solicitud);
        } catch (Exception e) {
            throw new Exception("Error en insertDocs: " + e.toString());
        }
        return lista;
    }

    public void actualizarSolicDoc(int num_solicitud,String ciudad_cheque,String tipo_negocio,String num_tipo_negocio,String banco,String sucursal,String num_chequera) throws Exception{
        try {
            gsadao.actualizarSolicDoc(num_solicitud,  ciudad_cheque, tipo_negocio, num_tipo_negocio, banco, sucursal, num_chequera);
        }
        catch (Exception e) {
            throw new Exception("Error en actualizarSolicDoc: "+e.toString());
        }
    }

    public String nombreAfiliado(String nit) throws Exception {
        String nombre = "";
        try {
            nombre = gsadao.nombreAfiliado(nit);
        } catch (Exception e) {
            throw new Exception("Error en nombreAfiliado: " + e.toString());
        }
        return nombre;
    }

    public String nombreTablagen(String table_type, String table_code) throws Exception {
        String nombre = "";
        try {
            nombre = gsadao.nombreTablagen(table_type, table_code);
        } catch (Exception e) {
            throw new Exception("Error en nombre: " + e.toString());
        }
        return nombre;
    }

    public String refTablagen(String table_type, String table_code) throws Exception {
        String nombre = "";
        try {
            nombre = gsadao.refTablagen(table_type, table_code);
        } catch (Exception e) {
            throw new Exception("Error en nombre: " + e.toString());
        }
        return nombre;
    }

    public String numeroSolc() throws Exception {
        return gsadao.NumeroSolc();
    }

    /**
     * Busca la informacion del formulario para un afiliado
     * @param nitprov nit del proveedor
     * @param idUsuario id del usuario
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<BeanGeneral> obtenerFormProv(String nitprov) throws Exception {
        return gsadao.obtenerFormProv(nitprov);
    }

    public SolicitudAval buscarSolicitud(int num_solicitud, String usuario) throws Exception {
        SolicitudAval bean = null;
        try {
            bean = gsadao.buscarSolicitud(num_solicitud, usuario);
        } catch (Exception e) {
            e.printStackTrace();
//            throw new Exception("Error al listar datos: "+e.toString());
        }
        return bean;
    }

    public String buscarAfils(String dato) throws Exception {
        String cadena = "";
        try {
            ArrayList<String> lista = gsadao.buscarAfils();
            String[] dato1 = null;
            for (int i = 0; i < lista.size(); i++) {
                dato1 = (lista.get(i)).split(";_;");
                cadena += "<option value='" + dato1[0] + "' " + (dato1[0].equals(dato) ? "selected" : "") + ">" + dato1[1] + "</option>";
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return cadena;
    }

    public String buscarConveniosAfil(String id_usuario, String afiliado, String dato) throws Exception {
        String cadena = "";
        try {
            ArrayList<String> lista = gsadao.buscarConveniosAfil(id_usuario, afiliado);
            String[] dato1 = null;
            for (int i = 0; i < lista.size(); i++) {
                dato1 = (lista.get(i)).split(";_;");
                cadena += "<option value='" + dato1[0] + "' " + (dato1[0].equals(dato) ? "selected" : "") + ">" + dato1[1] + "</option>";
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return cadena;
    }

    public ArrayList<String> buscarConveniosAfil(String afiliado) throws Exception {
        ArrayList<String> lista;
        try {
            lista = gsadao.buscarConveniosAfil(afiliado);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return lista;
    }

    public ArrayList<String> buscarConveniosAfil(String id_usuario, String afiliado) throws Exception {
        ArrayList<String> lista;
        try {
            lista = gsadao.buscarConveniosAfil(id_usuario, afiliado);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return lista;
    }

    public boolean isAvalTercero(int id_convenio) throws Exception {
        boolean sw;
        try {
            sw = gsadao.isAvalTercero(id_convenio);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return sw;
    }   

     /**
     * metodo que por medio de diferentes politicas informa si una persona es apta para credito
     * @return boolean
     * @throws Exception
     */
    public boolean aptoCredito(Convenio convenio, SolicitudPersona bean_pers,SolicitudLaboral bean_lab,  SolicitudAval bean_sol, Usuario usuario, int num_doc, double tot_pag, String tipo_proceso) throws Exception {
        NegociosDAO negdao = new NegociosDAO(usuario.getBd());
        boolean sw = true;
        String estado = "";
        String rescodeudor = "";

        if (!convenio.isAval_tercero()) {
            WSHistCreditoService wsdc = new WSHistCreditoService(usuario.getBd());
            RespuestaSuperfil respuesta = new RespuestaSuperfil();

            if (bean_pers != null && bean_sol != null) {
                boolean siniestros = false;
                String reporte = "";
                if (convenio.isAval_anombre()) {
                    nitEmpresa=convenio.getNit_anombre();
                    SiniestrosService ss = new SiniestrosService(usuario.getBd());

                    reporte = ss.cedulaReportada(bean_pers.getIdentificacion());
                    if (!reporte.equals("")) {
                        estado_neg = "R";
                        comentario = reporte;
                        causal = "17";
                        resdeudor = "RECHAZAD";

                    } else {

                        if (bean_sol.getTipoNegocio().equals("01")) {
                            reporte = ss.cuentaReportada(bean_sol.getNumTipoNegocio(), bean_sol.getBanco());
                            if (!reporte.equals("")) {
                                estado_neg = "R";
                                comentario = reporte;
                                causal = "18";
                                resdeudor = "RECHAZAD";
                            }
                        }
                        if (reporte.equals("")) {
                            //validara siniestros activos
                            siniestros = ss.Validar_Siniestros_Activos(bean_pers.getIdentificacion());
                            if (!siniestros) {
                                if (negdao.NumNegocioClienteHoy(bean_pers.getIdentificacion()) >= 3) {
                                    estado_neg = "R";
                                    comentario = "maxima operacion permitida en un dia";
                                    causal = "19";
                                    reporte = "error";
                                }

                            } else {
                                estado_neg = "R";
                                 causal = "20";
                                resdeudor = "RECHAZAD";
                            }
                        }

                    }
                } else {
                    CompaniaService ciaserv = new CompaniaService();
                    ciaserv.buscarCia(usuario.getDstrct());
                    Compania cia = ciaserv.getCompania();
                    nitEmpresa = cia.getnit();
                }
                if (!siniestros && reporte.equals("")) {
                    if (bean_pers.getTipoPersona().equals("N")) {
                        if (bean_lab != null) {

                            FormularioSuperfil form = new FormularioSuperfil();
                            form.setTipoIdentificacion("1");
                            form.setIdentificacion(bean_pers.getIdentificacion());
                            form.setPrimerApellido(bean_pers.getPrimerApellido());
                            form.setFechaNacimiento(bean_pers.getFechaNacimiento().substring(0, 10).replaceAll("-", ""));
                            form.setTipoVivienda(bean_pers.getTipoVivienda());
                            form.setHaTenidoCreditoFintra(negdao.tieneCreditoFintra(bean_pers.getIdentificacion()) ? "01" : "02");

                            form.setTipoProducto(bean_sol.getProducto());
                            form.setMontoSolicitado((int) Double.parseDouble(bean_sol.getValorSolicitado()) + "");
                            form.setArriendo((int) Double.parseDouble(bean_lab.getGastosArriendo()) + "");
                            form.setEstadoCivil(wsdc.buscarCodigo("H", "estado_civil", bean_pers.getEstadoCivil()));
                            form.setConyugueTrabaja(Double.parseDouble(bean_pers.getSalarioCony()) > 0 ? "01" : "02");
                            if (bean_sol.getProducto().equals("02")) {
                                form.setServicio(bean_sol.getServicio());
                                form.setCiudadMatricula(wsdc.buscarCodigo("H", "matricula", bean_sol.getCiudadMatricula()));
                                form.setVlrVehiculo((int) Double.parseDouble(bean_sol.getValorProducto()) + "");
                            }
                            if (bean_sol.getProducto().equals("01")) {
                                form.setVlrSemestre((int) Double.parseDouble(bean_sol.getValorProducto()) + "");
                                form.setPlazoUniversitario(wsdc.buscarCodigo("H", "plazo_uni", num_doc + ""));
                            } else {
                                ArrayList<Codigo> plazo = wsdc.buscarTabla("H", "plazo");
                                for (int i = 0; i < plazo.size(); i++) {
                                    if (num_doc <= Integer.parseInt(plazo.get(i).getValor())) {
                                        form.setPlazo(plazo.get(i).getCodigo());
                                        break;
                                    }
                                }
                            }

                            int ingresos = (int) (Double.parseDouble(bean_lab.getSalario()) + Double.parseDouble(bean_lab.getOtrosIngresos()));
                            form.setIngreso(ingresos + "");

                            int cuota = (int) (tot_pag / num_doc);
                            form.setVlrCuotaMensual(cuota + "");
                            form.setNitEmpresa(nitEmpresa);

                            respuesta = wsdc.consultarHistoriaCredito(form, usuario);

                            if (respuesta.getScore() == -1) {
                                comentario = "Error al consultar Deudor: " + respuesta.getCodigoRespuesta() + " " + wsdc.buscarValor("H", "cod_consultas", respuesta.getCodigoRespuesta()+" Nit responsable consulta: "+nitEmpresa);
                             } else {
                                String zonagris = this.nombreTablagen("CONCEPDATA", "P");

                                resdeudor = this.refTablagen("CONCEPDATA", respuesta.getClasificacion());
                                estado = this.nombreTablagen("CONCEPDATA", respuesta.getClasificacion());

                                if (resdeudor.equals("RECHAZAD") && tipo_proceso.equals("DSR")) {
                                    resdeudor = "ZONAGRIS";
                                    estado = zonagris;
                                    comentario = "Negocio Al dia Sin redescuento: Rechazado por Data Credito";
                                }

                                mensaje = "Deudor: " + estado + "<br/>";

                                if (!bean_sol.getProducto().equals("01") && !convenio.isFactura_tercero()) {
                                    SolicitudPersona bean_perc = this.buscarPersona(Integer.parseInt(bean_sol.getNumeroSolicitud()), "C");
                                    bean_lab = this.datosLaboral(Integer.parseInt(bean_sol.getNumeroSolicitud()), "C");

                                    if (bean_perc != null && bean_sol != null) {

                                        form.setTipoIdentificacion("1");
                                        form.setIdentificacion(bean_perc.getIdentificacion());
                                        form.setPrimerApellido(bean_perc.getPrimerApellido());
                                        form.setFechaNacimiento(bean_perc.getFechaNacimiento().substring(0, 10).replaceAll("-", ""));
                                        form.setTipoVivienda(bean_perc.getTipoVivienda());
                                        form.setHaTenidoCreditoFintra(negdao.tieneCreditoFintra(bean_perc.getIdentificacion()) ? "01" : "02");

                                        ingresos = (int) (Double.parseDouble(bean_lab.getSalario()) + Double.parseDouble(bean_lab.getOtrosIngresos()));
                                        form.setIngreso(ingresos + "");
                                        form.setArriendo((int) Double.parseDouble(bean_lab.getGastosArriendo()) + "");
                                        form.setEstadoCivil(wsdc.buscarCodigo("H", "estado_civil", bean_perc.getEstadoCivil()));
                                        form.setConyugueTrabaja(Double.parseDouble(bean_perc.getSalarioCony()) > 0 ? "01" : "02");

                                        respuesta = wsdc.consultarHistoriaCredito(form, usuario);

                                        if (respuesta.getScore() == -1) {
                                           mensaje += "Error al consultar Deudor Solidario: " + wsdc.buscarValor("H", "cod_consultas", respuesta.getCodigoRespuesta())+" Nit responsable consulta: "+nitEmpresa + "<br/>";
                                        } else {
                                            rescodeudor = this.refTablagen("CONCEPDATA", respuesta.getClasificacion());
                                            estado = this.nombreTablagen("CONCEPDATA", respuesta.getClasificacion());
                                            mensaje += "Codeudor: " + estado + "<br/>";
                                        }
                                    }
                                }
                                if (resdeudor.equals("RECHAZAD")) {
                                    causal = "21";
                                    estado_neg = "R";
                                }
                            }
                        } else {
                            sw = false;
                            mensaje = "No se puede consultar la informacion laboral en el formulario";

                    }
                }
              }
            } else {
                    sw = false;
                    mensaje = "no se puede consultar el formulario";
                }
        }
        return sw;
    }

    /**
     * Busca dato en tablagen
     * @param table_type table_type a buscar
     * @return listado con datos coincidentes
     * @throws Exception cuando hay error
     */
    public ArrayList ListaTablaGen(String table_type) throws Exception {
        ArrayList<String> lista;
        try {
            lista = gsadao.ListaTablaGen(table_type);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return lista;
    }

    public String buscarNumSolicitud(String cod_neg) throws Exception {
        return gsadao.buscarNumSolicitud(cod_neg);
    }

    public SolicitudNegocio buscarNegocio(int num_solicitud) throws Exception {
        SolicitudNegocio negocio = null;
        try {
            negocio = gsadao.buscarNegocio(num_solicitud);
        } catch (Exception e) {
            throw new Exception("Error en buscar Negocio: " + e.toString());
        }
        return negocio;
    }

    /**
     * Busca las solicitudes de consumo que fueron devueltas
     * @param idUsuario id del usuario
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<BeanGeneral> solicitudesDevueltasConsumo(String usuario) throws Exception {
        return gsadao.solicitudesDevueltasConsumo(usuario);
    }

    /**
     * Busca las solicitudes de microcredito que fueron devueltas
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<BeanGeneral> solicitudesDevueltasMicrocredito() throws Exception {
        return gsadao.solicitudesDevueltasMicrocredito();
    }

    public String nombreAsesor(String login) throws Exception {
        String nombre = "";
        try {
            nombre = gsadao.nombreAsesor(login);
        } catch (Exception e) {
            throw new Exception("Error en nombreAfiliado: " + e.toString());
        }
        return nombre;
    }

    public SolicitudLaboral buscarLaboralId(String id) throws Exception {
        SolicitudLaboral laboral = null;
        try {
            laboral = gsadao.buscarLaboralId(id);
        } catch (Exception e) {
            throw new Exception("Error en buscarLaboralId: " + e.toString());
        }
        return laboral;
    }
    
     /**
     * Busca los datos de una identificacion en la tabla girador del esquema fenalco
     * @param id identificacion de la persona a buscar
     * @return String con los resultados obtenidos
     * @autor ivargas
     * @throws Exception
     */
     public ArrayList<String> infoGirador(String id) throws Exception{
        ArrayList<String> info;
        try {
            info = gsadao.infoGirador(id);
        }
        catch (Exception e) {
            throw new Exception("Error en infoGirador: "+e.toString());
        }
        return info;
    }

      /**
     * Busca las solicitudes de consumo que fueron devueltas desde formalizacion
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<BeanGeneral> solicitudesDevueltasRad() throws Exception{
        return gsadao.solicitudesDevueltasRad();
    }           
    
    
    public String getCausal() {
        return causal;
    }

    public String getComentario() {
        return comentario;
    }

    public String getEstado_neg() {
        return estado_neg;
    }

    public String getMensaje() {
        return mensaje;
    }

    public String getResdeudor() {
        return resdeudor;
    }

    
    /**
     * consulta si un cheque fue girado
     * @param numero_chequera
     * @param numero_titulo
     * @return boolean
     * @throws Exception Cuando hay error
     */
    public boolean chequeGirado(String cuenta, String num_titulo) throws Exception {
        return gsadao.chequeGirado(cuenta, num_titulo);

    }
    
    /**
     * trae sql que actualiza un documento de solicitud.
     * @param objeto de SolicitudDocumento
     * @return Sql
     * @throws Exception Cuando hay error
     */
    public String updateComisionAvalDoc(SolicitudDocumentos soldoc){
         return gsadao.updateComisionAvalDoc(soldoc);
    }

    /**
     * trae sql que actualiza las devoluciones de un documento de solicitud.
     * @param objeto de SolicitudDocumento
     * @return Sql
     * @throws Exception Cuando hay error
     */
     public String updateDevueltoDocs(SolicitudDocumentos soldoc){
         return gsadao.updateDevueltoDocs(soldoc);
     }

     /**
     * trae sql que actualiza el estado de indemnizacion de un documento de solicitud.
     * @param objeto de SolicitudDocumento
     * @return Sql
     * @throws Exception Cuando hay error
     */
     public String updateEstIndDocs(SolicitudDocumentos soldoc){
         return gsadao.updateEstIndDocs(soldoc);
     }

    
     /**
     * clona una solicitud
     * @param idsolicitud
     * @return Sql
     * @throws Exception
      * jpinedo
     */
        public String ClonarSolicitud(String solicitud ,String cod_neg,double valor_negocio,String nueva_solicitud) throws Exception{
        return gsadao.ClonarSolicitud(solicitud, cod_neg, valor_negocio, nueva_solicitud);
    }


             /**
     * clona detalles  una solicitud
     * @param idsolicitud
     * @return Sql
     * @throws Exception
      * jpinedo
     */
        public String ClonarDetalleSolicitud(String solicitud ,String nueva_solicitud) throws Exception{
        return gsadao.ClonarDetalleSolicitud(solicitud, nueva_solicitud);
    }

    /**
     * trae sql para insertar una solicitud.
     * @param objeto de SolicitudAval
     * @return Sql
     * @throws Exception Cuando hay error
     */
    public String insertSolicitud(SolicitudAval sol) {
        return gsadao.insertSolicitud(sol);
    }

    /**
     * trae sql para insertar una SolicitudPersona
     * @param objeto de SolicitudPersona
     * @return Sql
     * @throws Exception Cuando hay error
     */
    public String insertPersona(SolicitudPersona persona) {
        return gsadao.insertPersona(persona);
    }

    /**
     * trae sql para insertar una SolicitudLaboral
     * @param objeto de SolicitudLaboral
     * @return Sql
     * @throws Exception Cuando hay error
     */
    public String insertLaboral(SolicitudLaboral laboral) {
        return gsadao.insertLaboral(laboral);

    }

     /**
     * metodo que por medio de diferentes politicas informa si una persona es apta para credito
     * @return boolean
     * @throws Exception
     */
    public boolean aptoCredito(Convenio convenio, String numero_form, Usuario usuario, int num_doc, double tot_pag, String tipo_proceso) throws Exception {
        boolean sw = true;
        if (!convenio.isAval_tercero()) {
            SolicitudAval bean_sol = this.buscarSolicitud(Integer.parseInt(numero_form));
            SolicitudPersona bean_pers = this.buscarPersona(Integer.parseInt(numero_form), "S");
            SolicitudLaboral bean_lab = this.datosLaboral(Integer.parseInt(numero_form), "S");
           if(!ValidarClientePreaprobado(bean_pers.getIdentificacion()))
                     {
           sw=this.aptoCredito(convenio, bean_pers, bean_lab, bean_sol, usuario, num_doc, tot_pag, tipo_proceso);
            }
        }
        return sw;
    }

     /**
     * Crea un objeto de tipo Document
     * @return Objeto creado
     */
    private Document createDoc() {
        Document doc = new Document(PageSize.LEGAL, 10, 10, 15, 15);

        return doc;
    }

    /**
     * Genera la ruta en que se guardara el archivo
     * @param user usuario que genera el archivo
     * @param cons consecutivo de la cotizacion
     * @param extension La extension del archivo
     * @return String con la ruta en la que queda el archivo
     * @throws Exception cuando ocurre algun error
     */
    private String directorioArchivo(String user, String cons, String extension) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
            ruta = ruta + "/" + cons + "_" + fmt.format(new Date()) + "." + extension;
        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }

    public boolean exportarPdf(int num_solicitud, String userlogin, String login_into) throws Exception {
        boolean generado = true;

        String directorio = "";
        System.out.println("inicia elaboracion pdf solicitud de aval");
        ResourceBundle rb = null;
        SolicitudAval bean_sol = null;
        bean_sol = this.buscarSolicitud(num_solicitud);
        GestionConveniosDAO convenioDao = new GestionConveniosDAO(this.gsadao.getDatabaseName());
        Convenio convenio = convenioDao.buscar_convenio("fintra", String.valueOf(bean_sol.getIdConvenio()));
        try {
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            directorio = this.directorioArchivo(userlogin, "solicitud_aval_" + num_solicitud, "pdf");
            System.out.println("elaborando directorio");
            Document documento = null;
            Font fuente = new Font(Font.ITALIC, 7);
            Font fuenteB = new Font(Font.ITALIC, 10, Font.BOLD, new java.awt.Color(0x0, 0x0, 0x0));
            Font fuenteD = new Font(Font.ITALIC, 10, Font.BOLD, java.awt.Color.white);
            Font fuenteR = new Font(Font.ITALIC, 12, Font.BOLD, new java.awt.Color(0x99, 0x0, 0x0));
            Font fuenteG = new Font(Font.ITALIC, 12, Font.BOLD, new java.awt.Color(0x0, 0x0, 0x0));
            documento = this.createDoc();
            PdfWriter.getInstance(documento, new FileOutputStream(directorio));
            documento.open();
            documento.newPage();
            float[] widths2 = {0.13f, 0.30f, 0.30f, 0.27f};
            PdfPTable thead = new PdfPTable(widths2);
            String espacio = "                                                            ";
            String espacio_ = "                                   ";

            bean_sol = this.buscarSolicitud(num_solicitud);
            PdfPCell celda = new PdfPCell();
            
             if (!convenio.getTipo().equals("Multiservicio")) {
               celda.setPhrase(new Phrase(espacio_ + (!convenio.getTipo().equals("Multiservicio")? "SOLICITUD DE CREDITO EDUCATIVO":"SOLICITUD DE CREDITO") + espacio_ +num_solicitud, fuenteG));
            } else {
               celda.setPhrase(new Phrase(espacio + (!convenio.getTipo().equals("Multiservicio")? "SOLICITUD DE AVAL":"SOLICITUD DE CREDITO") + espacio + num_solicitud, fuenteG));

            }
            
            
            //celda.setPhrase(new Phrase(espacio + (!convenio.getTipo().equals("Multiservicio")? "SOLICITUD DE AVAL":"SOLICITUD DE CREDITO") + espacio + num_solicitud, fuenteG));
            celda.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            celda.setColspan(4);
            thead.addCell(celda);
            Image imagen = Image.getInstance(rb.getString("ruta") + "/images/logo_fintra.jpg");

             //imagen del pdf solicitud aval
            if (login_into.equals("Fenalco")) {
                imagen = Image.getInstance(rb.getString("ruta") + "/images/fenalco_atlantico.jpg");
            }else if(login_into.equals("Fenalco_bol")){
                imagen = Image.getInstance(rb.getString("ruta") + "/images/fenalco_bolivar.jpg");
             }

            //  imagen.scaleAbsolute(500, 500);
            celda = new PdfPCell();
            celda.addElement(imagen);
            thead.addCell(celda);
            PdfPTable tabla_temp = this.tablaDiv(bean_sol);
            tabla_temp.setWidthPercentage(100);
            celda = new PdfPCell();
            celda.setColspan(3);
            celda.addElement(tabla_temp);
            thead.addCell(celda);
            thead.setWidthPercentage(100);
            documento.add(thead);
            PdfPTable tcont = new PdfPTable(1);
            tcont = new PdfPTable(1);
            celda = new PdfPCell();
            celda.setBorder(0);
            celda.addElement(new Phrase("\n", fuente));
            tcont.addCell(celda);
            tcont.setWidthPercentage(100);
            documento.add(tcont);
            SolicitudPersona bean_pers = this.buscarPersona(num_solicitud, "S");
            SolicitudLaboral bean_lab = this.datosLaboral(num_solicitud, "S");
            SolicitudNegocio bean_neg = this.buscarNegocio(num_solicitud);
            ArrayList<SolicitudBienes> bienList = this.buscarBienes(num_solicitud, "S");
            ArrayList<SolicitudVehiculo> vehList = this.buscarVehiculos(num_solicitud);
            ArrayList<SolicitudReferencias> refList = this.buscarReferencias(num_solicitud, "S", "P");
            ArrayList<SolicitudHijos> hijList = this.buscarHijos(num_solicitud, "S");
            ArrayList<SolicitudCuentas> lista_cuentas = this.buscarCuentas(num_solicitud);
            SolicitudReferencias ref = new SolicitudReferencias();
            ref.setNombre("");
            ref.setDireccion("");
            ref.setCelular("");
            ref.setCiudad("");
            ref.setDepartamento("");
            ref.setEmail("");
            ref.setExtension("");
            ref.setNumeroSolicitud(num_solicitud + "");
            ref.setParentesco("");
            ref.setPrimerApellido("");
            ref.setPrimerNombre("");
            ref.setSegundoApellido("");
            ref.setSegundoNombre("");
            ref.setTelefono("");
            ref.setTelefono2("");
            ref.setTiempoConocido("");
            SolicitudCuentas cue = new SolicitudCuentas();
            cue.setBanco("");
            cue.setCuenta("");
            cue.setFechaApertura("");
            cue.setNumeroTarjeta("");
            cue.setTipo("");
            float[] widths = {0.04f, 0.96f};
            PdfPTable tcontgen = new PdfPTable(widths);
            PdfPCell celdagen = new PdfPCell();
            tcont = new PdfPTable(1);
            celda = new PdfPCell();
            if (bean_sol.getTipoPersona().equals("J")) {
                celdagen.setBorder(0);
                celdagen.addElement(new Phrase("                        PERSONA JURIDICA", fuenteB));
                celdagen.setRotation(90);
                celdagen.setHorizontalAlignment(PdfPCell.ALIGN_TOP);
                celdagen.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));
                tcontgen.addCell(celdagen);
                celdagen = new PdfPCell();
                celdagen.setBorder(0);
                celda.setBorder(0);
                celda.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
                celda.addElement(new Phrase("Informaci\u00f3n B\u00e1sica", fuenteB));
                celda.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));

                tcont.addCell(celda);
                tcont.setWidthPercentage(100);
                PdfPTable tabla_pers = this.tablaPjuridica(bean_pers);
                tabla_pers.setWidthPercentage(100);
                PdfPCell celdita = new PdfPCell();
                celdita.addElement(tabla_pers);
                celdita.setBorder(0);
                tcont.addCell(celdita);
                //documento.add(tcont);
                celdagen.addElement(tcont);
                tcont = new PdfPTable(1);
                celda = new PdfPCell();
                celda.setBorder(0);
                celda.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
                celda.addElement(new Phrase("Referencias Comerciales", fuenteB));
                celda.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));
                tcont.addCell(celda);
                tcont.setWidthPercentage(100);
                refList = this.buscarReferencias(num_solicitud, "S", "C");
                if (bean_sol.getTipoConv().equals("Multiservicio") && refList.isEmpty()) {
                    ref.setTipoReferencia("C");
                    refList.add(ref);
                }
                PdfPTable tabla_ref = this.tablaReferencia(refList);
                tabla_ref.setWidthPercentage(100);
                celdita = new PdfPCell();
                celdita.addElement(tabla_ref);
                celdita.setBorder(0);
                tcont.addCell(celdita);
                //documento.add(tcont);
                celdagen.addElement(tcont);
            } else {
                celdagen.setBorder(0);
                celdagen.addElement(new Phrase("                                                     PERSONA NATURAL", fuenteB));
                celdagen.setRotation(90);
                celdagen.setHorizontalAlignment(PdfPCell.ALIGN_TOP);
                celdagen.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));
                tcontgen.addCell(celdagen);
                celdagen = new PdfPCell();
                celdagen.setBorder(0);
                celda.setBorder(0);
                celda.addElement(new Phrase("Informaci\u00f3n b\u00e1sica \n", fuenteB));
                celda.setVerticalAlignment(PdfPCell.ALIGN_TOP);
                celda.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));
                tcont.addCell(celda);
                tcont.setWidthPercentage(100);
                PdfPTable tabla_pers = this.tablaPnatural(bean_pers);
                tabla_pers.setWidthPercentage(100);
                PdfPCell celdita = new PdfPCell();
                celdita.addElement(tabla_pers);
                celdita.setBorder(0);
                tcont.addCell(celdita);
                //documento.add(tcont);
                celdagen.addElement(tcont);
                /* if (Integer.parseInt(bean_pers.getNumHijos()) > 0) {
                tcont = new PdfPTable(1);
                celda = new PdfPCell();
                celda.setBorder(0);
                celda.addElement(new Phrase("Informaci\u00f3n Hijos", fuenteB));
                celda.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));
                tcont.addCell(celda);
                tcont.setWidthPercentage(100);
                PdfPTable tabla_hij = this.tablaHijos(hijList);
                tabla_hij.setWidthPercentage(100);
                celdita = new PdfPCell();
                celdita.addElement(tabla_hij);
                celdita.setBorder(0);
                tcont.addCell(celdita);
                documento.add(tcont);
                }*/
                if (bean_pers.getEstadoCivil().equals("C") || bean_pers.getEstadoCivil().equals("U")) {
                    tcont = new PdfPTable(1);
                    celda = new PdfPCell();
                    celda.setBorder(0);
                    celda.addElement(new Phrase("Informaci\u00f3n del Conyuge", fuenteB));
                    celda.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));
                    celda.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
                    tcont.addCell(celda);
                    tcont.setWidthPercentage(100);
                    tabla_pers = this.tablaConyuge(bean_pers);
                    tabla_pers.setWidthPercentage(100);
                    celdita = new PdfPCell();
                    celdita.addElement(tabla_pers);
                    celdita.setBorder(0);
                    tcont.addCell(celdita);
                    //documento.add(tcont);
                    celdagen.addElement(tcont);
                }
                tcont = new PdfPTable(1);
                celda = new PdfPCell();
                celda.setBorder(0);
                celda.addElement(new Phrase("Informaci\u00f3n Laboral y Econ\u00f3mica", fuenteB));
                celda.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));
                celda.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
                tcont.addCell(celda);
                tcont.setWidthPercentage(100);
                PdfPTable tabla_lab = this.tablaLaboral(bean_lab);
                tabla_lab.setWidthPercentage(100);
                celdita = new PdfPCell();
                celdita.addElement(tabla_lab);
                celdita.setBorder(0);
                tcont.addCell(celdita);
                //documento.add(tcont);
                celdagen.addElement(tcont);
                if (vehList.size() > 0) {
                    tcont = new PdfPTable(1);
                    celda = new PdfPCell();
                    celda.setBorder(0);
                    celda.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
                    celda.addElement(new Phrase("Bienes y vehiculos", fuenteB));
                    celda.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));
                    tcont.addCell(celda);
                    tcont.setWidthPercentage(100);
                    PdfPTable tabla_bien = this.tablaBienVeh(bienList, vehList);
                    tabla_bien.setWidthPercentage(100);
                    celdita = new PdfPCell();
                    celdita.addElement(tabla_bien);
                    celdita.setBorder(0);
                    tcont.addCell(celdita);
                    //documento.add(tcont);
                    celdagen.addElement(tcont);
                }
                tcont = new PdfPTable(1);
                celda = new PdfPCell();
                celda.setBorder(0);
                celda.addElement(new Phrase("Referencias Personales", fuenteB));
                celda.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));
                tcont.addCell(celda);
                tcont.setWidthPercentage(100);
                if (bean_sol.getTipoConv().equals("Multiservicio") && refList.isEmpty()) {
                    ref.setTipoReferencia("P");
                    refList.add(ref);
                }
                PdfPTable tabla_ref = this.tablaReferencia(refList);
                tabla_ref.setWidthPercentage(100);
                celdita = new PdfPCell();
                celdita.addElement(tabla_ref);
                celdita.setBorder(0);
                tcont.addCell(celdita);
                //documento.add(tcont);
                celdagen.addElement(tcont);
                tcont = new PdfPTable(1);
                celda = new PdfPCell();
                celda.setBorder(0);
                celda.addElement(new Phrase("Referencias Familiares", fuenteB));
                celda.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));
                tcont.addCell(celda);
                tcont.setWidthPercentage(100);
                refList = this.buscarReferencias(num_solicitud, "S", "F");
                if (bean_sol.getTipoConv().equals("Multiservicio") && refList.isEmpty()) {
                    ref.setTipoReferencia("F");
                    refList.add(ref);
                }
                tabla_ref = this.tablaReferencia(refList);
                tabla_ref.setWidthPercentage(100);
                celdita = new PdfPCell();
                celdita.addElement(tabla_ref);
                celdita.setBorder(0);
                tcont.addCell(celdita);
                //documento.add(tcont);
                celdagen.addElement(tcont);
            }

            if (bean_sol.getTipoConv().equals("Multiservicio") && lista_cuentas.isEmpty()) {
                lista_cuentas.add(cue);
            }
            tcont = new PdfPTable(1);
            celda = new PdfPCell();
            celda.setBorder(0);
            celda.addElement(new Phrase("Cuentas", fuenteB));
            celda.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));
            tcont.addCell(celda);
            tcont.setWidthPercentage(100);
            //documento.add(tcont);
            celdagen.addElement(tcont);
            PdfPTable tabla_cuentas = this.tablaCuentas(lista_cuentas);
            tabla_cuentas.setWidthPercentage(100);
            // documento.add(tabla_cuentas);
            celdagen.addElement(tabla_cuentas);

            if (bean_neg != null) {
                tcont = new PdfPTable(1);
                celda = new PdfPCell();
                celda.setBorder(0);
                celda.addElement(new Phrase("Negocio", fuenteB));
                celda.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));
                tcont.addCell(celda);
                tcont.setWidthPercentage(100);
                PdfPTable tabla_neg = this.tablaNegocio(bean_neg, bean_sol.getTipoPersona());
                tabla_neg.setWidthPercentage(100);
                PdfPCell celdita = new PdfPCell();
                celdita.addElement(tabla_neg);
                celdita.setBorder(0);
                tcont.addCell(celdita);
                celdagen.addElement(tcont);
            }
            tcontgen.addCell(celdagen);
            tcontgen.setWidthPercentage(100);
            documento.add(tcontgen);
            bean_pers = this.buscarPersona(num_solicitud, "C");
            tcontgen = new PdfPTable(widths);
            if (bean_pers != null) {
                celdagen = new PdfPCell();
                celdagen.setBorder(0);
                celdagen.addElement(new Phrase("                               DEUDOR SOLIDARIO", fuenteB));
                celdagen.setRotation(90);
                celdagen.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
                celdagen.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));
                tcontgen.addCell(celdagen);
                celdagen = new PdfPCell();
                celdagen.setBorder(0);
                tcont = new PdfPTable(1);
                celda = new PdfPCell();
                celda.setBorder(0);
                celda.addElement(new Phrase("Informaci\u00f3n B\u00e1sica", fuenteB));
                celda.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));
                celda.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
                tcont.addCell(celda);
                tcont.setWidthPercentage(100);
                PdfPTable tabla_pers = this.tablaPnatural(bean_pers);
                tabla_pers.setWidthPercentage(100);
                PdfPCell celdita = new PdfPCell();
                celdita.addElement(tabla_pers);
                celdita.setBorder(0);
                tcont.addCell(celdita);
                celdagen.addElement(tcont);
                //documento.add(tcont);
           /*     if (Integer.parseInt(bean_pers.getNumHijos()) > 0) {
                tcont = new PdfPTable(1);
                celda = new PdfPCell();
                celda.setBorder(0);
                celda.addElement(new Phrase("Informaci\u00f3n Hijos", fuenteB));
                celda.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));
                tcont.addCell(celda);
                tcont.setWidthPercentage(100);
                hijList = this.buscarHijos(num_solicitud, "C");
                PdfPTable tabla_hij = this.tablaHijos(hijList);
                tabla_hij.setWidthPercentage(100);
                celdita = new PdfPCell();
                celdita.addElement(tabla_hij);
                celdita.setBorder(0);
                tcont.addCell(celdita);
                documento.add(tcont);
                }*/
                tcont = new PdfPTable(1);
                celda = new PdfPCell();
                celda.setBorder(0);
                celda.addElement(new Phrase("Informaci\u00f3n Laboral y Econ\u00f3mica", fuenteB));
                celda.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));
                celda.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
                tcont.addCell(celda);
                tcont.setWidthPercentage(100);
                bean_lab = this.datosLaboral(num_solicitud, "C");
                PdfPTable tabla_lab = this.tablaLaboral(bean_lab);
                tabla_lab.setWidthPercentage(100);
                celdita = new PdfPCell();
                celdita.addElement(tabla_lab);
                celdita.setBorder(0);
                tcont.addCell(celdita);
                //documento.add(tcont);
                celdagen.addElement(tcont);
                tcont = new PdfPTable(1);
                celda = new PdfPCell();
                celda.setBorder(0);
                celda.addElement(new Phrase("Referencias Personales", fuenteB));
                celda.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));
                tcont.addCell(celda);
                tcont.setWidthPercentage(100);
                refList = this.buscarReferencias(num_solicitud, "C", "F");
                PdfPTable tabla_ref = this.tablaReferencia(refList);
                tabla_ref.setWidthPercentage(100);
                celdita = new PdfPCell();
                celdita.addElement(tabla_ref);
                celdita.setBorder(0);
                tcont.addCell(celdita);
                // documento.add(tcont);
                celdagen.addElement(tcont);
                tcont = new PdfPTable(1);
                celda = new PdfPCell();
                celda.setBorder(0);
                celda.addElement(new Phrase("Referencias Familiares", fuenteB));
                celda.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));
                tcont.addCell(celda);
                tcont.setWidthPercentage(100);
                refList = this.buscarReferencias(num_solicitud, "C", "F");
                tabla_ref = this.tablaReferencia(refList);
                tabla_ref.setWidthPercentage(100);
                celdita = new PdfPCell();
                celdita.addElement(tabla_ref);
                celdita.setBorder(0);
                tcont.addCell(celdita);
                // documento.add(tcont);
                celdagen.addElement(tcont);
                tcontgen.addCell(celdagen);
                tcontgen.setWidthPercentage(100);
                documento.add(tcontgen);
            }
            tcontgen = new PdfPTable(widths);
            bean_pers = this.buscarPersona(num_solicitud, "E");
            if (bean_pers != null) {
                celdagen = new PdfPCell();
                celdagen.setBorder(0);
                celdagen.addElement(new Phrase("                       ESTUDIANTE", fuenteB));
                celdagen.setRotation(90);
                celdagen.setHorizontalAlignment(PdfPCell.ALIGN_MIDDLE);
                celdagen.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));
                tcontgen.addCell(celdagen);
                celdagen = new PdfPCell();
                celdagen.setBorder(0);
                tcont = new PdfPTable(1);
                celda = new PdfPCell();
                celda.setBorder(0);
                celda.addElement(new Phrase("Informaci\u00f3n B\u00e1sica", fuenteB));
                celda.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));
                celda.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
                tcont.addCell(celda);
                tcont.setWidthPercentage(100);
                PdfPTable tabla_pers = this.tablaPnatural(bean_pers);
                tabla_pers.setWidthPercentage(100);
                PdfPCell celdita = new PdfPCell();
                celdita.addElement(tabla_pers);
                celdita.setBorder(0);
                tcont.addCell(celdita);
                //documento.add(tcont);
                celdagen.addElement(tcont);
                tcontgen.addCell(celdagen);
                tcontgen.setWidthPercentage(100);
                documento.add(tcontgen);
                /*
                if (Integer.parseInt(bean_pers.getNumHijos()) > 0) {
                tcont = new PdfPTable(1);
                celda = new PdfPCell();
                celda.setBorder(0);
                celda.addElement(new Phrase("Informaci\u00f3n Hijos", fuenteB));
                celda.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));
                tcont.addCell(celda);
                tcont.setWidthPercentage(100);
                hijList = this.buscarHijos(num_solicitud, "E");
                PdfPTable tabla_hij = this.tablaHijos(hijList);
                tabla_hij.setWidthPercentage(100);
                celdita = new PdfPCell();
                celdita.addElement(tabla_hij);
                celdita.setBorder(0);
                tcont.addCell(celdita);
                documento.add(tcont);
                }*/

            }

            //pagare
            if (!(convenio.getTipo().equals("Consumo") && convenio.isRedescuento() && convenio.isAval_anombre())) {
                tcont = new PdfPTable(1);
                celda = new PdfPCell();
                celda.setBorder(0);
                celda.addElement(new Phrase("\n", fuente));
                tcont.addCell(celda);
                tcont.setWidthPercentage(100);
                documento.add(tcont);
                tcont = new PdfPTable(1);
                celda = new PdfPCell();
                celda.setBorder(0);
                celda.addElement(new Phrase("PAGARE", fuenteB));
                celda.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));
                tcont.addCell(celda);
                tcont.setWidthPercentage(100);
                documento.add(tcont);
                PdfPTable tabla_docs = this.tablaPagare(num_solicitud, bean_sol);
                tabla_docs.setWidthPercentage(100);
                documento.add(tabla_docs);
                documento.newPage();
                documento.add(thead);
                tcont = new PdfPTable(1);
                celda = new PdfPCell();
                celda.setBorder(0);
                celda.addElement(new Phrase("\n", fuente));
                tcont.addCell(celda);
                tcont.setWidthPercentage(100);
                documento.add(tcont);

// anexos


                tcont = this.texto1Pag2(bean_sol.getTipoConv());
                documento.add(tcont);
                tcont = new PdfPTable(1);
                celda = new PdfPCell();
                celda.setBorder(0);
                celda.addElement(new Phrase("\n", fuente));
                tcont.addCell(celda);
                tcont.setWidthPercentage(100);
                documento.add(tcont);
                tcont = this.texto2Pag2(bean_sol.getTipoConv());
                documento.add(tcont);
                tcont = new PdfPTable(1);
                celda = new PdfPCell();
                celda.setBorder(0);
                celda.addElement(new Phrase("\n", fuente));
                tcont.addCell(celda);
                tcont.setWidthPercentage(100);
                documento.add(tcont);
                tcont = this.texto3Pag2(bean_sol.getTipoConv());
                documento.add(tcont);
                tcont = new PdfPTable(1);
                celda = new PdfPCell();
                celda.setBorder(0);
                celda.addElement(new Phrase("\n", fuente));
                tcont.addCell(celda);
                tcont.setWidthPercentage(100);
                documento.add(tcont);
                tcont = this.texto4Pag2(bean_sol.getTipoConv());
                documento.add(tcont);
                //  tcont = this.texto5Pag2();
                //  documento.add(tcont);
                if (!bean_sol.getTipoConv().equals("Multiservicio")) {
                    //documentos
                    documento.newPage();
                    documento.add(thead);
                    tcont = new PdfPTable(1);
                    celda = new PdfPCell();
                    celda.setBorder(0);
                    celda.addElement(new Phrase("\n", fuente));
                    tcont.addCell(celda);
                    tcont.setWidthPercentage(100);
                    documento.add(tcont);
                    tcont = new PdfPTable(1);
                    celda = new PdfPCell();
                    celda.setBorder(0);
                    celda.addElement(new Phrase("DOCUMENTOS", fuenteB));
                    celda.setBackgroundColor(new java.awt.Color(0xD8, 0xD8, 0xD8));
                    tcont.addCell(celda);
                    tcont.setWidthPercentage(100);
                    documento.add(tcont);
                    tabla_docs = this.tablaDocs(num_solicitud, bean_sol);
                    tabla_docs.setWidthPercentage(100);
                    /*celdita = new PdfPCell();
                    celdita.addElement(tabla_docs);
                    celdita.setBorder(0);
                    tcont.addCell(celdita);*/
                    documento.add(tabla_docs);
                }



            }





            //aqui entra cuando es consumo
            if (convenio.getTipo().equals("Consumo") && convenio.isRedescuento() && convenio.isAval_anombre()) {


                documento.add(Chunk.NEWLINE);
                documento.add(Chunk.NEWLINE);
                documento.add(Chunk.NEWLINE);
                documento.add(Chunk.NEWLINE);
                documento.add(Chunk.NEWLINE);
                Paragraph p = new Paragraph();
                p = new Paragraph("SOLICITANTE DEUDOR " + "                                                                        " + " DEUDOR SOLIDARIO", fuenteB);
                documento.add(p);
                documento.add(Chunk.NEWLINE);

                tcont = this.Firmas();
                documento.add(tcont);
                documento.add(Chunk.NEWLINE);

                //aqui vamos mostrar el texto correspondiente a fenalco : atlantico o bolivar
                
                if(login_into.equals("Fenalco_bol")){
               
                p = new Paragraph(this.refTablagen("TPAGFENB", "08"), fuenteB);
                documento.newPage();
                p.setAlignment(Paragraph.ALIGN_CENTER);
                documento.add(p);
                tcont = this.getTextoRedes("08","TPAGFENB");
                documento.add(tcont);
                
                }else{
             
                p = new Paragraph(this.refTablagen("TPAGFEN", "08"), fuenteB);
                documento.newPage();
                p.setAlignment(Paragraph.ALIGN_CENTER);
                documento.add(p);
                tcont = this.getTextoRedes("08","TPAGFEN");
                documento.add(tcont);
                
                }


                documento.newPage();
                p = new Paragraph(this.refTablagen("TPAGFEN", "09"), fuenteB);
                p.setAlignment(Paragraph.ALIGN_CENTER);
                documento.add(p);
                tcont = this.getTextoRedes("09","TPAGFEN");
                documento.add(tcont);
                documento.add(Chunk.NEWLINE);
                documento.add(Chunk.NEWLINE);


                p = new Paragraph("SOLICITANTE DEUDOR " + "                                                                        " + " DEUDOR SOLIDARIO", fuenteB);
                documento.add(p);
                documento.add(Chunk.NEWLINE);

                tcont = this.Firmas();
                documento.add(tcont);
                documento.add(Chunk.NEWLINE);
                documento.add(Chunk.NEWLINE);

                tcont = Requisitos();
                documento.add(tcont);



            }



            documento.close();

        } catch (Exception e) {
            generado = false;
            System.out.println("error al generar pdf solicitud de aval: " + e.toString());
            e.printStackTrace();
        }
        System.out.println("fin elaboracion pdf solicitud de aval");
        return generado;
    }

    protected PdfPTable tablaDiv(SolicitudAval bean_sol) {
        PdfPTable tabla_temp = new PdfPTable(3);

        Font fuente = new Font(Font.ITALIC, 10);
        PdfPCell celda_temp = new PdfPCell();
        //1
        celda_temp.setPhrase(new Phrase("Fecha de consulta: \n" + (bean_sol.getFechaConsulta().substring(0, 10)), fuente));
        tabla_temp.addCell(celda_temp);
        //2
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Agente: \n" + (bean_sol.getAgente()), fuente));
        tabla_temp.addCell(celda_temp);
        //3
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Numero de aprobacion: \n" + (bean_sol.getNumeroAprobacion()), fuente));
        tabla_temp.addCell(celda_temp);
        //2.1
        if (bean_sol.getTipoConv().equals("Consumo")) {
            celda_temp = new PdfPCell();
            String nombre_afil = "";
            try {
                nombre_afil = this.nombreAfiliado(bean_sol.getAfiliado());
            } catch (Exception e) {
                System.out.println("error: " + e.toString());
                e.printStackTrace();
            }
            celda_temp.setPhrase(new Phrase("Razon social o nombre afiliado: \n" + (nombre_afil), fuente));
            celda_temp.setColspan(2);
            tabla_temp.addCell(celda_temp);
        }
        if (bean_sol.getTipoConv().equals("Microcredito")) {
            celda_temp = new PdfPCell();
            String nombre_asesor = "";
            try {
                nombre_asesor = this.nombreAsesor(bean_sol.getAsesor());
            } catch (Exception e) {
                System.out.println("error: " + e.toString());
                e.printStackTrace();
            }
            celda_temp.setPhrase(new Phrase("Asesor Comercial: \n" + (nombre_asesor), fuente));
            celda_temp.setColspan(2);
            tabla_temp.addCell(celda_temp);
        }
        //2.2
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Codigo: \n" + (bean_sol.getCodigo()), fuente));
        tabla_temp.addCell(celda_temp);
        //3.1
        celda_temp = new PdfPCell();


        celda_temp.setPhrase(new Phrase("Valor solicitado: \n" + (bean_sol.getValorSolicitado()), fuente));
        //celda_temp.setColspan(3);
        tabla_temp.addCell(celda_temp);
        //4.1

        //5.1
     /*   String tipo_persona = bean_sol.getTipoPersona().equals("N") ? "Natural" : "Juridica";
        celda_temp.setPhrase(new Phrase("Tipo de persona: \n" + (tipo_persona), fuente));
        celda_temp.setColspan(3);
        tabla_temp.addCell(celda_temp);*/
        return tabla_temp;
    }

    protected PdfPTable tablaPjuridica(SolicitudPersona bean_pers) {
        PdfPTable tabla_temp = new PdfPTable(6);
        Font fuente = new Font(Font.ITALIC, 6);
        PdfPCell celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Razon social: \n" + (bean_pers.getNombre()), fuente));
        celda_temp.setColspan(2);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Nit: \n" + (bean_pers.getIdentificacion()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Ciiu: \n" + (bean_pers.getCiiu()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        String cadena = "";
        try {
            cadena = this.nombreTablagen("CAREMP", bean_pers.getTipoEmpresa());
        } catch (Exception e) {
            System.out.println("error: " + e.toString());
            e.printStackTrace();
        }
        celda_temp.setPhrase(new Phrase("Caracter de la empresa: \n" + (cadena), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Fecha constitucion: \n" + (bean_pers.getFechaConstitucion().substring(0, 10).equals("0099-01-01") ? "" : bean_pers.getFechaConstitucion().substring(0, 10)), fuente));
        tabla_temp.addCell(celda_temp);
        //2
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("E-mail: \n" + (bean_pers.getEmail()), fuente));
        celda_temp.setColspan(2);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Telefono 1: \n" + (bean_pers.getTelefono()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Telefono 2: \n" + (bean_pers.getTelefono2()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Fax: \n" + (bean_pers.getFax()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(" ", fuente));
        tabla_temp.addCell(celda_temp);
        //3
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Direccion: \n" + (bean_pers.getDireccion()), fuente));
        celda_temp.setColspan(2);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        cadena = "";
        try {
            cadena = this.nombreDept(bean_pers.getDepartamento());
        } catch (Exception e) {
            System.out.println("error: " + e.toString());
            e.printStackTrace();
        }
        celda_temp.setPhrase(new Phrase("Departamento: \n" + (cadena), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        cadena = "";
        try {
            cadena = this.nombreCiudad(bean_pers.getCiudad());
        } catch (Exception e) {
            System.out.println("error: " + e.toString());
            e.printStackTrace();
        }
        celda_temp.setPhrase(new Phrase("Ciudad: \n" + (cadena), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Celular: \n" + (bean_pers.getCelular()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(" ", fuente));
        tabla_temp.addCell(celda_temp);


        //4
        celda_temp = new PdfPCell();
        String espacio = " ";
        if (bean_pers.getRepresentanteLegal().length() < 35) {
            for (int i = bean_pers.getRepresentanteLegal().length(); i < 35; i++) {
                espacio += "  ";
            }
        }
        celda_temp.setPhrase(new Phrase("Representante legal:                                    Genero:  \n" + (bean_pers.getRepresentanteLegal() + espacio + bean_pers.getGeneroRepresentante()), fuente));
        celda_temp.setColspan(2);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        cadena = "";
        try {
            cadena = this.nombreTablagen("TIPID", bean_pers.getTipoIdFirmador());
        } catch (Exception e) {
            System.out.println("error: " + e.toString());
            e.printStackTrace();
        }
        celda_temp.setPhrase(new Phrase("Identificacion: \n" + (cadena) + " " + (bean_pers.getTipoIdRepresentante()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        espacio = " ";
        if (bean_pers.getFirmadorCheques().length() < 35) {
            for (int i = bean_pers.getFirmadorCheques().length(); i < 35; i++) {
                espacio += "  ";
            }
        }
        celda_temp.setPhrase(new Phrase("Autorizado para firmar cheques:                  Genero: \n" + (bean_pers.getFirmadorCheques() + espacio + bean_pers.getGeneroFirmador()), fuente));
        celda_temp.setColspan(2);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        cadena = "";
        try {
            cadena = this.nombreTablagen("TIPID", bean_pers.getTipoIdFirmador());
        } catch (Exception e) {
            System.out.println("error: " + e.toString());
            e.printStackTrace();
        }
        celda_temp.setPhrase(new Phrase("Identificacion: \n" + (cadena) + " " + (bean_pers.getIdFirmador()), fuente));
        tabla_temp.addCell(celda_temp);
        return tabla_temp;
    }

    protected PdfPTable tablaPnatural(SolicitudPersona bean_pers) throws Exception {
        SolicitudEstudiante est = this.datosEstudiante(Integer.parseInt(bean_pers.getNumeroSolicitud()));
        PdfPTable tabla_temp = new PdfPTable(6);
        Font fuente = new Font(Font.ITALIC, 6);
        PdfPCell celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Primer apellido: \n" + (bean_pers.getPrimerApellido()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Segundo apellido: \n" + (bean_pers.getSegundoApellido()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Primer nombre: \n" + (bean_pers.getPrimerNombre()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Segundo nombre: \n" + (bean_pers.getSegundoNombre()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Genero: \n" + (bean_pers.getGenero()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        String cadena = "";
        try {
            cadena = this.nombreTablagen("ESTCIV", bean_pers.getEstadoCivil());
        } catch (Exception e) {
            System.out.println("error: " + e.toString());
            e.printStackTrace();
        }
        celda_temp.setPhrase(new Phrase("Estado civil: \n" + (cadena), fuente));
        tabla_temp.addCell(celda_temp);
        //fila 2
        celda_temp = new PdfPCell();
        cadena = "";
        try {
            cadena = this.nombreTablagen("TIPID", bean_pers.getTipoId());
        } catch (Exception e) {
            System.out.println("error: " + e.toString());
            e.printStackTrace();
        }
        celda_temp.setPhrase(new Phrase("Identificacion: \n" + (cadena) + " " + (bean_pers.getIdentificacion()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Fecha de expedicion: \n" + (bean_pers.getFechaExpedicionId().substring(0, 10).equals("0099-01-01") ? "" : bean_pers.getFechaExpedicionId().substring(0, 10)), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        cadena = "";
        try {
            cadena = this.nombreDept(bean_pers.getDptoExpedicionId());
        } catch (Exception e) {
            System.out.println("error: " + e.toString());
            e.printStackTrace();
        }
        celda_temp.setPhrase(new Phrase("Departamento: \n" + (cadena), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        cadena = "";
        try {
            cadena = this.nombreCiudad(bean_pers.getCiudadExpedicionId());
        } catch (Exception e) {
            System.out.println("error: " + e.toString());
            e.printStackTrace();
        }
        celda_temp.setPhrase(new Phrase("Ciudad: \n" + (cadena), fuente));
        tabla_temp.addCell(celda_temp);
        if (bean_pers.getTipo().equals("E")) {
            celda_temp = new PdfPCell();
            cadena = "";
            try {
                cadena = this.nombreTablagen("PARENT", est.getParentescoGirador());
            } catch (Exception e) {
                System.out.println("error: " + e.toString());
                e.printStackTrace();
            }
            celda_temp.setPhrase(new Phrase("Parentesco con el Girador: \n" + (cadena), fuente));
            celda_temp.setColspan(2);
            tabla_temp.addCell(celda_temp);

        } else {
            celda_temp = new PdfPCell();
            cadena = "";
            try {
                cadena = this.nombreTablagen("NIVEST", bean_pers.getNivelEstudio());
            } catch (Exception e) {
                System.out.println("error: " + e.toString());
                e.printStackTrace();
            }
            celda_temp.setPhrase(new Phrase("Nivel de estudio: \n" + (cadena), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Profesion: \n" + (bean_pers.getProfesion()), fuente));
            tabla_temp.addCell(celda_temp);
        }
        //fila 3
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Fecha de nacimiento: \n" + (bean_pers.getFechaNacimiento().substring(0, 10)), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        cadena = "";
        try {
            cadena = this.nombreDept(bean_pers.getDptoNacimiento());
        } catch (Exception e) {
            System.out.println("error: " + e.toString());
            e.printStackTrace();
        }
        celda_temp.setPhrase(new Phrase("Departamento: \n" + (cadena), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        cadena = "";
        try {
            cadena = this.nombreCiudad(bean_pers.getCiudadNacimiento());
        } catch (Exception e) {
            System.out.println("error: " + e.toString());
            e.printStackTrace();
        }
        celda_temp.setPhrase(new Phrase("Ciudad: \n" + (cadena), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Personas a cargo: \n" + (bean_pers.getPersonasACargo().equals("0") ? "" : bean_pers.getPersonasACargo()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("No. de hijos: \n" + (bean_pers.getNumHijos().equals("0") ? "" : bean_pers.getNumHijos()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Total grupo familiar: \n" + (bean_pers.getTotalGrupoFamiliar().equals("0") ? "" : bean_pers.getTotalGrupoFamiliar()), fuente));
        tabla_temp.addCell(celda_temp);
        //fila 4
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Direccion residencia: \n" + (bean_pers.getDireccion()), fuente));
        celda_temp.setColspan(2);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        cadena = "";
        try {
            cadena = this.nombreDept(bean_pers.getDepartamento());
        } catch (Exception e) {
            System.out.println("error: " + e.toString());
            e.printStackTrace();
        }
        celda_temp.setPhrase(new Phrase("Departamento: \n" + (cadena), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        cadena = "";
        try {
            cadena = this.nombreCiudad(bean_pers.getCiudad());
        } catch (Exception e) {
            System.out.println("error: " + e.toString());
            e.printStackTrace();
        }
        celda_temp.setPhrase(new Phrase("Ciudad: \n" + (cadena), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Barrio: \n" + (bean_pers.getBarrio()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Estrato: \n" + (bean_pers.getEstrato()), fuente));
        tabla_temp.addCell(celda_temp);
        //fila 5
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("E-mail: \n" + (bean_pers.getEmail()), fuente));
        celda_temp.setColspan(2);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Tipo de vivienda: \n" + (bean_pers.getTipoVivienda()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Tiempo residensia: \n" + (bean_pers.getTiempoResidencia().equals("0 A�os 0 Meses") ? "  A�os   Meses" : bean_pers.getTiempoResidencia()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Telefono: \n" + (bean_pers.getTelefono()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Celular: \n" + (bean_pers.getCelular()), fuente));
        tabla_temp.addCell(celda_temp);
        if (bean_pers.getTipo().equals("E")) {
            //fila 6
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Nombre de la universidad: \n" + (est.getUniversidad()), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Programa: \n" + (est.getPrograma()), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Fecha Ingreso Programa: \n" + (est.getFechaIngresoPrograma().substring(0, 10)), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Codigo Alumno: \n" + (est.getCodigo()), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Semestre a cursar: \n" + (est.getSemestre()), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Valor Semestre : \n" + (est.getValorSemestre()), fuente));
            tabla_temp.addCell(celda_temp);

            //fila 7
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Tipo de Carrera: \n" + (est.getTipoCarrera()), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Trabaja: \n" + (est.getTrabaja()), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Nombre de la Empresa: \n" + (est.getNombreEmpresa()), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Direccion: \n" + (est.getDireccionEmpresa()), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Telefono: \n" + (est.getTelefonoEmpresa()), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Salario : \n" + (Util.customFormat(Double.parseDouble( est.getSalario()))), fuente));
            tabla_temp.addCell(celda_temp);
        }

        return tabla_temp;
    }

    protected PdfPTable tablaConyuge(SolicitudPersona bean_pers) {
        PdfPTable tabla_temp = new PdfPTable(6);
        Font fuente = new Font(Font.ITALIC, 6);
        PdfPCell celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Primer apellido: \n" + (bean_pers.getPrimerApellidoCony()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Segundo apellido: \n" + (bean_pers.getSegundoApellidoCony()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Primer nombre: \n" + (bean_pers.getPrimerNombreCony()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Segundo nombre: \n" + (bean_pers.getSegundoNombreCony()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        String cadena = "";
        try {
            cadena = this.nombreTablagen("TIPID", bean_pers.getTipoIdentificacionCony());
        } catch (Exception e) {
            System.out.println("error: " + e.toString());
            e.printStackTrace();
        }
        celda_temp.setPhrase(new Phrase("Identificacion: \n" + (cadena) + " " + (bean_pers.getIdentificacionCony()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Celular: \n" + (bean_pers.getCelularCony()), fuente));
        tabla_temp.addCell(celda_temp);
        //fila 2
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Empresa donde labora: \n" + (bean_pers.getEmpresaCony()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Direccion empresa: \n" + (bean_pers.getDireccionEmpresaCony()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Telefono: \n" + (bean_pers.getTelefonoCony()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Cargo: \n" + (bean_pers.getCargoCony()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Salario: \n" + (Util.customFormat(Double.parseDouble( bean_pers.getSalarioCony()))), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("E-mail: \n" + (bean_pers.getEmail()), fuente));
        tabla_temp.addCell(celda_temp);

        return tabla_temp;
    }

    protected PdfPTable tablaLaboral(SolicitudLaboral bean_lab) {
        PdfPTable tabla_temp = new PdfPTable(6);
        Font fuente = new Font(Font.ITALIC, 6);
        PdfPCell celda_temp = new PdfPCell();
        String cadena = "";
        try {
            cadena = this.nombreTablagen("ACTECO", bean_lab.getActividadEconomica());
        } catch (Exception e) {
            System.out.println("error: " + e.toString());
            e.printStackTrace();
        }
        celda_temp.setPhrase(new Phrase("Actividad Economica: \n" + (cadena), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        cadena = "";
        try {
            cadena = this.nombreTablagen("OCUPAC", bean_lab.getOcupacion());
        } catch (Exception e) {
            System.out.println("error: " + e.toString());
            e.printStackTrace();
        }
        celda_temp.setPhrase(new Phrase("Ocupacion: \n" + (cadena), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Nit/Rut(para independientes): \n" + (bean_lab.getNit()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Nombre de la Empresa: \n" + (bean_lab.getNombreEmpresa()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Cargo: \n" + (bean_lab.getCargo()), fuente));
        tabla_temp.addCell(celda_temp);

        //fila 2
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Fecha de Ingreso: \n" + (bean_lab.getFechaIngreso().substring(0, 10).equals("0099-01-01") ? "" : bean_lab.getFechaIngreso().substring(0, 10)), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Tipo de Contrato: \n" + (bean_lab.getTipoContrato()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Eps: \n" + (bean_lab.getEps()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Telefono: \n" + (bean_lab.getTelefono()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Celular: \n" + (bean_lab.getCelular()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("E-mail: \n" + (bean_lab.getEmail()), fuente));
        tabla_temp.addCell(celda_temp);
        //fila 3
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Direccion: \n" + (bean_lab.getDireccion()), fuente));
        celda_temp.setColspan(2);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        cadena = "";
        try {
            cadena = this.nombreDept(bean_lab.getDepartamento());
        } catch (Exception e) {
            System.out.println("error: " + e.toString());
            e.printStackTrace();
        }
        celda_temp.setPhrase(new Phrase("Departamento: \n" + (cadena), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        cadena = "";
        try {
            cadena = this.nombreCiudad(bean_lab.getCiudad());
        } catch (Exception e) {
            System.out.println("error: " + e.toString());
            e.printStackTrace();
        }
        celda_temp.setPhrase(new Phrase("Ciudad: \n" + (cadena), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Direccion de Cobro: \n" + (bean_lab.getDireccionCobro()), fuente));
        celda_temp.setColspan(2);
        tabla_temp.addCell(celda_temp);

        //fila 4
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Salario: \n" + Util.customFormat(Double.parseDouble( (bean_lab.getSalario()))), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Otros Ingresos: \n" + (Double.parseDouble(bean_lab.getOtrosIngresos()) == 0 ? "" : bean_lab.getOtrosIngresos()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Concepto otros ingresos: \n" + (bean_lab.getConceptoOtrosIng()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Gastos manutencion: \n" + (bean_lab.getGastosManutencion()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Gastos por credito: \n" + (bean_lab.getGastosCreditos()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Gastos por arriendo o cuota de vivienda: \n" + (bean_lab.getGastosArriendo()), fuente));
        tabla_temp.addCell(celda_temp);
        return tabla_temp;
    }

    protected PdfPTable tablaBienVeh(ArrayList<SolicitudBienes> bienList, ArrayList<SolicitudVehiculo> vehList) {
        PdfPTable tabla_temp = new PdfPTable(6);
        Font fuente = new Font(Font.ITALIC, 6);
        PdfPCell celda_temp = new PdfPCell();
        // fila 1 bienes
        for (int i = 0; i < bienList.size(); i++) {
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Tipo de bien: \n" + (bienList.get(i).getTipoBien()), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Hipoteca: \n" + (bienList.get(i).getHipoteca()), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("A favor de: \n" + (bienList.get(i).getaFavorDe()), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Valor Comercial: \n" + (bienList.get(i).getValorComercial()), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Direccion: \n" + (bienList.get(i).getDireccion()), fuente));
            tabla_temp.addCell(celda_temp);
        }

        //fila 2 vehiculos
        for (int i = 0; i < vehList.size(); i++) {
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Marca del vehiculo: \n" + (vehList.get(i).getMarca()), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Tipo de vehiculo: \n" + (vehList.get(i).getTipoVehiculo()), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Placa: \n" + (vehList.get(i).getPlaca()) + " Modelo: " + (vehList.get(i).getModelo()), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Valor Comercial: \n" + (vehList.get(i).getValorComercial()), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Cuota Mensual: \n" + (vehList.get(i).getCuotaMensual()), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Pignorado a favor de: \n" + (vehList.get(i).getPignoradoAFavorDe()), fuente));
            tabla_temp.addCell(celda_temp);
        }
        return tabla_temp;
    }

    protected PdfPTable tablaReferencia(ArrayList<SolicitudReferencias> refList) {
        PdfPTable tabla_temp = new PdfPTable(6);
        Font fuente = new Font(Font.ITALIC, 6);
        PdfPCell celda_temp = new PdfPCell();

        for (int i = 0; i < refList.size(); i++) {
            if (!refList.get(i).getTipoReferencia().equals("C")) {
                // fila 1
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("Primer Apellido: \n" + (refList.get(i).getPrimerApellido()), fuente));
                tabla_temp.addCell(celda_temp);
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("Segundo Apellido: \n" + (refList.get(i).getSegundoApellido()), fuente));
                tabla_temp.addCell(celda_temp);
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("Primer Nombre: \n" + (refList.get(i).getPrimerNombre()), fuente));
                tabla_temp.addCell(celda_temp);
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("Segundo Nombre: \n" + (refList.get(i).getSegundoNombre()), fuente));
                tabla_temp.addCell(celda_temp);
                celda_temp = new PdfPCell();
                if (refList.get(i).getTipoReferencia().equals("P")) {
                    celda_temp.setPhrase(new Phrase("Tiempo de conocido: \n" + (refList.get(i).getTiempoConocido()), fuente));
                } else {
                    celda_temp.setPhrase(new Phrase("Parentesco: \n" + (refList.get(i).getParentesco()), fuente));
                }
                tabla_temp.addCell(celda_temp);
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("Celular: \n" + (refList.get(i).getCelular()), fuente));
                tabla_temp.addCell(celda_temp);

                // fila 2
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("Telefono: \n" + (refList.get(i).getTelefono()), fuente));
                tabla_temp.addCell(celda_temp);
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("telefono 2: \n" + (refList.get(i).getTelefono2()), fuente));
                tabla_temp.addCell(celda_temp);
                celda_temp = new PdfPCell();
                String cadena = "";
                try {
                    cadena = this.nombreDept(refList.get(i).getDepartamento());
                } catch (Exception e) {
                    System.out.println("error: " + e.toString());
                    e.printStackTrace();
                }
                celda_temp.setPhrase(new Phrase("Departamento: \n" + (cadena), fuente));
                tabla_temp.addCell(celda_temp);
                celda_temp = new PdfPCell();
                cadena = "";
                try {
                    cadena = this.nombreCiudad(refList.get(i).getCiudad());
                } catch (Exception e) {
                    System.out.println("error: " + e.toString());
                    e.printStackTrace();
                }
                celda_temp.setPhrase(new Phrase("Ciudad: \n" + (cadena), fuente));
                tabla_temp.addCell(celda_temp);
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("E-mail: \n" + (refList.get(i).getEmail()), fuente));
                tabla_temp.addCell(celda_temp);
            } else {
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("Nombre o razon social: \n" + (refList.get(i).getNombre()), fuente));
                tabla_temp.addCell(celda_temp);
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("Direccion: \n" + (refList.get(i).getDireccion()), fuente));
                tabla_temp.addCell(celda_temp);
                celda_temp = new PdfPCell();
                String cadena = "";
                try {
                    cadena = this.nombreDept(refList.get(i).getDepartamento());
                } catch (Exception e) {
                    System.out.println("error: " + e.toString());
                    e.printStackTrace();
                }
                celda_temp.setPhrase(new Phrase("Departamento: \n" + (cadena), fuente));
                tabla_temp.addCell(celda_temp);
                celda_temp = new PdfPCell();
                cadena = "";
                try {
                    cadena = this.nombreCiudad(refList.get(i).getCiudad());
                } catch (Exception e) {
                    System.out.println("error: " + e.toString());
                    e.printStackTrace();
                }
                celda_temp.setPhrase(new Phrase("Ciudad: \n" + (cadena), fuente));
                tabla_temp.addCell(celda_temp);
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("Telefono: \n" + (refList.get(i).getTelefono()), fuente));
                tabla_temp.addCell(celda_temp);
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("telefono 2: \n" + (refList.get(i).getTelefono2()), fuente));
                tabla_temp.addCell(celda_temp);

            }
        }
        return tabla_temp;
    }

    protected PdfPTable tablaHijos(ArrayList<SolicitudHijos> hijList) {
        PdfPTable tabla_temp = new PdfPTable(6);
        Font fuente = new Font(Font.ITALIC, 6);
        PdfPCell celda_temp = new PdfPCell();
        for (int i = 0; i < hijList.size(); i++) {
            // fila 1
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Nombre: \n" + (hijList.get(i).getNombre()), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("edad: \n" + (hijList.get(i).getEdad()), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Telefono: \n" + (hijList.get(i).getTelefono()), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Direccion: \n" + (hijList.get(i).getDireccion()), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Email: \n" + (hijList.get(i).getEmail()), fuente));
            celda_temp.setColspan(2);
            tabla_temp.addCell(celda_temp);
        }
        return tabla_temp;
    }

    protected PdfPTable tablaCuentas(ArrayList<SolicitudCuentas> lista_cuentas) {
        PdfPTable tabla_temp = new PdfPTable(6);

        Font fuente = new Font(Font.ITALIC, 6);
        PdfPCell celda_temp = new PdfPCell();
        if (lista_cuentas != null && lista_cuentas.size() > 0) {
            for (int i = 0; i < lista_cuentas.size(); i++) {
                SolicitudCuentas cuenta = null;
                cuenta = lista_cuentas.get(i);
                String tipo_cuenta = "";
                try {
                    tipo_cuenta = this.nombreTablagen("TIPCUE", cuenta.getTipo());
                } catch (Exception e) {
                    System.out.println("error: " + e.toString());
                    e.printStackTrace();
                }
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("Tipo de cuenta: \n" + (tipo_cuenta), fuente));
                tabla_temp.addCell(celda_temp);
                celda_temp = new PdfPCell();
                String banco = "";
                GestionConveniosService gcserv = new GestionConveniosService(this.gsadao.getDatabaseName());
                ArrayList<String> listacad = null;
                try {
                    if (!cuenta.getBanco().equals("")) {
                        listacad = gcserv.buscarBancos("codigo", cuenta.getBanco());
                        banco = ((listacad.get(0)).split(";_;"))[1];
                    }
                } catch (Exception e) {
                    System.out.println("error: " + e.toString());
                    e.printStackTrace();
                }
                celda_temp.setPhrase(new Phrase("Banco: \n" + (banco), fuente));
                tabla_temp.addCell(celda_temp);
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("Numero de cuenta: \n" + (cuenta.getCuenta()), fuente));
                tabla_temp.addCell(celda_temp);
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("Fecha de apertura: \n" + (cuenta.getFechaApertura().length() > 9 ? cuenta.getFechaApertura().substring(0, 10) : ""), fuente));
                tabla_temp.addCell(celda_temp);
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("Numero de tarjeta: \n" + (cuenta.getNumeroTarjeta()), fuente));
                tabla_temp.addCell(celda_temp);
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("\n", fuente));
                tabla_temp.addCell(celda_temp);
            }
        } else {
            System.out.println("lista de cuentas vacia o nula...");
        }
        return tabla_temp;
    }

    protected PdfPTable tablaDocs(int num_solicitud, SolicitudAval bean_sol) {
        PdfPTable tabla_temp = new PdfPTable(4);
        tabla_temp.setWidthPercentage(100);
        PdfPTable tabla_tot = new PdfPTable(2);
        tabla_tot.setWidthPercentage(100);
        PdfPCell celda_tot = new PdfPCell();
        Font fuente = new Font(Font.ITALIC, 9);
        PdfPCell celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Valor solicitado: \n" + bean_sol.getValorSolicitado(), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Valor aprobado: \n" + bean_sol.getValorAprobado(), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Tipo de negocio: \n", fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("No. tipo de negocio: \n" + bean_sol.getTipoNegocio(), fuente));
        tabla_temp.addCell(celda_temp);
        String banco = "";
        GestionConveniosService gcserv = new GestionConveniosService(this.gsadao.getDatabaseName());
        ArrayList<String> listacad = null;
        try {
            listacad = gcserv.buscarBancos("codigo", bean_sol.getBanco());
            if (!listacad.isEmpty()) {
                banco = ((listacad.get(0)).split(";_;"))[1];
            } else {
                banco = "";
            }
        } catch (Exception e) {
            System.out.println("error: " + e.toString());
            e.printStackTrace();
        }
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Banco: \n" + banco, fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Cod. banco: \n" + bean_sol.getBanco(), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Sucursal: \n" + bean_sol.getSucursal(), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("No. de chequera: \n" + bean_sol.getNumChequera(), fuente));
        tabla_temp.addCell(celda_temp);
        celda_tot.setColspan(2);
        celda_tot.addElement(tabla_temp);
        tabla_tot.addCell(celda_tot);
        //docs
        tabla_temp = new PdfPTable(2);
        PdfPTable tabla_dc = new PdfPTable(3);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("No. de titulo: \n", fuente));
        tabla_dc.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Valor: \n", fuente));
        tabla_dc.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Fecha: \n", fuente));
        tabla_dc.addCell(celda_temp);
        //listado de docs
        ArrayList<SolicitudDocumentos> lista_docs = null;
        int tam_docs = 0;
        try {
            lista_docs = this.buscarDocsSolicitud(num_solicitud);
            tam_docs = lista_docs.size();
        } catch (Exception e) {
            System.out.println("error: " + e.toString());
            e.printStackTrace();
        }
        //ciclo
        int limite = Math.round(tam_docs / 2) + 1;
        for (int i = 1; i < limite; i++) {
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(lista_docs.get(i - 1).getNumTitulo(), fuente));
            tabla_dc.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("$" + Util.customFormat(Double.parseDouble(lista_docs.get(i - 1).getValor())), fuente));
            tabla_dc.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(lista_docs.get(i - 1).getFecha().substring(0, 10), fuente));
            tabla_dc.addCell(celda_temp);
        }
        tabla_dc.setWidthPercentage(100);
        tabla_tot.addCell(tabla_dc);
        //agregar 1
        celda_tot = new PdfPCell();
        //celda_tot.addElement(tabla_dc);
        /*celda_temp = new PdfPCell();
        celda_temp.addElement(tabla_dc);
        tabla_temp.addCell(celda_temp);*/
        //head2
        tabla_dc = new PdfPTable(3);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("No. de titulo: \n", fuente));
        tabla_dc.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Valor: \n", fuente));
        tabla_dc.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Fecha: \n", fuente));
        tabla_dc.addCell(celda_temp);
        //ciclo 2
        for (int i = limite; i < tam_docs + 1; i++) {
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(lista_docs.get(i - 1).getNumTitulo(), fuente));
            tabla_dc.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("$" + Util.customFormat(Double.parseDouble(lista_docs.get(i - 1).getValor())), fuente));
            tabla_dc.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(lista_docs.get(i - 1).getFecha().substring(0, 10), fuente));
            tabla_dc.addCell(celda_temp);
        }
        tabla_dc.setWidthPercentage(100);
        //agregar2
        celda_tot.addElement(tabla_dc);
        /*celda_temp = new PdfPCell();
        celda_temp.addElement(tabla_dc);
        tabla_temp.addCell(celda_temp);*/
        tabla_tot.addCell(celda_tot);
        tabla_tot.setWidthPercentage(100);
        return tabla_tot;
    }

    protected PdfPTable tablaPagare(int num_solicitud, SolicitudAval bean_sol) {
        PdfPTable tabla_temp = new PdfPTable(3);
        Font fuente = new Font(Font.ITALIC, 6);
        PdfPCell celda_temp = new PdfPCell();
        String textoPag = "";
        try {
            if (bean_sol.getTipoConv().equals("Multiservicio")) {
                textoPag = this.nombreTablagen("TPAGFIN", "01");
            } else {
                textoPag = this.nombreTablagen("TPAGFEN", "01");
            }
        } catch (Exception e) {
            System.out.println("error: " + e.toString());
            e.printStackTrace();
        }
        celda_temp.setPhrase(new Phrase(textoPag, fuente));
        celda_temp.setBorder(0);
        celda_temp.setColspan(2);
        celda_temp.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
        tabla_temp.addCell(celda_temp);
        PdfPTable tabla_int = new PdfPTable(2);
        tabla_int.setWidthPercentage(100);
        PdfPCell celda_int = new PdfPCell();
        celda_int.setPhrase(new Phrase("\n\n\nRepresentante legal y deudor solidario, o girador"
                + "\n\n\n___________________\n\nC.C. ___________________", fuente));
        celda_int.setBorder(0);
        tabla_int.addCell(celda_int);
        PdfPTable tabla_hue = new PdfPTable(1);
        tabla_hue.setWidthPercentage(70);
        PdfPCell celda_hue = new PdfPCell();
        celda_hue.setFixedHeight(90);
        celda_hue.setPhrase(new Phrase("\n\n\n", fuente));
        tabla_hue.addCell(celda_hue);
        celda_hue = new PdfPCell();
        celda_hue.setBorder(0);
        celda_hue.setNoWrap(true);
        celda_hue.setPhrase(new Phrase("Huella indice derecho", fuente));
        tabla_hue.addCell(celda_hue);
        //

        celda_int.addElement(tabla_hue);
        celda_int.setBorder(0);
        tabla_int.addCell(celda_int);
        celda_int.setPhrase(new Phrase("\n\n\ndeudor solidario"
                + "\n\n\n___________________\n\nC.C. ___________________", fuente));
        tabla_int.addCell(celda_int);
        //
        tabla_hue = new PdfPTable(1);
        tabla_hue.setWidthPercentage(70);
        celda_hue = new PdfPCell();
        celda_hue.setFixedHeight(90);
        celda_hue.setPhrase(new Phrase("\n\n\n", fuente));
        tabla_hue.addCell(celda_hue);
        celda_hue = new PdfPCell();
        celda_hue.setBorder(0);
        celda_hue.setPhrase(new Phrase("Huella indice derecho", fuente));
        celda_hue.setNoWrap(true);
        tabla_hue.addCell(celda_hue);
        celda_int.addElement(tabla_hue);
        celda_int.setBorder(0);
        tabla_int.addCell(celda_int);
        celda_temp = new PdfPCell();
        celda_temp.addElement(tabla_int);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        return tabla_temp;
    }

    protected PdfPTable textoPagare() {
        PdfPTable tabla_temp = new PdfPTable(5);
        Font fuente = new Font(Font.ITALIC, 10);
        PdfPCell celda_temp = new PdfPCell();
        String textoPag = "";
        try {
            textoPag = this.nombreTablagen("TPAGFEN", "02");
        } catch (Exception e) {
            System.out.println("error: " + e.toString());
            e.printStackTrace();
        }
        celda_temp.setPhrase(new Phrase(textoPag, fuente));
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);
        return tabla_temp;
    }

    protected PdfPTable texto1Pag2(String tipo_conv) throws Exception {
        PdfPTable tabla_temp = new PdfPTable(1);
        Font fuente = new Font(Font.ITALIC, 7);
        Font fuenteD = new Font(Font.ITALIC, 8, Font.BOLD, java.awt.Color.white);
        PdfPCell celda_temp = new PdfPCell();
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.addElement(new Phrase(tipo_conv.equals("Multiservicio") ? this.refTablagen("TPAGFIN", "03") : this.refTablagen("TPAGFEN", "03"), fuenteD));
        celda_temp.setBackgroundColor(java.awt.Color.BLACK);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        Paragraph p = new Paragraph(tipo_conv.equals("Multiservicio") ? this.nombreTablagen("TPAGFIN", "03") : this.nombreTablagen("TPAGFEN", "03"), fuente);
        p.setAlignment(Element.ALIGN_JUSTIFIED);
        celda_temp.addElement(p);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        return tabla_temp;
    }

    protected PdfPTable texto3Pag2(String tipo_conv) throws Exception {
        PdfPTable tabla_temp = new PdfPTable(2);
        Font fuente = new Font(Font.ITALIC, 7);
        Font fuenteD = new Font(Font.ITALIC, 8, Font.BOLD, java.awt.Color.white);
        PdfPCell celda_temp = new PdfPCell();
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.addElement(new Phrase(tipo_conv.equals("Multiservicio") ? this.refTablagen("TPAGFIN", "05") : this.refTablagen("TPAGFEN", "05"), fuenteD));
        celda_temp.setBackgroundColor(java.awt.Color.BLACK);
        celda_temp.setColspan(2);
        celda_temp.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        Paragraph p = new Paragraph(tipo_conv.equals("Multiservicio") ? this.nombreTablagen("TPAGFIN", "05") : this.nombreTablagen("TPAGFEN", "05"), fuente);
        p.setAlignment(Element.ALIGN_JUSTIFIED);
        celda_temp.addElement(p);
        celda_temp.setColspan(2);
        tabla_temp.addCell(celda_temp);
        PdfPTable tabla_int = new PdfPTable(3);
        tabla_int.setWidthPercentage(100);
        PdfPCell celda_int = new PdfPCell();
        celda_int.setPhrase(new Phrase("\n\n\n\n\n________________________________________\n\nFirma deudor o Representante Legal"
                + "\n\nNombre Completo:_________________________________\n\nC.C:___________________________________\n\nNIT:____________________________________", fuente));
        celda_int.setBorder(0);
        celda_int.setColspan(2);
        tabla_int.addCell(celda_int);
        PdfPTable tabla_hue = new PdfPTable(1);
        tabla_hue.setWidthPercentage(70);
        PdfPCell celda_hue = new PdfPCell();
        celda_hue.setFixedHeight(90);
        celda_hue.setPhrase(new Phrase("\n\n\n", fuente));
        tabla_hue.addCell(celda_hue);
        celda_hue = new PdfPCell();
        celda_hue.setBorder(0);
        celda_hue.setNoWrap(true);
        celda_hue.setPhrase(new Phrase("Huella indice derecho", fuente));
        tabla_hue.addCell(celda_hue);
        //
        celda_int.addElement(tabla_hue);
        celda_int.setBorder(0);
        tabla_int.addCell(celda_int);

        celda_temp = new PdfPCell();
        celda_temp.addElement(tabla_int);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.addElement(tabla_int);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        return tabla_temp;
    }

    protected PdfPTable texto2Pag2(String tipo_conv) throws Exception {
        PdfPTable tabla_temp = new PdfPTable(1);
        Font fuente = new Font(Font.ITALIC, 7);
        Font fuenteD = new Font(Font.ITALIC, 8, Font.BOLD, java.awt.Color.white);
        PdfPCell celda_temp = new PdfPCell();
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.addElement(new Phrase(tipo_conv.equals("Multiservicio") ? this.refTablagen("TPAGFIN", "04") : this.refTablagen("TPAGFEN", "04"), fuenteD));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda_temp.setBackgroundColor(java.awt.Color.BLACK);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        Paragraph p = new Paragraph(tipo_conv.equals("Multiservicio") ? this.nombreTablagen("TPAGFIN", "04") : this.nombreTablagen("TPAGFEN", "04"), fuente);
        p.setAlignment(Element.ALIGN_JUSTIFIED);
        celda_temp.addElement(p);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        return tabla_temp;
    }

    protected PdfPTable texto4Pag2(String tipo_conv) throws Exception {
        PdfPTable tabla_temp = new PdfPTable(1);
        Font fuente = new Font(Font.ITALIC, 7);
        Font fuenteD = new Font(Font.ITALIC, 8, Font.BOLD, java.awt.Color.white);
        PdfPCell celda_temp = new PdfPCell();
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.addElement(new Phrase(tipo_conv.equals("Multiservicio") ? this.refTablagen("TPAGFIN", "04") : this.refTablagen("TPAGFEN", "06"), fuenteD));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda_temp.setBackgroundColor(java.awt.Color.BLACK);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        Paragraph p = new Paragraph(tipo_conv.equals("Multiservicio") ? this.nombreTablagen("TPAGFIN", "04") : this.nombreTablagen("TPAGFEN", "04"), fuente);
        p.setAlignment(Element.ALIGN_JUSTIFIED);
        celda_temp.addElement(p);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        return tabla_temp;
    }

    protected PdfPTable texto5Pag2() throws Exception {
        PdfPTable tabla_temp = new PdfPTable(2);
        Font fuente = new Font(Font.ITALIC, 7);
        Font fuenteD = new Font(Font.ITALIC, 8, Font.BOLD, java.awt.Color.white);
        Font fuenteE = new Font(Font.ITALIC, 7);
        PdfPCell celda_temp = new PdfPCell();
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.addElement(new Phrase(this.refTablagen("TPAGFEN", "07"), fuenteD));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda_temp.setBackgroundColor(java.awt.Color.BLACK);
        celda_temp.setColspan(2);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.addElement(new Phrase(this.nombreTablagen("TPAGFEN", "07"), fuente));
        tabla_temp.addCell(celda_temp);
        PdfPTable tabla_int = new PdfPTable(2);
        tabla_int.setWidthPercentage(100);
        PdfPCell celda_int = new PdfPCell();
        celda_int.setPhrase(new Phrase("DATOS PERSONALES DEL ASEGURDOR\n", fuenteE));
        celda_int.setBorder(0);
        celda_int.setColspan(2);
        tabla_int.addCell(celda_int);
        celda_int = new PdfPCell();
        celda_int.setPhrase(new Phrase("NOMBRES Y APELLIDOS DEL SOLICITANTE\n", fuente));
        celda_int.setBorder(0);
        celda_int.setColspan(2);
        tabla_int.addCell(celda_int);
        celda_int = new PdfPCell();
        celda_int.setPhrase(new Phrase("____________________________________________________\n", fuente));
        celda_int.setBorder(0);
        celda_int.setColspan(2);
        tabla_int.addCell(celda_int);
        celda_int = new PdfPCell();
        celda_int.setPhrase(new Phrase("____________________________________________________\n", fuente));
        celda_int.setBorder(0);
        celda_int.setColspan(2);
        tabla_int.addCell(celda_int);
        celda_int = new PdfPCell();
        celda_int.setPhrase(new Phrase("CEDULA", fuente));
        celda_int.setBorder(0);
        tabla_int.addCell(celda_int);
        celda_int = new PdfPCell();
        celda_int.setPhrase(new Phrase("EDAD", fuente));
        celda_int.setBorder(0);
        tabla_int.addCell(celda_int);
        celda_int = new PdfPCell();
        celda_int.setPhrase(new Phrase("_______________________", fuente));
        celda_int.setBorder(0);
        tabla_int.addCell(celda_int);
        celda_int = new PdfPCell();
        celda_int.setPhrase(new Phrase("________________________\n", fuente));
        celda_int.setBorder(0);
        tabla_int.addCell(celda_int);
        celda_int = new PdfPCell();
        celda_int.setPhrase(new Phrase("PROFESION\n", fuente));
        celda_int.setBorder(0);
        celda_int.setColspan(2);
        tabla_int.addCell(celda_int);
        celda_int = new PdfPCell();
        celda_int.setPhrase(new Phrase("____________________________________________________\n", fuente));
        celda_int.setBorder(0);
        celda_int.setColspan(2);
        tabla_int.addCell(celda_int);
        celda_int = new PdfPCell();
        celda_int.setPhrase(new Phrase("VALOR ASEGURADO", fuente));
        celda_int.setBorder(0);
        celda_int.setColspan(2);
        tabla_int.addCell(celda_int);
        celda_int = new PdfPCell();
        celda_int.setPhrase(new Phrase("____________________________________________________\n", fuente));
        celda_int.setBorder(0);
        celda_int.setColspan(2);
        tabla_int.addCell(celda_int);
        celda_int = new PdfPCell();
        celda_int.setPhrase(new Phrase("ESTATURA CMS:", fuente));
        celda_int.setBorder(0);
        tabla_int.addCell(celda_int);
        celda_int = new PdfPCell();
        celda_int.setPhrase(new Phrase("PESO KG:", fuente));
        celda_int.setBorder(0);
        tabla_int.addCell(celda_int);
        celda_int = new PdfPCell();
        celda_int.setPhrase(new Phrase("_______________________", fuente));
        celda_int.setBorder(0);
        tabla_int.addCell(celda_int);
        celda_int = new PdfPCell();
        celda_int.setPhrase(new Phrase("________________________\n", fuente));
        celda_int.setBorder(0);
        tabla_int.addCell(celda_int);
        celda_int = new PdfPCell();
        celda_int.setPhrase(new Phrase("____________________________________________________\n", fuente));
        celda_int.setBorder(0);
        celda_int.setColspan(2);
        tabla_int.addCell(celda_int);
        celda_int = new PdfPCell();
        celda_int.setPhrase(new Phrase("FIRMA DEL ASEGURADOR\n", fuente));
        celda_int.setBorder(0);
        celda_int.setColspan(2);
        tabla_int.addCell(celda_int);
        celda_int = new PdfPCell();
        celda_int.setPhrase(new Phrase("C.C. NO:", fuente));
        celda_int.setBorder(0);
        tabla_int.addCell(celda_int);
        PdfPTable tabla_hue = new PdfPTable(1);
        tabla_hue.setWidthPercentage(70);
        PdfPCell celda_hue = new PdfPCell();
        celda_hue.setFixedHeight(90);
        celda_hue.setPhrase(new Phrase("\n\n\n", fuente));
        tabla_hue.addCell(celda_hue);
        celda_hue = new PdfPCell();
        celda_hue.setBorder(0);
        celda_hue.setNoWrap(true);
        celda_hue.setPhrase(new Phrase("Huella indice derecho", fuente));
        tabla_hue.addCell(celda_hue);
        //
        celda_int.addElement(tabla_hue);
        celda_int.setBorder(0);
        tabla_int.addCell(celda_int);

        celda_temp = new PdfPCell();
        celda_temp.addElement(tabla_int);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        return tabla_temp;


    }

    protected PdfPTable tablaNegocio(SolicitudNegocio bean_neg, String tipopers) throws Exception {
        GestionConveniosService cvserv = new GestionConveniosService(this.gsadao.getDatabaseName());
        float[] widths = {0.33f, 0.16f, 0.16f, 0.09f, 0.08f, 0.09f, 0.09f};
        PdfPTable tabla_temp = new PdfPTable(widths);
        Font fuente = new Font(Font.ITALIC, 7);
        PdfPCell celda_temp = new PdfPCell();
        if (tipopers.equals("N")) {
            celda_temp.setPhrase(new Phrase("Nombre: \n" + (bean_neg.getNombre()), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(" ", fuente));
            celda_temp.setColspan(6);
            tabla_temp.addCell(celda_temp);
            //fila 2
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Direccion: \n" + (bean_neg.getDireccion()), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            String cadena = "";
            try {
                cadena = this.nombreDept(bean_neg.getDepartamento());
            } catch (Exception e) {
                System.out.println("error: " + e.toString());
                e.printStackTrace();
            }
            celda_temp.setPhrase(new Phrase("Departamento: \n" + (cadena), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            cadena = "";
            try {
                cadena = this.nombreCiudad(bean_neg.getCiudad());
            } catch (Exception e) {
                System.out.println("error: " + e.toString());
                e.printStackTrace();
            }
            celda_temp.setPhrase(new Phrase("Ciudad: \n" + (cadena), fuente));
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Barrio: \n" + (bean_neg.getBarrio()), fuente));
            celda_temp.setColspan(2);
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Telefono: \n" + (bean_neg.getTelefono()), fuente));
            celda_temp.setColspan(2);
            tabla_temp.addCell(celda_temp);
        }
        //fila 3
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Sector: \n" + (cvserv.nombreSector(bean_neg.getSector())), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Subsector: \n" + (cvserv.nombreSubSector(bean_neg.getSector(), bean_neg.getSubsector())), fuente));
        celda_temp.setColspan(2);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("A�os local actual: \n" + (bean_neg.getTiempoLocal()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("A�os exp. negocio actual: \n" + (bean_neg.getNumExpNeg()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("A�os microempresario: \n" + (bean_neg.getTiempoMicroempresario()), fuente));
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("No trabajadores : \n" + (bean_neg.getNumTrabajadores()), fuente));
        tabla_temp.addCell(celda_temp);

        return tabla_temp;
    }

   

  protected PdfPTable getTextoRedes(String code, String tgen) throws Exception {
        PdfPTable tabla_temp = new PdfPTable(1);
        Font fuente = new Font(Font.ITALIC, 9);
        Font fuenteD = new Font(Font.ITALIC, 8, Font.BOLD, java.awt.Color.white);
        PdfPCell celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        Paragraph p = new Paragraph(this.nombreTablagen(tgen, code), fuente);
        p.setAlignment(Element.ALIGN_JUSTIFIED);
        celda_temp.addElement(p);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(100);
        return tabla_temp;
    }

    /************************************** DATOS TABLA 1 ***************************************************/
    protected PdfPTable firmas_deudor() throws DocumentException, com.lowagie.text.DocumentException {



        PdfPTable tabla_temp = new PdfPTable(1);

        Font fuente = new Font(Font.TIMES_ROMAN, 8);
        Font fuenteB = new Font(Font.TIMES_ROMAN, 8, Font.BOLD);

        float[] medidaCeldas = {0.1000f};
        tabla_temp.setWidths(medidaCeldas);

        PdfPCell celda_temp = new PdfPCell();

        celda_temp = new PdfPCell();
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0);
        celda_temp.setPhrase(
                new Phrase("\n"
                + "__________________________________\n"
                + "Firma Deudor o Representante Legal\n"
                + "                                  \n"
                + "_________________________________________\n"        
                + "Nombre Completo Deudor/Representante Legal\n"
                + "                                  \n"
                + "____________________        ___________________\n"
                + "No. C.C. o Nit.                       Lugar de Expedicion \n\n\n ", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);



        tabla_temp.setWidthPercentage(100);

        return tabla_temp;
    }

    /************************************** DATOS TABLA HUELLA 1 ***************************************************/
    protected PdfPTable tablaHuella() throws DocumentException, com.lowagie.text.DocumentException {



        PdfPTable tabla_temp = new PdfPTable(1);

        java.awt.Color color_fuente_header = new java.awt.Color(0xAA, 0xAA, 0xAA);

        Font fuente_header = new Font(Font.TIMES_ROMAN, 7, Font.BOLD);
        fuente_header.setColor(color_fuente_header);

        float[] medidaCeldas = {0.1000f};
        tabla_temp.setWidths(medidaCeldas);

        PdfPCell celda_temp = new PdfPCell();

        celda_temp = new PdfPCell();
        celda_temp.setBorderWidthTop(0.5f);
        celda_temp.setBorderWidthLeft(0.5f);
        celda_temp.setBorderWidthRight(0.5f);
        celda_temp.setBorderWidthBottom(0.5f);
        celda_temp.setPhrase(new Phrase("\n\n\n\n\n\n\n"));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        tabla_temp.setWidthPercentage(50);

        return tabla_temp;
    }

    /************************************** DATOS TABLA 2 ***************************************************/
    protected PdfPTable firmas_codeudor() throws DocumentException, com.lowagie.text.DocumentException {



        PdfPTable tabla_temp = new PdfPTable(1);

        Font fuente = new Font(Font.TIMES_ROMAN, 8);
        Font fuenteB = new Font(Font.TIMES_ROMAN, 8, Font.BOLD);

        float[] medidaCeldas = {0.1000f};
        tabla_temp.setWidths(medidaCeldas);

        PdfPCell celda_temp = new PdfPCell();

        celda_temp = new PdfPCell();
        celda_temp.setBorderWidthTop(0);
        celda_temp.setBorderWidthLeft(0);
        celda_temp.setBorderWidthRight(0);
        celda_temp.setBorderWidthBottom(0);
        celda_temp.setPhrase(
                new Phrase("\n"
                + "__________________________________\n"
                + "Firma Deudor Solidario\n"
                + "                                  \n"        
                + "__________________________________\n"
                + "Nombre Completo Deudor Solidario\n"
                + "                                  \n"        
                + "____________________        ___________________\n"
                + "No. C.C. o Nit.                       Lugar de Expedici�n \n\n\n ", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);



        tabla_temp.setWidthPercentage(100);

        return tabla_temp;
    }

    /************************************** DATOS TABLA HUELLA 2 ***************************************************/
    protected PdfPTable Firmas() throws DocumentException, com.lowagie.text.DocumentException {

        PdfPTable tabla_temp = new PdfPTable(4);

        java.awt.Color color_fuente_header = new java.awt.Color(0xAA, 0xAA, 0xAA);

        Font fuente_header = new Font(Font.TIMES_ROMAN, 12, Font.BOLD);
        fuente_header.setColor(color_fuente_header);

        float[] medidaCeldas = {0.310f, 0.190f, 0.310f, 0.190f};
        tabla_temp.setWidths(medidaCeldas);

        PdfPCell cell = new PdfPCell(); //fila 1

        cell = new PdfPCell();
        cell.setPhrase(new Phrase("", fuente_header));
        cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        cell.addElement(this.firmas_deudor());// agrego tabla 1
        cell.setBorderWidthTop(0);
        cell.setBorderWidthLeft(0);
        cell.setBorderWidthRight(0);
        cell.setBorderWidthBottom(0);
        tabla_temp.addCell(cell);
        tabla_temp.setWidthPercentage(100);



        cell = new PdfPCell();
        cell.setPhrase(new Phrase("", fuente_header));
        cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        cell.addElement(this.tablaHuella());
        cell.setBorderWidthTop(0);
        cell.setBorderWidthLeft(0);
        cell.setBorderWidthRight(0);
        cell.setBorderWidthBottom(0);
        tabla_temp.addCell(cell);
        tabla_temp.setWidthPercentage(100);


        //fila 3

        cell = new PdfPCell();
        cell.setPhrase(new Phrase("", fuente_header));
        cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        cell.addElement(this.firmas_codeudor());
        cell.setBorderWidthTop(0);
        cell.setBorderWidthLeft(0);
        cell.setBorderWidthRight(0);
        cell.setBorderWidthBottom(0);
        tabla_temp.addCell(cell);
        tabla_temp.setWidthPercentage(100);


        //fila 4

        cell = new PdfPCell();
        cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        cell.setBorderWidthTop(0);
        cell.setBorderWidthLeft(0);
        cell.setBorderWidthRight(0);
        cell.setBorderWidthBottom(0);
        cell.addElement(this.tablaHuella());// agrego tabla 1
        tabla_temp.addCell(cell);


        return tabla_temp;
    }

    /******************************** requisitos***************************************/
    protected PdfPTable Requisitos() throws DocumentException, Exception {

        PdfPTable tabla_temp = new PdfPTable(4);
        java.awt.Color fondo = new java.awt.Color(0x33, 0x66, 0x33);
        java.awt.Color color_fuente_header = new java.awt.Color(0xFF, 0xFF, 0xFF);

        Font fuente_header = new Font(Font.TIMES_ROMAN, 9, Font.BOLD);
        fuente_header.setColor(color_fuente_header);
        float[] medidaCeldas = {0.500f, 0.130f, 0.240f, 0.130f};
        tabla_temp.setWidths(medidaCeldas);

        Font fuente = new Font(Font.TIMES_ROMAN, 9);
        Font fuenteB = new Font(Font.TIMES_ROMAN, 9, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();


        // fila 1
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("DOCUMENTOS", fuente_header));
        celda_temp.setBackgroundColor(fondo);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);



        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("PERSONA NATURAL", fuente_header));
        celda_temp.setBackgroundColor(fondo);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("INDEPENDIENTES", fuente_header));
        celda_temp.setBackgroundColor(fondo);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);


        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("PERSONAS JURIDICA", fuente_header));
        celda_temp.setBackgroundColor(fondo);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);



        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(this.nombreTablagen("TPAGFEN_RQ", "01"), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("X", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("X", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("X", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);



        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(this.nombreTablagen("TPAGFEN_RQ", "02"), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("X", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("X", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(this.nombreTablagen("TPAGFEN_RQ", "03"), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("X", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(this.nombreTablagen("TPAGFEN_RQ", "04"), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("X", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(this.nombreTablagen("TPAGFEN_RQ", "05"), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("X", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("X", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);



        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(this.nombreTablagen("TPAGFEN_RQ", "06"), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("X", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("X", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(this.nombreTablagen("TPAGFEN_RQ", "07"), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("X", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("X", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);


        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(this.nombreTablagen("TPAGFEN_RQ", "08"), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("X", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("X", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(this.nombreTablagen("TPAGFEN_RQ", "09"), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("X", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);


        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(this.nombreTablagen("TPAGFEN_RQ", "10"), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("X", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("X", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("X", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);



        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(this.nombreTablagen("TPAGFEN_RQ", "11"), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("X", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("X", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("X", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);


        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(this.nombreTablagen("TPAGFEN_RQ", "12"), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda_temp.setColspan(4);
        tabla_temp.addCell(celda_temp);






        tabla_temp.setWidthPercentage(80);



        return tabla_temp;
    }



       /**
     * clona una solicitud
     * @param idsolicitud
     * @return Sql
     * @throws Exception
      * jpinedo
     */
        public String MarcarSolicitud(String solicitud ,String cod_neg,String idconvenio) throws Exception{
        return gsadao.MarcarSolicitud(solicitud, cod_neg, idconvenio);
    }


         /**
     * Valida si un cliente tiene solicitudes en tramite
     * @param idcliente
     * @return Sql
     * @throws Exception
      * jpinedo
     */
      public boolean ValidarSolicitudCliente(String id) throws Exception {
      return gsadao.ValidarSolicitudCliente(id);
      }




   public String ValidarCambiosPersonas(ArrayList<SolicitudPersona> personaList,ArrayList<SolicitudLaboral> laboralList,ArrayList<SolicitudBienes> nuevos_bienes,ArrayList<SolicitudVehiculo> nuevos_vehiculos,ArrayList<SolicitudReferencias> nuevos_referencias,SolicitudEstudiante nuevo_estudiante,ArrayList<SolicitudCuentas> cuentaList,SolicitudNegocio datos_negocio, ArrayList<ObligacionesCompra> ocompraList) throws Exception {
         String datos = "#titulo";

        for (int i=0;i<personaList.size();i++)
        {
           datos += "#titulo";
            boolean sw=false;
            SolicitudPersona bean_pers = this.buscarPersona(Integer.parseInt(personaList.get(i).getNumeroSolicitud()), personaList.get(i).getTipo());

         if(!bean_pers.getTipoPersona().equals("J"))
         {
            if (!personaList.get(i).getPrimerApellido().equals(bean_pers.getPrimerApellido())) {
                datos += "Primer Apellido : " + " " + bean_pers.getPrimerApellido() + "\n";
                sw=true;
            }
            if (!personaList.get(i).getSegundoApellido().equals(bean_pers.getSegundoApellido())) {
                datos += "Segundo Apellido : " + " " + bean_pers.getSegundoApellido() + "\n";
                sw=true;
            }
            if (!personaList.get(i).getPrimerNombre().equals(bean_pers.getPrimerNombre())) {
                datos += "Primer Nombre : " + " " + bean_pers.getPrimerNombre() + "\n";
                sw=true;
            }

            if (!personaList.get(i).getSegundoNombre().equals(bean_pers.getSegundoNombre())) {
                datos += "Segundo Nombre : " + " " + bean_pers.getSegundoNombre() + "\n";
                sw=true;
            }

            if (!personaList.get(i).getGenero().equals(bean_pers.getGenero())) {
                datos += "Genero : " + " " + bean_pers.getGenero() + "\n";
                sw=true;
            }

            if (!personaList.get(i).getEstadoCivil().equals(bean_pers.getEstadoCivil())) {
                datos += "Estado Civil : " + " " + bean_pers.getEstadoCivil() + "\n";
                sw=true;
            }

            if (!personaList.get(i).getFechaExpedicionId().substring(0, 10).equals(bean_pers.getFechaExpedicionId().substring(0, 10))) {
                datos += "Fecha Exp. Doc : " + " " + bean_pers.getFechaExpedicionId() + "\n";
                sw=true;
            }

            if (!personaList.get(i).getDptoExpedicionId().equals(bean_pers.getDptoExpedicionId())) {
                datos += "Departamento Doc: " + " " + this.nombreDept(bean_pers.getDptoExpedicionId()) + "\n";
                sw=true;
            }

            if (!personaList.get(i).getCiudadExpedicionId().equals(bean_pers.getCiudadExpedicionId())) {
                datos += "Ciudad Doc: " + " " + this.nombreCiudad(bean_pers.getCiudadExpedicionId()) + "\n";
                sw=true;
            }
            if (!personaList.get(i).getNivelEstudio().equals(bean_pers.getNivelEstudio())) {
                datos += "Nivel Estudio : " + " " + bean_pers.getNivelEstudio() + "\n";
                sw=true;
            }
            if (!personaList.get(i).getFechaNacimiento().equals(bean_pers.getFechaNacimiento().substring(0, 10))) {
                datos += "Fecha Nacimiento : " + " " + bean_pers.getFechaNacimiento() + "\n";
                sw=true;
            }

            if (!personaList.get(i).getCiudadNacimiento().equals(bean_pers.getCiudadNacimiento())) {
                datos += "Ciudad : " + " " + this.nombreCiudad(bean_pers.getCiudadNacimiento()) + "\n";
                sw=true;
            }
            if (!personaList.get(i).getDptoNacimiento().equals(bean_pers.getDptoNacimiento())) {
                datos += "Departamento : " + " " + this.nombreDept(bean_pers.getDptoNacimiento()) + "\n";
                sw=true;
            }

            if (!personaList.get(i).getPersonasACargo().equals(bean_pers.getPersonasACargo())) {
                datos += "Personas a Cargo  : " + " " + bean_pers.getPersonasACargo() + "\n";
                sw=true;
            }

            if (!personaList.get(i).getNumHijos().equals(bean_pers.getNumHijos())) {
                datos += "N� de hijos : " + " " + bean_pers.getNumHijos() + "\n";
                sw=true;
            }

            if (!personaList.get(i).getTotalGrupoFamiliar().equals(bean_pers.getTotalGrupoFamiliar())) {
                datos += "Total grupo fmr : " + " " + bean_pers.getTotalGrupoFamiliar() + "\n";
                sw=true;
            }

            if (!personaList.get(i).getDireccion().equals(bean_pers.getDireccion())) {
                datos += "Direccion Residencia : " + " " + bean_pers.getDireccion() + "\n";
                sw=true;
            }
            if (!personaList.get(i).getCiudad().equals(bean_pers.getCiudad())) {
                datos += "Ciudad : " + " " + this.nombreCiudad(bean_pers.getCiudad()) + "\n";
                sw=true;
            }

            if (!personaList.get(i).getDepartamento().equals(bean_pers.getDepartamento())) {
                datos += "Departamento  : " + " " +this.nombreDept(bean_pers.getDepartamento()) + "\n";
                sw=true;
            }

            if (!personaList.get(i).getBarrio().equals(bean_pers.getBarrio())) {
                datos += "Barrio : " + " " + bean_pers.getBarrio() + "\n";
                sw=true;
            }

            if (!personaList.get(i).getEstrato().equals(bean_pers.getEstrato())) {
                datos += "Estrato : " + " " + bean_pers.getEstrato() + "\n";
                sw=true;
            }

            if (!personaList.get(i).getEmail().trim().equals(bean_pers.getEmail().trim())) {
                datos += "E-mail : " + " " + bean_pers.getEmail() + "\n";
                sw=true;
            }
            if (!personaList.get(i).getTipoVivienda().equals(bean_pers.getTipoVivienda())) {
                datos += "Tipo vivienda : " + " "+ bean_pers.getTipoVivienda() + "\n";
                sw=true;
            }

            if (!personaList.get(i).getTiempoResidencia().equals(bean_pers.getTiempoResidencia())) {
                datos += "Tiempo Residencia  : " + " " + bean_pers.getTiempoResidencia() + "\n";
                sw=true;
            }

            if (!personaList.get(i).getTelefono().equals(bean_pers.getTelefono())) {
                datos += "Telefono : " + " " + bean_pers.getTelefono() + "\n";
                sw=true;
            }

            if (!personaList.get(i).getCelular().equals(bean_pers.getCelular())) {
                datos += "Celular : " + " " + bean_pers.getCelular() + "\n";
                sw=true;
            }


            if(!personaList.get(i).getTipo().equals("E")||personaList.get(i).getTipoPersona().equals("J"))
            {
           if (!personaList.get(i).getProfesion().equals(bean_pers.getProfesion())) {
                    datos += "Profesion : " + " " + bean_pers.getProfesion() + "\n";
                    sw = true;
            }
            }


             }

           if (personaList.get(i).getTipo().equals("E")) {
                datos = this.ValidarCambiosEstudiantes(nuevo_estudiante,sw,datos);
            }

            if (sw) {
                String tipo=(personaList.get(i).getTipo().equals("S") ? "Solicitante" : (personaList.get(i).getTipo().equals("C") ? "Codeudor" : "Estudiante"));
                datos = datos.replaceAll("#titulo", "***Informacion Basica"+  tipo +" ***\n");
            } else {
               datos = datos.replaceAll("#titulo","");
            }




            /************************************Informacion Conyuge*******************************************/
            if (personaList.get(i).getTipo().equals("S")&& !personaList.get(i).getTipoPersona().equals("J")) {
                datos += this.ValidarCambiosConyuge(personaList.get(i), bean_pers);
            }
            if (personaList.get(i).getTipo().equals("S")) {
                datos += this.ValidarCambiosCuentas(cuentaList, bean_pers);
            }

            if(personaList.get(i).getTipoPersona().equals("J")){
                 datos += this.ValidarCambiosPersonaJuridica(personaList.get(i), bean_pers);
            }

            datos += this.ValidarCambiosLaboral(laboralList, bean_pers);
            datos += this.ValidarCambiosBienes(nuevos_bienes, bean_pers);
            datos += this.ValidarCambiosVehiculos(nuevos_vehiculos, bean_pers);
            datos += this.ValidarCambiosReferencias(nuevos_referencias, bean_pers, "P");
            datos += this.ValidarReferenciasNuevas(nuevos_referencias, bean_pers, "P");
            datos += this.ValidarCambiosReferencias(nuevos_referencias, bean_pers, "F");
            datos += this.ValidarReferenciasNuevas(nuevos_referencias, bean_pers, "F");

            if (!personaList.get(i).getTipo().equals("E")) {
            datos += this.ValidarCambiosReferencias(nuevos_referencias, bean_pers, "C");
            }

            datos +=this.ValidarCambiosNegocios(datos_negocio);
            
            datos += this.ValidarCambiosObligaciones(ocompraList, bean_pers);

        }
         datos = datos.replaceAll("#titulo","");
        return datos;
    }

   
   
public String ValidarCambiosLaboral(ArrayList<SolicitudLaboral> laboralList, SolicitudPersona persona) throws Exception {
        String datos = "#titulo";
          boolean sw=false;
        SolicitudLaboral bean_laboral = this.datosLaboral(Integer.parseInt(persona.getNumeroSolicitud()), persona.getTipo());
        for (int i = 0; i < laboralList.size(); i++) {

            if (laboralList.get(i).getTipo().equals(persona.getTipo())) {
                sw=false;
                if (!laboralList.get(i).getActividadEconomica().equals(bean_laboral.getActividadEconomica())) {
                    datos += "Actividad Economica : " + " " + bean_laboral.getActividadEconomica() + "\n";
                    sw=true;
                }

                if (!laboralList.get(i).getOcupacion().equals(bean_laboral.getOcupacion())) {
                    datos += "Ocupacion : " + " " + bean_laboral.getOcupacion() + "\n";
                    sw=true;
                }

                if (!laboralList.get(i).getNit().equals(bean_laboral.getNit())) {
                    datos += "Nit : " + " " + bean_laboral.getNit() + "\n";
                    sw=true;
                }

                if (!laboralList.get(i).getNombreEmpresa().equals(bean_laboral.getNombreEmpresa())) {
                    datos += "Empresa : " + " " + bean_laboral.getNombreEmpresa() + "\n";
                    sw=true;
                }

                if (!laboralList.get(i).getCargo().equals(bean_laboral.getCargo())) {
                    datos += "Cargo : " + " " + bean_laboral.getCargo() + "\n";
                    sw=true;
                }

                if (!laboralList.get(i).getFechaIngreso().substring(0, 10).equals(bean_laboral.getFechaIngreso().substring(0, 10))) {
                    datos += "Fecha Ingreso : " + " " + bean_laboral.getFechaIngreso().substring(0, 10) + "\n";
                    sw=true;
                }

                if (!laboralList.get(i).getTipoContrato().equals(bean_laboral.getTipoContrato())) {
                    datos += "Tipo Contrato : " + " " + bean_laboral.getTipoContrato() + "\n";
                    sw=true;
                }

                if (!laboralList.get(i).getEps().equals(bean_laboral.getEps())) {
                    datos += "EPS : " + " " + this.nombreTablagen("EPS",bean_laboral.getEps()) + "\n";
                    sw=true;
                }

                if (!laboralList.get(i).getTipoAfiliacion().equals(bean_laboral.getTipoAfiliacion())) {
                    datos += "Afiliacion : " + " " + this.nombreTablagen("TIPAFIL",bean_laboral.getTipoAfiliacion()) + "\n";
                    sw=true;
                }
                if (!laboralList.get(i).getTelefono().equals(bean_laboral.getTelefono())) {
                    datos += "Telefono : " + " " + bean_laboral.getTelefono() + "\n";
                    sw=true;
                }
                if (!laboralList.get(i).getExtension().equals(bean_laboral.getExtension())) {
                    datos += "Extension : " + " " + bean_laboral.getExtension() + "\n";
                    sw=true;
                }

                if (!laboralList.get(i).getCelular().equals(bean_laboral.getCelular())) {
                    datos += "Celular : " + " " + bean_laboral.getCelular() + "\n";
                    sw=true;
                }

                if (!laboralList.get(i).getEmail().trim().equals(bean_laboral.getEmail().trim())) {
                    datos += "Email : " + " " + bean_laboral.getEmail() + "\n";
                    sw=true;
                }
                if (!laboralList.get(i).getDireccion().equals(bean_laboral.getDireccion())) {
                    datos += "Direccion : " + " " + bean_laboral.getDireccion() + "\n";
                    sw=true;
                }

                if (!laboralList.get(i).getDepartamento().equals(bean_laboral.getDepartamento())) {
                    datos += "Departamento : " + " " + this.nombreDept(bean_laboral.getDepartamento()) + "\n";
                    sw=true;
                }

                if (!laboralList.get(i).getCiudad().equals(bean_laboral.getCiudad())) {
                    datos += "Ciudad : " + " " + this.nombreCiudad(bean_laboral.getCiudad()) + "\n";
                    sw=true;
                }

                if (!laboralList.get(i).getDireccionCobro().equals(bean_laboral.getDireccionCobro())) {
                    datos += "Direccion Cobro : " + " " + bean_laboral.getDireccionCobro() + "\n";
                    sw=true;
                }
                if (Float.parseFloat(laboralList.get(i).getSalario())!=Float.parseFloat(bean_laboral.getSalario())) {
                    datos += "Salario : " + " " + bean_laboral.getSalario() + "\n";
                    sw=true;
                }
                if (Float.parseFloat(laboralList.get(i).getOtrosIngresos())!= Float.parseFloat(bean_laboral.getOtrosIngresos())) {
                    datos += "Otros Ingresos : " + " " + bean_laboral.getOtrosIngresos() + "\n";
                    sw=true;
                }

                if (!laboralList.get(i).getConceptoOtrosIng().equals(bean_laboral.getConceptoOtrosIng())) {
                    datos += "Concepto Otros Ingresos : " + " " + bean_laboral.getConceptoOtrosIng() + "\n";
                    sw=true;
                }

                if (Float.parseFloat(laboralList.get(i).getGastosManutencion())!=   Float.parseFloat(bean_laboral.getGastosManutencion())) {
                    datos += "Gastos Manutencion : " + " " + bean_laboral.getGastosManutencion() + "\n";
                    sw=true;
                }
                if (Float.parseFloat(laboralList.get(i).getGastosCreditos()) !=Float.parseFloat(bean_laboral.getGastosCreditos())) {
                    datos += "Gastos Creditos : " + " " + bean_laboral.getGastosCreditos() + "\n";
                    sw=true;
                }
                if (Float.parseFloat(laboralList.get(i).getGastosArriendo())!=(Float.parseFloat(bean_laboral.getGastosArriendo()))) {
                    datos += "Gastos Arriendo : " + " " + bean_laboral.getGastosArriendo() + "\n";
                    sw=true;
                }

               if(sw){
                String tipo_persona=(persona.getTipo().equals("S") ? "Solicitante" : (persona.getTipo().equals("C") ? "Codeudor" : "Estudiante"));
               datos=datos.replaceAll("#titulo","\n***Informacion Laboral "  +  tipo_persona +"***\n");
                 } else {
               datos = datos.replaceAll("#titulo","");
            }
            }
        }
                       if(sw){
               datos=datos.replaceAll("#titulo","\n***Informacion Laboral***\n");
                 } else {
               datos = datos.replaceAll("#titulo","");
            }

        return datos;
    }


    public String ValidarCambiosBienes(ArrayList<SolicitudBienes> nuevos_bienes, SolicitudPersona persona) throws Exception {
        String datos = "#titulo";
         ArrayList<SolicitudBienes> bienes = this.buscarBienes(Integer.parseInt(persona.getNumeroSolicitud()), persona.getTipo());
         boolean sw=false;
         for (int i = 0; i < bienes.size(); i++) {
           sw=false;
             if (bienes.get(i).getTipo().equals(persona.getTipo())) {
                 for (int j = 0; j < bienes.size(); j++) {
                     boolean sw_secuencia = nuevos_bienes.get(i).getSecuencia().equals(bienes.get(j).getSecuencia());
                     if (sw_secuencia) {

                         if (!nuevos_bienes.get(i).getTipoBien().equals(bienes.get(j).getTipoBien())) {
                             datos += "Tipo  : " + " " + bienes.get(j).getTipoBien() + "\n";
                             sw=true;
                         }

                         if (!nuevos_bienes.get(i).getHipoteca().equals(bienes.get(j).getHipoteca())) {
                             datos += "Hipoteca  : " + " " + bienes.get(j).getHipoteca() + "\n";
                             sw=true;
                         }

                         if (!nuevos_bienes.get(i).getaFavorDe().equals(bienes.get(j).getaFavorDe())) {
                             datos += "Afavor de   : " + " " + bienes.get(j).getaFavorDe() + "\n";
                             sw=true;
                         }

                         if (Float.parseFloat(nuevos_bienes.get(i).getValorComercial()) != (Float.parseFloat(bienes.get(j).getValorComercial()))) {
                             datos += "Valor Comenrcial : " + " " + bienes.get(j).getValorComercial() + "\n";
                             sw=true;
                         }

                         if (!nuevos_bienes.get(i).getDireccion().equals(bienes.get(j).getDireccion())) {
                             datos += "Direccion  : " + " " + bienes.get(j).getDireccion() + "\n";
                             sw=true;
                         }


                     }
                 }
             }

         }
         if (sw) {
             datos = datos.replaceAll("#titulo", "\nInformacion Bienes\n");
         } else {
             datos = datos.replaceAll("#titulo", "");
         }

        return datos;
    }

  public String ValidarCambiosVehiculos(ArrayList<SolicitudVehiculo> nuevos_vehiculos, SolicitudPersona persona) throws Exception {
             String datos = "#titulo";
              boolean sw = false;
             ArrayList<SolicitudVehiculo> vehiculos = this.buscarVehiculos(Integer.parseInt(persona.getNumeroSolicitud()));
             for (int i = 0; i < nuevos_vehiculos.size(); i++) {
                 sw = false;
                 if (nuevos_vehiculos.get(i).getTipo().equals(persona.getTipo())) {
                     for (int j = 0; j < vehiculos.size(); j++) {
                         boolean sw_secuencia = nuevos_vehiculos.get(i).getSecuencia().equals(vehiculos.get(j).getSecuencia());
                         if (sw_secuencia) {

                             if (!nuevos_vehiculos.get(i).getMarca().equals(vehiculos.get(j).getMarca())) {
                                 datos += "Marca  : " + " " + vehiculos.get(j).getMarca() + "\n";
                                 sw = true;
                             }
                             if (!nuevos_vehiculos.get(i).getTipoVehiculo().equals(vehiculos.get(j).getTipoVehiculo())) {
                                 datos += "Tipo Vehiculo  : " + " " + vehiculos.get(j).getTipoVehiculo() + "\n";
                                 sw = true;
                             }

                             if (!nuevos_vehiculos.get(i).getPlaca().equals(vehiculos.get(j).getPlaca())) {
                                 datos += "Placa  : " + " " + vehiculos.get(j).getPlaca() + "\n";
                                 sw = true;
                             }
                             if (!nuevos_vehiculos.get(i).getModelo().equals(vehiculos.get(j).getModelo())) {
                                 datos += "Modelo  : " + " " + vehiculos.get(j).getModelo() + "\n";
                                 sw = true;
                             }

                             if (Float.parseFloat(nuevos_vehiculos.get(i).getValorComercial()) != (Float.parseFloat(vehiculos.get(j).getValorComercial()))) {
                                 datos += "Valor Comenrcial : " + " " + vehiculos.get(j).getValorComercial() + "\n";
                                 sw = true;
                             }

                             if (Float.parseFloat(nuevos_vehiculos.get(i).getCuotaMensual()) != (Float.parseFloat(vehiculos.get(j).getCuotaMensual()))) {
                                 datos += "Cuota Mensual : " + " " + vehiculos.get(j).getCuotaMensual() + "\n";
                                 sw = true;
                             }

                             if (!nuevos_vehiculos.get(i).getPignoradoAFavorDe().equals(vehiculos.get(j).getPignoradoAFavorDe())) {
                                 datos += "Pignoracion a favor:  : " + " " + vehiculos.get(j).getPignoradoAFavorDe() + "\n";
                                 sw = true;
                             }


                         }
                     }
                 }
                 if (sw) {
                     datos = datos.replaceAll("#titulo", "\n***Informacion Vehiculos***\n");
                 } else {
                     datos = datos.replaceAll("#titulo", "");
                 }
             }

              if (sw) {
                     datos = datos.replaceAll("#titulo", "\n***Informacion Vehiculos***\n");
                 } else {
                     datos = datos.replaceAll("#titulo", "");
                 }


        return datos;
    }



      public String ValidarCambiosReferencias(ArrayList<SolicitudReferencias> nuevos_referencias, SolicitudPersona persona,String tipo) throws Exception {
             String datos = "#titulo";
              String tipo_persona=(persona.getTipo().equals("S") ? "Solicitante" : (persona.getTipo().equals("C") ? "Codeudor" : "Estudiante"));

             boolean sw=false;
             for (int i = 0; i < nuevos_referencias.size(); i++) {
                 if ((nuevos_referencias.get(i).getTipo().equals(persona.getTipo())) &&(nuevos_referencias.get(i).getTipoReferencia().equals(tipo))) {
                        ArrayList<SolicitudReferencias> referencias = this.buscarReferencias(Integer.parseInt(persona.getNumeroSolicitud()),nuevos_referencias.get(i).getTipo(),tipo);
                     for (int j = 0; j < referencias.size(); j++) {
                         boolean sw_secuencia = nuevos_referencias.get(i).getSecuencia().equals(referencias.get(j).getSecuencia());
                         if (sw_secuencia) {
                            // sw=false;
                             if (!nuevos_referencias.get(i).getPrimerApellido().equals(referencias.get(j).getPrimerApellido())) {
                                 datos += "Primer Apellido  : " + " " + referencias.get(j).getPrimerApellido() + "\n";
                                  sw=true;
                             }
                             if (!nuevos_referencias.get(i).getSegundoApellido().equals(referencias.get(j).getSegundoApellido())) {
                                 datos += "Segundo Apellido  : " + " " + referencias.get(j).getSegundoApellido() + "\n";
                                  sw=true;
                             }
                             if (!nuevos_referencias.get(i).getPrimerNombre().equals(referencias.get(j).getPrimerNombre())) {
                                 datos += "Primer Nombre  : " + " " + referencias.get(j).getPrimerNombre() + "\n";
                                  sw=true;
                             }
                             if (!nuevos_referencias.get(i).getSegundoNombre().equals(referencias.get(j).getSegundoNombre())) {
                                 datos += "Segundo Nombre  : " + " " + referencias.get(j).getSegundoNombre() + "\n";
                                  sw=true;
                             }
                             if (!nuevos_referencias.get(i).getTiempoConocido().equals(referencias.get(j).getTiempoConocido())) {
                                 datos += "Tiempos De Conocidos  : " + " " + referencias.get(j).getTiempoConocido() + "\n";
                                  sw=true;
                             }
                             if (!nuevos_referencias.get(i).getCelular().equals(referencias.get(j).getCelular())) {
                                 datos += "Celular  : " + " " + referencias.get(j).getCelular() + "\n";
                                  sw=true;
                             }
                             if (!nuevos_referencias.get(i).getTelefono().equals(referencias.get(j).getTelefono())) {
                                 datos += "Telefono 1  : " + " " + referencias.get(j).getTelefono() + "\n";
                                  sw=true;
                             }
                             if (!nuevos_referencias.get(i).getTelefono2().equals(referencias.get(j).getTelefono2())) {
                                 datos += "Telefono 2 : " + " " + referencias.get(j).getTelefono2() + "\n";
                                  sw=true;
                             }
                             if (!nuevos_referencias.get(i).getExtension().equals(referencias.get(j).getExtension())) {
                                 datos += "Extension  : " + " " + referencias.get(j).getExtension() + "\n";
                                  sw=true;
                             }
                             if (!nuevos_referencias.get(i).getDepartamento().equals(referencias.get(j).getDepartamento())) {
                                 datos += "Departamento  : " + " " + this.nombreDept(referencias.get(j).getDepartamento()) + "\n";
                                  sw=true;
                             }
                             if (!nuevos_referencias.get(i).getCiudad().equals(referencias.get(j).getCiudad())) {
                                 datos += "Ciudad  : " + " " + this.nombreCiudad(referencias.get(j).getCiudad()) + "\n";
                                  sw=true;
                             }
                               if (!tipo.equals("C")) {
                                 if (!nuevos_referencias.get(i).getEmail().trim().equals(referencias.get(j).getEmail().trim())) {
                                 datos += "Email  : " + " " + referencias.get(j).getEmail() + "\n";
                                  sw=true;
                                   }
                                  
                              
                             }
                            
                             if (tipo.equals("F")) {
                                 if (!nuevos_referencias.get(i).getParentesco().equals(referencias.get(j).getParentesco())) {
                                     datos += "Parentesco  : " + " " + referencias.get(j).getParentesco() + "\n";
                                     sw = true;
                                 }
                             }
                              if (tipo.equals("C")) {
                                 if (!nuevos_referencias.get(i).getDireccion().equals(referencias.get(j).getDireccion())) {
                                     datos += "Direccion  : " + " " + referencias.get(j).getDireccion() + "\n";
                                     sw = true;
                                 }
                                
                              if (!nuevos_referencias.get(i).getNombre().trim().equals(referencias.get(j).getNombre().trim())) {
                                 datos += "Razon Social  : " + " " + referencias.get(j).getNombre() + "\n";
                                 sw=true;

                             }
                             }

                         }
                     }
                 }
             }
               if (sw) {
                                  datos = datos.replaceAll("#titulo","\n***Referencias " +(  tipo.equals("F")?"Familiares":tipo.equals("C")?"Comerciales":"Personales"  )+" "+  tipo_persona +"***\n");
                             } else {
                                 datos = datos.replaceAll("#titulo", "");
                             }

        return datos;
    }

 public String ValidarReferenciasNuevas(ArrayList<SolicitudReferencias> nuevas_referencias, SolicitudPersona persona, String tipo) throws Exception {
        String datos = "#titulo";
        String tipo_persona = (persona.getTipo().equals("S") ? "Solicitante" : (persona.getTipo().equals("C") ? "Codeudor" : "Estudiante"));

        boolean sw = false;
        for (int i = 0; i < nuevas_referencias.size(); i++) {
            if ((nuevas_referencias.get(i).getTipo().equals(persona.getTipo())) && (nuevas_referencias.get(i).getTipoReferencia().equals(tipo))) {
                if (!this.buscarReferencias(Integer.parseInt(persona.getNumeroSolicitud()), nuevas_referencias.get(i).getTipo(), tipo, nuevas_referencias.get(i).getSecuencia())) {
                    sw = true;
                    datos += "Primer Apellido  : " + " " + nuevas_referencias.get(i).getPrimerApellido() + "\n";
                    datos += "Segundo Apellido  : " + " " + nuevas_referencias.get(i).getSegundoApellido() + "\n";
                    datos += "Primer Nombre  : " + " " + nuevas_referencias.get(i).getPrimerNombre() + "\n";
                    datos += "Segundo Nombre  : " + " " + nuevas_referencias.get(i).getSegundoNombre() + "\n";
                    datos += "Tiempos De Conocidos  : " + " " + nuevas_referencias.get(i).getTiempoConocido() + "\n";
                    datos += "Celular  : " + " " + nuevas_referencias.get(i).getCelular() + "\n";
                    datos += "Telefono 1  : " + " " + nuevas_referencias.get(i).getTelefono() + "\n";
                    datos += "Telefono 2 : " + " " + nuevas_referencias.get(i).getTelefono2() + "\n";
                    datos += "Extension  : " + " " + nuevas_referencias.get(i).getExtension() + "\n";
                    datos += "Departamento  : " + " " + this.nombreDept(nuevas_referencias.get(i).getDepartamento()) + "\n";
                    datos += "Ciudad  : " + " " + this.nombreCiudad(nuevas_referencias.get(i).getCiudad()) + "\n";
                    if (!tipo.equals("C")) {
                        datos += "Email  : " + " " + nuevas_referencias.get(i).getEmail() + "\n";
                    }
                    if (tipo.equals("F")) {
                        datos += "Parentesco  : " + " " + nuevas_referencias.get(i).getParentesco() + "\n";
                    }
                }
                if (tipo.equals("C")) {
                    datos += "Direccion  : " + " " + nuevas_referencias.get(i).getDireccion() + "\n";
                }
            }

        }
             
               if (sw) {
                                  datos = datos.replaceAll("#titulo","\n***Nuevas Referencias " +(  tipo.equals("F")?"Familiares":tipo.equals("C")?"Comerciales":"Personales"  )+" "+  tipo_persona +"***\n");
                             } else {
                                 datos = datos.replaceAll("#titulo", "");
                             }

        return datos;
    }


   public String ValidarCambiosConyuge(SolicitudPersona personaList, SolicitudPersona bean_pers) {
        String datos = "#titulo";

            boolean sw = false;

            if (!personaList.getPrimerApellidoCony().equals(bean_pers.getPrimerApellidoCony())) {
                datos += "Primer Apellido  : " + " " + bean_pers.getPrimerApellidoCony() + "\n";
                sw = true;
            }

            if (!personaList.getSegundoApellidoCony().equals(bean_pers.getSegundoApellidoCony())) {
                datos += "Segundo Apellido : " + " " + bean_pers.getSegundoApellidoCony() + "\n";
                sw = true;
            }

            if (!personaList.getPrimerNombreCony().equals( bean_pers.getPrimerNombreCony())) {
                datos += "Primer Nombre : " + " " + bean_pers.getPrimerNombreCony() + "\n";
                sw = true;
            }
            if (!personaList.getSegundoNombreCony().equals(bean_pers.getSegundoNombreCony())) {
                datos += "Segundo Nombre : " + " " + bean_pers.getSegundoNombreCony() + "\n";
                sw = true;
            }

            if (!personaList.getTipoIdentificacionCony().equals(bean_pers.getTipoIdentificacionCony())) {
                datos += "Tipo Identificacion : " + " " + bean_pers.getTipoIdentificacionCony() + "\n";
                sw = true;
            }

            if (!personaList.getIdentificacionCony().equals(bean_pers.getIdentificacionCony())) {
                datos += "Identificacion : " + " " + bean_pers.getIdentificacionCony() + "\n";
                sw = true;
            }
            if (!personaList.getCelularCony().equals(bean_pers.getCelularCony())) {
                datos += "Celular : " + " " + bean_pers.getCelularCony() + "\n";
                sw = true;
            }
            if (!personaList.getEmpresaCony().equals(bean_pers.getEmpresaCony())) {
                datos += "Empresa : " + " " + bean_pers.getEmpresaCony() + "\n";
                sw = true;
            }

            if (!personaList.getDireccionEmpresaCony().equals(bean_pers.getDireccionEmpresaCony())) {
                datos += "Direccion : " + " " + bean_pers.getDireccionEmpresaCony() + "\n";
                sw = true;
            }

            if (!personaList.getTelefonoCony().equals(bean_pers.getTelefonoCony())) {
                datos += "Telefono : " + " " + bean_pers.getTelefonoCony() + "\n";
                sw = true;
            }
            if (!personaList.getCargoCony().equals(bean_pers.getCargoCony())) {
                datos += "Cargo : " + " " + bean_pers.getCargoCony() + "\n";
                sw = true;
            }
            if (!("" + Float.parseFloat(personaList.getSalarioCony())).equals("" + Float.parseFloat(bean_pers.getSalarioCony()))) {
                datos += "Salario : " + " " + bean_pers.getSalarioCony() + "\n";
                sw = true;
            }
            if (!personaList.getEmailCony().equals(bean_pers.getEmailCony())) {
                datos += "Email : " + " " + bean_pers.getEmailCony() + "\n";
                sw = true;
            }

        if (sw) {
            datos = datos.replaceAll("#titulo", "\n***Informacion Conyuge***\n");
        } else {
                datos = datos.replaceAll("#titulo", "");
        }

        return datos;

    }

  
 public String ValidarCambiosEstudiantes(SolicitudEstudiante nuevo_estudiante, boolean sw_persona, String datos) throws Exception {
        SolicitudEstudiante estudiante = this.datosEstudiante(Integer.parseInt(nuevo_estudiante.getNumeroSolicitud()));

        boolean sw = false;

        if (!nuevo_estudiante.getPrograma().equals(estudiante.getPrograma())) {
            datos += "Programa  : " + " " + estudiante.getPrograma() + "\n";
            sw = true;
        }
        if (!nuevo_estudiante.getFechaIngresoPrograma().substring(0, 10).equals(estudiante.getFechaIngresoPrograma().substring(0, 10))) {
            datos += "Fecha Ingreso Programa  : " + " " + estudiante.getFechaIngresoPrograma().substring(0, 10) + "\n";
            sw = true;
        }

        if (!nuevo_estudiante.getCodigo().equals(estudiante.getCodigo())) {
            datos += "Codigo Alumno  : " + " " + estudiante.getCodigo() + "\n";
            sw = true;
        }

        if (!nuevo_estudiante.getSemestre().equals(estudiante.getSemestre())) {
            datos += "Semestre a Cursar  : " + " " + estudiante.getSemestre() + "\n";
            sw = true;
        }

        if (!("" + Float.parseFloat(nuevo_estudiante.getValorSemestre())).equals("" + Float.parseFloat(estudiante.getValorSemestre()))) {
            datos += "Valor Semestre : " + " " + estudiante.getValorSemestre() + "\n";
            sw = true;
        }

        if (!nuevo_estudiante.getTipoCarrera().equals(estudiante.getTipoCarrera())) {
            datos += "Tipo Carrera  : " + " " + estudiante.getTipoCarrera() + "\n";
            sw = true;
        }


        if (!nuevo_estudiante.getTrabaja().equals(estudiante.getTrabaja())) {
            datos += "Trabaja : " + " " + estudiante.getTrabaja() + "\n";
            sw = true;
        }


        if (!nuevo_estudiante.getNombreEmpresa().equals(estudiante.getNombreEmpresa())) {
            datos += "Nombre Empresa  : " + " " + estudiante.getNombreEmpresa() + "\n";
            sw = true;
        }

        if (!nuevo_estudiante.getDireccionEmpresa().equals(estudiante.getDireccionEmpresa())) {
            datos += "Direccion Empresa : " + " " + estudiante.getDireccionEmpresa() + "\n";
            sw = true;
        }


        if (!nuevo_estudiante.getTelefonoEmpresa().equals(estudiante.getTelefonoEmpresa())) {
            datos += "Telefono Empresa : " + " " + estudiante.getTelefonoEmpresa() + "\n";
            sw = true;
        }


        if (!("" + Float.parseFloat(nuevo_estudiante.getSalario())).equals("" + Float.parseFloat(estudiante.getSalario()))) {
            datos += "Salario : " + " " + estudiante.getSalario() + "\n";
            sw = true;
        }

        if (!nuevo_estudiante.getParentescoGirador().equals(estudiante.getParentescoGirador())) {
            datos += "Parentesco Girador  : " + " " + estudiante.getParentescoGirador() + "\n";
            sw = true;
        }

        if (sw && !sw_persona) {
            datos = datos.replaceAll("#titulo", "\n***Informacion Basica  Estudiante***\n");
        }

        return datos;
    }
   
   
public String ValidarCambiosPersonaJuridica(SolicitudPersona persona_j,SolicitudPersona bean_pers) throws Exception {
           String datos = "#titulo";


             boolean sw=false;

            if (!persona_j.getNombre().equals(bean_pers.getNombre())) {
                datos += "Nombre : " + " " + bean_pers.getNombre() + "\n";
                sw=true;
            }


           if (!persona_j.getCiudad().equals(bean_pers.getCiudad())) {
                datos += "Ciudad : " + " " + this.nombreCiudad(bean_pers.getCiudad()) + "\n";
                sw=true;
            }
            if (!persona_j.getTipoEmpresa().equals(bean_pers.getTipoEmpresa())) {
                datos += "Caracter : " + " " + bean_pers.getTipoEmpresa() + "\n";
                sw=true;
            }

            if (!persona_j.getFechaConstitucion().substring(0, 10).equals(bean_pers.getFechaConstitucion().substring(0, 10))) {
                datos += "Fecha Constitucion : " + " " + bean_pers.getFechaConstitucion() + "\n";
                sw=true;
            }


            if (!persona_j.getEmail().trim().equals(bean_pers.getEmail().trim())) {
                datos += "E-mail : " + " " + bean_pers.getEmail() + "\n";
                sw=true;
            }

             if (!persona_j.getTelefono().trim().equals(bean_pers.getTelefono().trim())) {
                datos += "Telefono : " + " " + bean_pers.getTelefono() + "\n";
                sw=true;
            }

            if (!persona_j.getTelefono2().trim().equals(bean_pers.getTelefono2().trim())) {
                datos += "Telefono 2 : " + " " + bean_pers.getTelefono2() + "\n";
                sw=true;
            }


            if (!persona_j.getFax().trim().equals(bean_pers.getFax().trim())) {
                datos += "Fax : " + " " + bean_pers.getFax() + "\n";
                sw=true;
            }

            if (!persona_j.getCiudad().equals(bean_pers.getCiudad())) {
                datos += "Ciudad : " + " " + bean_pers.getCiudad() + "\n";
                sw=true;
            }
           if (!persona_j.getDepartamento().equals(bean_pers.getDepartamento())) {
                datos += "Departamento : " + " " + bean_pers.getDepartamento()+ "\n";
                sw=true;
            }

            if (!persona_j.getCelular().equals(bean_pers.getCelular())) {
                datos += "Celular : " + " " + bean_pers.getCelular()+ "\n";
                sw=true;
            }

            if (!persona_j.getRepresentanteLegal().equals(bean_pers.getRepresentanteLegal())) {
                datos += "Representante legal : " + " " + bean_pers.getRepresentanteLegal()+ "\n";
                sw=true;
            }

             if (!persona_j.getGeneroRepresentante().equals(bean_pers.getGeneroRepresentante())) {
                datos += "Genero Rep. legal : " + " " + bean_pers.getGeneroRepresentante()+ "\n";
                sw=true;
            }


            if (!persona_j.getFirmadorCheques().equals(bean_pers.getFirmadorCheques())) {
                datos += "Firmador Cheques : " + " " + bean_pers.getFirmadorCheques()+ "\n";
                sw=true;
            }

             if (!persona_j.getGeneroFirmador().equals(bean_pers.getGeneroFirmador())) {
                datos += "Genero Firmador : " + " " + bean_pers.getGeneroFirmador()+ "\n";
                sw=true;
            }
             
           if (!persona_j.getTipoIdFirmador().equals(bean_pers.getTipoIdFirmador())) {
                datos += "Tipo Id. Firmador : " + " " + bean_pers.getTipoIdFirmador()+ "\n";
                sw=true;
            }

            if (!persona_j.getIdFirmador().equals(bean_pers.getIdFirmador())) {
                datos += "Id. Firmador : " + " " + bean_pers.getIdFirmador()+ "\n";
                sw=true;
            }

            if (!persona_j.getTipoIdRepresentante().equals(bean_pers.getTipoIdRepresentante())) {
                datos += "Tipo Id. Rep: " + " " + bean_pers.getTipoIdRepresentante()+ "\n";
                sw=true;
            }
            if (!persona_j.getIdRepresentante().equals(bean_pers.getIdRepresentante())) {
                datos += "Id. Rep : " + " " + bean_pers.getIdRepresentante()+ "\n";
                sw=true;
            }

           if (!persona_j.getDireccion().trim().equals(bean_pers.getDireccion().trim())) {
                datos += "Direccion : " + " " + bean_pers.getDireccion() + "\n";
                sw=true;
            }

            if (sw) {               
                datos = datos.replaceAll("#titulo", "***Informacion Basica Persona Juridica ***\n");
            } else {
               datos = datos.replaceAll("#titulo","");
            }


        
        return datos;
    }


  public boolean  buscarReferencias(int num_solicitud, String tipo_per, String tipo_ref,String secuencia) throws Exception {
        return gsadao.buscarReferencias(num_solicitud, tipo_per, tipo_ref,secuencia);
    }

             public String ValidarCambiosCuentas(ArrayList<SolicitudCuentas> nuevas_cuentas,SolicitudPersona persona) throws Exception {
             String datos = "#titulo";
             ArrayList<SolicitudCuentas> cuentas = this.buscarCuentas(Integer.parseInt(persona.getNumeroSolicitud()));
             for (int i = 0; i < nuevas_cuentas.size(); i++) {
                 boolean sw = false;

                     for (int j = 0; j < cuentas.size(); j++) {
                         boolean sw_secuencia = nuevas_cuentas.get(i).getConsecutivo().equals(cuentas.get(j).getConsecutivo());
                         if (sw_secuencia) {
                             if (!nuevas_cuentas.get(i).getTipo().equals(cuentas.get(j).getTipo())) {
                                 datos += "Tipo  : " + " " + cuentas.get(j).getTipo() + "\n";
                                 sw = true;
                             }
                             if (!nuevas_cuentas.get(i).getBanco().equals(cuentas.get(j).getBanco())) {
                                 datos += "Banco  : " + " " + cuentas.get(j).getBanco() + "\n";
                                 sw = true;
                             }
                             if (!nuevas_cuentas.get(i).getCuenta().equals(cuentas.get(j).getCuenta())) {
                                 datos += "Numero Cuenta  : " + " " + cuentas.get(j).getCuenta() + "\n";
                                 sw = true;
                             }
                             if (!nuevas_cuentas.get(i).getFechaApertura().equals(cuentas.get(j).getFechaApertura().substring(0,10))) {
                                 datos += "Fecha Apertura  : " + " " + cuentas.get(j).getFechaApertura() + "\n";
                                 sw = true;
                             }
                             if (!nuevas_cuentas.get(i).getNumeroTarjeta().equals(cuentas.get(j).getNumeroTarjeta())) {
                                 datos += "Numero Tarjeta  : " + " " + cuentas.get(j).getNumeroTarjeta() + "\n";
                                 sw = true;
                             }
                         }
                     }

                 if (sw) {
                     datos = datos.replaceAll("#titulo", "\n***Informacion Cuentas***\n");
                 } else {
                     datos = datos.replaceAll("#titulo", "");
                 }
             }


        return datos;
    }

           public String ValidarCambiosNegocios(SolicitudNegocio nuevo_negocio) throws Exception {
        String datos = "#titulo";


        boolean sw = false;
        if (nuevo_negocio != null) {
            SolicitudNegocio negcio = this.buscarNegocio(Integer.parseInt(nuevo_negocio.getNumeroSolicitud()));
            if (!nuevo_negocio.getNombre().equals(negcio.getNombre())) {
                datos += "Nombre  : " + " " + negcio.getNombre() + "\n";
                sw = true;
            }
            if (!nuevo_negocio.getDireccion().equals(negcio.getDireccion())) {
                datos += "Direccion  : " + " " + negcio.getDireccion() + "\n";
                sw = true;
            }
            if (!nuevo_negocio.getDepartamento().equals(negcio.getDepartamento())) {
                datos += "Departamento  : " + " " + negcio.getDepartamento() + "\n";
                sw = true;
            }
            if (!nuevo_negocio.getCiudad().equals(negcio.getCiudad())) {
                datos += "Ciudad  : " + " " + negcio.getCiudad() + "\n";
                sw = true;
            }
            if (!nuevo_negocio.getBarrio().equals(negcio.getBarrio())) {
                datos += "Barrio  : " + " " + negcio.getBarrio() + "\n";
                sw = true;
            }
            if (!nuevo_negocio.getTelefono().equals(negcio.getTelefono())) {
                datos += "Telefono  : " + " " + negcio.getTelefono() + "\n";
                sw = true;
            }

            if (!nuevo_negocio.getSector().equals(negcio.getSector())) {
                datos += "Sector  : " + " " + negcio.getSector() + "\n";
                sw = true;
            }
            if (!nuevo_negocio.getSubsector().equals(negcio.getSubsector())) {
                datos += "Subsector  : " + " " + negcio.getSubsector() + "\n";
                sw = true;
            }
            if (!nuevo_negocio.getTiempoLocal().equals(negcio.getTiempoLocal())) {
                datos += "A�os local actual  : " + " " + negcio.getTiempoLocal() + "\n";
                sw = true;
            }
            if (!nuevo_negocio.getNumExpNeg().equals(negcio.getNumExpNeg())) {
                datos += "A�os exp. negocio actual  : " + " " + negcio.getNumExpNeg() + "\n";
                sw = true;
            }

            if (!nuevo_negocio.getTiempoMicroempresario().equals(negcio.getTiempoMicroempresario())) {
                datos += "A�os microempresario  : " + " " + negcio.getTiempoMicroempresario() + "\n";
                sw = true;
            }
            if (!nuevo_negocio.getNumTrabajadores().equals(negcio.getNumTrabajadores())) {
                datos += "N� trabajadores  : " + " " + negcio.getNumTrabajadores() + "\n";
                sw = true;
            }

        }

        if (sw) {
            datos = datos.replaceAll("#titulo", "\n***Informacion Negocio***\n");
        } else {
            datos = datos.replaceAll("#titulo", "");
        }

        return datos;

    }


public double  getMontoSugerido(SolicitudAval solicitud_aval,String cod_neg) throws Exception
     {

         SolicitudPersona persona =this.buscarPersona(Integer.parseInt(solicitud_aval.getNumeroSolicitud()),"S");
         SolicitudLaboral persona_laboral=this.buscarLaboralId(persona.getIdentificacion());
           GestionConveniosService convserv = new GestionConveniosService(this.gsadao.getDatabaseName());


          WSHistCreditoService wsdatacred= new WSHistCreditoService(this.gsadao.getDatabaseName());
          NegociosGenService negocios_service = new NegociosGenService(this.gsadao.getDatabaseName());
          Negocios negocio = negocios_service.buscarNegocio(cod_neg);
         negocio.getTasa();
          Convenio convenio = convserv.buscar_convenio(negocio.getCod_negocio());
         double monto=0;
         double interes=(Double.parseDouble(negocio.getTasa())*12)/100;
        double totalIngreso = (persona_laboral != null ? (Double.parseDouble(persona_laboral.getSalario()) + Double.parseDouble(persona_laboral.getOtrosIngresos())) : 0);
        double cuota = persona != null ? wsdatacred.buscarSumCuotas(persona.getIdentificacion(),convenio.getNit_anombre()) : 0;
        double porcgfam = (persona != null ? (Double.parseDouble(persona.getSalarioCony()) > 0 && (persona.getEstadoCivil().equals("C") || persona.getEstadoCivil().equals("U")) ? 0.3 : 0.4) : 0);
        double gastosfam = totalIngreso * porcgfam;
        double gastosarr = (persona_laboral != null ? (Double.parseDouble(persona_laboral.getGastosArriendo())) : 0);
        double totalegreso = cuota + gastosfam + gastosarr;
        double a=0;
        double b=0;
        double cuota_nueva= 0;
        cuota_nueva=(totalIngreso*0.90)-totalegreso;
        if(negocio.getNodocs()<=6)
        {
            cuota_nueva=(totalIngreso)-totalegreso;
        }

        a= (Math.pow( (1+(interes/12)),negocio.getNodocs())-1);
        b= (Math.pow( (1+(interes/12)),negocio.getNodocs()))*(interes/12);
        monto = (a/b)*cuota_nueva;

        return monto;
      }



    public boolean validarCausalMLE(String identificacion, String nitEmpresa) throws Exception {
        WSHistCreditoService wsservice = new WSHistCreditoService(this.gsadao.getDatabaseName());
        return wsservice.validarCausalMLE(identificacion, "1", nitEmpresa);
    }



    /**
     * trae sql que actualiza una solicitud.
     * @param objeto de Solicitud
     * @return Sql
     * @throws Exception Cuando hay error
     */
    public String updateSolicitud(SolicitudAval solicitud){
         return gsadao.updateSolicitud(solicitud);
    }

    public String getNitEmpresa() {
        return nitEmpresa;
    }

    public void setNitEmpresa(String nitEmpresa) {
        this.nitEmpresa = nitEmpresa;
    }

    //PRUEBAS 
    /**
     * Busca las solicitudes de multiservicios que fueron devueltas
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<BeanGeneral> solicitudesDevueltasMultiservicio() throws Exception {
        return gsadao.solicitudesDevueltasMultiservicio();
    }
    
   /**
     * Valida si una solicitud fue  devuelta
     * @param numero_solicitud numero de solicitud
     * @param act_actual actividad actual del negocio
     * @param act_anterior actividad anterior del negocio
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public boolean ValidarSolicitudDevuelta(String numero_solicitud,String act_actual,String act_anterior) throws Exception {
       return gsadao.ValidarSolicitudDevuelta(numero_solicitud, act_actual, act_anterior);
    }

    


   /**
     * lista las solicitudes devueltas
     * @param act_actual actividad actual del negocio
     * @param act_anterior actividad anterior del negocio
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<BeanGeneral> SolicitudesDevueltas(String act_actual,String act_anterior) throws Exception {
      return gsadao.SolicitudesDevueltas(act_actual, act_anterior);
    }
    /**
     * lista las solicitudes realizadas por los estudiantes pre-aprobados
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
     public ArrayList<BeanGeneral> getListaPreaprobados() throws Exception {
      return gsadao.getListarPreaprobados();
    }
    
    public String getUltimaSolicitud(String cedula) throws Exception {
        return gsadao.getUltimaSolicitud(cedula);
    }
      public boolean ValidarClientePreaprobado(String id) throws Exception {
           return gsadao.ValidarClientePreaprobado(id);
      }
      
//Este metodo salta el concepto de data credito cuando el departamento de riesgo lo pida.
//Autor : egonzalez   
   public boolean ValidarClienteSaltaData(String id) throws Exception {
        return gsadao.ValidarClienteSaltaData(id);
    }


    /**
     * metodo que por medio de diferentes politicas informa si una persona es
     * apta para credito fenalco_bolivar
     *
     * @return boolean
     * @throws Exception
     */
    public boolean aptoCreditoFen_bol(Convenio convenio, String numero_form, Usuario usuario, int num_doc, double tot_pag, String tipo_proceso) throws Exception {
        boolean sw = true;
        if (!convenio.isAval_tercero()) {
            SolicitudAval bean_sol = this.buscarSolicitud(Integer.parseInt(numero_form));
            SolicitudPersona bean_pers = this.buscarPersona(Integer.parseInt(numero_form), "S");
            SolicitudLaboral bean_lab = this.datosLaboral(Integer.parseInt(numero_form), "S");
            if (!ValidarClientePreaprobado(bean_pers.getIdentificacion())) {
                sw = this.aptoCreditoFen_bol(convenio, bean_pers, bean_lab, bean_sol, usuario, num_doc, tot_pag, tipo_proceso);
            }
        }
        return sw;
    }

    
     /**
     * metodo que por medio de diferentes politicas informa si una persona es apta para credito en fenalco bolivar
     * @return boolean
     * @throws Exception
     */
    public boolean aptoCreditoFen_bol(Convenio convenio, SolicitudPersona bean_pers,SolicitudLaboral bean_lab,  SolicitudAval bean_sol, Usuario usuario, int num_doc, double tot_pag, String tipo_proceso) throws Exception {
        NegociosDAO negdao = new NegociosDAO(usuario.getBd());
        boolean sw = true;
        String estado = "";
        String rescodeudor = "";

        if (!convenio.isAval_tercero()) {
            WSHistCreditoService wsdc = new WSHistCreditoService(usuario.getBd());
            RespuestaDecisor respuesta = new RespuestaDecisor();

            if (bean_pers != null && bean_sol != null) {
                boolean siniestros = false;
                String reporte = "";
                if (convenio.isAval_anombre()) {
                    nitEmpresa=convenio.getNit_anombre();
                    SiniestrosService ss = new SiniestrosService(usuario.getBd());

                    reporte = ss.cedulaReportada(bean_pers.getIdentificacion());
                    if (!reporte.equals("")) {
                        estado_neg = "R";
                        comentario = reporte;
                        causal = "17";
                        resdeudor = "RECHAZAD";

                    } else {

                        if (bean_sol.getTipoNegocio().equals("01")) {
                            reporte = ss.cuentaReportada(bean_sol.getNumTipoNegocio(), bean_sol.getBanco());
                            if (!reporte.equals("")) {
                                estado_neg = "R";
                                comentario = reporte;
                                causal = "18";
                                resdeudor = "RECHAZAD";
                            }
                        }
                        if (reporte.equals("")) {
                            //validara siniestros activos
                            siniestros = ss.Validar_Siniestros_Activos(bean_pers.getIdentificacion());
                            if (!siniestros) {
                                if (negdao.NumNegocioClienteHoy(bean_pers.getIdentificacion()) >= 3) {
                                    estado_neg = "R";
                                    comentario = "maxima operacion permitida en un dia";
                                    causal = "19";
                                    reporte = "error";
                                }

                            } else {
                                estado_neg = "R";
                                 causal = "20";
                                resdeudor = "RECHAZAD";
                            }
                        }

                    }
                } else {
                    CompaniaService ciaserv = new CompaniaService();
                    ciaserv.buscarCia(usuario.getDstrct());
                    Compania cia = ciaserv.getCompania();
                    nitEmpresa = cia.getnit();
                }
                if (!siniestros && reporte.equals("")) {
                    if (bean_pers.getTipoPersona().equals("N")) {
                        if (bean_lab != null) {
                            
                            //aqui setteamos los datos del formulario para la creacion del archivo xml
                            FormularioDecisor form = new FormularioDecisor();

                            form.setTipoIdentificacion("1");
                            form.setIdentificacion(bean_pers.getIdentificacion());
                            form.setPrimerApellido(bean_pers.getPrimerApellido());
                            form.setAnio_Nacimiento(bean_pers.getFechaNacimiento().substring(0, 4));
                            form.setMes_Nacimiento(bean_pers.getFechaNacimiento().substring(5, 7));
                            form.setTipo_Producto(bean_sol.getProducto());

                            if (bean_sol.getProducto().equals("02")) {
                                form.setServicio(bean_sol.getServicio());
                                form.setCiudad_Matricula(wsdc.buscarCodigo("H", "matricula", bean_sol.getCiudadMatricula()));
                                form.setValor_Vehiculo((int) Double.parseDouble(bean_sol.getValorProducto()) + "");
                            }

                            form.setTipo_Vivienda(bean_pers.getTipoVivienda());
                            form.setArriendo((int) Double.parseDouble(bean_lab.getGastosArriendo()) + "");
                            form.setEstado_Civil(wsdc.buscarCodigo("H", "estado_civil", bean_pers.getEstadoCivil()));
                            form.setConyugue(Double.parseDouble(bean_pers.getSalarioCony()) > 0 ? "01" : "02");//si el conyugue trabaja

                            //sumamos todos los ingresos del solicitante.
                            int ingresos = (int) (Double.parseDouble(bean_lab.getSalario()) + Double.parseDouble(bean_lab.getOtrosIngresos()));
                            form.setIngreso(ingresos + "");

                            form.setMonto_Solicitado((int) Double.parseDouble(bean_sol.getValorSolicitado()) + "");

                            //Valor cuota mensual del cr�dito solicitado
                            int cuota = (int) (tot_pag / num_doc);
                            form.setCuota_Credito_Solicitado(cuota + "");

                            if (bean_sol.getProducto().equals("01")) {//si es educativo.
                                form.setValor_Semestre((int) Double.parseDouble(bean_sol.getValorProducto()) + "");
                                form.setPlazo(wsdc.buscarCodigo("H", "plazo_uni", num_doc + ""));
                            } else {
                                ArrayList<Codigo> plazo = wsdc.buscarTabla("H", "plazo");
                                for (int i = 0; i < plazo.size(); i++) {
                                    if (num_doc <= Integer.parseInt(plazo.get(i).getValor())) {
                                        form.setPlazo(plazo.get(i).getCodigo());
                                        break;
                                    }
                                }
                            }

                            form.setCredito_Fenalco_Bolivar(negdao.tieneCreditoFintra(bean_pers.getIdentificacion()) ? "01" : "02");
                            form.setNitEmpresa(nitEmpresa);
                            
                          

                            respuesta = wsdc.consultarHistoriaCreditoDecisor(form, usuario);

                            if (respuesta.getScore() == -1) {
                                comentario = "Error al consultar Deudor: " + respuesta.getCodigoRespuesta() + " " + wsdc.buscarValor("H", "cod_consultas", respuesta.getCodigoRespuesta()+" Nit responsable consulta: "+nitEmpresa);
                             } else {
                                String zonagris = this.nombreTablagen("CONCEPDATA", "P");

                                resdeudor = this.refTablagen("CONCEPDATA", respuesta.getClasificacion());
                                estado = this.nombreTablagen("CONCEPDATA", respuesta.getClasificacion());

                                if (resdeudor.equals("RECHAZAD") && tipo_proceso.equals("DSR")) {
                                    resdeudor = "ZONAGRIS";
                                    estado = zonagris;
                                    comentario = "Negocio Al dia Sin redescuento: Rechazado por Data Credito";
                                }

                                mensaje = "Deudor: " + estado + "<br/>";

                                if (!bean_sol.getProducto().equals("01") && !convenio.isFactura_tercero()) {
                                    SolicitudPersona bean_perc = this.buscarPersona(Integer.parseInt(bean_sol.getNumeroSolicitud()), "C");
                                    bean_lab = this.datosLaboral(Integer.parseInt(bean_sol.getNumeroSolicitud()), "C");

                                    if (bean_perc != null && bean_sol != null) {

                                        form.setTipoIdentificacion("1");
                                        form.setIdentificacion(bean_perc.getIdentificacion());
                                        form.setPrimerApellido(bean_perc.getPrimerApellido());
                                        form.setAnio_Nacimiento(bean_pers.getFechaNacimiento().substring(0, 4));
                                        form.setMes_Nacimiento(bean_pers.getFechaNacimiento().substring(5, 7));
                                        form.setTipo_Vivienda((bean_perc.getTipoVivienda()));
                                        form.setCredito_Fenalco_Bolivar((negdao.tieneCreditoFintra(bean_perc.getIdentificacion()) ? "01" : "02"));

                                        ingresos = (int) (Double.parseDouble(bean_lab.getSalario()) + Double.parseDouble(bean_lab.getOtrosIngresos()));
                                        form.setIngreso(ingresos + "");
                                        form.setArriendo((int) Double.parseDouble(bean_lab.getGastosArriendo()) + "");
                                        form.setEstado_Civil(wsdc.buscarCodigo("H", "estado_civil", bean_perc.getEstadoCivil()));
                                        form.setConyugue(Double.parseDouble(bean_perc.getSalarioCony()) > 0 ? "01" : "02");
                                
                                     
                                        //metodo para consultar historia de credito.
                                        respuesta = wsdc.consultarHistoriaCreditoDecisor(form, usuario);

                                        if (respuesta.getScore() == -1) {
                                           mensaje += "Error al consultar Deudor Solidario: " + wsdc.buscarValor("H", "cod_consultas", respuesta.getCodigoRespuesta())+" Nit responsable consulta: "+nitEmpresa + "<br/>";
                                        } else {
                                            rescodeudor = this.refTablagen("CONCEPDATA", respuesta.getClasificacion());
                                            estado = this.nombreTablagen("CONCEPDATA", respuesta.getClasificacion());
                                            mensaje += "Codeudor: " + estado + "<br/>";
                                        }
                                    }
                                }
                                if (resdeudor.equals("RECHAZAD")) {
                                    causal = "21";
                                    estado_neg = "R";
                                }
                            }
                        } else {
                            sw = false;
                            mensaje = "No se puede consultar la informacion laboral en el formulario";

                    }
                }
              }
            } else {
                    sw = false;
                    mensaje = "no se puede consultar el formulario";
                }
        }
        return sw;
    }

    public  ArrayList<BeanGeneral> validarRenovacion(String identificacion) throws Exception{

        ArrayList<BeanGeneral> lista;
        lista = gsadao.verificarRenovacion(identificacion);
        return lista; 
}
    
    
     public boolean isRenovacion(String numero_sol) throws Exception{
    
        boolean sw;
        sw = gsadao.isRenovacion(numero_sol);
        return sw; 
    }
     
      public String ultimaFecha(String cod_neg) throws Exception{
       return  gsadao.ultimaFecha(cod_neg);
    }
      
    public String cargarVias(String codciu){
       return gsadao.cargarVias(codciu);
    }
    
    public String cargarNomenclaturas(){
       return gsadao.cargarNomenclaturas();
    }
        
    public String cargarNomenclaturas(String codciu){
       return gsadao.cargarNomenclaturas(codciu, false);
    }
    
    public String cargarNomenclaturasRel(String codciu){
       return gsadao.cargarNomenclaturas(codciu, true);
    }
    
    public String insertarNomenclatura(String nombre, String descripcion, String is_default, Usuario usuario){
        return gsadao.insertarNomenclatura(nombre, descripcion, is_default, usuario);
    }
    
    public String actualizarNomenclatura(String idNomenclatura, String nombre, String descripcion, String is_default, Usuario usuario) {
         return gsadao.actualizarNomenclatura(idNomenclatura, nombre, descripcion, is_default, usuario);
    }
    
    public boolean existeNomenclaturaRelCiudad(int idNomenclatura, String codciu){
       return gsadao.existeNomenclaturaRelCiudad(idNomenclatura, codciu);
    }
      
    public String cambiarEstadoNomenclatura(String idNomenclatura, String estado,  Usuario usuario) {
         return gsadao.cambiaEstadoNomenclatura(idNomenclatura, estado, usuario);
    }
      
    public String insertarRelNomenclaturaDireccion(String idNomenclatura, String ciudad, Usuario usuario){
        return gsadao.insertarRelNomenclaturaDireccion(idNomenclatura, ciudad, usuario);
    }
    
    public String eliminarRelNomenclaturaDireccion(String id) {
        return gsadao.eliminarRelNomenclaturaDireccion(id);
    }
    
    public ArrayList listadoNomenclaturasDefault() throws Exception {
        ArrayList cadena = new ArrayList();
        try {
            cadena = gsadao.listadoNomenclaturasDefault();

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar ciudades: " + e.toString());
        }
        return cadena;       
    }
    
    public JsonArray arrayEntidadesCartera(Usuario u) {
        
        JsonArray json = null;
        try {
            json = gsadao.EntidadesProveedor(u);
        } catch (Exception ex) {
            Logger.getLogger(GestionSolicitudAvalService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return json;
        }

    }
    
    public String datosProveedorEntidades(String nit_entidad, Usuario u) {
        String json = "{}";
        try {
            json = gsadao.datosEntidadesProveedor(nit_entidad, u);
        } catch (Exception ex) {
            Logger.getLogger(GestionSolicitudAvalService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return json;
        }
    }
    
    public String ValidarCambiosObligaciones(ArrayList<ObligacionesCompra> nuevas_obligacionesList, SolicitudPersona persona) throws Exception {
        String datos = "#titulo";
        ArrayList<ObligacionesCompra> obligaciones = this.buscarObligacionesForm(Integer.parseInt(persona.getNumeroSolicitud()));
        for (int i = 0; i < nuevas_obligacionesList.size(); i++) {
            boolean sw = false;
            ObligacionesCompra compra = nuevas_obligacionesList.get(i);
            for (int j = 0; j < obligaciones.size(); j++) {
                ObligacionesCompra oc = obligaciones.get(j);
                if (compra.getSecuencia() == oc.getSecuencia()) {
                    
                    //validamos los campos del formulario...
                    if (!compra.getNit_proveedor().equals(oc.getNit_proveedor())) {
                            datos += "nit_entidad  : " + " " + oc.getNit_proveedor() + "\n";
                            sw = true;
                    }
                    
                    if (!compra.getEntidad().equals(oc.getEntidad())) {
                            datos += "entidad  : " + " " + oc.getEntidad()+ "\n";
                            sw = true;
                    }
                    
                    if (!compra.getNumero_cuenta().equals(oc.getNumero_cuenta())) {
                            datos += "numero_cuenta  : " + " " + oc.getNumero_cuenta()+ "\n";
                            sw = true;
                    }
                    
                    if (!compra.getTipo_cuenta().equals(oc.getTipo_cuenta())) {
                            datos += "tipo_cuenta  : " + " " + oc.getTipo_cuenta()+ "\n";
                            sw = true;
                    }
                    
                    if (compra.getValor_comprar()!=oc.getValor_comprar()) {
                            datos += "valor_comprar  : " + " " + oc.getValor_comprar()+ "\n";
                            sw = true;
                    }                    

                }

            }

            if (sw) {
                datos = datos.replaceAll("#titulo", "\n***Informacion Obligacion***\n");
            } else {
                datos = datos.replaceAll("#titulo", "");
            }
        }

        return datos;

    }
    
    
    public ArrayList<ObligacionesCompra> buscarObligacionesForm(int num_solicitud) throws Exception {
        ArrayList<ObligacionesCompra> lista = null;
        try {
            lista = gsadao.buscarObligacionesForm(num_solicitud);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return lista;
    }

    /**
     * Genera una lista de las entidades con obligaciones
     * @return ArrayList
     * @throws Exception cuando hay error
     */
    public ArrayList listadoEntidadesComprar(String act) throws Exception {
        ArrayList cadena = new ArrayList();
        try {
            cadena = gsadao.listadoEntidadesComprar(act);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar Entidades: " + e.toString());
        }
        return cadena;
    }
    
    /**
     * Genera una lista los detalles de la entidad con obligaciones
     * @return ArrayList
     * @throws Exception cuando hay error
     */
    public ArrayList listadoDetalleEntidad(String entidad) throws Exception {
        ArrayList cadena = new ArrayList();
        try {
            cadena = gsadao.listadoDetalleEntidad(entidad);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar La info de la entidad: " + e.toString());
        }
        return cadena;
    }    

    public ArrayList<BeanGeneral> negociosPorLegalizar (String usuario) throws Exception{
        return gsadao.negociosPorLegalizar(usuario);
    }
    
    public ArrayList busquedaPagadurias(String id_usuario) throws Exception {
        ArrayList cadena = new ArrayList();
        try {
            //cadena = gsadao.buscarAfilsUsuario(id_usuario);
            cadena = gsadao.buscarPagaduriasLibranzas(id_usuario);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return cadena;
    }
    
    public boolean insertarSolicitudLibranza( SolicitudAval datoSolic
                                            , ArrayList<SolicitudPersona> lista_pers
                                            , ArrayList<SolicitudLaboral> lista_lab
                                            , ArrayList<SolicitudReferencias> lista_refs
                                            , ArrayList<SolicitudFinanzas> finanzasList 
                                            , ArrayList<SolicitudTransacciones> transaccionesList
                                            , ArrayList<SolicitudBienes> bienesList
                                            , ArrayList<SolicitudVehiculo> vehiculosList
                                            , ArrayList<SolicitudOblComprar> obligacionesList
                                            ) throws Exception {
        boolean sw;
        try {
            sw=gsadao.insertarDatosLibranza( datoSolic, lista_refs, new ArrayList<SolicitudCuentas>(),lista_lab,bienesList
                                 , vehiculosList, obligacionesList, lista_pers
                                 , new ArrayList<SolicitudHijos>(), null, null, null, finanzasList, transaccionesList);
        }
        catch (Exception e) {
            throw new Exception("Error en insertarDatosnat: "+e.toString());
        }
        return sw;
    }
    
    public ArrayList<SolicitudPersona> buscarPersonas(int numero_solicitud) throws SQLException {
        return gsadao.buscarPersonas(numero_solicitud);
    }
    
    public ArrayList<SolicitudLaboral> buscarLabores(int numero_solicitud) throws SQLException {
        return gsadao.buscarLabores(numero_solicitud);
    }
    
    public ArrayList<SolicitudReferencias> buscarReferencias(int numero_solicitud) throws SQLException {
        return gsadao.buscarReferencias(numero_solicitud);
    }
    
    public ArrayList<SolicitudFinanzas> buscarFinanzas(int numero_solicitud) throws SQLException {
        return gsadao.buscarFinanzas(numero_solicitud);
    }
    
    public ArrayList<SolicitudTransacciones> buscarTransacciones(int numero_solicitud) throws SQLException {
        return gsadao.buscarTransacciones(numero_solicitud);
    }

    public void rechazarSolicitud(String sol) throws Exception {
        gsadao.rechazarSolicitud(sol);
    }    
    
    public void actualizarSolicitudFiltroLibranza(int numero_solicitud, int id_filtro) throws Exception {
        gsadao.actualizarSolicitudFiltroLibranza(numero_solicitud, id_filtro);
    }
    
    /**
     * Genera un listado de barrios para colocar en un objeto select
     * @param codciu
     * @return ArrayList
     * @throws Exception cuando hay error
     */
    public ArrayList listadoBarrios(String codciu) throws Exception {
        ArrayList cadena = new ArrayList();
        try {
            cadena = gsadao.listadoBarrios(codciu);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar barrios: " + e.toString());
        }
        return cadena;
    }
        
    public String cargarGridBarrios(String codciu){
       return gsadao.cargarGridBarrios(codciu);
    }    
    
    public String insertarBarrio(String codciu, String nombre, Usuario usuario){
        return gsadao.insertarBarrio(codciu, nombre, usuario);
    }
    
    public String actualizarBarrio(String idBarrio, String nombre, Usuario usuario) {
         return gsadao.actualizarBarrio(idBarrio, nombre, usuario);
    }

    public String cambiarEstadoBarrio(String idBarrio, Usuario usuario) {
         return gsadao.cambiaEstadoBarrio(idBarrio, usuario);
    }

    /**
     * Busca la fecha para el calculo de los preaprobado x unidad de negocio
     * @param identificacion
     * @param idUnidadNegocio
     * @return
     */
    public BeanGeneral buscarFechaPreaprobado(String identificacion, int idUnidadNegocio) {
        return gsadao.buscarFechaPreaprobado(identificacion,idUnidadNegocio);
    }

    /**
     * Busca la informaci�n b�sica de un cliente
     * @param id documento del cliente
     * @return String en formato JSON
     */
    public String buscarInformacionBasicaPersona(String id) {
        try {
            SolicitudPersona persona = gsadao.buscarPersona(Integer.parseInt(id));
            if (persona == null) {
                return "{\"data\":0}";
            } else {
                // Se asigna de forma est�tica CO como pa�s, porque no se crean solicitudes con paises diferentes
                String json = String.format("{\"nombre\":\"%s\",\"direccion\":\"%s\",\"barrio\":\"%s\",\"pais\":\"CO\",\"departamento\":\"%s\",\"ciudad\":\"%s\", \"ciudadExpedicion\":\"%s\","
                        + "\"telefono\":\"%s\",\"celular\":\"%s\",\"email\":\"%s\"}",
                        persona.getNombre(), persona.getDireccion(), persona.getBarrio(), persona.getDepartamento(), persona.getCiudad(), persona.getCiudadExpedicionId(),
                        persona.getTelefono(), persona.getCelular(), persona.getEmail());
                return json;  
            }            
        } catch (Exception e) {
            return "{\"data\": \"Error\"}";
        }
    }
    
    public int obtenerIdFiltro(int numeroSolicitud) {
        return gsadao.obtenerIdFiltro(numeroSolicitud);
    }
    
    public String buscarCompraCartera(String negocio) {
        return gsadao.buscarCompraCartera(negocio);
    }
    
    public String modificarComprarCartera(String negocio, int[] secuencia, String[] nit, String[] cuenta, double[] valor) {
        try {
            gsadao.modificarCompraCartera(negocio, secuencia, nit, cuenta, valor);
            return "{\"respuesta\": \"Se actualiz� correctamente\"}";
        } catch (SQLException ex) {
            if (ex.getSQLState().equals("INV0")) {
                return String.format("{\"error\": \"%s\"}", ex.getMessage());
            }
            return "{\"error\": \"No se modificaron los registros, hubo en la comunicaci�n con la base de datos\"}";
        }
    }
    
    public AvalFianzaBeans getConfiguracionFianza(int idConvenio ,int plazo, int id_producto, int id_cobertura,String nit_empresa){
      return  gsadao.getConfiguracionFianza(idConvenio,plazo,id_producto, id_cobertura, nit_empresa);    
    }

    public JsonObject validarDireccionCliente(String direccion, String nit) {
      return gsadao.validarDireccionCliente(direccion,nit); 
    }
    
    public ArrayList<Destino_Credito> cargarDestinos(int unidadNegocio) throws Exception{
        return gsadao.cargarDestinos(unidadNegocio);
    }
}
