
/***********************************************************************************
 * Nombre clase : ............... ReimpresionService.java                            *
 * Descripcion :................. Clase que instancia los metodos de ReimpresionDAO  *
 *                                con la BD.                                         *
 * Autor :....................... Ing. Ivan Gomez                                    *
 * Fecha :....................... Created on 23 de febrero de 2007, 10:59 AM         *
 * Version :..................... 1.0                                                *
 * Copyright :................... Fintravalores S.A.                            *
 ***********************************************************************************/
package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.sql.*;
import java.io.*;

/**
 *
 * @author  Ivan DArio
 */
public class ReimpresionService {
    private ReimpresionDAO dao;
    private List listaCheques;
    /** Creates a new instance of ReimpresionService */
    public ReimpresionService() {
        dao = new ReimpresionDAO();
    }
    public ReimpresionService(String dataBaseName) {
        dao = new ReimpresionDAO(dataBaseName);
    }
    
    
     /**
     * Obtiene los cheques por un rango de cheques
     * @autor Ing. Ivan Gomez
     * @param String dstrct, String banco, String sucursal, String Cheque inicial, String Cheque final
     * @throws SQLException
     * @version 1.0
     */
    public void buscarCheques(String dstrct, String banco, String sucursal, String Cinicial, String Cfinal) throws SQLException{
        this.setListaCheques(dao.buscarCheques(dstrct, banco, sucursal, Cinicial, Cfinal));
    }
    
    
    /**
     * Metodo updateCheques, metodo para actualizar la reimpresion
     * @autor  Ing. Ivan Dario Gomez
     * @param  List chequesSelec,List chequesNoSelec
     * @version  1.0
     */
    public void updateCheques(List chequesSelec,List chequesNoSelec) throws SQLException {
        dao.updateCheques(chequesSelec, chequesNoSelec);
    }
    
    /**
     * Getter for property listaCheques.
     * @return Value of property listaCheques.
     */
    public java.util.List getListaCheques() {
        return listaCheques;
    }
    
    /**
     * Setter for property listaCheques.
     * @param listaCheques New value of property listaCheques.
     */
    public void setListaCheques(java.util.List listaCheques) {
        this.listaCheques = listaCheques;
    }
    
}
