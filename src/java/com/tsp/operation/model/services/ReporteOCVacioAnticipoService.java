/***********************************************************************************
 * Nombre clase : ............... ReporteOCAnticipoService.java                    *
 * Descripcion :................. Clase que maneja el Service que contiene los     *
 *                                metodos que interactuan entre el DAO y el Action *
 *                                para generar los datos del reporte               *
 *                                de OC con Anticipo sin tr�fico.                  *
 * Autor :....................... Ing. Iv�n Dar�o Devia Acosta                     *
 * Fecha :........................ 23 de noviembre de 2006, 10:25 PM               *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/
package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.sql.*;
/**
 * 
 * @author  Enrique
 */
public class ReporteOCVacioAnticipoService {
    /*
     *Declaracion de atributos
     */
    private ReporteOCVacioAnticipoDAO  RTDataAccess;
    private List                  ListaVacios;              
    private ReporteOCVacioAnticipoDAO     Datos;
    /** Creates a new instance of RegistroTarjetaService */
    public ReporteOCVacioAnticipoService() {
        RTDataAccess    = new ReporteOCVacioAnticipoDAO();
        ListaVacios     = new LinkedList();
        Datos           = new ReporteOCVacioAnticipoDAO();
    }
    public ReporteOCVacioAnticipoService(String dataBaseName) {
        RTDataAccess    = new ReporteOCVacioAnticipoDAO(dataBaseName);
        ListaVacios     = new LinkedList();
        Datos           = new ReporteOCVacioAnticipoDAO(dataBaseName);
    }
      
    public void ReiniciarDato(){
        this.Datos = null;
    }
 
    
     public void ReiniciarListaVacios(){
        this.ListaVacios = new LinkedList();
    }
    
    public ReporteOCVacioAnticipoDAO getDato(){
        return this.Datos;
    }
    

    
    public List getListVacios(){
        return this.ListaVacios;
    }
    
  /**
     * Metodo List, lista los registros las tablas de acuerdo a los parametros de busqueda
     * @autor : Ing. Enrique De Lavalle
     * @param : String fechainicial
     * @param : String fechafinal
     * @param : String distrito
     * @see   : ReporteOCAnticipoDAO
     * @version : 1.0
     */
    public void List_oc_vacias( String fechainicial, String fechafinal, String distrito ) throws Exception {
        try{
            ReiniciarListaVacios();
            ListaVacios = RTDataAccess.List_oc_vacias(fechainicial, fechafinal, distrito);
        }
        catch(Exception e){
            throw new Exception("Error en ListBus [ReporteOCAnticipoService]...\n"+e.getMessage());
        }
    }

    
    
}
