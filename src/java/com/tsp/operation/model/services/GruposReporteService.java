/*
 * Nombre        GruposReporteService.java
 * Descripción
 * Autor         Alejandro Payares
 * Fecha         10 de mayo de 2006, 11:56 AM
 * Version       1.0
 * Coyright      Transportes Sanchez Polo SA.
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.GruposReporteDAO;
import java.sql.SQLException;
import com.tsp.operation.model.beans.GruposReporte;
import java.util.Vector;

/**
 * Clase que brnda los servicios para el acceso a los datos de los grupos de un reporte
 * @author Alejandro Payares
 */
public class GruposReporteService {
    
    
    private GruposReporteDAO dao;
    
    private GruposReporte grAux;
    
    /**
     * Crea una nueva instancia de GruposReporteService
     * @autor  Alejandro Payares
     */
    public GruposReporteService() {
        dao = new GruposReporteDAO();
    }
    
    /**
     * Permite guardar los diferentes grupos de un reporte en la base de datos.
     * @param gr El objeto que contiene la información de los grupos del reporte
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     * @autor Alejandro Payares
     */
    public void guardarGrupos(GruposReporte gr)throws SQLException {
        dao.guardarGrupos(gr);
    }
    
    
    /**
     * Carga en un objeto <CODE>com.tsp.operation.model.beans.GruposReporte</CODE> los grupos de un reporte.
     * @param codigoReporte El codigo del reporte del que serán cargados los datos
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     * @return El objeto con los datos de los grupos del reporte
     * @autor Alejandro Payares
     */
    public GruposReporte cargarGrupos(String codigoReporte)throws SQLException{
        grAux = dao.cargarGrupos(codigoReporte);
        return grAux;
    }
    
    /**
     * Getter for property grAux.
     * @return Value of property grAux.
     */
    public GruposReporte getGrAux() {
        return grAux;
    }
    
    /**
     * Setter for property grAux.
     * @param grAux New value of property grAux.
     */
    public void setGrAux(GruposReporte grAux) {
        this.grAux = grAux;
    }
    
    /**
     * Agrega un grupo al reporte
     * @param nombreGrupo El nombre del nuevo grupo
     * @param color El color del grupo
     * @autor Alejandro Payares
     */
    public void agregarGrupo(String nombreGrupo, String color){
        if ( grAux == null ) {
            grAux = new GruposReporte();
        }
        grAux.agregarGrupo(nombreGrupo, color);
    }
    
    /**
     * Elimina un grupo del reporte
     * @param nombreGrupo El nombre del grupo a eliminar
     * @autor Alejandro Payares
     */
    public void eliminarGrupo(String nombreGrupo){
        if ( grAux == null ) {
            grAux = new GruposReporte();
        }
        grAux.eliminarGrupo(nombreGrupo);
    }
    
    /**
     * Busca los reportes configurables y los almacena en un vector
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     * @autor Alejandro Payares
     */
    public void buscarReportes()throws SQLException{
        dao.buscarReportes();
    }
    
    /**
     * Devuelve los reportes encontrados por el metodo buscarReportes
     * @return Un vector con los reportes
     * @autor Alejandro Payares
     */
    public Vector obtenerReportes(){
        return dao.obtenerReportes();
    }
    
    /**
     * Busca los diferentes grupos del reporte dado.
     * @param codReporte El codigo del reporte al cual se le buscaran los grupos
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     * @autor Alejandro Payares
     */
    public void buscarGrupos(String codReporte)throws SQLException{
        dao.buscarGrupos(codReporte);
    }
    
    
    
    /**
     * Devuelve los grupos encontrados por el metodo buscarGrupos
     * @return Un vector con los grupos
     * @autor Alejandro Payares
     */
    public Vector obtenerGrupos(){
        return dao.obtenerGrupos();
    }
    
    /**
     * Agrega un nuevo campo para el reporte específicado
     * @param dstrct El distrito
     * @param codigorpt El codigo del reporte
     * @param nomcampo El nombre del campo
     * @param titulocampo El titulo del campo
     * @param grupo EL nombre del grupo al que pertenece el campo
     * @param creation_user El usuario de creación del registro
     * @param base La base
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     * @autor Alejandro Payares
     */
    public void agregarCampo(String dstrct,String codigorpt,String nomcampo,String titulocampo,String grupo, String colorGrupo,String creation_user,String base)throws SQLException{
        dao.agregarCampo(dstrct, codigorpt, nomcampo, titulocampo, grupo, colorGrupo, creation_user, base);
    }
    
    /**
     * Modifica un campo del reporte especificado
     * @autor Alejandro Payares
     * @param nomcampoAntiguo El antiguo nombre del campo
     * @param colorgrupo El nuevo color del grupo
     * @param dstrct El distrito
     * @param antiguoOrden El antiguo orden del campo
     * @param orden El nuevo orden del campo
     * @param codigorpt El codigo del reporte al que pertenece el campo
     * @param nomcampo El nuevo nombre del campo
     * @param titulocampo EL titulo del campo
     * @param grupo El grupo del campo
     * @param user_update El usuario de actualización
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     */
    public void editarCampo(    String dstrct,
                                int antiguoOrden,
                                int orden,
                                String codigorpt,
                                String nomcampo,
                                String nomcampoAntiguo,
                                String titulocampo,
                                String grupo,
                                String colorgrupo,
                                String user_update
        )throws SQLException{
        dao.editarCampo(dstrct, antiguoOrden, orden, codigorpt, nomcampo, nomcampoAntiguo, titulocampo, grupo, colorgrupo, user_update);
    }
    
    /**
     * Elimina uno o más campos del reporte especificado
     * @autor Alejandro Payares
     * @param distrito EL distrito
     * @param user_update El usuario de actualización
     * @param codReporte EL codigo del reporte
     * @param nombres Los nombres de los campos a eliminar
     * @param ordenes Los ordenes de los campos
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     */
    public void eliminarCampos(String codReporte, String[] nombres, String [] ordenes, String distrito, String user_update)throws SQLException {
        dao.eliminarCampos(codReporte, nombres, ordenes, distrito, user_update);
    }
    
    /**
     * Guarda los cambios realizados al orden de los campos
     * @autor Alejandro Payares
     * @param codReporte EL codigo del reporte
     * @param nombres Los nombres de los campos a eliminar
     * @param ordenes Los ordenes de los campos
     * @param ordenesAntiguos Los ordenes antiguos de los campos
     * @param distrito EL distrito
     * @param user_update El usuario de actualización
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     */    
    public void guardarCambiosOrdenCampos(String codReporte, String[] nombres, String [] ordenes, String [] ordenesAntiguos, String distrito, String user_update)throws SQLException {
        dao.guardarCambiosOrdenCampos(codReporte, nombres, ordenes, ordenesAntiguos, distrito, user_update);
    }
    
}
