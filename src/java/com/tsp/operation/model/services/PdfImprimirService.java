/*
 * PdfImprimirService.java
 *
 * Created on 15 de enero de 2008, 06:00 PM
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;

import java.awt.Color;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Vector;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Calendar;
import java.util.Date;

import com.lowagie.text.Rectangle;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Paragraph;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.Font;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.Instrucciones_DespachoDAO;
import java.util.*;
import java.io.*;
import com.lowagie.text.Image;

import java.awt.Color;
/**
 *
 * @author  Administrador
 */
public class PdfImprimirService {

    private String nit = "";//nit del cliente
    private String nombre_cliente = "";//nombre del cliente
    private String direccion = "";//direccion del cliente
    private String telefono = "";//telefono del cliente
    private String telcontacto = "";//telefono contacto
    private String banco = "";//numero del banco
    private String aval = "";//numero de aprovacion
    private String cuenta_cheque="";//numero de cuenta
    private String beneficiario  ="";//nombre del beneficiario
    private String numero_cheque  ="";//numero del cheque
    private String numero_factura = "";//numero de la factura
    private String valor  ="";//valor del cheque
    private String[] observaciones = null;//Observaciones del la factura

    int diay = 0;
    int mesy = 0;
    int anoy = 0;

    int enum_letras=0;
    String ecc_representante="";
    String erepresentada="";
    String ecc_deudor="";
    String ecc_codeudor="";
    String eswcodeudor="";

    Document document;

    int n=0;

    int contador=0;
    int cantidad_de_letras=7;
    PdfPTable table;

    float[] widths = {0.12f, 0.17f, 0.1f, 0.27f, 0.17f, 0.17f};
    float[] widthsCabecera = {0.2f, 0.3f, 0.15f, 0.35f};

    BaseFont bf;

    Font fuente_normal    = new Font(bf, 9);
    Font fuente_normal_grande   = new Font(bf, 20);
    Font fuente_negrita_grande = new Font(bf, 20f);
    Font fuente_negrita8 = new Font(bf, 9);

    Font fuente_normal_media = new Font(bf,14);//jemartinez
    Font fuente_negrita_media = new Font(bf,14);//jemartinez

    PdfWriter writer;
    Rectangle page;

    PdfPCell linea_blanco;
    SimpleDateFormat fmt = null;
    String ruta = "";
    //Instrucciones_DespachoDAO dao = new Instrucciones_DespachoDAO();

    //Variables de datos
    String orden = "";
    Model model;
    File pdf;
    Model modelito;

    BeanGeneral bg;

    String x="xxx";
    Phrase texto_principal;

    String no_aprobacion="",ciudad="",fecha_otorgamiento="",fecha_vencimiento="",consecutivo="",digito="",nombre="",cedula="",ciudad_cedula="",representado="",nombre_codeudor="",cedula_codeudor="",dia_pagare="",mes_pagare="",ano_pagare="",empresa="",plata_letra="",plata_numero="",empresa_facultada="",ciudad_pago="",cedula_deudor="",cedula_firma_codeudor="";

    String fecha_otorgamiento_inicial="";//mar14d8

    java.util.Calendar Fecha_Negocio ;
    java.util.Calendar Fecha_Temp ;

    ResourceBundle rb;

    Phrase frase;
    //Phrase frase2;

    /** Creates a new instance of PdfImprimirService */

    public PdfImprimirService() throws Exception {
        bf = BaseFont.createFont( BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED );
        fuente_negrita_grande.setStyle( com.lowagie.text.Font.BOLD );
        fuente_negrita8.setStyle( com.lowagie.text.Font.BOLD );
        fuente_negrita_media.setStyle(com.lowagie.text.Font.BOLD); //jemartinez
    }

    public void iniciar() throws Exception {
        contador=0;
        rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");

        n=n+1;
        //.out.println("n"+n);
        this.ruta = rb.getString("ruta")+"/pdf/rotu"+n+".pdf";

        document            = new Document();

        PdfWriter.getInstance(document, new FileOutputStream( this.ruta ));
        //.out.println("n2"+n);
        page    = document.getPageSize();
        //.out.println("n2"+n);
        document.open();
        //.out.println("n2"+n);
    }

    public void iniciar(String numero, String usuario) throws Exception {
        contador=0;
        rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        this.ruta = rb.getString("ruta")+"/exportar/migracion/" + usuario;
        File file = new File( this.ruta );
        file.mkdirs();
        document            = new Document();
        this.ruta +=  "/Factura"+numero+".pdf";
        PdfWriter.getInstance(document, new FileOutputStream( this.ruta  ));
        //.out.println("n2"+n);
        page    = document.getPageSize();
        //.out.println("n2"+n);
        document.open();
        //.out.println("n2"+n);
    }

    public void reporteLetras(Model model ) throws Exception{
        iniciar();

        this.modelito      = model;
        try{
            this.escribirLetras( );
            document.close();
            //.out.println("TERMINO");
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void escribirLetras(  ) throws Exception{

        try {
            float[] anchos = {0.05f,1.0f,0.05f};//porcentajes de ancho de columnas de una tabla
            //.out.println("n3"+n);
            document.newPage();//nueva pag
            //.out.println("n4"+n);

            while (contador<cantidad_de_letras){
                //for(int i=1;i<=5;i++){//desde 1 hasta n
                for(int j=1;j<=2;j++){//2 en cada pag

                    if (contador<cantidad_de_letras){//si no ha terminado

                        Fecha_Temp = Util.TraerFecha2(contador, diay, mesy, anoy);//mar31d8
                        //Fecha_Temp.add(2,contador+1);//mar14d8
                        //Fecha_Temp.add(2,1);

                        fecha_vencimiento=Util.crearStringFechaDateLetraRemix2(Fecha_Temp);//mar31d8
                        //fecha_vencimiento=Util.crearStringFechaDateLetra2(Fecha_Temp);

                        int mesmostrabletem=(Integer.valueOf(fecha_vencimiento.substring(3,5)).intValue());
                        String mesmostrable="";
                        if (mesmostrabletem<10){
                                mesmostrable="0"+mesmostrabletem;
                        }else{
                                mesmostrable=""+mesmostrabletem;
                        }
                        int anomostrable=				Integer.valueOf(fecha_vencimiento.substring(6,10)).intValue();

                        String temxxx=fecha_vencimiento;

                        dia_pagare=		fecha_vencimiento.substring(0,2);//dia de la fecha de vencimiento

                        mes_pagare=Util.NombreMes3((Integer.valueOf(fecha_vencimiento.substring(3,5)).intValue()));//mar31d8

                        //mes_pagare=		fecha_vencimiento.substring(3,5);//mes de la fecha de vencimiento
                        //mes_pagare=Util.NombreMes3(Integer.valueOf(mes_pagare).intValue());


                        //ano_pagare=		fecha_vencimiento.substring(6,10);//a�o del mes de vancimiento
                        ano_pagare=""+anomostrable;

                        int cols=6;//cantidad de filas de 1 tabla
                        //float porcent=0.01f;

                        //.out.println("op1");
                        PdfPTable table = new PdfPTable(cols);//se crea una tabla de 6 columnas
                        //.out.println("op2");
                        table.setWidthPercentage(100);//uso de 100% horizontal

                        PdfPCell cell    = new PdfPCell();//se crea una celda

                        PdfPCell cell_vacia_izq    = new PdfPCell();//se crea una celda
                        //.out.println("op3");
                        cell_vacia_izq.setBorderWidthTop(0);//borde
                        cell_vacia_izq.setBorderWidthBottom(0);//borde
                        cell_vacia_izq.setBorderWidthLeft(1);//borde
                        cell_vacia_izq.setBorderWidthRight(0);//borde
                        //.out.println("op4");
                        cell_vacia_izq.setPhrase  ( new Phrase("", fuente_normal ) );//celda vacia
                        //.out.println("op5");
                        PdfPCell cell_vacia_der   = new PdfPCell();//se crea una celda
                        cell_vacia_der.setBorderWidthTop(0);//borde
                        cell_vacia_der.setBorderWidthBottom(0);//borde
                        cell_vacia_der.setBorderWidthLeft(0);//borde
                        cell_vacia_der.setBorderWidthRight(1);//borde
                        cell_vacia_der.setPhrase  ( new Phrase("", fuente_normal ) );//celda vacia
                        PdfPCell cell_vacia3    = new PdfPCell();//se crea una celda
                        cell_vacia3.setColspan(3);//celda gasta 2 columnas
                        cell_vacia3.setBorderWidthTop(0);//borde
                        cell_vacia3.setBorderWidthBottom(0);//borde
                        cell_vacia3.setBorderWidthLeft(1);//borde
                        cell_vacia3.setBorderWidthRight(1);//borde
                        cell_vacia3.setPhrase  ( new Phrase("", fuente_normal ) );//celda vacia

                        //cell.setBorderWidthTop(1);
                        //.out.println("op6");
                        rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                        Image jpg = Image.getInstance(rb.getString("ruta")+"/images/fen.jpg");//se crea una imagen
                        //Image jpg = Image.getInstance("C:/Tomcat5/webapps/Fintra/images/fen.jpg");//se crea una imagen
                        //.out.println("op7");
                        //jpg.scalePercent(porcent);
                        jpg.scaleToFit(35,25);//se reduce la imagen
                        //jpg.setBorder(0);
                        //jpg.setBorderColor(Color.WHITE);
                        cell.setImage(jpg);//se mete la imagen en la celda

                        cell.setBorderWidthTop(1);//borde
                        cell.setBorderWidthBottom(0);//borde
                        cell.setBorderWidthLeft(1);//borde
                        cell.setBorderWidthRight(0);//borde

                        table.addCell(cell);//se mete la celda de la imagen

                        //cell.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );

                        cell.setBorderWidthTop(1);//borde
                        cell.setBorderWidthBottom(0);//borde
                        cell.setBorderWidthLeft(0);//borde
                        cell.setBorderWidthRight(0);//borde
                        cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda vacia
                        table.addCell(cell);//se mete celda vacia

                        cell.setPhrase  ( new Phrase("LETRA DE CAMBIO SIN PROTESTO" , fuente_normal ) );//celda con texto
                        cell.setColspan(2);//celda gasta 2 columnas

                        cell.setVerticalAlignment( PdfPCell.ALIGN_MIDDLE );//alineamiento vertical
                        cell.setHorizontalAlignment( PdfPCell.ALIGN_CENTER );//alineamiento horizontal

                        table.addCell(cell);//se mete celda con texto

                        frase =  new Phrase("");
                        frase.add(  new Phrase("No Aprobacion ",fuente_normal ));
                        frase.add(  new Phrase(no_aprobacion, fuente_negrita8));

                        //cell.setPhrase  ( new Phrase("No Aprobacion "+no_aprobacion,fuente_normal));//celda con texto
                        cell.setColspan(2);
                        cell.setPhrase  ( frase);

                        cell.setBorderWidthTop(1);//borde
                        cell.setBorderWidthBottom(0);//borde
                        cell.setBorderWidthLeft(0);//borde
                        cell.setBorderWidthRight(1);//borde

                        cell.setHorizontalAlignment( PdfPCell.ALIGN_CENTER );//alineamiento horizontal
                        cell.setVerticalAlignment( PdfPCell.ALIGN_MIDDLE );//alineamiento vertical

                        table.addCell(cell);//se mete celda con texto

                        //.out.println ("n5"+n);
                        document.add(table);//se mete la tabla en el documento
                        //.out.println("n6"+n);

                        table = new PdfPTable(anchos);//se crea segunda tabla
                        table.setWidthPercentage(100);//uso de 100% horizontal

                        cell.setColspan(1);//celda gastara 1 columna

                        cell.setBorderWidthTop(0);//borde
                        cell.setBorderWidthBottom(0);//borde
                        cell.setBorderWidthLeft(0);//borde
                        cell.setBorderWidthRight(0);//borde

                        //cell.setFixedHeight(10);
                        cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia

                        table.addCell(cell_vacia_izq);//se mete celda vacia

                       /* if (temxxx.startsWith("28")){//temporal
                            .out.println("");
                            temxxx=temxxx.replaceAll("28", "29");
                        }*/

                        cell.setPhrase  ( new Phrase("(Ciudad y Fecha de Otorgamiento) "+ ciudad +" "+ fecha_otorgamiento +"                Fecha de Vencimiento            "+temxxx, fuente_normal ) );//celda con texto

                        cell.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );//alineamiento horizontal
                        table.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );//alineamiento vertical
                        table.addCell(cell);//se mete celda con texto

                        //table.completeRow() ;//
                        cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda vacia
                        table.addCell(cell_vacia_der);//se mete celda vacia

                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia

                        table.addCell(cell_vacia_izq);//se mete celda vacia

                        digito=""+(contador+1);
                        cell.setPhrase  ( new Phrase("(Consecutivo de Letras)       "+consecutivo+"-"+digito, fuente_normal ) );//celda con texto
                        table.addCell(cell);//se mete celda con texto

                        cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda vacia
                        table.addCell(cell_vacia_der);//se mete celda vacia
                        //table.completeRow() ;//

                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia

                        table.addCell(cell_vacia_izq);//se mete celda vacia

                        texto_principal=getTextoPrincipal();

                        cell.setPhrase  ( texto_principal );//celda con texto
                        cell.setHorizontalAlignment( PdfPCell.ALIGN_JUSTIFIED );//alineamiento horizontal
                        table.addCell(cell);//se mete celda con texto

                        cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda vacia
                        table.addCell(cell_vacia_der);//se mete celda vacia
                        //table.completeRow() ;//

                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia

                        table.addCell(cell_vacia_izq);//se mete celda vacia

                        cell.setPhrase  ( new Phrase("Otorgada y Aceptada (art 676 y 685 C�digo de Comercio)",fuente_negrita8 ) );//celda con texto
                        cell.setHorizontalAlignment( PdfPCell.ALIGN_CENTER );//alineamiento horizontal
                        table.addCell(cell);//se mete celda con texto

                        table.addCell(cell_vacia_der);//se mete celda vacia
                        //table.completeRow() ;//

                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia

                        table.addCell(cell_vacia_izq);//se mete celda vacia

                        frase =  new Phrase("");
                        frase.add(  new Phrase("Firma Deudor _______________________ Documento de Identidad ",fuente_normal ));
                        frase.add(  new Phrase(cedula_deudor, fuente_negrita8));
                        frase.add(  new Phrase("  Huella",fuente_normal ));
                        cell.setPhrase  ( frase);

                        //cell.setPhrase  ( new Phrase("Firma Deudor _______________________ Documento de Identidad " +cedula_deudor + "  Huella", fuente_normal ) );//celda con texto

                        cell.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );//alineamiento horizontal
                        table.addCell(cell);//se mete celda con texto
                        cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda vacia
                        table.addCell(cell_vacia_der);//se mete celda vacia

                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia

                        table.addCell(cell_vacia_izq);//se mete celda vacia

                        frase =  new Phrase("");
                        frase.add(  new Phrase("Firma Codeudor _______________________ Documento de Identidad ",fuente_normal ));
                        frase.add(  new Phrase(cedula_firma_codeudor, fuente_negrita8));
                        frase.add(  new Phrase("  Huella",fuente_normal ));
                        cell.setPhrase  ( frase);

                        //cell.setPhrase  ( new Phrase("Firma Codeudor _______________________ Documento de Identidad " + cedula_firma_codeudor + "  Huella", fuente_normal ) );//celda con texto

                        cell.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );//alineamiento horizontal
                        table.addCell(cell);//se mete celda con texto

                        cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda vacia
                        table.addCell(cell_vacia_der);//se mete celda vacia
                        //table.completeRow() ;//

                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia

                        table.addCell(cell_vacia_izq);//se mete celda vacia

                        cell.setPhrase  ( new Phrase("Afiliado", fuente_normal ) );//celda con texto
                        cell.setHorizontalAlignment( PdfPCell.ALIGN_RIGHT );//alineamiento horizontal
                        table.addCell(cell);//se mete celda con texto

                        cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda vacia
                        table.addCell(cell_vacia_der);//se mete celda vacia

                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia

                        cell.setPhrase  ( new Phrase(".", fuente_normal ) );//celda con punto
                        cell.setColspan(3);//celda gasta 3 columnas
                        cell.setBorderWidthTop(0);//borde
                        cell.setBorderWidthBottom(1);//borde
                        cell.setBorderWidthLeft(1);//borde
                        cell.setBorderWidthRight(1);//borde
                        table.addCell(cell);//se mete celda con punto

                        cell.setBorderWidthTop(0);//borde
                        cell.setBorderWidthBottom(0);//borde
                        cell.setBorderWidthLeft(0);//borde
                        cell.setBorderWidthRight(0);//borde
                        cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda vacia
                        cell.setColspan(1);//celda gasta 1 columna
                        table.addCell(cell);//se mete celda vacia
                        table.addCell(cell);//se mete celda vacia
                        table.addCell(cell);//se mete celda vacia
                        table.addCell(cell);//se mete celda vacia
                        table.addCell(cell);//se mete celda vacia
                        table.addCell(cell);//se mete celda vacia

                        table.addCell(cell);
                        table.addCell(cell);
                        table.addCell(cell);
                        table.addCell(cell);
                        table.addCell(cell);
                        table.addCell(cell);
                        table.addCell(cell);
                        table.addCell(cell);
                        table.addCell(cell);
                        table.addCell(cell);
                        table.addCell(cell);
                        table.addCell(cell);
                        document.add(table);

                    }
                    contador=contador+1;
                    Fecha_Temp = Util.crearCalendarDate(fecha_otorgamiento_inicial);//mar14d8

                }
                document.newPage();
                Fecha_Temp = Util.crearCalendarDate(fecha_otorgamiento_inicial);//mar14d8
            }
        }catch (Exception ex){
            System.out.println("ex"+ex.toString()+"__"+ex.getMessage());
            ex.printStackTrace();
        }
    }

    /*public static void main(String[] args)throws Exception {
        Model model = new Model();
        PdfImprimirService p = new PdfImprimirService();
        p.reporte(model);
    }*/

    public void setVariables(BeanGeneral bg1){
        bg=bg1;
        /*
        no_aprobacion=
        ciudad=
        fecha_otorgamiento=
        fecha_vencimiento=
        consecutivo=
        digito=
        nombre=
        cedula=
        ciudad_cedula=
        representado=
        nombre_codeudor=
        cedula_codeudor=
        dia_pagare=
        mes_pagare=
        ano_pagare=
        empresa=
        plata_letra=
        plata_numero=
        empresa_facultada=
        ciudad_pago=
        cedula_deudor=
        cedula_firma_codeudor=
        cantidad_de_letras=
         */
        //.out.println("en setVariables");
        //.out.println("bg.getValor_17()"+bg.getValor_17());
        no_aprobacion=		bg.getValor_17();
        //.out.println("en setVariables2");
        ciudad=			bg.getValor_19();
        fecha_otorgamiento=	bg.getValor_09();
        ///fecha_vencimiento=	bg.getValor_09();//mes+digito
        consecutivo=		bg.getValor_04();
        ///digito=			el contador del para
        nombre=			bg.getValor_02();
        cedula=			bg.getValor_06();
        cedula=Util.PuntoDeMil(cedula);

        ciudad_cedula=		bg.getValor_12();
        representado=		bg.getValor_10();//condicion si emp=""
        if (representado.equals("")){
            representado="_________________________";
        }
        nombre_codeudor=	bg.getValor_14();//condicion si es = ""
        if (nombre_codeudor.equals("")){
            nombre_codeudor="_________________________";
        }
        cedula_codeudor=	bg.getValor_15();//condicion si es = ""
        if (cedula_codeudor.equals("")){
            cedula_codeudor="_________________________";
        }else{
            cedula_codeudor=Util.PuntoDeMil(cedula_codeudor);
        }

	///dia_pagare=		fecha_vencimiento.substring(0,2);//dia de la fecha de vencimiento
        ///mes_pagare=		fecha_vencimiento.substring(3,5);//mes de la fecha de vencimiento
        ///ano_pagare=		fecha_vencimiento.substring(6,10);//a�o del mes de vancimiento
        //.out.println("aaaa1");
	empresa=		bg.getValor_03();

        plata_numero=		bg.getValor_05();

        //.out.println("antes de cant en letras");

        //Cantidad en Letras
	RMCantidadEnLetras ejem= new RMCantidadEnLetras();
	String[] a;
        //.out.println("plata_numero1"+plata_numero);
        double valor= 		Double.valueOf(plata_numero).doubleValue();
	a=ejem.getTexto(valor);
        //.out.println("getTexto");
	String res="";
        //.out.println("plata_numero"+plata_numero);
	for(int i=0;i<a.length;i++){
            res=res+((String)a[i]).replace('-',' ');
	}
	//Fin Cant Letras
        res=res.trim();
        plata_numero=Util.customFormat(valor);

        //.out.println("despues de cant en letras");
        plata_letra=		res;//Esto es un metodo que transforma a letras una cantidad    ///

        empresa_facultada=	bg.getValor_03();
        ciudad_pago=		bg.getValor_18();

        cedula_deudor=		bg.getValor_06();
        cedula_deudor=Util.PuntoDeMil(cedula_deudor);

        cedula_firma_codeudor=	bg.getValor_15();//condicion
        if (cedula_firma_codeudor.equals("")){
            cedula_firma_codeudor="_________________________";
        }else{
            cedula_firma_codeudor=Util.PuntoDeMil(cedula_firma_codeudor);
        }

        cantidad_de_letras=	Integer.parseInt(bg.getValor_01());
        //.out.println("aaaa");

        diay = Integer.parseInt(fecha_otorgamiento.substring(8,10));
        mesy = Integer.parseInt(fecha_otorgamiento.substring(5,7));
        anoy = Integer.parseInt(fecha_otorgamiento.substring(0,4));


        Fecha_Negocio = Util.crearCalendarDate(fecha_otorgamiento);
	Fecha_Temp = Util.crearCalendarDate(fecha_otorgamiento);

        fecha_otorgamiento_inicial=fecha_otorgamiento;//mar14d8

        fecha_otorgamiento=Util.crearStringFechaDateLetra2(Fecha_Negocio);

    }

    public Phrase getTextoPrincipal(){
       /* if (dia_pagare.equals("28")){//temporal
            dia_pagare="29";
        }*/
        String cadena="";
        Phrase respuesta =  new Phrase("");
        respuesta.add(  new Phrase("Yo ", fuente_normal));
        respuesta.add(  new Phrase(nombre, fuente_negrita8));
        respuesta.add(  new Phrase(" (Deudor) identificado con la c�dula de ciudadania No. ", fuente_normal));
        respuesta.add(  new Phrase(cedula, fuente_negrita8));
        respuesta.add(  new Phrase(" expedida en ", fuente_normal));
        respuesta.add(  new Phrase(ciudad_cedula, fuente_negrita8));
        respuesta.add(  new Phrase(" en calidad de girador y aceptante (art. 673 c�digo de comercio) ", fuente_normal));
        respuesta.add(  new Phrase("obrando en nombre propio y como representante legal de ", fuente_normal));
        respuesta.add(  new Phrase(representado, fuente_negrita8));
        respuesta.add(  new Phrase(" y como codeudor de ", fuente_normal));
        respuesta.add(  new Phrase("�sta, y, ", fuente_normal));
        respuesta.add(  new Phrase(nombre_codeudor, fuente_negrita8));
        respuesta.add(  new Phrase(" C.C. No. ", fuente_normal));
        respuesta.add(  new Phrase(cedula_codeudor, fuente_negrita8));
        respuesta.add(  new Phrase(" en calidad de codeudor, y en la ", fuente_normal));
        respuesta.add(  new Phrase("fecha cierta: dia ", fuente_normal));
        respuesta.add(  new Phrase(dia_pagare, fuente_negrita8));
        respuesta.add(  new Phrase(" del mes de ", fuente_normal));
        respuesta.add(  new Phrase(mes_pagare, fuente_negrita8));
        respuesta.add(  new Phrase(" del a�o ", fuente_normal));
        respuesta.add(  new Phrase(ano_pagare, fuente_negrita8));
        respuesta.add(  new Phrase(" pagar� solidaria e incondicionalmente en la ciudad de "+ciudad_pago+" (art. 677, 683 y 876. ", fuente_normal));
        respuesta.add(  new Phrase("C�digo de comercio) a la orden de: ", fuente_normal));
        respuesta.add(  new Phrase(empresa, fuente_negrita8));
        respuesta.add(  new Phrase(" o a su(s) tenedor(es) legitimo(s), la suma de: ", fuente_normal));
        respuesta.add(  new Phrase(plata_letra, fuente_negrita8));
        respuesta.add(  new Phrase(" ($", fuente_normal));
        respuesta.add(  new Phrase(plata_numero, fuente_negrita8));
        cadena=") mas intereses remuneratorios liquidables a la tasa ";
        cadena+="m�xima de interes corriente bancario y moratorios a la m�xima tasa legal vigente. En el evento ";
        cadena+="de mora ser�n de mi cargo los gastos de cobranza tasados al 20% del total adeudado al momento ";
        cadena+="del pago y los honorarios igualmente liquidados al 20% del total adeudado. Los giradores, ";
        cadena+="aceptantes de esta letra nos obligamos solidariamente y renunciamos a la presentaci�n para la ";
        cadena+="aceptaci�n y pago, y facultamos a ";
        respuesta.add(  new Phrase(cadena, fuente_normal));
        respuesta.add(  new Phrase(empresa_facultada, fuente_negrita8));
        respuesta.add(  new Phrase(" o a su(s) tenedor(es) legitimo(s) ", fuente_normal));
        respuesta.add(  new Phrase("para rehusar el pago parcial y los avisos de rechazo. Domicilio de pago ", fuente_normal));
        respuesta.add(  new Phrase(ciudad_pago, fuente_negrita8));

        cadena= "Yo " +nombre+ " (Deudor) identificado con la c�dula de ciudadania No. " + cedula + " expedida ";
        cadena+="en " + ciudad_cedula + " en calidad de girador y aceptante (art. 673 c�digo de comercio) ";
        cadena+="obrando en nombre propio y como representante legal de " + representado + " y como codeudor de ";
        cadena+="�sta, y, " +nombre_codeudor+" C.C. No. " + cedula_codeudor + " en calidad de codeudor, y en la ";
        cadena+="fecha cierta: dia " +dia_pagare+ " del mes de " +mes_pagare+ " del a�o " + ano_pagare ;
        cadena+=" pagar� solidaria e incondicionalmente en la ciudad de "+ciudad_pago+" (art. 677, 683 y 876. ";
        cadena+="C�digo de comercio) a la orden de: " +empresa+" o a su(s) tenedor(es) legitimo(s), la suma ";
        cadena+="de: "+plata_letra+" ($" +plata_numero+") mas intereses remuneratorios liquidables a la tasa ";
        cadena+="m�xima de interes corriente bancario y moratorios a la m�xima tasa legal vigente. En el evento ";
        cadena+="de mora ser�n de mi cargo los gastos de cobranza tasados al 20% del total adeudado al momento ";
        cadena+="del pago y los honorarios igualmente liquidados al 20% del total adeudado. Los giradores, ";
        cadena+="aceptantes de esta letra nos obligamos solidariamente y renunciamos a la presentaci�n para la ";
        cadena+="aceptaci�n y pago, y facultamos a "+empresa_facultada+" o a su(s) tenedor(es) legitimo(s) ";
        cadena+="para rehusar el pago parcial y los avisos de rechazo. Domicilio de pago "+ciudad_pago;
        return respuesta;
    }

    public void setBeanGeneral(BeanGeneral bg1){
        bg=bg1;
    }

    public BeanGeneral getBeanGeneral(){
        return bg;
    }

    public int getN(){
        return n;
    }

    public void setVariablesEndoso(String num_letras1,String cc_representante1,String representada1,String cc_deudor1,String cc_codeudor1,String swcodeudor1){
        enum_letras=Integer.parseInt(num_letras1);
        ecc_representante=cc_representante1;
        ecc_representante=Util.PuntoDeMil(ecc_representante);
        erepresentada=representada1;
        ecc_deudor=cc_deudor1;
        ecc_deudor=Util.PuntoDeMil(ecc_deudor);
        //.out.println("ecc_deudor"+ecc_deudor);
        ecc_codeudor=cc_codeudor1;
        ecc_codeudor=Util.PuntoDeMil(ecc_codeudor);
        //.out.println("ecc_codeudor"+ecc_codeudor);
        eswcodeudor=swcodeudor1;
    }

    public void reporteEndosos(Model model ) throws Exception{
        iniciar();
        this.modelito      = model;
        try{
            this.escribirEndosos( );
            document.close();
            //.out.println("TERMINO");
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void escribirEndosos(  ) throws Exception{
        //String supertexto="Endosamos en propiedad con responsabilidad el presente titulo valor a favor de la Fiduciaria Corficolombiana S.A Fideicomiso Fintra nit 800.256.768-6. En consecuencia el pago del mismo debe hacerse directamente a favor de la Fiduciaria Corficolombiana S.A Fideicomiso Fintra o a su orden en la fecha de su vencimiento en la cuenta corriente No. 800-57617-5 del Banco de Occidente.";

        String supertexto="Endosamos en propiedad y sin responsabilidad el presente t�tulo valor a favor de Fintra S. A. nit 802.022.016-1. "+
            "En consecuencia el pago del mismo debe hacerse directamente a favor de la Fiduciaria Corficolombiana S. A. Fideicomiso Fintra "+
            "o a su orden en la fecha de su vencimiento en la cuenta corriente No. 800-57617-5 del Banco de Occidente." ;

        String pie_endoso="Fintra S. A. endosa en propiedad con responsabilidad a la Fiduciaria Corficolombiana S. A. Fideicomiso Fintra.";

        String firma="Firma     ________________________________";
        try {
            float[] anchos = {0.05f,1.0f,0.05f};//porcentajes de ancho de columnas de una tabla
            document.newPage();//nueva pag
            while (contador<cantidad_de_letras){
                for(int j=1;j<=2;j++){//2 en cada pag

                    if (contador<enum_letras){//si no ha terminado

                        PdfPTable table = new PdfPTable(anchos);//se crea una tabla de 6 columnas
                        table.setWidthPercentage(100);//uso de 100% horizontal

                        PdfPCell cell_vacia3    = new PdfPCell();//se crea una celda vacia triple
                        cell_vacia3.setColspan(3);//triple
                        cell_vacia3.setBorderWidthTop(0);//borde
                        cell_vacia3.setBorderWidthBottom(0);//borde
                        cell_vacia3.setBorderWidthLeft(0);//borde
                        cell_vacia3.setBorderWidthRight(0);//borde
                        cell_vacia3.setPhrase  ( new Phrase("", fuente_normal ) );//celda triple vacia
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple

                        PdfPCell cell_vacia    = new PdfPCell();//se crea una celda vacia
                        cell_vacia.setBorderWidthTop(0);//borde
                        cell_vacia.setBorderWidthBottom(0);//borde
                        cell_vacia.setBorderWidthLeft(0);//borde
                        cell_vacia.setBorderWidthRight(0);//borde
                        cell_vacia.setPhrase  ( new Phrase("", fuente_normal ) );//celda triple vacia

                        PdfPCell cell    = new PdfPCell();//se crea una celda
                        cell.setBorderWidthTop(0);//borde
                        cell.setBorderWidthBottom(0);//borde
                        cell.setBorderWidthLeft(0);//borde
                        cell.setBorderWidthRight(0);//borde


                        table.addCell(cell_vacia);//se mete la celda vacia

                        cell.setPhrase  ( new Phrase(supertexto , fuente_normal ) );//celda con texto
                        cell.setHorizontalAlignment( PdfPCell.ALIGN_JUSTIFIED );//alineamiento horizontal
                        table.addCell(cell);//se mete celda con texto

                        table.addCell(cell_vacia);//se mete celda vacia

                        table.addCell(cell_vacia3);//se mete celda vacia triple
                        table.addCell(cell_vacia3);//se mete celda vacia triple
                        table.addCell(cell_vacia3);//se mete celda vacia triple
                        table.addCell(cell_vacia3);//se mete celda vacia triple

                        table.addCell(cell_vacia);//se mete celda vacia

                        cell.setPhrase  ( new Phrase(firma,fuente_normal ) );//celda con texto
                        table.addCell(cell);//se mete celda con texto

                        table.addCell(cell_vacia);//se mete celda vacia

                        table.addCell(cell_vacia3);//se mete celda vacia

                        table.addCell(cell_vacia);//se mete celda vacia

                        frase =  new Phrase("");
                        frase.add(  new Phrase("C.C            ",fuente_normal ));
                        frase.add(  new Phrase(ecc_representante, fuente_negrita8));
                        cell.setPhrase  ( frase);

                        //cell.setPhrase  ( new Phrase("C.C            "+ecc_representante, fuente_normal ) );//celda con texto

                        cell.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );//alineamiento horizontal
                        table.addCell(cell);//se mete celda con texto

                        table.addCell(cell_vacia);//se mete celda vacia

                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia

                        table.addCell(cell_vacia);//se mete celda vacia

                        //Firma     __

                        frase =  new Phrase("");
                        frase.add(  new Phrase("          En nombre y representaci�n de  ",fuente_normal ));
                        frase.add(  new Phrase(erepresentada, fuente_negrita8));
                        cell.setPhrase  ( frase);

                        //cell.setPhrase  ( new Phrase("          En nombre y representaci�n de  "+erepresentada, fuente_normal ) );//celda con texto

                        table.addCell(cell);//se mete celda con texto

                        table.addCell(cell_vacia);//se mete celda vacia

                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia

                        table.addCell(cell_vacia);//se mete celda vacia

                        cell.setPhrase  ( new Phrase("          En se�al de aceptaci�n de lo anterior: ", fuente_normal ) );//celda con texto
                        table.addCell(cell);//se mete celda con texto

                        table.addCell(cell_vacia);//se mete celda vacia

                        table.addCell(cell_vacia3);//se mete celda vacia

                        table.addCell(cell_vacia3);//se mete celda vacia

                        table.addCell(cell_vacia);//se mete celda vacia

                        cell.setPhrase  ( new Phrase("          El deudor.", fuente_normal ) );//celda con punto
                        table.addCell(cell);//se mete celda con punto

                        table.addCell(cell_vacia);//se mete celda vacia

                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia

                        if (eswcodeudor.equals("concodeudor")){

                            table.addCell(cell_vacia);//se mete celda vacia

                            cell.setPhrase  ( new Phrase(firma, fuente_normal ) );//celda vacia
                            table.addCell(cell);//se mete celda vacia

                            table.addCell(cell_vacia);//se mete celda vacia

                            table.addCell(cell_vacia3);

                            table.addCell(cell_vacia);//se mete celda vacia

                            frase =  new Phrase("");
                            frase.add(  new Phrase("C.C            ",fuente_normal ));
                            frase.add(  new Phrase(ecc_deudor, fuente_negrita8));
                            cell.setPhrase  ( frase);

                            //cell.setPhrase  ( new Phrase("C.C            "+ecc_deudor, fuente_normal ) );//celda con texto

                            table.addCell(cell);//se mete celda con texto

                            table.addCell(cell_vacia);

                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);

                            table.addCell(cell_vacia);//se mete celda vacia

                            cell.setPhrase  ( new Phrase("          El codeudor.", fuente_normal ) );//celda con punto
                            table.addCell(cell);//se mete celda con punto

                            table.addCell(cell_vacia);//se mete celda vacia
                        }else{
                            ecc_codeudor=ecc_deudor;
                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);
                        }

                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                         table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia

                        table.addCell(cell_vacia);//se mete celda vacia

                        cell.setPhrase  ( new Phrase(firma,fuente_normal ) );//celda con texto
                        table.addCell(cell);//se mete celda con texto

                        table.addCell(cell_vacia);//se mete celda vacia

                        table.addCell(cell_vacia3);//se mete celda vacia

                        table.addCell(cell_vacia);//se mete celda vacia

                        frase =  new Phrase("");
                        frase.add(  new Phrase("C.C            ",fuente_normal ));
                        frase.add(  new Phrase(ecc_codeudor, fuente_negrita8));
                        cell.setPhrase  ( frase);

                        //cell.setPhrase  ( new Phrase("C.C            "+ecc_codeudor, fuente_normal ) );//celda con texto

                        table.addCell(cell);//se mete celda con texto

                        table.addCell(cell_vacia);//se mete celda vacia

                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia

                        table.addCell(cell_vacia);//se mete celda vacia
                        cell.setPhrase  ( new Phrase(""+pie_endoso,fuente_normal ) );//celda con texto
                        table.addCell(cell);//se mete celda con texto
                        table.addCell(cell_vacia);//se mete celda vacia

                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple

                        document.add(table);

                    }
                    contador=contador+1;

                }

                document.newPage();
            }
        }catch (Exception ex){
            System.out.println("ex"+ex.toString()+"__"+ex.getMessage());
            ex.printStackTrace();
        }
    }


    public void imprimirDetalleCompraCartera (ArrayList detalle) throws Exception{
        iniciar();

        try{
            this.escribirDetalleCompraCartera( detalle);
            document.close();
            //.out.println("TERMINO pdf");
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void escribirDetalleCompraCartera(ArrayList detalle) throws Exception{
        //.out.println("escribirDetalleCompraCartera en pdfimprimirsvc");
        int d=3;//cantidad de decimales
        try {
            float[] anchos = {1f,1f,1f,1f,1f,1f,1f,1f,1f,1f,1f,1f};//porcentajes de ancho de columnas de una tabla
            document.setPageSize(new Rectangle(850,500));
            document.newPage();//nueva pag

            //.out.println("escribir en pdf");

            int contadordetalle=0;

            PdfPCell cell_vacia3    = new PdfPCell();//se crea una celda vacia triple
            cell_vacia3.setColspan(12);//triple
            cell_vacia3.setBorderWidthTop(0);//borde
            cell_vacia3.setBorderWidthBottom(0);//borde
            cell_vacia3.setBorderWidthLeft(0);//borde
            cell_vacia3.setBorderWidthRight(0);//borde
            cell_vacia3.setPhrase  ( new Phrase("", fuente_normal ) );//celda triple vacia

            PdfPTable table = new PdfPTable(anchos);//se crea una tabla de 6 columnas
            table.setWidthPercentage(100);//uso de 100% horizontal
            table.addCell(cell_vacia3);//se mete la celda vacia triple

            PdfPCell cell    = new PdfPCell();//se crea una celda
            cell.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );//alineamiento horizontal
            cell.setColspan(12);//triple
            cell.setBorderWidthTop(0);//borde
            cell.setBorderWidthBottom(0);//borde
            cell.setBorderWidthLeft(0);//borde
            cell.setBorderWidthRight(0);//borde
            cell.setPhrase  ( new Phrase("C�digo de negocio : "+((chequeCartera)detalle.get(0)).getCodigoNegocio(), fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda
            cell.setPhrase  ( new Phrase("Fecha de desembolso : "+((chequeCartera)detalle.get(0)).getFechaDesembolso().substring(0,10), fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda
            cell.setPhrase  ( new Phrase("Afiliado : "+((chequeCartera)detalle.get(0)).getNombreProveedor(), fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda
            cell.setPhrase  ( new Phrase("Cliente : "+((chequeCartera)detalle.get(0)).getNombreNit(), fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda

            String modremes=  ((chequeCartera)detalle.get(0)).getModRemesa();
            String modavalx=  ((chequeCartera)detalle.get(0)).getModAval();

            /*
            if (modremes.equals("0")){
                modremes="Cliente";
            }
            if (modremes.equals("1")){
                modremes="Establecimiento";
            }
            if (modremes.equals("2")){
                modremes="Sin remesa";
            }
            */

            if (modremes.equals("0")){
                if (modavalx.equals("0")){
                    modremes="Establecimiento";
                }
                if (modavalx.equals("1")){
                    modremes="Cliente";
		}
            }

            if (modremes.equals("1")){
                modremes="Sin remesa";
            }

            //.out.println("escribir en pdf");
            cell.setPhrase  ( new Phrase("Modalidad de remesa : "+modremes, fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda

            String modcustodi=  ((chequeCartera)detalle.get(0)).getModCustodia();
            if (modcustodi.equals("1")){
                modcustodi="Cliente";
            }
            if (modcustodi.equals("0")){
                modcustodi="Establecimiento";
            }
            cell.setPhrase  ( new Phrase("Modalidad de custodia : "+modcustodi, fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda

            cell.setPhrase  ( new Phrase("Cantidad de documentos : "+detalle.size(), fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda


            String tipodocx=((chequeCartera)detalle.get(0)).getTipoDoc();
            if (tipodocx!=null && tipodocx.equals("02")){
                tipodocx="Letra";
            }
            if (tipodocx!=null && tipodocx.equals("01")){
                tipodocx="Cheque";
            }
            cell.setPhrase  ( new Phrase("Tipo de documento : "+tipodocx, fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda

            chequeCartera chc;

            table.addCell(cell_vacia3);//se mete la celda vacia triple
            table.addCell(cell_vacia3);//se mete la celda vacia triple
            table.addCell(cell_vacia3);//se mete la celda vacia triple

            cell.setBorderWidthTop(1);//borde
            cell.setBorderWidthBottom(1);//borde
            cell.setBorderWidthLeft(1);//borde
            cell.setBorderWidthRight(0);//borde
            cell.setColspan(1);

            cell.setHorizontalAlignment( PdfPCell.ALIGN_CENTER );//alineamiento horizontal
            cell.setVerticalAlignment( PdfPCell.ALIGN_MIDDLE );//alineamiento horizontal

            cell.setPhrase  ( new Phrase("ITEM", fuente_normal ) );//celda
            //.out.println("escribir en pdf");
            table.addCell(cell);//se mete la celda

            cell.setBorderWidthTop(1);//borde
            cell.setBorderWidthBottom(1);//borde
            cell.setBorderWidthLeft(1);//borde
            cell.setBorderWidthRight(0);//borde

            cell.setPhrase  ( new Phrase("FECHA DOCUMENTO", fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda

            cell.setPhrase  ( new Phrase("FECHA CONSIGNAR", fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda
            cell.setPhrase  ( new Phrase("VALOR", fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda
            cell.setPhrase  ( new Phrase("PORTE", fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda
            cell.setPhrase  ( new Phrase("DIAS", fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda
            cell.setPhrase  ( new Phrase("FACTOR", fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda
            cell.setPhrase  ( new Phrase("REMESA", fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda
            cell.setPhrase  ( new Phrase("DESCUENTO", fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda
            cell.setPhrase  ( new Phrase("VALOR CON DESCUENTO", fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda
            cell.setPhrase  ( new Phrase("CUSTODIA", fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda

            cell.setBorderWidthTop(1);//borde
            cell.setBorderWidthBottom(1);//borde
            cell.setBorderWidthLeft(1);//borde
            cell.setBorderWidthRight(1);//borde

            cell.setPhrase  ( new Phrase("VALOR A GIRAR", fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda

            double total_valorgirable=0.0;
            double total_valor_cheque=0.0;
            double total_descuento=0.0;
            double total_valor_cheque_sincustodia=0.0;
            double total_custodia=0.0;
            //double total_valor_sincustodia_custodia=0.0;
            //double total_remesamasporte=0.0;
            //double total_valorgirable=0.0;
            double total_remesa=0.0;
            double total_porte=0.0;

            //.out.println("escribir en pdf");
            while (contadordetalle<detalle.size()){

                if (contadordetalle==20 || contadordetalle==40 || contadordetalle==60){
                    document.add(table);
                    document.newPage();
                    table = new PdfPTable(anchos);//se crea una tabla de 6 columnas
                    table.setWidthPercentage(100);//uso de 100% horizontal
                    table.addCell(cell_vacia3);//se mete la celda vacia triple
                }

                //.out.println("escribir en pdf");
                chc=(chequeCartera)detalle.get(contadordetalle);

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde


                cell.setPhrase  ( new Phrase(""+chc.getItem(), fuente_normal ) );//celda
                table.addCell(cell);//se mete la celda

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde

                cell.setPhrase  ( new Phrase(""+chc.getFechaCheque().substring(0,10), fuente_normal ) );//celda
                table.addCell(cell);//se mete la celda

                cell.setPhrase  ( new Phrase(""+chc.getFechaConsignacion().substring(0,10), fuente_normal ) );//celda
                table.addCell(cell);//se mete la celda

                cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(Double.parseDouble(chc.getValor()),d)), fuente_normal ) );//celda
                table.addCell(cell);//se mete la celda
                total_valor_cheque=total_valor_cheque+Double.parseDouble(chc.getValor());

                cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(Double.parseDouble(chc.getPorte()),d)), fuente_normal ) );//celda
                table.addCell(cell);//se mete la celda
                total_porte=total_porte+Double.parseDouble(chc.getPorte());

                cell.setPhrase  ( new Phrase(""+chc.getDias(), fuente_normal ) );//celda
                table.addCell(cell);//se mete la celda

                cell.setPhrase  ( new Phrase(""+(Util.roundByDecimal(Double.parseDouble(chc.getFactor()),5)), fuente_normal ) );//celda
                table.addCell(cell);//se mete la celda

                cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(Double.parseDouble(chc.getRemesa()),d)), fuente_normal ) );//celda
                table.addCell(cell);//se mete la celda
                total_remesa=total_remesa+Double.parseDouble(chc.getRemesa());

                cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(Double.parseDouble(chc.getDescuento()),d)), fuente_normal ) );//celda
                table.addCell(cell);//se mete la celda
                total_descuento=total_descuento+Double.parseDouble(chc.getDescuento());

                cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(Double.parseDouble(chc.getValorSinCustodia()),d)), fuente_normal ) );//celda
                table.addCell(cell);//se mete la celda
                total_valor_cheque_sincustodia=total_valor_cheque_sincustodia+Double.parseDouble(chc.getValorSinCustodia());

                cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(Double.parseDouble(chc.getCustodia()),d)), fuente_normal ) );//celda
                table.addCell(cell);//se mete la celda
                total_custodia=total_custodia+Double.parseDouble(chc.getCustodia());

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(1);//borde

                cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(Double.parseDouble(chc.getValorGirable()),d)), fuente_normal ) );//celda
                table.addCell(cell);//se mete la celda
                total_valorgirable=total_valorgirable+Double.parseDouble(chc.getValorGirable());

                contadordetalle=contadordetalle+1;

            }

            cell.setColspan(12);
            cell.setBorderWidthTop(0);//borde
            cell.setBorderWidthBottom(1);//borde
            cell.setBorderWidthLeft(1);//borde
            cell.setBorderWidthRight(1);//borde

            cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda

            //contadordetalle=contadordetalle+1;

            cell.setBorderWidthTop(0);//borde
            cell.setBorderWidthBottom(0);//borde
            cell.setBorderWidthLeft(0);//borde
            cell.setBorderWidthRight(0);//borde

            cell.setColspan(3);
            table.addCell(cell);//se mete la celda

            cell.setColspan(1);
            cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(total_valor_cheque,d)), fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda

            cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(total_porte,d)), fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda

            cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda
            table.addCell(cell);//se mete la celda

            cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(total_remesa,d)), fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda

            cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(total_descuento,d)), fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda

            cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(total_valor_cheque_sincustodia,d)), fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda

            cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(total_custodia,d)), fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda

            cell.setColspan(1);
            cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(total_valorgirable,d)), fuente_normal ) );//celda
            table.addCell(cell);//se mete la celda

            document.add(table);
            document.newPage();
            //.out.println("fin de escribir en pdf");
        }catch (Exception ex){
            System.out.println("ex"+ex.toString()+"__"+ex.getMessage());
            ex.printStackTrace();
        }

    }

    /*
     *jemartinez
     */
    public void setVariablesReporteFenalco(BeanGeneral beanGeneral){
        //Aniadir las 12 variables del reporte
        nit = beanGeneral.getValor_01();
        nombre_cliente = beanGeneral.getValor_02();
        direccion = beanGeneral.getValor_03();
        telefono = beanGeneral.getValor_04();
        telcontacto = beanGeneral.getValor_05();
        aval = beanGeneral.getValor_07();

        //Si es una letra
        if(beanGeneral.getValor_06().equals("170")){
            banco = "99";
            cuenta_cheque = "0";
        }else
            banco = beanGeneral.getValor_06();

        if(beanGeneral.getValor_08()==null)
            cuenta_cheque="0";
        else
            cuenta_cheque=beanGeneral.getValor_08();

        beneficiario  =beanGeneral.getValor_09();
        numero_cheque  = beanGeneral.getValor_10();
        numero_factura  = beanGeneral.getValor_11();
        valor  = beanGeneral.getValor_12();

        if(beanGeneral.getV1()==null){
            observaciones = new String[1];
            observaciones[1] = "No existen observaciones";
        }else{
            observaciones = beanGeneral.getV1();
        }
    }

    /**
     * Metodo que genera el reporte que sera enviado a fenalco
     * @param Clase Model
     * @throws Exception si ocurre una exception en la creacion del documento
     * @author jemartinez
     */
    public void reporteChequesDevueltosFenalco(Model model, String numero, String usuario) throws Exception{
        iniciar(numero,usuario);//el document se crea en este metodo
        this.modelito = model;
        try{
            this.obtenterReporteChequesDevueltosFenalco();
            document.close();
            //.out.print("Done!!");
        }catch(Exception e){
            throw new Exception("Error: "+e.getMessage());
        }
    }

    /*
     *jemartinez
     */
    private String getMonth(int month){
        String monthWord  ="";
        if(month == 0) monthWord = "Enero";
        if(month == 1) monthWord = "Febrero";
        if(month == 2) monthWord = "Marzo";
        if(month == 3) monthWord = "Abril";
        if(month == 4) monthWord = "Mayo";
        if(month == 5) monthWord = "Junio";
        if(month == 6) monthWord = "Julio";
        if(month == 7) monthWord = "Agosto";
        if(month == 8) monthWord = "Septiembre";
        if(month == 9) monthWord = "Octubre";
        if(month == 10) monthWord = "Noviembre";
        if(month == 11) monthWord = "Diciembre";
        return monthWord;
    }

    /*
     * jemartinez
     */
    private void obtenterReporteChequesDevueltosFenalco(){

        try {

            //for(int i=0;i<2;i++){//j9

                int cols=2;//cantidad de filas de 1 tabla

                PdfPTable tableImage = new PdfPTable(5);//Contendra la imagen
                tableImage.setWidthPercentage(100);

                PdfPTable tableTop = new PdfPTable(1);//Cabecera
                tableTop.setWidthPercentage(100);

                PdfPTable table = new PdfPTable(cols);//se crea una tabla de 6 columnas
                table.setWidthPercentage(100);//uso de 100% horizontal

                PdfPCell cell = new PdfPCell();//se crea una celda

                document.newPage();//nueva pag

                //--------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(1);
                cell.setBorderWidthLeft(1);
                cell.setBorderWidthRight(1);
                cell.setBorderWidthBottom(0);
                cell.setPhrase(new Phrase("REPORTE CHEQUES DEVUELTOS",this.fuente_negrita_grande));
                cell.setColspan(2);
                cell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tableTop.addCell(cell);
                document.add(tableTop);
                //---------------------------------------------------------------------------------------------------

                cell.setBorderWidthTop(0);
                cell.setBorderWidthBottom(0);
                cell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                cell.setColspan(1);
                cell.setBorderWidthLeft(1);
                cell.setBorderWidthRight(0);

                cell.setPhrase(new Phrase("",this.fuente_normal));
                tableImage.addCell(cell);

                cell.setBorderWidthLeft(0);
                cell.setPhrase(new Phrase("",this.fuente_normal));
                tableImage.addCell(cell);

                rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                Image jpg = Image.getInstance(rb.getString("ruta")+"/images/fen.jpg");//se crea una imagen
                //Image jpg = Image.getInstance("C:/Tomcat5/webapps/Fintra/images/fen.jpg");//se crea una imagen

                jpg.scaleAbsolute(35,25);//se reduce la imagen
                cell.setImage(jpg);//se mete la imagen en la celda
                tableImage.addCell(cell);

                cell.setPhrase(new Phrase("",this.fuente_normal));
                tableImage.addCell(cell);

                cell.setPhrase(new Phrase("",this.fuente_normal));
                cell.setBorderWidthRight(1);//borde
                tableImage.addCell(cell);

                document.add(tableImage);
                //-----------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(1);//borde

                Calendar cal = Calendar.getInstance();
                cell.setPhrase  ( new Phrase("FENALSISTEMAS", this.fuente_negrita_grande));
                cell.setColspan(2);
                table.addCell(cell);
                cell.setPhrase  ( new Phrase("", this.fuente_negrita_grande));
                table.addCell(cell);
                table.addCell(cell);
                table.addCell(cell);
                table.addCell(cell);
                table.addCell(cell);

                cell.setBorderWidthRight(0);//borde
                cell.setColspan(1);
                cell.setPhrase  ( new Phrase("FECHA: "+cal.get(Calendar.DAY_OF_MONTH)+" de "+getMonth(cal.get(Calendar.MONTH))+" de "+cal.get(Calendar.YEAR), this.fuente_negrita_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------

                cell.setColspan(1);
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase("CODIGO: ", fuente_negrita_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);


                cell.setBorderWidthLeft(1);
                cell.setColspan(2);
                cell.setPhrase  ( new Phrase("", this.fuente_negrita_grande));
                table.addCell(cell);
                table.addCell(cell);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------

                cell.setColspan(1);
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                cell.setPhrase  ( new Phrase("NOMBRE DEL AFILIADO: ", fuente_negrita_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);//se mete celda vacia

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase("Fintra S.A. ", fuente_normal_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setColspan(2);
                cell.setBorderWidthLeft(1);
                cell.setPhrase  ( new Phrase("", this.fuente_negrita_grande));
                table.addCell(cell);
                table.addCell(cell);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------

                cell.setColspan(1);
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                cell.setPhrase  ( new Phrase("NIT: ", fuente_negrita_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);//se mete celda vacia

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase("802.022.016-1", fuente_normal_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setColspan(2);
                cell.setBorderWidthLeft(1);
                cell.setPhrase  ( new Phrase("", this.fuente_negrita_grande));
                table.addCell(cell);
                table.addCell(cell);
                table.addCell(cell);
                table.addCell(cell);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setColspan(2);
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda
                table.addCell(cell);//se mete celda vacia
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda
                table.addCell(cell);//se mete celda vacia
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda
                table.addCell(cell);//se mete celda vacia

                cell.setPhrase  ( new Phrase("DATOS DEL GIRADOR ", this.fuente_negrita_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);//se mete celda vacia
                //------------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                cell.setColspan(1);
                cell.setPhrase  ( new Phrase("NUMERO DEL DOCUMENTO: ", fuente_normal_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);//se mete celda vacia

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase(this.nit, fuente_normal_media ) );//nit del cliente
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda
                cell.setBorderWidthLeft(1);
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                cell.setColspan(1);
                cell.setPhrase ( new Phrase("NOMBRE COMPLETO: ", fuente_normal_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);//se mete celda vacia

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase(this.nombre_cliente, fuente_normal_media ) );//nit del cliente
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setBorderWidthLeft(1);
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                cell.setColspan(1);
                cell.setPhrase  ( new Phrase("DIRECCION: ", fuente_normal_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);//se mete celda vacia

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase(this.direccion,fuente_normal_media ) );//nit del cliente
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);


                cell.setBorderWidthLeft(1);
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                cell.setColspan(1);
                cell.setPhrase  ( new Phrase("TELEFONOS: ", fuente_normal_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);//se mete celda vacia

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase(this.telefono+"-"+this.telcontacto, fuente_normal_media ) );//nit del cliente
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setBorderWidthLeft(1);
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia

                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setColspan(2);
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda
                table.addCell(cell);//se mete celda vacia
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda
                table.addCell(cell);//se mete celda vacia
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda
                table.addCell(cell);//se mete celda vacia

                cell.setPhrase  ( new Phrase("DATOS DEL CHEQUE ", this.fuente_negrita_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                cell.setColspan(1);
                cell.setPhrase  ( new Phrase("CODIGO DEL BANCO:\t"+this.banco, fuente_normal_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);//se mete celda vacia

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase("No. DE LA CUENTA:\t"+this.cuenta_cheque,fuente_normal_media ) );//nit del cliente
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setBorderWidthLeft(1);
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                cell.setColspan(1);
                cell.setPhrase  ( new Phrase("NUMERO DEL CHEQUE:       "+this.numero_cheque, fuente_normal_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);//se mete celda vacia

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase("VALOR:        $ "+Util.customFormat(Double.parseDouble(this.valor)),fuente_normal_media ) );//nit del cliente
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setBorderWidthLeft(1);
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                cell.setColspan(1);
                cell.setPhrase  ( new Phrase("NUMERO DE LA CONSULTA:       "+this.aval, fuente_normal_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);//se mete celda vacia

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase("",fuente_normal ) );//nit del cliente
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setBorderWidthLeft(1);
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                cell.setColspan(1);
                cell.setPhrase  ( new Phrase("BENEFICIARIO: ", fuente_normal_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);//se mete celda vacia

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase(this.beneficiario,fuente_normal_media ) );//nit del cliente
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setBorderWidthLeft(1);
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                cell.setColspan(1);
                cell.setPhrase  ( new Phrase("NOMBRE OPERADOR: ", fuente_normal_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);//se mete celda vacia

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase("_____________________________",fuente_normal_media ) );//nit del cliente
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setBorderWidthLeft(1);
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);
                table.addCell(cell);
                table.addCell(cell);
                table.addCell(cell);
                //------------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                cell.setColspan(1);
                cell.setPhrase  ( new Phrase("FIRMA Y SELLO DEL AFILIADO: ",this.fuente_negrita_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);//se mete celda vacia

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase("_____________________________",fuente_normal_media ) );//nit del cliente
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setBorderWidthLeft(1);
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);
                table.addCell(cell);
                table.addCell(cell);
                table.addCell(cell);

                cell.setBorderWidthRight(1);
                cell.setBorderWidthBottom(1);
                table.addCell(cell);

                document.add(table);//se mete la tabla en el documento

            //}//j9

            //---------------observaciones------------------------------------//
            /*document.newPage();//Crea una nueva pagina//j9

            PdfPTable tableObservaciones = new PdfPTable(1);//Contendra las observaciones
            tableObservaciones.setWidthPercentage(100);
            PdfPCell cell = new PdfPCell();//se crea una celda

            for(int i=0;i<this.observaciones.length;i++){
                String[] temp = observaciones[i].split(";");

                    cell.setBorderWidthTop(1);
                    cell.setBorderWidthLeft(1);
                    cell.setBorderWidthRight(1);
                    cell.setBorderWidthBottom(0);

                if(temp.length<4){

                    cell.setPhrase(new Phrase(observaciones[i],fuente_normal_media));
                    cell.setBorderWidthBottom(1);
                    tableObservaciones.addCell(cell);
                }else{

                    cell.setPhrase(new Phrase("     Observacion: "+temp[1]+"\n     Fecha: "+temp[2]+"\n     Usuario: "+temp[3],fuente_normal_media));
                    tableObservaciones.addCell(cell);

                    cell.setBorderWidthTop(0);
                    cell.setBorderWidthLeft(0);
                    cell.setBorderWidthRight(0);
                    cell.setBorderWidthBottom(0);
                    cell.setPhrase(new Phrase("",fuente_normal));
                    tableObservaciones.addCell(cell);
                    tableObservaciones.addCell(cell);
                }
            }

            document.add(tableObservaciones);*///j9

        }catch (Exception ex){
            System.out.println("ex"+ex.toString()+"__"+ex.getMessage());
            ex.printStackTrace();
        }
    }

        public void generarPdfPagare(ArrayList datos)
    {
        String numpagarex="numpagarex"    ;
        String ciudadpagare="ciudadpagare";
        String fechanegocio="2008-10-14";//fecha
        Calendar temcal = Calendar.getInstance();
        temcal.set(Integer.parseInt(fechanegocio.substring(0,4)),Integer.parseInt(fechanegocio.substring(5,7))-1,Integer.parseInt(fechanegocio.substring(8,10)));
        String fechanegocioletra=Util.ObtenerFechaCompleta(temcal)+"";
        temcal=null;
        String valornegocio="1234567.89";
        valornegocio=String.valueOf(Util.customFormat(Double.parseDouble(valornegocio)));
        String ciudadpago="ciudadpago";
        String fechavencimiento="2008-10-11";
        temcal = Calendar.getInstance();
        temcal.set(Integer.parseInt(fechavencimiento.substring(0,4)),Integer.parseInt(fechavencimiento.substring(5,7))-1,Integer.parseInt(fechavencimiento.substring(8,10)));
        String fechavencimientoletra=Util.ObtenerFechaCompleta(temcal)+"";
        temcal=null;
        String tasita="2.3";//tasa
        String tasanominal=""+((Math.pow((1+((Double.parseDouble(tasita))/100.0)),12))-1)*100;
        tasanominal=tasanominal.substring(0,4);
        String nombredeudor="nombredeudor";
        String ciudadeudor="ciudadeudor";
        String iddeudor="iddeudor";
        String nombreempresa="nombreempresa";
        String nitempresa="nitempresa";
        String domicilioempresa="domicilioempresa";
        String nombreafiliado="nombreafiliado";
        String nitafiliado="nitafiliado";
        String domicilioafiliado="domicilioafiliado";
        String domiciliosecundario="domiciliosecundario";
        String valorenletras="valorenletras";
        String valornumero="valornumero";
        String nombredeudor2="nombredeudor2";//nombredeudor;
        String ccdeudor="ccdeudor";
        String nombrecodeudor="nombrecodeudor";
        String cccodeudor="cccodeudor";

     try
        {
            Font fuente_negrita_azul;
            fuente_negrita_azul = new Font(bf, 7F);
            fuente_negrita_azul.setStyle(1);
            fuente_negrita_azul.setColor(Color.BLUE);
            bf = BaseFont.createFont("Helvetica", "Cp1252", false);
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = (new StringBuilder()).append(rb.getString("ruta")).append("/pdf/pagare"+getPagareN()+".pdf").toString();

            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(ruta));
            com.lowagie.text.Rectangle page = document.getPageSize();

            document.open();
            //.out.println("antes de newpage");
            document.newPage();
            //.out.println("despues de newpage");
            PdfPTable table = new PdfPTable(1);
            //PdfPTable tablegrande = new PdfPTable(1);

            table.setWidthPercentage(100F);
            PdfPCell cell_superior_vacia = new PdfPCell();
            cell_superior_vacia.setColspan(1);
            cell_superior_vacia.setBorderWidthTop(1.0F);
            cell_superior_vacia.setBorderWidthBottom(0.0F);
            cell_superior_vacia.setBorderWidthLeft(1.0F);
            cell_superior_vacia.setBorderWidthRight(1.0F);
            cell_superior_vacia.setPhrase(new Phrase("",  fuente_normal));
            table.addCell(cell_superior_vacia);
            PdfPCell cell = new PdfPCell();
            PdfPCell cellrectangle = new PdfPCell();

            cell.setPaddingLeft(11);
            cell.setPaddingRight(11);

            cell.setColspan(1);
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(1.0F);
            cell.setHorizontalAlignment(1);
            cell.setPhrase(new Phrase("", fuente_negrita_azul));
            cell.setVerticalAlignment(5);
            table.addCell(cell);
            cell.setPhrase(new Phrase("", fuente_negrita_azul));
            table.addCell(cell);
            cell.setPhrase(new Phrase("PAGAR� N�MERO "+numpagarex+"", fuente_negrita8));
            table.addCell(cell);
            cell.setPhrase(new Phrase("", fuente_negrita_azul));
            table.addCell(cell);
            table.addCell(cell);
            cell.setHorizontalAlignment(cell.ALIGN_JUSTIFIED);
            String texto1="Lugar y fecha de suscripci�n del pagar�: "+ciudadpagare+", "+fechanegocioletra+"\n"+
                "Valor $ "+valornegocio+"(VALOR TOTAL DE LA OBLIGACION INCLUYENDO TODOS LOS CONCEPTOS) \n\n"+
                "Ciudad donde se efectuar� el pago: "+ciudadpago+", Colombia.\n"+
                "Fecha de Vencimiento: "+fechavencimientoletra+" \n"+
                "Tasa de inter�s Nominal Anual: "+tasanominal+"% \n"+
                "Yo (Nosotros) "+nombredeudor+" mayor de edad y vecino(a) de la ciudad de "+ciudadeudor+", identificado(a)" +
                " con la c�dula de ciudadan�a No. "+iddeudor+", obrando en nombre propio como girador y/o como codeudor"+
                " "+nombreempresa+", identificado(a) con la c�dula de ciudadan�a No."+nitempresa+", con domicilio principal en "+domicilioempresa+", me "+
                "(nos) obligo (obligamos) a pagar solidaria e incondicionalmente a "+nombreafiliado+" NIT. "+nitafiliado+", "+
                "con domicilio principal en "+domicilioafiliado+" (el \"ACREEDOR\"), o a su orden, en "+domiciliosecundario+", en dinero efectivo, la suma de "+valorenletras+" "+
                "("+valornumero+") por concepto de capital, m�s los intereses de plazo sobre el saldo o saldos pendientes de capital, que pagar� "+
                "(pagaremos) en cuotas mensuales de la siguiente manera: \n\n\n";

            String texto2="\n\n; Desde la fecha de la primera cuota y mensual consecutivamente en forma ininterrumpida y hasta el pago total de la obligaci�n."+
                "En caso de retardo en el pago de una o varias de las cuotas anteriores, pagar� (mos) intereses moratorios a la m�xima tasa autorizada por la ley comercial, as� como los gastos de cobranza, inclusive extrajudicial que ser�n de 20% del total adeudado al momento del pago y honorarios de gesti�n de cobro, tambi�n del 20% del total adeudado al momento del pago, que ser�n a mi cargo. Advierto (Advertimos) que siempre ser� de mi (nuestro) cargo el impuesto de timbre que se cause con este Pagar�. El ACREEDOR (o el tenedor leg�timo de este pagar�, si fuere el caso), podr� declarar insubsistentes los plazos de la obligaci�n incorporada en este pagar� o de cualquiera de las cuotas, que constituyen el saldo y exigir su pago total e inmediato, judicial o extrajudicialmente en los siguientes casos: a) Cuando el deudor incumpla una cualquiera de las obligaciones derivadas del y/o incorporadas en el presente pagar�; b) Cuando el deudor inicie tr�mite de liquidaci�n obligatoria, se sometan a procesos concursales o convoque a concurso de acreedores. c) En caso de muerte del codeudor o avalista; d) Mora en el pago de uno o m�s de los vencimientos se�alados, o de cualquier otra obligaci�n que directa o indirectamente tengan los deudores para con el ACREEDOR (o tenedor leg�timo de este pagar�, si fuere el caso); e) Cuando el deudor, los codeudores o avalistas sean demandados judicialmente por un tercero distinto al ACREEDOR (o del tenedor leg�timo del pagar�, si fuere el caso); (f) el giro de cheques sin provisi�n de fondos. El recibo de abonos parciales no implica novaci�n y cualquier pago que hiciere (mos) se imputar� primero a los gastos, despu�s a penas e intereses, y por ultimo a capital.  Declaro (mos) excusada la presentaci�n para el pago y la noticia de rechazo.  Los suscriptores de �ste pagar� inclusive el (los) avalista(s) hacen constar que la obligaci�n de pagarlo subsiste en caso de cualquier modificaci�n a lo estipulado aunque se pacte con uno s�lo de los suscriptores. Acepto (amos) que el pago total o parcial de los gastos, penas, intereses, como del capital de �ste titulo constaran en los registros sistematizados y comprobantes del ACREEDOR (o al tenedor leg�timo de este pagar�, si fuere el caso)."
               + "Declaro (mos) que renuncio (amos) a mi (nuestro) domicilio. "+
                    "En caso de retardo o incumplimiento, no ser� necesario el requerimiento previo para ser constituido en mora, al cual renuncia(n) desde ahora el/los deudor(es). El deudor (los deudores) de este pagar� autorizan de manera permanente e irrevocable con fines estad�sticos, de control, supervisi�n y de informaci�n comercial a reportar, verificar, procesar, consultar, conservar, suministrar, actualizar y divulgar a cualquiera de las centrales de informaci�n de Colombia autorizadas para tal efecto, nuestro comportamiento crediticio y en especial el eventual incumplimiento de la obligaci�n de este pagar�.  "+
                "\n\n\nEn constancia se firma:  \n\n";

            String texto22="\n\n"+
                           "________________________"+
                           "\nFIRMA"+
                           "\n"+nombredeudor2+""+
                           "\nCC. "+ccdeudor+"";

            String texto23y25=" \nHUELLA ";
            //com.lowagie.text.Rectangle rectanglex=new com.lowagie.text.Rectangle(1,2);

            //rectangle.set
            String texto24="\n\n"+
                            "________________________"+
                           "\nFIRMA"+
                           "\n"+nombrecodeudor+""+
                           "\nCC. "+cccodeudor+"";



            String texto3="\n\n\nEndosamos en propiedad el presente titulo valor a favor de Fintra S. A Nit 802.022.016-1. En consecuencia el pago del mismo debe hacerse directamente a favor de la Fiduciaria Corficolombiana S. A. Fideicomiso Fintra o a su orden en la fecha de su vencimiento en la cuenta corriente No. 80057617-5 del Banco de Occidente.";

            String texto4="\n\n___________________________\n"+
                "Firma\n" +
                "En nombre y representaci�n de (Nombre del Afiliado)\n\n"+
                "En senal de aceptaci�n de lo anterior\n\n"+
                "El deudor\n\n\n"+
                "_____________________________________\n"+
                "Firma \n"+
                "C.C. \n\n"+
                "El codeudor\n\n\n"+
                "__________________________________\n"+
                "Firma\n"+
                "C. C.\n\n"+
                "Fintra S. A. endosa en propiedad con responsabilidad a la Fiduciaria Corficolombiana S. A. Fideicomiso Fintra.";

            cell.setPhrase(new Phrase(texto1, fuente_normal));
            table.addCell(cell);
            document.add(table);

            //inicio de tabla
            table = new PdfPTable(5);
            table.setWidthPercentage(100F);
            //table.setHorizontalAlignment(table.ALIGN_CENTER);
            cell.setHorizontalAlignment(cell.ALIGN_CENTER);
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(0.0F);
            cell.setPhrase(new Phrase("", fuente_normal));
            table.addCell(cell);

            cell.setBorderWidthTop(1.0F);
            cell.setBorderWidthBottom(1.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(0.0F);
            //Phrase phrasex=new Phrase("CUOTA", fuente_normal);
            //phrasex.setFont(fuente_negrita);
            cell.setPhrase(new Phrase("CUOTA", fuente_negrita8));
            table.addCell(cell);
            cell.setPhrase(new Phrase("FECHA DE VENCIMIENTO", fuente_negrita8));
            table.addCell(cell);
            /////////////////////////////////////////////////////
            cell.setBorderWidthTop(1.0F);
            cell.setBorderWidthBottom(1.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(1.0F);
            cell.setPhrase(new Phrase("VALOR CAPITAL", fuente_negrita8));
            table.addCell(cell);

            cell.setBorderWidthTop(1.0F);
            cell.setBorderWidthBottom(1.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(1.0F);
            cell.setPhrase(new Phrase("VALOR INTERESES", fuente_negrita8));
            table.addCell(cell);
            /////////////////////////////////////////////////////


            cell.setBorderWidthTop(1.0F);
            cell.setBorderWidthBottom(1.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(1.0F);
            cell.setPhrase(new Phrase("VALOR DE LA CUOTA", fuente_negrita8));
            table.addCell(cell);

            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(0.0F);
            cell.setBorderWidthRight(1.0F);
            cell.setPhrase(new Phrase("", fuente_normal));
            table.addCell(cell);

            //inicio de ciclo
            String fechilla="";
            for (int i=0;i<datos.size();i++){
                chequeCartera chequeCarterax=(chequeCartera) datos.get(i);;

                cell.setBorderWidthTop(0.0F);
                cell.setBorderWidthBottom(0.0F);
                cell.setBorderWidthLeft(1.0F);
                cell.setBorderWidthRight(0.0F);
                cell.setPhrase(new Phrase("", fuente_normal));
                table.addCell(cell);

                cell.setBorderWidthTop(0.0F);
                cell.setBorderWidthBottom(1.0F);
                cell.setBorderWidthLeft(1.0F);
                cell.setBorderWidthRight(0.0F);
                cell.setPhrase(new Phrase(""+(i+1), fuente_normal));
                table.addCell(cell);
                temcal = Calendar.getInstance();
                temcal.set(Integer.parseInt(chequeCarterax.getFechaCheque().substring(0,4)),Integer.parseInt(chequeCarterax.getFechaCheque().substring(5,7))-1,Integer.parseInt(chequeCarterax.getFechaCheque().substring(8,10)));
		fechilla=Util.ObtenerFechaCompleta(temcal)+"";
                temcal=null;

                cell.setPhrase(new Phrase(""+fechilla, fuente_normal));
                table.addCell(cell);
                cell.setBorderWidthTop(0.0F);
                cell.setBorderWidthBottom(1.0F);
                cell.setBorderWidthLeft(1.0F);
                cell.setBorderWidthRight(1.0F);
                double valorcillo=Double.parseDouble(chequeCarterax.getValor());
                String valorcillox=String.valueOf(Util.customFormat(valorcillo));
                cell.setPhrase(new Phrase(""+valorcillox, fuente_normal));
                table.addCell(cell);

                cell.setBorderWidthTop(0.0F);
                cell.setBorderWidthBottom(0.0F);
                cell.setBorderWidthLeft(0.0F);
                cell.setBorderWidthRight(1.0F);
                cell.setPhrase(new Phrase("", fuente_normal));
                table.addCell(cell);
            }
            //fin de ciclo

            document.add(table);

            //fin de tabla

            table = new PdfPTable(1);
            table.setWidthPercentage(100F);
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(1.0F);
            cell.setHorizontalAlignment(cell.ALIGN_JUSTIFIED);
	    cell.setPhrase(new Phrase(texto2, fuente_normal));
            table.addCell(cell);
            document.add(table);

            table = new PdfPTable(6);
            table.setWidthPercentage(100F);
            cell.setHorizontalAlignment(cell.ALIGN_CENTER);

            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(0.0F);
            cell.setPhrase(new Phrase("", fuente_normal));
            //cell.setPhrase(new Phrase(texto22, fuente_normal));
            table.addCell(cell);//1
            cellrectangle.setBorderWidthTop(1.0F);
            cellrectangle.setBorderWidthBottom(1.0F);
            cellrectangle.setBorderWidthLeft(1.0F);
            cellrectangle.setBorderWidthRight(1.0F);
            cellrectangle.setPhrase(new Phrase("\n\n\n\n\n\n", fuente_normal));
            table.addCell(cellrectangle);//2
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(0.0F);
            cell.setBorderWidthRight(0.0F);
            cell.setPhrase(new Phrase("", fuente_normal));
            table.addCell(cell);//3
            table.addCell(cell);//4
            table.addCell(cellrectangle);//5
            cell.setPhrase(new Phrase("", fuente_normal));
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(0.0F);
            cell.setBorderWidthRight(1.0F);
            table.addCell(cell);//6

            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(0.0F);
            cell.setPhrase(new Phrase("", fuente_normal));
            table.addCell(cell); //1
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(0.0F);
            cell.setBorderWidthRight(0.0F);
            cell.setPhrase(new Phrase(texto23y25, fuente_normal));
            table.addCell(cell);//2
            cell.setPhrase(new Phrase("", fuente_normal));
            table.addCell(cell);//3
            table.addCell(cell);//4
            cell.setPhrase(new Phrase(texto23y25, fuente_normal));
            table.addCell(cell);//5
            cell.setPhrase(new Phrase("", fuente_normal));
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(0.0F);
            cell.setBorderWidthRight(1.0F);
            table.addCell(cell);//6

            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(0.0F);
            cell.setHorizontalAlignment(cell.ALIGN_LEFT);
            cell.setColspan(2);
            cell.setPhrase(new Phrase(texto22, fuente_normal));
            table.addCell(cell);   //1  y 2
            cell.setColspan(1);
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(0.0F);
            cell.setBorderWidthRight(0.0F);
            cell.setPhrase(new Phrase("     ", fuente_normal));
            table.addCell(cell);//3
            cell.setColspan(2);
            cell.setPhrase(new Phrase(texto24, fuente_normal));
            table.addCell(cell);//4 y 5
            cell.setColspan(1);
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(0.0F);
            cell.setBorderWidthRight(1.0F);
            cell.setPhrase(new Phrase("     ", fuente_normal));
            table.addCell(cell);//6

            document.add(table);


            table = new PdfPTable(1);
            table.setWidthPercentage(100F);
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(1.0F);


            cell.setHorizontalAlignment(cell.ALIGN_JUSTIFIED);
	    cell.setPhrase(new Phrase(texto3, fuente_negrita8));

            table.addCell(cell);

  	    cell.setPhrase(new Phrase(texto4, fuente_normal));
            table.addCell(cell);

            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(1.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(1.0F);
            cell.setPhrase(new Phrase("\n\n", fuente_normal));
            table.addCell(cell);

            document.add(table);
            //.out.println("despues de add table");
            document.newPage();
            //.out.println("antes de documento cerrado");
            document.close();
            //.out.println("documento cerrado");


     }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.println("errorcittooo generarPdfPagare___"+e.toString()+"__"+e.getMessage());
        }
    }

    public String getPagareN   (){
        return "n";
    }

    public void generarPdfPagare(ArrayList datos,BeanGeneral bg,String ciudadotorgamiento,
        String domiciliopago,String nombrecoodeudor,String idcoodeudor,String domi2,String domi_cood,String aprob
        ,String idendoso,String usuariox,String tipoid)
    {
        try{
        //.out.println("gpdfpag");
    Font fuente_normal2    = new Font(bf, 6);
    Font fuente_normal2_blanca    = new Font(bf, 6);
    fuente_normal2_blanca.setColor(Color.WHITE);
    Font fuente_negrita2= new Font(bf, 7);
    fuente_negrita2.setStyle( com.lowagie.text.Font.BOLD );

    String numdocs=bg.getValor_04();

    chequeCartera chequeCarterax1=(chequeCartera)datos.get(0);
    double valorcillo1=Math.round(Double.parseDouble(chequeCarterax1.getValor()));
    valorcillo1=valorcillo1*Double.parseDouble(numdocs);

    RMCantidadEnLetras c = new RMCantidadEnLetras();
    String[] a;
    a = c.getTexto(Double.valueOf(valorcillo1+"").doubleValue());
    String res = "";
    for(int i=0;i<a.length;i++){
     res = res + ((String)a[i]).replace("-"," ");
    }
    //.out.println("res"+res+"_");
    res=res.trim();
    //.out.println("res"+res+"_");
    bg.setValor_14(res);//Total a Pagar en Letras

    String dirafiliado=bg.getValor_12();
    String dirclient=bg.getValor_08();

    String numpagarex=bg.getValor_01();//"numpagarex"    ;
    String ciudadpagare=ciudadotorgamiento;//"ciudadpagare";
    String fechanegocio=bg.getValor_02();//"2008-10-14";//fecha
    Calendar temcal = Calendar.getInstance();
    //.out.println("fechanegocio:"+fechanegocio);
    temcal.set(Integer.parseInt(fechanegocio.substring(0,4)),Integer.parseInt(fechanegocio.substring(5,7))-1,Integer.parseInt(fechanegocio.substring(8,10)));
    String fechanegocioletra=Util.ObtenerFechaCompleta(temcal)+"";
    temcal=null;
    String valornegocio=valorcillo1+"";//bg.getValor_03();//"1234567.89";
    valornegocio=String.valueOf(Util.customFormat(Double.parseDouble(valornegocio)));
    String ciudadpago=domiciliopago;//"ciudadpago";
    String fechavencimiento=bg.getValor_15();//"2008-10-11";
    temcal = Calendar.getInstance();
    temcal.set(Integer.parseInt(fechavencimiento.substring(0,4)),Integer.parseInt(fechavencimiento.substring(5,7))-1,Integer.parseInt(fechavencimiento.substring(8,10)));
    String fechavencimientoletra=Util.ObtenerFechaCompleta(temcal)+"";
    temcal=null;
    String tasita=bg.getValor_05();//"2.3";//tasa
    //String tasanominal=""+((Math.pow((1.0+((Double.parseDouble(tasita))/100.0)),12.0))-1.0)*100.0;
    String tasanominal=""+(Double.parseDouble(tasita)*12.0);
    //.out.println("tasita"+tasita+"tasanominal"+tasanominal);

    if (tasanominal.length()>5){//20100701
        tasanominal=tasanominal.substring(0,5);
    }

    String nombredeudor=bg.getValor_06();//"nombredeudor";
    String ciudadeudor=bg.getValor_09();//"ciudadeudor";
    String iddeudor=bg.getValor_07();//"iddeudor";
    String nombreempresa=nombrecoodeudor;//"_________-nombreempresa";
    String nitempresa=idcoodeudor;//"________nitempresa";
    String domicilioempresa=domi_cood;//"___________domicilioempresa";
    String nombreafiliado=bg.getValor_10();//"nombreafiliado";
    String nitafiliado=bg.getValor_11();//"nitafiliado";
    String domicilioafiliado=bg.getValor_13();//"domicilioafiliado";
    String domiciliosecundario=domi2;//"domiciliosecundario";
    String valorenletras=bg.getValor_14();//"valorenletras";
    String valornumero=String.valueOf(Util.customFormat(Double.parseDouble(valorcillo1+"")));//+"";//"_________valornumero";
    String nombredeudor2=nombredeudor;//"nombredeudor2";//nombredeudor;
    String ccdeudor=iddeudor;//"ccdeudor";
    String nombrecodeudor=nombrecoodeudor;//"nombrecodeudor";
    String cccodeudor=idcoodeudor;//"cccodeudor";

     try
        {
            /*Font fuente_negrita_azul;
            fuente_negrita_azul = new Font(bf, 7F);
            fuente_negrita_azul.setStyle(1);
            fuente_negrita_azul.setColor(Color.BLUE);*/
            bf = BaseFont.createFont("Helvetica", "Cp1252", false);
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = (new StringBuilder()).append(rb.getString("ruta")).append("/pdf/pagare"+getPagareN()+".pdf").toString();

            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(ruta));
            com.lowagie.text.Rectangle page = document.getPageSize();

            document.open();
            //.out.println("antes de newpage");
            document.newPage();
            //.out.println("despues de newpage");
            PdfPTable table = new PdfPTable(1);
            //PdfPTable tablegrande = new PdfPTable(1);

            table.setWidthPercentage(100F);
            PdfPCell cell_superior_vacia = new PdfPCell();
            cell_superior_vacia.setColspan(1);
            cell_superior_vacia.setBorderWidthTop(1.0F);
            cell_superior_vacia.setBorderWidthBottom(0.0F);
            cell_superior_vacia.setBorderWidthLeft(1.0F);
            cell_superior_vacia.setBorderWidthRight(1.0F);
            cell_superior_vacia.setPhrase(new Phrase("", fuente_normal2));
            table.addCell(cell_superior_vacia);
            PdfPCell cell = new PdfPCell();
            PdfPCell cellrectangle = new PdfPCell();

            cell.setPaddingLeft(11);
            cell.setPaddingRight(11);

            cell.setColspan(1);
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(1.0F);
            cell.setHorizontalAlignment(1);
            cell.setPhrase(new Phrase("", fuente_negrita2));
            cell.setVerticalAlignment(5);
            table.addCell(cell);
            cell.setPhrase(new Phrase("", fuente_negrita2));
            table.addCell(cell);
            cell.setPhrase(new Phrase("PAGAR� N�MERO "+aprob+"", fuente_negrita2));
            table.addCell(cell);
            cell.setPhrase(new Phrase("", fuente_negrita2));
            table.addCell(cell);
            table.addCell(cell);
            cell.setHorizontalAlignment(cell.ALIGN_JUSTIFIED);

            Phrase respuesta =  new Phrase("");
            respuesta.add(  new Phrase("Lugar y fecha de suscripci�n del pagar�: ", fuente_normal2));
            respuesta.add(  new Phrase(ciudadpagare, fuente_negrita2));
            respuesta.add(  new Phrase(", ", fuente_normal2));
            respuesta.add(  new Phrase(fechanegocioletra, fuente_negrita2));
            respuesta.add(  new Phrase("\nValor $ ", fuente_normal2));
            respuesta.add(  new Phrase(valornegocio, fuente_negrita2));
            respuesta.add(  new Phrase(" \n\nCiudad donde se efectuar� el pago: ", fuente_normal2));
            respuesta.add(  new Phrase(ciudadpago, fuente_negrita2));
            respuesta.add(  new Phrase(", Colombia.\nFecha de Vencimiento: ", fuente_normal2));
            //respuesta.add(  new Phrase(fechavencimientoletra, fuente_negrita2));//20100412

            //inicio de 20100412
            Calendar fecvencefinal = Calendar.getInstance();
            chequeCartera chequeFinalx=(chequeCartera)datos.get(datos.size()-1);
            fecvencefinal.set(Integer.parseInt(chequeFinalx.getFechaCheque().substring(0,4)),Integer.parseInt(chequeFinalx.getFechaCheque().substring(5,7))-1,Integer.parseInt(chequeFinalx.getFechaCheque().substring(8,10)));
            respuesta.add(  new Phrase(Util.ObtenerFechaCompleta(fecvencefinal), fuente_negrita2));
            //inicio de 20100412

            respuesta.add(  new Phrase(" \nTasa de inter�s Nominal Anual: ", fuente_normal2));
            respuesta.add(  new Phrase(tasanominal, fuente_negrita2));
            respuesta.add(  new Phrase("% \nYo (Nosotros) ", fuente_normal2));
            respuesta.add(  new Phrase(nombredeudor, fuente_negrita2));
            respuesta.add(  new Phrase(" mayor de edad y vecino(a) de la ciudad de ", fuente_normal2));
            respuesta.add(  new Phrase(ciudadeudor, fuente_negrita2));
            respuesta.add(  new Phrase(", identificado(a) con la c�dula de ciudadan�a No. ", fuente_normal2));
            respuesta.add(  new Phrase(iddeudor, fuente_negrita2));
            respuesta.add(  new Phrase(", obrando en nombre propio como girador",fuente_normal2));//
            if (nombreempresa!=null && !(nombreempresa.equals(""))){
                respuesta.add(  new Phrase(" y/o como codeudor "+((!tipoid.equals("CED"))?"de la empresa ":""),fuente_normal2));//            //y/o como codeudor de la empresa ", fuente_normal2));
                respuesta.add(  new Phrase(nombreempresa, fuente_negrita2));
                respuesta.add(  new Phrase(", ", fuente_normal2));
                respuesta.add(  new Phrase("NIT. "+nitempresa, fuente_negrita2));
                respuesta.add(  new Phrase(", con domicilio principal en ", fuente_normal2));
                respuesta.add(  new Phrase(domicilioempresa, fuente_negrita2));
            }
            respuesta.add(  new Phrase(", me (nos) obligo (obligamos) a pagar solidaria e incondicionalmente a ", fuente_normal2));//", fuente_normal2));
            respuesta.add(  new Phrase(nombreafiliado, fuente_negrita2));
            respuesta.add(  new Phrase( " NIT. ", fuente_normal2));
            //respuesta.add(  new Phrase(bg.getValor_20(), fuente_negrita2));
            respuesta.add(  new Phrase(nitafiliado, fuente_negrita2));//090429

            respuesta.add(  new Phrase(", con domicilio principal en ", fuente_normal2));
            respuesta.add(  new Phrase(domicilioafiliado, fuente_negrita2));
            respuesta.add(  new Phrase(" (el \"ACREEDOR\"), o a su orden, en ", fuente_normal2));
            respuesta.add(  new Phrase(domiciliosecundario, fuente_negrita2));
            respuesta.add(  new Phrase(", en dinero efectivo, la suma de ", fuente_normal2));
            respuesta.add(  new Phrase(valorenletras, fuente_negrita2));
            respuesta.add(  new Phrase(" ($", fuente_normal2));
            respuesta.add(  new Phrase(valornumero, fuente_negrita2));
            respuesta.add(  new Phrase(") por concepto de capital, m�s los intereses de plazo sobre el saldo o saldos pendientes de capital, que pagar� ", fuente_normal2));
            respuesta.add(  new Phrase("(pagaremos) en cuotas mensuales de la siguiente manera: \n\n\n", fuente_normal2));
            /*String texto1="Lugar y fecha de suscripci�n del pagar�: "+ciudadpagare+", "+fechanegocioletra+"\n"+
                "Valor $ "+valornegocio+" \n\n"+
                "Ciudad donde se efectuar� el pago: "+ciudadpago+", Colombia.\n"+
                "Fecha de Vencimiento: "+fechavencimientoletra+" \n"+
                "Tasa de inter�s Nominal Anual: "+tasanominal+"% \n"+
                "Yo (Nosotros) "+nombredeudor+" mayor de edad y vecino(a) de la ciudad de "+ciudadeudor+", identificado(a)" +
                " con la c�dula de ciudadan�a No. "+iddeudor+", obrando en nombre propio como girador y/o como codeudor de la empresa"+
                " "+nombreempresa+", NIT. "+nitempresa+", con domicilio principal en "+domicilioempresa+", me "+
                "(nos) obligo (obligamos) a pagar solidaria e incondicionalmente a "+nombreafiliado+" NIT. "+nitafiliado+", "+
                "con domicilio principal en "+domicilioafiliado+" (el \"ACREEDOR\"), o a su orden, en "+domiciliosecundario+", en dinero efectivo, la suma de "+valorenletras+" "+
                "($"+valornumero+") por concepto de capital, m�s los intereses de plazo sobre el saldo o saldos pendientes de capital, que pagar� "+
                "(pagaremos) en cuotas mensuales de la siguiente manera: \n\n\n";
            */

            String texto2="\n\n; Desde la fecha de la primera cuota y mensual consecutivamente en forma ininterrumpida y hasta el pago total de la obligaci�n."+
                "En caso de retardo en el pago de una o varias de las cuotas anteriores, pagar� (mos) intereses moratorios a la m�xima tasa autorizada por la ley comercial, as� como los gastos de cobranza, inclusive extrajudicial que ser�n de 20% del total adeudado al momento del pago y honorarios de gesti�n de cobro, tambi�n del 20% del total adeudado al momento del pago, que ser�n a mi cargo. Advierto (Advertimos) que siempre ser� de mi (nuestro) cargo el impuesto de timbre que se cause con este Pagar�. El ACREEDOR (o el tenedor leg�timo de este pagar�, si fuere el caso), podr� declarar insubsistentes los plazos de la obligaci�n incorporada en este pagar� o de cualquiera de las cuotas, que constituyen el saldo y exigir su pago total e inmediato, judicial o extrajudicialmente en los siguientes casos: a) Cuando el deudor incumpla una cualquiera de las obligaciones derivadas del y/o incorporadas en el presente pagar�; b) Cuando el deudor inicie tr�mite de liquidaci�n obligatoria, se sometan a procesos concursales o convoque a concurso de acreedores. c) En caso de muerte del codeudor o avalista; d) Mora en el pago de uno o m�s de los vencimientos se�alados, o de cualquier otra obligaci�n que directa o indirectamente tengan los deudores para con el ACREEDOR (o tenedor leg�timo de este pagar�, si fuere el caso); e) Cuando el deudor, los codeudores o avalistas sean demandados judicialmente por un tercero distinto al ACREEDOR (o del tenedor leg�timo del pagar�, si fuere el caso); (f) el giro de cheques sin provisi�n de fondos. El recibo de abonos parciales no implica novaci�n y cualquier pago que hiciere (mos) se imputar� primero a los gastos, despu�s a penas e intereses, y por ultimo a capital.  Declaro (mos) excusada la presentaci�n para el pago y la noticia de rechazo.  Los suscriptores de �ste pagar� inclusive el (los) avalista(s) hacen constar que la obligaci�n de pagarlo subsiste en caso de cualquier modificaci�n a lo estipulado aunque se pacte con uno s�lo de los suscriptores. Acepto (amos) que el pago total o parcial de los gastos, penas, intereses, como del capital de �ste titulo constaran en los registros sistematizados y comprobantes del ACREEDOR (o al tenedor leg�timo de este pagar�, si fuere el caso)."
                    + "Declaro (mos) que renuncio (amos) a mi (nuestro) domicilio. "+
                "En caso de retardo o incumplimiento, no ser� necesario el requerimiento previo para ser constituido en mora, al cual renuncia(n) desde ahora el/los deudor(es). El deudor (los deudores) de este pagar� autorizan de manera permanente e irrevocable con fines estad�sticos, de control, supervisi�n y de informaci�n comercial a reportar, verificar, procesar, consultar, conservar, suministrar, actualizar y divulgar a cualquiera de las centrales de informaci�n de Colombia autorizadas para tal efecto, nuestro comportamiento crediticio y en especial el eventual incumplimiento de la obligaci�n de este pagar�.  "+
                "\n\n\nEn constancia se firma:  \n\n";

            /*String texto22="\n\n"+
                           "________________________"+
                           "\nFIRMA"+
                           "\n"+nombredeudor2+""+
                           "\nCC. "+ccdeudor+"";*/

            Phrase text22 =  new Phrase("");
            text22.add(  new Phrase("\n\n________________________\nFIRMA\n", fuente_normal2));
            text22.add(  new Phrase(nombredeudor2, fuente_negrita2));
            text22.add(  new Phrase("\nCC. ", fuente_normal2));
            text22.add(  new Phrase(ccdeudor, fuente_negrita2));


            String texto23y25=" \nHUELLA ";
            //com.lowagie.text.Rectangle rectanglex=new com.lowagie.text.Rectangle(1,2);

            /*String texto24="\n\n"+
                            "________________________"+
                           "\nFIRMA"+
                           "\n"+nombrecodeudor+""+
                           "\nCC. "+cccodeudor+"";*/
            if(idcoodeudor.equals("____________"))
            {   nombrecodeudor="";cccodeudor="";
            }
            Phrase text24 =  new Phrase("");
            text24.add(  new Phrase("\n\n________________________\nFIRMA\n", fuente_normal2));
            text24.add(  new Phrase(nombrecodeudor, fuente_negrita2));
            text24.add(  new Phrase("\nCC. ", fuente_normal2));
            text24.add(  new Phrase(cccodeudor, fuente_negrita2));


            String texto3="";
                    /*if(usuariox.equals("PROVINT"))
                    {   texto3="\n\n\nEndosamos en propiedad y sin responsabilidad el presente titulo valor a favor de PROVINTEGRAL LTDA Nit 900.074.295-8. En consecuencia el pago del mismo debe hacerse directamente a favor de la Fiduciaria Corficolombiana S. A. Fideicomiso Fintra o a su orden en la fecha de su vencimiento en la cuenta corriente No. 80057617-5 del Banco de Occidente.";
                    }
                    else{               */
                        texto3="\n\n\nEndosamos en propiedad y sin responsabilidad el presente titulo valor a favor de Fintra S. A Nit 802.022.016-1. En consecuencia el pago del mismo debe hacerse directamente a favor de la Fiduciaria Corficolombiana S. A. Fideicomiso Fintra o a su orden en la fecha de su vencimiento en la cuenta corriente No. 80057617-5 del Banco de Occidente.";
                   // }
            /*String texto4="\n\n___________________________\n"+
                "Firma\n" +
                "En nombre y representaci�n de "+nombreafiliado+"\n\n"+
                "En se�al de aceptaci�n de lo anterior\n\n"+
                "El deudor\n\n\n"+
                "_____________________________________\n"+
                "Firma \n"+
                "C.C. "+ccdeudor+"\n\n"+
                "El codeudor\n\n\n"+
                "__________________________________\n"+
                "Firma\n"+
                "C. C. "+cccodeudor+"\n\n"+
                "Fintra S. A. endosa en propiedad con responsabilidad a la Fiduciaria Corficolombiana S. A. Fideicomiso Fintra.";*/
            Phrase text4 =  new Phrase("");
            if (!(usuariox.equals("FINTRA") || usuariox.equals("fintra"))){
                text4.add(  new Phrase("\n\n___________________________\nFirma", fuente_normal2));
                text4.add(  new Phrase("\nCC. ", fuente_normal2));
                text4.add(  new Phrase(idendoso, fuente_negrita2));
                text4.add(  new Phrase("\nEn nombre y representaci�n de ", fuente_normal2));
                text4.add(  new Phrase(nombreafiliado, fuente_negrita2));

                text4.add(  new Phrase("\n\nEn se�al de aceptaci�n de lo anterior\n\nEl deudor\n\n\n_____________________________________\nFirma \nC.C. ", fuente_normal2));
                text4.add(  new Phrase(ccdeudor, fuente_negrita2));
                text4.add(  new Phrase("\n\nEl codeudor\n\n\n__________________________________\nFirma\nC. C. ", fuente_normal2));
                text4.add(  new Phrase(cccodeudor, fuente_negrita2));
            }

                text4.add(  new Phrase("\n\nFintra S. A. endosa en propiedad con responsabilidad a la Fiduciaria Corficolombiana S. A. Fideicomiso Fintravalores.", fuente_normal2));


            cell.setPhrase(respuesta);
            table.addCell(cell);
            document.add(table);

            //inicio de tabla
            float[] anchos = {0.5f,0.5f,1.0f,0.7f,0.7f,0.7f,0.8f,0.5f};//porcentajes de ancho de columnas de una tabla

            table = new PdfPTable(anchos);
            table.setWidthPercentage(100F);
            //table.setHorizontalAlignment(table.ALIGN_CENTER);
            cell.setHorizontalAlignment(cell.ALIGN_CENTER);
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(0.0F);
            cell.setPhrase(new Phrase("", fuente_normal2));
            table.addCell(cell);

            cell.setBorderWidthTop(1.0F);
            cell.setBorderWidthBottom(1.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(0.0F);
            //Phrase phrasex=new Phrase("CUOTA", fuente_normal2);
            //phrasex.setFont(fuente_negrita);
            cell.setPhrase(new Phrase("CUOTA", fuente_negrita2));
            table.addCell(cell);
            cell.setPhrase(new Phrase("FECHA DE VENCIMIENTO", fuente_negrita2));
            table.addCell(cell);

            ////////////////////////////////////////////////////////////////////
            cell.setBorderWidthTop(1.0F);
            cell.setBorderWidthBottom(1.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(0.0F);
            cell.setPhrase(new Phrase("VALOR CAPITAL", fuente_negrita2));
            table.addCell(cell);


            cell.setBorderWidthTop(1.0F);
            cell.setBorderWidthBottom(1.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(0.0F);
            cell.setPhrase(new Phrase("VALOR INTERES", fuente_negrita2));
            table.addCell(cell);


            cell.setBorderWidthTop(1.0F);
            cell.setBorderWidthBottom(1.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(0.0F);
            cell.setPhrase(new Phrase("VALOR AVAL", fuente_negrita2));
            table.addCell(cell);

            ////////////////////////////////////////////////////////////////////
            cell.setBorderWidthTop(1.0F);
            cell.setBorderWidthBottom(1.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(1.0F);
            cell.setPhrase(new Phrase("VALOR DE LA CUOTA", fuente_negrita2));
            table.addCell(cell);

            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(0.0F);
            cell.setBorderWidthRight(1.0F);
            cell.setPhrase(new Phrase("", fuente_normal2));
            table.addCell(cell);

            //inicio de ciclo
            String fechilla="";
            double valtot=0;
            double valtot1=0;
            double valtot2=0;
            double valtot3=0;
            chequeCartera chequeCarterax=(chequeCartera) datos.get(0);
            for (int i=0;i<60;i++){//datos.size()
                if (i<datos.size()){
                    chequeCarterax=(chequeCartera) datos.get(i);
                }
                cell.setBorderWidthTop(0.0F);
                cell.setBorderWidthBottom(0.0F);
                cell.setBorderWidthLeft(1.0F);
                cell.setBorderWidthRight(0.0F);
                cell.setPhrase(new Phrase("", fuente_normal2));
                table.addCell(cell);

                cell.setBorderWidthTop(0.0F);
                cell.setBorderWidthBottom(1.0F);
                cell.setBorderWidthLeft(1.0F);
                cell.setBorderWidthRight(0.0F);
                if (i<datos.size()){
                    cell.setPhrase(new Phrase(""+(i+1), fuente_normal2));
                }else{
                    cell.setPhrase(new Phrase("***", fuente_normal2));
                }
                table.addCell(cell);
                if (i<datos.size()){
                    temcal = Calendar.getInstance();
                    temcal.set(Integer.parseInt(chequeCarterax.getFechaCheque().substring(0,4)),Integer.parseInt(chequeCarterax.getFechaCheque().substring(5,7))-1,Integer.parseInt(chequeCarterax.getFechaCheque().substring(8,10)));
                    fechilla=Util.ObtenerFechaCompleta(temcal)+"";
                    temcal=null;
                    //fechilla=fechilla.replaceAll("de", "  de");

                    cell.setPhrase(new Phrase(""+fechilla+"", fuente_normal2));
                }else{
                    cell.setPhrase(new Phrase("***", fuente_normal2));
                }
                table.addCell(cell);
                //////////////////////////////////////
                cell.setBorderWidthTop(0.0F);
                cell.setBorderWidthBottom(1.0F);
                cell.setBorderWidthLeft(1.0F);
                cell.setBorderWidthRight(0.0F);
                double valorcillo11=0;
                if (i<datos.size()){
                    //valorcillo11=Double.parseDouble(chequeCarterax.getValor())-Double.parseDouble(chequeCarterax.getValorInteres())-Double.parseDouble(chequeCarterax.getNoAval());

                    valorcillo11=(Math.round((Double.parseDouble(chequeCarterax.getValor())))-(Math.round(Double.parseDouble(chequeCarterax.getValorInteres())))-Math.round(Double.parseDouble(chequeCarterax.getNoAval())));

                    String valorcillox1=String.valueOf(Util.customFormat(valorcillo11));
                    cell.setPhrase(new Phrase(""+valorcillox1, fuente_normal2));
                }else{
                    cell.setPhrase(new Phrase("***", fuente_normal2));
                }
                table.addCell(cell);

                valtot1=valtot1+Math.round(valorcillo11);

                cell.setBorderWidthTop(0.0F);
                cell.setBorderWidthBottom(1.0F);
                cell.setBorderWidthLeft(1.0F);
                cell.setBorderWidthRight(0.0F);
                double valorcillo12=0;
                if (i<datos.size()){
                    valorcillo12=Math.round(Double.parseDouble(chequeCarterax.getValorInteres()));//darrieta 25/03/2010
                    String valorcillox2=String.valueOf(Util.customFormat(valorcillo12));
                    cell.setPhrase(new Phrase(""+valorcillox2, fuente_normal2));
                }else{
                    cell.setPhrase(new Phrase("***", fuente_normal2));
                }
                table.addCell(cell);

                valtot2=valtot2+Math.round(valorcillo12);


                cell.setBorderWidthTop(0.0F);
                cell.setBorderWidthBottom(1.0F);
                cell.setBorderWidthLeft(1.0F);
                cell.setBorderWidthRight(0.0F);
                double valorcillo13=0;
                double vlrCuota = Math.round(Double.parseDouble(chequeCarterax.getValor())); //darrieta 25/03/2010
                if (i<datos.size()){
                    valorcillo13 = vlrCuota - valorcillo12 - valorcillo11; //vlr aval = vlr cuota - vlr interes - vlr capital
                    String valorcillox2=String.valueOf(Util.customFormat(valorcillo13));
                    cell.setPhrase(new Phrase(""+valorcillox2, fuente_normal2));
                }else{
                    cell.setPhrase(new Phrase("***", fuente_normal2));
                }
                table.addCell(cell);

                valtot3=valtot3+Math.round(valorcillo13);


                //////////////////////////////////////
                cell.setBorderWidthTop(0.0F);
                cell.setBorderWidthBottom(1.0F);
                cell.setBorderWidthLeft(1.0F);
                cell.setBorderWidthRight(1.0F);
                double valorcillo=0;
                if (i<datos.size()){
                    valorcillo = Math.round(Double.parseDouble(chequeCarterax.getValor())); //darrieta 25/03/2010
                    String valorcillox=String.valueOf(Util.customFormat(valorcillo));
                    cell.setPhrase(new Phrase(""+valorcillox, fuente_normal2));
                }else{
                    cell.setPhrase(new Phrase("***", fuente_normal2));
                }
                table.addCell(cell);
                //////////////////////////////////////
                cell.setBorderWidthTop(0.0F);
                cell.setBorderWidthBottom(0.0F);
                cell.setBorderWidthLeft(0.0F);
                cell.setBorderWidthRight(1.0F);
                cell.setPhrase(new Phrase("", fuente_normal2));
                table.addCell(cell);

                valtot=valtot+Math.round(valorcillo);

            }
            //fin de ciclo

            //inicio de fila final
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(1.0F);//izquierda
            cell.setBorderWidthRight(0.0F);
            cell.setPhrase(new Phrase("", fuente_normal2));
            table.addCell(cell);


            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(1.0F);//medio
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(0.0F);
            cell.setPhrase(new Phrase("", fuente_normal2));
            table.addCell(cell);

            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(1.0F);//medio
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(0.0F);
            cell.setPhrase(new Phrase("TOTAL", fuente_normal2));
            table.addCell(cell);

            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(1.0F);//medio
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(0.0F);
            //cell.setPhrase(new Phrase(""+String.valueOf(Util.customFormat(Util.customFormat(valtot)-Util.customFormat(valtot2)-Util.customFormat(valtot3))), fuente_negrita2));

            cell.setPhrase(new Phrase(""+String.valueOf(Util.customFormat(((Math.round(valtot))-(Math.round(valtot2))-(Math.round(valtot3))))), fuente_negrita2));

            table.addCell(cell);

            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(1.0F);//total
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(0.0F);
            cell.setPhrase(new Phrase(""+String.valueOf(Util.customFormat(valtot2)), fuente_negrita2));
            table.addCell(cell);

            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(1.0F);//total
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(0.0F);
            cell.setPhrase(new Phrase(""+String.valueOf(Util.customFormat(valtot3)), fuente_negrita2));
            table.addCell(cell);

            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(1.0F);//total num
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(1.0F);
            String valortotx=String.valueOf(Util.customFormat(valtot));
            cell.setPhrase(new Phrase(""+valortotx, fuente_negrita2));
            table.addCell(cell);

            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(0.0F);
            cell.setBorderWidthRight(1.0F);//derecha
            cell.setPhrase(new Phrase("", fuente_normal2));
            table.addCell(cell);
            //fin de fila final

            //inicio de fila vacia
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(1.0F);
            cell.setBorderWidthLeft(1.0F);//izquierda
            cell.setBorderWidthRight(0.0F);
            cell.setPhrase(new Phrase("", fuente_normal2));
            table.addCell(cell);

            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(1.0F);
            cell.setBorderWidthLeft(0.0F);//5 medios
            cell.setBorderWidthRight(0.0F);
            cell.setPhrase(new Phrase("", fuente_normal2));
            table.addCell(cell);
            table.addCell(cell);
            table.addCell(cell);
            table.addCell(cell);
            table.addCell(cell);
            table.addCell(cell);

            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(1.0F);
            cell.setBorderWidthLeft(0.0F);
            cell.setBorderWidthRight(1.0F);//derecha
            cell.setPhrase(new Phrase("", fuente_normal2));
            table.addCell(cell);
            //fin de fila vacia

            document.add(table);

            //fin de tabla
            document.newPage();

            table = new PdfPTable(1);
            table.setWidthPercentage(100F);

            //inicio de fila final
            cell.setBorderWidthTop(1.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(1.0F);
            cell.setPhrase(new Phrase("", fuente_normal2));
            table.addCell(cell);

            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(1.0F);
            cell.setHorizontalAlignment(cell.ALIGN_JUSTIFIED);
	    cell.setPhrase(new Phrase(texto2, fuente_normal2));
            table.addCell(cell);
            document.add(table);

            table = new PdfPTable(6);
            table.setWidthPercentage(100F);
            cell.setHorizontalAlignment(cell.ALIGN_CENTER);

            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(0.0F);
            cell.setPhrase(new Phrase("", fuente_normal2));
            //cell.setPhrase(new Phrase(texto22, fuente_normal));
            table.addCell(cell);//1
            cellrectangle.setBorderWidthTop(1.0F);
            cellrectangle.setBorderWidthBottom(1.0F);
            cellrectangle.setBorderWidthLeft(1.0F);
            cellrectangle.setBorderWidthRight(1.0F);

            cellrectangle.setPhrase(new Phrase("\n\n\n\n\n\n\n\n", fuente_normal2));
            //cellrectangle.setPaddingLeft(35);
            //cellrectangle.setPaddingRight(35);
            table.addCell(cellrectangle);//2
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(0.0F);
            cell.setBorderWidthRight(0.0F);
            cell.setPhrase(new Phrase("", fuente_normal2));
            table.addCell(cell);//3
            table.addCell(cell);//4
            table.addCell(cellrectangle);//5
            cell.setPhrase(new Phrase("", fuente_normal2));
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(0.0F);
            cell.setBorderWidthRight(1.0F);
            table.addCell(cell);//6

            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(0.0F);
            cell.setPhrase(new Phrase("", fuente_normal2));
            table.addCell(cell); //1
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(0.0F);
            cell.setBorderWidthRight(0.0F);
            cell.setPhrase(new Phrase(texto23y25, fuente_normal2));
            table.addCell(cell);//2
            cell.setPhrase(new Phrase("", fuente_normal2));
            table.addCell(cell);//3
            table.addCell(cell);//4
            cell.setPhrase(new Phrase(texto23y25, fuente_normal2));
            table.addCell(cell);//5
            cell.setPhrase(new Phrase("", fuente_normal2));
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(0.0F);
            cell.setBorderWidthRight(1.0F);
            table.addCell(cell);//6

            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(0.0F);
            cell.setHorizontalAlignment(cell.ALIGN_LEFT);
            cell.setColspan(3);
            cell.setPhrase(text22);
            table.addCell(cell);   //1  y 2 y 3
            cell.setColspan(1);
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(0.0F);
            cell.setBorderWidthRight(0.0F);
            cell.setPhrase(new Phrase("     ", fuente_normal2));
            //table.addCell(cell);//3
            cell.setColspan(3);
            cell.setPhrase(text24);
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(0.0F);
            cell.setBorderWidthRight(1.0F);
            table.addCell(cell);//4 y 5 y 6
            cell.setColspan(1);
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(0.0F);
            cell.setBorderWidthRight(1.0F);
            cell.setPhrase(new Phrase("     ", fuente_normal2));
            //table.addCell(cell);//6

            document.add(table);


            table = new PdfPTable(1);
            table.setWidthPercentage(100F);
            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(0.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(1.0F);


            cell.setHorizontalAlignment(cell.ALIGN_JUSTIFIED);
	    cell.setPhrase(new Phrase(texto3, fuente_negrita2));

            if (!(usuariox.equals("FINTRA") || usuariox.equals("fintra"))){
                table.addCell(cell);
            }

  	    cell.setPhrase(text4);
            table.addCell(cell);

            cell.setBorderWidthTop(0.0F);
            cell.setBorderWidthBottom(1.0F);
            cell.setBorderWidthLeft(1.0F);
            cell.setBorderWidthRight(1.0F);
            cell.setPhrase(new Phrase("\n\n", fuente_normal2));
            table.addCell(cell);

            document.add(table);
            //.out.println("despues de add table");
            document.newPage();
            //.out.println("antes de documento cerrado");
            document.close();
            //.out.println("documento cerrado");


     }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.println("errorcittooo generarPdfPagare___"+e.toString()+"__"+e.getMessage());
        }
    }catch(Exception ex){
        ex.printStackTrace();
        System.out.println("error en servicillo__"+ex.toString()+"__"+ex.getMessage());
    }
  }

} 