package com.tsp.operation.model.services;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import java.util.*;
import com.tsp.operation.model.beans.Email2;

import com.tsp.operation.model.DAOS.EMailDAO;//091218

public class EmailSendingEngineService{

  Properties props;//091205
  EMailDAO eMailDAO;//091207

  Transport transport ;//= mailSession.getTransport();   ///
  Session mailSession;
  String servidor_correo ="smtp.mailgun.org";
  String correo_salidax="opav_asignacion@fintra.co";
  String pass_salidax="m4ilf1ntra";
  String aviso_c = "<span style='font-size: 16pt; font-family: Webdings; color: green;' lang='ES-AR'>P</span>"
            + "<span style='font-size: 7.5pt; color: green;' lang='ES-AR'>Antes de imprimir este e-mail piense bien si es"
            + " necesario hacerlo.&nbsp;Conservemos la naturaleza.<u></u><u></u></span><br><br>"
            + "<b><span style='font-size: 10pt; font-family: &quot;Courier New&quot;; color: rgb(0, 102, 0);'>NOTA DE CONFIDENCIALIDAD:</span>"
            + "</b><span style='font-size: 10pt; font-family: &quot;Courier New&quot;; color: rgb(0, 102, 0);'> La informaci&oacute;n contenida "
            + "en este E-mail es confidencial y s&oacute;lo puede ser utilizada por la persona o la compa&ntilde;&iacute;a a la cual est&aacute; dirigido y/o por "
            + "el emisor. Si no es el receptor autorizado, cualquier atenci&oacute;n, difusi&oacute;n, distribuci&oacute;n o copia de este mensaje es"
            + " prohibida y ser&aacute; sancionada por la ley. Si por error recibe este mensaje, favor borrar el mensaje recibido "
            + "inmediatamente. Si el texto esta en Espa&ntilde;ol y carece de acentos, es por las limitaciones de algunos software de "
            + "e-mail.</span></p><u></u><p><b><span style='font-size: 10pt; font-family: &quot;Courier New&quot;; "
            + "color: rgb(0, 102, 0);' lang='EN-US'>CONFIDENTIAL NOTE:</span></b><span style='font-size: 10pt; font-family: "
            + "&quot;Courier New&quot;; color: rgb(0, 102, 0);' lang='EN-US'> The"
            + " information in this E-mail is intended to be confidential and only for use of the individual or entity to whom it is"
            + "addressed and/or the issuer. If you are not the intended recipient, any retention, dissemination, distribution or"
            + "copying of this message is strictly prohibited and sanctioned by law. If you receive this message by error,"
            + "please immediately delete the message received.<u></u><u></u></span></p>";

    String aviso_1="";
    String img_logo = "<img  src= 'http://www.fintra.co:3/fintra/images/fintrapdf.gif' width='230' height='45'  />";
    String firma = "<br>"
            + img_logo + "<br>"
            + "<span style='color: #999; font-size:12px; font:Tahoma, Geneva, sans-serif' >"
            + "Carrera 53 # 79 - 01 Local 205 Barranquilla, Colombia<br />"
            + "PBX: 57 5 3679900 FAX: 57 5 3679906<br />"
            + "<a style='color: #007500' href='www.fintra.co'>www.fintra.co</a></span><br /> "
            +aviso_c;

  public EmailSendingEngineService(){
     try{
         props = new Properties();//091205

         eMailDAO= new EMailDAO();//091206

         props.put("mail.debug", "true");
         props.setProperty("mail.transport.protocol", "smtp");
         props.setProperty("mail.smtp.auth","true");
         mailSession = Session.getDefaultInstance(props, null);///
         transport = mailSession.getTransport();   ///
         transport.connect("smtp.mailgun.org", "transferencias@fintra.co", "h3g0y465");  ///
         // "noresponder@geotechsa.com", "Geo5M41lU9z"
     }catch(Exception e){
         System.out.println("error en EmailSendingEngineService:"+e.toString()+"__"+e.getMessage());
     }

  }



    public EmailSendingEngineService(String servidor_correo,String correo_salida,String pass_salida){
     try{
         props = new Properties();//091205

         eMailDAO= new EMailDAO();//091206

         props.put("mail.debug", "true");
         props.setProperty("mail.transport.protocol", "smtp");
         props.setProperty("mail.smtp.auth","true");

         mailSession = Session.getDefaultInstance(props, null);///
         transport = mailSession.getTransport();   ///
         transport.connect(servidor_correo, correo_salida, pass_salida);  ///
         
         pass_salidax=pass_salida;
         correo_salidax=correo_salida;
         this.servidor_correo =servidor_correo;
     }catch(Exception e){
         System.out.println("error en EmailSendingEngineService:"+e.toString()+"__"+e.getMessage());
     }

  }



  public boolean send(Email2 email) throws Exception  {
    long now = System.currentTimeMillis();
    String emailFrom    = email.getEmailfrom().trim();///
    String senderName   = email.getSenderName().trim();
    String emailSubject = email.getEmailsubject().trim();///
    String emailBody    = email.getEmailbody().trim()+firma;///
    String emailto      = email.getEmailto();
    String nombrecitoarchivo=email.getNombreArchivo();
    String ruticarchivo=email.getRutaArchivo();
    String Emailcopyto=email.getEmailcopyto();
    String html="<html><head><meta http-equiv='Content-Type' content='text/html'; charset='ISO-8859-1' /><title>Correo</title></head><body>";//jpinedo
    String finhtml="</body></html>";


    boolean debug = false;


    InternetAddress[] addressTo=new InternetAddress[email.getEmailToLength()];

    //.out.println("send");
    for (int i=0;i<addressTo.length;i++){
        addressTo[i]=new InternetAddress(email.getEmailto(i));
    }
    //.out.println("to ya"+email.getEmailCopytoLength());
    InternetAddress[] addressCopyTo=null;
    if (email.getEmailCopytoLength()>0){
        addressCopyTo=new InternetAddress[email.getEmailCopytoLength()];
        //.out.println("antes de for");
        for (int i=0;i<addressCopyTo.length;i++){
            addressCopyTo[i]=new InternetAddress(email.getEmailcopyto(i));//error si tiene vacio el getEmailcopyto
        }
    }
    //.out.println("for ya");

    //.out.println("copy ya");
    InternetAddress[] addressHiddenCopyTo=null;
    if (email.getEmailHiddenCopytoLength()>0){
        addressHiddenCopyTo=new InternetAddress[email.getEmailHiddenCopytoLength()];
        for (int i=0;i<addressHiddenCopyTo.length;i++){
            addressHiddenCopyTo[i]=new InternetAddress(email.getEmailHiddencopyto(i));
        }
    }


    MimeMultipart multiParte = new MimeMultipart();
    BodyPart texto = new MimeBodyPart();
    texto.setText(emailBody);

    texto.setContent(emailBody, "text/html");///
   if (!nombrecitoarchivo.equals("") ){
        BodyPart adjunto = new MimeBodyPart();
        adjunto.setDataHandler(new DataHandler(new FileDataSource(ruticarchivo+nombrecitoarchivo)));
        adjunto.setFileName(nombrecitoarchivo);
        multiParte.addBodyPart(adjunto);
    }
   
   
   
   if (email.getNombreArchivos() !=null){
       
        String [] archivos= email.getNombreArchivos();
         for (int i=0;i<archivos.length;i++)
         {
          BodyPart adjunto = new MimeBodyPart();
          adjunto.setDataHandler(new DataHandler(new FileDataSource(ruticarchivo+archivos[i])));
          adjunto.setFileName(archivos[i]);
          multiParte.addBodyPart(adjunto);
       }
   }
   
   
   
    multiParte.addBodyPart(texto);



    MimeMessage message2 = new MimeMessage(mailSession); ///
    message2.setFrom( new InternetAddress(emailFrom));///
    message2.setSubject(emailSubject);  ///
   message2.setText(html+emailBody+finhtml,"ISO-8859-1","html");
    message2.setContent(multiParte);///
    message2.setSentDate(new Date()    ); ///

   //message2.setText(html+emailBody+finhtml,"ISO-8859-1","html");
    //message2.sett
    message2.addRecipients(Message.RecipientType.TO, addressTo);///

    if (addressCopyTo!=null && addressCopyTo.length>0 && addressCopyTo[0]!=null && !(addressCopyTo[0].equals(""))){//090716
        message2.addRecipients(Message.RecipientType.CC, addressCopyTo);///
    }

    if (addressHiddenCopyTo!=null && addressHiddenCopyTo.length>0 && addressHiddenCopyTo[0]!=null && !(addressHiddenCopyTo[0].equals(""))){//090716
        message2.addRecipients(Message.RecipientType.BCC, addressHiddenCopyTo);///
    }
    //.out.println("antes de connect");


    boolean sw_enviar=false;//090716
    boolean enviado=true;
    int contador_intentos=0;//090716
    //while (sw_enviar && contador_intentos<4){//090716
    while (sw_enviar==false && contador_intentos < 5){//091206
        sw_enviar=true;//090716
        contador_intentos=contador_intentos+1;//090716
        try{//090716
            //.out.println("antes de enviar");
            if (!(transport.isConnected())){
                //System.out.println("se va a conectar"+new Date());
                System.out.println("se va a conectar ahora"+new Date());//091205
                mailSession = Session.getDefaultInstance(props, null);///091205
                transport = mailSession.getTransport();   /// 091205
                transport.connect(servidor_correo, correo_salidax, pass_salidax);  ///
            }
            transport.sendMessage(message2,    message2.getRecipients(Message.RecipientType.TO)); ///
            if (addressHiddenCopyTo!=null && addressHiddenCopyTo.length>0 && addressHiddenCopyTo[0]!=null && !(addressHiddenCopyTo[0].equals(""))){//090716
                transport.sendMessage(message2,    message2.getRecipients(Message.RecipientType.BCC));///
            }
            if (addressCopyTo!=null && addressCopyTo.length>0 && addressCopyTo[0]!=null && !(addressCopyTo[0].equals(""))){//090716
                transport.sendMessage(message2,    message2.getRecipients(Message.RecipientType.CC));///
            }
        }catch(Exception w){//090716
            sw_enviar=false;//090716
            System.out.println("error enviando msgremix:"+w.toString()+"__"+w.getMessage()+"contador_intentosremix:"+contador_intentos+"-->"+Message.RecipientType.TO+"--2"+message2.getRecipients(Message.RecipientType.TO));//090716
        }
    }




    if (sw_enviar==false){//091206
        eMailDAO.saveMailError(email);//091206
        enviado=true;
    }//091206

    transport.close();
    //.out.println("closed");
    return enviado;
  }
}