/*
 * Nombre        Tipo_contactoService.java
 * Autor         Ing. Jose de la Rosa
 * Fecha         17 de junio de 2005, 02:42 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

public class Tipo_ubicacionService {

    private Tipo_ubicacionDAO tipo_ubicacion;
    
    /** Creates a new instance of Tipo_ubicacionService */
    public Tipo_ubicacionService() {
        tipo_ubicacion = new Tipo_ubicacionDAO();
    }   
    
    /**
     * Getter for property tipo_ubicaciones.
     * @return Value of property tipo_ubicaciones
     */
    public Vector getTipo_ubicaciones() {
        return tipo_ubicacion.getTipo_ubicaciones();
    }
    
    /**
     * Setter for property tipo_ubicaciones.
     * @param ub New value of property tipo_ubicaciones.
     */
    public void setTipo_ubicaciones(Vector tipo_ubicaciones) {
        tipo_ubicacion.setTipo_ubicaciones(tipo_ubicaciones);
    }
    
    /**
     * Metodo searchTipo_ubicacion, obtienen un tipo de ubicacion dado un codigo    
     * @autor  Ing. Jose de la Rosa
     * @see  searchTipo_ubicacion(String cod)
     * @version  1.0
     */
    public void serchTipo_ubicacion(String cod)throws SQLException{
        tipo_ubicacion.searchTipo_ubicacion(cod);
    }
    
    /**
     * Metodo listTipo_ubicacion, obtienen los tipo de ubicaciones
     * @autor  Ing. Jose de la Rosa
     * @see  listTipo_ubicacion()
     * @version  1.0
     */
    public void listTipo_ubicacion()throws SQLException{
        tipo_ubicacion.listTipo_ubicacion();
    }
}
