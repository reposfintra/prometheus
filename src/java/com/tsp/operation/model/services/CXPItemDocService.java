/***********************************************************************************
 * Nombre clase : ............... DespachoManualService.java                            *
 * Descripcion :................. Clase que instancia los metodos de CXPItemDocDAO
 *
 *                                con la BD.                                            *
 * Autor :....................... Ing. David Lamadrid                                   *
 * Fecha :....................... 15 de noviembre de 2005, 11:02 AM                     *
 * Version :..................... 1.0                                                   *
 * Copyright :................... Fintravalores S.A.                               *
 ***********************************************************************************/
package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;

public class CXPItemDocService {
    
    public CXPItemDocDAO dao;
    /** Creates a new instance of CXPDocService */
    public CXPItemDocService() {
        dao = new CXPItemDocDAO();
    }
    public CXPItemDocService(String dataBaseName) {
        dao = new CXPItemDocDAO(dataBaseName);
    }
    public void buscarFacturasxUsuario(CXP_Doc factura) throws SQLException {
        dao.buscarItemsFactura(factura);
    }
    public Vector getVectorItemsFactura(){
        return dao.vectorItemsFacturas();
    }
    
    public com.tsp.operation.model.beans.CXPItemDoc getItemFactura() {
        return dao.getItemFactura();
    }
    
    public void setItemFactura(com.tsp.operation.model.beans.CXPItemDoc itemFactura) {
        dao.setItemFactura(itemFactura);
    }
    
    public java.util.Vector getVecCxpItemsDoc() {
        return dao.getVecCxpItemsDoc();
    }
    
    public void setVecCxpItemsDoc(java.util.Vector vecCxpItemsDoc) {
        dao.setVecCxpItemsDoc(vecCxpItemsDoc);
    }
    
     /**
     * Metodo: getAcuerdo_especial, permite retornar un objeto de registros de CXPItemDoc.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @see getCXPItem_doc - CXPItemDocDAO
     * @version : 1.0
     */    
    public CXPItemDoc getCXPItem_doc( )throws SQLException{
        return dao.getCXPItem_doc();
    }
    
    /**
     * Metodo: setAcuerdo_especial, permite obtener un objeto de registros de CXPItemDoc.
     * @autor : Ing. Jose de la rosa
     * @param : objeto
     * @see setCXPItem_doc - CXPItemDocDAO
     * @version : 1.0
     */
    public void setCXPItem_doc (CXPItemDoc acuerdo)throws SQLException {
        dao.setCXPItem_doc (acuerdo);
    }
    
    /**
     * Metodo ingresarCXPItem_doc, permite ingrear una factura
     * recibe por parametro
     * @autor : Ing. Jose de la rosa
     * @param : 
     * @see ingresarCXPItem_doc - CXPItemDocDAO
     * @version : 1.0
     */
    public void ingresarCXPItem_doc(CXPItemDoc acuerdo) throws SQLException {    
        try{
            dao.setCXPItem_doc (acuerdo);
            dao.ingresarCXPItem_doc();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    } 
    
}
