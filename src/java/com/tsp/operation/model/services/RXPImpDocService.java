/***********************************************************************************
 * Nombre clase : ............... RXPImpDocService.java                            *
 * Descripcion :................. Clase que instancia los metodos de CXPImpDocService   *
 *                                con la BD.                                            *
 * Autor :....................... Ing. David Lamadrid                                   *
 * Fecha :....................... 15 de noviembre de 2005, 11:02 AM                     *
 * Version :..................... 1.0                                                   *
 * Copyright :................... Fintravalores S.A.                               *
 ***********************************************************************************/
package com.tsp.operation.model.services;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;
/**
 *
 * @author  dlamadrid
 */
public class RXPImpDocService
{
        
        public RXPImpDocDAO dao = new RXPImpDocDAO ();
        /** Creates a new instance of CXPImpDocService */
        public RXPImpDocService()
        {
        }
        
        
        /**
         * Metodo que retorna un Objeto tipo ImpuestoDoc
         * @autor.......David Lamadrid
         * @param.......ninguno
         * @see.........Util.class
         * @throws.......
         * @version.....1.0.
         * @return......Objeto tipo ImpuestoDoc
         */
        public com.tsp.operation.model.beans.CXPImpDoc getImpuestoDoc ()
        {
                return dao.getImpuestoDoc ();
        }
        
        /**
         * Metodo que setea un Objeto tipo ImpuestoDoc
         * @autor.......David Lamadrid
         * @param.......ninguno
         * @see........ CXPImpDoc.class
         * @throws.......
         * @version.....1.0.
         * @return......Objeto tipo CXPImpDoc
         */
        public void setImpuestoDoc (com.tsp.operation.model.beans.CXPImpDoc impuestoDoc)
        {
                dao.setImpuestoDoc (impuestoDoc);
        }
        
        /**
         * Metodo que retorna un Vector de Objetos tipo CXPImpDoc
         * @autor.......David Lamadrid
         * @param.......ninguno
         * @see........ CXPImpDoc.class
         * @throws.......
         * @version.....1.0.
         * @return......Vector de Objetos tipo CXPImpDoc
         */
        public java.util.Vector getVImpuestosDoc ()
        {
                return dao.getVImpuestosDoc ();
        }
        
        /**
         * Metodo que setea un Vector de Objetos tipo CXPImpDoc
         * @autor.......David Lamadrid
         * @param.......Vector de Objetos tipo CXPImpDoc
         * @see........ CXPImpDoc.class
         * @throws.......
         * @version.....1.0.
         * @return......
         */
        public void setVImpuestosDoc (java.util.Vector vImpuestosDoc)
        {
                dao.setVImpuestosDoc (vImpuestosDoc);
        }
        
        
        /**
         * Metodo que genera un Vector de Objetos tipo CXPImpDoc y lo retorna.
         * @autor.......David Lamadrid
         * @param.......Vector de Objetos tipo CXPImpDoc
         * @see........ CXPImpDoc.class
         * @throws.......
         * @version.....1.0.
         * @return......Vector vImpuestos.
         */
        public Vector generarVImpuestosDoc (Vector vImpuestos)
        {
                return dao.generarVImpuestosDoc (vImpuestos);
        }
        
}
