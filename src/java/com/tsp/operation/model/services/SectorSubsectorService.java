/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.services;
import com.tsp.operation.model.DAOS.SectorSubsectorDAO;
import java.util.ArrayList;

/**
 *
 * @author maltamiranda
 */
public class SectorSubsectorService {
    ArrayList resultado;
    String nombre_sector,detalle_sector,codigo_sector, activo, reliquida, codigo_alterno, nombre_alterno;
    SectorSubsectorDAO cd;
    
    public SectorSubsectorService() {
        cd= new SectorSubsectorDAO();
    }
    public SectorSubsectorService(String dataBaseName) {
        cd= new SectorSubsectorDAO(dataBaseName);
    }
    
    public String getNombre_alterno() {
        return nombre_alterno;
    }

    public void setNombre_alterno(String nombre_alterno) {
        this.nombre_alterno = nombre_alterno;
    }

    public String getCodigo_alterno() {
        return codigo_alterno;
    }

    public void setCodigo_alterno(String codigo_alterno) {
        this.codigo_alterno = codigo_alterno;
    }

    public String getReliquida() {
        return reliquida;
    }

    public void setReliquida(String reliquida) {
        this.reliquida = reliquida;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getCodigo_sector() {
        return codigo_sector;
    }

    public void setCodigo_sector(String codigo_sector) {
        this.codigo_sector = codigo_sector;
    }

    public String getDetalle_sector() {
        return detalle_sector;
    }

    public void setDetalle_sector(String detalle_sector) {
        this.detalle_sector = detalle_sector;
    }

    public String getNombre_sector() {
        return nombre_sector;
    }

    public void setNombre_sector(String nombre_sector) {
        this.nombre_sector = nombre_sector;
    }

    public ArrayList getSectores(String bd)throws Exception {
        return cd.getSectores(bd);
    }

    public ArrayList getSubSectores(String id,String bd) throws Exception {
        return cd.getSubSectores(id,bd);
    }
   public String agregar_sector(String nombre,String detalle,String codigo,String activo,String usuario,String bd, String reliquida, String codigo_alterno, String nombre_alterno) throws Exception {
        return cd.agregar_sector(nombre,detalle,codigo,activo,usuario,bd, reliquida, codigo_alterno,nombre_alterno);
    }
    public String agregar_subsector(String nombre,String detalle,String codigo,String activo,String id, String usuario,String bd, String nombre_alterno) throws Exception {
        return cd.agregar_subsector(nombre,detalle,codigo,activo,id,usuario,bd, nombre_alterno);
    }
   public void editar_sector(String id,String nombre,String detalle,String activo,String usuario,String bd, String reliquida, String codigo_alterno,String nombre_alterno) throws Exception {
        cd.editar_sector(id,nombre,detalle,activo,usuario,bd, reliquida, codigo_alterno,nombre_alterno);
    }
    public void editar_subsector(String id,String idsub,String nombre,String detalle,String activo,String usuario,String bd,String nombre_alterno) throws Exception {
        cd.editar_subsector(id,idsub,nombre,detalle,activo,usuario,bd,nombre_alterno);
    }

    public void buscar_sector(String id,String bd) throws Exception {
        String [] vec = cd.buscar_sector(id,bd);
        this.setNombre_sector(vec[0]);
        this.setDetalle_sector(vec[1]);
        this.setCodigo_sector(vec[2]);
        this.setActivo(vec[3]);
        this.setReliquida(vec[4]);
        this.setCodigo_alterno(vec[5]);
        this.setNombre_alterno(vec[6]);
    
    }

    public void buscar_subsector(String id,String idsub,String bd) throws Exception {
        String [] vec = cd.buscar_subsector(id,idsub,bd);
        this.setNombre_sector(vec[0]);
        this.setDetalle_sector(vec[1]);
        this.setCodigo_sector(vec[2]);
        this.setActivo(vec[3]);
        this.setNombre_alterno(vec[4]);

    }


}
