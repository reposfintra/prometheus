/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.services;

import java.util.ArrayList;
import com.tsp.operation.model.DAOS.InteresesMoraDAO;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
/**
 *
 * @author maltamiranda
 */
public class InteresesMoraService {
    public ArrayList buscar_intereses(String idclie,String ms,String fechai,String fechaf,String factura,String nota)throws Exception{
        InteresesMoraDAO imd=new InteresesMoraDAO();
        return imd.buscar_intereses(idclie,ms,fechai,fechaf,factura,nota);
    }

    public void anular_intereses(String[] intereses, String login)throws Exception {
        InteresesMoraDAO imd=new InteresesMoraDAO();
        imd.anular_intereses(intereses,login);
    }

    public void ignorar_intereses(String[] intereses, String login)throws Exception {
        InteresesMoraDAO imd=new InteresesMoraDAO();
        imd.ignorar_intereses(intereses,login);
    }

    public void facturar_intereses(String[] intereses, String login)throws Exception {
        InteresesMoraDAO imd=new InteresesMoraDAO();
        this.generarArchivo(login, imd.facturar_intereses(intereses,login));
    }

    public void generarArchivo(String usuario,String contenido) throws Exception{
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario;
        File archivo = new File( ruta );
        archivo.mkdirs();
        String  url   ="";
        url=ruta + "/GeneracionFacturasInteresMora" +  (new SimpleDateFormat("yyyMMdd_hhmmss")).format( new Date())+".txt";
        PrintWriter pw = new PrintWriter( new BufferedWriter( new FileWriter(url) )  );
        pw.print(contenido);
        pw.close();
    }

}
