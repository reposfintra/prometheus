/***************************************
    * Nombre Clase ............. ImagenService.java
    * Descripción  .. . . . . .  Servicio para manipular las imagenes
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  10/10/2005
    * versión . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/



package com.tsp.operation.model.services;





import java.io.*;
import java.util.*;
import java.util.Date;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;


public class ImagenService {
    
    private ImagenDAO ImagenDataAccess;
    
    // Combos
    private List      actividades;
    private List      documentos;
    private List      imagenes;
    private List      agencias;
    private List      relacion;
    
   // Rutas
    private String    ruta;
    private String    rutaPrevia;
    
  // Datos  
    
    private String    actividad;
    private String    tipoDocumento;
    private String    documento;
    
    private String    directory;
    private String    procedencia;
    
    
    private String    user;   
    private String    agencia;   
    
    private int       tamañoImages;
    
    private boolean   load;
    
    
    //************* Diogenes
    private boolean   estado;
    private String    documento_conf;
    private String    estado_conf;
    private String    codact;
    //info cliente
    private List despPlanilla = null;
    private List despRemesa =  null;
    private List cumpPlanilla =  null;
    private List cumpRemesa = null;
    
    
    
    // Despacho:
    private  List       imagenesAsociadas; 
    private  String     carpetaDespacho;
    
    
    
 /**
 * Constructor de la Clase
 * @autor: ....... Fernel Villacob
 * @version ...... 1.0
 */ 
    
    public ImagenService() {
        ImagenDataAccess = new  ImagenDAO();
        actividades      = null;
        documentos       = null;
        agencias         = null;
        relacion         = null; 
                
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        ruta             = rb.getString("ruta")+ "/documentos/imagenes/";
        rutaPrevia       = ruta       + "viewPrevias/";
        carpetaDespacho  = rutaPrevia + "DESPACHO/";
        
        
        
        
        reset();
        tamañoImages     = 256000;    // 250 KB ;        
        
        //**********verificacion doc
        estado           = false;
        documento_conf   = "";
        estado_conf      = "";
        codact           = "";
    }
    
    
    
    
    
    
 /**
 * Seteo de variables
 * @autor: ....... Fernel Villacob
 * @version ...... 1.0
 */ 
    
    public void reset(){
        actividad        = "";
        tipoDocumento    = "";
        documento        = "";
        directory        = "";
        agencia          = "";   
        procedencia      = "";        
        user             = ""; 
        imagenes         = null;
        load             = false;
    }
    
    
    
      
 /**
 * Seteo de variables
 * @autor: ....... Fernel Villacob
 * @version ...... 1.0
 */ 
    
    public void atras(){
        directory        = "";  
        procedencia      = ""; 
        imagenes         = null;
        load             = false;
    }
    
    
    
    
    
 /**
 * Carga las lista para los combos
 * @autor: ....... Fernel Villacob
 * @version ...... 1.0
 */
    public void load()throws Exception{
       try{
            if( this.actividades == null ) searchActividades();
            if( this.relacion    == null ) searchRelacion();
            if( this.documentos  == null ) searchDocumentos();
            if( this.agencias    == null ) searchAgencias();  
       }catch(Exception e){ 
            throw new Exception(e.getMessage());
       }
    }
    
    
    
/**
 * devuelve el url
 * @autor: ....... Fernel Villacob
 * @version ...... 1.0
 */
    public String getURL(){
        return this.ruta; 
    }
    
    
    
    
 /**
 * Verificamos la existencia del documento
 * @autor: ....... Fernel Villacob
 * @param ........ tipoDoc, documento
 * @version ...... 1.0
 */   
    
   public boolean  valido(String tipoDoc, String documento) throws Exception{
       boolean estado = false;
        try{
            estado = this.ImagenDataAccess.validoDoc(tipoDoc, documento);            
        }catch(Exception e){ 
            throw new Exception(e.getMessage());
        }
       return estado;
  }
    
    
    
    
   
   // DESPACHO:
   
   
   
 /**
 * Seteo de variables para despacho
 * @autor: ....... Fernel Villacob
 * @version ...... 1.0
 */ 
    public void  resetImagesDespacho(String user)throws Exception{
        try{
             imagenesAsociadas = new LinkedList();
             reset();
             
             
             File carpeta = new File( this.carpetaDespacho  );
             if ( !carpeta.exists())
                   carpeta.mkdir();             
             
             this.createDir( this.carpetaDespacho + user  );
             
             
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
   
    
 /**
 * Adiciona a la lista para despacho, actualiza la existente si existe, sino lo adiciona
 * @autor: ....... Fernel Villacob
 * @version ...... 1.0
 */ 
   public void addElement(Hashtable  ht)throws Exception{
       
       try{
           
           String actividad  = (String) ht.get("actividad");
           String tipoDoc    = (String) ht.get("tipoDocumento");

           for(int i=0;i<this.imagenesAsociadas.size();i++){
                 Hashtable  element = (Hashtable)this.imagenesAsociadas.get(i);
                 String act     = (String) element.get("actividad");
                 String tDoc    = (String) element.get("tipoDocumento");
                 if ( act.equals( actividad )  && tDoc.equals( tipoDoc  )  ){
                      this.imagenesAsociadas.remove(i);
                      break;
                 }
           }

           this.imagenesAsociadas.add( ht );
           
       }catch(Exception e){
           throw new Exception ( e.getMessage() );
       }
       
   }
   
   
   
 /**
 * Obtiene un elemento de la lista para actividad y tipo docuemnto
 * @autor: ....... Fernel Villacob
 * @version ...... 1.0
 */ 
   private Hashtable getElement( String actividad, String tipoDocumento)throws Exception{
       Hashtable  ht = null;
       try{
           for(int i=0;i<this.imagenesAsociadas.size();i++){
                 Hashtable  element = (Hashtable)this.imagenesAsociadas.get(i);
                 String act     = (String) element.get("actividad");
                 String tDoc    = (String) element.get("tipoDocumento");
                 if ( act.equals( actividad )  && tDoc.equals( tipoDocumento  )  ){
                      ht = element;
                      break;
                 }
           }
       }catch(Exception e){
           throw new Exception ( e.getMessage() );
       }
       return ht;
   }
   
   
   
   
   
 /**
 * Coloca el numero del documento asociado a las imagenes de esa actividad y tipo de docuemnto
 * @autor: ....... Fernel Villacob
 * @version ...... 1.0
 */ 
   public void addDocument( String actividad, String tipoDocumento, String documento)throws Exception{ 
      try{
           Hashtable  element = getElement( actividad, tipoDocumento  );
           if( element !=null )
               element.put("documento", documento );
      }catch(Exception e){
           throw new Exception ( e.getMessage() );
      }
   }
   
   
   
   
   
   
   
 /**
 * Devuelve el sql que inserta las imagenes asociadas al documento
 * @autor: ....... Fernel Villacob
 * @version ...... 1.0
 */ 
   public String  getSQL( String actividad, String tipoDocumento, String documento)throws Exception{ 
      
       String  sql = "";
       try{
           Hashtable  element = getElement( actividad, tipoDocumento );
           
           if( element!=null ){
               
               // cargamos datos...
               Dictionary fields   = new Hashtable();            
                fields.put("actividad",      (String)element.get("actividad") );
                fields.put("tipoDocumento",  (String)element.get("tipoDocumento") );
                fields.put("documento",      (String)element.get("documento") );
                fields.put("agencia",        getAgencia()       );
                fields.put("usuario",        getUser()          );

               List   filename  = (List)   element.get("images");
               String url       = (String) element.get("url");
            
               
             // cargamos imagenes de la ruta...
               File  file    = new File( url );
               if( file.exists() &&  filename.size()>0 ){
                   File []arc =  file.listFiles();
                   for (int i=0;i< arc.length;i++)
                        if( ! arc[i].isDirectory()){
                              
                               String name = arc[i].getName();
                               if( isLoad(filename, name)  && this.ext(name) ){

                                      FileInputStream       in  = new FileInputStream( file +"/"+  name);
                                      ByteArrayOutputStream out = new ByteArrayOutputStream();
                                      ByteArrayInputStream  bfin;
                                      int input                 = in.read();
                                      byte[] savedFile          = null;
                                      while(input!=-1){             
                                          out.write(input);
                                          input  = in.read();
                                      }
                                      out.close();
                                      savedFile  = out.toByteArray();
                                      bfin       = new ByteArrayInputStream( savedFile ); 

                                      fields.put("imagen", name ); 

                                     // Insertar imagen  
                                     sql += insertImagenBatch(bfin, this.tamañoImages ,  fields);                                    

                               }
                            
                        }
                   
               }
               
               
           } 
           
       }catch(Exception e){
           throw new Exception ( e.getMessage() );
       }
       return  sql;
   }
   
   
   
   
 
 
 /**
 * Determina que imagen de la carpeta esta realmente asociada
 * @autor: ....... Fernel Villacob
 * @version ...... 1.0
 */ 
   
 public boolean isLoad(List listaFile, String name)throws Exception{
      boolean estado = false;
      if (  listaFile!=null){
          for ( int i=0;i<listaFile.size();i++ ){
              String filename  = (String)listaFile.get(i);
              if( filename.equals(name) ){
                  estado = true;
                  break;
              }
          }
      }
      return estado;
  }
    
    
    
 
 
 
 
    
   
   
   
 
/**
 * Retornamos el url del directorio de la imagen
 * @autor: ....... Fernel Villacob
 * @param ........ (s) la linea que contiene Content-Disposition: form-data;
 * @version ...... 1.0
 */ 
 
public  String getDirectory(String s){ 
    int pos;
    int end;
    String filepath=""; 
    if ( s != null ) {
        pos =  this.getPosition(s);             
        if (pos != -1) {            
            end     = s.lastIndexOf("\\");               
            filepath = s.substring( pos+10, end + 1);    
        }
   }
   return filepath;
} 





  
 /**
 * Crea y/o limpia en el servidor  un directorio con el login del usuario, en el cual guardamos las imagenes a subir a la bd
 * @autor: ....... Fernel Villacob
 * @param ........ (dir) Directorio
 * @version ...... 1.0
 */
  
public void createDir(String dir) throws Exception{
   try{
       
        File f = new File(dir); 
        if(! f.exists() ) f.mkdir();
        else{           
            File []arc =  f.listFiles(); 
            int  total =  arc.length;
            for(int i=0; i< total;i++){
               File  imagen = arc[i];
               imagen.delete();                
            }
            
        }
        
  }catch(Exception e){ throw new Exception ( e.getMessage());}
}







/**
 * Retornamos el nombre de la imagen a subir
 * @autor: ....... Fernel Villacob
 * @param ........ (s) la linea que contiene Content-Disposition: form-data;
 * @version ...... 1.0
 */

 public String getName(String s){
    String filename = "";
    int    pos;
    String filepath;
    String file = "";  
  // s  = s.replaceAll(" |+|-|_", "");
    if ( s != null ) {
        pos =  this.getPosition(s);
        if (pos != -1) {
                filepath = s.substring( pos+10, s.length()-1 );                  
                pos      = filepath.lastIndexOf("\\");
                if (pos != -1){
                    file = filepath.substring( pos+1 );
                }
                else{
                    file = filepath;                  
                }
        }
   }
   return file.toLowerCase();
 }
 
 
 
/**
 * Retornamos la posicion de Inicio del la ruta del archivo
 * @autor: ....... Fernel Villacob
 * @param ........ (s) la linea que contiene Content-Disposition: form-data;
 * @version ...... 1.0
 */
 
 public int getPosition(String s){
    int pos = s.indexOf("filename=\"");
    return pos;
 }
 
 
 
   
   
 /**
 * Devuelve el listado de imagenes montadas en el directorio del usuario
 * @autor: ....... Fernel Villacob
 * @param ........ ByteArrayInputStream bfin, int longitud,  Dictionary fields
 * @throws ....... ServletException
 * @version ...... 1.0
 */ 
  
  public List listFile(String Base)  throws Exception{
    List lista = new LinkedList();
    try{        
         File f   = new File(Base); 
         if(f.exists()){
             File []arc =  f.listFiles();
             for (int i=0;i< arc.length;i++)
                 if( ! arc[i].isDirectory()){
                     String name = arc[i].getName();
                     if(  ext( name ) )
                         lista.add(name);
                 } 
         }
         
    }catch(Exception e){ throw new Exception ( e.getMessage());}
    
    return lista;
 }
  
  
   
   
 /**
 *valida las extensiones validas permitidas para subir 
 * @autor: ....... Fernel Villacob
 * @param ........ (file)  Nombre de la imagen
 * @version ...... 1.0
 */
  
 
 public boolean ext(String file){
     boolean  estado = false;     
     int      punto  = file.indexOf(".");
     String   ext    = file.substring( punto+1 ,file.length());
              ext    = ext.toLowerCase().trim();
     if (  ext.equals("gif") || ext.equals("bmp") || ext.equals("jpeg") || ext.equals("jpg")  || ext.equals("tif")  || ext.equals("tiff") || ext.equals("pdf") ){
              estado = true;
     }
     return estado;
 }
 
 
 
 
 
    
    
 /**
 * Insertamos la imagen
 * @autor: ....... Fernel Villacob
 * @version ...... 1.0
 */
    
    public void insertImagen( ByteArrayInputStream bfin, int longitud,  Dictionary fields) throws Exception{
        try{
            this.ImagenDataAccess.insertImagen(bfin,longitud,fields);       
        }catch(Exception e){  throw new Exception( e.getMessage()); }
    }
     
    
    
    
 /**
 * Generamos el sql de insert de la imagen
 * @autor: ....... Fernel Villacob
 * @version ...... 1.0
 */
    
    public String insertImagenBatch( ByteArrayInputStream bfin, int longitud,  Dictionary fields) throws Exception{
        String  sql = "";
        try{
            sql = this.ImagenDataAccess.insertImagenBatch(bfin,longitud,fields);       
        }catch(Exception e){  throw new Exception( e.getMessage()); }
        return  sql;
    }
    
    
    
    
    
 /**
 * carga en la lista las actividades
 * @autor: ....... Fernel Villacob
 * @version ...... 1.0
 */
    public void searchActividades() throws Exception{
        this.actividades = null;
        try{
             this.actividades = this.ImagenDataAccess.searchActividades();       
        }catch(Exception e){ 
            throw new Exception( e.getMessage());
        }
    }
    
    
 /**
 * devuelve el listado de actividades
 * @autor: ....... Fernel Villacob
 * @version ...... 1.0
 */
    public List getActividades(){
        return this.actividades;
    }
    
    
    
    
    
    
    
 /**
 * devuelve  la relacion entre actividades y documentos
 * @autor: ....... Fernel Villacob
 * @version ...... 1.0
 */
    
    public void searchRelacion() throws Exception{
        this.relacion = null;
        try{
             this.relacion = this.ImagenDataAccess.searchActDoc();       
        }catch(Exception e){ 
            throw new Exception( e.getMessage());
        }
    }
    
    
    
 /**
 * devuelve una variable js que contiene la relacion
 * @autor: ....... Fernel Villacob
 * @version ...... 1.0
 */
    
    public String getJsRelacion() throws Exception{
        String js   = " var relacion "; 
        String body = "";
        try{
            if(this.relacion!=null  && this.relacion.size()>0){
                body = "='";
                Iterator it = this.relacion.iterator();
                while(it.hasNext()){
                    String   fila  = (String)it.next();
                    if(!body.equals("='")) body+="|";
                    body+=fila;  
                }
                body+="'";
            }       
            js+= body + ";";
        }catch(Exception e){ throw new Exception( e.getMessage());}
        return js;
    }
    
    
    
    
   
 /**
 * carga la lista de documentos
 * @autor: ....... Fernel Villacob
 * @version ...... 1.0
 */
    public void searchDocumentos() throws Exception{
        this.documentos = null;
        try{
             this.documentos = this.ImagenDataAccess.searchDocumentos();       
        }catch(Exception e){ 
            throw new Exception( e.getMessage());
        }
    }    
    
 /**
 * devuelve el listado de documentos
 * @autor: ....... Fernel Villacob
 * @version ...... 1.0
 */
    public List getDocumentos(){
        return this.documentos;
    }
    
    
    
    
    
   
 /**
 * busca el listado de agencias
 * @autor: ....... Fernel Villacob
 * @version ...... 1.0
 */
    
    public void searchAgencias() throws Exception{
        this.agencias = null;
        try{
             this.agencias = this.ImagenDataAccess.searchAgencias();       
        }catch(Exception e){ 
            throw new Exception( e.getMessage());
        }
    }
    
    
 /**
 * devuelve el listado de agencias
 * @autor: ....... Fernel Villacob
 * @version ...... 1.0
 */    
    public List getAgencias() {
        return this.agencias;
    }
    
    
    
   
    
    
    
    
 /**
 * buscamos la imagen de acuerdo a los parametros establecidos
 * @autor: ....... Fernel Villacob
 * @version ...... 1.0
 */
    public void searchImagen ( String checkActividad      , 
                               String checkTipoDocumento  , 
                               String checkDocumento      , 
                               String checkboxAgencia     , 
                               String checkboxFecha       ,
                               String checkboxCantidad    ,
                               
                               String actividad           , 
                               String tipoDocumento       ,
                               String documento           ,
                               String Agencia             ,
                               String fechaIni            ,
                               String fechaFin            ,
                               String cantidad            ,
                               String user 
                             ) throws Exception{
        imagenes = null;
        try{
             this.imagenes = this.ImagenDataAccess.searcImagenes( checkActividad      , 
                                                                  checkTipoDocumento  , 
                                                                  checkDocumento      ,
                                                                  checkboxAgencia     , 
                                                                  checkboxFecha       ,
                                                                  checkboxCantidad    ,
                                                                  
                                                                  actividad           , 
                                                                  tipoDocumento       , 
                                                                  documento           ,
                                                                  Agencia             ,
                                                                  fechaIni            ,
                                                                  fechaFin            ,
                                                                  cantidad            ,                                                                   
                                                                  this.ruta + user + "/"
                                                                 );       
        }catch(Exception e){ throw new Exception( e.getMessage());}
    }
    
    
    
    
    
    
    
    
    public void setImagenes(List lista){
        this.imagenes = lista;
    }
    
    
    public List getImagenes(){
        return this.imagenes;
    }
    
    
    
    
    
    
    
    
    // __________________________________________________________________________________
    // DIOGENES:
    //***********Verrificacion Doc*************
    
    public void setEstado(boolean val){
        this.estado = val;
    }
    
    public boolean getEstado(){
        return this.estado;
    }
    
    
    public String getDocumento_conf() {
        return this.documento_conf; 
    }
        
    public void   setDocumento_conf(String val){ 
        this.documento_conf= val; 
    }
    
    public String getEstado_conf() {
        return this.estado_conf; 
    }
        
    public void   setEstado_conf(String val){ 
        this.estado_conf= val; 
    }
    
    public String getCodActividad() {
        return this.codact; 
    }
        
    public void   setCodActividad(String val){ 
        this.codact= val; 
    }
    
    /**
   * existeImagen, verifica en la tabla de imagen si existe la imagen.
   * @autor: ....... Ing. Diogenes Bastidas
   * @param ........ codigos, tipo actividad, tipo documento y docuemnto
   * @throws ....... Exception
   * @version ...... 1.0
   */   
    public boolean existeImagen(String act, String tipodoc, String doc) throws Exception {
        try{
             return ImagenDataAccess.existeImagen(act, tipodoc, doc);
        }catch(Exception e){ 
            throw new Exception( e.getMessage());
        }
    }
    
    //_________________________________________________________________________________________
    
    
    
    
    
    
    
    /**
     * Getter for property procedencia.
     * @return Value of property procedencia.
     */
    public java.lang.String getProcedencia() {
        return procedencia;
    }
    
    /**
     * Setter for property procedencia.
     * @param procedencia New value of property procedencia.
     */
    public void setProcedencia(java.lang.String procedencia) {
        this.procedencia = procedencia;
    }
    
    
    
    
    
    
    /**
     * Getter for property rutaPrevia.
     * @return Value of property rutaPrevia.
     */
    public java.lang.String getRutaPrevia() {
        return rutaPrevia + this.user + "/";
    }
    
    
    /**
     * Setter for property rutaPrevia.
     * @param rutaPrevia New value of property rutaPrevia.
     */
    public void setRutaPrevia(java.lang.String rutaPrevia) {
        this.rutaPrevia = rutaPrevia ;
    }
    
    
    
    
    
    
    /**
     * Getter for property user.
     * @return Value of property user.
     */
    public java.lang.String getUser() {
        return user;
    }
    
    /**
     * Setter for property user.
     * @param user New value of property user.
     */
    public void setUser(java.lang.String user) {
        this.user = user;
    }
    
   
    
    
    
    
    /**
     * Getter for property directory.
     * @return Value of property directory.
     */
    public java.lang.String getDirectory() {
        return directory;
    }
    
    /**
     * Setter for property directory.
     * @param directory New value of property directory.
     */
    public void setDirectory(java.lang.String directory) {
        this.directory = directory;
    }
    
    
    
    
    
    
    /**
     * Getter for property actividad.
     * @return Value of property actividad.
     */
    public java.lang.String getActividad() {
        return actividad;
    }
    
    /**
     * Setter for property actividad.
     * @param actividad New value of property actividad.
     */
    public void setActividad(java.lang.String actividad) {
        this.actividad = actividad;
    }
    
    
    
    
    
    /**
     * Getter for property tipoDocumento.
     * @return Value of property tipoDocumento.
     */
    public java.lang.String getTipoDocumento() {
        return tipoDocumento;
    }
    
    /**
     * Setter for property tipoDocumento.
     * @param tipoDocumento New value of property tipoDocumento.
     */
    public void setTipoDocumento(java.lang.String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }
    
    
    
    
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    
    
    
    
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.lang.String getAgencia() {
        return agencia;
    }
    
    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }
    
    
    
    
    
    /**
     * Getter for property tamañoImages.
     * @return Value of property tamañoImages.
     */
    public int getTamañoImages() {
        return tamañoImages;
    }
    
    /**
     * Setter for property tamañoImages.
     * @param tamañoImages New value of property tamañoImages.
     */
    public void setTamañoImages(int tamañoImages) {
        this.tamañoImages = tamañoImages;
    }
    
    
    
    
    
    
    
    /**
     * Getter for property load.
     * @return Value of property load.
     */
    public boolean isLoad() {
        return load;
    }
    
    /**
     * Setter for property load.
     * @param load New value of property load.
     */
    public void setLoad(boolean load) {
        this.load = load;
    }
    
    
    
    
    
    
    /**
     * Getter for property imagenesAsociadas.
     * @return Value of property imagenesAsociadas.
     */
    public List getImagenesAsociadas() {
        return imagenesAsociadas;
    }    
    
    /**
     * Setter for property imagenesAsociadas.
     * @param imagenesAsociadas New value of property imagenesAsociadas.
     */
    public void setImagenesAsociadas(List imagenesAsociadas) {
        this.imagenesAsociadas = imagenesAsociadas;
    }
    
    
    
    
    /**
     * Getter for property carpetaDespacho.
     * @return Value of property carpetaDespacho.
     */
    public java.lang.String getCarpetaDespacho() {
        return carpetaDespacho;
    }
    
    /**
     * Setter for property carpetaDespacho.
     * @param carpetaDespacho New value of property carpetaDespacho.
     */
    public void setCarpetaDespacho(java.lang.String carpetaDespacho) {
        this.carpetaDespacho = carpetaDespacho;
    }

    
    /**
     * Metodo buscarDocumentosDigitalizados, metodo que busca las imagenes
     * digitalizadas del despacho y cumplido por planilla y remesa
     * @param: planilla, remesa y usuario
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    
    public void buscarDocumentosDigitalizados(String planilla, String remesa, String usuario) throws Exception{
        try{
            ////System.out.println("Usuario "+usuario);
            //cargo de los documentos digitados del despacho de la planilla 
            searchImagen("1","1","1",null,null,null,"004", "001",
                                               planilla,null,null,null,null,usuario);
            this.despPlanilla = getImagenes();  
            
            //cargo de los documentos digitados del despacho de la remesa 
            searchImagen("1","1","1",null,null,null,"004", "002",
                                               remesa,null,null,null,null,usuario);
            this.despRemesa = getImagenes();  
            
            //cargo de los documentos digitados de  cumplido de la planilla 
            searchImagen("1","1","1",null,null,null,"014", "001",
                                               planilla,null,null,null,null,usuario);
            this.cumpPlanilla = getImagenes(); 
            
            
            //cargo de los documentos digitados de  cumplido de la remesa 
            searchImagen("1","1","1",null,null,null,"014", "002",
                                               remesa,null,null,null,null,usuario);
            this.cumpRemesa = getImagenes();  
            
            
        }catch(Exception e){ 
            e.printStackTrace();
            throw new Exception( e.getMessage());
        }
    }
    
    /**
     * Getter for property despPlanilla.
     * @return Value of property despPlanilla.
     */
    public java.util.List getDespPlanilla() {
        return despPlanilla;
    }    
    
    /**
     * Setter for property despPlanilla.
     * @param despPlanilla New value of property despPlanilla.
     */
    public void setDespPlanilla(java.util.List despPlanilla) {
        this.despPlanilla = despPlanilla;
    }    
    
    /**
     * Getter for property despRemesa.
     * @return Value of property despRemesa.
     */
    public java.util.List getDespRemesa() {
        return despRemesa;
    }
    
    /**
     * Setter for property despRemesa.
     * @param despRemesa New value of property despRemesa.
     */
    public void setDespRemesa(java.util.List despRemesa) {
        this.despRemesa = despRemesa;
    }
    
    /**
     * Getter for property cumpPlanilla.
     * @return Value of property cumpPlanilla.
     */
    public java.util.List getCumpPlanilla() {
        return cumpPlanilla;
    }
    
    /**
     * Setter for property cumpPlanilla.
     * @param cumpPlanilla New value of property cumpPlanilla.
     */
    public void setCumpPlanilla(java.util.List cumpPlanilla) {
        this.cumpPlanilla = cumpPlanilla;
    }
    
    /**
     * Getter for property cumpRemesa.
     * @return Value of property cumpRemesa.
     */
    public java.util.List getCumpRemesa() {
        return cumpRemesa;
    }
    
    /**
     * Setter for property cumpRemesa.
     * @param cumpRemesa New value of property cumpRemesa.
     */
    public void setCumpRemesa(java.util.List cumpRemesa) {
        this.cumpRemesa = cumpRemesa;
    }
    
    
}
