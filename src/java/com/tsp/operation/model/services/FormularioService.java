/*
 * FormularioService.java
 *
 * Created on 26 de noviembre de 2006, 04:49 PM
 */

package com.tsp.operation.model.services;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
/**
 *
 * @author  dbastidas
 */
public class FormularioService {
    public FormularioDAO formulariodao;
    Vector validacion;
    String llaves;
    String datos;
    String tabla;
    /** Creates a new instance of FormularioService */
    public FormularioService() {
        formulariodao = new FormularioDAO();
    }
    
    /**
     * Getter for property llaves.
     * @return Value of property llaves.
     */
    public java.lang.String getLlaves() {
        return llaves;
    }
    
    /**
     * Setter for property llaves.
     * @param llaves New value of property llaves.
     */
    public void setLlaves(java.lang.String llaves) {
        this.llaves = llaves;
    }
    
    /**
     * Getter for property datos.
     * @return Value of property datos.
     */
    public java.lang.String getDatos() {
        return datos;
    }
    
    /**
     * Setter for property datos.
     * @param datos New value of property datos.
     */
    public void setDatos(java.lang.String datos) {
        this.datos = datos;
    }
    
    /**
     * Getter for property tabla.
     * @return Value of property tabla.
     */
    public java.lang.String getTabla() {
        return tabla;
    }
    
    /**
     * Setter for property tabla.
     * @param tabla New value of property tabla.
     */
    public void setTabla(java.lang.String tabla) {
        this.tabla = tabla;
    }
    
    /**
     * Getter for property validacion.
     * @return Value of property validacion.
     */
    public java.util.Vector getValidacion() {
        return validacion;
    }
    
    /**
     * Setter for property validacion.
     * @param validacion New value of property validacion.
     */
    public void setValidacion(java.util.Vector validacion) {
        this.validacion = validacion;
    }
    
    
    
    /**
     * Metodo: insertFormato_tabla, permite ingresar un registro a la tabla formato_tabla.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void insertar(Vector registro) throws Exception{
        String campos = "", valores="",tabla ="";
        try{
            for(int i=0; i<registro.size();i++){
                Formato_tabla formato = (Formato_tabla) registro.get(i);
                tabla = formato.getTabla();
                campos += formato.getCampo_tabla()+",";
                valores += datosSQL(formato.getTipo_campo() , formato.getValor_campo())+",";
            }
            if(campos.length()>0){
                valores = valores.substring(0,valores.length()-1);
                campos = campos.substring(0,campos.length()-1);
            }
            formulariodao.insertar(tabla+" ( "+campos+" ) VALUES ( "+valores+" )" );
        }catch(Exception ex){
            throw new Exception( ex.getMessage() );
        }
    }
    
    public void crearMetodosValidacion(Vector campos){
        String SQL = "";
        llaves = "";
        datos = "";
        validacion =  new Vector();
        for(int i=0; i < campos.size(); i++){
            Formato_tabla formato = (Formato_tabla) campos.get(i);
            this.tabla = formato.getTabla();
            //crea las consultas de validacion
            if(formato.getValidar().equals("S")){
                SQL = "SELECT "+
                formato.getCampo_val()+
                " FROM "+
                formato.getTabla_val()+
                " WHERE UPPER("+
                formato.getCampo_val()+") = "+
                datosSQL(formato.getTipo_campo() , formato.getValor_campo().toUpperCase());
                
                Hashtable reg = new Hashtable();
                reg.put("SQL", SQL);
                reg.put("Titulo", formato.getTitulo());
                validacion.add(reg);
            }
            
            //crea la cadena de las llaves primarias
            if(formato.getPrimaria().equals("S")){
                this.llaves  +=" "+formato.getCampo_tabla()+" = "+
                datosSQL(formato.getTipo_campo() , formato.getValor_campo().toUpperCase())+
                " AND";
                datos +=" "+formato.getCampo_tabla()+",";
            }
        }
        
        if(llaves.length()>0){
            llaves = llaves.substring(0,llaves.length()-3);
            datos = datos.substring(0,datos.length()-1);
        }
        
    }
    
    public String datosSQL(String tipo, String valor){
        String dato = "";
        if( tipo.equals("DO") || tipo.equals("INT") ){
            dato=" "+valor;
        }
        else{
            dato=" '"+valor+"'";
        }
        return dato;
    }
    
    
    /**
     * Metodo: existeRegistro, verificaa si existe un registro en la base de datos
     * con esos parametros
     * @autor : Ing. Diogenes Bastidas Morales
     * @param :
     * @version : 1.0
     */
    public boolean existeRegistro(String campos, String tabla, String condicion) throws SQLException{
        return formulariodao.existeRegistro(campos, tabla, condicion);
    }
    
    /**
     * Metodo: validarCampo, verificaa si existe un registro del campo a verificar
     * con los parametros definidos
     * @autor : Ing. Diogenes Bastidas Morales
     * @param :
     * @version : 1.0
     */
    public boolean validarCampo( String SQL ) throws Exception {
        try{
            return formulariodao.validarCampo(SQL);
        }
        catch (Exception ex){
            throw new Exception( ex.getMessage() );
        }
    }
    
    /**
     * Metodo: insertFormato_tabla, permite ingresar un registro a la tabla formato_tabla.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void actualizar(Vector registro) throws Exception{
        String datos = "", condicion="",tabla ="";
        try{
            for(int i=0; i<registro.size();i++){
                Formato_tabla formato = (Formato_tabla) registro.get(i);
                tabla = formato.getTabla();
                //arma las condiciones
                if(formato.getPrimaria().equals("S")){
                    condicion  +=" "+formato.getCampo_tabla()+" = "+
                    datosSQL(formato.getTipo_campo() , formato.getValor_campo().toUpperCase())+
                    " AND";
                }else{
                    datos += formato.getCampo_tabla()+" = "+datosSQL(formato.getTipo_campo() , formato.getValor_campo())+",";
                }
            }
            if(datos.length()>0){
                datos = datos.substring(0,datos.length()-1);
                condicion = condicion.substring(0,condicion.length()-3);
            }
            //System.out.println("tabla "+tabla+ " Datos "+ datos +" Condicion "+condicion);
            formulariodao.actualizar(tabla, datos, condicion);
        }catch(Exception ex){
            ex.printStackTrace();
            throw new Exception( ex.getMessage() );
        }
    }
    
    /**
     * Metodo: existeRegistro, verificaa si existe un registro en la base de datos
     * con esos parametros
     * @autor : Ing. Diogenes Bastidas Morales
     * @param :
     * @version : 1.0
     */
    public boolean buscarRegistro(Vector cam_busqueda, Vector campos_form)throws Exception{
        String SQL = "";
        llaves = "";
        datos = "";
        validacion =  new Vector();
        for(int i=0; i < cam_busqueda.size(); i++){
            Formato_tabla formato = (Formato_tabla) cam_busqueda.get(i);
            this.tabla = formato.getTabla();
            //crea la cadena de las llaves primarias
            if(formato.getPrimaria().equals("S")){
                this.llaves  +=" "+formato.getCampo_tabla()+" = "+
                datosSQL(formato.getTipo_campo() , formato.getValor_campo().toUpperCase())+
                " AND";
            }
        }
        
        if(llaves.length()>0){
            llaves = llaves.substring(0,llaves.length()-3);
        }
        try{
            return  formulariodao.buscarRegistro(tabla, llaves, campos_form);
        }catch(Exception ex){
            throw new Exception( ex.getMessage() );
        }
    }
    
    
    /**
     * Metodo: eliminar, permite eliminar un registro de la base de datos.
     * @autor : Ing. Diogenes Bastidas
     * @param :
     * @version : 1.0
     */
    public void eliminar( Vector registro ) throws Exception{
        String condicion="",tabla ="";
        try{
            for(int i=0; i<registro.size();i++){
                Formato_tabla formato = (Formato_tabla) registro.get(i);
                tabla = formato.getTabla();
                //arma las condiciones
                if(formato.getPrimaria().equals("S")){
                    condicion  +=" "+formato.getCampo_tabla()+" = "+
                    datosSQL(formato.getTipo_campo() , formato.getValor_campo().toUpperCase())+
                    " AND";
                }
            }
            if(condicion.length()>0){
                condicion = condicion.substring(0,condicion.length()-3);
            }
            //System.out.println("tabla "+tabla+ " Condicion "+condicion);
            formulariodao.eliminar(tabla, condicion);
        }catch(Exception ex){
            ex.printStackTrace();
            throw new Exception( ex.getMessage() );
        }
    }
    
     /**
     * Metodo: metodo que identifica si existe el campo en el formato
     * @autor : Ing. Diogenes Bastidas
     * @param : registros y el campo
     * @version : 1.0
     */
    public boolean existeCampo( Vector registro, String campo ) throws Exception{
        boolean est =  false;
        try{
            for(int i=0; i<registro.size();i++){
                Formato_tabla formato = (Formato_tabla) registro.get(i);
                if(formato.getCampo_tabla().equals(campo)){
                    est = true;
                    break;
                }
            }
            return est;
        }catch(Exception ex){
            throw new Exception( ex.getMessage() );
        }
    }
    
    /**
     * Metodo: eliminar campos agregados
     * @autor : Ing. Diogenes Bastidas
     * @param : registros y el campo
     * @version : 1.0
     */
    public Vector eliminarCamposFacturaMigracion( Vector registro ) throws Exception{
        String [] campos = new String[] {"formato","dstrct","creation_user","creation_date","user_update","last_update"};
        Vector copia = new Vector();
        boolean sw = true;
        try{
            for(int i=0; i<registro.size();i++){
                Formato_tabla formato = (Formato_tabla) registro.get(i);
                //System.out.println(formato.getCampo_tabla());
                sw = true;
                for(int j=0; j<campos.length; j++){
                    if(formato.getCampo_tabla().equals(campos[j]) ){
                        sw = false; 
                        break;
                    }
                }
                if(sw){
                    copia.add(formato);
                }
            }
            return copia;
        }catch(Exception ex){
            throw new Exception( ex.getMessage() );
        }
    }
    
    
}


