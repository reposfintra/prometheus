/*
 * Nombre        ReporteTiemposDeViajeConductoresService.java
 * Descripci�n   Clase que brinda los servicios al modelo para el acceso a los datos
 *               del reporte de tiempos de viajes para conductores.
 * Autor         Alejandro Payares
 * Fecha         24 de enero de 2006, 08:51 AM
 * Version       1.0
 * Coyright      Transportes Sanchez Polo SA.
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.ReporteTiemposDeViajeConductoresDAO;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 * Clase que brinda los servicios al modelo para el acceso a los datos
 * del reporte de tiempos de viajes para conductores.
 * @author Alejandro Payares
 */
public class ReporteTiemposDeViajeConductoresService {
    
    /**
     * Objeto para el acceso a los datos del reporte.
     */
    private ReporteTiemposDeViajeConductoresDAO dao;
    
    /**
     * Crea una nueva instancia de ReporteTiemposDeViajeConductoresService
     * @autor  Alejandro Payares
     */
    public ReporteTiemposDeViajeConductoresService() {
        dao = new ReporteTiemposDeViajeConductoresDAO();
    }
    
    /**
     * Se encarga de realizar la consulta para obtener los datos del reporte.
     * @param fechaInicio La fecha de inicio de consulta del reporte
     * @param fechaFin La fecha de fin de consulta del reporte
     * @param cliente El cliente de consulta del reporte
     * @param placa La placa de consulta del reporte
     * @param origen El origen de consulta del reporte
     * @param destino El destino de consulta del reporte
     * @throws SQLException Si algun error ocurre al conectarse a la base de datos
     */    
    public void buscarDatosReporte(String fechaInicio, String fechaFin, String cliente, String placa, String origen, String destino)throws SQLException{
        dao.buscarDatosReporte(fechaInicio, fechaFin, cliente, placa, origen, destino);
    }
    
    /**
     * Permite obtener la lista que contiene todos los datos del reporte.
     * @return Una lista de objetos <CODE>Hashtable</CODE> conteniendo la informaci�n de los viajes
     * y sus tiempos.
     */    
    public LinkedList obtenerDatosReporte(){
        return dao.obtenerDatosReporte();
    }
    
    /**
     * Devuelve el numero m�ximo de d�as que mostrara el reporte, este valor es util para saber
     * cuantas columnas tendr� la tabla que muestra los datos ya sea en la p�gina Web o en el
     * archivo excel.
     * @return EL numero de dias m�ximo encontrado en alguna placa del reporte
     */    
    public int obtenerNumeroMaximoDeDias(){
        return dao.obtenerNumeroMaximoDeDias();
    }
    
    public void borrarDatos(){
        dao.borrarDatos();
    }
}
