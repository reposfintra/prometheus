/***********************************************************************************
 * Nombre clase : ............... DespachoManualService.java                            *
 * Descripcion :................. Clase que instancia los metodos de CXPImpItemService
 *
 *                                con la BD.                                            *
 * Autor :....................... Ing. David Lamadrid                                   *
 * Fecha :....................... 15 de noviembre de 2005, 11:02 AM                     *
 * Version :..................... 1.0                                                   *
 * Copyright :................... Fintravalores S.A.                               *
 ***********************************************************************************/

package com.tsp.operation.model.services;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;
/**
 *
 * @author  dlamadrid
 */
public class CXPImpItemService
{
        
        public CXPImpItemDAO dao = new CXPImpItemDAO ();
        /** Creates a new instance of CXPImpItemService */
        public CXPImpItemService ()
        {
        }
        
        /**
         * Metodo que retorna un Objeto tipo CXPImpItem|
         * @autor.......David Lamadrid
         * @param.......Vector de Objetos tipo CXPImpDoc
         * @see........ CXPImpDoc.class
         * @throws.......
         * @version.....1.0.
         * @return......
         */
        public com.tsp.operation.model.beans.CXPImpItem getImpuestoItem ()
        {
                return dao.getImpuestoItem ();
        }
        
        /**
         * Metodo que retorna un Objeto tipo CXPImpItem|
         * @autor.......David Lamadrid
         * @param.......Vector de Objetos tipo CXPImpDoc
         * @see........ CXPImpDoc.class
         * @throws.......
         * @version.....1.0.
         * @return......
         */
        public void setImpuestoItem (com.tsp.operation.model.beans.CXPImpItem impuestoItem)
        {
                dao.setImpuestoItem (impuestoItem);
        }
        
        /**
         * Metodo que retorna un Objeto tipo CXPImpItem|
         * @autor.......David Lamadrid
         * @param.......Vector de Objetos tipo CXPImpDoc
         * @see........ CXPImpDoc.class
         * @throws.......
         * @version.....1.0.
         * @return......
         */
        public java.util.Vector getVImpuestosItem ()
        {
                return dao.getVImpuestosItem ();
        }
        
        public void setVImpuestosItem (java.util.Vector vImpuestosItem)
        {
                dao.setVImpuestosItem (vImpuestosItem);
        }
}
