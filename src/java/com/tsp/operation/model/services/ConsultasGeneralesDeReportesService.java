/*
 * Nombre        ConsultasGeneralesDeReportesService.java
 * Descripci�n   Brinda los servicios para el acceso a los datos de las consultas
 *               generales de los reportes configurables.
 * Autor         Alejandro Payares
 * Fecha         1 de marzo de 2006, 11:28 AM
 * Version       1.0
 * Coyright      Transportes Sanchez Polo SA.
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.ConsultasGeneralesDeReportesDAO;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Vector;


/**
 * Brinda los servicios para el acceso a los datos de las consultas generales de
 * los reportes configurables.
 * @author Alejandro Payares
 */
public class ConsultasGeneralesDeReportesService {
    
    /**
     * Objeto para el acceso a los datos de las consultas generales de los reportes.
     */
    private ConsultasGeneralesDeReportesDAO dao;
    
    /**
     * Crea una nueva instancia de ConsultasGeneralesDeReportesService
     * @autor  Alejandro Payares
     */
    public ConsultasGeneralesDeReportesService() {
        dao = new ConsultasGeneralesDeReportesDAO();
    }
    
    /**
     * Busca los campos de un reporte para un cliente espec�fico o para el usuario dado.
     * si ninguna configuraci�n de reporte es encontrada para el cliente o usuario dado
     * es buscada entonces la lista de campos generales para el reporte dado a buscar.
     * @param codigoCliente el codigo del cliente al cual se le buscar� la configuraci�n espec�fica.
     * @param codigoReporte el c�digo del reporte a buscar.
     * @param tipocliente el tipo de cliente (C = cliente, D = destinatario)
     * @param loggedUser El usuario en sessi�n. A este usuario se le buscar� una configuraci�n de reporte
     * para el reporte dado. si no es encontrada se buscan los campos generales del reporte.
     * @throws SQLException si algun error ocurre en el acceso a los datos.
     * @return Un array de tipo String que contiene los campos encontrados para los parametros dados.
     */    
    public String [] buscarCamposDeReporte(String codigoCliente,String codigoReporte, String tipocliente, String loggedUser) throws SQLException{
        return dao.buscarCamposDeReporte(codigoCliente, codigoReporte, tipocliente, loggedUser);
    }
    
    
    /**
     * Busca los campos generales del reporte dado.
     * @param codigoReporte el codigo del reporte a buscar sus campos
     * @throws SQLException Si algun error ocurre en el acceso a los datos.
     * @return Un array de tipo String que contiene los campos encontrados para los parametros dados.
     */    
    public String [] buscarCamposDeReporte(String codigoReporte) throws SQLException{
        return this.buscarCamposDeReporte("(ninguno)", codigoReporte, "", "(ninguno)");
    }
    
    /**
     * Devuelve un Hashtable que contiene los campos con sus respectivos titulos. donde el
     * cada entrada al hash es una pareja llave=nombre del campo y valor=titulo del campo.
     * @param codReporte el codigo del reporte al que se le buscar�n los titulos.
     * @throws SQLException si algun error ocurre en el acceso a los datos.
     * @return un hash con los titulos de los campos del reporte.
     */    
    public Hashtable obtenerTitulosDeReporte(String codReporte)throws SQLException{
        return dao.obtenerTitulosDeReporte(codReporte);
    }
    
    /**
     * Busca el detalle de los campos del reporte dado.
     * @param codReporte El codigo del reporte a buscar
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     * @autor Alejandro Payares
     */
    public void buscarDetalleDeCampos(String codReporte)throws SQLException {
        dao.buscarDetalleDeCampos(codReporte);
    }
    
    /**
     * Devuelve un vector con el detalle de todos los campos del reporte buscado por el metodo buscarDetalleDeCampos
     * @return vector con el detalle de todos los campos del reporte buscado por el metodo buscarDetalleDeCampos
     * @autor Alejandro Payares
     */    
    public Vector obtenerVectorDetalleCampos(){
        return dao.obtenerVectorDetalleCampos();
    }
}
