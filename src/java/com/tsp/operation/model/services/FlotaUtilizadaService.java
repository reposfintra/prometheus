/************************************************************************************
 * Nombre clase : ............... FlotaUtilizadaService.java                        *
 * Descripcion :................. Clase que maneja los Servicios                    *
 *                                asignados al Model relacionados con el            *
 *                                programa que genera el reporte de flotaUtilizada  * 
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                      *
 * Fecha :....................... 9 de Diciembre de 2005, 4:02 PM                   *
 * Version :..................... 1.0                                               *
 * Copyright :................... Fintravalores S.A.                           *
 ************************************************************************************/
package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;

public class FlotaUtilizadaService {
    
    public FlotaUtilizadaDAO dao = new FlotaUtilizadaDAO();
    
    public FlotaUtilizadaService() {
    }
    /**
     * Metodo buscarPlanillasEntregadas, Busca las planillas que llegaron de la tabla trafimo
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : String fecha inicial String fecha final
     * @see : buscarPlanillasEntregadas      -      FlotaUtilizadaDAO.java
     * @version : 1.0
     */
    public void buscarPlanillasEntregadas(String fechai, String fechaf) throws SQLException {
        dao.buscarPlanillasEntregadas(fechai, fechaf);
    }
    /**
     * Metodo buscarPlanillasSalidas, Busca las planillas despachadas de la tabla planilla
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : String fecha inicial String fecha final
     * @see : buscarPlanillasSalidas      -      FFlotaUtilizadaDAO.java
     * @version : 1.0
     */
    public void buscarPlanillasSalidas(String fechai, String fechaf) throws SQLException {
        dao.buscarPlanillasSalidas(fechai, fechaf);
    }
    /**
     * Metodo ingresarFlotaUtilizada, Agrega una flota a la tabla de flota_utilizada
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : String fecha inicial String fecha final
     * @see : buscarPlanillasSalidas      -      FlotaUtilizadaDAO.java
     * @version : 1.0
     */
    public void ingresarFlotaUtilizada(FlotaUtilizada flota) throws SQLException {
        dao.ingresarFlotaUtilizada(flota);
    }
    public Vector getVectorflotas() {
        return dao.getVectorFlotas();
    }
    /**
     * Metodo buscarVehiculosConEntradaSalida, Guarda en un vector los vehiculos que
     * hayan entrado y despues hayan sido despachados de una angencia dada
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @see : buscarVehiculosConEntradaSalida      -      FFlotaUtilizadaDAO.java
     * @version : 1.0
     */
    public void buscarVehiculosConEntradaSalida() throws SQLException {
        dao.buscarVehiculosConEntradaSalida();
    }
    /**
     * Metodo buscarVehiculosConEntradaSinSalida,Agreega al vector de floas los vehiculos
     * que tienen tipo Enrega y no  se les dio salida.
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @see : buscarVehiculosConEntradaSinSalida      -      FlotaUtilizadaDAO.java
     * @version : 1.0
     */
    public void buscarVehiculosConEntradaSinSalida() throws SQLException {
        dao.buscarVehiculosConEntradaSinSalida();
    }
    /**
     * Metodo buscarVehiculosConSalidaSinEntrada, Agrega al vector de flotas los vehiculos
     * que tienen salida desde la tabla de planillas
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @see : buscarVehiculosConSalidaSinEntrada      -     FlotaUtilizadaDAO.java
     * @version : 1.0
     */
    public void buscarVehiculosConSalidaSinEntrada() throws SQLException {
        dao.buscarVehiculosConSalidaSinEntrada();
    }
    /**
     * Metodo ingresarFlotaUtilizada, Agrega una flota a la tabla de flota_utilizada
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @see : buscarDatosResumen      -      FlotaUtilizadaDAO.java
     * @param : String fecha inicial String fecha final
     * @version : 1.0
     */
    public Vector buscarDatosResumen() throws SQLException {
        return dao.buscarDatosResumen();
    }
    /**
     * Metodo buscarVehiculosConSalidaSinEntrada, Agrega al vector de flotas los vehiculos
     * que tienen salida desde la tabla de planillas
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @see : getResumenFlota      -      FlotaUtilizadaDAO.java
     * @version : 1.0
     */
    public  Vector getResumenFlota() throws SQLException {
        return dao.getResumenFlota();
    }
}
