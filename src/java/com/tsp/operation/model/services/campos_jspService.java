/********************************************************************
 *      Nombre Clase.................   campos_jspService.java    
 *      Descripci�n..................   Service de campos_jspDAO
 *      Autor........................   Rodrigo Salazar
 *      Fecha........................   22.06.2005
 *      Versi�n......................   1.5
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

public class campos_jspService {
    private campos_jspDAO camposjsp;
    private Vector campos;
    
    /**
     * Obtiene la propiedad campos
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public Vector getCampos(){
        return campos;
    }
    /** Creates a new instance of campos_jspService */
    public campos_jspService() {
        camposjsp = new campos_jspDAO();
    }
    
    /**
     * Obtiene la propiedad campos_jsp de la clase campos_jspDAO
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public campos_jsp getcampos_jsp() {
        return camposjsp.getcampos_jsp();
    }
    
    /**
     * Obtiene la propiedad campos_jsps de la clase campos_jspDAO
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public Vector getcampos_jsps() {
        return camposjsp.getcampos_jsps();
    }
    
    /**
     * Establece el valor de la propiedad campos_jsps de la clase campos_jspDAO
     * @param campos Valor de la propiedad campos_jsps
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public void setcampos_jsps(Vector camposjsps) {
        camposjsp.setcampos_jsps(camposjsps);
    }
    
    /**
     * Inserta un nuevi registro en la tabla campos_jsp
     * @autor Rodrigo Salazar
     * @param tipo Campo a insertar
     * @throws SQLException
     * @version 1.0
     */
    public void insertcampos_jsp(campos_jsp tipo) throws SQLException{
        try{
            camposjsp.setcampos_jsp(tipo);
            camposjsp.insertcampos_jsp();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Establce si existe un determinado campo en la tabla
     * la p�gina JSP dada
     * @autor Rodrigo Salazar
     * @param jsp C�digo de la p�gina JSP
     * @param campo Nombre del campo
     * @throws SQLException
     * @version 1.0
     */
    public boolean existcampos_jsp(String jsp,String campo) throws SQLException{
        return camposjsp.existcampos_jsp(jsp,campo);
    }
    
    /**
     * Busca todos los registros en la tabla campos_jsp con el c�digo de
     * la p�gina JSP dada
     * @autor Rodrigo Salazar
     * @param cod C�digo de la p�gina JSP
     * @throws SQLException
     * @version 1.0
     */
    public void serchcampos_jsp(String cod)throws SQLException{
        camposjsp.searchcampos_jsp(cod);
    }
    
    /**
     * Lista todos lo registros de la tabla campos_jsp
     * @autor Rodrigo Salazar
     * @throws SQLException
     * @version 1.0
     */
    public void listcampos_jsp()throws SQLException{
        camposjsp.listcampos_jsp();
    }
    
    /**
     * Actualiza los registros en la tabla campos_jsp relacionados
     * a una p�gina determinada.
     * @autor Rodrigo Salazar
     * @param cod C�digo de la p�gina JSP
     * @param desc Nombre del campo
     * @param usu Login del usuario
     * @throws SQLException
     * @version 1.0
     */
    public void updatecampos_jsp(String cod,String desc,String usu)throws SQLException{
        camposjsp.updatecampos_jsp(cod, desc, usu);
    }
    
    /**
     * Anula un registro en la tabla campos_jsp
     * @autor Rodrigo Salazar
     * @param cod C�digo de la p�gina JSP
     * @param campo Nombre del campo
     * @throws SQLException
     * @version 1.0
     */
    public void anularcampos_jsp(String cod,String campo)throws SQLException{
        camposjsp.anularcampos_jsp(cod,campo);
    }
    
    /**
     * Anula los registros en la tabla campos_jsp relacionados
     * a una p�gina determinada.
     * @autor Rodrigo Salazar
     * @param cod C�digo de la p�gina JSP
     * @throws SQLException
     * @version 1.0
     */
    public void anulartodoscampos_jsp(String cod)throws SQLException{
        camposjsp.anulartodoscampos_jsp(cod);
    }
    
    /**
     * Obtiene los registros en la tabla campos_jsp relacionados
     * a una p�gina determinada.
     * @autor Rodrigo Salazar
     * @param cod C�digo de la p�gina JSP
     * @throws SQLException
     * @version 1.0
     */
    public Vector searchDetallecampos_jsps(String cod) throws SQLException{
        try{
            campos = new Vector();
            campos = camposjsp.searchDetallecampos_jsps(cod);
            return campos;
            
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Obtiene los registros en la tabla campos_jsp relacionados
     * a una p�gina determinada.
     * @autor Rodrigo Salazar
     * @param pag C�digo de la p�gina JSP
     * @throws SQLException
     * @version 1.0
     */
    public Vector listarCampos_jsp(String pag) throws SQLException{
        try{
            return camposjsp.listarCampos_jsp(pag);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Lee una archivo JSP y carga los campos y los almacena en la tabla campos_jsp
     * @autor Rodrigo Salazar
     * @param cod C�digo de la p�gina JSP
     * @param carpeta Ruta f�sica del archivo
     * @param jsp Nombre de la p�gina JSP
     * @param user Login del usuario
     * @throws SQLException
     * @version 1.5
     */
    public void leeArchivos(String cod,String carpeta, String jsp, String usuario) throws SQLException{
        try{
            anulartodoscampos_jsp(cod);
            camposjsp.leeArchivo(cod,carpeta,jsp,usuario);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    
    /**
     * Lee una archivo JSP y carga los campos y luego los almacena en la tabla campos_jsp
     * @autor Tito Andr�s Maturana D.
     * @param cod C�digo de la p�gina JSP
     * @param carpeta Ruta f�sica del archivo
     * @param jsp Nombre de la p�gina JSP
     * @param user Login del usuario
     * @see com.tsp.operation.model.DAOS.campos_jspDAO#cargarCampos(String,String,String,String)
     * @return Arreglo de tipo <code>Vector</code> con los campos enconrtrados
     * @throws SQLException
     * @version 1.0
     */
    public Vector cargarCampos(String cod, String carpeta, String jsp, String user) throws SQLException{
        return this.camposjsp.cargarCampos(cod, carpeta, jsp, user);
    }
    
    /**
     * Lee una archivo JSP y rescribe los tags segun los tipos de campos.
     * @autor Leonardo Parody Ponce.
     * @param nombre del campo
     * @param  tipo del campo
     * @param nombre de la p�gina JSP
     * @param carpeta Ruta f�sica del archivo
     * @param user Login del usuario
     * @throws SQLException
     * @version 1.0
     */
    
    public void archivojsp (String carpeta, String jsp, String campo, String tipo) throws SQLException{
        try{
            camposjsp.archivosjsp(carpeta, jsp, campo, tipo);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    
    /**
     * Establce si existe un determinado campo en la tabla
     * la p�gina JSP dada
     * @autor Ibg. Andr�s Maturana
     * @param jsp C�digo de la p�gina JSP
     * @param campo Nombre del campo
     * @throws SQLException
     * @version 1.0
     */
    public String existecampos_jsp(String jsp, String campo) throws SQLException{
        return camposjsp.existecampos_jsp(jsp, campo);
    }
    
}
