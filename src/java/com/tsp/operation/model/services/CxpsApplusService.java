/* * CxpsApplusService.java * Created on 23 de julio de 2009, 15:42 */
package com.tsp.operation.model.services;
import com.tsp.operation.model.DAOS.CxpsApplusDAO;
import java.util.ArrayList;
import java.util.Vector;
/** * @author  Fintra */
public class CxpsApplusService {    
    private CxpsApplusDAO cxpsApplusDAO;
    public CxpsApplusService() {
         cxpsApplusDAO= new CxpsApplusDAO();
    }    
    public CxpsApplusService(String dataBaseName) {
         cxpsApplusDAO= new CxpsApplusDAO(dataBaseName);
    }    
    public String buscarCxpDeCxcApp(String cxc_app)throws Exception{        
        return cxpsApplusDAO.buscarCxpDeCxcApp(cxc_app);        
    }
    public ArrayList getCxpDeCxcApp(){
        return cxpsApplusDAO.getCxpDeCxcApp();
    }
    
    public Vector bajarCxps(String[] idordenes,ArrayList cxps,String abonoPrefactura,String descripcionPrefactura,String loginx,String fecfac) throws Exception{
        return cxpsApplusDAO.bajarCxps(idordenes,cxps,abonoPrefactura,descripcionPrefactura,loginx,fecfac);
    }
    public String buscarCxpDeCxcBoni(String cxc_boni)throws Exception{        
        return cxpsApplusDAO.buscarCxpDeCxcBoni(cxc_boni);        
    }
     
    public ArrayList getCxpDeCxcBoni(){
        return cxpsApplusDAO.getCxpDeCxcBoni();
    }
    
    public Vector bajarCxpsBoni(String[] idacciones,ArrayList cxps,String abonoPrefactura,String descripcionPrefactura,String loginx,String fecfac) throws Exception{
        return cxpsApplusDAO.bajarCxpsBoni(idacciones,cxps,abonoPrefactura,descripcionPrefactura,loginx,fecfac);
    } 
    
    public String buscarCxpDeCxcPr(String cxc_pr)throws Exception{        
        //.out.println("ante s de pppr");
        return cxpsApplusDAO.buscarCxpDeCxcPr(cxc_pr);        
    }
    public ArrayList getCxpDeCxcPr(){
        //.out.println("antes de pr");
        return cxpsApplusDAO.getCxpDeCxcPr();
    }
    public Vector bajarCxpsPr(String[] idordenes,ArrayList cxps,String abonoPrefactura,String descripcionPrefactura,String loginx,String fecfac) throws Exception{
        return cxpsApplusDAO.bajarCxpsPr(idordenes,cxps,abonoPrefactura,descripcionPrefactura,loginx,fecfac);
    }
    public String buscarCxpDeCxcPf(String cxc_pf)throws Exception{        
        return cxpsApplusDAO.buscarCxpDeCxcPf(cxc_pf);        
    }
    public ArrayList getCxpDeCxcPf(){
        return cxpsApplusDAO.getCxpDeCxcPf();
    }
    public Vector bajarCxpsPf(String[] idacciones,ArrayList cxps,String abonoPrefactura,String descripcionPrefactura,String loginx,String fecfac) throws Exception{
        return cxpsApplusDAO.bajarCxpsPf(idacciones,cxps,abonoPrefactura,descripcionPrefactura,loginx,fecfac);
    }
    
    public String buscarCxpDeCxcAr(String cxc_ar)throws Exception{        
        return cxpsApplusDAO.buscarCxpDeCxcAr(cxc_ar);        
    }
    public ArrayList getCxpDeCxcAr(){
        return cxpsApplusDAO.getCxpDeCxcAr();
    }
    public Vector bajarCxpsAr(String[] idacciones,ArrayList cxps,String abonoPrefactura,String descripcionPrefactura,String loginx,String fecfac) throws Exception{
        return cxpsApplusDAO.bajarCxpsAr(idacciones,cxps,abonoPrefactura,descripcionPrefactura,loginx,fecfac);
    }
}
