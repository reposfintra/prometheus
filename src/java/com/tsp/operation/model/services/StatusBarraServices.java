/***************************************
    * Nombre Clase ............. StatusBarraServices.java
    * Descripci�n  .. . . . . .  Sevicio para el estado pendientes
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  25/08/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/




package com.tsp.operation.model.services;




import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.StatusBarraDAO;
import javax.servlet.*;
import javax.servlet.http.*;



public class StatusBarraServices {
    
    
    private  StatusBarraDAO  StatusBarraDataAccess;
    
    
// Anticipos:
    private int  CANT_ANTICIPOS_POR_APROBAR;
    private int  CANT_ANTICIPOS_POR_TRANSFERIR;
    
  
// Liquidaciones:
    private int  CANT_LIQ_POR_APROBAR;
    private int  CANT_LIQ_POR_MIGRAR;
  
    
    
    
    
    public StatusBarraServices() {
        StatusBarraDataAccess  =  new StatusBarraDAO();
    }
    
    
    
    
    
    
  
    
    //--------------------------------------------------------------------------
    // ANTICIPOS :
    
    /**
     * M�todo que busca la cantidad de anticipos por aprobar
     * @autor.......fvillacob
     * @version.....1.0.
     **/
    public  void  ANTICIPOS_Aprobar(String distrito, String proveedor)throws Exception{
        try{
            CANT_ANTICIPOS_POR_APROBAR = this.StatusBarraDataAccess.ANTICIPOS_Aprobar(distrito, proveedor);
        }catch(Exception e){
            throw new Exception( " ANTICIPOS_Aprobar "+ e.getMessage() );
        }
    }
    
    
    
    
    
    /**
     * M�todo que busca la cantidad de anticipos por transferir
     * @autor.......fvillacob
     * @version.....1.0.
     **/
    public  void ANTICIPOS_Transferir(String distrito, String proveedor)throws Exception{
        try{
            CANT_ANTICIPOS_POR_TRANSFERIR = this.StatusBarraDataAccess.ANTICIPOS_Transferir(distrito, proveedor);
        }catch(Exception e){
            throw new Exception( " ANTICIPOS_Transferir "+ e.getMessage() );
        }
    }
    
    
    
    
    
    
    //--------------------------------------------------------------------------
    // LIQUIDACIONES :
    
    /**
     * M�todo que busca la cantidad de liquidaciones por aprobar
     * @autor.......fvillacob
     * @version.....1.0.
     **/
    public  void LIQUIDACION_Aprobar()throws Exception{
        try{
            CANT_LIQ_POR_APROBAR  = this.StatusBarraDataAccess.getLIQUIDACION_Aprobar();
        }catch(Exception e){
            throw new Exception( " LIQUIDACION_Aprobar "+ e.getMessage() );
        }
    }
    
    
    
    
    /**
     * M�todo que busca la cantidad de liquidaciones por Migrar
     * @autor.......fvillacob
     * @version.....1.0.
     **/
    public  void  LIQUIDACION_Migrar()throws Exception{
        try{
           CANT_LIQ_POR_MIGRAR = this.StatusBarraDataAccess.getLIQUIDACION_Migrar(); 
        }catch(Exception e){
            throw new Exception( " LIQUIDACION_Migrar "+ e.getMessage() );
        }
    }
    
    
    
    
    
    
    
    
    
    //--------------------------------------------------------------------------
    // SET AND GET :
    
    
    /**
     * Getter for property CANT_ANTICIPOS_POR_APROBAR.
     * @return Value of property CANT_ANTICIPOS_POR_APROBAR.
     */
    public int getCANT_ANTICIPOS_POR_APROBAR() {
        return CANT_ANTICIPOS_POR_APROBAR;
    }    

    /**
     * Setter for property CANT_ANTICIPOS_POR_APROBAR.
     * @param CANT_ANTICIPOS_POR_APROBAR New value of property CANT_ANTICIPOS_POR_APROBAR.
     */
    public void setCANT_ANTICIPOS_POR_APROBAR(int CANT_ANTICIPOS_POR_APROBAR) {
        this.CANT_ANTICIPOS_POR_APROBAR = CANT_ANTICIPOS_POR_APROBAR;
    }   
    
    
   
    /**
     * Getter for property CANT_ANTICIPOS_POR_TRANSFERIR.
     * @return Value of property CANT_ANTICIPOS_POR_TRANSFERIR.
     */
    public int getCANT_ANTICIPOS_POR_TRANSFERIR() {
        return CANT_ANTICIPOS_POR_TRANSFERIR;
    }    
    
    /**
     * Setter for property CANT_ANTICIPOS_POR_TRANSFERIR.
     * @param CANT_ANTICIPOS_POR_TRANSFERIR New value of property CANT_ANTICIPOS_POR_TRANSFERIR.
     */
    public void setCANT_ANTICIPOS_POR_TRANSFERIR(int CANT_ANTICIPOS_POR_TRANSFERIR) {
        this.CANT_ANTICIPOS_POR_TRANSFERIR = CANT_ANTICIPOS_POR_TRANSFERIR;
    }    
    
    
    
    /**
     * Getter for property CANT_LIQ_POR_APROBAR.
     * @return Value of property CANT_LIQ_POR_APROBAR.
     */
    public int getCANT_LIQ_POR_APROBAR() {
        return CANT_LIQ_POR_APROBAR;
    }    
    
    /**
     * Setter for property CANT_LIQ_POR_APROBAR.
     * @param CANT_LIQ_POR_APROBAR New value of property CANT_LIQ_POR_APROBAR.
     */
    public void setCANT_LIQ_POR_APROBAR(int CANT_LIQ_POR_APROBAR) {
        this.CANT_LIQ_POR_APROBAR = CANT_LIQ_POR_APROBAR;
    }    
    
    
    
    /**
     * Getter for property CANT_LIQ_POR_MIGRAR.
     * @return Value of property CANT_LIQ_POR_MIGRAR.
     */
    public int getCANT_LIQ_POR_MIGRAR() {
        return CANT_LIQ_POR_MIGRAR;
    }
    
    /**
     * Setter for property CANT_LIQ_POR_MIGRAR.
     * @param CANT_LIQ_POR_MIGRAR New value of property CANT_LIQ_POR_MIGRAR.
     */
    public void setCANT_LIQ_POR_MIGRAR(int CANT_LIQ_POR_MIGRAR) {
        this.CANT_LIQ_POR_MIGRAR = CANT_LIQ_POR_MIGRAR;
    }
    
}
