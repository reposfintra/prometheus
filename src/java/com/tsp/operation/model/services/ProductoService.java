/********************************************************************************************
 * Nombre clase: ProductoService.java                                                       *
 * Descripci�n: Clase que maneja los servicios al model relacionados con los productos.     *
 * Autor: Ing. Jose de la rosa                                                              *
 * Fecha: 17 de octubre de 2005, 11:25 AM                                                   *
 * Versi�n: Java 1.0                                                                        *
 * Copyright: Fintravalores S.A. S.A.                                                  *
 ********************************************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

public class ProductoService {
    private ProductoDAO producto;
    /** Creates a new instance of ProductoService */

    public ProductoService() {
        producto = new ProductoDAO();
    }
    
    public Producto getProducto( )throws SQLException{
        return producto.getProducto();
    }
    
    public Vector getProductos() {
        return producto.getProductos();
    }
    
    public void setProductos(Vector productos) {
        producto.setProductos(productos);
    }
    
     /**
     * M�todo que inserta una producto a la BD
     * @autor.......Jose de la rosa
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void insertProducto(Producto user) throws SQLException{
        try{
            producto.setProducto(user);
            producto.insertProducto();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
     /**
     * M�todo que nos permite saber si el producto existe o no
     * @autor.......Jose de la rosa
     * @param.......Recibe un codigo de producto
     * @throws......SQLException
     * @version.....1.0.
     **/
    public boolean existProducto(String cod, String distrito, String cliente) throws SQLException{
        return producto.existProducto(cod, distrito, cliente);
    }
    
     /**
     * M�todo que Busca un producto en la tabla  de producto
     * @autor.......Jose de la rosa
     * @param.......Recibe un codigo de producto
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void serchProducto(String cod,String distrito, String cliente)throws SQLException{
        producto.searchProducto(cod, distrito, cliente);
    }
    
     /**
     * M�todo que nos permite obtener un listado de todos los productos existentes en la BD
     * @autor.......Jose de la rosa
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void listProducto()throws SQLException{
        producto.listProducto();
    }
    
     /**
     * M�todo que permite modificar una producto a la BD
     * @autor.......Jose de la rosa
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void updateProducto(Producto user)throws SQLException{
        producto.setProducto(user);
        producto.updateProducto();
    }
    
     /**
     * M�todo que permite anular una producto a la BD
     * @autor.......Jose de la rosa
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void anularProducto(Producto unit)throws SQLException{
            try{
            producto.setProducto(unit);
            producto.anularProducto();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }  
    }
    

     /**
     * M�todo que nos permite buscar unos productos de acuerdo a unos filtros.
     * @autor.......Jose de la rosa
     * @param.......Recibe un codigo de producto, una descrpcion, un codigo unidad y un codigo cliente
     * @throws......SQLException
     * @version.....1.0.
     **/
    public Vector searchDetalleProductos(String cod, String desc, String uni, String cli) throws SQLException{
        try{
            return producto.searchDetalleProductos(cod, desc, uni, cli);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
     /**
     * M�todo que nos permite obtener un listado de todos los productos existentes en la BD
     * @autor.......Jose de la rosa
     * @throws......SQLException
     * @version.....1.0.
     **/
    public Vector listarProductos() throws SQLException{
        try{
            return producto.listarProductos();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }     

      /**
     * M�todo que busca una lista de productos dado un 
     * codigo de cliente y una ubicacion
     * @param.......cliente Recibe un codigo de cliente
     * @param.......ubicacion Recibe un codigo de ubicacion
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.     
     **/     
    public void listarProductos (String cliente, String ubicacion )throws SQLException{
        producto.listarProductos(cliente, ubicacion);
    }
     /**
     * M�todo que busca una lista de productos dado un
     * codigo de cliente y una ubicacion sin importar si estan utilizados
     * @param.......cliente Recibe un codigo de cliente
     * @param.......ubicacion Recibe un codigo de ubicacion
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void listarTodosProductos(String cliente, String ubicacion, String numpla )throws SQLException{
        producto.listarTodosProductos(cliente, ubicacion,numpla);
    }
     /**
     * M�todo que busca una lista de productos dado un 
     * codigo de cliente y una ubicacion
     * @param.......cliente Recibe un codigo de cliente
     * @param.......ubicacion Recibe un codigo de ubicacion
     * @autor.......David Pi�a
     * @throws......SQLException
     * @version.....1.0.     
     **/     
    public void listarProductosUbicacion (String cliente, String ubicacion )throws SQLException{
        producto.listarProductosUbicacion(cliente, ubicacion);
    }
}
