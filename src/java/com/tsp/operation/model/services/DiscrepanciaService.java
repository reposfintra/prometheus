/************************************************************************************
 * Nombre clase: DiscrepanciaService.java                                           *
 * Descripci�n: Clase que maneja los servicios al model relacionados con sanci�n.   *
 * Autor: Ing. Jose de la rosa                                                      *
 * Fecha: 23 de septiembre de 2005, 10:57 AM                                        *
 * Versi�n: Java 1.0                                                                *
 * Copyright: Fintravalores S.A. S.A.                                          *
 ***********************************************************************************/
package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  Jose
 */
public class DiscrepanciaService {
    
    private DiscrepanciaDAO discrepancia;
    private List ListaDiscrepancia;




    
    /** Creates a new instance of DiscrepanciaService */
    public DiscrepanciaService () {
        this.discrepancia = new DiscrepanciaDAO ();
        this.ListaDiscrepancia  = new LinkedList();
    }
    
    /**
     * Metodo: getDiscrepancia, permite retornar un objeto de registros de discrepancia.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Discrepancia getDiscrepancia ( )throws SQLException{
        return discrepancia.getDiscrepancia ();
    }
    
    /**
     * Metodo: getDiscrepancias, permite retornar un vector de registros de discrepancia.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Vector getDiscrepancias () {
        return discrepancia.getDiscrepancias ();
    }
    
     /**
     * Metodo: setDiscrepancia, permite retornar un objeto de registros de discrepancia.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void setDiscrepancia (Discrepancia dis) {
        discrepancia.setDiscrepancia (dis);
    }
    
        /**
     * Metodo: setDiscrepancias, permite obtener un vector de registros de discrepancia.
     * @autor : Ing. Jose de la rosa
     * @param : vector
     * @version : 1.0
     */
    public void setDiscrepancias (Vector Discrepancias) {
        discrepancia.setDiscrepancias (Discrepancias);
    }
    
     /**
    * Metodo insertarDiscrepancia, ingresa las discrepancias
    * (discrepancia).
    * @autor : Ing. Jose de la rosa
    * @see insertarDiscrepancia - DiscrepanciaDAO
    * @param : discrepancia
    * @version : 1.0
    */
    
    public String insertarDiscrepancia (Discrepancia discrepancia) throws java.sql.SQLException{
        return this.discrepancia.insertarDiscrepancia ();
    }
    
    /**
     * Metodo: insertarDiscrepanciaInventario, permite ingresar un registro a la tabla inventario_discrepancia.
     * @autor : Ing. Jose de la rosa
     * @see anularDiscrepanciaGeneral - DiscrepanciaDAO
     * @param :
     * @version : 1.0
     */
    public String insertarDiscrepanciaInventario (Discrepancia discrepancia) throws SQLException {
        this.discrepancia.setDiscrepancia (discrepancia);
        return this.discrepancia.insertarDiscrepanciaInventario ();
    }

    /**
    * Metodo insertarDiscrepanciaProducto, ingresa los productos relacionados con las discrepancias
    * (discrepancia).
    * @autor : Ing. Jose de la rosa
    * @see insertarDiscrepancia - DiscrepanciaDAO
    * @param : discrepancia
    * @version : 1.0
    */
    public String insertarDiscrepanciaProducto (Discrepancia discrepancia) throws java.sql.SQLException{
        return this.discrepancia.insertarDiscrepanciaProducto ();
    }
    
   
   
    /**
    * Metodo existeDiscrepancia, obtiene la informacion de las discrepancias dado unos parametros
    * (planilla, remesa, tipo documento,documento ).
    * @autor : Ing. Jose de la rosa
    * @see existeDiscrepancia - DiscrepanciaDAO
    * @param : numero planilla, numero remesa, tipo documento y documento
    * @version : 1.0
    */    
    public boolean existeDiscrepancia (String numpla, String numrem, String tipo_doc, String documento, String tipo_doc_rel, String documento_rel, String distrito ) throws java.sql.SQLException{
        return this.discrepancia.existeDiscrepancia (numpla, numrem, tipo_doc, documento, tipo_doc_rel, documento_rel, distrito );
    }
    
    /**
    * Metodo existeDiscrepanciaProducto, obtiene la informacion de los productos de discrepancias dado unos parametros
    * (discrepancia, planilla, remesa, tipo documento,documento, codigo producto y codigo discrepancia ).
    * @autor : Ing. Jose de la rosa
    * @see existeDiscrepanciaProducto - DiscrepanciaDAO
    * @param : numero discrepancia, numero planilla, numero remesa, tipo documento, documento, codigo producto y codigo discrepancia
    * @version : 1.0
    */    
    public boolean existeDiscrepanciaProducto (int num_discre, String numpla, String numrem, String tipo_doc, String documento, String cod_producto, String cod_cdiscrepancia, String distrito ) throws java.sql.SQLException{
        return this.discrepancia.existeDiscrepanciaProducto (num_discre, numpla, numrem, tipo_doc, documento, cod_producto, cod_cdiscrepancia, distrito );
    }
    
    /**
    * Metodo listDiscrepancia, lista la informacion de las discrepancias
    * @autor : Ing. Jose de la rosa
    * @see listDiscrepancia - DiscrepanciaDAO
    * @param :
    * @version : 1.0
    */    
    public void listDiscrepancia ()throws SQLException{
        discrepancia.listDiscrepancia ();
    }
    
    /**
    * Metodo listDiscrepanciaProducto, obtiene la informacion de los productos de discrepancias dado unos parametros
    * (discrepancia, planilla, remesa, tipo documento,documento).
    * @autor : Ing. Jose de la rosa
    * @see listDiscrepanciaProducto - DiscrepanciaDAO
    * @param : numero discrepancia, numero planilla, numero remesa, tipo documento, documento
    * @version : 1.0
    */  
    public void listDiscrepanciaProducto (int num_discre, String numpla, String numrem, String tipo_doc, String documento, String tipo_doc_rel, String documento_rel,String dstrct )throws SQLException{
        discrepancia.listDiscrepanciaProducto (num_discre, numpla, numrem, tipo_doc, documento,tipo_doc_rel,documento_rel, dstrct );
    }
    
    /**
    * Metodo searchDiscrepancia, permite buscar una discrepancia dado unos parametros
    * (planilla, remesa, tipo documento,documento).
    * @autor : Ing. Jose de la rosa
    * @see listDiscrepanciaProducto - DiscrepanciaDAO
    * @param : numero de la planilla, numero de la remesa, tipo de documento y documento.
    * @version : 1.0
    */  
    public void searchDiscrepancia (String numpla, String numrem, String tipo_doc, String documento, String tipo_doc_rel, String documento_rel, String distrito )throws SQLException{
        discrepancia.searchDiscrepancia (numpla, numrem, tipo_doc, documento, tipo_doc_rel, documento_rel, distrito );
    }
    
    /**
    * Metodo searchDiscrepanciaProducto, permite buscar un producto de discrepancia dado unos parametros
    * (discrepancia,planilla, remesa, tipo documento,documento).
    * @autor : Ing. Jose de la rosa
    * @see searchDiscrepanciaProducto - DiscrepanciaDAO
    * @param : numero de discrepancia, numero de la planilla, numero de la remesa, tipo de documento, documento
    * @version : 1.0
    */ 
    public void searchDiscrepanciaProducto (int num_discre, String numpla, String numrem, String tipo_doc, String documento, String tipo_doc_rel, String documento_rel, String cod_producto, String cod_cdiscrepancia, String fecha, String distrito)throws SQLException{
        discrepancia.searchDiscrepanciaProducto (num_discre, numpla, numrem, tipo_doc, documento, tipo_doc_rel, documento_rel, cod_producto, cod_cdiscrepancia, fecha, distrito );
    }
    
    /**
    * Metodo updateDiscrepancia, permite actualizar una de discrepancia
    * (discrepancia).
    * @autor : Ing. Jose de la rosa
    * @see updateDiscrepancia - DiscrepanciaDAO
    * @param : discrepancia
    * @version : 1.0
    */ 
    public String updateDiscrepancia (Discrepancia discrepancia)throws SQLException{
        this.discrepancia.setDiscrepancia (discrepancia);
        return this.discrepancia.updateDiscrepancia ();
    }
    
    /**
    * Metodo updateDiscrepanciaProducto, permite actualizar un producto de discrepancia
    * (discrepancia).
    * @autor : Ing. Jose de la rosa
    * @see updateDiscrepanciaProducto - DiscrepanciaDAO
    * @param : discrepancia
    * @version : 1.0
    */ 
    public String updateDiscrepanciaProducto (Discrepancia discrepancia)throws SQLException{
        this.discrepancia.setDiscrepancia (discrepancia);
        return this.discrepancia.updateDiscrepanciaProducto ();
    }
    
    /**
    * Metodo updateSancion, permite actualizar una sanci�n
    * (discrepancia).
    * @autor : Ing. Jose de la rosa
    * @see updateSancion - DiscrepanciaDAO
    * @param : discrepancia
    * @version : 1.0
    */     
    public void updateSancion (Discrepancia discrepancia)throws SQLException{
        this.discrepancia.setDiscrepancia (discrepancia);
        this.discrepancia.updateSancion ();
    }
    
    /**
    * Metodo searchDetalleDiscrepancia, permite listar discrepancias por detalles dados unos parametros.
    * (String reg, String discre, String numpla,String ref,String resp,String cont).
    * @autor : Ing. Jose de la rosa
    * @see searchDetalleDiscrepancia - DiscrepanciaDAO
    * @param : discrepancia
    * @version : 1.0
    */  
    public void searchDetalleDiscrepancia (String reg, String discre, String numpla,String ref,String resp,String cont) throws SQLException{
        try{
            discrepancia.searchDetalleDiscrepancia (reg, discre, numpla,ref,resp,cont);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
    /**
    * Metodo anularDiscrepancia, permite anular una discrepancia de la bd
    * (discrepancia).
    * @autor : Ing. Jose de la rosa
    * @see anularDiscrepancia - DiscrepanciaDAO
    * @param : discrepancia
    * @version : 1.0
    */      
    public String anularDiscrepancia ( Discrepancia discrepancia) throws SQLException{
        this.discrepancia.setDiscrepancia (discrepancia);
        return this.discrepancia.anularDiscrepancia ();
    }
    
    /**
    * Metodo anularDiscrepanciaProducto, permite anular un producto de discrepancia de la bd
    * (discrepancia).
    * @autor : Ing. Jose de la rosa
    * @see anularDiscrepanciaProducto - DiscrepanciaDAO
    * @param : discrepancia
    * @version : 1.0
    */ 
    public String anularDiscrepanciaProducto ( Discrepancia discrepancia) throws SQLException{
        this.discrepancia.setDiscrepancia (discrepancia);
        return this.discrepancia.anularDiscrepanciaProducto ();
    }
    
        /**
    * Metodo anularDiscrepanciaProducto, permite anular un producto de discrepancia de la bd
    * (discrepancia).
    * @autor : Ing. Jose de la rosa
    * @see anularDiscrepanciaProducto - DiscrepanciaDAO
    * @param : discrepancia
    * @version : 1.0
    */ 
    public String anularDiscrepanciaTodosProducto ( Discrepancia discrepancia) throws SQLException{
        this.discrepancia.setDiscrepancia (discrepancia);
        return this.discrepancia.anularDiscrepanciaTodosProducto ();
    }
    
    /**
    * Metodo anularDiscrepanciaProducto, permite anular un producto de la bd
    * (discrepancia).
    * @autor : Ing. Jose de la rosa
    * @see anularDiscrepanciaProducto - DiscrepanciaDAO
    * @param : discrepancia
    * @version : 1.0
    */ 
    public void anularProducto (Discrepancia discrepancia) throws SQLException{
        this.discrepancia.setDiscrepancia (discrepancia);
        this.discrepancia.anularProducto ();
    }
    
    /**
    * Metodo listDiscrepanciaConCierre, permite listar la informaci�n de dicrepancia
    *        con fecha de cierre.
    * (fecha inicio, fecha fin).
    * @autor : Ing. Jose de la rosa
    * @see listDiscrepanciaConCierre - DiscrepanciaDAO
    * @param : fecha inicio, fecha fin
    * @version : 1.0
    */ 
    public void listDiscrepanciaConCierre (String fini,String ffin)throws SQLException{
        discrepancia.listDiscrepanciaConCierre (fini, ffin);
    }
    
    /**
    * Metodo listDiscrepanciaSinCierre, permite listar la informaci�n de dicrepancia
    *        sin fecha de cierre.
    * @autor : Ing. Jose de la rosa
    * @see listDiscrepanciaSinCierre - DiscrepanciaDAO
    * @param : 
    * @version : 1.0
    */
    public void listDiscrepanciaSinCierre ()throws SQLException{
        discrepancia.listDiscrepanciaSinCierre ();
    }
    
    /**
    * Metodo listDiscrepanciaPlanilla, permite buscar una discrepancia dado unos parametros.
    * @autor : Ing. Jose de la rosa
    * @see listDiscrepanciaPlanilla - DiscrepanciaDAO
    * @param : numero de planilla
    * @version : 1.0
    */    
    public void listDiscrepanciaPlanilla (String numpla, String distrito)throws SQLException{
        discrepancia.listDiscrepanciaPlanilla (numpla, distrito);
    }
        /**
     *  @autor Rodrigo Salazar
     */
    public boolean existeDiscrepanciaPlanilla (String cod_discre) throws java.sql.SQLException{
        return this.discrepancia.existeDiscrepanciaPlanilla (cod_discre );
    }
        /**
     *  @autor Rodrigo Salazar
     */
    public void listDiscrepanciaTodas (String numpla)throws SQLException{
        discrepancia.listDiscrepanciaTodas (numpla);
    }
    
    /**
     *  @autor Tito Andr�s
     */
    public void listarSancionesXplanilla (String numpla )throws SQLException{
        discrepancia.listarSancionesXplanilla (numpla);
    }
    
        /**
     *  @autor Tito Andr�s
     */
    public void listarDiscrepanciaXplanilla (String numpla )throws SQLException{
        discrepancia.listarDiscrepanciaXplanilla (numpla);
    }
    
        /**
     *  @autor Tito Andr�s
     */
    public void auorizarSancion (Discrepancia discrepancia)throws SQLException{
        this.discrepancia.setDiscrepancia (discrepancia);
        this.discrepancia.autorizarSancion ();
    }
    
    /**
    * Metodo productosNoCerrados, informa si los productos no estan estan cerrados
    * (discrepancia).
    * @autor : Ing. Jose de la rosa
    * @see productosNoCerrados - DiscrepanciaDAO
    * @param : numero discrepancia
    * @version : 1.0
    */ 
    public boolean productosNoCerrados (int numdiscre) throws SQLException {
        return discrepancia.productosNoCerrados (numdiscre);
    }
    /**
    * Metodo productosCerrados, informa si los productos estan estan cerrados
    * (discrepancia).
    * @autor : Ing. Jose de la rosa
    * @see productosCerrados - DiscrepanciaDAO
    * @param : numero discrepancia
    * @version : 1.0
    */ 
    public boolean productosCerrados (int numdiscre) throws SQLException {
        return discrepancia.productosCerrados (numdiscre);
    }
    
    /**
    * Metodo insertarCierreDiscrepancia, ingresar el cierre de las discrepancias
    * (discrepancia).
    * @autor : Ing. Jose de la rosa
    * @see insertarCierreDiscrepancia - DiscrepanciaDAO
    * @param : discrepancia
    * @version : 1.0
    */    
    public void insertarCierreDiscrepancia (Discrepancia discrepancia) throws SQLException {
        this.discrepancia.setDiscrepancia (discrepancia);
        this.discrepancia.insertarCierreDiscrepancia ();
    }

    /**
    * Metodo insertarDiscrepanciaProducto, ingresar el cierre de los productos relacionados con las discrepancias
    * (discrepancia).
    * @autor : Ing. Jose de la rosa
    * @see insertarDiscrepanciaProducto - DiscrepanciaDAO
    * @param : discrepancia
    * @version : 1.0
    */
    
    public void insertarCierreDiscrepanciaDocumento (Discrepancia discrepancia) throws SQLException {
        this.discrepancia.setDiscrepancia (discrepancia);
        this.discrepancia.insertarCierreDiscrepanciaDocumento ();
    }
    
    
     /**
    * Metodo listDiscrepanciaGeneral, lista las discrepancias generales
    * (discrepancia).
    * @autor : Ing. Jose de la rosa
    * @see listDiscrepanciaGeneral - DiscrepanciaDAO
    * @param : discrepancia
    * @version : 1.0
    */
    public void listDiscrepanciaGeneral (String numpla, String distrito)throws SQLException{
        this.discrepancia.listDiscrepanciaGeneral (numpla, distrito);
    }
    
        /**
    * Metodo searchDiscrepanciaGeneral, busca las discrepancias generales
    * (discrepancia).
    * @autor : Ing. Jose de la rosa
    * @see searchDiscrepanciaGeneral - DiscrepanciaDAO
    * @param : discrepancia
    * @version : 1.0
    */
    public void searchDiscrepanciaGeneral (String numpla, int num_discre, String dstrct )throws SQLException{
        this.discrepancia.searchDiscrepanciaGeneral (numpla,num_discre, dstrct );
    }
    
        /**
    * Metodo updateDiscrepanciaProductoGeneral, actualizar los productos de discrepancias generales
    * (discrepancia).
    * @autor : Ing. Jose de la rosa
    * @see updateDiscrepanciaProductoGeneral - DiscrepanciaDAO
    * @param : discrepancia
    * @version : 1.0
    */
    public void updateDiscrepanciaProductoGeneral (Discrepancia discrepancia) throws SQLException{
        this.discrepancia.setDiscrepancia (discrepancia);
        this.discrepancia.updateDiscrepanciaProductoGeneral();
    }
    
        /**
    * Metodo updateDiscrepanciaGeneral, actualizar las discrepancias generales
    * (discrepancia).
    * @autor : Ing. Jose de la rosa
    * @see updateDiscrepanciaGeneral - DiscrepanciaDAO
    * @param : discrepancia
    * @version : 1.0
    */
    public void updateDiscrepanciaGeneral (Discrepancia discrepancia) throws SQLException{
        this.discrepancia.setDiscrepancia (discrepancia);
        this.discrepancia.updateDiscrepanciaGeneral();
    }
    
        /**
    * Metodo anularDiscrepanciaProductoGeneral, anular los productos de discrepancias generales
    * (discrepancia).
    * @autor : Ing. Jose de la rosa
    * @see anularDiscrepanciaProductoGeneral - DiscrepanciaDAO
    * @param : discrepancia
    * @version : 1.0
    */
    public void anularDiscrepanciaProductoGeneral (Discrepancia discrepancia) throws SQLException{
        this.discrepancia.setDiscrepancia (discrepancia);
        this.discrepancia.anularDiscrepanciaProductoGeneral();        
    }
    
        /**
    * Metodo anularDiscrepanciaGeneral, anular las discrepancias generales
    * (discrepancia).
    * @autor : Ing. Jose de la rosa
    * @see anularDiscrepanciaGeneral - DiscrepanciaDAO
    * @param : discrepancia
    * @version : 1.0
    */
    public void anularDiscrepanciaGeneral (Discrepancia discrepancia) throws SQLException{
        this.discrepancia.setDiscrepancia (discrepancia);
        this.discrepancia.anularDiscrepanciaGeneral();        
    }    
    
 
    /**
     * Metodo: listClienteDiscrepancia, permite listar los clientes que sus productos
     *         tenga la cantidad > 0
     * @autor : Ing. Diogenes Bastidas Morales
     * @param :
     * @see : listClienteDiscrepancia - DiscrepanciaDAO
     * @version : 1.0
     */
    public void listClienteDiscrepancia (String dstrct)throws SQLException{
        this.discrepancia.listClienteDiscrepancia(dstrct);
    }
    
     /**
     * Metodo: listPoductosCliente, permite listar los clientes que sus productos
     *         tenga la cantidad > 0
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: listProductosCliente - DiscrepanciaDAO
     * @param : codigo cliente y ubicacion
     * @version : 1.0
     */
    public void listProductosCliente (String codcliente, String ubicacion, String dstrct)throws SQLException{
        this.discrepancia.listProductosCliente(codcliente, ubicacion,dstrct);
    }
    
    /**
     * Metodo: listUbicacion, permite listar la lista de ubicacion de los productos 
     *         de un cliente
     * @autor : Ing. Diogenes Bastidas Morales
     * @param : codigo cliente
     * @see: listUbicacion - DiscrepanciaDAO
     * @version : 1.0
     */
    public void listUbicacion(String codcliente,String dstrct)throws SQLException{
        this.discrepancia.listUbicacion(codcliente, dstrct);
    }
    
     
    /**
     * Metodo insertarMovDiscrepancia, inserta en la bd la los movimiento discrepancia
     * @param:Objeto discrepancia
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    
    public void insertarMovDiscrepancia (Discrepancia dis) throws SQLException {
        this.discrepancia.setDiscrepancia (dis);
        this.discrepancia.insertarMovDiscrepancia(); 
        
    }
    
    /**
     * Metodo , actualizarInventario, actualiza en la el registro de la cantidad despachada
     * @param: objeto discrepancia
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    
    public void actualizarInventario (Discrepancia dis) throws SQLException {
        this.discrepancia.setDiscrepancia (dis);
        this.discrepancia.actualizarInventario();        
    }
    
    /**
     * Metodo: listProductosMovTrafico, permite listar productos despachados de acuerdo al cliente,
     *         ubicacion un rango de fecha almacenados en movimiento_trafico
     * @autor : Ing. Diogenes Bastidas Morales
     * @see:
     * @param : codigo cliente y ubicacion
     * @version : 1.0
     */
    public void listProductosMovTrafico (String codcliente, String ubicacion, String fecini, String fecfin, String dstrct )throws SQLException{
        this.discrepancia.listProductosMovTrafico(codcliente, ubicacion, fecini, fecfin, dstrct);
    }   
    
    
     /**
     * Metodo: getUbicaciones, retorna la listas de ubicaciones.
     * @autor : Ing. Diogenes Bastidas Morales
     * @param :
     * @version : 1.0
     */
    public Vector getUbicaciones () {
        return this.discrepancia.getUbicaciones();
    }
    
     /**
     * Metodo: getUbicaciones, retorna la listas de ubicaciones.
     * @autor : Ing. Diogenes Bastidas Morales
     * @param :
     * @version : 1.0
     */
    public Vector getClientes () {
        return this.discrepancia.getClientes();
    }    
     /**
     * Metodo: utilizarProducto, marca los campos numpla_despacho y numrem_despacho
     * con el numero de planilla y remesa con que fueron despachados.
     * @autor : Ing. Karen Reales
     * @param :
     * @version : 1.0
     */
    public String utilizarProducto () throws SQLException{
        return discrepancia.utilizarProducto();
    }
     /**
     * Getter for property producto.
     * @return Value of property producto.
     */
    public com.tsp.operation.model.beans.Producto getProducto() {
        return discrepancia.getProducto();
    }
     /**
     * Setter for property producto.
     * @param producto New value of property producto.
     */
    public void setProducto(com.tsp.operation.model.beans.Producto producto) {
        discrepancia.setProducto(producto);
    }
     /**
     * Metodo: desmarcarProductos, desmarca los campos numpla_despacho y numrem_despacho
     * de los productos relacionados a un numero de planilla.
     * @autor : Ing. Karen Reales
     * @param :
     * @version : 1.0
     */
    public String desmarcarProductos (String numpla) throws SQLException{
        return discrepancia.desmarcarProductos(numpla);
    }
    /**
     * Metodo: obtenerProductos, retorna un string con los codigos
     * de los productos relacionados a un numero de planilla.
     * @autor : Ing. Karen Reales
     * @param :
     * @version : 1.0
     */
    public String obtenerProductos (String numpla) throws SQLException{
        return discrepancia.obtenerProductos(numpla);
    }
    /******** DiscrepanciaService **********/
     /**
     * Metodo: obtenerProductos, retorna un vector con los datos de la discrepancia en un rango de fechas.
     * @autor : Ing. Jose De la rosa
     * @param : fecha inicial. fecha final y estado
     * @version : 1.0
     */
    public void planillasMigradasMIMS ( String fecini, String fecfin, String anulada )throws SQLException{
        discrepancia.planillasMigradasMIMS( fecini, fecfin, anulada);
    }
    
    
    // jose 2006-09-15
    
    /**
     * Getter for property cabDiscrepancia.
     * @return Value of property cabDiscrepancia.
     */
    public com.tsp.operation.model.beans.Discrepancia getCabDiscrepancia () {
        return discrepancia.getCabDiscrepancia ();
    }
    
    /**
     * Setter for property cabDiscrepancia.
     * @param cabDiscrepancia New value of property cabDiscrepancia.
     */
    public void setCabDiscrepancia (Discrepancia cabDiscrepancia) {
        discrepancia.setCabDiscrepancia ( cabDiscrepancia );
    }
    
    /**
     * Getter for property itemsDiscrepancia.
     * @return Value of property itemsDiscrepancia.
     */
    public Vector getItemsDiscrepancia () {
        return discrepancia.getItemsDiscrepancia ();
    }
    
    /**
     * Setter for property itemsDiscrepancia.
     * @param itemsDiscrepancia New value of property itemsDiscrepancia.
     */
    public void setItemsDiscrepancia (Vector itemsDiscrepancia) {
        discrepancia.setItemsDiscrepancia( itemsDiscrepancia );
    }
    
     /**
     * Setter for property itemsDiscrepancia.
     * @param itemsDiscrepancia New value of property itemsDiscrepancia.
     */
    public void addItemsDiscrepancia ( Discrepancia discre ) {
        discrepancia.addItemsDiscrepancia ( discre );
    }
    
     /**
     * Setter for property itemsDiscrepancia.
     * @param itemsDiscrepancia New value of property itemsDiscrepancia.
     */
    public void deleteItemsDiscrepancia ( int x ) {
        discrepancia.deleteItemsDiscrepancia ( x );
    }
    
    public void addItemsObjetoDiscrepancia ( int x, Discrepancia discre ) {
        discrepancia.addItemsObjetoDiscrepancia( x, discre );
    }
    
    public void resetItemsObjetoDiscrepancia (  ) {
        discrepancia.resetItemsObjetoDiscrepancia();
    }
    
    public int numeroDiscrepancia() throws SQLException{
        return discrepancia.numeroDiscrepancia();
    }
    
    /**
     * Getter for property ListaDiscrepancia.
     * @return Value of property ListaDiscrepancia.
     */
    public java.util.List getListaDiscrepancia() {
        return ListaDiscrepancia;
    }
    
    /**
     * Setter for property ListaDiscrepancia.
     * @param ListaDiscrepancia New value of property ListaDiscrepancia.
     */
    public void setListaDiscrepancia(java.util.List ListaDiscrepancia) {
        this.ListaDiscrepancia = ListaDiscrepancia;
    }
    
    public void addListaDiscrepancia( Discrepancia dis ){
        this.ListaDiscrepancia.add(dis);
    }
    
     /**
     * Metodo para grabar una discrepancia desde el programa de importacion
     * @autor mfontalvo
     * @param d, Discrepancia a grabar
     * @throws Exception.
     */
    public void grabarDiscrepanciaImportacion (Discrepancia d) throws Exception {
        discrepancia.grabarDiscrepanciaImportacion(d);
    }

    /**
     * Metodo para obtener las dicrepancias a migrar
     * @autor mfontalvo
     * @param usuario que migra sus propias dicrepancias.
     * @return vector de nuevas discrepancias
     * @throws Exception.
     **/
    public Vector obtenerDiscrepanciasPendientes(String usuario) throws Exception{
        return discrepancia.obtenerDiscrepanciasPendientes(usuario);
    }
}
