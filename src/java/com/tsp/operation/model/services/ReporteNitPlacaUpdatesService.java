/********************************************************************
 *  Nombre Clase.................   ReporteNitPlacaUpdatesService.java
 *  Descripci�n..................   Service del reporte de actualizaciones 
 *                                  en las tablas nit y placa
 *  Autor........................   Ing. Tito Andr�s Maturana
 *  Fecha........................   19.12.2005
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import java.io.*;
import java.util.*;
import java.sql.*;

public class ReporteNitPlacaUpdatesService {  
    private Vector nits;
    private Vector placas;
    private Vector nitsFiles;
    private Vector placasFiles;
    
    
    /** Creates a new instance of ReporteNitPlacaUpdatesService */
    public ReporteNitPlacaUpdatesService() {
        this.nits = new Vector();
        this.placas = new Vector();
        this.nitsFiles = new Vector();
        this.placasFiles = new Vector();
    }    
    
    
    /**
     * Explora una ruta de archivo y selecciona los archivos dentro de la fecha especificada.
     * @autor Tito Andr�s Maturana
     * @param fechaI Fecha inicial.
     * @param fechaF Fecha final.
     * @throws Exception
     * @version 1.1
     */
    private List Load(String fechaI, String fechaF) throws Exception{
        List Listado = new LinkedList();
        File f = null;
        f = new File("/rmi/Sincronizacion");
        ////System.out.println("......................fecha inicio: " + fechaI);
        ////System.out.println("......................fecha final: " + fechaF);
        if(! f.exists() ) f.mkdir();
        
        if (f.isDirectory()){
            FilenameFilter filter = new FilenameFilter() {
                public boolean accept( File path, String name){                    
                    return ( name.toLowerCase().endsWith(".txt") );
                }
            };
            
            File []file =  f.listFiles(filter);
            Ordenar(file);
            
            for (int i=0;i<file.length;i++){
                String fecha;
                String filename = file[i].getName();
                
                if( filename.indexOf("_")!=-1 ){
                    fecha = filename.split("_")[1];                    
                    fecha = fecha.substring(0, 6);
                    if( Integer.parseInt(fecha) >= Integer.parseInt(fechaI) &&
                            Integer.parseInt(fecha)<=Integer.parseInt(fechaF) ){
                        Listado.add(file[i]);
                    }
                }                 
            }
        }
        
        ////System.out.println(".................. archivos encontrados: " + Listado.size());
        
        return Listado;
    }
    
    /**
     * Getter for property nits.
     * @return Value of property nits.
     */
    public java.util.Vector getNits() {
        return nits;
    }
    
    /**
     * Setter for property nits.
     * @param nits New value of property nits.
     */
    public void setNits(java.util.Vector nits) {
        this.nits = nits;
    }
    
    /**
     * Getter for property placas.
     * @return Value of property placas.
     */
    public java.util.Vector getPlacas() {
        return placas;
    }
    
    /**
     * Setter for property placas.
     * @param placas New value of property placas.
     */
    public void setPlacas(java.util.Vector placas) {
        this.placas = placas;
    }
    
    /**
     * Separa los arhivos de actualizaciones de las tablas nit
     * y placa.
     * @autor Ing. Tito Andr�s Maturana D.
     * @param fechaI Fecha inicial del reporte
     * @param fechaF Fecha final del reporte
     * @throws Exception
     * @version 1.1
     */ 
    private void clasificar(String fechaI, String fechaF) throws Exception{
        this.nitsFiles = new Vector();
        this.placasFiles = new Vector();
        
        List files = this.Load(fechaI, fechaF);
        
        for( int i=0; i<files.size(); i++ ){
            File f = (File) files.get(i);
            
            String filename = f.getName();
            
            if( filename.indexOf("nit")!=-1 ){
                this.nitsFiles.add(f);
            } else if ( filename.indexOf("placa")!=-1 ){
                this.placasFiles.add(f);
            }
        }
        
        ////System.out.println("................. Archivos nit: " + nitsFiles.size());
        ////System.out.println("................. Archivos placas: " + placasFiles.size());
    }
    
    /**
     * Getter for property placasFiles.
     * @return Value of property placasFiles.
     */
    public java.util.Vector getPlacasFiles() {
        return placasFiles;
    }
    
    /**
     * Setter for property placasFiles.
     * @param placasFiles New value of property placasFiles.
     */
    public void setPlacasFiles(java.util.Vector placasFiles) {
        this.placasFiles = placasFiles;
    }
    
    /**
     * Getter for property nitsFiles.
     * @return Value of property nitsFiles.
     */
    public java.util.Vector getNitsFiles() {
        return nitsFiles;
    }
    
    /**
     * Setter for property nitsFiles.
     * @param nitsFiles New value of property nitsFiles.
     */
    public void setNitsFiles(java.util.Vector nitsFiles) {
        this.nitsFiles = nitsFiles;
    }
    
    /**
     * Genera el reporte de actualizaciones de las tablas nit
     * y placa.
     * @autor Ing. Tito Andr�s Maturana D.
     * @param fechaI Fecha inicial del reporte
     * @param fechaF Fecha final del reporte
     * @throws Exception
     * @version 1.1
     */ 
    public void generarReporte(String fechaI, String fechaF) throws Exception{
        this.clasificar(fechaI, fechaF);
        this.ordenarPlacas();
        this.ordenarNits();
    }
    
    /**
     * Ordena las placas, de tal forma que la misma placa este en secuencia.
     * @autor Ing. Tito Andr�s Maturana D.
     * @throws Exception
     * @version 1.1
     */ 
    private void ordenarPlacas() throws Exception{
        String linea = "";
        Vector bases = new Vector();
        this.placas = new Vector();
        
        for(int i=0; i<this.placasFiles.size(); i++){
            File f = (File) this.placasFiles.elementAt(i);
            //////System.out.println("................... leyendo: " + f.getAbsolutePath());
            BufferedReader input = new BufferedReader(new FileReader(f.getAbsolutePath()));
            linea = "";
            String filename = f.getName();
            int j = filename.indexOf("placa");
            int k = filename.indexOf("_");
            String base = filename.substring(j+=5, k);
            //////System.out.println("................... base: " + base);            
            
            linea=input.readLine();
            while ((linea=input.readLine())!=null){
                String[] c = linea.split(",");                
                this.placas.add(c);
                bases.add(base);
            }
        }
        
        Object[] placas = this.placas.toArray();
        Object[] bs = bases.toArray();        
        
        for( int i=0; i<placas.length; i++){
            String[] c = (String[]) placas[i];            
            for( int j=i+1; j<placas.length; j++){
                String[] c_1 = (String[]) placas[j];
                
                if( !c[1].matches(c_1[1]) ){
                    for( int k=j+1; k<placas.length; k++){
                        String[] c_2 = (String[]) placas[k];
                        
                        if( c[1].matches(c_2[1]) ){
                            String[] temp = (String[]) placas[j];
                            String tempbs = (String) bs[j];
                            
                            placas[j] = placas[k];
                            placas[k] = temp;
                            
                            bs[j] = bs[k];
                            bs[k] = tempbs;
                            break;
                        }
                    }
                }                
            }            
            
        }
        
        this.placas = new Vector();        
        
        for( int i=0; i<placas.length; i++){
            Vector vec = new Vector();
            
            String[] c = (String[]) placas[i];            
            String base = (String) bs[i];
            vec.add(base);
            vec.add(c);            
            this.placas.add(vec);
            ////System.out.println("............................. Placa: " + c[1]);
        }
    }   
    
    /**
     * Ordena un arreglo de archivos por la fecha en el nombre de mismo.
     * @autor Ing. Tito Andr�s Maturana
     * @param arg El arreglo de archivos
     * @version 1.0
     */
    private void Ordenar(File []arg){
        File temp = null;
        for (int i = 0; i<arg.length;i++){
            String filename = arg[i].getName();
            String fecha = "0";
        
            if( filename.indexOf("_")!=-1 ){
                fecha = filename.split("_")[1];
                fecha = fecha.substring(0, 6);
            }
                
            for (int j = i+1; j<arg.length ;j++){
                String filename1 = arg[j].getName();
                String fecha1 = "0";
                
                if( filename1.indexOf("_")!=-1 ){
                    fecha1 = filename1.split("_")[1];                    
                    fecha1 = fecha1.substring(0, 6);
                }    
                
                if( Integer.parseInt(fecha) > Integer.parseInt(fecha1) ){
                    temp = arg[i];
                    arg[i] = arg[j];
                    arg[j] = temp;
                }
            }
        }           
    }    
    
    /**
     * Ordena los nits, de tal forma que el mismo nit este en secuencia.
     * @autor Ing. Tito Andr�s Maturana D.
     * @throws Exception
     * @version 1.1
     */ 
    private void ordenarNits() throws Exception{
        String linea = "";
        Vector bases = new Vector();
        this.nits = new Vector(); 
        
        for(int i=0; i<this.nitsFiles.size(); i++){
            File f = (File) this.nitsFiles.elementAt(i);
            ////System.out.println("................... leyendo: " + f.getAbsolutePath());
            BufferedReader input = new BufferedReader(new FileReader(f.getAbsolutePath()));
            linea = "";
            String filename = f.getName();
            int j = filename.indexOf("placa");
            int k = filename.indexOf("_");
            String base = filename.substring(j+=5, k);
            ////System.out.println("................... base: " + base);            
            
            linea=input.readLine();
            while ((linea=input.readLine())!=null){
                String[] c = linea.split(",");                
                this.nits.add(c);
                bases.add(base);
            }
        }
        
        Object[] nits = this.nits.toArray();
        Object[] bs = bases.toArray();        
        
        for( int i=0; i<nits.length; i++){
            String[] c = (String[]) nits[i];            
            for( int j=i+1; j<nits.length; j++){
                String[] c_1 = (String[]) nits[j];
                
                if( !c[1].matches(c_1[1]) ){
                    for( int k=j+1; k<nits.length; k++){
                        String[] c_2 = (String[]) nits[k];
                        
                        if( c[1].matches(c_2[1]) ){
                            String[] temp = (String[]) nits[j];
                            String tempbs = (String) bs[j];
                            
                            nits[j] = nits[k];
                            nits[k] = temp;
                            
                            bs[j] = bs[k];
                            bs[k] = tempbs;
                            break;
                        }
                    }
                }                
            }            
            
        }
        
        this.nits = new Vector();        
        
        for( int i=0; i<nits.length; i++){
            Vector vec = new Vector();
            
            String[] c = (String[]) nits[i];            
            String base = (String) bs[i];
            vec.add(base);
            vec.add(c);            
            this.nits.add(vec);
            ////System.out.println("............................. NIT: " + c[1]);
        }
    }
    
    
}


