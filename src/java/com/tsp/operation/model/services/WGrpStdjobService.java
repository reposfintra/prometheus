/********************************************************************
 *      Nombre Clase.................   WGrpStdjobService.java    
 *      Descripci�n..................   Service del archivo  wgroup_standart
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   16.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

public class WGrpStdjobService {
    
    private WGrpStdjobDAO dao;
    
    private String varSeparadorJS = "~";
    private String  varCamposJS = "var CamposJS = []";
    private String  varCamposJStd = "var CamposStd = []";
    
    /** Creates a new instance of WGrpStdjobService */
    public WGrpStdjobService() {
        this.dao = new WGrpStdjobDAO();
    }
    
    /**
     * Getter for property varSeparadorJS.
     * @return Value of property varSeparadorJS.
     */
    public java.lang.String getVarSeparadorJS() {
        return varSeparadorJS;
    }
    
    /**
     * Setter for property varSeparadorJS.
     * @param varSeparadorJS New value of property varSeparadorJS.
     */
    public void setVarSeparadorJS(java.lang.String varSeparadorJS) {
        this.varSeparadorJS = varSeparadorJS;
    }
    
    /**
     * Obtiene los work group asociados a los n�meros de stdjobs ingresados, y setea las propiedades
     * varCamposJS y varCamposStd
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param stdjobs Arreglo de n�meros de std jobs
     * @param wkgs Arreglo de c�digos de work groups
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void obtenerCampoJSWorkGroups(String[] stdjobs, String[] wkgs) throws SQLException {
        
        Vector wgs = new Vector();
        String var0 = "\n var CamposStd = [";
        
        for ( int i=0; i<stdjobs.length; i++){
            
            String[] std = stdjobs[i].split(this.varSeparadorJS);
            dao.workGroups(std[0]);
            Vector vec = dao.getVector();
            
            var0 += "\n '" + std[0] + this.varSeparadorJS + std[1] + "',";
            
            //////System.out.println(".............. entro standar job no: " + std[0]);
            
            for(int j=0; j<vec.size(); j++){
                Hashtable ht = new Hashtable();
                ht = (Hashtable) vec.elementAt(j);
                
                wgs.add(ht);
            }
        }
        
        var0 = var0.substring(0, var0.length() - 1) + "];";
        
        String var = "\n var CamposJS = [ ";
        
        if( wkgs!=null ){
            for( int i=0; i<wkgs.length; i++ ){
                var += "\n '" + wkgs[i] + "',";
            }
        }        
        
        for( int i=0; i<wgs.size(); i++){
            Hashtable ht = new Hashtable();
            ht = (Hashtable) wgs.elementAt(i);
            String codigo = (String) ht.get("codigo");
            String desc = (String) ht.get("descripcion");
            var += "\n '" + codigo + "/" + this.varSeparadorJS + desc + "',";            
        }
        var = var.substring(0,var.length()-1) + "];";
        
        this.varCamposJStd = var0;
        this.varCamposJS = var;
    }
    
    /**
     * Ingresa un registro en el archivo wgroup_standard.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @see com.tsp.operation.model.DAOS.WGrpStdjobDAO#ingresar(WgroupStd)
     * @version 1.0
     */
    public void ingresar()throws SQLException{
        dao.ingresar();
    }
    
    /**
     * Actualiza el campo reg_status del archivo wgroup_standard.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param reg_status Valor del reg_status
     * @param user Usuario que realiza la modificaci�n
     * @param wgroup C�digo del work group
     * @param stdjob N�mero del standard job
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @see com.tsp.operation.model.DAOS.WGrpStdjobDAO#reg_statusUpdate(String, String, String, String);
     * @version 1.0
     */
    public void reg_statusUpdate(String reg_status, String user, String wgroup, String stdjob)throws SQLException{
        dao.reg_statusUpdate(reg_status, user, wgroup, stdjob);
    }
    
    /**
     * Verifica la existencia de un registro en el archivo wgroup_standard.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param wgroup C�digo del work group
     * @param stdjob N�mero del standard job
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @see com.tsp.operation.model.DAOS.WGrpStdjobDAO#existe(String, String)
     * @version 1.0
     */
    public String existe(String wgroup, String stdjob)throws SQLException{
        return dao.existe(wgroup, stdjob);
    }
    
    /**
     * Obtiene el c�digo y la descripci�n de todos los work groups asociados
     * a un stdjob.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param stdjob N�mero de stdjob
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @see com.tsp.operation.model.DAOS.WGrpStdjobDAO#workGroups(String)
     * @version 1.0
     */
    public void workGroups(String stdjob)throws SQLException{
        dao.workGroups(stdjob);
    }
    
    /**
     * Setea la propiedad varCamposJS.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param wgs Arreglo de c�digos de work groups
     * @version 1.0
     */
    public void loadVarCamposJS(String[] wgs){
        String var = "\n var CamposJS = [ ";
        
        if( wgs!=null ){
            for( int i=0; i<wgs.length; i++ ){
                var += "\n '" + wgs[i] + "',";
            }
            
            var = var.substring(0,var.length()-1) + "];";
        } else {            
            var += "];";
        }       
        
        this.varCamposJS = var;
    }
    
    /**
     * Setea la propiedad varCamposStd.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param stds Arreglo de n�meros de stdjobs
     * @version 1.0
     */
    public void loadVarCamposJStd(String[] stds){
        String var0 = "\n var CamposStd = [";
        
        if ( stds!=null ){
            for ( int i=0; i<stds.length; i++){
                var0 += "\n '" + stds[i] + "',";
            }
            var0 = var0.substring(0, var0.length() - 1) + "];";
        } else {
            var0 += "];";
        }
        
        this.varCamposJStd = var0;
    }
    
    /**
     * Getter for property varCamposJS.
     * @return Value of property varCamposJS.
     */
    public java.lang.String getVarCamposJS() {
        return varCamposJS;
    }
    
    /**
     * Setter for property varCamposJS.
     * @param varCamposJS New value of property varCamposJS.
     */
    public void setVarCamposJS(java.lang.String varCamposJS) {
        this.varCamposJS = varCamposJS;
    }
    
    /**
     * Getter for property varCamposJStd.
     * @return Value of property varCamposJStd.
     */
    public java.lang.String getVarCamposJStd() {
        return varCamposJStd;
    }
    
    /**
     * Setter for property varCamposJStd.
     * @param varCamposJStd New value of property varCamposJStd.
     */
    public void setVarCamposJStd(java.lang.String varCamposJStd) {
        this.varCamposJStd = varCamposJStd;
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return dao.getVector();
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        dao.setVector(vector);
    }
    
    /**
     * Getter for property wg.
     * @return Value of property wg.
     */
    public com.tsp.operation.model.beans.WgroupStd getWg() {
        return dao.getWg();
    }
    
    /**
     * Setter for property wg.
     * @param wg New value of property wg.
     */
    public void setWg(com.tsp.operation.model.beans.WgroupStd wg) {
        dao.setWg(wg);
    }
    
}
