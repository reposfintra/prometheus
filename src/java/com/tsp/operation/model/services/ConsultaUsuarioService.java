/***********************************************************************************
 * Nombre clase : ............... ConsultaUsuarioService.java                           *
 * Descripcion :................. Clase que instancia los metodos de ConsultaUsuarioDAO *
 *                                con la BD.                                            *
 * Autor :....................... Ing. David Lamadrid                                   *
 * Fecha :....................... 4 de diciembre de 2005, 11:02 AM                      *
 * Version :..................... 1.0                                                   *
 * Copyright :................... Fintravalores S.A.                               *
 ***********************************************************************************/

package com.tsp.operation.model.services;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
/**
 *
 * @author  dlamadrid
 */
public class ConsultaUsuarioService
{
    ConsultaUsuarioDAO dao;
    Vector vTitulos = new Vector();
    Vector vSelect = new Vector();
    Vector vFrom = new Vector();
    private String from="";
    private String letras[]  = {"a","b","c","d","e","f","g","h","i","j","k","l","m","m","o"};

    /** Creates a new instance of ConsultaUsuarioService */
    public ConsultaUsuarioService ()
    {
        dao = new ConsultaUsuarioDAO ();
    }

    /**
    * inserta un registro en la tabla consulta_usuario
    * @autor David Lamadrid
    * @throws SQLException
    * @see ConsultaUsuarioDAO
    * @version 1.0
    */
    public void insertar () throws SQLException
    {
        dao.insertar ();
    }

    /**
    * Lista los registros de consulta_usuarios por usuario de creacion
    * @autor David Lamadrid
    * @throws SQLException
    * @see ConsultaUsuarioDAO
    * @version 1.0
    */
    public void listarPorU (String codigo) throws SQLException
    {
        dao.listarPorU (codigo);
    }

    /**
    * retorna false si no existe el registro y true si existe un registro con usuario y consulta igual a los de la bd
    * @autor David Lamadrid
    * @throws SQLException
    * @version 1.0
    */
    public boolean exsite (String usuario,String consulta) throws SQLException
    {
        return dao.exsite (usuario, consulta);
    }

    /**
    * Elimina el registro de consultas_usuario dado la llave primaria del registro
    * @autor David Lamadrid
    * @throws SQLException
    * @version 1.0
    */
    public void eliminar (int codigo) throws SQLException
    {
        dao.eliminar (codigo);
    }

    /**
    * Getter for property tConsultas.
    * @return Value of property tConsultas.
    * @see ConsultaUsuarioDAO
    */
    public java.util.TreeMap getTConsultas ()
    {
        return dao.getTConsultas ();
    }

    /**
    * Setter for property tConsultas.
    * @param tConsultas New value of property tConsultas.
    *@see ConsultaUsuarioDAO
    */
    public void setTConsultas (java.util.TreeMap tConsultas)
    {
        dao.setTConsultas (tConsultas);
    }

    /**
    * metodo que setea el TreeMap tConsultas con el Vector vConsultas que contiene una lista de Objetos con los registros de consultas_usuarios consernientes a un usuario
    * @autor David Lamadrid
    * @throws SQLException
    *@see ConsultaUsuarioDAO
    * @version 1.0
    */
    public void tConfiguracionesPorUsuario (String usuario) throws SQLException
    {
        dao.tConfiguracionesPorUsuario (usuario);
    }

    /**
     * Getter for property vConsultas.
     *@see ConsultaUsuarioDAO
     * @return Value of property vConsultas.
     */
    public java.util.Vector getVConsultas ()
    {
        return dao.getVConsultas ();
    }

    /**
    * Setter for property vConsultas.
    *@see ConsultaUsuarioDAO
    * @param vConsultas New value of property vConsultas.
    */
    public void setVConsultas (java.util.Vector vConsultas)
    {
        dao.setVConsultas (vConsultas);
    }

    /**
    * Getter for property consulta.
    *@see ConsultaUsuarioDAO
    * @return Value of property consulta.
    */
    public com.tsp.operation.model.beans.ConsultaUsuarios getConsulta ()
    {
        return  dao.getConsulta ();
    }

    /**
    * Setter for property consulta.
    *@see ConsultaUsuarioDAO
    * @param consulta New value of property consulta.
    */
    public void setConsulta (com.tsp.operation.model.beans.ConsultaUsuarios consulta)
    {
        dao.setConsulta (consulta);
    }

    /**
    * Metodo que setea el Vector vCampos con los campos de una tabla
    * @autor David Lamadrid
    * @parma String nombreTabla (Nombre de la Tabla)
    * @version 1.0
    *@throws SQLException
    *@see ConsultaUsuarioDAO
    */
    public void obtenerCampos (String nombreTabla)throws Exception
    {
        dao.obtenerCampos (nombreTabla);
    }

    /**
    * Setter for property vCampos.
    *@see ConsultaUsuarioDAO
    * @param vCampos New value of property vCampos.
    */
    public void setVCampos (java.util.Vector vCampos)
    {
        dao.setVCampos (vCampos);
    }

    /**
    * Metodo que retorna true si una consulta es valida y false de lo contrario
    * @autor David Lamadrid
    * @parma String consulta
    * @version 1.0
    *@throws SQLException
    *@see ConsultaUsuarioDAO
    */
    public  boolean existeConsulta (String consulta ) throws SQLException
    {
        return dao.existeConsulta (consulta);
    }

    /**
    * Metodo que genera un Vector con la consulta creada por el usuario
    * @autor David Lamadrid
    * @parma Vector campos(Campos seleccionados),String consulta
    * @version 1.0
    *@throws SQLException
    *@see ConsultaUsuarioDAO
    */
    public void generarConsulta (Vector campos,String consulta) throws SQLException
    {
        dao.generarConsulta (campos, consulta);
    }

    /**
     * getter for property vConuslta.
     *@see ConsultaUsuarioDAO
     * @param
     */
    public java.util.Vector getVConuslta ()
    {
        return dao.getVConuslta ();
    }

    /**
    * Setter for property vConuslta.
    *@see ConsultaUsuarioDAO
    * @param vConuslta New value of property vConuslta.
    */
    public void setVConuslta (java.util.Vector vConuslta)
    {
        dao.setVConuslta (vConuslta);
    }

    /**
    * Metodo que genera un Vector con los encabesados de la consulta creada por el usuario
    * @autor David Lamadrid
    * @parma Vector campos(Campos seleccionados)
    * @version 1.0
    *@throws SQLException
    */
    public Vector encabesado(Vector vCampos){
         Vector vTitulos = new Vector();
         for (int i=0; i< vCampos.size (); i++){
             String campo = ""+vCampos.elementAt (i);
             campo = campo.substring (campo.indexOf (".")+1,campo.length ());
             vTitulos.add (campo);
         }
         return vTitulos;
    }

     /**
      * Getter for property vTitulos.
      * @return Value of property vTitulos.
      */
     public java.util.Vector getVTitulos ()
     {
         return vTitulos;
     }

     public boolean existeSelectI(String campo){
         boolean sw=false;
         if(this.vSelect!=null){
             if(this.vSelect.size()>0){
                for(int i=0;i<vSelect.size();i++){
                    if(campo.equals(""+vSelect.elementAt(i))){
                        sw=true;
                    }
                }
             }
         }
         return sw;
     }

    /**
    * Setter for property vTitulos.
    * @param vTitulos New value of property vTitulos.
    */
     public void setVTitulos (java.util.Vector vTitulos)
     {
         this.vTitulos = vTitulos;
     }

    /**
    * Getter for property vSelect.
    * @return Value of property vSelect.
    */
     public java.util.Vector getVSelect() {
         return vSelect;
     }

    /**
    * Setter for property vSelect.
    * @param vSelect New value of property vSelect.
    */
     public void setVSelect(java.util.Vector vSelect) {
         this.vSelect = new Vector();
         this.vSelect = vSelect;
     }

    /**
    * Getter for property vFrom.
    * @return Value of property vFrom.
    */
     public java.util.Vector getVFrom() {
         return vFrom;
     }

    /**
    * Setter for property vFrom.
    * @param vFrom New value of property vFrom.
    */
     public void setVFrom(java.util.Vector vFrom) {
         this.vFrom = vFrom;
     }

    /**
    * Metodo que retorna true si el nombre de la tabla existe en el Vector vFrom
    * @autor David Lamadrid
    * @parma String campo (Nombre de la Tabla)
    * @version 1.0
    */
     public boolean existeFromI(String campo){
         boolean sw=false;
         if(this.vFrom!=null){
             if(this.vFrom.size()>0){
                for(int i=0;i< this.vFrom.size();i++){
                    if(campo.equals(""+this.vFrom.elementAt(i))){
                        sw=true;
                    }
                }
             }
         }
         return sw;
     }

    /**
    * Lista los registros de consulta_usuarios por codigo de consulta
    * @autor David Lamadrid
    * @throws SQLException
    *@see ConsultaUsuarioDAO
    * @version 1.0
    */
    public void listarPorCodigo(String codigo) throws SQLException
    {
        dao.listarPorCodigo (codigo);
    }

    /**
    * Getter for property letras.
    * @return Value of property letras.
    */
    public java.lang.String[] getLetras () {
        return this.letras;
    }

    /**
    * Setter for property letras.
    * @param letras New value of property letras.
    */
    public void setLetras (java.lang.String[] letras) {
        this.letras = letras;
    }

    /**
    * metodo que adicioana una tabla a la consulta setenado el Vector vFrom
    * @autor David Lamadrid
    * @param String nombreTabla
    * @version 1.0
    */
    public void adicionarTabla(String nombreTabla){
        boolean sw=false;
        for(int i=0;i<this.vFrom.size ();i++){
            Vector campo= (Vector)this.vFrom.elementAt (i);
            if(campo.elementAt (0).equals (nombreTabla)){
                sw=true;
            }
        }
        if(sw==false){
            int n = this.vFrom.size ();
            String letra= this.letras[n];
            Vector fila = new Vector();
            fila.add (nombreTabla);
            fila.add (letra);
            this.vFrom.add (fila);
            if(this.from.equals ("")){
                this.from = "from "+nombreTabla+" as "+letra;
            }
            else{
               this.from =from+","+nombreTabla+" as "+letra;
            }
        }
   }

    /**
    * Metodo que valida el Vector vFrom con las tablas que tinene selecciondas
    * el usuario en la pagina de Insercion de consultas
    * @autor David Lamadrid
    * @parma String campo (Nombre de la Tabla)
    * @version 1.0
    */
    public void validarFrom(String[] campos,String cadena){
        boolean sw=false;
        if(this.vFrom!=null){
            for(int x=0;x < this.vFrom.size ();x++){
                Vector fila=(Vector)vFrom.elementAt (x);
                sw=false;
                for ( int i=0;i<campos.length;i++){
                    if(campos[i].equals (""+fila.elementAt (0))){
                        sw=true;
                    }
                }
                if(sw==false){
                    this.vFrom.remove (x);
                    this.setFrom (cadena);
                }
            }
        }
    }

    /**
    * Metodo que retorna la letra correspondiente a una tabla dado el nombre de estas del Vector vFrom
    * @autor David Lamadrid
    * @parma String campo (Nombre de la Tabla)
    * @version 1.0
    */
    public String obtenerLetra(String nombreTabla){
        String letra="";
        if(vFrom !=null){
            for(int i=0;i<this.vFrom.size ();i++){
                Vector campo= (Vector)this.vFrom.elementAt (i);
                if(campo.elementAt (0).equals (nombreTabla)){
                    letra=""+campo.elementAt (1);
                }
            }
        }
        return letra;
    }

    /**
    * Getter for property vCampos.
    *@see ConsultaUsuarioDAO
    * @return Value of property vCampos.
    */
    public java.util.Vector getVCampos ()
    {
        return dao.getVCampos ();
    }

    /**
    * Getter for property from.
    *@see ConsultaUsuarioDAO
    * @return Value of property from.
    */
    public java.lang.String getFrom () {
        return from;
    }

    /**
    * Setter for property from.
    *@see ConsultaUsuarioDAO
    * @param from New value of property from.
    */
    public void setFrom (java.lang.String from) {
        this.from = from;
    }

}
