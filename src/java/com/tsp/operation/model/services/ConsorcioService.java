/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.services;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



import com.tsp.operation.model.beans.*;
import java.util.*;
import java.util.Date;

import java.sql.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.Model;
import com.tsp.util.Util;
import java.text.*;


import com.tsp.util.LogWriter;
import com.aspose.cells.*;
import java.io.*;



/**
 *
 * @author Alvaro
 */
public class ConsorcioService {
    

    private ConsorcioDAO consorcioDao;

    private SolicitudPorFacturar solicitud;


    /** Creates a new instance of ConsorcioService */
    public ConsorcioService() {
        consorcioDao = new ConsorcioDAO();
    }
    public ConsorcioService(String dataBaseName) {
        consorcioDao = new ConsorcioDAO(dataBaseName);
    }



    // -------------------------------------------------------------------------
    // DEFINICION PARA USAR EXCEL
    // Variables del archivo de excel

    AsposeUtil xlsUtil = new AsposeUtil();
    Workbook   wb;
    String     rutaInformes;
    String     nombre;

    Style header  , titulo1, titulo2, titulo3 , titulo4, titulo5, letra, numero,  numeroCentrado;
    Style dinero, dineroMarron, dineroAzul, dineroVerde;
    Style porcentaje, letraCentrada, numeroNegrita, ftofecha;
    Style dineroSinCentavo, dineroSinCentavoMarron , dineroSinCentavoAzul, dineroSinCentavoVerde;
    Style idMarron, idAzul, idVerde, idLetra;
    Style letraMarron, letraAzul, letraVerde;
    Style gtLetra, gtDineroSinCentavo, gtId, fLetra, fDineroSinCentavo, fId, T1Letra ;

    Hashtable colorPersonalizado = new Hashtable();

    // Variables
    private SimpleDateFormat fmt;


    int fila = 0;
    // FIN DEFINICION PARA USAR EXCEL
    // -------------------------------------------------------------------------




    /** Crea una lista de ofertas pendientes de facturar a los clientes */
    public void buscaSolicitudPorFacturar()throws SQLException{
        consorcioDao.buscaSolicitudPorFacturar();
    }


    public List getSolicitudPorFacturar() throws SQLException{
        return consorcioDao.getSolicitudPorFacturar();
    }


    /** Busca el objeto OfertaPorFacturar correspondiente a la oferta seleccionada */
    public void buscarSolicitudPorFacturar(String id_solicitud, int parcial){

        try {
            List listaSolicitudPorFacturar = getSolicitudPorFacturar();

            solicitud = null;

            Iterator it = listaSolicitudPorFacturar.iterator();
            while (it.hasNext()) {
                 solicitud = (SolicitudPorFacturar)it.next();

                 if( (id_solicitud.equalsIgnoreCase(solicitud.getId_solicitud()) ) &&
                     (parcial == solicitud.getParcial() )    ) {
                     break;
                 }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }


    }


    public SolicitudPorFacturar getSolicitud() {
        return solicitud;
    }


    /** Crea una lista de subclientes que estan asociados a una solicitud */
    public void buscaSubclientePorSolicitud(String id_solicitud, int parcial)throws SQLException{
        consorcioDao.buscaSubclientePorSolicitud(id_solicitud, parcial);
    }


   /** Retorna una lista de subclientes que estan asociados a una solicitud */
    public List getSubclientePorSolicitud() throws SQLException{
        return consorcioDao.getSubclientePorSolicitud();
    }


    /** Crea una lista de acciones que estan asociados a una solicitud */
    public void buscaAccionPorSolicitud(String id_solicitud, int parcial)throws SQLException{
        consorcioDao.buscaAccionPorSolicitud(id_solicitud, parcial);
    }


   /** Retorna una lista de acciones que estan asociados a una solicitud */
    public List getAccionPorSolicitud() throws SQLException{
        return consorcioDao.getAccionPorSolicitud();
    }



    public void CalculaFinanciacion() throws SQLException  {

        List listaAccionPorSolicitud = this.getAccionPorSolicitud();

        // Totaliza los valores a financiar de cada accion



        AccionPorSolicitud accion = null;
        double total_a_financiar = 0.00;
        double total_a_financiar_sin_iva = 0.00;
        double total_iva =  0.00;


        Iterator it = listaAccionPorSolicitud.iterator();
        while (it.hasNext()) {
           accion = (AccionPorSolicitud)it.next();
           total_a_financiar         += accion.getValor_a_financiar();
           total_a_financiar_sin_iva += accion.getValor_a_financiar_sin_iva();
           total_iva                 += accion.getSubtotal_iva();

        }



        // Determina parametros para financiar

        SolicitudPorFacturar solicitudPorFacturar = this.getSolicitud();

        String ano = "";
        String mes = "";
        String trimestre = "";


        String regulacion      = solicitudPorFacturar.getRegulacion();
        double porcentaje_mora = solicitudPorFacturar.getPorcentaje_mora();
        double dtf_semana = 0.00 ;
        int cuotas_reales = 0;
        double financiacion_fintra = 0.00;
        PuntosFinanciacion puntosFinanciacion = null;
        double dtf_puntos  = 0.00;
        String tipo_puntos = "";
        double cuota_pago = 0;
        double valor_con_financiacion = 0.00;





        // Actualiza el valor a financiar para cada subcliente.
        SubclientePorSolicitud subcliente = null;
        List listaSubclientePorSolicitud = consorcioDao.getSubclientePorSolicitud();
        ListIterator lit = listaSubclientePorSolicitud.listIterator();

        int numero_subclientes = listaSubclientePorSolicitud.size();
        int i = 0;

        TotalesConsorcio totalesConsorcio = new TotalesConsorcio();
        totalesConsorcio.setValorInicial(total_a_financiar_sin_iva,1);
        totalesConsorcio.setValorInicial(total_iva,2);
        totalesConsorcio.setValorTotal(0.00, 1);
        totalesConsorcio.setValorTotal(0.00, 2);

        while (lit.hasNext()) {

           i++;
           subcliente  = (SubclientePorSolicitud) lit.next();

           calculosPorSubcliente(subcliente, lit, ano,  mes, trimestre, regulacion, porcentaje_mora,
                                 total_a_financiar_sin_iva, total_iva, totalesConsorcio,
                                 numero_subclientes, i);
        }
    }



public void calculosPorSubcliente (SubclientePorSolicitud subcliente, ListIterator lit,
                                   String ano, String mes, String trimestre,
                                   String regulacion, double porcentaje_mora,
                                   double total_a_financiar_sin_iva,
                                   double total_iva, TotalesConsorcio totalesConsorcio,
                                   int numero_subclientes, int i) throws SQLException {


       PuntosFinanciacion puntosFinanciacion = null;
       double valor = 0.00;



       double dtf_semana = subcliente.getDtf_semana();

       subcliente.getFecha_financiacion();
       ano = subcliente.getFecha_financiacion().substring(0,4);
       mes = subcliente.getFecha_financiacion().substring(5,7);
       trimestre = getTrimestre(mes);

       // Valor de la solicitud sin iva = Valor contratista + bonificacion + comisiones
       subcliente.setValor_sin_iva(total_a_financiar_sin_iva);
       // Valor de la solicitud sin iva distribuido entre los subclientes a traves del porcentaje que le corresponde a cada uno de ellos
       valor = Util.redondear2(subcliente.getValor_sin_iva() * subcliente.getPorcentaje_base()/100,0);
       subcliente.setSubtotal_base_1 ( totalesConsorcio.set(valor, 1, numero_subclientes, i ));



       // Porcentaje de incremento trimestral por pagos extemporaneo
       subcliente.setPorcentaje_incremento(porcentaje_mora);
       // Porcentaje de incremento por extemporaneidad considerando el numero de meses de mora por cada subcliente
       subcliente.setPorcentaje_extemporaneo(subcliente.getPorcentaje_incremento() * subcliente.getMeses_incremento()  /100 );
       // Valor extemporaneo 1 correspondiente al valor sin iva para cada subcliente
       subcliente.setValor_extemporaneo_1( Util.redondear2( subcliente.getSubtotal_base_1() *   subcliente.getPorcentaje_extemporaneo(),0)  );

       // Valor de la solicitud sin iva mas el valor extemporaneo de cada subcliente
       subcliente.setSubtotal_base_2( subcliente.getSubtotal_base_1() + subcliente.getValor_extemporaneo_1()  );
       // Valor de la solicitud sin iva mas el valor extemporaneo de cada subcliente menos el valor de la cuota inicial
       subcliente.setSubtotal_base_3( subcliente.getSubtotal_base_2() - subcliente.getValor_cuota_inicial()  );

       // Valor iva total de la solicitud
       subcliente.setValor_iva(total_iva);
       // Valor iva distribuido por cada subcliente
       valor = Util.redondear2(subcliente.getValor_iva() * subcliente.getPorcentaje_base()/100,0) ;
       subcliente.setValor_base_iva(totalesConsorcio.set(valor, 2, numero_subclientes, i )  );




       // Valor extemporaneo 2 correspondiente al iva de cada subcliente
       subcliente.setValor_extemporaneo_2( Util.redondear2( subcliente.getValor_iva()  * subcliente.getPorcentaje_extemporaneo() ,0)  );

       // Valor del iva mas valor extemporaneo para cada subcliente
       subcliente.setSubtotal_iva( subcliente.getValor_base_iva() + subcliente.getValor_extemporaneo_2() ) ;

       // Valor a financiar
       subcliente.setSubtotal_a_financiar(subcliente.getSubtotal_base_3() + subcliente.getSubtotal_iva() );

       // Valor extemporaneo total
       subcliente.setValor_extemporaneo(subcliente.getValor_extemporaneo_1() + subcliente.getValor_extemporaneo_2() );



       // Calculo de la cuota de pago
       int cuotas_reales = subcliente.getPeriodo();
       double valorAFinanciar = subcliente.getSubtotal_a_financiar();
       puntosFinanciacion  = getPuntosFinanciacion("NUEVO", regulacion, valorAFinanciar, ano,
                                                    trimestre, cuotas_reales);
       double dtf_puntos = puntosFinanciacion.getPuntos();
       String tipo_puntos= puntosFinanciacion.getTipo();

       subcliente.setPuntos(dtf_puntos);
       subcliente.setTipo(tipo_puntos);
       subcliente.setClase_dtf(puntosFinanciacion.getClase_dtf());



       if(subcliente.getTipo().equalsIgnoreCase("MAXIMA")) {
           subcliente.setPorcentaje_interes(subcliente.getPuntos());
       }
       else
       {
            subcliente.setPorcentaje_interes(dtf_semana + subcliente.getPuntos());
       }

       double cuota_pago = 0.00;

       if (cuotas_reales == 1){
            cuota_pago = valorAFinanciar;
       }
       else {
           if (tipo_puntos.equalsIgnoreCase("MAXIMA")){
             cuota_pago  = Util.redondear2( get_anualidad(valorAFinanciar ,dtf_puntos , cuotas_reales ) , 0);
           }
           else {
             cuota_pago  = Util.redondear2( get_anualidad_dtf(valorAFinanciar , dtf_semana + dtf_puntos , cuotas_reales ) , 0);
           }
       }


       subcliente.setValor_cuota(cuota_pago);

       double valor_con_financiacion = cuota_pago * cuotas_reales;
       subcliente.setValor_con_financiacion(valor_con_financiacion);
       subcliente.setIntereses(valor_con_financiacion - valorAFinanciar );

       lit.set(subcliente);


}



public String  setAcciones(AccionPorSolicitud accion,  String last_update, String usuario) throws SQLException   {
    return  consorcioDao.setAcciones(accion,last_update, usuario );

}


public String setSubclientesOfertas( SubclientePorSolicitud subcliente, String simbolo_variable,
                                     String observacion_subcliente, String fecha_factura, String usuario)throws SQLException{
    return consorcioDao.setSubclientesOfertas(subcliente, simbolo_variable, observacion_subcliente, fecha_factura, usuario);
}



    public String ejecutarBatchSQL(Vector comandosSQL) throws SQLException {

        try {
            consorcioDao.ejecutarSQL(comandosSQL);
        }
        catch (Exception e) {
            return "ERROR";
        }
        return "";

    }



    /** Crea una lista de solicitudes pendientes de facturar a los clientes */
    public void buscaSolicitudParcialPorFacturar()throws SQLException{
        consorcioDao.buscaSolicitudParcialPorFacturar();
    }

    /** Retorna una lista de solicitudes pendientes de facturar a los clientes */
    public List getSolicitudParcialPorFacturar() throws SQLException{
        return consorcioDao.getSolicitudParcialPorFacturar();
    }




    /** Crea una lista de subclientes con solicitudes pendientes de facturar a los clientes */
    public void buscaSubclientePorFacturar(String id_solicitud, int parcial)throws SQLException{
        consorcioDao.buscaSubclientePorFacturar(id_solicitud, parcial);
    }

    /** Retorna una lista de subclientes con solicitudes pendientes de facturar a los clientes */
    public List getSubclientePorFacturar() throws SQLException{
        return consorcioDao.getSubclientePorFacturar();
    }



    public String insertarFacturaConsorcio(FacturaConsorcio facturaConsorcio)throws SQLException{
        return consorcioDao.insertarFacturaConsorcio(facturaConsorcio);
    }


    public String creaFacturaFiducia(SubclientePorFacturar subclientePorFacturar,
                                   FacturaConsorcio facturaConsorcio,
                                   String documento, int numeroCuota,
                                   TotalesConsorcio totalesConsorcio) {

        String error = "NO";
        double valor = 0.00;

        try {

            facturaConsorcio.setDocumento(documento);
            facturaConsorcio.setFecha_factura(subclientePorFacturar.getFecha_financiacion());


            // Calculo de la fecha de vencimiento de la factura

            DateFormat formato_fecha;
            formato_fecha = new SimpleDateFormat("yyyy-MM-dd");

            String fecha_factura_eca = facturaConsorcio.getFecha_factura();

            Calendar calendarFechaVencimiento = Calendar.getInstance();
            String ano = fecha_factura_eca.substring(0,4);
            String mes = fecha_factura_eca.substring(5,7);
            String dia = fecha_factura_eca.substring(8,10);

            calendarFechaVencimiento.set(Integer.parseInt(ano),Integer.parseInt(mes),Integer.parseInt(dia),8,0,0);
            calendarFechaVencimiento.add(Calendar.MONTH, numeroCuota);

            String fecha_vencimiento = formato_fecha.format(calendarFechaVencimiento.getTime()) ;


            facturaConsorcio.setFecha_vencimiento(fecha_vencimiento);
            facturaConsorcio.setId_cliente(subclientePorFacturar.getId_cliente() );
            facturaConsorcio.setId_solicitud(subclientePorFacturar.getId_solicitud() );
            facturaConsorcio.setNit(subclientePorFacturar.getNit());

            facturaConsorcio.setParcial(subclientePorFacturar.getParcial());


            valor = Util.redondear2(subclientePorFacturar.getVal_a_financiar() /  subclientePorFacturar.getPeriodo(),0) ;
            facturaConsorcio.setValor_capital(totalesConsorcio.set(valor, 3, subclientePorFacturar.getPeriodo(), numeroCuota ) ) ;


            valor = Util.redondear2(subclientePorFacturar.getVal_con_financiacion() /  subclientePorFacturar.getPeriodo(),0) ;
            facturaConsorcio.setValor_factura(totalesConsorcio.set(valor, 5, subclientePorFacturar.getPeriodo(), numeroCuota ) ) ;

            facturaConsorcio.setValor_interes(facturaConsorcio.getValor_factura() - facturaConsorcio.getValor_capital());
        }
        catch ( Exception e) {
            error = "SI";
        }

        return error;
    }



    public String setSolicitudSubcliente(SubclientePorFacturar subclientePorFacturar, String factura) throws SQLException {
        return consorcioDao.setSolicitudSubcliente(subclientePorFacturar, factura ) ;
    }



    public void generaCausacionIngresos(Model model, String distrito, String login, String anoFactura,
                                        String mesFactura, LogWriter logWriter) throws SQLException {                 // 2011-03-06

        generaINM(model, distrito, login, anoFactura, mesFactura, logWriter);

    }


    public void generaINM(Model model,String distrito, String login, String anoFactura,
                          String mesFactura,  LogWriter logWriter) throws SQLException {                               // 2011-03-06


        String comandoSql = "";
        @SuppressWarnings("UseOfObsoleteCollectionType")
        Vector vectorSql =new Vector();   // Almacena una lista de String SQL que actualizaran la BD en modo transaccion

        String  TIPO_DOCUMENTO_CDIAR        = model.constanteService.getValor("FINV", "TIPO_DOCUMENTO_CDIAR", "");
        String  CUENTA_DEBITO_NM_ESQUEMA_NUEVO = model.constanteService.getValor("FINV", "CUENTA_INTERES_FINTRA", "");
        String  TIPO_DOCUMENTO_FAC          = model.constanteService.getValor("FINV", "TIPO_DOCUMENTO_FAC", "");
        String  SIGLA_NM = model.constanteService.getValor("FINV", "SIGLA_NM", "");

        String  CUENTA_DEBITO_PM_ESQUEMA_NUEVO = model.constanteService.getValor("FINV", "CUENTA_DEBITO_PM_ESQUEMA_NUEVO", "");  // 16252093
        String  SIGLA_PM = model.constanteService.getValor("FINV", "SIGLA_PM", "");


        String  CUENTA_CREDITO_NM_ESQUEMA_NUEVO = model.constanteService.getValor("FINV", "CUENTA_CREDITO_NM_ESQUEMA_NUEVO", "");  // '16252093'
        String  SIGLA_INM  = model.constanteService.getValor("FINV", "SIGLA_INM", "");

        String  CUENTA_CREDITO_PM_ESQUEMA_NUEVO = model.constanteService.getValor("FINV", "CUENTA_CREDITO_PM_ESQUEMA_NUEVO", "");  // '16252094'
        String  SIGLA_IPM  = model.constanteService.getValor("FINV", "SIGLA_IPM", "");  // IPM

        String  OFICINA_PRINCIPAL = model.constanteService.getValor("FINV", "OFICINA_PRINCIPAL", "");
        String  MONEDA_LOCAL = model.constanteService.getValor("FINV", "MONEDA_LOCAL", "");
        String  BASE_LOCAL = model.constanteService.getValor("FINV", "BASE_LOCAL", "");




        String  CUENTA_ESQUEMA_ANTIGUO = model.constanteService.getValor("FINV", "CUENTA_ESQUEMA_ANTIGUO", "");
        String  CUENTA_INGRESO = model.constanteService.getValor("FINV", "CUENTA_INGRESO", "");







        // ESQUEMA NUEVO

        String errorProceso = "NO";

        int dia = Util.diasDelMes(  Integer.parseInt(mesFactura),  Integer.parseInt(anoFactura) ) ;
        String fechaFactura = anoFactura+"-"+mesFactura+"-"+Util.ceroPad(  dia, 2 ) ;


        int cantidad_facturas = consorcioDao.determinaFacturas(  CUENTA_DEBITO_NM_ESQUEMA_NUEVO, SIGLA_NM, fechaFactura, logWriter);


        if (cantidad_facturas > 0) {


            logWriter.log("Se localizaron " + cantidad_facturas + " facturas NM a procesar, en los periodos menores a : " + fechaFactura );

            // Crea el debito de la INM

            comandoSql = consorcioDao.crearDebitoNMEsquemaNuevo(distrito,TIPO_DOCUMENTO_CDIAR, CUENTA_DEBITO_NM_ESQUEMA_NUEVO,
                                                                TIPO_DOCUMENTO_FAC, SIGLA_NM, fechaFactura, login, logWriter ) ;
            if(comandoSql.equals("")) {
                errorProceso = "SI";
            }
            else {
                vectorSql.add(comandoSql);
            }

            // Crea el debito de la IPM


            comandoSql = consorcioDao.crearDebitoPMEsquemaNuevo(distrito,TIPO_DOCUMENTO_CDIAR, CUENTA_DEBITO_PM_ESQUEMA_NUEVO,
                                                                TIPO_DOCUMENTO_FAC, SIGLA_PM, fechaFactura, login, logWriter ) ;
            if(comandoSql.equals("")) {
                errorProceso = "SI";
            }
            else {
                vectorSql.add(comandoSql);
            }


            // Crea el credito de la INM

            comandoSql = consorcioDao.crearCreditoNMEsquemaNuevo(CUENTA_CREDITO_NM_ESQUEMA_NUEVO, SIGLA_INM, logWriter ) ;
            if(comandoSql.equals("")) {
                errorProceso = "SI";
            }
            else {
                vectorSql.add(comandoSql);
            }


            // Crea el credito de la IPM

            comandoSql = consorcioDao.crearCreditoPMEsquemaNuevo(CUENTA_CREDITO_PM_ESQUEMA_NUEVO, SIGLA_IPM, logWriter ) ;
            if(comandoSql.equals("")) {
                errorProceso = "SI";
            }
            else {
                vectorSql.add(comandoSql);
            }

            // Crea la cabecera del comprobante

            comandoSql = consorcioDao.crearCabeceraEsquemaNuevo( distrito,  OFICINA_PRINCIPAL,  MONEDA_LOCAL,  login,
                                      BASE_LOCAL,  TIPO_DOCUMENTO_FAC,  logWriter) ;
            if(comandoSql.equals("")) {
                errorProceso = "SI";
            }
            else {
                vectorSql.add(comandoSql);
            }


            // Inserta los comprobantes

            comandoSql = consorcioDao.insertarEsquemaNuevo(logWriter) ;
            if(comandoSql.equals("")) {
                errorProceso = "SI";
            }
            else {
                vectorSql.add(comandoSql);
            }


            // Actualiza las facturas NM e PM

            comandoSql = consorcioDao.actualizarFacturaEsquemaNuevo( TIPO_DOCUMENTO_CDIAR,  login,  distrito,
                                                                     TIPO_DOCUMENTO_FAC,  logWriter) ;
            if(comandoSql.equals("")) {
                errorProceso = "SI";
            }
            else {
                vectorSql.add(comandoSql);
            }



            // Ejecuta el batch para actualizar en productivo

            if(errorProceso.equals("NO") ) {
                // Ejecutar batch de transaccion
                String estado = model.consorcioService.ejecutarBatchSQL(vectorSql);
                if (estado.equalsIgnoreCase("ERROR")){
                    // Al final no se pudo actualizar en productivo
                    logWriter.log("Los datos no fueron registrados. Se genero un rollback sobre la base de datos ");
                    if(vectorSql.size() > 0) {
                        // Existen script SQL que se iban a procesar
                        for(int i=0; i<vectorSql.size() ; i++) {
                            // Imprimiendo los SQL que estan por ejecutarse
                            String sql = (String) vectorSql.get(i);
                            logWriter.log(sql);
                        }
                    }
                }
            }
            else {
                logWriter.log("No se pudo procesar la creacion de comprobantes diarios para NM sin PM " );
            }

            vectorSql.clear();

        } // Fin de numero de comprobantes diarios a crear

        else {

             logWriter.log("No se localizaron facturas NM a procesar, en los periodos menores a : " + fechaFactura );


        }






        /*



        // ESQUEMA ANTIGUO

        // Busca NM de esquema antiguo que no tienen PM
        // A las NM que no tienen PM, se les debe generar una INM con fecha vencimiento
        // y con cuenta diferentes. Todas estas NM deben deben ser del esquema viejo.

        errorProceso = "NO";

        comandoSql = consorcioDao.buscaNMsinPM(distrito,TIPO_DOCUMENTO_FAC,SIGLA_NM+"%",SIGLA_PM+"%", CUENTA_ESQUEMA_ANTIGUO, logWriter ) ;
        if(comandoSql.equals("")) {
            logWriter.log("Se ha producido un error de modo que no se ha generado el script SQL para busca las NM sin PM del esquema antiguo ");
            errorProceso = "SI";
        }
        else {
            vectorSql.add(comandoSql);
        }


        // Arma el item debito
        if(!comandoSql.equals("")) {
            comandoSql = consorcioDao.crearItemDebito(distrito,TIPO_DOCUMENTO_CDIAR,TIPO_DOCUMENTO_FAC, SIGLA_N,SIGLA_PM+"%",
                                                              CUENTA_ESQUEMA_ANTIGUO,login,anoVencimiento, mesVencimiento, logWriter) ;
            if(comandoSql.equals("")) {
                logWriter.log("Se ha producido un error de modo que no se ha generado el script SQL para generar el item debito del esquema antiguo ");
                errorProceso = "SI";
            }
            else {
                vectorSql.add(comandoSql);
            }
        }

        // Arma el item credito
        if(!comandoSql.equals("")) {
            comandoSql = consorcioDao.crearItemCredito(CUENTA_INGRESO, SIGLA_N+"%", logWriter) ;
            if(comandoSql.equals("")) {
                logWriter.log("Se ha producido un error de modo que no se ha generado el script SQL para crear el item credito del esquema antiguo ");
                errorProceso = "SI";
            }
            else {
                vectorSql.add(comandoSql);
            }

        }

        // Arma la cabecera
        if(!comandoSql.equals("")) {
            comandoSql = consorcioDao.crearCabecera(OFICINA_PRINCIPAL, MONEDA_LOCAL, login, BASE_LOCAL, logWriter) ;
            if(comandoSql.equals("")) {
                logWriter.log("Se ha producido un error de modo que no se ha generado el script SQL para crear la cabecera del esquema antiguo ");
                errorProceso = "SI";
            }
            else {
                vectorSql.add(comandoSql);
            }
        }

        // Actualizar en productivo
        if(!comandoSql.equals("")) {
            comandoSql = consorcioDao.actualizarProductivo(TIPO_DOCUMENTO_CDIAR, distrito, TIPO_DOCUMENTO_FAC, logWriter) ;
            if(comandoSql.equals("")) {
                logWriter.log("Se ha producido un error de modo que no se ha generado el script SQL para actualizar en productivo del esquema antiguo ");
                errorProceso = "SI";
            }
            else {
                vectorSql.add(comandoSql);
            }
        }


        if(errorProceso.equals("NO") ) {
            // Ejecutar batch de transaccion
            String estado = model.consorcioService.ejecutarBatchSQL(vectorSql);
            if (estado.equalsIgnoreCase("ERROR")){
                // Al final no se pudo actualizar en productivo
                logWriter.log("Los datos no fueron registrados. Se genero un rollback sobre la base de datos ");
                if(vectorSql.size() > 0) {
                    // Existen script SQL que se iban a procesar
                    for(int i=0; i<vectorSql.size() ; i++) {
                        // Imprimiendo los SQL que estan por ejecutarse
                        String sql = (String) vectorSql.get(i);
                        logWriter.log(sql);
                    }
                }
            }
        }
        else {
            logWriter.log("No se pudo procesar la creacion de comprobantes diarios para NM sin PM " );
        }

        vectorSql.clear();


        */
    }





    // *************************************************************************
    // *
    // *  AREA DE METODOS DEL PROCESO DE RECAUDOS
    // *
    // *************************************************************************



     public void setInsertarRecaudo( Recaudo recaudo ,String dstrct,  String  usuario , String fechaCreacion, LogWriter logWriter ) throws SQLException {



        consorcioDao.setInsertarRecaudo( recaudo , dstrct,  usuario,  fechaCreacion,  logWriter);


     }


     public void creaTablaRecaudo( LogWriter logWriter ) throws SQLException {

        consorcioDao.creaTablaRecaudo(logWriter);

     }




     public ArrayList  getRecaudoProceso(LogWriter logWriter )throws SQLException{

           return  consorcioDao.getRecaudoProceso(logWriter);

     }





     public void procesarRecaudo(Model model, String login, String fechaCreacion, LogWriter logWriter, boolean archivoMultiservicio) {



        // Constantes
        final String  NOMBRE_LIBRO_1 = "Estado Informe_de_Recaudo_MS_Junio_2010";
        final String  NOMBRE_LIBRO_2 = "Cruce Recaudos Electricaribe";
        final String  ETIQUETA_HOJA_1 = "Recaudo_Multiservicios";
        final String  ETIQUETA_HOJA_2 = "Cruce Recaudos";
        final String  ETIQUETA_HOJA_3 = "Recaudos no cruzados";
        final String  ETIQUETA_HOJA_4 = "NC Recaudos no cruzados";
        final String  ETIQUETA_HOJA_5 = "Recaudos menores no cruzados";
        final int     LINEA_TITULO_COLUMNA = 3;




        // -------------------------------------------------------------------------
        // DEFINICION PARA USAR EXCEL
        // Variables del archivo de excel

        AsposeUtil xlsUtil = new AsposeUtil();
        Workbook   wb;
        String     rutaInformes;
        String     nombre;

        int fila  = 0;

        int fila3 = 0;
        // FIN DEFINICION PARA USAR EXCEL
        // -------------------------------------------------------------------------


        try{

            rutaInformes = this.generarRUTA(login);   // crear el directorio donde va a incluirse el archivo excell
            String nombreLibro = xlsUtil.crearNombreLibro(NOMBRE_LIBRO_2, FileFormatType.EXCEL2003, true);
            wb = xlsUtil.crearLibro(nombreLibro);
            inicializarEstilos(wb);

            Worksheet sheet  = inicializarHoja( wb,ETIQUETA_HOJA_2, login, "Cruce Recaudos de Electrificadora", 0);
            Worksheet sheet3 = inicializarHoja( wb,ETIQUETA_HOJA_3, login, "Recaudos no cruzados", 1);
            Worksheet sheet4 = inicializarHoja( wb,ETIQUETA_HOJA_4, login, "NC Recaudos no cruzados", 2);
            Worksheet sheet5 = inicializarHoja( wb,ETIQUETA_HOJA_5, login, "Recaudos menores no cruzados", 3);

            // ENCABEZADO HOJA 1 : titulos de las columnas para la hoja Cruce Recaudos de Electrificadora
            String[] cabecera = {"REFERENCIA 1", "SIMBOLO VARIABLE", "DOCUMENTO", "FIDUCIA", "NOMBRE CLIENTE",
                                "FECHA FACTURA", "FECHA VENCIMIENTO", "VALOR FACTURA", "VALOR ABONO", "VALOR SALDO",
                                "VALOR INGRESO INICIAL", "VALOR APLICADO", "DIFERENCIA INICIAL", "VALOR SOBRANTE"};

            // ancho de las columnas
            float[] dimensiones = new float[]{
                15, 15, 14, 14, 50, 15, 15, 11, 11, 11, 15, 11, 12, 13
            };
            // arma los tiulos en el excell
            xlsUtil.generarTitulos(sheet,LINEA_TITULO_COLUMNA,titulo2, cabecera, dimensiones);




            // ENCABEZADO HOJA 2 : titulos de las columnas para la hoja Recaudos no cruzados


            String [] cabecera3 = {"PERIODO RECAUDO","NOM EMPRESA","NOM UNICOM","COD UNICOM" ,
                                  "GESTOR", "NOM CLI", "NIC", "NIS RAD", "NUM ACU",
                                  "SIMBOLO VAR", "F FACT", "F PUESTA", "CO CONCEPTO" ,"DESC CONCEPTO",
                                  "IMP FACTURADO CONCEPTO", "IMP PAGADO CONCEPTO", "IMP RECAUDO"};


            // ancho de las columnas
            float  [] dimensiones3 = new float [] {
                16, 35, 15, 12, 35, 35, 12, 12, 12, 12, 12, 12, 12, 35, 18, 16, 15
            };
            // arma los tiulos en el excell
            xlsUtil.generarTitulos(sheet3,LINEA_TITULO_COLUMNA,titulo2, cabecera3, dimensiones3);


            // ENCABEZADO HOJA 3 : titulos de las columnas para la hoja NC Recaudos no cruzados
            // arma los tiulos en el excell

            xlsUtil.generarTitulos(sheet4,LINEA_TITULO_COLUMNA,titulo2, cabecera3, dimensiones3);


            // ENCABEZADO HOJA 4 : titulos de las columnas para la hoja Recaudos menores no cruzados
            // arma los tiulos en el excell

            xlsUtil.generarTitulos(sheet5,LINEA_TITULO_COLUMNA,titulo2, cabecera3, dimensiones3);


            Style letraColor = letra;
            letraColor = letraAzul;
            Style dineroColor = dinero;
            dineroColor = dineroAzul;

            int COLUMNA_PRIMERA_FACTURA = 12;


            // Seleccionar los recaudos que se procesaran

            ArrayList listaRecaudoProceso = model.consorcioService.getRecaudoProceso(logWriter);

            if (listaRecaudoProceso.isEmpty()) {

                // No hay recaudos para procesar
                logWriter.log("ADVERTENCIA: No existen recaudos para procesar. \n",LogWriter.INFO);
            }
            else {

                // Hay recaudos

                List listaFacturaCabecera =  new LinkedList();

                String condicion = "";
                String ordenamiento = "";
                int encontradas = 0;

                fila = 3;
                fila3 = 3;

                for( int i= 1; i<=listaRecaudoProceso.size() ; i++ ) {

                    RecaudoProceso recaudoProceso = (RecaudoProceso) listaRecaudoProceso.get(i-1);

                    condicion = " dstrct = 'FINV' and ";
                    condicion += " tipo_documento = 'FAC' and ";
                    condicion += " substring(documento,1,2) in ('PM','RM','NM') and ";
                    condicion += (archivoMultiservicio)?" clasificacion1 !='' and":" ref1 ilike 'ACON%' and ";
                    condicion += " ref2 = '" + recaudoProceso.getSimbolo_var() + "' and ";
                    condicion += " valor_saldo >= 10 and ";
                    condicion += " reg_status != 'A' ";

                    ordenamiento = " order by  ref2, ";
                    ordenamiento += " fecha_vencimiento ";


                    listaFacturaCabecera = consorcioDao.buscaFacturaCabeceraCxC(condicion + ordenamiento , "fintra");

                    if (!listaFacturaCabecera.isEmpty()) {


                        // Se encontraron facturas con simbolo variable que cruzan con el ingreso


                        // Contando las encontradas

                        encontradas++;

                        // Determinando el color a sombrear las lineas del ingreso
                        if (Util.esImpar(encontradas)) {
                            letraColor = letraAzul;
                            dineroColor = dineroAzul;
                        }
                        else {
                            letraColor = letraVerde;
                            dineroColor = dineroVerde;
                        }

                        FacturaCabecera facturaCabecera = new FacturaCabecera();
                        Iterator it = listaFacturaCabecera.iterator();

                        double valorIngreso = recaudoProceso.getImp_recaudo();


                        int itemFactura = 0;
                        int filaPrimeraFactura = 0;


                        while (it.hasNext()  ) {

                           facturaCabecera = (FacturaCabecera)it.next();

                           itemFactura++;

                           String nombreCliente = consorcioDao.getNombreCliente(facturaCabecera.getCodcli());

                           double valorSaldo = facturaCabecera.getValor_saldo();
                           double valorAplicado = 0.00;
                           double diferenciaInicial = 0.00;
                           double valorIngresoInicial = 0.00;

                           if (valorIngreso >= valorSaldo) {
                               valorAplicado = valorSaldo;
                               valorIngreso  = valorIngreso - valorAplicado;
                           }
                           else {
                               valorAplicado = valorIngreso;
                               valorIngreso  = 0.00;
                           }

                           if(itemFactura == 1){
                               diferenciaInicial = recaudoProceso.getImp_recaudo() - facturaCabecera.getValor_saldo();
                               valorIngresoInicial =  recaudoProceso.getImp_recaudo();
                           }
                           else {
                               diferenciaInicial   = 0.00;
                               valorIngresoInicial = 0.00;
                           }


                           if (valorAplicado != 0.00) {

                               fila++;
                               int col = 0;

                               if(itemFactura == 1) {
                                   filaPrimeraFactura = fila;
                               }

                               xlsUtil.setCelda(sheet, fila  , col++ , facturaCabecera.getRef1(), letraColor  );
                               xlsUtil.setCelda(sheet, fila  , col++ , facturaCabecera.getRef2(), letraColor  );
                               xlsUtil.setCelda(sheet, fila  , col++ , facturaCabecera.getDocumento(), letraColor  );
                               xlsUtil.setCelda(sheet, fila, col++,
                                       (facturaCabecera.getClasificacion1().equals("FIDCOP") ? "COLPATRIA"
                                       : facturaCabecera.getClasificacion1().equals("CORFIA") ? "CORFICOLOMBIANA A"
                                       : facturaCabecera.getClasificacion1().equals("CORFIB") ? "CORFICOLOMBIANA B"
                                       : facturaCabecera.getClasificacion1().equals("FIDFUP") ? "FUENTE DE PAGO"
                                       : facturaCabecera.getClasificacion1().equals("FIDFIV") ? "FINTRA S.A" 
                                       : facturaCabecera.getClasificacion1().equals("FIDTSP") ? "TSP" 
                                       : facturaCabecera.getClasificacion1()), letraColor);//nuevo campo egonzalez
                               xlsUtil.setCelda(sheet, fila  , col++ , nombreCliente, letraColor  );
                               xlsUtil.setCelda(sheet, fila  , col++ , facturaCabecera.getFecha_factura(), letraColor  );
                               xlsUtil.setCelda(sheet, fila  , col++ , facturaCabecera.getFecha_vencimiento(), letraColor );
                               xlsUtil.setCelda(sheet, fila  , col++ , facturaCabecera.getValor_factura(), dineroColor  );
                               xlsUtil.setCelda(sheet, fila  , col++ , facturaCabecera.getValor_abono(), dineroColor  );
                               xlsUtil.setCelda(sheet, fila  , col++ , facturaCabecera.getValor_saldo(), dineroColor  );


                               if(itemFactura == 1) {
                                   xlsUtil.setCelda(sheet, fila  , col++ , valorIngresoInicial, dineroColor  );
                               }
                               else {
                                   xlsUtil.setCelda(sheet, fila  , col++ , "", letraColor  );
                               }

                               xlsUtil.setCelda(sheet, fila  , col++ , valorAplicado, dineroColor  );

                               if(itemFactura == 1) {
                                   xlsUtil.setCelda(sheet, fila  , col++ , diferenciaInicial, dineroColor  );
                               }
                               else {
                                   xlsUtil.setCelda(sheet, fila  , col++ , "", letraColor  );
                               }

                               xlsUtil.setCelda(sheet, fila  , col , "", letraColor  );
                               
                            }

                        } // Final del while que recorre las facturas a aplicar

                        if(valorIngreso != 0) {
                            xlsUtil.setCelda(sheet, filaPrimeraFactura  , COLUMNA_PRIMERA_FACTURA , valorIngreso, dineroColor  );
                        }
                        else {
                             xlsUtil.setCelda(sheet, filaPrimeraFactura  , COLUMNA_PRIMERA_FACTURA , "", letraColor  );
                        }

                    }
                    else {
                        // El ingreso no encontro facturas con la cual cruzar

                        fila3 = this.registrarNoCruce(sheet3, fila3, recaudoProceso.getSimbolo_var(), logWriter);



                    }
                }  // Final del for que recorre los ingresos a procesar


                this.registrarNCRecaudo(sheet4,logWriter);
                this.registrarRecaudosMenores(sheet5,logWriter);


            }  // Final de Hay Recaudos

            // Salvar la hoja electronica

            xlsUtil.cerrarLibro(wb, rutaInformes, nombreLibro, FileFormatType.EXCEL2003);
            wb = null;
            sheet = null;
            sheet3 = null;
            sheet4 = null;
            sheet5 = null;


        }
        catch (Exception e){
            logWriter.log(e.getMessage());
            logWriter.log("ERROR: Se genero error de Exception al ejecutar el Metodo : procesarRecaudo. " +
                          "                                 Mensaje : " + e.getMessage() + "\n" ,LogWriter.INFO) ;
        }
     }




     public int   registrarNoCruce(Worksheet sheet, int fila,  String simbolo_var, LogWriter logWriter )  {


        ArrayList listaRecaudoDetalle = new ArrayList();


        Style letraColor = letra;
        letraColor = letraAzul;
        Style dineroColor = dinero;
        dineroColor = dineroAzul;


         try {

            String condicion    =  " simbolo_var = '" + simbolo_var + "' ";
            String ordenamiento  = " order by  co_concepto ";

            listaRecaudoDetalle = this.getRecaudoDetalle(condicion + ordenamiento );



            for( int i= 1; i<=listaRecaudoDetalle.size() ; i++ ) {

               Recaudo recaudo = (Recaudo) listaRecaudoDetalle.get(i-1);

               fila++;

               int col = 0;

               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getPeriodo_recaudo(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNom_empresa(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNom_unicom(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getCod_unicom(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getGestor(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNom_cli(), letraColor );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNic(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNis_rad(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNum_acu(), dineroSinCentavoAzul  );

               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getSimbolo_var(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getF_fact(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getF_puesta(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getCo_concepto(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getDesc_concepto(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getImp_facturado_concepto(), dineroColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getImp_pagado_concepto(), dineroColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getImp_recaudo(), dineroColor  );

            }



         }
         catch (Exception e){
             logWriter.log(e.getMessage());
             logWriter.log("ERROR: Se genero error de Exception al ejecutar el Metodo : registrarNoCruce. " +
                          "                                 Mensaje : " + e.getMessage() + "\n" ,LogWriter.INFO) ;
         }

        return fila;

     }





     public void registrarNCRecaudo(Worksheet sheet, LogWriter logWriter )  {


        ArrayList listaRecaudoDetalle = new ArrayList();


        Style letraColor = letra;
        letraColor = letraAzul;
        Style dineroColor = dinero;
        dineroColor = dineroAzul;
        Style dineroSinCentavoColor = dinero;
        dineroSinCentavoColor = dineroSinCentavoAzul;


        int fila = 3;
        String simboloVarAnterior = "";
        int numeroSimbolo = 0;


         try {


            String condicion    =  " nic in (select distinct  nic  from con.recaudo where imp_recaudo < 0) ";
            String ordenamiento  = " order by nic, simbolo_var, co_concepto ";

            listaRecaudoDetalle = this.getRecaudoDetalle(condicion + ordenamiento );



            for( int i= 1; i<=listaRecaudoDetalle.size() ; i++ ) {

               Recaudo recaudo = (Recaudo) listaRecaudoDetalle.get(i-1);

               if(!recaudo.getSimbolo_var().equalsIgnoreCase(simboloVarAnterior)) {
                   numeroSimbolo++;
                   simboloVarAnterior = recaudo.getSimbolo_var();
               }


               // Determinando el color a sombrear las lineas del ingreso
               if (Util.esImpar(numeroSimbolo)) {
                   letraColor = letraAzul;
                   dineroColor = dineroAzul;
                   dineroSinCentavoColor = dineroSinCentavoAzul;
               }
               else {
                   letraColor = letraVerde;
                   dineroColor = dineroVerde;
                   dineroSinCentavoColor = dineroSinCentavoVerde;
               }

               fila++;

               int col = 0;

               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getPeriodo_recaudo(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNom_empresa(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNom_unicom(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getCod_unicom(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getGestor(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNom_cli(), letraColor );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNic(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNis_rad(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNum_acu(), dineroSinCentavoColor  );

               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getSimbolo_var(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getF_fact(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getF_puesta(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getCo_concepto(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getDesc_concepto(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getImp_facturado_concepto(), dineroColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getImp_pagado_concepto(), dineroColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getImp_recaudo(), dineroColor  );

            }
         }
         catch (Exception e){
             logWriter.log(e.getMessage());
             logWriter.log("ERROR: Se genero error de Exception al ejecutar el Metodo : registrarNoCruce. " +
                          "                                 Mensaje : " + e.getMessage() + "\n" ,LogWriter.INFO) ;
         }
     }






     public void registrarRecaudosMenores(Worksheet sheet, LogWriter logWriter )  {


        ArrayList listaRecaudoDetalle = new ArrayList();


        Style letraColor = letra;
        letraColor = letraAzul;

        Style dineroColor = dinero;
        dineroColor = dineroAzul;

        Style dineroSinCentavoColor = dinero;
        dineroSinCentavoColor = dineroSinCentavoAzul;

        int fila = 3;
        String simboloVarAnterior = "";
        int numeroSimbolo = 0;


         try {

            String condicion    =  " simbolo_var in  (select simbolo_var  ";
            condicion += " from con.recaudo ";
            condicion += " where nic not in (select distinct  nic  from con.recaudo where imp_recaudo < 0) ";
            condicion += " group by simbolo_var ";
            condicion += " having sum(imp_recaudo) <=  10 and sum(imp_recaudo) !=  0 ) and imp_recaudo != 0 ";

            String ordenamiento  = " order by nic, simbolo_var, co_concepto ";

            listaRecaudoDetalle = this.getRecaudoDetalle(condicion + ordenamiento );

            for( int i= 1; i<=listaRecaudoDetalle.size() ; i++ ) {

               Recaudo recaudo = (Recaudo) listaRecaudoDetalle.get(i-1);

               if(!recaudo.getSimbolo_var().equalsIgnoreCase(simboloVarAnterior)) {
                   numeroSimbolo++;
                   simboloVarAnterior = recaudo.getSimbolo_var();
               }

               // Determinando el color a sombrear las lineas del ingreso
               if (Util.esImpar(numeroSimbolo)) {
                   letraColor = letraAzul;
                   dineroColor = dineroAzul;
                   dineroSinCentavoColor = dineroSinCentavoAzul;
               }
               else {
                   letraColor = letraVerde;
                   dineroColor = dineroVerde;
                   dineroSinCentavoColor = dineroSinCentavoVerde;
               }

               fila++;

               int col = 0;

               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getPeriodo_recaudo(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNom_empresa(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNom_unicom(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getCod_unicom(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getGestor(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNom_cli(), letraColor );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNic(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNis_rad(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNum_acu(), dineroSinCentavoColor  );

               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getSimbolo_var(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getF_fact(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getF_puesta(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getCo_concepto(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getDesc_concepto(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getImp_facturado_concepto(), dineroColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getImp_pagado_concepto(), dineroColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getImp_recaudo(), dineroColor  );

            }
         }
         catch (Exception e){
             logWriter.log(e.getMessage());
             logWriter.log("ERROR: Se genero error de Exception al ejecutar el Metodo : registrarRecaudosMenores. " +
                          "                                 Mensaje : " + e.getMessage() + "\n" ,LogWriter.INFO) ;
         }
     }






    public ArrayList  getRecaudoDetalle(String condicion)throws SQLException{

        return consorcioDao.getRecaudoDetalle( condicion);
    }









        /**
         * Ubica la informacion de la ruta donde quedara el informe
         * Si no existe crea el directorio
         *
         * @author  Alvaro Pabon Martinez
         * @version %I%, %G%
         * @since   1.0
         *
         */

        public String  generarRUTA( String login) throws Exception{

            String ruta = "";

            try{



                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                ruta = rb.getString("ruta") + "/exportar/migracion/" + login;
                File archivo = new File( ruta );
                if (!archivo.exists()) archivo.mkdirs();

            }catch (Exception ex){
                Util.imprimirTrace(ex);
                throw new Exception(ex.getMessage());
            }
            return ruta;

        }





    /**
     * Metodo para definir y etiquetar la hoja de excell
     * @autor apabon
     * @param wb Libro de excell
     * @param etiqueta Nombre para etiquetar la hoja
     * @throws Exception.
     */
    private Worksheet inicializarHoja(Workbook wb, String etiqueta, String login, String titulo, int numeroHoja) throws Exception{
        try{


            Worksheets worksheets = wb.getWorksheets();
            Worksheet sheet = worksheets.addSheet();

            sheet = wb.getWorksheets().getSheet(numeroHoja);


            // sheet = wb.getWorksheets().getActiveSheet() ;

            fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            xlsUtil.etiquetarHoja(sheet, etiqueta);
            titularHoja(sheet,titulo, login);

            return sheet;

        }catch (Exception e){
            Util.imprimirTrace(e);
            throw new Exception( "Error en inicializar Hoja ....\n"  + e.getMessage() );
        }


    }


    /**
     * Metodo para crear el  archivo de excel
     * @autor apabon
     * @param sheet  Nombre de la hoja donde queda el informe
     * @param titulo Titulo en la hoja del informe
     * @throws Exception.
     */
    private void titularHoja(Worksheet sheet, String titulo, String login) throws Exception{
        try{
            fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            xlsUtil.combinarCeldas(sheet, 0, 0, 0, 8);
            xlsUtil.setCelda(sheet, 0,0, titulo, header);
            xlsUtil.setCelda(sheet, 1,0, "FECHA" , titulo1);
            xlsUtil.setCelda(sheet, 1,1, fmt.format( new Date())  , titulo1 );
            xlsUtil.setCelda(sheet, 2,0, "USUARIO", titulo1);
            xlsUtil.setCelda(sheet, 2,1, login , titulo1);


        }catch (Exception ex){
            Util.imprimirTrace(ex);
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }



    /**
     * Metodo para Incializar los colores propios y los estilos
     * @autor apabon
     * @param wb Libro de excell especifico para el cual se definen los colores y estilos propios
     * @throws Exception.
     */
    private void inicializarEstilos(Workbook wb) throws Exception{
        try{

            // colores
            xlsUtil.crearColor(wb,colorPersonalizado, "AZUL_CLARO_FINTRA", 55, 149,179,215);
            xlsUtil.crearColor(wb,colorPersonalizado, "VERDE_CLARO_FINTRA", 52, 194,214,154);
            xlsUtil.crearColor(wb,colorPersonalizado, "VERDE_FINTRA", 53, 51,153,102);
            xlsUtil.crearColor(wb,colorPersonalizado, "MARRON_CLARO_FINTRA", 51, 221,217,195);
            xlsUtil.crearColor(wb,colorPersonalizado, "GRIS", 54, 192,192,192);
            xlsUtil.crearColor(wb,colorPersonalizado, "NONE", 50, 0,0,0);
            xlsUtil.crearColor(wb,colorPersonalizado, "MEDIO", 49, 127,127,127);
            xlsUtil.crearColor(wb,colorPersonalizado, "AZUL_FORMULA", 48, 23,55,93);

            // estilos

            header  = xlsUtil.crearEstilo(wb, "Tahoma", 10, true, false, 0, "49",  com.aspose.cells.Color.GREEN, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.LEFT);
            titulo1 = xlsUtil.crearEstilo(wb, "Tahoma", 8, true, false,  0, "49", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.LEFT);
            titulo2 = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false,  0,"49", com.aspose.cells.Color.WHITE, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "VERDE_FINTRA"), TextAlignmentType.CENTER);
            titulo3 = xlsUtil.crearEstilo(wb, "Tahoma", 8, true, false,  0,"49", com.aspose.cells.Color.WHITE, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.CENTER);
            titulo4 = xlsUtil.crearEstilo(wb, "Tahoma", 8, true, false,  0,"49", com.aspose.cells.Color.WHITE, true,com.aspose.cells.Color.OLIVE, TextAlignmentType.CENTER);
            titulo5 = xlsUtil.crearEstilo(wb, "Tahoma", 14, true, false,  0,"49", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.LEFT);

            letra   = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false, 0, "49", com.aspose.cells.Color.BLACK, false, com.aspose.cells.Color.OLIVE , TextAlignmentType.LEFT);

            letraMarron = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"49", com.aspose.cells.Color.BLACK,  true, xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MARRON_CLARO_FINTRA"), TextAlignmentType.LEFT);
            letraAzul   = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"49", com.aspose.cells.Color.BLACK,  true, xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "AZUL_CLARO_FINTRA"),   TextAlignmentType.LEFT);
            letraVerde  = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"49", com.aspose.cells.Color.BLACK,  true, xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "VERDE_CLARO_FINTRA"),  TextAlignmentType.LEFT);

            letraCentrada    = xlsUtil.crearEstilo(wb, "Tahoma", 8, false, false,  0,"49", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.CENTER);
            numero           = xlsUtil.crearEstilo(wb, "Tahoma", 8, false, false,  0,"#", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.RIGHT);
            numeroCentrado   = xlsUtil.crearEstilo(wb, "Tahoma", 8, false, false,  0,"#", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.CENTER);
            dinero           = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"#,##0.00", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.RIGHT);

            dineroMarron     = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"#,##0.00", com.aspose.cells.Color.BLACK, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MARRON_CLARO_FINTRA"), TextAlignmentType.RIGHT);
            dineroAzul       = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"#,##0.00", com.aspose.cells.Color.BLACK, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "AZUL_CLARO_FINTRA"), TextAlignmentType.RIGHT);
            dineroVerde      = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"#,##0.00", com.aspose.cells.Color.BLACK, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "VERDE_CLARO_FINTRA"), TextAlignmentType.RIGHT);

            dineroSinCentavo = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"#,##0", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MARRON_CLARO_FINTRA"), TextAlignmentType.RIGHT);

            dineroSinCentavoMarron = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"#,##0", com.aspose.cells.Color.BLACK, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MARRON_CLARO_FINTRA"), TextAlignmentType.RIGHT);
            dineroSinCentavoAzul   = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"#,##0", com.aspose.cells.Color.BLACK, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "AZUL_CLARO_FINTRA"),   TextAlignmentType.RIGHT);
            dineroSinCentavoVerde  = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"#,##0", com.aspose.cells.Color.BLACK, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "VERDE_CLARO_FINTRA"),  TextAlignmentType.RIGHT);

            idMarron  = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"###0.00_____)", com.aspose.cells.Color.GRAY, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MARRON_CLARO_FINTRA"), TextAlignmentType.RIGHT);
            idAzul    = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"###0.00_____)", com.aspose.cells.Color.GRAY, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "AZUL_CLARO_FINTRA"), TextAlignmentType.RIGHT);
            idVerde   = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"###0.00_____)", com.aspose.cells.Color.GRAY, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "VERDE_CLARO_FINTRA"), TextAlignmentType.RIGHT);
            idLetra   = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"###0.00_____)", com.aspose.cells.Color.GRAY, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MARRON_CLARO_FINTRA"), TextAlignmentType.RIGHT);

            gtLetra             = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false,  0,"49", com.aspose.cells.Color.WHITE, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MEDIO"), TextAlignmentType.LEFT);
            gtDineroSinCentavo  = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false, 0, "#,##0", com.aspose.cells.Color.WHITE, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MEDIO"),  TextAlignmentType.RIGHT);
            gtId                = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false, 0, "###0.00_____)", com.aspose.cells.Color.GRAY, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MEDIO"),  TextAlignmentType.RIGHT);

            fLetra              = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false, 0, "49", com.aspose.cells.Color.WHITE, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "AZUL_FORMULA"), TextAlignmentType.LEFT);
            fDineroSinCentavo   = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false, 0, "#,##0", com.aspose.cells.Color.WHITE, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "AZUL_FORMULA"),  TextAlignmentType.RIGHT);
            fId                 = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false, 0, "###0.00_____)", com.aspose.cells.Color.GRAY, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "AZUL_FORMULA"),  TextAlignmentType.RIGHT);
            T1Letra             = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false, 1, "49", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "AZUL_FORMULA"), TextAlignmentType.LEFT);
            numeroNegrita  = xlsUtil.crearEstilo(wb, "Tahoma", 8, true, false, 0, "#", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.RIGHT);
            porcentaje     = xlsUtil.crearEstilo(wb, "Tahoma", 8, false, false, 0, "0.00%", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.RIGHT);
            ftofecha       = xlsUtil.crearEstilo(wb, "Tahoma", 8, false, false, 0, "yyyy-mm-dd", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.CENTER);

        }catch (Exception e){
            Util.imprimirTrace(e);
            throw new Exception("Error en Inicializar Estilos .... \n" + e.getMessage());
        }

   }





    // *************************************************************************
    // *
    // *  FIN DEL AREA DE METODOS DEL PROCESO DE RECAUDOS
    // *
    // *************************************************************************







    /* Area original */






    public double get_dtf_mv (double r) {

        double ipa = (r/100)/4;                            // trimestre anticipado
        double ipv = ipa/(1-ipa);                          // trimestre vencido
        double iev =  Math.pow((1+ipv),4) - 1;             // efectivo anual
        double iea =  Math.pow((1-ipa),-4) - 1;            // efectivo anual
        double rn = 1/12;
        double ip  =  Math.pow((1+ iea),0.083333333) - 1;  // Tasa periódica a partir de la tasa efectiva anual

        return ip * 100;
    }


    public double get_anualidad_dtf(double vp , double dtf , double n ) {

        // Sirve para pasar conseguir el valor de una anualidad especificando una DTF + puntos que sera convertida en Mensual Vencida
        // (DTF dada en TA+puntos a una MV)

        // r = anual trimestre anticipado.

        double i = get_dtf_mv(dtf);
        double r = Math.pow((1+(i/100)),n);
        double factor =  (r-1)/(i/100*r);
        double a = vp / factor;

        return a;
    }


    public double get_anualidad(double vp , double i , double n ) {

        // Sirve para pasar conseguir el valor de una anualidad especificando un interes en  Mensual Vencida

        // r = anual trimestre anticipado.

        double r = Math.pow((1+(i/100)),n);
        double factor =  (r-1)/(i/100*r);
        double a = vp / factor;

        return a;
    }



    public PuntosFinanciacion getPuntosFinanciacion(String esquema, String regulacion, double valor, String ano,
                                                    String trimestre, int cuotas)throws SQLException{
        return consorcioDao.getPuntosFinanciacion(esquema, regulacion, valor,  ano,
                                               trimestre, cuotas);
    }



    public String getTrimestre(String mes) {

        int mesNumerico = Integer.parseInt(mes);
        String trimestre   = "1";

        if (mesNumerico <= 3) {
            trimestre = "1";
        }
        else if (mesNumerico <= 6){
            trimestre = "2";
        }
        else if (mesNumerico <= 9){
            trimestre = "3";
        }
        else {
            trimestre = "4";
        }
        return trimestre;
    }









}



