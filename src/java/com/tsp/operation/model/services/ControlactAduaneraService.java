/***********************************************************************************
 * Nombre clase : ............... ControlactAduaneraService.java                   *
 * Descripcion :................. Clase que maneja los Servicios                   *
 *                                asignados al Model relacionados con el           *
 *                                programa de controlactAduanera                   *
 * Autor :....................... Ing. Diogenes Antonio Bastidas Morales           *
 * Fecha :........................ 30 de noviembre de 2005, 09:35 AM               *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/

package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  dbastidas
 */
public class ControlactAduaneraService {
    
    
    private ControlactAduaneraDAO conact;
    /** Creates a new instance of ControlactAduaneraService */
    public ControlactAduaneraService() {
        conact = new ControlactAduaneraDAO();
    }
    
    /**
     * Metodo reporteActividadAduanera, lista todas la remesa dadas entra un rango de fechas
     *  que el wo_type sea RM,RC,RE, ademas que el reg_status <> C (las que no estan cumplidas)
     * @param: fecha inicio y fecha fin del rango
     * @see: reporteActividadAduanera - ControlactAduaneraDAO
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    
    public void reporteActividadAduanera(String fecini, String fecfin ) throws SQLException{
        try{
            conact.reporteActividadAduanera(fecini,fecfin);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo getActividadAduanera, retrorna la lista de actividades aduaneras
     * @see:  getActividadAduanera - ControlactAduaneraDAO
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    
    public List getActividadAduanera(){
        return conact.getActividadAduanera();
    }
    
    
    /**
     * Metodo buscarPlanillaRemesa, busca las planillas relacionadas a una  remesa
     * @param: numero de la remesa
     * @see: buscarPlanillaRemesa - ControlactAduaneraDAO
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarPlanillaRemesa( String numrem ) throws SQLException{
        try{
            conact.buscarPlanillaRemesa(numrem);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo getPlanillasRem, retrorna la lista de las planillas realacionadas a una remesa
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public List getPlanillasRem (){
        return conact.getPlanillasRem();
    }
    
}
