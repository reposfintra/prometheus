/*
* Nombre        ReporteDevolucionesService.java
* Descripci�n   Clase para el acceso a los datos del reporte de devoluciones
* Autor         Alejandro Payares
* Fecha         1 de septiembre de 2005, 09:05 AM
* Versi�n       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/


package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.ReporteDevolucionesDiscrepDAO;


/**
 * Clase para el acceso a los datos del reporte de devoluciones
 * @author  Alejandro Payares
 */
public class ReporteDevolucionesDiscrepService {
    
    private ReporteDevolucionesDiscrepDAO dao;
    
    /** Creates a new instance of ConfigurarReporteClientesService */
    public ReporteDevolucionesDiscrepService() {
        try {
            dao = new ReporteDevolucionesDiscrepDAO();
        }
        catch( Exception ex ){
            ex.printStackTrace();
        }
    }
    
    /**
     * Buscar las devoluciones hechas por un cliente determinado.
     * @param loggedUser Usuario de la sesi�n activa.
     * @param args Criterios de b�squeda. Son los siguientes:
     * <OL>
     *  <LI>Cliente (Requerido)
     *  <LI>Fecha Inicial (Opcional)
     *  <LI>Fecha Final (Opcional)
     *  <LI>Tipo de Devoluci�n (Requerido)
     *  <LI>N�mero del Pedido (Este anula los dem�s)
     * </OL>
     * @throws SQLException Si un error de acceso a la base de datos ocurre.
     */
    public void buscarDevoluciones(com.tsp.operation.model.beans.Usuario loggedUser, String[] args) throws java.sql.SQLException {
        try{
        dao.buscarDevoluciones(loggedUser, args);
        }catch(Exception e){
            e.printStackTrace();
            //System.out.println("mensaje --"+ e.getMessage());
        }
    }
    
    /**
     * Permite obtener la lista donde estan almacenados los datos del reporte,
     * esta lista es llenada por el metodo buscarDevoluciones(...). , la lista
     * contiene en cada nodo un objeto Hashtable cuyas claves son los nombres de
     * los campos del reporte. Para obtener acceso a cada registro, basta con
     * recorrer la lista y extraer cada elemento Hashtable, y para obtener el valor
     * de un campo, basta con pasarle por parametro al metodo get(...) del Hashtable
     * el nombre del campo requerido, un ejemplo de esto ser�a asi:
     * <code>
     * <u>
     *    dao.buscarDevoluciones(... parametros ...);
     *    String [] campos = dao.obtenerCamposReporteDevoluciones();
     *    LinkedList lista = obtenerDatosDevoluciones();
     *    Iterator ite = lista.iterator();
     *    while( ite.hasNext() ) {
     *    <u>
     *        Hashtable fila = (Hashtable) ite.next();
     *        for( int i = 0; i < campos.length; i++ ) {
     *        <u>
     *            String valor = ((String)fila.get(campos[i]))
     *            //System.out.println(campos[i] + " = " + valor );
     *        </u>
     *        }
     *    </u>
     *    }
     * </u>
     * <code>
     * @return La lista con los datos del reporte.
     */ 
    public java.util.LinkedList obtenerDatosDevoluciones(){
        return dao.obtenerDatosDevoluciones();
    }
    
    /**
     * Este m�todo permite obtener los titulos del encabezado del reporte de
     * devoluciones. El Hashtable que es devuelto contiene los titulos de todos
     * los campos del reporte y la clave de cada titulo es su respectivo nombre
     * de campo en la Base de datos, para obtener los nombre de los campos del
     * reporte utilize el metodo <CODE>String [] obtenerCamposReporteDevoluciones()</CODE>.
     * @throws SQLException Si algun problema ocurre en el acceso a la Base de Datos.
     * @return Una tabla con los titulos de los campos del reporte de devoluciones.
     */   
    public java.util.Hashtable obtenerTitulosDeReporteDevoluciones() throws java.sql.SQLException{
        return dao.obtenerTitulosDeReporteDevoluciones();
    }
    
    /**
     * Devuelve un array de tipo String [] donde se encuentran almacenados los nombres
     * de los campos del reporte de devoluciones.
     * @return El array con los nombres de los campos del reporte de devoluciones.
     */  
    public String [] obtenerCamposReporteDevoluciones(){
        return dao.obtenerCamposReporteDevoluciones();
    }
    
}
