/**
 * Nombre        PlanillaCostosDAO.java
 * Descripci�n
 * Autor         Mario Fontalvo Solano
 * Fecha         9 de mayo de 2006, 09:16 AM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.PlanillaCostosDAO;
import com.tsp.operation.model.beans.Remesa;
import com.tsp.operation.model.beans.Planilla;
import com.tsp.operation.model.beans.SJRuta;
import java.util.*;


public class PlanillaCostosService {
    
    private PlanillaCostosDAO dataAccess;
    private SJCostoService    costoSvc;
    private Vector listaPlanillas;
    private Vector listaRemesas;
    private String estandarAnterior;
    
    
    /** Crea una nueva instancia de  PlanillaCostosDAO */
    public PlanillaCostosService() {
        dataAccess = new PlanillaCostosDAO();
        costoSvc   = new SJCostoService();
    }
    
    
    
    /**
     * Metodo para extraer las remesas de una planilla
     * @autor mfontalvo
     * @param numpla numero de la planilla.
     * @return Vector listado de remesas.
     * @throws Exception.
     */    
    public void obtenerRemesasPlanilla (String numpla) throws Exception {
        try{
            listaRemesas = dataAccess.obtenerRemesasPlanilla(numpla);
        }catch (Exception ex){
            throw new Exception("Error en PlanillaCostosService.obtenerRemesasPlanilla() ....\n"+ex.getMessage());
        }
    }
    
    
    /**
     * Metodo para extraer las planillas de una remesa
     * @autor mfontalvo
     * @param numrem numero de la remesa.
     * @throws Exception.
     */
    public void obtenerPlanillasRemesa (String numrem) throws Exception {
        try{
            listaPlanillas = dataAccess.obtenerPlanillasRemesa(numrem);
        }catch (Exception ex){
            throw new Exception("Error en PlanillaCostosService.obtenerPlanillasRemesa() ....\n"+ex.getMessage());
        }        
    }
    
    
    
    /**
     * Metodo para extraer las planillas por rango de fechas
     * @autor mfontalvo
     * @param fechaInicial fecha inicial de consulta
     * @param fechaFinal fecha final de consulta de planillas
     * @throws Exception.
     */    
    public void obtenerRemesasPorPeriodosDePlanilla (String fechaInicial, String fechaFinal) throws Exception {
        try{
            listaRemesas = dataAccess.obtenerRemesasPorPeriodosDePlanilla(fechaInicial, fechaFinal);
        }catch (Exception ex){
            throw new Exception("Error en PlanillaCostosService.obtenerRemesasPorPeriodosDePlanilla() ....\n"+ex.getMessage());
        }                
    }
    
    
    /**
     * Procedimineto para generar los porcentajes de utilidad de una planilla
     * @autor mfontalvo
     * @param numpla numero de la planilla
     * @throws Exception.
     */
    
    public void generarCostos (String numpla) throws Exception{
        try{
            obtenerRemesasPlanilla(numpla); 
            Vector remesas = getListaRemesas();
            for (int  i = 0; i < remesas.size(); i++ ){
                Remesa r = (Remesa) remesas.get(i);
                generarCostos(r);
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ("Error en PlanillaCostosService.generarCostos() ....\n" +ex.getMessage());
        }
    }
    
    
    
    /**
     * Procedimiento para generar los porcentajes de utilidad de planillas de un 
     * periodo
     * @autor mfontalvo
     * @param fechaInicial fecha inicial del periodo
     * @param fechaFinal fecha final del periodo
     * @throws Exception.
     */
    public void generarCostos (String fechaInicial, String fechaFinal) throws Exception{
        try{
            obtenerRemesasPorPeriodosDePlanilla (fechaInicial, fechaFinal);
            Vector listado = getListaRemesas();
            if (listado!=null){
                for (int i=0; i<listado.size(); i++){
                    Remesa r = (Remesa) listado.get(i);
                    generarCostos( r );
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ("Error en PlanillaCostosService.generarCostos() ....\n" +ex.getMessage());
        }
    }
     
    
    
    /**
     * Procedimiento para generar los porcentajes de utilidad de planillas de un 
     * periodo
     * @autor mfontalvo
     * @param Remesa remesa a procesar
     * @throws Exception.
     */    
    public void generarCostos (Remesa r) throws Exception{
        try{
            if (r!=null){
                obtenerPlanillasRemesa( r.getNumrem() );
                Vector planillas = getListaPlanillas();
                if (verificacionPlanillaTerminada(r, planillas) || !isReExpedicion(r) ){
                    generarPorcentajes(r, planillas);
                }
                else {
                    
                    // verificacion para evitar generar los costos del mismo estandar
                    if (estandarAnterior==null || !estandarAnterior.equals(r.getStd_job_no()) ){
                        costoSvc.obtenerCostos(r.getDistrito(), r.getStd_job_no());
                        costoSvc.inicializarIndices();
                        estandarAnterior =  r.getStd_job_no();
                    }
                    
                    
                    // armar rutas de la ultima planilla hasta el destino de std.
                    // si no hay pundo de conexion se calculan todo el tramo del estandar
                    Planilla p = obtenerUltimaPlanilla(planillas);
                    if (p!=null)
                        costoSvc.generarRutas( p.getDespla() , r.getDesrem());
                    else
                        costoSvc.generarRutas( r.getOrirem() , r.getDesrem());
                    
                    
                    
                    // obtencion de la ruta minima
                    SJRuta ruta = (SJRuta) costoSvc.obtenerRutaMinima();
                    if (ruta!=null){
                        Planilla pfinal = new Planilla();
                        pfinal.setNumpla     ( "ESTIMADA" );
                        pfinal.setOripla     ( (p!=null?p.getDespla() : r.getOrirem() ) );
                        pfinal.setDespla     ( r.getDesrem() );
                        pfinal.setNomori     ( (p!=null?p.getNomdest()  : r.getNombreOrigen() ) );
                        pfinal.setNomdest    ( r.getNombreDestino() );
                        pfinal.setVlrPlanilla( ruta.getCostoTotal() );
                        pfinal.setPorcentaje ( 100  );
                        pfinal.setTasa       ( 1    );
                        pfinal.setCurrency   ( "PES");
                        pfinal.setPlaveh     ( ""   );
                        pfinal.setTipoviaje  ( ""   );
                        pfinal.setMoneda     ( ""   );
                        planillas.add(pfinal);
                    }
                    generarPorcentajes(r, planillas);
                }
                r.setPlanillas(planillas);
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ("Error en PlanillaCostosService.generarCostos() ....\n" +ex.getMessage());
        }
    }    
    
    
    
    /**
     * Metodo que indica s una remesa es de re-expedicion o no
     * @autor mfontalvo
     * @param Remesa remesa a procesar
     * @return boolean indica si de re-expedicion o no
     */
    private boolean isReExpedicion (Remesa r){
        boolean estado = false;
        if (r!=null){
            if (r.getTipoViaje().equals("RC") ||
                r.getTipoViaje().equals("RE") ||
                r.getTipoViaje().equals("RM") )
                estado = true;
        }
        return estado;
    }
    
    
    /**
     * Metodo que indica si alguna planilla concuerda con el destino de la remesa
     * @autor mfontalvo
     * @param Remesa remesa a verificar
     * @param Vector listado de planillas a verificar
     * @return boolean indica algunas de la planilla a llegado al destino de la remesa
     */
    private boolean verificacionPlanillaTerminada (Remesa r, Vector lista){
        boolean estado = false;
        for (int i = 0; i<lista.size() && !estado ; i++){
            Planilla p = (Planilla) lista.get(i);
            if (p.getDespla().equals(r.getDesrem()) ||
                p.getDestinoRelacionado().equals(r.getDestinoRelacionado()) ||
                p.getDestinoRelacionado().equals(r.getDesrem()) ||
                p.getDespla().equals(r.getDestinoRelacionado())){
                estado = true;
            }
        }
        return estado;
    }
    
    
    /**
     * Metodo que obtiene la ultima planilla despachada para 
     * armar la ruta estimada desde este punto
     * @autor mfontalvo
     * @param planillas listado de planillas a verificar
     * @return Planilla ultima planilla despachada
     */
    private Planilla obtenerUltimaPlanilla (Vector planillas){
        TreeMap IndicesIniciales = costoSvc.getIndicesIniciales();
        if (planillas!=null && !planillas.isEmpty()){
            
            for (int i = planillas.size()-1; i>=0  ; i--){
                Planilla p = (Planilla) planillas.get( i );
                if (  IndicesIniciales.get(p.getDespla())!=null ){
                    return p;
                }
            }
        }
        return null;
    }
    
    
    
    
    /**
     * Metodo para generar los porcentajes de utilidad de las planillas
     * con respecto a la remesa
     * @autor mfontalvo
     * @param Remesa remesa a verificar
     * @param Vector listado de planillas a verificar
     */
     private void generarPorcentajes(Remesa rem, Vector planillas){
        double total = 0;
        // totales y asginacion de valores parciales
        for (int i = 0; i < planillas.size(); i++  ){
            Planilla p = (Planilla) planillas.get(i);
            if ( !p.getTipoviaje().equalsIgnoreCase("VAC") || planillas.size()==1 ){
                p.setVlrParcial( p.getVlrPlanilla() * p.getTasa() * p.getPorcentaje() / (double) 100 );
                total += p.getVlrParcial();
            }
        }
        rem.setValorTotalPlanillas(total);

        // porcentaje prorrateados contra la remesa
        for (int i = 0; i < planillas.size() && total!=0 ; i++  ){
            Planilla p = (Planilla) planillas.get(i);
            if ( !p.getTipoviaje().equalsIgnoreCase("VAC") || planillas.size()==1 ){
                p.setPorcentajeProrrateo( (p.getVlrParcial() / total) * (double) 100);
                p.setVlrProrrateado     ( (rem.getValorRemesa() * rem.getTasa() ) * p.getPorcentajeProrrateo() / (double) 100);
            }
        }     
    }
    
    
    
    
    /**
     * Getter for property listaPlanillas.
     * @return Value of property listaPlanillas.
     */
    public java.util.Vector getListaPlanillas() {
        return listaPlanillas;
    }
    
    /**
     * Setter for property listaPlanillas.
     * @param listaPlanillas New value of property listaPlanillas.
     */
    public void setListaPlanillas(java.util.Vector listaPlanillas) {
        this.listaPlanillas = listaPlanillas;
    }
    
    
    /**
     * Getter for property listaRemesas.
     * @return Value of property listaRemesas.
     */
    public java.util.Vector getListaRemesas() {
        return listaRemesas;
    }
    
    /**
     * Setter for property listaRemesas.
     * @param listaRemesas New value of property listaRemesas.
     */
    public void setListaRemesas(java.util.Vector listaRemesas) {
        this.listaRemesas = listaRemesas;
    }
    
    
    /**
     * Modificado el : 30 de septiembre de 2006, 10:01 AM
     * Modificado por : LREALES
     */
      
    /** Funcion publica que obtiene el metodo existeReporteControl del DAO */
    public boolean existeReporteControl ( String login_usuario ) throws Exception {
        
        dataAccess.existeReporteControl ( login_usuario );
        return dataAccess.existeReporteControl( login_usuario );
        
    }
    
    /** Funcion publica que obtiene el metodo insertarReporteControl del DAO */
    public void insertarReporteControl ( String login_usuario, String nombre_reporte ) throws Exception {
        
        dataAccess.insertarReporteControl ( login_usuario, nombre_reporte );
        
    }
    
    /** Funcion publica que obtiene el metodo actualizarReporteControl del DAO */
    public void actualizarReporteControl ( String nombre_reporte, String login_usuario ) throws Exception {
        
        dataAccess.actualizarReporteControl ( nombre_reporte, login_usuario );
        
    }
    
    /**
     * Metodo para actualizar los campos de utilidad en la tabla de plarem.
     * @autor mfontalvo
     * @param numrem, numero de la remesa.
     * @param numpla, numero de la planilla.
     * @param vlrpla, valor de la planilla.
     * @param vlrpla_pro, valor de la plamilla prorrateado.
     * @param vlrrem, valor de la remesa
     * @param vlrrem_pro, valor de la remesa prorrateado.
     * @throws Exception.
     */    
    public void updatePlarem(String numrem, String numpla, double vlrpla, double vlrpla_pro, double vlrrem, double vlrrem_pro) throws Exception {
        dataAccess.updatePlarem(numrem, numpla, vlrpla, vlrpla_pro, vlrrem, vlrrem_pro);
    }
   
}
