/************************************************************************************
 * Nombre clase : ............... ProveedorEscoltaService.java                      *
 * Descripcion :................. Clase que maneja los Servicios                    *
 *                                asignados al Model relacionados con el            *
 *                                programa de Proveedores de Escoltas               * 
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                      *
 * Fecha :....................... 28 de Noviembre de 2005, 4:02 PM                  *
 * Version :..................... 1.0                                               *
 * Copyright :................... Fintravalores S.A.                           *
 ************************************************************************************/
package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;

public class ProveedorEscoltaService {
    
    public Proveedor_EscoltaDAO dao = new Proveedor_EscoltaDAO();
    
    public ProveedorEscoltaService() {
    }   
     /**
     * Metodo insertarProveedorEscolta, ingresa una tarifa o precio del escolta de proveedores     
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : Proveedor Escolta  proveedor
     * @see: insertarProveedorEscolta - Proveedor_EscoltaDAO
     * @version : 1.0
     */
    public void insertarProveedorEscolta(Proveedor_Escolta prov) throws SQLException {
        dao.insertarProveedorEscolta(prov);
    }
     /**
     * Metodo existeProveedorEscolta, verifica que no exista una tarifa ya ingresada por un 
     * proveedor para un origen y destino determinado
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : Proveedor Escolta  proveedor
     * @see: existeProveedorEscolta - Proveedor_EscoltaDAO      
     * @version : 1.0
     */
    public boolean existeProveedorEscolta(Proveedor_Escolta prov) throws SQLException {
        return dao.existeProveedorEscolta(prov);
    }
     /**
     * Metodo buscarProveedorEscolta, busca tarifa(s) de un proveedor especifico
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : String nit, String origen 
     * @see: buscarProveedorEscolta - Proveedor_EscoltaDAO      
     * @version : 1.0
     */
    public void buscarProveedorEscoltas(String nit,String origen) throws SQLException {
        dao.buscarProveedorEscolta(nit,origen);
    }
    public Vector getVectorProveedores() {
        return dao.getVectorProveedores();
    }
     /**
     * Metodo updateProveedorEscolta, actualiza la tarifa o el codigo contable de un proveedor de
     * escolta especifico
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : Proveedor Escolta  proveedor
     * @see: updateProveedorEscolta - Proveedor_EscoltaDAO      
     * @version : 1.0
     */
    public void updateProveedorEscolta(Proveedor_Escolta prov) throws SQLException {    
        dao.updateProveedorEscolta(prov);
    }
}
