/*
 * MigracionFacturasClientesService.java
 *
 * Created on 25 de agosto de 2006, 11:33 AM
 */

package com.tsp.operation.model.services;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.threads.MigracionFacturasClientesThread;
import com.tsp.operation.model.Model;
/**
 *
 * @author  ALVARO
 */
public class MigracionFacturasClientesService {
    MigracionFacturasClientesDao mfcd;
    MigracionFacturasClientesThread h;
    /** Creates a new instance of MigracionFacturasClientesService */
    public MigracionFacturasClientesService() {
        mfcd = new MigracionFacturasClientesDao();
    }
    
    public boolean buscarFacturasClientesCabeceraOracle(String f1, String f2)throws SQLException{
        try{
            return  mfcd.buscarFacturasClientesCabeceraOracle(f1,f2);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public boolean buscarFacturasClientesDetallesOracle()throws SQLException{
        try{
            return mfcd.buscarFacturasClientesDetallesOracle(); 
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public boolean ingresarFacturaCabecera(String usuario,String dstrct,String base)throws SQLException{
        try{
            return mfcd.ingresarFacturaCabecera(usuario,dstrct,base); 
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public boolean ingresarFacturaDetalle(String usuario,String dstrct,String base)throws SQLException{
        try{
            return mfcd. ingresarFacturaDetalle(usuario,dstrct,base); 
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public java.lang.String getFecha1() {
        return mfcd.getFecha1();
    }
    public java.lang.String getFecha2() {
        return mfcd.getFecha2();
    }
    public void setMigracionFacturasClientes(String fechaInicio,String fechaFinal,Model model, Usuario usuario) {
      try{
        h = new MigracionFacturasClientesThread(fechaInicio,fechaFinal,model, usuario);  
      }catch(Exception e){
          e.printStackTrace();
      }  
    }
    
     public java.util.Vector getVFacturaCabecera() {
        return mfcd. getVFacturaCabecera();
    }
     
     public java.util.Vector getVFacturaDetalle() {
        return mfcd.getVFacturaDetalle();
    }
}

