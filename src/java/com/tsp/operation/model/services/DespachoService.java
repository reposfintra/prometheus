/***********************************************************************************
 * Nombre clase : ............... DespachoService.java                             *
 * Descripcion :................. Clase que maneja los Service que tienen relacion *
 *                                directa con los daos.                            *
 * Autor :....................... Ing.Karen Reales                                 *
 * Fecha :........................Enero 31 de 2006, 08:36 AM                       *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/
package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;


public class DespachoService {
    
    private DespachoDAO despacho;
    /** Creates a new instance of ActividadService */
    public DespachoService() {
        despacho = new DespachoDAO();
    }
    public DespachoService(String dataBaseName) {
        despacho = new DespachoDAO(dataBaseName);
    }
    
    /**
     * Metodo InsertarDespacho, ingresa un registros en las diferentes tablas.
     * @param: Vector de Comandos
     * @autor : Ing. Karen Reales.
     * @version : 1.0
     */
    public void insertar(Vector comandos) throws SQLException {
        despacho.insertar(comandos);
    }
    
}
