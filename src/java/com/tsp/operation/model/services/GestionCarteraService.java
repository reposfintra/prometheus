/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * GestionCarteraService.java :
 * Copyright 2010 Rhonalf Martinez Villa <rhonaldomaster@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tsp.operation.model.services;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.html.simpleparser.StyleSheet;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.GestionCarteraDAO;
import com.tsp.util.Util;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;

/**
 *
 * @author rhonalf
 */
public class GestionCarteraService {

    private GestionCarteraDAO cdao;

    public GestionCarteraService() {
        cdao = new GestionCarteraDAO();
    }
    public GestionCarteraService(String dataBaseName) {
        cdao = new GestionCarteraDAO(dataBaseName);
    }

    /**
     * Busca datos de clientes con negocios en estado aprobado y/o transferido dado un filtro
     * @param filtro Columna de la tabla cliente aplicada como filtro
     * @param cadena Dato que se quiere buscaar
     * @param tipodoc Titulo valor del negocio (CH,PG,LT)
     * @return Listado con los registros que coincidieron
     * @throws Exception Cuando ocurre un error
     */
    public ArrayList<ClienteNeg> buscarDatos(String filtro, String cadena, String tipodoc, String conv_sect_subs, String sald, String fecha) throws Exception {
        ArrayList<ClienteNeg> lista = null;
        /*System.out.println("filtro: "+filtro);
        System.out.println("cadena: "+cadena);
        System.out.println("tipodoc: "+tipodoc);*/
        try {
            lista = cdao.buscarDatos(filtro, cadena, tipodoc, conv_sect_subs, sald, fecha);
        } catch (Exception e) {
            throw new Exception("Error en buscarDatos en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    /**
     * Busca los negocios de un cliente que tengan un tipo de titulo valor
     * @param nit Nit del cliente
     * @param tipodoc Tipo del titulo valor (CH,PG,LT)
     * @return Listado con los datos encontrados
     * @throws Exception Cuando hay un error
     */
    public ArrayList<Negocios> listaNegocios(String nit, String tipodoc, String fecha) throws Exception {
        ArrayList<Negocios> lista = null;
        try {
            lista = cdao.listaNegocios(nit, tipodoc, fecha);
        } catch (Exception e) {
            throw new Exception("Error en listaNegocios en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    /**
     * Datos del encabezado de la pagina /jsp/fenalco/clientes/facturas_cartera.jsp
     * @param cod_neg Codigo del negocio
     * @return Listado con los datos
     * @throws Exception Cuando hay error
     */
    public ArrayList<String> cabeceraTabla(String cod_neg) throws Exception {
        ArrayList<String> lista = null;
        try {
            lista = cdao.cabeceraTabla(cod_neg);
        } catch (Exception e) {
            throw new Exception("Error en cabeceraTabla en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    /**
     * Datos de las facturas de un negocio
     * @param cod_neg Codigo del negocio
     * @return Listado con los datos
     * @throws Exception Cuando hay error
     */
    public ArrayList<factura> facturasNegocio(String cod_neg) throws Exception {
        ArrayList<factura> lista = null;
        try {
            lista = cdao.facturasNegocio(cod_neg);
        } catch (Exception e) {
            throw new Exception("Error en facturasNegocio en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    /**
     * Datos de las facturas de un negocio
     * @param cod_neg Codigo del negocio
     * @param filtro Estado en el que estan las facturas
     * @return Listado con los datos
     * @throws Exception Cuando hay error
     */
    public ArrayList<factura> facturasNegocio(String cod_neg, String filtro) throws Exception {
        ArrayList<factura> lista = null;
        try {
            lista = cdao.facturasNegocio(cod_neg, filtro);
        } catch (Exception e) {
            throw new Exception("Error en facturasNegocio (2) en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    /**
     * Datos de los ingresos de un negocio. Incluye anulados
     * @param documento Numero de la factura
     * @return Listado con los datos
     * @throws Exception Cuando hay un error
     */
    public ArrayList<Ingreso> ingresosNegocio(String cod_neg) throws Exception {
        ArrayList<Ingreso> lista = null;
        try {
            lista = cdao.ingresosNegocio(cod_neg);
        } catch (Exception e) {
            throw new Exception("Error en ingresosNegocio en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    /**
     * Encuentra datos del cliente asociado a un negocio. Se usa en la pagina del estado de cuenta
     * @param cod_neg El codigo del negocio
     * @return String con el dato encontrado
     * @throws Exception Cuando hay error
     */
    public String nitNegocio(String cod_neg) throws Exception {
        String nit = "";
        try {
            nit = cdao.nitNegocio(cod_neg);
        } catch (Exception e) {
            throw new Exception("Error en nitNegocio en GestionCarteraService.java: " + e.toString());
        }
        return nit;
    }

    /**
     * Encuentra el saldo de un negocio con la diferencia entre sus facturas y los ingresos de las facturas
     * @param cod_neg El codigo del negocio
     * @return String con el dato buscado
     * @throws Exception Cuando hay error
     */
    public String saldoNegocio(String cod_neg) throws Exception {
        String sal = "";
        try {
            sal = cdao.saldoNegocio(cod_neg);
        } catch (Exception e) {
            throw new Exception("Error en saldoNegocio en GestionCarteraService.java: " + e.toString());
        }
        return sal;
    }

    /**
     * Datos de los ingresos de una factura
     * @param documento Numero de la factura
     * @return Listado con los datos
     * @throws Exception Cuando hay un error
     */
    public ArrayList<Ingreso> ingresosFactura(String documento) throws Exception {
        ArrayList<Ingreso> lista = null;
        try {
            lista = cdao.ingresosFactura(documento);
        } catch (Exception e) {
            throw new Exception("Error en ingresosFactura en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    /**
     * Encuentra las facturas afectadas por un ingreso
     * @param coding El numero del ingreso
     * @return Listado con las facturas afectadas
     * @throws Exception Cuando hay error
     */
    public ArrayList<factura> facturasIngreso(String coding) throws Exception {
        ArrayList<factura> lista = null;
        try {
            lista = cdao.facturasIngreso(coding);
        } catch (Exception e) {
            throw new Exception("Error en facturasIngreso en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    /**
     * Lista la gestion hecha sobre una factura
     * @param documento La factura a buscar
     * @return Listado con los datos encontrados
     * @throws Exception Cuando hay un error
     */
    public ArrayList<BeanGeneral> observacionesFact(String documento) throws Exception {
        ArrayList<BeanGeneral> lista = null;
        try {
            lista = cdao.observacionesFact(documento);
        } catch (Exception e) {
            throw new Exception("Error en observacionesFact en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    /**
     * Lista la gestion hecha sobre las facturas de un negocio
     * @param documento Una factura del negocio a buscar
     * @return Listado con los datos encontrados
     * @throws Exception Cuando hay un error
     */
    public ArrayList<BeanGeneral> observacionesNeg(String documento) throws Exception {
        ArrayList<BeanGeneral> lista = null;
        try {
            lista = cdao.observacionesNeg(documento);
        } catch (Exception e) {
            throw new Exception("Error en observacionesNeg en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    public ArrayList<BeanGeneral> observacionesCliente(String documento, String codcli) throws Exception {
        ArrayList<BeanGeneral> lista = null;
        try {
            lista = cdao.observacionesClinte(documento, codcli);
        } catch (Exception e) {
            throw new Exception("Error en observacionescliente en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    /**
     * Datos para la pagina de gestion de cartera
     * @param documento Numero de la factura
     * @return Datos requeridos
     * @throws Exception Cuando hay error
     */
    public BeanGeneral datosHeader(String documento) throws Exception {
        BeanGeneral bean = null;
        try {
            bean = cdao.datosHeader(documento);
        } catch (Exception e) {
            throw new Exception("Error en datosHeader en GestionCarteraService.java: " + e.toString());
        }
        return bean;
    }

    /**
     * Nombre del tipo de gestion
     * @param codigo Codigo del tipo de gestion
     * @param table_type El tipo de tablagen. Es para saber si es listado de gestion actual o proxima gestion
     * @return String con el nombre del tipo de gestion
     * @throws Exception Cuando hay un error
     */
    public String nombreGestion(String codigo, String table_type) throws Exception {
        String nombre = "";
        try {
            nombre = cdao.nombreGestion(codigo, table_type);
        } catch (Exception e) {
            throw new Exception("Error en nombreGestion en GestionCarteraService.java: " + e.toString());
        }
        return nombre;
    }

    /**
     * Inserta observacion de una factura
     * @param documento Numero de la factura
     * @param observacion Texto de la observacion
     * @param user Usuario que crea la observacion
     * @param tipo_gestion Codigo del tipo de gestion
     * @param fecha_prox_gestion Fecha de la proxima gestion
     * @param prox_accion Codigo de la proxima accion
     * @param tipo tipo de dato a actualizar
     * @param dato dato actualizar
     * @throws Exception Cuando hay error
     */
    public void insObservacion(String documento, String observacion, String user, String tipo_gestion, String fecha_prox_gestion, String prox_accion, String tipo, String dato) throws Exception {
        try {
            cdao.insObservacion(documento, observacion, user, tipo_gestion, fecha_prox_gestion, prox_accion, tipo, dato);
        } catch (Exception e) {
            throw new Exception("Error en insObservacion en GestionCarteraService.java: " + e.toString());
        }
    }

    /**
     * Lista los diferentes tipos de gestion
     * @param table_type El tipo de tablagen. Es para saber si es listado de gestion actual o proxima gestion
     * @return Listado con los datos obtenidos
     * @throws Exception Cuando hay error
     */
    public ArrayList<String> listaGestion(String table_type, String estado) throws Exception {
        ArrayList<String> lista = null;
        try {
            lista = cdao.listaGestion(table_type, estado);
        } catch (Exception e) {
            throw new Exception("Error en listaGestion en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    /**
     * Datos de los ingresos de un negocio
     * @param documento Numero de la factura
     * @return Listado con los datos
     * @throws Exception Cuando hay un error
     */
    public ArrayList<Ingreso> ingresosNegocio2(String cod_neg) throws Exception {
        ArrayList<Ingreso> lista = null;
        try {
            lista = cdao.ingresosNegocio2(cod_neg);
        } catch (Exception e) {
            throw new Exception("Error en ingresosNegocio en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    /**
     * Busca el texto de una observacion de una factura
     * @param codigo El codigo de la observacion a ver
     * @return Cadena con el texto de la observacion
     * @throws Exception Cuando hay error
     */
    public String verObs(String codigo) throws Exception {
        String obs = "";
        try {
            obs = cdao.verObs(codigo);
        } catch (Exception e) {
            throw new Exception("Error en verObs en GestionCarteraService.java: " + e.toString());
        }
        return obs;
    }

    public ArrayList<String> ver_dato(String tipo, String codigo) throws Exception {
        ArrayList<String> lista = new ArrayList<String>();
        try {
            lista = cdao.ver_dato(tipo, codigo);
        } catch (Exception e) {
            throw new Exception("Error en verObs en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    public String actualiza_dato(String tipo, String codigo, String dato) throws Exception {
        String msg = "";
        try {
            msg = cdao.actualiza_dato(tipo, codigo, dato);
        } catch (Exception e) {
            throw new Exception("Error en verObs en GestionCarteraService.java: " + e.toString());
        }
        return msg;
    }

    public String inserta_dato(String tipo, String dato, String user) throws Exception {
        String msg = "";
        try {
            msg = cdao.inserta_dato(tipo, dato, user);
        } catch (Exception e) {
            throw new Exception("Error en verObs en GestionCarteraService.java: " + e.toString());
        }
        return msg;
    }

    /**
     * Datos de las facturas de un negocio
     * @param cod_neg Codigo del negocio
     * @filtro Estado de la factura
     * @param columna Columna por la que se quiere ordenar
     * @param orden Si es ascendente o descendente
     * @return Listado con los datos
     * @throws Exception Cuando hay error
     */
    public ArrayList<factura> facturasNegocioSort(String cod_neg, String filtro, String columna, String orden) throws Exception {
        ArrayList<factura> lista = null;
        try {
            lista = cdao.facturasNegocioSort(cod_neg, filtro, columna, orden);
        } catch (Exception e) {
            throw new Exception("Error en facturasNegocio en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    /**
     * Verifica si para el nit hay que mostrar las pf y ff
     * @param nit Dato a verificar
     * @return Si hay que mostrar o no
     * @throws Exception Cuando ocurre un error
     */
    public boolean clienteEspecial(String nit) throws Exception {
        boolean esp = false;
        try {
            esp = cdao.clienteEspecial(nit);
        } catch (Exception e) {
            throw new Exception("Error en clienteEspecial en GestionCarteraService.java: " + e.toString());
        }
        return esp;
    }

    /**
     * Busca todas las pf y ff no anuladas
     * @return Listado con datos
     * @throws Exception Cuando hay error
     */
    public ArrayList<factura> facturasPf() throws Exception {
        ArrayList<factura> lista = null;
        try {
            lista = cdao.facturasPf();
        } catch (Exception e) {
            throw new Exception("Error en facturasPf en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    /**
     * Busca la lista de pf segun un filtro
     * @param filtro El filtro
     * @param orden Orden
     * @param columna columna por la que se ordena
     * @return Listado con los coincidentes
     * @throws Exception Cuando hay error
     */
    public ArrayList<factura> facturasPf2(String filtro, String orden, String columna) throws Exception {
        ArrayList<factura> lista = null;
        try {
            lista = cdao.facturasPf2(filtro, orden, columna);
        } catch (Exception e) {
            throw new Exception("Error en facturasPf en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    /**
     * Busca todos los ingresos asociados a pf y ff
     * @return Listado con datos
     * @throws Exception Cuando hay error
     */
    public ArrayList<Ingreso> ingresosPf() throws Exception {
        ArrayList<Ingreso> lista = null;
        try {
            lista = cdao.ingresosPf();
        } catch (Exception e) {
            throw new Exception("Error en ingresosPf en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    /**
     * Busca todos los ingresos asociados a pf y ff. No eascoge anulados
     * @return Listado con datos
     * @throws Exception Cuando hay error
     */
    public ArrayList<Ingreso> ingresosPf2() throws Exception {
        ArrayList<Ingreso> lista = null;
        try {
            lista = cdao.ingresosPf2();
        } catch (Exception e) {
            throw new Exception("Error en ingresosPf en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    //20101230
    /**
     * Busca los tipos de titulo valor
     * @return Listado con los resultados encontrados
     * @throws Exception Cuando hay error
     */
    public ArrayList<String> titulosValor() throws Exception {
        ArrayList<String> lista = null;
        try {
            lista = cdao.titulosValor();
        } catch (Exception e) {
            throw new Exception("Error en titulosValor en GestionCondicionesService.java: " + e.toString());
        }
        return lista;
    }

    public BeanGeneral datosFacturasNegocio(String codneg) throws Exception {
        BeanGeneral bean = new BeanGeneral();
        try {
            bean = cdao.datosFacturasNegocio(codneg);
        } catch (Exception e) {
            throw new Exception("Error en datosFacturasNegocio en GestionCondicionesService.java: " + e.toString());
        }
        return bean;
    }

    public ArrayList<Ingreso> ingresosNegocioSort(String cod_neg, String columna, String orden) throws Exception {
        ArrayList<Ingreso> lista = null;
        try {
            lista = cdao.ingresosNegocioSort(cod_neg, columna, orden);
        } catch (Exception e) {
            throw new Exception("Error en ingresosNegocioSort en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    public ArrayList<BeanGeneral> estadoCuenta(String codneg, String columna, String orden) throws Exception {
        ArrayList<BeanGeneral> lista = null;
        try {
            lista = cdao.estadoCuenta(codneg, columna, orden);
        } catch (Exception e) {
            throw new Exception("Error en estadoCuenta en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    public String cuentasAjusteIngreso(String num_ingreso) throws Exception {
        String cuentas = "";
        try {
            cuentas = cdao.cuentasAjusteIngreso(num_ingreso);
        } catch (Exception e) {
            throw new Exception("Error en cuentasAjusteIngreso en GestionCarteraService.java: " + e.toString());
        }
        return cuentas;
    }

    public ArrayList<BeanGeneral> estadoCuentaPf(String columna, String orden) throws Exception {
        ArrayList<BeanGeneral> lista = null;
        try {
            lista = cdao.estadoCuentaPf(columna, orden);
        } catch (Exception e) {
            throw new Exception("Error en estadoCuentaPf en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    public double sumatoria_ic_ia(String cod_neg) throws Exception {
        double suma = 0;
        try {
            suma = cdao.sumatoria_ic_ia(cod_neg);
        } catch (Exception e) {
            throw new Exception("Error en sumatoria_ic_ia en GestionCarteraService.java: " + e.toString());
        }
        return suma;
    }

    public double sumatoria_ajustes(String cod_neg) throws Exception {
        double suma = 0;
        try {
            suma = cdao.sumatoria_ajustes(cod_neg);
        } catch (Exception e) {
            throw new Exception("Error en sumatoria_ajustes en GestionCarteraService.java: " + e.toString());
        }
        return suma;
    }

    public double sumatoria_nc(String cod_neg) throws Exception {
        double suma = 0;
        try {
            suma = cdao.sumatoria_nc(cod_neg);
        } catch (Exception e) {
            throw new Exception("Error en sumatoria_nc en GestionCarteraService.java: " + e.toString());
        }
        return suma;
    }

    public ArrayList<Ingreso> ingresosPf(String col, String orden) throws Exception {
        ArrayList<Ingreso> lista = null;
        try {
            lista = cdao.ingresosPf(col, orden);
        } catch (Exception e) {
            throw new Exception("Error en ingresosPf en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    public void actualizarEstadoCartera(String codcli, String estado) throws Exception {
        try {
            cdao.actualizarEstadoCartera(codcli, estado);
        } catch (Exception e) {
            throw new Exception("Error en actualizarEstadoCartera en GestionCarteraService.java: " + e.toString());
        }
    }

    public int conteoFacturasNegocio(String cod_neg) throws Exception {
        int num = 0;
        try {
            num = cdao.conteoFacturasNegocio(cod_neg);
        } catch (Exception e) {
            throw new Exception("Error en conteoFacturasNegocio en GestionCarteraService.java: " + e.toString());
        }
        return num;
    }

    /**
     * Envia correo electronico con el estado de cuenta del cliente
     * @param negocio
     * @param email
     * @return Listado de textos
     * @throws Exception Cuando hay error
     */
    public String enviarEstadoCuenta(String negocio, String email) throws Exception {
        String msj = "";
        String tox, copytox, hiddencopytox;
        String fromx, asuntox, msgx;
        String ruta, nombre_archivo;
        String correo_salida = "fintravalores@geotechsa.com";
        String pass_salida = "fintra21";
        ArrayList<factura> listafactscli = null;
        ArrayList<String> listahead = null;
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        ruta = rb.getString("ruta") + "/exportar/migracion/EstadoCuenta/";
        fromx = "servicioalcliente@fintravalores.com";
        asuntox = "Su Estado De Cuenta - Fintra S.A.";
        copytox = "";
        hiddencopytox = "";
        nombre_archivo = "estado_cuenta.pdf";
        msgx = "<p><b>Estimado cliente</b>,</p><p>&nbsp;</p><p>Reciba un cordial saludo.  </p><p>&nbsp;</p><p>Adjunto "
                + "a la presente, enviamos su estado de cuentas para que proceda con el "
                + "pago inmediato de las cuotas vencidas. Si el pago ya fue realizado le solicitamos hacer caso "
                + "omiso a este e-mail.  </p><p>&nbsp;</p><p>&nbsp;<b>Nota: </b>Este es un "
                + " correo autom&aacute;tico, no responder directamente. Para mayor informaci&oacute;n "
                + "puede comunicarse a los tel&eacute;fonos 57+(5) 3679900 - 3679902 - 3679906 &oacute; "
                + "escribirnos a la direcci&oacute;n de correo electr&oacute;nico <i><a href='mailto:director.cartera@fintra.co'"
                + "target='_blank' >director.cartera@fintra.co</a> "
                + "</i></p><p> &nbsp; </p><p>Atentamente, </p><p></p><p>&nbsp;</p><p><b><i><span>"
                + "Departamento De Cartera  </span></i></b></p>";
        try {

            listafactscli = this.facturasNegocioClie(negocio);
            listahead = this.cabeceraTabla(negocio);

            if (this.generarPdf4("EstadoCuenta", listafactscli, listahead)) {

                tox =/*dat[1]*+";"+*/ "ivargas@geotech.com.co;";//correo modificar para subir en productivo
                if (this.envia_correo(fromx, tox, copytox, correo_salida, pass_salida, nombre_archivo, ruta, hiddencopytox, asuntox, msgx)) {
                    msj = "Estado de cuenta enviado correctamente";
                } else {
                    msj = "El correo no fue enviado, intente más tarde";
                }
            } else {
                msj = "A ocurrido un problema en la generacion del pdf";
            }
        } catch (Exception e) {
            msj = "Error: " + e.toString();
            throw new Exception("Error en enviarEstadoCuenta en GestionCarteraService.java: " + e.toString());
        }
        return msj;
    }

    /**
     * Trae los textos de la carta referenciada
     * @param referencia codigo de la carta
     * @return Listado de textos
     * @throws Exception Cuando hay error
     */
    public ArrayList<String> textoCarta(String referencia) throws Exception {
        ArrayList<String> lista = null;
        try {
            lista = cdao.textoCarta(referencia);
        } catch (Exception e) {
            throw new Exception("Error en textoCarta en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    public ArrayList<BeanGeneral> gestionesPendientes(String login) throws Exception {
        ArrayList<BeanGeneral> lista = null;
        try {
            lista = cdao.gestionesPendientes(login);
        } catch (Exception e) {
            throw new Exception("Error en gestionesPendientes en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    /**
     * Lista los fomularios mas recientes del cliente a partir de un negocio
     * @param negocio del cliente
     * @return Listado con los datos encontrados
     * @throws Exception Cuando hay un error
     */
    public ArrayList<BeanGeneral> buscarForm(String negocio) throws Exception {
        ArrayList<BeanGeneral> lista = null;
        try {
            lista = cdao.buscarForm(negocio);
        } catch (Exception e) {
            throw new Exception("Error en buscarForm en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    public boolean envia_correo(String fromx, String tox, String copytox, String correo_salida, String pass_salida, String nombre_archivo, String ruta, String hiddencopytox, String asuntox, String msgx) {
        boolean enviado = false;
        try {
            /************************* Envio de Correo*************************/
            Email2 emailData = null;
            String ahora = new java.util.Date().toString();
            emailData = new Email2();
            emailData.setEmailId(12345);
            emailData.setRecstatus("A");//A
            emailData.setEmailcode("emailcode");
            emailData.setEmailfrom(fromx);
            emailData.setEmailto(tox);
            emailData.setEmailcopyto(copytox);
            emailData.setEmailHiddencopyto(hiddencopytox);
            emailData.setEmailsubject(asuntox);//"WebServiceMultiple_Fintra2" );
            emailData.setEmailbody(msgx);
            emailData.setLastupdat(new Timestamp(System.currentTimeMillis()));
            emailData.setSenderName("sender name");
            emailData.setRemarks("remark2");
            emailData.setTipo("tipito");
            emailData.setRutaArchivo(ruta);
            emailData.setNombreArchivo(nombre_archivo);


            EmailSendingEngineService emailSendingEngineService = new EmailSendingEngineService("smtpbar.une.net.co", correo_salida, pass_salida);
            //.out.println("casi send");
            enviado = emailSendingEngineService.send(emailData);
            emailSendingEngineService = null;//091206
            /*************************Fin Envio de Correo*************************/
        } catch (Exception e) {

            System.out.println("Error  " + e.getMessage());
        }

        return enviado;
    }

    /**
     * Datos de las facturas de un negocio
     * @param cod_neg Codigo del negocio
     * @return Listado con los datos
     * @throws Exception Cuando hay error
     */
    public ArrayList<factura> facturasNegocioClie(String cod_neg) throws Exception {
        ArrayList<factura> lista = null;
        try {
            lista = cdao.facturasNegocioClie(cod_neg);
        } catch (Exception e) {
            throw new Exception("Error en facturasNegocio en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    /**
     * Crea un objeto de tipo Document
     * @return Objeto creado
     */
    public Document createDoc() {
        Document doc = new Document(PageSize.LETTER, 30, 30, 35, 30);
        return doc;
    }

    /**
     * Trae los negocios que tengan una factura que vence en n dias
     * @param vence numero dias en que vence la factura
     * @return Listado de negocios
     * @throws Exception Cuando hay error
     */
    public ArrayList<String> facturasPorVencer(int vence, int dias) throws Exception {
        ArrayList<String> lista = null;
        try {
            lista = cdao.facturasPorVencer(vence, dias);
        } catch (Exception e) {
            throw new Exception("Error en facturasNegocio en GestionCarteraService.java: " + e.toString());
        }
        return lista;
    }

    /**
     * Genera el pdf del estado de cuenta
     * @param user Usuario que lo genera
     * @param estado_cuenta Lista de documentos
     * @param listahead Datos para el encabezado
     * @throws Exception Cuando hay un error
     */
    public boolean generarCarta(String cod, String user, ArrayList<String> textos, String neg, double saldo, double mora, double cobro, int dias) throws Exception {
        String directorio = "";
        System.out.println("inicia elaboracion pdf");
        try {

            directorio = this.directorioArchivo(user, "carta_" + neg + "_" + cod, "pdf");

            System.out.println("Elaborando directorio");
            Document documento = null;
            ArrayList<String> listahead = cdao.cabeceraTabla(neg);
            documento = this.createDoc();
            PdfWriter.getInstance(documento, new FileOutputStream(directorio));
            documento.setHeader(this.getMyHeader());
            //documento.setFooter(this.getMyFooter());

            documento.open();
            documento.newPage();
            //encabezado
            documento.add(this.getEncabezadoCarta(listahead, cod));
            //cuerpo del documento
            StyleSheet styles = new StyleSheet();
            styles.loadTagStyle("body", "face", "times new roman");
            styles.loadTagStyle("body", "size", "10px");
            styles.loadTagStyle("span", "face", "times new roman");
            styles.loadTagStyle("span", "size", "10px");
            styles.loadTagStyle("span", "color", "red");
            //inicia la generacion
            for (int i = 0; i < textos.size(); i++) {
                String despues = "";
                String texto[] = textos.get(i).split(";_;");

                if (i == 0) {

                    if (cod.equals("03") || cod.equals("04")) {
                        despues = " " + listahead.get(7) + "</span> - " + listahead.get(9) + "</b></p><p></br></p>";
                    } else {
                        despues = " " + listahead.get(7) + " - " + listahead.get(9) + "</b></p><p></br></p>";
                    }
                }
                if (i == 2 && !cod.equals("01")) {
                    documento.add(this.getCuentas(saldo, cobro, mora, dias, cod));
                }
                //Creas un párrafo:
                Paragraph parrafo = new Paragraph();
                //Estableces el tipo de alineación:
                parrafo.setAlignment(Element.ALIGN_JUSTIFIED);

                //Así se alineará todo lo que tengas en el párrfo. Si el texto, por ejemplo se toma de un HTML, que puede estar formateado y con varios párrafos, todos los párrafos que tenga el HTML se justifican:
                ArrayList htmlObjs = HTMLWorker.parseToList(new StringReader(texto[1] + despues), styles);
                for (int k = 0; k < htmlObjs.size(); ++k) {
                    Element elementHtml = (Element) htmlObjs.get(k);
                    //Vamos añadiendo el elemento HTML al párrafo anterior:
                    parrafo.add(elementHtml);
                }                //Añadimos el párrafo al PDF:
                documento.add(parrafo);

            }

            documento.close();
        } catch (Exception e) {


            return false;
        }

        System.out.println("fin generacion pdf");
        return true;
    }

    /**
     * Genera el pdf del estado de cuenta
     * @param user Usuario que lo genera
     * @param estado_cuenta Lista de documentos
     * @param listahead Datos para el encabezado
     * @throws Exception Cuando hay un error
     */
    public boolean generarPdf4(String user, ArrayList<factura> estado_cuenta, ArrayList<String> listahead) throws Exception {
        boolean sw = false;
        String directorio = "";
        System.out.println("inicia elaboracion pdf");
        try {
            if (user.equals("EstadoCuenta")) {
                directorio = this.directorioArchivo(user, "estado_cuenta", "pdf");
            } else {
                directorio = this.directorioArchivo(user, "estado_cuenta_" + listahead.get(7), "pdf");
            }
            System.out.println("Elaborando directorio");
            Document documento = null;
            Font fuente = new Font(Font.HELVETICA, 8);
            Font fuenteG = new Font(Font.HELVETICA, 8, Font.BOLD);
            PdfPCell celda;

            documento = this.createDoc();
            PdfWriter.getInstance(documento, new FileOutputStream(directorio));
            documento.setHeader(this.getMyHeader());
            documento.setFooter(this.getMyFooter());
            documento.open();
            documento.newPage();
            //encabezado
            documento.add(this.getEncabezado(listahead));
            //cuerpo del documento
            PdfPTable tbody = this.getTablaTitle();
            //inicia la generacion
            factura factcli = null;
            double sum1 = 0;
            double sum2 = 0;
            double sum3 = 0;
            for (int i = 0; i < estado_cuenta.size(); i++) {
                factcli = estado_cuenta.get(i);
                sum1 += factcli.getValor_factura();
                sum2 += factcli.getValor_saldo();
                sum3 += factcli.getValor_facturame();

                if (i == 30) {
                    documento.add(tbody);
                    documento.newPage();
                    documento.add(this.getEncabezado(listahead));
                    tbody = this.getTablaTitle();
                }

                celda = new PdfPCell();
                celda.setPhrase(new Phrase((i + 1) + "", fuente));
                celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tbody.addCell(celda);
                celda = new PdfPCell();
                celda.setPhrase(new Phrase(factcli.getFecha_vencimiento(), fuente));
                celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tbody.addCell(celda);
                celda = new PdfPCell();
                celda.setPhrase(new Phrase(factcli.getTipo_documento(), fuente));
                celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tbody.addCell(celda);
                celda = new PdfPCell();
                celda.setPhrase(new Phrase(factcli.getEstado(), fuente));
                celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tbody.addCell(celda);
                celda = new PdfPCell();
                celda.setPhrase(new Phrase((factcli.getValor_facturame() > 0 ? factcli.getDias_mora() : 0) + "", fuente));
                celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tbody.addCell(celda);
                celda = new PdfPCell();
                celda.setPhrase(new Phrase(Util.customFormat(factcli.getValor_factura()), fuente));
                celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
                tbody.addCell(celda);
                celda = new PdfPCell();
                celda.setPhrase(new Phrase(Util.customFormat(factcli.getValor_saldo()), fuente));
                celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
                tbody.addCell(celda);
                celda = new PdfPCell();
                celda.setPhrase(new Phrase(Util.customFormat(factcli.getValor_facturame()), fuente));
                celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
                tbody.addCell(celda);
                celda = new PdfPCell();
                celda.setPhrase(new Phrase(factcli.getFecha_ultimo_pago(), fuente));
                celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tbody.addCell(celda);
                celda = null;
            }
            //se totaliza
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("TOTALES", fuenteG));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            celda.setColspan(5);
            tbody.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(Util.customFormat(sum1), fuente));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            tbody.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(Util.customFormat(sum2), fuente));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            tbody.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(Util.customFormat(sum3), fuente));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            tbody.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("", fuente));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            tbody.addCell(celda);
            System.out.println("estado listo");
            documento.add(tbody);
            documento.close();
        } catch (Exception e) {


            return false;
        }

        System.out.println("fin generacion pdf");
        return true;
    }

    public HeaderFooter getMyFooter() throws ServletException {
        try {
            Phrase p = new Phrase();
            Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
            p.clear();


            p.add(new Paragraph("Carrera 53 # 79 - 01 Local 205 Barranquilla, Colombia\n", fuenteB));
            p.add(new Paragraph("PBX: 57 5 3679900 FAX: 57 5 3679906\n", fuenteB));
            p.add(new Paragraph("www.fintra.co\n", fuenteB));
            HeaderFooter header = new HeaderFooter(p, false);
            header.setBorder(Rectangle.NO_BORDER);
            header.setAlignment(Paragraph.ALIGN_CENTER);

            return header;
        } catch (Exception ex) {
            throw new ServletException("Header Error");
        }
    }

    public HeaderFooter getMyHeader() throws ServletException {
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String url_logo = "fintra.gif";
            Image img = Image.getInstance(rb.getString("ruta") + "/images/" + url_logo);
            Phrase p = new Phrase(new Chunk(
                    img,
                    0, -20));

            HeaderFooter header = new HeaderFooter(p, false);
            header.setBorder(Rectangle.NO_BORDER);
            header.setAlignment(Paragraph.ALIGN_CENTER);

            return header;
        } catch (Exception ex) {
            throw new ServletException("Header Error");
        }
    }

    protected PdfPTable getEncabezadoCarta(ArrayList<String> listahead, String cod) throws DocumentException, BadElementException, IOException {
        PdfPTable tcontainer = new PdfPTable(2);
        String aviso = "";
        try {

            if (cod.equals("03")) {
                aviso = "PRE-AVISO";
            }
            if (cod.equals("04")) {
                aviso = "\u00DALTIMO-AVISO";
            }
            Font fuente = new Font(Font.TIMES_ROMAN, 11);
            Font fuenteA = new Font(Font.TIMES_ROMAN, 15, Font.UNDERLINE, new java.awt.Color(255, 0, 0));
            //encabezado del documento

            PdfPCell celda;
            PdfPCell celda_vacia = new PdfPCell();
            String fecha = Util.ObtenerFechaCompleta(Calendar.getInstance());


            tcontainer.setWidthPercentage(100);

            celda = new PdfPCell();
            celda.setBorderWidthTop(0);
            celda.setBorderWidthBottom(0);
            celda.setBorderWidthLeft(0);
            celda.setBorderWidthRight(0);
            celda.setColspan(2);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            celda.setPhrase(new Phrase(cdao.UpCP("COBRO_EJECUTIVO"), fuente));
            tcontainer.addCell(celda);
            celda = new PdfPCell();
            celda.setBorderWidthTop(0);
            celda.setBorderWidthBottom(0);
            celda.setBorderWidthLeft(0);
            celda.setBorderWidthRight(0);
            celda.setColspan(2);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            celda.setPhrase(new Phrase("Barranquilla, " + fecha, fuente));
            tcontainer.addCell(celda);
            celda_vacia = new PdfPCell();
            celda_vacia.setBorderWidthTop(0);
            celda_vacia.setBorderWidthBottom(0);
            celda_vacia.setBorderWidthLeft(0);
            celda_vacia.setBorderWidthRight(0);
            celda_vacia.setColspan(2);
            celda_vacia.setPhrase(new Phrase(" ", fuente));
            tcontainer.addCell(celda_vacia);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Se\u00F1or(a)", fuente));
            celda.setBorderWidthTop(0);
            celda.setBorderWidthBottom(0);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            celda.setBorderWidthLeft(0);
            celda.setBorderWidthRight(0);
            tcontainer.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(aviso, fuenteA));
            celda.setBorderWidthTop(0);
            celda.setBorderWidthBottom(0);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            celda.setBorderWidthLeft(0);
            celda.setBorderWidthRight(0);
            tcontainer.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(listahead.get(0), fuente));
            celda.setBorderWidthTop(0);
            celda.setBorderWidthBottom(0);
            celda.setColspan(2);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            celda.setBorderWidthLeft(0);
            celda.setBorderWidthRight(0);
            tcontainer.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(listahead.get(5), fuente));
            celda.setBorderWidthTop(0);
            celda.setBorderWidthBottom(0);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            celda.setBorderWidthLeft(0);
            celda.setBorderWidthRight(0);
            celda.setColspan(2);
            tcontainer.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(listahead.get(11), fuente));
            celda.setBorderWidthTop(0);
            celda.setBorderWidthBottom(0);
            celda.setColspan(2);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            celda.setBorderWidthLeft(0);
            celda.setBorderWidthRight(0);
            tcontainer.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(listahead.get(6), fuente));
            celda.setBorderWidthTop(0);
            celda.setBorderWidthBottom(0);
            celda.setColspan(2);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            celda.setBorderWidthLeft(0);
            celda.setBorderWidthRight(0);
            tcontainer.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(" ", fuente));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_JUSTIFIED);
            celda.setBorderWidthTop(0);
            celda.setBorderWidthBottom(0);
            celda.setColspan(2);
            celda.setBorderWidthLeft(0);
            celda.setBorderWidthRight(0);
            tcontainer.addCell(celda);
        } catch (Exception ex) {
            Logger.getLogger(GestionCarteraService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tcontainer;
    }

    protected PdfPTable getCuentas(double saldo, double cobro, double mora, int dias, String cod) throws DocumentException, BadElementException, IOException {
        PdfPTable tcontainer = new PdfPTable(2);
        try {
            Font fuente = new Font(Font.TIMES_ROMAN, 11);
            //encabezado del documento

            PdfPCell celda;
            tcontainer.setHorizontalAlignment(tcontainer.ALIGN_LEFT);
            tcontainer.setWidthPercentage(40);
            float[] medidaCeldas = {0.600f, 0.400f};
            tcontainer.setWidths(medidaCeldas);
            if (cod.equals("03")) {
                celda = new PdfPCell();
                celda.setBorderWidthTop(0);
                celda.setBorderWidthBottom(0);
                celda.setBorderWidthLeft(0);
                celda.setBorderWidthRight(0);
                celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                celda.setPhrase(new Phrase("Altura de mora:", fuente));
                tcontainer.addCell(celda);
                celda = new PdfPCell();
                celda.setPhrase(new Phrase(dias + " d\u00EDas", fuente));
                celda.setBorderWidthTop(0);
                celda.setBorderWidthBottom(0);
                celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
                celda.setBorderWidthLeft(0);
                celda.setBorderWidthRight(0);
                tcontainer.addCell(celda);
            }

            celda = new PdfPCell();
            celda.setBorderWidthTop(0);
            celda.setBorderWidthBottom(0);
            celda.setBorderWidthLeft(0);
            celda.setBorderWidthRight(0);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            celda.setPhrase(new Phrase("Saldo vencido:", fuente));
            tcontainer.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("$" + Util.customFormat(saldo), fuente));
            celda.setBorderWidthTop(0);
            celda.setBorderWidthBottom(0);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            celda.setBorderWidthLeft(0);
            celda.setBorderWidthRight(0);
            tcontainer.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Interes por mora:", fuente));
            celda.setBorderWidthTop(0);
            celda.setBorderWidthBottom(0);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            celda.setBorderWidthLeft(0);
            celda.setBorderWidthRight(0);
            tcontainer.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("$" + Util.customFormat(mora), fuente));
            celda.setBorderWidthTop(0);
            celda.setBorderWidthBottom(0);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            celda.setBorderWidthLeft(0);
            celda.setBorderWidthRight(0);
            tcontainer.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Gastos Cobranza:", fuente));
            celda.setBorderWidthTop(0);
            celda.setBorderWidthBottom(0);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            celda.setBorderWidthLeft(0);
            celda.setBorderWidthRight(0);
            tcontainer.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("$" + Util.customFormat(cobro), fuente));
            celda.setBorderWidthTop(0);
            celda.setBorderWidthBottom(0);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            celda.setBorderWidthLeft(0);
            celda.setBorderWidthRight(0);
            tcontainer.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Total a pagar:", fuente));
            celda.setBorderWidthTop(0);
            celda.setBorderWidthBottom(0);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            celda.setBorderWidthLeft(0);
            celda.setBorderWidthRight(0);
            tcontainer.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("$" + Util.customFormat((cobro + saldo + mora)), fuente));
            celda.setBorderWidthTop(0);
            celda.setBorderWidthBottom(0);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            celda.setBorderWidthLeft(0);
            celda.setBorderWidthRight(0);
            tcontainer.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("", fuente));
            celda.setBorderWidthTop(0);
            celda.setBorderWidthBottom(0);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            celda.setBorderWidthLeft(0);
            celda.setBorderWidthRight(0);
            celda.setColspan(2);
            tcontainer.addCell(celda);
        } catch (Exception ex) {
            Logger.getLogger(GestionCarteraService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tcontainer;
    }

    public String espacio(int numero) {
        String espacio = "";
        for (int i = 0; i < numero * 2; i++) {
            espacio += " ";
        }
        return espacio;
    }

    protected PdfPTable getEncabezado(ArrayList<String> listahead) throws DocumentException, BadElementException, IOException {
        PdfPTable tcontainer = new PdfPTable(2);
        try {
            Font fuente = new Font(Font.HELVETICA, 8);
            Font fuenteF = new Font(Font.HELVETICA, 12, Font.BOLD, new java.awt.Color(3, 167, 12));
            Font fuenteG = new Font(Font.HELVETICA, 8, Font.BOLD);
            Font fuenteH = new Font(Font.HELVETICA, 12, Font.BOLD, new java.awt.Color(255, 255, 255));
            Font fuenteI = new Font(Font.HELVETICA, 8, Font.BOLD, new java.awt.Color(255, 255, 255));
            //encabezado del documento
            PdfPCell celda;
            PdfPCell celda_vacia = new PdfPCell();

            SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");

            float[] medidaCeldas = {0.600f, 0.400f};
            tcontainer.setWidths(medidaCeldas);
            tcontainer.setWidthPercentage(100);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("", fuente));
            celda.setBorderWidthTop(0);
            celda.setBorderWidthBottom(0);
            celda.setBorderWidthLeft(0);
            celda.setBorderWidthRight(0);
            tcontainer.addCell(celda);
            celda = new PdfPCell();
            celda.setBorderWidthTop(0);
            celda.setBorderWidthBottom(0);
            celda.setBorderWidthLeft(0);
            celda.setBorderWidthRight(0);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            celda.setPhrase(new Phrase("Fecha de Consulta: " + fmt.format(new Date()), fuenteF));
            tcontainer.addCell(celda);
            celda_vacia = new PdfPCell();
            celda_vacia.setBorderWidthTop(0);
            celda_vacia.setBorderWidthBottom(0);
            celda_vacia.setBorderWidthLeft(0);
            celda_vacia.setBorderWidthRight(0);
            celda_vacia.setColspan(2);
            celda_vacia.setPhrase(new Phrase(" ", fuente));
            celda_vacia.setBackgroundColor(new java.awt.Color(255, 255, 255));
            tcontainer.addCell(celda_vacia);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("ESTADO DE CUENTA", fuenteH));
            celda.setBackgroundColor(new java.awt.Color(3, 167, 12));
            celda.setBorderWidthTop(0);
            celda.setBorderWidthBottom(0);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            celda.setBorderWidthLeft(0);
            celda.setBorderWidthRight(0);
            celda.setColspan(2);
            tcontainer.addCell(celda);
            celda_vacia = new PdfPCell();
            celda_vacia.setBorderWidthTop(0);
            celda_vacia.setBorderWidthBottom(0);
            celda_vacia.setBorderWidthLeft(0);
            celda_vacia.setBorderWidthRight(0);
            celda_vacia.setColspan(2);
            celda_vacia.setPhrase(new Phrase(" ", fuente));
            celda_vacia.setBackgroundColor(new java.awt.Color(255, 255, 255));
            tcontainer.addCell(celda_vacia);
            PdfPTable thead = new PdfPTable(4);
            float[] medidaCeldas2 = {0.150f, 0.350f, 0.150f, 0.350f};
            thead.setWidths(medidaCeldas2);
            thead.setWidthPercentage(100);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Datos del Cliente", fuenteI));
            celda.setBackgroundColor(new java.awt.Color(3, 167, 12));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            celda.setColspan(4);
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Nombre", fuenteG));
            celda.setBackgroundColor(new java.awt.Color(213, 255, 213));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setColspan(2);
            celda.setPhrase(new Phrase(listahead.get(0), fuente));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(listahead.get(1), fuente));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Telefono", fuenteG));
            celda.setBackgroundColor(new java.awt.Color(213, 255, 213));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(listahead.get(2), fuente));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Celular", fuenteG));
            celda.setBackgroundColor(new java.awt.Color(213, 255, 213));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(listahead.get(3), fuente));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Email", fuenteG));
            celda.setBackgroundColor(new java.awt.Color(213, 255, 213));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(listahead.get(4), fuente));
            celda.setColspan(3);
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Dirección", fuenteG));
            celda.setBackgroundColor(new java.awt.Color(213, 255, 213));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(listahead.get(5), fuente));
            celda.setColspan(2);
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(listahead.get(6), fuente));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Negocio", fuenteG));
            celda.setBackgroundColor(new java.awt.Color(213, 255, 213));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(listahead.get(7), fuente));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Tipo", fuenteG));
            celda.setBackgroundColor(new java.awt.Color(213, 255, 213));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(listahead.get(8), fuente));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Afiliado", fuenteG));
            celda.setBackgroundColor(new java.awt.Color(213, 255, 213));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setColspan(3);
            celda.setPhrase(new Phrase(listahead.get(9), fuente));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Sector", fuenteG));
            celda.setBackgroundColor(new java.awt.Color(213, 255, 213));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setColspan(3);
            celda.setPhrase(new Phrase(listahead.get(10), fuente));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.addElement(thead);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            celda.setBorder(0);
            tcontainer.addCell(celda);
            //
            thead = new PdfPTable(2);
            //thead.setWidthPercentage(100);
            GestionCarteraService cserv = new GestionCarteraService(this.cdao.getDatabaseName());
            String[] sald = cserv.saldoNegocio(listahead.get(7)).split(";");
            String tot_vigente = "0";
            String tot_vencido = "0";
            String gastos_cobranza = "0";
            String tot_intereses = "0";
            String saldo_total = "0";
            if (sald.length > 0) {
                double vigente = Double.parseDouble(sald[0]);
                tot_vigente = Util.customFormat(vigente);
                double vencido = Double.parseDouble(sald[1]);
                tot_vencido = Util.customFormat(vencido);
                double intereses = Double.parseDouble(sald[2]);
                tot_intereses = Util.customFormat(intereses);
                double gastos = vencido * 0.2;
                gastos_cobranza = Util.customFormat(gastos);
                saldo_total = Util.customFormat(vigente + vencido + intereses + gastos);
            }
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Resumen", fuenteI));
            celda.setBackgroundColor(new java.awt.Color(3, 167, 12));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            celda.setColspan(2);
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Saldo Total a Pagar", fuenteG));
            celda.setBackgroundColor(new java.awt.Color(213, 255, 213));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(saldo_total, fuente));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Saldo Corriente", fuenteG));
            celda.setBackgroundColor(new java.awt.Color(213, 255, 213));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(tot_vigente, fuente));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Saldo Vencido", fuenteG));
            celda.setBackgroundColor(new java.awt.Color(213, 255, 213));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(tot_vencido, fuente));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Intereses x Mora", fuenteG));
            celda.setBackgroundColor(new java.awt.Color(213, 255, 213));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(tot_intereses, fuente));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Gastos de Cobranza", fuenteG));
            celda.setBackgroundColor(new java.awt.Color(213, 255, 213));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(gastos_cobranza, fuente));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.addElement(thead);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            celda.setBorder(0);
            tcontainer.addCell(celda);
            celda_vacia = new PdfPCell();
            celda_vacia.setBorderWidthTop(0);
            celda_vacia.setBorderWidthBottom(0);
            celda_vacia.setBorderWidthLeft(0);
            celda_vacia.setBorderWidthRight(0);
            celda_vacia.setColspan(2);
            celda_vacia.setPhrase(new Phrase(" ", fuente));
            celda_vacia.setBackgroundColor(new java.awt.Color(255, 255, 255));
            tcontainer.addCell(celda_vacia);

        } catch (Exception ex) {
            Logger.getLogger(GestionCarteraService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tcontainer;
    }

    protected PdfPTable getTablaTitle() throws DocumentException {
        PdfPTable tbody = new PdfPTable(9);
        float[] medidaCeldas2 = {0.080f, 0.100f, 0.100f, 0.100f, 0.080f, 0.140f, 0.150f, 0.150f, 0.100f};
        tbody.setWidths(medidaCeldas2);
        Font fuenteI = new Font(Font.HELVETICA, 8, Font.BOLD, new java.awt.Color(255, 255, 255));
        PdfPCell celda;
        tbody.setWidthPercentage(100);
        celda = new PdfPCell();
        celda.setPhrase(new Phrase("Cuota", fuenteI));
        celda.setBackgroundColor(new java.awt.Color(3, 167, 12));
        celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tbody.addCell(celda);
        celda.setPhrase(new Phrase("Fecha Vencimiento", fuenteI));
        tbody.addCell(celda);
        celda.setPhrase(new Phrase("Tipo", fuenteI));
        tbody.addCell(celda);
        celda.setPhrase(new Phrase("Estado", fuenteI));
        tbody.addCell(celda);
        celda.setPhrase(new Phrase("Dias", fuenteI));
        tbody.addCell(celda);
        celda.setPhrase(new Phrase("Vr. Factura", fuenteI));
        tbody.addCell(celda);
        celda.setPhrase(new Phrase("Vr. Saldo Factura", fuenteI));
        tbody.addCell(celda);
        celda.setPhrase(new Phrase("Vr a Pagar", fuenteI));
        tbody.addCell(celda);
        celda.setPhrase(new Phrase("Fecha Ultimo Pago", fuenteI));
        tbody.addCell(celda);
        return tbody;
    }

    /**
     * Genera la ruta en que se guardara el archivo
     * @param user usuario que genera el archivo
     * @param cons consecutivo de la cotizacion
     * @param extension La extension del archivo
     * @return String con la ruta en la que queda el archivo
     * @throws Exception cuando ocurre algun error
     */
    public String directorioArchivo(String user, String cons, String extension) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
            if (!user.equals("EstadoCuenta")) {
                ruta = ruta + "/" + cons + "_" + fmt.format(new Date()) + "." + extension;
            } else {
                ruta = ruta + "/" + cons + "." + extension;
            }
        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }
    
    
    public Cliente estadoClienteCartera(String nit) throws Exception {
        Cliente bean = null;
        try {
           bean = cdao.estadoClienteCartera(nit);
        } catch (Exception e) {
            throw new Exception("Error en datosHeader en GestionCarteraService.java: " + e.toString());
        }
        return bean;
    }
    
    
     public String[] getFacturasAGestionar(String negocio) throws Exception {
        String resultado[];
        try {
           resultado = cdao.getFacturasAGestionar(negocio);
        } catch (Exception e) {
            throw new Exception("Error en datosHeader en GestionCarteraService.java: " + e.toString());
        }
        return resultado;
    }
     
    public ArrayList<BeanGeneral> getGestionesPorNegocio(String negocio) throws Exception {
        ArrayList<BeanGeneral> beans = null;
        try {
           beans = cdao.getGestionesPorNegocio(negocio);
        } catch (Exception e) {
            throw new Exception("Error en datosHeader en GestionCarteraService.java: " + e.toString());
        }
        return beans;
    } 
    
    public void insCompromisoPago(String documento, String observacion, double valorPagar, String fechaPagar, String ciudad, String barrio, String direccion, String usuario)  throws Exception {
        try {
            cdao.insCompromisoPago(documento, observacion, valorPagar, fechaPagar, ciudad, barrio, direccion, usuario);
        } catch (Exception e) {
            throw new Exception("Error en insCompromisoPago en GestionCarteraService.java: " + e.toString());
        }
    }
    
     public ArrayList<BeanGeneral> getCompromisosPorNegocio(String negocio) throws Exception {
        ArrayList<BeanGeneral> beans = null;
        try {
           beans = cdao.getCompromisosPorNegocio(negocio);
        } catch (Exception e) {
            throw new Exception("Error en datosHeader en GestionCarteraService.java: " + e.toString());
        }
        return beans;
    } 
    
    
}