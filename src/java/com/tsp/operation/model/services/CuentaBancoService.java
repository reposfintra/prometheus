 /***************************************
    * Nombre Clase ............. LiquidacionService.java
    * Descripci�n  .. . . . . .  Ofrece los servicios para las cuentas de los propietarios
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  09/04/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/




package com.tsp.operation.model.services;



import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.DAOS.TransferenciaDAO;
import com.tsp.operation.model.beans.Transferencia;




public class CuentaBancoService {
    
    
    
    private  TransferenciaDAO  TransferenciaDataAccess;
    private  List              listado;
    private  Hashtable         cuentaPropietio;
    
    private  List              tipoCuenta;
    private  List              listaBancos;
    private  String            TABLA_BANCOS = "LBANCOS";
    
    private  int               TOTAL_LIMIT_CTA      = 3;
    private  int               LIMIT_CTA            = 6;
    
    public   String             CTA_AHORR0     = "CA";
    public   String             CTA_CORRIENTE  = "CC";
    public   String             CTA_EFECTIVO   = "EF";
    
    
    
    
    
    
    public CuentaBancoService() {
        TransferenciaDataAccess = new  TransferenciaDAO();
        tipoCuenta              = new  LinkedList();
        listaBancos             = new  LinkedList();
        reset();
    }
    public CuentaBancoService(String dataBaseName) {
        TransferenciaDataAccess = new  TransferenciaDAO(dataBaseName);
        tipoCuenta              = new  LinkedList();
        listaBancos             = new  LinkedList();
        reset();
    }
    
    
    
    public void reset(){ 
        cuentaPropietio         = new  Hashtable();
        listado                 = new  LinkedList();
    }
    
    
    
    
    public void loadTipoCuentas()throws Exception{
        try{
           this.tipoCuenta = this.TransferenciaDataAccess.getTipoCuentas(); 
        }catch(Exception e){
             throw new Exception(e.getMessage());
        }
    }
    
    
    
    public void loadBancos()throws Exception{
        try{
           this.listaBancos = this.TransferenciaDataAccess.getTablaGen(  TABLA_BANCOS  ); 
        }catch(Exception e){
             throw new Exception(e.getMessage());
        }
    }
    
    public String insert()throws Exception{
        String msj = "";
        try{
            
             String tpCta    =  (String) this.cuentaPropietio.get("tipo");
             String nit      =  (String) this.cuentaPropietio.get("nit");
             
             int total       =  getListaCuentas( nit,"ALL").size();
             if( total <  LIMIT_CTA ){
                 
                 int cantidad    = ( tpCta.equals( this.CTA_EFECTIVO)   )?  getListaCuentas( nit, CTA_EFECTIVO  ).size()   :   getListaCuentas( nit, this.CTA_CORRIENTE  ).size()  +  getListaCuentas( nit, this.CTA_AHORR0  ).size()  ; 
             
                 if( cantidad < TOTAL_LIMIT_CTA){
                     
                     int  ultimoSec =  TransferenciaDataAccess.getUltimaSecuencia(nit);
                     List listaGen  =  getListaCuentas( nit,"ALL");                 
                     msj =  TransferenciaDataAccess.saveCuenta( this.cuentaPropietio, ultimoSec + 1 ) ;                 
                 }
                 else
                     msj = "No se puede grabar la cuenta, el propietario ya tiene la cantidad de cuenta establacidad para el tipo de cuenta dado" ;
                 
                 
             }else
                 msj = "No se puede crear mas cuentas para dicho propietario, ya tiene la cantidad de cuentas permitidas creadas, si desea crear una nueva deber� eliminar una de las existentes...."; 
                
                
        }catch(Exception e){
           throw new Exception(e.getMessage());
        }        
        return msj;
    }
    
    
    
    
    
    public List getListaCuentas( String nit, String tipo )throws Exception{
        List listaCta = new LinkedList();
        try{
            listaCta =  TransferenciaDataAccess.getListCuentas(nit, tipo );
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return listaCta;
    }
    
    
    
    
     public String update()throws Exception{
        String msj = "";
        try{
             msj =  TransferenciaDataAccess.updateCuenta(this.cuentaPropietio) ;
        }catch(Exception e){
           throw new Exception(e.getMessage());
        }        
        return msj;
    }
    
     
     
     
     public void asignarCTA(Transferencia  cta)throws Exception{
        try{            
            this.TransferenciaDataAccess.asignarCTA(cta);            
        }catch(Exception e){
           throw new Exception(e.getMessage());
        }  
    }
    
 
    
    
    
    public void listar()throws Exception{
      reset();
      try{
         this.listado = this.TransferenciaDataAccess.getCuentas();  
      }catch(Exception e){
          throw new Exception(e.getMessage());
      } 
    }
    
    
    
    
    public void listar(String  nit)throws Exception{
      reset();
      try{
         this.listado = this.TransferenciaDataAccess.getCuentas(nit);  
      }catch(Exception e){
          throw new Exception(e.getMessage());
      } 
    }
    
    
    
    
     
    public void  delete(String[]  ids)throws Exception{
      try{
          List aux = new LinkedList();
          if(ids!=null)
              for(int i=0;i<ids.length;i++){
                  String id = ids[i];
                  for(int j=0;j<listado.size();j++){
                     Hashtable  ht = (Hashtable)listado.get(j);
                     if( id.equals( (String)ht.get("id") ) ){
                        listado.remove(j);
                        aux.add( ht );
                        break;
                     }
                  }
              }
          
          if(aux.size()>0)
             this.TransferenciaDataAccess.deleteCuentas(aux); 
          
          
      }catch(Exception e){
          throw new Exception(e.getMessage());
      } 
    }
    
    
    
    
    
    
    
  public List getInfoPropietario(String tipo, String id)throws Exception{
      List lista = new LinkedList();
      try{
        
          lista =  this.TransferenciaDataAccess.getInfoProveedor(tipo, id);
          
      }catch(Exception e){
          throw new Exception( " getInfoPropietario " + e.getMessage());
      } 
      return lista;
    }
    
    
    
    
    
    
    
    
    
     public void obtenerRegistro(String id){
        if(listado.size()>0){
           for(int i=0;i<this.listado.size();i++){
               Hashtable  ht = (Hashtable)listado.get(i);
               if( id.equals( (String)ht.get("id") ) ){
                   this.cuentaPropietio = ht;
                   break;
               }
           }
        }
    }
     
     
    
    
    public String reset(String val){
        if(val==null)
           val = "";
        return val;
    }
    
    
    
    
    
    
    
    /**
     * Getter for property listado.
     * @return Value of property listado.
     */
    public List getListado() {
        return listado;
    }
    
    /**
     * Setter for property listado.
     * @param listado New value of property listado.
     */
    public void setListado(List listado) {
        this.listado = listado;
    }
    
    /**
     * Getter for property cuentaPropietio.
     * @return Value of property cuentaPropietio.
     */
    public Hashtable getCuentaPropietio() {
        return cuentaPropietio;
    }
    
    /**
     * Setter for property cuentaPropietio.
     * @param cuentaPropietio New value of property cuentaPropietio.
     */
    public void setCuentaPropietio(Hashtable cuentaPropietio) {
        this.cuentaPropietio = cuentaPropietio;
    }
    
    
    /**
     * Getter for property tipoCuenta.
     * @return Value of property tipoCuenta.
     */
    public java.util.List getTipoCuenta() {
        return tipoCuenta;
    }
    
    /**
     * Setter for property tipoCuenta.
     * @param tipoCuenta New value of property tipoCuenta.
     */
    public void setTipoCuenta(java.util.List tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }
    
    /**
     * Getter for property listaBancos.
     * @return Value of property listaBancos.
     */
    public java.util.List getListaBancos() {
        return listaBancos;
    }
    
    /**
     * Setter for property listaBancos.
     * @param listaBancos New value of property listaBancos.
     */
    public void setListaBancos(java.util.List listaBancos) {
        this.listaBancos = listaBancos;
    }
    
    public void loadRequest( HttpServletRequest request)throws Exception{
        try{
            reset();
            
            cuentaPropietio.put("distrito",      reset( (request.getParameter("distrito")==null)?"FINV":request.getParameter("distrito") )  );
            cuentaPropietio.put("idmims",        reset( request.getParameter("mims")          ) );
            cuentaPropietio.put("nit",           reset( request.getParameter("nit")           ) );
            cuentaPropietio.put("banco",         reset( request.getParameter("banco")         ) );
            cuentaPropietio.put("sucursal",      reset( request.getParameter("sucursal")      ) );
            cuentaPropietio.put("cuenta",        reset( request.getParameter("cuenta")        ) );
            cuentaPropietio.put("tipo",          reset( request.getParameter("tipo")          ) );
            cuentaPropietio.put("nombre_cuenta", reset( request.getParameter("nombre_cuenta") ) );
            cuentaPropietio.put("ced_cuenta",    reset( request.getParameter("cedula_cuenta") ) );
            cuentaPropietio.put("user",          reset( request.getParameter("usuario")       ) );
            cuentaPropietio.put("descuento",     reset( request.getParameter("descuento")     ) );
            cuentaPropietio.put("principal",     reset( request.getParameter("principal")     ) );
            cuentaPropietio.put("secuencia",     reset( request.getParameter("secuencia")     ) );
            cuentaPropietio.put("e_mail",        reset( request.getParameter("e_mail")     ) );
            
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
}
