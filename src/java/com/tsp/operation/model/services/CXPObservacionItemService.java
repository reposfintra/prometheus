/***********************************************************************************
 * Nombre clase : ............... CXPDocObservacionItemService.java                *
 * Descripcion :................. Clase que maneja los Servicios                   *
 *                                asignados a Model relacionados con el            *
 *                                programa de Autorizacion de cuentas por pagar    *
 * Autor :....................... Ing. Henry A.Osorio González                     *
 * Fecha :....................... 10 de octubre de 2005, 11:02 AM                  *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;

public class CXPObservacionItemService {
    
    public CXPObservacionItemDAO dao;
    
    public CXPObservacionItemService() {
        dao = new CXPObservacionItemDAO();
    }
    public CXPObservacionItemService(String dataBaseName) {
        dao = new CXPObservacionItemDAO(dataBaseName);
    }
    /**
     * Metodo buscarObservacionesItem, permite buscar si existe una observacion de un item
     * @autor : Ing. Henry A. Osorio González
     * @see   : buscarObservacionesItem - CXPObservacionItemDAO
     * @param : CXPOItemDoc item de la factura
     * @version : 1.0
     */
    public void buscarObservacionItem(CXPItemDoc item) throws SQLException {
        dao.buscarObservacionesItem(item);
    }
    /**
     * Metodo getObservacionItem, retorna la observacion abierta de un item especifico
     * @autor : Ing. Henry A. Osorio González
     * @see   : getObservacionItem - CXPObservacionItemDAO
     * @version : 1.0
     */
    public CXPObservacionItem getCXPObservacionItem(){
        return dao.getObservacionItem();
    }
    /**
     * Metodo insertarObservacionItem, recibe el objeto observacion el cual contiene información
     * del item al cual se le hará una observación
     * @autor : Ing. Henry A. Osorio González
     * @see   : insertObservacionItem - CXPObservacionItemDAO
     * @param : CXPObservacionItem observacion del item
     * @version : 1.0
     */
    public void insertarObservacionItem(CXPObservacionItem observacion, String Usuario) throws SQLException{
        dao.insertObservacionItem(observacion,Usuario); 
    }
    /**
     * Metodo cerrarObservacion, permite cerrar la observacion abierta de un item especifico
     * @autor : Ing. Henry A. Osorio González
     * @see   : cerrarObservacion - CXPObservacionItemDAO
     * @param : CXPObservacionItem observacion del item
     * @version : 1.0
     */
    public void cerrarObservcacion(CXPObservacionItem item) throws SQLException{
        dao.cerrarObservacion(item);
    }
    /**
     * Metodo updateObservacionItem, recibe el objeto Observacion Item y el usuario autorizador
     * @autor : Ing. Henry A. Osorio González
     * @see   : updateObservacionItem - CXPObservacionItemDAO
     * @param : CXPObservacionItem Observacion del item , String usuario autorizador
     * @version : 1.0
     */
    public void updateObservacion(CXPObservacionItem item, String usuario) throws SQLException{
        dao.updateObservacionItem(item,usuario);
    }
    
     /**
     * Metodo updateObservacionItem, Actualiza las observaciones del pagador
     * @autor : Ing. Ivan Dario Gomez Vanegas 
     * @see   : updateObservacionItemPagador - CXPObservacionItemDAO
     * @param : CXPObservacionItem Observacion del item , String usuario autorizador
     * @version : 1.0
     */
    public void updateObservacionPagador(CXPObservacionItem item, String usuario) throws SQLException{
        dao.updateObservacionItemPagador(item,usuario);
    }
    
     /**
     * Metodo obtenerTextoActivo, devuelve el valor del texto activo
     * @autor : Ing. Ivan Dario Gomez Vanegas 
     * @see   : buscarTextoActivoObservacion - CXPObservacionItemDAO
     * @param : CXPObservacionItem Observacion del item 
     * @version : 1.0
     */
    public String obtenerTextoActivo(CXPItemDoc item) throws SQLException{ 
        return dao.buscarTextoActivoObservacion(item);
    }
}