/*
 * UsuarioService.java
 *
 * Created on 20 de noviembre de 2004, 11:33 AM
 */

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import org.apache.log4j.Logger;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  KREALES
 */
public class UsuarioService {
    
    private Logger logger = Logger.getLogger(UsuarioService.class);
    private UsuarioDAO usuario;
    private Usuario u;
    /** Creates a new instance of UsuarioService */
    public UsuarioService() {
        
        usuario = new UsuarioDAO();
    }
    
    public Usuario getUsuario( )throws SQLException{
        
        return usuario.getUsuario();
        
    }
    
    public void buscaUsuario(String login)throws SQLException{
        try{
            usuario.searchUsuario(login);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    
    public void buscaUsuario(String login, String distrito)throws SQLException{
        try{
            usuario.searchUsuario(login, distrito);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    
    public Usuario getUsuarioOnline(){
        return this.u;
    }
    public void setUsuario(Usuario u){
        this.u = u;
    }
    public Usuario obtenerUsuario(String idusuario) throws SQLException {
        return usuario.obtenerUsuario(idusuario);
    }
    
    ////////sandrameg
    public boolean validarClave(String login, String claveEncr) throws SQLException{
        return usuario.validarClave(login, claveEncr);
    }
    
    public boolean verificarDistrito(String us, String dstrct) throws SQLException{
        return usuario.verificarDistrito(us, dstrct);
    }
    public boolean verificarProyecto(String u, String dstrct, String proyecto) throws SQLException{
        return usuario.verificarProyecto(u, dstrct, proyecto);
    }
    public boolean verificarPerfil(String usu, String perfil) throws SQLException{
        return usuario.verificarPerfil(usu, perfil);
    }
    public void actualizarClave(String login, String nclave) throws SQLException{
        usuario.actualizarClave(login,  nclave);
    }
    public void actualizarFechasInc(String login) throws SQLException{
        usuario.actualizarFechasInc(login);
    }
    ///210905
    public void obtenerUsuarios() throws SQLException{
        usuario.obtenerUsuarios();
    }
    public Vector getUsuarios() throws SQLException{
        return usuario.getUsuarios();
    }
    public void buscarUsuariosAgencia(String agencia)throws SQLException{
        usuario.buscarUsuariosAgencia(agencia);
    }
    public java.util.TreeMap getTusuarios() {
        return usuario.getTusuarios();
    }
    public void setTusuarios(java.util.TreeMap Tusuarios) {
        usuario.setTusuarios(Tusuarios);
    }
    
    //DBastidas 16.12.05
    /**
     * Metodo buscarEmailPerfil, buasca los email de los usuarios relacionados,
     * a un perfil y retorna una cadena con los correos
     * @param: perfil
     * @see: buscarEmailPerfil - UsuarioDao
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public String buscarEmailPerfil(String perfil)throws SQLException{
        try{
            return usuario.buscarEmailPerfil(perfil);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    //Alejandro Payares, nov 17 de 2005
    public void agregarUsuario(String[] createArguments, String [] perfiles)
    throws SQLException, com.tsp.exceptions.EncriptionException {
        usuario.agregarUsuario(createArguments,perfiles);
    }
    
    public void actualizarUsuario(String [] userData, String [] perfiles)
    throws SQLException, com.tsp.exceptions.EncriptionException {
        usuario.actualizarUsuario(userData,perfiles);
    }
    
    public void eliminarUsuario(String userID) throws SQLException {
        usuario.eliminarUsuario(userID);
    }
    
    // LOS SIGUIENTES M�TODOS Y/O ATRIBUTOS FUERON COPIADOS DE LA APLICACI�N SOT
    // PARA IMPLEMENTAR EL MODULO DE ADMINISTRACI�N DE USUARIOS.
    // Alejandro Payares - nov 18 de 2005 - 3:05 pm
    
    public void searchUsuario(String login )throws SQLException{
        usuario.searchUsuario(login);
    }
    
    public void usuariosCreadosSearch(final Usuario userLoggedIn)throws SQLException {
        usuario.usuariosCreadosSearch(userLoggedIn);
    }
    
    public List getUsuariosCreados() {
        return usuario.getUsuariosCreados();
    }
    //David 23.12.05
    /**
     * Este metodo retorna un Vector con los Usuarios de Aprobacion
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public Vector getUsuariosAprobacion() throws SQLException {
        return usuario.getUsuariosAprobacion();
    }
    
    /**
     * Este metodo retorna un TreeMap con los Usuarios de Aprobacion
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public TreeMap getUsuariosAprobacionT() throws SQLException{
        TreeMap usuarios = new TreeMap();
        Vector vusuarios = this.getUsuariosAprobacion();
        ////System.out.println("n usuarios Aprobados"+vusuarios.size());
        for(int i=0; i< vusuarios.size(); i++){
            
            Usuario usuario = (Usuario)vusuarios.elementAt(i);
            ////System.out.println("hola");
            // if( usuario.getBanco().matches(banco) )
            ////System.out.println("jkhsk"+usuario.getIdusuario()+ usuario.getNombre());
            usuarios.put(usuario.getIdusuario(), usuario.getNombre());
        }
        return usuarios;
    }
    
    /* Procedimiento que genera un Vector con objetos tipo Usuarios pertenencientes a un departamento en especifico
     * parametros
     * @params String dpto ........ departamento de la empresa
     * @throws Exception.
     * @return
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     * @see
     */
    public void getUsuariosPorDpto(String dpto) throws SQLException {
        usuario.getUsuariosPorDpto(dpto);
    }
    
    /**
     * Getter for property vUsuarios.
     * @return Value of property vUsuarios.
     */
    public java.util.Vector getVUsuarios() {
        return usuario.getVUsuarios();
    }
    
    
    /**
     * Setter for property vUsuarios.
     * @param vUsuarios New value of property vUsuarios.
     */
    public void setVUsuarios(java.util.Vector vUsuarios) {
        usuario.setVUsuarios(vUsuarios);
    }
    
    /**
     * Este m�todo genera un Treemap con los usuarios pertenecientes a un departamento
     * @param String agencia
     * @throws
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public TreeMap usuariosPorDptoT() throws SQLException{
        TreeMap usuarios = new TreeMap();
        Vector users = this.getVUsuarios();
        for(int i=0; i< users.size(); i++){
            Usuario u = (Usuario) users.elementAt(i);
            usuarios.put( u.getNombre(),u.getIdusuario());
        }
        return usuarios;
    }
    
    /**
     * Actualiza la ultima fecha en que el usuario dado entr� al sistema.
     * @param idusuario el id del usuario que ser� actualizado
     * @throws SQLException si algun error ocurre en la base de datos
     * @autor Alejandro Payares
     */
    public void actualizarFechaUltimoIngreso(String idusuario, String dstrct) throws SQLException{
        usuario.actualizarFechaUltimoIngreso(idusuario, dstrct);
    }
    
    /**
     * Setea un Vector con los registros de la tabla tabla usuarios que empiesen con la cadena de prametro nombre
     * @autor David Lamadrid
     * @throws SQLException
     * @version 1.0
     */
    /*public void obtenerUsuariosPorNombre(String nombre) throws SQLException{
        usuario.obtenerUsuariosPorNombre(nombre);
    }*/
    
    
    /**
     * verifica si existe un usuario en la tabla usuarios dado el login retorna false si no existe y true de lo contrario
     * @autor David Lamadrid
     * @throws SQLException
     * @version 1.0
     */
    public boolean existeUsuario(String login) throws SQLException{
        return usuario.existeUsuario(login);
    }
    
     /**
     * listarUsuarios lista los usuario registradosos
     * @param 
     * @throws SQLException si algun error ocurre en la base de datos
     * @autor diogenes Bastidas
     */   
     public Vector listarUsuarios() throws Exception {         
        try{
            return usuario.listarUsuarios();
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
     }
     /**
     * Setea un Vector con los registros de la tabla tabla usuarios que empiesen con la cadena de prametro nombre
     * @autor David Lamadrid
     * @throws SQLException
     * @version 1.0
     */
    public void obtenerUsuariosPorNombre(String nombre) throws SQLException{
        usuario.obtenerUsuariosPorNombre(nombre);
    }
    
    /**
     * Busca aquellos usuarios visibles para el loggedUser dado cuyo id de usuario comienze
     * con los criterios de busqueda dados
     * @param userLoggedIn El usuario en sesi�n
     * @param busqueda El texto de busqueda
     * @throws SQLException Si alg�n error ocurren en la base de datos.
     * @autor Alejandro Payares
     */    
    public void buscarUsuarios(Usuario userLoggedIn, String busqueda)throws SQLException {
        usuario.buscarUsuarios(userLoggedIn, busqueda);
    }
    /**
     * metodo que obtine el nombre de la agencia de un usuario
     * @autor Diogenes Bastidas
     * @throws SQLException
     * @version 1.0
     */
    public String obtenerAgenciaUsuario(String login) throws Exception{
        return usuario.obtenerAgenciaUsuario(login);
    }
     /**
      * Metodo que retorna los datos de horas_zonificada dado un id de usuario
      * en un hashtable.
      * @parameter Login del usuario (String)
      * @autor Karen Reales
      * @throws SQLException
      * @version 1.0
      */
    public Hashtable getHoraZonificada(String login) throws Exception{
        return usuario.getHoraZonificada(login);
    }
    
        /**
     *
     * @param login
     * @throws SQLException
     */
    public boolean  ValidarOpcion(String login,String id_opcion )throws SQLException{
       return usuario.ValidarOpcion(login, id_opcion);
    }
    
    public ArrayList<Usuario> buscarUsuariosPreaprobados()throws SQLException {
        return usuario.buscarUsuariosPreaprobados();
    }
    
    /**
     *
     * @param ipAddress
     */
    public void saveRemoteAddress(String ipAddress, String login, String lat , String lon)throws SQLException{        
        usuario.saveRemoteAdress(ipAddress, login, lat, lon);
    }

}
