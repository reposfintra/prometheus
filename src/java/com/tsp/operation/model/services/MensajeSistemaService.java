/* * MensajeSistemaService.java * * Created on 3 de abril de 2008 */
package com.tsp.operation.model.services;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.MensajeSistema;//20100721
import com.tsp.util.Util;//20100721
/** * * @author NAVI */
public class MensajeSistemaService {
    MensajeSistemaDAO mensajeSistemaDAO;
    String mensajeSistema;
    public MensajeSistemaService() {
        mensajeSistema="";
        mensajeSistemaDAO = new MensajeSistemaDAO();
    }
    public MensajeSistemaService(String dataBaseName) {
        mensajeSistema="";
        mensajeSistemaDAO = new MensajeSistemaDAO(dataBaseName);
    }
    public String getMensajeSistema(){
        return mensajeSistemaDAO.getMensajeSistema();
    }
    public String actualizarMensajeSistema(){
        String respuesta="";
        String mensajeSistema="";
        String tipoMensaje="";
        mensajeSistema = getMensajeSistema();
        if (mensajeSistema.length()>5){
            tipoMensaje=mensajeSistema.substring(0,4);
        }
        if (tipoMensaje.equals("2msg")){
            mensajeSistemaDAO.actualizarMensajeSistema();
        }
        return mensajeSistema;
    }

    public String consultarMensajeSistema(){
        String respuesta="";
        String mensajeSistema="";
        String tipoMensaje="";
        mensajeSistema = getMensajeSistema();
        return mensajeSistema;
    }

    public int getSegundo(){
        java.util.Calendar fech = java.util.Calendar.getInstance();
        return fech.SECOND;
    }

    public MensajeSistema obtainMensajeSistema(String loginn,String hora_actual) throws Exception{//para obtener variables en bd asociadas al usuario y su interaccion con el sistema//20100722
        return mensajeSistemaDAO.obtainMensajeSistema( loginn,hora_actual);
    }
    public String getVariablesBd(String loginn) throws Exception{
        String c="&&";
        String hora_actual=Util.getFechaActual_String(10);
        MensajeSistema ds=obtainMensajeSistema(loginn,hora_actual);
        String respuesta="";
        respuesta=ds.getFrecuenciaRelojServer()+c+ds.getFrecuenciaRelojJs()+c+ds.getFrecuenciaConsultaBd()+c+ds.getTipoMensaje()+c+ds.getEstadoMensaje()+c+ds.getMensaje()+c+hora_actual+c+ds.getAncho()+c+ds.getAltura()+c+ds.getCss()+c+ds.getShowProgress()+c+ds.getMostrarSegundos();
        return respuesta;
    }

    public String obtainCiaMostrable(String loginn) throws Exception{
        return mensajeSistemaDAO.obtainCiaMostrable( loginn);
    }

}