/*********************************************************************************
 * Nombre clase :      ReporteGeneralService.java                                *
 * Descripcion :       Service de los reportes                                   *
 * Autor :             Jose de la rosa                                           *
 * Fecha :             16 de junio de 2006, 08:27 AM                             *
 * Version :           1.0                                                       *
 * Copyright :         Fintravalores S.A.                                   *
 *********************************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import java.io.*;

public class ReporteGeneralService {
    private ReporteGeneralDAO reporte;
    /** Creates a new instance of ReporteGeneralService */
    public ReporteGeneralService() {
        reporte = new ReporteGeneralDAO();
    }
    
    /**
     * Metodo: consultaDiferenciaViajes, permite generar el reporte de diferencia de tiempos de carge y descarge de un cliente en un intervalo de tiempo.
     * @autor : Ing. Jose de la rosa
     * @param : fecha de inicio, fecha fin y cliente
     * @version : 1.0
     */
    public Vector consultaDiferenciaViajes(String fecha_ini, String fecha_fin, String cliente) throws SQLException{
        return reporte.consultaDiferenciaViajes(fecha_ini, fecha_fin, cliente);
    }
    
    /**
     * Metodo: consultaTrailers, permite generar el reporte de trailers
     * @autor : Ing. Jose de la rosa
     * @param : fecha de inicio, fecha fin, trailer, cabezote
     * @version : 1.0
     */
    public void consultaTrailers(String fecha_ini, String fecha_fin, String trailer, String cabezote) throws SQLException{
        reporte.consultaTrailers(fecha_ini, fecha_fin, trailer, cabezote);
    }
    
    /**
     * Getter for property VecReporteGeneral.
     * @return Value of property VecReporteGeneral.
     */
    public java.util.Vector getVecReporteGeneral() {
        return reporte.getVecReporteGeneral();
    }
    
    /**
     * Obtiene el porcentaje utilizado del rango autorizado de series de planilla
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct Distrito
     * @version 1.0
     */
    public double sentinelaSeriePlanillas(String dstrct) throws SQLException{
        return reporte.sentinelaSeriePlanillas(dstrct);
    }
    
    /**
     * Metodo: reporteControlDiscrepancia, permite generar el reporte de control de discrepancia
     * @autor : Ing. Jose de la rosa
     * @param : distrito, fecha de inicio, fecha fin y aprobado
     * @version : 1.0
     */
    public Vector reporteControlDiscrepancia( String distrito, String fecha_ini, String fecha_fin, String tipo ) throws SQLException{
        return reporte.reporteControlDiscrepancia( distrito, fecha_ini, fecha_fin, tipo );
    }
    /**
     * @autor : Ing. Luigi
     * @version : 1.0
     */
    public Vector ajustesFletes( String fecha_ini, String fecha_fin, String distrito ) throws SQLException{
        return reporte.ajustesFletes(fecha_ini, fecha_fin, distrito);
    }
    
    //ffernandez 18/04/2007
    public void ReoporteIca(String FechaI,String FechaF) throws Exception {
        reporte.ReoporteIca(FechaI,FechaF);
    }
    
    //ffernandez 18/04/2007
    public java.util.Vector getVector() {
        return reporte.getVector();
    }
    
    /**
     * Metodo: consultaTrailers, permite generar el reporte de trailers
     * @autor : Ing. Jose de la rosa
     * @param : fecha de inicio, fecha fin, trailer, cabezote
     * @version : 1.0
     */
    public Vector reporteFormatoCumplimiento(String fecha_ini, String fecha_fin, String agencia, String cliente) throws SQLException{
        return reporte.reporteFormatoCumplimiento(fecha_ini, fecha_fin, agencia, cliente);
    }
    
    /**
     * Metodo: reporteProduccionEquipo, permite generar el reporte de produccion de equipos
     * @autor : Ing. Jose de la rosa
     * @param : fecha de inicio, fecha fin
     * @version : 1.0
     */
    public Vector reporteProduccionEquipo( String fecha_ini, String fecha_fin ) throws SQLException{
        return reporte.reporteProduccionEquipo(fecha_ini, fecha_fin);
    }
    
    /**
     * Metodo: reporteUtilizacionPlaca, permite generar el reporte Utilizacion de placas
     * @autor : Ing. Jose de la rosa
     * @param : fecha de inicio, fecha fin, agencia destino
     * @version : 1.0
     */
    public Vector reporteUtilizacionPlaca(String fecha_ini, String fecha_fin, String agencia) throws SQLException{
        return reporte.reporteUtilizacionPlaca(fecha_ini, fecha_fin, agencia);
    }
    
    public void ReoporteEquipo() throws Exception {
        reporte.ReoporteEquipo();
    }
    
        /**
     * Metodo: reporteRemesasNacionales, permite generar el reporte de remesas nacionales
     * @autor : Ing. Jose de la rosa
     * @param : fecha de inicio, fecha fin
     * @version : 1.0
     */
    public LinkedList reporteRemesasNacionalesCaso1( String fecha_ini, String fecha_fin ) throws SQLException{
        return reporte.reporteRemesasNacionalesCaso1( fecha_ini, fecha_fin );
    }
    
     /**
     * Metodo: reporteRemesasNacionales, permite generar el reporte de remesas nacionales
     * @autor : Ing. Jose de la rosa
     * @param : remesa
     * @version : 1.0
     */
    public Vector reporteRemesasNacionalesCaso2( String remesa ) throws SQLException{
        return reporte.reporteRemesasNacionalesCaso2( remesa );
    }
    
     /**
     * Metodo: reporteRemesasNacionalesCaso3, permite generar el reporte de remesas nacionales
     * @autor : Ing. Jose de la rosa
     * @param : planilla
     * @version : 1.0
     */
    public LinkedList reporteRemesasNacionalesCaso3( String planilla ) throws SQLException{
        return reporte.reporteRemesasNacionalesCaso3( planilla );
    }
}
