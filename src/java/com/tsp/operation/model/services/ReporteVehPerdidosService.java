/***************************************************************************
 * Nombre clase :                 ReporteVehPerdidosService.java           *
 * Descripcion :                  Clase que maneja los Servicios           *
 *                                asignados a Model relacionados con el    *
 *                                programa de generacion del reporte de    *
 *                                vehiculos perdidos                       *
 * Autor :                        Ing. Juan Manuel Escandon Perez          *
 * Fecha :                        10 de enero de 2006, 11:05 AM            *
 * Version :                      1.0                                      *
 * Copyright :                    Fintravalores S.A.                  *
 ***************************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.util.*;
public class ReporteVehPerdidosService {
    private ReporteVehPerdidosDAO    VPDataAccess;
    private List                     ListaVP;
    
    /** Creates a new instance of ReporteVehPerdidosService */
    public ReporteVehPerdidosService() {
        VPDataAccess    = new ReporteVehPerdidosDAO();
        ListaVP         = new LinkedList();
    }
    
    
    
    /**
     * Metodo listVehPerdidos, lista todas las recursos que no han sido asignados aun requerimiento de un cliente
     * @autor : Ing. Juan Manuel Escandon Perez
     * @see   : listVehPerdidos - ReporteVehPerdidosDAO
     * @version : 1.0
     */
    public void listVehPerdidos() throws Exception {
        try{
            this.ReiniciarLista();
            this.ListaVP = VPDataAccess.listVehPerdidos();
        }
        catch(Exception e){
            throw new Exception("Error en listVehPerdidos [ReporteVehPerdidosService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Metodo ReiniciarLista, le asigna el valor null al atributo ListaRSD,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public void ReiniciarLista(){
        this.ListaVP = null;
    }
    
    /**
     * Metodo getList, retorna el valor del atributo ListaVP,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public List getList(){
        return this.ListaVP;
    }
    
}
