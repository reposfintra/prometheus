/***********************************************************************************
 * Nombre clase :  IdentidadService.java
 * Descripcion :   Clase que maneja los Servicios                   
 *                 asignados a Model relacionados con el            
 *                 programa de Identidad
 * Autor :    Ing. Diogenes Antonio Bastidas Morales           
 * Fecha :    16 de julio de 2005, 08:12 PM             
 * Version :   1.0                                             
 * Copyright : Fintravalores S.A.                         
 ***********************************************************************************/


package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;

public class IdentidadService {
    private IdentidadDAO identidad_d;
    /** Creates a new instance of IdentidadService */
    public IdentidadService() {
        identidad_d = new IdentidadDAO();
    }
    public IdentidadService(String dataBaseName) {
        identidad_d = new IdentidadDAO(dataBaseName);
    }
    /**
     * Metodo obtIdentidad, retorna el objeto de Identidades
     * @param: 
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: obtIdentidad - IdentidadDAO()
     * @version : 1.0
     */
    public Identidad obtIdentidad(){
        return identidad_d.obtIdentidad();
    }
    /**
     * Metodo obtVecIdentidad, retorna el vector de Identidades
     * @param: 
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: obtVecIdentidad - IdentidadDAO()
     * @version : 1.0
     */
    public Vector obtVecIdentidad() {
        return identidad_d.obtVecIdentidad();
    }
    /**
     * Metodo listarIdentidad, lista las Identidades                  
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: listarIdentidad - IdentidadDAO()
     * @version : 1.0
     */ 
    public void listarIdentidad() throws SQLException {
        try{
            identidad_d.listarIdentidad();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo buscarIdentidad, busca la identidad                  
     * @param: cedula, cia
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: buscarIdentidad - IdentidadDAO()
     * @version : 1.0
     */ 
    public void buscarIdentidad(String ced,String cia) throws SQLException {
        try{
            identidad_d.buscarIdentidad(ced, cia);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo insertarIdentidad, ingresa un registro en la tabla identidad
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @see : insertarIdentidad - IdentidadDAO()
     * @version : 1.0
     */
    public void insertarIdentidad(Identidad identidad) throws SQLException{
        try{
            identidad_d.setIdentidad(identidad);
            identidad_d.insertarIdentidad();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo  existeIdentidad, retorna true o false si existe la identidad.
     * @param: codigo identidad
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: existeIdentidad - IdentidadDAO()
     * @version : 1.0
     */
    public boolean existeIdentidad(String cod) throws SQLException {
        try{
            return identidad_d.existeIdentidad(cod);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
     /**
     * Metodo  existeIdentidadAnulado, retorna true o false si la identidad esta anulada.
     * @param:compa�ia, codigo identidad
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: existeIdentidadAnulado - IdentidadDAO()
     * @version : 1.0
     */ 
    public boolean existeIdentidadAnulado(String cod, String cia) throws SQLException {
        try{
            return identidad_d.existeIdentidadAnulado(cod,cia);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
     /**
     * Metodo modificarIdentidad, modifica un registro en la tabla identidad
     * @param: objeto Identidad
     * @autor : Ing. Diogenes Bastidas Morales
     * @see:  modificarIdentidad - IdentidadDAO()
     * @version : 1.0
     */ 
    public void modificarIdentidad(Identidad identidad) throws SQLException {
        try{
            identidad_d.setIdentidad(identidad);
            identidad_d.modificarIdentidad();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo  anularIdentidad, anula la identidad.
     * @param: objeto Identidad
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: anularIdentidad - IdentidadDAO()
     * @version : 1.0
     */
    public void anularIdentidad(Identidad identidad) throws SQLException {
        try{
            identidad_d.setIdentidad(identidad);
            identidad_d.anularIdentidad();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo  activarIdentidad, modifica la identidad.
     * @param: objeto Identidad
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: activarIdentidad - IdentidadDAO()
     * @version : 1.0
     */
    public void activarIdentidad(Identidad identidad) throws SQLException {
        try{
            identidad_d.setIdentidad(identidad);
            identidad_d.activarIdentidad();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo listarIdentidadXBusq, lista las identidades dependiendo al documento,
     * nombre y compa�ia
     * @param:documento, nombre cia
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: listarIdentidadXBusq - IdentidadDAO
     * @version : 1.0
     */ 
    public void listarIdentidadXBusq(String ced, String nomc,  String cia) throws SQLException {
        try{
            identidad_d.listarIdentidadXBusq(ced, nomc, cia);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
   /**
     * Metodo busquedaxNit, busca identidades NIT
     * Autor Sandra Escalante
     * @param: String frase
     * @see: busquedaxNit - IdentidadDAO()
     */ 
    public void busquedaxNit(String frase) throws SQLException{
        try{
            identidad_d.busquedaxNit(frase);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
     /**
     * Metodo busquedaxCedula, busca identidades CED
     * Autor Sandra Escalante
     * @param String frase
     * @see: busquedaxCedula - IdentidadDAO()
     */
    public void busquedaxCedula(String frase) throws SQLException{
        try{
            identidad_d.busquedaxCedula(frase);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
   /**
     * Metodo buscarPersonaNatural, busca las identidad dependiendo del la cedula y 
     * es cedula su tipo documento.
     * @param: documento
     * @autor : Ing. Sandra Escalante
    *  @see:  buscarPersonaNatural - IdentidadDAO()
     * @version : 1.0
     */  
    public void buscarPersonaNatural(String ced) throws SQLException {
        try{
            identidad_d.buscarPersonaNatural(ced);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo reset, restea el objeto identidad
     * @param:
     * @autor : Ing. Sandra Escalante
     * @version : 1.0
     * @see : reset - IdentidadDAO 
     */ 
    public void reset(){
        identidad_d.reset();
    }
    /**
     * Metodo existeNomnemo, retorna true o false si existe el nombre nemotecnico
     * @param: nombre nemotecnico
     * @autor : Ing. Diogenes Bastidas
     * @see: existeNomnemo - IdentidadDAO()
     * @version : 1.0
     */ 
    public boolean existeNomnemo(String nom) throws SQLException {
        try{
            return identidad_d.existeNomnemo(nom);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    //henry 151005
    /**
     * Metodo buscarTenedorxCedula, retorna el objeto Identidad depandiendo de la cedula
     * @param: cedula, atitulo
     * @autor : Ing. Henry Osorio
     * @see : buscarTenedorxCedula -IdentidadDAO()
     * @version : 1.0
     */ 
    public void buscarTenedorxCedula(String cedula, String atituo) throws SQLException {
        identidad_d.buscarTenedorxCedula(cedula,atituo);
    }
    /**
     * Getter for property tenedor.
     * @return Value of property tenedor.
     */
    public Tenedor getTenedor(){
        return identidad_d.getTenedor();
    }
    /////////////////////////////////////////////////////SERVICE////////////////////////////////////////////////////////////////
    /**
     * Metodo archivoTxt , Metodo que llama una funcion que escribe un archivo txt con los registros de la tabla Placa
     * @autor : Ing. Leonardo Parody
     * @param : String ruta
     * @param : String usuario
     * @param : String cedula
     * @version : 1.0
     */
    public void archivoTxt(String ruta, String cedula, Usuario usuario) throws SQLException{
        try{
            String base = usuario.getBase();
            ////System.out.println("Vamos pal DAO");
            identidad_d.consultaTxt(ruta, base, cedula);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    /**
     * Obtiene el nombre de las columnas de la tabla placa
     * @autor Ing. Tito Andr�s Maturana
     * @version 1.0
     * @throws SQLException Si se presenta un error en la conexion con la base de datos.
     */
    public Vector obtenerNombresCamposNit() throws SQLException{
        Vector cols = new Vector();
        
        ResultSetMetaData rsmd = identidad_d.obtenerMetadata();
        
        for( int i=1; i<=rsmd.getColumnCount(); i++){
            cols.add(rsmd.getColumnName(i));
        }
        
        return cols;
    }
    
    /**
     * listarIdentidadNOAprobada, lista los nit no aprobados de acuerdo al usuario
     * asignado aprobar las identidaes.
     * @autor Ing. Diogenes Bastidas Morales
     * @version 1.0
     * @see: listarIdentidadNOAprobada - IdentidadDAO
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public void listarIdentidadNOAprobada(String login) throws SQLException {
        identidad_d.listarIdentidadNOAprobada(login);
    }
    
    /**
     * aprobarRegistrio, aprueba los registros de la tabla nit
     * @autor Ing. Diogenes Bastidas Morales
     * @param: usuario, estado, cedula
     * @see: aprobarRegistrio - IdentidadDAO
     * @version 1.0
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public void aprobarRegistrio(String usuario, String estado, String cedula ) throws SQLException{
        try{
            identidad_d.aprobarRegistrio(usuario, estado, cedula);
        }
        catch(Exception e){
            throw new SQLException(e.getMessage());
        }
    }
    /**
     * Establece el valor de la propieda identidad
     * @autor Ing. Tito Andr�s Maturana
     * @version 1.0
     */
    public void setIdentidad(Identidad id){
        identidad_d.setIdentidad(id);
    }
    /**
     * Metodo buscarIdentidadxTid, busca identidades dado unos parametros
     * Autor Sandra Escalante
     * @param String ced (cedula), String tid (tipo de identidad)
     * @see: buscarIdentidadxTid- IdentidadDAO
     */
    public boolean buscarIdentidadxTid( String ced, String tid ) throws SQLException {
        return identidad_d.buscarIdentidadxTid(ced, tid);
    }
    /**
     * buscarPropietarioxCedula, busca el propietario en la tabla nit
     * dependiendo de la cedula
     * @autor Ing. Diogenes Bastidas Morales
     * @param:  cedula
     * @version 1.0
     * @see: buscarPropietarioxCedula -  IdentidadDAO
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public void buscarPropietarioxCedula(String cedula)throws SQLException{
        identidad_d.buscarPropietarioxCedula(cedula);
    }
    /**
     * Getter for property prov.
     * @return Value of property prov.
     */
    public com.tsp.operation.model.beans.Proveedor getPropietario() {
        return identidad_d.getPropietario();
    }  
    /**
     * existePropietario, busca el propietario en la tabla nit
     * y propietario dependiendo de la cedula
     * @autor Ing. Diogenes Bastidas Morales
     * @param:  cedula
     * @version 1.0
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public boolean existePropietario(String cedula)throws SQLException{
        return identidad_d.existePropietario(cedula);
    }
}
