/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import java.util.*;

import java.sql.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.Model;
import com.tsp.util.Util;
import java.text.*;





/**
 *
 * @author Alvaro
 */
public class ApplusService {

    private ApplusDAO applusDao;
    private ImpuestoApplus impuestoApplus;


    /** Creates a new instance of ClienteService */
    public ApplusService() {
        applusDao = new ApplusDAO();
        impuestoApplus = new ImpuestoApplus();
    }
    public ApplusService(String dataBaseName) {
        applusDao = new ApplusDAO(dataBaseName);
        impuestoApplus = new ImpuestoApplus();
    }


    public void buscaPrefactura(String id_contratista)throws SQLException{
        applusDao.buscaPrefactura(id_contratista);
    }
    public List getPrefactura() throws SQLException{
        return applusDao.getPrefactura();
    }

    public void buscaContratista(String id_contratista)throws SQLException{
        applusDao.buscaContratista(id_contratista);
    }
    public Contratista getContratista() throws SQLException{
        return applusDao.getContratista();
    }

    public void buscaComision(double valor)throws SQLException{
        applusDao.buscaComision(valor);
    }
    public Comision getComision() throws SQLException{
        return applusDao.getComision();
    }


    public void buscaImpuestoContrato(String codigo_general_impuesto,String distrito,String tipo_impuesto )throws SQLException{
        applusDao.buscaImpuestoContrato(codigo_general_impuesto, distrito, tipo_impuesto);
    }
    public ImpuestoContrato getImpuestoContrato() throws SQLException{
        return applusDao.getImpuestoContrato();
    }

    public void setSecuenciaPrefactura(String id_contratista)throws SQLException{
        applusDao.setSecuenciaPrefactura (id_contratista);
    }

    public void setAccion(String id_accion, String prefactura,double comision_ejecutiva,
                          double comision_canal, String codigo_iva, double vlr_iva,
                          String login,String codigo_rmat, double vlr_rmat,
                          String codigo_rmao, double vlr_rmao,
                          String codigo_rotr, double vlr_rotr,
                          double  porcentajeIva, double porcentajeRetMat, double porcentajeRetMao, double porcentajeRetOtr,
                          double vlr_factoring, double porcentajeFormula, double vlr_formula ,
                          double base_iva,double vlr_formula_provintegral)throws SQLException{
        applusDao.setAccion (id_accion, prefactura,comision_ejecutiva, comision_canal,
                             codigo_iva, vlr_iva,login, codigo_rmat, vlr_rmat,
                             codigo_rmao, vlr_rmao,codigo_rotr, vlr_rotr,
                             porcentajeIva,porcentajeRetMat,porcentajeRetMao,porcentajeRetOtr,vlr_factoring,
                             porcentajeFormula,vlr_formula, base_iva, vlr_formula_provintegral);
    }


    public void buscaPrefacturaResumen(String id_contratista,String fechaInicial,String fechaFinal)throws SQLException{
        applusDao.buscaPrefacturaResumen(id_contratista,fechaInicial, fechaFinal);
    }
    public List getPrefacturaResumen() throws SQLException{
        return applusDao.getPrefacturaResumen();
    }




    public String getCodigoImpuesto(String tipoImpuesto){
        return impuestoApplus.getCodigo(tipoImpuesto);
    }

    public String getDescripcion(String tipoImpuesto){
        return impuestoApplus.getDescripcion(tipoImpuesto);
    }
    public double getPorcentaje(String tipoImpuesto){
        return impuestoApplus.getPorcentaje(tipoImpuesto);
    }


    public void setImpuesto(ImpuestoContrato impuestoContrato,String tipoImpuesto) {
        impuestoApplus.setImpuesto(impuestoContrato, tipoImpuesto);
    }


    public void buscaDetallePrefactura(String prefactura)throws SQLException{
        applusDao.buscaDetallePrefactura(prefactura);
    }
    public List getDetallePrefactura() throws SQLException{
        return applusDao.getDetallePrefactura();
    }


    public List getContratistaPrefactura() throws SQLException{
        return applusDao.getContratistaPrefactura();
    }

    public List getFacturaGeneral() throws SQLException{
        return applusDao.getFacturaGeneral();
    }


    public String setCxp_doc (String dstrct,String proveedor, String tipo_documento,String documento,
                              String descripcion,String agencia,
                              String handle_code,String aprobador,String usuario_aprobacion,
                              String banco,String sucursal,String moneda, Double vlr_neto,
                              Double vlr_total_abonos,Double vlr_saldo,Double vlr_neto_me,Double vlr_total_abonos_me,
                              Double vlr_saldo_me, Double tasa,String observacion,String user_update,
                              String creation_user,String base,String clase_documento,String moneda_banco,
                              String fecha_documento,String fecha_vencimiento,String clase_documento_rel,
                              String last_update, String creation_date)throws SQLException{

        return applusDao.setCxp_doc(dstrct, proveedor, tipo_documento, documento, descripcion,
                                    agencia, handle_code, aprobador, usuario_aprobacion, banco, sucursal,
                                    moneda, vlr_neto, vlr_total_abonos, vlr_saldo, vlr_neto_me,
                                    vlr_total_abonos_me, vlr_saldo_me, tasa, observacion, user_update,
                                    creation_user, base, clase_documento, moneda_banco,
                                    fecha_documento, fecha_vencimiento, clase_documento_rel,
                                    last_update, creation_date);
    }

    public String setCxp_items_doc (String dstrct, String proveedor, String tipo_documento,
                                   String documento, String item,String descripcion,
                                   double vlr, double vlr_me, String codigo_cuenta,
                                   String user_update,
                                   String creation_user, String base,
                                   String concepto, String auxiliar,
                                   String last_update, String creation_date)throws SQLException{

        return applusDao.setCxp_items_doc(dstrct, proveedor, tipo_documento, documento,
                                          item, descripcion, vlr, vlr_me, codigo_cuenta,
                                          user_update,
                                          creation_user, base, concepto, auxiliar,
                                          last_update, creation_date);
    }


   public String setFacturaContratista (String factura_contratista, String fecha_factura_contratista,
                                        String id_contratista, String prefactura) throws SQLException {
       return applusDao.setFacturaContratista(factura_contratista, fecha_factura_contratista, id_contratista, prefactura);

   }


    public void ejecutarSQL(Vector comandosSQL) throws SQLException {
        applusDao.ejecutarSQL(comandosSQL);
    }


    public List getFacturaApplus(String fechaInicial,String fechaFinal, String tipoFactura) throws SQLException{
        return applusDao.getFacturaApplus(fechaInicial,fechaFinal,tipoFactura);
    }

   public String setFacturaApplus (String factura_applus, String fecha_factura_applus,
                                   String id_contratista, String prefactura,String tipoFactura) throws SQLException {
       return applusDao.setFacturaApplus(factura_applus, fecha_factura_applus, id_contratista, prefactura,tipoFactura);

   }


    public void buscaOfertaEca()throws SQLException{
        applusDao.buscaOfertaEca();
    }
    public List getOfertaEca() throws SQLException{
        return applusDao.getOfertaEca();
    }


    public void buscaOfertaEcaDetalle(int id_orden, String fecha_financiacion)throws SQLException{
        applusDao.buscaOfertaEcaDetalle(id_orden,fecha_financiacion);
    }
    public List getOfertaEcaDetalle() throws SQLException{
        return applusDao.getOfertaEcaDetalle();
    }




    // Metodo para armar una lista con los datos de cada accion de una orden
    // para proceder a liquidar la orden para prefacturar a ECA

    public void listaOfertas(Model model, String orden)throws SQLException {

        int id_orden = Integer.parseInt(orden);

        java.util.Date date = new java.util.Date();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        String fecha_financiacion = sdf.format(date);

        String f_facturado_cliente=applusDao.buscaF_facturado_cliente(orden);

        if (!(f_facturado_cliente.equals("0099-01-01")) ){
            fecha_financiacion=f_facturado_cliente;
        }

        model.applusService.buscaOfertaEcaDetalle(id_orden,fecha_financiacion);


        List listaOfertaEcaDetalle = model.applusService.getOfertaEcaDetalle();
        if(listaOfertaEcaDetalle.size()!=0) {

            TotalOferta totalOferta = new TotalOferta ();
            totalizaOferta(listaOfertaEcaDetalle, totalOferta, fecha_financiacion);

            PuntosFinanciacion puntosFinanciacion = new PuntosFinanciacion();
            puntosFinanciacion    = totalOferta.getPuntosFinanciacion();
            String clase_dtf      = puntosFinanciacion.getClase_dtf();

            int cuotas_reales     = totalOferta.getCuotas_reales();

            double porcentajeFactoringFintra = getPorcentajeFactoringFintra( clase_dtf,  cuotas_reales);
            totalOferta.setPorcentaje_factoring_fintra(porcentajeFactoringFintra);

            int i=0;
            Iterator it = listaOfertaEcaDetalle.iterator();
            while (it.hasNext()) {

              i++;

              OfertaEcaDetalle  ofertaEcaDetalle = (OfertaEcaDetalle)it.next();

              ofertaEcaDetalle.setFecha_financiacion(fecha_financiacion);

              // Calculo Totalprev1

              double total_prev1_mat = ofertaEcaDetalle.getTotal_prev1_mat();
              double total_prev1_mob = ofertaEcaDetalle.getTotal_prev1_mob();
              double total_prev1_otr = ofertaEcaDetalle.getTotal_prev1_otr();
              double administracion  = ofertaEcaDetalle.getAdministracion();
              double imprevisto      = ofertaEcaDetalle.getImprevisto();
              double utilidad        = ofertaEcaDetalle.getUtilidad();

              double total_prev1     = Util.redondear2(total_prev1_mat + total_prev1_mob + total_prev1_otr +
                                                       administracion + imprevisto + utilidad , 2) ;
              ofertaEcaDetalle.setTotal_prev1(total_prev1);

              double porcentaje_iva = ofertaEcaDetalle.getPorcentaje_iva();



              // Calcula el iva de la bonificacion y del iva total_prev1

              double iva_bonificacion = 0.00;
              double iva_total_prev1 = 0.00;
              double iva_utilidad = 0.00;


              // Caso en el que la utilidad es diferente de cero
              if(ofertaEcaDetalle.getUtilidad() != 0) {

                  // Calculo iva de la bonificacion
                  iva_bonificacion = ( (ofertaEcaDetalle.getUtilidad() * ofertaEcaDetalle.getBonificacion()) / total_prev1 ) * ofertaEcaDetalle.getPorcentaje_iva() ;

                  // Calculo del iva de total_prev1

                  iva_utilidad =  ofertaEcaDetalle.getUtilidad() * ofertaEcaDetalle.getPorcentaje_iva();
                  iva_utilidad =  Util.redondear2(iva_utilidad , 2) ;
                  iva_total_prev1 =iva_utilidad - iva_bonificacion;

              }
              // Caso en que la utilidad es cero
              else {
                  // Calculo iva de la bonificacion
                  iva_bonificacion = ofertaEcaDetalle.getBonificacion() * ofertaEcaDetalle.getPorcentaje_iva()  ;//091229

                  // Calculo del iva de total_prev1
                  iva_total_prev1 =  total_prev1 * ofertaEcaDetalle.getPorcentaje_iva();//091229

              }
              iva_bonificacion = Util.redondear2(iva_bonificacion , 2) ;
              ofertaEcaDetalle.setIvaBonificacion(iva_bonificacion);
              iva_total_prev1 = Util.redondear2(iva_total_prev1 , 2) ;
              ofertaEcaDetalle.setIva_total_prev1(iva_total_prev1);




              // Calculo de los ivas de oferta y eca oferta provenientes de Applus

              // oferta = valor venta
              double valor_temporal = Util.redondear2(ofertaEcaDetalle.getOferta() * porcentaje_iva,2);
              ofertaEcaDetalle.setIva_oferta(valor_temporal);
              ofertaEcaDetalle.setOferta_mas_iva(ofertaEcaDetalle.getOferta() + valor_temporal);

              // eca_oferta = eca valor venta
              valor_temporal = Util.redondear2(ofertaEcaDetalle.getEca_oferta() * porcentaje_iva,2);
              ofertaEcaDetalle.setIva_eca_oferta(valor_temporal);
              ofertaEcaDetalle.setEca_oferta_mas_iva(ofertaEcaDetalle.getEca_oferta() + valor_temporal);


              // Calculo Provintegral

              double porcentaje_comision_provintegral = ofertaEcaDetalle.getPorcentaje_comision_provintegral();
              double comision_provintegral = Util.redondear2(total_prev1*porcentaje_comision_provintegral , 2) ;
              ofertaEcaDetalle.setComision_provintegral(comision_provintegral);
              
              
              double iva_comision_provintegral = 0.00;
              if(ofertaEcaDetalle.getUtilidad() != 0) {
                  iva_comision_provintegral = Util.redondear2(utilidad * porcentaje_comision_provintegral * porcentaje_iva , 2);
              }
              else {
                  iva_comision_provintegral = Util.redondear2(comision_provintegral * porcentaje_iva , 2);
              }
              ofertaEcaDetalle.setIva_comision_provintegral(iva_comision_provintegral);


              // Calculo de Comision Applus completa. Este porcentaje inicialmente contiene una parte de Applus y
              // una parte para Factoring Fintra.

              //Calculo Factoring fintra

              // Corrige porcentaje factoring fintra leido de archivo para cada detalle
              ofertaEcaDetalle.setPorcentaje_factoring_fintra(totalOferta.getPorcentaje_factoring_fintra());

              double porcentaje_factoring_fintra = ofertaEcaDetalle.getPorcentaje_factoring_fintra();
              double comision_factoring_fintra = Util.redondear2( total_prev1*porcentaje_factoring_fintra , 2);
              ofertaEcaDetalle.setComision_factoring_fintra(comision_factoring_fintra);

              double iva_comision_factoring_fintra = Util.redondear2(comision_factoring_fintra * porcentaje_iva , 2);
              ofertaEcaDetalle.setIva_comision_factoring_fintra(iva_comision_factoring_fintra);

              /*
               * Metodo inicialmente implementado
              // Calculo Applus
              // Se descuenta el porcentaje de Factoring fintra

              double porcentaje_comision_applus = ofertaEcaDetalle.getPorcentaje_comision_applus() -
                                                  porcentaje_factoring_fintra;
              ofertaEcaDetalle.setPorcentaje_comision_applus(porcentaje_comision_applus);
              double comision_applus       = Util.redondear2( total_prev1 * porcentaje_comision_applus , 2);
              ofertaEcaDetalle.setComision_applus(comision_applus);

              double iva_comision_applus   = Util.redondear2( comision_applus * porcentaje_iva , 2);
              ofertaEcaDetalle.setIva_comision_applus(iva_comision_applus);
              */


              // Calculo Fintra


              double porcentaje_comision_fintra = ofertaEcaDetalle.getPorcentaje_comision_fintra();
              double comision_fintra            = Util.redondear2( total_prev1 *  porcentaje_comision_fintra , 2);
              ofertaEcaDetalle.setComision_fintra(comision_fintra);

              double iva_comision_fintra = Util.redondear2( comision_fintra * porcentaje_iva , 2);
              ofertaEcaDetalle.setIva_comision_fintra(iva_comision_fintra);



              // CALCULO COMISION ECA
              
              // Determina el valor base para el calculo de la comision
              // Se utiliza oferta o eca_oferta: Valores proporcionados por AP+

              double valor_base_comision_eca = 0.00;
              
              if(ofertaEcaDetalle.getEstudio_economico().equalsIgnoreCase("APPLUS NORCONTROL")){
                  // UTILIZA EC_OFERTA    (no contiene eca)
                  valor_base_comision_eca = ofertaEcaDetalle.getOferta();
              }
              else if (ofertaEcaDetalle.getEstudio_economico().equalsIgnoreCase("CONSORCIO ECA-APPLUS-FINTRAVALORES")){
                  // UTILIZA EC_ECA_OFERTA  (contiene eca)
                  valor_base_comision_eca = ofertaEcaDetalle.getEca_oferta();
              }              
              valor_base_comision_eca = Util.redondear2(valor_base_comision_eca, 0);
              

              double porcentaje_comision_eca = ofertaEcaDetalle.getPorcentaje_comision_eca();
              double comision_eca            = Util.redondear2( valor_base_comision_eca * porcentaje_comision_eca , 2 );
              ofertaEcaDetalle.setComision_eca(comision_eca);

              
              double iva_comision_eca = 0.00;
              if(ofertaEcaDetalle.getUtilidad() != 0) {
                  double u_oferta_incrementada = ( 1 + ( (valor_base_comision_eca - total_prev1)/total_prev1 ) ) * utilidad ;
                  iva_comision_eca = Util.redondear2(u_oferta_incrementada * porcentaje_comision_eca * porcentaje_iva , 2);
              }
              else {
                  //iva_comision_eca = Util.redondear2(comision_provintegral * porcentaje_iva , 2);//20100127
                  iva_comision_eca = Util.redondear2(comision_eca * porcentaje_iva , 2);//20100127

              }
              ofertaEcaDetalle.setIva_comision_eca(iva_comision_eca);



              // CALCULO COMISION AP+

              // (Oferta: no contiene eca,  Eca oferta: contiene eca)
              // Por calcularse por diferencias el porcentaje de comision AP+ sigue seteado en el objeto ofertaEcaDetalle = 0


              double valor_base = 0.00;
              if(ofertaEcaDetalle.getEsquema_comision().equalsIgnoreCase("MODELO_NUEVO")){
                  if(ofertaEcaDetalle.getEstudio_economico().equalsIgnoreCase("APPLUS NORCONTROL")){
                      valor_base = ofertaEcaDetalle.getOferta();
                  }
                  else if (ofertaEcaDetalle.getEstudio_economico().equalsIgnoreCase("CONSORCIO ECA-APPLUS-FINTRAVALORES")){
                      valor_base = ofertaEcaDetalle.getEca_oferta();
                  }
              }
              else if(ofertaEcaDetalle.getEsquema_comision().equalsIgnoreCase("MODELO_ANTERIOR")){
                  if(ofertaEcaDetalle.getEstudio_economico().equalsIgnoreCase("APPLUS NORCONTROL")){
                      valor_base = ofertaEcaDetalle.getOferta();
                  }
                  else if (ofertaEcaDetalle.getEstudio_economico().equalsIgnoreCase("CONSORCIO ECA-APPLUS-FINTRAVALORES")){
                      valor_base = ofertaEcaDetalle.getEca_oferta();
                  }
              }
              else {
                  valor_base = ofertaEcaDetalle.getEca_oferta();
              }
              valor_base = Util.redondear2(valor_base, 0);

              double comision_applus = valor_base - comision_eca - comision_factoring_fintra - comision_provintegral -
                                       comision_fintra - ofertaEcaDetalle.getTotal_prev1_applus();

              comision_applus        = Util.redondear2( comision_applus , 2);
              ofertaEcaDetalle.setComision_applus(comision_applus);


              
              
              double iva_comision_applus = 0.00;
              if(ofertaEcaDetalle.getUtilidad() != 0) {
                  double u_oferta_incrementada = ( 1 + ( (valor_base_comision_eca - total_prev1)/total_prev1 ) ) * utilidad ;
                  iva_comision_applus = Util.redondear2(u_oferta_incrementada * porcentaje_iva - iva_bonificacion - iva_total_prev1 - iva_comision_provintegral - iva_comision_eca , 2);
              }
              else {
                  iva_comision_applus = Util.redondear2(comision_applus * porcentaje_iva , 2);
              }
            
              
           
              ofertaEcaDetalle.setIva_comision_applus(iva_comision_applus);
              



              double eca_oferta_calculada  = total_prev1 + comision_factoring_fintra + comision_applus +
                                             comision_provintegral + comision_fintra + comision_eca ;

              ofertaEcaDetalle.setEca_oferta_calculada(eca_oferta_calculada);





              // Calculo de financiacion

              double financiacion_fintra = 0;

              String prefijo =   ofertaEcaDetalle.getNum_os().substring(0,2);
              if (prefijo.equalsIgnoreCase("VA")){

                  financiacion_fintra   = total_prev1 + iva_total_prev1;
                  financiacion_fintra   = Util.redondear2((financiacion_fintra/10),2)*10;//090703

                  ofertaEcaDetalle.setFinanciacion_fintra(financiacion_fintra);
                  double cuota_pago  = financiacion_fintra;
                  ofertaEcaDetalle.setCuota_pago(cuota_pago);

                  double total_financiacion = financiacion_fintra;
                  ofertaEcaDetalle.setTotal_financiacion(total_financiacion);

              }
              else {

                  financiacion_fintra   = total_prev1 + comision_factoring_fintra + comision_applus + comision_provintegral + comision_fintra + comision_eca +
                                          iva_total_prev1 + iva_comision_factoring_fintra +  iva_comision_applus +
                                          iva_comision_provintegral + iva_comision_fintra +
                                          iva_comision_eca;



                  ofertaEcaDetalle.setFinanciacion_fintra(financiacion_fintra);

                  cuotas_reales            = ofertaEcaDetalle.getCuotas_reales();


                  double dtf_puntos = totalOferta.getDtf_puntos();
                  String tipo_puntos= totalOferta.getTipo_puntos();

                  ofertaEcaDetalle.setPuntos_dtf(dtf_puntos);

                  double cuota_pago = 0;


                  if (cuotas_reales == 1){
                      cuota_pago = financiacion_fintra;
                  }
                  else {
                      if (tipo_puntos.equalsIgnoreCase("MAXIMA")){
                        cuota_pago  = Util.redondear2( get_anualidad(financiacion_fintra ,dtf_puntos , cuotas_reales ) , 2);
                      }
                      else {
                        cuota_pago  = Util.redondear2( get_anualidad_dtf(financiacion_fintra , totalOferta.getDtf_semana() + dtf_puntos , cuotas_reales ) , 2);
                      }
                  }

                  ofertaEcaDetalle.setCuota_pago(cuota_pago);

                  double total_financiacion = Util.redondear2(  (cuota_pago * cuotas_reales)/10 , 2) * 10;//090703
                  ofertaEcaDetalle.setTotal_financiacion(total_financiacion);



              } // Fin del calculo de financiacion



              // -------------------------------------------------------------------------------------------------------------
              // -------------------------------------------------------------------------------------------------------------


              // PROCESO DE CALCULO DE FINANCIACION ADICIONAL CUANDO ESQUEMA COMISION = VIEJO y cuando el metodo es automatico

              // Inicializacion de variables

              double ec_porcentaje_iva = 0;
              double ec_porcentaje_comision_provintegral = 0;
              double ec_porcentaje_factoring_fintra = 0;
              double ec_porcentaje_comision_applus = 0;
              double ec_porcentaje_comision_fintra = 0;
              double ec_porcentaje_comision_eca = 0;
              double ec_eca_oferta = 0;
              double ec_oferta = 0;
              double ec_valor_mat = 0;
              double ec_comision_applus = 0;
              double ec_comision_provintegral = 0;
              double ec_comision_factoring_fintra = 0;
              double ec_comision_fintra = 0;
              double ec_comision_eca = 0;
              double ec_financiacion_fintra = 0;
              double ec_iva_valor_mat = 0;
              double ec_iva_comision_applus = 0;
              double ec_iva_comision_factoring_fintra  = 0;
              double ec_iva_comision_fintra = 0;
              double ec_iva_comision_provintegral = 0;
              double ec_iva_comision_eca = 0;
              double ec_total_comisiones = 0;
              double ec_iva_total = 0;

              double ec_cuota_pago = 0;
              double ec_total_financiacion = 0;

              // valores traidos de ap+
              ec_oferta     = ofertaEcaDetalle.getOferta();         // No Contiene la comision eca
              ec_eca_oferta = ofertaEcaDetalle.getEca_oferta();     // Contiene la comision eca

              // Proceso para el metodo viejo : Determinacion de porcentajes y valor material
              if (ofertaEcaDetalle.getEsquema_financiacion().equalsIgnoreCase("VIEJO")){

                  ec_porcentaje_iva = ofertaEcaDetalle.getEc_porcentaje_iva();
                  // distribucion de la parte de applus   (modelo anterior =  10-x:ap+  x:porcentaje factoring 1:provintegral ,   modelo nuevo =  10:ap+ y 1:provintegral)
                  ec_porcentaje_factoring_fintra      = ofertaEcaDetalle.getEc_porcentaje_factoring_fintra();
                  ec_porcentaje_comision_applus       = ofertaEcaDetalle.getEc_porcentaje_comision_applus() - ec_porcentaje_factoring_fintra;
                  ec_porcentaje_comision_provintegral = ofertaEcaDetalle.getEc_porcentaje_comision_provintegral();
                  // distribucion de la parte de fintra   (modelo anterior =  0 ,   modelo nuevo =  2)
                  ec_porcentaje_comision_fintra       = ofertaEcaDetalle.getEc_porcentaje_comision_fintra();
                  // distribucion de la parte de eca      (modelo anterior = 12 ,  modelo nuevo = 10)
                  ec_porcentaje_comision_eca          = ofertaEcaDetalle.getEc_porcentaje_comision_eca();

                  // Calculo de valor material. Se toma para ambos casos

                  if(ofertaEcaDetalle.getEstudio_economico().equalsIgnoreCase("APPLUS NORCONTROL")){
                      // UTILIZA EC_OFERTA    (no contiene eca)
                      // valor material que es la base del calculo de comisiones
                      ec_valor_mat = ofertaEcaDetalle.getTotal_prev1_applus();
                      // ec_valor_mat  = ec_oferta / ( (1+ec_porcentaje_factoring_fintra + ec_porcentaje_comision_applus + ec_porcentaje_comision_provintegral + ec_porcentaje_comision_fintra) * (1 + ec_porcentaje_comision_eca) ) ;
                      ec_valor_mat  = Util.redondear2(ec_valor_mat, 0);
                  }
                  else if (ofertaEcaDetalle.getEstudio_economico().equalsIgnoreCase("CONSORCIO ECA-APPLUS-FINTRAVALORES")){
                      // UTILIZA EC_ECA_OFERTA  (contiene eca)
                      // valor material que es la base del calculo de comisiones
                      ec_valor_mat = ofertaEcaDetalle.getTotal_prev1_applus();
                      // ec_valor_mat = ec_eca_oferta /( (1+ec_porcentaje_factoring_fintra + ec_porcentaje_comision_applus + ec_porcentaje_comision_provintegral + ec_porcentaje_comision_fintra) * (1 + ec_porcentaje_comision_eca) ) ;
                      ec_valor_mat = Util.redondear2(ec_valor_mat, 0);
                  }
              }
              else {
                 // Proceso para el metodo automatico : Determinacion de porcentajes y valor material

                  ec_porcentaje_iva                   = ofertaEcaDetalle.getPorcentaje_iva();
                  ec_porcentaje_factoring_fintra      = ofertaEcaDetalle.getPorcentaje_factoring();
                  ec_porcentaje_comision_applus       = ofertaEcaDetalle.getPorcentaje_comision_applus() - ec_porcentaje_factoring_fintra;
                  ec_porcentaje_comision_provintegral = ofertaEcaDetalle.getPorcentaje_comision_provintegral();
                  ec_porcentaje_comision_fintra       = ofertaEcaDetalle.getPorcentaje_comision_fintra();
                  ec_porcentaje_comision_eca          = ofertaEcaDetalle.getPorcentaje_comision_eca();

                  // ec_valor_mat = ec_eca_oferta /( (1+ec_porcentaje_factoring_fintra + ec_porcentaje_comision_applus + ec_porcentaje_comision_provintegral + ec_porcentaje_comision_fintra) * (1 + ec_porcentaje_comision_eca) ) ;
                  ec_valor_mat = ofertaEcaDetalle.getTotal_prev1_applus();
                  ec_valor_mat = Util.redondear2(ec_valor_mat, 0);

              }

              // Calculo de comisiones , ivas y financiacion

              ec_comision_applus           = Util.redondear2(ec_valor_mat * ec_porcentaje_comision_applus,2);
              ec_comision_factoring_fintra = Util.redondear2(ec_valor_mat * ec_porcentaje_factoring_fintra,2);
              ec_comision_provintegral     = Util.redondear2(ec_valor_mat * ec_porcentaje_comision_provintegral,2);
              ec_comision_fintra           = Util.redondear2(ec_valor_mat * ec_porcentaje_comision_fintra,2);
              double valor                 = ec_valor_mat + ec_comision_applus + ec_comision_provintegral + ec_comision_factoring_fintra + ec_comision_fintra;
              ec_comision_eca              = ec_eca_oferta - valor;

              // calculo de los ivas
              
              //inicio de 090707
              if(totalOferta.getEstudio_economico().equalsIgnoreCase("APPLUS NORCONTROL")){
                  ec_iva_total = Util.redondear2(ec_oferta * ec_porcentaje_iva, 2);
              }else{
                  ec_iva_total = Util.redondear2(ec_eca_oferta * ec_porcentaje_iva, 2);
              }              
              //fin de 090707
              
              //ec_iva_total = Util.redondear2(ec_oferta * ec_porcentaje_iva, 2);//090707
              ec_iva_valor_mat                 = Util.redondear2(ec_valor_mat * ec_porcentaje_iva,2);
              ec_iva_comision_applus           = Util.redondear2(ec_comision_applus * ec_porcentaje_iva,2);
              ec_iva_comision_factoring_fintra = Util.redondear2(ec_comision_factoring_fintra * ec_porcentaje_iva,2);
              ec_iva_comision_fintra           = Util.redondear2(ec_comision_fintra * ec_porcentaje_iva,2);
              ec_iva_comision_provintegral     = Util.redondear2(ec_comision_provintegral * ec_porcentaje_iva,2);

              valor                            = ec_iva_valor_mat + ec_iva_comision_applus + ec_iva_comision_factoring_fintra +
                                                 ec_iva_comision_fintra + ec_iva_comision_provintegral;
              ec_iva_comision_eca              = ec_iva_total - valor;

              // Calculo de financiacion

              ec_financiacion_fintra = 0;

              prefijo =   ofertaEcaDetalle.getNum_os().substring(0,2);
              if (prefijo.equalsIgnoreCase("VA")){

                  ec_financiacion_fintra   = ec_valor_mat + ec_iva_valor_mat;
                  ec_financiacion_fintra   = Util.redondear2((ec_financiacion_fintra/10),2)*10;//090703

                  ofertaEcaDetalle.setEc_financiacion_fintra(ec_financiacion_fintra);
                  ec_cuota_pago  = ec_financiacion_fintra;
                  ofertaEcaDetalle.setEc_cuota_pago(ec_cuota_pago);

                  ec_total_financiacion = ec_financiacion_fintra;
                  ofertaEcaDetalle.setEc_total_financiacion(ec_total_financiacion);


              }
              else {
                  
                  //inicio de 090707
                  if(totalOferta.getEstudio_economico().equalsIgnoreCase("APPLUS NORCONTROL")){
                        ec_financiacion_fintra      = ec_oferta + ec_iva_total;
                  }else{
                        ec_financiacion_fintra      = ec_eca_oferta + ec_iva_total;
                  }                  
                  //fin de 090707
                  
                  //ec_financiacion_fintra      = ec_oferta + ec_iva_total;//090707

                  cuotas_reales           = ofertaEcaDetalle.getCuotas_reales();
                  double dtf_semana           = ofertaEcaDetalle.getDtf_semana();

                  String esquema_financiacion = ofertaEcaDetalle.getEsquema_financiacion();
                  String regulacion           = ofertaEcaDetalle.getTipo_identificacion();

                  // Busqueda de ano y trimestre de la financiacion

                  double dtf_puntos           = totalOferta.getDtf_puntos();
                  String tipo_puntos          = totalOferta.getTipo_puntos();

                  ofertaEcaDetalle.setPuntos_dtf(dtf_puntos);

                  //.out.println("2_____-ec_financiacion_fintra"+ec_financiacion_fintra+"dtf_puntos"+dtf_puntos+"cuotas_reales"+ cuotas_reales+"tipo_puntos"+tipo_puntos);
                  //.out.println("2dtf_semana"+dtf_semana+"clase_dtf"+clase_dtf+"dtf_puntos"+dtf_puntos);
                  String extipo_puntos=tipo_puntos;      
                  if (clase_dtf.startsWith("DTF")){//090907
                      tipo_puntos="NO MAXIMA";
                  }

                  if (cuotas_reales == 1) {
                      ec_cuota_pago    = ec_financiacion_fintra;
                  }
                  else {
                      if (tipo_puntos.equalsIgnoreCase("MAXIMA")){
                        ec_cuota_pago  = Util.redondear2( get_anualidad(ec_financiacion_fintra ,dtf_puntos , cuotas_reales ) , 2);
                      }
                      else {
                        if (clase_dtf.startsWith("DTF")){//090907
                            dtf_puntos=Double.parseDouble(clase_dtf.substring(4,clase_dtf.length()));
                        }
                        //.out.println("_____-ec_financiacion_fintra"+ec_financiacion_fintra+"dtf_puntos"+dtf_puntos+"cuotas_reales"+ cuotas_reales+"tipo_puntos"+tipo_puntos);
                        //.out.println("dtf_semana"+dtf_semana+"clase_dtf"+clase_dtf+"dtf_puntos"+dtf_puntos);
                        
                        ec_cuota_pago  = Util.redondear2( get_anualidad_dtf(ec_financiacion_fintra , dtf_semana + dtf_puntos , cuotas_reales ) , 2);
                      }
                  }
                  tipo_puntos=extipo_puntos;

                  ec_total_financiacion = Util.redondear2(  (ec_cuota_pago * cuotas_reales)/10 , 2) * 10;//090703
              }
              // Actualizando objeto

              ofertaEcaDetalle.setEc_porcentaje_iva(ec_porcentaje_iva);
              ofertaEcaDetalle.setEc_porcentaje_comision_provintegral(ec_porcentaje_comision_provintegral);
              ofertaEcaDetalle.setEc_porcentaje_factoring_fintra(ec_porcentaje_factoring_fintra);
              ofertaEcaDetalle.setEc_porcentaje_comision_applus(ec_porcentaje_comision_applus);
              ofertaEcaDetalle.setEc_porcentaje_comision_fintra(ec_porcentaje_comision_fintra);
              ofertaEcaDetalle.setEc_porcentaje_comision_eca(ec_porcentaje_comision_eca);
              ofertaEcaDetalle.setEc_valor_mat(ec_valor_mat);
              ofertaEcaDetalle.setEc_comision_applus(ec_comision_applus);
              ofertaEcaDetalle.setEc_comision_provintegral(ec_comision_provintegral);
              ofertaEcaDetalle.setEc_comision_factoring_fintra(ec_comision_factoring_fintra);
              ofertaEcaDetalle.setEc_comision_fintra(ec_comision_fintra);
              ofertaEcaDetalle.setEc_comision_eca(ec_comision_eca);
              ofertaEcaDetalle.setEc_iva_valor_mat(ec_iva_valor_mat);
              ofertaEcaDetalle.setEc_iva_comision_applus(ec_iva_comision_applus);
              ofertaEcaDetalle.setEc_iva_comision_factoring_fintra(ec_iva_comision_factoring_fintra);
              ofertaEcaDetalle.setEc_iva_comision_fintra(ec_iva_comision_fintra);
              ofertaEcaDetalle.setEc_iva_comision_provintegral(ec_iva_comision_provintegral);
              ofertaEcaDetalle.setEc_iva_comision_eca(ec_iva_comision_eca);
              ofertaEcaDetalle.setEc_financiacion_fintra(ec_financiacion_fintra);
              ofertaEcaDetalle.setEc_cuota_pago(ec_cuota_pago);
              ofertaEcaDetalle.setEc_total_financiacion(ec_total_financiacion);


              // Fin de proceso


              // -------------------------------------------------------------------------------------------------------------
              // -------------------------------------------------------------------------------------------------------------
            }

        }
    } // Fin de listaOfertas




 public Double  getPorcentajeFactoringFintra(String clase_dtf, int cuotas_reales)throws SQLException{
     return applusDao.getPorcentajeFactoringFintra(clase_dtf, cuotas_reales);
 }




    public void setReliquidarOfertas(Model model, String orden,  String s_porcentaje_comision_applus,
                                     String s_porcentaje_comision_provintegral ,
                                     String s_porcentaje_comision_fintra , String s_porcentaje_comision_eca ,
                                     String s_porcentaje_iva,
                                     String s_porcentaje_factoring_fintra,
                                     String s_puntos_dtf)throws SQLException {

        int id_orden = Integer.parseInt(orden.trim());

        double porcentaje_comision_applus = Double.parseDouble(s_porcentaje_comision_applus.trim())/100;
        double porcentaje_factoring_fintra = Double.parseDouble(s_porcentaje_factoring_fintra.trim())/100;
        double porcentaje_comision_provintegral = Double.parseDouble(s_porcentaje_comision_provintegral.trim())/100;
        double porcentaje_comision_fintra = Double.parseDouble(s_porcentaje_comision_fintra.trim())/100;
        double porcentaje_comision_eca = Double.parseDouble(s_porcentaje_comision_eca.trim())/100;
        double porcentaje_iva = Double.parseDouble(s_porcentaje_iva.trim())/100;
        double puntos_dtf = Double.parseDouble(s_puntos_dtf.trim());


        java.util.Date date = new java.util.Date();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        String fecha_financiacion = sdf.format(date);

        String f_facturado_cliente=applusDao.buscaF_facturado_cliente(orden);

        if (!(f_facturado_cliente.equals("0099-01-01")) ){
            fecha_financiacion=f_facturado_cliente;
        }



        List listaOfertaEcaDetalle = model.applusService.getOfertaEcaDetalle();
        if(listaOfertaEcaDetalle.size()!=0) {

            int i=0;
            Iterator it = listaOfertaEcaDetalle.iterator();
            while (it.hasNext()) {

              i++;

              OfertaEcaDetalle  ofertaEcaDetalle = (OfertaEcaDetalle)it.next();

              ofertaEcaDetalle.setFecha_financiacion(fecha_financiacion);

              // Calculo Totalprev1

              double total_prev1_mat = ofertaEcaDetalle.getTotal_prev1_mat();
              double total_prev1_mob = ofertaEcaDetalle.getTotal_prev1_mob();
              double total_prev1_otr = ofertaEcaDetalle.getTotal_prev1_otr();
              double administracion  = ofertaEcaDetalle.getAdministracion();
              double imprevisto      = ofertaEcaDetalle.getImprevisto();
              double utilidad        = ofertaEcaDetalle.getUtilidad();

              double total_prev1     = Util.redondear2( total_prev1_mat + total_prev1_mob + total_prev1_otr +
                                                        administracion + imprevisto + utilidad , 2);
              ofertaEcaDetalle.setTotal_prev1(total_prev1);

              double iva_total_prev1       = Util.redondear2( total_prev1 * porcentaje_iva , 2);
              ofertaEcaDetalle.setIva_total_prev1(iva_total_prev1);


              // Iva
              ofertaEcaDetalle.setPorcentaje_iva(porcentaje_iva);




              // Calcula el iva de la bonificacion y del iva total_prev1

              double iva_bonificacion = 0.00;
              double iva_utilidad = 0.00;


              // Caso en el que la utilidad es diferente de cero
              if(ofertaEcaDetalle.getUtilidad() != 0) {

                  // Calculo iva de la bonificacion
                  iva_bonificacion = ( (ofertaEcaDetalle.getUtilidad() * ofertaEcaDetalle.getBonificacion()) / total_prev1 ) * ofertaEcaDetalle.getPorcentaje_iva() ;

                  // Calculo del iva de total_prev1

                  iva_utilidad =  ofertaEcaDetalle.getUtilidad() * ofertaEcaDetalle.getPorcentaje_iva();
                  iva_utilidad =  Util.redondear2(iva_utilidad , 2) ;
                  iva_total_prev1 =iva_utilidad - iva_bonificacion;

              }
              // Caso en que la utilidad es cero
              else {
                  // Calculo iva de la bonificacion
                  iva_bonificacion = ofertaEcaDetalle.getIvaBonificacion() * ofertaEcaDetalle.getPorcentaje_iva() / 100 ;

                  // Calculo del iva de total_prev1
                  iva_total_prev1 =  total_prev1 * ofertaEcaDetalle.getPorcentaje_iva()/ 100;

              }
              iva_bonificacion = Util.redondear2(iva_bonificacion , 2) ;
              ofertaEcaDetalle.setIvaBonificacion(iva_bonificacion);
              iva_total_prev1 = Util.redondear2(iva_total_prev1 , 2) ;
              ofertaEcaDetalle.setIva_total_prev1(iva_total_prev1);




              // Calculo Factoring fintra


              double comision_factoring_fintra       = Util.redondear2( total_prev1 * porcentaje_factoring_fintra , 2);
              ofertaEcaDetalle.setComision_factoring_fintra(comision_factoring_fintra);

              double iva_comision_factoring_fintra   = Util.redondear2( comision_factoring_fintra * porcentaje_iva , 2);
              ofertaEcaDetalle.setIva_comision_factoring_fintra(iva_comision_factoring_fintra);
              ofertaEcaDetalle.setPorcentaje_factoring_fintra(porcentaje_factoring_fintra);


              // Calculo de los ivas de oferta y eca oferta provenientes de Applus

              // oferta = valor venta
              double valor_temporal = Util.redondear2(ofertaEcaDetalle.getOferta() * porcentaje_iva,2);
              ofertaEcaDetalle.setIva_oferta(valor_temporal);
              ofertaEcaDetalle.setOferta_mas_iva(ofertaEcaDetalle.getOferta() + valor_temporal);

              // eca_oferta = eca valor venta
              valor_temporal = Util.redondear2(ofertaEcaDetalle.getEca_oferta() * porcentaje_iva,2);
              ofertaEcaDetalle.setIva_eca_oferta(valor_temporal);
              ofertaEcaDetalle.setEca_oferta_mas_iva(ofertaEcaDetalle.getEca_oferta() + valor_temporal);


              // Calculo Provintegral

              double comision_provintegral = Util.redondear2( total_prev1*porcentaje_comision_provintegral , 2);
              ofertaEcaDetalle.setComision_provintegral(comision_provintegral);
              ofertaEcaDetalle.setPorcentaje_comision_provintegral(porcentaje_comision_provintegral);

              double iva_comision_provintegral = 0.00;
              if(ofertaEcaDetalle.getUtilidad() != 0) {
                  iva_comision_provintegral = Util.redondear2(utilidad * porcentaje_comision_provintegral * porcentaje_iva , 2);
              }
              else {
                  iva_comision_provintegral = Util.redondear2(comision_provintegral * porcentaje_iva , 2);
              }
              ofertaEcaDetalle.setIva_comision_provintegral(iva_comision_provintegral); 
              
              
              
              
              
              
              // Calculo Fintra

              double comision_fintra            = Util.redondear2( total_prev1 *  porcentaje_comision_fintra , 2);
              ofertaEcaDetalle.setComision_fintra(comision_fintra);
              ofertaEcaDetalle.setPorcentaje_comision_fintra(porcentaje_comision_fintra);

              double iva_comision_fintra = 0.00;
              if(ofertaEcaDetalle.getUtilidad() != 0) {
                  iva_comision_fintra = Util.redondear2(utilidad * porcentaje_comision_fintra * porcentaje_iva , 2);
              }
              else {
                  iva_comision_fintra = Util.redondear2(comision_fintra * porcentaje_iva , 2);
              }
              ofertaEcaDetalle.setIva_comision_fintra(iva_comision_fintra);


              
              
              // Calculo Eca


              double valor_base_comision_eca = 0.00;
              
              if(ofertaEcaDetalle.getEstudio_economico().equalsIgnoreCase("APPLUS NORCONTROL")){
                  // UTILIZA EC_OFERTA    (no contiene eca)
                  valor_base_comision_eca = ofertaEcaDetalle.getOferta();
              }
              else if (ofertaEcaDetalle.getEstudio_economico().equalsIgnoreCase("CONSORCIO ECA-APPLUS-FINTRAVALORES")){
                  // UTILIZA EC_ECA_OFERTA  (contiene eca)
                  valor_base_comision_eca = ofertaEcaDetalle.getEca_oferta();
              }              
              valor_base_comision_eca = Util.redondear2(valor_base_comision_eca, 0);
              


              double comision_eca            = Util.redondear2( valor_base_comision_eca * porcentaje_comision_eca , 2 );
              ofertaEcaDetalle.setComision_eca(comision_eca);
              ofertaEcaDetalle.setPorcentaje_comision_eca(porcentaje_comision_eca);
              
              
              double iva_comision_eca = 0.00;
              if(ofertaEcaDetalle.getUtilidad() != 0) {
                  double u_oferta_incrementada = ( 1 + ( (valor_base_comision_eca - total_prev1)/total_prev1 ) ) * utilidad ;
                  iva_comision_eca = Util.redondear2(u_oferta_incrementada * porcentaje_comision_eca * porcentaje_iva , 2);
              }
              else {
                  //iva_comision_eca = Util.redondear2(comision_provintegral * porcentaje_iva , 2);//20100127
                  iva_comision_eca = Util.redondear2(comision_eca * porcentaje_iva , 2);//20100127
              }
              ofertaEcaDetalle.setIva_comision_eca(iva_comision_eca);              
              



              // Calculo Applus


              double comision_applus       = Util.redondear2( total_prev1 * porcentaje_comision_applus , 2);
              ofertaEcaDetalle.setComision_applus(comision_applus);
              ofertaEcaDetalle.setPorcentaje_comision_applus(porcentaje_comision_applus);

              double iva_comision_applus = 0.00;
              if(ofertaEcaDetalle.getUtilidad() != 0) {
                  double u_oferta_incrementada = ( 1 + ( (valor_base_comision_eca - total_prev1)/total_prev1 ) ) * utilidad ;
                  iva_comision_applus = Util.redondear2(u_oferta_incrementada * porcentaje_iva - iva_total_prev1 - iva_comision_provintegral - iva_comision_eca - iva_comision_fintra, 2);
              }
              else {
                  iva_comision_applus = Util.redondear2(comision_applus * porcentaje_iva , 2);
              }

              ofertaEcaDetalle.setIva_comision_applus(iva_comision_applus);




              double eca_oferta_calculada  = total_prev1 +  comision_applus + comision_factoring_fintra + comision_provintegral +
                                             comision_fintra + comision_eca ;
              ofertaEcaDetalle.setEca_oferta_calculada(eca_oferta_calculada);

              
              
              
              
              // Calculo de financiacion

              double financiacion_fintra = 0;
              ofertaEcaDetalle.setPuntos_dtf(puntos_dtf);

              String prefijo =   ofertaEcaDetalle.getNum_os().substring(0,2);
              if (prefijo.equalsIgnoreCase("VA")){

                  financiacion_fintra   = total_prev1 + iva_total_prev1;

                  financiacion_fintra=Util.redondear2((financiacion_fintra/10),2)*10;//090703

                  ofertaEcaDetalle.setFinanciacion_fintra(financiacion_fintra);
                  double cuota_pago  = financiacion_fintra;
                  ofertaEcaDetalle.setCuota_pago(cuota_pago);

                  double total_financiacion = financiacion_fintra;
                  ofertaEcaDetalle.setTotal_financiacion(total_financiacion);



              }
              else {

                  financiacion_fintra   = total_prev1 + comision_applus + comision_factoring_fintra + comision_provintegral + comision_fintra + comision_eca +
                                          iva_total_prev1 + iva_comision_applus + iva_comision_factoring_fintra +
                                          iva_comision_provintegral + iva_comision_fintra +
                                          iva_comision_eca;
                  ofertaEcaDetalle.setFinanciacion_fintra(financiacion_fintra);

                  int cuotas_reales   = ofertaEcaDetalle.getCuotas_reales();
                  double porcentaje_factoring  = ofertaEcaDetalle.getPorcentaje_factoring();
                  double dtf_semana = ofertaEcaDetalle.getDtf_semana();


                  String esquema_financiacion = ofertaEcaDetalle.getEsquema_financiacion();
                  String regulacion = ofertaEcaDetalle.getTipo_identificacion();

                  // Busqueda de ano y trimestre de la financiacion

                  String ano = fecha_financiacion.substring(0,4);
                  String mes = fecha_financiacion.substring(5,7);
                  String trimestre = getTrimestre(mes);

                  PuntosFinanciacion puntosFinanciacion = getPuntosFinanciacion(esquema_financiacion, regulacion, financiacion_fintra, ano,
                                                          trimestre, cuotas_reales);

                  double dtf_puntos = puntosFinanciacion.getPuntos();
                  String tipo_puntos= puntosFinanciacion.getTipo();

                  ofertaEcaDetalle.setPuntos_dtf(dtf_puntos);

                  double cuota_pago = 0;

                  if (tipo_puntos.equalsIgnoreCase("MAXIMA")){
                    cuota_pago  = Util.redondear2( get_anualidad(financiacion_fintra ,dtf_puntos , cuotas_reales ) , 2);
                  }
                  else {
                    cuota_pago  = Util.redondear2( get_anualidad_dtf(financiacion_fintra , dtf_semana + dtf_puntos , cuotas_reales ) , 2);
                  }

                  ofertaEcaDetalle.setCuota_pago(cuota_pago);

                  double total_financiacion = Util.redondear2(  (cuota_pago * cuotas_reales)/10 , 2) * 10;//090703
                  ofertaEcaDetalle.setTotal_financiacion(total_financiacion);

              }

            }

        }
    } // fin de setReliquidarOfertas



    public void totalizaOferta(List listaOfertaEcaDetalle, TotalOferta totalOferta, String fecha_financiacion)throws SQLException {

        // Inicializa de las variables del objeto

        totalOferta.setTotalOferta();


        int i=0;
        Iterator it = listaOfertaEcaDetalle.iterator();
        while (it.hasNext()) {
            i++;
            OfertaEcaDetalle  ofertaEcaDetalle = (OfertaEcaDetalle)it.next();

            if(i==1){
                inicializaVariables(ofertaEcaDetalle,totalOferta);      // Paso 1
            }

            determinaValorBase_metodoAutomatico( ofertaEcaDetalle,totalOferta);           // Paso 2
            determinaValorBase_metodoApplus    ( ofertaEcaDetalle,totalOferta);
        }

        PuntosFinanciacion puntosFinanciacion = new PuntosFinanciacion() ;
        determinaValorFinanciados_metodoAutomatico(totalOferta, fecha_financiacion, puntosFinanciacion);    // Paso 3A
        determinaValorFinanciados_metodoApplus    (totalOferta, fecha_financiacion, puntosFinanciacion);    // Paso 3B


    }



public void inicializaVariables(OfertaEcaDetalle ofertaEcaDetalle,TotalOferta totalOferta) {

    // PASO 1   INICIALIZACION DE VARIABLES


    totalOferta.setNum_os(ofertaEcaDetalle.getNum_os());
    totalOferta.setId_orden(ofertaEcaDetalle.getId_orden());

    // PASO 1A.   INICIALIZA VARIABLES PARA METODO AUTOMATICO , EL STANDARD

    totalOferta.setId_orden(ofertaEcaDetalle.getId_orden());

    // Extrae los porcentajes
    // El porcentaje de comision Applus incluye inicialmente el porcentaje de factoring fintra
    // posteriormente se dividira ese porcentaje, las comisiones y el iva
    totalOferta.setPorcentaje_iva                   ( ofertaEcaDetalle.getPorcentaje_iva() );
    totalOferta.setPorcentaje_comision_provintegral ( ofertaEcaDetalle.getPorcentaje_comision_provintegral() );
    totalOferta.setPorcentaje_factoring_fintra      ( 0.0 );
    totalOferta.setPorcentaje_comision_applus       ( ofertaEcaDetalle.getPorcentaje_comision_applus()  );
    totalOferta.setPorcentaje_comision_fintra       ( ofertaEcaDetalle.getPorcentaje_comision_fintra() );
    totalOferta.setPorcentaje_comision_eca          ( ofertaEcaDetalle.getPorcentaje_comision_eca() );

    // Determina si es VA

    totalOferta.setPrefijo   ( ofertaEcaDetalle.getNum_os().substring(0,2) );

    // Parametros para poder calcular la financiacion
    totalOferta.setCuotas_reales         ( ofertaEcaDetalle.getCuotas_reales() );
    totalOferta.setPorcentaje_factoring  ( ofertaEcaDetalle.getPorcentaje_factoring() );
    totalOferta.setDtf_semana            ( ofertaEcaDetalle.getDtf_semana() );
    totalOferta.setEsquema_financiacion  ( ofertaEcaDetalle.getEsquema_financiacion() );
    totalOferta.setRegulacion            ( ofertaEcaDetalle.getTipo_identificacion() );

    totalOferta.setEstudio_economico     ( ofertaEcaDetalle.getEstudio_economico() );
    totalOferta.setEsquema_comision      ( ofertaEcaDetalle.getEsquema_comision()  );


    // PASO 1B.   INICIALIZA VARIABLES PARA METODO CON CALCULOS QUE USAN OFERTA Y ECA OFERTA

    // Proceso para el metodo viejo : Determinacion de porcentajes y valor material
    if (ofertaEcaDetalle.getEsquema_financiacion().equalsIgnoreCase("VIEJO")){


        // El porcentaje de comision Applus incluye inicialmente el porcentaje de factoring fintra
        // posteriormente se dividira ese porcentaje, las comisiones y el iva

        totalOferta.setEc_porcentaje_iva                   ( ofertaEcaDetalle.getEc_porcentaje_iva() );
        // distribucion de la parte de applus   (modelo anterior =  10-x:ap+  x:porcentaje factoring 1:provintegral ,   modelo nuevo =  10:ap+ y 1:provintegral)
        totalOferta.setEc_porcentaje_factoring_fintra      ( 0.0 );
        totalOferta.setEc_porcentaje_comision_applus       ( ofertaEcaDetalle.getEc_porcentaje_comision_applus()  );
        totalOferta.setEc_porcentaje_comision_provintegral ( ofertaEcaDetalle.getEc_porcentaje_comision_provintegral() );
        // distribucion de la parte de fintra   (modelo anterior =  0 ,   modelo nuevo =  2)
        totalOferta.setEc_porcentaje_comision_fintra       ( ofertaEcaDetalle.getEc_porcentaje_comision_fintra() );
        // distribucion de la parte de eca      (modelo anterior = 12 ,  modelo nuevo = 10)
        totalOferta.setEc_porcentaje_comision_eca          ( ofertaEcaDetalle.getEc_porcentaje_comision_eca() );
    }
    else {
        // Proceso para el metodo automatico : Determinacion de porcentajes y valor material

        // El porcentaje de comision Applus incluye inicialmente el porcentaje de factoring fintra
        // posteriormente se dividira ese porcentaje, las comisiones y el iva

        totalOferta.setEc_porcentaje_iva                   ( ofertaEcaDetalle.getPorcentaje_iva() );
        totalOferta.setEc_porcentaje_factoring_fintra      ( 0.0 );
        totalOferta.setEc_porcentaje_comision_applus       ( ofertaEcaDetalle.getPorcentaje_comision_applus() );
        totalOferta.setEc_porcentaje_comision_provintegral ( ofertaEcaDetalle.getPorcentaje_comision_provintegral() );
        totalOferta.setEc_porcentaje_comision_fintra       ( ofertaEcaDetalle.getPorcentaje_comision_fintra() );
        totalOferta.setEc_porcentaje_comision_eca          ( ofertaEcaDetalle.getPorcentaje_comision_eca() );
    }




}

public void determinaValorBase_metodoAutomatico(OfertaEcaDetalle ofertaEcaDetalle,TotalOferta totalOferta) {

    // El valor base es el valor TotalPrev1 en los metodos automaticos que no toman en cuenta
    // los valores oferta y eca oferta proveniente de APPLUS

    // PASO 2.    DETERMINACION DEL VALOR TOTAL BASE DE CALCULOS.
    // PASO 2A.   DETERMINACION DEL VALOR TOTAL  PARA PROCEDIMIENTO QUE UTILIZARAN LOS VALORES DEL METODO AUTOMATICO STANDARD

    // Suma de todos los valores que componen el total_prev1 de todas las acciones de la orden

    totalOferta.setGranTotal_prev1        ( totalOferta.getGranTotal_prev1() +
                                            ofertaEcaDetalle.getTotal_prev1_mat() +
                                            ofertaEcaDetalle.getTotal_prev1_mob() +
                                            ofertaEcaDetalle.getTotal_prev1_otr() +
                                            ofertaEcaDetalle.getAdministracion()  +
                                            ofertaEcaDetalle.getImprevisto()      +
                                            ofertaEcaDetalle.getUtilidad() );

    // El valor base en el metodo cuando esquema de financiacion = VIEJO y cuando se recalcula
    // el metodo automatico es el valor de oferta y eca oferta.

    // PASO 2B.   DETERMINACION DEL VALOR TOTAL PARA PROCEDIMIENTO QUE UTILIZARA LOS VALORES OFERTA Y ECA OFERTA

    // Tanto para el metodo con esquema financiacion = VIEJO o con el metodo automatico recalculado se toma en cuenta el estudio economico

    // Suma de todas las Ofertas     = valor venta
    totalOferta.setGranOferta             ( totalOferta.getGranOferta()  + ofertaEcaDetalle.getOferta() );
    // Suma de todas las Eca Ofertas = eca valor venta
    totalOferta.setGranEca_oferta         ( totalOferta.getGranEca_oferta() + ofertaEcaDetalle.getEca_oferta() );


}


public void determinaValorBase_metodoApplus(OfertaEcaDetalle ofertaEcaDetalle,TotalOferta totalOferta) {

    // Redondear valores acumulados


    totalOferta.setGranOferta      ( Util.redondear2(totalOferta.getGranOferta(),2) );
    totalOferta.setGranEca_oferta  ( Util.redondear2(totalOferta.getGranEca_oferta (),2) );

    // Determina valor total material en base a valores de oferta y eca oferta acumulados. Es como si fuera el totalprev1
    if (totalOferta.getEsquema_financiacion().equalsIgnoreCase("VIEJO")) {

        if(totalOferta.getEstudio_economico().equalsIgnoreCase("APPLUS NORCONTROL")){
           // UTILIZA EC_OFERTA    (no contiene eca)
        totalOferta.setEc_valor_mat  (  totalOferta.getGranOferta() ) ;
        }
        else if (totalOferta.getEstudio_economico().equalsIgnoreCase("CONSORCIO ECA-APPLUS-FINTRAVALORES")){
            // UTILIZA EC_ECA_OFERTA  (contiene eca)
            totalOferta.setEc_valor_mat  (  totalOferta.getGranEca_oferta() )  ;
        }
    }
    else {
        totalOferta.setEc_valor_mat  (  totalOferta.getGranEca_oferta() )  ;
    }

    totalOferta.setEc_granTotal_prev1 ( Util.redondear2(totalOferta.getEc_valor_mat(), 0) );
    totalOferta.setEc_valor_mat       ( Util.redondear2(totalOferta.getEc_valor_mat(), 0) );


}










public void determinaValorFinanciados_metodoAutomatico(TotalOferta totalOferta,String fecha_financiacion,
                                                       PuntosFinanciacion puntosFinanciacion) throws SQLException {

    // PASO 3.   CALCULOS DE VALOR A FINANCIAR , CUOTAS


    // PASO 3A.  METODO AUTOMATICO UTILIZANDO LOS VALORES STANDARES

    // Redondear valores acumulados


    totalOferta.setGranTotal_prev1 ( Util.redondear2(totalOferta.getGranTotal_prev1(),2) );

    // Calculo de las comisiones
    // La comision factoring fintra esta incluida en la comision applus, ya que el porcentaje de factoring fintra = 0 y el porcentaje applus lo contiene

    totalOferta.setComision_provintegral     ( Util.redondear2( totalOferta.getGranTotal_prev1() * totalOferta.getPorcentaje_comision_provintegral() , 2) );
    totalOferta.setComision_factoring_fintra ( Util.redondear2( totalOferta.getGranTotal_prev1() * totalOferta.getPorcentaje_factoring_fintra() , 2) );
    totalOferta.setComision_fintra           ( Util.redondear2( totalOferta.getGranTotal_prev1() * totalOferta.getPorcentaje_comision_fintra() , 2) );


    // CALCULO COMISION ECA

    // Determina el valor base para el calculo de la comision
    // Se utiliza oferta o eca_oferta: Valores proporcionados por AP+

    double valor_base_comision_eca = 0.00;

    if(totalOferta.getEstudio_economico().equalsIgnoreCase("APPLUS NORCONTROL")){
        valor_base_comision_eca = totalOferta.getGranOferta();
    }
    else if (totalOferta.getEstudio_economico().equalsIgnoreCase("CONSORCIO ECA-APPLUS-FINTRAVALORES")){
        valor_base_comision_eca = totalOferta.getGranEca_oferta();
    }
    valor_base_comision_eca = Util.redondear2(valor_base_comision_eca, 0);


    double porcentaje_comision_eca = totalOferta.getPorcentaje_comision_eca();
    double comision_eca            = Util.redondear2( valor_base_comision_eca * porcentaje_comision_eca , 2 );
    totalOferta.setComision_eca(comision_eca);


    // CALCULO COMISION AP+

    // (Oferta: no contiene eca,  Eca oferta: contiene eca)
    // Por calcularse por diferencias el porcentaje de comision AP+ sigue seteado en el objeto ofertaEcaDetalle = 0


    double valor_base = 0.00;
    if(totalOferta.getEsquema_comision().equalsIgnoreCase("MODELO_NUEVO")){
        if(totalOferta.getEstudio_economico().equalsIgnoreCase("APPLUS NORCONTROL")){
            valor_base = totalOferta.getGranOferta();
        }
    }
    else if(totalOferta.getEsquema_comision().equalsIgnoreCase("MODELO_ANTERIOR")){
        if(totalOferta.getEstudio_economico().equalsIgnoreCase("APPLUS NORCONTROL")){
            valor_base = totalOferta.getGranOferta();
        }
        else if (totalOferta.getEstudio_economico().equalsIgnoreCase("CONSORCIO ECA-APPLUS-FINTRAVALORES")){
            valor_base = totalOferta.getGranEca_oferta();
        }
    }
    else {
        valor_base = totalOferta.getGranEca_oferta();
    }
    valor_base = Util.redondear2(valor_base, 0);

    double comision_applus = valor_base - comision_eca - totalOferta.getComision_factoring_fintra() -
                           - totalOferta.getComision_provintegral() -
                             totalOferta.getComision_fintra() - totalOferta.getGranTotal_prev1();

    comision_applus        = Util.redondear2( comision_applus , 2);
    totalOferta.setComision_applus(comision_applus);



    // Metodos inicialmente definidos
    // totalOferta.setComision_eca              ( Util.redondear2((totalOferta.getGranTotal_prev1()       + totalOferta.getComision_applus() + totalOferta.getComision_factoring_fintra() +
    //                                                             totalOferta.getComision_provintegral() + totalOferta.getComision_fintra() ) * totalOferta.getPorcentaje_comision_eca() , 2 ) );
    // totalOferta.setComision_applus           ( Util.redondear2( totalOferta.getGranTotal_prev1() * totalOferta.getPorcentaje_comision_applus() , 2) );




    totalOferta.setGranEca_oferta_calculada  ( totalOferta.getGranTotal_prev1()       + totalOferta.getComision_applus() + totalOferta.getComision_factoring_fintra() +
                                               totalOferta.getComision_provintegral() + totalOferta.getComision_fintra() + totalOferta.getComision_eca() ) ;

    // Calculo de los ivas

    totalOferta.setIva_granTotal_prev1           ( Util.redondear2(totalOferta.getGranTotal_prev1() * totalOferta.getPorcentaje_iva() , 2) ) ;

    totalOferta.setIvaGranOferta                 ( Util.redondear2(totalOferta.getGranOferta() * totalOferta.getPorcentaje_iva(),2) );
    totalOferta.setGranOferta_mas_iva            ( totalOferta.getGranOferta() + totalOferta.getIvaGranOferta() );

    totalOferta.setIvaGranEca_oferta             ( Util.redondear2(totalOferta.getGranEca_oferta() * totalOferta.getPorcentaje_iva(),2) );
    totalOferta.setGranEca_oferta_mas_iva        ( totalOferta.getGranEca_oferta () + totalOferta.getIvaGranEca_oferta() );

    totalOferta.setIva_comision_provintegral     ( Util.redondear2( totalOferta.getComision_provintegral() * totalOferta.getPorcentaje_iva() , 2) );
    totalOferta.setIva_comision_factoring_fintra ( Util.redondear2( totalOferta.getComision_factoring_fintra() * totalOferta.getPorcentaje_iva() , 2) );
    totalOferta.setIva_comision_applus           ( Util.redondear2( totalOferta.getComision_applus() * totalOferta.getPorcentaje_iva() , 2) );
    totalOferta.setIva_comision_fintra           ( Util.redondear2( totalOferta.getComision_fintra() * totalOferta.getPorcentaje_iva() , 2) );
    totalOferta.setIva_comision_eca              ( Util.redondear2( totalOferta.getComision_eca() * totalOferta.getPorcentaje_iva() , 2) );


    // Determina el valor a financiar

    if (totalOferta.getPrefijo().equalsIgnoreCase("VA")){

      totalOferta.setFinanciacion_fintra    ( totalOferta.getGranTotal_prev1() + totalOferta.getIva_granTotal_prev1() );
      totalOferta.setFinanciacion_fintra    ( Util.redondear2((totalOferta.getFinanciacion_fintra()/10),2)*10 ) ;//090703

      totalOferta.setCuota_pago             ( totalOferta.getFinanciacion_fintra() );
      totalOferta.setTotal_financiacion     ( totalOferta.getFinanciacion_fintra() );

    }
    else {

      // Financiacion fintra es el valor antes de financiar
      totalOferta.setFinanciacion_fintra    ( totalOferta.getGranTotal_prev1() + totalOferta.getComision_applus() + totalOferta.getComision_factoring_fintra() +
                                              totalOferta.getComision_provintegral() + totalOferta.getComision_fintra() +
                                              totalOferta.getComision_eca() +
                                              totalOferta.getIva_granTotal_prev1() + totalOferta.getIva_comision_applus() +
                                              totalOferta.getIva_comision_factoring_fintra() +
                                              totalOferta.getIva_comision_provintegral() + totalOferta.getIva_comision_fintra() +
                                              totalOferta.getIva_comision_eca() );


      // Busqueda de ano y trimestre de la financiacion

      totalOferta.setAno       ( fecha_financiacion.substring(0,4)  );
      totalOferta.setMes       ( fecha_financiacion.substring(5,7)  );
      totalOferta.setTrimestre ( getTrimestre(totalOferta.getMes()) );

      try {
          puntosFinanciacion = getPuntosFinanciacion(totalOferta.getEsquema_financiacion(),
                                                     totalOferta.getRegulacion(),
                                                     totalOferta.getFinanciacion_fintra(), totalOferta.getAno(),
                                                     totalOferta.getTrimestre(), totalOferta.getCuotas_reales() );

          totalOferta.setPuntosFinanciacion(puntosFinanciacion);

      }catch(Exception e){
          puntosFinanciacion.setPuntos(0.0);
          puntosFinanciacion.setTipo("DTF");
          puntosFinanciacion.setClase_dtf("DTF+11");
          totalOferta.setPuntosFinanciacion(puntosFinanciacion);
          System.out.println("error en service::"+e.toString());
          e.printStackTrace();
          throw new SQLException("ERROR EN LA BUSQUEDA DE LA LOCALIZACION DE LOS PUNTOS DE FINANCIACION. \n " + e.getMessage());
      }      


      totalOferta.setDtf_puntos  ( puntosFinanciacion.getPuntos() );
      totalOferta.setTipo_puntos ( puntosFinanciacion.getTipo() );


      if (totalOferta.getCuotas_reales() == 1){
          totalOferta.setCuota_pago ( totalOferta.getFinanciacion_fintra() );
      }
      else {
          if (totalOferta.getTipo_puntos().equalsIgnoreCase("MAXIMA")){
            totalOferta.setCuota_pago  ( Util.redondear2( get_anualidad(totalOferta.getFinanciacion_fintra() ,
                                                                        totalOferta.getDtf_puntos() ,
                                                                        totalOferta.getCuotas_reales() ) , 2) );
          }
          else {
            totalOferta.setCuota_pago  ( Util.redondear2( get_anualidad_dtf(totalOferta.getFinanciacion_fintra() ,
                                                                            totalOferta.getDtf_semana() + totalOferta.getDtf_puntos() ,
                                                                            totalOferta.getCuotas_reales() ) , 2) );
          }
      }

      totalOferta.setTotal_financiacion ( Util.redondear2(  (totalOferta.getCuota_pago() * totalOferta.getCuotas_reales() )/10 , 2) * 10 );//090703


    }
}



public void  determinaValorFinanciados_metodoApplus (TotalOferta totalOferta,String fecha_financiacion,
                                                     PuntosFinanciacion puntosFinanciacion)  throws SQLException {


    // PASO 3B. METODO AUTOMATICO y VIEJO  UTILIZANDO LOS VALORES OFERTA Y ECA OFERTA


    // Calculo de comisiones e ivas

    if(totalOferta.getEstudio_economico().equalsIgnoreCase("APPLUS NORCONTROL")){
        // UTILIZA EC_OFERTA    (no contiene eca)
        // El valor oferta contiene el totalPrev1 mas las comisiones sin la comision eca

        // Comisiones

        double oferta     = totalOferta.getGranOferta();
        double totalPrev1 = totalOferta.getEc_granTotal_prev1();

        // totalComisionesSinEca se compone de las de comisiones AP+, Factoring fintra, Provintegral y Fintra
        double totalComisionesSinEca = oferta - totalPrev1;


        totalOferta.setEc_comision_applus           ( Util.redondear2(totalOferta.getEc_granTotal_prev1() * totalOferta.getEc_porcentaje_comision_applus(),2) );
        totalOferta.setEc_comision_factoring_fintra ( Util.redondear2(totalOferta.getEc_granTotal_prev1() * totalOferta.getEc_porcentaje_factoring_fintra(),2) );
        totalOferta.setEc_comision_provintegral     ( Util.redondear2(totalOferta.getEc_granTotal_prev1() * totalOferta.getEc_porcentaje_comision_provintegral(),2) );

        // Calculamos la comision fintra por diferencia

        totalOferta.setEc_comision_fintra           ( totalComisionesSinEca - totalOferta.getEc_comision_applus()
                                                                            - totalOferta.getEc_comision_factoring_fintra()
                                                                            - totalOferta.getEc_comision_provintegral() );

        totalOferta.setEc_comision_eca   ( Util.redondear2(oferta * totalOferta.getEc_porcentaje_comision_eca() , 2 ) );

        // Ivas

        double totalSinIva  = oferta + totalOferta.getEc_comision_eca();

        double ivaTotal     = totalSinIva * totalOferta.getEc_porcentaje_iva();
        ivaTotal            = Util.redondear2(ivaTotal, 2);

        totalOferta.setEc_iva_total                     ( ivaTotal);
        totalOferta.setEc_iva_valor_mat                 ( Util.redondear2(totalOferta.getEc_granTotal_prev1() * totalOferta.getEc_porcentaje_iva(),2) );

        totalOferta.setEc_iva_comision_applus           ( Util.redondear2(totalOferta.getEc_comision_applus() * totalOferta.getEc_porcentaje_iva(),2) );
        totalOferta.setEc_iva_comision_factoring_fintra ( Util.redondear2(totalOferta.getEc_comision_factoring_fintra() * totalOferta.getEc_porcentaje_iva(),2) );
        totalOferta.setEc_iva_comision_fintra           ( Util.redondear2(totalOferta.getEc_comision_fintra() * totalOferta.getEc_porcentaje_iva(),2) );
        totalOferta.setEc_iva_comision_provintegral     ( Util.redondear2(totalOferta.getEc_comision_provintegral() * totalOferta.getEc_porcentaje_iva(),2) );

        double ivaTotalSinEca  = totalOferta.getEc_iva_valor_mat()                 + totalOferta.getEc_iva_comision_applus() +
                                 totalOferta.getEc_iva_comision_factoring_fintra() + totalOferta.getEc_iva_comision_fintra() +
                                 totalOferta.getEc_iva_comision_provintegral() ;

        totalOferta.setEc_iva_comision_eca              (ivaTotal - ivaTotalSinEca) ;

        // GranOferta es un acumulado de todas las ofertas
        totalOferta.setIvaGranOferta         (ivaTotalSinEca);
        totalOferta.setGranOferta_mas_iva    (oferta +  ivaTotalSinEca);

        totalOferta.setGranEca_oferta        (totalSinIva);
        totalOferta.setIvaGranEca_oferta     (ivaTotal) ;
        totalOferta.setGranEca_oferta_mas_iva(totalSinIva + ivaTotal);


    }


    else if (totalOferta.getEstudio_economico().equalsIgnoreCase("CONSORCIO ECA-APPLUS-FINTRAVALORES")){
        // UTILIZA EC_ECA_OFERTA  (contiene eca)

        double eca_oferta  = totalOferta.getGranEca_oferta();
        double totalPrev1  = totalOferta.getEc_granTotal_prev1();



        double totalComisionesConEca = eca_oferta - totalPrev1;


        totalOferta.setEc_comision_applus           ( Util.redondear2(totalOferta.getEc_granTotal_prev1() * totalOferta.getEc_porcentaje_comision_applus(),2) );
        totalOferta.setEc_comision_factoring_fintra ( Util.redondear2(totalOferta.getEc_granTotal_prev1() * totalOferta.getEc_porcentaje_factoring_fintra(),2) );
        totalOferta.setEc_comision_provintegral     ( Util.redondear2(totalOferta.getEc_granTotal_prev1() * totalOferta.getEc_porcentaje_comision_provintegral(),2) );
        totalOferta.setEc_comision_fintra           ( Util.redondear2(totalOferta.getEc_granTotal_prev1() * totalOferta.getEc_porcentaje_comision_fintra(),2) );


        // Calculamos la comision eca por diferencia

        totalOferta.setEc_comision_eca              ( totalComisionesConEca - totalOferta.getEc_comision_applus()
                                                                            - totalOferta.getEc_comision_factoring_fintra()
                                                                            - totalOferta.getEc_comision_provintegral()
                                                                            - totalOferta.getEc_comision_fintra() );

        // calculando la oferta en base a eca_oferta

        totalOferta.setGranOferta(eca_oferta - totalOferta.getEc_comision_eca() );


        // Ivas

        double totalSinIva  = eca_oferta;


        double ivaTotal     = totalSinIva * totalOferta.getEc_porcentaje_iva();
        ivaTotal            = Util.redondear2(ivaTotal, 2);

        totalOferta.setEc_iva_total                     ( ivaTotal);
        totalOferta.setEc_iva_valor_mat                 ( Util.redondear2(totalOferta.getEc_granTotal_prev1() * totalOferta.getEc_porcentaje_iva(),2) );

        totalOferta.setEc_iva_comision_applus           ( Util.redondear2(totalOferta.getEc_comision_applus() * totalOferta.getEc_porcentaje_iva(),2) );
        totalOferta.setEc_iva_comision_factoring_fintra ( Util.redondear2(totalOferta.getEc_comision_factoring_fintra() * totalOferta.getEc_porcentaje_iva(),2) );
        totalOferta.setEc_iva_comision_fintra           ( Util.redondear2(totalOferta.getEc_comision_fintra() * totalOferta.getEc_porcentaje_iva(),2) );
        totalOferta.setEc_iva_comision_provintegral     ( Util.redondear2(totalOferta.getEc_comision_provintegral() * totalOferta.getEc_porcentaje_iva(),2) );

        double ivaTotalSinEca  = totalOferta.getEc_iva_valor_mat()                 + totalOferta.getEc_iva_comision_applus() +
                                 totalOferta.getEc_iva_comision_factoring_fintra() + totalOferta.getEc_iva_comision_fintra() +
                                 totalOferta.getEc_iva_comision_provintegral() ;

        totalOferta.setEc_iva_comision_eca              (ivaTotal - ivaTotalSinEca) ;



        totalOferta.setGranOferta       (eca_oferta - totalOferta.getEc_comision_eca() );
        totalOferta.setIvaGranOferta    (ivaTotalSinEca);
        totalOferta.setGranOferta_mas_iva(totalOferta.getGranOferta() +  ivaTotalSinEca);

        // GranEca_oferta es acumulado de todas las eca_oferta
        totalOferta.setIvaGranEca_oferta(ivaTotal) ;
        totalOferta.setGranEca_oferta_mas_iva(eca_oferta + ivaTotal);




    }



    // Calculo de financiacion


    totalOferta.setEc_financiacion_fintra   ( totalOferta.getGranEca_oferta_mas_iva() );

    if (totalOferta.getPrefijo().equalsIgnoreCase("VA")){

        totalOferta.setEc_financiacion_fintra   ( Util.redondear2((totalOferta.getEc_financiacion_fintra()/10),2)*10 );//090703
        totalOferta.setEc_cuota_pago            ( totalOferta.getEc_financiacion_fintra () );
        totalOferta.setEc_total_financiacion    ( totalOferta.getEc_financiacion_fintra () );
    }
    else {

        int cuotas_reales           = totalOferta.getCuotas_reales();
        double dtf_semana           = totalOferta.getDtf_semana();


        try {
            puntosFinanciacion = getPuntosFinanciacion(totalOferta.getEsquema_financiacion(),
                                                       totalOferta.getRegulacion(),
                                                       totalOferta.getEc_financiacion_fintra(), totalOferta.getAno(),
                                                       totalOferta.getTrimestre(), totalOferta.getCuotas_reales() );

            totalOferta.setPuntosFinanciacion(puntosFinanciacion);

        }catch(Exception e){
            puntosFinanciacion.setPuntos(0.0);
            puntosFinanciacion.setTipo("DTF");
            totalOferta.setPuntosFinanciacion(puntosFinanciacion);
            System.out.println("error en service raro::"+e.toString());
            e.printStackTrace();
            throw new SQLException("ERROR EN LA BUSQUEDA DE LA LOCALIZACION DE LOS PUNTOS DE FINANCIACION. \n " + e.getMessage());
        }


        double dtf_puntos = totalOferta.getDtf_puntos();
        String tipo_puntos= totalOferta.getTipo_puntos();


        if (cuotas_reales == 1) {
            totalOferta.setEc_cuota_pago (totalOferta.getEc_financiacion_fintra() );
        }
        else {
            if (tipo_puntos.equalsIgnoreCase("MAXIMA")){
              totalOferta.setEc_cuota_pago ( Util.redondear2( get_anualidad(totalOferta.getEc_financiacion_fintra() ,dtf_puntos , cuotas_reales ) , 2) );
            }
            else {
              totalOferta.setEc_cuota_pago ( Util.redondear2( get_anualidad_dtf(totalOferta.getEc_financiacion_fintra() , dtf_semana + dtf_puntos , cuotas_reales ) , 2) );
            }
        }

       totalOferta.setEc_total_financiacion ( Util.redondear2(  (totalOferta.getEc_cuota_pago() * cuotas_reales)/10 , 2) * 10 );//090703
    }
}




    public double get_dtf_mv (double r) {

        double ipa = (r/100)/4;                            // trimestre anticipado
        double ipv = ipa/(1-ipa);                          // trimestre vencido
        double iev =  Math.pow((1+ipv),4) - 1;             // efectivo anual
        double iea =  Math.pow((1-ipa),-4) - 1;            // efectivo anual
        double rn = 1/12;
        double ip  =  Math.pow((1+ iea),0.083333333) - 1;  // Tasa periódica a partir de la tasa efectiva anual

        return ip * 100;
    }


    public double get_anualidad_dtf(double vp , double dtf , double n ) {

        // Sirve para pasar conseguir el valor de una anualidad especificando una DTF + puntos que sera convertida en Mensual Vencida
        // (DTF dada en TA+puntos a una MV)

        // r = anual trimestre anticipado.

        double i = get_dtf_mv(dtf);
        double r = Math.pow((1+(i/100)),n);
        double factor =  (r-1)/(i/100*r);
        double a = vp / factor;

        return a;
    }


    public double get_anualidad(double vp , double i , double n ) {

        // Sirve para pasar conseguir el valor de una anualidad especificando un interes en  Mensual Vencida

        // r = anual trimestre anticipado.

        double r = Math.pow((1+(i/100)),n);
        double factor =  (r-1)/(i/100*r);
        double a = vp / factor;

        return a;
    }



    public String setSimboloVariable(String simbolo_variable, String observacion, int id_estado_negocio,
                                     int id_orden,String userx)throws SQLException{

        return applusDao.setSimboloVariable(simbolo_variable, observacion, id_estado_negocio, id_orden,userx);

    }


   public String setLiquidacionEca(OfertaEcaDetalle ofertaEcaDetalle)throws SQLException{
    return applusDao.setLiquidacionEca(ofertaEcaDetalle)  ;
   }





    public List  buscaPrefacturaEca()throws SQLException{
        return applusDao.buscaPrefacturaEca();
    }



    public List  buscaPrefacturaEcaContratista(int id_orden)throws SQLException{
        return applusDao.buscaPrefacturaEcaContratista(id_orden);
    }






   public String  setFactura(String dstrct, String tipo_documento, String documento,
                             String nit, String codcli, String concepto,
                             String fecha_factura, String fecha_vencimiento,
                             String fecha_impresion,String descripcion,
                             Double valor_factura, Double valor_abono, Double valor_saldo,
                             Double valor_facturame, Double valor_abonome, Double valor_saldome,
                             Double valor_tasa, String moneda,
                             int cantidad_items, String forma_pago, String agencia_facturacion,
                             String agencia_cobro,String zona, String base,String last_update, String user_update,
                             String creation_date, String creation_user, String cmc,
                             String formato, String agencia_impresion,
                             String tipo_ref1,String ref1,String tipo_ref2,String ref2)throws SQLException{


       return applusDao.setFactura(dstrct, tipo_documento, documento, nit, codcli,
                                   concepto, fecha_factura, fecha_vencimiento, fecha_impresion,
                                   descripcion, valor_factura, valor_abono, valor_saldo, valor_facturame,
                                   valor_abonome, valor_saldome, valor_tasa, moneda, cantidad_items,
                                   forma_pago, agencia_facturacion, agencia_cobro,
                                   zona, base, last_update, user_update, creation_date,
                                   creation_user, cmc, formato, agencia_impresion,
                                   tipo_ref1, ref1, tipo_ref2,  ref2);

   }


    public String setFacturaDetalle(String dstrct, String tipo_documento, String documento,
                                    int item, String nit, String concepto,
                                    String descripcion, String codigo_cuenta_contable,
                                    double cantidad, double valor_unitario , double valor_unitariome , double valor_item,
                                    double valor_itemme , double valor_tasa, String moneda,
                                    String last_update, String user_update, String creation_date,
                                    String creation_user, String base, String auxiliar)throws SQLException{

        return applusDao.setFacturaDetalle(dstrct, tipo_documento, documento, item,
                                           nit, concepto, descripcion, codigo_cuenta_contable,
                                           cantidad, valor_unitario, valor_unitariome,
                                           valor_item, valor_itemme, valor_tasa,
                                           moneda, last_update, user_update, creation_date,
                                           creation_user, base, auxiliar);


    }


    public String setOferta(int id_orden, String factura_eca, String fecha_factura_eca)throws SQLException{
        return applusDao.setOferta(id_orden, factura_eca, fecha_factura_eca);
    }


    public String setFechaFacturaEca(int id_orden, String fecha_factura_eca)throws SQLException{
        return applusDao.setFechaFacturaEca(id_orden,  fecha_factura_eca);
    }

    public List  buscaPrefacturaApp()throws SQLException{
        return applusDao.buscaPrefacturaApp();
    }

    public String setFacturaComision(int id_orden, String documento, String fecha_factura_eca)throws SQLException{
        return applusDao.setFacturaComision(id_orden, documento, fecha_factura_eca);
    }

    public List  buscaPrefacturaPro()throws SQLException{
        return applusDao.buscaPrefacturaPro();
    }

    public String setFacturaPro(int id_orden, String documento, String fecha_factura_eca)throws SQLException{
        return applusDao.setFacturaPro(id_orden, documento, fecha_factura_eca);
    }


    public List  buscaPrefacturaComisionEca()throws SQLException{
        return applusDao.buscaPrefacturaComisionEca();
    }

    public String setFacturaComisionEca(int id_orden, String documento, String fecha_factura_eca)throws SQLException{
        return applusDao.setFacturaComisionEca(id_orden, documento, fecha_factura_eca);
    }

    public PuntosFinanciacion getPuntosFinanciacion(String esquema, String regulacion, double valor, String ano,
                                                    String trimestre, int cuotas)throws SQLException{
        return applusDao.getPuntosFinanciacion(esquema, regulacion, valor,  ano,
                                               trimestre, cuotas);
    }



    public String getTrimestre(String mes) {

        int mesNumerico = Integer.parseInt(mes);
        String trimestre   = "1";

        if (mesNumerico <= 3) {
            trimestre = "1";
        }
        else if (mesNumerico <= 6){
            trimestre = "2";
        }
        else if (mesNumerico <= 9){
            trimestre = "3";
        }
        else {
            trimestre = "4";
        }
        return trimestre;
    }









}



