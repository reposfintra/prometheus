/*
    Document   : Modulo de Requisiciones Fintra
    Created on : 10/07/2013, 11:00:00 AM
    Author     : hcuello
*/

package com.tsp.operation.model.services;

import java.util.ArrayList;
import com.tsp.operation.model.DAOS.SeguimientoCarteraDAO;
import com.tsp.operation.model.beans.CmbGeneralScBeans;
import com.tsp.operation.model.beans.Politicas;
import com.tsp.operation.model.beans.ReporteSanciones;
import com.tsp.operation.model.beans.SeguimientoCarteraBeans;
import java.sql.SQLException;
import java.util.Collection;

public class SeguimientoCarteraService {
    SeguimientoCarteraDAO lpr;
    
    public SeguimientoCarteraService() {
        lpr = new SeguimientoCarteraDAO();
    }
    public SeguimientoCarteraService(String dataBaseName) {
        lpr = new SeguimientoCarteraDAO(dataBaseName);
    }
    
    public ArrayList<CmbGeneralScBeans> GetComboGenerico(String Query, String IdCmb, String DescripcionCmb, String wuereCmb)throws Exception{
       return lpr.GetComboGenerico(Query, IdCmb, DescripcionCmb, wuereCmb);
    }
    
    public ArrayList<CmbGeneralScBeans> GetComboGenericoStr(String Query, String IdCmb, String DescripcionCmb, String wuereCmb)throws Exception{
       return lpr.GetComboGenericoStr(Query, IdCmb, DescripcionCmb, wuereCmb);
    }

    public ArrayList<SeguimientoCarteraBeans> SeguimientoCarteraConsolidadoBeans(String Query, String unidad_negocio, int periodo_foto, String wHrItem)throws Exception{
       return lpr.GetSeguimientoCarteraConsolidadoBeans(Query, unidad_negocio, periodo_foto, wHrItem);
    }

    public ArrayList<SeguimientoCarteraBeans> SCarteraTramoAnteriorBeans(String Query, String unidad_negocio, int periodo_foto, String wHrItem)throws Exception{
       return lpr.GetSCarteraTramoAnteriorBeans(Query, unidad_negocio, periodo_foto, wHrItem);
    }
    
    public ArrayList<SeguimientoCarteraBeans> SCarteraRecaudoConsolidadoBeans(String Query, String unidad_negocio, int periodo_foto, String UserUso, String wHrItem)throws Exception{
       return lpr.GetSCarteraRecaudoConsolidadoBeans(Query, unidad_negocio, periodo_foto, UserUso, wHrItem);
    }
    
    public ArrayList<SeguimientoCarteraBeans> SCarteraCargarSeguimientoBeans(String Query, String unidad_negocio, int periodo_foto, String aldia, String avencer, String vencido, String SuperHaving, String StatusVcto, String wHrItem, String UserUso, String DiaMes)throws Exception{
       return lpr.GetSCarteraCargarSeguimientoBeans(Query, unidad_negocio, periodo_foto, aldia, avencer, vencido, SuperHaving, StatusVcto, wHrItem, UserUso, DiaMes);
    }
    
    public ArrayList<SeguimientoCarteraBeans> SCarteraCargarSeguimientoBeans(String Query, String unidad_negocio, int periodo_foto, String cedula,String UserUso,  String aldia, String avencer, String vencido)throws Exception{
       return lpr.GetSCarteraCargarSeguimientoClienteBeans(Query, unidad_negocio, periodo_foto, cedula, UserUso, aldia, avencer, vencido);
    }
    public ArrayList<SeguimientoCarteraBeans> SCarteraDetalleCuentaBeans(String Query, String unidad_negocio, int periodo_foto, String wHrItem)throws Exception{
       return lpr.GetSCarteraDetalleCuentaBeans(Query, unidad_negocio, periodo_foto, wHrItem);
    }
    
    public ArrayList<SeguimientoCarteraBeans> SCarteraDetallePagosBeans(String Query, int periodo_foto, String wHrItem)throws Exception{
       return lpr.GetSCarteraDetallePagosBeans(Query, periodo_foto, wHrItem);
    }
    
    public ArrayList<SeguimientoCarteraBeans> SCarteraDetalleCuentaBeans2(String unidad_negocio, int periodo_foto, String wHrItem, String cuota)throws Exception{
       return lpr.GetSCarteraDetalleCuentaBeans2( unidad_negocio, periodo_foto, wHrItem,cuota);
    }
    
     public ArrayList<SeguimientoCarteraBeans> SCarteraDetallePagosBeans2(int periodo_foto, String wHrItem, String num_ingreso)throws Exception{
       return lpr.GetSCarteraDetallePagosBeans2( periodo_foto, wHrItem, num_ingreso);
    }

    public ArrayList<SeguimientoCarteraBeans> GetResumenConsolidado(int periodo_foto, String uni_neg, String user)throws Exception{
       return lpr.GetResumenConsolidado(periodo_foto, uni_neg, user);
    }
    
     public ArrayList<SeguimientoCarteraBeans> SCarteraDetallePagosBeansTodos(int periodo_foto, String wHrItem, String num_ingreso)throws Exception{
       return lpr.GetSCarteraDetallePagosBeansTodos( periodo_foto, wHrItem, num_ingreso);
    }

    public ArrayList<SeguimientoCarteraBeans> obtenerDetalleCarteraUndNegocio(String periodo, String undNeg, String iduser) throws SQLException {
        return lpr.GetObtenerDetalleCarteraUndNegocio(periodo,undNeg,iduser);
    }

    public ArrayList<ReporteSanciones> generarSancionesFenalco(String periodo, String und_negocio) throws SQLException {
        return lpr.generarSancionesFenalco(periodo,und_negocio);
    }

    public ArrayList<ReporteSanciones>  generarSancionesMicro(String periodo, String und_negocio) throws SQLException {
        return lpr.generarSancionesMicro(periodo,und_negocio);
    }
    
    public String  cargarCompromisosPago(String fechaini, String fechafin, String agente, String tipo, String domicilio, String usuario) throws SQLException {
        return lpr.cargarCompromisosPago(fechaini, fechafin, agente, tipo, domicilio, usuario);
    }
    
    public String  tipoAgenteCartera(String usuario) throws SQLException {
        return lpr.tipoAgenteCartera(usuario);
    }
    
    public String guardarReciboCaja(Integer consecutivo,String fecha_entrega,String asesor,String area,String estado_recibo, String usuario) {
        return lpr.guardarReciboCaja(consecutivo,fecha_entrega,asesor,area,estado_recibo,usuario);
    }
         
    public String actualizarRecibosCaja(String consecutivo, String fecha_entrega, String asesor, String area, String estado_recibo, String fecha_recibido, String id_cliente, String nombre_cliente, 
            String tipo_recaudo, String valor_recaudo, String cod_neg, String usuario) {
        return lpr.actualizarRecibosCaja(consecutivo, fecha_entrega, asesor, area, estado_recibo, fecha_recibido, id_cliente, nombre_cliente, tipo_recaudo, valor_recaudo, cod_neg, usuario);
    }

    public String anularReciboCaja(String consecutivo) {
        return lpr.anularReciboCaja(consecutivo);
    }
    
    public String cargarInfoRecibo(String num_recibo){
        return lpr.cargarInfoRecibo(num_recibo);
    }
      
    public String cargarRecibosCaja(String asesor, String fechaini, String fechafin, String estado, String num_recibo){
        return lpr.cargarRecibosCaja(asesor, fechaini, fechafin, estado, num_recibo);
    }
    
    public String cambiarEstadoReciboCaja(String num_recibo, String estado_recibo, String usuario){
         return lpr.cambiarEstadoReciboCaja(num_recibo, estado_recibo, usuario);
    }
    
    public boolean isAllowToApplyRC(String usuario) {
         return lpr.isAllowToApplyRC(usuario);
    }
    
    public boolean existe_Recibo_Caja(String num_recibo, String sWhere){
        return lpr.existe_Recibo_Caja(num_recibo, sWhere);
    }
    
    public String fn_get_Estado_Recibo_Caja(String num_recibo, String sWhere){
        return lpr.fn_get_Estado_Recibo_Caja(num_recibo, sWhere);
    }
    
    public String get_Next_Recibo_Caja(){
        return lpr.get_Next_Recibo_Caja();
    }
    
    public String get_Nombre_Cliente(String cedula) {
        return lpr.get_Nombre_Cliente(cedula);
    }
    
    public void actualizaConsecutivoRC() {
          lpr.actualizaConsecutivoRC();
    }
    
    public String reasignarAsesor(String num_recibo, String asesor, String usuario){
         return lpr.reasignarAsesor(num_recibo, asesor, usuario);
    }
    
    public String actualizaComentarioRC(String consecutivo, String comment, String usuario) {
        return lpr.actualizaComentarioRC(consecutivo, comment, usuario);
    }
 
    public String cargarNegociosCliente(String idCliente) {
        return lpr.cargarNegociosCliente(idCliente);
    }
}
