/*****************************************************************************
 * Nombre clase : ...............   ExtractoService.java                     *
 * Descripcion :.................   Clase que maneja los Servicios           *
 *                                  asignados a Model relacionados con el    *
 *                                  programa de generacion del reporte de    *
 *                                  Extractos                                *
 * Autor :.......................   Ing. Henry A. Osorio Gonzalez            *
 * Fecha :........................  18 de noviembre de 2005, 10:21 AM        *
 * Version :......................  1.0                                      *
 * Copyright :....................  Fintravalores S.A.                  *
 *****************************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.ExtractoDAO;
import java.util.Vector;
import java.sql.*;
import com.tsp.exceptions.*;
import java.util.*;


public class ExtractoService {
    
    public ExtractoDAO extractoDataAccess;
    private List corridaBanco;
    private String aprobadas;
    private String pagadas;
    private String distrito;
    private String tipo;
    /** Creates a new instance of FacturacionService */
    public ExtractoService() {
        extractoDataAccess = new ExtractoDAO();
    }
    public ExtractoService(String dataBaseName) {
        extractoDataAccess = new ExtractoDAO(dataBaseName);
    }
    
    public java.util.Vector getExtracto() {
        return extractoDataAccess.getExtracto();
    }
    
   public void extracto(String distrito, String banco, String cuenta, String corrida, String aprobadas,String pagadas, String tipo) throws SQLException, InformationException{
       extractoDataAccess.extracto(distrito, banco, cuenta, corrida, aprobadas,pagadas, tipo); 
   }
   public String getMensaje(){
       return extractoDataAccess.getMensaje();
   }
    public void setMensaje(String mensaje){
        extractoDataAccess.setMensaje(mensaje);
   }
   
   /**
     * Metodo listaCorridaBanco , Metodo para buscar las corridas de un banco sucursal
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @throws  : SQLException
     * @version : 1.0
     */
    public void listaCorridaBanco(String Dstrct, String banco, String sucursal,String aprobadas,String pagadas) throws SQLException{
       this.corridaBanco =  extractoDataAccess.listaCorridaBanco(Dstrct, banco, sucursal, aprobadas, pagadas);     
    }
    
    /**
     * Getter for property corridaBanco.
     * @return Value of property corridaBanco.
     */
    public java.util.List getCorridaBanco() {
        return corridaBanco;
    }
    
    /**
     * Getter for property aprobadas.
     * @return Value of property aprobadas.
     */
    public java.lang.String getAprobadas() {
        return aprobadas;
    }    
    
    /**
     * Setter for property aprobadas.
     * @param aprobadas New value of property aprobadas.
     */
    public void setAprobadas(java.lang.String aprobadas) {
        this.aprobadas = aprobadas;
    }    
    
    /**
     * Getter for property pagadas.
     * @return Value of property pagadas.
     */
    public java.lang.String getPagadas() {
        return pagadas;
    }
    
    /**
     * Setter for property pagadas.
     * @param pagadas New value of property pagadas.
     */
    public void setPagadas(java.lang.String pagadas) {
        this.pagadas = pagadas;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    public Vector obtenerCorridasXLS (String dstrct, String banco, String sucursal, String corrida, String aprobados, String pagados) throws SQLException{
       return extractoDataAccess.obtenerCorridasXLS(dstrct, banco, sucursal, corrida, aprobados, pagados);
   }
   /**
     * Getter for property corridaBanco.
     * @return Value of property corridaBanco.
     */
    public java.util.List setCorridaBanco( List corr ) {
        return corridaBanco = corr;
    } 
    
    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.lang.String getTipo() {
        return tipo;
    }
    
    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }
    public void setPropietarios (String propietarios){                
        String vecPropietarios[] = propietarios.split("-");
        propietarios = "";
        for (int i=0; i<vecPropietarios.length; i++){
            propietarios+= "'"+vecPropietarios[i]+"',";
        }
        propietarios = propietarios.substring(0,propietarios.lastIndexOf(","));
        System.out.println("PROPIETARIOS :"+propietarios);
        extractoDataAccess.setPropietarios(propietarios);
    }    
    
    /**
     *Metodo para obtener todos los propietarios
     */
    public void searchBeneficiariosCorrida(String distrito, String banco, String cuenta, String corrida, String aprobadas,String pagadas) throws SQLException{
        extractoDataAccess.searchBeneficiariosCorrida(distrito, banco, cuenta, corrida, aprobadas, pagadas);
    }
    
     /**
     * Getter for property beneficiarios.
     * @return Value of property beneficiarios.
     */
    public java.util.Vector getBeneficiarios() {
        return extractoDataAccess.getBeneficiarios();
    }
}
