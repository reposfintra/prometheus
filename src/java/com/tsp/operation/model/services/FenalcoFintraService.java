
/***************************************
 * Nombre Clase  FenalcoFintraService.java
 * Descripci�n   Permite Operar las captaciones
 * Autor         JULIO ERNESTO BARROS RUEDA
 * Fecha         06/06/2007
 * versi�n       1.0
 * Copyright     Fintra Valores S.A.
 *******************************************/

package com.tsp.operation.model.services;



import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.util.Utility;
import com.tsp.util.Util;
import java.util.*;
import java.io.*;


/**
 *
 * @author  USER
 */
public class FenalcoFintraService {
    
    
    FenalcoFintraDAO   FenalcoFintraDataAccess;
    Vector Avales =new Vector();
    private Vector clientes= new Vector() ;
    
    
    public FenalcoFintraService() {
        FenalcoFintraDataAccess = new FenalcoFintraDAO(); 
    }    
    public FenalcoFintraService(String dataBaseName) {
        FenalcoFintraDataAccess = new FenalcoFintraDAO(dataBaseName); 
    }
    
   
    Propietario     Propietario      = new Propietario(); 
    
    /*
     * Metodo que retorna los datos de un propietario de Captaciones
     *
     */
    public void buscarPropietarioF(String propietario) throws Exception {
        try{
            Propietario = FenalcoFintraDataAccess.buscarPropietarioF(propietario);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    /*
     *
     *
     */
    public void buscarPorcentajeAval (String fomapag,String tiponeg,String nit) throws Exception {
        try{
            Avales = FenalcoFintraDataAccess.buscarPorcentajeAval(fomapag,tiponeg,nit);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    /*
     *
     *
     */
    public boolean buscarFestivo (String fecha) throws Exception {
        
        boolean festivo = false;
        try{
            festivo = FenalcoFintraDataAccess.buscarFestivo(fecha); 
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        return festivo;
    }
    
    /**
     * Getter for property Propietario.
     * @return Value of property Propietario.
     */
    public com.tsp.operation.model.beans.Propietario getPropietario() {
        return Propietario;
    }    
    
    /**
     * Setter for property Propietario.
     * @param Propietario New value of property Propietario.
     */
    public void setPropietario(com.tsp.operation.model.beans.Propietario Propietario) {
        this.Propietario = Propietario;
    }
    
    /**
     * Getter for property Avales.
     * @return Value of property Avales.
     */
    public java.util.Vector getAvales() {
        return Avales;
    }    
    
    /**
     * Setter for property Avales.
     * @param Avales New value of property Avales.
     */
    public void setAvales(java.util.Vector Avales) {
        this.Avales = Avales;
    }    
    
    public String getcodtab()
    {
        return (FenalcoFintraDataAccess.getcodt()); 
    }
    
     public String buscarnitas (String fecha) throws Exception {
        
        String  nit;
        try{
            nit = FenalcoFintraDataAccess.busconitas(fecha);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        return nit;
    }
      
    public String buscarnitas2 (String fecha) throws Exception {
        
        String  nit;
        try{
            nit = FenalcoFintraDataAccess.busconitas2(fecha);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        return nit;
    }
    
     public void buscarPropietarioF2(String propietario) throws Exception {
        try{
            Propietario = FenalcoFintraDataAccess.buscarPropietarioF2(propietario);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
     
     public void buscarPorcentajeAval2 (String fomapag,String tiponeg,String coneg,String codtb) throws Exception {
        try{
            Avales = FenalcoFintraDataAccess.buscarPorcentajeAval2(fomapag,tiponeg,coneg,codtb);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    ///clientes
     
    public void loadClientes(String a) throws Exception {
       clientes = new Vector();
        try{
            clientes = FenalcoFintraDataAccess.obtenerpaises(a);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
   }
    ///retorna el resultado
    public Vector getClientes() {
       return clientes;
   }

    public String obtenerPrimeraFechaPago(String ciclo,String fechai) throws Exception {
        return FenalcoFintraDataAccess.obtenerPrimeraFechaPago(ciclo,fechai);
    }

   
}







    



    

