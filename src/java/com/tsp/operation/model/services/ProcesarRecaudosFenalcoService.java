/*
 * ProcesarRecaudosService.java
 *
 * Created on 13 de agosto de 2009, 10:41
 */

package com.tsp.operation.model.services;
import java.io.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.DAOS.ProcesarRecaudosFenalcoDAO;
import javax.servlet.*;
import com.tsp.exceptions.*;
import java.sql.*;
/**
 *
 * @author  Miguel
 */
public class ProcesarRecaudosFenalcoService {
    private boolean process;
    String Errores;
    private String dataBaseName = "";
    /** Creates a new instance of ProcesarRecaudosService */
    public ProcesarRecaudosFenalcoService() {}
    public ProcesarRecaudosFenalcoService(String dataBaseName) {
        this.dataBaseName = dataBaseName;
    }
    
    public Ingreso crearCabeceraIngreso(String num_ingreso,String fecha_ingreso,String banco,String sucursal,String user)
    {   Ingreso ing=new Ingreso();
        ing.setReg_status("");
        ing.setDstrct("FINV");
        ing.setNum_ingreso(num_ingreso);
       // st.setString(6, ingreso.getConcepto());
        ing.setTipo_ingreso("C");
        ing.setFecha_ingreso(fecha_ingreso);
        ing.setFecha_consignacion(fecha_ingreso);
        ing.setBranch_code(banco);
        ing.setBank_account_no(sucursal);
        ing.setCodmoneda("PES");
        ing.setAgencia_ingreso("OP");
        ing.setPeriodo("000000");
        ing.setConcepto("FE");
        ing.setTransaccion(0);
        ing.setVlr_tasa(1.000000);
        ing.setFecha_tasa(fecha_ingreso);
        ing.setCreation_user(user);
        ing.setUser_update(user);
        ing.setBase("COL");
        ing.setTransaccion_anulacion(0);
        ing.setTipo_documento("ING");
        ing.setNro_consignacion("");
        ing.setCuenta("");
        ing.setAuxiliar("");
        ing.setTasaDolBol(0.000000);
        ing.setAbc("");
        ing.setVlr_saldo_ing(0);
        ing.setCmc("");
        ing.setTipo_aux("");
        
        return ing;
    }
    public void ProcesarRecaudos(String user) throws Exception
    {   TransaccionService tService = new TransaccionService(this.dataBaseName);
        tService.crearStatement();
        Errores="";
        String arrayQuerys[];
        int sucess=0;
        ProcesarRecaudosFenalcoDAO recdao=new ProcesarRecaudosFenalcoDAO(this.dataBaseName);
        ArrayList recaudos=recdao.buscarRecaudos();
        for(int i=0;i<recaudos.size();i++)
        {   String SQL="";
            Recaudos rec=(Recaudos)recaudos.get(i);
            System.out.println(rec.getFacturas());
            String [] facturas=rec.getFacturas().split(",");
            System.out.println(facturas[0]);
            double valor=recdao.ValorFacturas(facturas);
            String cuenta="";
            String desc="";
            double valor_recaudo=rec.getValor();
            double temp=valor_recaudo;
            if((valor+rec.getIntereses()<=rec.getValor()))
            {   sucess++;
                int j;
                Ingreso ing=crearCabeceraIngreso(recdao.buscarSerie("INGC"),rec.getFecha(),rec.getBanco(),rec.getSucursal(),user);
                Errores=Errores+"El Ingreso "+ing.getNum_ingreso()+" por valor de "+rec.getValor()+" pesos";
                String SQL2="";
                for(j=0;j<facturas.length && !rec.getFacturas().equals("");j++)
                {   String sql[]=recdao.cancelarFactura(facturas[j]);
                    if(sql[1]!=null && Double.parseDouble(sql[1])!=0)
                    {   Errores=Errores+"\nAfecto la factura "+facturas[j];
                        if(j<facturas.length-1)
                        {   Errores=Errores+",";
                        }
                        else
                        {   Errores=Errores+"\n";
                        }
                        SQL2=SQL2+sql[0];
                        ing.setVlr_ingreso(Double.parseDouble(sql[1]));
                        ing.setVlr_ingreso_me(ing.getVlr_ingreso());
                        ing.setCuenta(recdao.getCuentaCmc(facturas[j])); 
                        if(!(facturas[j].substring(0,2).equals("PF")))
                        {   ing.setAuxiliar(sql[2]);
                            ing.setTipo_aux("RD");
                        }
                        else
                        {   ing.setAuxiliar("");
                            ing.setTipo_aux("");
                        }
                        if(facturas[j].substring(0,2).equals("PG"))
                        {   desc="REDPAGARE";
                        }
                        else
                        {   if(facturas[j].substring(0,2).equals("PC"))
                            {   desc="REDCHEQUE";
                            }
                            else
                            {   if(facturas[j].substring(0,2).equals("PL"))
                                {   desc="REDLETRA";
                                }
                            }
                        }
                        SQL2=SQL2+recdao.insertarIngresoDetalle(ing,j+1,facturas[j],desc);
                    }
                    else
                    {   Errores=Errores+"ERROR!! La factura "+facturas[j]+" no existe o no tiene saldo\n";
                        SQL2="ERROR!!!";
                        j=facturas.length;
                    }
                } 
                ing.setAuxiliar("");
                ing.setTipo_aux("");
                valor_recaudo=valor_recaudo-valor;
                if(!SQL2.equals("ERROR!!!"))
                {  SQL=SQL+SQL2;
                   if(rec.getIntereses()!=0)
                    {   Errores=Errores+"\nCancelo "+rec.getIntereses()+" pesos de intereses\n";
                        ing.setCuenta("16252012");
                        ing.setVlr_ingreso(rec.getIntereses());
                        ing.setVlr_ingreso_me(rec.getIntereses());
                        SQL=SQL+recdao.insertarIngresoDetalle(ing,j+1,"","Ajuste saldo al ingreso Nro. "+ing.getNum_ingreso());
                        valor_recaudo=valor_recaudo-rec.getIntereses();j++;
                    }
                    if(valor_recaudo!=0)
                    {   if(rec.getCuenta().equals(""))
                        {   if(valor_recaudo>=5000)
                            {   cuenta="16252012";
                            }
                            else
                            {   cuenta="16252011";
                            }
                        }
                        else
                        {   cuenta=rec.getCuenta();
                        }
                        Errores=Errores+"\nCancelo "+valor_recaudo+" pesos a la cuenta "+cuenta+"\n";                  
                        ing.setCuenta(cuenta);
                        ing.setVlr_ingreso(valor_recaudo);
                        ing.setVlr_ingreso_me(valor_recaudo);
                        SQL=SQL+recdao.insertarIngresoDetalle(ing,j+1,"","Ajuste saldo al ingreso Nro. "+ing.getNum_ingreso());j++;
                    }
                    else
                    {   if(facturas.length==0)
                        {   ing.setVlr_saldo_ing(rec.getValor());
                        }
                    }
                    ing.setVlr_ingreso(temp);
                    ing.setVlr_ingreso_me(temp);
                    ing.setCant_item(j);
                    if(j==0)
                    {   Errores=Errores+" PENDIENTE POR DETALLAR\n";
                    }
                    if(rec.getFacturas().equals(""))
                    {   SQL=recdao.insertarIngreso(ing,"",rec.getCod())+SQL;
                    }
                    else
                    {   SQL=recdao.insertarIngreso(ing,facturas[0],"")+SQL;
                    }
                    SQL=SQL+recdao.actualizarRecaudo(rec.getOid());
                    
                    //Esto es para que pueda guardar el Ingreso
                    arrayQuerys = SQL.split(";");
                    for (int k = 0; k < arrayQuerys.length; k++) {
                         tService.getSt().addBatch(arrayQuerys[k]);
                    }
                    
                   
                }
            }
            else
            {   Ingreso ing=crearCabeceraIngreso(recdao.buscarSerie("INGC"),rec.getFecha(),rec.getBanco(),rec.getSucursal(),user);
                Errores=Errores+"El Ingreso "+ing.getNum_ingreso()+" por valor de "+rec.getValor()+" pesos";
                Errores=Errores+" PENDIENTE POR DETALLAR\n";
                ing.setVlr_ingreso(temp);
                ing.setVlr_ingreso_me(temp);
                ing.setCant_item(0);
                if(rec.getFacturas().equals(""))
                {   SQL=recdao.insertarIngreso(ing,"",rec.getCod())+SQL;
                }
                else
                {   SQL=recdao.insertarIngreso(ing,facturas[0],"")+SQL;
                }
                SQL=SQL+recdao.actualizarRecaudo(rec.getOid());
                
                arrayQuerys = SQL.split(";");
                for (int k = 0; k < arrayQuerys.length; k++) {
                         tService.getSt().addBatch(arrayQuerys[k]);
                }
            }
        }
        
        Errores="Se procesaron "+sucess+ " registros exitosamente!!\n"+Errores;
        System.out.println(Errores);
        tService.execute();
        tService.closeAll();
    }
    
    /**
     * Getter for property process.
     * @return Value of property process.
     */
    public boolean isProcess() {
        return process;
    }
    
    /**
     * Setter for property process.
     * @param process New value of property process.
     */
    public void setProcess(boolean process) {
        this.process = process;
    }
    
    /**
     * Getter for property Errores.
     * @return Value of property Errores.
     */
    public java.lang.String getErrores() {
        return Errores;
    }

    public void ProcesarNotas(String login,int opcion)throws Exception {
        ProcesarRecaudosFenalcoDAO recdao=new ProcesarRecaudosFenalcoDAO(this.dataBaseName);
        Errores=recdao.ProcesarNotas(opcion);
    }
    
}
