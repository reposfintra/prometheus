/*
 * Codigo_discrepanciaService.java
 *
 * Created on 28 de junio de 2005, 01:12 AM
 */

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  Jose
 */
public class Tipo_Recuperacion_AnticipoService {
    
    private Tipo_Recuperacion_AnticipoDAO trecuperacionDAO;
    /** Creates a new instance of Codigo_discrepanciaService */
    public Tipo_Recuperacion_AnticipoService() {
        trecuperacionDAO = new  Tipo_Recuperacion_AnticipoDAO();
    }
    
    /**
     * Getter for property tipo_recuperacion.
     * @return Value of property tipo_recuperacion.
     */
    public com.tsp.operation.model.beans.Tipo_Recuperacion_Anticipo getTipo_Recuperacion() {
        return trecuperacionDAO.getTipo_Recuperacion();
    }
    
    /**
     * Setter for property tipo_recuperacion.
     * @param tipo_recuperacion New value of property tipo_recuperacion.
     */
    public void settipo_recuperacion(com.tsp.operation.model.beans.Tipo_Recuperacion_Anticipo tipo_recuperacion) {
        trecuperacionDAO.setTipo_Recuperacion(tipo_recuperacion);
    }
    
    /**
     * Getter for property Tipo_Recuperacion_Anticipoe.
     * @return Value of property Tipo_Recuperacion_Anticipoe.
     */
    public java.util.Vector getTipo_Recuperacion_Anticipo() {
        return trecuperacionDAO.getTipo_Recuperacion_Anticipo();
    }
    
    /**
     * Setter for property Tipo_Recuperacion_Anticipoe.
     * @param Tipo_Recuperacion_Anticipoe New value of property Tipo_Recuperacion_Anticipoe.
     */
    public void setTipo_Recuperacion_Anticipo(java.util.Vector Tipo_Recuperacion_Anticipoe) {
        trecuperacionDAO.setTipo_Recuperacion_Anticipo(Tipo_Recuperacion_Anticipoe );
    }
    
    
    public void insert() throws SQLException{
        trecuperacionDAO.insert();
    }
    
    public void search(String cod)throws SQLException{
        trecuperacionDAO.search(cod);
    }
    
    public boolean exist(String cod) throws SQLException{
        return trecuperacionDAO.exist(cod);
    }
    
    public void list()throws SQLException{
        trecuperacionDAO.list();
        
    }
    
    public void update() throws SQLException{
        trecuperacionDAO.update();
    }
    
    public void anular() throws SQLException{
        trecuperacionDAO.anular();
    }
    public void consultar(String codigo, String desc)throws SQLException{
        trecuperacionDAO.consultar(codigo, desc);
    }
    public java.util.TreeMap getTreeTRA() {
        return trecuperacionDAO.getTreeTRA();
    }
    
    /**
     * Setter for property treeTRA.
     * @param treeTRA New value of property treeTRA.
     */
    public void setTreeTRA(java.util.TreeMap treeTRA) {
        trecuperacionDAO.setTreeTRA(treeTRA);
    }
    public void llenarTree()throws SQLException{
        trecuperacionDAO.llenarTree();
    }
    
}
