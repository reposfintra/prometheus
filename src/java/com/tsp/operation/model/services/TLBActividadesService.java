/*
 * TLBActividadesService.java
 *
 * Created on 13 de octubre de 2005, 11:24 AM
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;


/**
 *
 * @author  LEONARDO PARODY
 */
public class TLBActividadesService {
    
    private TBLActividadesDAO ActividadDataAccess; 
    /** Creates a new instance of TLBActividadesService */
    
     //Tito Andr�s 201005
    private String  varCamposJS;
    private String  varSeparadorJS = "~";
    
    public TLBActividadesService() {
        ActividadDataAccess = new TBLActividadesDAO(); 
    }
    
    public void InsertarActividad(TBLActividades actividad) throws Exception{
                       
        try{
            this.ActividadDataAccess.InsertarActividad(actividad);       
            }catch(Exception e){  
                throw new Exception( e.getMessage()); 
            }
    }
    public List ConsultarTBLActividades (String distrito)throws SQLException{
        try{
            return ActividadDataAccess.ConsultarTBLActividades(distrito);
        }
        catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
    public void AnularActividad(TBLActividades actividades) throws SQLException{
                       
        try{
            this.ActividadDataAccess.AnularActividad(actividades);       
            }catch(SQLException e){  
                throw new SQLException( e.getMessage()); 
            }
    }
    
    public TBLActividades ObtenerTBLActividad(String codigo, String distrito) throws SQLException{
        try{
            return ActividadDataAccess.ConsultarTBLActividad(codigo, distrito);
           }catch(SQLException e){  
                throw new SQLException( e.getMessage()); 
           }
    }
    public boolean ExisteTBLActividad(String codigo, String distrito) throws SQLException{
        try{
            return ActividadDataAccess.ExisteTBLActividad(codigo, distrito);
            }catch(SQLException e){  
                throw new SQLException( e.getMessage()); 
            }
    }
 /**
        *  @author Tito Andr�s
        *
        */
        public void GenerarJSCampos(String distrito) throws SQLException{
                String var = "\n var CamposJS2 = [ ";
                List acts = this.ConsultarTBLActividades(distrito);
                if (acts!=null)
                        for (int i=0; i<acts.size(); i++){
                                TBLActividades act = (TBLActividades) acts.get(i);
                                var += "\n '"+ act.getCodigo() + varSeparadorJS + act.getDescripcion() +"',";
                        }
                var = var.substring(0,var.length()-1) + "];";
                varCamposJS = var;
        }
        
        public String obtenerJSCampos(String distrito) throws SQLException{
                this.GenerarJSCampos(distrito);
                return this.varCamposJS;
        }   
}
