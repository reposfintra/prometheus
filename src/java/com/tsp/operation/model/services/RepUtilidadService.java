/********************************************************************
 *      Nombre Clase.................   RepUtilidadService.java
 *      Descripci�n..................   Service del reporte de utilidad
 *      Autor........................   Ing. Tito Andr�s Maturana De La Cruz
 *      Fecha........................   22 de febrero de 2006, 09:33 AM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  Ing. Tito Andr�s Maturana De La Cruz
 */
public class RepUtilidadService {
    
    RepUtilidadDAO dao;
    
    /** Crea una nueva instancia de  RepUtilidadService */
    public RepUtilidadService() {
        dao = new RepUtilidadDAO();
    }
    
    /**
     * Obtiene informacion espec�fica de las planillas despachadas en un per�odo determinado y de las remesas correspondientes
     * @autor Ing. Tito Andr�s Maturana D.
     * @param ano A�o del per�odo
     * @param mes Mes del per�odo
     * @param cia C�digo del distrito
     * @tipo 1- Carga General, 2 - Carb�n
     * @throws SQLException
     * @version 1.0.
     **/
    public void planillasPeriodo(String ano, String mes, String cia, String tipo) throws SQLException{
        dao.planillasPeriodo(ano, mes, cia, tipo);
    }
    
    /**
     * Getter for property rs.
     * @return Value of property rs.
     */
    public java.sql.ResultSet getRs() {
        return dao.getRs();
    }    
    
    /**
     * Setter for property rs.
     * @param rs New value of property rs.
     */
    public void setRs(java.sql.ResultSet rs) {
        dao.setRs(rs);
    }
    
    /**
     * Obtiene informacion espec�fica de las planillas perteneciente a una remesa
     * @autor Ing. Tito Andr�s Maturana D.
     * @param numrem N�mero de la remesa
     * @throws SQLException
     * @version 1.0.
     **/
    public void getPlanillas(String numrem) throws SQLException{
        dao.getPlanillas(numrem);
    }
    
    /**
     * Obtiene informacion espec�fica de las planillas perteneciente a una remesa
     * @autor Ing. Tito Andr�s Maturana D.
     * @param numpla N�mero de la planilla
     * @throws SQLException
     * @version 1.0.
     **/
    public void getRemesas(String numpla) throws SQLException{
        dao.getRemesas(numpla);
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return dao.getVector();
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        dao.setVector(vector);
    }
    
    /**
     * Getter for property repUtil.
     * @return Value of property repUtil.
     */
    public com.tsp.operation.model.beans.RepUtilidad getRepUtil() {
        return dao.getRepUtil();
    }
    
    /**
     * Setter for property repUtil.
     * @param repUtil New value of property repUtil.
     */
    public void setRepUtil(com.tsp.operation.model.beans.RepUtilidad repUtil) {
        dao.setRepUtil(repUtil);
    }
    
    /**
     * Crea una tabla temporal para la generacion del reporte
     * @autor Ing. Tito Andr�s Maturana D.
     * @param tblname Terminacion del nombre de la tabla _AAAA_MM_DD
     * @cia C�digo del Distrito
     * @throws SQLException
     * @version 1.0.
     **/
    public void crearTabla(String tblname, String cia) throws SQLException{
        dao.crearTabla(tblname, cia);
    }
    
    /**
     * Inserta un registro en la tabla temporal.
     * @autor Ing. Tito Andr�s Maturana D.
     * @throws SQLException
     * @version 1.0.
     **/
    public void insertar() throws SQLException{
        dao.insertar();
    }
    
    /**
     * Verifica la existencia de un registro en la tabla temporal.
     * @autor Ing. Tito Andr�s Maturana D.
     * @throws SQLException
     * @returns True si existe, false si no
     * @version 1.0.
     **/
    public boolean existe() throws SQLException{
        return dao.existe();
    }
    
    /**
     * Elimina la tabla temporal
     * @autor Ing. Tito Andr�s Maturana D.
     * @throws SQLException
     * @version 1.0.
     **/
    public void dropTable() throws SQLException{
        dao.dropTable();
    }
    
    /**
     * Lee la informacion del reporte de la tabla temporal
     * @autor Ing. Tito Andr�s Maturana D.
     * @throws SQLException
     * @version 1.0.
     **/
    public void leer(String cia, String periodo) throws SQLException{
        dao.leer(cia, periodo);
    }
}
