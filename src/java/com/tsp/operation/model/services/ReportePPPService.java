/*
 * ReportePPPService.java
 *
 * Created on 9 de marzo de 2007, 10:55 AM
 */

package com.tsp.operation.model.services;


import com.tsp.operation.model.DAOS.ReportePPPDAO;

import java.util.*;
/**
 *
 * @author  equipo
 */
public class ReportePPPService {
    
    ReportePPPDAO dao;
    Vector remesasNF;
    Vector remesasFNP;
    Vector remesasFP;
    
    /** Creates a new instance of ReportePPPService */
    public ReportePPPService() {
        dao = new ReportePPPDAO();
    }
    
    
    /**
     * Metodo para extraer las remesas pendientes por facturar 
     * @autor mfontalvo
     * @param fi, fecha inicial de busqueda
     * @param ff, fecha final de busqueda
     * @throws Exception.
     * @return Vector, remesas encontradas
     */
    public void obtenerRNF(String fi, String ff) throws Exception {
        remesasNF = dao.obtenerRNF(fi, ff);
    }
        
    
    /**
     * Metodo para extraer las remesas facturadas y no pagadas
     * @autor mfontalvo
     * @param fi, fecha inicial de busqueda
     * @param ff, fecha final de busqueda
     * @throws Exception.
     * @return Vector, remesas encontradas
     */
    public void obtenerRFNP(String fi, String ff) throws Exception {
        remesasFNP = dao.obtenerRFNP(fi, ff);
    }
    
    
    /**
     * Metodo para extraer las remesas facturadas y pagadas
     * @autor mfontalvo
     * @param fi, fecha inicial de busqueda
     * @param ff, fecha final de busqueda
     * @throws Exception.
     * @return Vector, remesas encontradas
     */
    public void obtenerRFP(String fi, String ff) throws Exception {
        remesasFP = dao.obtenerRFP(fi, ff);
    }
    
    
    
    /**
     * Metodo para extraer las remesas facturadas y pagadas
     * @autor mfontalvo
     * @param fi, fecha inicial de busqueda
     * @param ff, fecha final de busqueda
     * @throws Exception.
     * @return Vector, remesas encontradas
     */
    public String obtenerDatoTG(String table_code) throws Exception {
        return dao.obtenerDatoTG(table_code);
    }
    
    
    
    /**
     * Getter for property remesasNF.
     * @return Value of property remesasNF.
     */
    public java.util.Vector getRemesasNF() {
        return remesasNF;
    }    
    
    /**
     * Setter for property remesasNF.
     * @param remesasNF New value of property remesasNF.
     */
    public void setRemesasNF(java.util.Vector remesasNF) {
        this.remesasNF = remesasNF;
    }    

    /**
     * Getter for property remesasFNP.
     * @return Value of property remesasFNP.
     */
    public java.util.Vector getRemesasFNP() {
        return remesasFNP;
    }    
    
    /**
     * Setter for property remesasFNP.
     * @param remesasFNP New value of property remesasFNP.
     */
    public void setRemesasFNP(java.util.Vector remesasFNP) {
        this.remesasFNP = remesasFNP;
    }
    
    /**
     * Getter for property remesasFP.
     * @return Value of property remesasFP.
     */
    public java.util.Vector getRemesasFP() {
        return remesasFP;
    }
    
    /**
     * Setter for property remesasFP.
     * @param remesasFP New value of property remesasFP.
     */
    public void setRemesasFP(java.util.Vector remesasFP) {
        this.remesasFP = remesasFP;
    }
    
}
