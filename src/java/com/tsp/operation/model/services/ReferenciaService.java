/***********************************************************************************
 * Nombre clase :  ReferenciaService.java
 * Descripcion : Clase que maneja los Servicios    
 *                asignados a Model relacionados con el  
 *               programa de Referencia
 * Autor :    Ing. Henry Osorio           
 * Fecha :     26 de septiembre de 2005, 04:38 PM               
 * Version :   1.0                                             
 * Copyright : Fintravalores S.A.                         
 ***********************************************************************************/
 
package com.tsp.operation.model.services;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;

public class ReferenciaService {
    
    ReferenciaDAO dao = new ReferenciaDAO();
    /** Creates a new instance of ReferenciaService */
    public ReferenciaService() {
    }
    /**
     * Metodo insertarReferenciaEmpresaCarga, insertar los registros a la tabla referencia 
     *         de los conductores y/o propieatios.
     * @param: objeto Referencia
     * @see   : insertarReferenciaEmpresaCarga - ReferenciaDAO
     * @autor : Ing. Henry Osorio
     * @version : 1.0
     */
    public void insertarReferenciaEmpresaCarga(Referencia ref) throws SQLException{
        dao.insertarReferenciaEmpresaCarga(ref);
    }
    /**
     * Metodo insertarReferenciaConductorPropietario, insertar los registros a la tabla referencia 
     *         de los conductores y/o propieatios.
     * @param: objeto Referencia
     * @see   : insertarReferenciaConductorPropietario - ReferenciaDAO
     * @autor : Ing. Henry Osorio
     * @version : 1.0
     */
    public void insertarReferenciaConductorPropietario(Referencia ref) throws SQLException{
        dao.insertarReferenciaConductorPropietario(ref);
    }
    /**
     * Metodo insertarReferenciaEmpresaAfiliada, insertar los registros a la tabla referencia 
     *         para la empresa afiliadora,
     * @param: objeto Referencia
     * @see   : insertarReferenciaEmpresaAfiliada - ReferenciaDAO
     * @autor : Ing. Henry Osorio
     * @version : 1.0
     */
    public void insertarReferenciaEmpresaAfiliada(Referencia ref) throws SQLException{
        dao.insertarReferenciaEmpresaAfiliada(ref);
    }
    /**
     * Metodo updateReferencia, actualiza los registros a la tabla referencia 
     * @param: objeto Referencia
     * @autor : Ing. Henry Osorio
     * @see   : updateReferencia - ReferenciaDAO
     * @version : 1.0
     */
    public void updateReferencia(Referencia ref) throws SQLException{
        dao.updateReferencia(ref);
    }
    /**
     * Metodo getVectorReferencias, retorna el vector de tipo referencia
     * @param: objeto Referencia
     * @see   : getVectorReferencias - ReferenciaDAO
     * @autor : Ing. Henry Osorio
     * @version : 1.0
     */
    public Vector getVectorReferencias() {
        return dao.getVectorReferencias();
    }
    /**
     * Metodo buscarReferencias, busca el objeto referencia por medio
     *                           el documento
     * @param: objeto Referencia
     * @see   : buscarReferencias - ReferenciaDAO
     * @autor : Ing. Henry Osorio
     * @version : 1.0
     */
    public void buscarReferencias(String doc) throws SQLException{
        dao.buscarReferencias(doc);
    }
    /**
     * Metodo ocurrenciasTipoReferencia, cuenta el numero de referencias de acurdo al tipo,
     *        retorna una arreglo de enteros con la cantidad de referencia de cada tipo
     * @param: vector
     * @see   : buscarReferencias - ReferenciaDAO
     * @autor : Ing. Henry Osorio
     * @version : 1.0
     */
    public int[] getOcurrenciasTipoReferencias(Vector vec){
        return dao.ocurrenciasTipoReferencia(vec);
    }
    /**
     * Metodo existeEmpresaAfiliada, retorna la cantidad de registros que tiene 
     *    la tabla referencia deacuerdo al documento 
     * @param: objeto Referencia
     * @autor : Ing. Henry Osorio
     * @version : 1.0
     */
    public int existeEmpresaAfiliada(Referencia ref) throws SQLException {
        return dao.existeEmpresaAfiliada(ref);
    }
    /**
     * Metodo verificarReferencia, Verifica las referencias si es correcta o incorrecta,
     * actualizando el usuario que realiza la verificación  
     * @param: objeto Referencia
     * @see   : verificarReferencia - ReferenciaDAO()
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
     public void verificarReferencia(Referencia ref) throws SQLException {
         dao.verificarReferencia(ref);
     }
     /**
     * Metodo buscarReferencias, buscar la referencias del documento dependiendo el 
     * documento y el distrito    
     * @param: documento, distrito, tipo
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarReferencias(String doc, String dstrct, String tipo) throws SQLException {
        dao.buscarReferencias(doc,dstrct,tipo);
    }
}