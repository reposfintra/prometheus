/*
 * ViaService.java
 *
 * Created on 9 de diciembre de 2004, 04:19 PM
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.SendMail;
import com.tsp.operation.model.DAOS.SendMailDAO;
import java.sql.*;
import javax.servlet.http.HttpServletRequest;
import java.io.File;

/**
 *
 * @author  ARMANDO
 */
public class SendMailService {
    
    private SendMailDAO sendMailDataAccess;
    /** Creates a new instance of ViaService */
    public SendMailService() {
        sendMailDataAccess = new SendMailDAO();
    }
    
    public void sendMail(SendMail sendMail) throws SQLException{
        sendMailDataAccess.sendMail(sendMail);
    }
    
    public void sendMail(String from, String to, String subject, String body) throws SQLException{
        sendMailDataAccess.sendMail(from , to, subject, body); 
    }
    
    public synchronized String bugReportSend( HttpServletRequest httpRequest, Throwable bug ) {
        return sendMailDataAccess.bugReportSend(httpRequest, bug);
    }
        /**
 * Envia un email con adjuntos
 * @param sendMail El objeto con los datos del mail
 * @param file El archivo que se va a adjuntar
 * @autor: ....... David Pi�a L�pez
 * @throws ....... SQLException
 * @version ...... 1.0
 */
     public void sendMail(SendMail sendMail, File file) throws SQLException{
        sendMailDataAccess.sendMail( sendMail, file );
    }
    
   //ADDED June-19-2009  
   public String sendMailString(SendMail sendMail) throws SQLException{
        return sendMailDataAccess.sendMailString(sendMail);
   }
}
