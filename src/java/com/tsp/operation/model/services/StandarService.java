/*
 * StandarService.java
 *
 * Created on 9 de enero de 2007, 04:37 PM
 */

package com.tsp.operation.model.services;
import com.tsp.operation.model.DAOS.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
/**
 *
 * @author  Ivan Dario Gomez
 */
public class StandarService {
    private StandarDAO dao;
    private List lista;
    private List listaAgentes;
    private List tiporuta;
    private List tipoFacturacion;
    private List fronteraAsociada;
    private Cliente cliente;
    private Ciudad ciudad;
    private List listaCiudad;
    private List listaClientes;
    private Standar sj = null;
    private List listaUnidades;
    private List listaMonedas;
    private EquivalenciaCarga eqivalenciaCarga;
    private List listaCargas;
    private Recursosdisp recurso;
    private List listaRecursos;
    private List listaEstandares;
    
    /** Creates a new instance of StandarService */
    public StandarService() {
        dao          = new StandarDAO();
        lista        = new LinkedList();
        listaAgentes = new LinkedList();
        tiporuta     = new LinkedList();
    }
    public StandarService(String dataBaseName) {
        dao          = new StandarDAO(dataBaseName);
        lista        = new LinkedList();
        listaAgentes = new LinkedList();
        tiporuta     = new LinkedList();
    }
    
    /**
     * M�todo buscarTableCode  metodo para buscar datos en tablagen
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String nombre de la tabla
     * @see......... buscarTableCode - StantarDAO
     * @version.....1.0.
     **/
    public void buscarTableCode(String table_type)throws SQLException {
        this.setLista(dao.buscarTableCode(table_type));
        
    }
    
    /**
     * M�todo buscarUnidades  metodo para buscar datos en tablagen
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String nombre de la tabla
     * @see......... buscarTableCode - StantarDAO
     * @version.....1.0.
     **/
    public void buscarUnidades(String table_type)throws SQLException {
        this.setListaUnidades(dao.buscarTableCode(table_type));
        
    }
    
    /**
     * M�todo buscarTableCode  metodo para buscar datos en tablagen
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String nombre de la tabla
     * @see......... buscarTableCode - StantarDAO
     * @version.....1.0.
     **/
    public void buscarAgentesResponsable(String table_type)throws SQLException {
        this.setListaAgentes(dao.buscarTableCode(table_type));
        
    }
    
    
    /**
     * M�todo buscarTableCode  metodo para buscar datos en tablagen
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String nombre de la tabla
     * @see......... buscarTableCode - StantarDAO
     * @version.....1.0.
     **/
    public void buscarTiporuta(String table_type)throws SQLException {
        this.setTiporuta(dao.buscarTableCode(table_type));
        
    }
     /**
     * M�todo buscarTipoFacturacion  metodo para buscar datos en tablagen
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String nombre de la tabla
     * @see......... buscarTableCode - StantarDAO
     * @version.....1.0.
     **/
    public void buscarTipoFacturacion(String table_type)throws SQLException {
        this.setTipoFacturacion(dao.buscarTableCode(table_type));
        
    }
    
     /**
     * M�todo buscarFronteraAsociada  metodo para buscar datos en tablagen
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String nombre de la tabla
     * @see......... buscarTableCode - StantarDAO
     * @version.....1.0.
     **/
    public void buscarFronteraAsociada(String table_type)throws SQLException {
        this.setFronteraAsociada(dao.buscarTableCode(table_type));
        
    }
    
    /**
     * M�todo buscarCliente  metodo para buscar los datos de un cliente
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String nombre de la tabla
     * @version.....1.0.
     **/
    public void buscarCliente(String codcli)throws SQLException {
        this.setCliente(dao.buscarCliente(codcli));
    }
    
    
    /**
     * M�todo buscarCliente  metodo para buscar los datos de un cliente
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String nombre de la tabla
     * @version.....1.0.
     **/
    public void buscarCarga(String codcarga)throws SQLException {
        this.setEqivalenciaCarga(dao.buscarCarga(codcarga));
    }
    
    /**
     * M�todo buscarCiudad  metodo para buscar la ciudad
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String nombre de la tabla
     * @version.....1.0.
     **/
    public void buscarCiudad(String nomciu)throws SQLException {
        this.setListaCiudad(dao.buscarCiudad(nomciu));
    }
    
    
    /**
     * M�todo buscarCliente  metodo para buscar los datos de un cliente
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String nombre de la tabla
     * @version.....1.0.
     **/
    public void buscarClientePorNombre(String nomcli)throws SQLException {
        this.setListaClientes(dao.buscarClientePorNombre(nomcli));
    }
    
    /**
     * M�todo buscarCargaPorDescripcion  metodo para buscar los datos de la carga
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String descripcion de la carga
     * @version.....1.0.
     **/
    public void buscarCargaPorDescripcion(String desc)throws SQLException {
        this.setListaCargas(dao.buscarCargaPorDescripcion(desc));
    }
    
    
    /**
     * M�todo buscarCiudadPorCodigo  metodo para buscar la ciudad
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String nombre de la tabla
     * @version.....1.0.
     **/
    public void buscarCiudadPorCodigo(String codciu)throws SQLException {
        this.setCiudad(dao.buscarCiudadPorCodigo(codciu));
    }
    
    /**
     * M�todo buscarMonedas  metodo para buscar las monedas
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......
     * @version.....1.0.
     **/
    public void buscarMonedas()throws SQLException {
        this.setListaMonedas(dao.buscarMonedas());
    }
    
    /**
     * M�todo existeSj  metodo para saber si existe el standar
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String distrito y String codigo del estandar
     * @version.....1.0.
     **/
    public boolean existeSj(String dstrct, String cod_sj)throws SQLException {
        return  dao.existeSj(dstrct, cod_sj);
    }
    
    
    /**
     * M�todo buscarCiudad  metodo para buscar la ciudad
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String nombre de la tabla
     * @version.....1.0.
     **/
    public void grabarEstandar(Standar sj)throws SQLException {
        String sql =  dao.grabarEstandar(sj);
        TransaccionService tService = new TransaccionService(this.dao.getDatabaseName());
        tService.crearStatement();
        tService.getSt().addBatch(sql);
        tService.execute();
        
    }
    /**
     * M�todo updateEstandar  metodo para guardar el estandar
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......objeto Standar
     * @version.....1.0.
     **/
    public void updateEstandar(Standar sj)throws SQLException {
        String sql =  dao.updateEstandar(sj);
        TransaccionService tService = new TransaccionService(this.dao.getDatabaseName());
        tService.crearStatement();
        tService.getSt().addBatch(sql);
        tService.execute();
    }
    
    /**
     * M�todo buscarRecurso  metodo para buscar los datos de un recurso
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String codigo del recurso
     * @version.....1.0.
     **/
    public void buscarRecurso(String codrecurso)throws SQLException {
        this.setRecurso(dao.buscarRecurso(codrecurso));
    }
    
    /**
     * M�todo buscarRecursoPorDescripcion  metodo para buscar los datos de un recurso
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String codigo del recurso
     * @version.....1.0.
     **/
    public void buscarRecursoPorDescripcion(String codrecurso)throws SQLException {
        this.setListaRecursos(dao.buscarRecursoPorDescripcion(codrecurso));
    }
    
    /**
     * M�todo existeTramo  metodo para buscar el tramo
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String origen String destino
     * @version.....1.0.
     **/
    public boolean existeTramo(String origen, String destino)throws SQLException {
        return dao.existeTramo(origen, destino);
    }
    
    
    /**
     * M�todo ListarEstandares  metodo para buscar estandares
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String dstrct, String codsj, String codcli,String fechaini,String fechafin
     * @version.....1.0.
     **/
    public void ListarEstandares(String dstrct, String codsj, String codcli,String fechaini,String fechafin)throws SQLException {
        this.setListaEstandares(dao.ListarEstandares(dstrct, codsj, codcli, fechaini, fechafin));
    }
    
    
    /**
     * M�todo ListarEstandares  metodo para buscar estandares
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String codigo del recurso
     * @version.....1.0.
     **/
    public void buscarEstandar(String dstrct, String codsj, String codcli)throws SQLException {
        this.setSj(dao.buscarEstandar(dstrct, codsj, codcli));
    }
    /**
     * M�todo buscarCiudad  metodo para buscar la ciudad
     * @autor.......Ivan Dario Gomez Vanegas
     * @param.......String nombre de la tabla
     * @version.....1.0.
     **/
    public void setStandarVacio()throws SQLException {
        Standar standar = new Standar();
        standar.setOrigen("");
        standar.setCod_cli("");
        standar.setNomcli("");
        standar.setAg_duenia("");
        standar.setPagador("");
        standar.setUnidad_negocio("");
        standar.setCodigo_sj("");
        standar.setDescripcion_sj("");
        standar.setTipo("");
        standar.setStandar_general("");
        standar.setBloq_despacho(false);
        standar.setE_mail("");
        standar.setAg_responsable("");
        standar.setAprobado(false);
        standar.setPlaneable(false);
        standar.setNom_origen("");
        standar.setDestino("");
        standar.setNom_destino("");
        standar.setInd_vacio(false);
        standar.setReq_remesa_padre(false);
        standar.setRemesaFacturable(true);
        standar.setTipo_ruta("");
        standar.setCodigo_carga("");
        standar.setTipo_facturacion("");
        standar.setVlr_tope_carga(0);
        standar.setPeriodoFac_final(0);
        standar.setPeriodoFac_inicial(0);
        standar.setDesc_carga("");
        standar.setDesc_carga_corta("");
        standar.setFrontera_asoc("");
        
        
        // llenando la informacion de las tarifas
        Standar tarifa = new Standar();
        tarifa.setVlr_tarifa(0);
        tarifa.setMoneda_tarifa("PES");
        tarifa.setUnidad_tarifa("");
        tarifa.setVigente_tarifa(false);
        List listaTarifa = new LinkedList();
        listaTarifa.add(tarifa);
        
        standar.setListaTarifa(listaTarifa);
        
        /////////////////////////////////////////
        
        
        // llenando la informacion de tramos
        Standar tramos = new Standar();
        
        tramos.setCod_origen_tramo("");
        tramos.setCodigo_destino_tramo("");
        tramos.setNombre_origen_tramo("");
        tramos.setNombre_destino_tramo("");
        tramos.setPorcentaje_tramo("");
        tramos.setPeso_lleno_tramo("");
        tramos.setNumero_escoltas(0);
        tramos.setTipo_trafico("MAS");
        tramos.setInd_vacio_tramo(false);
        tramos.setControl_cargue(false);
        tramos.setControl_descargue(false);
        tramos.setCaravana(false);
        tramos.setRequiere_planviaje(false);
        tramos.setPreferencia(false);
        tramos.setRequiere_hojareporte(false);
        tramos.setContingente(false);
        
        //Informacion de los recursos
        Standar recursos = new Standar();
        
        recursos.setCodigo_recurso("");
        recursos.setTipo_recurso("");
        recursos.setDescripcion_recurso("");
        recursos.setPrioridad(0);
        
        //Informacion de los fletes
        Standar fletes = new Standar();
        
        fletes.setVlr_flete(0);
        fletes.setMoneda_flete("PES");
        fletes.setUnidad_flete("");
        fletes.setVigente_flete(false);
        
        
        List listaFletes = new LinkedList();
        listaFletes.add(fletes);
        recursos.setListaFletes(listaFletes);
        /////////////////////////////////////
        
        
        List listaRecursos = new LinkedList();
        listaRecursos.add(recursos);
        
        tramos.setListaRecursos(listaRecursos);
        ///////////////////////////////////
        
        
        
        
        List listaTramos = new LinkedList();
        listaTramos.add(tramos);
        
        standar.setListaTramos(listaTramos);
        
        /////////////////////////////////////////
        
        
        this.setSj(standar);
        
        
        
        
    }
    
    
    
    /**
     * Getter for property lista.
     * @return Value of property lista.
     */
    public java.util.List getLista() {
        return lista;
    }
    
    /**
     * Setter for property lista.
     * @param lista New value of property lista.
     */
    public void setLista(java.util.List lista) {
        this.lista = lista;
    }
    
    /**
     * Getter for property listaAgentes.
     * @return Value of property listaAgentes.
     */
    public java.util.List getListaAgentes() {
        return listaAgentes;
    }
    
    /**
     * Setter for property listaAgentes.
     * @param listaAgentes New value of property listaAgentes.
     */
    public void setListaAgentes(java.util.List listaAgentes) {
        this.listaAgentes = listaAgentes;
    }
    
    /**
     * Getter for property tiporuta.
     * @return Value of property tiporuta.
     */
    public java.util.List getTiporuta() {
        return tiporuta;
    }
    
    /**
     * Setter for property tiporuta.
     * @param tiporuta New value of property tiporuta.
     */
    public void setTiporuta(java.util.List tiporuta) {
        this.tiporuta = tiporuta;
    }
    
    /**
     * Getter for property cliente.
     * @return Value of property cliente.
     */
    public com.tsp.operation.model.beans.Cliente getCliente() {
        return cliente;
    }
    
    /**
     * Setter for property cliente.
     * @param cliente New value of property cliente.
     */
    public void setCliente(com.tsp.operation.model.beans.Cliente cliente) {
        this.cliente = cliente;
    }
    
    /**
     * Getter for property ciudad.
     * @return Value of property ciudad.
     */
    public com.tsp.operation.model.beans.Ciudad getCiudad() {
        return ciudad;
    }
    
    /**
     * Setter for property ciudad.
     * @param ciudad New value of property ciudad.
     */
    public void setCiudad(com.tsp.operation.model.beans.Ciudad ciudad) {
        this.ciudad = ciudad;
    }
    
    /**
     * Getter for property listaCiudad.
     * @return Value of property listaCiudad.
     */
    public java.util.List getListaCiudad() {
        return listaCiudad;
    }
    
    /**
     * Setter for property listaCiudad.
     * @param listaCiudad New value of property listaCiudad.
     */
    public void setListaCiudad(java.util.List listaCiudad) {
        this.listaCiudad = listaCiudad;
    }
    
    /**
     * Getter for property listaClientes.
     * @return Value of property listaClientes.
     */
    public java.util.List getListaClientes() {
        return listaClientes;
    }
    
    /**
     * Setter for property listaClientes.
     * @param listaClientes New value of property listaClientes.
     */
    public void setListaClientes(java.util.List listaClientes) {
        this.listaClientes = listaClientes;
    }
    
    /**
     * Getter for property sj.
     * @return Value of property sj.
     */
    public com.tsp.operation.model.beans.Standar getSj() {
        return sj;
    }
    
    /**
     * Setter for property sj.
     * @param sj New value of property sj.
     */
    public void setSj(com.tsp.operation.model.beans.Standar sj) {
        this.sj = sj;
    }
    
    /**
     * Getter for property listaUnidades.
     * @return Value of property listaUnidades.
     */
    public java.util.List getListaUnidades() {
        return listaUnidades;
    }
    
    /**
     * Setter for property listaUnidades.
     * @param listaUnidades New value of property listaUnidades.
     */
    public void setListaUnidades(java.util.List listaUnidades) {
        this.listaUnidades = listaUnidades;
    }
    
    /**
     * Getter for property listaMonedas.
     * @return Value of property listaMonedas.
     */
    public java.util.List getListaMonedas() {
        return listaMonedas;
    }
    
    /**
     * Setter for property listaMonedas.
     * @param listaMonedas New value of property listaMonedas.
     */
    public void setListaMonedas(java.util.List listaMonedas) {
        this.listaMonedas = listaMonedas;
    }
    
    /**
     * Getter for property eqivalenciaCarga.
     * @return Value of property eqivalenciaCarga.
     */
    public com.tsp.operation.model.beans.EquivalenciaCarga getEqivalenciaCarga() {
        return eqivalenciaCarga;
    }
    
    /**
     * Setter for property eqivalenciaCarga.
     * @param eqivalenciaCarga New value of property eqivalenciaCarga.
     */
    public void setEqivalenciaCarga(com.tsp.operation.model.beans.EquivalenciaCarga eqivalenciaCarga) {
        this.eqivalenciaCarga = eqivalenciaCarga;
    }
    
    /**
     * Getter for property listaCargas.
     * @return Value of property listaCargas.
     */
    public java.util.List getListaCargas() {
        return listaCargas;
    }
    
    /**
     * Setter for property listaCargas.
     * @param listaCargas New value of property listaCargas.
     */
    public void setListaCargas(java.util.List listaCargas) {
        this.listaCargas = listaCargas;
    }
    
    /**
     * Getter for property recurso.
     * @return Value of property recurso.
     */
    public com.tsp.operation.model.beans.Recursosdisp getRecurso() {
        return recurso;
    }
    
    /**
     * Setter for property recurso.
     * @param recurso New value of property recurso.
     */
    public void setRecurso(com.tsp.operation.model.beans.Recursosdisp recurso) {
        this.recurso = recurso;
    }
    
    /**
     * Getter for property listaRecursos.
     * @return Value of property listaRecursos.
     */
    public java.util.List getListaRecursos() {
        return listaRecursos;
    }
    
    /**
     * Setter for property listaRecursos.
     * @param listaRecursos New value of property listaRecursos.
     */
    public void setListaRecursos(java.util.List listaRecursos) {
        this.listaRecursos = listaRecursos;
    }
    
    /**
     * Getter for property listaEstandares.
     * @return Value of property listaEstandares.
     */
    public java.util.List getListaEstandares() {
        return listaEstandares;
    }
    
    /**
     * Setter for property listaEstandares.
     * @param listaEstandares New value of property listaEstandares.
     */
    public void setListaEstandares(java.util.List listaEstandares) {
        this.listaEstandares = listaEstandares;
    }
    
    /**
     * Getter for property tipoFacturacion.
     * @return Value of property tipoFacturacion.
     */
    public java.util.List getTipoFacturacion() {
        return tipoFacturacion;
    }
    
    /**
     * Setter for property tipoFacturacion.
     * @param tipoFacturacion New value of property tipoFacturacion.
     */
    public void setTipoFacturacion(java.util.List tipoFacturacion) {
        this.tipoFacturacion = tipoFacturacion;
    }
    
    /**
     * Getter for property fronteraAsociada.
     * @return Value of property fronteraAsociada.
     */
    public java.util.List getFronteraAsociada() {
        return fronteraAsociada;
    }
    
    /**
     * Setter for property fronteraAsociada.
     * @param fronteraAsociada New value of property fronteraAsociada.
     */
    public void setFronteraAsociada(java.util.List fronteraAsociada) {
        this.fronteraAsociada = fronteraAsociada;
    }
    
}
