/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import java.util.*;

import java.sql.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.Model;
import com.tsp.util.Util;
import java.text.*;



/**
 *
 * @author Alvaro
 */


public class EstadoFinancieroService {

    private EstadoFinancieroDAO estadoFinancieroDao;
    // private Util utilidades;



    /** Creates a new instance of ClienteService */
    public EstadoFinancieroService() {
        estadoFinancieroDao = new EstadoFinancieroDAO();
        // utilidades = new Util();
    }
    public EstadoFinancieroService(String dataBaseName) {
        estadoFinancieroDao = new EstadoFinancieroDAO(dataBaseName);
        // utilidades = new Util();
    }


    public List getEstadoFinanciero1(String distrito, String anio, String informe,double secuencia1,double secuencia2) throws SQLException {

        return estadoFinancieroDao.getEstadoFinanciero1( distrito,  anio, informe,secuencia1,secuencia2);
    }

    public void creaMayorTercero(String distrito, String anio, String informe, String unidad) throws SQLException {
        estadoFinancieroDao.creaMayorTercero(distrito, anio, informe, unidad);
    }

    public void creaMovimientoTercero(String distrito, String anio, String informe, String unidad) throws SQLException {
        estadoFinancieroDao.creaMovimientoTercero(distrito, anio, informe, unidad);
    }

    public void setMayorTercero(String ano, String mes) throws SQLException {
        estadoFinancieroDao.setMayorTercero(ano,mes);
    }
    
public void creaMayorBalance(String distrito,String anio,String mes)  throws SQLException {
        
        int mesFinal = Integer.valueOf(mes).intValue();
        String valorAcumulado = "saldoant";
        for (int i=1; i<= mesFinal ; i++ ) {
            String mesProceso = Util.ceroPad(i,2);
            valorAcumulado = valorAcumulado + "+movdeb" + mesProceso + "-movcre" + mesProceso;
        }

        String valorAcumuladoAnioAnterior = "saldoant";
        for (int i=1; i<=13 ; i++ ) {
            String mesProceso = Util.ceroPad(i,2);
            valorAcumuladoAnioAnterior = valorAcumuladoAnioAnterior + "+movdeb" + mesProceso + "-movcre" + mesProceso;
        }

        estadoFinancieroDao.creaMayorBalance(distrito,anio, valorAcumulado, valorAcumuladoAnioAnterior);
    }



    public List getEstadoFinanciero2(String distrito, String informe) throws SQLException {

        return estadoFinancieroDao.getEstadoFinanciero2( distrito,  informe);
    }
    
    public List validaCuadreContable(String distrito,String anio,String mes)throws SQLException {
         return estadoFinancieroDao.validaCuadreContable(distrito,anio, mes);
    }

    public boolean validaExistenciaComprobante(String distrito,String tipo_documento, String documento)throws SQLException{

        return estadoFinancieroDao.validaExistenciaComprobante ( distrito, tipo_documento,  documento);
    }

    public int getSecuencia(String nombreSecuencia)throws SQLException{
        return estadoFinancieroDao.getSecuencia(nombreSecuencia);
    }

    public void creaResumenPuc(String distrito,String anio,String mes)throws SQLException {
        estadoFinancieroDao.creaResumenPuc(distrito, anio, mes);
    }

    public void creaResumenPucAnual(String distrito,String anio,String mes,  final String CUENTA_UTILIDAD)throws SQLException {
        estadoFinancieroDao.creaResumenPucAnual(distrito, anio, mes, CUENTA_UTILIDAD);
    }


    public String setCabecera(String distrito, String usuario, String anio, String mesCaracter, int grupoTransaccion, String detalle, String numero) throws SQLException{

        return estadoFinancieroDao.setCabecera( distrito,  usuario,  anio,  mesCaracter,  grupoTransaccion, detalle, numero);
    }


    public String setDetalle(String distrito, String usuario, String anio, String mesCaracter, int grupoTransaccion, String detalle, String numero) throws SQLException{

        return estadoFinancieroDao.setDetalle( distrito,  usuario,  anio,  mesCaracter,  grupoTransaccion, detalle, numero);
    }

    public String eliminaComprobante(String tipoDocumento, String documento)throws SQLException{
        return estadoFinancieroDao.eliminaComprobante( tipoDocumento,  documento);
    }


    public String setMayor(String distrito, String usuario, String anio, String mes, int grupoTransaccion, final String NUMERO_CD) throws SQLException {
        return estadoFinancieroDao.setMayor(distrito, usuario, anio, mes, grupoTransaccion, NUMERO_CD);

    }


    public String setReversaMayor(String distrito, String usuario, String anio, String mes, final String NUMERO_CD) throws SQLException {
        return estadoFinancieroDao.setReversaMayor(distrito, usuario, anio, mes, NUMERO_CD);

    }

    public String trasladaSaldos(String distrito, String usuario, String anio, String base) throws SQLException {
        return estadoFinancieroDao.trasladaSaldos( distrito,  usuario,  anio, base);
    }






}