/******************************************************************************
 * Nombre clase :      ReporteFacturasClientesService.java                    *
 * Descripcion :       Service del ReporteFacturasClientesService.java        *
 * Autor :             LREALES                                                *
 * Fecha :             29 de noviembre de 2006, 04:00 PM                     *
 * Version :           1.0                                                    *
 * Copyright :         Fintravalores S.A.                                *
 ******************************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;

public class ReporteFacturasClientesService {
    
    private ReporteFacturasClientesDAO reporte;
    
    /** Creates a new instance of ReporteFacturasClientesService */
    public ReporteFacturasClientesService() {        
        reporte = new ReporteFacturasClientesDAO ();
    }
    public ReporteFacturasClientesService(String dataBaseName) {        
        reporte = new ReporteFacturasClientesDAO(dataBaseName);
    }
    
    /** Funcion publica que obtiene el metodo getVector del DAO */
    public Vector getVectorReporte () throws Exception {
        
        return reporte.getVector();
        
    }
    
    /** Funcion publica que obtiene el metodo getOtro_vector del DAO */
    public Vector getVectorFactura () throws Exception {
        
        return reporte.getOtro_vector();
        
    }
     
    /** Funcion publica que obtiene el metodo reporte del DAO */
   public void reporte ( String codcli, String fecini, String fecfin, String numfac, String numrem, String usu_creacion, String fec_creacion, String opcion,String prov,String est,String fven,String est1 ) throws Exception {
        
        reporte.reporte ( codcli, fecini, fecfin, numfac, numrem, usu_creacion, fec_creacion, opcion,prov,est,fven,est1);
        
    }
    
    /** Funcion publica que obtiene el metodo buscarFactura del DAO */
    public void buscarFactura ( String distrito, String tipo_documento, String documento ) throws Exception {
        
        reporte.buscarFactura ( distrito, tipo_documento, documento );
        
    }
    
    public void reporte ( String codcli, String fecini, String fecfin, String numfac, String numrem, String usu_creacion, String fec_creacion, String opcion,String prov,String est,String fven,String est1,String multi ) throws Exception {
        
        reporte.reporte ( codcli, fecini, fecfin, numfac, numrem, usu_creacion, fec_creacion, opcion,prov,est,fven,est1,multi);
        
    }
    
}