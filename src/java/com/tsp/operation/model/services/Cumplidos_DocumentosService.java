/*
 * Cumplidos_DocumentosService.java
 *
 * Created on 27 de octubre de 2005, 06:07 PM
 */

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  Jose
 */
public class Cumplidos_DocumentosService {
    private Cumplidos_DocumentosDAO cumplidos_documentos;
    /** Creates a new instance of Cumplidos_DocumentosService */
    public Cumplidos_DocumentosService() {
        cumplidos_documentos = new Cumplidos_DocumentosDAO();
    }
        public Cumplidos_documentos getCumplidos_Documento( )throws SQLException{
        return cumplidos_documentos.getCumplidos_Documento();
    }
    
    public Vector getCumplidos_Documentos() {
        return cumplidos_documentos.getCumplidos_Documentos();
    }
    
    public void setCumplidos_Documentos(Vector Codigo_discrepancias) {
        cumplidos_documentos.setCumplidos_Documentos(Codigo_discrepancias);
    }
    
    public void insertCumplidos_Documentos(Cumplidos_documentos user) throws SQLException{
        try{
            cumplidos_documentos.setCumplidos_Documento(user);
            cumplidos_documentos.insertCumplidos_Documentos();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public boolean existCumplidos_Documentos(String numpla, String numrem, String document_type, String std_job_no,String distrito) throws SQLException{
        try{
            return cumplidos_documentos.existCumplidos_Documentos(numpla, numrem, document_type, std_job_no, distrito);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }  
}
