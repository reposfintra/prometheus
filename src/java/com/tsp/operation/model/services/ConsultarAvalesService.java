/**
* Autor : Ing. Roberto Rocha P..
* Date  : 10 de Julio de 2007
* Copyrigth Notice : Fintravalores S.A. S.A
* Version 1.0
-->
<%--
-@(#)
--Descripcion : Services que maneja los avales de Fenalco
**/

package com.tsp.operation.model.services;

import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.*;

public class ConsultarAvalesService {
    
    private Avales_FenalcoDAO list;
    private String a;
    private Aval_Fenalco Aval;
    private Vector Avales;
    //private Avales_FenalcoDAO AvalesData; 
    
    public ConsultarAvalesService() {
        list= new Avales_FenalcoDAO();
    }
    public ConsultarAvalesService(String dataBaseName) {
        list= new Avales_FenalcoDAO(dataBaseName);
    }
    
    public void listado(String nid,String us ) throws ServletException, InformationException,Exception{
      try{   
        list.ListadoAvales(nid,us);
        //System.out.println( "Entro al listado" );
       }
        catch(SQLException e){
        throw new SQLException (e.getMessage ());
        }
    }

     
    public Vector getListado(){
        return   list.getAvales();
    }
     
    public String verificar()
    {
        return list.verificar();
    }
    /*public static void main(String arg[]) throws Exception
	{
	    ConsultarAvalesService test;
            test=new ConsultarAvalesService() ;
            //test.listado();
	}*/

}
