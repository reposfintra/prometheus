 /**************************************************************************
 * Nombre clase: ReporteDrummondService.java                               *
 * Descripci�n: Clase que maneja los servicios de las consultas de los     *
 * reportes del cliente  DRUNMONND                                         *
 * Autor: Ing. Ivan DArio Gomez Vanegas                                    *
 * Fecha: Created on 1 de octubre de 2005, 08:20 AM                        *
 * Versi�n: Java 1.0                                                       *
 * Copyright: Fintravalores S.A. S.A.                                 *
 ***************************************************************************/
 
package com.tsp.operation.model.services;

import java.util.*;
import com.tsp.operation.model.DAOS.*;
/**
 *
 * @author  Igomez
 */
public class ReporteDrummondService {
    private ReporteDrummondDAO ReporteDrummondDataAccess;   
    private List listadoCliente;
    private List Reporte;
    
    
    /** Creates a new instance of ReporteDrummondService */
    public ReporteDrummondService() {
        ReporteDrummondDataAccess = new ReporteDrummondDAO(); 
        listadoCliente            = new LinkedList();
        Reporte                   = new LinkedList();
    }
    
     public void searchClientes() throws Exception {
        try{
            this.listadoCliente = ReporteDrummondDataAccess.searchCliente(); 
        }catch(Exception e){
            throw new Exception("Error en searchClientes ReporteDrummondService]...\n"+e.getMessage());
        }
    }
   
     public void searchReporte(String FecIni, String FecFin, String CodCliente) throws Exception {
        try{
            this.Reporte = ReporteDrummondDataAccess.searchReporte(FecIni, FecFin, CodCliente);  
        }catch(Exception e){
            throw new Exception("Error en searchReporte ReporteDrummondService]...\n"+e.getMessage());
        }
    }
    public List getListadoCliente(){ return this.listadoCliente; }
    public List getReporte(){ return this.Reporte; }
}
