/*
 * PdfImprimirService.java
 *
 * Created on 15 de enero de 2008, 06:00 PM
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;

import java.awt.Color;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Vector;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Calendar;
import java.util.Date;

import com.lowagie.text.Rectangle;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Paragraph;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.Font;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.Instrucciones_DespachoDAO;
import java.util.*;
import java.io.*;
import com.lowagie.text.Image;

import java.awt.Color;
/**
 *
 * @author  Administrador
 */
public class PdfImprimirService2 {
    
    private String nit = "";//nit del cliente
    private String nombre_cliente = "";//nombre del cliente
    private String direccion = "";//direccion del cliente
    private String telefono = "";//telefono del cliente
    private String telcontacto = "";//telefono contacto
    private String banco = "";//numero del banco
    private String aval = "";//numero de aprovacion
    private String cuenta_cheque="";//numero de cuenta
    private String beneficiario  ="";//nombre del beneficiario
    private String numero_cheque  ="";//numero del cheque
    private String numero_factura = "";//numero de la factura
    private String valor  ="";//valor del cheque
    private String[] observaciones = null;//Observaciones del la factura
    
    int diay = 0;
    int mesy = 0;
    int anoy = 0;	
    
    int enum_letras=0;
    String ecc_representante="";
    String erepresentada="";
    String ecc_deudor="";
    String ecc_codeudor="";
    String eswcodeudor="";
    
    Document document;
    
    int n=0;
    
    int contador=0;
    int cantidad_de_letras=7;
    PdfPTable table;
    
    float[] widths = {0.12f, 0.17f, 0.1f, 0.27f, 0.17f, 0.17f};
    float[] widthsCabecera = {0.2f, 0.3f, 0.15f, 0.35f};
    
    BaseFont bf;
    
    Font fuente_normal    = new Font(bf, 9);
    Font fuente_normal_grande   = new Font(bf, 20);
    Font fuente_negrita_grande = new Font(bf, 20f);
    Font fuente_negrita8 = new Font(bf, 9);
    
    Font fuente_normal_media = new Font(bf,14);//jemartinez
    Font fuente_negrita_media = new Font(bf,14);//jemartinez
    
    PdfWriter writer;
    Rectangle page;
    
    PdfPCell linea_blanco;
    SimpleDateFormat fmt = null;
    String ruta = "";
    //Instrucciones_DespachoDAO dao = new Instrucciones_DespachoDAO();
    
    //Variables de datos
    String orden = "";
    Model model;
    File pdf;
    Model modelito;
    
    BeanGeneral bg;
    
    String x="xxx";
    Phrase texto_principal;
    
    String no_aprobacion="",ciudad="",fecha_otorgamiento="",fecha_vencimiento="",consecutivo="",digito="",nombre="",cedula="",ciudad_cedula="",representado="",nombre_codeudor="",cedula_codeudor="",dia_pagare="",mes_pagare="",ano_pagare="",empresa="",plata_letra="",plata_numero="",empresa_facultada="",ciudad_pago="",cedula_deudor="",cedula_firma_codeudor="";
    
    String fecha_otorgamiento_inicial="";//mar14d8
    
    java.util.Calendar Fecha_Negocio ;
    java.util.Calendar Fecha_Temp ;
    
    ResourceBundle rb;
    
    Phrase frase;
    //Phrase frase2;
    
    /** Creates a new instance of PdfImprimirService */
    
    public PdfImprimirService2() throws Exception {
        bf = BaseFont.createFont( BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED );
        fuente_negrita_grande.setStyle( com.lowagie.text.Font.BOLD );
        fuente_negrita8.setStyle( com.lowagie.text.Font.BOLD );
        fuente_negrita_media.setStyle(com.lowagie.text.Font.BOLD); //jemartinez
    }
    
    public void iniciar() throws Exception {
        contador=0;
        rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        
        n=n+1;
        //System.out.println("n"+n);
        this.ruta = rb.getString("ruta")+"/pdf/rotu"+n+".pdf";
        
        document            = new Document();
        
        PdfWriter.getInstance(document, new FileOutputStream( this.ruta ));
        //System.out.println("n2"+n);
        page    = document.getPageSize();
        //System.out.println("n2"+n);
        document.open();
        //System.out.println("n2"+n);
    }
    
    public void reporteLetras(Model model ) throws Exception{
        iniciar();
        
        this.modelito      = model;
        try{
            this.escribirLetras( );
            document.close();
            //System.out.println("TERMINO");
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    
    public void escribirLetras(  ) throws Exception{
        
        try {
            float[] anchos = {0.05f,1.0f,0.05f};//porcentajes de ancho de columnas de una tabla
            //System.out.println("n3"+n);
            document.newPage();//nueva pag
            //System.out.println("n4"+n);
            
            while (contador<cantidad_de_letras){
                //for(int i=1;i<=5;i++){//desde 1 hasta n
                for(int j=1;j<=2;j++){//2 en cada pag
               
                    if (contador<cantidad_de_letras){//si no ha terminado
                        
                        Fecha_Temp = Util.TraerFecha(contador+1, diay, mesy, anoy, "30");//mar31d8
                        //Fecha_Temp.add(2,contador+1);//mar14d8
                        //Fecha_Temp.add(2,1);
                        
                        fecha_vencimiento=Util.crearStringFechaDateLetraRemix(Fecha_Temp);//mar31d8                        
                        //fecha_vencimiento=Util.crearStringFechaDateLetra2(Fecha_Temp);
                        
                        int mesmostrabletem=(Integer.valueOf(fecha_vencimiento.substring(3,5)).intValue()%12)+1;
                        String mesmostrable="";
                        if (mesmostrabletem<10){
                                mesmostrable="0"+mesmostrabletem;
                        }else{
                                mesmostrable=""+mesmostrabletem;
                        }
                        int anomostrable=				Integer.valueOf(fecha_vencimiento.substring(6,10)).intValue();
                        if (mesmostrable.equals("01") ){
                                anomostrable=anomostrable+1;
                        }
                        String temxxx=fecha_vencimiento.substring(0,2)+"-"+mesmostrable+"-"+anomostrable;
                        
                        dia_pagare=		fecha_vencimiento.substring(0,2);//dia de la fecha de vencimiento                        
                        
                        mes_pagare=Util.NombreMes3((Integer.valueOf(fecha_vencimiento.substring(3,5)).intValue()%12)+1);//mar31d8         
                        
                        //mes_pagare=		fecha_vencimiento.substring(3,5);//mes de la fecha de vencimiento                        
                        //mes_pagare=Util.NombreMes3(Integer.valueOf(mes_pagare).intValue());
                        
                        
                        //ano_pagare=		fecha_vencimiento.substring(6,10);//a�o del mes de vancimiento
                        ano_pagare=""+anomostrable;
                        
                        int cols=6;//cantidad de filas de 1 tabla
                        //float porcent=0.01f;

                        //System.out.println("op1");
                        PdfPTable table = new PdfPTable(cols);//se crea una tabla de 6 columnas
                        //System.out.println("op2");
                        table.setWidthPercentage(100);//uso de 100% horizontal

                        PdfPCell cell    = new PdfPCell();//se crea una celda
                        
                        PdfPCell cell_vacia_izq    = new PdfPCell();//se crea una celda
                        //System.out.println("op3");
                        cell_vacia_izq.setBorderWidthTop(0);//borde
                        cell_vacia_izq.setBorderWidthBottom(0);//borde
                        cell_vacia_izq.setBorderWidthLeft(1);//borde
                        cell_vacia_izq.setBorderWidthRight(0);//borde
                        //System.out.println("op4");
                        cell_vacia_izq.setPhrase  ( new Phrase("", fuente_normal ) );//celda vacia
                        //System.out.println("op5");
                        PdfPCell cell_vacia_der   = new PdfPCell();//se crea una celda
                        cell_vacia_der.setBorderWidthTop(0);//borde
                        cell_vacia_der.setBorderWidthBottom(0);//borde
                        cell_vacia_der.setBorderWidthLeft(0);//borde
                        cell_vacia_der.setBorderWidthRight(1);//borde
                        cell_vacia_der.setPhrase  ( new Phrase("", fuente_normal ) );//celda vacia
                        PdfPCell cell_vacia3    = new PdfPCell();//se crea una celda
                        cell_vacia3.setColspan(3);//celda gasta 2 columnas
                        cell_vacia3.setBorderWidthTop(0);//borde
                        cell_vacia3.setBorderWidthBottom(0);//borde
                        cell_vacia3.setBorderWidthLeft(1);//borde
                        cell_vacia3.setBorderWidthRight(1);//borde
                        cell_vacia3.setPhrase  ( new Phrase("", fuente_normal ) );//celda vacia
                        
                        //cell.setBorderWidthTop(1); 
                        //System.out.println("op6");
                        Image jpg = Image.getInstance("C:/Tomcat5/webapps/fintravalores/images/fen.jpg");//se crea una imagen
                        //System.out.println("op7");
                        //jpg.scalePercent(porcent);
                        jpg.scaleToFit(35,25);//se reduce la imagen
                        //jpg.setBorder(0);
                        //jpg.setBorderColor(Color.WHITE);
                        cell.setImage(jpg);//se mete la imagen en la celda

                        cell.setBorderWidthTop(1);//borde
                        cell.setBorderWidthBottom(0);//borde
                        cell.setBorderWidthLeft(1);//borde
                        cell.setBorderWidthRight(0);//borde

                        table.addCell(cell);//se mete la celda de la imagen

                        //cell.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );
                        
                        cell.setBorderWidthTop(1);//borde
                        cell.setBorderWidthBottom(0);//borde
                        cell.setBorderWidthLeft(0);//borde
                        cell.setBorderWidthRight(0);//borde
                        cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda vacia
                        table.addCell(cell);//se mete celda vacia

                        cell.setPhrase  ( new Phrase("LETRA DE CAMBIO SIN PROTESTO" , fuente_normal ) );//celda con texto
                        cell.setColspan(2);//celda gasta 2 columnas
                        
                        cell.setVerticalAlignment( PdfPCell.ALIGN_MIDDLE );//alineamiento vertical
                        cell.setHorizontalAlignment( PdfPCell.ALIGN_CENTER );//alineamiento horizontal
                        
                        table.addCell(cell);//se mete celda con texto
                        
                        frase =  new Phrase("");
                        frase.add(  new Phrase("No Aprobacion ",fuente_normal ));
                        frase.add(  new Phrase(no_aprobacion, fuente_negrita8));
                                           
                        //cell.setPhrase  ( new Phrase("No Aprobacion "+no_aprobacion,fuente_normal));//celda con texto
                        cell.setColspan(2);
                        cell.setPhrase  ( frase);
                        
                        cell.setBorderWidthTop(1);//borde
                        cell.setBorderWidthBottom(0);//borde
                        cell.setBorderWidthLeft(0);//borde
                        cell.setBorderWidthRight(1);//borde

                        cell.setHorizontalAlignment( PdfPCell.ALIGN_CENTER );//alineamiento horizontal
                        cell.setVerticalAlignment( PdfPCell.ALIGN_MIDDLE );//alineamiento vertical

                        table.addCell(cell);//se mete celda con texto

                        //System.out.println("n5"+n);
                        document.add(table);//se mete la tabla en el documento
                        //System.out.println("n6"+n);
                        
                        table = new PdfPTable(anchos);//se crea segunda tabla
                        table.setWidthPercentage(100);//uso de 100% horizontal
                        
                        cell.setColspan(1);//celda gastara 1 columna
                        
                        cell.setBorderWidthTop(0);//borde
                        cell.setBorderWidthBottom(0);//borde
                        cell.setBorderWidthLeft(0);//borde
                        cell.setBorderWidthRight(0);//borde
                        
                        //cell.setFixedHeight(10);
                        cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia

                        table.addCell(cell_vacia_izq);//se mete celda vacia
                        
                        if (temxxx.startsWith("28")){//temporal
                            System.out.println("");
                            temxxx=temxxx.replaceAll("28", "29");
                        }

                        cell.setPhrase  ( new Phrase("(Ciudad y Fecha de Otorgamiento) "+ ciudad +" "+ fecha_otorgamiento +"                Fecha de Vencimiento            "+temxxx, fuente_normal ) );//celda con texto

                        cell.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );//alineamiento horizontal
                        table.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );//alineamiento vertical
                        table.addCell(cell);//se mete celda con texto

                        //table.completeRow() ;//
                        cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda vacia
                        table.addCell(cell_vacia_der);//se mete celda vacia

                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        
                        table.addCell(cell_vacia_izq);//se mete celda vacia

                        digito=""+(contador+1);
                        cell.setPhrase  ( new Phrase("(Consecutivo de Letras)       "+consecutivo+"-"+digito, fuente_normal ) );//celda con texto
                        table.addCell(cell);//se mete celda con texto

                        cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda vacia
                        table.addCell(cell_vacia_der);//se mete celda vacia
                        //table.completeRow() ;//

                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia

                        table.addCell(cell_vacia_izq);//se mete celda vacia

                        texto_principal=getTextoPrincipal();
                        
                        cell.setPhrase  ( texto_principal );//celda con texto
                        cell.setHorizontalAlignment( PdfPCell.ALIGN_JUSTIFIED );//alineamiento horizontal
                        table.addCell(cell);//se mete celda con texto

                        cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda vacia
                        table.addCell(cell_vacia_der);//se mete celda vacia
                        //table.completeRow() ;//

                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        
                        table.addCell(cell_vacia_izq);//se mete celda vacia

                        cell.setPhrase  ( new Phrase("Otorgada y Aceptada (art 676 y 685 C�digo de Comercio)",fuente_negrita8 ) );//celda con texto
                        cell.setHorizontalAlignment( PdfPCell.ALIGN_CENTER );//alineamiento horizontal
                        table.addCell(cell);//se mete celda con texto

                        table.addCell(cell_vacia_der);//se mete celda vacia
                        //table.completeRow() ;//

                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia

                        table.addCell(cell_vacia_izq);//se mete celda vacia

                        frase =  new Phrase("");
                        frase.add(  new Phrase("Firma Deudor _______________________ Documento de Identidad ",fuente_normal ));
                        frase.add(  new Phrase(cedula_deudor, fuente_negrita8));
                        frase.add(  new Phrase("  Huella",fuente_normal ));
                        cell.setPhrase  ( frase);
                        
                        //cell.setPhrase  ( new Phrase("Firma Deudor _______________________ Documento de Identidad " +cedula_deudor + "  Huella", fuente_normal ) );//celda con texto
                        
                        cell.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );//alineamiento horizontal
                        table.addCell(cell);//se mete celda con texto
                        cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda vacia
                        table.addCell(cell_vacia_der);//se mete celda vacia

                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia

                        table.addCell(cell_vacia_izq);//se mete celda vacia

                        frase =  new Phrase("");
                        frase.add(  new Phrase("Firma Codeudor _______________________ Documento de Identidad ",fuente_normal ));
                        frase.add(  new Phrase(cedula_firma_codeudor, fuente_negrita8));
                        frase.add(  new Phrase("  Huella",fuente_normal ));
                        cell.setPhrase  ( frase);
                        
                        //cell.setPhrase  ( new Phrase("Firma Codeudor _______________________ Documento de Identidad " + cedula_firma_codeudor + "  Huella", fuente_normal ) );//celda con texto
                        
                        cell.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );//alineamiento horizontal
                        table.addCell(cell);//se mete celda con texto

                        cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda vacia
                        table.addCell(cell_vacia_der);//se mete celda vacia
                        //table.completeRow() ;//

                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia

                        table.addCell(cell_vacia_izq);//se mete celda vacia
                        
                        cell.setPhrase  ( new Phrase("Afiliado", fuente_normal ) );//celda con texto
                        cell.setHorizontalAlignment( PdfPCell.ALIGN_RIGHT );//alineamiento horizontal
                        table.addCell(cell);//se mete celda con texto

                        cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda vacia
                        table.addCell(cell_vacia_der);//se mete celda vacia
                        
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        
                        cell.setPhrase  ( new Phrase(".", fuente_normal ) );//celda con punto
                        cell.setColspan(3);//celda gasta 3 columnas
                        cell.setBorderWidthTop(0);//borde
                        cell.setBorderWidthBottom(1);//borde
                        cell.setBorderWidthLeft(1);//borde
                        cell.setBorderWidthRight(1);//borde
                        table.addCell(cell);//se mete celda con punto
                        
                        cell.setBorderWidthTop(0);//borde
                        cell.setBorderWidthBottom(0);//borde
                        cell.setBorderWidthLeft(0);//borde
                        cell.setBorderWidthRight(0);//borde
                        cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda vacia
                        cell.setColspan(1);//celda gasta 1 columna
                        table.addCell(cell);//se mete celda vacia
                        table.addCell(cell);//se mete celda vacia
                        table.addCell(cell);//se mete celda vacia
                        table.addCell(cell);//se mete celda vacia
                        table.addCell(cell);//se mete celda vacia
                        table.addCell(cell);//se mete celda vacia
                        
                        table.addCell(cell);
                        table.addCell(cell);
                        table.addCell(cell);
                        table.addCell(cell);
                        table.addCell(cell);
                        table.addCell(cell);
                        table.addCell(cell);
                        table.addCell(cell);
                        table.addCell(cell);
                        table.addCell(cell);    
                        table.addCell(cell);
                        table.addCell(cell);
                        document.add(table);

                    }
                    contador=contador+1;
                    Fecha_Temp = Util.crearCalendarDate(fecha_otorgamiento_inicial);//mar14d8
            
                }
                document.newPage();
                Fecha_Temp = Util.crearCalendarDate(fecha_otorgamiento_inicial);//mar14d8
            }
        }catch (Exception ex){
            System.out.println("ex"+ex.toString()+"__"+ex.getMessage());
            ex.printStackTrace();
        }
    }

    /*public static void main(String[] args)throws Exception {
        Model model = new Model();
        PdfImprimirService p = new PdfImprimirService();
        p.reporte(model);
    }*/
    
    public void setVariables(BeanGeneral bg1){
        bg=bg1;
        /*
        no_aprobacion=
        ciudad=
        fecha_otorgamiento=
        fecha_vencimiento=
        consecutivo=
        digito=
        nombre=
        cedula=
        ciudad_cedula=
        representado=
        nombre_codeudor=
        cedula_codeudor=
        dia_pagare=
        mes_pagare=
        ano_pagare=
        empresa=
        plata_letra=
        plata_numero=
        empresa_facultada=
        ciudad_pago=
        cedula_deudor=
        cedula_firma_codeudor=
        cantidad_de_letras=
         */
        //System.out.println("en setVariables");
        //System.out.println("bg.getValor_17()"+bg.getValor_17());
        no_aprobacion=		bg.getValor_17();
        //System.out.println("en setVariables2");
        ciudad=			bg.getValor_19();
        fecha_otorgamiento=	bg.getValor_09();
        ///fecha_vencimiento=	bg.getValor_09();//mes+digito
        consecutivo=		bg.getValor_04();
        ///digito=			el contador del para
        nombre=			bg.getValor_02();
        cedula=			bg.getValor_06();
        cedula=Util.PuntoDeMil(cedula);
        
        ciudad_cedula=		bg.getValor_12();
        representado=		bg.getValor_10();//condicion si emp=""
        if (representado.equals("")){
            representado="_________________________";
        }
        nombre_codeudor=	bg.getValor_14();//condicion si es = ""
        if (nombre_codeudor.equals("")){
            nombre_codeudor="_________________________";
        }
        cedula_codeudor=	bg.getValor_15();//condicion si es = ""
        if (cedula_codeudor.equals("")){
            cedula_codeudor="_________________________";
        }else{
            cedula_codeudor=Util.PuntoDeMil(cedula_codeudor);
        }           
        
	///dia_pagare=		fecha_vencimiento.substring(0,2);//dia de la fecha de vencimiento
        ///mes_pagare=		fecha_vencimiento.substring(3,5);//mes de la fecha de vencimiento
        ///ano_pagare=		fecha_vencimiento.substring(6,10);//a�o del mes de vancimiento
        //System.out.println("aaaa1");
	empresa=		bg.getValor_03();
        
        plata_numero=		bg.getValor_05();
        
        //System.out.println("antes de cant en letras");
        
        //Cantidad en Letras
	RMCantidadEnLetras ejem= new RMCantidadEnLetras();
	String[] a;
        //System.out.println("plata_numero1"+plata_numero);
        double valor= 		Double.valueOf(plata_numero).doubleValue();
	a=ejem.getTexto(valor);
        //System.out.println("getTexto");
	String res="";
        //System.out.println("plata_numero"+plata_numero);
	for(int i=0;i<a.length;i++){
            res=res+((String)a[i]).replace('-',' ');
	}
	//Fin Cant Letras
        res=res.trim();
        plata_numero=Util.customFormat(valor);
        
        //System.out.println("despues de cant en letras");
        plata_letra=		res;//Esto es un metodo que transforma a letras una cantidad    ///
        
        empresa_facultada=	bg.getValor_03();
        ciudad_pago=		bg.getValor_18();
        
        cedula_deudor=		bg.getValor_06();
        cedula_deudor=Util.PuntoDeMil(cedula_deudor);
        
        cedula_firma_codeudor=	bg.getValor_15();//condicion
        if (cedula_firma_codeudor.equals("")){
            cedula_firma_codeudor="_________________________";
        }else{
            cedula_firma_codeudor=Util.PuntoDeMil(cedula_firma_codeudor);
        }
                
        cantidad_de_letras=	Integer.parseInt(bg.getValor_01());
        //System.out.println("aaaa");
        
        diay = Integer.parseInt(fecha_otorgamiento.substring(8,10));
        mesy = Integer.parseInt(fecha_otorgamiento.substring(5,7));    
        anoy = Integer.parseInt(fecha_otorgamiento.substring(0,4));
	
        
        Fecha_Negocio = Util.crearCalendarDate(fecha_otorgamiento);
	Fecha_Temp = Util.crearCalendarDate(fecha_otorgamiento);
        
        fecha_otorgamiento_inicial=fecha_otorgamiento;//mar14d8
        
        fecha_otorgamiento=Util.crearStringFechaDateLetra2(Fecha_Negocio);
        
    } 
    
    public Phrase getTextoPrincipal(){
        if (dia_pagare.equals("28")){//temporal
            dia_pagare="29";
        }
        String cadena="";
        Phrase respuesta =  new Phrase("");
        respuesta.add(  new Phrase("Yo ", fuente_normal));
        respuesta.add(  new Phrase(nombre, fuente_negrita8));
        respuesta.add(  new Phrase(" (Deudor) identificado con la c�dula de ciudadania No. ", fuente_normal));
        respuesta.add(  new Phrase(cedula, fuente_negrita8));
        respuesta.add(  new Phrase(" expedida en ", fuente_normal));
        respuesta.add(  new Phrase(ciudad_cedula, fuente_negrita8));
        respuesta.add(  new Phrase(" en calidad de girador y aceptante (art. 673 c�digo de comercio) ", fuente_normal));
        respuesta.add(  new Phrase("obrando en nombre propio y como representante legal de ", fuente_normal));
        respuesta.add(  new Phrase(representado, fuente_negrita8));
        respuesta.add(  new Phrase(" y como codeudor de ", fuente_normal));
        respuesta.add(  new Phrase("�sta, y, ", fuente_normal));
        respuesta.add(  new Phrase(nombre_codeudor, fuente_negrita8));
        respuesta.add(  new Phrase(" C.C. No. ", fuente_normal));
        respuesta.add(  new Phrase(cedula_codeudor, fuente_negrita8));
        respuesta.add(  new Phrase(" en calidad de codeudor, y en la ", fuente_normal));
        respuesta.add(  new Phrase("fecha cierta: dia ", fuente_normal));
        respuesta.add(  new Phrase(dia_pagare, fuente_negrita8));
        respuesta.add(  new Phrase(" del mes de ", fuente_normal));
        respuesta.add(  new Phrase(mes_pagare, fuente_negrita8));
        respuesta.add(  new Phrase(" del a�o ", fuente_normal));
        respuesta.add(  new Phrase(ano_pagare, fuente_negrita8)); 
        respuesta.add(  new Phrase(" pagar� solidaria e incondicionalmente en la ciudad de "+ciudad_pago+" (art. 677, 683 y 876. ", fuente_normal));        
        respuesta.add(  new Phrase("C�digo de comercio) a la orden de: ", fuente_normal));
        respuesta.add(  new Phrase(empresa, fuente_negrita8)); 
        respuesta.add(  new Phrase(" o a su(s) tenedor(es) legitimo(s), la suma de: ", fuente_normal));
        respuesta.add(  new Phrase(plata_letra, fuente_negrita8)); 
        respuesta.add(  new Phrase(" ($", fuente_normal));
        respuesta.add(  new Phrase(plata_numero, fuente_negrita8)); 
        cadena=") mas intereses remuneratorios liquidables a la tasa ";
        cadena+="m�xima de interes corriente bancario y moratorios a la m�xima tasa legal vigente. En el evento ";
        cadena+="de mora ser�n de mi cargo los gastos de cobranza tasados al 20% del total adeudado al momento ";
        cadena+="del pago y los honorarios igualmente liquidados al 20% del total adeudado. Los giradores, ";
        cadena+="aceptantes de esta letra nos obligamos solidariamente y renunciamos a la presentaci�n para la ";
        cadena+="aceptaci�n y pago, y facultamos a ";
        respuesta.add(  new Phrase(cadena, fuente_normal)); 
        respuesta.add(  new Phrase(empresa_facultada, fuente_negrita8)); 
        respuesta.add(  new Phrase(" o a su(s) tenedor(es) legitimo(s) ", fuente_normal));
        respuesta.add(  new Phrase("para rehusar el pago parcial y los avisos de rechazo. Domicilio de pago ", fuente_normal));
        respuesta.add(  new Phrase(ciudad_pago, fuente_negrita8)); 
        
        cadena= "Yo " +nombre+ " (Deudor) identificado con la c�dula de ciudadania No. " + cedula + " expedida ";
        cadena+="en " + ciudad_cedula + " en calidad de girador y aceptante (art. 673 c�digo de comercio) ";
        cadena+="obrando en nombre propio y como representante legal de " + representado + " y como codeudor de ";
        cadena+="�sta, y, " +nombre_codeudor+" C.C. No. " + cedula_codeudor + " en calidad de codeudor, y en la ";
        cadena+="fecha cierta: dia " +dia_pagare+ " del mes de " +mes_pagare+ " del a�o " + ano_pagare ;
        cadena+=" pagar� solidaria e incondicionalmente en la ciudad de "+ciudad_pago+" (art. 677, 683 y 876. ";
        cadena+="C�digo de comercio) a la orden de: " +empresa+" o a su(s) tenedor(es) legitimo(s), la suma ";
        cadena+="de: "+plata_letra+" ($" +plata_numero+") mas intereses remuneratorios liquidables a la tasa ";
        cadena+="m�xima de interes corriente bancario y moratorios a la m�xima tasa legal vigente. En el evento ";
        cadena+="de mora ser�n de mi cargo los gastos de cobranza tasados al 20% del total adeudado al momento ";
        cadena+="del pago y los honorarios igualmente liquidados al 20% del total adeudado. Los giradores, ";
        cadena+="aceptantes de esta letra nos obligamos solidariamente y renunciamos a la presentaci�n para la ";
        cadena+="aceptaci�n y pago, y facultamos a "+empresa_facultada+" o a su(s) tenedor(es) legitimo(s) ";
        cadena+="para rehusar el pago parcial y los avisos de rechazo. Domicilio de pago "+ciudad_pago;
        return respuesta;
    }
    
    public void setBeanGeneral(BeanGeneral bg1){
        bg=bg1;
    }
    
    public BeanGeneral getBeanGeneral(){
        return bg;
    }
    
    public int getN(){
        return n;
    }
    
    public void setVariablesEndoso(String num_letras1,String cc_representante1,String representada1,String cc_deudor1,String cc_codeudor1,String swcodeudor1){
        enum_letras=Integer.parseInt(num_letras1);
        ecc_representante=cc_representante1;
        ecc_representante=Util.PuntoDeMil(ecc_representante);
        erepresentada=representada1;
        ecc_deudor=cc_deudor1;
        ecc_deudor=Util.PuntoDeMil(ecc_deudor);
        //System.out.println("ecc_deudor"+ecc_deudor);
        ecc_codeudor=cc_codeudor1;
        ecc_codeudor=Util.PuntoDeMil(ecc_codeudor);
        //System.out.println("ecc_codeudor"+ecc_codeudor);
        eswcodeudor=swcodeudor1;
    }
    
    public void reporteEndosos(Model model ) throws Exception{
        iniciar();
        this.modelito      = model;
        try{
            this.escribirEndosos( );
            document.close();
            //System.out.println("TERMINO");
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    
    public void escribirEndosos(  ) throws Exception{
        //String supertexto="Endosamos en propiedad con responsabilidad el presente titulo valor a favor de la Fiduciaria Corficolombiana S.A Fideicomiso Fintravalores nit 800.256.768-6. En consecuencia el pago del mismo debe hacerse directamente a favor de la Fiduciaria Corficolombiana S.A Fideicomiso Fintravalores o a su orden en la fecha de su vencimiento en la cuenta corriente No. 800-57617-5 del Banco de Occidente.";
        
        String supertexto="Endosamos en propiedad el presente t�tulo valor a favor de Fintravalores S. A. nit 802.022.016-1. "+
            "En consecuencia el pago del mismo debe hacerse directamente a favor de la Fiduciaria Corficolombiana S. A. Fideicomiso Fintravalores "+
            "o a su orden en la fecha de su vencimiento en la cuenta corriente No. 800-57617-5 del Banco de Occidente." ;
        
        String pie_endoso="Fintravalores S. A. endosa en propiedad con responsabilidad a la Fiduciaria Corficolombiana S. A. Fideicomiso Fintravalores.";
                
        String firma="Firma     ________________________________";
        try {  
            float[] anchos = {0.05f,1.0f,0.05f};//porcentajes de ancho de columnas de una tabla
            document.newPage();//nueva pag
            while (contador<cantidad_de_letras){
                for(int j=1;j<=2;j++){//2 en cada pag
               
                    if (contador<enum_letras){//si no ha terminado   

                        PdfPTable table = new PdfPTable(anchos);//se crea una tabla de 6 columnas
                        table.setWidthPercentage(100);//uso de 100% horizontal
                        
                        PdfPCell cell_vacia3    = new PdfPCell();//se crea una celda vacia triple
                        cell_vacia3.setColspan(3);//triple
                        cell_vacia3.setBorderWidthTop(0);//borde
                        cell_vacia3.setBorderWidthBottom(0);//borde
                        cell_vacia3.setBorderWidthLeft(0);//borde
                        cell_vacia3.setBorderWidthRight(0);//borde
                        cell_vacia3.setPhrase  ( new Phrase("", fuente_normal ) );//celda triple vacia
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        
                        PdfPCell cell_vacia    = new PdfPCell();//se crea una celda vacia 
                        cell_vacia.setBorderWidthTop(0);//borde
                        cell_vacia.setBorderWidthBottom(0);//borde
                        cell_vacia.setBorderWidthLeft(0);//borde
                        cell_vacia.setBorderWidthRight(0);//borde
                        cell_vacia.setPhrase  ( new Phrase("", fuente_normal ) );//celda triple vacia
                        
                        PdfPCell cell    = new PdfPCell();//se crea una celda
                        cell.setBorderWidthTop(0);//borde
                        cell.setBorderWidthBottom(0);//borde
                        cell.setBorderWidthLeft(0);//borde
                        cell.setBorderWidthRight(0);//borde

                        
                        table.addCell(cell_vacia);//se mete la celda vacia
                                                               
                        cell.setPhrase  ( new Phrase(supertexto , fuente_normal ) );//celda con texto
                        cell.setHorizontalAlignment( PdfPCell.ALIGN_JUSTIFIED );//alineamiento horizontal
                        table.addCell(cell);//se mete celda con texto
                                               
                        table.addCell(cell_vacia);//se mete celda vacia
                                                
                        table.addCell(cell_vacia3);//se mete celda vacia triple
                        table.addCell(cell_vacia3);//se mete celda vacia triple
                        table.addCell(cell_vacia3);//se mete celda vacia triple
                        table.addCell(cell_vacia3);//se mete celda vacia triple
                        
                        table.addCell(cell_vacia);//se mete celda vacia
                        
                        cell.setPhrase  ( new Phrase(firma,fuente_normal ) );//celda con texto
                        table.addCell(cell);//se mete celda con texto

                        table.addCell(cell_vacia);//se mete celda vacia
                        
                        table.addCell(cell_vacia3);//se mete celda vacia
                        
                        table.addCell(cell_vacia);//se mete celda vacia

                        frase =  new Phrase("");
                        frase.add(  new Phrase("C.C            ",fuente_normal ));
                        frase.add(  new Phrase(ecc_representante, fuente_negrita8));
                        cell.setPhrase  ( frase);                        
                        
                        //cell.setPhrase  ( new Phrase("C.C            "+ecc_representante, fuente_normal ) );//celda con texto
                        
                        cell.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );//alineamiento horizontal
                        table.addCell(cell);//se mete celda con texto
                        
                        table.addCell(cell_vacia);//se mete celda vacia

                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia

                        table.addCell(cell_vacia);//se mete celda vacia
                        
                        //Firma     __
                        
                        frase =  new Phrase("");
                        frase.add(  new Phrase("          En nombre y representaci�n de  ",fuente_normal ));
                        frase.add(  new Phrase(erepresentada, fuente_negrita8));
                        cell.setPhrase  ( frase); 
                        
                        //cell.setPhrase  ( new Phrase("          En nombre y representaci�n de  "+erepresentada, fuente_normal ) );//celda con texto
                        
                        table.addCell(cell);//se mete celda con texto

                        table.addCell(cell_vacia);//se mete celda vacia
                        
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia

                        table.addCell(cell_vacia);//se mete celda vacia
                        
                        cell.setPhrase  ( new Phrase("          En se�al de aceptaci�n de lo anterior: ", fuente_normal ) );//celda con texto
                        table.addCell(cell);//se mete celda con texto

                        table.addCell(cell_vacia);//se mete celda vacia
                        
                        table.addCell(cell_vacia3);//se mete celda vacia
                        
                        table.addCell(cell_vacia3);//se mete celda vacia
                                                
                        table.addCell(cell_vacia);//se mete celda vacia
                        
                        cell.setPhrase  ( new Phrase("          El deudor.", fuente_normal ) );//celda con punto
                        table.addCell(cell);//se mete celda con punto
                        
                        table.addCell(cell_vacia);//se mete celda vacia
                        
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        
                        if (eswcodeudor.equals("concodeudor")){
                            
                            table.addCell(cell_vacia);//se mete celda vacia

                            cell.setPhrase  ( new Phrase(firma, fuente_normal ) );//celda vacia
                            table.addCell(cell);//se mete celda vacia

                            table.addCell(cell_vacia);//se mete celda vacia

                            table.addCell(cell_vacia3);

                            table.addCell(cell_vacia);//se mete celda vacia

                            frase =  new Phrase("");
                            frase.add(  new Phrase("C.C            ",fuente_normal ));
                            frase.add(  new Phrase(ecc_deudor, fuente_negrita8));
                            cell.setPhrase  ( frase); 
                            
                            //cell.setPhrase  ( new Phrase("C.C            "+ecc_deudor, fuente_normal ) );//celda con texto
                            
                            table.addCell(cell);//se mete celda con texto

                            table.addCell(cell_vacia);

                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);

                            table.addCell(cell_vacia);//se mete celda vacia

                            cell.setPhrase  ( new Phrase("          El codeudor.", fuente_normal ) );//celda con punto
                            table.addCell(cell);//se mete celda con punto

                            table.addCell(cell_vacia);//se mete celda vacia
                        }else{
                            ecc_codeudor=ecc_deudor;
                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);
                            table.addCell(cell_vacia3);
                        }
                        
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                         table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        
                        table.addCell(cell_vacia);//se mete celda vacia
                        
                        cell.setPhrase  ( new Phrase(firma,fuente_normal ) );//celda con texto
                        table.addCell(cell);//se mete celda con texto

                        table.addCell(cell_vacia);//se mete celda vacia
                        
                        table.addCell(cell_vacia3);//se mete celda vacia
                        
                        table.addCell(cell_vacia);//se mete celda vacia

                        frase =  new Phrase("");
                        frase.add(  new Phrase("C.C            ",fuente_normal ));
                        frase.add(  new Phrase(ecc_codeudor, fuente_negrita8));
                        cell.setPhrase  ( frase); 
                        
                        //cell.setPhrase  ( new Phrase("C.C            "+ecc_codeudor, fuente_normal ) );//celda con texto
                        
                        table.addCell(cell);//se mete celda con texto
                          
                        table.addCell(cell_vacia);//se mete celda vacia
                        
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        
                        table.addCell(cell_vacia);//se mete celda vacia
                        cell.setPhrase  ( new Phrase(""+pie_endoso,fuente_normal ) );//celda con texto
                        table.addCell(cell);//se mete celda con texto
                        table.addCell(cell_vacia);//se mete celda vacia
                        
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete celda vacia
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        table.addCell(cell_vacia3);//se mete la celda vacia triple
                        
                        document.add(table);

                    }  
                    contador=contador+1;
            
                }
                
                document.newPage();
            }
        }catch (Exception ex){
            System.out.println("ex"+ex.toString()+"__"+ex.getMessage());
            ex.printStackTrace();
        }
    }
    
    
    public void imprimirDetalleCompraCartera (ArrayList detalle) throws Exception{
        iniciar();
       
        try{
            this.escribirDetalleCompraCartera( detalle);
            document.close();
            //System.out.println("TERMINO pdf");
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    
    public void escribirDetalleCompraCartera(ArrayList detalle) throws Exception{
        //System.out.println("escribirDetalleCompraCartera en pdfimprimirsvc");
        int d=3;//cantidad de decimales
        try {  
            float[] anchos = {1f,1f,1f,1f,1f,1f,1f,1f,1f,1f,1f,1f};//porcentajes de ancho de columnas de una tabla
            document.setPageSize(new Rectangle(850,500));
            document.newPage();//nueva pag
            
            //System.out.println("escribir en pdf");
            
            int contadordetalle=0;
                
            PdfPCell cell_vacia3    = new PdfPCell();//se crea una celda vacia triple
            cell_vacia3.setColspan(12);//triple
            cell_vacia3.setBorderWidthTop(0);//borde
            cell_vacia3.setBorderWidthBottom(0);//borde
            cell_vacia3.setBorderWidthLeft(0);//borde
            cell_vacia3.setBorderWidthRight(0);//borde
            cell_vacia3.setPhrase  ( new Phrase("", fuente_normal ) );//celda triple vacia
                
            PdfPTable table = new PdfPTable(anchos);//se crea una tabla de 6 columnas
            table.setWidthPercentage(100);//uso de 100% horizontal
            table.addCell(cell_vacia3);//se mete la celda vacia triple
            
            PdfPCell cell    = new PdfPCell();//se crea una celda
            cell.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );//alineamiento horizontal
            cell.setColspan(12);//triple
            cell.setBorderWidthTop(0);//borde
            cell.setBorderWidthBottom(0);//borde
            cell.setBorderWidthLeft(0);//borde
            cell.setBorderWidthRight(0);//borde
            cell.setPhrase  ( new Phrase("C�digo de negocio : "+((chequeCartera)detalle.get(0)).getCodigoNegocio(), fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda 
            cell.setPhrase  ( new Phrase("Fecha de desembolso : "+((chequeCartera)detalle.get(0)).getFechaDesembolso().substring(0,10), fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda
            cell.setPhrase  ( new Phrase("Afiliado : "+((chequeCartera)detalle.get(0)).getNombreProveedor(), fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda
            cell.setPhrase  ( new Phrase("Cliente : "+((chequeCartera)detalle.get(0)).getNombreNit(), fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda
            
            String modremes=  ((chequeCartera)detalle.get(0)).getModRemesa();
            String modavalx=  ((chequeCartera)detalle.get(0)).getModAval();
            
            /*
            if (modremes.equals("0")){
                modremes="Cliente";
            }
            if (modremes.equals("1")){
                modremes="Establecimiento";
            }
            if (modremes.equals("2")){
                modremes="Sin remesa";
            }
            */
            
            if (modremes.equals("0")){
                if (modavalx.equals("0")){
                    modremes="Establecimiento";
                }
                if (modavalx.equals("1")){
                    modremes="Cliente";
		}								
            }
                            
            if (modremes.equals("1")){
                modremes="Sin remesa";
            }
            
            //System.out.println("escribir en pdf");
            cell.setPhrase  ( new Phrase("Modalidad de remesa : "+modremes, fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda
            
            String modcustodi=  ((chequeCartera)detalle.get(0)).getModCustodia();
            if (modcustodi.equals("1")){
                modcustodi="Cliente";
            }
            if (modcustodi.equals("0")){
                modcustodi="Establecimiento";
            }
            cell.setPhrase  ( new Phrase("Modalidad de custodia : "+modcustodi, fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda
            
            cell.setPhrase  ( new Phrase("Cantidad de documentos : "+detalle.size(), fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda
            
            
            String tipodocx=((chequeCartera)detalle.get(0)).getTipoDoc();
            if (tipodocx!=null && tipodocx.equals("02")){
                tipodocx="Letra";
            }
            if (tipodocx!=null && tipodocx.equals("01")){
                tipodocx="Cheque";
            }
            cell.setPhrase  ( new Phrase("Tipo de documento : "+tipodocx, fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda
                        
            chequeCartera chc;
            
            table.addCell(cell_vacia3);//se mete la celda vacia triple
            table.addCell(cell_vacia3);//se mete la celda vacia triple
            table.addCell(cell_vacia3);//se mete la celda vacia triple
            
            cell.setBorderWidthTop(1);//borde
            cell.setBorderWidthBottom(1);//borde
            cell.setBorderWidthLeft(1);//borde
            cell.setBorderWidthRight(0);//borde
            cell.setColspan(1);
            
            cell.setHorizontalAlignment( PdfPCell.ALIGN_CENTER );//alineamiento horizontal
            cell.setVerticalAlignment( PdfPCell.ALIGN_MIDDLE );//alineamiento horizontal
            
            cell.setPhrase  ( new Phrase("ITEM", fuente_normal ) );//celda 
            //System.out.println("escribir en pdf");
            table.addCell(cell);//se mete la celda
            
            cell.setBorderWidthTop(1);//borde
            cell.setBorderWidthBottom(1);//borde
            cell.setBorderWidthLeft(1);//borde
            cell.setBorderWidthRight(0);//borde
            
            cell.setPhrase  ( new Phrase("FECHA DOCUMENTO", fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda
           
            cell.setPhrase  ( new Phrase("FECHA CONSIGNAR", fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda
            cell.setPhrase  ( new Phrase("VALOR", fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda
            cell.setPhrase  ( new Phrase("PORTE", fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda
            cell.setPhrase  ( new Phrase("DIAS", fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda
            cell.setPhrase  ( new Phrase("FACTOR", fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda
            cell.setPhrase  ( new Phrase("REMESA", fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda
            cell.setPhrase  ( new Phrase("DESCUENTO", fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda
            cell.setPhrase  ( new Phrase("VALOR CON DESCUENTO", fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda
            cell.setPhrase  ( new Phrase("CUSTODIA", fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda
            
            cell.setBorderWidthTop(1);//borde
            cell.setBorderWidthBottom(1);//borde
            cell.setBorderWidthLeft(1);//borde
            cell.setBorderWidthRight(1);//borde
                        
            cell.setPhrase  ( new Phrase("VALOR A GIRAR", fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda
            
            double total_valorgirable=0.0;
            double total_valor_cheque=0.0;
            double total_descuento=0.0;
            double total_valor_cheque_sincustodia=0.0;
            double total_custodia=0.0;
            //double total_valor_sincustodia_custodia=0.0;
            //double total_remesamasporte=0.0;
            //double total_valorgirable=0.0;
            double total_remesa=0.0;
            double total_porte=0.0;
            
            //System.out.println("escribir en pdf");
            while (contadordetalle<detalle.size()){
                 
                if (contadordetalle==20 || contadordetalle==40 || contadordetalle==60){
                    document.add(table);
                    document.newPage();
                    table = new PdfPTable(anchos);//se crea una tabla de 6 columnas
                    table.setWidthPercentage(100);//uso de 100% horizontal
                    table.addCell(cell_vacia3);//se mete la celda vacia triple
                }
                
                //System.out.println("escribir en pdf");
                chc=(chequeCartera)detalle.get(contadordetalle);
                
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                
                
                cell.setPhrase  ( new Phrase(""+chc.getItem(), fuente_normal ) );//celda 
                table.addCell(cell);//se mete la celda 
                
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde

                cell.setPhrase  ( new Phrase(""+chc.getFechaCheque().substring(0,10), fuente_normal ) );//celda 
                table.addCell(cell);//se mete la celda 
                
                cell.setPhrase  ( new Phrase(""+chc.getFechaConsignacion().substring(0,10), fuente_normal ) );//celda 
                table.addCell(cell);//se mete la celda 
                
                cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(Double.parseDouble(chc.getValor()),d)), fuente_normal ) );//celda 
                table.addCell(cell);//se mete la celda 
                total_valor_cheque=total_valor_cheque+Double.parseDouble(chc.getValor());
                
                cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(Double.parseDouble(chc.getPorte()),d)), fuente_normal ) );//celda 
                table.addCell(cell);//se mete la celda 
                total_porte=total_porte+Double.parseDouble(chc.getPorte());
                
                cell.setPhrase  ( new Phrase(""+chc.getDias(), fuente_normal ) );//celda 
                table.addCell(cell);//se mete la celda 
                
                cell.setPhrase  ( new Phrase(""+(Util.roundByDecimal(Double.parseDouble(chc.getFactor()),5)), fuente_normal ) );//celda 
                table.addCell(cell);//se mete la celda 
                
                cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(Double.parseDouble(chc.getRemesa()),d)), fuente_normal ) );//celda 
                table.addCell(cell);//se mete la celda 
                total_remesa=total_remesa+Double.parseDouble(chc.getRemesa());
                
                cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(Double.parseDouble(chc.getDescuento()),d)), fuente_normal ) );//celda 
                table.addCell(cell);//se mete la celda 
                total_descuento=total_descuento+Double.parseDouble(chc.getDescuento());
                
                cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(Double.parseDouble(chc.getValorSinCustodia()),d)), fuente_normal ) );//celda 
                table.addCell(cell);//se mete la celda 
                total_valor_cheque_sincustodia=total_valor_cheque_sincustodia+Double.parseDouble(chc.getValorSinCustodia());
                
                cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(Double.parseDouble(chc.getCustodia()),d)), fuente_normal ) );//celda 
                table.addCell(cell);//se mete la celda 
                total_custodia=total_custodia+Double.parseDouble(chc.getCustodia());       
                
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(1);//borde

                cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(Double.parseDouble(chc.getValorGirable()),d)), fuente_normal ) );//celda 
                table.addCell(cell);//se mete la celda 
                total_valorgirable=total_valorgirable+Double.parseDouble(chc.getValorGirable());
                
                contadordetalle=contadordetalle+1; 
                                      
            }
            
            cell.setColspan(12);
            cell.setBorderWidthTop(0);//borde
            cell.setBorderWidthBottom(1);//borde
            cell.setBorderWidthLeft(1);//borde
            cell.setBorderWidthRight(1);//borde

            cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda 

            //contadordetalle=contadordetalle+1; 
            
            cell.setBorderWidthTop(0);//borde
            cell.setBorderWidthBottom(0);//borde
            cell.setBorderWidthLeft(0);//borde
            cell.setBorderWidthRight(0);//borde

            cell.setColspan(3);
            table.addCell(cell);//se mete la celda 
            
            cell.setColspan(1);
            cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(total_valor_cheque,d)), fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda 
            
            cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(total_porte,d)), fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda 
            
            cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda 
            table.addCell(cell);//se mete la celda 
            
            cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(total_remesa,d)), fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda 
            
            cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(total_descuento,d)), fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda 
            
            cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(total_valor_cheque_sincustodia,d)), fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda 
            
            cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(total_custodia,d)), fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda 
                        
            cell.setColspan(1);
            cell.setPhrase  ( new Phrase(""+Util.customFormat(Util.roundByDecimal(total_valorgirable,d)), fuente_normal ) );//celda 
            table.addCell(cell);//se mete la celda 

            document.add(table);
            document.newPage();
            //System.out.println("fin de escribir en pdf");
        }catch (Exception ex){
            System.out.println("ex"+ex.toString()+"__"+ex.getMessage());
            ex.printStackTrace();
        }
        
    }
    
    /*
     *jemartinez
     */
    public void setVariablesReporteFenalco(BeanGeneral beanGeneral){
        //Aniadir las 12 variables del reporte
        nit = beanGeneral.getValor_01();
        nombre_cliente = beanGeneral.getValor_02();
        direccion = beanGeneral.getValor_03();
        telefono = beanGeneral.getValor_04();
        telcontacto = beanGeneral.getValor_05();
        aval = beanGeneral.getValor_07();
        
        //Si es una letra
        if(beanGeneral.getValor_11().trim().substring(0,2).equals("FL")){
            banco = "99";
            cuenta_cheque = "0";
        }else
            banco = beanGeneral.getValor_06();
        
        if(beanGeneral.getValor_08()==null)
            cuenta_cheque="0";
        else
            cuenta_cheque=beanGeneral.getValor_08();
        
        beneficiario  =beanGeneral.getValor_09();
        numero_cheque  = beanGeneral.getValor_10();
        numero_factura  = beanGeneral.getValor_11();
        valor  = beanGeneral.getValor_12();
        
        if(beanGeneral.getV1()==null){
            observaciones = new String[1];
            observaciones[1] = "No existen observaciones";
        }else{
            observaciones = beanGeneral.getV1();
        }
    }
    
    /**
     * Metodo que genera el reporte que sera enviado a fenalco
     * @param Clase Model
     * @throws Exception si ocurre una exception en la creacion del documento
     * @author jemartinez
     */
    public void reporteChequesDevueltosFenalco(Model model) throws Exception{
        iniciar();//el document se crea en este metodo
        this.modelito = model;
        try{
            this.obtenterReporteChequesDevueltosFenalco();
            document.close();
            //System.out.print("Done!!");
        }catch(Exception e){
            throw new Exception("Error: "+e.getMessage());
        }
    }
  
    /*
     *jemartinez
     */
    private String getMonth(int month){
        String monthWord  ="";
        if(month == 0) monthWord = "Enero";
        if(month == 1) monthWord = "Febrero";
        if(month == 2) monthWord = "Marzo";
        if(month == 3) monthWord = "Abril";
        if(month == 4) monthWord = "Mayo";
        if(month == 5) monthWord = "Junio";
        if(month == 6) monthWord = "Julio";
        if(month == 7) monthWord = "Agosto";
        if(month == 8) monthWord = "Septiembre";
        if(month == 9) monthWord = "Octubre";
        if(month == 10) monthWord = "Noviembre";
        if(month == 11) monthWord = "Diciembre";
        return monthWord;
    }
    
    /*
     * jemartinez
     */
    private void obtenterReporteChequesDevueltosFenalco(){
          
        try {
                       
            for(int i=0;i<2;i++){
            
                int cols=2;//cantidad de filas de 1 tabla

                PdfPTable tableImage = new PdfPTable(5);//Contendra la imagen
                tableImage.setWidthPercentage(100);    

                PdfPTable tableTop = new PdfPTable(1);//Cabecera
                tableTop.setWidthPercentage(100);

                PdfPTable table = new PdfPTable(cols);//se crea una tabla de 6 columnas
                table.setWidthPercentage(100);//uso de 100% horizontal

                PdfPCell cell = new PdfPCell();//se crea una celda
                
                document.newPage();//nueva pag
                
                //--------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(1);
                cell.setBorderWidthLeft(1);
                cell.setBorderWidthRight(1);
                cell.setBorderWidthBottom(0);
                cell.setPhrase(new Phrase("REPORTE CHEQUES DEVUELTOS",this.fuente_negrita_grande));
                cell.setColspan(2);
                cell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tableTop.addCell(cell);  
                document.add(tableTop);
                //---------------------------------------------------------------------------------------------------

                cell.setBorderWidthTop(0);
                cell.setBorderWidthBottom(0);
                cell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                cell.setColspan(1);
                cell.setBorderWidthLeft(1);
                cell.setBorderWidthRight(0);

                cell.setPhrase(new Phrase("",this.fuente_normal));
                tableImage.addCell(cell);

                cell.setBorderWidthLeft(0);
                cell.setPhrase(new Phrase("",this.fuente_normal));
                tableImage.addCell(cell);

                Image jpg = Image.getInstance("C:/Tomcat5/webapps/fintravalores/images/fen.jpg");//se crea una imagen
                jpg.scaleAbsolute(35,25);//se reduce la imagen
                cell.setImage(jpg);//se mete la imagen en la celda
                tableImage.addCell(cell);

                cell.setPhrase(new Phrase("",this.fuente_normal));
                tableImage.addCell(cell);

                cell.setPhrase(new Phrase("",this.fuente_normal));
                cell.setBorderWidthRight(1);//borde
                tableImage.addCell(cell);

                document.add(tableImage);
                //-----------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(1);//borde

                Calendar cal = Calendar.getInstance();
                cell.setPhrase  ( new Phrase("FENALSISTEMAS", this.fuente_negrita_grande));
                cell.setColspan(2);
                table.addCell(cell);
                cell.setPhrase  ( new Phrase("", this.fuente_negrita_grande));
                table.addCell(cell);
                table.addCell(cell);
                table.addCell(cell);
                table.addCell(cell);
                table.addCell(cell);

                cell.setBorderWidthRight(0);//borde
                cell.setColspan(1);
                cell.setPhrase  ( new Phrase("FECHA: "+cal.get(Calendar.DAY_OF_MONTH)+" de "+getMonth(cal.get(Calendar.MONTH))+" de "+cal.get(Calendar.YEAR), this.fuente_negrita_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------

                cell.setColspan(1);
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase("CODIGO: ", fuente_negrita_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);


                cell.setBorderWidthLeft(1);
                cell.setColspan(2);
                cell.setPhrase  ( new Phrase("", this.fuente_negrita_grande));
                table.addCell(cell);
                table.addCell(cell);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------

                cell.setColspan(1);
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                cell.setPhrase  ( new Phrase("NOMBRE DEL AFILIADO: ", fuente_negrita_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);//se mete celda vacia

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase("FINTRAVALORES S.A. ", fuente_normal_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setColspan(2);
                cell.setBorderWidthLeft(1);
                cell.setPhrase  ( new Phrase("", this.fuente_negrita_grande));
                table.addCell(cell);
                table.addCell(cell);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------

                cell.setColspan(1);
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                cell.setPhrase  ( new Phrase("NIT: ", fuente_negrita_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);//se mete celda vacia

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase("802.022.016-1", fuente_normal_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setColspan(2);
                cell.setBorderWidthLeft(1);
                cell.setPhrase  ( new Phrase("", this.fuente_negrita_grande));
                table.addCell(cell);
                table.addCell(cell);
                table.addCell(cell);
                table.addCell(cell);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setColspan(2);
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda 
                table.addCell(cell);//se mete celda vacia
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda
                table.addCell(cell);//se mete celda vacia
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda 
                table.addCell(cell);//se mete celda vacia

                cell.setPhrase  ( new Phrase("DATOS DEL GIRADOR ", this.fuente_negrita_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda 
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);//se mete celda vacia
                //------------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                cell.setColspan(1);
                cell.setPhrase  ( new Phrase("NUMERO DEL DOCUMENTO: ", fuente_normal_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);//se mete celda vacia

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase(this.nit, fuente_normal_media ) );//nit del cliente
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda 
                cell.setBorderWidthLeft(1);
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                cell.setColspan(1);
                cell.setPhrase ( new Phrase("NOMBRE COMPLETO: ", fuente_normal_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);//se mete celda vacia

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase(this.nombre_cliente, fuente_normal_media ) );//nit del cliente
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setBorderWidthLeft(1);
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda 
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                cell.setColspan(1);
                cell.setPhrase  ( new Phrase("DIRECCION: ", fuente_normal_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);//se mete celda vacia

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase(this.direccion,fuente_normal_media ) );//nit del cliente
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);


                cell.setBorderWidthLeft(1);
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda 
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                cell.setColspan(1);
                cell.setPhrase  ( new Phrase("TELEFONOS: ", fuente_normal_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);//se mete celda vacia

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase(this.telefono+"-"+this.telcontacto, fuente_normal_media ) );//nit del cliente
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setBorderWidthLeft(1);
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda 
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia

                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda 
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setColspan(2);
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda 
                table.addCell(cell);//se mete celda vacia
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda
                table.addCell(cell);//se mete celda vacia
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda 
                table.addCell(cell);//se mete celda vacia

                cell.setPhrase  ( new Phrase("DATOS DEL CHEQUE ", this.fuente_negrita_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda 
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                cell.setColspan(1);
                cell.setPhrase  ( new Phrase("CODIGO DEL BANCO:\t"+this.banco, fuente_normal_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);//se mete celda vacia

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase("No. DE LA CUENTA:\t"+this.cuenta_cheque,fuente_normal_media ) );//nit del cliente
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setBorderWidthLeft(1);
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda 
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                cell.setColspan(1);
                cell.setPhrase  ( new Phrase("NUMERO DEL CHEQUE:       "+this.numero_cheque, fuente_normal_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);//se mete celda vacia

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase("VALOR:        $ "+Util.customFormat(Double.parseDouble(this.valor)),fuente_normal_media ) );//nit del cliente
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setBorderWidthLeft(1);
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda 
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                cell.setColspan(1);
                cell.setPhrase  ( new Phrase("NUMERO DE LA CONSULTA:       "+this.aval, fuente_normal_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);//se mete celda vacia

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase("",fuente_normal ) );//nit del cliente
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setBorderWidthLeft(1);
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda 
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                cell.setColspan(1);
                cell.setPhrase  ( new Phrase("BENEFICIARIO: ", fuente_normal_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);//se mete celda vacia

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase(this.beneficiario,fuente_normal_media ) );//nit del cliente
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setBorderWidthLeft(1);
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda 
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);
                table.addCell(cell);

                //------------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                cell.setColspan(1);
                cell.setPhrase  ( new Phrase("NOMBRE OPERADOR: ", fuente_normal_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);//se mete celda vacia

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase("_____________________________",fuente_normal_media ) );//nit del cliente
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setBorderWidthLeft(1);
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda 
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);
                table.addCell(cell);
                table.addCell(cell);
                table.addCell(cell);
                //------------------------------------------------------------------------------------------------------
                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(1);//borde
                cell.setBorderWidthRight(0);//borde
                cell.setColspan(1);
                cell.setPhrase  ( new Phrase("FIRMA Y SELLO DEL AFILIADO: ",this.fuente_negrita_media ) );//celda vacia
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);//se mete celda vacia

                cell.setBorderWidthTop(0);//borde
                cell.setBorderWidthBottom(0);//borde
                cell.setBorderWidthLeft(0);//borde
                cell.setBorderWidthRight(1);//borde
                cell.setPhrase  ( new Phrase("_____________________________",fuente_normal_media ) );//nit del cliente
                cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                table.addCell(cell);

                cell.setBorderWidthLeft(1);
                cell.setPhrase  ( new Phrase("", fuente_normal ) );//celda 
                cell.setColspan(2);
                table.addCell(cell);//se mete celda vacia
                table.addCell(cell);
                table.addCell(cell);
                table.addCell(cell);
                table.addCell(cell);

                cell.setBorderWidthRight(1);
                cell.setBorderWidthBottom(1);
                table.addCell(cell);

                document.add(table);//se mete la tabla en el documento
                
            }
            
            //---------------observaciones------------------------------------//
            document.newPage();//Crea una nueva pagina
            
            PdfPTable tableObservaciones = new PdfPTable(1);//Contendra las observaciones
            tableObservaciones.setWidthPercentage(100);
            PdfPCell cell = new PdfPCell();//se crea una celda
            
            for(int i=0;i<this.observaciones.length;i++){
                String[] temp = observaciones[i].split(";");
                                    
                    cell.setBorderWidthTop(1);
                    cell.setBorderWidthLeft(1);
                    cell.setBorderWidthRight(1);
                    cell.setBorderWidthBottom(0);
                
                if(temp.length<4){
                    
                    cell.setPhrase(new Phrase(observaciones[i],fuente_normal_media));
                    cell.setBorderWidthBottom(1);
                    tableObservaciones.addCell(cell);
                }else{

                    cell.setPhrase(new Phrase("     Observacion: "+temp[1]+"\n     Fecha: "+temp[2]+"\n     Usuario: "+temp[3],fuente_normal_media));
                    tableObservaciones.addCell(cell);
                    
                    cell.setBorderWidthTop(0);
                    cell.setBorderWidthLeft(0);
                    cell.setBorderWidthRight(0);
                    cell.setBorderWidthBottom(0);
                    cell.setPhrase(new Phrase("",fuente_normal));           
                    tableObservaciones.addCell(cell);
                    tableObservaciones.addCell(cell);
                }
            }
            
            document.add(tableObservaciones);
            
        }catch (Exception ex){
            System.out.println("ex"+ex.toString()+"__"+ex.getMessage());
            ex.printStackTrace();
        }
    }

    
}
