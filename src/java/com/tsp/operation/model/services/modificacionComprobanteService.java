/*
 * modificacionComprobanteService.java
 *
 * Created on 17 de abril de 2008, 15:11
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.modificacionComprobanteDAO;
/**
 *
 * @author  Fintra
 */
public class modificacionComprobanteService {
    modificacionComprobanteDAO modificacionComprobDAO;
    /** Creates a new instance of modificacionComprobanteService */
    public modificacionComprobanteService() {
        modificacionComprobDAO=new modificacionComprobanteDAO();
    }
    public modificacionComprobanteService(String dataBaseName) {
        modificacionComprobDAO=new modificacionComprobanteDAO(dataBaseName);
    }
    
    public String  actualizarComprobante(String doc,String tipodoc,String proveedor,String fecha,String periodo,String user,String grupo_transaccion,String tipond)  throws Exception{
        return modificacionComprobDAO.actualizarComprobante( doc, tipodoc, proveedor, fecha, periodo, user,grupo_transaccion,tipond);
    }
    
}
