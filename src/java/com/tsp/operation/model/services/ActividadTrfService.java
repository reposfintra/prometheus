/*
 * ActividadTrfService.java
 *
 * Created on 10 de septiembre de 2005, 11:18 AM
 */

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  INTEL
 */
public class ActividadTrfService {
    private ActividadTraficoDAO act;
    /** Creates a new instance of ActividadTrfService */
    public ActividadTrfService() {
        act = new ActividadTraficoDAO();
    }
    
    public ActividadTrafico getActividadTrafico( )throws SQLException{
        return act.getActividadTrafico();
    }
    
    public Vector getActividadTraficos() {
        return act.getActividadTraficos();
    }
    
    public void setActividadTraficos(Vector ActividadTraficos) {
        act.setActividadTraficos(ActividadTraficos);
    }
    
    public void insertActividadTrafico(ActividadTrafico user) throws SQLException{
        try{
            act.setActividadTrafico(user);
            act.insertActividadTrafico();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public boolean existActividadTrafico(String cod) throws SQLException{
        return act.existActividadTrafico(cod);
    }
    
    public void serchActividadTrafico(String cod)throws SQLException{
        act.searchActividadTrafico(cod);
    }
    
    public void listActividadTrafico()throws SQLException{
        act.listActividadTrafico();
    }
    
    public void updateActividadTrafico(String cod,String desc,String usu, String base)throws SQLException{
        act.updateActividadTrafico(cod, desc, usu, base);
    }
    public void anularActividadTrafico(ActividadTrafico dem)throws SQLException{
            try{
            act.setActividadTrafico(dem);
            act.anularActividadTrafico();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }  
    }
    public Vector searchDetalleActividadTraficos(String cod, String desc, String base) throws SQLException{
        try{
            return act.searchDetalleActividadTraficos(cod, desc, base);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
        public Vector listarActividadTrafico() throws SQLException{
        try{
            return act.listarActividadTrafico();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
        
    public TreeMap searchActividadTrafico() throws SQLException{
        TreeMap tact = new TreeMap();
        Vector vact = this.listarActividadTrafico();
        ActividadTrafico a = new ActividadTrafico();        
        for( int i=0; i<vact.size(); i++){
            a = (ActividadTrafico) vact.elementAt(i);            
            tact.put(a.getDescripcion(),  a.getCodigo());
        }
        return tact;
    }
    
    public String obtenerActividadTrafico(String cod) throws SQLException{              
        return this.act.obtenerActividadTrafico(cod);
    }
}
