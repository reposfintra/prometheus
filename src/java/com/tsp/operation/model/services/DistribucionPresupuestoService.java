/*
 * DistribucionPresupuestoService.java
 *
 * Created on 15 de abril de 2005, 10:39 AM
 */

package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  Mario
 */
public class DistribucionPresupuestoService {
    ////////////////////////////////////////////////////////////////////////////////////
    private DistribucionPresupuestoDAO DPDataAccess = new DistribucionPresupuestoDAO();
    private Hashtable    Listado;
    private Hashtable    Datos;
    private List         Historial;
    public  Paginacion   paginacion = new Paginacion();
    ////////////////////////////////////////////////////////////////////////////////////
    private List         ListaAgenciaClienteStandar;
    private String       varJSAgenciaClienteStandar;
    private Hashtable    ListaTasa;
    private String       varJSTasa;
    private final String SEPARADOR = "~";
    private String       infoCliente;
    ////////////////////////////////////////////////////////////////////////////////////
    private String Origen = "";
    
    /** Creates a new instance of DistribucionPresupuestoService */
    public DistribucionPresupuestoService() {
        paginacion.setConfiguracion("Opciones/Presupuesto", 20, 20);
    }
    
    public void Init_I() throws Exception {
        try{
           // CREATE_TABLE();
        }
        catch(Exception e){
            throw new Exception("Error en Init_I [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    public void Init_II(String Agencia) throws Exception {
        try{
            //CREATE_TABLE();
            BuscarAgenciaClienteStandar(Agencia, "%", 0);
        }
        catch(Exception e){
            throw new Exception("Error en Init_II [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void CREATE_TABLE() throws Exception{
        DPDataAccess.CREATE_TABLE();
    }
    
    public void INSERT(Connection conn, String ESTADO, String DSTRCT_CODE, String ANO, String MES, String TIPO, String STD_JOB_NO, String AGENCIA, String CLIENTE, String CMENSUAL, String CSEM01, String CSEM02, String CSEM03, String CSEM04, String CSEM05, String CDIA01, String CDIA02, String CDIA03, String CDIA04, String CDIA05, String CDIA06, String CDIA07, String CDIA08, String CDIA09, String CDIA10, String CDIA11, String CDIA12, String CDIA13, String CDIA14, String CDIA15, String CDIA16, String CDIA17, String CDIA18, String CDIA19, String CDIA20, String CDIA21, String CDIA22, String CDIA23, String CDIA24, String CDIA25, String CDIA26, String CDIA27, String CDIA28, String CDIA29, String CDIA30, String CDIA31, String USUARIO) throws Exception {
        try{
            String []Parametros = {ESTADO, DSTRCT_CODE, ANO, MES, TIPO, STD_JOB_NO, AGENCIA, CLIENTE, CMENSUAL, CSEM01 ,CSEM02, CSEM03, CSEM04, CSEM05, CDIA01, CDIA02, CDIA03, CDIA04, CDIA05, CDIA06, CDIA07, CDIA08, CDIA09, CDIA10, CDIA11, CDIA12, CDIA13, CDIA14, CDIA15, CDIA16, CDIA17, CDIA18, CDIA19, CDIA20, CDIA21, CDIA22, CDIA23, CDIA24, CDIA25, CDIA26, CDIA27, CDIA28, CDIA29, CDIA30, CDIA31, USUARIO};
            DPDataAccess.INSERT(conn, Parametros);
        }
        catch(Exception e){
            throw new Exception("Error en INSERT [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public String INSERT_DIARIO(String DSTRCT_CODE, String ANO, String MES, String STD_JOB_NO, String AGENCIA, String CLIENTE, String CMENSUAL, String CDIA[], String USUARIO) throws Exception {
        try{
            String [] CSEMANAL = CalculoSemanal( CDIA);
            String [] Parametros = {"", DSTRCT_CODE, ANO, MES, "D" ,STD_JOB_NO, AGENCIA, CLIENTE, CMENSUAL, CSEMANAL[0], CSEMANAL[1],CSEMANAL[2], CSEMANAL[3], CSEMANAL[4], CDIA[0] ,CDIA[1] ,CDIA[2] ,CDIA[3] ,CDIA[4] ,CDIA[5] ,CDIA[6] ,CDIA[7] ,CDIA[8] ,CDIA[9] ,CDIA[10] ,CDIA[11] ,CDIA[12] ,CDIA[13] ,CDIA[14] ,CDIA[15] ,CDIA[16] ,CDIA[17] ,CDIA[18] ,CDIA[19] ,CDIA[20] ,CDIA[21] ,CDIA[22] ,CDIA[23] ,CDIA[24] , CDIA[25] ,CDIA[26] ,CDIA[27] ,CDIA[28] ,CDIA[29] ,CDIA[30] , USUARIO};
            return DPDataAccess.INSERT(null, Parametros);
        }
        catch(Exception e){
            throw new Exception("Error en INSERT_DIARIO [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    
    public String INSERT_DIARIO(Connection conn, String DSTRCT_CODE, String ANO, String MES, String STD_JOB_NO, String AGENCIA, String CLIENTE, String CMENSUAL, String CDIA[], String USUARIO) throws Exception {
        try{
            String [] CSEMANAL = CalculoSemanal( CDIA);
            String [] Parametros = {"", DSTRCT_CODE, ANO, MES, "D", STD_JOB_NO, AGENCIA, CLIENTE, CMENSUAL, CSEMANAL[0], CSEMANAL[1],CSEMANAL[2], CSEMANAL[3], CSEMANAL[4], CDIA[0] ,CDIA[1] ,CDIA[2] ,CDIA[3] ,CDIA[4] ,CDIA[5] ,CDIA[6] ,CDIA[7] ,CDIA[8] ,CDIA[9] ,CDIA[10] ,CDIA[11] ,CDIA[12] ,CDIA[13] ,CDIA[14] ,CDIA[15] ,CDIA[16] ,CDIA[17] ,CDIA[18] ,CDIA[19] ,CDIA[20] ,CDIA[21] ,CDIA[22] ,CDIA[23] ,CDIA[24] , CDIA[25] ,CDIA[26] ,CDIA[27] ,CDIA[28] ,CDIA[29] ,CDIA[30] , USUARIO};
            return DPDataAccess.INSERT(conn, Parametros);
        }
        catch(Exception e){
            throw new Exception("Error en INSERT_DIARIO [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    
    public void INSERT_MENSUAL(String DSTRCT_CODE, String ANO, String MES, String STD_JOB_NO, String AGENCIA, String CLIENTE, String CMENSUAL, String USUARIO) throws Exception {
        try{
            String [] Parametros = { "", DSTRCT_CODE, ANO, MES, "M", STD_JOB_NO, AGENCIA, CLIENTE, CMENSUAL, CMENSUAL, "0", "0", "0", "0", CMENSUAL , "0" , "0", "0", "0", "0" , "0", "0" , "0", "0", "0", "0", "0", "0", "0", "0" , "0", "0", "0", "0", "0", "0", "0", "0", "0", "0" , "0", "0", "0", "0", "0", USUARIO };
            DPDataAccess.INSERT(null, Parametros);
        }
        catch(Exception e){
            throw new Exception("Error en INSERT_MENSUAL [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    
    public void INSERT_MENSUAL(Connection conn, String DSTRCT_CODE, String ANO, String MES, String STD_JOB_NO, String AGENCIA, String CLIENTE, String CMENSUAL, String USUARIO) throws Exception {
        try{
            String [] Parametros = { "", DSTRCT_CODE, ANO, MES, "M", STD_JOB_NO, AGENCIA, CLIENTE, CMENSUAL, CMENSUAL, "0", "0", "0", "0", CMENSUAL , "0" , "0", "0", "0", "0" , "0", "0" , "0", "0", "0", "0", "0", "0", "0", "0" , "0", "0", "0", "0", "0", "0", "0", "0", "0", "0" , "0", "0", "0", "0", "0", USUARIO };
            DPDataAccess.INSERT(conn, Parametros);
        }
        catch(Exception e){
            throw new Exception("Error en INSERT_MENSUAL [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    
    public void INSERT_SEMANAL(String DSTRCT_CODE, String ANO, String MES, String STD_JOB_NO, String AGENCIA, String CLIENTE, String CMENSUAL, String CSEMANAL[], String USUARIO) throws Exception {
        try{
            String [] CDIA = CalculoDiario(ANO,MES,CSEMANAL);
            String [] Parametros = {"", DSTRCT_CODE, ANO, MES, "S",  STD_JOB_NO, AGENCIA, CLIENTE, CMENSUAL, CSEMANAL[0], CSEMANAL[1],CSEMANAL[2], CSEMANAL[3], CSEMANAL[4], CDIA[0] ,CDIA[1] ,CDIA[2] ,CDIA[3] ,CDIA[4] ,CDIA[5] ,CDIA[6] ,CDIA[7] ,CDIA[8] ,CDIA[9] ,CDIA[10] ,CDIA[11] ,CDIA[12] ,CDIA[13] ,CDIA[14] ,CDIA[15] ,CDIA[16] ,CDIA[17] ,CDIA[18] ,CDIA[19] ,CDIA[20] ,CDIA[21] ,CDIA[22] ,CDIA[23] ,CDIA[24] , CDIA[25] ,CDIA[26] ,CDIA[27] ,CDIA[28] ,CDIA[29] ,CDIA[30] , USUARIO};
            DPDataAccess.INSERT(null, Parametros);
        }
        catch(Exception e){
            throw new Exception("Error en INSERT_SEMANAL [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    
    
    public void DELETE(String Distrito, String  Ano, String Mes) throws Exception {
        try{
            DPDataAccess.BORRAR_PTO(Distrito, Ano, Mes);
        }
        catch(Exception e){
            throw new Exception("Error en DELETE [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private String []CalculoSemanal(String []Viajes){
        String [] Semana = {"0","0","0","0","0","0"};
        for (int i=0, j=0;i<31;i++){
            if (i%7==0 && i!=0) j++;
            Semana[j] = String.valueOf(Integer.parseInt(Semana[j]) + Integer.parseInt(Viajes[i]));
        }
        
        return Semana;
    }
    
    private String []CalculoDiario(String Ano, String Mes, String []Viajes){
        String [] CDIA = {"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"};
        for (int i=0,j=0;i<=30 && j<5;i+=7,j++)
            CDIA[i] = Viajes[j];
        return CDIA;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Tipo M: Mensual, S:Semanal, D: Diaria
    public void ObtenerListado(String Tipo, String Ano, String Mes, String Stdjob, String Cliente , String Agencia, boolean MantenerPaginacion, String Filtro) throws Exception{
        try{
            this.ReiniciarListado();
            this.Listado = DPDataAccess.BuscarPresupuesto((Tipo.equals("M")?0:(Tipo.equals("S")?1:2))  , Ano, Mes, Stdjob, Cliente, Agencia, (Filtro.equals("Ok")?true: false) );
            this.Listado.put("Stdjob" ,Stdjob);
            this.Listado.put("Cliente",Cliente);
            this.Listado.put("Agencia",Agencia);
            this.Listado.put("Tipo",Tipo);
            this.Listado.put("Ano" ,Ano);
            this.Listado.put("Mes" ,Mes);
            this.Listado.put("Filtro" ,Filtro);
            paginacion.setLista(this.getLista());
            if (!MantenerPaginacion) paginacion.setVista_Actual(1);
        }catch (Exception e){
            throw new Exception("Error en ObtenerListado [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
        
    }
    
    public void Buscar(String Ano, String Mes, String Stdjob, String Cliente , String Agencia ) throws Exception{
        try{
            this.ReiniciarDatos();
            this.Datos = DPDataAccess.BuscarPresupuesto(2 , Ano, Mes, Stdjob, Cliente, Agencia, false);
            this.Datos.put("Stdjob" ,Stdjob);
            this.Datos.put("Cliente",Cliente);
            this.Datos.put("Agencia",Agencia);
            this.Datos.put("Tipo","D");
            this.Datos.put("Ano" ,Ano);
            this.Datos.put("Mes" ,Mes);
        }catch (Exception e){
            throw new Exception("Error en Buscar [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    
    public void BuscarHistorial(String Distrito, String Estandar, String Ano, String Mes) throws Exception{
        try{
            this.ReiniciarHistorial();
            this.Historial = DPDataAccess.Historial(Distrito, Estandar, Ano, Mes, null);
        }catch (Exception e){
            throw new Exception("Error en BuscarHistorial [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void BuscarInfoCliente(Connection conPostgres, String CodigoCliente, int tipo) throws Exception{
        try{
            this.ReiniciarInfoCliente();
            this.infoCliente = DPDataAccess.infoCliente(conPostgres, CodigoCliente, tipo);
        }catch (Exception e){
            throw new Exception("Error en BuscarInfoCliente [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    
    public void BuscarAgenciaClienteStandar(String agencia, String cliente, int modo) throws Exception{
        try{
            this.ReiniciarListaAgenciaClienteStandar();
            this.ListaAgenciaClienteStandar = DPDataAccess.AgenciasClientesStandar(agencia, cliente, modo);
        }catch (Exception e){
            throw new Exception("Error en BuscarAgenciaClienteStandar [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    
   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void BuscarTasa(String Ano) throws Exception{
        try{
            this.ReiniciarTasa();
            this.ListaTasa = DPDataAccess.Tasa(Ano);
        }catch (Exception e){
            throw new Exception("Error en BuscarTasa [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    
    public void ConstruirVarJSTasa(){
        if (this.ListaTasa!=null){
            String varJS = "var tasa = [ ";
            for (int i=1; i<=12; i++){
                Beans ts = (Beans) ListaTasa.get( (i<10?"0":"") + String.valueOf(i)) ;
                String item = "\n['"+ ts.getValor("ANO")  + SEPARADOR + ts.getValor("MES")  + SEPARADOR+
                ts.getValor("DOL")  + SEPARADOR + ts.getValor("BOL")  + SEPARADOR +
                ts.getValor("DTF")  + "']";
                varJS += item + ",";
            }
            varJS = varJS.substring(0,varJS.length()-1) + " ]; ";
            this.varJSTasa = varJS;
        }
        else
            this.varJSTasa = " var tasa = []; ";
    }
    
    
    /////////////////////////////////////////////////////////////////////////////
    // setter
    public void ReiniciarListado(){
        this.Listado = null;
    }
    public void ReiniciarDatos(){
        this.Datos = null;
    }
    public void ReiniciarListaAgenciaClienteStandar(){
        this.ListaAgenciaClienteStandar = null;
    }
    public void ReiniciarTasa(){
        this.ListaTasa = null;
    }
    public void setOrigen(String valor){
        this.Origen = valor;
    }
    public void ReiniciarInfoCliente(){
        this.infoCliente = "";
    }
    public void ReiniciarHistorial(){
        this.Historial = null;
    }
    /////////////////////////////////////////////////////////////////////////////
    // getter
    public List getLista(){
        return (List) this.Listado.get("valores");
    }
    public Hashtable getListaTotales(){
        return (Hashtable) this.Listado.get("totales");
    }
    public int getNroColumnas(){
        return Integer.parseInt(this.Listado.get("nrocols").toString());
    }
    public String getValor(String key){
        return this.Listado.get(key).toString();
    }
    
    public List getDatos(){
        return (List) this.Datos.get("valores");
    }
    public String getDValor(String key){
        return this.Datos.get(key).toString();
    }
    
    public List getListadoAgenciasClientesStandar(){
        return this.ListaAgenciaClienteStandar;
    }
    public String getVarJSAgenciaClienteStandar(){
        return this.varJSAgenciaClienteStandar;
    }
    public Hashtable getListadoTasa(){
        return this.ListaTasa;
    }
    public String getVarJSTasa(){
        return this.varJSTasa;
    }
    public String getVarJSSeparador(){
        return " var separador = '"+ this.SEPARADOR +"';";
    }
    public String getOrigen(){
        return this.Origen;
    }
    public String getInfoCliente(){
        return this.infoCliente;
    }
    public List getHistorial(){
        return this.Historial;
    }
    
    public boolean Modificable(String TipoBD, String TipoVT){
        int tBD = (TipoBD.equals("M")?0:TipoBD.equals("S")?1:2);
        int tVT = (TipoVT.equals("M")?0:TipoVT.equals("S")?1:2);
        return (tVT >= tBD ? true: false);
    }
    
}