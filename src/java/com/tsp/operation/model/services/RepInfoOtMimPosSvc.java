/***********************************************************************************
 * Nombre clase : ............... RepInfoOtMimPosSvc.java                                   *
 * Descripcion :................. clase que maneja los servicios                               *
 * Autor :....................... Ing. Ivan Gomez                                  *
 * Fecha :....................... 22 de octubre de 2005, 11:02 AM                   *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/

package com.tsp.operation.model.services;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author amendez
 */
public class RepInfoOtMimPosSvc {
    RepInfoOtMimPosDAO repInfoOtMimPosDAO;
    
    /** Creates a new instance of RepInfoOtMimPosSvc */
    public RepInfoOtMimPosSvc() {
        repInfoOtMimPosDAO = new RepInfoOtMimPosDAO();
    }
    
    public void genRepInfoOtMimVsPos(String fecini, String fecfin) throws SQLException{
        repInfoOtMimPosDAO.genRepInfoOtMimVsPos(fecini, fecfin);
    }
    
    public Vector getReporte() {
        return repInfoOtMimPosDAO.getReporte();
    }
    
    public void setReporte(Vector reporte) {
        repInfoOtMimPosDAO.setReporte(reporte);
    }
}