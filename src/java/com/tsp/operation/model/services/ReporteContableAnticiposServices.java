/***************************************
    * Nombre Clase ............. ReporteContableAnticiposServices.java
    * Descripci�n  .. . . . . .  Sevicio para generar reporte 
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  04/08/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/



package com.tsp.operation.model.services;




import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.ReporteContableAnticiposDAO;
import com.tsp.operation.model.DAOS.AnticiposPagosTercerosDAO;
import javax.servlet.*;
import javax.servlet.http.*;




public class ReporteContableAnticiposServices {
    
    
    private ReporteContableAnticiposDAO  ReporteContableAnticiposDataAccess;
    private AnticiposPagosTercerosDAO    AnticiposPagosTercerosDataAccess;
    private List                         listReporte;
    
    private String                       fechaIni;
    private String                       fechaFin;    
    private String                       proveedor;
    private String                       nombreProveedor;
    
    
    
    public ReporteContableAnticiposServices() {
        ReporteContableAnticiposDataAccess  = new ReporteContableAnticiposDAO();
        AnticiposPagosTercerosDataAccess    =  new  AnticiposPagosTercerosDAO();
        reset();
    }
    
    public ReporteContableAnticiposServices(String dataBaseName) {
        ReporteContableAnticiposDataAccess  = new ReporteContableAnticiposDAO(dataBaseName);
        AnticiposPagosTercerosDataAccess    =  new  AnticiposPagosTercerosDAO(dataBaseName);
        reset();
    }
    
    
    
    
    
    public void reset(){
        listReporte     = new LinkedList();
        fechaIni        = "";
        fechaFin        = "";        
        proveedor       = "";
        nombreProveedor = "";
    }
    
    
    
    
    /**
     * M�todos que permite buscar la produccion por rango de fechas
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/
    public void  searchProduccion( String distrito,  String proveedor, String fechaIni, String fechaFin )throws Exception{
        try{
            reset();
            this.fechaIni         = fechaIni;
            this.fechaFin         = fechaFin;
            this.proveedor        = proveedor;
            this.nombreProveedor  = this.AnticiposPagosTercerosDataAccess.getNamePropietario(proveedor);
            
            this.listReporte  =  this.ReporteContableAnticiposDataAccess.getTransferidasByFechas(distrito, proveedor, fechaIni, fechaFin);
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
    
    
    public String getFechas(){
        return this.fechaIni +" "+ this.fechaFin;
    }
    
    
    /**
     * Getter for property listReporte.
     * @return Value of property listReporte.
     */
    public java.util.List getListReporte() {
        return listReporte;
    }    
    
    /**
     * Setter for property listReporte.
     * @param listReporte New value of property listReporte.
     */
    public void setListReporte(java.util.List listReporte) {
        this.listReporte = listReporte;
    }    
    
    /**
     * Getter for property fechaIni.
     * @return Value of property fechaIni.
     */
    public java.lang.String getFechaIni() {
        return fechaIni;
    }    
    
    /**
     * Setter for property fechaIni.
     * @param fechaIni New value of property fechaIni.
     */
    public void setFechaIni(java.lang.String fechaIni) {
        this.fechaIni = fechaIni;
    }    
    
    /**
     * Getter for property fechaFin.
     * @return Value of property fechaFin.
     */
    public java.lang.String getFechaFin() {
        return fechaFin;
    }    
    
    /**
     * Setter for property fechaFin.
     * @param fechaFin New value of property fechaFin.
     */
    public void setFechaFin(java.lang.String fechaFin) {
        this.fechaFin = fechaFin;
    }    
    
    /**
     * Getter for property proveedor.
     * @return Value of property proveedor.
     */
    public java.lang.String getProveedor() {
        return proveedor;
    }    
    
    /**
     * Setter for property proveedor.
     * @param proveedor New value of property proveedor.
     */
    public void setProveedor(java.lang.String proveedor) {
        this.proveedor = proveedor;
    }
    
    /**
     * Getter for property nombreProveedor.
     * @return Value of property nombreProveedor.
     */
    public java.lang.String getNombreProveedor() {
        return nombreProveedor;
    }
    
    /**
     * Setter for property nombreProveedor.
     * @param nombreProveedor New value of property nombreProveedor.
     */
    public void setNombreProveedor(java.lang.String nombreProveedor) {
        this.nombreProveedor = nombreProveedor;
    }
    
}
