/***********************************************************************************
 * Nombre clase : ............... ReporteOCAnticipoService.java                    *
 * Descripcion :................. Clase que maneja el Service que contiene los     *
 *                                metodos que interactuan entre el DAO y el Action *
 *                                para generar los datos del reporte               *
 *                                de OC con Anticipo sin tr�fico.                  *
 * Autor :....................... Ing. Iv�n Dar�o Devia Acosta                     *
 * Fecha :........................ 23 de noviembre de 2006, 10:25 PM               *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/
package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.sql.*;
/**
 *
 * @author  Ivan
 */
public class ReporteOCAnticipoService {
    /*
     *Declaracion de atributos
     */
    private ReporteOCAnticipoDAO  RTDataAccess;
    private List                  ListaRT;
    private List                  ListaVacios;              
    private ReporteOCAnticipo     Datos;
    
    // mfontalvo 20070306
    private Vector vector;
    
    /** Creates a new instance of RegistroTarjetaService */
    public ReporteOCAnticipoService() {
        RTDataAccess    = new ReporteOCAnticipoDAO();
        ListaRT         = new LinkedList();
        ListaVacios     = new LinkedList();
        Datos           = new ReporteOCAnticipo();
    }
    public ReporteOCAnticipoService(String dataBaseName) {
        RTDataAccess    = new ReporteOCAnticipoDAO(dataBaseName);
        ListaRT         = new LinkedList();
        ListaVacios     = new LinkedList();
        Datos           = new ReporteOCAnticipo();
    }
    
    
    /**
     * Metodo List, lista los registros las tablas de acuerdo a los parametros de busqueda
     * @autor : Ing. Iv�n Dar�o Devia Acosta
     * @param : String fechainicial
     * @param : String fechafinal
     * @param : String distrito
     * @see   : ReporteOCAnticipoDAO
     * @version : 1.0
     */
    public void List( String fechainicial, String fechafinal, String distrito ) throws Exception {
        try{
            ReiniciarLista();
            ListaRT = RTDataAccess.List(fechainicial, fechafinal, distrito);
        }
        catch(Exception e){
            throw new Exception("Error en ListBus [ReporteOCAnticipoService]...\n"+e.getMessage());
        }
    }
    
    
    public void ReiniciarDato(){
        this.Datos = null;
    }
    public void ReiniciarLista(){
        this.ListaRT = new LinkedList();
    }
    
     public void ReiniciarListaVacios(){
        this.ListaVacios = new LinkedList();
    }
    
    public ReporteOCAnticipo getDato(){
        return this.Datos;
    }
    
    public List getList(){
        return this.ListaRT;
    }
    
    public List getListVacios(){
        return this.ListaVacios;
    }
    
  /**
     * Metodo List, lista los registros las tablas de acuerdo a los parametros de busqueda
     * @autor : Ing. Enrique De Lavalle
     * @param : String fechainicial
     * @param : String fechafinal
     * @param : String distrito
     * @see   : ReporteOCAnticipoDAO
     * @version : 1.0
     */
    public void List_oc_vacias( String fechainicial, String fechafinal, String distrito ) throws Exception {
        try{
            ReiniciarListaVacios();
            ListaVacios = RTDataAccess.List_oc_vacias(fechainicial, fechafinal, distrito);
        }
        catch(Exception e){
            throw new Exception("Error en ListBus [ReporteOCAnticipoService]...\n"+e.getMessage());
        }
    }

    
    
    
    /**
     * Metodo para obtener las ocs generadas con anticipos sin reporte en trafico
     * @autor mfontalvo
     * @param fechainicial, fecha inicial donde se va a correr el proceso
     * @param fechafinal, fecha final donde se va a correr el proceso
     * @param distrito, distrito de las plamillas a consultar
     * @throws Exception.
     * @return Vector, arreglo de datos q contine las ocs.
     */
    public void buscarOcsConAnticiposSinReporte( String fechai, String fechaf, String distrito ) throws Exception{
        this.vector = RTDataAccess.obtenerOcsConAnticiposSinReporte(fechai, fechaf, distrito);
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return vector;
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        this.vector = vector;
    }
    
}
