/********************************************************************
 *      Nombre Clase.................   Perfil_vistaService.java
 *      Descripci�n..................   Service de PerfilVistaUsuarioDAO
 *      Autor........................   Rodrigo Salazar
 *      Fecha........................   5.07.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

public class PerfilVistaUsuarioService {
    
    private PerfilVistaUsuarioDAO pvud;
    
    /** Creates a new instance of PerfilVistaUsuarioService */
    public PerfilVistaUsuarioService() {
        pvud = new PerfilVistaUsuarioDAO();
    } 
    
    /**
     * Obtiene la propiedad pvu
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public PerfilVistaUsuario getPerfilVistaUsuario() {
        return pvud.getPerfilVistaUsuario();
    }
    
    /**
     * Obtiene la propiedad PerfilVistaUsuarios
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public Vector getPerfilVistaUsuarios() {
        return pvud.getPerfilVistaUsuarios();
    }
    
    /**
     * Establece la propiedad pvu
     * @autor Tito Andr�s Maturana
     * @param pvu Nuevo valor de la propiedad pvu
     * @version 1.0
     */
    public void setPerfilVistaUsuario(PerfilVistaUsuario pvu) {
        pvud.setPerfilVistaUsuario(pvu);
    }
    
    /**
     * Establece la propiedad PerfilVistaUsuarios
     * @autor Rodrigo Salazar
     * @param PerfilVistaUsuarios Nuevo valor de la propiedad pvu
     * @version 1.0
     */
    public void setPerfilVistaUsuarios(Vector pvuds) {
        pvud.setPerfilVistaUsuarios(pvuds);
    }
    
    /**
     * Inserta un nuevo registro en la tabla perfil_vista_usuario
     * @autor Rodrigo Salazar
     * @throws SQLException
     * @version 1.0
     */
    public void insertPerfilVistaUsuario(PerfilVistaUsuario tipo) throws SQLException{
        try{
            ////System.out.println("perfil service: "+tipo.getPerfil());
            pvud.setPerfilVistaUsuario(tipo);
            pvud.insertPerfilVistaUsuario();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Verifica la existencia de un registro en la tabla perfil_vista_usuario
     * @autor Rodrigo Salazar
     * @throws SQLException
     * @version 1.0
     */
    public boolean existPerfilVistaUsuario(String usuario, String perfil) throws SQLException{
        return pvud.existPerfilVistaUsuario(usuario, perfil);
    }
    
    
    /**
     * Obtiene un registro en la tabla perfil_vista_usuario
     * @autor Rodrigo Salazar
     * @param usuario Login del usuario
     * @param perfil Nombre del perfil
     * @throws SQLException
     * @version 1.0
     */
    public void serchPerfilVistaUsuario(String usuario, String perfil)throws SQLException{
        pvud.searchPerfilVistaUsuario(usuario, perfil);
    }
    
    /**
     * Lista todos los registros no anulados de la tabla perfil_vista_usuario
     * @autor Rodrigo Salazar
     * @throws SQLException
     * @version 1.0
     */
    public void listPerfilVistaUsuario()throws SQLException{
        pvud.listPerfilVistaUsuario();
    }
    
    /**
     * Anula un registro en la tabla perfil_vista_usuario
     * @autor Rodrigo Salazar
     * @param usu Login del usuario
     * @param codigo C�digo del perfil-vista-usuario
     * @throws SQLException
     * @version 1.0
     */
    public void anularPerfilVistaUsuario(String usuario, String codigo)throws SQLException{
        pvud.anularPerfilVistaUsuario(usuario ,codigo);
    }
    
    /**
     * Obtiene un registro en la tabla perfil_vista_usuario
     * @autor Rodrigo Salazar
     * @param cod C�digo del perfil-vista-usuario
     * @throws SQLException
     * @version 1.0
     */
    public Vector searchDetallePerfilVistaUsuarios(String codigo ) throws SQLException{
        try{
            return pvud.searchDetallePerfilVistaUsuarios(codigo);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Actualiza un registro en la tabla perfil_vista_usuario
     * @autor Rodrigo Salazar
     * @throws SQLException
     * @version 1.0
     */
    public void modificarPvu (PerfilVistaUsuario jsp) throws SQLException {
        try{
            pvud.setPerfilVistaUsuario(jsp);
            pvud.updatePerfilVistaUsuario();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
 
}
