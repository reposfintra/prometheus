/******************************************************************************
 * Nombre clase :      ReporteDemorasService.java                             *
 * Descripcion :       Service del ReporteDemorasService.java                 *
 * Autor :             LREALES                                                *
 * Fecha :             12 de septiembre de 2006, 12:00 PM                     *
 * Version :           1.0                                                    *
 * Copyright :         Fintravalores S.A.                                *
 ******************************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;

public class ReporteDemorasService {
    
    private ReporteDemorasDAO reporte;
    
    /** Creates a new instance of ReporteDemorasService */
    public ReporteDemorasService() {
        reporte = new ReporteDemorasDAO ();
    }
    public ReporteDemorasService(String dataBaseName) {
        reporte = new ReporteDemorasDAO (dataBaseName);
    }
    
    /** Funcion publica que obtiene el metodo getVector del DAO */
    public Vector getVectorReporte () throws SQLException {
        
        return reporte.getVector();
        
    }
    
    /** Funcion publica que obtiene el metodo getVector_clientes del DAO */
    public Vector getVector_clientes () throws SQLException {
        
        return reporte.getVector_clientes();
        
    }
    
    /** Funcion publica que obtiene el metodo reporteEspecifico del DAO */
    public void reporteEspecifico ( String cod_cli, String fecini, String fecfin ) throws SQLException {
        
        reporte.reporteEspecifico ( cod_cli, fecini, fecfin );
        
    }
     
    /** Funcion publica que obtiene el metodo reporteTodos del DAO */
    public void reporteTodos ( String fecini, String fecfin ) throws SQLException {
        
        reporte.reporteTodos ( fecini, fecfin );
        
    }
         
    /** Funcion publica que obtiene el metodo clientes del DAO */
    public void clientes ( String nom_cli ) throws SQLException {
        
        reporte.clientes ( nom_cli );
        
    }
    
}