/*
 * CaravanaService.java
 *
 * Created on 02 de agosto de 2005, 14:34
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.util.Util;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  Henry
 */
public class EscoltaVehiculoService {
    public EscoltaVehiculoDAO dao;
    public Vector vecEscoltas;
    
    public EscoltaVehiculoService() {
        dao = new EscoltaVehiculoDAO();
    }
  
    public void listarEscoltas() throws SQLException {
         dao.listarEscoltas();
    }    
    public Vector getVectorEscoltas(){
        return dao.getVectorEscoltas();
    }
    public void modificarVectorEscoltas(int pos, boolean val){
        dao.modificarVectorEscoltas(pos,val);
    }
    public void insertarEscoltaVehiculo(EscoltaVehiculo e) throws SQLException {
         dao.insertarEscoltaVehiculo(e);
    }    
    public void getEscoltaXPlanilla(String numpla) throws SQLException {
        dao.getEscoltaXPlanilla(numpla);         
    }
    public Vector getVectorModificarEscoltaCaravana() {         
         return dao.getVectorModificarEscolta();
    }     
     public void agregarVectorExcluido(Escolta es){
        dao.agregarVectorExcluidos(es);
    }
    public void eliminarPosicionVectorExcluido(int pos){
        dao.eliminarPosicionVectorExcluidos(pos);
    }    
     public Vector getVectorExcluidos(){
        return dao.getVectorExcluidos();
    }
     public Vector getVectorEscoltaCaravanaActual(){
        return dao.getVectorEscoltaCaravanaActual();
    }
    public void agregarVectorEscoltaCaravanaActual(Escolta es){
        dao.agregarVectorEscoltaCaravanaActual(es);
    }
    public void eliminarPosicionVectorEscoltaCaravanaActual(int pos){
        dao.eliminarPosicionVectorEscoltaCaravanaActual(pos);
    }    
    public boolean existeEscoltaCaravanaActual(Escolta es){
        return dao.existeEscoltaCaravanaActual(es);
    }    
    public void setRazonVectorExcluido(int pos, String razon){
        dao.setRazonVectorExcluido(pos, razon);
    }
    public void modificarEscoltaVehiculo(Escolta e) throws SQLException{
        dao.modificarEscoltaVehiculo(e);
    }
    public void limpiarVectorEscoltaActual(){
        dao.limpiarVectorEscoltaActual();
    }
    public void borrarPosicionMayorUnoVectorEscoltaActual(){
        dao.borrarPosicionMayorUnoVectorEscoltaActual();
    }
    public Escolta getInfoEscolta(String ced) throws SQLException{
        return dao.getInfoEscolta(ced);
    }
    public void buscarEscoltaAsignado(String placa,String opcion) throws SQLException {
        this.vecEscoltas = dao.buscarEscoltaAsignado(placa,opcion);
    }
    
    public void limpiarVectorExcluidos(){
        dao.limpiarVectorExcluidos();
    }
    public boolean  liberarEscoltas(String placa, String planilla) throws SQLException {
        return dao.liberarEscolta(placa,planilla);
    }
     public boolean  liberarEscoltasCaravana(String placa, int numC) throws SQLException {
        return dao.liberarEscoltaCaravana(placa,numC);
    }
    public void anularAsignacionEscolta(String numpla) throws SQLException {
        dao.anularAsignacionEscolta(numpla);
    }
    public void actualizarEscoltaIngresoTrafico(String planilla, String valor) throws SQLException{
        dao.actualizarEscoltaIngresoTrafico(planilla, valor);
    }
    public void getEscoltaXNumCaravana(String numCaravana) throws SQLException {
        dao.getEscoltaXNumCaravana(Integer.parseInt(numCaravana));
    }
    public int searchEscoltasCaravana(int numCaravana) throws SQLException {
        return dao.searchEscoltasCaravana(numCaravana);
    }
    public void insertarEscoltaCaravana(EscoltaVehiculo e) throws SQLException {
         dao.insertarEscoltaCaravana(e);
    }    
   
    public void getVectorEscoltasXCaravana(String numCaravana) throws SQLException {
        dao.getVectorEscoltasXCaravana(Integer.parseInt(numCaravana));
    }
    public String getPlacaXPlanilla(String numpla) throws SQLException {
        return dao.getPlacaXPlanilla(numpla);
    }
    public void actualizarPlanillaAsignada(String numpla) throws SQLException {
        dao.actualizarPlanillaAsignada(numpla);
    }
    
    /**
     * Obtiene los escoltas de las planillas con fecpla en un per�odod dado.
     * @autor Ing. Tito Andr�s Maturana
     * @param fechaI Fecha inicial del per�odo
     * @param fechaF Fecha final del per�odo
     * @throws SQLException
     * @see com.tsp.operation.model.DAOS.EscoltaVehiculoDAO#reporteEscoltas(String, String)
     * @version 1.0
     */    
    public void reporteEscoltas(String fechaI, String fechaF) throws SQLException {
        dao.reporteEscoltas(fechaI, fechaF);
    }
    
    /**
     * Obtiene la propiedad reporte
     * @autor Ing. Tito Andr�s Maturana
     * @throws SQLException
     * @see com.tsp.operation.model.DAOS.EscoltaVehiculoDAO#getReporte()
     * @version 1.0
     */   
    public Vector getReporte(){
        return dao.getReporte();
    }
    public void modificarEscoltaVehiculoCaravana(Escolta e) throws SQLException{
        dao.modificarEscoltaVehiculoCaravana(e);
    }
    /**
     * Actualiza el origen y el destino de un vehiculo escolta de una Caravana
     * @param Objeto EscoltaVehiculo contiene los datos del escolta a asignar a la caravana
     * @throws SQLException si existe un error en la conculta.
     */
    public void updateOriDestEscoltaCaravana( Escolta e ) throws SQLException {
        dao.updateOriDestEscoltaCaravana(e);
    }
      /**
     * Actualiza el origen y el destino de un vehiculo escolta de una Planilla
     * @param Objeto EscoltaVehiculo contiene los datos del escolta a asignar a la caravana
     * @throws SQLException si existe un error en la conculta.
     */
    public void updateOriDestEscolta( Escolta e ) throws SQLException {
        dao.updateOriDestEscolta(e);
    }
    
    /**
     * Getter for property vecEscoltas.
     * @return Value of property vecEscoltas.
     */
    public java.util.Vector getVecEscoltas() {
        return vecEscoltas;
    }
    
    /**
     * Setter for property vecEscoltas.
     * @param vecEscoltas New value of property vecEscoltas.
     */
    public void setVecEscoltas(java.util.Vector vecEscoltas) {
        this.vecEscoltas = vecEscoltas;
    }
    
      /**
     * Setter for property vecEscoltas.
     * @param vecEscoltas New value of property vecEscoltas.
     */
    public Vector getPlanillasEscolta(String placa) throws SQLException{
        return dao.getPlanillasEscolta(placa);
    }
    /**
     * Getter for property placas.
     * @return Value of property placas.
     */
    public java.util.Vector getPlacasEscolta() {
        return dao.getPlacas();
    }    
    
    
    /************************************************************************
     *                      ANDRES MATURANA DE LA CRUZ                      *
     ************************************************************************/
    /**
     * crea una lista de los escoltas disponibles
     * @throws SQLException si existe un error en la conculta.
     * @return void <tt>VistaCaravana</tt>
     */
    
    public void listarEscoltasSearch(String nomcond) throws SQLException {
        dao.listarEscoltasSearch(nomcond);
    }
     /**
     * Elimina el registro de la tabla escolta_vehiculo
     * @throws SQLException si existe un error en la conculta.
     * @return void <tt>VistaCaravana</tt>
     */
    public boolean eliminarEscoltas(String placa, String planilla) throws SQLException {
        return dao.eliminarEscolta(placa,planilla);
    }
      /**
     * Cambia el estado de un escolta asignado a un vehiculo a libre
     * @throws SQLException si existe un error en la conculta.
     * @return vacio
     */
    
    public boolean eliminarEscoltaCaravana(String placa,int numC) throws SQLException {
        return dao.eliminarEscoltaCaravana(placa,numC);
    }
       /**
     * Returna true si la planilla esta asignada a un escolta
     * @throws SQLException si existe un error en la conculta.
     * @return true,false
     */
    public boolean planillaAsignadaVehiculo(String planilla) throws SQLException {
        return dao.planillaAsignadaVehiculo(planilla);
    }
    
     /**
     * Retorna un vector de placas de escoltas asignadas a una planilla
     * @throws SQLException si existe un error en la conculta.
     * @return <tt>Vector escoltas</tt>
     */   
    
    public Vector getPlacaEscoltaAsignada(String numpla) throws SQLException {        
        return dao.getPlacaEscoltaAsignada(numpla);
    }
    
    
     /**
     * Metodo: listadoEscoltaSinGeneracion, permite buscar los escoltas que no se lengthhan generado facturas
     * @autor : Ing. jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void listadoEscoltaSinGeneracion( String proveedor, String distrito ) throws SQLException{
        dao.listadoEscoltaSinGeneracion( proveedor, distrito );
    }
    
     /**
     * Metodo: listadoEscoltaConFactura, permite buscar los escoltas que no se lengthhan generado facturas
     * @autor : Ing. jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void listadoEscoltaConFactura( String proveedor, String distrito ) throws SQLException{
        dao.listadoEscoltaConFactura( proveedor, distrito );
    }
    
     /**
     * Metodo: updateEscoltaVehiculo, permite actualizar los campos de escoltas vehiculos
     * @autor : Ing. Jose de la rosa
     * @param : objeto escolta vehiculo
     * @version : 1.0
     */
    public String updateEscoltaVehiculo(EscoltaVehiculo es) throws SQLException{
        return dao.updateEscoltaVehiculo( es );
    }
    
    /**
     * Metodo: updateGeneracionEscoltaVehiculo, permite actualizar los campos de escoltas vehiculos
     * @autor : Ing. Jose de la rosa
     * @param : objeto escolta vehiculo
     * @version : 1.0
     */
    public String updateGeneracionEscoltaVehiculo(String usuario, String estado, String numpla, String placa) throws SQLException{
         return dao.updateGeneracionEscoltaVehiculo(usuario, estado, numpla, placa);
    }
}