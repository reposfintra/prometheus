/***************************************
    * Nombre Clase ............. AdminSoftwareService.java
    * Descripci�n  .. . . . . .  Sevicio para la administracion de software
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  10/01/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/


package com.tsp.operation.model.services;



import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import javax.servlet.*;
import javax.servlet.http.*;



public class AdminSoftwareService {
    
    private  AdminSoftwareDAO  AdminSoftwareDataAccess;
    private  List              listaEquipos;
    private  List              listaSitios;
    private  List              listaArchivosSitio;    
    private  String[]          vectorArchivos;
    private  Programa          programa; 
    public   String            SEPERADOR_COLUMN;
    public   String            SEPERADOR_ROW;
    
    private  List              listaExt;
    private  List              listaFileByExt;
    private  String            extension;
    
    
    public AdminSoftwareService() {
        AdminSoftwareDataAccess = new AdminSoftwareDAO();
        reset();
    }
    
        
    
    
    /**
     * M�todos que setea las variables
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public void reset(){
        listaArchivosSitio      = new LinkedList();
        programa                = new Programa();
        this.SEPERADOR_COLUMN   = this.AdminSoftwareDataAccess.SEPARADOR;
        this.SEPERADOR_ROW      = "|";
        this.listaExt           = new LinkedList();
        this.listaFileByExt     = new LinkedList();
        this.extension          = "";
    }
    
    
    
    
    
     /**
     * M�todos que recibe los parametros enviados por el request y forma el objeto progranma
     * @autor.......fvillacob
     * @version.....1.0.     
     **/ 
    public void loadRequest(HttpServletRequest request){    
        this.programa.setDistrito   ( format( request.getParameter("distrito"))     );
        this.programa.setEquipo     ( format( request.getParameter("equipo"))       );
        this.programa.setSitio      ( format( request.getParameter("sitio") )       );
        this.programa.setPrograma   ( format( request.getParameter("programa"))     );
        this.programa.setDescripcion( format( request.getParameter("descripcion"))  );
        this.programa.setProgramador( format( request.getParameter("programador"))  );          
    }
    
    
    
    
    
    
    
    /**
     * M�todos que carga los equipos y sitios
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/ 
    public void loadDatos()throws Exception{
        try{
            searchEquipos();
            searchSitios();            
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    
    
    
    
    /**
     * M�todos que formatea valores
     * @autor.......fvillacob
     * @version.....1.0.     
     **/ 
    public String format(String val){
        if(val==null)
            val = "";
        return val;
    }
    
    
    
    /**
     * M�todos que busca los dif. equipos de la tabla inventario
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/ 
    private void searchEquipos()throws Exception{
        this.listaEquipos = new LinkedList();
        try{
            this.listaEquipos = this.AdminSoftwareDataAccess.searchEquipos();
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    
    
    
    /**
     * M�todos que busca los dif. sitios por equipos de la tabla inventario
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/ 
    private void searchSitios()throws Exception{
        this.listaSitios = new LinkedList();
        try{
            this.listaSitios = this.AdminSoftwareDataAccess.searchSitios();
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    
    
     /**
     * M�todos que genera la variables js del listado de equipo
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/ 
    public String getEquiposJS()throws Exception{
        String varJs="";
        try{            
            if(this.listaEquipos.size()>0){
               varJs = " var equipos ='"; 
               for(int i=0; i<this.listaEquipos.size();i++){
                  String eq = (String)this.listaEquipos.get(i);
                  if(i>0)
                      varJs += this.SEPERADOR_ROW;
                  varJs += eq;
               }
               varJs+="';";
            }            
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return varJs;
    }
    
    
    
    
    
    
    
    /**
     * M�todos que genera la variables js del listado de sitios
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/ 
    public String getSitiosJS()throws Exception{
        String varJs="";
        try{            
            if(this.listaSitios.size()>0){
               varJs = " var sitios ='"; 
               
               for(int i=0; i<this.listaSitios.size();i++){
                  String sitio = (String)this.listaSitios.get(i);
                  if(i>0)
                      varJs += this.SEPERADOR_ROW;
                  varJs += sitio;
               }
               
               varJs+="';";
            }            
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return varJs;
    }
    
    
    
    
    /**
     * M�todos que busca los dif. programas del sitio
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/ 
    public void searchFile()throws Exception{
        this.listaArchivosSitio = new LinkedList();
        try{
            this.listaArchivosSitio = this.AdminSoftwareDataAccess.searchFile(this.programa.getEquipo(),this.programa.getSitio());
             distinctExt();
             String ext = (String) this.listaExt.get(0);
             this.fileExt(ext);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    
    
    /**
     * M�todos que busca los dif. programas del sitio
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/ 
    public void distinctExt()throws Exception{
        this.listaExt = new LinkedList();
        if(this.listaArchivosSitio!=null){
            for(int i=0; i<this.listaArchivosSitio.size();i++){
                InventarioSoftware file = (InventarioSoftware) this.listaArchivosSitio.get(i);
                String ext  = file.getExtension();
                int sw      = 0;                
                for(int j=0; j<this.listaExt.size(); j++){
                     String extNew = (String) this.listaExt.get(j);
                     if( extNew.equals(ext) ){                         
                         sw = 1;
                         break;
                     }
                }
                if(sw==0)
                    this.listaExt.add( ext );
            }
        }
    }
    
    
    
    
    
    
    
    /**
     * M�todos que selecciona los file de la Ext
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/ 
    public void fileExt(String ext)throws Exception{
        this.extension = ext;
        this.listaFileByExt = new LinkedList();
        if(this.listaArchivosSitio!=null){
            for(int i=0; i<this.listaArchivosSitio.size();i++){
                InventarioSoftware file = (InventarioSoftware) this.listaArchivosSitio.get(i);
                if(file.getExtension().equals(ext)){
                    listaFileByExt.add(file);
                }
            }
        }
    }
    
    
    
    
    
    
    
    
    /**
     * M�todos que guarda el registro
     * @autor.......fvillacob
     * @throws      Exception 
     * @version.....1.0.     
     **/ 
    public String save(String user)throws Exception{
        String msj = "El programa " + this.programa.getDescripcion() + " ha sido guardado exitosamente....";
        try{            
            selectFile();
            this.AdminSoftwareDataAccess.save(this.programa, user);
        }catch(Exception e){
            msj = e.getMessage();
        }
        return msj;
    }
    
    
    
    /**
     * M�todos que selecciona los archivos del programa del listado del sitio
     * @autor.......fvillacob
     * @version.....1.0.     
     **/ 
    
    public void selectFile()throws Exception{
        try{
            List aux = new LinkedList();
            if(this.vectorArchivos.length>0){
                for(int i=0; i<this.vectorArchivos.length;i++){
                    int id = Integer.parseInt( this.vectorArchivos[i] );
                    InventarioSoftware  file = (InventarioSoftware ) this.listaArchivosSitio.get(id);
                    aux.add(file);
                }               
            }
            this.programa.setListFile(aux);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    
    
    
    
    
    
    // SET:
    /**
     * M�todos que setea la lista de archivos del  programa
     * @autor.......fvillacob
     * @version.....1.0.     
     **/ 
    public void setFilesProgram(String[] val){
        this.vectorArchivos = val;
    }
    
    
    
    
    
    
    
    
    // GET:
    /**
     * M�todos que devuelve la lista de Extensiones de Archivos del sitio
     * @autor.......fvillacob
     * @version.....1.0.     
     **/ 
    public List getExt(){
        return this.listaExt;
    }
    
    
    
    /**
     * M�todos que devuelve la Extension seleccionada
     * @autor.......fvillacob
     * @version.....1.0.     
     **/ 
    public String getExtension(){
        return this.extension;
    }
    
    
    
    /**
     * M�todos que devuelve la lista de Archivos de Extensiones de Archivos del sitio
     * @autor.......fvillacob
     * @version.....1.0.     
     **/ 
    public List getFilesExt(){
        return this.listaFileByExt;
    }
    
    
    
    /**
     * M�todos que devuelve la lista de archivos del sitio del programa
     * @autor.......fvillacob
     * @version.....1.0.     
     **/ 
    public List getFileSitio(){
        return this.listaArchivosSitio;
    }
    
    
    
    
    
    /**
     * M�todos que devuelve el programa
     * @autor.......fvillacob
     * @version.....1.0.     
     **/ 
    public Programa getPrograma(){
        return this.programa;
    }
    
    
    
}
