/*
 * ConsultaService.java
 *
 * Created on 28 de julio de 2005, 07:27 PM
 *
 * Service para la opcion de gestion de consulta
 * 
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.*;
import java.util.*;
import java.sql.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author amendez
 */
public class ConsultaService {
    
    private ConsultaDao consultaDataAcces;
    /** Creates a new instance of ConsultaService */
    public ConsultaService() {
        consultaDataAcces = new ConsultaDao();
    }
    public ConsultaService(String dataBaseName) {
        consultaDataAcces = new ConsultaDao(dataBaseName);
    }
    
    public List<ConsultaSQL> getConsultasSQL( Usuario user ) throws SQLException {
        return consultaDataAcces.getConsultasSQL(user);
    }
    
    public void consultasManageSqlCreate(Hashtable<String, String[]> parameters) throws SQLException, InformationException {
        consultaDataAcces.consultasManageSqlCreate(parameters);
    }
    
    public synchronized void consultasManageSqlExecute(Hashtable<String, String[]> parameters) throws SQLException, InformationException {
        consultaDataAcces.consultasManageSqlExecute(parameters);
    }
    
    public ConsultaSQL getQry(String id) throws SQLException{
        return consultaDataAcces.getQry(id);
    }
    
    public void consultasManageSqlUpdate(Hashtable<String, String[]> parameters) throws SQLException, InformationException {
        consultaDataAcces.consultasManageSqlUpdate(parameters);
    }
    
    public void consultasManageSqlDelete( String id ) throws SQLException{
        consultaDataAcces.consultasManageSqlDelete(id);
    }
    
    public Usuario UsuarioSearch(final String id) throws Exception {
      return consultaDataAcces.UsuarioSearch(id);
    }
    
    public List getUsuariosSot(String tipo) throws Exception {
      return consultaDataAcces.getUsuariosSot(tipo);
    }

    
    public synchronized List<ConsultaSQL> getActionResultsList() {
        return consultaDataAcces.getActionResultsList();
    }
    
    public synchronized void setActionResultsList(List<ConsultaSQL> actionResultsList) {
        consultaDataAcces.setActionResultsList(actionResultsList);
    }
    
    public ArrayList<ComponentesCosulta> getValidarComponente(String id) {
        ArrayList<ComponentesCosulta> lista = null;
        try {
            lista = consultaDataAcces.getValidarComponente(id);
            lista.isEmpty();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return lista;

    }
    
      public String getObtenerCuentas(Usuario user) {
        String lista = "";
        try {
            lista = consultaDataAcces.getObtenerCuentas(user);
            String [] cuentas =consultaDataAcces.getObtenerCuentas(user).split(",");
            for(int j=0;j<=cuentas.length-1 ;j++){
                
                if(cuentas.length-1==0 && cuentas[j].equals("")){
                    System.out.println("No tines cuentas asociadas");
                }else{
                   System.out.println("si tines cuentas asociadas");
                }
                
                
               System.out.println(cuentas[j]);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return lista;

    }
}
