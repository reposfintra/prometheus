/*
 * Nombre        ContactoService.java
 * Autor         Ing. Sandra M. Escalante G.
 * Fecha         24 de febrero de 2005, 10:42 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;

public class ContactoService {
    
    private ContactoDAO contactod;
    
    /** Creates a new instance of ContactoService */
    public ContactoService() {
        contactod = new ContactoDAO();
    }
    
    /**
     * Metodo insertarContacto, inserta un contacto
     * @autor  Ing. Rodrigo Salazar
     * @see  setContacto(Contacto contacto), insertarContacto()
     * @version  1.0
     */
    public void insertarContacto(Contacto contacto)throws SQLException {
        try{
            contactod.setContacto(contacto);
            contactod.insertarContacto();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo contactos, listar contactos
     * @autor  Ing. Rodrigo Salazar
     * @see  contactos()
     * @version  1.0
     */
    public void contactos() throws SQLException {
        try{
            contactod.contactos();
        }
        catch (SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    
    /**
     * Metodo obtenerContacto, obtener objeto contacto ya instanciado
     * @autor  Ing. Rodrigo Salazar
     * @see  obtenerContacto()
     * @version  1.0
     */
    public Contacto obtenerContacto()throws SQLException{
        return contactod.obtenerContacto();
    }
    
    /**
     * Metodo obtcontactos, obtener el vector de contactos
     * @autor  Ing. Rodrigo Salazar
     * @see  obtcontactos()
     * @version  1.0
     */
    public Vector obtcontactos()throws SQLException{
        return contactod.obtContactos();
    }
    
    
    /**
     * Metodo buscarContacto, buscar un contacto dado el codigo y la compa�ia
     * @autor  Ing. Rodrigo Salazar
     * @see  buscarContacto( String cod, String codcia)
     * @version  1.0
     */
    public void buscarContacto(String cod, String codcia) throws SQLException {
        try{
            contactod.buscarContacto(cod, codcia);
        }
        catch(SQLException e){
            throw new SQLException( e.getMessage());
        }
    }
    
    
    /**
     * Metodo buscarContactoNombre, busca los contactos dados codigo
     *  compania, tipo de contacto
     * @autor  Ing. Rodrigo Salazar
     * @see  buscarContactoNombre( String cto, String ccia, String tipo)
     * @version  1.0
     */
    public void buscarContactoNombre(String cto, String ccia, String tipo) throws SQLException {
        try{
            contactod.buscarContactoNombre(cto, ccia, tipo);
        }
        catch(SQLException e){
            throw new SQLException( e.getMessage());
        }
    }
    
    
    /**
     * Metodo modificarContacto, actualiza un registro contacto en el sistema
     * @autor  Ing. Rodrigo Salazar
     * @see  setContacto(Contacto contacto), modificarContacto()
     * @version  1.0
     */
    public void modificarContacto(Contacto contacto)throws SQLException {
        try{
            contactod.setContacto(contacto);
            contactod.modificarContacto();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    
    /**
     * Metodo anularcontacto, eliminar un registro contacto del sistema
     * @autor  Ing. Rodrigo Salazar
     * @see  setContacto(Contacto contacto), anularContacto()
     * @version  1.0
     */
    public void anularcontacto(Contacto contacto)throws SQLException {
        try{
            contactod.setContacto(contacto);
            contactod.anularContacto();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    
    /**
     * Metodo buscarContactoCia, buscar contactos dado una compania
     * @autor  Ing. Rodrigo Salazar
     * @see  buscarContactoCia( String cia)
     * @version  1.0
     */
    public void buscarContactoCia(String cia) throws SQLException {
        contactod.buscarContactoCia(cia);
    }
    
    
    /**
     * Metodo getVecContacto, obtiene el vector de contactos
     * @autor  Ing. Rodrigo Salazar
     * @see  getVecContacto()
     * @version  1.0
     */
    public java.util.Vector getVecContactos() {
        return contactod.getVecContactos();
    }
    
    
    /**
     * Metodo setVecContacto, obtiene el vector de contactos
     * @autor  Ing. Rodrigo Salazar
     * @see  setVecContacto()
     * @version  1.0
     */
    public void setVecContactos(java.util.Vector VecContactos) {
        contactod.setVecContactos(VecContactos);
    }
}
