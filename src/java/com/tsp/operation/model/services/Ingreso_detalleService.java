/****************************************************************************************************
 * Nombre clase: Ingreso_detalleService.java                                                        *             *
 * Descripci�n: Clase que maneja los servicios al model relacionados con los detalles deingreso.    *                                                   *
 * Autor: Ing. Jose de la rosa                                                                      *
 * Fecha: 10 de mayo de 2006, 08:34 AM                                                              *
 * Versi�n: Java 1.0                                                                                *
 * Copyright: Fintravalores S.A. S.A.                                                          *
 ***************************************************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.TransaccionService;

public class Ingreso_detalleService {
    public Ingreso_detalleDAO ingresodetalle;
    public Vector itemsMiscelaneo;
    
    //Datos para modificar items
    String items = "";
    Vector vecing =  new Vector ();
    Vector vecmod = new Vector ();
    
    /** Creates a new instance of Ingreso_detalleService */
    public Ingreso_detalleService () {
        ingresodetalle = new Ingreso_detalleDAO ();
    }
    public Ingreso_detalleService (String dataBaseName) {
        ingresodetalle = new Ingreso_detalleDAO (dataBaseName);
    }
    
    
    /**
     * Getter for property ingreso_detalle.
     * @return Value of property ingreso_detalle.
     */
    public Ingreso_detalle getIngreso_detalle () {
        return ingresodetalle.getIngreso_detalle ();
    }
    
    /**
     * Setter for property ingreso_detalle.
     * @param ingreso_detalle New value of property ingreso_detalle.
     */
    public void setIngreso_detalle (Ingreso_detalle ingreso) {
        ingresodetalle.setIngreso_detalle (ingreso);
    }
    
    /**
     * Getter for property vecIngreso_detalle.
     * @return Value of property vecIngreso_detalle.
     */
    public Vector getVecIngreso_detalle () {
        return ingresodetalle.getVecIngreso_detalle ();
    }
    
    /**
     * Setter for property vecIngreso_detalle.
     * @param vecIngreso_detalle New value of property vecIngreso_detalle.
     */
    public void setVectorIngreso_detalle ( Vector VIngreso ) {
        ingresodetalle.setVectorIngreso_detalle ( VIngreso );
    }
    
    /**
     * Setter for property vecIngreso_detalle.
     * @param vecIngreso_detalle New value of property vecIngreso_detalle.
     */
    public void setVecIngreso_detalle (Ingreso_detalle ingreso) {
        ingresodetalle.setVecIngreso_detalle ( ingreso );
    }
    
    /**
     * Setter for property vecIngreso_detalle.
     * @param vecIngreso_detalle New value of property vecIngreso_detalle.
     */
    public void resetVecIngreso_detalle () {
        ingresodetalle.resetVecIngreso_detalle ();
    }
    
    
    /**
     * Setter for property vecIngreso_detalle.
     * @param vecIngreso_detalle New value of property vecIngreso_detalle.
     */
    public void deleteVecIngreso_detalle (int pos){
        ingresodetalle.deleteVecIngreso_detalle (pos);
    }
    
    /**
     * Getter for property cabecera.
     * @return Value of property cabecera.
     */
    public Ingreso_detalle getCabecera () {
        return ingresodetalle.getCabecera ();
    }
    
    /**
     * Setter for property cabecera.
     * @param cabecera New value of property cabecera.
     */
    public void setCabecera (Ingreso_detalle cabecera) {
        ingresodetalle.setCabecera (cabecera);
    }
    
    /**
     * Getter for property factura.
     * @return Value of property factura.
     */
    public Ingreso_detalle getFactura () {
        return ingresodetalle.getFactura ();
    }
    
    /**
     * Setter for property factura.
     * @param factura New value of property factura.
     */
    public void setFactura (Ingreso_detalle factura) {
        ingresodetalle.setFactura (factura);
    }
    
    /**
     * Getter for property vecFactura.
     * @return Value of property vecFactura.
     */
    public java.util.Vector getVecFactura () {
        return ingresodetalle.getVecFactura ();
    }
    
    /**
     * Setter for property vecFactura.
     * @param vecFactura New value of property vecFactura.
     */
    public void setVecFactura (Vector vec) {
        ingresodetalle.setVecFactura ( vec );
    }
    
    /**
     * Metodo: insertarIngresoDetalle, permite ingresar un registro en la tabla ingreso_detalle en la BD.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String insertarIngresoDetalle (Ingreso_detalle ingreso) throws SQLException {
        try{
            ingresodetalle.setIngreso_detalle (ingreso);
            return ingresodetalle.insertarIngresoDetalle ();
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
    /**
     * Metodo: updateIngresoDetalle, permite actualizar un registro en la tabla ingreso_detalle en la BD.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String updateIngresoDetalle (Ingreso_detalle ingreso) throws SQLException {
        try{
            ingresodetalle.setIngreso_detalle (ingreso);
            return ingresodetalle.updateIngresoDetalle ();
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
    /**
     * Metodo: anularIngresoDetalle, permite anular un registro en la tabla ingreso_detalle en la BD.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String anularIngresoDetalle (Ingreso_detalle ingreso) throws Exception {
        try{
            ingresodetalle.setIngreso_detalle (ingreso);
            return ingresodetalle.anularIngresoDetalle ();
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    /**
     * Metodo: datosCabecera, permite buscar los datos de la cabecera del ingreso
     * @autor : Ing. Jose de la rosa
     * @param : distrito, numero de ingreso y el tipo de dosumento
     * @version : 1.0
     */
    public void datosCabecera ( String distrito, String numero_ingreso, String tipo_documento, String tipo_ingreso ) throws SQLException {
        try{
            ingresodetalle.datosCabecera ( distrito, numero_ingreso, tipo_documento, tipo_ingreso );
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
    /**
     * Metodo: existeItem, permite verificar si existe el item de ingreso
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public boolean existeItem ( String distrito, String numero_ingreso, String tipo_documento, int item ) throws SQLException {
        try{
            return ingresodetalle.existeItem ( distrito, numero_ingreso, tipo_documento, item );
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    /**
     * Metodo: datosFactura, permite buscar los datos de la factura
     * @autor : Ing. Jose de la rosa
     * @param : distrito, numero de la factura y el tipo de dosumento
     * @version : 1.0
     */
    public void datosFactura ( String distrito, String numero_factura, String tipo_documento, String cliente ) throws SQLException {
        try{
            ingresodetalle.datosFactura ( distrito, numero_factura, tipo_documento, cliente );
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
    /**
     * Metodo: monedaLocal, permite buscar la moneda local del distrito
     * @autor : Ing. Jose de la rosa
     * @param : distrito
     * @version : 1.0
     */
    public String monedaLocal ( String distrito ) throws SQLException {
        try{
            return ingresodetalle.monedaLocal (distrito);
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
    /**
     * Metodo : funcion que guarda un objeto tipo Ingreso detalle en un Archivo
     * @autor : Ing. Jose de la rosa
     * @param : objeto ingreso, un vector de ingreso, nombre del archivo, el usuario
     * @version : 1.0
     */
    public void escribirArchivo (Ingreso_detalle ingreso, Vector items, String nombreArchivo, String user)throws IOException{
        ResourceBundle rb = ResourceBundle.getBundle ("com/tsp/util/connectionpool/db");
        String path = rb.getString ("ruta");
        String Ruta  = path + "/exportar/migracion/" + user;
        String RutaA = Ruta + "/"+nombreArchivo;
        File archivo = new File (Ruta);
        archivo.mkdirs ();
        File archivoR =  new File (RutaA);
        FileOutputStream  salidaArchivoR =   new FileOutputStream (archivoR);
        ObjectOutputStream salidaObjetoR =   new ObjectOutputStream (salidaArchivoR);
        salidaObjetoR.writeObject (ingreso);
        salidaObjetoR.writeObject (items);
        salidaObjetoR.close ();
    }
    
    /**
     * Metodo: funcion que Lee  un objeto tipo ingreso detalle en un Archivo
     * @autor : Ing. Jose de la rosa
     * @param : nombre del archivo y el usuario
     * @version : 1.0
     * @return : objeto de tipo ingreso detalle.
     */
    public Ingreso_detalle leerArchivo (String nombreArchivo, String user)throws IOException,ClassNotFoundException{
        Ingreso_detalle ingreso = new Ingreso_detalle ();
        ResourceBundle rb = ResourceBundle.getBundle ("com/tsp/util/connectionpool/db");
        String path = rb.getString ("ruta");
        String Ruta  = path + "/exportar/migracion/" + user + "/"+nombreArchivo;
        File archivo = new File (Ruta);
        FileInputStream entradaArchivo = new FileInputStream (archivo);
        ObjectInputStream entradaObjeto= new ObjectInputStream (entradaArchivo);
        try{
            ingreso = (Ingreso_detalle) entradaObjeto.readObject ();
        }
        catch(ClassNotFoundException e){
            ingreso = null;
        }
        entradaObjeto.close ();
        return ingreso;
    }
    
    /**
     * Metodo: funcion que Lee  un objeto tipo Vector en un Archivo
     * @autor : Ing. Jose de la rosa
     * @param : nombre del archivo y el usuario
     * @version : 1.0
     * @return : Objeto tipo Vector con los Items del ingreso detalle.
     */
    public Vector leerArchivoItems (String nombreArchivo,String user)throws IOException,ClassNotFoundException{
        Vector items;
        Ingreso_detalle ingreso;
        ResourceBundle rb = ResourceBundle.getBundle ("com/tsp/util/connectionpool/db");
        String path = rb.getString ("ruta");
        String Ruta  = path + "/exportar/migracion/" + user + "/"+nombreArchivo;
        File archivo = new File (Ruta);
        FileInputStream entradaArchivo = new FileInputStream (archivo);
        ObjectInputStream entradaObjeto= new ObjectInputStream (entradaArchivo);
        try{
            ingreso = (Ingreso_detalle) entradaObjeto.readObject ();
            items = (Vector) entradaObjeto.readObject ();
        }
        catch(ClassNotFoundException e){
            items=null;
        }
        entradaObjeto.close ();
        return items;
    }
    
    
    /**
     * Metodo : funcion que guarda un objeto tipo Ingreso detalle en un Archivo
     * @autor : Ing. Jose de la rosa
     * @param : objeto ingreso, un vector de ingreso, nombre del archivo, el usuario
     * @version : 1.0
     */
    public void borrarArchivo (String nombreArchivo, String user)throws IOException{
        ResourceBundle rb = ResourceBundle.getBundle ("com/tsp/util/connectionpool/db");
        String path = rb.getString ("ruta");
        String Ruta  = path + "/exportar/migracion/" + user+"/";
        File f = new File (Ruta + nombreArchivo );
        if(f.exists ()){
            f.delete ();
        }
    }
    /**
     * Metodo: valorConversionTasa, permite buscar el valor de convercion de la tasa
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public double valorConversionTasa ( String moneda_origen, double valor_origen, String moneda_destino, String distrito, String valor ) throws SQLException {
        try{
            return ingresodetalle.valorConversionTasa (moneda_origen, valor_origen, moneda_destino, distrito, valor);
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
     /* Metodo: datosListadoFactura, permite listar las facturas de un cliente
      * @autor : Ing. Jose de la rosa
      * @param :
      * @version : 1.0
      */
    public void datosListadoFactura ( String distrito, String nit_cliente, String tipo_documento ) throws SQLException {
        try{
            ingresodetalle.datosListadoFactura (distrito, nit_cliente, tipo_documento);
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
    /**
     * Metodo: datosListadoFactura, permite listar las facturas de un cliente
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void datosListadoFacturaUpdate ( String distrito, String nit_cliente, String tipo_documento ) throws SQLException {
        try{
            ingresodetalle.datosListadoFacturaUpdate (distrito, nit_cliente, tipo_documento);
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
     /* Metodo: updateCantidadItemsIngreso, permite actualizar la cantidad de item de la tabla ingreso en la BD.
      * @autor : Ing. Jose de la rosa
      * @param : cantidad de items de ingreso, numero del ingreso, distrito, el tipo de documento
      * @version : 1.0
      */
    public String updateCantidadItemsIngreso ( int cantidad, String num_ingreso, String distrito,String tipo_ingreso, String tipo_documento) throws SQLException {
        try{
            return ingresodetalle.updateCantidadItemsIngreso ( cantidad, num_ingreso, distrito, tipo_ingreso, tipo_documento);
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
     /* Metodo: updateSaldoFactura, permite actualizar el saldo de la factura
      * @autor : Ing. Jose de la rosa
      * @param : valor del abono de la factura, valor del abono moneda extranjera de la factura, factura, distrito, el tipo de documento
      * @version : 1.0
      */
    public String updateSaldoFactura (double abono, double abono_me, String factura, String distrito, String tipo_documento) throws SQLException {
        try{
            return ingresodetalle.updateSaldoFactura (abono, abono_me, factura, distrito, tipo_documento);
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
    /* Metodo: updateSaldoFactura, permite actualizar el saldo de la factura
     * @autor : Ing. Jose de la rosa
     * @param : valor del abono de la factura, valor del abono moneda extranjera de la factura, factura, distrito, el tipo de documento
     * @version : 1.0
     */
    public String anularSaldoFactura (double abono, double abono_me, String factura, String distrito, String tipo_documento) throws SQLException {
        try{
            return ingresodetalle.anularSaldoFactura (abono, abono_me, factura, distrito, tipo_documento);
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
     /* Metodo: updateSaldoFactura_modificacion, permite actualizar el saldo de la factura
      * @autor : Ing. Jose de la rosa
      * @param : valor del abono de la factura, valor del abono moneda extranjera de la factura, factura, distrito, el tipo de documento
      * @version : 1.0
      */
    public String updateSaldoFactura_modificacion (double abonoA, double abono_meA, double abonoV, double abono_meV, String factura, String distrito, String tipo_documento) throws SQLException {
        try{
            return ingresodetalle.updateSaldoFactura_modificacion (abonoA, abono_meA, abonoV, abono_meV, factura, distrito, tipo_documento);
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    /**
     * Metodo: datosListadoItems, permite listar los items de ingreso de un cliente
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Vector datosListadoItems ( String num_ingreso, String tipo_doc, String distrito) throws SQLException {
        try{
            return ingresodetalle.datosListadoItems (num_ingreso, tipo_doc, distrito);
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
        
    }
    /**
     * Metodo: insertarIngresoMiscelaneoDetalle, permite ingresar un registro
     * en la tabla ingreso_detalle en la BD.
     * @autor : Ing. Diogenes Bastidas M
     * @param : Objeto tipo Ingreso detalle
     * @version : 1.0
     */
    public void insertarIngresoMiscelaneoDetalle (Vector vecitems) throws SQLException {
        try{
            String SQL = ingresodetalle.insertarIngresoMiscelaneoDetalle (vecitems);
            TransaccionService tService = new TransaccionService (this.ingresodetalle.getDatabaseName());
            tService.crearStatement ();
            tService.getSt ().addBatch (SQL);
            tService.execute ();
            
        }catch(Exception e){
            e.printStackTrace ();
        }
    }
    
    /**
     * Metodo: itemsMiscelaneos, permite listar los items de ingreso miscelaneo
     * @autor : Ing. Diogenes Bastidas
     * @param : numero de ingreso, tipo documento, ditrito
     * @version : 1.0
     */
    public void itemsMiscelaneo ( String num_ingreso, String tipo_doc, String distrito) throws Exception {
        try{
            setItemsMiscelaneo (ingresodetalle.itemsMiscelaneo (num_ingreso,tipo_doc, distrito));
        }catch(Exception e){
            e.printStackTrace ();
        }
    }
    
    /**
     * Getter for property itemsMiscelaneo.
     * @return Value of property itemsMiscelaneo.
     */
    public java.util.Vector getItemsMiscelaneo () {
        return itemsMiscelaneo;
    }
    
    /**
     * Setter for property itemsMiscelaneo.
     * @param itemsMiscelaneo New value of property itemsMiscelaneo.
     */
    public void setItemsMiscelaneo (Vector itemsMiscelaneo) {
        this.itemsMiscelaneo = itemsMiscelaneo;
    }
    /**
     * Metodo: nroMayorItems, busca el numero maximo de los items de un ingreso
     * @autor : Ing. Diogenes Bastidas
     * @param :  ditrito, numero de ingreso, tipo documento
     * @version : 1.0
     */
    public int nroMayorItems (String dstrct, String tipo, String nroing) throws Exception {
        return ingresodetalle.nroMayorItems (dstrct, tipo, nroing);
    }
    
    public void clasificarItemsMiscelaneos (String num_ingreso, String tipo_doc, String distrito, Vector itemsMiscelaneo){
        boolean sw = false;
        try{
            items = "";
            vecing =  new Vector ();
            vecmod = new Vector ();
            
            //busco los ingresos miscelaneos
            Vector vecbd = ingresodetalle.codigositemsMiscelaneo (num_ingreso, tipo_doc, distrito );
            
            // obtengo los que se deben anular
            for(int i=0; i < vecbd.size (); i++ ){
                Ingreso_detalle item = (Ingreso_detalle) vecbd.get (i);
                for (int j=0; j < itemsMiscelaneo.size (); j++){
                    Ingreso_detalle item1 = (Ingreso_detalle) itemsMiscelaneo.get (j);
                    if( item.getItem () == item1.getItem () ){
                        sw =  true;
                        this.vecmod.add (item1);
                        break;
                    }
                }
                if(!sw){
                    items+=item.getItem ()+",";
                }
                sw =  false;
            }
            
            //verifico los nuevos para insertarlos
            sw =  false;
            for(int i=0; i < itemsMiscelaneo.size (); i++ ){
                Ingreso_detalle item1 = (Ingreso_detalle) itemsMiscelaneo.get (i);
                for (int j=0; j < vecbd.size (); j++){
                    Ingreso_detalle item = (Ingreso_detalle) vecbd.get (j);
                    if( item.getItem () == item1.getItem () ){
                        sw =  true;
                        break;
                    }
                }
                if(!sw){
                    this.vecing.add (item1);
                }
                sw =  false;
            }
            
            if(!items.equals ("")){
                items = items.substring (0,items.length ()-1);
            }
        }catch(Exception e){
            e.printStackTrace ();
        }
    }
    
    /**
     * Metodo: actualizarIngresoMiscelaneo,
     * actualiza el registro items de ingreso miscelaneo
     * @autor : Ing. Diogenes Bastidas
     * @param :
     * @version : 1.0
     */
    public void actualizarIngresoMiscelaneo (String dstrct, String num_ingreso, String tipo_doc, Vector itemsMiscelaneo, String usuario) throws Exception {
        try{
            clasificarItemsMiscelaneos (num_ingreso, tipo_doc, dstrct, itemsMiscelaneo);
            String SQL_ACT = (vecmod.size ()>0)?ingresodetalle.actualizarIngresoMiscelaneo (vecmod) : "";
            String SQL_ING = (vecing.size ()>0)?ingresodetalle.insertarIngresoMiscelaneoDetalle (vecing):"";
            String SQL_ANU = (!items.equals ("")) ? ingresodetalle.anularIngresoMiscelaneo (dstrct, tipo_doc, num_ingreso, items,usuario) : "";
            String SQL_TODOS = SQL_ACT+" "+SQL_ING+" "+SQL_ANU ;
            TransaccionService tService = new TransaccionService (this.ingresodetalle.getDatabaseName());
            tService.crearStatement ();
            tService.getSt ().addBatch ( SQL_TODOS );
            tService.execute ();
        }catch(Exception e){
            e.printStackTrace ();
        }
        
    }
    
    /**
     * Metodo : escribirArchivoTemporal, funcion que guarda un objeto tipo Ingreso detalle en un Archivo
     * @autor : Diogenes Bastidas
     * @param : objeto ingreso, un vector de ingreso, nombre del archivo, el usuario
     * @version : 1.0
     */
    public void escribirArchivoTemporal (Ingreso ingreso, Vector items, String nombreArchivo, String user)throws IOException{
        ResourceBundle rb = ResourceBundle.getBundle ("com/tsp/util/connectionpool/db");
        String path = rb.getString ("ruta");
        String Ruta  = path + "/exportar/migracion/" + user;
        String RutaA = Ruta + "/"+nombreArchivo;
        File archivo = new File (Ruta);
        archivo.mkdirs ();
        File archivoR =  new File (RutaA);
        FileOutputStream  salidaArchivoR =   new FileOutputStream (archivoR);
        ObjectOutputStream salidaObjetoR =   new ObjectOutputStream (salidaArchivoR);
        salidaObjetoR.writeObject (ingreso);
        salidaObjetoR.writeObject (items);
        salidaObjetoR.close ();
    }
    
    
    public Ingreso leerArchivoTemporal (String user, String nro )throws IOException,ClassNotFoundException{
        Ingreso ingreso;
        ResourceBundle rb = ResourceBundle.getBundle ("com/tsp/util/connectionpool/db");
        String path = rb.getString ("ruta");
        String Ruta  = path + "/exportar/migracion/" + user + "/Ing_miscelaneo"+nro+".txt";
        File archivo = new File (Ruta);
        FileInputStream entradaArchivo = new FileInputStream (archivo);
        ObjectInputStream entradaObjeto= new ObjectInputStream (entradaArchivo);
        try{
            ingreso = (Ingreso) entradaObjeto.readObject ();
        }
        catch(ClassNotFoundException e){
            ingreso=null;
        }
        entradaObjeto.close ();
        return ingreso;
    }
    
    
    
    
    public Vector leerArchivoItemsTemporal (String user, String nro)throws IOException,ClassNotFoundException{
        Vector items;
        Ingreso ingreso;
        ResourceBundle rb = ResourceBundle.getBundle ("com/tsp/util/connectionpool/db");
        String path = rb.getString ("ruta");
        String Ruta  = path + "/exportar/migracion/" + user + "/Ing_miscelaneo"+nro+".txt";
        File archivo = new File (Ruta);
        FileInputStream entradaArchivo = new FileInputStream (archivo);
        ObjectInputStream entradaObjeto= new ObjectInputStream (entradaArchivo);
        try{
            ingreso = (Ingreso) entradaObjeto.readObject ();
            items = (Vector) entradaObjeto.readObject ();
        }
        catch(ClassNotFoundException e){
            items=null;
        }
        entradaObjeto.close ();
        return items;
    }
    
    /**
     * Metodo: BorrarArchivo, permite borrar el archivo temporal
     * @autor : Ing. Diogenes Bastidas Morales
     * @param : String Nombre del archivo, String Usuario
     * @version : 1.0
     */
    public boolean BorrarArchivo (String nombreArchivo,String user)throws IOException,ClassNotFoundException{
        ResourceBundle rb = ResourceBundle.getBundle ("com/tsp/util/connectionpool/db");
        String path = rb.getString ("ruta");
        String Ruta  = path + "/exportar/migracion/" + user + "/"+nombreArchivo;
        File archivo = new File (Ruta);
        return archivo.delete ();
    }
    
    /**
     * Metodo: numero_items, permite buscar el numero de items de un ingreso
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public int numero_items ( String distrito, String tipo_doc, String no_ingreso ) throws SQLException {
        try{
            return ingresodetalle.numero_items (distrito, tipo_doc, no_ingreso);
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
        /**
     * Metodo: updateIngresoDiferencia, permite actualizar un registro en la tabla ingreso_detalle en la BD.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String updateIngresoDiferencia (Ingreso_detalle ingreso) throws SQLException {
        try{
            ingresodetalle.setIngreso_detalle (ingreso);
            return ingresodetalle.updateIngresoDiferencia ();
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
    /**
     * Metodo: numero_items, permite buscar el numero de items de un ingreso
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public double valorDiferencia (Ingreso_detalle ingreso, double val) throws SQLException {
        try{
            ingresodetalle.setIngreso_detalle (ingreso);
            return ingresodetalle.valorDiferencia ( val );
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
         /* Metodo: updateSaldoFactura_modificacion, permite actualizar el saldo de la factura
      * @autor : Ing. Jose de la rosa
      * @param : valor del abono de la factura, valor del abono moneda extranjera de la factura, factura, distrito, el tipo de documento
      * @version : 1.0
      */
    public double esTotal ( double abono_meA, double abono_meV, String factura, String distrito, String tipo_documento) throws SQLException {
        try{
            return ingresodetalle.esTotal (abono_meA, abono_meV, factura, distrito, tipo_documento);
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
    
    /**
     * Metodo: datosListadoFactura, permite listar las facturas de un cliente
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void datosListadoFacturaUpdate ( String distrito, String nit_cliente, String tipo_documento, String num_ingreso ) throws SQLException {
        try{
            ingresodetalle.datosListadoFacturaUpdate (distrito, nit_cliente, tipo_documento, num_ingreso);
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
        /**
     * Metodo: numeroIngresoRI, devuelve el consecutivo del ingreso en RI
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String numeroIngresoRI ( String num_ingreso, String tipo, String distrito ) throws SQLException {
        try{
            return ingresodetalle.numeroIngresoRI (num_ingreso, tipo, distrito);
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
    /**
     * Metodo: numeroIngresoRI, devuelve el consecutivo del ingreso en RI
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String numeroSiguienteIngresoRI ( String num_ingreso, String tipo, String distrito ) throws SQLException {
        try{
            return ingresodetalle.numeroSiguienteIngresoRI (num_ingreso, tipo, distrito);
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }        
    }
    
     /**
     * Metodo: updateSaldoIngreso, permite actualizar el saldo del ingreso
     * @autor : Ing. Jose de la rosa
     * @param : valor viejo, nuevo valor. distrito, tipo de ingreso, numero de ingreso
     * @version : 1.0
     */
    public String updateSaldoIngreso ( double Val_v, String distrito, String tipo, String num_ing ) throws SQLException {
        try{
            return ingresodetalle.updateSaldoIngreso (Val_v, distrito, tipo, num_ing);
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }         
    }

     /**
     * Metodo: updateSaldoConsignacionIngreso, permite actualizar el saldo y la consignacion del ingreso
     * @autor : Ing. Jose de la rosa
     * @param : valor, distrito, tipo de ingreso, numero de ingreso
     * @version : 1.0
     */
    public String updateSaldoConsignacionIngreso ( String distrito, String tipo, String num_ing ) throws SQLException {
        try{
            return ingresodetalle.updateSaldoConsignacionIngreso ( distrito, tipo, num_ing);
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        } 
    }
    
    public String insertarNewIngresoDetalle (Ingreso_detalle ingreso) throws SQLException {
        try{
            ingresodetalle.setIngreso_detalle (ingreso);
            return ingresodetalle.insertarNewIngresoDetalle ();
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    
    /*public String updateSaldoFactura (double abono, double abono_me, String factura, String distrito, String tipo_documento) throws SQLException {
        try{
            return ingresodetalle.updateSaldoFactura (abono, abono_me, factura, distrito, tipo_documento);
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }*/
    
}
