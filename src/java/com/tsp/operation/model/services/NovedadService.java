/*
 * NovedadService.java
 *
 * Created on 13 de Julio de 2005, 09:00 AM
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  HENRY
 */
public class NovedadService {
    
    private NovedadDAO novedadDao; 
    
    /** Creates a new instance of NovedadService */
    public NovedadService() {
        novedadDao = new NovedadDAO(); 
    }
     
   public void insertarNovedad(Novedad novedad)throws SQLException {
       try{
           novedadDao.setNovedad(novedad);
           novedadDao.insertarNovedad();
       }
       catch(SQLException e){
           throw new SQLException(e.getMessage());
       }
   }
   public boolean existeNovedad(String cod) throws SQLException {
       return novedadDao.existeNovedad(cod); 
   }         
   /*public boolean existNovedadNom(String nom) throws SQLException {
       return novedadDao.existNovedadNom(nom); 
   } */  
   public List obtenerNovedades( ) throws SQLException {
       List novedades = null;
       novedades = novedadDao.obtenerNovedades();
       return novedades;
   }   
   public void listarNovedades () throws SQLException {
       try{
           novedadDao.listarNovedades();
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
   }
   public void novedades () throws SQLException {
       try{
           novedadDao.novedades();
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
   }
   public Novedad obtenerNovedad()throws SQLException{
       return novedadDao.obtenerNovedad();
   }   
   public Vector obtNovedades()throws SQLException{
       return novedadDao.obtNovedades();
   }   
   public void buscarNovedad(String cod) throws SQLException {
       try{
           novedadDao.buscarNovedad(cod);     
       }
       catch(SQLException e){
           throw new SQLException ( e.getMessage()); 
       }       
   }
   public void buscarNovedadNombre(String nom) throws SQLException {
       try{
           novedadDao.buscarNovedadNombre(nom);     
       }
       catch(SQLException e){
           throw new SQLException ( e.getMessage()); 
       }       
   }
   public void modificarNovedad(Novedad novedad)throws SQLException {
       try{
           novedadDao.setNovedad(novedad);
           novedadDao.modificarNovedad();
       }
       catch(SQLException e){
           throw new SQLException(e.getMessage());
       }
   }   
   public void eliminarNovedad(Novedad novedad)throws SQLException {
       try{
           novedadDao.setNovedad(novedad);
           novedadDao.eliminarNovedad();
       }
       catch(SQLException e){
           throw new SQLException(e.getMessage());
       }
   }
}
