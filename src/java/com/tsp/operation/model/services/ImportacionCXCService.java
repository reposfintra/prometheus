/**
 * Nombre        ImportacionCXPService.java
 * Descripci�n
 * Autor         Ivan Dario Gomez
 * Fecha         16 de febrero de 2006, 10:45 AM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.ImportacionCXCDAO;
import com.tsp.operation.model.beans.*;
import java.util.*;

public class ImportacionCXCService {
    
    ImportacionCXCDAO dao;
    
    
    /** Crea una nueva instancia de  ImportacionCXPService */
    public ImportacionCXCService() {
        dao = new ImportacionCXCDAO();
    }
    public ImportacionCXCService(String dataBaseName) {
        dao = new ImportacionCXCDAO(dataBaseName);
    }
    
    public List searchFacturaPendiente(String usuario) throws Exception {
        try{
            return dao.searchFacturaPendiente(usuario);
        }catch (Exception ex){
            throw new Exception("Error en searchFacturaPendiente [ImportacionCXCService] ....\n" + ex.getMessage());
        }
    }
    
    
    public Cliente searchDatosCliente(String codcli) throws Exception {
        return dao.searchDatosCliente(codcli);
    }
    
     public boolean exiteRemesa(String numrem,String codPagador) throws Exception {
         return dao.exiteRemesa(numrem,codPagador);
     }
    
}
