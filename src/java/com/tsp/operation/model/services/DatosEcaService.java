/*
 * DatosEcaService.java
 * Created on 20 de mayo de 2009, 9:07
 */
package com.tsp.operation.model.services;
import com.tsp.operation.model.DAOS.DatosEcaDAO;
import java.sql.*;
import java.util.List;
import java.util.Vector;
import com.tsp.operation.model.beans.DatosEca;
/**
 * @author  Fintra
 */
public class DatosEcaService {
    private DatosEcaDAO datosEcaDao;
    /** Creates a new instance of DatosEcaService */
    public DatosEcaService() {
        datosEcaDao = new DatosEcaDAO();
    }
    public DatosEcaService(String dataBaseName) {
        datosEcaDao = new DatosEcaDAO(dataBaseName);
    }

    public void obtainRegistrosArchivo()throws SQLException{
        datosEcaDao.obtainRegistrosArchivo();
    }

    public List getRegistrosArchivo() {
        return datosEcaDao.getRegistrosArchivo();
    }

    public List obtainFacturasEca(DatosEca datosEca) throws SQLException{
        return datosEcaDao.obtainFacturasEca(datosEca);
    }
    public Vector cruzarFacIng(DatosEca de,  List facturasEca,String num_ingreso  ) throws SQLException{
        return datosEcaDao.cruzarFacIng(de ,facturasEca,num_ingreso);
    }

    public Vector cruzarFacIng(DatosEca de,  List facturasEca,String num_ingreso,int SwUltimaCxcEs0,int i  ) throws SQLException{
        return datosEcaDao.cruzarFacIng(de ,facturasEca,num_ingreso,SwUltimaCxcEs0,i);
    }

    public Vector cruzarFacIng2(DatosEca de,  List facturasEca,String num_ingreso,int SwUltimaCxcEs0,int i  ) throws SQLException{
        return datosEcaDao.cruzarFacIng2(de ,facturasEca,num_ingreso,SwUltimaCxcEs0,i);
    }

    public void obtainRegistrosArchivo2()throws SQLException{
        datosEcaDao.obtainRegistrosArchivo2();
    }

    public List obtainFacturasEca2(DatosEca datosEca) throws SQLException{
        return datosEcaDao.obtainFacturasEca2(datosEca);
    }
    public Vector cruzarFacIngPms2(DatosEca de,  List facturasEca,String num_ingreso,int SwUltimaCxcEs0,int i  ) throws SQLException{
        return datosEcaDao.cruzarFacIngPms2(de ,facturasEca,num_ingreso,SwUltimaCxcEs0,i);
    }

    //inicio de 20100602
    public void obtainRegistrosArchivo3()throws SQLException{
        datosEcaDao.obtainRegistrosArchivo3();
    }
    public List getRegistrosArchivo3() {
        return datosEcaDao.getRegistrosArchivo3();
    }
    public List obtainFacturasEca3(DatosEca datosEca) throws SQLException{
        return datosEcaDao.obtainFacturasEca3(datosEca);
    }
    public Vector cruzarFacIngPms3(DatosEca de,  List facturasEca,String num_ingreso,int SwUltimaCxcEs0,int i ,String usr ) throws SQLException{
        return datosEcaDao.cruzarFacIngPms3(de ,facturasEca,num_ingreso,SwUltimaCxcEs0,i,usr);
    }
    public String generarRe() throws SQLException{
        return datosEcaDao.generarRe();
    }
    //fin de 20100602

}