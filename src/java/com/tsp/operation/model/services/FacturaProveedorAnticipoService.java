/***********************************************************************************
 * Nombre clase : ............... FacturaProveedorAnticipoService.java             *
 * Descripcion :................. Clase que maneja el Service que contiene los     *
 *                                metodos que interactuan entre el DAO y el Action *
 *                                para generar                                     *
 * Autor :.......................                                                  *
 * Fecha :........................                                                 *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/
package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.Proveedor;
import com.tsp.operation.model.Model;

/**
 * 
 * @author  Enrique
 */
public class FacturaProveedorAnticipoService {
    /*
     *Declaracion de atributos
     */
    private FacturaProveedorAnticipoDAO  RTDataAccess;
    private FacturaProveedorAnticipoDAO  RTDataAccess2;
    private List                         Items; 

    /** Creates a new instance of RegistroTarjetaService */
    public FacturaProveedorAnticipoService() {
        RTDataAccess    = new FacturaProveedorAnticipoDAO();
        RTDataAccess2    = new FacturaProveedorAnticipoDAO();
        Items           = new LinkedList();      
    }
    public FacturaProveedorAnticipoService(String dataBaseName) {
        RTDataAccess    = new FacturaProveedorAnticipoDAO(dataBaseName);
        RTDataAccess2    = new FacturaProveedorAnticipoDAO(dataBaseName);
        Items           = new LinkedList();      
    }
      
 
  /**
     * Metodo List_Items, lista los registros las tablas de acuerdo a los parametros de busqueda
     * @autor : Ing. Enrique De Lavalle
     * @param : String fechainicial
     * @param : String fechafinal
     * @param : String distrito
     * @see   : ReporteOCAnticipoDAO
     * @version : 1.0
     */
    public void List_Items(String distrito, String proveedor,String fechainicial, String fechafinal) throws Exception {
        
        try { 
        
            ReiniciarItems();
            Items = RTDataAccess.List_Items(distrito, proveedor, fechainicial, fechafinal);
            
        
        }catch(Exception ex){
            ex.printStackTrace();
        }
        
    }
    
    public void generarFact(String distrito, String proveedor, String documento, String descrip, String agencia, String hc, String idmims, String banco, String sucursal, String moneda, double vlr_neto, double vlr_total_abonos, double vlr_saldo, double vlr_neto_me, double vlr_total_abonos_me, double vlr_saldo_me, double tasa, String userUp, String creationUser, String base, String moneda_banco, String fechaVenc,  int lastNum, Vector vec, String conceptCode, String aprob ) throws Exception {
        
        try {
            RTDataAccess2.insertarFacturaCab(distrito,proveedor,documento,descrip,agencia,hc,idmims,banco,sucursal,moneda,vlr_neto,vlr_total_abonos,vlr_saldo,vlr_neto_me,vlr_total_abonos_me,vlr_saldo_me,tasa,userUp,creationUser,base,moneda_banco,fechaVenc,lastNum, vec,conceptCode,aprob);
        }
        catch(Exception e) { e.printStackTrace();
            throw new SQLException("Error en la rutina insertarFactura [FacturaProveedorAnticipoService]...\n"+e.getMessage());
        }
       
    }
    
     /* Metodo ReiniciarItems, 
     * @autor : Ing. Enrique De Lavalle
     * @version : 1.0 */
     public void ReiniciarItems(){
        this.Items = new LinkedList();
    }    
  
    /* Metodo getItems, 
     * @autor : Ing. Enrique De Lavalle
     * @version : 1.0*/
    public List getItems(){
        return this.Items;
    }

    
}
