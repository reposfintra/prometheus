/*********************************************************************
 * Nombre      ............... PerfilesGenService.java
 * Autor       ............... lfrieri
 * Copyright   ............... Fintravalores S.A. S.A
 *********************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.PerfilesGenDAO;
import java.util.*;

public class PerfilesGenService {
    
    // Declaracion de elementos de acceso a la base de datos
    PerfilesGenDAO PerfilesGenDa;
    
    
    // Declaracion de elementos de Retorno
    List ListaPerfiles;
    List ListaTablas;
    List ListaTP;
    
   
    public PerfilesGenService() {
        PerfilesGenDa = new PerfilesGenDAO();
    }
      
    
    public void buscarPerfiles()throws Exception{
        this.ListaPerfiles = PerfilesGenDa.listarPerfiles();
    }
    
    public void buscarTP()throws Exception{
        this.ListaTP = PerfilesGenDa.listarTP();
    }
    
    public List getListaTP(){
        
        return ListaTP;
    }
    
    public List getListaPerfiles(){
        
        return ListaPerfiles;
    }
    
    public void buscarTablas()throws Exception{
        this.ListaTablas = PerfilesGenDa.listarTablas();
    }
    
    public List getListaTablas(){
        
        return ListaTablas;
    }
    
    public void addRelacionPerfilesGen(String [] Perfiles, String [] Tablas) throws Exception{
            PerfilesGenDa.addRelacionPerfilesGen(Perfiles, Tablas);
    }
    
    public void eliminarRelaciones(String [] Perfiles) {
        try {
            PerfilesGenDa.EliminarRelaciones(Perfiles);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
