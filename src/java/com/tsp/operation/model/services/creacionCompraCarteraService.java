/*
 * creacionCompraCarteraService.java
 *
 * Created on 1 de enero de 2008, 05:58 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.tsp.operation.model.services;
import java.util.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;
import java.sql.*;
import com.tsp.operation.model.Model;

/**
 *
 * @author NAVI
 */
public class creacionCompraCarteraService {
    chequeCartera cheque;
    ArrayList cheques;
    creacionCompraCarteraDAO creacionCompraCartera;
    String archivopdf;
    boolean SwImprimir;
    
    /** Creates a new instance of creacionCompraCarteraService */
    public creacionCompraCarteraService() {
        archivopdf="";
        creacionCompraCartera = new creacionCompraCarteraDAO();
        cheques=new ArrayList();        
    }
    public creacionCompraCarteraService(String dataBaseName) {
        archivopdf="";
        creacionCompraCartera = new creacionCompraCarteraDAO(dataBaseName);
        cheques=new ArrayList();        
    }
    
    public ArrayList getCheques(){
       return cheques;
    }
    public void setCheques(ArrayList cheques1){
       cheques=cheques1;
    }
    
    public String agregarOcambiarCheque (String item,String fechacheque,String fechaconsignacion,String porte,String valor,String nit,String proveedor,String fechainicio,String modalidadRemesa1) throws Exception{
        String respuesta="ok";
        try{
            int i=0;
            boolean sw=false;
            String dias,factor, descuento, valorsincustodia, custodia, remesa, valorgirable,valorgirablesinremesaniporte;
            String tasanominal,tasaefectiva;
            String codigo_negocio="nadita";
            String nombre_nit,nombre_proveedor;
            chequeCartera chc=new chequeCartera();
            
            String porcentaje_remesa;
            ArrayList tem=creacionCompraCartera.obtenerDatosChequeCartera(fechaconsignacion,fechainicio,proveedor,valor,porte,nit,modalidadRemesa1);
            if (tem!=null && tem.size()>0){
                dias=(String)tem.get(0);
                factor=(String)tem.get(1);  
                valorsincustodia=(String)tem.get(2);
                descuento=(String)tem.get(3);
                custodia=(String)tem.get(4);
                remesa=(String)tem.get(5);
                if (modalidadRemesa1.equals("2")){
                    remesa="0";
                }
                valorgirable=(String)tem.get(6);
                valorgirablesinremesaniporte=(String) tem.get(7);
                tasanominal=(String) tem.get(8);
                tasaefectiva=(String) tem.get(9);
                porcentaje_remesa=(String) tem.get(10);
                nombre_proveedor=(String) tem.get(11);
                nombre_nit=(String)tem.get(12);
                //System.out.println("nombrenit"+nombre_nit);
                //System.out.println("valorgirablesinremesaniporte en service="+valorgirablesinremesaniporte);
                chc=new chequeCartera(item,fechacheque,fechaconsignacion,porte,valor,nit,proveedor,fechainicio,dias, factor, descuento, valorsincustodia, custodia, remesa, valorgirable,valorgirablesinremesaniporte,tasanominal,tasaefectiva,codigo_negocio,porcentaje_remesa,nombre_proveedor,nombre_nit,modalidadRemesa1,"","","");
            }else{
                respuesta="no se pudo crear cheque";
                sw=true;
            }
            while (i<cheques.size() && sw==false ){
                if (((chequeCartera)cheques.get(i)).getItem().equals(item)){
                    /*((chequeCartera)cheques.get(i)).setFechaCheque(fechacheque);
                    ((chequeCartera)cheques.get(i)).setFechaConsignacion(fechaconsignacion);
                    ((chequeCartera)cheques.get(i)).setPorte(porte);
                    ((chequeCartera)cheques.get(i)).setValor(valor);
                    */ 
                    cheques.set(i,chc);
                    sw=true;
                }
                i=i+1;
            }
            if (sw==false ){
                cheque=chc;
                cheques.add(cheque);
            }
            
        }catch(Exception e){
            System.out.println("error al obtener datos de che" +e.toString()+"...."+e.getMessage());
            throw new Exception("ERROR DURANTE service .." + e.getMessage());            
        }
        return respuesta;
    }
    
    public String getNextItem(){
        int i=0;
        String itemcillo="0";
        while (i<getCheques().size()){
            if (Integer.parseInt(((chequeCartera)getCheques().get(i)).getItem())>Integer.parseInt(itemcillo)){
                itemcillo=((chequeCartera)getCheques().get(i)).getItem();
            }
            i=i+1;
        }
        itemcillo=""+(Integer.parseInt(itemcillo)+1);
        return itemcillo;
    }
    
    public void cancelarCompraCartera(){
        cheques=null;
        cheques=new ArrayList();
    }
    
    public String aceptarConjuntoDeCheques(ArrayList cheques,String fechadesembolso,Usuario usuario,String total_valorgirable,String modalidadcustodia,String modalidadremesa,String total_valor_cheque,String tpr,String tdes,String tipodocx ) throws SQLException {
        String respuesta="";
        try{
            //System.out.println("aceptarConjuntoDeCheques en service");
            respuesta=creacionCompraCartera.aceptarConjuntoDeCheques(cheques,fechadesembolso,usuario,total_valorgirable,modalidadcustodia,modalidadremesa,total_valor_cheque,tpr,tdes,tipodocx);
            
        }catch(Exception ex){
            System.out.println("ERROR AL INSERTAR NEGOCIO CON CHEQUES");
            throw new SQLException ("ERROR AL INSERTAR NEGOCIO CON CHEQUES,  "+ex.getMessage());
        }
        return respuesta;
    }
    
    public ArrayList obtenerCheques(String codigo_negocio) throws SQLException{
        ArrayList respuesta=new ArrayList();
        try{
            respuesta =creacionCompraCartera.obtenerCheques(codigo_negocio); 
        }catch(Exception ex){
            System.out.println("ERROR AL obtener cheques de negocio");
            throw new SQLException ("ERROR AL obtener cheques de negocio,  "+ex.getMessage());
        }
        return respuesta;
    }   
    
    public String obtenerPorte() throws SQLException{
        String respuesta=creacionCompraCartera.obtenerPorte(); 
        return respuesta;
    }
    
    public void copiarCheque(int fila,chequeCartera primercheque1) throws SQLException{
        try{
        chequeCartera primercheque=new chequeCartera();
        primercheque.setFechaCheque(primercheque1.getFechaCheque());
        primercheque.setCodeNegocio(primercheque1.getCodigoNegocio());
        primercheque.setPorcentajeRemesa(primercheque1.getPorcentajeRemesa());
        primercheque.setItem(""+(fila+1));
        primercheque.setFechaConsignacion(primercheque1.getFechaConsignacion());
        primercheque.setPorte(primercheque1.getPorte());
        primercheque.setValor(primercheque1.getValor());
        primercheque.setNit(primercheque1.getNit());
        primercheque.setProveedor(primercheque1.getProveedor());
        primercheque.setFechaInicio(primercheque1.getFechaDesembolso());
        primercheque.setDias(primercheque1.getDias());
        primercheque.setFactor(primercheque1.getFactor());
        primercheque.setDescuento(primercheque1.getDescuento());
        primercheque.setValorSinCustodia(primercheque1.getValorSinCustodia());
        primercheque.setCustodia(primercheque1.getCustodia());
        primercheque.setRemesa(primercheque1.getRemesa());
	primercheque.setValorGirable(primercheque1.getValorGirable());
        primercheque.setValorGirableSinRemesaNiPorte(primercheque1.getValorGirableSinRemesaNiPorte());
        primercheque.setTasaEfectiva(primercheque1.getTasaEfectiva());
        primercheque.setTasaNominal(primercheque1.getTasaNominal());
        primercheque.setNombreNit(primercheque1.getNombreNit());
        primercheque.setNombreProveedor(primercheque1.getNombreProveedor());
        primercheque.setModalidadCustodia(primercheque1.getModCustodia());
        primercheque.setModalidadRemesa(primercheque1.getModRemesa());
        
        //primercheque=(chequeCartera)cheques.get(0);
        //System.out.println("fecha00"+primercheque.getFechaCheque());
        //FenalcoFintraService fenalcoFintraService=new FenalcoFintraService();
        //System.out.println("antes de dia");
        //public static java.util.Calendar TraerFecha(int fila, int dia, int mes, int ano, String dias_primer_cheque){
        int dia=Integer.parseInt(primercheque.getFechaCheque().substring(8,10));
        //System.out.println("dia"+dia);
        int mes=Integer.parseInt(primercheque.getFechaCheque().substring(5,7));
        int ano=Integer.parseInt(primercheque.getFechaCheque().substring(0,4));
        
        java.util.Date fechacheque=((Calendar)Util.TraerFecha(fila,dia,mes,ano,"30")).getTime();
        
        String fechitacheque=Util.dateFormat(fechacheque);
        //System.out.println("fechadcheque"+Util.dateFormat(fechacheque)+"dateTimeFormat"+Util.dateTimeFormat(fechacheque));
        //System.out.println("fechitacheque"+fechitacheque);
        primercheque.setFechaCheque(fechitacheque);
        
        dia=Integer.parseInt(fechitacheque.substring(8,10));
        mes=Integer.parseInt(fechitacheque.substring(5,7));
        ano=Integer.parseInt(fechitacheque.substring(0,4));
        
        String fechaconsignacion=obtenerFechaConsignacion(ano, mes,dia);
        //System.out.println("fechaconsignacion"+fechaconsignacion);
        primercheque.setFechaConsignacion(fechaconsignacion);
        //java.util.Date fc=((Calendar)fenalcoFintraService.buscarFestivo(fechitacheque)).getTime();
        ArrayList tem=null;
        try{
            tem=creacionCompraCartera.obtenerDatosChequeCartera(fechaconsignacion,primercheque.getFechaDesembolso(),primercheque.getProveedor(),primercheque.getValor(),primercheque.getPorte(),primercheque.getNit(),primercheque.getModRemesa());
        }catch(Exception e){
            System.out.println("x en copiar cheque en obtenerDatosChequeCartera"+e.toString());
        }
        String dias=(String)tem.get(0);                        
        String factor=(String)tem.get(1);  
        String valorsincustodia=(String)tem.get(2);
        String descuento=(String)tem.get(3);
        String remesa=(String)tem.get(5);
        String modalidadRemesa1=primercheque.getModRemesa();
        if (modalidadRemesa1.equals("2")){
            remesa="0";
        }
        String valorgirable=(String)tem.get(6);
        String valorgirablesinremesaniporte=(String) tem.get(7);
        String tasanominal=(String) tem.get(8);
        String tasaefectiva=(String) tem.get(9);
        String porcentaje_remesa=(String) tem.get(10);
        
        primercheque.setDias(dias);
        primercheque.setFactor(factor);
        primercheque.setValorSinCustodia(valorsincustodia);
        primercheque.setDescuento(descuento);
        primercheque.setRemesa(remesa);
        primercheque.setValorGirable(valorgirable);
        primercheque.setValorGirableSinRemesaNiPorte(valorgirablesinremesaniporte);
        primercheque.setTasaNominal(tasanominal);
        primercheque.setTasaEfectiva(tasaefectiva);
        primercheque.setPorcentajeRemesa(porcentaje_remesa);
        
        cheques.add(primercheque);
        
        }catch(Exception ex){
            System.out.println("ERROR en servicexxx");
            throw new SQLException ("ERROR en service ,  "+ex.getMessage());
        }
        
    }

    public String obtenerFechaConsignacion(int ano_s, int mes_s,int dia_s) throws SQLException{
        String respuesta="";
        String cadenadia="";
        String cadenames=""; 
        try{
            int j=0;
            Calendar Fecha_Sugerida_2=Util.TraerFecha(1, 1, 1, 2001, "30");
            FenalcoFintraService fenalcoFintraService=new FenalcoFintraService();
            while (fenalcoFintraService.buscarFestivo(ano_s+"-"+mes_s+"-"+dia_s)){
                    mes_s=mes_s-1;
                    Fecha_Sugerida_2.set(ano_s, mes_s, dia_s);
                    Fecha_Sugerida_2.add(Calendar.DAY_OF_YEAR,1);
                    //System.out.println("Fecha Antes de Festivo= "+Util.ObtenerFechaCompleta(Fecha_Sugerida_2));
                    //Fecha_Sugerida_2.set(ano_s, mes_s, dia_s);
                    dia_s = Fecha_Sugerida_2.get(Calendar.DATE);
                    mes_s = Fecha_Sugerida_2.get(Calendar.MONTH);
                    ano_s = Fecha_Sugerida_2.get(Calendar.YEAR);
                    mes_s=mes_s+1;
                    j=1;

            }
            if (mes_s<10){
                cadenames="0"+mes_s;
            }else{
                cadenames=""+mes_s;
            }
            if (dia_s<10){
                cadenadia="0"+dia_s;
            }else{
                cadenadia=""+dia_s;
            }
            respuesta=""+ano_s+"-"+cadenames+"-"+cadenadia;
        }catch(Exception e){
            System.out.println("x"+e.toString());
            throw new SQLException ("ERROR en service_ ,  "+e.getMessage());
        }
        return respuesta;
    }
    
    public chequeCartera getPrimerCheque(){
        return (chequeCartera)cheques.get(0);
    }
    
    public String obtenerFechaConsignacion(String fechaCheque) throws SQLException{
        int dia=Integer.parseInt(fechaCheque.substring(8,10));
        int mes=Integer.parseInt(fechaCheque.substring(5,7));
        int ano=Integer.parseInt(fechaCheque.substring(0,4));
        return obtenerFechaConsignacion( ano,  mes, dia); 
    }
    
    public void imprimirDetalleCompraCartera(Model model1,ArrayList detalle) throws Exception{
        //System.out.println("imprimirDetalle(detalle)"+detalle+detalle.size());
        //PdfImprimirService pis =new PdfImprimirService();
        model1.PdfImprimirSvc.imprimirDetalleCompraCartera(detalle);
        
        archivopdf="/pdf/rotu"+model1.PdfImprimirSvc.getN()+".pdf";
        
        //this.dispatchRequest(next);
        
        //pis=null;
    }
    
    public String getArchivoPdf(){
        return archivopdf;
    }
    
    public void setSwImprimir(boolean x){
        SwImprimir=x;
    }
    
    public boolean getSwImprimir(){
        return SwImprimir;
    }
}