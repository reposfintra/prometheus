/******************************************************************
* Nombre                        ReporteDevolucionService.java
* Descripci�n                   Clase Service para el reporte devolucion
* Autor                         ricardo rosero
* Fecha                         20/01/2006
* Versi�n                       1.0
* Coyright                      Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;

/**
 *
 * @author  rrosero
 */
public class ReporteDevolucionService {
    //atributos
    private ReporteDevolucionDAO rdd;
    
    /** Creates a new instance of ReporteDevolucionService */
    public ReporteDevolucionService() {
        rdd = new ReporteDevolucionDAO();
    }
    
    /**
    * obtenerInforme
    * @autor            Ing. Ricardo Rosero
    * @throws           SQLException
    * @see              obtenerInforme EstCargaPlacaDAO
    * @version          1.0
    */   
    public Vector obtenerInforme(String fechai, String fechaf) throws SQLException{
        return rdd.obtenerInforme(fechai, fechaf);
    }
    
    /**
    * obtenerRegistros
    * @autor            Ing. Ricardo Rosero
    * @throws           SQLException
    * @see              obtenerRegistros EstCargaPlacaDAO
    * @version          1.0
    */   
    public int obtenerRegistros(String fechai, String fechaf) throws SQLException{
        return rdd.obtenerRegistros(fechai, fechaf);
    }
    
    
    /**
    * setVector
    * @autor            Ing. Ricardo Rosero
    * @param            Vector vec
    * @see              setEcpVec EstCargaPlacaDAO
    * @version          1.0
    */   
    public void setVector(Vector vec){
        rdd.setVecReporteDev(vec);
    }
    
    /**
    * getVector
    * @autor            Ing. Ricardo Rosero
    * @see              getEcpVec EstCargaPlacaDAO
    * @version          1.0
    */   
    public Vector getVector(){
        return rdd.getVecReporteDev();
    }
    
    /**
     * Getter for property rdd.
     * @return Value of property rdd.
     */
    public com.tsp.operation.model.DAOS.ReporteDevolucionDAO getRdd() {
        return rdd;
    }
    
    /**
     * Setter for property rdd.
     * @param rdd New value of property rdd.
     */
    public void setRdd(com.tsp.operation.model.DAOS.ReporteDevolucionDAO rdd) {
        this.rdd = rdd;
    }
    
}
