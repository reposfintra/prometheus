/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.AsesoresDAO;
import com.tsp.operation.model.beans.Asesores;
import com.tsp.operation.model.beans.BeanGeneral;
import java.util.ArrayList;

/**
 *
 * @author ivargas
 */
public class AsesoresService {

    AsesoresDAO dao;
    
    public AsesoresService() {
        dao=new AsesoresDAO();
    }
    public AsesoresService(String dataBaseName) {
        dao=new AsesoresDAO(dataBaseName);
    }

    /**
     * Busca listado de asesores activos
     * @return listado de asesores
     * @throws Exception cuando hay error
     */
    public ArrayList<Asesores> buscarAsesoresActivos(String convenio) throws Exception{
        return dao.buscarAsesoresActivos(convenio);
    }

    public  ArrayList<Asesores> cargarAsesoresConvenio (String convenio)throws Exception{
        return dao.cargarAsesoresConvenio(convenio);
    }
     /**
     * Busca listado de asesores
     * @return listado de asesores
     * @throws Exception cuando hay error
     */
    public ArrayList<Asesores> buscarAsesores() throws Exception{
        return dao.buscarAsesores();
    }

     /**
     * Inserta y actualiza listado de asesores
     * @throws Exception cuando hay error
     */
    public void grabarAsesores(ArrayList<Asesores> listamod, ArrayList<Asesores> listains) throws Exception {
      dao.grabarAsesores(listamod, listains);
    }

       /**
     * M�todo que trae los negocios de un asesor
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public ArrayList<BeanGeneral> negociosAsesor(String asesor) throws Exception {
        return dao.negociosAsesor(asesor);
    }

     /**
     * M�todo que trae el query para la actualizacion del asesor al formulario
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public String updateAsesorFormulario(String formulario,String asesor) throws Exception {
        return dao.updateAsesorFormulario(formulario, asesor);
    }
    
    public ArrayList<Asesores> cargarAsesores() throws Exception{
        return dao.cargarAsesores();
    }

}
