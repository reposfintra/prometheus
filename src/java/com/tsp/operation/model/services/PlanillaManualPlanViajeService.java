/*********************************************************************************
 * Nombre clase :      PlanillaManualPlanViajeService.java                       *
 * Descripcion :       Service del PlanillaManualPlanViajeService.java           *
 * Autor :             LREALES                                                   *
 * Fecha :             10 de agosto de 2006, 10:55 PM                            *
 * Version :           1.0                                                       *
 * Copyright :         Fintravalores S.A.                                   *
 *********************************************************************************/
package com.tsp.operation.model.services;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;

public class PlanillaManualPlanViajeService {
    
    private PlanillaManualPlanViajeDAO dao;
    
    /** Creates a new instance of PlanillaManualPlanViajeService */
    public PlanillaManualPlanViajeService () {
        
        dao = new PlanillaManualPlanViajeDAO ();
        
    }
    
    /** Funcion publica que obtiene el metodo getVectorPlanilla del DAO */
    public Vector getVectorPlanilla () throws SQLException {
        
        return dao.getPlanilla();
        
    }
    
    /** Funcion publica que obtiene el metodo buscarInformacion del DAO */
    public void buscarInformacion ( String numpla ) throws SQLException {
        
        dao.consulta ( numpla );
        
    }
    
}