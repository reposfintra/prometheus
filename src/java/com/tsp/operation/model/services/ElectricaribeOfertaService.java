package com.tsp.operation.model.services;

import com.lowagie.text.*;
import com.lowagie.text.pdf.*;
import com.tsp.operation.model.DAOS.ElectricaribeOfertaDAO;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;

import java.awt.Color;
import java.util.*;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase para tratar todo lo referente al PDF de la oferta y
 * la modificacion de la oferta.
 *
 * @author Pablo Emilio Bassil Orozco
 */
public class ElectricaribeOfertaService {

    BaseFont bf;

    private Font mTitulo    = new Font(bf, 14, com.lowagie.text.Font.BOLD);
    private Font mSubtitulo = new Font(bf, 14, com.lowagie.text.Font.BOLDITALIC);
    private Font mNormal    = new Font(bf, 12);

    //Estos son los tipos de letras para parrafos normales
    private Font titulo     = new Font(bf, 10, com.lowagie.text.Font.BOLD);
    private Font subtitulo  = new Font(bf, 10, com.lowagie.text.Font.BOLDITALIC);
    private Font normal     = new Font(bf, 9);

    //Letras para tablas
    private Font tTitulo    = new Font(bf, 10, com.lowagie.text.Font.BOLD);
    private Font tSubtitulo = new Font(bf, 10, com.lowagie.text.Font.BOLDITALIC);
    private Font tNormal    = new Font(bf, 9);

    //Letras para la parte de los anexos
    private Font cTitulo    = new Font(bf, 8, com.lowagie.text.Font.BOLD);
    private Font cSubtitulo = new Font(bf, 8, com.lowagie.text.Font.BOLDITALIC);
    private Font cNormal    = new Font(bf, 8);

    private double subtMateriales       = 0;
    private double subtManoDeObra       = 0;
    private double subtOtros            = 0;

    private double totalOffer = 0;

    private Document        document;
    private ResourceBundle  rb;
    private String          ruta;
    private String          next;
    private String[]        meses = {"","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};

    private DateFormat offerFormat  = new SimpleDateFormat("yyyy-MM-dd");
    private DateFormat emergFormat  = new SimpleDateFormat("yyyyMMdd");
    private DateFormat df           = new SimpleDateFormat("yyyyMM");
    private Date dat                = new Date();

    private PdfWriter               writer;
    private ElectricaribeOfertaDAO  epd;
    private MyPageEvent             mpe;

    private ArrayList<AccionesEca> aecas;

    /**
     * Constructor de la clase ElectricaribeOfertaService, el cual
     * inicia las letras y la fecha para la creacion del PDF
     *
     * @throws Exception
     */
    public ElectricaribeOfertaService() throws Exception {
        bf = BaseFont.createFont( BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED );
        epd = new ElectricaribeOfertaDAO();
    }
    public ElectricaribeOfertaService(String dataBaseName) throws Exception {
        bf = BaseFont.createFont( BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED );
        epd = new ElectricaribeOfertaDAO(dataBaseName);
    }

    /**
     * Este metodo inicia la ruta donde se va a guardar el archivo y
     * es el primer paso para la creacion del PDF, ya que inicia el objeto
     * para la creacion del mismo.
     *
     * @param num Numero de la oferta
     * @param user Usuario logueado
     * @throws Exception
     */
    public void iniciar(String num, String user) throws Exception {

        try {
            epd.setNumOferta(num);
            epd.getEcaInfoForPDF();
            aecas = epd.getAcciones();

            rb          = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            this.ruta   = rb.getString("ruta") + "/exportar/migracion/"+user+"/"+ epd.getOferta().getConsecutivo() + " - " + emergFormat.format(dat).substring(0, 8) + ".pdf";

            File dir = new File(rb.getString("ruta") + "/exportar/migracion/"+user);
            dir.mkdir();

            document    = new Document(PageSize.LETTER, 110, 70, 140, 70);
            writer = PdfWriter.getInstance(document, new FileOutputStream(this.ruta));

            if (epd.getOferta().getTipo_cliente().equals("R")){
             //   mpe = new MyPageEvent(writer, document, rb.getString("ruta"), 0);
            }
            else{
               //// mpe = new MyPageEvent(writer, document, rb.getString("ruta"), 1);
            }

            //writer.setPageEvent(mpe);

            subtMateriales      = 0;
            subtManoDeObra      = 0;
            subtOtros           = 0;

            totalOffer          = 0;
        }
        catch (Exception ex) {
            System.out.println("error pdf:"+ex.toString());
                ex.printStackTrace();
            next="Error creando la ruta del archivo";
        }
    }

    /**
     * Este metodo sirve para devolver el precio venta de
     * la solicitud
     *
     * @param id_sol Numero de la solicitud
     * @return precio venta de la solicitud
     */
    public String getOfferValue(String id_sol) throws Exception{
        return epd.getOfferValue(id_sol);
    }

    /**
     * Este m�todo me devuelve el DAO de la oferta de electricaribe
     * con todos los datos.
     *
     * @return ElectricaribeOfertaDAO
     */
    public ElectricaribeOfertaDAO getOfferDao(){
        return epd;
    }

    /**
     * Este metodo es el segundo paso para la creacion del PDF.
     * Ac� es donde todo se agrega a la hoja del PDF.
     */
    public void doOfferPDF() throws Exception{
        try {
            document.open();

            document.add(new Paragraph(" "));

            document.add(new Paragraph(epd.getOferta().getOferta().toUpperCase(), mSubtitulo));
            document.add(new Paragraph(" "));
            document.add(new Paragraph("1.     Objeto", mNormal));
            document.add(new Paragraph("2.     Alcance y descripcion tecnica", mNormal));
            document.add(new Paragraph("    2.1     Alcance de la oferta", mNormal));
            document.add(new Paragraph("    2.2     Descripcion de los trabajos", mNormal));
            document.add(new Paragraph("3.     Organizacion y medios", mNormal));
            document.add(new Paragraph("4.     Oferta econ�mica", mNormal));
            document.add(new Paragraph("5.     Validez de la oferta", mNormal));
            document.add(new Paragraph("6.     Forma y programa de pago", mNormal));
            document.add(new Paragraph("7.     Convenio electricaribe - Consorcio multiservicios", mNormal));
            document.add(new Paragraph("8.     Anexos", mNormal));
            document.add(new Paragraph("    A-1     Contrato de Cr�dito", mNormal));
            document.add(new Paragraph("    A-2     Carta de Aceptaci�n", mNormal));
            document.add(new Paragraph(" "));
            document.add(new Paragraph(" "));
            document.add(new Paragraph(" "));
            document.add(new Paragraph(" "));
            document.add(new Paragraph(" "));

            tablaPrincipal(document);

            //--------------------------------------------------------------

            document.newPage();

            document.add(new Paragraph("1.  OBJETO", titulo));
            document.add(new Paragraph("El objeto de este documento es presentar la mejor oferta de servicios del CONSORCIO MULTISERVICIOS al cliente "+ epd.getOferta().getCliente() +", ubicado en la direcci�n "+ epd.getOferta().getDireccion() +" en la ciudad de "+ epd.getOferta().getCiudad() +", "+ epd.getOferta().getDepartamento() +" para " + epd.getOferta().getOferta().toUpperCase(), normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph("2.  ALCANCE Y DESCRIPCI�N T�CNICA", titulo));
            document.add(new Paragraph(" ", normal));

            if(aecas.size() > 0){
                document.add(new Paragraph("2.1 ALCANCES DE LA OFERTA", subtitulo));
                for(int i = 0; i < aecas.size(); i++){
                    if (aecas.get(i).getAlcance().length() > 0){
                        document.add(new Paragraph("  - "+aecas.get(i).getAlcance(), normal));
                    }
                }
            }

            document.add( new Paragraph(" ", normal) );

            if(aecas.size() > 0){
                document.add(new Paragraph("2.2 DESCRIPCIONES DE LA OFERTA", subtitulo));
                for(int i = 0; i < aecas.size(); i++){
                    if (aecas.get(i).getDescripcion().length() > 0){
                        document.add(new Paragraph("  - "+aecas.get(i).getDescripcion(), normal));
                    }
                }
            }

            document.add( new Paragraph(" ", normal) );

            //--------------------------------------------------------------

            document.add(new Paragraph("3.  ORGANIZACI�N Y MEDIOS", titulo));
            document.add(new Paragraph("EL CONSORCIO MULTISERVICIOS a trav�s del Ejecutivo de Cuenta de ELECTRICARIBE coordinar� con EL CLIENTE, el inicio de los trabajos contratados con el fin de planificar de forma adecuada entre las partes.", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph("EL CONSORCIO MULTISERVICIOS., se presentar� en obra en el plazo, con los siguientes medios:", normal));
            document.add(new Paragraph("    - Equipo humano", normal));
            document.add(new Paragraph("    - Equipos de inspecci�n requeridos, as� como con los materiales consumibles y accesorios.", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph("As� mismo, EL CONSORCIO MULTISERVICIOS., se compromete a:", normal));
            document.add(new Paragraph("    - Informar verbalmente y de forma inmediata, al personal designado por EL CLIENTE, de los defectos aparecidos y acciones recomendadas.", normal));
            document.add(new Paragraph("    - Elaborar y entregar un informe final con el resultado de las inspecciones y/o trabajos efectuados.", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph("EL CLIENTE realizar�, a trav�s de su personal de mantenimiento o un designado por �l, las maniobras de apertura o cierre de seccionadores, breakers o totalizadores, en caso que no se cuente con esta persona EL CONSORCIO MULTISERVICIOS., realizar� dichas maniobras a cuenta y riesgo del CLIENTE.", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph("EL CLIENTE pondr� a disposici�n de los expertos de EL CONSORCIO MULTISERVICIOS. El personal auxiliar necesario conocedor de la red interna de distribuci�n con los permisos de acceso necesarios y las llaves o instrumentos precisos para acceder a las celdas o gabinetes de circuito.", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph("El Cliente facilitar� todos los esquemas, informaci�n y planos disponibles, necesarios para la correcta realizaci�n de los trabajos.", normal));
            document.add(new Paragraph(" ", normal));

            //--------------------------------------------------------------

            //document.newPage();
            document.add(new Paragraph("La duraci�n de la inspecci�n y/o de los trabajos estar� pactada o acordada entre el personal de EL CONSORCIO MULTISERVICIOS, el Ejecutivo de Cuenta de ELECTRICARIBE S.A. E.S.P y el representante por parte del CLIENTE.", normal));
            document.add(new Paragraph(" ", normal));

            document.add(new Paragraph("4.  OFERTA ECONOMICA", titulo));
            document.add(new Paragraph("A continuaci�n se muestra el precio orientativo para la realizaci�n de los trabajos descritos en la oferta t�cnica, el precio real variar� en funci�n de los trabajos contratados y liquidados al final de la obra. En caso de requerir trabajos adicionales estos ser�n ofertados y se esperar� por parte del CLIENTE su aprobaci�n.", normal));

            document.add(new Paragraph(" ", normal));

            for(int i = 0; i < aecas.size(); i++){
                document.add(new Paragraph("Alcance "+(i+1), normal));
                document.add(new Paragraph(" ", normal));

                if(epd.getMaterialesPorTipo(aecas.get(i).getId_accion(), "M").size() > 0){
                    subtMateriales  = tablaItems(document, aecas.get(i), epd.getMaterialesPorTipo(aecas.get(i).getId_accion(), "M"), "MATERIALES");
                    document.add(new Paragraph(" ", normal));
                }

                if(epd.getMaterialesPorTipo(aecas.get(i).getId_accion(), "D").size() > 0){
                    subtManoDeObra  = tablaItems(document, aecas.get(i), epd.getMaterialesPorTipo(aecas.get(i).getId_accion(), "D"), "MANO DE OBRA");
                    document.add(new Paragraph(" ", normal));
                }

                if(epd.getMaterialesPorTipo(aecas.get(i).getId_accion(), "O").size() > 0){
                    subtOtros       = tablaItems(document, aecas.get(i), epd.getMaterialesPorTipo(aecas.get(i).getId_accion(), "O"), "OTROS");
                    document.add(new Paragraph(" ", normal));
                }

                double[] res = {subtMateriales, subtManoDeObra, subtOtros};
                tablaTotales(document, aecas.get(i), res);

                res = null;
                document.add(new Paragraph(" ", normal));
                document.add(new Paragraph(" ", normal));

                subtMateriales = 0;
                subtManoDeObra = 0;
                subtOtros = 0;
            }

            tablaTotalesGeneral(document);
            document.add(new Paragraph(" ", normal));

            document.add(new Paragraph("Consideraciones adicionales sobre la presente oferta:", subtitulo));
            for(int i = 0; i < epd.getConsideracionesByOffer().size(); i++){
                document.add(new Paragraph("    - " + epd.getConsideracionesByOffer().get(i).getDescripcion(), normal));
            }
            document.add(new Paragraph(" ", normal));

            document.add(new Paragraph(epd.getOferta().getOtras_consideraciones().toUpperCase(), normal));
            document.add(new Paragraph(" ", normal));

            document.add(new Paragraph("5.  VALIDEZ DE LA OFERTA", titulo));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph("La presente oferta es v�lida por quince (15) d�as.", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph("En caso de ser aprobada la oferta nos debe hacer llegar carta firmada por el administrador informando la forma de pago y el NIC al cual se cargar� el servicio, de igual manera autorizar el cobro del anticipo de los trabajos.", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph("La validez y el plazo de entrega se mantendr�n siempre y cuando, antes de su aprobaci�n no se presenten hechos graves e impredecibles que modifiquen sustancialmente nuestros costos totales, como una s�bita devaluaci�n monetaria, cambio radical en el precio del acero y del cobre, cambio en otros componentes o problemas de fuerza mayor en el suministro de materia prima.", normal));
            document.add(new Paragraph(" ", normal));

            if (epd.getOferta().getTipo_cliente().equals("R")){
                document.add(new Paragraph("6.  FORMA Y PROGRAMA DE PAGO", titulo));
                document.add(new Paragraph(" ", normal));
                document.add(new Paragraph("EL CLIENTE cancelar� el total de la oferta, el cual se facturar� a trav�s del recibo de energ�a; seg�n la siguiente formalidad de pago:", normal));
                document.add(new Paragraph(" ", normal));
                document.add(new Paragraph("De 0 a 6 Meses:     DTF + 9", normal));
                document.add(new Paragraph(" ", normal));
                document.add(new Paragraph("De 6 a 9 Meses:     DTF + 12", normal));
                document.add(new Paragraph(" ", normal));
                document.add(new Paragraph("De 9 a 12 Meses:    MAXIMA TASA LEGAL VIGENTE", normal));
                document.add(new Paragraph(" ", normal));
            }
            else{
                document.add(new Paragraph("6.  FORMA Y PROGRAMA DE PAGO", titulo));
                document.add(new Paragraph(" ", normal));
                document.add(new Paragraph("EL CLIENTE cancelar� el total de la oferta, el cual se facturar� a trav�s del recibo de energ�a; seg�n la siguiente formalidad de pago:", normal));
                document.add(new Paragraph(" ", normal));
                document.add(new Paragraph("De $0 a $10 Millones: 	Tasa M�xima Legal Vigente (TMLV)", normal));
                document.add(new Paragraph(" ", normal));
                document.add(new Paragraph("De $10.1 a $100 Millones: 	DTF + 12", normal));
                document.add(new Paragraph(" ", normal));
                document.add(new Paragraph("De $100.1 en adelante: 	DTF + 9", normal));
                document.add(new Paragraph(" ", normal));
                document.add(new Paragraph("El plazo m�ximo de financiaci�n ser� de 12 meses.", normal));
                document.add(new Paragraph(" ", normal));
            }

            document.add(new Paragraph("7.  CONVENIO ELECTRICARIBE - CONSORCIO MULTISERVICIOS", titulo));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph("ELECTRICARIBE S.A E.S.P - ENERGIA EMPRESARIAL DE LA COSTA S.A, ha realizado un convenio para el desarrollo, ejecuci�n y puesta en marcha de sus proyectos el�ctricos con el CONSORCIO MULTISERVICIOS.", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph("El CONSORCIO MULTISERVICIOS Es el directo responsable de la ejecuci�n de los trabajos, y ELECTRICARIBE S.A. E.S.P. actuara exclusivamente como Agente que factura y recauda estos Servicios a trav�s del recibo de energ�a y quien est� facultado para negociar dichos trabajos en t�rminos de precio, financiaci�n o acuerdo a Plazos.", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph("Para cualquier inquietud respecto a los trabajos realizados, garant�as, p�lizas, contrataci�n, acuerdo a plazos, puede contactar a su ejecutivo de cuenta asignado de ELECTRICARIBE S.A, para gestionar cualquier solicitud ante el CONSORCIO MULTISERVICIOS.", normal));

            //-------------------------------------------------------------------------------------------------

            document.newPage();

            document.add(new Paragraph("8.  ANEXOS", titulo));
            document.add(new Paragraph(" ", normal));

            PdfPTable table = new PdfPTable(1);
            table.setWidthPercentage(100);

            celda("CONVENIO COMERCIAL", table, tTitulo, Color.white, Color.white);
            document.add(table);

            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "Los suscritos, ALBERTO CARBONELL MARTINEZ, mayor de edad, domiciliado en la ciudad de Barranquilla, actuando como representante del CONSORCIO denominado CONSORCIO MULTISERVICIOS y como Apoderado Especial de cada una de las sociedades que conforman el CONSORCIO MULTISERVICIOS, tal como consta en los documentos adjuntos, quien en adelante y para todos los efectos se denominar� EL CONSORCIO MULTISERVICIOS, y "+epd.getOferta().getRepresentante()+", tambi�n mayor, actuando debidamente facultado en su condici�n de representante legal de "+epd.getOferta().getCliente()+", sociedad domiciliada en "+epd.getOferta().getCiudad()+", todo lo cual consta en el certificado de existencia y representaci�n legal y autorizaciones adjuntas, y/o en nombre propio , quien en adelante y para todos los efectos se denominar � EL CLIENTE, ambos identificados como aparece al pie de sus respectivas firmas , acuerdan celebrar el presente contrato de mutuo con intereses, que se regir� en gener al por la normatividad vigente y aplicable a este tipo de contratos y en particular por las siguientes cl�usulas:", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "PRIMERA: EL CLIENTE solicit� unos trabajos al CONSORCIO MULTISERVICIOS por un valor total de "+numToStr( totalOffer )+" ($"+Util.customFormat( totalOffer )+"), el cual coincidir� exactamente con el valor total del trabajo contenido en el acta final de entrega de dicho trabajo, firmada por EL CLIENTE y por un apoderado o representante del CONSORCIO MULTISERVICIOS. Si los valores contenidos en este documento y en el acta final de entrega de los trabajos realizados por el CONSORCIO MULTISERVICIOS fueren diferentes por alguna raz�n, las partes declaran y aceptan que el valor del cr�dito ser� el que contenga la mencionada acta final de entrega.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "SEGUNDA: En consecuencia, EL CLIENTE se obliga a pagar al CONSORCIO MULTISERVICIOS o su endosatario, el pr�stamo recibido por el valor del capital anotado en la cl�usula anterior, m�s los intereses de plazo pactados a una tasa mensual equivalente al DTF vigente para la fecha en que se termine el trabajo (seg�n la fecha especificada en el acta de terminaci�n del (los) trabajo(s), o simplemente en la fecha en que estos hayan terminado), m�s ___ puntos porcentuales, en ______ cuotas, en las fechas igualmente convenidas, conforme con la tabla que se relaciona en el Anexo No. 1 de este contrato, el cual tambi�n ser� firmado por ambas partes en se�al de aceptaci�n. Si por alguna raz�n no fuere posible firmar el se�alado anexo, el CONSORCIO MULTISERVICIOS estar� facultado para cobrar el capital y los intereses convenidos en este documento, en el n�mero de cuotas se�alado en esta cl�usula. Las cuotas se cobraran mensualmente, siendo la primera pagadera m�ximo a los treinta (30) d�as de la fecha de terminaci�n del (los) trabajo(s) contratado(s). El inter�s de plazo se liquidar� sobre los saldos insolutos, comenzando desde el d�a siguiente al de aquel en que se termine el o los trabajos contratados.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "El o los trabajos cotizados y contratados iniciar�n una vez recibida por parte del CONSORCIO MULTISERVICIOS la carta de aceptaci�n suscrito por EL CLIENTE. No obstante, si por alguna raz�n esta carta no fuere enviada por EL CLIENTE o recibida por el CONSORCIO MULTISERVICIOS, se entender� que la cotizaci�n de los trabajos fue aceptada el d�a en que comiencen a realizarse el o los respectivos trabajos cotizados.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "TERCERA: Si EL CLIENTE incumpliere siquiera uno de los pagos se�alados en la tabla contenida en el anexo mencionado en la cl�usula anterior, deber� reconocer y pagar un inter�s moratorio igual a una y media veces el inter�s bancario corriente certificado por la Superintendencia Financiera de Colombia, liquidado sobre el saldo insoluto de capital por cada d�a que permanezca en mora. El CONSORCIO MULTISERVICIOS estar� facultado igualmente para declarar como vencidos los plazos de pago convenidos con EL CLIENTE y requerir inmediatamente, a�n por v�a judicial, el pago de la obligaci�n completa, con sus accesorios, en este caso. Las partes acuerdan y aceptan que, en aplicaci�n de lo consagrado en el art. 886 del C.Co. y dem�s normas que lo complementen o modifiquen, al iniciarse o luego de iniciarse el respectivo proceso judicial para el cobro de toda o parte de la obligaci�n convenida en este contrato que se encuentre vencida, podr�n capitalizarse los intereses que lleven un a�o o m�s de vencidos.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "De igual forma, El CONSORCIO MULTISERVICIOS podr� declarar como vencidos los plazos de pago convenidos con EL CLIENTE y requerir inmediatamente, a�n por v�a judicial, el pago de la obligaci�n completa, con sus accesorios, en los siguientes casos: a) muerte de cualquiera de los obligados como CLIENTES o declaraci�n de disoluci�n de cualquiera de estos si fuere persona jur�dica; b) si cualquiera de los obligados como CLIENTES solicitare o iniciare proceso de reorganizaci�n, liquidaci�n judicial, concurso de acreedores, hiciere oferta de cesi�n de bienes, reestructuraci�n econ�mica, o cualquier clase de proceso concursal; c) si cualquiera de los obligados como CLIENTES fuere embargado o requerido judicialmente con cualquier acci�n; d) el simple retraso en el cumplimiento de los pagos de las cuotas convenidas, o del capital, los intereses o cualquier otra obligaci�n accesoria o principal pactada en este contrato de cr�dito; e) el giro siquiera de un cheque sin provisi�n de fondos por parte de cualquiera de los obligados como CLIENTES.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "PAR�GRAFO: El recibo de abonos parciales no implica novaci�n de la obligaci�n convenida con la firma de este contrato y cualquier pago que haga EL CLIENTE se imputar� primero al pago de los gastos de cobranza, luego a honorarios de abogado, despu�s a penas, intereses de mora e intereses de plazo (en este orden) y por �ltimo a capital.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "CUARTA: Con la firma de este contrato de cr�dito, el CONSORCIO MULTISERVICIOS estar� facultado para retirar todos y cada uno de los bienes y/o materiales instalados en ejecuci�n del contrato de obra contratado por EL CLIENTE en su beneficio. Dichos bienes deber�n ser avaluados en el estado en que sean retirados, a fin de abonar ese valor al pago de la obligaci�n dineraria contra�da por EL CLIENTE con el CONSORCIO MULTISERVICIOS. Con la firma de este contrato, EL CLIENTE acepta el aval�o que el CONSORCIO MULTISERVICIOS hiciere de los bienes retirados al retirarlos de acuerdo con lo expresado en esta cl�usula, siempre que no difiera en m�s de un 15% del o los precios promedio que tuvieren en el mercado secundario local de la ciudad de Barranquilla. De igual forma, EL CLIENTE se obliga a pagar o reconocer el costo en que sea necesario incurrir para recuperar o retirar dichos bienes para que el CONSORCIO MULTISERVICIOS los reciba. Para tal efecto, EL CLIENTE acepta, con la firma de este contrato, que el valor de dicho costo ser� el que el CONSORCIO MULTISERVICIOS exprese en su cobro prejudicial o en la respectiva demanda ejecutiva. El CONSORCIO MULTISERVICIOS estar� facultado para cobrar al CLIENTE y este estar� obligado a pagar tanto el costo del retiro de los bienes y materiales de que se trata en esta cl�usula, declarado por el CONSORCIO MULTISERVICIOS, como un inter�s de mora liquidado sobre dicho valor a una tasa igual a una y media veces el inter�s bancario corriente certificado por la Superintendencia Financiera de Colombia, por cada d�a que transcurra entre el d�a del retiro de un bien o material y aquel en que EL CLIENTE realice el pago total de dicho costo.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "QUINTA: EL CLIENTE se obliga igualmente a reconocer y pagar, como m�nimo, un valor igual al 10% del valor total requerido o recaudado por cualquier concepto, a t�tulo de honorarios profesionales del abogado escogido por el CONSORCIO MULTISERVICIOS, cuando este cobro se haga en forma prejudicial. Si el cobro se hiciere en forma judicial, estar� obligado a pagar, como m�nimo un 20% liquidado de la misma forma. El CONSORCIO MULTISERVICIOS podr� requerir el pago tanto del valor del capital y los intereses de plazo y de mora no cancelados, como el de los gastos de cobranza, honorarios de abogado utilizado para dicha cobranza, seg�n lo especificado arriba en esta cl�usula y el costo del retiro de bienes y materiales que tuviere instalados EL CLIENTE y que haya necesidad de hacer.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "SEXTA: Con la firma de este contrato, EL CLIENTE renuncia a cualquier requerimiento que por ley fuere necesario que el CONSORCIO MULTISERVICIOS le hiciere para exigir el pago del dinero prestado, los intereses y dem�s costos en que deba incurrir esta �ltima en desarrollo de lo convenido en este contrato, o para constituirlo en mora.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "S�PTIMA: EL CLIENTE (todos quienes suscriben este documento en calidad de CLIENTES) autoriza con la firma de este contrato, de manera permanente e irrevocable, con fines estad�sticos, de control, supervisi�n y de informaci�n comercial, a reportar, verificar, procesar, consultar, conservar, suministrar, actualizar y divulgar a cualquiera de las centrales de informaci�n de Colombia autorizadas para tal efecto, su comportamiento crediticio y, en especial, el eventual incumplimiento de una cualquiera de las obligaciones pactadas en este contrato.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "OCTAVA: EL CLIENTE expresamente autoriza a El CONSORCIO MULTISERVICIOS para que a cualquier t�tulo ceda el cr�dito incorporado en �l a favor de cualquier tercero sin necesidad de su notificaci�n, de conformidad con lo establecido art�culo 651 del C�digo de Comercio en cuyo caso, adicionalmente, dicho tercero adquirir� autom�ticamente y sin necesidad de cesi�n adicional alguna el car�cter de beneficiario a titulo oneroso de las p�lizas de seguro que se expidan a favor de el CONSORCIO MULTISERVICIOS en los t�rminos del presente pagare.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "En constancia de todo lo anterior, suscriben este contrato quienes intervienen en el acto, en Barranquilla a los _____d�as de ________ de ______.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            document.add(new Paragraph(" ", cNormal));
            document.add(new Paragraph(" ", cNormal));

            tablaFirmas(document);

            document.newPage();

            document.add(new Paragraph("8.  ANEXOS", titulo));
            document.add(new Paragraph(" ", normal));

            table = null;
            table = new PdfPTable(1);
            table.setWidthPercentage(100);

            celda("CONVENIO COMERCIAL", table, tTitulo, Color.white, Color.white);
            document.add(table);

            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "Los suscritos, ALBERTO CARBONELL MARTINEZ, mayor de edad, domiciliado en la ciudad de Barranquilla, actuando como representante del CONSORCIO denominado CONSORCIO MULTISERVICIOS y como Apoderado Especial de cada una de las sociedades que conforman el CONSORCIO MULTISERVICIOS, tal como consta en los documentos adjuntos, quien en adelante y para todos los efectos se denominar� EL CONSORCIO MULTISERVICIOS, y "+epd.getOferta().getRepresentante()+", tambi�n mayor, actuando debidamente facultado en su condici�n de representante legal de "+epd.getOferta().getCliente()+", sociedad domiciliada en "+epd.getOferta().getCiudad()+", todo lo cual consta en el certificado de existencia y representaci�n legal y autorizaciones adjuntas, y/o en nombre propio , quien en adelante y para todos los efectos se denominar � EL CLIENTE, ambos identificados como aparece al pie de sus respectivas firmas , acuerdan celebrar el presente contrato de mutuo con intereses, que se regir� en gener al por la normatividad vigente y aplicable a este tipo de contratos y en particular por las siguientes cl�usulas:", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "PRIMERA: EL CLIENTE solicit� unos trabajos al CONSORCIO MULTISERVICIOS por un valor total de "+numToStr( totalOffer )+" ($"+Util.customFormat( totalOffer )+"), el cual coincidir� exactamente con el valor total del trabajo contenido en el acta final de entrega de dicho trabajo, firmada por EL CLIENTE y por un apoderado o representante del CONSORCIO MULTISERVICIOS. Si los valores contenidos en este documento y en el acta final de entrega de los trabajos realizados por el CONSORCIO MULTISERVICIOS fueren diferentes por alguna raz�n, las partes declaran y aceptan que el valor del cr�dito ser� el que contenga la mencionada acta final de entrega.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "Con la firma de este contrato, las partes entienden que EL CLIENTE acept� la oferta de servicios que le fuere presentada por CONSORCIO MULTISERVICIOS para la realizaci�n del (los) trabajo(s) contratado(s).", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "SEGUNDA: EL CLIENTE se obliga a cancelar el total del valor del trabajo contratado en la oportunidad que le sea incluido en la factura que le presente ELECTRICARIBE S.A. E.S.P., o la empresa comercializadora de energ�a que llegare a reemplazarla, en la primera o segunda factura que recibir� con posterioridad a la fecha en que los trabajos contratados sean terminados. La fecha en que reciba dicha factura corresponder� a la fecha que el sistema de ELECTRICARIBE S.A. E.S.P. tenga programado presentarle factura al CLIENTE, seg�n el tipo de usuario de que se trate. El correspondiente valor deber� pagarlo a m�s tardar en la fecha que en la factura en que sea incluido el valor se se�ale como fecha l�mite de pago.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "TERCERA: Si EL CLIENTE no pagare el valor del (los) materiales y/o trabajo(s) contratado(s) en la fecha expresada en la �ltima oraci�n de la cl�usula anterior, incurrir� en mora y estar� obligado a reconocer y pagar intereses de mora, liquidados a la tasa m�xima permitida por la ley comercial por cada d�a de retraso en el pago. Con la firma de este contrato, EL CLIENTE renuncia a ser constituido en mora y renuncia a tener que recibir de parte de COSORCIO MULTISERVICIOS cualquier requerimiento relacionado con su obligaci�n de pagar o con que esta se encuentra vencida.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "CUARTA: Si EL CLIENTE no pagare por la obra y/o servicio(s) prestado(s) por CONSORCIO MULTISERVICIOS, a m�s tardar en la fecha indicada como fecha l�mite en la factura que le sea presentada por ELECTRICARIBE S.A. E.S.P., con la firma de este contrato autoriza y CONSORCIO MULTISERVICIOS estar� facultado para retirar todos y cada uno de los bienes y/o materiales instalados en ejecuci�n de los servicios contratados por EL CLIENTE en su beneficio. Dichos bienes deber�n ser avaluados en el estado en que sean retirados, a fin de abonar ese valor al pago de la obligaci�n dineraria contra�da por EL CLIENTE con CONSORCIO MULTISERVICIOS. Con la firma de este contrato, EL CLIENTE acepta el aval�o que CONSORCIO MULTISERVICIOS hiciere de los bienes retirados al retirarlos de acuerdo con lo expresado en esta cl�usula, siempre que no difiera en m�s de un 15% del o los precios promedio que tuvieren en el mercado secundario local de la ciudad de Barranquilla. De igual forma, EL CLIENTE se obliga a pagar o reconocer el costo en que sea necesario incurrir para recuperar o retirar dichos bienes para que CONSORCIO MULTISERVICIOS los reciba. Para tal efecto, EL CLIENTE acepta, con la firma de este contrato, que el valor de dicho costo ser� el que CONSORCIO MULTISERVICIOS exprese en su cobro prejudicial o en la respectiva demanda ejecutiva. CONSORCIO MULTISERVICIOS estar� facultado para cobrar al CLIENTE y este estar� obligado a pagar tanto el costo del retiro de los bienes y materiales de que se trata  en esta cl�usula, declarado por CONSORCIO MULTISERVICIOS, como un inter�s de mora liquidado sobre dicho valor a una tasa igual a una y media veces el inter�s bancario corriente certificado por la Superintendencia Financiera de Colombia, por cada d�a que transcurra entre el d�a del retiro de un bien o material y aquel en que EL CLIENTE realice el pago total de dicho costo.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "QUINTA: EL CLIENTE se obliga igualmente a reconocer y pagar, como m�nimo, un valor igual al 10% del valor total recaudado por cualquier concepto, a t�tulo de honorarios profesionales del abogado escogido por CONSORCIO MULTISERVICIOS, cuando este cobro se haga en forma prejudicial. Si el cobro se hiciere en forma judicial, estar� obligado a pagar, como m�nimo un 20% liquidado de la misma forma. CONSORCIO MULTISERVICIOS podr� requerir el pago tanto del valor del capital y los intereses de mora, como el de los gastos de cobranza, honorarios de abogado utilizado para dicha cobranza, seg�n lo especificado arriba en esta cl�usula y el costo del retiro de bienes y materiales que tuviere instalados EL CLIENTE y que haya necesidad de hacer.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "SEXTA: EL CLIENTE (todos quienes suscriben este documento en calidad de CLIENTES) autoriza con la firma de este contrato, de manera permanente e irrevocable, con fines estad�sticos, de control, supervisi�n y de informaci�n comercial, a reportar, verificar, procesar, consultar, conservar, suministrar, actualizar y divulgar a cualquiera de las centrales de informaci�n de Colombia autorizadas para tal efecto, su comportamiento crediticio y, en especial, el eventual incumplimiento de una cualquiera de las obligaciones pactadas en este contrato.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "S�PTIMA: EL CLIENTE autoriza, con la firma de este contrato, la cesi�n del mismo al tercero que CONSORCIO MULTISERVICIOS determine. Para que surta efecto la correspondiente cesi�n, no ser� necesario que EL CLIENTE sea notificado o acepte la misma. Sin embargo, si el nuevo acreedor fuere a recibir los pagos de una forma o en un lugar distinto a aquellos en los cuales lo haga CONSORCIO MULTISERVICIOS, ser� obligaci�n, ya sea de este �ltimo o del nuevo acreedor, informarle al CLIENTE la manera o el sitio en donde se recibir�n los correspondientes pagos. Por consiguiente, antes de la mencionada comunicaci�n informando sobre el cambio, se entender� que EL CLIENTE cumplir� con sus obligaciones como lo pacta en este contrato, especialmente la relacionada con el pago.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "En constancia de todo lo anterior, suscriben este contrato quienes intervienen en el acto, en Barranquilla a los _____d�as de ________ de ______.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            document.add(new Paragraph(" ", cNormal));
            document.add(new Paragraph(" ", cNormal));

             tablaFirmas(document);

            //-------------------------------------------------------------------------------------------------

            document.newPage();

            // Esta es la parte de la carta
            parrafo(document, "_______________, ____________________", Element.ALIGN_JUSTIFIED, normal);
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));

            parrafo(document, "SE�ORES", Element.ALIGN_JUSTIFIED, normal);
            parrafo(document, "CONSORCIO MULTISERVICIOS", Element.ALIGN_JUSTIFIED, normal);
            parrafo(document, "ELECTRICARIBE S.A. E.S.P.", Element.ALIGN_JUSTIFIED, normal);
            parrafo(document, "Atn : "+epd.getOferta().getEjecutivo(), Element.ALIGN_JUSTIFIED, normal);
            parrafo(document, "CIUDAD", Element.ALIGN_JUSTIFIED, normal);
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));

            parrafo(document, "REF.: AUTORIZACI�N TRABAJOS OFERTA No. "+epd.getOferta().getConsecutivo(), Element.ALIGN_JUSTIFIED, normal);
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));

            parrafo(document, "Por medio de la presente yo, "+epd.getOferta().getRepresentante()+", en representaci�n de "+epd.getOferta().getCliente()+". con NIT nro. "+epd.getOferta().getNIT()+" y en calidad de REPRESENTANTE LEGAL, me permito autorizar a Ustedes ejecutar las labores cotizadas en la oferta en la referencia.", Element.ALIGN_JUSTIFIED, normal);
            document.add(new Paragraph(" ", normal));
            parrafo(document, "De igual manera autorizo sea cargada un anticipo del trabajo del 15% al recibo de energ�a el cual esta en su sistema comercial identificado con el NIC. "+epd.getOferta().getNIC()+" El 85% restante por favor diferirlo en _____ cuotas mensuales a trav�s del mismo N�mero de contrato.", Element.ALIGN_JUSTIFIED, normal);
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));

            parrafo(document, "Agradezco su atenci�n.", Element.ALIGN_JUSTIFIED, normal);
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));
            parrafo(document, "Cordialmente,", Element.ALIGN_JUSTIFIED, normal);
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));
            parrafo(document, epd.getOferta().getRepresentante(), Element.ALIGN_JUSTIFIED, normal);
            parrafo(document, "C.C."+epd.getOferta().getNIT(), Element.ALIGN_JUSTIFIED, normal);
        }
        catch (Exception ex) {
            System.out.println("error pdf:"+ex.toString());
                ex.printStackTrace();
            next="Error escribiendo en el archivo";
        }
    }

    /**
     * Este metodo es el segundo paso para la creacion del PDF.
     * Ac� es donde todo se agrega a la hoja del PDF.
     */
    public void doEmergencyPDF() throws Exception{

        int ano = 0;
        int mes = 0;
        int dia = 0;

        try {
            document.open();

            ano = Integer.valueOf( epd.getOferta().getFechaGeneracion().substring(0, 4) );
            mes = Integer.valueOf( epd.getOferta().getFechaGeneracion().substring(5, 7) );
            dia = Integer.valueOf( epd.getOferta().getFechaGeneracion().substring(8, 10) );

            parrafo(document, epd.getOferta().getCiudad()+", "+dia+" de "+meses[mes]+" de "+ano, Element.ALIGN_JUSTIFIED, normal);
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));

            parrafo(document, "SE�ORES",                                Element.ALIGN_JUSTIFIED, normal);
            parrafo(document, epd.getOferta().getCliente(),             Element.ALIGN_JUSTIFIED, normal);
            parrafo(document, "NIC: "+epd.getOferta().getNIC(),         Element.ALIGN_JUSTIFIED, normal);
            parrafo(document, "AVISO : "+epd.getOferta().getAviso(),    Element.ALIGN_JUSTIFIED, normal);
            parrafo(document, "CIUDAD",                                 Element.ALIGN_JUSTIFIED, normal);
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));

            parrafo(document, epd.getOferta().getOferta().toUpperCase(), Element.ALIGN_CENTER, normal);
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));

            parrafo(document, "", Element.ALIGN_JUSTIFIED, normal);
            document.add(new Paragraph(" ", normal));
            parrafo(document, "De la manera m�s cordial me dirijo a ustedes con el fin de informales los costos de los trabajos que fueron realizados el d�a "+dia+" de "+meses[mes]+" de "+ano+", en las instalaciones del cliente "+epd.getOferta().getCliente()+" ubicado, en la direccion "+epd.getOferta().getDireccion()+" en el municipio de "+epd.getOferta().getCiudad()+" en el departamento del "+epd.getOferta().getDepartamento()+", los cuales consistieron en:", Element.ALIGN_JUSTIFIED, normal);
            document.add(new Paragraph(" ", normal));

            if(aecas.size() > 0){
                for(int i = 0; i < aecas.size(); i++){
                    if (aecas.get(i).getDescripcion().length() > 0){
                        document.add(new Paragraph("  - "+aecas.get(i).getDescripcion(), normal));
                    }
                }
            }

            document.add(new Paragraph(" ", normal));

            parrafo(document, "Estos trabajos fueron necesarios para la normalizaci�n del servicio de energ�a en las instalaciones del cliente.", Element.ALIGN_JUSTIFIED, normal);
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));

            parrafo(document, "Cordialmente,", Element.ALIGN_JUSTIFIED, normal);
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));

            parrafo(document, epd.getOferta().getEjecutivo().toUpperCase(), Element.ALIGN_JUSTIFIED, normal);
            parrafo(document, "Ejecutivo de cuentas", Element.ALIGN_JUSTIFIED, normal);
            parrafo(document, "Electricaribe S.A. E.S.P.", Element.ALIGN_JUSTIFIED, normal);

            //---------------------------------------------------

            document.newPage();

            parrafo(document, "COSTOS", Element.ALIGN_CENTER, titulo);

            for(int i = 0; i < aecas.size(); i++){
                document.add(new Paragraph("Alcance "+(i+1), subtitulo));
                document.add(new Paragraph(" ", normal));

                if(epd.getMaterialesPorTipo(aecas.get(i).getId_accion(), "M").size() > 0){
                    subtMateriales  = tablaItems(document, aecas.get(i), epd.getMaterialesPorTipo(aecas.get(i).getId_accion(), "M"), "MATERIALES");
                    document.add(new Paragraph(" ", normal));
                }

                if(epd.getMaterialesPorTipo(aecas.get(i).getId_accion(), "D").size() > 0){
                    subtManoDeObra  = tablaItems(document, aecas.get(i), epd.getMaterialesPorTipo(aecas.get(i).getId_accion(), "D"), "MANO DE OBRA");
                    document.add(new Paragraph(" ", normal));
                }

                if(epd.getMaterialesPorTipo(aecas.get(i).getId_accion(), "O").size() > 0){
                    subtOtros       = tablaItems(document, aecas.get(i), epd.getMaterialesPorTipo(aecas.get(i).getId_accion(), "O"), "OTROS");
                    document.add(new Paragraph(" ", normal));
                }

                double[] res = {subtMateriales, subtManoDeObra, subtOtros};
                tablaTotales(document, aecas.get(i), res);

                res = null;
                document.add(new Paragraph(" ", normal));
                document.add(new Paragraph(" ", normal));

                subtMateriales = 0;
                subtManoDeObra = 0;
                subtOtros = 0;
            }

            tablaTotalesGeneral(document);
            document.add(new Paragraph(" ", normal));

            document.add(new Paragraph("Los precios de la presente oferta son firmes y pagaderos en Pesos Colombianos. El valor agregado I.V.A, se cobrar� seg�n el porcentaje vigente en la fecha de facturaci�n.", subtitulo));
            document.add(new Paragraph(" ", normal));

            document.add(new Paragraph("Nuevos impuesto o aumentos de los impuestos existentes, de orden nacional, Departamental o Municipal que intervengan en la facturaci�n final, se liquidar�n y cobrar�n seg�n porcentaje vigente en la fecha de facturaci�n.", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));

            document.add(new Paragraph("CONDICIONES DE PAGO", subtitulo));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph("EL CLIENTE cancelar� el total de la oferta, el cual se facturar� a trav�s del recibo de energ�a; seg�n la siguiente formalidad de pago:", normal));
            document.add(new Paragraph(" ", normal));

            document.add(new Paragraph("De 0 a 6 Meses:     DTF + 9", subtitulo));
            document.add(new Paragraph("De 7 a 9 Meses:     DTF + 12", subtitulo));
            document.add(new Paragraph("De 10 a 12 Meses:   MAXIMA TASA LEGAL VIGENTE", subtitulo));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));

            document.add(new Paragraph("INTERESES DE MORA", subtitulo));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph("En caso de mora en los pagos, cobraremos los intereses moratorios la m�xima tasa autorizada por la Superintendencia Bancaria.", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));

            parrafo(document, "Atentamente,", Element.ALIGN_JUSTIFIED, normal);
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));

            parrafo(document, epd.getOferta().getEjecutivo().toUpperCase(), Element.ALIGN_JUSTIFIED, normal);
            parrafo(document, "Ejecutivo de cuentas", Element.ALIGN_JUSTIFIED, normal);
            parrafo(document, "Electricaribe S.A. E.S.P.", Element.ALIGN_JUSTIFIED, normal);

            //---------------------------------------------------

            document.newPage();

            document.add(new Paragraph("ANEXO", titulo));
            document.add(new Paragraph(" ", normal));

            tablaTitulos(document, "CONVENIO COMERCIAL");

            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "Los suscritos, ALBERTO CARBONELL MARTINEZ, mayor de edad, domiciliado en la ciudad de Barranquilla, actuando como representante del CONSORCIO denominado CONSORCIO MULTISERVICIOS y como Apoderado Especial de cada una de las sociedades que conforman el CONSORCIO MULTISERVICIOS, tal como consta en los documentos adjuntos, quien en adelante y para todos los efectos se denominar� EL CONSORCIO MULTISERVICIOS, y "+epd.getOferta().getRepresentante()+", tambi�n mayor, actuando debidamente facultado en su condici�n de representante legal de "+epd.getOferta().getCliente()+", sociedad domiciliada en "+epd.getOferta().getCiudad()+", todo lo cual consta en el certificado de existencia y representaci�n legal y autorizaciones adjuntas, y/o en nombre propio , quien en adelante y para todos los efectos se denominar � EL CLIENTE, ambos identificados como aparece al pie de sus respectivas firmas , acuerdan celebrar el presente contrato de mutuo con intereses, que se regir� en gener al por la normatividad vigente y aplicable a este tipo de contratos y en particular por las siguientes cl�usulas:", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "PRIMERA: EL CLIENTE solicit� unos trabajos al CONSORCIO MULTISERVICIOS por un valor total de "+numToStr( totalOffer )+" ($"+Util.customFormat( totalOffer )+"), el cual coincidir� exactamente con el valor total del trabajo contenido en el acta final de entrega de dicho trabajo, firmada por EL CLIENTE y por un apoderado o representante del CONSORCIO MULTISERVICIOS. Si los valores contenidos en este documento y en el acta final de entrega de los trabajos realizados por el CONSORCIO MULTISERVICIOS fueren diferentes por alguna raz�n, las partes declaran y aceptan que el valor del cr�dito ser� el que contenga la mencionada acta final de entrega.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "Con la firma de este contrato, las partes entienden que EL CLIENTE acept� la oferta de servicios que le fuere presentada por CONSORCIO MULTISERVICIOS para la realizaci�n del (los) trabajo(s) contratado(s).", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "SEGUNDA: EL CLIENTE se obliga a cancelar el total del valor del trabajo contratado en la oportunidad que le sea incluido en la factura que le presente ELECTRICARIBE S.A. E.S.P., o la empresa comercializadora de energ�a que llegare a reemplazarla, en la primera o segunda factura que recibir� con posterioridad a la fecha en que los trabajos contratados sean terminados. La fecha en que reciba dicha factura corresponder� a la fecha que el sistema de ELECTRICARIBE S.A. E.S.P. tenga programado presentarle factura al CLIENTE, seg�n el tipo de usuario de que se trate. El correspondiente valor deber� pagarlo a m�s tardar en la fecha que en la factura en que sea incluido el valor se se�ale como fecha l�mite de pago.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "TERCERA: Si EL CLIENTE no pagare el valor del (los) materiales y/o trabajo(s) contratado(s) en la fecha expresada en la �ltima oraci�n de la cl�usula anterior, incurrir� en mora y estar� obligado a reconocer y pagar intereses de mora, liquidados a la tasa m�xima permitida por la ley comercial por cada d�a de retraso en el pago. Con la firma de este contrato, EL CLIENTE renuncia a ser constituido en mora y renuncia a tener que recibir de parte de COSORCIO MULTISERVICIOS cualquier requerimiento relacionado con su obligaci�n de pagar o con que esta se encuentra vencida.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "CUARTA: Si EL CLIENTE no pagare por la obra y/o servicio(s) prestado(s) por CONSORCIO MULTISERVICIOS, a m�s tardar en la fecha indicada como fecha l�mite en la factura que le sea presentada por ELECTRICARIBE S.A. E.S.P., con la firma de este contrato autoriza y CONSORCIO MULTISERVICIOS estar� facultado para retirar todos y cada uno de los bienes y/o materiales instalados en ejecuci�n de los servicios contratados por EL CLIENTE en su beneficio. Dichos bienes deber�n ser avaluados en el estado en que sean retirados, a fin de abonar ese valor al pago de la obligaci�n dineraria contra�da por EL CLIENTE con CONSORCIO MULTISERVICIOS. Con la firma de este contrato, EL CLIENTE acepta el aval�o que CONSORCIO MULTISERVICIOS hiciere de los bienes retirados al retirarlos de acuerdo con lo expresado en esta cl�usula, siempre que no difiera en m�s de un 15% del o los precios promedio que tuvieren en el mercado secundario local de la ciudad de Barranquilla. De igual forma, EL CLIENTE se obliga a pagar o reconocer el costo en que sea necesario incurrir para recuperar o retirar dichos bienes para que CONSORCIO MULTISERVICIOS los reciba. Para tal efecto, EL CLIENTE acepta, con la firma de este contrato, que el valor de dicho costo ser� el que CONSORCIO MULTISERVICIOS exprese en su cobro prejudicial o en la respectiva demanda ejecutiva. CONSORCIO MULTISERVICIOS estar� facultado para cobrar al CLIENTE y este estar� obligado a pagar tanto el costo del retiro de los bienes y materiales de que se trata  en esta cl�usula, declarado por CONSORCIO MULTISERVICIOS, como un inter�s de mora liquidado sobre dicho valor a una tasa igual a una y media veces el inter�s bancario corriente certificado por la Superintendencia Financiera de Colombia, por cada d�a que transcurra entre el d�a del retiro de un bien o material y aquel en que EL CLIENTE realice el pago total de dicho costo.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "QUINTA: EL CLIENTE se obliga igualmente a reconocer y pagar, como m�nimo, un valor igual al 10% del valor total recaudado por cualquier concepto, a t�tulo de honorarios profesionales del abogado escogido por CONSORCIO MULTISERVICIOS, cuando este cobro se haga en forma prejudicial. Si el cobro se hiciere en forma judicial, estar� obligado a pagar, como m�nimo un 20% liquidado de la misma forma. CONSORCIO MULTISERVICIOS podr� requerir el pago tanto del valor del capital y los intereses de mora, como el de los gastos de cobranza, honorarios de abogado utilizado para dicha cobranza, seg�n lo especificado arriba en esta cl�usula y el costo del retiro de bienes y materiales que tuviere instalados EL CLIENTE y que haya necesidad de hacer.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "SEXTA: EL CLIENTE (todos quienes suscriben este documento en calidad de CLIENTES) autoriza con la firma de este contrato, de manera permanente e irrevocable, con fines estad�sticos, de control, supervisi�n y de informaci�n comercial, a reportar, verificar, procesar, consultar, conservar, suministrar, actualizar y divulgar a cualquiera de las centrales de informaci�n de Colombia autorizadas para tal efecto, su comportamiento crediticio y, en especial, el eventual incumplimiento de una cualquiera de las obligaciones pactadas en este contrato.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "S�PTIMA: EL CLIENTE autoriza, con la firma de este contrato, la cesi�n del mismo al tercero que CONSORCIO MULTISERVICIOS determine. Para que surta efecto la correspondiente cesi�n, no ser� necesario que EL CLIENTE sea notificado o acepte la misma. Sin embargo, si el nuevo acreedor fuere a recibir los pagos de una forma o en un lugar distinto a aquellos en los cuales lo haga CONSORCIO MULTISERVICIOS, ser� obligaci�n, ya sea de este �ltimo o del nuevo acreedor, informarle al CLIENTE la manera o el sitio en donde se recibir�n los correspondientes pagos. Por consiguiente, antes de la mencionada comunicaci�n informando sobre el cambio, se entender� que EL CLIENTE cumplir� con sus obligaciones como lo pacta en este contrato, especialmente la relacionada con el pago.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            parrafo(document, "En constancia de todo lo anterior, suscriben este contrato quienes intervienen en el acto, en Barranquilla a los _____d�as de ________ de ______.", Element.ALIGN_JUSTIFIED, cNormal);
            document.add(new Paragraph(" ", cNormal));
            document.add(new Paragraph(" ", cNormal));
            document.add(new Paragraph(" ", cNormal));

            tablaFirmas(document);

            //-------------------------------------------------------------------------------------------------

            document.newPage();

            // Esta es la parte de la carta
            parrafo(document, "_______________, ____________________", Element.ALIGN_JUSTIFIED, normal);
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));

            parrafo(document, "SE�ORES", Element.ALIGN_JUSTIFIED, normal);
            parrafo(document, "CONSORCIO MULTISERVICIOS", Element.ALIGN_JUSTIFIED, normal);
            parrafo(document, "ELECTRICARIBE S.A. E.S.P.", Element.ALIGN_JUSTIFIED, normal);
            parrafo(document, "Atn : "+epd.getOferta().getEjecutivo(), Element.ALIGN_JUSTIFIED, normal);
            parrafo(document, "CIUDAD", Element.ALIGN_JUSTIFIED, normal);
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));

            parrafo(document, "REF.: AUTORIZACI�N TRABAJOS OFERTA No. "+epd.getOferta().getConsecutivo(), Element.ALIGN_JUSTIFIED, normal);
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));

            parrafo(document, "Por medio de la presente yo, "+epd.getOferta().getRepresentante()+", en representaci�n de "+epd.getOferta().getCliente()+". con NIT nro. "+epd.getOferta().getNIT()+" y en calidad de REPRESENTANTE LEGAL, me permito autorizar a Ustedes ejecutar las labores cotizadas en la oferta en la referencia.", Element.ALIGN_JUSTIFIED, normal);
            document.add(new Paragraph(" ", normal));
            parrafo(document, "De igual manera autorizo sea cargada un anticipo del trabajo del 15% al recibo de energ�a el cual esta en su sistema comercial identificado con el NIC. "+epd.getOferta().getNIC()+" El 85% restante por favor diferirlo en _____ cuotas mensuales a trav�s del mismo N�mero de contrato.", Element.ALIGN_JUSTIFIED, normal);
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));

            parrafo(document, "Agradezco su atenci�n.", Element.ALIGN_JUSTIFIED, normal);
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));
            parrafo(document, "Cordialmente,", Element.ALIGN_JUSTIFIED, normal);
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));
            parrafo(document, epd.getOferta().getRepresentante(), Element.ALIGN_JUSTIFIED, normal);
            parrafo(document, "C.C."+epd.getOferta().getNIT(), Element.ALIGN_JUSTIFIED, normal);
        }
        catch (Exception e){
            System.out.println("error pdf:"+e.toString());
                e.printStackTrace();
            next="Error escribiendo en el archivo";
        }
    }

    /**
     * Este metodo es el segundo paso para la creacion del PDF.
     * Ac� es donde todo se agrega a la hoja del PDF.
     */
    public void tablaTitulos(Document doc, String cad) throws Exception{
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(100);

        celda(cad, table, tTitulo, Color.white, Color.white);
        doc.add(table);
    }

    /**
     * Este metodo es el que crea la tabla de la primera pagina, la
     * cual es la tabla que contiene elementos como el cliente y el
     * NIC del mismo.
     *
     * @param doc El documento donde se va a incluir la tabla (Variable tipo Document)
     */
    public void tablaPrincipal(Document doc){
        try {
            float[] widths = {0.2375f, 0.2375f, 0.05f, 0.2375f, 0.2375f};
            PdfPTable table = new PdfPTable(widths);
            table.setWidthPercentage(100);
            PdfPCell cell;

            String[] col1 = {"Elaborada por:"                   , "Fecha:"                  , "Aprobada por:"                       , "Ejecutivo de cuenta:"};
            String[] col2 = {epd.getOferta().getElaboradoPor()  , offerFormat.format(dat)    , epd.getOferta().getAprobadoPor()      , epd.getOferta().getEjecutivo()};
            String[] col3 = {"Cliente:"                         , "NIC:"                    , "Oferta:"                             , "No. de referencia"};
            String[] col4 = {epd.getOferta().getCliente()       , epd.getOferta().getNIC()  , epd.getOferta().getOferta()           , epd.getOferta().getConsecutivo()};

            for (int i = 0; i < 4; i++) {
                celda(col1[i],  table, cSubtitulo,  Color.lightGray,    Color.lightGray);
                celda(col2[i],  table, cNormal,     Color.lightGray,    Color.lightGray);
                celda("",       table, cNormal,     Color.white,        Color.white);
                celda(col3[i],  table, cSubtitulo,  Color.lightGray,    Color.lightGray);
                celda(col4[i],  table, cNormal,     Color.lightGray,    Color.lightGray);
            }

            doc.add(table);
        }
        catch (DocumentException ex) {
            System.out.println("error pdf:"+ex.toString());
                ex.printStackTrace();
        }
    }

    /**
     * Esta tabla sirve para listar la lista de los materiales de cada accion
     * de la oferta ya sean MANO DE OBRA, MATERIAL o OTROS.
     *
     * @param doc El documento donde se va a incluir la tabla (Variable tipo Document)
     * @param al ArrayList que contiene la informacion de cada elemento que va a ser ingresado en tabla
     * @param titulo Titulo que va a llevar la tabla
     * @return Devuelve el dinero total que da la tabla
     */
    public double tablaItems(Document doc, AccionesEca ae, ArrayList<MaterialEca> al, String titulo){
        try{
            float[] widths = {0.10f, 0.4f, 0.10f, 0.10f, 0.15f, 0.15f};

            double total = 0;
            double num = 0;

            MaterialEca ti;
            PdfPTable table;
            PdfPTable subt;

            table = new PdfPTable(widths);
            table.setWidthPercentage(100);
            subt = new PdfPTable(widths);
            subt.setWidthPercentage(100);

            //Titulo
            celda(titulo, table, tTitulo, Color.white, Color.black, 6);

            //Subtitulos
            celda("ITEM", table, tTitulo, new Color(173, 216, 230));
            celda("DESCRIPCI�N", table, tTitulo, new Color(173, 216, 230));
            celda("UNDS", table, tTitulo, new Color(173, 216, 230));
            celda("CANT", table, tTitulo, new Color(173, 216, 230));
            celda("VR UNITARIO", table, tTitulo, new Color(173, 216, 230));
            celda("VR TOTAL", table, tTitulo, new Color(173, 216, 230));

            //Valores
            for (int i = 0; i < al.size(); i++) {
                ti = al.get(i);

                num = Double.parseDouble(ti.getVlr_unitario());

                celda(i + 1, table, tNormal, Color.white, Color.black);
                celda(ti.getDescripcion(), table, tNormal, Color.white, Color.black);
                celda(ti.getUnidad(), table, tNormal, Color.white, Color.black);
                celda(ti.getCantidad(), table, tNormal, Color.white, Color.black);
                celda("$ " + Util.customFormat( Double.parseDouble(ti.getVlr_unitario()) ) , table, tNormal, Color.white, Color.black);
                celda("$ " + Util.customFormat( Double.parseDouble(ti.getVlr_total()) ) , table, tNormal, Color.white, Color.black);
                total += Double.parseDouble(ti.getVlr_total());
            }

            //Parte de subtotal
            celda(" ", table, tSubtitulo, Color.white, Color.white, true);
            celda("Subtotal", table, tSubtitulo, Color.white, Color.white, true);
            celda(" ", table, tSubtitulo, Color.white, Color.white, true);
            celda(" ", table, tSubtitulo, Color.white, Color.white, true);
            celda(" ", table, tSubtitulo, Color.white, Color.white, true);
            celda("$ " + Util.customFormat(total), table, tSubtitulo, Color.white, Color.white, true);

            doc.add(table);

            return total;

        }
        catch (DocumentException ex){
            System.out.println("error pdf:"+ex.toString());
                ex.printStackTrace();
            next="Error escribiendo la tabla de "+titulo+" en el archivo";
            return 0;
        }
    }

    /**
     * Tabla que devuelve los totales de cada acci�n.
     *
     * @param doc       El documento donde se va a incluir la tabla (Variable tipo Document).
     * @param aeca      Es el bean AccionesEca el cual contiene la informacion de la accion.
     * @param results   Un Array de tipo entero que tiene los resultados de MANO DE OBRA, MATERIALES y OTROS de la accion.
     */
    public void tablaTotales(Document doc, AccionesEca aeca, double[] results) throws Exception{
        float[] widths  = {0.5f, 0.5f};
        double total = 0;

        PdfPTable table;
        table = new PdfPTable(widths);
        table.setWidthPercentage(80);

        double numero = Double.parseDouble(aeca.getUtilidad());

        celda("COSTOS DIRECTOS", table, tSubtitulo, Color.white, Color.white);
        celda(  Util.customFormat( results[0]+results[1]+results[2] ),
                table, tSubtitulo, Color.white, Color.white); 

        //inicio de 20100223
        aeca.setAdministracion(""+(results[0]+results[1]+results[2])*Double.parseDouble(aeca.getPorc_administracion())/100.0);
        aeca.setImprevisto(""+(results[0]+results[1]+results[2])*Double.parseDouble(aeca.getPorc_imprevisto())/100.0);
        aeca.setUtilidad(""+(results[0]+results[1]+results[2])*Double.parseDouble(aeca.getPorc_utilidad())/100.0);
        //fin de 20100223

        if(numero > 0){
            celda("Administracion "+(int)Double.parseDouble(aeca.getPorc_administracion()), table, tSubtitulo, Color.white, Color.white);
            celda(  Util.customFormat( Double.parseDouble(aeca.getAdministracion())),
                    table, tSubtitulo, Color.white, Color.white);
            celda("Imprevistos "+(int)Double.parseDouble(aeca.getPorc_imprevisto())       , table, tSubtitulo, Color.white, Color.white);
            celda(  Util.customFormat( Double.parseDouble(aeca.getImprevisto())),
                    table, tSubtitulo, Color.white, Color.white);
            celda("Utilidad "+(int)Double.parseDouble(aeca.getPorc_utilidad())                 , table, tSubtitulo, Color.white, Color.white);
            celda(  Util.customFormat( Double.parseDouble(aeca.getUtilidad())),
                    table, tSubtitulo, Color.white, Color.white);
        }

        celda("IVA 16%", table, tSubtitulo, Color.white, Color.white);
        if((int)Double.parseDouble(aeca.getUtilidad()) > 0){
            celda(  Util.customFormat((Double.parseDouble(aeca.getUtilidad())*0.19)),
                    table, tSubtitulo, Color.white, Color.white);
        }
        else{
            celda(  Util.customFormat(((results[0]+results[1]+results[2])*0.19)),
                    table, tSubtitulo, Color.white, Color.white);
        }

        celda("VALOR TOTAL", table, tSubtitulo, Color.white, Color.white);
        if((int)Double.parseDouble(aeca.getUtilidad()) > 0){
            total = ((results[0]+results[1]+results[2]) +
                    Double.parseDouble(aeca.getAdministracion()) +
                    Double.parseDouble(aeca.getImprevisto()) +
                    Double.parseDouble(aeca.getUtilidad()) +
                    ( Double.parseDouble(aeca.getUtilidad()) *0.19));

        }
        else{
            total = ((results[0]+results[1]+results[2]) + ((results[0]+results[1]+results[2])*0.19));
        }
            float inc = epd.getIncremento(df.format(dat));
            //total = total * inc;//20100223

            totalOffer += total;

            celda(  Util.customFormat(total), table, tSubtitulo, Color.white, Color.white);
        try {
            doc.add(table);
        }
        catch (DocumentException ex) {
            System.out.println("error en pdf:"+ex.toString());
            ex.printStackTrace();
            next="Error escribiendo la tabla de totales en el archivo";
        }
    }

    /**
     * Este metodo arma la tabla de totales de todas las acciones.
     *
     * @param doc El documento donde se va a incluir la tabla (Variable tipo Document)
     */
    public void tablaTotalesGeneral(Document doc){
        float[] widths  = {1f};

        PdfPTable table;
        table = new PdfPTable(widths);
        table.setWidthPercentage(60);

        celda("TOTAL DE LA OFERTA", table, tTitulo, new Color(173, 216, 230));
        celda( "$ " + Util.customFormat(totalOffer) , table, tNormal, Color.white);

        try {
            doc.add(table);
            doc.add(new Paragraph(" ", normal));

            try {
                doc.add(new Paragraph(numToStr( (int)totalOffer ), normal));
            }
            catch (Exception ex) {
                System.out.println("error pdf:"+ex.toString());
                ex.printStackTrace();
                doc.add(new Paragraph("EL NUMERO NO SERA MOSTRADO POR SER DEMASIADO GRANDE", normal));
            }
        }
        catch (Exception ex) {
            System.out.println("error pdf:"+ex.toString());
            ex.printStackTrace();
            next="Error escribiendo la tabla de totales generales en el archivo";
        }
    }

    /**
     * Este m�todo arma la tabla de firmas que esta a lo ultimo del documento PDF.
     *
     * @param doc El documento donde se va a incluir la tabla (Variable tipo Document)
     */
    public void tablaFirmas(Document doc){

        PdfPTable table;
        table = new PdfPTable(2);
        table.setWidthPercentage(100);

        celda("ALBERTO CARBONELL MARTINEZ", table, cTitulo, Color.white, true, Element.ALIGN_LEFT);
        celda("_____________________________________", table, cTitulo, Color.white, true, Element.ALIGN_LEFT);
        celda("C.C. No. 72.187.584 de Barranquilla", table, cTitulo, Color.white, true, Element.ALIGN_LEFT);
        celda("Doc No. "+epd.getOferta().getNIT()+"", table, cTitulo, Color.white, true, Element.ALIGN_LEFT);
        celda("Representante", table, cTitulo, Color.white, true, Element.ALIGN_LEFT);
        celda("Representante Legal ("+epd.getOferta().getRepresentante()+")", table, cTitulo, Color.white, true, Element.ALIGN_LEFT);
        celda("", table, cTitulo, Color.white, true, Element.ALIGN_LEFT);
        celda("Nombre Propio (_____________________________)", table, cTitulo, Color.white, true, Element.ALIGN_LEFT);
        celda("", table, cTitulo, Color.white, true, Element.ALIGN_LEFT);
        celda("", table, cTitulo, Color.white, true, Element.ALIGN_LEFT);
        celda("CONSORCIO MULTISERVICIOS", table, cTitulo, Color.white, true, Element.ALIGN_LEFT);
        celda("El CLIENTE", table, cTitulo, Color.white, true, Element.ALIGN_LEFT);

        try {
            doc.add(table);
        }
        catch (DocumentException ex){
            System.out.println("error pdf:"+ex.toString());
            ex.printStackTrace();
            next="Error escribiendo la tabla de las firmas en el archivo";
        }
    }

    /**
     * Este metodo es para hacer celdas ya preparadas.
     *
     * @param img Esta es la imagen a colocar en la celda.
     * @param tab Tabla la cual va a recibir la celda.
     */
    public void celda(Image img, PdfPTable tab){
        PdfPCell cell;
        cell = new PdfPCell(img);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorderColor(Color.white);
        tab.addCell(cell);
    }

    /**
     * Este metodo es para hacer celdas ya preparadas.
     *
     * @param val       Valor a colocar en la celda
     * @param tab       Tabla la cual va a recibir la celda.
     * @param font      Tipo de letra para la celda.
     * @param backg     Color de fondo de la celda.
     */
    public void celda(Object val, PdfPTable tab, Font font, Color backg){
        PdfPCell cell;
        Paragraph p = new Paragraph( String.valueOf(val), font);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBackgroundColor(backg);
        tab.addCell(cell);
    }

    /**
     * Este metodo es para hacer celdas ya preparadas.
     *
     * @param val       Valor a colocar en la celda
     * @param tab       Tabla la cual va a recibir la celda.
     * @param font      Tipo de letra para la celda.
     * @param backg     Color de fondo de la celda.
     * @param border    Color del borde de la celda.
     */
    public void celda(Object val, PdfPTable tab, Font font, Color backg, Color border){
        PdfPCell cell;
        Paragraph p = new Paragraph( String.valueOf(val), font);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorderColor(border);
        cell.setBackgroundColor(backg);
        tab.addCell(cell);
    }

    /**
     * Este metodo es para hacer celdas ya preparadas.
     *
     * @param val       Valor a colocar en la celda.
     * @param tab       Tabla la cual va a recibir la celda.
     * @param font      Tipo de letra para la celda.
     * @param backg     Color de fondo de la celda.
     * @param disable   Deshabilita los bordes.
     * @param align     El alineamiento del texto en la celda.
     */
    public void celda(Object val, PdfPTable tab, Font font, Color backg, boolean disable, int align){
        PdfPCell cell;
        Paragraph p = new Paragraph( String.valueOf(val), font);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(align);

        if (disable = true){
            cell.disableBorderSide(PdfPCell.BOTTOM);
            cell.disableBorderSide(PdfPCell.TOP);
            cell.disableBorderSide(PdfPCell.LEFT);
            cell.disableBorderSide(PdfPCell.RIGHT);
        }

        cell.setBackgroundColor(backg);
        tab.addCell(cell);
    }

    /**
     * Este metodo es para hacer celdas ya preparadas.
     *
     * @param val       Valor a colocar en la celda.
     * @param tab       Tabla la cual va a recibir la celda.
     * @param font      Tipo de letra para la celda.
     * @param backg     Color de fondo de la celda.
     * @param border    Color de los bordes.
     * @param disable   Deshabilita los bordes.
     */
    public void celda(Object val, PdfPTable tab, Font font, Color backg, Color border, boolean disable){
        PdfPCell cell;
        Paragraph p = new Paragraph( String.valueOf(val), font);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);

        if (disable = true){
            cell.disableBorderSide(PdfPCell.BOTTOM);
            cell.disableBorderSide(PdfPCell.TOP);
            cell.disableBorderSide(PdfPCell.LEFT);
            cell.disableBorderSide(PdfPCell.RIGHT);
        }

        cell.setBorderColor(border);
        cell.setBackgroundColor(backg);
        tab.addCell(cell);
    }

    /**
     * Este metodo es para hacer celdas ya preparadas.
     *
     * @param val       Valor a colocar en la celda.
     * @param tab       Tabla la cual va a recibir la celda.
     * @param font      Tipo de letra para la celda.
     * @param backg     Color de fondo de la celda.
     * @param border    Color de los bordes.
     * @param Colspan   Ancho de la celda en columnas.
     */
    public void celda(Object val, PdfPTable tab, Font font, Color backg, Color border, int Colspan){
        PdfPCell cell;
        Paragraph p = new Paragraph( String.valueOf(val), font);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorderColor(border);
        cell.setColspan(Colspan);
        cell.setBackgroundColor(backg);
        tab.addCell(cell);
    }

    /**
     *
     * @param doc   El documento donde se va a incluir la tabla (Variable tipo Document).
     * @param val   Texto que se va a introducir en el parrafo.
     * @param align Alineamiento del texto.
     * @param letra Tipo de letra a utilizar en el texto.
     */
    public void parrafo(Document doc, String val, int align, Font letra){
        try {
            Paragraph p = new Paragraph(val, letra);
            p.setAlignment(align);
            document.add(p);
        }
        catch (DocumentException documentException) {
            System.out.println("error pdf:"+documentException.toString());
            documentException.printStackTrace();
            next="Error escribiendo un p�rrafo en el archivo";
        }
    }

    /**
     * Este metodo recoje un entero, le elimina los espacios en blanco
     * y me devuelve el numero en letras.
     *
     * @param val   Numero a convertir.
     * @return      Cadena con el numero convertido en palabras.
     */
    public String numToStr(int val){
        String[] texto = RMCantidadEnLetras.getTexto(val);
        String res = "";

        for (int i = 0; i < texto.length; i++) {
            res += texto[i]+" ";
        }

        return res.trim();
    }

    /**
     * Este metodo recoje un entero, le elimina los espacios en blanco
     * y me devuelve el numero en letras.
     *
     * @param val   Numero a convertir.
     * @return      Cadena con el numero convertido en palabras.
     */
    public String numToStr(double val){
        String[] texto = RMCantidadEnLetras.getTexto(val);
        String res = "";

        for (int i = 0; i < texto.length; i++) {
            res += texto[i]+" ";
        }

        return res.trim();
    }

    /**
     * Este metodo cierra el documento principal donde
     * se esta ingresando la informacion.
     */
    public void cerrar(){
        document.close();
    }

    /* Los procedimientos siguientes son para
     * la actualizacion de las ofertas
     */

    public String getMesesOficial() throws Exception{
        return epd.getMesesOficial();
    }

    /**
     * Este metodo me actualiza la informacion principal
     * de la oferta. Antes de actualizar se revisa si se
     * tiene las acciones en estado 040.
     *
     * @param oeca Es el bean de la oferta a modificar.
     * @param flag Indica si se va a reemplazar el consecutivo.
     */
    public void actualizarOferta(Usuario u, OfertaElca oelca, boolean flag){
        try {
            epd.actualizarOferta(u.getLogin(), oelca, flag);
            next="Oferta generada exitosamente";
        }
        catch (Exception ex) {
            System.out.println("error pdf:"+ex.toString());
            ex.printStackTrace();
            next="Error generando la oferta";
        }
    }

    /**
     * Este metodo indica si la solicitud esta
     * preparada para generarse como oferta
     *
     * @param id_sol    ID de la oferta.
     * @return          Devuelve un bean tipo OfertaEca.
     */
    public boolean isOfferReady(String id_acc) throws Exception{
        return epd.isOfferReady(id_acc);
    }

    /**
     * Este metodo devuelve un bean con la info
     * de la oferta deseada.
     *
     * @param num   Consecutivo de la oferta.
     * @return      Devuelve un bean tipo OfertaEca
     */
    public DatosOferta ofertaInfo(String num) throws Exception{
        epd.setNumOferta(num);
        epd.getEcaInfo();
        return epd.getOferta();
    }

    /**
     * Este metodo devuelve un String con la ruta
     * a la cual la pagina va a ser destinada automaticamente.
     *
     * @return String con la ruta proxima.
     */
    public String returnNext(){
        return next;
    }

    /**
     * Este metodo devuelve todas las consideraciones disponibles
     *
     * @return ArrayList con las consideraciones en beans.
     * @throws Exception
     */
    public ArrayList<TablaGen> getConsideracionesItems() throws Exception{
        return epd.getConsideraciones();
    }

    /**
     * Este metodo es para devolver las
     * consideraciones por oferta
     *
     * @return ArrayList con las consideraciones de las ofertas
     * @throws Exception
     */
    public ArrayList<TablaGen> getConsideracionesByOffer() throws Exception{
        return epd.getConsideracionesByOffer();
    }


    public String actualizarAnulacionOferta(OfertaElca oelca, boolean flag, String userx){
        String respuesta="Denegacion pendiente...";
        try {
            ////.out.println("antes isAnulable ");
            if(epd.isAnulable(oelca.getId_solicitud())){
                respuesta=epd.actualizarAnulacionOferta(oelca, flag,userx);
                //next="Oferta anulada exitosamente";
            }else{
                respuesta="Las acciones no estan en estado para denegar.";//091222
            }
        }
        catch (Exception ex) {
            System.out.println("error pdf:"+ex.toString());
            ex.printStackTrace();
            System.out.println("error anulando la oferta___"+ex.toString());
        }
        return respuesta;
    }

    public DatosOferta ofertaInfoAnul(String num) throws Exception{

        return epd.ofertaInfoAnul(num);
    }

    public ArrayList<TablaGen> getAnulacionesItems(String num_of) throws Exception{
        return epd.getAnulaciones(num_of);
    }
}