/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.services;



import com.tsp.operation.model.beans.*;
import java.util.*;

import java.sql.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.Model;
import com.tsp.util.Util;
import java.text.*;
import java.util.*;
import java.lang.*;



/**
 *
 * @author Alvaro
 */
public class ProntoPagoService {

    private ProntoPagoDAO prontoPagoDao;


    /** Creates a new instance of ProntoPagoService */
    public ProntoPagoService() {
        prontoPagoDao = new ProntoPagoDAO();
    }
    public ProntoPagoService(String dataBaseName) {
        prontoPagoDao = new ProntoPagoDAO(dataBaseName);
    }


    public void buscaProntoPago()throws SQLException{
        prontoPagoDao.buscaProntoPago();
    }


    public List getProntoPago(){
       return prontoPagoDao.getProntoPago();
    }


    public void buscaProntoPagoEfectivo()throws SQLException{
        prontoPagoDao.buscaProntoPagoEfectivo();
    }


    public List getProntoPagoEfectivo(){
       return prontoPagoDao.getProntoPagoEfectivo();
    }

    public void buscaProntoPagoDetalle(String secuencia)throws SQLException{
        prontoPagoDao.buscaProntoPagoDetalle(secuencia);
    }


    public List getProntoPagoDetalle(){
       return prontoPagoDao.getProntoPagoDetalle();
    }


    public TotalExtracto getTotalExtracto(String secuencia)throws SQLException{
        return prontoPagoDao.get_TotalExtracto(secuencia);
    }



   public Vector getBanco(String cuenta_transferencia, String banco_transferencia, String tcta_transferencia )throws SQLException{

       String cuentaCorriente            = "%" + cuenta_transferencia + "%";
       String tipoCuenta                 = "%" + tcta_transferencia   + "%";
       String cuentaCorriente_tipoCuenta = cuenta_transferencia + "," + tcta_transferencia;

       return prontoPagoDao.getBanco(cuenta_transferencia,  banco_transferencia, tcta_transferencia,
                                     cuentaCorriente, tipoCuenta, cuentaCorriente_tipoCuenta);
   }


   public Vector getBancoAnticipo(String agencia )throws SQLException{

      return prontoPagoDao.getBancoAnticipo(agencia);
   }



   public Cmc getCmc(String dstrct, String tipodoc, String codigoCmc)throws SQLException{
       return prontoPagoDao.getCmc(dstrct, tipodoc, codigoCmc);
   }

    public String setOrdenServicio(ProntoPago prontoPago,String dstrct, String user, String creation_date)throws SQLException{
        return prontoPagoDao.setOrdenServicio(prontoPago, dstrct, user, creation_date);
    }

    public int getGrupoTransaccion()throws SQLException{
        return prontoPagoDao.getGrupoTransaccion();

    }


    public int getTransaccion()throws SQLException{
        return prontoPagoDao.getTransaccion();

    }


    public String setAnticipo(ProntoPago prontoPago,String dstrct, String creation_date)throws SQLException{
        return prontoPagoDao.setAnticipo(prontoPago, dstrct, creation_date);

    }

    public String setOrdenServicioDetalle(ProntoPago prontoPago,ProntoPagoDetalle prontoPagoDetalle,
                                          String dstrct, String user,String creation_date)throws SQLException{
        return prontoPagoDao.setOrdenServicioDetalle(prontoPago, prontoPagoDetalle, dstrct, user, creation_date);

    }

    public String setComprobante(Comprobante comprobante)throws SQLException{
        return prontoPagoDao.setComprobante(comprobante);
    }

    public String setComprobanteDetalle(ComprobanteDetalle comprobanteDetalle)throws SQLException{
        return prontoPagoDao.setComprobanteDetalle(comprobanteDetalle);
    }



    public List getOrdenServicio( )throws SQLException{
        return prontoPagoDao.getOrdenServicio();
    }


    public List getOrdenServicioDetalle(String distrito, String tipo_operacion, String numero_operacion )throws SQLException{
        return prontoPagoDao.getOrdenServicioDetalle(distrito, tipo_operacion, numero_operacion);
    }

    public String setEgreso(EgresoCabecera egreso)throws SQLException{
        return prontoPagoDao.setEgreso(egreso);
    }

    public String setEgresoDetalle(EgresoDetalle egresoDetalle)throws SQLException{
        return prontoPagoDao.setEgresoDetalle(egresoDetalle);
    }



    public String setFechaEgreso(String distrito, String tipo_operacion, String numero_operacion,
                                 String egreso, String fecha_egreso)throws SQLException{
        return prontoPagoDao.setFechaEgreso(distrito, tipo_operacion, numero_operacion, egreso, fecha_egreso);
    }


    public String setSaldos(double vlr_cheque, String cheque, String distrito, String proveedor,
                            String tipo_documento, String documento) throws SQLException{
        return prontoPagoDao.setSaldos(vlr_cheque, cheque, distrito, proveedor, tipo_documento, documento);
    }

    public String buscarDocumento(String  distrito,String  proveedor,String  tipo_documento, String  secuencia) throws SQLException {
        String numeroFactura = "";
        try {

            String documento = "";
            int inicio = 0;
            String consecutivo = "";


            // BUSCAR PRIMERO CON RAYITA ABAJO
            documento = prontoPagoDao.buscarDocumento (distrito, proveedor, tipo_documento,  secuencia);
             System.out.println("[documento_cxp_"+documento+"]");
            if (documento.equals("")){
                // NO EXISTE CON RAYITA ABAJO

                // BUSCAR DE SEGUNDO SI EXISTE CON EL NUMERO DE SECUENCIA
                documento = prontoPagoDao.buscarDocumentoUnico (distrito, proveedor, tipo_documento,  secuencia);
                if(documento.equals("")){
                    // NO EXISTE CON RAYITA ABAJO NI CON EL NUMERO ORIGINAL
                    numeroFactura = secuencia;
                }
                else {
                    // EXISTE CON EL NUMERO ORIGINAL
                    numeroFactura = secuencia + "_1";
                }
            }


            //  EXISTE CON RAYITA ABAJO
            else {
                int posicion = documento.lastIndexOf("_");
                if (posicion>0){
                    System.out.println("[posicion_cxp"+posicion+"]");
                    inicio = posicion + 1;
                    consecutivo = documento.substring(inicio);
                    System.out.println("[consecutivo_cxp"+consecutivo+"]");
                    int numero = Integer.parseInt(consecutivo);
                    System.out.println("[numero__cxp"+numero+"]");
                    numero = numero + 1;
                    System.out.println("[numero_cxp+1"+numero+"]");
                    String nuevoNumero =  Integer.toString(inicio);
                    numeroFactura = documento.substring(0,inicio)+numero;
                }
                else {
                    numeroFactura = secuencia + "_1";
                }
            }
            System.out.println("[numero Final_"+numeroFactura+"]");
            return numeroFactura;

        }catch(Exception e){
            System.out.println("buscarDocumento"+e.getMessage());
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL NUMERO DE LA FACTURA. \n " + e.getMessage());
        }finally{
            if(numeroFactura.equals(""))
                return secuencia;
        }


    }

    public String buscarDocumentoOS(String  distrito, String  tipo_operacion, String  numero_operacion) throws SQLException {
        String numeroOS = "";
        try {

            String documento = "";
            int inicio = 0;
            String consecutivo = "";
            documento = prontoPagoDao.buscarDocumentoOS (distrito, tipo_operacion, numero_operacion);
            System.out.println("[documento_"+documento+"]");
            if (documento.equals("")){
                numeroOS = numero_operacion;
            }
            else {
                int posicion = documento.lastIndexOf("_");
                System.out.println("[posicion_"+posicion+"]");
                if (posicion>0){
                    inicio = posicion + 1;
                    consecutivo = documento.substring(inicio);
                    System.out.println("[consecutivo_"+consecutivo+"]");
                    int numero = Integer.parseInt(consecutivo);
                    System.out.println("[numero_"+numero+"]");
                    numero = numero + 1;
                    String nuevoNumero =  Integer.toString(inicio);
                    System.out.println("[nuevoNumero"+nuevoNumero+"]");
                    numeroOS = documento.substring(0,inicio)+numero;
                }
                else {
                    numeroOS = numero_operacion + "_1";
                }
            }
            System.out.println("[numero Final_"+numeroOS+"]");
            return numeroOS;

        }catch(Exception e){
            System.out.println("buscarDocumentoOS"+e.getMessage());
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL NUMERO DE LA ORDEN DE SERVICIO. \n " + e.getMessage());
        }finally{
            if(numeroOS.equals(""))
                return numero_operacion;
        }
    }

    public String buscarDocumentoEgreso(String  distrito,String  banco,String  sucursal, String  secuencia) throws SQLException {
        String numeroFactura = "";
        try {

            String documento = "";
            int inicio = 0;
            String consecutivo = "";
            documento = prontoPagoDao.buscarDocumentoEgreso (distrito, banco, sucursal,  secuencia);
            if (documento.equals("")){
                numeroFactura = secuencia;
            }
            else {
                String temp[] = documento.split("_");

                if(temp.length <= 2){
                    numeroFactura = secuencia + "_1";
                }else{
                    int numero = Integer.parseInt(temp[2]);
                    numero = numero + 1;
                    numeroFactura = temp[0] + "_" + temp[1] +"_" + numero;
                }
            }
            return numeroFactura;

        }catch(Exception e){
            System.out.println("buscarDocumentoEgreso"+e.getMessage());
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL NUMERO DEL EGRESO. \n " + e.getMessage());
        }


    }

    public String  setFacturaCxC(String factura_cxc, String distrito, String tipo_operacion, String numero_operacion, String tipo_documento, String documento )throws SQLException{

        return prontoPagoDao.setFacturaCxC(factura_cxc, distrito, tipo_operacion, numero_operacion, tipo_documento, documento);

    }


     public String setCxp_doc (String dstrct,String proveedor, String tipo_documento,String documento,
                              String descripcion,String agencia,
                              String handle_code,String aprobador,String usuario_aprobacion,
                              String banco,String sucursal,String moneda, double vlr_neto,
                              double vlr_total_abonos,double vlr_saldo,double vlr_neto_me,double vlr_total_abonos_me,
                              double vlr_saldo_me, double tasa,String observacion,String user_update,
                              String creation_user,String base,String clase_documento,String moneda_banco,
                              String fecha_documento,String fecha_vencimiento,String clase_documento_rel,
                              String last_update, String creation_date, String fecha_aprobacion)throws SQLException{

        return prontoPagoDao.setCxp_doc(dstrct, proveedor, tipo_documento, documento, descripcion,
                                    agencia, handle_code, aprobador, usuario_aprobacion, banco, sucursal,
                                    moneda, vlr_neto, vlr_total_abonos, vlr_saldo, vlr_neto_me,
                                    vlr_total_abonos_me, vlr_saldo_me, tasa, observacion, user_update,
                                    creation_user, base, clase_documento, moneda_banco,
                                    fecha_documento, fecha_vencimiento, clase_documento_rel,
                                    last_update, creation_date, fecha_aprobacion);
    }


  public List getEgresoTsp(String distrito )throws SQLException{
        return prontoPagoDao.getEgresoTsp(distrito);
    }

    public List getEgresoDetalleTsp(String distrito, String branch_code,  String bank_account_no,  String document_no )throws SQLException{
        return prontoPagoDao.getEgresoDetalleTsp(distrito, branch_code,  bank_account_no, document_no);
    }

    public List getItem(String dstrct, String proveedor,String tipo_documento,String documento  )throws SQLException{
        return prontoPagoDao.getItem( dstrct,  proveedor, tipo_documento, documento );
    }

    public List getEgresoItem(String dstrct, String branch_code, String bank_account_no, String document_no  )throws SQLException{
        return prontoPagoDao.getEgresoItem( dstrct,  branch_code, bank_account_no, document_no );
    }

    public void creaFacturaFintra() throws SQLException {
        prontoPagoDao.creaFacturaFintra();
    }



    public FacturaFintra getFacturaFintra(String tipo_factura, String factura,
                                          String tipo_operacion,
                                          String numero_operacion,
                                          double valor_item,
                                          String numero_query )throws SQLException{
        return prontoPagoDao.getFacturaFintra(tipo_factura, factura, tipo_operacion, numero_operacion, valor_item, numero_query);
    }


    public List getListaFacturaFintra()throws SQLException{
        return prontoPagoDao.getListaFacturaFintra();
    }





    public void actualizaFacturaFintra(String dstrct_egreso, String branch_code_egreso, String bank_account_no_egreso,
                                       String document_no_egreso, String item_no_egreso,
                                       String dstrct, String tipo_documento, String documento,
                                       int item)throws SQLException{

        prontoPagoDao.actualizaFacturaFintra( dstrct_egreso,  branch_code_egreso,  bank_account_no_egreso,
                                        document_no_egreso,  item_no_egreso,
                                        dstrct,  tipo_documento,  documento,
                                        item ) ;
    }



    public String setEgresodetTsp ( String dstrct_factura, String tipo_documento_factura,
                                    String documento_factura, int item_factura,

                                    String dstrct_ingreso,
                                    String tipo_documento_ingreso,
                                    String num_ingreso,
                                    int item_ingreso,
                                    String dstrct , String branch_code,
                                    String bank_account_no, String document_no, String item_no)throws SQLException{

        return prontoPagoDao.setEgresodetTsp(dstrct_factura, tipo_documento_factura, documento_factura, item_factura,
                                             dstrct_ingreso, tipo_documento_ingreso, num_ingreso, item_ingreso,
                                             dstrct, branch_code, bank_account_no, document_no, item_no);

    }


    public String setIngreso (
            String reg_status, String dstrct, String tipo_documento, String num_ingreso, String codcli, String nitcli, String
            concepto, String tipo_ingreso, String fecha_consignacion, String fecha_ingreso, String branch_code, String
            bank_account_no, String codmoneda, String agencia_ingreso, String descripcion_ingreso, String
            periodo, double vlr_ingreso, double vlr_ingreso_me, double vlr_tasa, String fecha_tasa, int cant_item, int
            transaccion, int transaccion_anulacion, String fecha_impresion, String fecha_contabilizacion, String
            fecha_anulacion_contabilizacion, String fecha_anulacion, String creation_user, String
            creation_date, String user_update, String last_update, String base, String nro_consignacion, String
            periodo_anulacion, String cuenta, String auxiliar, String abc, double tasa_dol_bol, double saldo_ingreso, String
            cmc, String corficolombiana) throws SQLException {

        return prontoPagoDao.setIngreso(reg_status, dstrct, tipo_documento, num_ingreso, codcli, nitcli,
            concepto, tipo_ingreso, fecha_consignacion, fecha_ingreso, branch_code,
            bank_account_no, codmoneda, agencia_ingreso, descripcion_ingreso,
            periodo, vlr_ingreso, vlr_ingreso_me, vlr_tasa, fecha_tasa, cant_item,
            transaccion, transaccion_anulacion, fecha_impresion, fecha_contabilizacion,
            fecha_anulacion_contabilizacion, fecha_anulacion, creation_user,
            creation_date, user_update, last_update, base, nro_consignacion,
            periodo_anulacion, cuenta, auxiliar, abc, tasa_dol_bol, saldo_ingreso,
            cmc, corficolombiana);
    }





    public String setIngresoDetalle (
                        String reg_status, String dstrct, String tipo_documento, String num_ingreso, int item, String nitcli, double
                        valor_ingreso, double valor_ingreso_me, String factura, String fecha_factura, String codigo_retefuente, double
                        valor_retefuente, double valor_retefuente_me, String tipo_doc, String documento, String codigo_reteica, double
                        valor_reteica, double valor_reteica_me, double valor_diferencia_tasa, String creation_user, String
                        creation_date, String user_update, String last_update, String base, String cuenta, String auxiliar, String
                        fecha_contabilizacion, String fecha_anulacion_contabilizacion, String periodo, String
                        fecha_anulacion, String periodo_anulacion, int transaccion, int transaccion_anulacion,
                        String descripcion, double valor_tasa, double saldo_factura) throws SQLException {

        return prontoPagoDao.setIngresoDetalle(reg_status, dstrct, tipo_documento, num_ingreso, item, nitcli,
            valor_ingreso, valor_ingreso_me, factura, fecha_factura, codigo_retefuente,
            valor_retefuente, valor_retefuente_me, tipo_doc, documento, codigo_reteica,
            valor_reteica, valor_reteica_me, valor_diferencia_tasa, creation_user,
            creation_date, user_update, last_update, base, cuenta, auxiliar,
            fecha_contabilizacion, fecha_anulacion_contabilizacion, periodo,
            fecha_anulacion, periodo_anulacion, transaccion, transaccion_anulacion,
            descripcion,  valor_tasa, saldo_factura) ;
    }


    public String setEgresoTsp ( String tipo_documento_ingreso, String num_ingreso,
                                    String fecha_creacion_ingreso,
                                    String dstrct , String branch_code,
                                    String bank_account_no, String document_no)throws SQLException{
        return prontoPagoDao.setEgresoTsp(tipo_documento_ingreso, num_ingreso, fecha_creacion_ingreso,
                                          dstrct, branch_code, bank_account_no, document_no);
    }


   public String getCuentaTipoOperacion(String tipo_operacion)throws SQLException{
       return prontoPagoDao.getCuentaTipoOperacion(tipo_operacion);
   }


    public String setFactura ( double valor_ingreso, String dstrct, String tipo_documento, String documento,
                               String dstrct_ultimo_ingreso, String tipo_documento_ultimo_ingreso,
                               String num_ingreso_ultimo_ingreso, int item_ultimo_ingreso) throws SQLException{
        return prontoPagoDao.setFactura(valor_ingreso, dstrct, tipo_documento, documento,
                                dstrct_ultimo_ingreso,  tipo_documento_ultimo_ingreso,
                                num_ingreso_ultimo_ingreso,  item_ultimo_ingreso);
    }



    public String setFacturaDetalle ( double valor_ingreso, String dstrct, String tipo_documento, String documento, int item,
                                      String dstrct_ultimo_ingreso, String tipo_documento_ultimo_ingreso,
                                      String num_ingreso_ultimo_ingreso, int item_ultimo_ingreso) throws SQLException{
        return prontoPagoDao.setFacturaDetalle(valor_ingreso, dstrct, tipo_documento, documento, item,
                                dstrct_ultimo_ingreso,  tipo_documento_ultimo_ingreso,
                                num_ingreso_ultimo_ingreso,  item_ultimo_ingreso);
    }


    public void buscaProntoPagoPrestamo()throws SQLException{
        prontoPagoDao.buscaProntoPagoPrestamo();
    }





}




