/****************************************************************************************************
 * Nombre clase: Ingreso_especialService.java                                                       *
 * Descripci�n: Clase que maneja los servicios al model relacionados con los ingresos especiales.   *
 * Autor: Ing. Jose de la rosa                                                                      *
 * Fecha: 6 de diciembre de 2005, 11:14 AM                                                          *
 * Versi�n: Java 1.0                                                                                *
 * Copyright: Fintravalores S.A. S.A.                                                          *
 ***************************************************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

public class Ingreso_especialService {
    private Ingreso_especialDAO ingreso_especial;
    /** Creates a new instance of ingreso_especialService */
    public Ingreso_especialService () {
        ingreso_especial = new Ingreso_especialDAO();
    }
    /**
     * Metodo: getIngreso_especial, permite retornar un objeto de registros de Ingreso especial.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */    
    public Ingreso_especial getIngreso_especial( )throws SQLException{
        return ingreso_especial.getIngreso_especial();
    }
    
    /**
     * Metodo: getIngreso_especiales, permite retornar un vector de registros de Ingreso especial.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */    
    public Vector getIngreso_especiales()throws SQLException{
        return ingreso_especial.getIngreso_especiales();
    }
    
    /**
     * Metodo: setIngreso_especial, permite obtener un objeto de registros de Ingreso especial.
     * @autor : Ing. Jose de la rosa
     * @param : objeto
     * @version : 1.0
     */
    public void setIngreso_especial (Ingreso_especial ingreso)throws SQLException {
        ingreso_especial.setIngreso_especial(ingreso);
    }
    
    /**
     * Metodo: setIngreso_especiales, permite obtener un vector de registros de Ingreso especial.
     * @autor : Ing. Jose de la rosa
     * @param : vector
     * @version : 1.0
     */    
    public void setIngreso_especiales(Vector ingreso_especiales)throws SQLException{
        ingreso_especial.setIngreso_especiales(ingreso_especiales);
    }
    
    /**
    * Metodo insertIngreso_especial, ingresa los Ingresos especiales (Ingreso_especial).
    * @autor : Ing. Jose de la rosa
    * @see insertarIngreso_especial - Ingreso_especialDAO
    * @param : Ingreso_especial
    * @version : 1.0
    */    
    public void insertIngreso_especial(Ingreso_especial user) throws SQLException{
        try{
            ingreso_especial.setIngreso_especial(user);
            ingreso_especial.insertIngreso_especial();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }    
    
    /**
    * Metodo existIngreso_especial, obtiene la informacion de los Ingresos especiales dado unos parametros
    * (standar, codigo).
    * @autor : Ing. Jose de la rosa
    * @see existIngreso_especial - Ingreso_especialDAO
    * @param : nit, propietario y placa
    * @version : 1.0
    */       
    public boolean existIngreso_especial(String tipo, String concepto, String clase, String distrito)throws SQLException{
        try{
            return ingreso_especial.existIngreso_especial(tipo, concepto, clase, distrito);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
    * Metodo searchIngreso_especial, permite buscar un Ingreso especial dado unos parametros
    * (standar, codigo).
    * @autor : Ing. Jose de la rosa
    * @see searchIngreso_especial - Ingreso_especialDAO
    * @param : numero del standar y el codigo de concepto
    * @version : 1.0
    */      
    public void searchIngreso_especial(String tipo, String concepto, String clase, String distrito)throws SQLException{
        try{        
            ingreso_especial.searchIngreso_especial(tipo, concepto, clase, distrito);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
    * Metodo listIngreso_especial, lista la informacion de los Ingresos especiales
    * @autor : Ing. Jose de la rosa
    * @see listIngreso_especial - Ingreso_especialDAO
    * @param :
    * @version : 1.0
    */    
    public void listIngreso_especial(String distrito)throws SQLException{
        try{         
            ingreso_especial.listIngreso_especial(distrito);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
    * Metodo updateIngreso_especial, permite actualizar un Ingreso especial dados unos parametros
    * @autor : Ing. Jose de la rosa
    * @see updateIngreso_especial - Ingreso_especialDAO
    * @param : 
    * @version : 1.0
    */     
    public void updateIngreso_especial(Ingreso_especial res)throws SQLException{
        try{       
            ingreso_especial.setIngreso_especial(res);
            ingreso_especial.updateIngreso_especial();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }

    /**
    * Metodo anularIngreso_especial, permite anular un Ingreso especial de la bd
    * (Ingreso_especial).
    * @autor : Ing. Jose de la rosa
    * @see anularIngreso_especial - Ingreso_especialDAO
    * @param : Ingreso_especial
    * @version : 1.0
    */      
    public void anularIngreso_especial(Ingreso_especial res)throws SQLException{
        try{
            ingreso_especial.setIngreso_especial(res);
            ingreso_especial.anularIngreso_especial();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo: searchDetalleIngreso_especial, permite listar los Ingresos especiales por detalles dados unos parametros.
     * (standar, codigo).
     * @autor : Ing. Jose de la rosa
     * @see anularIngreso_especial - Ingreso_especialDAO
     * @param : numero del standar, el codigo de concepto y clase de equipo.
     * @version : 1.0
     */       
    public void searchDetalleIngreso_especial(String tipo, String concepto, String clase, String distrito) throws SQLException{
        try{
            ingreso_especial.searchDetalleIngreso_especial(tipo, concepto, clase, distrito);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
}
