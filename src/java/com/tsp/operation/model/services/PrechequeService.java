/***********************************************************************************
 * Nombre clase : ............... PrechequeService.java                            *
 * Descripcion :................. Clase que instancia los metodos de PrechequeDAO  *
 *                                con la BD.                                       *
 * Autor :....................... Ing. Andr�s Maturana De La Cruz                  *
 * Fecha :....................... 24 de febrero de 2007                            *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/
package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.sql.*;
import java.io.*;

/**
 *
 * @author  Andres
 */
public class PrechequeService {
    
    private PrechequeDAO dao;
    
    /** Creates a new instance of PrechequeService */
    public PrechequeService() {
        this.dao = new PrechequeDAO();
    }
    public PrechequeService(String dataBaseName) {
        this.dao = new PrechequeDAO(dataBaseName);
    }
    
  /*
   * M�todo que obtiene la cabecera de un precheque
   * @autor Andr�s Maturana De La Cruz
   * @version 1.0
   */
    public void obtenerCabeceraPrecheque() throws Exception{
        this.dao.obtenerCabeceraPrecheque();
    }
    
    /**
     * Getter for property egreso.
     * @return Value of property egreso.
     */
    public Egreso getEgreso() {
        return this.dao.getEgreso();
    }
    
    /**
     * Setter for property egreso.
     * @param egreso New value of property egreso.
     */
    public void setEgreso(Egreso egreso) {
        this.dao.setEgreso(egreso);
    }
    
    /**
     * Getter for property egresos.
     * @return Value of property egresos.
     */
    public java.util.Vector getEgresos() {
        return this.dao.getEgresos();
    }
    
    /**
     * Setter for property egresos.
     * @param egresos New value of property egresos.
     */
    public void setEgresos(java.util.Vector egresos) {
        this.dao.setEgresos(egresos);
    }
    
    /*     
     * M�todo que obtiene el detalle de un egreso
     * @autor Ing. Andr�s Maturana De La Cruz
     * @version 1.0
     */
    public void obtenerDetallePrecheque() throws Exception{
        this.dao.obtenerDetallePrecheque();
    }
    
     public void consultaPrecheque(
            String serial,
            String factura,
            String nit,
            String nitbe,
            String agc,
            String banco,
            String sucursal,
            String usercre,
            String fechai,
            String fechaf,
            String dstrct,
            String estado) throws Exception{
                
        this.dao.consultaPrecheque(serial, factura, nit, nitbe, agc, banco, sucursal, usercre, fechai, fechaf, dstrct, estado);
    }
        
}
