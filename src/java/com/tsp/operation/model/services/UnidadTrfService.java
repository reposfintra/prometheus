/*
 * Nombre        UnidadService.java
 * Autor         Ing Jose de la Rosa
 * Modificado    Ing Sandra Escalante
 * Fecha         26 de junio de 2005, 11:15 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

public class UnidadTrfService {
    
    private UnidadDAO unidad;
    private Vector uvol = new Vector();
    private Vector udst = new Vector();
    private Vector utmp = new Vector();
    private Vector upeso = new Vector();
    
    /** Creates a new instance of UnidadService */
    public UnidadTrfService() {
        unidad = new UnidadDAO();
    }
    
    /**
     * Getter for property unidad
     * @return Value of property unidad
     */
    public Unidad getUnidad( )throws SQLException{
        return unidad.getUnidad();
    }
    
    /**
     * Getter for property unidades
     * @return Value of property unidades
     */
    public Vector getUnidades() {
        return unidad.getUnidades();
    }
    
    /**
     * Setter for property unidades
     * @param base New value of property unidades
     */
    public void setUnidades(Vector Unidades) {
        unidad.setUnidades(Unidades);
    }
    
    /**
     * Metodo <tt>insertUnidad</tt>, registra una unidad
     * @autor : Ing. Jose de la Rosa
     * @see setUnidad, insertUnidad
     * @version : 1.0
     */
    public void insertUnidad(Unidad user) throws SQLException{
        try{
            unidad.setUnidad(user);
            unidad.insertUnidad();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo <tt>existUnidad</tt>, verifica la existencia de  una unidad en la tabla
     * @autor : Ing. Jose de la Rosa
     * @see existUnidad
     * @return boolean
     * @version : 1.0
     */
    public boolean existUnidad(String cod) throws SQLException{
        return unidad.existUnidad(cod);
    }
    
    /**
     * Metodo <tt>searchUnidad</tt>, registra una unidad
     * @autor : Ing. Jose de la Rosa
     * @see serchUnidad
     * @param codigo y tipo de unidad (String)
     * @version : 1.0
     */
    public void serchUnidad(String cod, String tipo)throws SQLException{
        unidad.searchUnidad(cod, tipo);
    }
    
    /**
     * Metodo <tt>listUnidad</tt>, obtiene las unidades registradas
     * @autor : Ing. Jose de la Rosa
     * @see listUnidad
     * @version : 1.0
     */
    public void listUnidad()throws SQLException{
        unidad.listUnidad();
    }
    
    /**
     * Metodo <tt>updateUnidad</tt>, modifica la informacion de una unidad
     * @autor : Ing. Jose de la Rosa
     * @see updateUnidad
     * @param codigo, descripcion, usuario modificacion, tipo de unidad, base, antiguo tipo de unidad (String)
     * @version : 1.0
     */
    public void updateUnidad(String cod,String desc,String usu, String tipo, String base, String atipo)throws SQLException{
        unidad.updateUnidad(cod, desc, usu, tipo, base, atipo);
    }
    
    /**
     * Metodo <tt>anularUnidad</tt>, elimina una unidad de la tabla
     * @autor : Ing. Jose de la Rosa
     * @see setUnidad, anularUnidad
     * @version : 1.0
     */
    public void anularUnidad(Unidad unit)throws SQLException{
        try{
            unidad.setUnidad(unit);
            unidad.anularUnidad();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo <tt>searchDetalleUnidades</tt>, busca una unidad dado unos parametros
     * @autor : Ing. Jose de la Rosa
     * @see searchDetalleUnidades
     * @param codigo, descripcion y tipo de unidad
     * @version : 1.0
     */
    public void searchDetalleUnidades(String cod, String desc, String tipo) throws SQLException{
        unidad.searchDetalleUnidades(cod, desc, tipo);
    }
    
    /**
     * Metodo <tt>getDetalleUnidades</tt>, obtiene el valor del vector unidades
     * @autor : Ing. Jose de la Rosa
     * @see getDetalleUnidades
     * @return Vector
     * @version : 1.0
     */
    public Vector getDetalleUnidades() throws SQLException{
        return unidad.getDetalleUnidades();
    }
    
    /**
     * Metodo <tt>listarUnidades</tt>, obtiene las unidades registradas
     * @autor : Ing. Jose de la Rosa
     * @see listarUnidades
     * @return Vector
     * @version : 1.0
     */
    public Vector listarUnidades() throws SQLException{
        try{
            return unidad.listarUnidades();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo <tt>listarUnidadesxTipo</tt>, obtiene las unidades segun el tipo
     * @autor : Ing. Sandra Escalante
     * @see listarUnidadesxTipo
     * @version : 1.0
     */
    public void listarUnidadesxTipo( String tipo)throws SQLException{
        try{
            unidad.listarUnidadesxTipo(tipo);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo: listarUnidadesxVolumen, obtiene las unidades de volumen
     * @autor : Ing. Sandra Escalante
     * @see listarUnidadesxTipo, setUvol
     * @version : 1.0
     */
    public void listarUnidadesxVolumen(String tipo)throws SQLException{
        try{
            unidad.listarUnidadesxTipo("V");
            this.setUvol(unidad.getUnidades());
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo: listarUnidadesxTiempo, obtiene las unidades de tiempo
     * @autor : Ing. Sandra Escalante
     * @see listarUnidadesxTipo, setUtmp
     * @version : 1.0
     */
    public void listarUnidadesxTiempo(String tipo)throws SQLException{
        try{
            unidad.listarUnidadesxTipo("T");
            this.setUtmp(unidad.getUnidades());
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo: listarUnidadesxDistancia, obtiene las unidades de distancia
     * @autor : Ing. Sandra Escalante
     * @see listarUnidadesxTipo, setUdst
     * @version : 1.0
     */
    public void listarUnidadesxDistancia(String tipo)throws SQLException{
        try{
            unidad.listarUnidadesxTipo("D");
            this.setUdst(unidad.getUnidades());
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo: listarUnidadesxPeso, obtiene las unidades de peso
     * @autor : Ing. Sandra Escalante
     * @see listarUnidadesxTipo, setUpeso
     * @version : 1.0
     */
    public void listarUnidadesxPeso(String tipo)throws SQLException{
        try{
            unidad.listarUnidadesxTipo("P");
            this.setUpeso(unidad.getUnidades());
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Getter for property udst.
     * @return Value of property udst.
     */
    public java.util.Vector getUdst() {
        return udst;
    }
    
    /**
     * Setter for property udst.
     * @param udst New value of property udst.
     */
    public void setUdst(java.util.Vector udst) {
        this.udst = udst;
    }
    
    /**
     * Getter for property upeso.
     * @return Value of property upeso.
     */
    public java.util.Vector getUpeso() {
        return upeso;
    }
    
    /**
     * Setter for property upeso.
     * @param upeso New value of property upeso.
     */
    public void setUpeso(java.util.Vector upeso) {
        this.upeso = upeso;
    }
    
    /**
     * Getter for property utmp.
     * @return Value of property utmp.
     */
    public java.util.Vector getUtmp() {
        return utmp;
    }
    
    /**
     * Setter for property utmp.
     * @param utmp New value of property utmp.
     */
    public void setUtmp(java.util.Vector utmp) {
        this.utmp = utmp;
    }
    
    /**
     * Getter for property uvol.
     * @return Value of property uvol.
     */
    public java.util.Vector getUvol() {
        return uvol;
    }
    
    /**
     * Setter for property uvol.
     * @param uvol New value of property uvol.
     */
    public void setUvol(java.util.Vector uvol) {
        this.uvol = uvol;
    }
    
    /**
     * Metodo <tt>cargarTipoUnidad</tt>, define un vector con 4 tipos de unidades 
     * @autor : Ing. Sandra Escalante
     * @version : 1.0
     */
    public void cargarTipoUnidad()throws Exception{
        this.unidad.cargarTipoUnidad();
    }
    
    /**
     * Getter for property tipos.
     * @return Value of property tipos.
     */
    public java.util.TreeMap getTipos() {
        return unidad.getTipos();
    }
    
}
