/***********************************************************************************
 * Nombre clase : ............... ActividadService.java                             *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.                                       *
 * Autor :....................... Ing. Diogenes Antonio Bastidas Morales           *
 * Fecha :........................ 25 de agosto de 2005, 11:23 AM                  *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/

package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;


public class ActividadService {
    private ActividadDAO Actd;
    /** Creates a new instance of ActividadService */
    public ActividadService() {
        Actd = new ActividadDAO();
    }
    
    /**
     * Metodo obtActividad, obtiene  objeto de tipo actividad
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public Actividad ObtActividad(){
        return Actd.obtActividad();
    }
    /**
     * Metodo obtVecActividad, retorna el vector de actividades
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public Vector ObtVecActividad (){
        return Actd.obtVecActividad();
    }
    /**
     * Metodo InsertarActividad, ingresa un registro en la tabla actividad
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void insertarActividad(Actividad act) throws SQLException {
        try{
            Actd.setActividad(act);
            Actd.insertarActividad();
        }
        catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
     /**
     * Metodo ModificarActividad, modifica un registro en la tabla actividad
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void modificarActividad(Actividad act) throws SQLException {
        try{
            Actd.setActividad(act);
            Actd.modificarActividad();
        }
        catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo ListarActividad, lista las actividades                  
     * @param:compa�ia, codigo actividad decripcion
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */ 
    public void listarActividad( ) throws SQLException {
        try{
            Actd.listarActividad();
        }
        catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
     /**
     * Metodo ListarActividad, lista las actividades                  
     * @param:compa�ia, codigo actividad decripcion
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */ 
    public void buscarActividad(String cod,String cia) throws SQLException {
        try{
            Actd.buscarActividad(cod, cia);
        }
        catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo ListarActividadxBusq, lista las actividades dependiendo al codigo de la compa�ia,
     * codigo de actividad y/o  descripcion. nota: la busqueda es con like.                                
     * @param:compa�ia, codigo actividad decripcion
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void listarActividadxBusq(String cia, String cod, String des ) throws SQLException {
        try{
            Actd.listarActividadxBusq(cia, cod, des);
        }catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo  existeActividadAnulada, retorna true o false si la actividad esta anulada.
     * @param:compa�ia, codigo actividad
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean existeActividadAnulada(String cod,String cia) throws SQLException {
        try{
            return Actd.existeActividadAnulada(cod, cia);
        }
        catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
   /**
     * Metodo  AnularActividad, anula la actividad.
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */ 
    public void anularActividad(Actividad act) throws SQLException {
        try{
            Actd.setActividad(act);
            Actd.anularActividad();
        }
        catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo  existActividad, retorna true o false si existe la actividad.
     * @param:compa�ia, codigo actividad
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean existActividad(String cod) throws SQLException{
        return Actd.existActividad(cod);
    }
}
