/**
 * Nombre        ImportacionCXPService.java
 * Descripci�n
 * Autor         Mario Fontalvo Solano
 * Fecha         16 de febrero de 2006, 10:45 AM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.ImportacionCXPDAO;
import com.tsp.operation.model.beans.Proveedor;
import com.tsp.operation.model.beans.Banco;
import com.tsp.operation.model.beans.Tipo_impuesto;
import com.tsp.operation.model.beans.CXP_Doc;
import java.util.*;

public class ImportacionCXPService {
    
    ImportacionCXPDAO ImpDataAccess;
    
    
    /** Crea una nueva instancia de  ImportacionCXPService */
    public ImportacionCXPService() {
        ImpDataAccess = new ImportacionCXPDAO();
    }
    public ImportacionCXPService(String dataBaseName) {
        ImpDataAccess = new ImportacionCXPDAO(dataBaseName);
    }
    
    
    public double searchTasa(String cia, String fecha, String MonBase, String MonConv) throws Exception {
        try{
            return ImpDataAccess.searchTasa(cia, fecha, MonBase, MonConv);
        }catch (Exception ex){
            throw new Exception("Error en searchTasa [ImportacionCXPService] ....\n" + ex.getMessage());
        }
    }
        
    public Tipo_impuesto searchValorImpuesto(String dstrct, String codigoImpuesto, String concepto, String tipo, String fecha_documento, String agencia) throws Exception {
        try{
            return ImpDataAccess.searchValorImpuesto(dstrct, codigoImpuesto, concepto, tipo, fecha_documento, agencia);
        }catch (Exception ex){
            throw new Exception("Error en searchValorImpuesto [ImportacionCXPService] ....\n" + ex.getMessage());
        }
    }
    
    public TreeMap searchTipoDocumento() throws Exception {
        try{
            return ImpDataAccess.searchTipoDocumento();
        }catch (Exception ex){
            throw new Exception("Error en searchTipoDocumento [ImportacionCXPService] ....\n" + ex.getMessage());
        }
    }
    
    public boolean searchAgencia(String id_agencia) throws Exception {
        try{
            return ImpDataAccess.searchAgencia(id_agencia);
        }catch (Exception ex){
            throw new Exception("Error en searchAgencia [ImportacionCXPService] ....\n" + ex.getMessage());
        }
    }
    
    public boolean searchPlanilla(String planilla) throws Exception {
        try{
            return ImpDataAccess.searchPlanilla(planilla);
        }catch (Exception ex){
            throw new Exception("Error en searchPlanilla [ImportacionCXPService] ....\n" + ex.getMessage());
        }
    }    
    
    public Banco searchBanco(String Distrito, String Banco, String Sucursal) throws Exception {
        try{
            return ImpDataAccess.searchBanco(Distrito, Banco, Sucursal);
        }catch (Exception ex){
            throw new Exception("Error en searchBanco [ImportacionCXPService] ....\n" + ex.getMessage());
        }
    }
    
    public Proveedor searchDatosProveedor (String dstrct, String proveedor) throws Exception{
        try{
            return ImpDataAccess.searchDatosProveedor(dstrct, proveedor);
        }catch (Exception ex){
            throw new Exception("Error en searchDatosProveedor [ImportacionCXPService] ....\n" + ex.getMessage());
        }        
    }
    
    public TreeMap searchMonedasCias () throws Exception {
        try{
            return ImpDataAccess.searchMonedasCias();
        }catch (Exception ex){
            throw new Exception("Error en searchMonedasCias [ImportacionCXPService] ....\n" + ex.getMessage());
        } 
    }
    
    public CXP_Doc searchCXPDoc (String Distrito, String Proveedor, String TipoDocumento, String Documento) throws Exception {
        try{
            return ImpDataAccess.searchCXPDoc(Distrito, Proveedor, TipoDocumento, Documento);
        }catch (Exception ex){
            throw new Exception("Error en searchCXPDoc [ImportacionCXPService] ....\n" + ex.getMessage());
        }         
    }
    
    public List searchFacturasPendientes (String usuario) throws Exception {
        try{
            return ImpDataAccess.searchFacturasPendientes(usuario);
        }catch (Exception ex){
            throw new Exception("Error en searchFacturasPendientes [ImportacionCXPService] ....\n" + ex.getMessage());
        }         
    }
    
    public void updateFacturasMigrada(CXP_Doc factura) throws Exception {
        try{
            ImpDataAccess.updateFacturasMigrada(factura);
        }catch (Exception ex){
            throw new Exception("Error en updateFacturasMigrada [ImportacionCXPService] ....\n" + ex.getMessage());
        }
    }
    
    
    public String searchCuenta(String dstrct, String cuenta, String tipoauxiliar, String auxiliar) throws Exception {
        try{
            return ImpDataAccess.searchCuenta(dstrct, cuenta, tipoauxiliar, auxiliar);
        }catch (Exception ex){
            throw new Exception("Error en searchCuenta [ImportacionCXPService] ....\n" + ex.getMessage());
        }        
    }
    
    public boolean searchABC(String abc) throws Exception {
        try{
            return ImpDataAccess.searchABC(abc);
        }catch (Exception ex){
            throw new Exception("Error en searchABC [ImportacionCXPService] ....\n" + ex.getMessage());
        }
    }    
  
    public boolean searchHC(String hc) throws Exception {
        try{
            return ImpDataAccess.searchHC(hc);
        }catch (Exception ex){
            throw new Exception("Error en searchHC [ImportacionCXPService] ....\n" + ex.getMessage());
        }
    }   
    
    public boolean searchAutorizador(String usuario) throws Exception {
        try{
            return ImpDataAccess.searchAutorizador(usuario);
        }catch (Exception ex){
            throw new Exception("Error en searchAutorizador [ImportacionCXPService] ....\n" + ex.getMessage());
        }
    }     
     public boolean searchRXPDoc (String Distrito, String Proveedor, String TipoDocumento, String Documento) throws Exception {
        try{
            return ImpDataAccess.searchRXPDoc(Distrito, Proveedor, TipoDocumento, Documento);
        }catch (Exception ex){
            throw new Exception("Error en searchRXPDoc [ImportacionCXPService] ....\n" + ex.getMessage());
        }
    }

    public List searchFacturasPendientesRXP (String usuario) throws Exception {
        try{
            return ImpDataAccess.searchFacturasPendientesRXP(usuario);
        }catch (Exception ex){
            throw new Exception("Error en searchFacturasPendientesRXP [ImportacionCXPService] ....\n" + ex.getMessage());
        }
    }
}
