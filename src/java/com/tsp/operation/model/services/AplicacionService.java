/********************************************************************
 *      Nombre Clase.................   AplicacionService.java    
 *      Descripci�n..................   Service de la tabla tblapp    
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  Andres
 */
public class AplicacionService {
        
        private AplicacionDAO app;
        
        private String  varSeparadorJS = "~";
        private String  varCamposJS;
        
        /** Creates a new instance of AplicacionService */
        public AplicacionService() {
                this.app = new AplicacionDAO();
        }
        
        /**
         * Inserta un nuevo registro.
         * @autor Tito Andr�s Maturana 
         * @param app Objeto de la clase Aplicacion a actualizar
         * @throws SQLException
         * @version 1.0
         */
        public void insertarAplicacion(Aplicacion app) throws SQLException{
                this.app.setApp(app);
                this.app.insertar();
        }
        
        /**
         * Actualiza un registro
         * @autor Tito Andr�s Maturana
         * @param app Objeto de la clase Aplicacion a actualizar
         * @throws SQLException
         * @version 1.0
         */
        public void actualizarAplicacion(Aplicacion app) throws SQLException{
                this.app.setApp(app);
                this.app.actualizar();
        }
        
        /**
         * Anula un registro. Establece el reg_status en 'A'.
         * @autor Tito Andr�s Maturana
         * @param cia Distrito
         * @param codigo Codigo de la aplicaci�n
         * @throws SQLException
         * @version 1.0
         */
        public void anularAplicacion(String cia, String codigo) throws SQLException{
                this.app.anular(cia, codigo);
        }
        
        /**
         * Obtiene un registro
         * @autor Tito Andr�s Maturana
         * @param cia Distrito
         * @param codigo Codigo de la aplicaci�n
         * @throws SQLException
         * @version 1.0
         */
        public void obtenerAplicacion(String cia, String codigo) throws SQLException{
                this.app.obtener(cia, codigo);
        }
        
        /**
         * Obtiene la propiedad app.
         * @autor Tito Andr�s Maturana
         * @throws SQLException
         * @version 1.0
         */
        public Aplicacion getAplicacion() throws SQLException{
                return this.app.getApp();
        }
        
        /**
         * Establece la propiedad app.
         * @autor Tito Andr�s Maturana
         * @param app Nuevo valor de la propiedad app
         * @throws SQLException
         * @version 1.0
         */
        public void setAplicacion(Aplicacion app) throws SQLException{
                this.app.setApp(app);
        }
        
        /**
         * Obtiene la propiedad apps.
         * @autor Tito Andr�s Maturana
         * @throws SQLException
         * @version 1.0
         */
        public Vector obtenerAplicaciones() throws SQLException{
                this.app.listar();
                return this.app.getApps();
        }
        
        /**
         * Establece la propiedad apps.
         * @autor Tito Andr�s Maturana
         * @param app Nuevo valor de la propiedad app
         * @throws SQLException
         * @version 1.0
         */
        public Vector obtenerAplicaciones(String distrito) throws SQLException{
                this.app.listar(distrito);
                return this.app.getApps();
        }
        
        
        /**
         * Genera una cadena de caracteres de la declaraci�n de
         * un arreglo en lenguaje JavaScript a partir del listado de
         * aplicaciones de un distrito.
         * @autor Tito Andr�s Maturana
         * @param distrito Distrito de las aplicaciones.
         * @throws SQLException
         * @version 1.0
         */
        public void GenerarJSCampos(String distrito) throws SQLException{
                String var = "\n var CamposJSAp = [ ";
                Vector apps = this.obtenerAplicaciones(distrito);
                if (apps!=null)
                        for (int i=0; i<apps.size(); i++){
                                Aplicacion app = (Aplicacion) apps.elementAt(i);
                                var += "\n '" + app.getC_codigo() + varSeparadorJS + app.getC_descripcion().trim() +"',";
                        }
                var = var.substring(0,var.length()-1) + "];";
                this.setAplicacion(null);
                
                varCamposJS = var;
        }
        
        /**
         * Obtiene una cadena de caracteres de la declaraci�n de
         * un arreglo en lenguaje JavaScript a partir del listado de
         * aplicaciones de un distrito.
         * @autor Tito Andr�s Maturana
         * @param distrito Distrito de las aplicaciones.
         * @throws SQLException
         * @version 1.0
         */
        public String obtenerJSCampos(String distrito) throws SQLException{
                this.GenerarJSCampos(distrito);
                this.setAplicacion(null);
                return this.getVarCamposJS();
        }
        
        
        /**
         * Getter for property varCamposJS.
         * @return Value of property varCamposJS.
         */
        public java.lang.String getVarCamposJS() {
                return varCamposJS;
        }
        
        /**
         * Setter for property varCamposJS.
         * @param varCamposJS New value of property varCamposJS.
         */
        public void setVarCamposJS(java.lang.String varCamposJS) {
                this.varCamposJS = varCamposJS;
        }
        
        /**
         * Getter for property varSeparadorJS.
         * @return Value of property varSeparadorJS.
         */
        public java.lang.String getVarSeparadorJS() {
                return varSeparadorJS;
        }
        
        /**
         * Setter for property varSeparadorJS.
         * @param varSeparadorJS New value of property varSeparadorJS.
         */
        public void setVarSeparadorJS(java.lang.String varSeparadorJS) {
                this.varSeparadorJS = varSeparadorJS;
        }
        
}
