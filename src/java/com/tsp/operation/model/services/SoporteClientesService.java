/*********************************************************************
 * Nombre      ............... SoporteClientesService.java
 * Descripcion ............... Clase de la relacion entre los diferentes tipos
 *                             de soportes y sus respectivos clientes
 * Autor       ............... mfontalvo
 * Fecha       ............... Diciembre - 21 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 *********************************************************************/

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.SoporteClientesDAO;
import com.tsp.operation.model.beans.General;
import java.util.*;

public class SoporteClientesService {
    
    // Declaracion de elementos de acceso a la base de datos
    SoporteClientesDAO dao;
    
    
    // Declaracion de elementos de Retorno
    List ListaClientes;
    List ListaSoportes;
    List ListaRelacion;
    List ListaAgencias;
    
    Vector soportes;
    
    /** Creates a new instance of SoporteClientesService */
    public SoporteClientesService() {
        dao = new SoporteClientesDAO();
    }
    
    
    
    /**
     * Procedimiento para consultar los clientes en la base de datos
     * @autor  mfontalvo
     * @see dao.EXECUTE_QUERY de acceso a la base de datos
     * @throws Exception.
     **/
    
    
    public void buscarClientes()throws Exception{
        try{
            ListaClientes = dao.EXECUTE_QUERY(dao.SELECT_CLIENTES,  null );
        }catch (Exception ex){
            throw new Exception("Error en buscarClientes [SoporteClientesService] ... \n"+ex.getMessage());
        }
    }
    
    
    /* Procedimiento para consultar los soportes en la base de datos
     * @autor  mfontalvo
     * @see dao.EXECUTE_QUERY de acceso a la base de datos
     * @throws Exception.
     **/
    
    public void buscarSoportes()throws Exception{
        try{
            ListaSoportes = dao.EXECUTE_QUERY(dao.SELECT_SOPORTE,  null );
        }catch (Exception ex){
            throw new Exception("Error en buscarSoportes [SoporteClientesService] ... \n"+ex.getMessage());
        }
    }
    
    
    /* Procedimiento para consultar las relaciones entre soportes y clientes
     * @autor  mfontalvo
     * @see dao.EXECUTE_QUERY de acceso a la base de datos
     * @throws Exception.
     **/
    
    public void buscarRelaciones()throws Exception{
        try{
            ListaRelacion = dao.EXECUTE_QUERY(dao.SELECT_RELACION, null );
        }catch (Exception ex){
            throw new Exception("Error en buscarRelaciones [SoporteClientesService] ... \n"+ex.getMessage());
        }
    }
    
    
    
    /* Procedimiento para consultar las relaciones entre soportes y clientes
     * @autor  mfontalvo
     * @see dao.EXECUTE_QUERY de acceso a la base de datos
     * @throws Exception.
     **/
    
    public void buscarAgencias()throws Exception{
        try{
            ListaAgencias = dao.EXECUTE_QUERY(dao.SELECT_AGENCIAS, null );
        }catch (Exception ex){
            throw new Exception("Error en buscarRelaciones [SoporteClientesService] ... \n"+ex.getMessage());
        }
    }
    
    
    /* Procedimiento para adicionar la relacion entre Soportes y Clientes
     * @autor  mfontalvo
     * @params soportes [] ..... Listado de Soportes a relacionar
     * @params clientes [] ..... Listado de Clientes a relacionar
     * @params usuarios   ...... Usuario en session
     * @see dao.addRelacionSoporteCliente de acceso a la base de datos
     * @throws Exception.
     **/
    
    public void addRelacion(String []soportes, String []clientes, String usuario )throws Exception{
        try{
            dao.addRelacionSoporteCliente(soportes, clientes, usuario);
        }catch (Exception ex){
            throw new Exception("Error en addRelacion [SoporteClientesService] ... \n"+ex.getMessage());
        }
    }
    
    
    
    /* Procedimiento que retorna el listado de relaciones */
    public List getListaRelacion(){
        return ListaRelacion;
    }
    
    /* Procedimiento que retorna el listado de Soportes */
    public List getListaSoportes(){
        return ListaSoportes;
    }
    
    /* Procedimiento que retorna el listado de Clientes */
    public List getListaClientes(){
        return ListaClientes;
    }    
    
    /* Procedimiento que retorna el listado de Agencias */
    public List getListaAgencias(){
        return ListaAgencias;
    }    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * Extraccion de soportes por remesa segun los criterios de 
     * tipo (Envio, Recibido) 
     * clase (Fisica, Logica, Ambas)
     */
    public void obtenerSorportes_x_Remesa(String remesa , String tipo, String clase) throws Exception{
        soportes = dao.obtenerSorportes_x_Remesa(remesa, tipo, clase);
    }
    
    /**
     * Extraccion de soportes por cliente en un rango de fechas de cumplido 
     * segun los criterios de 
     * tipo (Envio, Recibido) 
     * clase (Fisica, Logica, Ambas)
     */
    public void obtenerSorportes_x_Cliente(String cliente, String fi, String ff, String tipo, String clase) throws Exception{
        soportes = dao.obtenerSorportes_x_Cliente(cliente, fi, ff, tipo, clase);
    }
    
    /**
     * Extraccion de soportes por agencia facturacion en un rango de fechas de cumplido 
     * segun los criterios de 
     * tipo (Envio, Recibido) 
     * clase (Fisica, Logica, Ambas)
     */
    public void obtenerSorportes_x_AgenciaFacturadora(String agencia, String fi, String ff, String tipo, String clase) throws Exception{
        soportes = dao.obtenerSorportes_x_AgenciaFacturadora(agencia, fi, ff, tipo, clase);
    }
    
    
    /**
     * Metodo para convertir una lista en un combo;
     * @param lista
     */
    public String obtenerCombo(List lista, String attributes , String selected)throws Exception{
        try {
            String combo = "<select "+ attributes +"><option selected value=''></option>";
            if (lista!=null && !lista.isEmpty()){
                for (int i =0 ; i< lista.size() ; i++){
                    General g = (General) lista.get(i);
                    combo += "<option value='" + com.tsp.util.Util.coalesce(g.getCodigo(),"") + "' " +
                    (com.tsp.util.Util.coalesce(g.getCodigo(),"").equals(selected)?" selected ":"") +">" + 
                    com.tsp.util.Util.coalesce(g.getDescripcion(),"") + "</option>";
                }
            } 
            combo += "</select>";
            return combo;
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    
    
    
    /**
     * Metodo, que modifica las fechas de envio y recibido tanto
     * fisico como logico y agencia de envio de los soportes de
     * una remesa
     * @fecha 2006-01-18
     * @autor mfontalvo
     * @param Remesa, que se va a actualizar
     * @param Soporte, que se va a actualizar
     * @param editAGE, indica si se puede modificar o no la agencia
     * @param Agencia, nueva agencia de envio
     * @param editFEF, indica si se puede modificar o no la fecha de envio fisico
     * @param fechaEnvioFisico, nueva fecha de envio fisico
     * @param editFRF, indica si se puede modificar o no la fecha de recibido fisico
     * @param fechaRecibidoFisico, nueva fecha de recibido fisico
     * @param editFEL, indica si se puede modificar o no la fecha de envio logico
     * @param fechaEnvioLogico, nueva fecha de envio logico
     * @param editFRL, indica si se puede modificar o no la fecha de recibido logico
     * @param fechaRecibidoLogico, nueva fecha de recibido logico
     * @param Usuario, que registra el envio
     * @throws Exception.
     * @version : 1.0
     */
    
    public void ActualizarFechas(
    String Remesa,
    String Soporte,
    boolean editAGE, String Agencia,
    boolean editFEF, String fechaEnvioFisico,
    boolean editFRF, String fechaRecibidoFisico,
    boolean editFEL, String fechaEnvioLogico,
    boolean editFRL, String fechaRecibidoLogico,
    String Usuario
    )throws Exception{
        dao.ActualizarFechas(Remesa, Soporte, editAGE, Agencia, editFEF, fechaEnvioFisico, editFRF, fechaRecibidoFisico, editFEL, fechaEnvioLogico, editFRL, fechaRecibidoLogico, Usuario);
    }
    /**
     * Getter for property soportes.
     * @return Value of property soportes.
     */
    public java.util.Vector getSoportes() {
        return soportes;
    }
    
    /**
     * Setter for property soportes.
     * @param soportes New value of property soportes.
     */
    public void setSoportes(java.util.Vector soportes) {
        this.soportes = soportes;
    }
    

    
    /**
     * Extraccion de soportes por remesa segun los criterios de 
     * tipo (Envio, Recibido) 
     * clase (Fisica, Logica, Ambas)
     */
    public String obtenerCausa(String remesa , String tipo, String clase) throws Exception{
        return dao.obtenerCausa(remesa, tipo, clase);
    }  
    
    /**
     * Metodo,que obtiene el listado de documentos soporte de una remesa
     * @autor mfontalvo
     * @param Remesa, filtro a consultar    
     * @throws Exception.
     * @return List, Listado tipo de documentos soportes
     * @version : 1.0
     */
    public Vector obtenerSoportesRemesa(String remesa)throws Exception{
        return dao.obtenerSoportesRemesa(remesa);
    }
    
}
